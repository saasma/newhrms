<%@ Page Title="Year End Tax Report" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="YearEndTaxReport.aspx.cs" Inherits="Web.YearEndTaxReport" %>

<%@ Import Namespace="BLL.Manager" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Scripts/ScrollTableHeader103.min.js?v=200" type="text/javascript"></script>
    <script type="text/javascript">

        var skipLoadingCheck = true;

        $(document).ready(
            function () {

                $('.tableLightColor tbody').attr('id', 'scrollMe');
                var sth = new ScrollTableHeader();

                sth.addTbody("scrollMe");
                sth.delayAfterScroll = 150;
                sth.minTableRows = 10;
            }
        );
    </script>
    <style type="text/css">
        .tableLightColor a:hover
        {
            color: #048FC2;
            text-decoration: non;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
  
         <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                   Year End Tax Report
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
    <div class="attribute" style='padding:10px;'>
        <table>
            <tr>
                <td>
                    <asp:DropDownList ID="ddlYear" AutoPostBack="true" runat="server" AppendDataBoundItems="true"
                        Width="200px" DataValueField="FinancialDateId" DataTextField="Name">
                        <asp:ListItem Text="--Select--" Value="-1" />
                    </asp:DropDownList>
                </td>
                <td style="padding-left: 800px;">
                    <asp:Button ID="btnUpdate" CssClass="excel" runat="server" Text="Export" OnClick="btnUpdate_Click" />
                </td>
            </tr>
        </table>
    </div>
    <div class="clear">
        <cc2:EmptyDisplayGridView CssClass="tableLightColor" UseAccessibleHeader="true" ShowHeaderWhenEmpty="True"
            ID="gvEmployeeIncome" runat="server" DataKeyNames="EmployeeId" AutoGenerateColumns="False"
            CellPadding="4" GridLines="None" AllowSorting="True" ShowFooterWhenEmpty="False">
            <RowStyle BackColor="#E3EAEB" />
            <Columns>
                <asp:BoundField DataField="EmployeeId" HeaderText="EIN" DataFormatString="{0:000}">
                </asp:BoundField>
                <asp:TemplateField HeaderText="Name" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <asp:HyperLink CssClass="employeeClass" Target="_blank" Width="150px" ID="Label2"
                            runat="server" NavigateUrl='<%# "~/CP/Report/EmployeeListDetails.aspx?Id=" +  Eval("EmployeeId") %>'
                            Text='<%# Eval("Name") %>'></asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField ControlStyle-Width="90px" DataField="SST1" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="SST1" />
                <asp:BoundField DataField="TDS1" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="TDS1" />
                <asp:BoundField DataField="AddOnTDS1" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="AddOnTDS1" />
                <asp:BoundField DataField="SSTTaxAmount1" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="SSTTaxAmount1" />
                <asp:BoundField DataField="TDSTaxAmount1" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="TDSTaxAmount1" />
                <asp:BoundField DataField="SST2" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="SST2" />
                <asp:BoundField DataField="TDS2" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="TDS2" />
                <asp:BoundField DataField="AddOnTDS2" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="AddOnTDS2" />
                <asp:BoundField DataField="SSTTaxAmount2" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="SSTTaxAmount2" />
                <asp:BoundField DataField="TDSTaxAmount2" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="TDSTaxAmount2" />
                <asp:BoundField DataField="SST3" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="SST3" />
                <asp:BoundField DataField="TDS3" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="TDS3" />
                <asp:BoundField DataField="AddOnTDS3" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="AddOnTDS3" />
                <asp:BoundField DataField="SSTTaxAmount3" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="SSTTaxAmount3" />
                <asp:BoundField DataField="TDSTaxAmount3" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="TDSTaxAmount3" />
                <asp:BoundField DataField="SST4" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="SST4" />
                <asp:BoundField DataField="TDS4" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="TDS4" />
                <asp:BoundField DataField="AddOnTDS4" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="AddOnTDS4" />
                <asp:BoundField DataField="SSTTaxAmount4" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="SSTTaxAmount4" />
                <asp:BoundField DataField="TDSTaxAmount4" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="TDSTaxAmount4" />
                <asp:BoundField DataField="SST5" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="SST5" />
                <asp:BoundField DataField="TDS5" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="TDS5" />
                <asp:BoundField DataField="AddOnTDS5" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="AddOnTDS5" />
               <asp:BoundField DataField="SSTTaxAmount5" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="SSTTaxAmount5" />
                <asp:BoundField DataField="TDSTaxAmount5" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="TDSTaxAmount5" />
                <asp:BoundField DataField="SST6" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="SST6" />
                <asp:BoundField DataField="TDS6" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="TDS6" />
                <asp:BoundField DataField="AddOnTDS6" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="AddOnTDS6" />
                <asp:BoundField DataField="SSTTaxAmount6" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="SSTTaxAmount6" />
                <asp:BoundField DataField="TDSTaxAmount6" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="TDSTaxAmount6" />
                <asp:BoundField DataField="SST7" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="SST7" />
                <asp:BoundField DataField="TDS7" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="TDS7" />
                <asp:BoundField DataField="AddOnTDS7" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="AddOnTDS7" />
               <asp:BoundField DataField="SSTTaxAmount7" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="SSTTaxAmount7" />
                <asp:BoundField DataField="TDSTaxAmount7" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="TDSTaxAmount7" />
                <asp:BoundField DataField="SST8" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="SST8" />
                <asp:BoundField DataField="TDS8" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="TDS8" />
                <asp:BoundField DataField="AddOnTDS8" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="AddOnTDS8" />
                <asp:BoundField DataField="SSTTaxAmount8" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="SSTTaxAmount8" />
                <asp:BoundField DataField="TDSTaxAmount8" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="TDSTaxAmount8" />
                <asp:BoundField DataField="SST9" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="SST9" />
                <asp:BoundField DataField="TDS9" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="TDS9" />
                <asp:BoundField DataField="AddOnTDS9" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="AddOnTDS9" />
              <asp:BoundField DataField="SSTTaxAmount9" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="SSTTaxAmount9" />
                <asp:BoundField DataField="TDSTaxAmount9" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="TDSTaxAmount9" />
                <asp:BoundField DataField="SST10" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="SST10" />
                <asp:BoundField DataField="TDS10" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="TDS10" />
                <asp:BoundField DataField="AddOnTDS10" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="AddOnTDS10" />
                <asp:BoundField DataField="SSTTaxAmount10" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="SSTTaxAmount10" />
                <asp:BoundField DataField="TDSTaxAmount10" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="TDSTaxAmount10" />
                <asp:BoundField DataField="SST11" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="SST11" />
                <asp:BoundField DataField="TDS11" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="TDS11" />
                <asp:BoundField DataField="AddOnTDS11" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="AddOnTDS11" />
                <asp:BoundField DataField="SSTTaxAmount11" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="SSTTaxAmount11" />
                <asp:BoundField DataField="TDSTaxAmount11" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="TDSTaxAmount11" />
                <asp:BoundField DataField="SST12" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="SST12" />
                <asp:BoundField DataField="TDS12" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="TDS12" />
                <asp:BoundField DataField="AddOnSST12" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="AddOnSST11" />
                <asp:BoundField DataField="AddOnTDS12" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="AddOnTDS12" />
               <asp:BoundField DataField="SSTTaxAmount12" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="SSTTaxAmount12" />
                <asp:BoundField DataField="TDSTaxAmount12" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="TDSTaxAmount12" />
                <asp:BoundField DataField="TotalSST" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="Total SST" />
                <asp:BoundField DataField="TotalTDS" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="Total TDS" />
                 <asp:BoundField DataField="TotalAddOnSST" HeaderStyle-HorizontalAlign="Left"  DataFormatString="{0:N2}"
                    HeaderText="Add Total SST" />
                <asp:BoundField DataField="TotalAddOnTDS" HeaderStyle-HorizontalAlign="Left"  DataFormatString="{0:N2}"
                    HeaderText="Add Total TDS" />
                <asp:BoundField DataField="SSTTotalAmount" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="SST Total Amount" />
                <asp:BoundField DataField="TDSTotalAmount" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:N2}"
                    HeaderText="TDS Total Amount" />
            </Columns>
            <RowStyle CssClass="odd" />
            <HeaderStyle Font-Bold="true" />
            <AlternatingRowStyle CssClass="even" />
            <EmptyDataTemplate>
                <b>No employee list. </b>
            </EmptyDataTemplate>
        </cc2:EmptyDisplayGridView>
    </div>
  </div>

</asp:Content>
