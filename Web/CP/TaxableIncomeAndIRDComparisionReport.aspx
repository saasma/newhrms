<%@ Page Title="Taxable Income and IRD Deposited" Language="C#" EnableViewState="true" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="TaxableIncomeAndIRDComparisionReport.aspx.cs" Inherits="Web.TaxableIncomeAndIRDComparisionReport" %>

<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<%@ Import Namespace="BLL.Manager" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        var skipLoadingCheck = true;

        var selEmpId = null;
        function ACE_item_selected(source, eventArgs) {
            var value = eventArgs.get_value();
            selEmpId = value;
            document.getElementById('<%= hiddenEmployeeID.ClientID %>').value = selEmpId;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Taxable Income and IRD Deposited Income Comparision Report
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="attribute">
            <table cellpadding="3" cellspacing="0" class="fieldTable">
                <tr>
                    <td class="filterHeader" runat="server" id="rowPayrollFrom1">
                        <strong>Payroll Period</strong>
                    </td>
                    <td class="filterHeader" runat="server" id="Td2" style="display:none">
                        <strong>Branch</strong>
                    </td>
                    <td class="filterHeader" runat="server" id="Td1">
                        <strong>Employee</strong>
                    </td>
                    <td rowspan="2" valign="bottom"  >
                        <asp:CheckBox runat="server" ID="chkHideRetired" Text="Hide Retired Employees" />
                    </td>
                    <td rowspan="2" valign="bottom">
                        <asp:Button ID="btnLoad" Style='float: left; width: 80px;' CssClass="btn btn-default btn-sm btn-sect"
                            runat="server" Text="Load" />
                    </td>
                </tr>
                <tr>
                    <td runat="server" id="rowPayrollFrom2">
                        <asp:DropDownList ID="ddlPayrollFromMonth" runat="server">
                        </asp:DropDownList>
                        <asp:DropDownList ID="ddlPayrollFromYear" runat="server">
                        </asp:DropDownList>
                    </td>
                    <td  style="display:none">

                        <asp:DropDownList ID="ddlBranch" Width="120px" DataTextField="Name" DataValueField="BranchId"
                                AppendDataBoundItems="true" runat="server">
                                <asp:ListItem Text="" Value="-1" />
                            </asp:DropDownList>
                    </td>
                    <td valign="bottom">
                        <asp:HiddenField ID="hiddenEmployeeID" runat="server" />
                        <asp:TextBox ID="txtEmpSearch" runat="server" AutoPostBack="true" Width="160" />
                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtEmpSearch"
                            WatermarkText="Search Employee" WatermarkCssClass="searchBoxText" />
                        <cc1:AutoCompleteExtender EnableCaching="true" ID="AutoCompleteExtenderOrganization"
                            runat="server" MinimumPrefixLength="2" ServiceMethod="GetEmployeeNamesAndEINWithRetiredAlso"
                            ServicePath="~/PayrollService.asmx" TargetControlID="txtEmpSearch" OnClientItemSelected="ACE_item_selected"
                            CompletionSetCount="10" CompletionInterval="250" CompletionListCssClass="AutoExtender"
                            CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
                        </cc1:AutoCompleteExtender>
                    </td>
                </tr>
            </table>
        </div>

        <div class="clear gridBlock">
            <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
            <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
            <cc2:EmptyDisplayGridView EnableViewState="false" CssClass="table table-primary mb30 table-bordered table-hover"
                UseAccessibleHeader="true" ShowHeaderWhenEmpty="True" ID="gvEmployeeIncome" runat="server"
                DataKeyNames="EIN" AutoGenerateColumns="False" CellPadding="4" GridLines="None"
                ShowFooterWhenEmpty="False" OnDataBound="gvw_DataBound">
                <Columns>
                    <asp:BoundField DataField="RowNum" HeaderText="SN"></asp:BoundField>
                    <asp:BoundField DataField="EIN" HeaderText="EIN"></asp:BoundField>
                    <asp:BoundField DataField="INo" HeaderText="INo"></asp:BoundField>
                    <asp:BoundField DataField="Name" HeaderText="Name"></asp:BoundField>
                    
                    <asp:BoundField DataField="AccountNo" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="Account No"></asp:BoundField>
                       
                    <asp:BoundField DataField="TotalTaxableIncome" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="Total Taxable Income" DataFormatString="{0:N2}" />
                    <asp:BoundField DataField="SSTIncome" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="SST Income" DataFormatString="{0:N2}" />
                    <asp:BoundField DataField="TDSIncome" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="TDS Income" DataFormatString="{0:N2}" />
                    <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="TotalSST"
                        DataFormatString="{0:N2}" HeaderStyle-HorizontalAlign="Left" HeaderText="Total SST" />
                    <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="TotalTDS"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="Total TDS" DataFormatString="{0:N2}" />
                    <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="SSTIncomeDeposited"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="SST Income Deposited" DataFormatString="{0:N2}" />
                     <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="SSTDeposited"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="SST Deposited" DataFormatString="{0:N2}" />
                    <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="TDSIncomeDeposited"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="TDS Income Deposited" DataFormatString="{0:N2}" />


                    <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="TDSDeposited"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="TDS Deposited" DataFormatString="{0:N2}" />
                    <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="RemainingSSTIncome"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="Remaining SST Income" DataFormatString="{0:N2}" />
                    <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="RemainingSST"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="Remaining SST" DataFormatString="{0:N2}" />
                    <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="RemainingTDSIncome"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="Remaining TDS Income" DataFormatString="{0:N2}" />

                    <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="RemainingTDS"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="Remaining TDS" DataFormatString="{0:N2}" />
                </Columns>
                <EmptyDataTemplate>
                    <b>No employee list. </b>
                </EmptyDataTemplate>
            </cc2:EmptyDisplayGridView>
        </div>
        <div class="buttonsDiv">
            <asp:Button ID="btnUpdate" CssClass="excel" runat="server" Text="Export" OnClick="btnUpdate_Click" />
        </div>
    </div>
</asp:Content>
