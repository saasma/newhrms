﻿<%@ Page Title="Change medical tax credit" Language="C#" MasterPageFile="~/Master/ForPopupPage.Master"
    AutoEventWireup="true" CodeBehind="AEMedicalTax.aspx.cs" Inherits="Web.CP.AEMedicalTax" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">
        window.onunload = closePopup;
        function closePopup() {
            clearUnload();
          

            //if ($.browser.msie == false && typeof (window.opener.parentReloadCallbackFunction) != 'undefined' && ($.browser.chrome || $.browser.safari)) {
                window.opener.parentReloadCallbackFunction(window, Changed);
//            } else {
//                if (typeof (Changed) != 'undefined')
//                    window.returnValue = Changed;
//                window.close();
//            }

        }

        function clearUnload() {
            window.onunload = null;
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <div class="popupHeader">
        <h2 class="headlinespop">
            Medical Tax Credit</h2>
    </div>
    <fieldset style='padding-top: 20px'>
        
        <table>
            <tr>
                <td class="lf">
                    Employee
                </td>
                <td>
                    <asp:TextBox ID="txtEmployee" runat="server" Enabled="False"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="lf">
                    Last year amount
                </td>
                <td>
                    <asp:TextBox ID="txtPYUnAdjAmount" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="valReqdName" runat="server" ControlToValidate="txtPYUnAdjAmount"
                        Display="None" ErrorMessage="P/Y Unadjusted amount is required." ValidationGroup="AETaxCredit1"></asp:RequiredFieldValidator>
                    <asp:CompareValidator Operator="GreaterThanEqual" ValueToCompare="0" ValidationGroup="AETaxCredit1"
                        Type="Double" ID="valCompAmt1" Display="None" ControlToValidate="txtPYUnAdjAmount"
                        runat="server" ErrorMessage="Invalid P/Y Unadjusted amount."></asp:CompareValidator>
                </td>
            </tr>
        </table>
        <asp:Button ID="btnUpdatePYAmt" CssClass="update" OnClientClick="valGroup='AETaxCredit1';return CheckValidation()"
            runat="server" Text="Update" OnClick="btnUpdatePYAmt_Click" />
    </fieldset>
    <fieldset>
        <legend><b>Details</b></legend>
        <cc2:EmptyDisplayGridView CssClass="tableLightColor" UseAccessibleHeader="true" ID="gvw"
            ShowHeaderWhenEmpty="True" runat="server" AutoGenerateColumns="False" DataKeyNames="MedicalTaxCreditDetailId"
            ShowFooterWhenEmpty="False" GridLines="None" OnSelectedIndexChanged="gvw_SelectedIndexChanged"
              OnRowEditing="gvw_Editing" OnRowDeleting="gvw_Deleting" >
            <Columns>
                <asp:BoundField DataField="Date" HeaderText="Date" />
                <asp:BoundField DataField="RefNo" HeaderText="Ref No" />
                <asp:BoundField DataField="Institution" HeaderText="Institution" />
                <asp:BoundField DataField="Amount" DataFormatString="{0:N2}" HeaderText="Amount" />
                <asp:BoundField DataField="Notes" HeaderText="Notes" HeaderStyle-Width="200px">
                    <HeaderStyle Width="200px"></HeaderStyle>
                </asp:BoundField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:ImageButton ID="ImageButton1" OnClientClick="clearUnload()" ImageUrl="~/images/edit.gif"
                            CommandName="Select" runat="server" />
                             <asp:ImageButton ID="ImageButton2" OnClientClick="if( confirm('Are your sure, you want to delete the item?') ) {clearUnload();return true;} else return false;"  ImageUrl="~/images/delete.gif"
                            CommandName="Delete" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <RowStyle CssClass="odd" />
            <AlternatingRowStyle CssClass="even" />
            <EmptyDataTemplate>
                No medical tax credit details.
            </EmptyDataTemplate>
        </cc2:EmptyDisplayGridView>
        <br />
        <table>
            <tr>
                <td class="lf">
                    Date
                </td>
                <td>
                    <My:Calendar runat="server" Id="calDate" />
                </td>
            </tr>
            <tr>
                <td class="lf">
                    Ref No
                </td>
                <td>
                    <asp:TextBox ID="txtRefNo" Width="180px" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="lf">
                    Institution
                </td>
                <td>
                    <asp:TextBox ID="txtInstitution" Width="180px"  runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="lf">
                    Amount
                </td>
                <td>
                    <asp:TextBox ID="txtAmount" Width="180px"  runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtAmount"
                        Display="None" ErrorMessage="Amount is required." ValidationGroup="AETaxCredit"></asp:RequiredFieldValidator>
                    <asp:CompareValidator Operator="GreaterThanEqual" ValueToCompare="1" ValidationGroup="AETaxCredit"
                        Type="Double" ID="CompareValidator1" Display="None" ControlToValidate="txtAmount"
                        runat="server" ErrorMessage="Invalid amount."></asp:CompareValidator>
                </td>
            </tr>
            <tr>
                <td class="lf">
                    Notes
                </td>
                <td>
                    <asp:TextBox ID="txtNotes" Width="180px"  TextMode="MultiLine" Rows="4" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Button ID="btnSave" CssClass="save" OnClientClick="valGroup='AETaxCredit';return CheckValidation()"
                        runat="server" Text="Save" OnClick="btnSave_Click" />
                    &nbsp;
                    <asp:Button ID="Button1" CssClass="cancel" runat="server" OnClientClick="clearUnload()" Text="Cancel"
                        OnClick="Button1_Click" />
                </td>
            </tr>
        </table>
    </fieldset>
</asp:Content>
