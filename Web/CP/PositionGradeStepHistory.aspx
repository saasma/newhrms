<%@ Page Title="Position/Grade History" Language="C#" MasterPageFile="~/Master/ForPopupPage.Master" AutoEventWireup="true"
    CodeBehind="PositionGradeStepHistory.aspx.cs" Inherits="Web.CP.PositionGradeStepHistory" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">
        function closePopup() {            
            window.returnValue = value;
            window.close();          
            
        }
        <%Page.Response.Write("window.onunload = closePopup;");%>        
         function clearUnload() {
            window.onunload = null;
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <div class="popupHeader">
       
          <h3>  Position/Grade history</h3>
    </div>
    <div class="marginal">
        <asp:GridView ID="gvw" Width="100%" DataKeyNames="PositionId" PagerStyle-HorizontalAlign="Center"
            PagerStyle-CssClass="defaultPagingBar" AllowPaging="true" runat="server" AutoGenerateColumns="False"
            CellPadding="4" GridLines="None" PageSize="10" 
            OnPageIndexChanging="gvw_PageIndexChanging" >
            <Columns>
                 
                <asp:TemplateField HeaderText="Position" HeaderStyle-Width="120px" HeaderStyle-HorizontalAlign="Left"
                    ItemStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <%# Eval("PositionName")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Grade" HeaderStyle-Width="120px" HeaderStyle-HorizontalAlign="Left"
                    ItemStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <%# Eval("GradeName")%>
                    </ItemTemplate>
                </asp:TemplateField>

<asp:TemplateField HeaderText="Step" HeaderStyle-Width="120px" HeaderStyle-HorizontalAlign="Left"
                    ItemStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <%# Eval("StepName")%>
                    </ItemTemplate>
                </asp:TemplateField>


                <asp:TemplateField HeaderText="Effective Till" HeaderStyle-Width="120px" HeaderStyle-HorizontalAlign="Left"
                    ItemStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <%# GetPrevDate(Eval("EffectiveFromDate"))%>
                    </ItemTemplate>
                </asp:TemplateField>

            </Columns>
            <RowStyle CssClass="odd" />
            <AlternatingRowStyle CssClass="even" />
            <SelectedRowStyle CssClass="selected" />
            <PagerStyle HorizontalAlign="Center" CssClass="defaultPagingBar"></PagerStyle>
            <EmptyDataTemplate>
                <b>No Position/Grade history. </b>
            </EmptyDataTemplate>
        </asp:GridView>
          <uc2:MsgCtl Width='' ID="divMsgCtl"
            EnableViewState="false" Hide="true" runat="server" />
        <uc2:WarningCtl Width='' ID="divWarningMsg"
            EnableViewState="false" Hide="true" runat="server" />
    </div>
</asp:Content>
