﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using System.Text;
using BLL.Manager;
using Utils.Helper;
using DAL;
using System.Drawing;
using BLL.Base;


namespace Web.CP
{
    public partial class BranchTransferNew : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.HumanResource;
            }
        }
        TaxManager taxMgr = new TaxManager();
        protected void Page_Load(object sender, EventArgs e)
        {          
            if (!IsPostBack)
            {
                pagingCtl.Visible = false;
                Initialise();

            }

            if (Request.Form["__EVENTTARGET"] != null && Request.Form["__EVENTTARGET"].Equals("Reload"))
                LoadTaxCreditList();

            JavascriptHelper.AttachPopUpCode(Page, "popupBrTransferDetails", "BrTransferDetails.aspx", 650, 590);

            JavascriptHelper.AttachPopUpCode(Page, "popup", "BranchTransferLeaveSetting.aspx", 1200, 700);
            
            //JavascriptHelper.AttachPopUpCode(Page, "medicalTaxCreditPopup", "AEMedicalTax.aspx", 600, 590);
            //JavascriptHelper.AttachNonDialogPopUpCode(Page, "medicalTaxImportPopup", "../ExcelWindow/MedicalTaxDetailExcel.aspx", 450, 500);

        }

        public bool IsTeamColumnVisible()
        {
            if (CommonManager.Setting.LeaveRequestSettingUsingPreDefinedList != null &&
                CommonManager.Setting.LeaveRequestSettingUsingPreDefinedList.Value)
                return false;

            return true;
        }

        void Initialise()
        {
            int empId = UrlHelper.GetIdFromQueryString("EmpId");
            if (empId != 0)
            {
                EEmployee emp = EmployeeManager.GetEmployeeById(empId);
                if (emp != null)
                {
                    txtEmpSearchText.Text = emp.Name;
                    btnLoad_Click(null, null);
                }
            }
            else
            {
                btnLoad_Click(null, null);
            }

            
        }

        void LoadTaxCreditList()
        {
            int totalRecords = 0;
            List<BranchDepartmentHistory> list = BranchManager.GetBranchTransferList
                (false,pagingCtl.CurrentPage - 1, int.Parse(pagingCtl.DDLRecords.SelectedValue), ref totalRecords,txtEmpSearchText.Text.Trim(),-1);
                
            //    taxMgr.GetMedicalTaxCreditList(
            //    SessionManager.CurrentCompanyId, txtEmpSearchText.Text.Trim(), pagingCtl.CurrentPage-1, int.Parse(pagingCtl.DDLRecords.SelectedValue), ref totalRecords);
            gvwList.DataSource = list;
            gvwList.DataBind();
            if (totalRecords == 0)
                pagingCtl.Visible = false;
            else
                pagingCtl.Visible = true;
            pagingCtl.UpdatePagingBar(totalRecords);

            //int totalRecords = 0;
            //gvwList.DataSource = ProjectManager.GetProjectList(
            //    GetEngDate(calFilterFrom.SelectedDate.ToString()), GetEngDate(calFilterTo.SelectedDate.ToString()),
            //    chkAllProjects.Checked, txtSearch.Text.Trim(), pagintCtl.CurrentPage, int.Parse(pagintCtl.DDLRecords.SelectedValue), ref totalRecords);


            //gvwList.DataBind();


            //if (list.Count > 0)
            //{
                
            //    bool newFound = false;
            //    //foreach (GetMedicalTaxCreditListResult item in list)
            //    //{
            //    //    if (item.MedicalTaxCreditId == 0)
            //    //    {
            //    //        newFound = true;
            //    //        break;
            //    //    }
            //    //}
            //    //if (newFound)
            //    //    btnSave.Visible = true;
            //    //else
            //    //    btnSave.Visible = false;
            //}
            //else
            //    btnSave.Visible = false;


        }

        protected void btnLoad_Click(object sender, EventArgs e)
        {
            pagingCtl.CurrentPage = 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage -= 1;
            LoadTaxCreditList();;
        }

       


        public bool IsVisible(object val)
        {
            if (val == null)
                return false;
            int mId = int.Parse(val.ToString());
            if (mId == 0)
                return false;
            return true;
        }

        protected void gvwList_RowCreated(object sender, GridViewRowEventArgs e)        
        {
           

        }

        protected void txtEmpSearchText_TextChanged(object sender, EventArgs e)
        {
            LoadTaxCreditList();
        }

        protected void btnPrevious_Click()
        {
            pagingCtl.CurrentPage -= 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage -= 1;
            LoadTaxCreditList();

        }

        protected void btnNext_Click()
        {
            pagingCtl.CurrentPage += 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage += 1;
            LoadTaxCreditList();
        }

        protected void ddlRecords_SelectedIndexChanged()
        {
            //SessionManager.EmployeeDisplayRecodsPerPage = int.Parse(pagintCtl.DDLRecords.SelectedValue);
            //_tempCurrentPage = 1;
            pagingCtl.CurrentPage = 1;
            LoadTaxCreditList();
        }

        protected void gvwList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            
            int branchDepId = int.Parse(e.CommandArgument.ToString());
            Status status = CommonManager.DeleteBranchTransfer(branchDepId);

            if (status.IsSuccess)
            {
                msgInfo.InnerHtml = "Branch transfer deleted.";
                msgInfo.Hide = false;
                LoadTaxCreditList();
            }
            else
            {

                msgWarning.InnerHtml = status.ErrorMessage;
                msgWarning.Hide = false;
            }
        }
    }
}
