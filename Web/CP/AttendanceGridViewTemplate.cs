﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BLL.Manager;
using System.Drawing;

namespace Web.CP
{
    public class AttendanceGridViewTemplate : ITemplate
    {
        private const int Width = 40;
        private DataControlRowType templateType;
        private string columnName;
        private string columnValue;
        private bool isEditable;
        private List<LLeaveType> leaves;

        private bool isReadonly = false;
       
        //private  bool isCalculation


        public AttendanceGridViewTemplate(DataControlRowType type, string colname,string colValue, List<LLeaveType> leaves,bool isEditable
            , bool isReadonlyAtt)
        {
            templateType = type;
            columnName = colname;
            columnValue = colValue;
            this.leaves = leaves;

            this.isReadonly = isReadonlyAtt;
            this.isEditable = isEditable;
        }

        public void InstantiateIn(System.Web.UI.Control container)
        {
            switch (templateType)
            {
                case DataControlRowType.Header:

                    Label lbl = new Label();
                    lbl.Text = columnValue;
                    lbl.ID = "lbl" + columnName;
                    
                    container.Controls.Add(lbl);
                    ((System.Web.UI.WebControls.WebControl)(container)).Width = new Unit(Width);


                    //if (HolidayManager.IsWeeklyHoliday(int.Parse(columnName), holidays))
                    //{
                    //    ((System.Web.UI.WebControls.WebControl) (container)).BackColor =
                    //        System.Drawing.ColorTranslator.FromHtml(HolidaysConstant.GetColor(HolidaysConstant.Weekly_Holiday));
                    //    ((System.Web.UI.WebControls.WebControl)(container)).Style.Add("border-left", "1px solid #1C9AC8");
                    //    ((System.Web.UI.WebControls.WebControl)(container)).Style.Add("border-right", "1px solid #1C9AC8");
                    //}
                    break;
                case DataControlRowType.DataRow:
                    TextBox txt = new TextBox();
                    txt.ID = "txt" + columnName;


                    txt.Attributes.Add("autocomplete", "off" );
                    txt.Attributes.Add("oncontextmenu", "return false");
                    txt.CssClass = "attendanceCell";
                    txt.Attributes.Add("onkeydown", "return false");
                    txt.Attributes.Add("onclick", "stopPropagation(event);");
                    txt.Attributes.Add("onfocus", "displayOption(this,event)");
                    //txt.Text = string.Format(@"'<%# Eval(""D{0}"") %>'", columnName);
                    txt.Width = new Unit(Width);
                    txt.Height = new Unit(16);
                    txt.DataBinding +=new EventHandler(txt_DataBinding);

                    //if (isReadonly == false)
                    {
                        HiddenField hfType = new HiddenField();
                        hfType.ID = "hfType" + columnName;
                        hfType.DataBinding += new EventHandler(hf_DataBinding);

                        //HiddenField hfTypeValue = new HiddenField();
                        //hfTypeValue.ID = "hfTypeValue" + columnName;
                        //hfTypeValue.DataBinding += new EventHandler(hf_DataBinding);

                        //HiddenField hfDeductDays = new HiddenField();
                        //hfDeductDays.ID = "hfDeductDays" + columnName;
                        //hfDeductDays.DataBinding += new EventHandler(hf_DataBinding);


                        container.Controls.Add(hfType);
                        //container.Controls.Add(hfTypeValue);
                        //container.Controls.Add(hfDeductDays);

                    }
                        container.Controls.Add(txt);
                    //container.Controls.Add(txtHf);
                    break;
                default:
                    break;
            }
        }

        public void txt_DataBinding(object sender, EventArgs e)
        {
            GridViewRow row =
                ((System.Web.UI.WebControls.GridViewRow) (((System.Web.UI.Control) (sender)).NamingContainer));

            GetAttandenceListResult data = row.DataItem as DAL.GetAttandenceListResult;

            TextBox txt = (sender as TextBox);

            txt.ToolTip = data.EmployeeId + " : " + data.Name;


            if (data.AttendenceList != string.Empty || data.LeaveRequest != string.Empty)
            {
                txt.Text = data.GetDayValue(columnName);
                txt.Style.Add("background-color", GetLegendColor(txt.Text));
                // custom attributes so as to preseve the orginal color for the cell
                txt.Attributes.Add("OriginalText", txt.Text);
                txt.Attributes.Add("OrginalColor", GetLegendColor(txt.Text));

                // If has leave request & Attendance is not saved, then attach js to call total present/pay days
                if (!string.IsNullOrEmpty(data.LeaveRequest) && data.AttendenceId == 0)
                {
                    //string clientId = txt.ClientID.Remove(txt.ClientID.LastIndexOf("txt")) + "txt";
                    //row.Page.ClientScript.RegisterStartupScript
                    //    (row.Page.GetType(), "Key" + data.EmployeeId, string.Format("calculateDays('{0}');",
                    //    clientId), true);
                }

            }
            else
                txt.Text = string.Empty;

            if(isEditable==false)
            //if (data.IsUsedInCalculation.Value == 1)
            {
                //txt.Enabled = false;
                txt.ReadOnly = true;
                txt.CssClass = "disabledTxt attendanceCell";
            }


            if (data.HasLeaveRequestOnThisDay(columnName))
            {
                txt.Enabled = false;
                
            }
            //check if the cell needs to be disabled due to salary calculate start date or ret/reg date
            else if (IsDisableDay(data))
            {
                txt.Enabled = false;
                txt.Text = Resources.FixedValues.AttendanceCellPresentValue;
            }

        }

        private bool IsDisableDay(GetAttandenceListResult data)
        {
             int day = int.Parse(columnName);
             return (day <= data.DisableCellStartDay || day >= data.DisableCellEndDay);
        }

        public void hf_DataBinding(object sender, EventArgs e)
        {
            GridViewRow row = ((System.Web.UI.WebControls.GridViewRow)(((System.Web.UI.Control)(sender)).NamingContainer));

            GetAttandenceListResult data = row.DataItem as DAL.GetAttandenceListResult;

            HiddenField hf = sender as HiddenField;


            if (data.AttendenceList != string.Empty)
            {
                hf.Value = data.GetType(columnName);// +":" + data.GetTypeValue(columnName) + ":" + data.GetDeductDays(columnValue);
                
                //else if (hf.ID.IndexOf("hfTypeValue") >= 0)
                //    hf.Value = ;
               
                //else
                //{
                //    hf.Value = data.GetDeductDays(columnValue);
                //}
            }
            else
                hf.Value = string.Empty;

            //if cell disable then ...
            if (IsDisableDay(data))
            {
                hf.Value = string.Empty;
            }


        }

        public string GetAttendenceCellValue(string atteList,string columnName)
        {
            try
            {
                string[] values = atteList.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                int columnIndex = int.Parse(columnName);

                string[] cells = values[columnIndex - 1].Split(new char[] { ':' });
                return cells[1];
            }
            catch
            {
                return string.Empty;
            }
        }

        

        public string GetLegendColor(string value)
        {
            if (string.IsNullOrEmpty(value))
                return "";
            if( leaves==null)
                return "";
            if (value.Contains(LeaveAttendanceManager.HalfDayPostFix))
                value = value.Replace(LeaveAttendanceManager.HalfDayPostFix, "");
            if (value.Contains(LeaveAttendanceManager.HourlyLeaveSeparator))
                value = value.Substring(0, value.IndexOf(LeaveAttendanceManager.HourlyLeaveSeparator));

            foreach (LLeaveType leave in leaves)
            {
                if (leave.Abbreviation.Equals(value))
                    return leave.LegendColor;
            }

            return HolidaysConstant.GetColor(value);
           
        }
       
    }
}