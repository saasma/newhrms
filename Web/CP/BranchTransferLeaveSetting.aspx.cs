﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using BLL;
using DAL;
using Utils.Web;
using Utils.Helper;
using System.Text;

using Ext.Net;
namespace Web.CP
{



    public partial class BranchTransferLeaveSetting : BasePage
    {
        CompanyManager compMgr = new CompanyManager();
        int? branchId = null;
        int? employeeId = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["BranchId"]))
                    branchId = int.Parse(Request.QueryString["BranchId"]);

                if (!string.IsNullOrEmpty(Request.QueryString["EmployeeId"]))
                    employeeId = int.Parse(Request.QueryString["EmployeeId"]);

                LoadCombos();
                SetPagePermission();
            }
        }


        void SetPagePermission()
        {
            if (SessionManager.IsReadOnlyUser)
            {
                btnExtSave.Visible = false;
                gvList.ColumnModel.Columns[5].Hidden = true;
            }

            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.PSI)
            {
                timesheetHRHeader1.Visible = true;
                chkTimeSheetHRApproval.Visible = true;
                timesheetInfo.Visible = true;

                gvList.ColumnModel.Columns[4].Text = "Timesheet Approve";
            }
            else
            {
                timesheetHRHeader1.Visible = false;
                chkTimeSheetHRApproval.Visible = false;
                timesheetInfo.Visible = false;
            }
        }

        public void btnDelete_Click(object sender, DirectEventArgs e)
        {
            int id = int.Parse(hiddenID.Text);

            LeaveRequestManager.DeleteLeaveApprovel(id);

            X.Js.AddScript("#{storeLeaveApproval}.reload();");
        }
        public void btnDelete2_Click(object sender, DirectEventArgs e)
        {
            int id = int.Parse(hiddenID.Text);

            LeaveRequestManager.DeleteLeaveApprovel(id);

            X.Js.AddScript("#{store2}.reload();");
        }
        public void btnEdit_Click(object sender, DirectEventArgs e)
        {
            ClearField();
            cboProject.Disabled = true;
            //int departmentId = int.Parse(hiddenDepartmentId.Text);
            hdnIsFirstGrid.Text = "true";
            int leaveProjectId = -1;

            // if (!string.IsNullOrEmpty(hiddenProjectId.Text))
            leaveProjectId = int.Parse(hiddenProjectId.Text);


            LeaveApprovalEmployee approval = LeaveRequestManager.GetLeaveApprovalEmployee(leaveProjectId);

            if (approval != null)
            {


                cboProject.SetValue(approval.LeaveProjectId.ToString());

                LoadData(approval.LeaveProjectId.Value);

                //else
                //    cboDepartment_Select(null, null);
                LeaveApprovalWindow.Center();
                LeaveApprovalWindow.Show();

            }

        }

        public void btnEdit2_Click(object sender, DirectEventArgs e)
        {
            ClearField();
            cboProject.Disabled = true;
            //int departmentId = int.Parse(hiddenDepartmentId.Text);
            hdnIsFirstGrid.Text = "false";
            int leaveProjectId = -1;

            // if (!string.IsNullOrEmpty(hiddenProjectId.Text))
            leaveProjectId = int.Parse(hiddenProjectId.Text);


            LeaveApprovalEmployee approval = LeaveRequestManager.GetLeaveApprovalEmployee(leaveProjectId);

            if (approval != null)
            {


                cboProject.SetValue(approval.LeaveProjectId.ToString());

                LoadData(approval.LeaveProjectId.Value);

                //else
                //    cboDepartment_Select(null, null);
                LeaveApprovalWindow.Center();
                LeaveApprovalWindow.Show();

            }

        }

        void LoadCombos()
        {
            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.NIBL)
            {
                ColumnR1.Hide();
                ColumnR2.Hide();

                ColumnR3.Hide();
                ColumnR4.Hide();

                rowReview1.Visible = false;
                rowReview2.Visible = false;
                rowReview3.Visible = false;
                rowReview4.Visible = false;
            }

            storeLeaveProject.DataSource = LeaveRequestManager.GetLeaveProjectListByDepartment(-1);
            storeLeaveProject.DataBind();

            storeAppliesTo.DataSource = EmployeeManager.GetEmployeeForLeaveSetting();
            storeAppliesTo.DataBind();
            //    storeAppliesTo.DataBind();

            List<Branch> list = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId); ;
            storeBranch.DataSource = list;
            storeBranch.DataBind();



            //BranchDepartmentHistory history
            //    = CommonManager.GetBranchDepartmentHistoryById(GetBranchTransferId());
            LeaveProjectEmployee employeeTeam = LeaveRequestManager.GetLeaveProjectForEmployee(GetEmployeeId());
            if (employeeTeam != null)
            {
                dispTeam1.Text = employeeTeam.LeaveProject.Name;
                LeaveApprovalEmployee approval = LeaveRequestManager.GetLeaveApprovalEmployee(employeeTeam.LeaveProjectId.Value);
                if (approval != null)
                {
                    dispReview1.Text = approval.CC4 == null ? "" : new EmployeeManager().GetById(approval.CC4.Value).Name;
                    dispReview2.Text = approval.CC5 == null ? "" : new EmployeeManager().GetById(approval.CC5.Value).Name;

                    dispApprove1.Text = approval.ApplyTo == null ? "" : new EmployeeManager().GetById(approval.ApplyTo.Value).Name;
                    dispApprove2.Text = approval.CC1 == null ? "" : new EmployeeManager().GetById(approval.CC1.Value).Name;
                }
            }

            int fromBranchId = GetFromBranchId();
            if (fromBranchId != 0)
                dispOldBranch.Text = new BranchManager().GetById(fromBranchId).Name;
            dispNewBranch.Text = new BranchManager().GetById(GetToBranchId()).Name;


            ExtControlHelper.ComboBoxSetSelected(GetFromBranchId().ToString(), cboBranch);
            ExtControlHelper.ComboBoxSetSelected(GetToBranchId().ToString(), cmbNewBranch);

        }


        
        protected void btnSaveNewTeam_Click(object sender, DirectEventArgs e)
        {
            if (cmbNewTeam.SelectedItem == null || cmbNewTeam.SelectedItem.Value == null)
            {
                NewMessage.ShowWarningMessage("New team is required.");
                return;
            }

             LeaveProjectEmployee project = new LeaveProjectEmployee();
            //BranchDepartmentHistory history
            //    = CommonManager.GetBranchDepartmentHistoryById(GetBranchTransferId());
            project.LeaveProjectId = int.Parse(cmbNewTeam.SelectedItem.Value);
            project.EmployeeId = GetEmployeeId();



            if (LeaveRequestManager.SaveUpdateEmployeeProject(project))
            {
                NewMessage.ShowNormalMessage("Employee team has been changed.");
            }
        }

        public int GetBranchTransferId()
        {
            return int.Parse(Request.QueryString["Id"]);
        }
        public int GetEmployeeId()
        {
            if (employeeId != null)
                return employeeId.Value;

            if (string.IsNullOrEmpty(Request.QueryString["type"]))
            {
                BranchDepartmentHistory history
                    = CommonManager.GetBranchDepartmentHistoryById(GetBranchTransferId());
                return history.EmployeeId.Value;
            }
            else
            {
                DeputationEmployee dep = NewPayrollManager.GetDeputationEmployeeById(GetBranchTransferId());
                return dep.EmployeeId.Value;
            }
        }
        public int GetFromBranchId()
        {
            if (employeeId != null)
                return 0;

            if (string.IsNullOrEmpty(Request.QueryString["type"]))
            {
                BranchDepartmentHistory history
                    = CommonManager.GetBranchDepartmentHistoryById(GetBranchTransferId());
                if (history == null || history.FromBranchId == null)
                    return 0;

                return history.FromBranchId.Value;
            }
            else
            {
                DeputationEmployee dep = NewPayrollManager.GetDeputationEmployeeById(GetBranchTransferId());
                return dep.FromBranchId.Value;
            }
        }
        public int GetToBranchId()
        {
            if (branchId != null)
                return branchId.Value;

            if (string.IsNullOrEmpty(Request.QueryString["type"]))
            {
                BranchDepartmentHistory history
                    = CommonManager.GetBranchDepartmentHistoryById(GetBranchTransferId());
                return history.BranchId.Value;
            }
            else
            {
                DeputationEmployee dep = NewPayrollManager.GetDeputationEmployeeById(GetBranchTransferId());
                return dep.BranchId.Value;
            }
        }

        public void LeaveApprovalAddBtn_Click(object sender, DirectEventArgs e)
        {
            //if (cboDepartment.SelectedItem == null || string.IsNullOrEmpty(cboDepartment.SelectedItem.Value))
            //{
            //    X.Msg.Show
            //           (
            //               new MessageBoxConfig
            //               {

            //                   Message = "Please select the department for new entry.",
            //                   Title = "Warning",
            //                   Buttons = MessageBox.Button.OK,
            //                    Icon=MessageBox.Icon.WARNING
            //               }
            //           );
            //    return;
            //}

            BindLeaveApprovalWindowCommonData();
            cboProject.Disabled = false;

            chkCC1.Checked = true;
            chkCC2.Checked = true;
            chkCC3.Checked = true;
            chkCC4.Checked = true;
            chkCC5.Checked = true;
            LeaveApprovalWindow.Show();
        }

        private  void BindLeaveApprovalWindowCommonData()
        {

            if (storeLeaveProject.DataSource == null)
            {

                cboProject.ClearValue();
               
            }

            ClearField();
            List<Branch> list = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId); ;
           
            if(list.Count>0)
            {

                this.BindDepartmentStoreData(list[0].BranchId);
               
            }
        }

        private void BindDepartmentStoreData(int branchId)
        {
            //storeDepartment1.DataSource = DepartmentManager.GetAllDepartmentsByBranch(branchId);
            //storeDepartment1.DataBind();
            //storeDepartment2.DataSource = DepartmentManager.GetAllDepartmentsByBranch(branchId);
            //storeDepartment2.DataBind();
            //storeDepartment3.DataSource = DepartmentManager.GetAllDepartmentsByBranch(branchId);
            //storeDepartment3.DataBind();
            //storeDepartment4.DataSource = DepartmentManager.GetAllDepartmentsByBranch(branchId);
            //storeDepartment4.DataBind();
            //storeDepartment5.DataSource = DepartmentManager.GetAllDepartmentsByBranch(branchId);
            //storeDepartment5.DataBind();
            //storeDepartment6.DataSource = DepartmentManager.GetAllDepartmentsByBranch(branchId);
            //storeDepartment6.DataBind();
        }


        protected void cboBranch_Select(object sender, DirectEventArgs e)
        {

            if (sender == cboBranch)
            {
               
                //ClearField();
               

                if (cboBranch.SelectedItem == null && cboBranch.SelectedItem.Value != null)
                    return;

                
              
                X.Js.AddScript("#{PagingToolbar1}.doRefresh();");
            }
           


        }

        protected void cboDepartment_Select(object sender, DirectEventArgs e)
        {
            

            List<LeaveProject> projects = LeaveRequestManager.GetLeaveProjectListByDepartment(-1);

            //storeLeaveProject.DataSource = projects;
            //storeLeaveProject.DataBind();

            if (projects.Count == 0)
            {
                if (!LoadData(-1))
                {
                   // cboDepartment1.SelectedItem.Value = cboDepartment.Text;
                   // cboDepartment1_Select(null, null);
                   // //cboDepartment2.SelectedItem.Value = cboDepartment.Text;
                   // cboDepartment2_Select(null, null);
                   // //cboDepartment3.SelectedItem.Value = cboDepartment.Text;
                   // cboDepartment3_Select(null, null);
                   //// cboDepartment4.SelectedItem.Value = cboDepartment.Text;
                   // cboDepartment4_Select(null, null);
                   //// cboDepartment5.SelectedItem.Value = cboDepartment.Text;
                   // cboDepartment5_Select(null, null);
                   // //cboDepartment6.SelectedItem.Value = cboDepartment.Text;
                   // cboDepartment6_Select(null, null);
                }
            }
            
            X.Js.AddScript("#{PagingToolbar1}.doRefresh();");
           // cboProject.ClearValue();
        }

        protected void cboProject_Select(object sender, DirectEventArgs e)
        {

            if (cboProject.SelectedItem == null || cboProject.SelectedItem.Value == null)
                return;

            if (!LoadData(int.Parse(cboProject.SelectedItem.Value)))
            {
                // if (storeDepartment1.DataMember.Count() == 0)
                {
                    List<Branch> list = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId); ;

                    if (list.Count > 0)
                    {

                        this.BindDepartmentStoreData(list[0].BranchId);

                    }
                }


            }
            else
            {
                lblEmail1.Text = "";
                lblEmail2.Text = "";
                lblEmail3.Text = "";
                lblEmail4.Text = "";
                lblEmail5.Text = "";
                lblEmail6.Text = "";
            }
        }

        protected void cmbNewTeam_Select(object sender, DirectEventArgs e)
        {




         

            LeaveApprovalEmployee approval = LeaveRequestManager.GetLeaveApprovalEmployee(int.Parse( cmbNewTeam.SelectedItem.Value));
            if (approval != null)
            {
                dispNewReview1.Text = approval.CC4 == null ? "" : new EmployeeManager().GetById(approval.CC4.Value).Name;
                dispNewReview2.Text = approval.CC5 == null ? "" : new EmployeeManager().GetById(approval.CC5.Value).Name;

                dispNewApprove1.Text = approval.ApplyTo == null ? "" : new EmployeeManager().GetById(approval.ApplyTo.Value).Name;
                dispNewApprove2.Text = approval.CC1 == null ? "" : new EmployeeManager().GetById(approval.CC1.Value).Name;
            }


         
        }
        //protected void cboDepartment1_Select(object sender, DirectEventArgs e)
        //{
        //    cboAppliesTo.Clear();
        //    cboAppliesTo.Items.Clear();
        //    lblEmail1.Text = string.Empty;
        //    storeAppliesTo.DataSource = EmployeeManager.GetEmployeeByDepartmentId(int.Parse(cboDepartment1.SelectedItem.Value));
        //    storeAppliesTo.DataBind();
           

        //}

        //protected void cboDepartment2_Select(object sender, DirectEventArgs e)
        //{
        //    cboCC1.Clear();
        //    cboCC1.Items.Clear();
        //    lblEmail2.Text = string.Empty;
        //    if ( (cboDepartment2.SelectedItem.Value != "") && (cboDepartment2.SelectedItem.Value != null))
        //    {
        //        storeCC1.DataSource = EmployeeManager.GetEmployeeByDepartmentId(int.Parse(cboDepartment2.SelectedItem.Value));
        //        storeCC1.DataBind();
        //    }

        //}

        //protected void cboDepartment3_Select(object sender, DirectEventArgs e)
        //{
        //    cboCC2.Clear();
        //    cboCC2.Items.Clear();
        //    lblEmail3.Text = string.Empty;
        //    if ( (cboDepartment3.SelectedItem.Value != "") && (cboDepartment3.SelectedItem.Value != null))
        //    {
        //        storeCC2.DataSource = EmployeeManager.GetEmployeeByDepartmentId(int.Parse(cboDepartment3.SelectedItem.Value));
        //        storeCC2.DataBind();
        //    }


        //}
        //protected void cboDepartment4_Select(object sender, DirectEventArgs e)
        //{
        //    cboCC3.Clear();
        //    cboCC3.Items.Clear();
        //    lblEmail4.Text = string.Empty;
        //    if ((cboDepartment4.SelectedItem.Value != "") && (cboDepartment4.SelectedItem.Value != null))
        //    {
        //        storeCC3.DataSource = EmployeeManager.GetEmployeeByDepartmentId(int.Parse(cboDepartment4.SelectedItem.Value));
        //        storeCC3.DataBind();
        //    }


        //}
        //protected void cboDepartment5_Select(object sender, DirectEventArgs e)
        //{
        //    cboCC4.Clear();
        //    cboCC4.Items.Clear();
        //    lblEmail5.Text = string.Empty;
        //    if ((cboDepartment5.SelectedItem.Value != "") && (cboDepartment5.SelectedItem.Value != null))
        //    {
        //        storeCC4.DataSource = EmployeeManager.GetEmployeeByDepartmentId(int.Parse(cboDepartment5.SelectedItem.Value));
        //        storeCC4.DataBind();
        //    }


        //}
        //protected void cboDepartment6_Select(object sender, DirectEventArgs e)
        //{
        //    cboCC5.Clear();
        //    cboCC5.Items.Clear();
        //    lblEmail6.Text = string.Empty;
        //    if ((cboDepartment6.SelectedItem.Value != "") && (cboDepartment6.SelectedItem.Value != null))
        //    {
        //        storeCC5.DataSource = EmployeeManager.GetEmployeeByDepartmentId(int.Parse(cboDepartment6.SelectedItem.Value));
        //        storeCC5.DataBind();
        //    }


        //}
        protected bool LoadData(int leaveProjectId)
        {
            ClearField();
            //int depratmentId = int.Parse(cboDepartment.SelectedItem.Value);
            int depId = -1;
            //if( cboDepartment != null && cboDepartment.SelectedItem.Value != null)

            GetLeaveApprovalEmployeeByDepartmentIdResult leaveapproval = new GetLeaveApprovalEmployeeByDepartmentIdResult();
            leaveapproval = LeaveAttendanceManager.GetLeaveApprovalEmployeeByDepartmentId(depId, leaveProjectId);
            if (leaveapproval != null)
            {
                //int.Parse(leaveapproval.DepartmentId1.ToString())
                int branchId;
                //if (leaveapproval.DepartmentId1 != null && storeDepartment1.DataSource==null)
                //{
                //    branchId = LeaveRequestManager.GetBranchIDByDepartment((int)leaveapproval.DepartmentId1);
                //    storeDepartment1.DataSource = DepartmentManager.GetAllDepartmentsByBranch(branchId);
                //    storeDepartment1.DataBind();
                //    cboBranch1.SelectedItem.Value = branchId.ToString();
                //}
                
                
                //cboDepartment1.SelectedItem.Value = leaveapproval.DepartmentId1.ToString();
                //cboDepartment1_Select(null, null);
                //if (leaveapproval.DepartmentId2 != null && storeDepartment2.DataSource == null)
                //{
                //    branchId = LeaveRequestManager.GetBranchIDByDepartment((int)leaveapproval.DepartmentId2);
                //    storeDepartment2.DataSource = DepartmentManager.GetAllDepartmentsByBranch(branchId);
                //    storeDepartment2.DataBind();
                //    cboBranch2.SelectedItem.Value = branchId.ToString();
                //}
                
                //cboDepartment2.SelectedItem.Value = leaveapproval.DepartmentId2.ToString();
                //cboDepartment2_Select(null, null);
                //if (leaveapproval.DepartmentId3 != null && storeDepartment3.DataSource == null)
                //{
                //    branchId = LeaveRequestManager.GetBranchIDByDepartment((int)leaveapproval.DepartmentId3);
                //    storeDepartment3.DataSource = DepartmentManager.GetAllDepartmentsByBranch(branchId);
                //    storeDepartment3.DataBind();
                //    cboBranch3.SelectedItem.Value = branchId.ToString();
                //}
                //cboDepartment3.SelectedItem.Value = leaveapproval.DepartmentId3.ToString();
                //cboDepartment3_Select(null, null);
                //if (leaveapproval.DepartmentId4 != null && storeDepartment4.DataSource == null)
                //{
                //    branchId = LeaveRequestManager.GetBranchIDByDepartment((int)leaveapproval.DepartmentId4);
                //    storeDepartment4.DataSource = DepartmentManager.GetAllDepartmentsByBranch(branchId);
                //    storeDepartment4.DataBind();
                //    cboBranch4.SelectedItem.Value = branchId.ToString();
                //}
                
                //cboDepartment4.SelectedItem.Value = leaveapproval.DepartmentId4.ToString();
                //cboDepartment4_Select(null, null);
                //if (leaveapproval.DepartmentId5 != null && storeDepartment5.DataSource == null)
                //{
                //    branchId = LeaveRequestManager.GetBranchIDByDepartment((int)leaveapproval.DepartmentId5);
                //    storeDepartment5.DataSource = DepartmentManager.GetAllDepartmentsByBranch(branchId);
                //    storeDepartment5.DataBind();
                //    cboBranch5.SelectedItem.Value = branchId.ToString();
                //}
                //cboDepartment5.SelectedItem.Value = leaveapproval.DepartmentId5.ToString();
                //cboDepartment5_Select(null, null);
                //if (leaveapproval.DepartmentId6 != null && storeDepartment6.DataSource == null)
                //{
                //    branchId = LeaveRequestManager.GetBranchIDByDepartment((int)leaveapproval.DepartmentId6);
                //    storeDepartment6.DataSource = DepartmentManager.GetAllDepartmentsByBranch(branchId);
                //    storeDepartment6.DataBind();
                //    cboBranch6.SelectedItem.Value = branchId.ToString();
                //}
                //cboDepartment6.SelectedItem.Value = leaveapproval.DepartmentId6.ToString();
                //cboDepartment6_Select(null, null);

                cboAppliesTo.SetValue(leaveapproval.ApplyTo.ToString());
                //cboAppliesTo.ShowTrigger(cboAppliesTo.SelectedIndex);
                cboCC1.SetValue(leaveapproval.CC1.ToString());
                cboCC2.SetValue(leaveapproval.CC2.ToString());
                cboCC3.SetValue(leaveapproval.CC3.ToString());
                cboCC4.SetValue(leaveapproval.CC4.ToString());
                cboCC5.SetValue(leaveapproval.CC5.ToString());

                if (leaveapproval.AllowCC1ToApprove != null)
                    chkCC1.Checked = leaveapproval.AllowCC1ToApprove.Value;
                else
                    chkCC1.Checked = false;

                if (leaveapproval.AllowCC2ToApprove != null)
                    chkCC2.Checked = leaveapproval.AllowCC2ToApprove.Value;
                else
                    chkCC2.Checked = false;

                if (CommonManager.CompanySetting.WhichCompany == WhichCompany.PSI)
                {
                    if (leaveapproval.TimeSheetHRReviewerCC2 != null)
                        chkTimeSheetHRApproval.Checked = leaveapproval.TimeSheetHRReviewerCC2.Value;
                    else
                        chkTimeSheetHRApproval.Checked = false;
                }


                if (leaveapproval.AllowCC3TOApprove != null)
                    chkCC3.Checked = leaveapproval.AllowCC3TOApprove.Value;
                else
                    chkCC3.Checked = false;

                if (leaveapproval.AllowCC4ToApprove != null)
                    chkCC4.Checked = leaveapproval.AllowCC4ToApprove.Value;
                else
                    chkCC4.Checked = false;

                if (leaveapproval.AllowCC5ToApprove != null)
                    chkCC5.Checked = leaveapproval.AllowCC5ToApprove.Value;
                else
                    chkCC5.Checked = false;


                if (leaveapproval.ApplyToLevel != null)
                    txtLevelApplyTo.Text = leaveapproval.ApplyToLevel.ToString();
                else
                    txtLevelApplyTo.Text = "";

                if (leaveapproval.CC1Level != null)
                    txtLevelCC1.Text = leaveapproval.CC1Level.ToString();
                else
                    txtLevelCC1.Text = "";

                if (leaveapproval.CC2Level != null)
                    txtLevelCC2.Text = leaveapproval.CC2Level.ToString();
                else
                    txtLevelCC2.Text = "";

                if (leaveapproval.CC3Level != null)
                    txtLevelCC3.Text = leaveapproval.CC3Level.ToString();
                else
                    txtLevelCC3.Text = "";

                if (leaveapproval.CC4Level != null)
                    txtLevelCC4.Text = leaveapproval.CC4Level.ToString();
                else
                    txtLevelCC4.Text = "";

                if (leaveapproval.CC5Level != null)
                    txtLevelCC5.Text = leaveapproval.CC5Level.ToString();
                else
                    txtLevelCC5.Text = "";

                lblEmail1.Text = leaveapproval.AppliesToEmail;
                lblEmail2.Text = leaveapproval.CC1Email;
                lblEmail3.Text = leaveapproval.CC2Email;
                lblEmail4.Text = leaveapproval.CC3Email;
                lblEmail5.Text = leaveapproval.CC4Email;
                lblEmail6.Text = leaveapproval.CC5Email;

                if (leaveapproval.ApprovalNotificationToType == null)
                    cmbApprovalNotificationType.ClearValue();
                else
                    cmbApprovalNotificationType.SetValue(leaveapproval.ApprovalNotificationToType.ToString());
                return true;
            }
            return false;
        }

        //public void btnRemove1_Click(object sender, DirectEventArgs e)
        //{
        //    if(cboAppliesTo.SelectedItem.Value == null)
        //        return;
        //    if (LeaveAttendanceManager.RemoveRecipient(int.Parse(cboAppliesTo.SelectedItem.Value), int.Parse(cboDepartment1.SelectedItem.Value)))
        //    {
        //        divMsgCtl.InnerHtml = "Notification recipient removed.";
        //        divMsgCtl.Hide = false;
        //    }

        //}

        //public void btnRemove2_Click(object sender, DirectEventArgs e)
        //{
        //    if (cboCC1.SelectedItem.Value == null)
        //        return;
        //    if (LeaveAttendanceManager.RemoveRecipient(int.Parse(cboCC1.SelectedItem.Value),0))
        //    {
        //        divMsgCtl.InnerHtml = "Notification recipient removed.";
        //        divMsgCtl.Hide = false;
        //    }

        //}

        //public void btnRemove3_Click(object sender, DirectEventArgs e)
        //{
        //    if (cboCC2.SelectedItem.Value == null)
        //        return;
        //    if (LeaveAttendanceManager.RemoveRecipient(int.Parse(cboCC2.SelectedItem.Value),0))
        //    {
        //        divMsgCtl.InnerHtml = "Notification recipient removed.";
        //        divMsgCtl.Hide = false;
        //    }

        //}
        //protected void btnSave_Click(object sender, EventArgs e)
        //{


        //    Page.Validate("LeaveApproval");
        //    if (Page.IsValid)
        //    {
        //        LeaveApprovalEmployee approval = new LeaveApprovalEmployee();
        //        approval.ApplyTo = int.Parse(cboAppliesTo.SelectedItem.Value);
        //        if (cboCC1.SelectedItem != null)
        //            approval.CC1 = int.Parse(cboCC1.SelectedItem.Value);
        //        if (cboCC2.SelectedItem != null)
        //            approval.CC2 = int.Parse(cboCC2.SelectedItem.Value);
        //    }
        //}

        protected void btnExtSave_Click(object sender, EventArgs e)
        {

            if (cboProject.SelectedItem == null || cboProject.SelectedItem.Value == null)
            {
                X.Msg.Show
                       (
                           new MessageBoxConfig
                           {

                               Message = "Team must be selected.",
                               Title = "Warning",
                               Icon = MessageBox.Icon.WARNING,
                               Buttons = MessageBox.Button.OK
                           }
                       );
                return;
            }

            if (cboAppliesTo.SelectedItem == null || cboAppliesTo.SelectedItem.Value == null)
            {
                X.Msg.Show
                       (
                           new MessageBoxConfig
                           {

                               Message = "Applies to is required.",
                               Title = "Warning",
                               Icon = MessageBox.Icon.WARNING,
                               Buttons = MessageBox.Button.OK
                           }
                       );
                return;
            }




            EventMask mask = new EventMask();
            mask.ShowMask = true;
            Page.Validate("LeaveApproval");
            if (Page.IsValid)
            {
                LeaveApprovalEmployee approval = new LeaveApprovalEmployee();

                approval.LeaveProjectId = int.Parse(cboProject.SelectedItem.Value);

                LeaveProject leaveProject = LeaveRequestManager.GetLeaveProject(approval.LeaveProjectId.Value);


                //approval.DepartmentID = leaveProject.DepartmentId;


                approval.ApplyTo = int.Parse(cboAppliesTo.SelectedItem.Value);




                if (cboCC1.SelectedItem.Value != null)
                {
                    approval.CC1 = int.Parse(cboCC1.SelectedItem.Value);
                    approval.AllowCC1ToApprove = true;// chkCC1.Checked;
                }
                if (cboCC2.SelectedItem.Value != null)
                {
                    approval.CC2 = int.Parse(cboCC2.SelectedItem.Value);
                    approval.AllowCC2ToApprove = true;//chkCC2.Checked;
                }

                if (CommonManager.CompanySetting.WhichCompany == WhichCompany.PSI)
                {
                    if (cboCC2.SelectedItem.Value != null)
                        approval.TimeSheetHRReviewerCC2 = chkTimeSheetHRApproval.Checked;
                    else
                    {
                        if (chkTimeSheetHRApproval.Checked)
                        {
                            X.Msg.Show
                              (
                                  new MessageBoxConfig
                                  {

                                      Message = "HR employee must be selected if review is checked.",
                                      Title = "Warning",
                                      Icon = MessageBox.Icon.WARNING,
                                      Buttons = MessageBox.Button.OK
                                  }
                              );
                            return;
                        }
                    }
                }

                if (cboCC3.SelectedItem.Value != null)
                {
                    approval.CC3 = int.Parse(cboCC3.SelectedItem.Value);
                    approval.AllowCC3TOApprove = true;//chkCC3.Checked;
                }
                if (cboCC4.SelectedItem.Value != null)
                {
                    approval.CC4 = int.Parse(cboCC4.SelectedItem.Value);
                    approval.AllowCC4ToApprove = true;//chkCC4.Checked;
                }
                if (cboCC5.SelectedItem.Value != null)
                {
                    approval.CC5 = int.Parse(cboCC5.SelectedItem.Value);
                    approval.AllowCC5ToApprove = true;//chkCC5.Checked;
                }

                int value = 0;
                if (txtLevelApplyTo.Text != "" && int.TryParse(txtLevelApplyTo.Text.Trim(), out value))
                    approval.ApplyToLevel = value;
                if (txtLevelCC1.Text != "" && int.TryParse(txtLevelCC1.Text.Trim(), out value))
                    approval.CC1Level = value;
                if (txtLevelCC2.Text != "" && int.TryParse(txtLevelCC2.Text.Trim(), out value))
                    approval.CC2Level = value;
                if (txtLevelCC3.Text != "" && int.TryParse(txtLevelCC3.Text.Trim(), out value))
                    approval.CC3Level = value;

                if (txtLevelCC4.Text != "" && int.TryParse(txtLevelCC4.Text.Trim(), out value))
                    approval.CC4Level = value;
                if (txtLevelCC5.Text != "" && int.TryParse(txtLevelCC5.Text.Trim(), out value))
                    approval.CC5Level = value;

                if (cmbApprovalNotificationType.SelectedItem != null && cmbApprovalNotificationType.SelectedItem.Value != null)
                    approval.ApprovalNotificationToType = int.Parse(cmbApprovalNotificationType.SelectedItem.Value);

                if (LeaveAttendanceManager.UpdateLeaveApproveNotification(approval))
                {
                    X.Msg.Show
                        (
                            new MessageBoxConfig
                                                    {

                                                        Message = "Leave approval notification saved.",
                                                        Title = "Information",
                                                        Buttons = MessageBox.Button.OK,
                                                        Icon = MessageBox.Icon.INFO
                                                    }
                        );
                    //divMsgCtl.InnerHtml = "Leave approval notification saved.";
                    //divMsgCtl.Hide = false;
                    ClearField();
                    LeaveApprovalWindow.Hide();

                }
                else
                {
                    X.Msg.Show
                        (
                            new MessageBoxConfig
                            {

                                Message = "Leave approval could not be saved, please refresh the page.",
                                Title = "Warning",
                                Buttons = MessageBox.Button.OK,
                                Icon = MessageBox.Icon.WARNING
                            }
                        );
                }
                mask.ShowMask = false;

                if(hdnIsFirstGrid.Text=="true")
                    X.Js.AddScript("#{PagingToolbar1}.doRefresh();");
                else
                    X.Js.AddScript("#{PagingToolbar2}.doRefresh();");

            }
        }

      

        private void ClearField()
        {
            //cboBranch1.ClearValue();
            //cboBranch2.ClearValue();
            //cboBranch3.ClearValue();
            //cboBranch4.ClearValue();
            //cboBranch5.ClearValue();
            //cboBranch6.ClearValue();
            //cboDepartment1.ClearValue();
            //cboDepartment2.ClearValue();
            //cboDepartment3.ClearValue();
            //cboDepartment4.ClearValue();
            //cboDepartment5.ClearValue();
            //cboDepartment6.ClearValue();
            //cboDepartment1.GetStore().RemoveAll();
            //cboDepartment2.GetStore().RemoveAll();
            //cboDepartment3.GetStore().RemoveAll();
            //cboDepartment4.GetStore().RemoveAll();
            //cboDepartment5.GetStore().RemoveAll();
            //cboDepartment6.GetStore().RemoveAll();
            cboAppliesTo.ClearValue();
            cboCC1.ClearValue();
            cboCC2.ClearValue();
            cboCC3.ClearValue();
            cboCC4.ClearValue();
            cboCC5.ClearValue();
           // cboCC1.GetStore().RemoveAll();
           // cboCC2.GetStore().RemoveAll();
           // cboCC3.GetStore().RemoveAll();
           // cboCC4.GetStore().RemoveAll();
           // cboCC5.GetStore().RemoveAll();
            
        }
    }

}

