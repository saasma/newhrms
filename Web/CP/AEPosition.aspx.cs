﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using Utils.Base;
using BLL;
using DAL;
using Utils.Web;
using Utils.Helper;
using System.Text.RegularExpressions;

namespace Web.CP
{
    public partial class AEPosition : BasePage
    {
        /// <summary>
        /// 0-> Data Insert Mode
        /// 1-> Data Update Mode
        /// </summary>
        public string SaveMode
        {
            get { return this.hdSaveMode.Value; }
            set { this.hdSaveMode.Value = value; }
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                
                Initialize();
            }
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            if (SaveMode.Equals("0"))
            {
                string errMsg = string.Empty;
                Position position = new Position();
                position.Name = txtPositionName.Text.Trim();
                position.Order = int.Parse(txtOrder.Text.Trim() == "" ? "0" : txtOrder.Text.Trim());
                position.CompanyId = SessionManager.CurrentCompanyId;
                position.CreatedOn = System.DateTime.Now;
                position.CreatedBy = SessionManager.CurrentLoggedInEmployeeId;
                if (PositionGradeStepAmountManager.SavePosition(position, ref errMsg))
                {
                    ClearField();
                    BindPosition();
                    hdIsChange.SetValue("1");
                }
                else
                {
                    divWarningMsg.InnerHtml = errMsg;
                    divWarningMsg.Hide = false;
                }
            }
            else
            {
                string errMsg = string.Empty;
                Position position = new Position();
                position.PositionId = int.Parse(hdPositionId.Value);
                position.Name = txtPositionName.Text.Trim();
                position.Order = int.Parse(txtOrder.Text.Trim() == "" ? "0" : txtOrder.Text.Trim());
                position.CompanyId = SessionManager.CurrentCompanyId;
                position.ModifiedOn = System.DateTime.Now;
                position.ModifiedBy = SessionManager.CurrentLoggedInEmployeeId;
                if (PositionGradeStepAmountManager.UpdatePosition(position, ref errMsg))
                {                    
                    ClearField();
                    BindPosition();
                    hdIsChange.SetValue("1");
                }
                else
                {
                    divWarningMsg.InnerHtml = errMsg;
                    divWarningMsg.Hide = false;
                }
            }
        }



        protected void gvPosition_OnRowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            string errMessage =string.Empty;
            int id = (int)gvPosition.DataKeys[e.RowIndex][0];
            if (!PositionGradeStepAmountManager.CanPositionBeDeleted(id,ref errMessage))
            {
                divWarningMsg.InnerHtml = errMessage;
                divWarningMsg.Hide = false;
                return;

            }
            if (PositionGradeStepAmountManager.DeletePosition(id))
            {
                ClearField();
                BindPosition();
            }

        }

        protected void gvPosition_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvPosition.PageIndex = e.NewPageIndex;
            BindPosition();
        }

        void Initialize()
        {
            BindPosition();

        }

        void BindPosition()
        {
            
            List<Position> list = PositionGradeStepAmountManager.GetAllPositions(SessionManager.CurrentCompanyId);
            gvPosition.DataSource = list;
            gvPosition.DataBind();
            if (list.Count == 0)
                txtOrder.Text = "1";
            else
                txtOrder.Text = (list[list.Count - 1].Order + 1).ToString();
        }

        void ClearField()
        {
            hdPositionId.Value = "0";
            SaveMode = "0";
            txtOrder.Text = string.Empty;
            txtPositionName.Text = string.Empty;
            divWarningMsg.Hide = true;
        }
    }
}
