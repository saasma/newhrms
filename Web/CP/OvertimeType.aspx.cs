﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using System.Xml;

namespace Web.CP
{
    public partial class OvertimeType : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.HumanResource;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        public void Initialise()
        {
            List<PIncome> list = PayManager.GetVariableIncomeList(SessionManager.CurrentCompanyId, false)
                .OrderBy(x => x.Title).ToList();

            list.Insert(0, new PIncome { IncomeId = -1, Title = "No Income" });

            storeIncomes.DataSource = list;
            storeIncomes.DataBind();

            Load();

            LoadLevels();

            BindApplicableToLevelTree();
        }

        public void Load()
        {
            List<GetIncomeForIncomesResult> list = new PayManager().GetIncomeListForIncome(SessionManager.CurrentCompanyId,0);

            //Node rootNode = new Node();

            //foreach (GetIncomeForIncomesResult module in list)
            //{
            //    Node moduleChild = new Node();
            //    moduleChild.Checked = false;
            //    moduleChild.Expanded = true;


            //    moduleChild.CustomAttributes.Add(new ConfigItem { Name = "Title", Value = module.Title });
            //    moduleChild.CustomAttributes.Add(new ConfigItem { Name = "IncomeId", Value = module.IncomeId.ToString() });

            //    rootNode.Children.Add(moduleChild);


            //}

           // treePanel.SetRootNode(rootNode);
            gridIncome.GetStore().DataSource = list;
            gridIncome.GetStore().DataBind();

        }

        public bool IsContainsPage(int incomeId, List<OvertimeTypeIncome> pages)
        {
            foreach (OvertimeTypeIncome page in pages)
            {
                if(page.IncomeId == incomeId)
                    return true;
            }
            return false;
        }


        public void LoadNodes(List<OvertimeTypeIncome> pages)
        {
      

            Node rootNode = new Node();

           

            RowSelectionModel sm = this.gridIncome.GetSelectionModel() as RowSelectionModel;
            List<GetIncomeForIncomesResult> list = new PayManager().GetIncomeListForIncome(SessionManager.CurrentCompanyId, 0);
            sm.ClearSelection();
            sm.SelectedRows.Clear();
            sm.UpdateSelection();
            foreach (OvertimeTypeIncome _list in pages)
            {
                sm.SelectedRows.Add(new SelectedRow(_list.IncomeId.ToString().ToLower()));
            }
            sm.UpdateSelection();

            //foreach (GetIncomeForIncomesResult module in list)
            //{
            //    Node moduleChild = new Node();
            //    moduleChild.Checked = false;
            //    moduleChild.Expanded = true;
            //    moduleChild.Leaf = true;


            //    if (pages != null && IsContainsPage(module.IncomeId,pages))
            //    {
            //        moduleChild.Checked = true;
            //    }
            //    else
            //    {
            //        moduleChild.Checked = false;
            //    }



            //    moduleChild.CustomAttributes.Add(new ConfigItem { Name = "Title", Value = module.Title });
            //    moduleChild.CustomAttributes.Add(new ConfigItem { Name = "IncomeId", Value = module.IncomeId.ToString() });


            //    rootNode.Children.Add(moduleChild);

               

            //}

            //treePanel.SetRootNode(rootNode);
        }

        private void LoadLevels()
        {
            GridLevels.GetStore().DataSource = OvertimeManager.GetOvertimeList();
            GridLevels.GetStore().DataBind();

        }


        
        public void ClearLevelFields()
        {
            hiddenValue.Text = "";
            txtName.Text = "";

        }

      
        
        protected void btnAddLevel_Click(object sender, DirectEventArgs e)
        {
            
            ClearLevelFields();
            
        }

        protected void btnAddNewLine_click(object sender, DirectEventArgs e)
        {
            RowSelectionModel sm = this.gridNotApplicableTo.GetSelectionModel() as RowSelectionModel;
            sm.ClearSelection();
            sm.SelectedRows.Clear();
            sm.UpdateSelection();

            RowSelectionModel sm1 = this.gridIncome.GetSelectionModel() as RowSelectionModel;
            sm1.ClearSelection();
            sm1.SelectedRows.Clear();
            sm1.UpdateSelection();
            cmbIncomes.Clear();
            txtName.Clear();

        }

        protected void btnDeleteLevel_Click(object sender, DirectEventArgs e)
        {
            int overtimeTypeId = int.Parse(hiddenValue.Text.Trim());
            Status status = OvertimeManager.DeleteOvertimeType(overtimeTypeId);
            if (status.IsSuccess)
            {
                LoadLevels();
                NewMessage.ShowNormalPopup("Overtime type deleted.");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        

        protected void btnEditLevel_Click(object sender, DirectEventArgs e)
        {
            //int levelId = int.Parse(hiddenValue.Text.Trim());
            //TAAllowance entity = TravelAllowanceManager.GetTravelAllowanceById(levelId);
            //txtName.Text = entity.Name;
            //txtDescription.Text = entity.Description;

            //WindowLevel.Show();

            int overtimeId = int.Parse(hiddenValue.Text.Trim());
            DAL.OvertimeType overtime = OvertimeManager.GetOvertimeType(overtimeId);
            if (overtime != null)
            {
                txtName.Text = overtime.Name;
                
                //cmbCalculationType.SetValue(overtime.CalculationType.ToString());
                cmbIncomes.ClearValue();
                storeIncomes.ClearFilter();
                if (overtime.IncomeId != null)
                    cmbIncomes.SetValue(overtime.IncomeId.ToString());
                //if (overtime.CalculationType == (int)OvertimeCalculationType.BasedOnSalary)
                //{
                    //txtPercent.Number = overtime.Rate;

                    //List<PIncome> list = PayManager.GetFixedIncomeListByCompany(SessionManager.CurrentCompanyId);
                    //foreach (PIncome item in list)
                    //{
                    //    chkIncomeList.Add(new Checkbox { ID=item.IncomeId.ToString(), Value = item.IncomeId.ToString(), BoxLabel = item.Title });
                    //}

                    //chkIncomeList.CheckedItems.Clear();

                    //foreach (Checkbox item in chkIncomeList.Items)
                    //{
                    //    int incomeId = int.Parse(item.ID.ToString());
                    //    if (overtime.OvertimeTypeIncomes.Any(x => x.IncomeId == incomeId))
                    //        item.Checked = true;
                    //}
                    LoadNodes(overtime.OvertimeTypeIncomes.ToList());

                    BindApplicableToLevelTreeList(overtime.OvertimeNotApplicableTos.ToList());

                //    X.AddScript("typeChange('1');");
                //}
                //else
                //{
                //    X.AddScript("typeChange('2');");
                //    txtFixedRate.Number = overtime.Rate;
                //}
            }
            windowAddEditOvertime.Show();

        }



        protected void btnDetailLevel_Click(object sender, DirectEventArgs e)
        {
            int overtypeId = int.Parse(hiddenValue.Text.Trim());
            List<OvertimeTypeLevel> detailGridList = OvertimeManager.GetOvertimeLevelList(overtypeId);
            storeAllowanceDetail.DataSource = detailGridList.OrderBy(x => x.LevelName);
            storeAllowanceDetail.DataBind();

            storeCopyList.DataSource = detailGridList;//.OrderBy(x => x.LevelName);
            storeCopyList.DataBind();
            Window1.Show();
        }


        public void btnOvertimeType_Click(object sender, DirectEventArgs e)
        {
            DAL.OvertimeType overtime = new DAL.OvertimeType();
            bool isInsert = string.IsNullOrEmpty(hiddenValue.Text.Trim());
            if (!isInsert)
                overtime.OvertimeTypeId = int.Parse(hiddenValue.Text.Trim());
            
            overtime.Name = txtName.Text.Trim();
            //overtime.CalculationType = int.Parse(cmbCalculationType.SelectedItem.Value);
            if (cmbIncomes.SelectedItem != null && cmbIncomes.SelectedItem.Value != null)
                overtime.IncomeId = int.Parse(cmbIncomes.SelectedItem.Value);
            //if (overtime.CalculationType == (int)OvertimeCalculationType.BasedOnSalary)
            //{
                //overtime.Rate = txtPercent.Number;

                //if (txtPercent.Number <= 0)
                //{
                //    NewMessage.ShowWarningMessage("Rate is required.");
                //    return;
                //}

                //if (treePanel.CheckedNodes != null)
                //{
                //    foreach (SubmittedNode item in treePanel.CheckedNodes)
                //    {
                        

                //        {
                           

                //            {
                //                int incomeId = int.Parse(item.Attributes["IncomeId"].ToString());
                //                overtime.OvertimeTypeIncomes.Add(new OvertimeTypeIncome { IncomeId=incomeId });
                //            }
                //        }
                //    }
                //}

                List<OvertimeTypeIncome> lines = new List<OvertimeTypeIncome>();
                string json = e.ExtraParams["GridIncomeValues"];

                if (string.IsNullOrEmpty(json))
                {
                    return;
                }

                lines = JSON.Deserialize<List<OvertimeTypeIncome>>(json);

                foreach (OvertimeTypeIncome item in lines)
                {
                    int incomeId = item.IncomeId;//int.Parse(item.Attributes["IncomeId"].ToString());
                    overtime.OvertimeTypeIncomes.Add(new OvertimeTypeIncome { IncomeId = incomeId });
                }

                List<OvertimeNotApplicableTo> lines2 = new List<OvertimeNotApplicableTo>();
                string json2 = e.ExtraParams["GridLevelsValues"];

                if (string.IsNullOrEmpty(json2))
                {
                    return;
                }

                lines2 = JSON.Deserialize<List<OvertimeNotApplicableTo>>(json2);

                foreach (OvertimeNotApplicableTo item in lines2)
                {
                    int levelId = item.LevelId;//int.Parse(item.Attributes["LevelId"].ToString());
                    overtime.OvertimeNotApplicableTos.Add(new OvertimeNotApplicableTo { LevelId = levelId });
                }


                //if (treePanelApplicableTo.CheckedNodes != null)
                //{
                //    foreach (SubmittedNode item in treePanelApplicableTo.CheckedNodes)
                //    {
                //        int levelId = int.Parse(item.Attributes["LevelId"].ToString());
                //        overtime.OvertimeNotApplicableTos.Add(new OvertimeNotApplicableTo { LevelId = levelId });
                //    }
                //}

            //}
            //else
            //{
            //    if (txtFixedRate.Number <= 0)
            //    {
            //        NewMessage.ShowWarningMessage("Rate is required.");
            //        return;
            //    }

            //    overtime.Rate = txtFixedRate.Number;
            //}


            Status status = OvertimeManager.InsertUpdateOvertimeType(overtime,isInsert);
            if (status.IsSuccess)
            {
                windowAddEditOvertime.Hide();
                LoadLevels();
                NewMessage.ShowNormalPopup("Overtime saved.");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }

        }

        protected void btnDetailSaveUpdate_Click(object sender, DirectEventArgs e)
        {
            List<OvertimeTypeLevel> lines = new List<OvertimeTypeLevel>();
            string json = e.ExtraParams["Values"];

            if (string.IsNullOrEmpty(json))
            {
                return;
            }


            lines = JSON.Deserialize<List<OvertimeTypeLevel>>(json);

            Status status = OvertimeManager.InsertUpdateOvertimeLevelDetail(lines);
            if (status.IsSuccess)
            {
                Window1.Hide();
                LoadLevels();
                NewMessage.ShowNormalPopup("Overtime rate saved.");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }


        }



        protected void btnCopy_Click(object sender, DirectEventArgs e)
        {
            List<OvertimeTypeLevel> lines = new List<OvertimeTypeLevel>();
            string json = e.ExtraParams["CopyValue"];

            if (string.IsNullOrEmpty(json))
            {
                return;
            }


            lines = JSON.Deserialize<List<OvertimeTypeLevel>>(json);

            if (lines == null)
            {
                X.Msg.Alert("", "No Value Selected").Show();
                return;
            }


            if (lines!=null && lines.Count>0)
            {
                if (lines.First().Rate != null)
                {
                    Hidden_CopyValue.Text = lines.First().Rate.ToString();
                    //X.Msg.Alert("", Hidden_CopyValue.Text).Show();
                    

                }
                else
                    Hidden_CopyValue.Text = "0";

                txtValueToBeCopied.Text = Hidden_CopyValue.Text;
                WindowCopy.Show();
            }
            else
            {
                X.Msg.Alert("", "No Value Selected").Show();
                return;
            }
            


        }


        protected void btnCopyTo_Click(object sender, DirectEventArgs e)
        {
            List<OvertimeTypeLevel> lines = new List<OvertimeTypeLevel>();
            string json = e.ExtraParams["SelectedLevels"];

            if (string.IsNullOrEmpty(json))
            {
                X.Msg.Alert("", "Please select atleast one Level.").Show();
                return;
            }


            lines = JSON.Deserialize<List<OvertimeTypeLevel>>(json);
            //X.Msg.Alert("", lines.Count()).Show();

            
            
                foreach (var val1 in lines)
                {
                    if(val1!=null)
                    {
                        //grid.store.getById(id).set('field_name', value)
                        storeAllowanceDetail.GetById(val1.LevelId.ToString()).Set("Rate", Hidden_CopyValue.Text);
                        storeAllowanceDetail.CommitChanges();
                    }
                }

                if (lines == null || lines.Count < 1)
                {
                    X.Msg.Alert("", "Please select atleast one Level.").Show();
                }
                else
                    WindowCopy.Close();
            
        }


        protected void btnLevelSaveUpdate_Click(object sender, DirectEventArgs e)
        {

            //Page.Validate("InsertUpdateLevel");
            //if (Page.IsValid)
            //{
            //    TAAllowance allowanceInstance = new TAAllowance();
            //    int allowanceID;
            //    allowanceInstance.Name = txtName.Text.Trim();
            //    allowanceInstance.Description = txtDescription.Text.Trim();

            //    bool isInsert = true;
                
            //    if (!string.IsNullOrEmpty(hiddenValue.Text))
            //    {
            //        allowanceInstance.AllowanceID = int.Parse(hiddenValue.Text.Trim());
            //        isInsert = false;
            //    }

            //    Status status = TravelAllowanceManager.InsertUpdateAllowance(allowanceInstance, isInsert);
            //    if (status.IsSuccess)
            //    {
            //        WindowLevel.Hide();
            //        LoadLevels();
            //        NewMessage.ShowNormalPopup("Allowance saved.");
            //    }
            //    else
            //    {
            //        NewMessage.ShowWarningMessage(status.ErrorMessage);
            //    }

            //}
             
             
        }


        private void BindApplicableToLevelTree()
        {           
            
            RowSelectionModel sm = this.GridLevels.GetSelectionModel() as RowSelectionModel;
            List<BLevel> levelList = NewPayrollManager.GetAllParentLevelList();
            gridNotApplicableTo.GetStore().DataSource = levelList;
            gridNotApplicableTo.GetStore();

  

            //Node rootNode = new Node();

            //foreach (var item in levelList)
            //{
            //    Node childNode = new Node();
            //    childNode.Checked = false;
            //    childNode.Expanded = true;
            //    childNode.CustomAttributes.Add(new ConfigItem{Name = "Name", Value = item.Name + " - "  + item.BLevelGroup.Name});
            //    childNode.CustomAttributes.Add(new ConfigItem { Name = "LevelId", Value = item.LevelId.ToString() });
            //    rootNode.Children.Add(childNode);
            //}

            //treePanelApplicableTo.SetRootNode(rootNode);

           

        }

        private void BindApplicableToLevelTreeList(List<OvertimeNotApplicableTo> list)
        {
            //List<OvertimeNotApplicableTo> levelList = NewPayrollManager.GetAllParentLevelList();
            RowSelectionModel sm = this.gridNotApplicableTo.GetSelectionModel() as RowSelectionModel;

            sm.ClearSelection();
            sm.SelectedRows.Clear();
            sm.UpdateSelection();
            foreach (OvertimeNotApplicableTo _list in list)
            {
                sm.SelectedRows.Add(new SelectedRow(_list.LevelId.ToString().ToLower()));

            }
            sm.UpdateSelection();


            //Node rootNode = new Node();

            //foreach (var item in levelList)
            //{
            //    Node childNode = new Node();

            //    if (list.Count > 0 && list.Any(x => x.LevelId == item.LevelId))
            //        childNode.Checked = true;
            //    else
            //        childNode.Checked = false;


            //    childNode.Expanded = true;
            //    childNode.CustomAttributes.Add(new ConfigItem { Name = "Name", Value = item.Name + " - " + item.BLevelGroup.Name });
            //    childNode.CustomAttributes.Add(new ConfigItem { Name = "LevelId", Value = item.LevelId.ToString() });
            //    rootNode.Children.Add(childNode);
            //}


            //gridNotApplicableTo.GetStore().DataSource = levelList;
            //gridNotApplicableTo.GetStore();


           // treePanelApplicableTo.SetRootNode(rootNode);
        }
 
    }
}

/*
 
 
 
*/