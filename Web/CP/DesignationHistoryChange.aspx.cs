﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using System.Text;

namespace Web.CP
{
    public partial class DesignationHistoryChange : BasePage
    {
        private CommonManager comm = new CommonManager();
        private List<DesignationHistory> list;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            this.CustomId = int.Parse(Request.QueryString["EIN"]);
            

         
            if (!IsPostBack)
            {
                Initialise();               
            }
            Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "clearUnload();");
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (gvw.SelectedIndex == -1)
                RegisterLastFromDate();
            else
            {
                int currentHistory = (int)gvw.DataKeys[gvw.SelectedIndex][0];

                DesignationHistory prevHistory = CommonManager.GetPreviousDesignationHistory(currentHistory);

                if (prevHistory != null)
                {
                    RegisterLastFromDateScript(prevHistory);
                }
            }

            //List<BranchDepartmentHistory> list = CommonManager.GetBranchDepartmentHistory(CustomId);
            //int branchID = -1,departmentID=-1;
            //if (list.Count > 0)
            //{
            //    branchID = list[list.Count - 1].BranchId.Value;
            //    departmentID = list[list.Count - 1].DepeartmentId.Value;
            //}
            //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "changedValue", "var ebranchID=" + branchID + ";var edepartmentID=" + departmentID +";", true);

            //if (this.Value == "true")
            //{

            //}

        }
        private void Initialise()
        {

            //ddlGrade.DataSource = comm.GetAllLocations();
            //ddlGrade.DataBind();

            CommonManager.SaveFirstIfDesignationNotExists(this.CustomId);

            calFromDate.IsEnglishCalendar = IsEnglish; calFromDate.SelectTodayDate();
            calFromDate.SelectTodayDate();

            LoadBranches();

            BindData();


           
            

        }
       

        void LoadBranches()
        {
            //ddlFilterByBranch.DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            //ddlFilterByBranch.DataBind();

            ddlDesignation.DataSource = new CommonManager().GetAllDesignations();
            ddlDesignation.DataBind();


            //if( ddlFilterByBranch.Items.Count >0)
            //{
            //    ddlFilterByBranch.Items[1].Selected = true;
            //    LoadEmployeesByBranch();
            //}
            //LoadEmployeesByBranch();
        }

        private void RegisterLastFromDate()
        {
            List<DesignationHistory> list = CommonManager.GetDesignationHistory(CustomId);

            if (list.Count > 0)
            {
                DesignationHistory lastHistory = list[list.Count - 1];

                RegisterLastFromDateScript(lastHistory);

            }

        }

        public bool IsEditable(int index)
        {
            if (list != null && list.Count>0)
            {
                if (list.Count - 1 == index)
                    return true;
            }
            return false;
        }

        private void RegisterLastFromDateScript(DesignationHistory lastHistory)
        {
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "regfromdate",
               string.Format("var fromDate = '{0}';", lastHistory.FromDate), true);
        }

        private void BindData()
        {
            PayManager mgr = new PayManager();


            list = CommonManager.GetDesignationHistory(CustomId);
            gvw.DataSource = list;
            gvw.DataBind();


           
        }

       

        protected void gvw_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvw.PageIndex = e.NewPageIndex;
            BindData();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

            if (Page.IsValid)
            {
                this.Value = "true";

                DesignationHistory history = Process(null);
                if (gvw.SelectedIndex != -1)
                {
                    
                    history.EmployeeDesignationId = (int)gvw.DataKeys[gvw.SelectedIndex][0];
                    CommonManager.UpdateDesignationHistory(history);

                    
                        divMsgCtl.InnerHtml = "Designation updated.";
                }
                else
                {
                    CommonManager.SaveDesignationHistory(history);
                
                        divMsgCtl.InnerHtml = "Designation saved.";
                }
                divMsgCtl.Hide = false;

                // Prepare Sub-Department List
                //StringBuilder str = new StringBuilder("");
                //bool first = true;
                //List<SubDepartment> deps = DepartmentManager.GetAllSubDepartmentsByDepartment(history.DepeartmentId.Value);
                //foreach (SubDepartment obj in deps)
                //{
                //    if (first == false)
                //        str.Append(",");
                //    str.Append("\"" + obj.SubDepartmentId + "$$" + obj.Name + "\"");
                //    first = false;
                //}
                ////Page.ClientScript.RegisterArrayDeclaration("texts", str.ToString());

                // Register to set Branch/Subdepartment from js in Employee page
                string updateJS = string.Format("opener.updateDesignationFromChangeHistory({0});",
                    history.DesignationId);
                Page.ClientScript.RegisterClientScriptBlock(GetType(), "sdfdsdfdsfsd", updateJS, true);


                ClearFields();
                gvw.SelectedIndex = -1;
                BindData();
                RegisterLastFromDate();
            }
        }

        private DesignationHistory Process(DesignationHistory entity)
        {
            if (entity == null)
            {
                entity = new DesignationHistory();
                entity.DesignationId = int.Parse(ddlDesignation.SelectedValue);
                entity.FromDate = calFromDate.SelectedDate.ToString();
                entity.FromDateEng = GetEngDate(entity.FromDate);
                entity.EmployeeId = this.CustomId;
                entity.Note = txtNote.Text.Trim();
                return entity;
            }
            else
            {
                UIHelper.SetSelectedInDropDown(ddlDesignation, entity.DesignationId);

                

                calFromDate.SetSelectedDate(entity.FromDate, IsEnglish);
                txtNote.Text = entity.Note;
            }
            return null;
        }

        protected void gvw_SelectedIndexChanged(object sender, EventArgs e)
        {
            //first record 
            int currentGradeHistory = (int)gvw.DataKeys[gvw.SelectedIndex][0];

            //EGradeHistory prevGradeHistory = CommonManager.GetPreviousGradeHistory(currentGradeHistory);

            //if (prevGradeHistory != null)
            //{
            //    RegisterLastFromDateScript(prevGradeHistory);
            //}


            DesignationHistory history = CommonManager.GetDesignationHistoryById(currentGradeHistory);
            Process(history);
            btnSave.Text = Resources.Messages.Update;

            if (history.IsFirst.Value)
                calFromDate.Enabled = false;
            else
                calFromDate.Enabled = true;

            //if first one then disable from date as it should be the same of emp first status from date
            if (CommonManager.GetDesignationHistory(this.CustomId)[0].EmployeeDesignationId == history.EmployeeDesignationId)
            {
                calFromDate.ToolTip = Resources.Messages.HistoryFirstFromDateNotEditable;
                //calFromDate.Enabled = false;
            }
            else
            {
                calFromDate.Enabled = true; ;
                calFromDate.ToolTip = "";
            }

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            gvw.SelectedIndex = -1;
            BindData();
            ClearFields();
        }


        private void ClearFields()
        {
            calFromDate.Enabled = true;
            txtNote.Text = "";
            calFromDate.ToolTip = "";
            btnSave.Text = Resources.Messages.Save;
            UIHelper.SetSelectedInDropDown(ddlDesignation, -1);
            calFromDate.SelectTodayDate();
        }

      
    }
}
