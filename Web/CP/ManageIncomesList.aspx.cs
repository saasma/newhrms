﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;
using System.Text;
using Utils.Helper;

namespace Web.CP
{
    public partial class ManageIncomesList : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                BindIncomeGrid();
            }

            JavascriptHelper.AttachPopUpCode(Page, "popupIncome", "AEIncome.aspx", 600, 600);
            
        }

        private void BindIncomeGrid()
        {
            if (BLL.BaseBiz.PayrollDataContext.VoucherGroups.Any() == false)
                voucherAlsoChange.Visible = false;

            //load incomes
            List<PIncome> list = PayManager.GetIncomeListByCompany(SessionManager.CurrentCompanyId);

            list.Add(new PIncome
            {
                IncomeId = 0,
                Title = "Provident Fund",
                Abbreviation = "PF",
                Calculation = IncomeCalculation.FIXED_AMOUNT,
                IsTaxable = true,
                IsEnabled = true,
                Order =
                    SessionManager.CurrentCompany.PFOrder
            });

            foreach (var item in list)
            {
                item.IsUpdatedWithLeaveValue = (item.IsUpdatedWithLeave == true ? "No" : "Yes");
                item.IsPFCalculatedValue = (item.IsPFCalculated == true ? "Yes" : "No");
                item.CalculationType = new DAL.IncomeCalculation().Get(item.Calculation);
                item.IncomeDeletable = PayManager.IsIncomeDeletable(item.IncomeId, item.IsBasicIncome);
                item.IsDefinedTypeValue = (item.IsDefinedType == null ? "" : (item.IsDefinedType == true ? "Yes" : ""));
                item.IsTaxableValue = (item.IsTaxable == true ? "Yes" : "No");
                item.IsPFCalculatedValue = (item.IsPFCalculated == true ? "Yes" : "");
                item.IsEnabledValue = (item.IsEnabled == true ? "Yes" : "");
            }

            string searchIncome = txtIncomeSearch.Text.Trim();
            storeIncomes.DataSource = list.Where(x => (searchIncome == "" || x.Title.ToLower().Contains(searchIncome.ToLower()))).OrderBy(x => x.Order).ToList();
            storeIncomes.DataBind();
        }

        protected void IncomeData_Refresh(object sender, StoreReadDataEventArgs e)
        {
            BindIncomeGrid();
        }

        protected void btnChangeOrder_Click(object sender, DirectEventArgs e)
        {
            int sourceId = int.Parse(hdnSource.Text.Trim());
            int destId = int.Parse(hdnDest.Text.Trim());
            string mode = hdnDropMode.Text.Trim();

            PayManager.ChangeIncomeOrder(sourceId, destId, mode);

            BindIncomeGrid();
        }

        protected void btnDelete_Click(object sender, DirectEventArgs e)
        {
            int incomeId = int.Parse(hdnIncomeId.Text);
            PayManager.DeleteIncome(incomeId);
            NewMessage.ShowNormalMessage("Income deleted successfully.");
            BindIncomeGrid();
        }

        protected void btnEnableDisable_Click(object sender, DirectEventArgs e)
        {
            int incomeId = int.Parse(hdnIncomeId.Text);
            Status status = PayManager.EnableDisablePIncome(incomeId);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage(string.Format("Income {0} successfully.", status.ErrorMessage));
                BindIncomeGrid();
            }
            else
                NewMessage.ShowWarningMessage(status.ErrorMessage);
        }

        protected void btnLoad_Click(object sender, DirectEventArgs e)
        {
            BindIncomeGrid();
        }
    }
}