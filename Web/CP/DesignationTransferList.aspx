﻿<%@ Page Title="Designation Change List" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="DesignationTransferList.aspx.cs" Inherits="Web.CP.DesignationTransferList" %>

<%@ MasterType VirtualPath="~/Master/NewDetails.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .empName
        {
            font-size: 20px;
            color: #439EC6;
            font-weight: bold;
        }
        .titleDesign
        {
            font-size: 15px;
            color: #000000;
            text-decoration: underline;
            padding-top: 10px;
        }
        
        .items span
        {
            display: block;
            padding-top: 3px;
            font-weight: bold;
        }
        
        .paddinAll
        {
            padding: 10px;
        }
       <%-- table tr td
        {
            padding: 0 10px 8px 0;
        }--%>
        h3
        {
            font: bold 13px Arial;
        }
        .marginTopss
        {
            margin-top: 10px;
            display: block;
        }
        .loanAdvTable
        {
            background-color: white;
            width: 60px;
            border: 1px solid white !important;
            padding: 0px;
        }
        .tableLightColor th
        {
            text-align: left;
        }
        strong
        {
            display: block;
            padding-bottom: 3px;
        }
        .tableLightColor input[type=text]
        {
            height: 20px;
        }
        .clsColor
        {
            background-color: #e8f1ff;
        }
         .BlueCls
        {
            color:Blue;
        }
    </style>
    <script type="text/javascript">

    function searchList() {
             <%=gridDesignationChange.ClientID %>.getStore().pageSize = <%=cmbPageSize.ClientID %>.getValue();
            <%=PagingToolbar1.ClientID %>.doRefresh();
        }

    function EmpSelect()
    {
        <%=hdnEmployeeId.ClientID %>.setValue(<%=cmbEmployee.ClientID %>.getValue());
        <%=btnSelectEmp.ClientID %>.fireEvent('click');
    }

    var CommandHandler = function(command, record){
            <%= hdnEmployeeDesignationId.ClientID %>.setValue(record.data.EmployeeDesignationId);
            <%=hdnEmployeeId.ClientID %>.setValue(record.data.EmployeeId);
                if(command=="Edit")
                {
                    <%= btnEdit.ClientID %>.fireEvent('click');
                }
                else
                {
                    <%= btnDelete.ClientID %>.fireEvent('click');
                }

             };

     function refreshWindow()
    {
        searchList();
    }
    
     var skipLoadingCheck = true;

    function closePopup() {
       window.close();
      window.opener.refreshWindow();
    }

    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
  <script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>
    <ext:Hidden ID="hdnEmployeeId" runat="server" />
    <ext:Hidden ID="hdnEmployeeDesignationId" runat="server" />
    <ext:Hidden ID="hdnMsg" runat="server" />
    <ext:Hidden ID="hdnEmpSearch" runat="server" />
    <ext:LinkButton ID="btnSelectEmp" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnSelectEmp_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:LinkButton ID="btnEdit" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnEdit_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Designation Change List
                </h4>
            </div>
        </div>
    </div>
    <ext:LinkButton ID="btnDelete" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnDelete_Click">
                <Confirmation ConfirmRequest="true" Message="Are you sure,you want to delete the change?" />
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <div class="contentpanel" style="margin-top: 0px !important; padding-top: 0px !important;">
        <div class="innerLR">
            <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                <Proxy>
                    <ext:AjaxProxy Url="../Handler/EmpSearchID.ashx">
                        <ActionMethods Read="GET" />
                        <Reader>
                            <ext:JsonReader Root="plants" TotalProperty="total" />
                        </Reader>
                    </ext:AjaxProxy>
                </Proxy>
                <Model>
                    <ext:Model ID="Model5" IDProperty="Name" Name="ItemLineModel" runat="server">
                        <Fields>
                            <ext:ModelField Name="Name" Type="String" />
                            <ext:ModelField Name="EmployeeId" Type="String" />
                        </Fields>
                    </ext:Model>
                </Model>
            </ext:Store>
            <div class="alert alert-info" style="margin-top: 10px; padding-top: 0px!important;">
                <ext:Container ID="listingContainer" runat="server">
                    <Content>
                        <table>
                            <tr>
                                <td style="width: 160px;">
                                    <ext:DateField ID="txtFromDate" runat="server" FieldLabel="From" LabelSeparator=""
                                        LabelAlign="Top" Width="150">
                                        <Plugins>
                                            <ext:GenericPlugin ID="HighlightDates1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                                        </Plugins>
                                    </ext:DateField>
                                </td>
                                <td style="width: 160px;">
                                    <ext:DateField ID="txtToDate" runat="server" FieldLabel="To" LabelSeparator="" LabelAlign="Top"
                                        Width="150">
                                        <Plugins>
                                            <ext:GenericPlugin ID="GenericPlugin1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                                        </Plugins>
                                    </ext:DateField>
                                </td>
                                <td style="width: 310px;">
                                    <ext:ComboBox LabelSeparator="" ID="cmbEmpSearch" FieldLabel="Search Employee" EmptyText="Employee Name"
                                        LabelWidth="80" LabelAlign="Top" runat="server" DisplayField="Name" ValueField="EmployeeId"
                                        StoreID="storeSearch" TypeAhead="false" Width="300" PageSize="9999" HideBaseTrigger="true"
                                        MinChars="1" TriggerAction="All" ForceSelection="false">
                                        <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                                            <ItemTpl ID="ItemTpl2" runat="server">
                                                <Html>
                                                    <tpl>
                                                        <div class="search-item">
                                                                        <span>{Name}</span>  
                                                         </div>
					                            </tpl>
                                                </Html>
                                            </ItemTpl>
                                        </ListConfig>
                                        <Triggers>
                                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                        </Triggers>
                                        <Listeners>
                                            <Select Handler="this.getTrigger(0).show();" />
                                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                            <TriggerClick Handler="if (index == 0) { 
                                                       this.clearValue(); 
                                                       this.getTrigger(0).hide();
                                                   }" />
                                        </Listeners>
                                    </ext:ComboBox>
                                </td>
                                <td>
                                    <ext:Button ID="btnLoad" runat="server" Text="Load" Width="100" Cls="btn btn-default"
                                        MarginSpec="25 10 10 10">
                                        <Listeners>
                                            <Click Fn="searchList">
                                            </Click>
                                        </Listeners>
                                    </ext:Button>
                                </td>
                                <td>
                                    <ext:Button ID="btnNew" MarginSpec="25 10 10 10" Width="180" Height="30" runat="server"
                                        Text="New Designation Change">
                                        <DirectEvents>
                                            <Click OnEvent="btnNewTransfer_Click">
                                                <EventMask ShowMask="true" />
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </td>
                                <td>
                                    <ext:LinkButton runat="server" StyleSpec="padding:0px;text-decoration: underline; font-size:14px;" ID="btnImport" MarginSpec="25 10 10 10"
                                        OnClientClick="DesignationHistImport();return false;" Text="<i></i>Import from Excel">
                                    </ext:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </Content>
                </ext:Container>
            </div>
            <ext:KeyMap ID="KeyMap1" runat="server" Target="={#{listingContainer}.getBody()}">
                <Binding>
                    <ext:KeyBinding Handler="#{btnLoad}.fireEvent('click');">
                        <Keys>
                            <ext:Key Code="ENTER" />
                        </Keys>
                    </ext:KeyBinding>
                </Binding>
            </ext:KeyMap>
            <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridDesignationChange" runat="server"
                Cls="itemgrid" OnReadData="Store_ReadData">
                <Store>
                    <ext:Store ID="storeEmpList" runat="server" AutoLoad="true" OnReadData="Store_ReadData"
                        RemotePaging="true" RemoteSort="true">
                        <Proxy>
                            <ext:PageProxy />
                        </Proxy>
                        <Model>
                            <ext:Model ID="Model1" runat="server" IDProperty="EmployeeDesignationId">
                                <Fields>
                                    <ext:ModelField Name="EmployeeDesignationId" Type="Int" />
                                    <ext:ModelField Name="FromDate" Type="String" />
                                    <ext:ModelField Name="FromDateEng" Type="Date" />
                                    <ext:ModelField Name="EmployeeId" Type="String" />
                                    <ext:ModelField Name="EmployeeName" Type="String" />
                                    <ext:ModelField Name="FromDesignation" Type="String" />
                                    <ext:ModelField Name="ToDesignation" Type="String" />
                                    <ext:ModelField Name="INo" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Sorters>
                            <ext:DataSorter Property="FromDateEng" Direction="ASC" />
                        </Sorters>
                    </ext:Store>
                </Store>
                <ColumnModel ID="ColumnModel1" runat="server">
                    <Columns>
                        <ext:Column ID="colFromDate" runat="server" Text="Date" DataIndex="FromDate" MenuDisabled="true"
                            Sortable="false" Align="Left" Width="100" />
                        <ext:DateColumn ID="colFromDateEng" runat="server" Text="Date(AD)" DataIndex="FromDateEng"
                            Format="yyyy-MMM-dd" MenuDisabled="true" Sortable="false" Align="Left" Width="100" />
                        <ext:Column ID="colEmployeeId" runat="server" Text="EIN" DataIndex="EmployeeId" MenuDisabled="true"
                            Sortable="false" Align="Center" Width="80" />
                             <ext:Column ID="Column1" runat="server" Text="I No" DataIndex="INo" MenuDisabled="true"
                            Sortable="false" Align="Center" Width="80" />
                        <ext:Column ID="colEmployeeName" runat="server" Text="Name" DataIndex="EmployeeName"
                            MenuDisabled="true" Sortable="false" Align="Left" Width="250" />
                        <ext:Column ID="colFromBranch" runat="server" Text="From Designation" DataIndex="FromDesignation"
                            MenuDisabled="true" Sortable="false" Align="Left" Width="250" />
                        <ext:Column ID="colToBranch" runat="server" Text="To Designation" DataIndex="ToDesignation"
                            MenuDisabled="true" Sortable="false" Align="Left" Width="250" />
                        <ext:CommandColumn ID="CommandColumn1" runat="server" Width="80" Text="" Align="Center">
                            <Commands>
                                <ext:CommandSeparator />
                                <ext:GridCommand Cls="editGridButton" ToolTip-Text="Edit" Text="<i class='fa fa-pencil'></i>"
                                    CommandName="Edit" />
                                  <ext:GridCommand Cls="editGridButton" ToolTip-Text="Delete" Text="<i class='fa fa-trash-o'></i>"
                                    CommandName="Delete" />
                            </Commands>
                            <Listeners>
                                <Command Handler="CommandHandler(command,record);" />
                            </Listeners>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
                <Plugins>
                    <ext:FilterHeader ID="FilterHeader1" runat="server" OnCreateFilterableField="OnCreateFilterableField" />
                </Plugins>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar1" runat="server" StoreID="storeEmpList" DisplayInfo="true">
                        <Items>
                            <ext:Label ID="lblPageSize" runat="server" Text="Page size:" />
                            <ext:ToolbarSpacer ID="ToolbarSpacer_listItems" runat="server" Width="10" />
                            <ext:ComboBox ID="cmbPageSize" runat="server" Width="80" SelectOnFocus="true" Selectable="true"
                                ValueField="Value" DisplayField="Text" ForceSelection="true" AllowBlank="false">
                                <Listeners>
                                    <Select Handler="searchList()" />
                                    <%--#{cmbPageSize}.pageSize = this.getValue();#{PagingToolbar1}.moveFirst();--%>
                                </Listeners>
                                <Items>
                                    <ext:ListItem Value="20" Text="20" />
                                    <ext:ListItem Value="50" Text="50" />
                                    <ext:ListItem Value="100" Text="100" />
                                </Items>
                                <SelectedItems>
                                    <ext:ListItem Index="0">
                                    </ext:ListItem>
                                </SelectedItems>
                            </ext:ComboBox>
                        </Items>
                    </ext:PagingToolbar>
                </BottomBar>
            </ext:GridPanel>
            <div class="alert alert-info" style="margin-top: 15px; height: 50px;">
                <div style="float: right;">
                    <ext:Button runat="server" AutoPostBack="true" OnClick="btnExport_Click" ID="btnExport"
                        Text="<i></i>Export To Excel">
                    </ext:Button>
                </div>
            </div>
        </div>
    </div>
    <ext:Window ID="WDesignation" runat="server" Title="Designation Change Details" Icon="Application"
        Width="550" Height="600" BodyPadding="5" Hidden="true" Modal="true">
        <Content>
            <table class="fieldTable" style="margin-left: 20px;">
                <tr>
                    <td>
                        <ext:ComboBox LabelSeparator="" ID="cmbEmployee" FieldLabel="Search Employee *" EmptyText="Search Employee"
                            LabelWidth="118" LabelAlign="Left" runat="server" DisplayField="Name" ValueField="EmployeeId"
                            StoreID="storeSearch" TypeAhead="false" Width="300" PageSize="9999" HideBaseTrigger="true"
                            MinChars="1" TriggerAction="All" ForceSelection="false">
                            <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                                <ItemTpl ID="ItemTpl1" runat="server">
                                    <Html>
                                        <tpl>
                                            <div class="search-item">
                                                            <span>{Name}</span>  
                                             </div>
					                </tpl>
                                    </Html>
                                </ItemTpl>
                            </ListConfig>
                            <Triggers>
                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                            </Triggers>
                            <Listeners>
                                <Select Handler="this.getTrigger(0).show(); EmpSelect();" />
                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                            </Listeners>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator ValidationGroup="SaveDesTransfer" ID="valEmployeeReq"
                            ControlToValidate="cmbEmployee" Display="None" runat="server" ErrorMessage="Please select an Employee."></asp:RequiredFieldValidator>
                    </td>
                </tr>
            </table>
            <div id="block" runat="server">
                <table style='clear: both; padding: 10px; margin-top: 10px;'>
                    <tr>
                        <td valign="top" style="padding-left: 10px;">
                            <ext:Image ID="image1" runat="server" ImageUrl="~/images/sample.jpg" Width="120"
                                Height="120" />
                        </td>
                        <td valign="top" style="padding-left: 15px" class="items">
                            <ext:Label ID="lblEName" StyleHtmlCls="empName" runat="server" />
                            <br />
                            <ext:Label ID="lblEWorkingIn" runat="server" Text="Working In" StyleHtmlCls="titleDesign" />
                             <br />
                             <ext:Label ID="lblLevelDesignation" runat="server" />
                            <br />
                            <ext:Label ID="lblEBranch" runat="server" />, 
                            <ext:Label ID="lblEDepartment" runat="server" />
                            <br />
                            <ext:Label ID="lblESince" runat="server" />
                            <br />
                            <ext:Label ID="lblInTime" runat="server" />
                            <br />
                        </td>
                    </tr>
                </table>
            </div>
            <table class="fieldTable" style="margin-left: 20px;">
                <tr>
                    <td colspan="4">
                        <ext:ComboBox ID="cmbEventType" runat="server" ValueField="EventID" DisplayField="Name"
                            Width="180" LabelWidth="180" LabelAlign="Top" LabelSeparator="" FieldLabel="Event Type" ForceSelection="true"
                            QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store4" runat="server">
                                    <Model>
                                        <ext:Model ID="Model6" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="EventID" Type="String" />
                                                <ext:ModelField Name="Name" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator ID="reqdEventType" runat="server" ControlToValidate="cmbEventType"
                            Display="None" ErrorMessage="Event is required." ValidationGroup="SaveDesTransfer"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:ComboBox ID="cmbLevel" runat="server" LabelAlign="Top" FieldLabel="Level/Position *"
                            ValueField="LevelId" DisplayField="GroupLevel" Width="280" LabelSeparator=""
                            ForceSelection="true" QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store2" runat="server">
                                    <Model>
                                        <ext:Model ID="Model2" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="LevelId" Type="String" />
                                                <ext:ModelField Name="GroupLevel" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <DirectEvents>
                                <Select OnEvent="cmbLevel_Select">
                                    <EventMask ShowMask="true" />
                                </Select>
                            </DirectEvents>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="cmbLevel"
                            Display="None" ErrorMessage="Level/Position is required." ValidationGroup="SaveDesTransfer"></asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <ext:ComboBox ID="cmbDesignation" runat="server" LabelAlign="Top" FieldLabel=" Designation *"
                            ValueField="DesignationId" DisplayField="Name" Width="180" LabelSeparator=""
                            ForceSelection="true" QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store1" runat="server">
                                    <Model>
                                        <ext:Model ID="Model3" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="DesignationId" Type="String" />
                                                <ext:ModelField Name="Name" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator ID="rfvDepartment" runat="server" ControlToValidate="cmbDesignation"
                            Display="None" ErrorMessage="Designation is required." ValidationGroup="SaveDesTransfer"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        <pr:CalendarExtControl FieldLabel="Applicable Date *" LabelAlign="Top" Width="180"
                            ID="calFromDate" runat="server" LabelSeparator="" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="calFromDate"
                            Display="None" ErrorMessage="Applicable date is required." ValidationGroup="SaveDesTransfer"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <ext:TextArea FieldLabel="Note" LabelAlign="Top" ID="txtNote" runat="server" LabelSeparator=""
                            Rows="3" Width="470" Height="80" />
                    </td>
                </tr>
                <tr>
                    <td valign="bottom" colspan="4">
                        <div id="buttonsBar" runat="server" class="popupButtonDiv" style="float: right; margin-right: 2px;">
                            <div style="width: 100px;">
                                <ext:Button runat="server" ID="btnSave" Cls="btn btn-primary" Width="90" Text="<i></i>Save">
                                    <DirectEvents>
                                        <Click OnEvent="btnSave_Click">
                                            <EventMask ShowMask="true" />
                                        </Click>
                                    </DirectEvents>
                                    <Listeners>
                                        <Click Handler="valGroup = 'SaveDesTransfer'; if(CheckValidation()) return this.disable(); else return false;">
                                        </Click>
                                    </Listeners>
                                </ext:Button>
                            </div>
                            <ext:Button runat="server" ID="btnCancel" Cls="btn btn-default" Text="Cancel">
                                <Listeners>
                                    <Click Handler="#{WDesignation}.hide()" />
                                </Listeners>
                            </ext:Button>
                        </div>
                    </td>
                </tr>
            </table>
        </Content>
    </ext:Window>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
