﻿<%@ Page Title="Stop Payment" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="StopPaymentPG.aspx.cs" MaintainScrollPositionOnPostback="true"
    Inherits="Web.CP.StopPaymentPG" %>

<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .mainFieldset
        {
            -moz-box-shadow: 1px 1px 3px #DDDDDD;
            background: none repeat scroll 0 0 #E9F0FB;
            border-color: #10B0EA;
            border-style: solid;
            border-width: 3px 1px 1px;
            float: left;
            font: bold 18px Arial,Helvetica,sans-serif;
            height: auto !important;
            margin-bottom: 20px;
            margin-top: 0;
            min-height: 80px;
            padding: 8px;
            position: relative;
            width: 800px !important;
        }
        
        .mainFieldset table
        {
            padding: 10px 20px 0;
        }
        .mainFieldset table tr td
        {
            padding: 0 10px 10px 0;
            text-align: left;
        }
        strong
        {
            display: block;
            padding-bottom: 5px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Stop Payment</h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
        <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
        <div class="mainFieldset">
            <table cellpadding="0" cellspacing="0" width="700px">
                <tr>
                    <td width="320px">
                        <asp:DropDownList AppendDataBoundItems="true" ID="ddlEmployees" DataTextField="Name"
                            DataValueField="EmployeeId" onchange="setEIN(this);" runat="server" OnSelectedIndexChanged="ddlEmployees_SelectedIndexChanged">
                            <asp:ListItem Value="-1">--Select Employee--</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator InitialValue="-1" ID="valReqdResPerson" runat="server"
                            ControlToValidate="ddlEmployees" Display="None" ErrorMessage="Please select an employee."
                            ValidationGroup="StopPayment"></asp:RequiredFieldValidator>
                        EIN:
                        <asp:Label ID="lblEmpId" Font-Bold="true" runat="server"></asp:Label>
                    </td>
                    <td>
                        <%--                        <strong>Financial Year:</strong>
                        <asp:DropDownList ID="ddlFinancialYears" runat="server" DataTextField="Name" DataValueField="FinancialDateId">
                            <asp:ListItem Value="-1" Text="All" />
                        </asp:DropDownList>--%>
                    </td>
                </tr>
                <tr>
                    <td width="360px" style='padding-top: 10px'>
                        <strong>From date:</strong>
                        <My:Calendar Id="calFromDate" runat="server" />
                    </td>
                    <td width="360px">
                        <strong>To date:</strong>
                        <My:Calendar Id="calToDate" runat="server" />
                        <asp:TextBox Style='display: none' ID="txt" runat="server" />
                        <asp:CustomValidator IsCalendar="true" ID="valCustomeToDate2" runat="server" ValidateEmptyText="true"
                            ValidationGroup="StopPayment" ControlToValidate="txt" Display="None" ErrorMessage="Ending date must be greater than starting date by month."
                            ClientValidationFunction="validateDOJToDate" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Exclude the period in Workdays Count (Gratuity)
                         <asp:DropDownList AppendDataBoundItems="true" style='margin-top: 5px;' ID="ddlWorkdays" 
                            runat="server">
                            <asp:ListItem Value="-1">--Select Workday setting--</asp:ListItem>
                            <asp:ListItem Value="true" Text="Yes" />
                            <asp:ListItem Value="false" Text="No" />
                        </asp:DropDownList>
                         <asp:RequiredFieldValidator InitialValue="-1" ID="RequiredFieldValidator1" runat="server"
                            ControlToValidate="ddlWorkdays" Display="None" ErrorMessage="Workday setting is required."
                            ValidationGroup="StopPayment"></asp:RequiredFieldValidator>
                        <%--<asp:CheckBox ID="chkExcludeWorkdays" runat="server" Checked="true" Text="Exclude the period in Workdays Count (Gratuity)" />--%>
                    </td>
                </tr>
                <tr style="display: none">
                    <td>
                        <asp:CheckBox ID="chkTreatLikeRetForTax" runat="server" Text="Treat like Retiring Employee for Tax Calculation" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>Notes</strong>
                        <asp:TextBox MaxLength="200" ID="txtNotes" TextMode="MultiLine" Width="300px" Rows="4"
                            runat="server"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </div>
        <div class="clear">
            <asp:Button ID="btnSave" OnClientClick="valGroup='StopPayment';return CheckValidation()"
                runat="server" Text="Save" CssClass="save" OnClick="btnSave_Click" />
            <asp:Button ID="btnCancel" runat="server" CssClass="cancel" Text="Cancel" OnClick="btnCancel_Click" />
            <br />
            <div style="margin-left: 700px">
                <asp:CheckBox ID="chkShowArchieve" AutoPostBack="true" OnCheckedChanged="chkShowArchieve_Changed"
                    runat="server" Text="Show Archieve" />
            </div>
            <br />
            <div style='clear: both'>
                <cc2:EmptyDisplayGridView ID="gvw" ShowHeaderWhenEmpty="true" runat="server" CellPadding="4"
                    CssClass="tableLightColor" GridLines="None" DataKeyNames="StopPaymentId,EmployeeId"
                    Width="1100px" AutoGenerateColumns="False" OnPageIndexChanging="gvw_PageIndexChanging"
                    OnSelectedIndexChanged="gvw_SelectedIndexChanged1" OnRowDeleting="gvw_RowDeleting">
                    <RowStyle BackColor="#E3EAEB" />
                    <Columns>
                        <asp:BoundField DataField="EmployeeId" ItemStyle-Width="70" DataFormatString="{0:000}"
                            HeaderText="EIN" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField DataField="Name" HeaderText="Name" ItemStyle-Width="150px" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField DataField="FromDate" HeaderText="From Date" ItemStyle-Width="80"
                            HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField DataField="EngFromDate" DataFormatString='{0:yyyy-MMM-dd}' HeaderText="From Date Eng"
                            ItemStyle-Width="90" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField DataField="ToDate" HeaderText="To Date" ItemStyle-Width="80" HeaderStyle-HorizontalAlign="Left" />
                         <asp:BoundField DataField="EngToDate" DataFormatString='{0:yyyy-MMM-dd}' HeaderText="To Date Eng"
                            ItemStyle-Width="90" HeaderStyle-HorizontalAlign="Left" />
                        <asp:TemplateField HeaderText="Workdays Ded" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <%# Convert.ToBoolean( Eval("ExcludePeriodInWorkDaysCount").ToString()) ? "Yes" : "No"  %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Notes" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <%# Eval("Notes") %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="30px" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:ImageButton ID="ImageButton2" Visible='<%# IsDeletable(Eval("StopPaymentId")) %>'
                                    OnClientClick="return confirm('Confirm delete the stop payment?');" runat="server"
                                    CommandName="Delete" ImageUrl="~/images/delete.gif" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="30px" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:ImageButton ID="ImageButton1" Visible='<%# IsStopPaymentEditable(Eval("StopPaymentId")) %>'
                                    runat="server" CommandName="Select" ImageUrl="~/images/edit.gif" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="odd" />
                    <AlternatingRowStyle CssClass="even" />
                    <EmptyDataTemplate>
                        No Stop Payments.
                    </EmptyDataTemplate>
                </cc2:EmptyDisplayGridView>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="pageScriptsContent" runat="server">
    <script type="text/javascript">

        function setEIN(ddl) {
            document.getElementById('<%= lblEmpId.ClientID %>').innerHTML = ddl.value;
        }

        function resumePaymentPopupCall(stopPaymentId) {
            var ret = resumePaymentPopup("Id=" + stopPaymentId);
            if (typeof (ret) != 'undefined') {
                if (ret == "Reload") {
                    __doPostBack("Reload", "");
                }
            }
        }


        function validateDOJToDate(source, args) {
            //debugger;

            args.IsValid = isSecondCalendarCtlDateGreater(getCalendarSelectedDate('<%= calFromDate.ClientID %>'),
         getCalendarSelectedDate('<%= calToDate.ClientID %>'));

        }

    </script>
</asp:Content>
