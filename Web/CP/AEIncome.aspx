<%@ Page Language="C#" MasterPageFile="~/Master/ForPopupPage.Master" AutoEventWireup="true"
    CodeBehind="AEIncome.aspx.cs" Title="Income details" Inherits="Web.CP.AEIncome" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningMsgCtl" TagPrefix="u2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .paddinAll
        {
            padding: 10px;
        }
        table tr td
        {
            padding: 0 10px 8px 0;
        }
        .chkUnitList span
        {
            display: block;
            padding-top: 8px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <asp:HiddenField ID="hdEditSequence" runat="server" />
    <asp:HiddenField ID="hdEmployeeEditSequence" runat="server" />
    <asp:HiddenField ID="hiddenEmployeeId" Value="0" runat="server" />
    <div class="popupHeader">
        <table class="style1" runat="server" id="Table1" style='margin-left: 30px; padding-top: 3px'
            cellpadding="0" cellspacing="0">
            <tr runat="server" id="rowIncrement">
                <td width="100px">
                    <asp:Label ID="lblMode" runat="server" Text="Edit Mode" />
                </td>
                <td colspan="3">
                    <asp:HiddenField ID="hdnIncrementId" runat="server" />
                    <asp:DropDownList ID="ddlEvents" AppendDataBoundItems="true" DataTextField="Name"
                        DataValueField="EventID" runat="server" Width="150px">
                        <asp:ListItem Text="--Select Event--" Value="-1" Selected="True" />
                    </asp:DropDownList>
                    <%-- <asp:RequiredFieldValidator ID="valReqdEvent" InitialValue="-1" runat="server"
                        Display="None" ValidationGroup="AEIncome" ErrorMessage="Event type selection is required.."
                        ControlToValidate="ddlEvents" />--%>
                    <asp:CheckBox ID="chkIsIncrementMode" runat="server" />
                    <span style="padding-right: 5px">Effective from</span>
                    <My:Calendar Id="calIncrementDate" runat="server" />
                    <a href='javascript:incrementHistoryCall(<%= this.CustomId %>)' style="padding-left: 5px;
                        color: white; text-decoration: underline">History</a>
                </td>
            </tr>
        </table>
    </div>
    <div class=" marginal" style="margin-top: 10px">
        <table class="style1" style='margin-left: 10px;' runat="server" id="tblIncrement"
            cellpadding="0" cellspacing="0">
            <tr>
                <td colspan="4">
                    <u2:WarningMsgCtl ID="divWarningMsg" Width="100%" EnableViewState="false" Hide="true"
                        runat="server" />
                </td>
            </tr>
            <tr>
                <td width="40px">
                    Title&nbsp;*
                </td>
                <td width="185px">
                    <asp:TextBox ID="txtTitle" Width="185px" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="valReqdTitle" runat="server" ControlToValidate="txtTitle"
                        Display="None" ErrorMessage="Income title is required." ValidationGroup="AEIncome"></asp:RequiredFieldValidator>
                </td>
                <td width="80px">
                    Short name&nbsp;*
                </td>
                <td width="165px">
                    <asp:TextBox ID="txtAbbreviation" Width="165px" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtAbbreviation"
                        Display="None" ErrorMessage="Income short name is required." ValidationGroup="AEIncome"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td colspan="4" runat="server" id="tdNotes">
                    Notes
                    <asp:TextBox Style="margin-left: 15px;" runat="server" ID="txtNotes" Width="452px" />
                </td>
            </tr>
        </table>
        <div class="clear">
            <fieldset class="large-heading">
                <asp:Image ID="imageOptions" runat="server" onclick="showHideOptions()" ImageAlign="Left"
                    ImageUrl="~/images/expand.jpg" Style='padding-right: 4px; padding-top: 2px; cursor: pointer' />
                <h2 style='cursor: pointer; color: #2F7CD3' runat="server" id="headerOptions" onclick="showHideOptions()">
                    More Options</h2>
                <asp:Panel runat="server" class="bevel paddinAll" ID="pnlOptions" Style="width: 500px;
                    display: none">
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="300">
                                <asp:CheckBox ID="chkBasicIncome" Text="Basic income" runat="server" Enabled="false" />
                            </td>
                            <td id="rowPF" runat="server" width="200">
                                <asp:CheckBox ID="chkPF" runat="server" Text="PF Calculated on this Income" />
                            </td>
                        </tr>
                        <tr>
                            <td width="300">
                                <asp:CheckBox ID="chkIsTaxable" runat="server" Text="Income is not Taxable" />
                            </td>
                            <td width="300" style="display: none">
                                <asp:CheckBox ID="chkCountForLeaveEncasement" runat="server" Text="Include for Leave Encashment" />
                            </td>
                        </tr>
                        <tr>
                            <td width="300">
                                <asp:CheckBox ID="chkUpdateWithLeave" runat="server" Text="Unpaid Leaves do not affect this income" />
                            </td>
                            <td width="300">
                                <asp:CheckBox ID="chkIsIncomeEnabled" runat="server" Checked="true" Text="Is Income Active" />
                            </td>
                        </tr>
                        <tr>
                            <td width="300">
                                <asp:CheckBox Visible="false" ID="chkIsForeignAllowance" Text="Foreign Allowance"
                                    runat="server" />
                            </td>
                            <td id="rowIsGrade" runat="server" width="200">
                                <asp:CheckBox ID="chkIsGrade" Text="Grade Type" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td runat="server" id="rowchkIsTreatLikeOneTimeIncome">
                                <asp:CheckBox Checked="true" ID="chkIsTreatLikeOneTimeIncome" Text="Treat like one time for tax calculation"
                                    runat="server" />
                            </td>
                            <td runat="server" id="rowIsChildrenAllowance">
                                <asp:CheckBox ID="chkIsChildrenAllowance" Text="Children Allowance" runat="server" />
                            </td>
                        </tr>
                        <tr runat="server" id="rowUnitRateWorkdays">
                            <td runat="server">
                                <asp:CheckBox ID="chkUnitRateBasedOnWorkdays" Text="Based on workdays" runat="server" />
                            </td>
                        </tr>
                        <tr runat="server" id="rowLateType">
                            <td id="Td1" runat="server">
                                <asp:CheckBox ID="chkIsLateIncomeType" Text="Late Income Type" runat="server" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </fieldset>
        </div>
        <fieldset class="large-heading" runat="server" id="pnlParent">
            <h2>
                Default Setup</h2>
            <div class="bevel paddinAll" style="width: 500px">
                <table cellpadding="0" cellspacing="0" width="500px">
                    <tr>
                        <td width="140px">
                            Calculation method
                        </td>
                        <td width="340px">
                            <asp:DropDownList AppendDataBoundItems="true" ID="ddlCalculation" runat="server"
                                onchange="changeCalculationDisplay(this)">
                                <asp:ListItem Text="--Income Calculation Type--" Value="-1"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:CheckBox ID="chkIsEmployeeIncomeEnabled" runat="server" Checked="true" Text="Is Active for Employee" />
                            <asp:RequiredFieldValidator InitialValue="-1" ID="valReqdCalculation" runat="server"
                                ControlToValidate="ddlCalculation" Display="None" ErrorMessage="Please select an income calulation method."
                                ValidationGroup="AEIncome"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr runat="server" id='rowStatusFilter1' style='display: none'>
                        <td width="140px" style='vertical-align: top'>
                            <asp:CheckBox ID="chkApplyStatusFilter" runat="server" Text="Status Filter" />
                        </td>
                        <td width="340px">
                            <div runat="server" id="divStatus" style="overflow-y: scroll; display: none; height: 70px;
                                width: 250px; border: 1px solid #C1D6F3; padding: 0px">
                                <asp:CheckBoxList ID="chkStatusList" RepeatColumns="2" RepeatDirection="Horizontal"
                                    runat="server" DataTextField="Value" DataValueField="Key">
                                </asp:CheckBoxList>
                            </div>
                        </td>
                    </tr>
                    <tr>
                    </tr>
                </table>
                <%--for Fixed & Variable amount--%>
                <asp:Panel ID="pnlFixedVariable" runat="server">
                    <table cellpadding="0" cellspacing="0" width="500px">
                        <tr>
                            <td width="140px">
                                Amount
                            </td>
                            <td width="340px">
                                <asp:TextBox ID="txtAmount" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtAmount"
                                    Display="None" ErrorMessage="Amount is required." ValidationGroup="AEIncome"></asp:RequiredFieldValidator>
                                <asp:CompareValidator Operator="DataTypeCheck" ValueToCompare="0" ValidationGroup="AEIncome"
                                    Type="Currency" ID="valCompareVariableFixedAmount" Display="None" ControlToValidate="txtAmount"
                                    runat="server" ErrorMessage="Invalid amount."></asp:CompareValidator>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="pnlUnitRate" runat="server">
                    <table cellpadding="0" cellspacing="0" width="500px">
                        <tr>
                            <td width="140px" valign="top" style="padding-top: 5px;">
                                <span runat="server" id="span1">Half day value </span>
                            </td>
                            <td width="340px">
                                <asp:TextBox Width='70px' ID="txtUnitRateHalfdayValue" runat="server"></asp:TextBox>
                                <br />
                                <br />
                                Note : if blank, half day will be counted as 1 fullday in workday calculation
                                <asp:CompareValidator Operator="GreaterThanEqual" ValueToCompare="0" ValidationGroup="txtUnitRate"
                                    Type="Double" ID="CompareValidator6" Display="None" ControlToValidate="txtUnitRateHalfdayValue"
                                    runat="server" ErrorMessage="Invalid unit rate half day value."></asp:CompareValidator>
                            </td>
                        </tr>
                        <tr>
                            <td width="140px" valign="top" style="padding-top: 5px;">
                                <span runat="server" id="spanUnitRate">Rate </span>
                            </td>
                            <td width="340px">
                                <asp:TextBox ID="txtUnitRate" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtUnitRate"
                                    Display="None" ErrorMessage="Rate is required." ValidationGroup="AEIncome"></asp:RequiredFieldValidator>
                                <asp:CompareValidator Operator="GreaterThanEqual" ValueToCompare="0" ValidationGroup="txtUnitRate"
                                    Type="Currency" ID="CompareValidator1" Display="None" ControlToValidate="txtAmount"
                                    runat="server" ErrorMessage="Invalid rate."></asp:CompareValidator>
                                <fieldset title="Workday Calcualtion" class="chkUnitList1" style="margin-top: 10px;
                                    display: none">
                                    <legend style="font-weight: bold; font-size: 12px;">Workday Calculation</legend>
                                    <asp:CheckBox ID="chkDeductHoliday" runat="server" Text="Deduct Holiday" ToolTip="Weekly and National holiday" />
                                    <asp:CheckBox ID="chkDeductPaidLeave" runat="server" Text="Deduct Paid Leave" />
                                    <asp:CheckBox ID="chkDeductUnpaidLeave" runat="server" Text="Deduct Un-Paid Leave" />
                                    <asp:CheckBox ID="chkDeductHalfdayHoliday" runat="server" Text="Deduct Half-day holiday" />
                                    <asp:CheckBox ID="chkDeductHalfdayLeave" runat="server" Text="Deduct Half-day Paid Leave" />
                                    <asp:CheckBox ID="chkProportionatePreviousMonth" runat="server" ToolTip="If previous month attendance is changed affecting workdays, then previous month amount will be adjusted in current month."
                                        Text="Proportionate Previous Month" />
                                </fieldset>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="pnlPercentOfSales" runat="server">
                    <table cellpadding="0" cellspacing="0" width="500px">
                        <tr>
                            <td width="160px">
                                Rate %
                            </td>
                            <td width="320px">
                                <asp:TextBox ID="txtRatePercent1" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtRatePercent1"
                                    Display="None" ErrorMessage="Rate percent is required." ValidationGroup="AEIncome"></asp:RequiredFieldValidator>
                                <asp:RangeValidator runat="server" ErrorMessage="Invalid rate percent." ValidationGroup="AEIncome"
                                    Type="Double" ID="CompareValidator141" Display="None" MinimumValue="1" MaximumValue="100"
                                    ControlToValidate="txtRatePercent1"></asp:RangeValidator>
                            </td>
                        </tr>
                        <tr>
                            <td width="160px">
                                Amount
                            </td>
                            <td width="320px">
                                <asp:TextBox ID="txtPOSAmount" runat="server" />
                                <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtPOSAmount"
                                Display="None" ErrorMessage="Rate percent is required." ValidationGroup="AEIncome" />--%>
                                <asp:CompareValidator Operator="DataTypeCheck" ValueToCompare="0" ValidationGroup="AEIncome"
                                    Type="Currency" ID="valCompPOSAmount" Display="None" ControlToValidate="txtPOSAmount"
                                    runat="server" ErrorMessage="Invalid percentage of sales amount."></asp:CompareValidator>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <%--hourly daily rate--%>
                <asp:Panel ID="pnlHourlyDailyRate" runat="server">
                    <%--<table>
                    <tr>
                        <td class="lf TDWidth">
                            Rate
                        </td>
                        <td>
                            <asp:TextBox ID="txtRate" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtRate"
                                Display="None" ErrorMessage="Rate is required." ValidationGroup="AEIncome"></asp:RequiredFieldValidator>
                            <asp:CompareValidator Operator="GreaterThanEqual" ValueToCompare="1" ValidationGroup="AEIncome"
                                Type="Double" ID="CompareValidator1" Display="None" ControlToValidate="txtRate"
                                runat="server" ErrorMessage="Invalid rate."></asp:CompareValidator>
                        </td>
                </table>--%></asp:Panel>
                <asp:Panel ID="pnlConcessionalFacility" runat="server">
                    <%--Deemed income section--%>
                    <table cellpadding="0" cellspacing="0" width="500px" style="margin-top: 10px">
                        <tr>
                            <td width="140px" valign="top">
                                Deemed Option
                            </td>
                            <td width="340px">
                                <asp:DropDownList Width="182px" ID="ddlDeemedCalculationOption" runat="server" onchange="changeDeemedDisplay(this)">
                                    <asp:ListItem Text="Monthly Calculation" Value="false"></asp:ListItem>
                                    <asp:ListItem Text="Variable Calculation" Value="true"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 10px" colspan="2">
                            </td>
                        </tr>
                        <tr>
                            <td width="140px" valign="top">
                                <asp:RadioButton GroupName="Deemed" Checked="true" onclick="onDeemedChanged(this)"
                                    ID="rdbDeemedAmount" Text="Amount" runat="server" />
                            </td>
                            <td width="340px">
                                <table cellpadding="0" cellspacing="0" runat="server" id="tableDeemedAmount">
                                    <tr>
                                        <td style='width: 100px'>
                                            Deemed Amount
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtDeemedAmount" Text="0" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="valReqd1" runat="server" ControlToValidate="txtDeemedAmount"
                                                Display="None" ErrorMessage="Deemed amount is required." ValidationGroup="AEIncome"></asp:RequiredFieldValidator>
                                            <asp:CompareValidator Operator="DataTypeCheck" ValueToCompare="0" ValidationGroup="AEIncome"
                                                Type="Currency" ID="valComp1" Display="None" ControlToValidate="txtDeemedAmount"
                                                runat="server" ErrorMessage="Invalid deemed amount value."></asp:CompareValidator>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr runat="server" id="rowDeemedPercent">
                            <td width="140px" valign="top">
                                <asp:RadioButton GroupName="Deemed" onclick="onDeemedChanged(this)" ID="rdbDeemedPercent"
                                    Text="Percent of Incomes" runat="server" />
                            </td>
                            <td width="340px">
                                <table cellpadding="0" cellspacing="0" id="tableDeemedPercent" runat="server">
                                    <tr>
                                        <td style='width: 100px'>
                                            Rate %
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtDeemedRate" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="reqdValDeemedRate" runat="server" ControlToValidate="txtDeemedRate"
                                                Display="None" ErrorMessage="Rate percent is required." ValidationGroup="AEIncome"></asp:RequiredFieldValidator>
                                            <asp:RangeValidator runat="server" ErrorMessage="Invalid rate percent." ValidationGroup="AEIncome"
                                                Type="Double" ID="rangeValDeemedRate" Display="None" MinimumValue="0" MaximumValue="100"
                                                ControlToValidate="txtDeemedRate"></asp:RangeValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            <div style="overflow-y: scroll; height: 120px; width: 200px; border: 1px solid #C1D6F3;
                                                padding: 0px">
                                                <asp:CheckBoxList ID="chkDeemedIncomeList" runat="server" DataTextField="Title" DataValueField="IncomeId">
                                                </asp:CheckBoxList>
                                                <cc2:RequiredForCheckBoxLists type="checkboxlist" ID="reqdValDeemedIncomeList" Display="None"
                                                    ControlToValidate="chkDeemedIncomeList" ErrorMessage="Income(s) must be selected."
                                                    ValidationGroup="AEIncome" runat="server">
                            &nbsp;
                                                </cc2:RequiredForCheckBoxLists>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr style="display: none">
                            <td width="140px">
                                <asp:CheckBox ID="chkDeemedIsYearIncome" runat="server" Text="Yearly Income" />
                            </td>
                            <td width="340px">
                                <asp:DropDownList DataTextField="Value" Width="180px" DataValueField="Key" AppendDataBoundItems="true"
                                    ID="ddlDeemedMonth" runat="server">
                                    <asp:ListItem Text="--Select Month--" Value="-1"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator InitialValue="-1" ID="reqdValueDeemedMonth" runat="server"
                                    ControlToValidate="ddlDeemedMonth" Display="None" ErrorMessage="Please select a month."
                                    ValidationGroup="AEIncome"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="pnlPercentOfIncome" runat="server">
                    <table cellpadding="0" cellspacing="0" width="500px">
                        <tr runat="server" id="rowRateIncomePercent">
                            <td width="140px">
                                Rate %
                            </td>
                            <td width="340px">
                                <asp:TextBox ID="txtRatePercent2" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtRatePercent2"
                                    Display="None" ErrorMessage="Rate percent is required." ValidationGroup="AEIncome"></asp:RequiredFieldValidator>
                                <asp:RangeValidator runat="server" ErrorMessage="Invalid rate percent." ValidationGroup="AEIncome"
                                    Type="Double" ID="CompareValidator2" Display="None" MinimumValue="0" MaximumValue="300"
                                    ControlToValidate="txtRatePercent2"></asp:RangeValidator>
                            </td>
                        </tr>
                        <tr runat="server" id="Tr1">
                            <td colspan="2">
                                <table>
                                    <tr>
                                        <td style='width:140px'>
                                            Minimum Amount
                                        </td>
                                        <td >
                                            <asp:TextBox ID="txtMinAmount" Width="100px" runat="server"></asp:TextBox>
                                            <asp:CompareValidator runat="server" Operator="DataTypeCheck" ErrorMessage="Invalid minimum amount."
                                                ValidationGroup="AEIncome" Type="Currency" ID="CompareValidator7" Display="None"
                                                ControlToValidate="txtMinAmount"></asp:CompareValidator>
                                        </td>
                                        <td>
                                            Maximum
                                        </td>
                                        <td>
                                             <asp:TextBox ID="txtMaxAmount"  Width="100px" runat="server"></asp:TextBox>
                                            <asp:CompareValidator runat="server" Operator="GreaterThanEqual"  ControlToCompare="txtMinAmount" ErrorMessage="Invalid maximum amount,should be greater than minimum value."
                                                ValidationGroup="AEIncome" Type="Currency" ID="CompareValidator8" Display="None"
                                                ControlToValidate="txtMaxAmount"></asp:CompareValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style='width:140px'>
                                           
                                        </td>
                                        <td colspan="3" >
                                           
                                               <asp:CheckBox runat="server" Visible="false" ID="chkSkipMinMaxChecking" Text="Skip Min/Max for this Employee" />
                                        </td>
                                    </tr>
                                </table>

                                
                            </td>
                           

                        </tr>
                        <tr>
                            <td width="140px">
                                Income
                            </td>
                            <td width="340px">
                                <div style="overflow-y: scroll; height: 120px; width: 200px; border: 1px solid #C1D6F3;
                                    padding: 0px">
                                    <asp:CheckBoxList ID="chkListIncomes1" runat="server" DataTextField="Title" DataValueField="IncomeId">
                                    </asp:CheckBoxList>
                                    <cc2:RequiredForCheckBoxLists type="checkboxlist" ID="reqdCheckboxlist1" Display="None"
                                        ControlToValidate="chkListIncomes1" ErrorMessage="Income(s) must be selected."
                                        ValidationGroup="AEIncome" runat="server">
                            &nbsp;
                                    </cc2:RequiredForCheckBoxLists>
                                </div>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="pnlFestivalAllowance" runat="server">
                    <table cellpadding="0" cellspacing="0" width="500px">
                        <tr>
                            <td width="155px">
                                <asp:RadioButton GroupName="Festival" onclick="onFetivalChanged(this)" ID="rdbVariableAmount"
                                    Text="Variable Amount" runat="server" />
                            </td>
                            <td runat="server" width="325px" id="colFestival">
                                <asp:TextBox ID="txtVariableAmount" Width="151px" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtVariableAmount"
                                    Display="None" ErrorMessage="Variable amount is required." ValidationGroup="AEIncome"></asp:RequiredFieldValidator>
                                <asp:CompareValidator runat="server" Operator="DataTypeCheck" ErrorMessage="Invalid variable amount."
                                    ValueToCompare="1" ValidationGroup="AEIncome" Type="Currency" ID="CompareValidator3"
                                    Display="None" ControlToValidate="txtVariableAmount"></asp:CompareValidator>
                            </td>
                        </tr>
                        <tr>
                            <td width="115x" valign="top">
                                <asp:RadioButton GroupName="Festival" onclick="onFetivalChanged(this)" ID="rdbCalculatedAmount"
                                    Text="Calculated Amount" runat="server" />
                            </td>
                            <td width="365px">
                                <div id="divFestival" runat="server">
                                    <asp:TextBox ID="txtPercent" Style='margin-bottom: 5px' Width="47px" runat="server"></asp:TextBox>
                                    <asp:Label ID="lblFestival" runat="server" Text=" % of" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="txtPercent"
                                        Display="None" ErrorMessage="Percent is required." ValidationGroup="AEIncome"></asp:RequiredFieldValidator>
                                    <asp:RangeValidator runat="server" ErrorMessage="Invalid percent." ValidationGroup="AEIncome"
                                        Type="Double" ID="CompareValidator4" Display="None" MinimumValue="0" MaximumValue="200"
                                        ControlToValidate="txtPercent"></asp:RangeValidator>
                                    <%--<asp:CompareValidator runat="server" Operator="GreaterThanEqual" ErrorMessage="Invalid percent."
                                ValueToCompare="1" ValidationGroup="AEIncome" Type="Integer" ID="CompareValidator4"
                                Display="None" ControlToValidate="txtPercent"></asp:CompareValidator>--%>
                                    <div style="overflow-y: scroll; height: 140px; width: 258px; border: 1px solid #C1D6F3;
                                        padding: 0px">
                                        <asp:CheckBoxList ID="chkListIncomes2" runat="server" DataTextField="Title" DataValueField="IncomeId">
                                        </asp:CheckBoxList>
                                        <cc2:RequiredForCheckBoxLists ID="reqdchkListIncomes2" Display="None" ControlToValidate="chkListIncomes2"
                                            ErrorMessage="Income(s) must be selected." ValidationGroup="AEIncome" runat="server">
                            &nbsp;
                                        </cc2:RequiredForCheckBoxLists>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <table runat="server" id="tblApplyOnce" cellpadding="0" cellspacing="0" width="500px">
                    <tr>
                        <td style='vertical-align: top' width="140px">
                            <asp:Image ID="imageApplyMonth" runat="server" onclick="showHideApplyMonth()" ImageAlign="Left"
                                ImageUrl="~/images/expand.jpg" Style='padding-right: 4px; cursor: pointer' />
                            <h4 style='cursor: pointer; display: inline; color: #2F7CD3' onclick="showHideApplyMonth()">
                                Apply month of</h4>
                        </td>
                        <td>
                            <table runat="server" id="tblStatuesContainer" width="340px" style='display: none1'>
                                <tr>
                                    <td colspan="2">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:DropDownList DataTextField="Value" Width="180px" DataValueField="Key" AppendDataBoundItems="true"
                                            ID="ddlApplyOnceFor" runat="server">
                                            <asp:ListItem Text="--Select Month--" Value="-1"></asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator callback="applyOnceIsValidationReqd" InitialValue="-1"
                                            ID="valReqdApplyOnce" runat="server" ControlToValidate="ddlApplyOnceFor" Display="None"
                                            ErrorMessage="Please select a month." ValidationGroup="AEIncome"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Salary Status
                                    </td>
                                    <td>
                                        <asp:DropDownList AppendDataBoundItems="true" Width="153px" ID="ddlFestivalSalaryStatus"
                                            runat="server" DataValueField="Key" DataTextField="Value">
                                            <asp:ListItem Text="Show In Salary" Value="false"></asp:ListItem>
                                            <asp:ListItem Text="Do Not Show In Salary" Value="true"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Employee status
                                    </td>
                                    <td>
                                        <asp:DropDownList AppendDataBoundItems="true" Width="153px" ID="ddlEmployeeStatus"
                                            runat="server" DataValueField="Key" DataTextField="Value">
                                            <asp:ListItem Text="---Select---" Value="-1"></asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator callback="applyOnceIsValidationReqd" ID="valStatus" runat="server"
                                            ControlToValidate="ddlEmployeeStatus" InitialValue="-1" Display="None" ErrorMessage="Please select status."
                                            ValidationGroup="AEIncome"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr runat="server" id="trApplyFestivalIncome">
                                    <td width="120px" colspan="2">
                                        <asp:CheckBox ID="chkApplyFestivalIncome" runat="server" Text="Apply income to this Employee" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="120px">
                                        <asp:CheckBox ID="chkEligibilityDate" runat="server" Text="Eligibility Date" />
                                    </td>
                                    <td width="220x">
                                        <My:Calendar Id="calEligibilityDate" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:CheckBox ID="chkDeductUPLAbsentFestival" runat="server" Text="Deduct UPL / LWP in workdays" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:CheckBox ID="chkDeductAbsentFestival" runat="server" Text="Deduct Absent in workdays" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:CheckBox ID="chkDeductStopPayment" runat="server" Text="Deduct Stop payment in workdays" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:CheckBox ID="chkExcludeRetBeforeEligiblityDate" runat="server" Text="Exclude Retiring Before Eligibility date" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="120px">
                                        Minimum work days for full amount
                                    </td>
                                    <td width="180px">
                                        <asp:TextBox ID="txtMinimumWorkDays" Width="151px" runat="server" />
                                        <asp:RequiredFieldValidator callback="applyOnceIsValidationReqd" ID="valReqdMimDays"
                                            runat="server" ControlToValidate="txtMinimumWorkDays" Display="None" ErrorMessage="Minimum work days is required."
                                            ValidationGroup="AEIncome"></asp:RequiredFieldValidator>
                                        <asp:CompareValidator runat="server" Operator="GreaterThanEqual" ErrorMessage="Invalid days."
                                            ValueToCompare="0" callback="applyOnceIsValidationReqd" ValidationGroup="AEIncome"
                                            Type="Double" ID="valCompMinDays" Display="None" ControlToValidate="txtMinimumWorkDays"></asp:CompareValidator>
                                        <%--  <asp:CustomValidator callback="applyOnceIsValidationReqd" runat="server" ClientValidationFunction="ValidateMinimumDays"
                                            ValidationGroup="AEIncome" ID="valCompMinGreaterProport" ErrorMessage="Minimum days should be greater than proportionate days."
                                            Display="None" ControlToValidate="txtMinimumWorkDays"></asp:CustomValidator>--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="120px">
                                        Minimum work days for proportionate amount
                                    </td>
                                    <td width="180px">
                                        <asp:TextBox ID="txtProportionateWorkDays" Width="151px" runat="server" />
                                        <asp:RequiredFieldValidator callback="applyOnceIsValidationReqd" ID="valReqdProDays"
                                            runat="server" ControlToValidate="txtProportionateWorkDays" Display="None" ErrorMessage="Proportionate work days is required."
                                            ValidationGroup="AEIncome"></asp:RequiredFieldValidator>
                                        <asp:CompareValidator runat="server" Operator="GreaterThanEqual" ErrorMessage="Invalid days."
                                            ValueToCompare="0" callback="applyOnceIsValidationReqd" ValidationGroup="AEIncome"
                                            Type="Integer" ID="valCompProDays" Display="None" ControlToValidate="txtProportionateWorkDays"></asp:CompareValidator>
                                        <%--Proportionate can be 0 so in that case no amount is allowed--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="120px">
                                        Proportionate calculation based on
                                    </td>
                                    <td width="180px">
                                        <asp:TextBox ID="txtProportionateWorkDaysBasedOn" Width="151px" runat="server" />
                                        <%-- <asp:RequiredFieldValidator callback="applyOnceIsValidationReqd" ID="RequiredFieldValidator5"
                                            runat="server" ControlToValidate="txtProportionateWorkDays" Display="None" ErrorMessage="Proportionate work days is required."
                                            ValidationGroup="AEIncome"></asp:RequiredFieldValidator>--%>
                                        <asp:CompareValidator runat="server" Operator="GreaterThanEqual" ErrorMessage="Invalid days."
                                            ValueToCompare="0" callback="applyOnceIsValidationReqd" ValidationGroup="AEIncome"
                                            Type="Double" ID="CompareValidator5" Display="None" ControlToValidate="txtProportionateWorkDaysBasedOn"></asp:CompareValidator>
                                        <%--Proportionate can be 0 so in that case no amount is allowed--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="120px">
                                        Count workdays from
                                    </td>
                                    <td width="180px">
                                        <asp:DropDownList onchange="onChangeWorkdaysFrom(this)" AppendDataBoundItems="true"
                                            Width="153px" ID="ddlCountWorkdaysFrom" runat="server" DataValueField="Key" DataTextField="Value">
                                            <asp:ListItem Text="---Select---" Value="-1"></asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator callback="applyOnceIsValidationReqd" InitialValue="-1"
                                            ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlCountWorkdaysFrom"
                                            Display="None" ErrorMessage="Count workdays from is required." ValidationGroup="AEIncome"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="120px">
                                    </td>
                                    <td width="185px">
                                        <My:Calendar Id="calWorkdaysFromDate" runat="server" />
                                        <asp:TextBox ID="txtVal" runat="server" Style='position: absolute; left: -1000px;
                                            top: -1000px' />
                                        <asp:CustomValidator validateOnHiddenAlso="true" ID="val123" Text="" Display="None"
                                            ErrorMessage="Workdays date should be less than eligibility date." ControlToValidate="txtVal"
                                            ClientValidationFunction="ValidateWorkdaysFromDate" runat="server" ValidateEmptyText="true"
                                            ValidationGroup="AEIncome" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <div runat="server" style="color: #F49D25" id="Div1">
                                            Total employees : {0}, Income applied employees : {1}
                                            Apply Income to all Employees before proceeding</div>
                                        <asp:CheckBox ID="chkAddTaxFromTheBeginning" runat="server" Text="Add this income for income tax from the beginning" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                       
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                 <asp:LinkButton Style='text-decoration: underline; color: blue;    margin-left: 152px;' ID="btnCheckEligibility"
                                            runat="server" Text="Check Eligibility" OnClick="btnCheckEligibility_Click" />  
            </div>
        </fieldset>
        <fieldset class="large-heading" runat="server" id="fieldSetDefinedType">
            <asp:Image ID="imageOptions1" runat="server" onclick="showHideDefinedType()" ImageAlign="Left"
                ImageUrl="~/images/expand.jpg" Style='padding-right: 4px; padding-top: 2px; cursor: pointer' />
            <h2 style='cursor: pointer; color: #2F7CD3' runat="server" id="h2" onclick="showHideDefinedType()">
                Defined Type</h2>
            <asp:Panel runat="server" class="bevel paddinAll" ID="pnlOptions1" Style="width: 500px;
                display: none">
                <table>
                    <tr>
                        <td style="width: 140px;">
                            Defined Type
                        </td>
                        <td>
                            <asp:DropDownList Width="100px" ID="ddlDefinedType" onchange="definedTypeChange(this)"
                                runat="server">
                                <asp:ListItem Text="No" Value="false" />
                                <asp:ListItem Text="Yes" Value="true" />
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 140px;">
                            Column
                        </td>
                        <td>
                            <asp:DropDownList Width="100px" ID="ddlXType" runat="server">
                                <asp:ListItem Text="" Value="0" />
                                <asp:ListItem Text="Branch" Value="1" />
                                <asp:ListItem Text="Designation" Value="3" />
                                <asp:ListItem Text="Level" Value="2" />
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 140px;">
                            Row
                        </td>
                        <td>
                            <asp:DropDownList Width="100px" ID="ddlYType" runat="server">
                                <asp:ListItem Text="" Value="0" />
                                <asp:ListItem Text="Branch" Value="1" />
                                <asp:ListItem Text="Department" Value="4" />
                                <asp:ListItem Text="Level" Value="2" />
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
                <span style="color: #FF0063">Note : After the change in Defined type, please reimport
                    the matrix values from "Other Defined Salary". </span>
            </asp:Panel>
        </fieldset>
        <div style="margin: 0px 50px 0 8px; text-align: right">
            <asp:Button ID="btnOk" runat="server" CssClass="save" OnClientClick="valGroup='AEIncome';if(CheckValidation())  return ValidateIncrement()"
                OnClick="btnOk_Click" Text="Ok" ValidationGroup="AEIncome" />
            <asp:Button ID="btnCancel" runat="server" CssClass="cancel" Text="Cancel" OnClientClick="window.close()" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="pageScriptsContent" runat="server">
    <script type="text/javascript">
        function closePopup() {
            window.returnValue = "Reload";
            window.close();
        }

        function closePopupFromPay() {

            // alert(window.opener.parentReloadCallbackFunction)
            //if ($.browser.msie == false && typeof (window.opener.parentIncomeCallbackFunction) != 'undefined' && ($.browser.chrome || $.browser.safari)) {
            window.opener.parentIncomeCallbackFunction("ReloadIncome", window, document.getElementById('<%= hiddenEmployeeId.ClientID %>').value);
            //            } else {
            //                window.returnValue = "ReloadIncome";

            //                window.close();
            //            }
        }

        function onChangeWorkdaysFrom(ddl) {

            var calWorkdaysFromDate = '#<%= calWorkdaysFromDate.ClientID %>';

            if (ddl.value == 'SpecificDate') {
                enableElement(calWorkdaysFromDate + '_date');
            }
            else
                disableElement(calWorkdaysFromDate + '_date');
        }

        function modeDateChanged(chk) {



            var lblMode = '<%= lblMode.ClientID %>';
            if (chk.checked) {
                $('#' + lblMode).html("Increment Mode");
            }
            else {
                $('#' + lblMode).html("Edit Mode");
            }
            changeIncrementState(chk);

        }



        function ValidateMinimumDays(source, args) {

            var minimumDays = parseInt(args.Value);
            var porportionateDays = parseInt($('#<%= txtProportionateWorkDays.ClientID %>').val());

            if (minimumDays == 0)
            { }

            else if (porportionateDays >= minimumDays) {
                //alert("Minimum days should be greater than proportionate days.");
                args.IsValid = false;
                return;
            }

            args.IsValid = true;
        }



        var rdbVariableAmount = '<%= rdbVariableAmount.ClientID %>';

        function onFetivalChanged(rdb) {
            var isVariableAmountChecked = document.getElementById(rdbVariableAmount).checked;
            if (isVariableAmountChecked) {
                enableElement('<%= txtVariableAmount.ClientID %>');
                disableElement('<%= divFestival.ClientID %>');
            }
            else {
                disableElement('<%= txtVariableAmount.ClientID %>');
                enableElement('<%= divFestival.ClientID %>');
            }
        }

        var rdbDeemedAmount = '<%= rdbDeemedAmount.ClientID %>';
        var tableDeemedAmount = '#<%= tableDeemedAmount.ClientID %>';
        var tableDeemedPercent = '#<%= tableDeemedPercent.ClientID %>';
        function onDeemedChanged(rdb) {
            var isVariableAmountChecked = document.getElementById(rdbDeemedAmount).checked;
            if (isVariableAmountChecked) {
                enableElement('<%= tableDeemedAmount.ClientID %>');
                disableElement('<%= tableDeemedPercent.ClientID %>');
                disableElement(txtDeemedIncome);
            }
            else {
                disableElement('<%= tableDeemedAmount.ClientID %>');
                enableElement('<%= tableDeemedPercent.ClientID %>');
                disableElement(txtDeemedIncome);
            }
        }

        var rowIncrement = '#<%= rowIncrement.ClientID %>';
        var rowPF = '#<%= rowPF.ClientID %>';
        var rowIsGrade = '#<%= rowIsGrade.ClientID %>';
        var tblApplyOnce = '<%= tblApplyOnce.ClientID %>';
        var pnlFixedVariable = '#<%= pnlFixedVariable.ClientID %>';
        var pnlPercentOfSales = '#<%= pnlPercentOfSales.ClientID %>';
        var pnlHourlyDailyRate = '#<%= pnlHourlyDailyRate.ClientID %>';
        var pnlConcessionalFacility = '#<%= pnlConcessionalFacility.ClientID %>';
        var pnlPercentOfIncome = '#<%= pnlPercentOfIncome.ClientID %>';
        var pnlFestivalAllowance = '#<%= pnlFestivalAllowance.ClientID %>';
        var rowRateIncomePercent = '#<%= rowRateIncomePercent.ClientID %>';
        var pnlUnitRate = '#<%= pnlUnitRate.ClientID %>';

        var rowDeemedPercent = '#<%=rowDeemedPercent.ClientID %>';
        var chkBasicIncome = '#<%=chkBasicIncome.ClientID %>';
        var chkIncludeForLeaveEncashment = '#<%= chkCountForLeaveEncasement.ClientID %>';
        var chkIsTaxable = '#<%= chkIsTaxable.ClientID %>';
        var chkUpdateWithLeave = '#<%= chkUpdateWithLeave.ClientID %>';
        var chkIsForeignAllowance = '#<%= chkIsForeignAllowance.ClientID %>';
        var rowchkIsTreatLikeOneTimeIncome = '#<%= rowchkIsTreatLikeOneTimeIncome.ClientID %>';
        var fieldSetDefinedType = '#<%=fieldSetDefinedType.ClientID %>';
        var rowUnitRateWorkdays = '#<%= rowUnitRateWorkdays.ClientID %>'
        var rowLateType = '#<%= rowLateType.ClientID %>';
        var rowStatusFilter1 = '#<%=rowStatusFilter1.ClientID %>'
        // var chkCountForLeaveEncasement = '#<%= chkCountForLeaveEncasement.ClientID %>';
        function changePaymentType(ddl) {
            if (ddl.value == 'true')
                $(divPaymentMonthList).show();
            else
                $(divPaymentMonthList).hide();
        }

        function changeDeemedDisplay(ddl) {
            var value = ddl.value;

            if (value == "false") {
                $(rowDeemedPercent).show();
            }
            else {
                $('#' + rdbDeemedAmount).attr('checked', true);

                $(rowDeemedPercent).hide();
            }
        }

        function changeCalculationDisplay(ddl) {
            var value = ddl.value;

            $(rowStatusFilter1).hide();
            $(pnlUnitRate).hide();
            $(rowPF).hide();
            $(rowIsGrade).hide();
            $(rowIncrement).show();
            disableElement(tblApplyOnce);
            $(fieldSetDefinedType).hide();
            $(rowUnitRateWorkdays).hide();
            $(rowLateType).hide();

            if (value == "DeemedIncome") {
                disableElement(chkIncludeForLeaveEncashment);
                disableElement(chkIsTaxable);
                disableElement(chkIsForeignAllowance);
                $(chkUpdateWithLeave).attr('checked', true);
            }
            else {
                //remove disabled from parent node for IE
                $(chkIncludeForLeaveEncashment).parent().removeAttr('disabled');
                $(chkIsTaxable).parent().removeAttr('disabled');
                $(chkIsForeignAllowance).parent().removeAttr('disabled');
                enableElement(chkIncludeForLeaveEncashment);
                enableElement(chkIsTaxable);
                enableElement(chkIsForeignAllowance);
                $(chkUpdateWithLeave).attr('checked', false);
            }

            if (value == "VariableAmount" || value == 'UnitRate')
                $(rowchkIsTreatLikeOneTimeIncome).show();
            else
                $(rowchkIsTreatLikeOneTimeIncome).hide();

            // enableElement(chkCountForLeaveEncasement);
            if (value == "VariableAmount" || value == "FixedAmount") {
                $(pnlFixedVariable).show();
                $(pnlPercentOfSales).hide();
                $(pnlHourlyDailyRate).hide();
                $(pnlConcessionalFacility).hide();
                $(pnlPercentOfIncome).hide();
                $(pnlFestivalAllowance).hide();
                $(chkUpdateWithLeave).attr('checked', true);

                if (value == "FixedAmount") {

                    $(fieldSetDefinedType).show();
                    $(chkUpdateWithLeave).attr('checked', false);
                    $(rowPF).show();
                    $(rowIsGrade).show();

                    if ($(rowIsGrade).is(':checked'))
                        $(rowIncrement).hide();

                    if ($(rowIsGrade).is(':checked') == false && $(chkBasicIncome).is(':checked') == false) {

                    }
                }
                else
                    $(rowLateType).show();
            }
            else if (value == "UnitRate") {
                $(rowPF).show();
                $(chkUpdateWithLeave).attr('checked', true);
                $(pnlUnitRate).show();
                $(pnlFixedVariable).hide();
                $(pnlPercentOfSales).hide();
                $(pnlHourlyDailyRate).hide();
                $(pnlConcessionalFacility).hide();
                $(pnlPercentOfIncome).hide();
                $(pnlFestivalAllowance).hide();
                $(rowIncrement).hide();
                $(rowUnitRateWorkdays).show();
            }
            else if (value == "PercentageOfSales") {
                $(chkUpdateWithLeave).attr('checked', true);
                $(pnlFixedVariable).hide();
                $(pnlPercentOfSales).show();
                $(pnlHourlyDailyRate).hide();
                $(pnlConcessionalFacility).hide();
                $(pnlPercentOfIncome).hide();
                $(pnlFestivalAllowance).hide();
                $(rowIncrement).hide();
            }
            //            else if (value == "DailyRate" || value == "HourlyRate") {
            //                $(pnlFixedVariable).hide();
            //                $(pnlPercentOfSales).hide();
            //                $(pnlHourlyDailyRate).show();
            //                $(pnlConcessionalFacility).hide();
            //                $(pnlPercentOfIncome).hide();
            //                $(pnlFestivalAllowance).hide();
            //            }
            else if (value == "DeemedIncome") {
                $(pnlFixedVariable).hide();
                $(pnlPercentOfSales).hide();
                $(pnlHourlyDailyRate).hide();
                $(pnlConcessionalFacility).show();
                $(pnlPercentOfIncome).hide();
                $(pnlFestivalAllowance).hide();

                $(rowIncrement).hide();
            }
            else if (value == "PercentOfIncome") {
                $(rowPF).show();
                $(rowStatusFilter1).show();
                $(fieldSetDefinedType).show();
                $(chkUpdateWithLeave).attr('checked', true);
                $(pnlFixedVariable).hide();
                $(pnlPercentOfSales).hide();
                $(pnlHourlyDailyRate).hide();
                $(pnlConcessionalFacility).hide();
                $(pnlPercentOfIncome).show();
                $(pnlFestivalAllowance).hide();

                //don't show rate in add mode
                //                if (isAddMode) {
                //                    $(rowRateIncomePercent).hide();                    
                //                }
                //                else
                //                    $(rowRateIncomePercent).hide();
            }
            else if (value == "FestivalAllowance") {
                $(fieldSetDefinedType).show();
                $(chkUpdateWithLeave).attr('checked', true);
                $(pnlFixedVariable).hide();
                $(pnlPercentOfSales).hide();
                $(pnlHourlyDailyRate).hide();
                $(pnlConcessionalFacility).hide();
                $(pnlPercentOfIncome).hide();
                $(pnlFestivalAllowance).show();
                enableElement(tblApplyOnce);
                $(rowIncrement).hide();
                $(rowPF).show()
                //disable reqd radiobutton related items
                onFetivalChanged();


                //disableElement(chkCountForLeaveEncasement);
            }
            else {
                $(pnlFixedVariable).hide();
                $(pnlPercentOfSales).hide();
                $(pnlHourlyDailyRate).hide();
                $(pnlConcessionalFacility).hide();
                $(pnlPercentOfIncome).hide();
                $(pnlFestivalAllowance).hide();
            }


            //check if Apply once checked, then only enable or else disable so as validation works
            if (value == "FestivalAllowance") {
                //enableElement('<%= tblStatuesContainer.ClientID %>');
                $('#<%= tblApplyOnce.ClientID %>').show();
                $('#<%= tblStatuesContainer.ClientID %>').show();
                applyOnceStateChanged(document.getElementById('<%= chkEligibilityDate.ClientID %>'));

            }
            else {
                //disableElement('<%= tblStatuesContainer.ClientID %>');
                $('#<%= tblApplyOnce.ClientID %>').hide();
                $('#<%= tblStatuesContainer.ClientID %>').show();
            }
        }

        function incrementHistoryCall(id) {
            var ret = incrementHistory("id=" + id);
            if (typeof (ret) != 'undefined') {

            }
        }

        function definedTypeChange(ddl) {

            var ddlXType = '#<%=ddlXType.ClientID %>';
            var ddlYType = '#<%=ddlYType.ClientID %>';


            if (ddl.value == "true") {
                enableElement(ddlXType);
                enableElement(ddlYType);

            }
            else {
                disableElement(ddlXType);
                disableElement(ddlYType);
            }

        }

        function changeIncrementState(chk) {
            //then enable the inputs to enter increment as disabled due to salary generation
            var txtAmount = '#<%= txtAmount.ClientID %>'
            var txtRatePercent1 = '#<%= txtRatePercent1.ClientID %>'


            var txtRatePercent2 = '#<%= txtRatePercent2.ClientID %>'
            var txtVariableAmount = '#<%= txtVariableAmount.ClientID %>'
            var txtPercent = '#<%= txtPercent.ClientID %>'



            if (chk.checked && isIncluded) {
                removeReadonly(txtAmount);
                removeReadonly(txtRatePercent1);


                removeReadonly(txtRatePercent2);

                removeReadonly(txtAmount);

                if (document.getElementById(rdbVariableAmount).checked)
                    removeReadonly(txtVariableAmount);
                else
                    removeReadonly(txtPercent);

            }
            else if (!chk.checked && isIncluded) {


                makeReadonly(txtAmount);
                makeReadonly(txtRatePercent1);


                makeReadonly(txtRatePercent2);

                makeReadonly(txtAmount);

                if (document.getElementById(rdbVariableAmount).checked)
                    makeReadonly(txtVariableAmount);
                else
                    makeReadonly(txtPercent);

            }
        }


        function ValidateIncrement() {

            //don't need this code as not we have allowed to set increment date crossing last increment also
            return true;

            //            var incrementCheckbox = '<%= chkIsIncrementMode.ClientID %>';

            //            if (document.getElementById(incrementCheckbox) == null)
            //                return true; //First time so no increment so return true

            //            if (!document.getElementById(incrementCheckbox).checked)
            //                return true;

            //            var incrementStartingDate = getCalendarSelectedDate('<%= calIncrementDate.ClientID %>');

            //            if (isDateSame(startingDate, incrementStartingDate) ||
            //                isSecondCalendarCtlDateGreater(startingDate, incrementStartingDate))
            //                return true;


            //            //alert("Increment date should be greater than or equal to current payroll period " + startingDate + ".");
            //            //return false;


            //            //logic for reterospect
            //            if (confirm("<%= Resources.Messages.IncomeRetrospectIncrementSettingWarningMsg %>")) {

            //                if (typeof (lastIncludedIncrementDate) != 'undefined') {
            //                    //current increment date should be greater than the last increment
            //                    if (!isDateSame(lastIncludedIncrementDate, incrementStartingDate)
            //                        && isSecondCalendarCtlDateGreater(lastIncludedIncrementDate, incrementStartingDate) == false) {
            //                        alert(String.format('<%= Resources.Messages.IncomeRetrospectCurrentDateGreaterThanLastIncMsg %>', lastIncludedIncrementDate));
            //                        //alert("Current increment date should be greater than the last increment date " + lastIncludedIncrementDate + ".");
            //                        return false;
            //                    }
            //                }

            //                return true;
            //            }
            //            else {
            //                alert("<%=  Resources.Messages.IncomeRetropsectIncrementNotWantedMsg %>");
            //                return false;
            //            }


        }

        function applyOnceStateChanged(chk) {
            var chkEligibilityDate = document.getElementById('<%= chkEligibilityDate.ClientID %>');


            if (chk.checked) {
                eligibilityStateChanged(chkEligibilityDate);

            }
            else {
                chkEligibilityDate.checked = false;
                eligibilityStateChanged(chkEligibilityDate);
            }
        }

        function eligibilityStateChanged(chk) {
            var txtMinimumWorkDays = '<%= txtMinimumWorkDays.ClientID %>';
            var txtProportionateWorkDays = '<%= txtProportionateWorkDays.ClientID %>';
            var calEligibilityDate = '<%= calEligibilityDate.ClientID %>';
            var ddlCountWorkdaysFrom = '<%= ddlCountWorkdaysFrom.ClientID %>';
            var calWorkdaysFromDate = '<%= calWorkdaysFromDate.ClientID %>';
            var chkDeductUPLAbsentFestival = '<%=chkDeductUPLAbsentFestival.ClientID %>';

            if (chk.checked) {
                enableElement(txtMinimumWorkDays);
                enableElement(chkDeductUPLAbsentFestival);
                enableElement(txtProportionateWorkDays);
                enableElement(calEligibilityDate + '_date');
                enableElement(ddlCountWorkdaysFrom);
                enableElement(calWorkdaysFromDate + "_date");
            }
            else {
                disableElement(txtMinimumWorkDays);
                disableElement(chkDeductUPLAbsentFestival);
                disableElement(txtProportionateWorkDays);
                disableElement(calEligibilityDate + '_date');
                disableElement(ddlCountWorkdaysFrom);
                disableElement(calWorkdaysFromDate + "_date");
            }
        }

        function ValidateWorkdaysFromDate(source, args) {

            var value = document.getElementById('<%= ddlCountWorkdaysFrom.ClientID %>').value;
            if (value == 'SpecificDate') {

                var calEligibilityDate = getCalendarSelectedDate('<%= calEligibilityDate.ClientID %>');
                var calWorkdaysFromDate = getCalendarSelectedDate('<%= calWorkdaysFromDate.ClientID %>');

                if (isSecondCalendarCtlDateGreater(calWorkdaysFromDate, calEligibilityDate))
                    args.IsValid = true;
                else {
                    args.IsValid = false;
                }
            }
            else {
                args.IsValid = true;
            }
        }


        var imageOptions1 = null;
        var pnlOptions1 = null;
        function showHideDefinedType() {

            if (imageOptions1 == null) {
                imageOptions1 = document.getElementById('<%= imageOptions1.ClientID %>');
                pnlOptions1 = document.getElementById('<%= pnlOptions1.ClientID %>');
            }


            if (pnlOptions1.style.display == 'none') {
                pnlOptions1.style.display = 'block';
                imageOptions1.src = '../images/collapse.jpg';
            }
            else {
                pnlOptions1.style.display = 'none';
                imageOptions1.src = '../images/expand.jpg';
            }
        }

        var imageOptions = null;
        var pnlOptions = null;
        function showHideOptions() {

            if (imageOptions == null) {
                imageOptions = document.getElementById('<%= imageOptions.ClientID %>');
                pnlOptions = document.getElementById('<%= pnlOptions.ClientID %>');
            }


            if (pnlOptions.style.display == 'none') {
                pnlOptions.style.display = 'block';
                imageOptions.src = '../images/collapse.jpg';
            }
            else {
                pnlOptions.style.display = 'none';
                imageOptions.src = '../images/expand.jpg';
            }
        }

        var tblStatuesContainer = null;
        var imageApplyMonth = null;
        function showHideApplyMonth() {

            if (imageApplyMonth == null) {
                imageApplyMonth = document.getElementById('<%= imageApplyMonth.ClientID %>');
                tblStatuesContainer = document.getElementById('<%= tblStatuesContainer.ClientID %>');
            }


            if (tblStatuesContainer.style.display == 'none') {
                tblStatuesContainer.style.display = 'block';
                imageApplyMonth.src = '../images/collapse.jpg';
            }
            else {
                tblStatuesContainer.style.display = 'none';
                imageApplyMonth.src = '../images/expand.jpg';
            }
        }

        function applyOnceIsValidationReqd() {
            var tblApplyOnce = document.getElementById('<%= tblApplyOnce.ClientID %>');
            if (tblApplyOnce.style.display == 'block' || tblApplyOnce.style.display == "") {
                return true;
            }
            return false;
        }
       
    </script>
</asp:Content>
