﻿<%@ Page Title="Salary Calculation" EnableEventValidation="false" MaintainScrollPositionOnPostBack="true" Language="C#" MasterPageFile="~/Master/Details.Master" AutoEventWireup="true" CodeBehind="NewCalculation.aspx.cs" Inherits="Web.CP.NewCalculation" %>
<%@ Register src="../UserControls/NewCalculation.ascx" tagname="Calculation" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script src="../Scripts/currency.js" type="text/javascript"></script>

    <script src="../Scripts/ScrollTableHeader-v1.03.js" type="text/javascript"></script>
     <%--   <script src="../Scripts/ScrollTableHeader103.min.js" type="text/javascript"></script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <uc1:Calculation Id="Calculation1" runat="server" />
</asp:Content>
