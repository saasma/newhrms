﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Manager;
using BLL;
using DAL;

using Utils.Helper;
using BLL.Base;
using Utils.Calendar;
using Bll;
using Ext.Net;

namespace Web.CP
{
    public partial class OxfamOptimumPFAndCIT : BasePage
    {
        public static int companyId = -1;
     
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
                {
                    int empId = int.Parse(Request.QueryString["ID"]);
                    string name = EmployeeManager.GetEmployeeName(empId);
                    filterCtl.SetFilterEmployee(empId, name);
                }

                pagingCtl.CurrentPage = 1;
                BindEmployee();

                // Hide PF related columns if Optimum PF not reqd
                //if (CommonManager.CompanySetting.UseOptimumPF == false)
                //{
                //    gvwEmployeeList.Columns[9].Visible = false;

                //    gvwEmployeeList.Columns[10].Visible = false;
                //}
            }

            if (Request.Form["EVENTTARGET"] != null && Request.Form["EVENTTARGET"].Equals("Reload"))
                BindEmployee();
            if (Request.Form["__EventTarget"] != null &&
                (Request.Form["__EventTarget"].Equals(btnLoad.ClientID.Replace("_", "$"))
               
                )
                )
            {
                BindEmployee();
            }

            JavascriptHelper.AttachNonDialogPopUpCode(Page, "pfImportPopup", "../ExcelWindow/FixedPFExcel.aspx", 450, 500);

         
            JavascriptHelper.AttachNonDialogPopUpCode(Page, "importPopup", "../ExcelWindow/OxfamDesiredContributionExcel.aspx", 450, 500);
        }



        protected void btnLoadHead_Click(object sender, DirectEventArgs e)
        {
            int ein = int.Parse(hdnEmployeeId.Text);
            string name = EmployeeManager.GetEmployeeName(ein);
            lblEmployee.Text = name + " - " + ein;

            EEmployee emp = EmployeeManager.GetEmployeeById(ein);

            decimal? amount = emp.EFundDetails[0].OxfamDesiredContribution;

            dispMaxAllowed.Text = GetCurrency(hiddenMaxAllowed.Value.ToString());

            if (amount == null || amount == 0)
                txtNewAmount.Text = "";
            else
                txtNewAmount.Text = GetCurrency(amount);

            if (emp.EFundDetails[0].OxfamPFPercentage != null)
            {
                txtNewPFPercent.Text = emp.EFundDetails[0].OxfamPFPercentage.ToString();
            }
            else
                txtNewPFPercent.Text = "";
            
            window.Show();

        }


        public void ReBindEmployees()
        {
            BindEmployee();
        }

        protected void btnPrevious_Click()
        {
            pagingCtl.CurrentPage -= 1;          
            BindEmployee();
        }

        protected void btnNext_Click()
        {
            pagingCtl.CurrentPage += 1;         
            BindEmployee();
        }

        protected void ddlRecords_SelectedIndexChanged()
        {          
            pagingCtl.CurrentPage = 1;
            BindEmployee();
        }

        protected void ChangePageNumber()
        {
            BindEmployee();
        }

        protected void btnLoad_Click(object sender, EventArgs e)
        {
            BindEmployee();
        }
        protected void BindEmployee()
        {

            PayrollPeriod validPayrollPeriod = CommonManager.GetValidLastPayrollPeriod();
            //if (validPayrollPeriod != null && CalculationManager.IsPayrollNotSavedAtAll(validPayrollPeriod.PayrollPeriodId))
            
            if(validPayrollPeriod != null)
            {

                int totalRecords = 0;
                List<GetEmployeeListForOxfamOptimumPFAndCITResult> list =
                    PayManager.GetEmployeeListForOxfamOptimum(validPayrollPeriod.PayrollPeriodId, filterCtl.CITRoundOffValue, pagingCtl.CurrentPage - 1, 
                    int.Parse(pagingCtl.DDLRecords.SelectedItem.Value), ref totalRecords
                    ,filterCtl.BranchId,filterCtl.DepartmentId,filterCtl.EmployeeNameValue).ToList();
                gvwEmployeeList.DataSource = list;
                gvwEmployeeList.DataBind();
                pagingCtl.UpdatePagingBar(totalRecords);
                return;               
            }
            else
            {
                divWarningMsg.InnerHtml = "Salary already saved for current period.";
                divWarningMsg.Hide = false;
            }

        }

        #region Paging
        #region "Control state related"

        private int _tempCurrentPage;
        private int? _tempCount;

        protected override void OnInit(EventArgs e)
        {
            Page.RegisterRequiresControlState(this);
            base.OnInit(e);
        }
        protected override void LoadControlState(object savedState)
        {
            object[] rgState = (object[])savedState;
            base.LoadControlState(rgState[0]);
            //_sortBy = rgState[1].ToString();
            _tempCurrentPage = (int)rgState[2];

        }
        protected override object SaveControlState()
        {
            object[] rgState = new object[3];
            rgState[0] = base.SaveControlState();
            //rgState[1] = _sortBy;
            rgState[2] = _tempCurrentPage;
            return rgState;
        }

        //protected void btnPrevious_Click()
        //{
        //    _tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
        //    _tempCurrentPage -= 1;
        //    BindEmployee();
        //}

        //protected void btnNext_Click()
        //{
        //    _tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
        //    _tempCurrentPage += 1;
        //    BindEmployee();
        //}

        //protected void ddlRecords_SelectedIndexChanged()
        //{
        //    SessionManager.EmployeeDisplayRecodsPerPage = int.Parse(pagintCtl.DDLRecords.SelectedValue);
        //    _tempCurrentPage = 1;
        //    BindEmployee();
        //}
        #endregion
        #endregion

        protected void btnExport_Click(object sender, EventArgs e)
        {
            PayrollPeriod validPayrollPeriod = CommonManager.GetValidLastPayrollPeriod();
            //if (validPayrollPeriod != null && CalculationManager.IsPayrollNotSavedAtAll(validPayrollPeriod.PayrollPeriodId))

            if (validPayrollPeriod != null)
            {

                int totalRecords = 0;
                List<GetEmployeeListForOxfamOptimumPFAndCITResult> list =
                    PayManager.GetEmployeeListForOxfamOptimum(validPayrollPeriod.PayrollPeriodId, filterCtl.CITRoundOffValue, 0,
                    999999, ref totalRecords
                    , filterCtl.BranchId, filterCtl.DepartmentId, filterCtl.EmployeeNameValue).ToList();


                ExcelHelper.ExportToExcel<GetEmployeeListForOxfamOptimumPFAndCITResult>(
                    "Optimum List",
                    list,
                    new List<string> {"AdjustedCIT", "TotalRows", "RowNumber", "OptimumPF", "AdjustedPF", "TotalOptimumCIT", "OptimumId", "PayrollPeriodId","ReferencePayroll" }
                    , new List<string> { }
                    , new Dictionary<string, string>{{"ReferencePayroll","Ref Month"},{"IdCardNo","I No"},{"CurrentDeductionPF","Current PF Deduction"},{"EmployeePFAmount","Income PF"},{"CITAmount","Current CIT"},{"YearlyForeCast","Yearly ForeCast"}
                    ,{"Allowed","Max Allowed"},{"EID","EIN"},{"MonthlyOptimumRF","Monthly Optimum RF"},{"NewPF","PF Amount"},{"NewCIT","CIT Amount"},{"DesiredAllowed","Desired Contribution"},{"PFPercentage","PF Percentage"},{"PastPFAndCIT","Past RF Contribution"},{"RemainingAllowed","Remaining Allowed"},{"AdjustedCIT","Adjusted CIT"}}
                    , new List<string> { "EmployeePFAmount","RemainingAllowed","DesiredAllowed", "CurrentDeductionPF","CITAmount","MonthlyOptimumRF","NewPF","NewCIT","PastPFAndCIT", "YearlyForeCast", "OneThird", "OptimumCIT", "AdjustedCIT","Actual","Allowed" }, 
                    new Dictionary<string, string> { },
                    new List<string> { "EID","IdCardNo","Name","EmployeePFAmount","CurrentDeductionPF","CITAmount","Allowed","DesiredAllowed","PastPFAndCIT",
                        "RemainingAllowed",
                        "RemainingMonths","MonthlyOptimumRF","PFPercentage","NewPF","NewCIT"});

            }
        }

        protected void gvwDepartments_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvwEmployeeList.PageIndex = e.NewPageIndex;
            gvwEmployeeList.SelectedIndex = -1;
            BindEmployee();            
        }

        protected void ddlIncome_SelectedIndexChanged(object sender, EventArgs e)
        {            
        }

        protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
        {           
            BindEmployee();
        }

        protected void ddlGrade_SelectedIndexChanged(object sender, EventArgs e)
        {          
            BindEmployee();
        }

        protected void ddlDesignation_SelectedIndexChanged(object sender, EventArgs e)
        {            
            BindEmployee();
        }

        protected void btnUpdatePFAndCIT_Click(object sender, EventArgs e)
        {
             PayrollPeriod validPayrollPeriod = CommonManager.GetValidLastPayrollPeriod();
            //if (validPayrollPeriod != null && CalculationManager.IsPayrollNotSavedAtAll(validPayrollPeriod.PayrollPeriodId))

             if (validPayrollPeriod != null)
             {

                 int totalRecords = 0;
                 List<GetEmployeeListForOxfamOptimumPFAndCITResult> list =
                     PayManager.GetEmployeeListForOxfamOptimum(validPayrollPeriod.PayrollPeriodId, filterCtl.CITRoundOffValue, 0,
                     999999, ref totalRecords
                     , filterCtl.BranchId, filterCtl.DepartmentId, filterCtl.EmployeeNameValue).ToList();

                 int imported = PayManager.UpdateFixedDeductionPFAndCITForOxfam(list);

                 BindEmployee();

                 divMsgCtl.InnerHtml = "Updated for " + imported + " employees.";
                 divMsgCtl.Hide = false;
             }
             else
             {
                 divWarningMsg.InnerHtml = "Salary already saved for current period.";
                 divWarningMsg.Hide = false;
             }

           

            //int count = 0;

            //foreach (GridViewRow row in gvwEmployeeList.Rows)
            //{
            //    int EID = (int)(gvwEmployeeList.DataKeys[row.RowIndex]["EID"]);
            //    int OptimumId = (int)(gvwEmployeeList.DataKeys[row.RowIndex]["OptimumId"]);
            //    int PayrollPeriodId = (int)(gvwEmployeeList.DataKeys[row.RowIndex]["PayrollPeriodId"]);

            //    decimal oldAdjustedPF = 0;// (decimal)(gvwEmployeeList.DataKeys[row.RowIndex]["AdjustedPF"]);
            //    decimal oldAdjustedCIT = (decimal)(gvwEmployeeList.DataKeys[row.RowIndex]["AdjustedCIT"]);
             
            //    Label lblEmpPF = row.FindControl("lblEmpPF") as Label;
            //    Label lblCIT = row.FindControl("lblCIT") as Label;
            //    Label lblYearlyForecast = row.FindControl("lblYearlyForecast") as Label;

            //    Label lblOneThird = row.FindControl("lblOneThird") as Label;
            //    Label lblActual = row.FindControl("lblActual") as Label;
            //    Label lblAllowed = row.FindControl("lblAllowed") as Label;

            //    Label lblOptPF = row.FindControl("lblOptPF") as Label;
            //    TextBox txtAdjsutedPF = row.FindControl("txtAdjustedPF") as TextBox;
            //    Label lblOptCIT = row.FindControl("lblOptCIT") as Label;
            //    Label lblTotalOptCIT = row.FindControl("lblTotalOptCIT") as Label;
            //    TextBox txtAdjsutedCIT = row.FindControl("txtAdjustedCIT") as TextBox;

            //    decimal newAdjustedPF = 0;

            //    //decimal.TryParse(txtAdjsutedPF.Text,out newAdjustedPF);

            //    decimal newAdjustedCIT = decimal.Parse(txtAdjsutedCIT.Text);

            //    bool isPfOrCITAdjustmentChanged = (oldAdjustedPF != newAdjustedPF || oldAdjustedCIT != newAdjustedCIT);

            //    CheckBox chkApply = row.FindControl("chkApply") as CheckBox;

            //    //for not saved employee
            //    if (chkApply.Checked == false && OptimumId == 0)
            //        continue;

            //    //For already saved employee
            //    if (chkApply.Checked == true && OptimumId != 0 && isPfOrCITAdjustmentChanged == false)
            //        continue;

            //    DAL.OptimumPFAndCIT entity = new DAL.OptimumPFAndCIT();
            //    entity.EmployeeId = EID;
            //    entity.PayrollPeriodId = PayrollPeriodId;
            //    entity.OptimumId = OptimumId;

            //    entity.EmployeePFAmount = decimal.Parse(lblEmpPF.Text);
            //    entity.CIT = decimal.Parse(lblCIT.Text);
            //    entity.YearlyForeCast = decimal.Parse(lblYearlyForecast.Text);

            //    entity.Limit = decimal.Parse(lblOneThird.Text);
            //    entity.Actual = decimal.Parse(lblActual.Text);
            //    entity.Allowed = decimal.Parse(lblAllowed.Text);

            //    entity.OptimumPF = 0;// decimal.Parse(lblOptPF.Text);
            //    entity.AdjustedPF = newAdjustedPF;
            //    entity.OptimumCIT = decimal.Parse(lblOptCIT.Text);
            //    entity.TotalOptimumCIT = decimal.Parse(lblTotalOptCIT.Text);
            //    entity.AdjustedCIT = newAdjustedCIT;
            //    //entity.ActualPFCITLimitAmount = ActualPFCITLimitAmount;

            //    count += 1;

            //    if( isPfOrCITAdjustmentChanged && OptimumId != 0)

            //        PayManager.UpdateOptimum(entity);
            //    else
            //        PayManager.SaveDeleteOptimumList(entity);

            //}

            //divMsgCtl.InnerHtml = count + " employees information changed.";
            //divMsgCtl.Hide = false;


            //BindEmployee();

            
        }



        protected void btnChangeAmount_Click(object sender, EventArgs e)
        {
            int ein = 0;


            ein = int.Parse(hdnEmployeeId.Text);

            if (string.IsNullOrEmpty(txtNewAmount.Text))
            {
                NewMessage.ShowWarningMessage("Amount can not be blank, type zero to use default value.");
                BindEmployee();
                return;
            }

            decimal amount = 0;
            double? pfPercent = null;

            if (decimal.TryParse(txtNewAmount.Text.Trim(),out amount) == false)
            {
                NewMessage.ShowWarningMessage("Invalid amount.");
                BindEmployee();
                return;
            }

            if (!string.IsNullOrEmpty(txtNewPFPercent.Text))
            {
                double val = 0;
                if (double.TryParse(txtNewPFPercent.Text.Trim(), out val) == false)
                {
                    NewMessage.ShowWarningMessage("Invalid pf percent.");
                    BindEmployee();
                    return;
                }
                pfPercent = val;

            }

            

            PayManager.InsertUpdateAddonAmount(ein, amount,pfPercent);


            {
                NewMessage.ShowNormalMessage("Amount has been changed.");//, "refreshWindow");
                BindEmployee();
            }

        }
        

        protected void ddlRounding_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindEmployee();
        }


    }
}
