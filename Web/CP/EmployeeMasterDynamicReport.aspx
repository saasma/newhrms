﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EmployeeMasterDynamicReport.aspx.cs"
    Inherits="Web.Report.EmployeeMasterDynamicReport" Title="Employee Master" %>

<%@ Register Assembly="DevExpress.XtraReports.v16.1.Web, Version=16.1.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraReports.Web" TagPrefix="dx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script>
      var storeDynamic = null;
      Ext.onReady(
        function () {        
            storeDynamic = <%= storeDynamic.ClientID %>;
        }
    ); 
      

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <ext:ResourceManager ID="ResourceManager1" runat="server" />
    <script>
     var PagingToolbar1 = null;
      Ext.onReady(
        function () {        
            PagingToolbar1=<%=PagingToolbar1.ClientID%>;
        }
    ); 
//    if(storeDynamic!=null)
//    storeDynamic.currentPage =1;

    var getDragDropText = function () {

        var buf = [];

        buf.push("<ul>");

        Ext.each(this.view.panel.getSelectionModel().getSelection(), function (record) {
            buf.push("<li>" + record.data.FieldName + "</li>");
        });

        buf.push("</ul>");

        return buf.join("");
    };

      var PagingToolbar_ListItems = <%= PagingToolbar_ListItems.ClientID %>;
        function searchList()
        {
            PagingToolbar_ListItems.doRefresh();
        }

        function searchListDynamic()
        {
            PagingToolbar1.doRefresh();
        }
        
                var saveData = function () {
                    <%= Hidden1.ClientID %>.setValue(Ext.encode(<%= grid.ClientID %>.getRowsValues({selectedOnly : false})));
                };


    function GetServiceList() 
    {
    searchList();
    }
    
    </script>
    <div id="1">
        <ext:Hidden ID="Hidden1" runat="server" />
        <ext:Hidden ID="HiddenFilterHeader" runat="server" />
        <ext:Hidden ID="HiddenCurrentPage" runat="server" />
        <ext:Hidden ID="HiddenSelectedFields" runat="server" />
        <ext:Hidden ID="Hidden_ClassOrGroupID" runat="server" />
        <div class="page-title">
            <div class="titleBlock">
                <h2 id="header" runat="server">
                    Employee Master</h2>
            </div>
        </div>
    </div>
    <div id="container" style="width: inherit;">
        <div id="fullBlock" class="account_rev">
            <ext:Viewport ID="Viewport2" runat="server" Layout="BorderLayout" StyleSpec="background:white">
                <Items>
                    <ext:Container ID="Toolbar_AddNewCustomer" runat="server" Region="North" Border="false"
                        Cls="">
                        <Content>
                            <div class="page-title">
                                <div class="titleBlock">
                                    <h2>
                                        Employee Master
                                    </h2>
                                </div>
                            </div>
                        </Content>
                    </ext:Container>
                    <ext:Panel ID="Panel2" runat="server" Title="Filter Report" Region="West" Layout="FitLayout"
                        Width="225" MinWidth="500" MaxWidth="450" Split="true" Collapsible="true">
                        <Items>
                            <ext:Container ID="tabTransactions" Region="West" runat="server" Width="450" Title="Transactions"
                                AutoHeight="true" Cls="bg">
                                <Content>
                                    <table>
                                        <tr>
                                            <td style="width: 300px;">
                                                <ext:FileUploadField ID="FileDocumentUpload" runat="server" Width="360" Icon="Attach"
                                                    MarginSpec='10 0 0 10' FieldLabel="Report Template" LabelAlign="top" LabelSeparator=" :" />
                                                <%--  <ext:ComboBox ID="cmbReportType" Width="200" runat="server" ValueField="BranchID"
                                                    MarginSpec='10 0 0 10' DisplayField="Name" FieldLabel="Report Type" LabelAlign="Top"
                                                    LabelSeparator="" QueryMode="Local" ForceSelection="true">
                                                    <Store>
                                                        <ext:Store ID="Store1" runat="server">
                                                            <Model>
                                                                <ext:Model ID="Model6" runat="server">
                                                                    <Fields>
                                                                        <ext:ModelField Name="BranchID" />
                                                                        <ext:ModelField Name="Name" />
                                                                    </Fields>
                                                                </ext:Model>
                                                            </Model>
                                                        </ext:Store>
                                                    </Store>
                                                </ext:ComboBox>--%>
                                            </td>
                                            <td style="width: 50px;">
                                                <ext:Button Width="50" ID="btnViewShowReportTemplateWise" runat="server" Text="Ok"
                                                    Cls="" MarginSpec='35 0 0 10'>
                                                    <DirectEvents>
                                                        <Click OnEvent="btnViewShowReportTemplateWise_Click">
                                                            <EventMask ShowMask="true" />
                                                            <ExtraParams>
                                                                <ext:Parameter Name="SelectedFieldsLineJson" Value="Ext.encode(#{gridSelectedFields}.getRowsValues({selectedOnly : false}))"
                                                                    Mode="Raw" />
                                                            </ExtraParams>
                                                        </Click>
                                                    </DirectEvents>
                                                </ext:Button>
                                            </td>
                                        </tr>
                                    </table>
                                    <table>
                                        <tr>
                                            <td style="width: 250">
                                                <ext:GridPanel ID="gridFields" runat="server" Border="false" Width="200" Cls="itemgrid"
                                                    MarginSpec='10 10 0 10' Height="400" Scroll="Vertical">
                                                    <Store>
                                                        <ext:Store PageSize="20" runat="server" AutoLoad="true" ID="StoreFields" RemoteSort="true">
                                                            <Model>
                                                                <ext:Model ID="Model2" runat="server" IDProperty="ID">
                                                                    <Fields>
                                                                        <ext:ModelField Name="FieldName" Type="String" />
                                                                    </Fields>
                                                                </ext:Model>
                                                            </Model>
                                                        </ext:Store>
                                                    </Store>
                                                    <ColumnModel ID="ColumnModel1" runat="server">
                                                        <Columns>
                                                            <ext:Column ID="Column12" runat="server" Text="Avilable Fields" DataIndex="FieldName"
                                                                Sortable="false" Width="195">
                                                            </ext:Column>
                                                        </Columns>
                                                    </ColumnModel>
                                                    <SelectionModel>
                                                        <ext:RowSelectionModel ID="RowSelectionModel1" Mode="Multi" runat="server">
                                                        </ext:RowSelectionModel>
                                                    </SelectionModel>
                                                    <View>
                                                        <ext:GridView ID="GridView2" runat="server" StripeRows="false">
                                                            <Plugins>
                                                                <ext:GridDragDrop ID="GridDragDrop2" runat="server" DragGroup="firstGridDDGroup"
                                                                    DropGroup="secondGridDDGroup" />
                                                            </Plugins>
                                                            <Listeners>
                                                                <AfterRender Handler="this.plugins[0].dragZone.getDragText = getDragDropText;" Delay="1" />
                                                                <Drop Handler="var dropOn = overModel ? ' ' + dropPosition + ' ' + overModel.get('FieldName') : ' on empty view';" />
                                                            </Listeners>
                                                        </ext:GridView>
                                                    </View>
                                                </ext:GridPanel>
                                            </td>
                                            <td style="width: 250">
                                                <ext:GridPanel ID="gridSelectedFields" runat="server" MaxHeight="600" Height="400"
                                                    Scroll="Vertical" Border="false" Width="200" Cls="gridheight grid-header-bg"
                                                    MarginSpec='10 0 0 10'>
                                                    <Store>
                                                        <ext:Store PageSize="20" runat="server" AutoLoad="true" ID="StoreSelectedFields"
                                                            RemoteSort="true">
                                                            <Model>
                                                                <ext:Model ID="Model3" runat="server">
                                                                    <Fields>
                                                                        <ext:ModelField Name="FieldName" Type="String" />
                                                                    </Fields>
                                                                </ext:Model>
                                                            </Model>
                                                        </ext:Store>
                                                    </Store>
                                                    <ColumnModel ID="ColumnModel2" runat="server">
                                                        <Columns>
                                                            <ext:Column ID="Column13" runat="server" Text="Report Fields" DataIndex="FieldName"
                                                                Sortable="false" Width="195">
                                                            </ext:Column>
                                                        </Columns>
                                                    </ColumnModel>
                                                    <SelectionModel>
                                                        <ext:RowSelectionModel ID="RowSelectionModel2" Mode="Multi" runat="server">
                                                        </ext:RowSelectionModel>
                                                    </SelectionModel>
                                                    <View>
                                                        <ext:GridView ID="GridView1" runat="server" StripeRows="false">
                                                            <Plugins>
                                                                <ext:GridDragDrop ID="GridDragDrop1" runat="server" DragGroup="secondGridDDGroup"
                                                                    DropGroup="firstGridDDGroup" />
                                                                <ext:GridDragDrop ID="GridDragDrop3" runat="server" DragGroup="secondGridDDGroup"
                                                                    DropGroup="secondGridDDGroup" />
                                                            </Plugins>
                                                            <Listeners>
                                                                <AfterRender Handler="this.plugins[0].dragZone.getDragText = getDragDropText;" Delay="1" />
                                                                <Drop Handler="var dropOn = overModel ? ' ' + dropPosition + ' ' + overModel.get('FieldName') : ' on empty view';" />
                                                            </Listeners>
                                                        </ext:GridView>
                                                    </View>
                                                </ext:GridPanel>
                                            </td>
                                        </tr>
                                    </table>
                                </Content>
                            </ext:Container>
                        </Items>
                    </ext:Panel>
                    <ext:Panel ID="panelCenter" runat="server" Region="Center">
                        <Items>
                            <ext:Container ID="Container1" Region="Center" runat="server" Cls="bg">
                                <Content>
                                    <table>
                                        <tr>
                                            <td style="padding: 10px 0 10px 10px">
                                                <ext:ComboBox ID="cmbGroupBy" Width="250" runat="server" ValueField="FieldServerMaping"
                                                    Hidden="true" LabelWidth="70" DisplayField="FieldName" FieldLabel="Group By"
                                                    LabelAlign="left" LabelSeparator="" QueryMode="Local" ForceSelection="true">
                                                    <Store>
                                                        <ext:Store ID="Store2" runat="server">
                                                            <Model>
                                                                <ext:Model ID="Model1" runat="server">
                                                                    <Fields>
                                                                        <ext:ModelField Name="FieldServerMaping" />
                                                                        <ext:ModelField Name="FieldName" />
                                                                    </Fields>
                                                                </ext:Model>
                                                            </Model>
                                                        </ext:Store>
                                                    </Store>
                                                </ext:ComboBox>
                                            </td>
                                            <td style="padding: 10px 0 10px 10px">
                                                <span class="right">
                                                    <ext:Button Width="70" ID="btnSearch" runat="server" Text="Search" Cls="bluebutton left">
                                                        <%-- <Listeners>
                                                            <Click Handler="GetServiceList()" />
                                                        </Listeners>--%>
                                                        <DirectEvents>
                                                            <Click OnEvent="btnSearch_Click">
                                                            </Click>
                                                        </DirectEvents>
                                                    </ext:Button>
                                                </span>
                                            </td>
                                            <td style="padding: 10px 0 10px 10px">
                                                <ext:Button ID="Button1" runat="server" Text="Export" Cls="updatebtn">
                                                    <Menu>
                                                        <ext:Menu ID="Menu2" runat="server" ShowSeparator="false" Cls="dropdownbtn">
                                                            <Items>
                                                                <ext:MenuItem AutoPostBack="true" runat="server" OnClick="btnDynamicExcelPrint_Click"
                                                                    Text="Excel" ID="btnExcelPrint" Handler="var filters=Ext.encode((#{GridPanel1}).filters.getFilterData());#{HiddenFilterHeader}.setValue(filters);var SelectedFields = Ext.encode(#{gridSelectedFields}.getRowsValues({selectedOnly : false}));#{HiddenSelectedFields}.setValue(SelectedFields)">
                                                                </ext:MenuItem>
                                                            </Items>
                                                        </ext:Menu>
                                                    </Menu>
                                                </ext:Button>
                                            </td>
                                            <td style="padding: 10px 0 10px 10px">
                                                <ext:Button Width="100" ID="btnSaveReportTemplate" runat="server" Text="Save Template"
                                                    Cls="bluebutton left">
                                                    <DirectEvents>
                                                        <Click OnEvent="btnSaveReportTemplate_Click">
                                                            <EventMask ShowMask="true" />
                                                            <ExtraParams>
                                                                <ext:Parameter Name="SelectedFieldsLineJson" Value="Ext.encode(#{gridSelectedFields}.getRowsValues({selectedOnly : false}))"
                                                                    Mode="Raw" />
                                                            </ExtraParams>
                                                        </Click>
                                                    </DirectEvents>
                                                </ext:Button>
                                            </td>
                                        </tr>
                                    </table>
                                </Content>
                            </ext:Container>
                            <ext:Container ID="Container3" Region="Center" runat="server" Layout="FitLayout"
                                Cls="bg">
                                <Content>
                                    <ext:GridPanel ID="GridPanel1" runat="server" Border="false" Region="Center" AutoScroll="true"
                                        Height="490" Scroll="Both" Cls="itemgrid">
                                        <Store>
                                            <ext:Store PageSize="20" runat="server" ID="storeDynamic" RemoteSort="true" OnReadData="Store_ReadDataDynamic">
                                                <Model>
                                                    <ext:Model ID="DynamicModel" runat="server" />
                                                </Model>
                                                <Proxy>
                                                    <ext:PageProxy>
                                                    </ext:PageProxy>
                                                </Proxy>
                                                <Parameters>
                                                    <ext:StoreParameter Name="SelectedFieldsLineJson" Value="Ext.encode(#{gridSelectedFields}.getRowsValues({selectedOnly : false}))"
                                                        Mode="Raw" />
                                                </Parameters>
                                            </ext:Store>
                                        </Store>
                                        <Features>
                                            <ext:GridFilters ID="GridFilters2" runat="server">
                                            </ext:GridFilters>
                                            <ext:GroupingSummary ID="GroupingSummary2" runat="server" GroupHeaderTplString='{columnName}: {name} ({rows.length} Item{[values.rows.length > 1 ? "s" : ""]})'
                                                HideGroupedHeader="true" EnableGroupingMenu="false" />
                                        </Features>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar1" runat="server" StoreID="storeDynamic" DisplayInfo="true">
                                                <Items>
                                                    <ext:Label ID="Label1" runat="server" Text="Page size:" />
                                                    <ext:ToolbarSpacer ID="ToolbarSpacer1" runat="server" Width="10" />
                                                    <ext:ComboBox ID="ComboBox1" runat="server" Width="80" SelectOnFocus="true" Selectable="true"
                                                        ValueField="Value" DisplayField="Text">
                                                        <Listeners>
                                                            <Select Handler="#{storeDynamic}.pageSize = this.getValue();#{PagingToolbar1}.moveFirst(); searchListDynamic()" />
                                                        </Listeners>
                                                        <Items>
                                                            <ext:ListItem Value="20" Text="20" />
                                                            <ext:ListItem Value="50" Text="50" />
                                                            <ext:ListItem Value="100" Text="100" />
                                                        </Items>
                                                        <SelectedItems>
                                                            <ext:ListItem Index="0">
                                                            </ext:ListItem>
                                                        </SelectedItems>
                                                    </ext:ComboBox>
                                                </Items>
                                            </ext:PagingToolbar>
                                        </BottomBar>
                                    </ext:GridPanel>
                                </Content>
                            </ext:Container>
                            <ext:Container ID="Container2" Region="Center" runat="server" Layout="FitLayout"
                                Cls="bg">
                                <Content>
                                    <ext:GridPanel ID="grid" runat="server" Border="false" Region="Center" AutoScroll="true"
                                        Visible="false" Height="490" Scroll="Both" Cls="itemgrid">
                                        <Store>
                                            <ext:Store PageSize="20" runat="server" AutoLoad="false" ID="storeGrid" RemoteSort="true"
                                                GroupField="AssetName">
                                                <Model>
                                                    <ext:Model ID="Model5" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="SysNo" Type="String" />
                                                            <ext:ModelField Name="AssetNumber" Type="String" />
                                                            <ext:ModelField Name="Status" Type="String" />
                                                            <ext:ModelField Name="AssetName" Type="String" />
                                                            <ext:ModelField Name="AssetHolder" Type="String" />
                                                            <ext:ModelField Name="AcquisitionDate" Type="String" />
                                                            <ext:ModelField Name="AcquisitionCost" Type="Float" />
                                                            <ext:ModelField Name="AdditionalCost" Type="Float" />
                                                            <ext:ModelField Name="LocationRef_Name" Type="String" />
                                                            <ext:ModelField Name="ClassOrGroupRef_Name" Type="String" />
                                                            <ext:ModelField Name="GLAssetAccount" Type="string" />
                                                            <ext:ModelField Name="CapitalizedDate" Type="String" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                                <Parameters>
                                                    <ext:StoreParameter Name="SelectedFieldsLineJson" Value="Ext.encode(#{gridSelectedFields}.getRowsValues({selectedOnly : false}))"
                                                        Mode="Raw" />
                                                </Parameters>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel ID="GridPanel_ListItems_ColumnModel" runat="server">
                                            <Columns>
                                                <ext:Column ID="Column4" runat="server" Text="System Number" DataIndex="SysNo" Sortable="true"
                                                    Width="150">
                                                </ext:Column>
                                                <ext:Column ID="Column1" runat="server" Text="Asset Code" DataIndex="AssetNumber"
                                                    Sortable="true" Width="150">
                                                </ext:Column>
                                                <ext:Column ID="Column2" runat="server" Text="Asset Name" DataIndex="AssetName" Sortable="true"
                                                    Width="200">
                                                </ext:Column>
                                                <ext:Column ID="Column8" runat="server" Text="Held by" DataIndex="AssetHolder" Sortable="true"
                                                    Width="100" />
                                                <ext:Column ID="Column3" runat="server" Text="Status" DataIndex="Status" Sortable="true"
                                                    Width="50" />
                                                <ext:DateColumn ID="Columna4" runat="server" Text="Acquisition Date" DataIndex="AcquisitionDate"
                                                    Width="125" />
                                                <ext:Column ID="Column9" runat="server" Text="Acquisition Cost" DataIndex="AcquisitionCost"
                                                    Width="125">
                                                    <Renderer Fn="getFormattedAmount">
                                                    </Renderer>
                                                </ext:Column>
                                                <ext:Column ID="Column11" runat="server" Text="Additional Cost" DataIndex="AdditionalCost"
                                                    Width="125">
                                                    <Renderer Fn="getFormattedAmount">
                                                    </Renderer>
                                                </ext:Column>
                                                <ext:Column ID="Column5" Align="Left" runat="server" Text="Class" DataIndex="ClassOrGroupRef_Name"
                                                    Width="150">
                                                </ext:Column>
                                                <ext:Column ID="Column6" Align="Left" runat="server" Text="Location" DataIndex="LocationRef_Name"
                                                    Width="100">
                                                </ext:Column>
                                                <ext:Column ID="Column7" Align="Left" runat="server" Text="G/L Asset Account" DataIndex="GLAssetAccount"
                                                    Width="200" />
                                                <ext:DateColumn ID="Columna10" Align="Left" runat="server" Text="Capitalized Date"
                                                    DataIndex="CapitalizedDate" Width="200" />
                                            </Columns>
                                        </ColumnModel>
                                        <Features>
                                            <ext:GridFilters ID="GridFilters1" runat="server">
                                                <Filters>
                                                    <ext:StringFilter DataIndex="SysNo" />
                                                    <ext:StringFilter DataIndex="AssetNumber" />
                                                    <ext:StringFilter DataIndex="Status" />
                                                    <ext:StringFilter DataIndex="AssetName" />
                                                    <ext:StringFilter DataIndex="AssetHolder" />
                                                    <ext:NumericFilter DataIndex="AcquisitionCost" />
                                                    <ext:NumericFilter DataIndex="AdditionalCost" />
                                                    <ext:StringFilter DataIndex="LocationRef_Name" />
                                                    <ext:StringFilter DataIndex="ClassOrGroupRef_Name" />
                                                    <ext:StringFilter DataIndex="GLAssetAccount" />
                                                </Filters>
                                            </ext:GridFilters>
                                            <ext:GroupingSummary ID="GroupingSummary1" runat="server" GroupHeaderTplString='{columnName}: {name} ({rows.length} Item{[values.rows.length > 1 ? "s" : ""]})'
                                                HideGroupedHeader="true" EnableGroupingMenu="false" />
                                        </Features>
                                        <SelectionModel>
                                            <ext:RowSelectionModel ID="RowSelectionModel" Mode="Single" runat="server">
                                            </ext:RowSelectionModel>
                                        </SelectionModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar_ListItems" runat="server" StoreID="storeGrid"
                                                DisplayInfo="true">
                                                <Items>
                                                    <ext:Label ID="lblPageSize" runat="server" Text="Page size:" />
                                                    <ext:ToolbarSpacer ID="ToolbarSpacer_listItems" runat="server" Width="10" />
                                                    <ext:ComboBox ID="ddlListItemPageSize" runat="server" Width="80" SelectOnFocus="true"
                                                        Selectable="true" ValueField="Value" DisplayField="Text">
                                                        <Listeners>
                                                            <Select Handler="#{storeGrid}.pageSize = this.getValue();#{PagingToolbar_ListItems}.moveFirst(); searchList()" />
                                                        </Listeners>
                                                        <Items>
                                                            <ext:ListItem Value="20" Text="20" />
                                                            <ext:ListItem Value="50" Text="50" />
                                                            <ext:ListItem Value="100" Text="100" />
                                                        </Items>
                                                        <SelectedItems>
                                                            <ext:ListItem Index="0">
                                                            </ext:ListItem>
                                                        </SelectedItems>
                                                    </ext:ComboBox>
                                                </Items>
                                            </ext:PagingToolbar>
                                        </BottomBar>
                                    </ext:GridPanel>
                                </Content>
                            </ext:Container>
                        </Items>
                    </ext:Panel>
                </Items>
            </ext:Viewport>
        </div>
    </div>
    </form>
</body>
</html>
