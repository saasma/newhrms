<%@ Page Title="CIT Balance Report" Language="C#" EnableViewState="false" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="CITBalanceReport.aspx.cs" Inherits="Web.CITBalanceReport" %>

<%@ Import Namespace="BLL.Manager" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
      <script src="../Scripts/ScrollTableHeader103.min.js?v=200 type="text/javascript"></script>
    <script type="text/javascript">

        var skipLoadingCheck = true;

        $(document).ready(
            function () {

                $('.tableLightColor tbody').attr('id', 'scrollMe');
                var sth = new ScrollTableHeader();

                sth.addTbody("scrollMe");
                sth.delayAfterScroll = 150;
                sth.minTableRows = 10;
            }
        );
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">

<div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    CIT Balance Report
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">

    <div class="clear gridBlock">

            <div class="buttonsDiv">
                    <table>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtEmp" runat="server" Width="200" 
                                    style="margin:7px;">
                                </asp:TextBox>
                            </td>
                            <td>
                                <asp:Button ID="btnSearch" runat="server" Width="100" 
                                    Text="Search" CssClass="update" style="margin:7px;" 
                                    onclick="btnSearch_Click" AutoPostBack="true">
                                </asp:Button>
                            </td>
                        </tr>
                    </table>
            </div>
        

        <cc2:EmptyDisplayGridView CssClass="tableLightColor" UseAccessibleHeader="true" ShowHeaderWhenEmpty="True"
            ID="gvEmployeeIncome" runat="server" DataKeyNames="EmployeeId" AutoGenerateColumns="False"
            CellPadding="4" GridLines="None" AllowSorting="True" ShowFooterWhenEmpty="False">
            <RowStyle BackColor="#E3EAEB" />
            <Columns>
                <%--<asp:BoundField DataField="SN" HeaderText="SN"></asp:BoundField>--%>
                <asp:BoundField DataField="EmployeeId" HeaderText="EIN"></asp:BoundField>
                <asp:BoundField DataField="Title" HeaderStyle-HorizontalAlign="Left" HeaderText="Title">
                </asp:BoundField>
                <asp:TemplateField HeaderText="Name" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <asp:Label runat="server" Width="140" Text='<%# Eval("Name") %>' />
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Opening CIT" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Width="90" Text='<%# GetCurrency( Eval("OpeningCIT")) %>' />
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Opening Interest" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" Width="90" Text='<%# GetCurrency( Eval("OpeningCITInterest")) %>' />
                    </ItemTemplate>
                </asp:TemplateField>

                
            </Columns>
            <RowStyle CssClass="odd" />
            <AlternatingRowStyle CssClass="even" />
            <EmptyDataTemplate>
                <b>No employee list. </b>
            </EmptyDataTemplate>
        </cc2:EmptyDisplayGridView>
    </div>
    <div class="buttonsDiv">
        <asp:Button ID="btnUpdate" CssClass="excel" runat="server" Text="Export" OnClick="btnUpdate_Click" />
    </div>
</div>
</asp:Content>
