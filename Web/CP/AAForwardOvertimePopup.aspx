﻿<%@ Page Language="C#" AutoEventWireup="true" Title="Edit Overtime" MasterPageFile="~/Master/ForPopupPage.Master"
    CodeBehind="AAForwardOvertimePopup.aspx.cs" Inherits="Web.CP.AAForwardOvertimePopup" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<%@ Register Assembly="DevExpress.Web.v16.1, Version=16.1.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function ConfirmInstitutionDeletion() {
            if (ddllist.selectedIndex == -1) {
                alert('No Insurance company to delete.');
                clearUnload();
                return false;
            }
            if (confirm('Do you want to delete the insurance company?')) {
                clearUnload();
                return true;
            }
            else {
                return false;
            }
        }

        function DisplayInstitutionInTextBox(dropdown_id) {

        }



        //capture window closing event


        function closePopup() {

            window.opener.refreshEventList(window);

        }

      
    </script>
    <style type="text/css">
    .tablefield>tbody>tr>td{padding:5px;}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <div class="popupHeader">
        <span style='margin-left: 20px; padding-top: 8px; display: block' runat="server"
            id="title">Overtime : </span>
    </div>
    <div class="marginal" style='    margin-top: 5px;'>
        <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" Width="390px" runat="server" />
        <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
        <asp:HiddenField ID="Hidden_OldMinute" runat="server" />
        <asp:HiddenField ID="Hidden_RequestID" runat="server" />
        <asp:HiddenField ID="Hidden_DurationPrev" runat="server" />
        <asp:HiddenField ID="Hidden_OvertimeTypePrev" runat="server" />
        <table style="margin-bottom: 10px;" class="tablefield">
            <tbody>
                <tr>
                    <td colspan="2">
                        <p style="margin-bottom: -3px; margin-top: 0px;">
                            Overtime type
                        </p>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:DropDownList DataTextField="Name" DataValueField="OvertimeTypeId" ID="ddlOvertimeType"
                            runat="server" Style="width: 200px; margin-right: 25px;">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator5" ValidationGroup="SaveUpdate"
                            Display="None" ErrorMessage="Overtime type is required." ControlToValidate="ddlOvertimeType"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>Date : </strong>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblDate"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>Name : </strong>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblName"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>Description : </strong>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblReason"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>In Time : </strong>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblInTime"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>Out Time : </strong>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblOutTime"></asp:Label>
                    </td>
                </tr>
                <tr runat="server" id="rowRecommender">
                    <td>
                        <strong>Recommender : </strong>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblRecommender"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>Approval : </strong>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblApproval"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>Status : </strong>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblStatus"></asp:Label>
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="tbl">
            <tr>
                <td>
                    <strong>Requested Hour : </strong>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox Enabled="false" ID="txtRequested" runat="server" ValidationGroup="SaveUpdate"
                        Style="width: 100px;" ReadOnly="true"></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <strong>Approve For : </strong>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Hour<br />
                    <asp:TextBox ID="txtHour" runat="server" ValidationGroup="SaveUpdate" Style="width: 100px;"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" Display="None" ErrorMessage="Hour is required, if no value place 0."
                        ControlToValidate="txtHour" ValidationGroup="SaveUpdate" />
                </td>
                <td style="padding-left: 20px;" runat="server" id="colMin">
                    Minute<br />
                    <asp:TextBox ID="txtMinute" runat="server" ValidationGroup="SaveUpdate" Style="width: 100px;"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="None"
                        ErrorMessage="Minute is required, if no value place 0." ControlToValidate="txtMinute"
                        ValidationGroup="SaveUpdate" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnUpdate" ValidationGroup="SaveUpdate" runat="server" OnClientClick="valGroup='SaveUpdate'; if(confirm('Are you sure you want to update this request?')) return CheckValidation(); else return false;"
                        Text="Update" Width="100" Height="30" Style="margin-top: 25px;" OnClick="btnUpdate_Click"
                        CssClass="save" />
                </td>
                <td style="padding-left: 20px;">
                    <asp:Button ID="btnReject" ValidationGroup="SaveUpdate" runat="server" OnClientClick="valGroup='SaveUpdate'; if(confirm('Are you sure you want to reject this request?')) return CheckValidation(); else return false;"
                        Text="Reject" Width="100" Height="30" Style="margin-top: 25px;" OnClick="btnReject_Click"
                        CssClass="save" />
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td colspan="10" colspan="2">
                    <cc2:EmptyDisplayGridView CssClass="tableLightColor" UseAccessibleHeader="true" ShowHeaderWhenEmpty="True"
                        ID="gridHistory" runat="server" AutoGenerateColumns="False" CellPadding="4" GridLines="None"
                        ShowFooterWhenEmpty="False">
                        <RowStyle BackColor="#E3EAEB" />
                        <Columns>
                            <asp:BoundField HeaderStyle-Width="160px" HeaderStyle-HorizontalAlign="Left" HeaderText="Modified by"
                                DataField="ModifiedByName"></asp:BoundField>
                            <asp:BoundField DataField="ModifiedOn" DataFormatString="{0:dd-M-yyyy}" HeaderStyle-Width="125px"
                                HeaderStyle-HorizontalAlign="Left" HeaderText="Modified on"></asp:BoundField>
                            <asp:BoundField DataField="Column1Before" HeaderStyle-Width="160px" HeaderStyle-HorizontalAlign="Left"
                                HeaderText="Before"></asp:BoundField>
                            <asp:BoundField DataField="Column1After" HeaderStyle-Width="160px" HeaderStyle-HorizontalAlign="Left"
                                HeaderText="After"></asp:BoundField>
                        </Columns>
                        <RowStyle CssClass="odd" />
                        <AlternatingRowStyle CssClass="even" />
                        <EmptyDataTemplate>
                            <b>No list. </b>
                        </EmptyDataTemplate>
                    </cc2:EmptyDisplayGridView>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
