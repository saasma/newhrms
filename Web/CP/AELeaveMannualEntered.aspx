<%@ Page Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/Master/ForPopupPage.Master"
    AutoEventWireup="true" CodeBehind="AELeaveMannualEntered.aspx.cs" Title="Mannual Leave details"
    Inherits="Web.CP.AELeaveMannualEntered" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Assembly="Karpach.WebControls" Namespace="Karpach.WebControls" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/UpDownCtl.ascx" TagName="UpDownCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="InfoMsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .colorStyle td
        {
            padding: 2px !important;
        }
        .transparent
        {
            /* this class makes a window partially transparent */
            opacity: 0; /* Standard style for transparency */
            -moz-opacity: 0; /* Transparency for older Mozillas */
            filter: alpha(opacity=00); /* Transparency for IE */
            width: 0;
        }
        .paddinAll
        {
            padding: 10px;
        }
        div.notify
        {
            margin-left: 0;
            width: 41%;
        }
        .lf
        {
            width: 150px;
            padding: 0 0px 0px 0;
        }
        .lfs
        {
            width: 160px;
            padding: 0 10px 0px 0;
            vertical-align: top;
        }
        table tr td
        {
            padding-bottom: 10px;
        }
        .bevel
        {
            margin-bottom: 20px;
        }
        
        div.notify
        {
            width: 510px;
        }
        .displayInline
        {
            display: inline;
        }
    </style>
    <script type="text/javascript">
        function closePopup() {
            // alert(window.opener.parentReloadCallbackFunction)
            //if ($.browser.msie == false && typeof (window.opener.reloadLeave) != 'undefined' && ($.browser.chrome || $.browser.safari)) {
            window.opener.reloadLeave(window);
            //            } else {
            //                window.returnValue = "Reload";
            //                window.close();
            //            }

        }


        

        
     
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <asp:HiddenField ID="hdEditSequence" runat="server" />
    <asp:HiddenField ID="hdEmployeeEditSequence" runat="server" />
    <div class="popupHeader">
        <h3 runat="server" id="leaveDetails">
        </h3>
    </div>
    <div class="marginal" style='margin-top: 0px'>
        <uc2:InfoMsgCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
        <uc2:MsgCtl ID="divMsg" EnableViewState="false" runat="server" Hide="true" />
        <fieldset class="large-heading">
        </fieldset>
        <div class="bevel paddinAll" style="width: 750px!important; margin-bottom: 5px!important">
            <table cellpadding="0" cellspacing="0" width="750px!important">
                <tr>
                    <td class="lf">
                        Leave name<label>*</label>
                    </td>
                    <td>
                        <asp:TextBox Enabled="false" ID="txtTitle" runat="server" Width="200px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="valReqdTitle" runat="server" ControlToValidate="txtTitle"
                            Display="None" ErrorMessage="Title is required." ValidationGroup="Leave"></asp:RequiredFieldValidator>
                    </td>
                    <td class="lf">
                        Total Maximum Period
                    </td>
                    <td>
                        <uc1:UpDownCtl ID="upDownMannualMaxPeriod" Value="0" AllowDecimal="true" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="lf">
                        Leave Type<label>*</label>
                    </td>
                    <td>
                        <asp:DropDownList Enabled="false" ID="ddlLeaveType" runat="server" Width="200px">
                            <asp:ListItem Selected="True" Value="-1">-- Select Type --</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td class="lf">
                        <asp:Label ID="lblHalfDay" runat="server" Text="Half Day Leave" />
                    </td>
                    <td>
                        <asp:DropDownList Enabled="false" ID="ddlAllowHalfDay" runat="server" Width="200px"
                            AppendDataBoundItems="true">
                            <asp:ListItem Selected="True" Text="--Select Half day--" Value="-1"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="lf">
                        Applies To<label>*</label>
                    </td>
                    <td>
                        <asp:DropDownList Enabled="false" AppendDataBoundItems="true" Width="200px" ID="ddlAppliesTo"
                            runat="server" DataValueField="Key" DataTextField="Value">
                            <asp:ListItem Text="---Select---" Value="-1"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td class="lf">
                        <asp:Label ID="Label1" runat="server" Text="Count Holiday as Leave" />
                    </td>
                    <td>
                        <asp:DropDownList Enabled="false" ID="ddlCountHolidayAsLeave" runat="server" Width="200px"
                            AppendDataBoundItems="true">
                            <asp:ListItem Selected="True" Text="--Select--" Value="-1"></asp:ListItem>
                            <asp:ListItem Text="Yes" Value="false"></asp:ListItem>
                            <asp:ListItem Text="No" Value="true"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="lf" runat="server" id="gender">
                        Applies To ({0})
                    </td>
                    <td>
                        <asp:DropDownList Enabled="false" AppendDataBoundItems="true" Width="200px" ID="ddlAppliesToGender"
                            runat="server" DataValueField="Key" DataTextField="Value">
                            <asp:ListItem Text="---Select---" Value="-1" />
                            <asp:ListItem Text="Female Only" Value="0" />
                        </asp:DropDownList>
                    </td>
                    <td class="lf">
                        <asp:Label ID="Label2" runat="server" Text="Allowable Past days" />
                    </td>
                    <td>
                        <asp:TextBox Enabled="false" ID="txtAllowablePastDays" Width="100px" runat="server"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </div>
        <div runat="server" id="sectionEmployee" style='display: none; clear: both;'>
            <div class="bevel paddinAll" style="width: 600px!important">
                <asp:GridView Style="clear: both" ID="gvw" PagerStyle-HorizontalAlign="Center" PagerStyle-CssClass="defaultPagingBar"
                    CssClass="tableLightColor" AutoGenerateColumns="false" UseAccessibleHeader="true"
                    runat="server" OnRowCommand="gvw_RowCommand" CellPadding="3" DataKeyNames="Month,Year"
                    rateColumns="False" OnSelectedIndexChanged="gvw_SelectedIndexChanged">
                    <Columns>
                        <asp:TemplateField HeaderText="Activity" HeaderStyle-Width="90px" HeaderStyle-HorizontalAlign="Left"
                            ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <%# Eval("Type")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Date" HeaderStyle-Width="250px" HeaderStyle-HorizontalAlign="Left"
                            ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <%# Eval("DateDisplay")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Period" HeaderStyle-Width="90px" HeaderStyle-HorizontalAlign="Right"
                            ItemStyle-HorizontalAlign="Right">
                            <ItemTemplate>
                                <%# Eval("Period")%>&nbsp;
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Notes" HeaderStyle-Width="300px" HeaderStyle-HorizontalAlign="Left"
                            ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <%# Eval("Notes")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Balance" HeaderStyle-Width="90px" HeaderStyle-HorizontalAlign="Right"
                            ItemStyle-HorizontalAlign="Right">
                            <ItemTemplate>
                                <%# Eval("Balance")%>&nbsp;
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-Width="30px" HeaderText="Delete" HeaderStyle-HorizontalAlign="Center"
                            ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:ImageButton ID="btnEdit" Visible='<%# Eval("Type")=="Taken" ? false : true %>'
                                    CommandName="Select" ImageUrl="~/images/edit.gif" runat="server" AlternateText="Edit" />
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="odd" />
                    <AlternatingRowStyle CssClass="even" />
                </asp:GridView>
            </div>
        </div>
        <div style="clear: both">
            <asp:Button ID="btnAdd" CssClass="update" OnClick="btnAdd_Click" runat="server" OnClientClick="valGroup='Leave';return CheckValidation()"
                Text="Add Leave" ValidationGroup="Leave" /></div>
        <div runat="server" id="divAddLeave" style='clear: both; display: none'>
            <div class="bevel paddinAll" style="width: 540px!important">
                <table>
                    <tr>
                        <td style="padding-bottom: 0px; padding-left: 2px;">
                            <strong>Add On</strong>
                        </td>
                        <td style="width: 10px">
                        </td>
                        <td style="padding-bottom: 0px">
                            <strong>Period</strong>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <My:Calendar Enabled="false" IsSkipDay="true" Id="calPeriod" runat="server" />
                        </td>
                        <td style="width: 10px">
                        </td>
                        <td>
                            <uc1:UpDownCtl ID="upDownPeriodValue" Value="0" AllowDecimal="true" runat="server" />
                        </td>
                    </tr>
                </table>
                <div style="padding-left: 5px; padding-bottom: 10px">
                    <strong>Notes</strong>
                    <br />
                    <asp:TextBox TextMode="MultiLine" Width="400" Height="40" runat="server" ID="txtNotes">
                    </asp:TextBox></div>
                <div class="clear" style='padding-bottom: 10px; padding-left: 4px;'>
                    <asp:Button ID="btnSave" CssClass="save" runat="server" OnClientClick='confirm("Confirm to assign leave?");'
                        OnClick="btnOk_Click" Text="Save" ValidationGroup="Leave" />
                    <asp:Button ID="btnCancel" runat="server" CssClass="cancel" Text="Cancel" OnClick="btnCancel_Click" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
