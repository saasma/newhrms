<%@ Page MaintainScrollPositionOnPostback="true" Title="Leave Settings" Language="C#"
    MasterPageFile="~/Master/HROld.Master" AutoEventWireup="true" CodeBehind="LeaveSetting.aspx.cs"
    Inherits="Web.CP.LeaveSetting" %>

   
<%@ Register Assembly="DevExpress.Web.v16.1, Version=16.1.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        tr
        {
            padding-top: 10px;
        }
        .headerWithUnderLine
        {
            color: #1F4E78;
            padding-bottom: 2px;
            border-bottom: 1px solid #2F75B5;
            width: 300px;
        }
        .fieldHeader
        {
            padding-top: 15px;
        }
        .fieldHeader span
        {
            color: #000000;
            font-weight: bold;
        }
        input[type=submit]
        {
            cursor: pointer;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
<ext:ResourceManager runat="server" ScriptMode="Release" DisableViewState="false" />
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Leave Settings
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="panel panel-default">
            <!-- panel-heading -->
            <div class="panel-body">
                <div class="contentArea1">
                    <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
                    <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
                    <table style='clear: both; float: left'>
                        <tr>
                            <td valign="top">
                                <table>
                                    <tr>
                                        <td colspan="2" style="padding-top: 0px">
                                            <h3 class="headerWithUnderLine">
                                                Leave Month Settings</h3>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fieldHeader" style='width: 190;'>
                                            <My:Label ID="Label4" Text="Leave start month" runat="server" ShowAstrick="false" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:DropDownList Enabled="false" AppendDataBoundItems="true" Width="183px" ID="ddlLeaveStartMonth"
                                                runat="server" DataValueField="Key" DataTextField="Value">
                                                <asp:ListItem Text="" Value="-1"></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlLeaveStartMonth"
                                                InitialValue="-1" Display="None" ErrorMessage="Please select leave start month."
                                                ValidationGroup="AECompany"></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnEdit1" Width="60" Height="22" Style="margin-top: 1px; border: 1px solid gray"
                                                Visible="true" runat="server" Text="Edit" OnClick="btnEdit1_Click" />
                                            <asp:Button ID="btnCancel1" Width="60" Height="22" Style="margin-top: 1px; border: 1px solid gray"
                                                runat="server" Text="Cancel" Visible="false" OnClick="btnCancel1_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fieldHeader" style='width: 190;'>
                                            <My:Label ID="Label21" Text="Lapse and Encash processing month" runat="server" ShowAstrick="false" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:DropDownList Enabled="false" AppendDataBoundItems="true" Width="183px" ID="ddlLapseEncashProcessingMonth"
                                                runat="server" DataValueField="Key" DataTextField="Value">
                                                <asp:ListItem Text="Before Leave start month or Default" Value="-1"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnLapseEncashProcessingEnable" Width="60" Height="22" Style="margin-top: 1px;
                                                border: 1px solid gray" Visible="true" runat="server" Text="Edit" OnClick="btnLapseEncashProcessingEnable_Click" />
                                            <asp:Button ID="btnLapseEncashProcessingCancel" Width="60" Height="22" Style="margin-top: 1px;
                                                border: 1px solid gray" runat="server" Text="Cancel" Visible="false" OnClick="btnLapseEncashProcessingCancel_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fieldHeader" colspan="2">
                                            <My:Label ID="Label1" Text="Pay Encash type leaves " runat="server" ShowAstrick="false" />
                                            <%--<My:Label ID="Label5" Text="IRO name:" runat="server" ShowAstrick="true" /> --%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:DropDownList Enabled="false" AppendDataBoundItems="true" Width="183px" ID="ddlEncashLeavePaysOn"
                                                runat="server" DataValueField="Key" DataTextField="Value">
                                                <asp:ListItem Text="" Value="-1"></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlEncashLeavePaysOn"
                                                InitialValue="-1" Display="None" ErrorMessage="Please select encash leave pays on."
                                                ValidationGroup="AECompany"></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnEdit2" Width="60" Height="22" Style="margin-top: 1px; border: 1px solid gray"
                                                runat="server" Text="Edit" OnClick="btnEdit2_Click" />
                                            <asp:Button ID="btnCancel2" Width="60" Height="22" Style="margin-top: 1px; border: 1px solid gray"
                                                runat="server" Text="Cancel" Visible="false" OnClick="btnCancel2_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fieldHeader" colspan="2">
                                            <My:Label ID="Label2" Text="Pay Lapse and Encash type leaves on" runat="server" ShowAstrick="false" />
                                            <%--<My:Label ID="Label5" Text="IRO name:" runat="server" ShowAstrick="true" /> --%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:DropDownList Enabled="false" AppendDataBoundItems="true" Width="183px" ID="ddlLapseEncashLeavePaysOn"
                                                runat="server" DataValueField="Key" DataTextField="Value">
                                                <asp:ListItem Text="" Value="-1"></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlLapseEncashLeavePaysOn"
                                                InitialValue="-1" Display="None" ErrorMessage="Please select lapse encash leave pays on."
                                                ValidationGroup="AECompany"></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnEdit3" Width="60" Height="22" Style="margin-top: 1px; border: 1px solid gray"
                                                runat="server" Text="Edit" OnClick="btnEdit3_Click" />
                                            <asp:Button ID="btnCancel3" Width="60" Height="22" Style="margin-top: 1px; border: 1px solid gray"
                                                runat="server" Text="Cancel" Visible="false" OnClick="btnCancel3_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td valign="top" style="padding-left: 20px">
                                <table>
                                    <tr>
                                        <td colspan="2" style="padding-top: 0px">
                                            <h3 class="headerWithUnderLine">
                                                Leave Request Settings</h3>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fieldHeader" colspan="2">
                                            <My:Label ID="Label3" Text="Allow to change past leaves after approval for (days)"
                                                runat="server" ShowAstrick="false" />
                                            <%--<My:Label ID="Label5" Text="IRO name:" runat="server" ShowAstrick="true" /> --%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="leaveApprovalPastDays" runat="server" Width="183px" Height="18"
                                                Enabled="false" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="leaveApprovalPastDays"
                                                InitialValue="-1" Display="None" ErrorMessage="Past leave approval days is required."
                                                ValidationGroup="AECompany"></asp:RequiredFieldValidator>
                                            <asp:CompareValidator ValidationGroup="AECompany" ID="reqdpastLeave" runat="server"
                                                ControlToValidate="leaveApprovalPastDays" Type="Integer" Operator="GreaterThanEqual"
                                                ValueToCompare="0" Display="None" ErrorMessage="Invalid leave approval past days, must be 0 or greater than 0." />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnEdit4" Width="60" Height="22" Style="margin-top: 1px; border: 1px solid gray"
                                                runat="server" Text="Edit" OnClick="btnEdit4_Click" />
                                            <asp:Button ID="btnCancel4" Width="60" Height="22" Style="margin-top: 1px; border: 1px solid gray"
                                                runat="server" Text="Cancel" Visible="false" OnClick="btnCancel4_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fieldHeader" colspan="2">
                                            <My:Label ID="Label6" Text="Days leaves can be requested in past" runat="server"
                                                ShowAstrick="false" />
                                            <%--<My:Label ID="Label5" Text="IRO name:" runat="server" ShowAstrick="true" /> --%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="leaveRequestPastDays" runat="server" Width="183px" Height="18" Enabled="false" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="leaveRequestPastDays"
                                                InitialValue="-1" Display="None" ErrorMessage="Past leave request days is required."
                                                ValidationGroup="AECompany"></asp:RequiredFieldValidator>
                                            <asp:CompareValidator ValidationGroup="AECompany" ID="CompareValidator2" runat="server"
                                                ControlToValidate="leaveRequestPastDays" Type="Integer" Operator="GreaterThanEqual"
                                                ValueToCompare="0" Display="None" ErrorMessage="Invalid leave request past days, must be 0 or greater than 0." />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnEdit55" Width="60" Height="22" Style="margin-top: 1px; border: 1px solid gray"
                                                runat="server" Text="Edit" OnClick="btnEdit55_Click" />
                                            <asp:Button ID="btnCancel55" Width="60" Height="22" Style="margin-top: 1px; border: 1px solid gray"
                                                runat="server" Text="Cancel" Visible="false" OnClick="btnCancel55_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fieldHeader" colspan="2">
                                            <My:Label ID="Label5" Text="Allow leave request upto (months in future)" runat="server"
                                                ShowAstrick="false" />
                                            <%--<My:Label ID="Label5" Text="IRO name:" runat="server" ShowAstrick="true" /> --%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtLeaveRequestValidMonths" runat="server" Width="183px" Height="18"
                                                Enabled="false" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtLeaveRequestValidMonths"
                                                InitialValue="-1" Display="None" ErrorMessage="Leave request valid month is required."
                                                ValidationGroup="AECompany"></asp:RequiredFieldValidator>
                                            <asp:CompareValidator ValidationGroup="AECompany" ID="CompareValidator1" runat="server"
                                                ControlToValidate="txtLeaveRequestValidMonths" Type="Integer" Operator="GreaterThanEqual"
                                                ValueToCompare="0" Display="None" ErrorMessage="Invalid leave request valid month, must be 1 or greater than 1." />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnEdit5" Width="60" Height="22" Style="margin-top: 1px; border: 1px solid gray"
                                                runat="server" Text="Edit" OnClick="btnEdit5_Click" />
                                            <asp:Button ID="btnCancel5" Width="60" Height="22" Style="margin-top: 1px; border: 1px solid gray"
                                                runat="server" Text="Cancel" Visible="false" OnClick="btnCancel5_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fieldHeader" colspan="2">
                                            <My:Label ID="Label7" Text="Allow negative/advance balance for leave in leave request"
                                                runat="server" ShowAstrick="false" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:CheckBox ID="chkAllowNegative" runat="server" Width="183px" Height="18" Enabled="false" />
                                        </td>
                                        <td>
                                            <asp:Button ID="Button1" Width="60" Height="22" Style="margin-top: 1px; border: 1px solid gray"
                                                runat="server" Text="Edit" OnClick="btnEditNegative_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fieldHeader" colspan="2">
                                            <My:Label ID="Label8" Text="Treat holiday as leave if lies in between leave for UPL and other defined leave <br/> (For other defined leave, this setting can be override from individual leave setting also)"
                                                runat="server" ShowAstrick="false" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:CheckBox ID="chkTreatHolidayAsLeave" runat="server" Width="183px" Height="18"
                                                Enabled="false" />
                                        </td>
                                        <td>
                                            <asp:Button ID="Button2" Width="60" Height="22" Style="margin-top: 1px; border: 1px solid gray"
                                                runat="server" Text="Edit" OnClick="btnEditTreatHolidy_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fieldHeader" colspan="2">
                                            <My:Label ID="Label10" Text="Treatment of encashment leaves on retirement " runat="server"
                                                ShowAstrick="false" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:DropDownList ID="ddlTreatmentOfEncashment" Enabled="false" runat="server" Width="180px">
                                                <asp:ListItem Text="Standard" Value="1" />
                                                <asp:ListItem Text="Retirement Benefit (15% TDS)" Value="2" />
                                                <asp:ListItem Text="Regular Income" Value="3" />
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:Button ID="Button4" Width="60" Height="22" Style="margin-top: 1px; border: 1px solid gray"
                                                runat="server" Text="Edit" OnClick="ddlTreatmentOfEncashment_Click" />
                                        </td>
                                    </tr>
                                    <tr style="display: none">
                                        <td class="fieldHeader" colspan="2">
                                            <My:Label ID="Label91" Text="Should be taken in order <br/> (Be sure to set order in Leave List)"
                                                runat="server" ShowAstrick="false" />
                                        </td>
                                    </tr>
                                    <tr style="display: none">
                                        <td>
                                            <asp:DropDownList ID="ddlTakenOrder" Enabled="false" runat="server" Width="180px">
                                                <asp:ListItem Text="Yes" Value="true" />
                                                <asp:ListItem Text="No" Value="false" />
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:Button ID="Button3" Width="60" Height="22" Style="margin-top: 1px; border: 1px solid gray"
                                                runat="server" Text="Edit" OnClick="btnEditTakenOrder_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fieldHeader" colspan="2">
                                            <My:Label ID="Label9" Text="Leave Request and Management Method" runat="server" ShowAstrick="false" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:DropDownList Enabled="false" AutoPostBack="true" OnSelectedIndexChanged="ddlRequestManagementMethods_Select" ID="ddlRequestManagementMethods" runat="server"
                                                Width="180px">
                                                <asp:ListItem Text="Leave Teams" Value="0" />
                                                <asp:ListItem Text="Leave Authorities" Value="1" />
                                                <asp:ListItem Text="Define Each supervisor to each Employee" Value="2" />
                                                
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnManagement" Width="60" Height="22" Style="margin-top: 1px; border: 1px solid gray"
                                                runat="server" Text="Edit" OnClick="btnManagement_Click" />
                                        </td>
                                    </tr>
                                     <tr  runat="server" id="rowSelectionOptionInLeaveApprovalForTeamOptions2" visible="false">
                                        <td class="fieldHeader" colspan="2">
                                            <My:Label ID="Label26" Text="Selection option in leave approval for Teams" runat="server"
                                                ShowAstrick="false" />
                                        </td>
                                    </tr>
                                    <tr runat="server" id="rowSelectionOptionInLeaveApprovalForTeamOptions" visible="false">
                                        <td>
                                            <asp:CheckBox ID="chkSelectionOptionInLeaveApproval" runat="server" Width="183px" Height="18" Enabled="false" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnSelectionOptionInLeaveApproval" Width="60" Height="22" Style="margin-top: 1px;
                                                border: 1px solid gray" runat="server" Text="Edit" OnClick="btnSelectionOptionInLeaveApproval_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fieldHeader" colspan="2">
                                            <My:Label ID="lblReview" Text="Multiple Review in Leave Authority Type" runat="server"
                                                ShowAstrick="false" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:CheckBox ID="chkMultipleReview" runat="server" Width="183px" Height="18" Enabled="false" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnMutlipleReview" Width="60" Height="22" Style="margin-top: 1px;
                                                border: 1px solid gray" runat="server" Text="Edit" OnClick="btnMutlipleReview_Click" />
                                        </td>
                                    </tr>
                                   
                                    <tr>
                                        <td class="fieldHeader" colspan="2">
                                            <My:Label ID="Label22" Text="Lock Leave change from Employee Portal upto" runat="server" ShowAstrick="false" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <pr:CalendarExtControl Enabled="false" runat="server" ID="calLockDate" />
                                        </td>
                                        <td>
                                           <%-- <asp:Button ID="btnLockDateEdit" Width="60" Height="22" Style="margin-top: 1px; border: 1px solid gray"
                                                runat="server" Text="Edit" OnClick="btnLockDateEdit_Click" />--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style='padding-top: 10px'>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td valign="top" style="padding-left: 20px">
                                <table>
                                    <tr>
                                        <td class="fieldHeader" colspan="2">
                                            <My:Label ID="Label11" Text="Rounding in Joining/Retirement" runat="server" ShowAstrick="false" />
                                            <%--Round Down(e.g if sick leave is 3.45 make it equal to 3, if it is 3.58 make it 3.5)--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:DropDownList ID="ddlLeaveBalanceRounding" Enabled="false" runat="server" Width="180px">
                                                <asp:ListItem Text="No Rouding" Value="1" />
                                                <asp:ListItem Text="Rounding Up with decimal checking" Value="2" />
                                                <asp:ListItem Text="Round Down" Value="3" />
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:Button ID="Button5" Width="60" Height="22" Style="margin-top: 1px; border: 1px solid gray"
                                                runat="server" Text="Edit" OnClick="ddlLeaveBalanceRounding_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fieldHeader" colspan="2">
                                            <My:Label ID="Label12" Text="Add/Edit Attendance Comment" runat="server" ShowAstrick="false" />
                                            <%--Round Down(e.g if sick leave is 3.45 make it equal to 3, if it is 3.58 make it 3.5)--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <%--<My:Label ID="Label5" Text="IRO name:" runat="server" ShowAstrick="true" /> --%>
                                            <asp:DropDownList Enabled="false" AppendDataBoundItems="true" Width="183px" ID="cmbAttCommentDays"
                                                runat="server">
                                                <asp:ListItem Text="Current Day" Value="0" Selected="True" />
                                                <asp:ListItem Text="This Week" Value="1" />
                                                <asp:ListItem Text="This Month" Value="2" />
                                                <asp:ListItem Text="Last Month" Value="3" />
                                                <asp:ListItem Text="Any Date" Value="5" />
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="cmbAttCommentDays"
                                                InitialValue="-1" Display="None" ErrorMessage="Please select One" ValidationGroup="AECompany"></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnSaveAttCommentDays" Width="60" Height="22" Style="margin-top: 1px;
                                                border: 1px solid gray" Visible="true" runat="server" Text="Edit" OnClick="btnEditAttComment_Click" />
                                            <asp:Button ID="btnCancelDays" Width="60" Height="22" Style="margin-top: 1px; border: 1px solid gray"
                                                runat="server" Text="Cancel" Visible="false" OnClick="btnCancelAttComment_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fieldHeader" colspan="2">
                                            <My:Label ID="Label13" Text="Disable Attendance Time Change" runat="server" ShowAstrick="false" />
                                            <%--Round Down(e.g if sick leave is 3.45 make it equal to 3, if it is 3.58 make it 3.5)--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <%--<My:Label ID="Label5" Text="IRO name:" runat="server" ShowAstrick="true" /> --%>
                                            <asp:DropDownList Enabled="false" AppendDataBoundItems="true" Width="183px" ID="ddlDisableToChangeAtteTime"
                                                runat="server">
                                                <asp:ListItem Text="No" Value="false" Selected="True" />
                                                <asp:ListItem Text="Yes" Value="true" />
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnTimeChange" Width="60" Height="22" Style="margin-top: 1px; border: 1px solid gray"
                                                Visible="true" runat="server" Text="Edit" OnClick="btnTimeChange_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fieldHeader" colspan="2">
                                            <My:Label ID="lblTARD" Text="Add/Edit Employee Time Attendance Request" runat="server"
                                                ShowAstrick="false" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <%--<My:Label ID="Label5" Text="IRO name:" runat="server" ShowAstrick="true" /> --%>
                                            <asp:DropDownList Enabled="false" AppendDataBoundItems="true" Width="183px" ID="ddlETARD"
                                                runat="server">
                                                <asp:ListItem Text="Any Days" Value="-1" Selected="True" />
                                                <asp:ListItem Text="Current Day" Value="0" />
                                                <asp:ListItem Text="This Week" Value="1" />
                                                <asp:ListItem Text="This Month" Value="2" />
                                                <asp:ListItem Text="Past 3 days" Value="4" />
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnETARD" Width="60" Height="22" Style="margin-top: 1px; border: 1px solid gray"
                                                Visible="true" runat="server" Text="Edit" OnClick="btnETARD_Click" />
                                            <asp:Button ID="btnETARDCancel" Width="60" Height="22" Style="margin-top: 1px; border: 1px solid gray"
                                                runat="server" Text="Cancel" Visible="false" OnClick="btnETARDCancel_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fieldHeader" colspan="2">
                                            <My:Label ID="lblMOTA" Text="Multiple Overtime Request Approval" runat="server" ShowAstrick="false" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:DropDownList Enabled="false" AppendDataBoundItems="true" Width="183px" ID="ddlMORA"
                                                runat="server">
                                                <asp:ListItem Text="No" Value="false" Selected="True" />
                                                <asp:ListItem Text="Yes" Value="true" />
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnMORA" Width="60" Height="22" Style="margin-top: 1px; border: 1px solid gray"
                                                Visible="true" runat="server" Text="Edit" OnClick="btnMORA_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fieldHeader" colspan="2">
                                            <My:Label ID="Label14" Text="Late Entry Deduction Grace Min" runat="server" ShowAstrick="false" />
                                            <%--<My:Label ID="Label5" Text="IRO name:" runat="server" ShowAstrick="true" /> --%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtLateGraceMin" Text="0" runat="server" Width="183px" Height="18"
                                                Enabled="false" />
                                            <asp:CompareValidator ValidationGroup="AECompany" ID="CompareValidator3" runat="server"
                                                ControlToValidate="txtLateGraceMin" Type="Integer" Operator="GreaterThanEqual"
                                                ValueToCompare="0" Display="None" ErrorMessage="Invalid late deduction grace min, must be 0 or greater than 0." />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnGraceMinEdit" Width="60" Height="22" Style="margin-top: 1px; border: 1px solid gray"
                                                runat="server" Text="Edit" OnClick="btnGraceMinEdit_Click" />
                                            <asp:Button ID="btnGraceMinCancel" Width="60" Height="22" Style="margin-top: 1px;
                                                border: 1px solid gray" runat="server" Text="Cancel" Visible="false" OnClick="btnGraceMinCancel_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fieldHeader" colspan="2">
                                            <My:Label ID="Label19" Text="Late Entry Display Grace Min" runat="server" ShowAstrick="false" />
                                            <%--<My:Label ID="Label5" Text="IRO name:" runat="server" ShowAstrick="true" /> --%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtLateDisplayGraceMin" Text="0" runat="server" Width="183px" Height="18"
                                                Enabled="false" />
                                            <asp:CompareValidator ValidationGroup="AECompany" ID="CompareValidator4" runat="server"
                                                ControlToValidate="txtLateDisplayGraceMin" Type="Integer" Operator="GreaterThanEqual"
                                                ValueToCompare="0" Display="None" ErrorMessage="Invalid late deduction grace min, must be 0 or greater than 0." />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnGraceMinDisplayEdit" Width="60" Height="22" Style="margin-top: 1px;
                                                border: 1px solid gray" runat="server" Text="Edit" OnClick="btnGraceMinDisplayEdit_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fieldHeader" colspan="2">
                                            <My:Label ID="Label17" Text="Status selection in Leave setting" runat="server" ShowAstrick="false" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:DropDownList Enabled="false" AppendDataBoundItems="true" Width="183px" ID="ddlStatusSelectionInLeave"
                                                runat="server">
                                                <asp:ListItem Text="Default" Value="-1" Selected="true" />
                                                <asp:ListItem Text="No" Value="false" />
                                                <asp:ListItem Text="Yes" Value="true" />
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnEditLeaveStatusSelection" Width="60" Height="22" Style="margin-top: 1px;
                                                border: 1px solid gray" runat="server" Text="Edit" OnClick="btnEditLeaveStatusSelection_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fieldHeader" colspan="2">
                                            <My:Label ID="Label18" Text="Disable Employee portal login" runat="server" ShowAstrick="false" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:DropDownList Enabled="false" AppendDataBoundItems="true" Width="183px" ID="ddlDisableEmpoyeePortal"
                                                runat="server">
                                                <asp:ListItem Text="No" Value="false" />
                                                <asp:ListItem Text="Yes" Value="true" />
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnDisableEmpPortal" Width="60" Height="22" Style="margin-top: 1px;
                                                border: 1px solid gray" runat="server" Text="Edit" OnClick="btnDisableEmpPortal_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fieldHeader" colspan="2">
                                            <My:Label ID="Label20" Text="Hide Unpaid Leave in leave request" runat="server" ShowAstrick="false" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:DropDownList Enabled="false" AppendDataBoundItems="true" Width="183px" ID="ddlHideUnapidLeave"
                                                runat="server">
                                                <asp:ListItem Text="No" Value="false" />
                                                <asp:ListItem Text="Yes" Value="true" />
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnHideUnpaidLeave" Width="60" Height="22" Style="margin-top: 1px;
                                                border: 1px solid gray" runat="server" Text="Edit" OnClick="btnHideUnpaidLeave_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fieldHeader" colspan="2">
                                            <My:Label ID="Label15" Text="Approved Leave notification type" runat="server" ShowAstrick="false" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:DropDownList AppendDataBoundItems="true" Width="183px" ID="ddlApprovedLeaveNotificationType"
                                                runat="server">
                                                <asp:ListItem Text="None" Value="-1" Selected="True" />
                                                <asp:ListItem Text="All Employee" Value="1" />
                                                <asp:ListItem Text="Branch Employee" Value="2" />
                                                <asp:ListItem Text="Department Employee" Value="3" />
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fieldHeader" colspan="2">
                                            <My:Label ID="Label16" Text="Approved Leave notification mails (separated by comma)"
                                                runat="server" ShowAstrick="false" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:TextBox ID="txtApprovedLeaveNotificationMails" Text="0" runat="server" Width="300px"
                                                Height="18" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fieldHeader" colspan="2">
                                            <My:Label ID="Label24" Text="Request Leave notification mails (separated by comma)"
                                                runat="server" ShowAstrick="false" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:TextBox ID="txtRequestLeaveNotificationMails"  runat="server" Width="300px"
                                                Height="18" />
                                        </td>
                                    </tr>


                                     <tr>
                                        <td class="fieldHeader" colspan="2">
                                            <My:Label ID="Label25" Text="Absent Email notification mails (separated by comma)"
                                                runat="server" ShowAstrick="false" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:TextBox ID="txtAbsentNotificationMails"  runat="server" Width="300px"
                                                Height="18" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fieldHeader" colspan="2">
                                            <My:Label ID="Label23" Text="Add/Edit Employee Activity" runat="server" ShowAstrick="false" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <%--<My:Label ID="Label5" Text="IRO name:" runat="server" ShowAstrick="true" /> --%>
                                            <asp:DropDownList Enabled="false" AppendDataBoundItems="true" Width="183px" ID="ddlEmpActivity"
                                                runat="server">
                                                <asp:ListItem Text="Current Day" Value="0"  />
                                                <asp:ListItem Text="Past 2 days" Value="4" Selected="True" />
                                                <asp:ListItem Text="This Week" Value="1" />
                                                <asp:ListItem Text="This Month" Value="2" />
                                                <asp:ListItem Text="Last Month" Value="3" />
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="ddlEmpActivity"
                                                InitialValue="-1" Display="None" ErrorMessage="Please select One" ValidationGroup="AECompany"></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnEmpActivity" Width="60" Height="22" Style="margin-top: 1px;
                                                border: 1px solid gray" Visible="true" runat="server" Text="Edit" OnClick="btnEmpActivity_Click" />
                                            <asp:Button ID="btnCancelEmpActivity" Width="60" Height="22" Style="margin-top: 1px; border: 1px solid gray"
                                                runat="server" Text="Cancel" Visible="false" OnClick="btnCancelEmpActivity_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <div style="clear: both">
                    </div>
                    <asp:Button ID="btnSave" CssClass="btn btn-primary btn-sm" OnClientClick="valGroup = 'AECompany';if( CheckValidation() ) { return confirm('Are you sure, you want to change the leave setting?');}"
                        runat="server" Text="Save Changes" ValidationGroup="AECompany" OnClick="btnSave_Click" />
                    <div class="rightHelp" style="display: none; float: right; width: 500px; height: 200px;">
                        <div class="title">
                            <h2>
                                Help Block</h2>
                        </div>
                        <div class="content">
                            <p>
                                &nbsp;&nbsp;
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

