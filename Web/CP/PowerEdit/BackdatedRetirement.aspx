﻿<%@ Page Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" Title="Backdated Retirement" EnableEventValidation="false"
    CodeBehind="BackdatedRetirement.aspx.cs" Inherits="Web.CP.PowerEdit.BackdatedRetirement" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .GridRowHover
        {
            cursor: pointer;
        }
        .empName
        {
            font-size: 20px;
            color: #439EC6;
            font-weight: bold;
        }
        .titleDesign
        {
            font-size: 15px;
            color: #000000;
            text-decoration: underline;
            padding-top: 10px;
        }
        
        .items span
        {
            display: block;
            padding-top: 3px;
            font-weight: bold;
        }
        .selected td
        {
            background-color: #E0EDF9 !important;
        }
        form strong
        {
            display: block !important;
            padding-bottom: 3px;
        }
        .fieldTable > tbody > tr > td
        {
            padding-left: 15px !important;
            padding-top: 15px;
        }
        .total
        {
            border-top: 1px solid black;
            border-bottom: 4px double black;
            margin-top: 9px;
            padding: 5px;
            font-weight: bold;
            font-size: 15px;
        }
        
        .total .totalAmount
        {
            float: right;
        }
        .netPayTotal
        {
            width: 396px;
            margin-left: 570px;
        }
    </style>
    <style type="text/css">
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
        .hideLeftBlockCssInPage
        {
            padding-left: 10px !important;
        }
        .widget .widget-body
        {
            padding-right: 10px !important;
        }
    </style>
    <script type="text/javascript">
        function openCalculationPopup(ein) {
            if (typeof (ein) == 'undefined')
                ein = 0;
            window.open('../CP/retirementcalculation.aspx?EmployeeID=' + ein, 'Retirement Saving', 'height=600,width=1200');
        }

        function refreshPage() {
            __doPostBack('Reload', '');
        }

        function ACE_item_selected1(source, eventArgs) {
            var value = eventArgs.get_value();
            document.getElementById('<%=hiddenEmpID.ClientID %>').value = value;
            //window.location = "Employee.aspx?Id=" + value; 
            //alert(value);
            //document.getElementById('<%= txtEmpSearchText.ClientID%>').value = eventArgs.get_text();


        }
    </script>
    <link href="../../css/core.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <ext:ResourceManager ID="ResourceManager1" runat="server" DisableViewState="false" />
    <div class="contentpanel">
        <h4 style="padding-left: 0px; margin-left: 0px; padding-bottom: 10px; margin-top: 10px;"
            id="header" runat="server">
            Backdated Employee Retirement or Change Retirement date before :
        </h4>
        <asp:HiddenField ID="hiddenEmpID" runat="server" />
        <div class="contentArea">
            Note : Use the page to set retirement for past month, for current or future use
            as normal
            <div class="attribute" style="margin-top: 10px">
                <div class="left" style="padding-left: 10px;">
                    <strong>Employee</strong>
                    <asp:TextBox AutoCompleteType="None" ID="txtEmpSearchText" AutoPostBack="true" OnTextChanged="LoadEmployeeDetails"
                        Style='width: 200px;' runat="server"></asp:TextBox>
                    <cc1:TextBoxWatermarkExtender ID="TBWE2" runat="server" TargetControlID="txtEmpSearchText"
                        WatermarkText="Employee Search" WatermarkCssClass="searchBoxText" />
                    <cc1:AutoCompleteExtender EnableCaching="true" ID="AutoCompleteExtenderOrganization"
                        runat="server" MinimumPrefixLength="2" ServiceMethod="GetEmployeeNamesWithRetiredAndID"
                        ServicePath="~/PayrollService.asmx" OnClientItemSelected="ACE_item_selected1"
                        TargetControlID="txtEmpSearchText" CompletionSetCount="10" CompletionInterval="250"
                        CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList"
                        CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
                    </cc1:AutoCompleteExtender>
                </div>
            </div>
            <div style="clear: both" />
            <uc2:MsgCtl ID="divMsgCtl" Width="900px" EnableViewState="false" Hide="true" runat="server" />
            <uc2:WarningCtl ID="divWarningMsg" Width="900px" EnableViewState="false" Hide="true"
                runat="server" />
            <table>
                <tr>
                    <td>
                        <table style='clear: both; padding-bottom: 10px; margin-top: 10px;'>
                            <tr>
                                <td valign="top" style="padding-left: 5px;">
                                    <asp:Image ID="image" runat="server" ImageUrl="~/images/sample.jpg" Width="150px"
                                        Height="150px" />
                                </td>
                                <td valign="top" style="padding-left: 40px" class="items">
                                    <asp:Label ID="lblName" CssClass="empName" runat="server" />
                                    <asp:Label ID="lblBranch" runat="server" />
                                    <asp:Label ID="lblDepartment" runat="server" />
                                    <asp:Label ID="lblSince" runat="server" />
                                    <asp:Label ID="lblTime" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" valign="top" style="padding-left: 0px" class="items">
                                    <table class="fieldTable firsttdskip" style="margin-left: -15px;">
                                        <tr>
                                            <td>
                                                <strong style="padding-bottom: 0px; margin-top: -3px;">Retirement Date</strong>
                                                <pr:CalendarExtControl Width="150px" ID="calRetirementDate" runat="server" LabelSeparator="" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="calRetirementDate"
                                                    Display="None" ErrorMessage="Please choose a Retirement Date." ValidationGroup="AEEmployee"></asp:RequiredFieldValidator>
                                            </td>
                                            <td valign="bottom">
                                                <strong style="padding-bottom: 0px; margin-top: -3px;">Retirement Type</strong>
                                                <asp:DropDownList ID="ddlRetirementType" AppendDataBoundItems="true" DataTextField="Name"
                                                    DataValueField="EventID" runat="server" Width="170px">
                                                    <asp:ListItem Text="--Select Type--" Value="-1" Selected="True" />
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator InitialValue="-1" ID="reqdRetirementTypes" runat="server"
                                                    ControlToValidate="ddlRetirementType" Display="None" ErrorMessage="Retirement type is required."
                                                    ValidationGroup="AEEmployee"></asp:RequiredFieldValidator>
                                            </td>
                                            <td valign="bottom">
                                                <asp:Button ID="btnSetRetirement" CssClass="save" runat="server" Width="120px" Text="Set Retirement"
                                                    OnClientClick="if(confirm('Are you sure to retire this employee in the past month?')) {valGroup='AEEmployee';return CheckValidation();} else return false;"
                                                    OnClick="btnSetRetirement_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <div style="clear: both">
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
