﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using Utils.Helper;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Calendar;
using System.IO;
using Utils;
using System.Text;
using BLL.BO;

namespace Web.CP.PowerEdit
{
    public partial class BackdatedRetirement : BasePage
    {
        BranchManager branchMgr = new BranchManager();
        EmployeeManager empMgr = new EmployeeManager();
        InsuranceManager insMgr = new InsuranceManager();
        CommonManager cmpMgr = new CommonManager();
        List<BranchDepartmentHistory> list = new List<BranchDepartmentHistory>();
        List<PIncome> variableIncomeList = new List<PIncome>();
        protected void Page_Load(object sender, EventArgs e)
        {

            if (SessionManager.User.URole.RoleId != (int)Role.Administrator)
            {
                Response.Write("Not enough permission.");
                Response.End();
                return;
            }

            variableIncomeList = PayManager.GetVariableIncomeList(SessionManager.CurrentCompanyId, false);

            if (!IsPostBack)
            {
                PayrollPeriod period = CommonManager.GetLastPayrollPeriod();
                if (period != null)
                {
                    header.InnerHtml += period.Name + " (" + period.StartDateEng.Value.ToString("dd MMM yyyy")
                        + ")";
                }


                Initialise();
            }

            string eventTarget = Request.Form["__EVENTTARGET"];
         
            if (eventTarget != null && eventTarget.Equals("Reload"))
            {
                LoadEmployeeDetails(null, null);
            }

            ResourceManager1.RegisterClientInitScript("Date1", string.Format("isEnglish = {0};",this.IsEnglish.ToString().ToLower()));



            ResourceManager1.RegisterClientInitScript("Date2", string.Format("todayDate = '{0}';function calendarOnClick(event, sourceElement, e3){{}};",
                                                                      CustomDate.GetTodayDate(this.IsEnglish).ToString()));

        }

        void LoadRetiringThisMonth()
        {
        

        }


        void Initialise()
        {

            List<HR_Service_Event> list = CommonManager.GetServieEventTypes();

            ddlRetirementType.DataSource = list;
            ddlRetirementType.DataBind();


            LoadBranches();
            LoadRetiringThisMonth();

        }



        void LoadBranches()
        {
           // ddlFilterByBranch.DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
           // ddlFilterByBranch.DataBind();

         

        
            //if( ddlFilterByBranch.Items.Count >0)
            //{
            //    ddlFilterByBranch.Items[1].Selected = true;
            //    LoadEmployeesByBranch();
            //}
            LoadEmployeesByBranch();
        }

        protected void LoadEmployees(object sender, EventArgs e)
        {
            LoadEmployeesByBranch();
        }

        void LoadEmployeesByBranch()
        {
           
        }

       


        protected void ddlFilterByValues_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadEmployees();
        }
        void LoadEmployees()
        {
            string type = string.Empty; int id = 0;

        }


        public void LoadEmployeeDetails(object sender, EventArgs e)
        {
          



            int employeeId = 0;

            if (int.TryParse(hiddenEmpID.Value, out employeeId) == false)
            {
                divWarningMsg.InnerHtml = "Please select an employee.";
                divWarningMsg.Hide = false;
                return;
            }
            
            employeeId = int.Parse(hiddenEmpID.Value);


            PayrollPeriod payrollPeriod = CommonManager.GetLastPayrollPeriod();

           
            EEmployee emp = EmployeeManager.GetEmployeeById(employeeId);

            if (emp == null)
            {
              
                divWarningMsg.InnerHtml = "Please select an employee.";
                divWarningMsg.Hide = false;
                return;
            }
                

            

            EHumanResource hr = emp.EHumanResources[0];
            

            DisplayEmployeeHRDetails(emp);

            
            // If salary saved then only show amount details
            if (payrollPeriod != null &&
                CalculationManager.IsCalculationSavedForEmployee(payrollPeriod.PayrollPeriodId, employeeId))
            {
                divMsgCtl.InnerHtml = "Salary already saved for the employee, please delete the salary for any changes.";
                divMsgCtl.Hide = false;

                //btnSetRetirement.Visible = false;
              
            }
            else
            {
               // btnSetRetirement.Visible = true;
              

                
            }



            calRetirementDate.Text = "";

            // check for already past retired emp
            if( (emp.IsRetired != null && emp.IsRetired.Value)
                ||
                 (emp.IsResigned != null && emp.IsResigned.Value)
                )
            {
                if (emp.EHumanResources[0].DateOfRetirementEng != null)
                {
                    divWarningMsg.InnerHtml = "Retired : " + hr.DateOfRetirement + ".";

                    calRetirementDate.Text = hr.DateOfRetirement;
                }
                if (emp.EHumanResources[0].DateOfResignationEng != null)
                {
                    divWarningMsg.InnerHtml = "Resigned : " + hr.DateOfResignation + ".";
                    calRetirementDate.Text = hr.DateOfResignation;
                }

                divWarningMsg.Hide = false;

                //btnSetRetirement.Visible = false;
                
            }

        }

        private void DisplayEmployeeHRDetails(EEmployee emp)
        {
            // Photo
            bool hasPhoto = false;
            if (emp.HHumanResources.Count > 0)
            {
                if (!string.IsNullOrEmpty(emp.HHumanResources[0].UrlPhoto))
                {
                    image.ImageUrl = Page.ResolveUrl(Config.UploadLocation + "/" + emp.HHumanResources[0].UrlPhoto);
                    if (File.Exists(Server.MapPath(image.ImageUrl)))
                        hasPhoto = true;
                }
            }
            //if (!hasPhoto)
            //{
            //    image.ImageUrl = Page.ResolveUrl(Web.Helper.WebHelper.GetEmployeeNoImage(0, emp));
            //}

            lblName.Text = emp.EmployeeId + " - " +  emp.Title + " " + emp.Name;
            lblBranch.Text = emp.Branch.Name;
            lblDepartment.Text = emp.Department.Name;

            int years, months, days, hours;
            int minutes, seconds, milliseconds;
            ServicePeroid firstStatus = EmployeeManager.GetServicePeroidDate(emp.EmployeeId);
            GetElapsedTime(firstStatus.FromDateEng, BLL.BaseBiz.GetCurrentDateAndTime(), out years, out months, out days, out hours, out minutes, out seconds, out milliseconds);

            lblSince.Text = "Since " +
                (IsEnglish ? firstStatus.FromDateEng.ToShortDateString() : firstStatus.FromDate + " (" + firstStatus.FromDateEng.ToShortDateString() + ")");
            lblTime.Text = string.Format("For {0} years {1} months {2} days",
                years, months, days);
        }

        // Return the number of years, months, days, hours, minutes, seconds,
        // and milliseconds you need to add to from_date to get to_date.
        private void GetElapsedTime(DateTime from_date, DateTime to_date,
            out int years, out int months, out int days, out int hours,
            out int minutes, out int seconds, out int milliseconds)
        {
            // If from_date > to_date, switch them around.
            if (from_date > to_date)
            {
                GetElapsedTime(to_date, from_date,
                    out years, out months, out days, out hours,
                    out minutes, out seconds, out milliseconds);
                years = -years;
                months = -months;
                days = -days;
                hours = -hours;
                minutes = -minutes;
                seconds = -seconds;
                milliseconds = -milliseconds;
            }
            else
            {
                // Handle the years.
                years = to_date.Year - from_date.Year;

                // See if we went too far.
                DateTime test_date = from_date.AddMonths(12 * years);
                if (test_date > to_date)
                {
                    years--;
                    test_date = from_date.AddMonths(12 * years);
                }

                // Add months until we go too far.
                months = 0;
                while (test_date <= to_date)
                {
                    months++;
                    test_date = from_date.AddMonths(12 * years + months);
                }
                months--;

                // Subtract to see how many more days,
                // hours, minutes, etc. we need.
                from_date = from_date.AddMonths(12 * years + months);
                TimeSpan remainder = to_date - from_date;
                days = remainder.Days;
                hours = remainder.Hours;
                minutes = remainder.Minutes;
                seconds = remainder.Seconds;
                milliseconds = remainder.Milliseconds;
            }
        }

        public int GetEmployeeId()
        {
            return int.Parse(hiddenEmpID.Value);
        }
      


        protected void btnCancel_Click(object sender, EventArgs e)
        {
            //btnSave.Text = "Save";

            //int employeeId = int.Parse(ddlEmployeeList.SelectedValue);

            //if (employeeId != -1)
            //{

            //    gvw.SelectedIndex = -1;
            //    CommonManager.SaveFirstIBranchfNotExists(employeeId);
            //    BindHistory(employeeId);

            //    ClearFields();
            //}
        }

        void ClearFields()
        {
            //UIHelper.SetSelectedInDropDown(ddlTransferToBranch, -1);
            //UIHelper.SetSelectedInDropDown(ddlTransferToDepartment, -1);
            //txtNote.Text = "";
            //calFromDate.Text = "";  
            //calDepartureDate.Text = "";
            //calLetterDate.Text = "";
            //calFromDate.Enabled = true;
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
           
        }

        /// <summary>
        /// Button is hidden as for internal use only
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnDelete_Click(object sender, EventArgs e)
        {
        }

        public bool IsEditable(object typeE, object sourceIdE)
        {
            int type = int.Parse(typeE.ToString());
            int source = int.Parse(sourceIdE.ToString());
            PayrollPeriod payrollPeriod = CommonManager.GetLastPayrollPeriod();
            List<KeyValue> list = PayManager.GetEmployeesIncomeDeductionList(payrollPeriod.PayrollPeriodId);

            //income deduction that comes in Income Adjustment are editable
            foreach (KeyValue item in list)
            {
                // disable adjsutment to income pf
                //if (source + ":" + type == ((int)CalculationColumnType.IncomePF + ":" + (int)CalculationColumnType.IncomePF))
                //    return false;

                if (item.Value == source + ":" + type)
                {
                    return true;
                }
            }

            if(type==1 && variableIncomeList.Any(x=>x.IncomeId==source))
                return true;

            return false;
        }

      
       
        protected void btnSetRetirement_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(calRetirementDate.Text.Trim()))
            {
                NewMessage.ShowNormalMessage("Retirement Date is required");
                return;
            }

            if (string.IsNullOrEmpty(txtEmpSearchText.Text.Trim()))
            {
                NewMessage.ShowNormalMessage("Employee is required");
                return;
            }

            int employeeId = GetEmployeeId();
            



            bool isRetirement,isResigned;

            //if (rdbIsRetired.Checked)
            //{
                isRetirement = true;
                isResigned = false;
            //}
            //else
            //{
            //    isRetirement = false;
            //    isResigned = true;
            //}
            string retirementdate = calRetirementDate.Text.Trim();
            DateTime retirementDateEng = GetEngDate(retirementdate);

            

            

            //EmployeeManager.SetRetirement(employeeId, retirementdate, retirementDateEng, isRetirement, isResigned);

          
            //btnSetRetirement.Visible = false;

            EmployeeManager.SetPastRetirement(employeeId, retirementdate, retirementDateEng
                ,int.Parse(ddlRetirementType.SelectedValue));


            divMsgCtl.InnerHtml = "Retirement date set.";
            divMsgCtl.Hide = false;

        }

     

        public List<CalculationValue> AddOtherIncome(List<CalculationValue> incomeList)
        {

            return incomeList;
        }
    }
}

