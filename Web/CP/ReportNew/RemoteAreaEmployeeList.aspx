﻿<%@ Page Title="Remote Area Listing" Language="C#" MasterPageFile="~/Master/NewDetails.master"
    AutoEventWireup="true" CodeBehind="RemoteAreaEmployeeList.aspx.cs" Inherits="Web.NewHR.RemoteAreaEmployeeList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
        #content
        {
            margin: inherit;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <ext:Hidden runat="server" ID="hdn">
    </ext:Hidden>
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Current Remote Area Employee List
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel" style="padding-top:0px">
        <ext:Label ID="lblMsg" runat="server" />
      
        <div class="buttonBlock" style="padding-top:0px">
            <div class="left">
                <ext:LinkButton runat="server" AutoPostBack="true" OnClick="btnExport_Click" StyleSpec="padding:0px;margin-bottom:0px;"
                    ID="btnAdd" Cls="btnFlat" BaseCls="btnFlat" Text="<i></i>Export" runat="server">
                </ext:LinkButton>
            </div>
        </div>
        <div style="clear: both">
        </div>
        <ext:GridPanel StyleSpec="margin-top:25px;" ID="gridList" runat="server" Cls="itemgrid"
            Width="1000">
            <Store>
                <ext:Store ID="gridAppraisalStore" PageSize="50" runat="server">
                    <Model>
                        <ext:Model Root="data" runat="server">
                            <Fields>
                                <ext:ModelField Name="Branch" Type="string" />
                                <ext:ModelField Name="Name" Type="string" />
                                <ext:ModelField Name="VDC" Type="string" />
                                <ext:ModelField Name="RemoteAreaGroup" Type="string" />
                                <ext:ModelField Name="Level" Type="string" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>
            <ColumnModel>
                <Columns>
                    <ext:Column ID="Column_Employee" Sortable="true" runat="server" Text="Branch" Align="Left"
                        Width="150" DataIndex="Branch" Hideable="false" />
                    <ext:Column ID="Column1" Sortable="true" runat="server" Text="Employee" Align="Left"
                        Width="200" DataIndex="Name" Hideable="false" />
                    <ext:Column ID="Column2" Sortable="true" runat="server" Text="VDC" Align="Left" Width="100"
                        DataIndex="VDC" Hideable="false" />
                    <ext:Column ID="Column3" Sortable="true" runat="server" Text="Remote Area Group"
                        Align="Left" Width="150" DataIndex="RemoteAreaGroup" Hideable="false" />
                    <ext:Column ID="Column4" Sortable="true" runat="server" Text="Level" Align="Left"
                        Width="150" DataIndex="Level" Hideable="false" />
                </Columns>
            </ColumnModel>
            <View>
                <ext:GridView ID="GridView1" runat="server">
                    <%--<GetRowClass Fn="getRowClass" />--%>
                </ext:GridView>
            </View>
            <SelectionModel>
                <ext:RowSelectionModel ID="RowSelectionModel2111" runat="server" Mode="Single" />
            </SelectionModel>
            <%--    <BottomBar>
            <ext:PagingToolbar PageSize="50" ID="PagingToolbar1" DisplayInfo="true" DisplayMsg="Displaying record {0} - {1} of {2}"
                EmptyMsg="No record to display" runat="server">
            </ext:PagingToolbar>
        </BottomBar>--%>
        </ext:GridPanel>
        <div style="clear: both">
        </div>
    </div>
</asp:Content>
