﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using Utils.Calendar;
using Utils.Helper;
using Bll;

namespace Web.NewHR
{
    public partial class RemoteAreaEmployeeList : BasePage
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMsg.Text = "";
            if (!X.IsAjaxRequest)
            {
                Initialise();

            }
        }

        public void Initialise()
        {
            LoadData();
        }

        protected void LoadData()
        {
            List<Report_GetRemoteAreaEmployeeListResult> _ListAppraisalForm = ReportManager.GetRemoteAreaList();
            gridAppraisalStore.DataSource = _ListAppraisalForm;
            gridAppraisalStore.DataBind();

        }


        public void btnExport_Click(object sender, EventArgs e)
        {
            
            List<Report_GetRemoteAreaEmployeeListResult> _ListAppraisalForm = ReportManager.GetRemoteAreaList();

            Dictionary<string, string> title = new Dictionary<string, string>();
            title["Remote Area Listing"] = "";



            ExcelHelper.ExportToExcel(
                           "Remote Area Listing",
                           _ListAppraisalForm, new List<string> { }, new List<string> { }, new Dictionary<string, string> { }
                           , new List<string> { },title
                           );
        }

       
    }
}