﻿<%@ Page Title="Allowance Settings" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="AllowanceSettings.aspx.cs" Inherits="Web.CP.AllowanceSettings" %>

<%--<%@ Register Src="~/NewHR/UserControls/EmployeeDetailsCtl.ascx" TagName="EmployeeDetailsCtl"
    TagPrefix="uc1" %>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        var CommandHandler1 = function(command, record){
            <%= hiddenValue.ClientID %>.setValue(record.data.AllowanceID);
                if(command=="Edit")
                {
                    <%= btnEditLevel.ClientID %>.fireEvent('click');
                }
                else if(command=="Detail")
                {
                     <%= btnDetailLevel.ClientID %>.fireEvent('click');
                }
                else
                {
                
                    <%= btnDeleteLevel.ClientID %>.fireEvent('click');
                }

             }

            function refreshWindow()
            {
                <%= btnDetailLevel.ClientID %>.fireEvent('click');
                
            }

            function isPayrollSelected(btn) {
                if ($('#' + btn.id).prop('disabled'))
                    return false;
       

                var ret = allowancePopup("ID=" + <%=hiddenValue.ClientID %>.getValue());
                return false;
            }
             
    </script>
    <style type="text/css">
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Travel Allowance Settings
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel" style="width:958px">
    <ext:Hidden runat="server" ID="hiddenValue" />
    <ext:Button Cls="btn btn-primary" runat="server" Hidden="true" ID="btnEditLevel" runat="server">
        <DirectEvents>
            <Click OnEvent="btnEditLevel_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>
    <ext:Button Cls="btn btn-primary" runat="server" Hidden="true" ID="btnDetailLevel" runat="server">
        <DirectEvents>
            <Click OnEvent="btnDetailLevel_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>
    <ext:Button Cls="btn btn-primary" ID="btnDeleteLevel" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnDeleteLevel_Click">
                <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the Allowance settings?" />
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>
    <div class="separator bottom">
    </div>
    <div class="innerLR">
        <%--<uc1:EmployeeDetailsCtl Id="EmployeeDetailsCtl1" runat="server" />--%>
        <div class="widget">


            <div class="widget-body">
                <ext:GridPanel StyleSpec="margin-top:15px;" ID="GridLevels" runat="server" Cls="itemgrid">
                    <Store>
                        <ext:Store ID="Store3" runat="server">
                            <Model>
                                <ext:Model ID="Model4" runat="server" IDProperty="AllowanceID">
                                    <Fields>
                                        <ext:ModelField Name="AllowanceID" Type="String" />
                                        <ext:ModelField Name="Name" Type="string" />
                                        <ext:ModelField Name="Description" Type="string" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                    </Store>
                    <ColumnModel>
                        <Columns>
                            <ext:Column ID="Column5" Sortable="false" MenuDisabled="true" runat="server" Text="Allowance Name"
                                Width="300" Align="Left" DataIndex="Name" />
                            <ext:Column ID="Column6" Sortable="false" MenuDisabled="true" runat="server" Text="Description"
                                Width="500" Align="Left" DataIndex="Description">
                            </ext:Column>
                            <ext:CommandColumn runat="server" Text="" Align="Center" Width="125">
                                <Commands>
                                    <ext:GridCommand Text="<i></i>" Icon="pencil" CommandName="Edit" />
                                    <ext:GridCommand Text="<i></i>" Icon="Cancel" CommandName="Delete" />
                                    <ext:GridCommand Text="<i></i>" Icon="MoneyAdd" CommandName="Detail" />
                                </Commands>
                                <Listeners>
                                    <Command Handler="CommandHandler1(command,record);" />
                                </Listeners>
                            </ext:CommandColumn>
                        </Columns>
                    </ColumnModel>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                    </SelectionModel>
                </ext:GridPanel>
                <div class="buttonBlock">
                    <ext:Button Cls="btn btn-primary" runat="server" ID="btnAddLevel"  Text="<i></i>Add New Allowance" >
                        <DirectEvents>
                            <Click OnEvent="btnAddLevel_Click">
                                <EventMask ShowMask="true" />
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </div>
            </div>
        </div>
        <ext:Window ID="WindowLevel" runat="server" Title="Allowance Settings" Icon="Application"
            Width="475" Height="350" BodyPadding="5" Hidden="true" Modal="true">
            <Content>
                <table class="fieldTable">
                    <tr>
                        <td>
                            <ext:TextField ID="txtName" FieldLabel="Name" runat="server" LabelAlign="Top" Width="180"
                                LabelSeparator="">
                            </ext:TextField>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ext:Checkbox ID="chkIsEditable" FieldLabel="Allow user to change amount" runat="server" LabelAlign="Top" Width="180"
                                LabelSeparator="">
                            </ext:Checkbox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <ext:TextArea ID="txtDescription" runat="server" FieldLabel="Description" LabelAlign="Top"
                                Width="425">
                            </ext:TextArea>
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom" colspan="2">
                            <div class="popupButtonDiv">
                                <ext:Button Cls="btn btn-primary" runat="server"  ID="btnLevelSaveUpdate"  Text="<i></i>Save">
                                    <DirectEvents>
                                        <Click OnEvent="btnLevelSaveUpdate_Click">
                                            <EventMask ShowMask="true" />
                                        </Click>
                                    </DirectEvents>
                                    <Listeners>
                                        <Click Handler="valGroup = 'SaveUpdateLevel'; return CheckValidation();">
                                        </Click>
                                    </Listeners>
                                </ext:Button>
                                <div class="btnFlatOr">
                                    or</div>
                                <ext:LinkButton runat="server" StyleSpec="padding:0px;" Cls="btnFlatLeftGap" ID="LinkButton1"
                                    Text="<i></i>Cancel" >
                                    <Listeners>
                                        <Click Handler="#{WindowLevel}.hide();">
                                        </Click>
                                    </Listeners>
                                </ext:LinkButton>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtName"
                                    ValidationGroup="SaveUpdateLevel">
                                </asp:RequiredFieldValidator>
                            </div>
                        </td>
                    </tr>
                </table>
            </Content>
        </ext:Window>
        <ext:Window ID="Window1" AutoScroll="true" runat="server" Title="Travel Allowances"
            Icon="Application" Width="900" Layout="BorderLayout" Height="600" BodyPadding="1"
            Hidden="true" Modal="true">
            <Items>
                <ext:GridPanel ID="gridAllowanceRates" runat="server" Cls="itemgrid" Region="Center"
                    ClicksToEdit="1">
                    <Store>
                        <ext:Store ID="storeAllowanceDetail" runat="server" AutoLoad="true">
                            <Reader>
                                <ext:ArrayReader>
                                </ext:ArrayReader>
                            </Reader>
                            <Model>
                                <ext:Model ID="Model1" runat="server">
                                    <Fields>
                                        <%-- <ext:ModelField Name="LevelID" Type="string" />--%>
                                        <ext:ModelField Name="LevelName" Type="string" />
                                        <%--   <ext:ModelField Name="NepalNPR" Type="Float" />
                                                    <ext:ModelField Name="IndiaINR" Type="Float" />
                                                    <ext:ModelField Name="SaarcUSD" Type="Float" />
                                                    <ext:ModelField Name="OtherUSD" Type="Float" />--%>
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                    </Store>
                    <Plugins>
                        <ext:CellEditing ID="CellEditing1" ClicksToEdit="1" runat="server">
                            <Listeners>
                            </Listeners>
                        </ext:CellEditing>
                    </Plugins>
                    <ColumnModel>
                        <Columns>
                            <ext:Column ID="Column1" Locked="true" TdCls="wrap" runat="server" Text="Level" Sortable="false"
                                MenuDisabled="true" Width="200" DataIndex="LevelName" Border="false">
                            </ext:Column>
                        </Columns>
                    </ColumnModel>
                    <SelectionModel>
                        <ext:CellSelectionModel ID="CellSelectionModel1" runat="server" Mode="Single" />
                    </SelectionModel>
                </ext:GridPanel>
                <ext:Container runat="server" Region="South" Height="60">
                    <Content>
                        <table class="fieldTable">
                            <tr>
                                <td>
                                    <asp:Button Cls="btn btn-primary" ID="btnExport"  runat="server" Text="Import from Excel" OnClientClick="return isPayrollSelected(this)" />
                                </td>
                                <td>
                                </td>
                                <td valign="bottom">
                                    <ext:Button Cls="btn btn-primary" runat="server"  ID="LinkButton3"
                                        Text="<i></i>Cancel">
                                        <Listeners>
                                            <Click Handler="#{Window1}.hide();">
                                            </Click>
                                        </Listeners>
                                    </ext:Button>
                               </td>
                            </tr>
                        </table>
                    </Content>
                </ext:Container>
            </Items>
        </ext:Window>
    </div>
    </div>
    <br />
</asp:Content>
