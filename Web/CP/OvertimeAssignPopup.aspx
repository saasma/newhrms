﻿<%@ Page Language="C#" Title="Overtime Assign" AutoEventWireup="true" MasterPageFile="~/Master/ForEmployeePopupPage.Master"
    CodeBehind="OvertimeAssignPopup.aspx.cs" Inherits="Web.Employee.OvertimeAssignPopup" %>


<%@ Register src="~/Employee/UserControls/OvertimeRequestApproveCtl.ascx" tagname="OvertimeRequestApproveCtl" tagprefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
   
    <uc1:OvertimeRequestApproveCtl ID="OvertimeRequestApproveCtl1" AssignOvertimeFromAdmin="true" runat="server" />
   
</asp:Content>
