﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using DAL;
using BLL;
using Utils.Helper;
using Utils;

namespace Web.CP
{
    public partial class Service : BasePage
    {
        CommonManager commonMgr = new CommonManager();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialise();
            }
        }

        void Initialise()
        {
            valRegPhone.ValidationExpression = Config.PhoneRegExp;
            valRegFax.ValidationExpression = Config.FaxRegExp;

            ServiceProvider service = commonMgr.GetServiceProvider();
            if (service != null)
            {
                this.CustomId = service.Id;
                Process(service);
                btnSave.Text = Resources.Messages.Update;
            }
        }


        ServiceProvider Process(ServiceProvider entity)
        {
            if (entity == null)
            {
                entity = new ServiceProvider();
                entity.CompanyName = txtCompanyName.Text.Trim();
                entity.Address = txtAddress.Text.Trim();
                entity.VatPanNo = txtVatPanNo.Text.Trim();
                entity.PhoneNo = txtPhoneNo.Text.Trim();
                entity.FaxNo = txtFaxNo.Text.Trim();
                entity.Email = txtEmail.Text.Trim();
                entity.Website = txtWebsite.Text.Trim();
                return entity;
            }
            else
            {
                txtCompanyName.Text = entity.CompanyName;
                txtAddress.Text = entity.Address;
                txtVatPanNo.Text = entity.VatPanNo;
                txtPhoneNo.Text = entity.PhoneNo;
                txtFaxNo.Text = entity.FaxNo;
                txtEmail.Text = entity.Email;
                txtWebsite.Text = entity.Website;
            }
            return null;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                ServiceProvider entity = Process(null);
                if (this.CustomId == 0)
                {
                    commonMgr.Save(entity);
                    JavascriptHelper.DisplayClientMsg("Service provider information saved.", Page);
                    btnSave.Text = Resources.Messages.Update;
                }
                else
                {
                    entity.Id = this.CustomId;
                    commonMgr.Save(entity);
                    JavascriptHelper.DisplayClientMsg("Service provider information updated.", Page);
                }
            }
        }
    }
}
