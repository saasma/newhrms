﻿<%@ Page Title="Branch Transfer list" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="BranchTransferNew.aspx.cs" Inherits="Web.CP.BranchTransferNew" %>

    
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningMsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="InfoMsgCtl" TagPrefix="uc2" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function medicalTaxCreditPopupCall(medicalTaxId) {
            var ret = medicalTaxCreditPopup("Id=" + medicalTaxId);
            if (ret == "Yes")
                __doPostBack('Reload', '');
        }

        function parentReloadCallbackFunction(childWindow, ret) {
            childWindow.close();
            if (ret == "Yes")
                __doPostBack('Reload', '');

        }

        function refreshWindow() {
            __doPostBack('Reload', '');
        }

        function popupAddBrTransferCall() {

            var ret = popupBrTransferDetails();

            if (typeof (ret) != 'undefined') {
                if (ret == 'Reload') {
                    __doPostBack('Reload', '');
                }
            }

            return false;
        }

        function reloadBrTransfer(childWindow) {

            childWindow.close();
            __doPostBack('Reload', '');
        }

        function popupUpdateBrTransferCall(BranchDepartmentId) {
            var ret = popupBrTransferDetails('Id=' + BranchDepartmentId);

            if (typeof (ret) != 'undefined') {
                if (ret == 'Reload') {
                    //refreshIncomeList(0);
                    __doPostBack('Reload', '');
                }
            }
            return false;
        }

        function popupUpdateLeaveCall(BranchDepartmentId) {
            var ret = popup('Id=' + BranchDepartmentId);


            return false;
        }


     

    </script>
    <style type="text/css">
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
    </style>
    <link href="../css/core.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Branch Transfer List
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="contentArea" style="margin-top:0px;">
            <%--to generate __PostBack function--%>
            <asp:LinkButton runat="server" Visible="false"></asp:LinkButton>
            <div class="attribute" style="padding:10px">
                <table>
                    <tr>
                        <td>
                            <strong>Search </strong>&nbsp;
                            <asp:TextBox Width="160px" ID="txtEmpSearchText" runat="server" OnTextChanged="txtEmpSearchText_TextChanged"></asp:TextBox>
                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtEmpSearchText"
                                WatermarkText="Employee Search" WatermarkCssClass="searchBoxText" />
                            <cc1:AutoCompleteExtender EnableCaching="true" ID="AutoCompleteExtenderOrganization"
                                runat="server" MinimumPrefixLength="2" ServiceMethod="GetEmployeeNames" ServicePath="~/PayrollService.asmx"
                                TargetControlID="txtEmpSearchText" CompletionSetCount="10" CompletionInterval="250"
                                CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList"
                                CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
                            </cc1:AutoCompleteExtender>
                        </td>
                        <td>
                            <asp:Button ID="btnLoad" CssClass="btn btn-default btn-sm" Style="margin-left: 10px; width: 100px;" runat="server"
                                OnClick="btnLoad_Click" Text="Load" />
                            <cc1:TextBoxWatermarkExtender ID="TBWE2" runat="server" TargetControlID="txtEmpSearchText"
                                WatermarkText="Employee name" WatermarkCssClass="searchBoxText" />
                        </td>
                     
                    </tr>
                </table>
            </div>
            <div style="clear: both">
            </div>
          
          <uc2:InfoMsgCtl ID="msgInfo" EnableViewState="false" Hide="true" runat="server" />
        <uc2:WarningMsgCtl ID="msgWarning" EnableViewState="false" Hide="true" runat="server" />

            <asp:Button ID="btnSave" Width="120px" CausesValidation="false" CssClass="btn btn-primary btn-sm" runat="server"
                Text="New Transfer" OnClientClick="return popupAddBrTransferCall();"></asp:Button>
            <div class="clear" style="width: 1168px; margin-top: 10px;">
                <cc2:EmptyDisplayGridView CssClass="tableLightColor" UseAccessibleHeader="true" ID="gvwList"
                    runat="server" AutoGenerateColumns="False" DataKeyNames="BranchDepartmentId,EmployeeId"
                    GridLines="None" ShowFooterWhenEmpty="False" ShowHeaderWhenEmpty="True" OnRowCreated="gvwList_RowCreated"
                    Width="100%" onrowcommand="gvwList_RowCommand">
                    <Columns>
                        <asp:BoundField HeaderStyle-Width="150px" HeaderStyle-HorizontalAlign="Left" DataField="FromDate"
                            HeaderText="Transfer Date" />
                        <asp:BoundField HeaderStyle-Width="150px" HeaderStyle-HorizontalAlign="Left" DataField="FromDateEng"
                            DataFormatString="{0:yyyy-M-dd}" HeaderText="Transfer Date(AD)" />
                        <asp:BoundField HeaderStyle-HorizontalAlign="Left" DataField="EmployeeId" HeaderText="EIN"
                            HeaderStyle-Width="40px" />
                        <asp:BoundField HeaderStyle-Width="200px" HeaderStyle-HorizontalAlign="Left" DataField="EmployeeName"
                            HeaderText="Name" />
                        <asp:BoundField HeaderStyle-Width="150px" HeaderStyle-HorizontalAlign="Left" DataField="FromBranch"
                            HeaderText="From Branch" />
                        <asp:BoundField HeaderStyle-Width="150px" HeaderStyle-HorizontalAlign="Left" DataField="ToBranch"
                            HeaderText="To Branch" />
                        <asp:TemplateField HeaderText="" HeaderStyle-Width="90px" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <a href="javascript:void(0)" runat="server" visible='<%# IsTeamColumnVisible() %>'
                                    onclick='<%# "popupUpdateLeaveCall(" +  Eval("BranchDepartmentId") + ");return false;" %>'
                                    style="width: 80px; display: block;" id="ImageButton12">Review Team</a>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" Width="90px"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <%--<asp:HyperLink ID="linkEdit" ImageUrl="~/images/edit.gif" NavigateUrl='<%# "~/CP/BranchTransferDetails.aspx?Id=" + Eval("EmployeeId") %>'
                                runat="server"></asp:HyperLink>--%>
                                <asp:ImageButton ID="btnEdit" CommandName="Edit" ImageUrl="~/images/edit.gif" runat="server"
                                    OnClientClick='<%# "return popupUpdateBrTransferCall(" +  Eval("BranchDepartmentId") + ");" %>'
                                    AlternateText="Edit" />
                                    &nbsp;&nbsp;
                                <asp:ImageButton ID="btnDelete" CommandArgument='<%# Eval("BranchDepartmentId") %>' CommandName="Delete1" ImageUrl="~/images/delete.gif" runat="server"
                                    OnClientClick='return confirm("Are you sure, you want to delete the branch transfer?");'
                                    AlternateText="Delete" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="odd" />
                    <AlternatingRowStyle CssClass="even" />
                    <EmptyDataTemplate>
                        No records.
                    </EmptyDataTemplate>
                </cc2:EmptyDisplayGridView>
                <uc1:PagingCtl ID="pagingCtl" OnNextRecord="btnNext_Click" OnPrevRecord="btnPrevious_Click"
                    OnDropDownShowRecordsChanged="ddlRecords_SelectedIndexChanged" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>
