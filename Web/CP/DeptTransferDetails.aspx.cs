﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using Utils.Helper;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Calendar;
using System.IO;
using Utils;

namespace Web.CP
{
    public partial class DeptTransferDetails : BasePage
    {
        BranchManager branchMgr = new BranchManager();
        EmployeeManager empMgr = new EmployeeManager();
        InsuranceManager insMgr = new InsuranceManager();
        CommonManager cmpMgr = new CommonManager();
        List<BranchDepartmentHistory> list = new List<BranchDepartmentHistory>();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialise();
            }
            JavascriptHelper.AttachPopUpCode(Page, "popup", "BranchTransferLeaveSetting.aspx", 1200, 700);
        }

        void Initialise()
        {           
            List<FinancialDate> years = new CommonManager().GetAllFinancialDates();
            foreach (FinancialDate date in years)
            {
                date.SetName(IsEnglish);
            }

            List<SubDepartment> listSubDepartment = ListManager.GetSubDepartments();
            SubDepartment obj = new SubDepartment() { SubDepartmentId = -1, Name = "" };
            listSubDepartment.Insert(0, obj);

            ddlSubDepartment.DataSource = listSubDepartment;
            ddlSubDepartment.DataBind();

            ddlEventType.DataSource = CommonManager.GetServieEventTypes();
            ddlEventType.DataBind();

            LoadBranches();

            hdnEmployeeId.Value = "";
            hdnMessage.Value = "";

            //load for emp from querystring
            LoadEmpFromQueryString();

           
        }

        private void LoadEmpFromQueryString()
        {
            int branchDepartmentId = UrlHelper.GetIdFromQueryString("Id");
            if (branchDepartmentId != 0)
            {
                int empId = CommonManager.GetBranchDepartmentHistoryById(branchDepartmentId).EmployeeId.Value;
                EEmployee emp = new EmployeeManager().GetById(empId);

                hdnEmployeeId.Value = empId.ToString();
                txtEmpSearchText.Text = emp.Name;
                txtEmpSearchText.Enabled = false;

                LoadEmployeeDetails(null, null);

                FillDetailsFromBranchDeptId(branchDepartmentId);
            }
        }
        

        public void LoadDepartments(int branchId)
        {
            ListItem item = ddlTransferToDepartment.Items[0];

            ddlTransferToDepartment.DataSource = DepartmentManager.GetAllDepartments();
            ddlTransferToDepartment.DataBind();

            ddlTransferToDepartment.Items.Insert(0, item);
        }

        void LoadBranches()
        {           
            LoadDepartments(0);             
        }

        

        void LoadInsuranceName()
        {
            //ddlInsuranceName.DataSource = insMgr.GetInsuranceName();
            //ddlInsuranceName.DataBind();
        }



        protected void ddlFilterByValues_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadEmployees();
        }
        void LoadEmployees()
        {
            string type = string.Empty; int id = 0;

        }

        private BranchDepartmentHistory Process(BranchDepartmentHistory entity)
        {
            if (entity == null)
            {
                entity = new BranchDepartmentHistory();
                entity.LetterNumber = txtLetterNumber.Text.Trim();
                entity.LetterDate = calLetterDate.Text.Trim();
                entity.LetterDateEng = GetEngDate(entity.LetterDate);
                //entity.BranchId = int.Parse(ddlTransferToBranch.SelectedValue);
                entity.DepeartmentId = int.Parse(ddlTransferToDepartment.SelectedValue);
                entity.SpecialResponsibility = txtSpecialResp.Text.Trim();
                //entity.DepartureDate = calDepartureDate.Text.Trim();
                if (!string.IsNullOrEmpty(entity.DepartureDate))
                    entity.DepartureDateEng = GetEngDate(entity.DepartureDate);
                entity.FromDate = calFromDate.Text.Trim();
                entity.FromDateEng = GetEngDate(entity.FromDate);
                entity.EmployeeId = this.CustomId;
                entity.Note = txtNote.Text.Trim();

                entity.EventID = int.Parse(ddlEventType.SelectedValue);

                if(ddlSubDepartment.SelectedValue != "-1")
                    entity.SubDepartmentId = int.Parse(ddlSubDepartment.SelectedValue);

                return entity;
            }
            else
            {
                //UIHelper.SetSelectedInDropDown(ddlTransferToBranch, entity.BranchId);


                UIHelper.SetSelectedInDropDown(ddlTransferToDepartment, entity.DepeartmentId);



                txtLetterNumber.Text = entity.LetterNumber;
                txtSpecialResp.Text = entity.SpecialResponsibility;

                if (!string.IsNullOrEmpty(entity.LetterDate))
                    calLetterDate.Text = entity.LetterDate;
                //if (!string.IsNullOrEmpty(entity.DepartureDate))
                //    calDepartureDate.Text = entity.DepartureDate; //.SetSelectedDate(entity.DepartureDate, IsEnglish);
                calFromDate.Text = entity.FromDate;
                txtNote.Text = entity.Note;

                if (entity.EventID != null && entity.EventID != -1)
                    UIHelper.SetSelectedInDropDown(ddlEventType, entity.EventID.ToString());

                if (entity.SubDepartmentId != null)
                    ddlSubDepartment.SelectedValue = entity.SubDepartmentId.ToString();

            }
            return null;
        }

        public void LoadEmployeeDetails(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(hdnEmployeeId.Value))
                return;

            int employeeId = int.Parse(hdnEmployeeId.Value);

            EEmployee emp = EmployeeManager.GetEmployeeById(employeeId);

            if (emp == null)
            {
                buttonsBar.Visible = false;
                divWarningMsg.InnerHtml = "Please select an employee.";
                divWarningMsg.Hide = false;
                return;
            }
            else
                buttonsBar.Visible = true;

            CommonManager.SaveFirstIBranchfNotExists(employeeId);
           
            // Photo
            bool hasPhoto = false;
            if (emp.HHumanResources.Count > 0)
            {
                if (!string.IsNullOrEmpty(emp.HHumanResources[0].UrlPhotoThumbnail))
                {
                    image.ImageUrl = Page.ResolveUrl(Config.UploadLocation + "/" + emp.HHumanResources[0].UrlPhotoThumbnail);
                    if (File.Exists(Server.MapPath(image.ImageUrl)))
                        hasPhoto = true;
                }
            }
            if (!hasPhoto)
            {
                image.ImageUrl = Page.ResolveUrl(Web.Helper.WebHelper.GetEmployeeNoImage(employeeId));
            }

            lblWorkingIn.Text = "EIN : " + emp.EmployeeId + " ,I No : " + emp.EHumanResources[0].IdCardNo;
            lblName.Text = emp.Title + " " + emp.Name;
            lblBranch.Text = emp.Branch.Name;
            lblDepartment.Text = emp.Department.Name;

            int years, months, days, hours;
            int minutes, seconds, milliseconds;
            ServicePeroid firstStatus = EmployeeManager.GetServicePeroidDate(emp.EmployeeId);
            GetElapsedTime(firstStatus.FromDateEng, BLL.BaseBiz.GetCurrentDateAndTime(), out years, out months, out days, out hours, out minutes, out seconds, out milliseconds);

            lblSince.Text = "Since " +
                (IsEnglish ? firstStatus.FromDateEng.ToShortDateString() : firstStatus.FromDate + " (" + firstStatus.FromDateEng.ToShortDateString() + ")");
            lblTime.Text = string.Format("For {0} years {1} months {2} days",
                years, months, days);



        }

        // Return the number of years, months, days, hours, minutes, seconds,
        // and milliseconds you need to add to from_date to get to_date.
        private void GetElapsedTime(DateTime from_date, DateTime to_date,
            out int years, out int months, out int days, out int hours,
            out int minutes, out int seconds, out int milliseconds)
        {
            // If from_date > to_date, switch them around.
            if (from_date > to_date)
            {
                GetElapsedTime(to_date, from_date,
                    out years, out months, out days, out hours,
                    out minutes, out seconds, out milliseconds);
                years = -years;
                months = -months;
                days = -days;
                hours = -hours;
                minutes = -minutes;
                seconds = -seconds;
                milliseconds = -milliseconds;
            }
            else
            {
                // Handle the years.
                years = to_date.Year - from_date.Year;

                // See if we went too far.
                DateTime test_date = from_date.AddMonths(12 * years);
                if (test_date > to_date)
                {
                    years--;
                    test_date = from_date.AddMonths(12 * years);
                }

                // Add months until we go too far.
                months = 0;
                while (test_date <= to_date)
                {
                    months++;
                    test_date = from_date.AddMonths(12 * years + months);
                }
                months--;

                // Subtract to see how many more days,
                // hours, minutes, etc. we need.
                from_date = from_date.AddMonths(12 * years + months);
                TimeSpan remainder = to_date - from_date;
                days = remainder.Days;
                hours = remainder.Hours;
                minutes = remainder.Minutes;
                seconds = remainder.Seconds;
                milliseconds = remainder.Milliseconds;
            }
        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                //this.Value = "true";

                BranchDepartmentHistory history = Process(null);
            
                history.EmployeeId = int.Parse(hdnEmployeeId.Value);
                history.IsDepartmentTransfer = true;

                if (Request.QueryString["Id"] != null)
                {
                    history.BranchDepartmentId = int.Parse(Request.QueryString["Id"].ToString());

                    // If the it is last branch transfer then only allow to update
                    if (CommonManager.IsLastDepartmentTransfer(history.BranchDepartmentId, history.EmployeeId.Value) == false)
                    {
                        divWarningMsg.InnerHtml = "Only latest department transfer can be edited.";
                        divWarningMsg.Hide = false;
                        return;
                    }

                    CommonManager.UpdateBranchDepartmentHistory(history);

                    hdnMessage.Value = "Department transfer updated.";
                }
                else
                {
                    CommonManager.SaveBranchDepartmentHistory(history);

                    hdnMessage.Value = "Department transfer saved.";
                }


                // Prepare Sub-Department List
                //StringBuilder str = new StringBuilder("");
                //bool first = true;
                //List<SubDepartment> deps = DepartmentManager.GetAllSubDepartmentsByDepartment(history.DepeartmentId.Value);
                //foreach (SubDepartment obj in deps)
                //{
                //    if (first == false)
                //        str.Append(",");
                //    str.Append("\"" + obj.SubDepartmentId + "$$" + obj.Name + "\"");
                //    first = false;
                //}
                ////Page.ClientScript.RegisterArrayDeclaration("texts", str.ToString());

                // Register to set Branch/Subdepartment from js in Employee page
                string updateJS = string.Format("opener.updateBranchDepartmentFromChangeHistory({0},{1},'{2}','{3}');",
                    history.BranchId, history.DepeartmentId, new DepartmentManager().GetById(history.DepeartmentId.Value).Name, history.SubDepartmentId);
                Page.ClientScript.RegisterClientScriptBlock(GetType(), "sdfdsdfdsfsd", updateJS, true);


                ClearFields();
                btnCancel_Click(null, null);
            }
        }

        IIndividualInsurance ProcessInsurance(IIndividualInsurance indiv)
        {

            return null;
        }


        protected void btnCancel_Click(object sender, EventArgs e)
        {
            btnSave.Text = "Save";

            if (string.IsNullOrEmpty(txtEmpSearchText.Text.Trim()))
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "test", "javascript:closePopup();", true);
                return;
            }

            int employeeId = int.Parse(hdnEmployeeId.Value);

            if (employeeId != -1)
            {
                CommonManager.SaveFirstIBranchfNotExists(employeeId);
             
                ClearFields();
            }

            if (e == null)
                JavascriptHelper.DisplayClientMsg(hdnMessage.Value, Page, "closePopup();");
            else
                Page.ClientScript.RegisterStartupScript(this.GetType(), "test", "javascript:closePopup();", true);
        }

        void ClearFields()
        {
            //UIHelper.SetSelectedInDropDown(ddlTransferToBranch, -1);
            UIHelper.SetSelectedInDropDown(ddlTransferToDepartment, -1);
            txtNote.Text = "";
            calFromDate.Text = "";
            //calDepartureDate.Text = "";
            calLetterDate.Text = "";
            calFromDate.Enabled = true;
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Button is hidden as for internal use only
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnDelete_Click(object sender, EventArgs e)
        {
        }

        private void FillDetailsFromBranchDeptId(int branchDepartmentId)
        {
            BranchDepartmentHistory history = CommonManager.GetBranchDepartmentHistoryById(branchDepartmentId);

            Process(history);
            btnSave.Text = Resources.Messages.Update;

            //if (history.IsFirst.Value)
            //    calFromDate.Enabled = false;
            //else
            //    calFromDate.Enabled = true;

            int employeeId = int.Parse(hdnEmployeeId.Value);

            //if first one then disable from date as it should be the same of emp first status from date
            //if (CommonManager.GetBranchDepartmentHistory(employeeId)[0].BranchDepartmentId == history.BranchDepartmentId)
            //{
            //    calFromDate.ToolTip = Resources.Messages.HistoryFirstFromDateNotEditable;
            //    //calFromDate.Enabled = false;
            //}
            //else
            {
                calFromDate.Enabled = true; ;
                calFromDate.ToolTip = "";
            }
        }

        protected void btnLoadEmpDetls_Click(object sender, EventArgs e)
        {
            LoadEmployeeDetails(null, null);
        }

    }
}