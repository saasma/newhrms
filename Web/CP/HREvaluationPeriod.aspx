﻿<%@ Page Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/Master/ForPopupPage.Master"
    AutoEventWireup="true" Title="Period" CodeBehind="HREvaluationPeriod.aspx.cs"
    Inherits="Web.CP.HREvaluationPeriod" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Assembly="Karpach.WebControls" Namespace="Karpach.WebControls" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/UpDownCtl.ascx" TagName="UpDownCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="InfoMsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .colorStyle td
        {
            padding: 2px !important;
        }
        .transparent
        {
            /* this class makes a window partially transparent */
            opacity: 0; /* Standard style for transparency */
            -moz-opacity: 0; /* Transparency for older Mozillas */
            filter: alpha(opacity=00); /* Transparency for IE */
            width: 0;
        }
        .paddinAll
        {
            padding: 10px;
        }
        div.notify
        {
            margin-left: 0;
            width: 41%;
        }
        .lf
        {
            width: 150px;
            padding: 0 0px 0px 0;
        }
        .lfs
        {
            width: 160px;
            padding: 0 10px 0px 0;
            vertical-align: top;
        }
        table tr td
        {
            padding-bottom: 10px;
        }
        .bevel
        {
            margin-bottom: 20px;
        }
        
        div.notify
        {
            width: 510px;
        }
        .displayInline
        {
            display: inline;
        }
    </style>
    <script type="text/javascript">

        //capture window closing event

        function closePopup() {
            clearUnload();
            // alert(window.opener.parentReloadCallbackFunction)
            if ($.browser.msie == false && typeof (window.opener.parentReloadCallbackFunction) != 'undefined' && ($.browser.chrome || $.browser.safari)) {
                window.opener.parentReloadCallbackFunction(window);
            } else {
                //if (typeof (texts) != 'undefined')
                //  window.returnValue = texts;

                window.close();
            }

        }

        function clearUnload() {
            window.onunload = null;
        }

        function validateDOJToDate(source, args) {
            //debugger;

            args.IsValid = isSecondCalendarCtlDateGreater(getCalendarSelectedDate('<%=txtFromDate.ClientID %>'),
            getCalendarSelectedDate('<%= txtToDate.ClientID %>'));

        }


       

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <asp:HiddenField ID="hdEditSequence" runat="server" />
    <asp:HiddenField ID="hdEmployeeEditSequence" runat="server" />
    <div class="popupHeader">
        <h3>
            Evaluation Period</h3>
    </div>
    <div class="marginal" style='margin-top: 10px'>
        <uc2:InfoMsgCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
        <asp:HiddenField ID="hdPeriodId" runat="server" Value="0" />
        <asp:HiddenField ID="hdSaveMode" runat="server" Value="0" />
        <div class="clear">
            <cc2:EmptyDisplayGridView CssClass="tableLightColor" AllowPaging="true" PageSize="10" UseAccessibleHeader="true" ShowHeaderWhenEmpty="True"
                ID="gvPeriod" runat="server" DataKeyNames="EvaluationPeriodId,FromDate,ToDate,Name,IsActive"
                GridLines="None" AutoGenerateColumns="False" AllowSorting="True" OnRowCommand="gvPeriod_RowCommand"
                ShowFooterWhenEmpty="False"  OnPageIndexChanging="gvPosition_OnPageIndexChanging" OnRowDeleting="gvPeriod_OnRowDeleting" OnRowEditing="gvPeriod_RowEditing">
                <Columns>
                    <asp:BoundField DataField="EvaluationPeriodId" HeaderText="GradeId" Visible="false" />
                    <asp:BoundField DataField="FromDate" HeaderText="FromDate" Visible="false" />
                    <asp:BoundField DataField="ToDate" HeaderText="ToDate" Visible="false" />
                    <asp:BoundField DataField="Name" HeaderText="Name" Visible="false" />
                    <asp:BoundField DataField="Status" HeaderText="Status" Visible="false" />
                    <asp:TemplateField HeaderText="Friendly Name" HeaderStyle-Width="150px">
                        <ItemTemplate>
                            <asp:Label ID="lblName" runat="server" Text='<%#Eval("Name")%>' />
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" Width="150px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="From" HeaderStyle-Width="90px">
                        <ItemTemplate>
                            <asp:Label ID="lblFromDate" runat="server" Text='<%#Eval("FromDate")%>' />
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" Width="90px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="To" HeaderStyle-Width="90px">
                        <ItemTemplate>
                            <asp:Label ID="lblToDate" runat="server" Text='<%#Eval("ToDate")%>' />
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" Width="90px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Status" HeaderStyle-Width="50px">
                        <ItemTemplate>
                            <asp:CheckBox ID="st" runat="server" Checked='<%#Eval("IsActive")%>' Enabled="false" />
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" Width="50px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Edit" HeaderStyle-Width="20px" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:ImageButton ID="ImageButton1" CommandName="Edit" runat="server" ImageUrl="~/images/edit.gif" />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Delete" HeaderStyle-Width="20px" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:ImageButton ID="ImageButton2" OnClientClick="return confirm('Do you want to confirm delete the period?')"
                                runat="server" CommandName="Delete" ImageUrl="~/images/delete.gif" />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:TemplateField>
                </Columns>
                <RowStyle CssClass="odd" />
                <SelectedRowStyle CssClass="selected" />
                <AlternatingRowStyle CssClass="even" />
                <EmptyDataTemplate>
                    <b>No period records found. </b>
                </EmptyDataTemplate>
            </cc2:EmptyDisplayGridView>
        </div>
        <div class="bevel paddinAll" style="width: 500px; margin-bottom: 5px!important; margin-top: 10px">
            <table cellpadding="0" cellspacing="0" width="500px">
                <tr>
                    <td class="lf">
                        Friendly name<label>*</label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtFriendlyName" runat="server" Width="200px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="valGradeName" runat="server" ControlToValidate="txtFriendlyName"
                            Display="None" ErrorMessage="Friendly name is required." ValidationGroup="Position"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="lf">
                        From Date<label>*</label>
                    </td>
                    <td>
                        <My:Calendar Id="txtFromDate" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="lf">
                        To Date<label>*</label>
                    </td>
                    <td>
                        <My:Calendar Id="txtToDate" runat="server" />
                        <asp:TextBox Style='display: none' ID="txt" runat="server" />
                        <asp:CustomValidator IsCalendar="true" ID="valCustomeToDate2" runat="server" ValidateEmptyText="true"
                            ValidationGroup="Position" ControlToValidate="txt" Display="None" ErrorMessage="To date must be greater than From date."
                            ClientValidationFunction="validateDOJToDate" />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td class="lf">
                        Status
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="StatusCheckBox"  Checked="true"/>
                    </td>
                </tr>
            </table>
        </div>
        <div class="clear" style='text-align: right; padding-bottom: 10px; width: 525px'>
            <asp:Button ID="btnSave" CssClass="save" runat="server" OnClientClick="valGroup='Position';return CheckValidation()"
                OnClick="btnOk_Click" Text="Save" ValidationGroup="Leave" />
            <asp:Button ID="btnCancel" runat="server" CssClass="cancel" Text="Cancel" OnClientClick="window.close()" />
        </div>
    </div>
</asp:Content>
