﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using System.Text;
using BLL.Manager;
using Utils.Helper;
using DAL;
using System.Drawing;
using BLL.Base;
using Ext.Net;


namespace Web.CP
{
    public partial class AAEmployeeCatalogue : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.HumanResource;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {

                Initialise();

            }

            
        }

        protected void MenuButton_click(object sender, DirectEventArgs e)
        { 
        }

        //protected void ButtonSearch_Click(object sender, DirectEventArgs e)
        //{
        //    Hidden_MemberName.Text = txtSearchWord.Text;
        //    StoreRoomList.Reload();
        //}   

        protected void ToggleButton_Click(object sender, DirectEventArgs e)
        {
            Ext.Net.Button but = (Ext.Net.Button)sender;
            if (but.ID == "Toggle1")
            {
                Hidden_GridOrThumbnail.Text = "true";
            }
            else
            {
                Hidden_GridOrThumbnail.Text = "false";
            }

            StoreRoomList.Reload();
        }
        

        
        protected void SortByButton(object sender, DirectEventArgs e)
        {
        }

        protected void btnAddNewMember_Click(object sender, DirectEventArgs e)
        {
            
        }
        


        void Initialise()
        {

            cmbBranch.Store[0].DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            cmbBranch.Store[0].DataBind();

            cmbDepartment.Store[0].DataSource = DepartmentManager.GetAllDepartments().OrderBy(x => x.Name).ToList();
            cmbDepartment.Store[0].DataBind();

            cmbDesignation.Store[0].DataSource = new CommonManager().GetAllDesignations().OrderBy(x => x.Name).ToList();
            cmbDesignation.Store[0].DataBind();
            
        }

       

    }
}
