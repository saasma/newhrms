﻿<%@ Page Title="Hold Payment" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="HoldPaymentPG.aspx.cs" MaintainScrollPositionOnPostback="true"
    Inherits="Web.CP.HoldPaymentPG" %>

<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="Paging" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .mainFieldset
        {
            -moz-box-shadow: 1px 1px 3px #DDDDDD;
            background: none repeat scroll 0 0 #E9F0FB;
            border-color: #10B0EA;
            border-style: solid;
            border-width: 3px 1px 1px;
            float: left;
            font: bold 18px Arial,Helvetica,sans-serif;
            height: auto !important;
            margin-bottom: 20px;
            margin-top: 0;
            min-height: 80px;
            padding: 8px;
            position: relative;
            width: 800px !important;
        }
        .mainFieldset table
        {
            padding: 10px 20px 0;
        }
        .mainFieldset table tr td
        {
            padding: 0 10px 10px 0;
            text-align: left;
        }
        strong
        {
            display: block;
            padding-bottom: 5px;
        }
        
        .pagerCss td{padding-right:5px;}
        .pagerCss a,span{font-size:12px;}
    </style>
    <script type="text/javascript">
        function openCalculationPopup(payrollPeriodId) {
            if (typeof (ein) == 'undefined')
                ein = 0;
            window.open('../CP/holdpaymentcalculation.aspx?HoldPayment=true&ID=' + payrollPeriodId, 'Hold Payment List', 'height=600,width=1000');
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Hold Payment</h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
        <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
        <div class="mainFieldset">
            <table cellpadding="0" cellspacing="0" width="700px">
                <tr>
                    <td width="200px" valign="top">
                        <strong>Employee Name</strong>
                        <asp:DropDownList AppendDataBoundItems="true" ID="ddlEmployees" DataTextField="Name"
                            DataValueField="EmployeeId" onchange="setEIN(this);" runat="server" OnSelectedIndexChanged="ddlEmployees_SelectedIndexChanged">
                            <asp:ListItem Value="-1">--Select Employee--</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator InitialValue="-1" ID="valReqdResPerson" runat="server"
                            ControlToValidate="ddlEmployees" Display="None" ErrorMessage="Please select an employee."
                            ValidationGroup="StopPayment"></asp:RequiredFieldValidator>
                        <br />
                        <br />
                        <strong>Payroll Period</strong>
                        <asp:Label ID="lblPayrollPeriod" runat="server" />
                    </td>
                    <td valign="top" style="width: 60px">
                        <strong>EIN</strong>
                        <asp:Label ID="lblEmpId" Font-Bold="true" runat="server"></asp:Label>
                    </td>
                    <td>
                        <strong>Notes</strong>
                        <asp:TextBox MaxLength="200" ID="txtNotes" TextMode="MultiLine" Width="300px" Rows="4"
                            runat="server"></asp:TextBox>
                    </td>
                </tr>
                <%--   <tr>
                    <td width="360px" valign="top">
                    </td>
                </tr>--%>
                <tr>
                    <td>
                    </td>
                </tr>
            </table>
        </div>
        <div class="clear">
            <asp:Button ID="btnSave" OnClientClick="valGroup='StopPayment';return CheckValidation()"
                runat="server" Text="Save" CssClass="save" OnClick="btnSave_Click" />
            <asp:Button ID="btnCancel" runat="server" CssClass="cancel" Text="Cancel" OnClick="btnCancel_Click" />

            <asp:LinkButton ID="btnImport" runat="server" Text="Import" OnClientClick="importPopup();return false;" />

            <br />
             <table  style="margin-top:10px;" class="tableLightColor" cellpadding="3px" cellspacing="2px" width="900px">

                <tr>
                    <td>Employee Name:
                    </td>
                    <td>
                        <asp:TextBox ID="txtFilterEmpName" runat="server" placeholder="Search Employee" ></asp:TextBox>
                    </td>
                    <td>Hold month:
                    </td>
                    <td>
                        <asp:DropDownList Width="180px" AppendDataBoundItems="true" DataTextField="Name"
                            DataValueField="PayrollPeriodId" ID="ddlPayrollPeriodsFrom" runat="server">
                            <asp:ListItem Text="--Select Period--" Value="-1" > </asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>Released month:
                    </td>
                    <td>
                        <asp:DropDownList Width="180px" AppendDataBoundItems="true" DataTextField="Name"
                            DataValueField="PayrollPeriodId" ID="ddlPayrollPeriodTo" runat="server">
                             <asp:ListItem Text="--Select Period--" Value="-1" > </asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:Button ID="btnSearch" runat="server" CssClass="update" Text="Search" OnClick="btnSearch_Click" />
                    </td>
                </tr>
            </table>
            <br />
            <div style='clear: both'>
                <strong>Payment held this Month</strong>
                <cc2:EmptyDisplayGridView ID="gvw" AllowPaging="false" 
                    ShowHeaderWhenEmpty="true" runat="server" CellPadding="4"
                    CssClass="tableLightColor" GridLines="None" DataKeyNames="HoldPaymentId,EmployeeId,PayrollPeriodId"
                    Width="900px" AutoGenerateColumns="False"   OnSelectedIndexChanged="gvw_SelectedIndexChanged1"
                    
                    OnRowDeleting="gvw_RowDeleting">
                    <RowStyle BackColor="#E3EAEB" />
                    <AlternatingRowStyle CssClass="even" />
                    <Columns>
                        <asp:BoundField DataField="EmployeeId" ItemStyle-Width="50" DataFormatString="{0:000}"
                            HeaderText="EIN" HeaderStyle-HorizontalAlign="Left" >
<HeaderStyle HorizontalAlign="Left"></HeaderStyle>

<ItemStyle Width="50px"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="Name" HeaderText="Name" ItemStyle-Width="150px" 
                            HeaderStyle-HorizontalAlign="Left" >
<HeaderStyle HorizontalAlign="Left"></HeaderStyle>

<ItemStyle Width="150px"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="PayrollPeriodName" HeaderText="Hold Payment for" ItemStyle-Width="100"
                            HeaderStyle-HorizontalAlign="Left" >
<HeaderStyle HorizontalAlign="Left"></HeaderStyle>

<ItemStyle Width="100px"></ItemStyle>
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Notes" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <%# Eval("Notes") %>
                            </ItemTemplate>

<HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Preview" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:HyperLink ID="HyperLink1" Target="_blank" NavigateUrl='javascript:void(0);'
                                    onclick='<%# "openCalculationPopup(" + Eval("PayrollPeriodId") + ");return false;" %>'
                                    runat="server" Text="Preview All" />
                            </ItemTemplate>

<HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="30px">
                            <ItemTemplate>
                                <asp:ImageButton ID="ImageButton1" ToolTip="Edit" runat="server" CommandName="Select"
                                    ImageUrl="~/images/edit.gif" />
                            </ItemTemplate>

<ItemStyle Width="30px"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="30px">
                            <ItemTemplate>
                                <asp:ImageButton ID="ImageButton2" ToolTip="Delete" OnClientClick="return confirm('Confirm delete the hold payment?');"
                                    runat="server" CommandName="Delete" ImageUrl="~/images/delete.gif" />
                            </ItemTemplate>

<ItemStyle Width="30px"></ItemStyle>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        No Current month Hold Payments.
                    </EmptyDataTemplate>
                    <PagerSettings Mode="NumericFirstLast" />
                    <PagerStyle CssClass="pagerCss" Width="20px" />
                    <RowStyle CssClass="odd" />
                </cc2:EmptyDisplayGridView>
            </div>
             <div style="width: 900px">
                <uc1:Paging ID="pagingCtl1" OnNextRecord="btnNext1_Click" OnPrevRecord="btnPrevious1_Click"
                    OnDropDownShowRecordsChanged="ddlRecords1_SelectedIndexChanged" runat="server" />
            </div>
            <div style='clear: both; margin-top: 15px'>
                <strong>Payment held in the past months</strong>
                <cc2:EmptyDisplayGridView ID="gvwHoldPaymentPastMonths" AllowPaging="false"  ShowHeaderWhenEmpty="true"
                    runat="server" CssClass="tableLightColor"  GridLines="None" DataKeyNames="HoldPaymentId,EmployeeId,PayrollPeriodId"
                    Width="900px" AutoGenerateColumns="False" 
                     OnSelectedIndexChanged="gvwHoldPaymentPastMonths_SelectedIndexChanged1"
                     OnRowDeleting="gvwHoldPaymentPastMonths_RowDeleting">
                    <RowStyle BackColor="#EFF3FB" />
                    <Columns>
                        <asp:BoundField DataField="EmployeeId" ItemStyle-Width="50" DataFormatString="{0:000}"
                            HeaderText="EIN" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField DataField="Name" HeaderText="Name" ItemStyle-Width="150px" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField DataField="PayrollPeriodName" HeaderText="Hold Period for" ItemStyle-Width="100"
                            HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField DataField="ReleasePayrollPeriodName" HeaderText="Release Period"
                            ItemStyle-Width="120" HeaderStyle-HorizontalAlign="Left" />
                        <asp:TemplateField HeaderText="Is Released" HeaderStyle-HorizontalAlign="Center"
                            ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <%# Convert.ToBoolean( Eval("IsReleased")) == true ? "Yes" : "No"%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Notes" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <%# Eval("Notes") %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Preview" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                             
                                <asp:HyperLink ID="HyperLink1" Target="_blank" NavigateUrl='javascript:void(0);' onclick='<%# "openCalculationPopup(" + Eval("PayrollPeriodId") + ");return false;" %>'
                                    runat="server" Text="Preview All" />
                           
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="30px">
                            <ItemTemplate>
                                <asp:ImageButton ID="ImageButton1" Visible='<%# IsCancelReleased(Eval("ReleasedPayrollPeriodId")) %>'
                                    Width="16" Height="16" OnClientClick="return confirm('Confirm release the hold payment in this month?');"
                                    ToolTip="Release in current Period" runat="server" CommandName="Select" ImageUrl="~/images/release.png" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="30px">
                            <ItemTemplate>
                                <asp:ImageButton Visible='<%# IsReleased(Eval("IsReleased")) %>' ID="ImageButton2"
                                    Width="16" Height="16" ToolTip="Cancel Release" OnClientClick="return confirm('Confirm delete the release of the hold payment in this month?');"
                                    runat="server" CommandName="Delete" ImageUrl="~/images/delete.gif" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
               <RowStyle CssClass="odd" />
                    <AlternatingRowStyle CssClass="even" />
                    <EmptyDataTemplate>
                        No past month Hold Payments.
                    </EmptyDataTemplate>
                    <PagerSettings Mode="NumericFirstLast" />
                    <PagerStyle CssClass="pagerCss" Width="20px" />
                </cc2:EmptyDisplayGridView>
            </div>
             <div style="width: 900px">
                <uc1:Paging ID="pagingCtl2" runat="server" OnNextRecord="btnNext2_Click" OnPrevRecord="btnPrevious2_Click"
                    OnDropDownShowRecordsChanged="ddlRecords2_SelectedIndexChanged" />
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="pageScriptsContent" runat="server">
    <script type="text/javascript">

        function setEIN(ddl) {
            document.getElementById('<%= lblEmpId.ClientID %>').innerHTML = ddl.value;
        }

        function resumePaymentPopupCall(stopPaymentId) {
            var ret = resumePaymentPopup("Id=" + stopPaymentId);
            if (typeof (ret) != 'undefined') {
                if (ret == "Reload") {
                    __doPostBack("Reload", "");
                }
            }
        }


        function validateDOJToDate(source, args) {
            //debugger;



        }

        function refreshWindow() {
            __doPostBack('Reload', '');
        }

    </script>
    <style type="text/css">
        .marginCls
        {
            margin-left:490px !important;
        }
    </style>
</asp:Content>
