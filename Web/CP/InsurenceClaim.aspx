﻿<%@ Page Title="Medical Claims Management" Language="C#" AutoEventWireup="true" MasterPageFile="~/Master/NewDetails.Master"
    CodeBehind="InsurenceClaim.aspx.cs" Inherits="Web.NewHR.InsurenceClaim" %>

<%@ Register Src="~/Controls/FooterCtl.ascx" TagName="FooterCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/HeaderCtl.ascx" TagName="HeaderCtl" TagPrefix="uc3" %>
<%@ Register Src="~/Employee/UserControls/InsuranceClaim.ascx" TagName="InsuranceClaim" TagPrefix="ucF" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
 
    </style>
    <script type="text/javascript">
        var CommandHandler = function(command, record){
            <%= hiddenClaimId.ClientID %>.setValue(record.data.ClaimID);
            if(command=="Change")
            {
                <%=txtCurrentStatus.ClientID%>.setValue('');
                <%=cmbStatus.ClientID%>.setValue('');
                <%=dfStatusChangeDate.ClientID%>.setValue('');
                <%=btnEdit.ClientID %>.fireEvent('click');
            }
            if(command=="View")
                <%=btnView.ClientID %>.fireEvent('click');
        }
        var CommandHandlerClaimHistory = function(command, record){
        }

        var CommandHandlerLotHistory = function(command, record){
            if(record.data.LotID!=null && record.data.LotID!="")
            {
                debugger;
                if(command=="ViewLot")
                {
                    var url='InsurenceClaimLot.aspx?LotID='+record.data.LotID;
                    window.open(url, '_blank');   
                }
                <%--if(command=="Export")
                {
                    <%=hiddenLotNo.ClientID %>.setValue(record.data.LotID);
                    <%=btnExport.ClientID %>.fireEvent('click');
                }--%>
                if(command=="Import")
                {
                    var LotID=record.data.LotID; 
                    <%=hiddenLotNo.ClientID %>.setValue(LotID);
                    var ret = InuranceImportPopUp("LotID=" +LotID);
                    return false;
                }
            } 
        }
        

        function searchList() {     
            <%=GridClaimList.ClientID %>.getStore().pageSize = <%=cmbPageSize.ClientID %>.getValue();
            <%=PagingToolbar1.ClientID%>.doRefresh();
        }

        function searchList1() {
            <%=GridClaimHistoryList.ClientID %>.getStore().pageSize = <%=cmbPageSize1.ClientID %>.getValue();
            <%=PagingToolbar2.ClientID%>.doRefresh();
        }

        function searchList2() {     
            <%=GridLotHistory.ClientID %>.getStore().pageSize = <%=cmbPageSize2.ClientID %>.getValue();
            <%=PagingToolbar3.ClientID%>.doRefresh();
        }
        

        function LotPageRedirect() {
            window.open('InsurenceClaimLot.aspx', '_blank');
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>
    <ext:Hidden ID="hiddenClaimId" runat="server" />
    <ext:Hidden ID="hiddenLotNo" runat="server" />
    <ext:LinkButton runat="server" Hidden="true" ID="btnEdit">
        <DirectEvents>
            <Click OnEvent="btnEdit_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:LinkButton runat="server" Hidden="true" ID="btnView">
        <DirectEvents>
            <Click OnEvent="btnView_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>

    <%--<ext:LinkButton AutoPostBack="false" runat="server" Hidden="true" ID="btnExport">
        <DirectEvents>
            <Click OnEvent="btnExport_Click">
            </Click>
        </DirectEvents>
    </ext:LinkButton>--%>


    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>Medical Claims Management
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <table>
            <tr>
                <td style="width: 220px;">
                    <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                        <Proxy>
                            <ext:AjaxProxy Url="../Handler/EmpSearchID.ashx">
                                <ActionMethods Read="GET" />
                                <Reader>
                                    <ext:JsonReader Root="plants" TotalProperty="total" />
                                </Reader>
                            </ext:AjaxProxy>
                        </Proxy>
                        <Model>
                            <ext:Model ID="Model3" IDProperty="Name" Name="ItemLineModel" runat="server">
                                <Fields>
                                    <ext:ModelField Name="Name" Type="String" />
                                    <ext:ModelField Name="EmployeeId" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                    <ext:ComboBox LabelSeparator="" ID="cmbSearch" runat="server" DisplayField="Name"
                        FieldLabel="Employee" LabelAlign="Top" ValueField="EmployeeId" EmptyText="Search Employee"
                        StoreID="storeSearch" TypeAhead="false" Width="200" HideBaseTrigger="true" MinChars="1"
                        TriggerAction="All" ForceSelection="false">
                        <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                            <ItemTpl ID="ItemTpl1" runat="server">
                                <Html>
                                    <tpl>
                                            <div class="search-item">
                                                            <span>{Name}</span>  
                                             </div>
					                </tpl>
                                </Html>
                            </ItemTpl>
                        </ListConfig>
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                        </Triggers>
                        <Listeners>
                            <Select Handler="this.getTrigger(0).show();searchList();searchList1();" />
                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                            <TriggerClick Handler="if (index == 0) {  this.clearValue();  this.getTrigger(0).hide();searchList();searchList1();}" />
                        </Listeners>
                    </ext:ComboBox>
                </td>
                <td style="padding-right: 10px;">
                    <ext:DateField ID="dfSearchClaimDateFrom" runat="server" FieldLabel="From" LabelAlign="top" LabelSeparator="" Width="150">
                        <Plugins>
                            <ext:GenericPlugin ID="GenericPlugin1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                        </Plugins>
                    </ext:DateField>
                </td>
                <td style="padding-right: 10px;">
                    <ext:DateField ID="dfSearchClaimDateTo" runat="server" FieldLabel="To" LabelAlign="top" LabelSeparator="" Width="150">
                        <Plugins>
                            <ext:GenericPlugin ID="GenericPlugin3" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                        </Plugins>
                    </ext:DateField>
                </td>
                <td style="padding-top: 20px;">
                    <ext:Button ID="btnLoad" runat="server" Cls="btn btn-default" Text="Load">
                        <Listeners>
                            <Click Handler="searchList();searchList1();searchList2();">
                            </Click>
                        </Listeners>
                    </ext:Button>
                </td>
                <td style="padding-top: 20px; padding-left: 10px;">
                    <ext:Button ID="btnCreateLot" runat="server" Cls="btn btn-save" Text="Create Lot" Width="120">
                        <Listeners>
                            <Click Fn="LotPageRedirect">
                            </Click>
                        </Listeners>
                    </ext:Button>
                </td>
                <td>
                    <ucF:InsuranceClaim ID="InsuranceClaim" runat="server" />
                    <ext:Button runat="server" MarginSpec="20 0 0 10" ID="btnAddClaim" Cls="btn btn-primary" Text="<i></i>Assign Claim">
                        <Listeners>
                            <Click Handler="#{hiddenClaimId}.setValue('');" />
                        </Listeners>
                        <DirectEvents>
                            <Click OnEvent="btnAddClaim_Click">
                                <EventMask ShowMask="true" />
                            </Click>
                        </DirectEvents>
                    </ext:Button>

                </td>
            </tr>
        </table>
        <br />
        <ext:TabPanel ID="TabPanel1" runat="server" Width="1265px">
            <Items>
                <ext:Panel ID="pnlProcessing" runat="server" Title="Processing Claims">
                    <Content>
                    </Content>
                </ext:Panel>
                <ext:Panel ID="pnlLot" runat="server" Title="Lots History">
                    <Content>
                    </Content>
                </ext:Panel>
                <ext:Panel ID="pnlClaim" runat="server" Title="Claims History">
                    <Content>
                    </Content>
                </ext:Panel>
            </Items>
            <DirectEvents>
                <TabChange OnEvent="TabPanel1_TabChange">
                </TabChange>
            </DirectEvents>
        </ext:TabPanel>

        <ext:GridPanel ID="GridClaimList" runat="server" Header="true" Scroll="Horizontal">
            <Store>
                <ext:Store ID="StoreGridClaimList" runat="server" OnReadData="Store_ReadData" RemoteSort="true" AutoLoad="true">
                    <Proxy>
                        <ext:PageProxy />
                    </Proxy>
                    <Model>
                        <ext:Model ID="ModelGridClaimList" runat="server" IDProperty="ClaimID">
                            <Fields>
                                <ext:ModelField Name="ClaimID" />
                                <ext:ModelField Name="EmployeeID" />
                                <ext:ModelField Name="Name" />
                                <ext:ModelField Name="ClaimValue" />
                                <ext:ModelField Name="ClaimDate" Type="Date" />
                                <ext:ModelField Name="RelationName" />
                                <ext:ModelField Name="TotalClaimAmount" />
                                <ext:ModelField Name="statusValue" />
                                <ext:ModelField Name="LotRef_ID" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>
            <ColumnModel>
                <Columns>
                    <ext:Column ID="Column1" Sortable="false" MenuDisabled="true" runat="server" Text="EIN" Width="80" DataIndex="EmployeeID" />
                    <ext:Column ID="Column2" Sortable="false" MenuDisabled="true" runat="server" Text="Employee Name" Width="200" DataIndex="Name" />
                    <ext:Column ID="Column3" Sortable="false" MenuDisabled="true" runat="server" Text="Claim Type" Width="120" DataIndex="ClaimValue" />
                    <ext:DateColumn ID="Column4" Sortable="false" MenuDisabled="true" runat="server" Text="Claim Date" Format="yyyy-MM-dd" Width="120" DataIndex="ClaimDate" />
                    <ext:Column ID="Column5" Sortable="false" MenuDisabled="true" runat="server" Text="Claim Of" Width="120" DataIndex="RelationName" />
                    <ext:NumberColumn ID="Column6" Sortable="false" MenuDisabled="true" runat="server" Text="Claim Amount" Width="150" DataIndex="TotalClaimAmount" />
                    <ext:Column ID="Column7" Sortable="false" MenuDisabled="true" runat="server" Text="Status" Width="120" DataIndex="statusValue" />
                    <ext:Column ID="Column9" Sortable="false" MenuDisabled="true" runat="server" Text="Lot No." Width="120" DataIndex="LotRef_ID" />
                    <ext:CommandColumn ID="Column8" runat="server" Align="Center" Flex="1">
                        <Commands>
                            <ext:GridCommand Cls="editGridButton" Text="<i></i>" Icon="ApplicationViewDetail" ToolTip-Text="View Details" CommandName="View"></ext:GridCommand>
                            <ext:GridCommand Cls="editGridButton" Text="<i></i>" ToolTip-Text="Change Status" Icon="BookEdit" CommandName="Change"></ext:GridCommand>
                        </Commands>
                        <Listeners>
                            <Command Handler="CommandHandler(command,record);" />
                        </Listeners>
                    </ext:CommandColumn>
                </Columns>
            </ColumnModel>
            <SelectionModel>
                <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
            </SelectionModel>
            <BottomBar>
                <ext:PagingToolbar Height="40" ID="PagingToolbar1" runat="server" StoreID="StoreGridClaimList" DisplayInfo="true">
                    <Items>
                        <ext:Label ID="lblPageSize" runat="server" Text="Page size:" />
                        <ext:ToolbarSpacer ID="ToolbarSpacer_listItems" runat="server" Width="10" />
                        <ext:ComboBox ID="cmbPageSize" runat="server" Width="80" SelectOnFocus="true" Selectable="true"
                            ValueField="Value" DisplayField="Text" ForceSelection="true" AllowBlank="false">
                            <Listeners>
                                <Select Handler="searchList()" />
                            </Listeners>
                            <Items>
                                <ext:ListItem Value="25" Text="25" />
                                <ext:ListItem Value="50" Text="50" />
                                <ext:ListItem Value="100" Text="100" />
                                <ext:ListItem Value="100000" Text="All" />
                            </Items>
                            <SelectedItems>
                                <ext:ListItem Index="0">
                                </ext:ListItem>
                            </SelectedItems>
                        </ext:ComboBox>
                    </Items>
                </ext:PagingToolbar>
            </BottomBar>
        </ext:GridPanel>

        <ext:GridPanel ID="GridClaimHistoryList" Hidden="true" runat="server" Header="true" Scroll="Horizontal">
            <Store>
                <ext:Store ID="StoreGridClaimHistoryList" runat="server" OnReadData="Store_ReadData1" RemoteSort="true" AutoLoad="true">
                    <Proxy>
                        <ext:PageProxy />
                    </Proxy>
                    <Model>
                        <ext:Model ID="ModelGridClaimHistoryList" runat="server" IDProperty="ClaimID">
                            <Fields>
                                <ext:ModelField Name="ClaimID" />
                                <ext:ModelField Name="EmployeeID" />
                                <ext:ModelField Name="Name" />
                                <ext:ModelField Name="ClaimValue" />
                                <ext:ModelField Name="ClaimDate" Type="Date" />
                                <ext:ModelField Name="RelationName" />
                                <ext:ModelField Name="TotalClaimAmount" />
                                <ext:ModelField Name="statusValue" />
                                <ext:ModelField Name="LotRef_ID" />
                                <ext:ModelField Name="ReceivedAmount" />
                                <ext:ModelField Name="ReceivedDate" Type="Date" />
                                <ext:ModelField Name="Remarks" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>
            <ColumnModel>
                <Columns>
                    <ext:Column ID="Column10" Sortable="false" MenuDisabled="true" runat="server" Text="EIN" Width="50" DataIndex="EmployeeID" />
                    <ext:Column ID="Column11" Sortable="false" MenuDisabled="true" runat="server" Text="Employee Name" Width="150" DataIndex="Name" />
                    <ext:Column ID="Column12" Sortable="false" MenuDisabled="true" runat="server" Text="Claim Type" Width="100" DataIndex="ClaimValue" />
                    <ext:DateColumn ID="DateColumn1" Sortable="false" MenuDisabled="true" runat="server" Text="Claim Date" Format="yyyy-MM-dd" Width="120" DataIndex="ClaimDate" />
                    <ext:Column ID="Column13" Sortable="false" MenuDisabled="true" runat="server" Text="Claim Of" Width="100" DataIndex="RelationName" />
                    <ext:NumberColumn ID="NumberColumn1" Sortable="false" MenuDisabled="true" runat="server" Text="Claim Amount" Width="120" DataIndex="TotalClaimAmount" />
                    <ext:NumberColumn ID="NumberColumn2" Sortable="false" MenuDisabled="true" runat="server" Text="Received Amount" Width="120" DataIndex="ReceivedAmount" />
                    <ext:DateColumn ID="DateColumn2" Sortable="false" MenuDisabled="true" runat="server" Text="Received Date" Format="yyyy-MM-dd" Width="120" DataIndex="ReceivedDate" />
                    <ext:Column ID="Column15" Sortable="false" MenuDisabled="true" runat="server" Text="Lot No." Width="100" DataIndex="LotRef_ID" />
                    <ext:Column ID="Column14" Sortable="false" MenuDisabled="true" runat="server" Text="Remarks" Width="150" DataIndex="Remarks" Flex="1" />
                    <ext:CommandColumn ID="CommandColumn2" Hidden="true" runat="server" Align="Center">
                        <Commands>
                            <ext:GridCommand Cls="editGridButton" Text="View" Icon="ApplicationViewDetail" ToolTip-Text="View Details" CommandName="ViewClaim"></ext:GridCommand>
                        </Commands>
                        <Listeners>
                            <Command Handler="CommandHandlerClaimHistory(command,record);" />
                        </Listeners>
                    </ext:CommandColumn>
                </Columns>
            </ColumnModel>
            <SelectionModel>
                <ext:RowSelectionModel ID="RowSelectionModel3" runat="server" Mode="Single" />
            </SelectionModel>
            <BottomBar>
                <ext:PagingToolbar Height="40" ID="PagingToolbar2" runat="server" StoreID="StoreGridClaimHistoryList" DisplayInfo="true">
                    <Items>
                        <ext:Label ID="lblPageSize1" runat="server" Text="Page size:" />
                        <ext:ToolbarSpacer ID="ToolbarSpacer_listItems1" runat="server" Width="10" />
                        <ext:ComboBox ID="cmbPageSize1" runat="server" Width="80" SelectOnFocus="true" Selectable="true"
                            ValueField="Value" DisplayField="Text" ForceSelection="true" AllowBlank="false">
                            <Listeners>
                                <Select Handler="searchList1()" />
                            </Listeners>
                            <Items>
                                <ext:ListItem Value="25" Text="25" />
                                <ext:ListItem Value="50" Text="50" />
                                <ext:ListItem Value="100" Text="100" />
                                <ext:ListItem Value="100000" Text="All" />
                            </Items>
                            <SelectedItems>
                                <ext:ListItem Index="0">
                                </ext:ListItem>
                            </SelectedItems>
                        </ext:ComboBox>
                    </Items>
                </ext:PagingToolbar>
            </BottomBar>
        </ext:GridPanel>

        <ext:GridPanel ID="GridLotHistory" Hidden="true" runat="server" Header="true" AutoScroll="true" Scroll="Horizontal">
            <Store>
                <ext:Store ID="StoreGridLotHistory" runat="server" OnReadData="Store_ReadData2" RemoteSort="true" AutoLoad="true">
                    <Proxy>
                        <ext:PageProxy />
                    </Proxy>
                    <Model>
                        <ext:Model ID="ModelStoreGridLotHistory" runat="server" IDProperty="LotID">
                            <Fields>
                                <ext:ModelField Name="LotID" />
                                <ext:ModelField Name="NoOfEmp" />
                                <ext:ModelField Name="CreatedOn" Type="Date" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>
            <ColumnModel>
                <Columns>
                    <ext:Column ID="Column16" Sortable="false" MenuDisabled="true" runat="server" Text="Lot No." Width="80" DataIndex="LotID" />
                    <ext:DateColumn ID="DateColumn3" Sortable="false" MenuDisabled="true" runat="server" Text="Submission Date" Format="yyyy-MM-dd hh:mm:ss a" Width="200" DataIndex="CreatedOn" />
                    <ext:Column ID="Column19" Sortable="false" MenuDisabled="true" runat="server" Text="No. Of Claims" Width="120" DataIndex="NoOfEmp" />
                    <ext:DateColumn ID="DateColumn4" Sortable="false" MenuDisabled="true" runat="server" Text="Returned Date" Format="yyyy-MM-dd" Width="120" />
                    <ext:CommandColumn ID="CommandColumn1" Sortable="false" MenuDisabled="true" runat="server" Align="Center" Width="200" Flex="1">
                        <Commands>
                            <ext:GridCommand Cls="editGridButton" Text="<i></i>" Icon="ApplicationViewDetail" ToolTip-Text="View Details" CommandName="ViewLot"></ext:GridCommand>
                            <ext:GridCommand Text="Import" ToolTip-Text="Import" Icon="PageExcel" CommandName="Import"></ext:GridCommand>
                            <%-- <ext:GridCommand Text="Export" ToolTip-Text="Export" Icon="PageWhiteExcel" CommandName="Export"></ext:GridCommand>--%>
                        </Commands>
                        <Listeners>
                            <Command Handler="CommandHandlerLotHistory(command,record);" />
                        </Listeners>
                    </ext:CommandColumn>
                </Columns>
            </ColumnModel>
            <SelectionModel>
                <ext:RowSelectionModel ID="RowSelectionModel4" runat="server" Mode="Single" />
            </SelectionModel>
            <BottomBar>
                <ext:PagingToolbar Height="40" ID="PagingToolbar3" runat="server" StoreID="StoreGridLotHistory" DisplayInfo="true">
                    <Items>
                        <ext:Label ID="lblPageSize2" runat="server" Text="Page size:" />
                        <ext:ToolbarSpacer ID="ToolbarSpacer_listItems2" runat="server" Width="10" />
                        <ext:ComboBox ID="cmbPageSize2" runat="server" Width="80" SelectOnFocus="true" Selectable="true"
                            ValueField="Value" DisplayField="Text" ForceSelection="true" AllowBlank="false">
                            <Listeners>
                                <Select Handler="searchList2()" />
                            </Listeners>
                            <Items>
                                <ext:ListItem Value="25" Text="25" />
                                <ext:ListItem Value="50" Text="50" />
                                <ext:ListItem Value="100" Text="100" />
                                <ext:ListItem Value="100000" Text="All" />
                            </Items>
                            <SelectedItems>
                                <ext:ListItem Index="0">
                                </ext:ListItem>
                            </SelectedItems>
                        </ext:ComboBox>
                    </Items>
                </ext:PagingToolbar>
            </BottomBar>
        </ext:GridPanel>

        <ext:Store ID="storecmbStatus" runat="server">
            <Model>
                <ext:Model ID="modelcmbStatus" runat="server" IDProperty="Value">
                    <Fields>
                        <ext:ModelField Name="Text" Type="string" />
                        <ext:ModelField Name="Value" Type="string" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>

        <ext:Window ID="WindowStatus" runat="server" Title="Change Status" Icon="Application" Width="400" Height="250" BodyPadding="5" Hidden="true" Modal="true">
            <Content>
                <table class="fieldTable">
                    <tr>
                        <td>
                            <ext:TextField ID="txtCurrentStatus" FieldLabel="Current Status" Disabled="true" runat="server" LabelAlign="Top" Width="150" LabelSeparator="">
                            </ext:TextField>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ext:ComboBox runat="server" Width="150" ID="cmbStatus" StoreID="storecmbStatus"
                                ForceSelection="true" FieldLabel="New Status" LabelAlign="Top" DisplayField="Text" ValueField="Value" QueryMode="Local" LabelSeparator="">
                            </ext:ComboBox>
                            <asp:RequiredFieldValidator Display="None" ID="rfvcmbStatus" runat="server" ValidationGroup="SaveUpdateStatus" ControlToValidate="cmbStatus" ErrorMessage="New status is required." />

                        </td>
                        <td>
                            <ext:DateField ID="dfStatusChangeDate" runat="server" CausesValidation="true" FieldLabel="Status Change Date"
                                LabelAlign="top" LabelSeparator="" Width="150">
                                <Plugins>
                                    <ext:GenericPlugin ID="GenericPlugin2" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                                </Plugins>
                            </ext:DateField>
                            <asp:RequiredFieldValidator Enabled="true" Display="None" ID="rfvStatusChangeDate" runat="server" ValidationGroup="SaveUpdateStatus"
                                ControlToValidate="dfStatusChangeDate"
                                ErrorMessage="Status change date is required." />

                        </td>
                    </tr>
                </table>
            </Content>
            <Buttons>
                <ext:Button runat="server" Cls="btn btn-primary" StyleSpec="margin-left:10px" ValidationGroup="SaveUpdateStatus"
                    ID="btnSave" Text="<i></i>Save">
                    <DirectEvents>
                        <Click OnEvent="btnSave_Click">
                            <EventMask ShowMask="true" />
                        </Click>
                    </DirectEvents>
                    <Listeners>
                        <Click Handler="valGroup = 'SaveUpdateStatus'; return CheckValidation();">
                        </Click>
                    </Listeners>
                </ext:Button>
                <ext:LinkButton runat="server" StyleSpec="margin-left:10px;padding:0px;margin-right:10px" Cls="btnFlatLeftGap" ID="LinkButton2"
                    Text="<i></i>Cancel">
                    <Listeners>
                        <Click Handler="#{WindowStatus}.hide();">
                        </Click>
                    </Listeners>
                </ext:LinkButton>
            </Buttons>
        </ext:Window>
    </div>
</asp:Content>
