<%@ Page MaintainScrollPositionOnPostback="true" Title="Salary Processing Day" Language="C#"
    MasterPageFile="~/Master/ForPopupPage.Master" AutoEventWireup="true" CodeBehind="SalaryProcessingDate.aspx.cs"
    Inherits="Web.CP.SalaryProcessingDate" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.numeric.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.bgiframe.min.js" type="text/javascript"></script>
    <link href="../css-combo/sexy-combo.css" rel="stylesheet" type="text/css" />
    <link href="../css-combo/custom/custom.css" rel="stylesheet" type="text/css" />
    <link href="../css-combo/sexy/sexy.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        tr
        {
            padding-top: 10px;
        }
    </style>
    <script type="text/javascript">

        function closePopup() {


            window.opener.refreshSalaryDay();

            //window.opener.setCurrency(document.getElementById('ctl00_mainContent_hdn').value);
        }


        window.onunload = closePopup;   
        
    </script>
    <%--  <script src="../Scripts/jquery.sexy-combo.pack.js" type="text/javascript"></script>

   <script src="../Scripts/jquery.sexy-combo.min.js" type="tet/javascript"></script>

    <script src="../Scripts/jquery.sexy-combo.js" type="text/javascript"></script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <div class="popupHeader">
        <h3>
            Salary Processing Day</h3>
    </div>
    <asp:HiddenField ID="hdn" runat="server" />
    <div class=" marginal" style='margin-top: 15px'>
          <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
            <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
            <table style='clear: both; float: left'>
                <tr>
                    <td class="fieldHeader" style='width: 190;'>
                        <My:Label ID="Label4" Text="Processing upto day" runat="server" ShowAstrick="false" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <%--<My:Label ID="Label5" Text="IRO name:" runat="server" ShowAstrick="true" /> --%>
                        <asp:TextBox ID="txtContractDay" Enabled="false" runat="server" Width="100" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtContractDay"
                            Display="None" ErrorMessage="Please select day." ValidationGroup="AECompany"></asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <asp:Button ID="btnEdit1" Width="60" Height="22" Style="margin-top: 1px; border: 1px solid gray"
                            Visible="true" runat="server" Text="Edit" OnClick="btnEdit1_Click" />
                        <asp:Button ID="btnCancel1" Width="60" Height="22" Style="margin-top: 1px; border: 1px solid gray"
                            runat="server" Text="Cancel" Visible="false" OnClick="btnCancel1_Click" />
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style='padding-top: 10px'>
                        <asp:Button ID="btnSave" CssClass="update" OnClientClick="valGroup = 'AECompany';if( CheckValidation() ) { return confirm('Are you sure, you want to change the leave setting?');}"
                            runat="server" Text="Save" ValidationGroup="AECompany" OnClick="btnSave_Click" />
                    </td>
                </tr>
            </table>
    </div>
</asp:Content>
