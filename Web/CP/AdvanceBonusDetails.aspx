<%@ Page Title="Bonus Details" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="AdvanceBonusDetails.aspx.cs" Inherits="Web.CP.AdvanceBonusDetails" %>

<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../_assets/themes/yui/tabs.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">

        function insertUpdateGratuityClassCall(classId) {
            var ret = insertUpdateGratuityClass('Id=' + classId);

            if (typeof (ret) != 'undefined') {
                if (ret == 'Reload') {
                    __doPostBack('Reload', '');
                }
            }
        }
        function insertUpdateGratuityRuleCall(gratuityRuleId) {
            var ret = insertUpdateGratuityRule('Id=' + gratuityRuleId);

            if (typeof (ret) != 'undefined') {
                if (ret == 'Reload') {
                    __doPostBack('Reload', '');
                }
            }

        }


        function refresh(popupWindow) {
            popupWindow.close();
            __doPostBack('Reload', '');
        }

        function changeStates(chk) {
            var isChecked = $(chkAll).is(':checked');

            $("INPUT[type='checkbox']").attr('checked', isChecked);

        }

        function updateBonusPopup(bonusId, empId) {
            var ret = updateBonus("bonusId=" + bonusId + "&empid=" + empId);


            return false;
        }
        function importPopupProcess() {


            var ret = importPopup("bonusId=" + bonusId);


            return false;
        }
        var skipLoadingCheck = true;
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4 runat="server" id="title">
                    Bonus Calculation
                </h4>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hdnBonusId" runat="server" />
    <div class="contentpanel">
        <div class="contentArea">
            <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
            <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
            <br />
            <div>
                <asp:GridView CssClass="table table-primary mb30 table-bordered table-hover" UseAccessibleHeader="true"
                    ID="gvwGratuityRules" showheaderwhenempty="True" runat="server" AutoGenerateColumns="False"
                    GridLines="None" DataKeyNames="BonusId" showfooterwhenempty="True" Width="1000px">
                    <Columns>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                            HeaderText="Name">
                            <ItemTemplate>
                                <asp:Label ID="Label1" Text='<%#   Eval("Name")%>' runat="server" Width="120px" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                            HeaderText="Bonus Year">
                            <ItemTemplate>
                                <%#     GetYear(Eval("YearId"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                            HeaderStyle-Width="150px" HeaderText="Start Date">
                            <ItemTemplate>
                                <%#     (Eval("StartDate","{0:yyyy-MMM-dd}"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                            HeaderStyle-Width="150px" HeaderText="End Date">
                            <ItemTemplate>
                                <%#     (Eval("EndDate","{0:yyyy-MMM-dd}"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                            HeaderStyle-Width="150px" HeaderText="Distribution Date">
                            <ItemTemplate>
                                <%#     (Eval("DistributionDateEng","{0:yyyy-MMM-dd}"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"
                            HeaderText="Allocated Bonus">
                            <ItemTemplate>
                                <asp:Label ID="Label1" Style='text-align: right' Text='<%#     GetCurrency(Eval("BonusAmount"))%>'
                                    runat="server" Width="100px" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"
                            HeaderText="Distributed Bonus">
                            <ItemTemplate>
                                <asp:Label ID="Label2" Style='text-align: right' Text='<%#     GetCurrency(Eval("DistributedAmount"))%>'
                                    runat="server" Width="100px" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"
                            HeaderText="Total Annual Salary">
                            <ItemTemplate>
                                <asp:Label ID="Label2" Style='text-align: right' Text='<%#     GetCurrency(Eval("TotalAnnualSalary"))%>'
                                    runat="server" Width="100px" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"
                            HeaderText="Bonus %">
                            <ItemTemplate>
                                <asp:Label ID="Label2" Style='text-align: right' Text='<%#    Convert.ToDecimal(Eval("BonusPercent")) * 100%>'
                                    runat="server" Width="60px" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                            HeaderStyle-Width="150px" HeaderText="No of Eligible Employees">
                            <ItemTemplate>
                                <%#     (Eval("NoOfEmployees"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="50px">
                            <ItemTemplate>
                                <input type='image' style='border-width: 0px; vertical-align: sub;' onclick='return insertUpdateGratuityRuleCall(<%# Eval("BonusId") %>);return false;'
                                    src='../images/edit.gif' />
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="odd" />
                    <AlternatingRowStyle CssClass="even" />
                </asp:GridView>
                <br />
                <table style="margin-bottom: 15px;">
                    <tr>
                        <td>
                            <asp:Button OnClick="btnGenerate_Click" CssClass="save" runat="server" ID="b1" Width="150px"
                                Text="List Eligible Employees" OnClientClick='return confirm("Are you sure,you want to generate the employee list for Bonus, imported data will be lost?");' />
                        </td>
                        <td>
                            <asp:Button ID="Button2" runat="server" CssClass="save" CommandName="Generate" Width="90px"
                                Text="Import Data" OnClientClick="importPopupProcess();return false;" />
                        </td>
                        <td>
                            <asp:Button OnClick="btnCalculate_Click" CssClass="save" ID="Button3" runat="server"
                                CommandName="Calculate" OnClientClick="return confirm('Confirm calculate the Bonus?');"
                                Width="120px" Text="Calculate Bonus" />
                        </td>
                        <td>
                            <asp:Button OnClick="btnPostToAddOn_Click" CssClass="save" runat="server" ID="btnPostToAddOn"
                                Width="130px" Text="Post To Add-On" OnClientClick='return confirm("Are you sure,you want to post amount?");' />
                        </td>
                        <td style="padding-left: 82px">
                            Employee
                            <br />
                            <asp:TextBox AutoPostBack="true" Width="180px" OnTextChanged="btnLoad_Click" ID="txtEmpSearchText"
                                runat="server"></asp:TextBox>
                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtEmpSearchText"
                                WatermarkText="Employee Name" WatermarkCssClass="searchBoxText" />
                            <cc1:AutoCompleteExtender EnableCaching="true" ID="AutoCompleteExtenderOrganization"
                                runat="server" MinimumPrefixLength="2" ServiceMethod="GetEmployeeNames" ServicePath="~/PayrollService.asmx"
                                TargetControlID="txtEmpSearchText" CompletionSetCount="10" CompletionInterval="250"
                                CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList"
                                CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
                            </cc1:AutoCompleteExtender>
                        </td>
                        <%-- </td>
                        <td style="padding-left: 10px">
                            Eligibility
                            <br />
                            <asp:DropDownList ID="ddlEligibility" runat="server" Width="100px">
                                <asp:ListItem Text="All" Value="-1" />
                                <asp:ListItem Text="Eligible" Value="1" />
                                <asp:ListItem Text="Not Eligible" Value="0" />
                            </asp:DropDownList>
                        </td>--%>
                        <td style="padding-left: 10px; margin-top: 5px;">
                            <asp:Button ID="Button41" CssClass="update" OnClick="btnLoad_Click" runat="server"
                                CommandName="Load" Width="90px" Text="Load" />
                        </td>
                        <td style="padding-left: 20px; margin-top: 5px;">
                            <asp:Button ID="Button4" CssClass="update" OnClick="btnExport_Click" runat="server"
                                CommandName="Export" Width="90px" Text="Export" />
                        </td>
                    </tr>
                </table>
                <cc1:TabContainer ID="tab" runat="server" AutoPostBack="true" OnActiveTabChanged="btnTab_click"
                    CssClass="yui" ActiveTabIndex="0">
                    <cc1:TabPanel runat="server" ID="tabSummary">
                        <HeaderTemplate>
                            <asp:Label runat="server" ID="lblEligible" Text="Eligible List" />
                        </HeaderTemplate>
                    </cc1:TabPanel>
                    <cc1:TabPanel runat="server" ID="tabDetails">
                        <HeaderTemplate>
                            <asp:Label runat="server" ID="lblInEligible" Text="Ineligible List" />
                        </HeaderTemplate>
                    </cc1:TabPanel>
                </cc1:TabContainer>
                <cc2:EmptyDisplayGridView CssClass="table table-primary mb30 table-bordered table-hover"
                    Style="margin-bottom: -3px;" UseAccessibleHeader="true" ShowHeaderWhenEmpty="True"
                    ID="gvwEmployees" runat="server" DataKeyNames="EmployeeId" AutoGenerateColumns="False"
                    AllowSorting="True" OnSorting="gvwEmployees_Sorting" ShowFooterWhenEmpty="False"
                    Width="100%" GridLines="None">
                    <Columns>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                            HeaderStyle-Width="50px" HeaderText="SN">
                            <ItemTemplate>
                                <%#     (Eval("SN"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                            HeaderStyle-Width="60px" HeaderText="EIN">
                            <ItemTemplate>
                                <%#     (Eval("EmployeeId"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                            HeaderStyle-Width="70px" HeaderText="Acc No">
                            <ItemTemplate>
                                <%#     (Eval("AccountNo"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                            HeaderStyle-Width="120px" HeaderText="Employee">
                            <ItemTemplate>
                                <%#     (Eval("Name"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                            HeaderStyle-Width="100px" HeaderText="Status">
                            <ItemTemplate>
                                <%#     (Eval("StatusName"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                            HeaderStyle-Width="120px" HeaderText="Joined">
                            <ItemTemplate>
                                <%#    Eval("JoinDate","{0:yyyy-MMM-dd}")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                            HeaderStyle-Width="120px" HeaderText="Retire Date">
                            <ItemTemplate>
                                <%#    Eval("RetirementDate")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                            HeaderStyle-Width="100px" HeaderText="Standard Days">
                            <ItemTemplate>
                                <%#     (Eval("StdWorkDays"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                            HeaderStyle-Width="100px" HeaderText="Unpaid Leave">
                            <ItemTemplate>
                                <%#     (Eval("UnpaidLeave"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                            HeaderStyle-Width="100px" HeaderText="Stop Pay Days">
                            <ItemTemplate>
                                <%#     (Eval("OtherUneligibleDays"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                            HeaderStyle-Width="100px" HeaderText="Actual Work Days">
                            <ItemTemplate>
                                <%#     (Eval("ActualWorkdays"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                            HeaderStyle-Width="100px" HeaderText="Eligibility">
                            <ItemTemplate>
                                <%#     GetEligibilityType(Eval("Eligibility"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--show for advance, hide for final--%>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                            HeaderStyle-Width="80px" HeaderText="Proportion">
                            <ItemTemplate>
                                <%#     (Eval("Proportion"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--  show for final,hide for advance--%>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                            HeaderStyle-Width="80px" HeaderText="Calculation Type">
                            <ItemTemplate>
                                <%#     (Eval("CalculationType"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                       
                        <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"
                            HeaderStyle-Width="100px" HeaderText="Monthly Salary">
                            <ItemTemplate>
                                <%#     (Eval("MonthlySalary", "{0:N2}"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                            HeaderStyle-Width="100px" HeaderText="Group">
                            <ItemTemplate>
                                <%#     (Eval("Cat"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--  <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"
                            HeaderStyle-Width="100px" HeaderText="Calculated Bonus">
                            <ItemTemplate>
                                <%#     (Eval("Bonus", "{0:N2}"))%>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                        <%--<asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"
                            HeaderStyle-Width="100px" HeaderText="SWF Bonus Amount Monthly Based">
                            <ItemTemplate>
                                <%#     (Eval("SWFMonthlyBased", "{0:N2}"))%>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"
                            HeaderStyle-Width="100px" HeaderText="Maximum Bonus">
                            <ItemTemplate>
                                <%#     (Eval("MaxBonus", "{0:N2}"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%-- <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"
                            HeaderStyle-Width="100px" HeaderText="Advance Bonus Monthly Salary Based">
                            <ItemTemplate>
                                <%#     (Eval("AdvanceBonusMonthlySalaryBased", "{0:N2}"))%>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                        <%--<asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"
                            HeaderStyle-Width="100px" HeaderText="Advance Bonus">
                            <ItemTemplate>
                                <%#     (Eval("AdvanceBonus", "{0:N2}"))%>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"
                            HeaderStyle-Width="80px" HeaderText="Remaining Bonus">
                            <ItemTemplate>
                                <%#     (Eval("BonusPaid", "{0:N2}"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                       
                        <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"
                            HeaderStyle-Width="100px" HeaderText="SST">
                            <ItemTemplate>
                                <%#     (Eval("SST", "{0:N2}"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"
                            HeaderStyle-Width="100px" HeaderText="TDS">
                            <ItemTemplate>
                                <%#     (Eval("TDS", "{0:N2}"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"
                            HeaderStyle-Width="100px" HeaderText="Net Paid">
                            <ItemTemplate>
                                <%#     (Eval("NetPaid", "{0:N2}"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="50px">
                            <ItemTemplate>
                                <input type='image' style='border-width: 0px; vertical-align: sub;' onclick='return updateBonusPopup(<%# Eval("BonusId") %>,<%# Eval("EmployeeId") %>);return false;'
                                    src='../images/edit.gif' />
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="odd" />
                    <AlternatingRowStyle CssClass="even" />
                    <EmptyDataTemplate>
                        <b>No employee records found. </b>
                    </EmptyDataTemplate>
                </cc2:EmptyDisplayGridView>
                <uc1:PagingCtl ID="pagingCtl" OnNextRecord="btnNext_Click" OnPrevRecord="btnPrevious_Click"
                    OnDropDownShowRecordsChanged="ddlRecords_SelectedIndexChanged" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>
