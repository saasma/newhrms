﻿<%@ Page Title="Location Transfer List" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="LocationTransferList.aspx.cs" Inherits="Web.CP.LocationTransferList" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="InfoMsgCtl" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        function popupAddLocationTransfer() {
            var ret = popLocationTransferDetls();

            if (typeof (ret) != 'undefined') {
                if (ret == 'Reload') {
                    __doPostBack('Reload', '');
                }
            }

            return false;
        }

        function reloadLocationTransferList(childWindow) {
            childWindow.close();
            __doPostBack('Reload', '');
        }

        function popupEditLocationTransferDetls(locationHistoryId) {
            var ret = popLocationTransferDetls('Id=' + locationHistoryId);

            if (typeof (ret) != 'undefined') {
                if (ret == 'Reload') {
                    __doPostBack('Reload', '');
                }
            }
            return false;
        }
    
    </script>
    <style type="text/css">
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
    </style>
    <link href="../css/core.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Location Transfer List
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <uc2:InfoMsgCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />

        <div class="attribute" style="padding:10px">
            <table>
                <tr>
                    <td>
                        <strong>Search </strong>&nbsp;
                        <asp:TextBox Width="160px" ID="txtEmpSearchText" runat="server" OnTextChanged="txtEmpSearchText_TextChanged"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Button ID="btnLoad" CssClass="btn btn-default btn-sm" Style="margin-left: 10px; Width: 100px;" runat="server"
                            OnClick="btnLoad_Click" Text="Load" />
                        <cc1:TextBoxWatermarkExtender ID="TBWE2" runat="server" TargetControlID="txtEmpSearchText"
                            WatermarkText="Employee name" WatermarkCssClass="searchBoxText" />
                    </td>
                </tr>
            </table>
        </div>
        <div style="clear: both;" />
        <div style="text-align: left; clear: both">
            <asp:Button ID="btnSave" Width="170px" Style="margin-left: 0px;" CausesValidation="false"
                CssClass="btn btn-primary btn-sm" runat="server" Text="New Location Transfer" OnClientClick="popupAddLocationTransfer();return false;" />
        </div>
        <div class="clear" style="width: 1180px; margin-top: 10px;">
            <cc2:EmptyDisplayGridView CssClass="tableLightColor" UseAccessibleHeader="true" ID="gvwList"
                runat="server" AutoGenerateColumns="False" DataKeyNames="LocationHistoryId,EmployeeId"
                GridLines="None" ShowFooterWhenEmpty="False" ShowHeaderWhenEmpty="True" Width="100%" OnRowDeleting="gvwList_OnRowDeleting">
                <Columns>
                    <asp:BoundField HeaderStyle-Width="150px" HeaderStyle-HorizontalAlign="Left" DataField="FromDate"
                        HeaderText="Date" />
                    <asp:BoundField HeaderStyle-Width="150px" HeaderStyle-HorizontalAlign="Left" DataField="FromDateEng"
                        DataFormatString="{0:yyyy-M-dd}" HeaderText="Date(AD)" />
                    <asp:BoundField HeaderStyle-HorizontalAlign="Center" DataField="EmployeeId" HeaderText="EIN"
                        ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="80px" />
                    <asp:BoundField HeaderStyle-Width="200px" HeaderStyle-HorizontalAlign="Left" DataField="EmployeeName"
                        HeaderText="Name" />
                    <asp:BoundField HeaderStyle-Width="150px" HeaderStyle-HorizontalAlign="Left" DataField="FromLocation"
                        HeaderText="From Location" />
                    <asp:BoundField HeaderStyle-Width="150px" HeaderStyle-HorizontalAlign="Left" DataField="ToLocation"
                        HeaderText="To Location" />
                    <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-HorizontalAlign="Center" HeaderText="Edit">
                        <ItemTemplate>
                            <asp:ImageButton ID="btnEdit" ImageUrl="~/images/edit.gif" runat="server" ToolTip="Edit"
                                OnClientClick='<%# "return popupEditLocationTransferDetls(" +  Eval("LocationHistoryId") + ");" %>'
                                AlternateText="Edit" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Delete" HeaderStyle-Width="20px" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:ImageButton ID="ImageButton2" OnClientClick="return confirm('Do you want to confirm delete the record?')" ToolTip="Delete"
                                runat="server" CommandName="Delete" ImageUrl="~/images/delete.gif" />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:TemplateField>
                </Columns>
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                <EmptyDataTemplate>
                    No records.
                </EmptyDataTemplate>
            </cc2:EmptyDisplayGridView>
            <uc1:PagingCtl ID="pagingCtl" OnNextRecord="btnNext_Click" OnPrevRecord="btnPrevious_Click"
                OnDropDownShowRecordsChanged="ddlRecords_SelectedIndexChanged" runat="server" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
