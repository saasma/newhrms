﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Manager;
using Utils.Helper;
using BLL;
using DAL;
using Utils.Web;
using BLL.Base;
using BLL.BO;
using Bll;
using Web.Helper;

namespace Web.CP
{
    public partial class FestivalPaymentDetails : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.AdministratorTools;
            }
        }

        CommonManager commonMgr = new CommonManager();
        PayManager payMgr = new PayManager();

        #region "Control state related"
        private string _sortBy = "EIN";
        private SortDirection _sortDirection = SortDirection.Ascending;

        protected override void OnInit(EventArgs e)
        {
            Page.RegisterRequiresControlState(this);
            base.OnInit(e);
        }
        protected override void LoadControlState(object savedState)
        {
            object[] rgState = (object[])savedState;
            base.LoadControlState(rgState[0]);
            _sortBy = rgState[1].ToString();
            _sortDirection = (SortDirection)rgState[2];
            //_tempCurrentPage = (int)rgState[2];

        }
        protected override object SaveControlState()
        {
            object[] rgState = new object[3];
            rgState[0] = base.SaveControlState();
            rgState[1] = _sortBy;
            rgState[2] = _sortDirection;
            // rgState[2] = _tempCurrentPage;
            return rgState;
        }
        #endregion

        int festivalId = 0;

        protected void Page_Load(object sender, EventArgs e)
        {

            festivalId = int.Parse(Request.QueryString["id"]);

            FestivalPaymentPeriod overtime = commonMgr.GetFestivalPaymentPeriod(festivalId);
            if (!IsPostBack)
            {
                // show hide for arrear type
                if (overtime.IsArrearType != null && overtime.IsArrearType.Value)
                {
                    gvwEmployees.Columns[10].HeaderText = "Diff " + gvwEmployees.Columns[10].HeaderText;
                    gvwEmployees.Columns[11].HeaderText = "Diff " + gvwEmployees.Columns[11].HeaderText;
                    gvwEmployees.Columns[12].HeaderText = "Diff " + gvwEmployees.Columns[12].HeaderText;
                    gvwEmployees.Columns[13].HeaderText = "Diff " + gvwEmployees.Columns[13].HeaderText;

                }
            }


            if (!IsPostBack)
            {
                Initialise();
            }
            if (Request.Form["__EVENTTARGET"] != null && Request.Form["__EVENTTARGET"].Equals("Reload"))
            {
                LoadOvertime(festivalId);        
            }




           JavascriptHelper.AttachPopUpCode(Page, "insertUpdateGratuityRule", "EditEmployeeFestival.aspx", 500, 400);
           Page.ClientScript.RegisterClientScriptBlock(GetType(), "sdfsdf", "festivalId = " + festivalId + ";", true);

           
        }

        public int GetFestival()
        {
            return festivalId;
        }
        public void LoadDesignation()
        {
            List<TextValue> list = new List<TextValue>();

            list = CommonManager.GetUniqueDesignationList();

            list.Insert(0, new TextValue { Text = "--Select Designation--", Value = "--Select Designation--" });

            ddlDesignation.DataSource = list;
            ddlDesignation.DataBind();
        }

        void Initialise()
        {
            FestivalPaymentPeriod overtime = commonMgr.GetFestivalPaymentPeriod(festivalId);

            title.InnerHtml += overtime.Name + " for " + CommonManager.GetPayrollPeriod(overtime.PayrollPeriodId.Value).Name;

            ddlBranch.DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            ddlBranch.DataBind();

            LoadDesignation();
            IncomeList();
            LoadGratuity();

            LoadOvertime(festivalId);

            if (overtime.PayrollPeriodId != null && overtime.PayrollPeriodId != -1 && overtime.PayrollPeriodId
                != CommonManager.GetLastPayrollPeriod().PayrollPeriodId)
            {
                btnCalculate.Visible = false;
                btnPostToAddOn.Visible = false;
            }


           
        }
        void LoadGratuity()
        {
            

           
        }

        public string GetYear(object yearid)
        {
            FinancialDate year = CommonManager.GetFiancialDateById(int.Parse(yearid.ToString()));
            year.SetName(IsEnglish);
            return year.Name;
        }

        public string GetApplicableTo(object val)
        {
           
            return "";
        }


        protected void btnPostToAddOn_Click(object sender, EventArgs e)
        {
            CommonManager.PostFestivalDashainToAddOn(festivalId);

            divMsgCtl.InnerHtml = "Amount posted to add-on.";
            divMsgCtl.Hide = false;
        }

        protected void gvwClass_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int GratuityClassId = (int)-1 ;

            if (GratuityClassId != 0)
            {
                bool deleted = CommonManager.DeleteGratuityClass(GratuityClassId);

                if (deleted)
                {

                    divMsgCtl.InnerHtml = "Class deleted.";
                    divMsgCtl.Hide = false;
                }
                else
                {
                    divMsgCtl.InnerHtml = "Class is being used, can not be deleted.";
                    divMsgCtl.Hide = false;
                }

            }
            LoadGratuity();
        }

        protected void gvwDepartments_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int GratuityRuleId = -1;

            if (GratuityRuleId != 0)
            {
                CommonManager.DeleteGratuityRule(GratuityRuleId);

                divMsgCtl.InnerHtml = Resources.Messages.GratutiyRuleDeletedMsg;
                divMsgCtl.Hide = false;
                
                
            }
            LoadGratuity();
        }

        void IncomeList()
        {
          
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

           
        }

        public string GetEligibilityType(object type)
        {
            int val = int.Parse(type.ToString());

            if (val == 0)
                return "Not Eligible";
            if (val == 1)
                return BonusEligiblity.Full.ToString(); ;

            return BonusEligiblity.Proportionate.ToString();
        }

        protected void btnAddNew_Click(object sender, EventArgs e)
        {
           
        }


        public void btnTab_click(object sender, EventArgs e)
        {
            LoadOvertime(festivalId);
        }

        void LoadOvertime(int bonusId)
        {
            hdnBonusId.Value = bonusId.ToString();


            int eligiblity = -1;

           

            int totalRecords = 0;

            List<GetFestivalPaymentEmployeeListResult> list = CommonManager.GetFestivalPaymentList(festivalId
                , pagingCtl.CurrentPage - 1, int.Parse(pagingCtl.DDLRecords.SelectedValue), ref totalRecords,txtEmpSearchText.Text.Trim()
                , eligiblity, ddlDesignation.SelectedItem.Text, int.Parse(ddlBranch.SelectedValue));

            gvwEmployees.DataSource = list;
            gvwEmployees.DataBind();

              if (totalRecords == 0)
                pagingCtl.Visible = false;
            else
                pagingCtl.Visible = true;
            pagingCtl.UpdatePagingBar(totalRecords);

            //int totalRecords = 0;
            //gvwList.DataSource = ProjectManager.GetProjectList(
            //    GetEngDate(calFilterFrom.SelectedDate.ToString()), GetEngDate(calFilterTo.SelectedDate.ToString()),
            //    chkAllProjects.Checked, txtSearch.Text.Trim(), pagintCtl.CurrentPage, int.Parse(pagintCtl.DDLRecords.SelectedValue), ref totalRecords);


            //gvwList.DataBind();



        }

      

        protected void gvwEmployees_Sorting(object sender, GridViewSortEventArgs e)
        {

            if (e.SortExpression != _sortBy)
            {
                e.SortDirection = SortDirection.Ascending;
                _sortDirection = e.SortDirection;
            }
            else if (_sortDirection == SortDirection.Ascending)
            {
                e.SortDirection = SortDirection.Descending;
            }
            else if (_sortDirection == SortDirection.Descending)
            {
                e.SortDirection = SortDirection.Ascending;
            }

            _sortDirection = e.SortDirection;

            _sortBy = e.SortExpression;


            e.Cancel = true;

            if (!string.IsNullOrEmpty(hdnBonusId.Value))
                //pagintCtl.CurrentPage = 1;
                //_tempCurrentPage = 1;
                LoadOvertime(int.Parse(hdnBonusId.Value));
        }


        protected void btnPrevious_Click()
        {
            pagingCtl.CurrentPage -= 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage -= 1;
            LoadOvertime(festivalId);
        }

        protected void btnNext_Click()
        {
            pagingCtl.CurrentPage += 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage += 1;
            LoadOvertime(festivalId);
        }

        protected void ddlRecords_SelectedIndexChanged()
        {
            //SessionManager.EmployeeDisplayRecodsPerPage = int.Parse(pagintCtl.DDLRecords.SelectedValue);
            //_tempCurrentPage = 1;
            pagingCtl.CurrentPage = 1;
            LoadOvertime(festivalId);
        }

        protected void btnLoad_Click(object sender, EventArgs e)
        {
            LoadOvertime(festivalId);
        }
        protected void btnExport_Click(object sender, EventArgs e)
        {
            int total = 0;

            int eligiblity = -1;


            //OvertimePeriod overtime = commonMgr.GetOvertimePeriod(overtimeId);



            List<GetFestivalPaymentEmployeeListResult> list = CommonManager.GetFestivalPaymentList(festivalId
                , 0, 9999999, ref total, txtEmpSearchText.Text.Trim()
                , eligiblity, ddlDesignation.SelectedItem.Text, int.Parse(ddlBranch.SelectedValue));


            Dictionary<string, string> renameList = new Dictionary<string, string>() { {"UPLAbsent","LWP / UPL"},{"TotalWorkDays","Work Days" },{"JoinDateText","Join Date"},{"Amount1","Basic Salary"},{"Amount2","Allowance"}
                        ,{"Amount3","Incentive Allowance"},{"Amount","Dashain Amount"},{"PreventResetText","Skip in ReCalculate"}
                                };

            FestivalPaymentPeriod overtime = commonMgr.GetFestivalPaymentPeriod(festivalId);
            if (overtime.IsArrearType != null && overtime.IsArrearType.Value)
            {
                renameList = new Dictionary<string, string>() { {"UPLAbsent","LWP / UPL"},{"TotalWorkDays","Work Days" },{"JoinDateText","Join Date"},{"Amount1","Diff Basic Salary"},{"Amount2","Diff Allowance"}
                        ,{"Amount3","Diff Incentive Allowance"},{"Amount","Diff Dashain Amount"},{"PreventResetText","Skip in ReCalculate"}
                                };
            }


            ExcelHelper.ExportToExcel("Dashain List", list,
                new List<String>() { "JoinDate","PreventReset","TotalRows" },
                new List<String>() { },
                renameList,
                new List<string>() { "Amount1", "Amount2", "Amount3", "Amount", "SST", "TDS", "NetPay" },
                new Dictionary<string, string>() { {"Dashain List ",""} },
                new List<string> { "SN","EIN", "AccountNo", "Name", "Designation", "Branch","JoinDateText","UPLAbsent","StopPay",
                    "TotalWorkDays","Amount1","Amount2","Amount3","Amount","SST","TDS","NetPay","Remarks","PreventResetText"});
        }
        protected void btnGenerate_Click(object sender, EventArgs e)
        {
            bool status = CommonManager.GenerateFestivalPayment(festivalId);

            if (status)
            {
                divMsgCtl.InnerHtml = "Employee list generated for dashain.";
                divMsgCtl.Hide = false;

                LoadOvertime(festivalId);

                
            }
            else
            {
                divWarningMsg.InnerHtml = "Dashain already distributed, can not be changed.";
                divWarningMsg.Hide = false;
            }
        }

        protected void btnCalculate_Click(object sender, EventArgs e)
        {





            bool status = CommonManager.GenerateFestivalPayment(festivalId);

            if (status)
            {

                divMsgCtl.InnerHtml = "Dashain calculated.";
                divMsgCtl.Hide = false;

                LoadGratuity();

                LoadOvertime(festivalId);
            }
            else
            {
                divWarningMsg.InnerHtml = "Dashain already distributed, can not be changed.";
                divWarningMsg.Hide = false;
            }
        }
    }
}
