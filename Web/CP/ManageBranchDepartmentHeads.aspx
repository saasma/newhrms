﻿<%@ Page Title="Manage Branch Department Heads" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="ManageBranchDepartmentHeads.aspx.cs" Inherits="Web.CP.ManageBranchDepartmentHeads" %>

<%--<%@ Register Src="~/NewHR/UserControls/EmployeeDetailsCtl.ascx" TagName="EmployeeDetailsCtl"
    TagPrefix="uc1" %>--%>
<%--#TODO: Group the Branc-Department-Department Head with Branch Column--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        var CommandHandler1 = function(command, record){
            <%= hiddenValue.ClientID %>.setValue(record.data.BranchID);
            <%= hiddenValueDept.ClientID %>.setValue(record.data.DepartmentID);
                if(command=="Edit")
                {
                    <%= btnEditLevel.ClientID %>.fireEvent('click');
                }
             }
             


             var totalCost = function (records) {
            var i = 0,
                length = records.length,
                total = 0,
                record;

            for (; i < length; ++i) {
                record = records[i];
                total += record.get('Quantity') * record.get('Rate');
            }

            return total;
        };
        
    </script>
    <style>
        table#albums
        {
            border-collapse: separate;
            border-spacing: 0 10px;
        }
        
        table#albums1
        {
            border-collapse: separate;
            border-spacing: 0 40px;
        }
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <ext:Hidden runat="server" ID="hiddenValue" />
    <ext:Hidden runat="server" ID="hiddenValueDept">
    </ext:Hidden>
    <ext:LinkButton runat="server" Hidden="true" ID="btnEditLevel">
        <DirectEvents>
            <Click OnEvent="btnEditLevel_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Define Department Heads
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="separator bottom">
        </div>
        <div class="innerLR">
            <%--<uc1:EmployeeDetailsCtl Id="EmployeeDetailsCtl1" runat="server" />--%>
            <ext:GridPanel StyleSpec="margin-top:15px;" ID="GridLevels" runat="server" Width="750">
                <Store>
                    <ext:Store ID="storeBranchDepartment" runat="server" PageSize="50" OnReadData="BindBranchDept">
                        <Model>
                            <ext:Model ID="Model4" runat="server">
                                <Fields>
                                    <ext:ModelField Name="BranchID" Type="String" />
                                    <ext:ModelField Name="DepartmentID" Type="string" />
                                    <ext:ModelField Name="HeadEmployeeId" Type="Date" />
                                    <ext:ModelField Name="BranchName" Type="string" />
                                    <ext:ModelField Name="DepartmentName" Type="string" />
                                    <ext:ModelField Name="HeadName" Type="string" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:Column ID="Column10" Sortable="false" MenuDisabled="true" runat="server" Text="Branch"
                            Width="175" Align="Left" DataIndex="BranchName" />
                        <ext:Column ID="Column3" Sortable="false" MenuDisabled="true" runat="server" Text="Department"
                            Width="175" Align="Left" DataIndex="DepartmentName" />
                        <ext:Column ID="Column4" Sortable="false" MenuDisabled="true" runat="server" Text="Main Person"
                            Width="250" Align="Left" DataIndex="HeadName" />
                        <ext:CommandColumn runat="server" Text="Actions" Align="Center" Width="125">
                            <Commands>
                                <ext:GridCommand Text="<i></i>" Icon="pencil" CommandName="Edit" />
                            </Commands>
                            <Listeners>
                                <Command Handler="CommandHandler1(command,record);" />
                            </Listeners>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
                <SelectionModel>
                    <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                </SelectionModel>
                <BottomBar>
                    <ext:PagingToolbar ID="paging1" runat="server">
                    </ext:PagingToolbar>
                </BottomBar>
            </ext:GridPanel>
        </div>
        <div class="alert alert-info" style="margin-top: 15px; height: 50px;">
            <div style="float: right;">
                <ext:Button runat="server" AutoPostBack="true" OnClick="btnExport_Click" ID="btnExport"
                    Text="<i></i>Export To Excel">
                </ext:Button>
            </div>
        </div>
    </div>
    <ext:Window ID="Window1" runat="server" Title="Set Department Head" Icon="Application"
        Width="425" Height="240" BodyPadding="5" Hidden="true" Modal="true">
        <Content>
            <ext:DisplayField runat="server" ID="txtBranch" LabelAlign="Left" FieldLabel="Branch"
                LabelWidth="140">
            </ext:DisplayField>
            <br />
            <ext:DisplayField runat="server" ID="txtDepartment" LabelAlign="Left" FieldLabel="Department"
                LabelWidth="140">
            </ext:DisplayField>
            <br />
            <ext:ComboBox ID="cmbEmployee" ForceSelection="true" runat="server" Width="400" FieldLabel="Head Of Department"
                LabelAlign="Left" LabelWidth="140" QueryMode="Local" DisplayField="NameEIN" ValueField="EmployeeId">
                <Store>
                    <ext:Store ID="storeEmployeeList" runat="server">
                        <Fields>
                            <ext:ModelField Name="EmployeeId" Type="String" />
                            <ext:ModelField Name="NameEIN" />
                        </Fields>
                    </ext:Store>
                </Store>
                <Triggers>
                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                </Triggers>
                <Listeners>
                    <Select Handler="this.getTrigger(0).show();" />
                    <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                    <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                </Listeners>
            </ext:ComboBox>
            <div style="margin-top: 10px;">
                <ext:Button ID="btnSaveAndLater" Cls="btn btn-primary" runat="server" Text="&nbsp;&nbsp;Save&nbsp;&nbsp;"
                    ValidationGroup="InsertUpdate">
                    <DirectEvents>
                        <Click OnEvent="ButtonNext_Click">
                            <EventMask ShowMask="true" />
                        </Click>
                    </DirectEvents>
                    <Listeners>
                        <Click Handler="valGroup = 'InsertUpdate'; return CheckValidation();">
                        </Click>
                    </Listeners>
                </ext:Button>
            </div>
        </Content>
    </ext:Window>
   <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="cmbEmployee"
        ErrorMessage="Employee must be selected." Display="None" ValidationGroup="InsertUpdate">
    </asp:RequiredFieldValidator>--%>
</asp:Content>
