<%@ Page Title="Add On Setting" Language="C#" MasterPageFile="~/Master/ForPopupPage.Master"
    AutoEventWireup="true" CodeBehind="AddOnSetting.aspx.cs" Inherits="Web.CP.AddOnSetting" %>

<%@ Register Src="~/UserControls/Gratuity/AEGratuityCtl.ascx" TagName="AEGratuityCtl"
    TagPrefix="uc1" %>
<%@ Register Src="~/Controls/ErrorMsgCtl.ascx" TagName="ErrorMsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningMsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        function closePopup() {

            window.opener.refreshWindow1(window);

        }

    
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <div class="popupHeader">
        <h2 class="headlinespop">
            Manage Add On</h2>
    </div>
    <div style="margin-left: 15px">
        <uc2:WarningMsgCtl Width="300px" ID="divWarningMsg" EnableViewState="false" Hide="true"
            runat="server" />
    </div>
    <fieldset style='margin-top: 15px'>
        <table>
            <tr>
                <td class="lf">
                    Add On Name
                </td>
                <td>
                    <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ValidationGroup="Gratuity" ID="RequiredFieldValidator3"
                        Display="None" runat="server" ErrorMessage="Name is required." ControlToValidate="txtName">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="lf">
                    <strong>Add On Type</strong>
                </td>
                <td>
                    <asp:DropDownList Width="170px" AppendDataBoundItems="true" ID="ddlType" runat="server">
                        <asp:ListItem Text="Default Type" Value="-1" />
                        <asp:ListItem Text="Leave Fare" Value="1" />
                         <asp:ListItem Text="Arrear Type" Value="2" />
                         <asp:ListItem Text="Rectification" Value="3" />
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="lf">
                    <strong>Show After</strong>
                </td>
                <td>
                   <asp:DropDownList Width="170px" AppendDataBoundItems="true" DataTextField="Name"
            DataValueField="PayrollPeriodId" ID="ddlShowAfterPeriod" runat="server">
        </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="lf" colspan="2">
                    <asp:CheckBox ID="chkShowInPayslip" runat="server" Text="Show in Payslip" />
                </td>
            </tr>
        </table>
        <br />
        <asp:Button ID="btnSave" CssClass="save" ValidationGroup="Gratuity" OnClientClick="valGroup='Gratuity';return CheckValidation();"
            runat="server" Text="Save" OnClick="btnSave_Click" />
        <asp:Button ID="btnDelete" CssClass="save" ValidationGroup="Gratuity" OnClientClick="return confirm('Confirm delete the addon?');"
            runat="server" Text="Delete" OnClick="btnDelete_Click" />
        <input id="Button1" type="button" class="cancel" value="Cancel" onclick="window.close();" />
        <%--for displaying __doPostBack--%>
        <asp:LinkButton Style="display: none" ID="LinkButton1" runat="server">LinkButton</asp:LinkButton>
    </fieldset>
</asp:Content>
