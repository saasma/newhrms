﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using Utils.Helper;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Calendar;
using System.IO;
using Utils;


namespace Web.CP
{
    public partial class LocationTransferPopUp : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialise();
            }
        }

        private void Initialise()
        {
            hdnEmployeeId.Value = "";
            ddlLocation.SelectedIndex = 0;
            BindLocation();

            if (Request.QueryString["Id"] != null)
            {
                int locationHistoryId = int.Parse(Request.QueryString["Id"].ToString());
                hdnLocationHistoryId.Value = Request.QueryString["Id"];
                LoadEmployeeFromQueryString(locationHistoryId);
            }
        }

        private void BindLocation()
        {
            List<ELocation> list = ListManager.GetELocationList();

            ELocation obj = new ELocation() { LocationId = 0, Name = "" };
            list.Insert(0, obj);

            ddlLocation.DataSource = list;
            ddlLocation.DataBind();
        }

        private void LoadEmployeeFromQueryString(int locationHistoryId)
        {
            ELocationHistory obj = NewHRManager.GetELocationHistoryById(locationHistoryId);
            if (obj != null)
            {
                ddlLocation.SelectedValue = obj.LocationId.ToString();
                calFromDate.Text = obj.FromDate;

                if (obj.Notes != null)
                    txtNote.Text = obj.Notes;

                LoadEmployeeDetails(obj.EmployeeId.Value);

                hdnEmployeeId.Value = obj.EmployeeId.Value.ToString();

                btnSave.Text = Resources.Messages.Update;
            }
        }

        public void LoadEmployeeDetails(int employeeId)
        {
            EEmployee emp = EmployeeManager.GetEmployeeById(employeeId);

            if (Request.QueryString["Id"] != null)
            {
                txtEmpSearchText.Text = emp.Name;
                txtEmpSearchText.Enabled = false;
            }

            if (emp == null)
            {
                buttonsBar.Visible = false;
                divWarningMsg.InnerHtml = "Please select an employee.";
                divWarningMsg.Hide = false;
                return;
            }
            else
                buttonsBar.Visible = true;

            // Photo
            bool hasPhoto = false;
            if (emp.HHumanResources.Count > 0)
            {
                if (!string.IsNullOrEmpty(emp.HHumanResources[0].UrlPhotoThumbnail))
                {
                    image.ImageUrl = Page.ResolveUrl(Config.UploadLocation + "/" + emp.HHumanResources[0].UrlPhotoThumbnail);
                    if (File.Exists(Server.MapPath(image.ImageUrl)))
                        hasPhoto = true;
                }
            }
            if (!hasPhoto)
            {
                image.ImageUrl = Page.ResolveUrl(Web.Helper.WebHelper.GetEmployeeNoImage(emp.EmployeeId));
            }

            lblName.Text = emp.Title + " " + emp.Name;
            lblBranch.Text = emp.Branch.Name;
            lblDepartment.Text = emp.Department.Name;

            int years, months, days, hours;
            int minutes, seconds, milliseconds;
            ServicePeroid firstStatus = EmployeeManager.GetServicePeroidDate(emp.EmployeeId);
            GetElapsedTime(firstStatus.FromDateEng, BLL.BaseBiz.GetCurrentDateAndTime(), out years, out months, out days, out hours, out minutes, out seconds, out milliseconds);

            lblSince.Text = "Since " +
                (IsEnglish ? firstStatus.FromDateEng.ToShortDateString() : firstStatus.FromDate + " (" + firstStatus.FromDateEng.ToShortDateString() + ")");
            lblTime.Text = string.Format("For {0} years {1} months {2} days",
                years, months, days);
            
        }

        // Return the number of years, months, days, hours, minutes, seconds,
        // and milliseconds you need to add to from_date to get to_date.
        private void GetElapsedTime(DateTime from_date, DateTime to_date,
            out int years, out int months, out int days, out int hours,
            out int minutes, out int seconds, out int milliseconds)
        {
            // If from_date > to_date, switch them around.
            if (from_date > to_date)
            {
                GetElapsedTime(to_date, from_date,
                    out years, out months, out days, out hours,
                    out minutes, out seconds, out milliseconds);
                years = -years;
                months = -months;
                days = -days;
                hours = -hours;
                minutes = -minutes;
                seconds = -seconds;
                milliseconds = -milliseconds;
            }
            else
            {
                // Handle the years.
                years = to_date.Year - from_date.Year;

                // See if we went too far.
                DateTime test_date = from_date.AddMonths(12 * years);
                if (test_date > to_date)
                {
                    years--;
                    test_date = from_date.AddMonths(12 * years);
                }

                // Add months until we go too far.
                months = 0;
                while (test_date <= to_date)
                {
                    months++;
                    test_date = from_date.AddMonths(12 * years + months);
                }
                months--;

                // Subtract to see how many more days,
                // hours, minutes, etc. we need.
                from_date = from_date.AddMonths(12 * years + months);
                TimeSpan remainder = to_date - from_date;
                days = remainder.Days;
                hours = remainder.Hours;
                minutes = remainder.Minutes;
                seconds = remainder.Seconds;
                milliseconds = remainder.Milliseconds;
            }
        }

        protected void btnLoadEmpDetls_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(hdnEmployeeId.Value))
                return;
            int employeeId = int.Parse(hdnEmployeeId.Value);
            LoadEmployeeDetails(employeeId);
        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                ELocationHistory obj = new ELocationHistory();

                if (!string.IsNullOrEmpty(hdnLocationHistoryId.Value))
                    obj.LocationHistoryId = int.Parse(hdnLocationHistoryId.Value);

                obj.EmployeeId = int.Parse(hdnEmployeeId.Value);
                obj.LocationId = int.Parse(ddlLocation.SelectedValue);

                obj.FromDate = calFromDate.Text.Trim();
                obj.FromDateEng = BLL.BaseBiz.GetEngDate(obj.FromDate, IsEnglish);

                if (!string.IsNullOrEmpty(txtNote.Text.Trim()))
                    obj.Notes = txtNote.Text.Trim();

                Status status = NewHRManager.SaveUpdateELocationHistory(obj);

                if (status.IsSuccess)
                {
                    JavascriptHelper.DisplayClientMsg("Location transfer saved successfully.", Page, "closePopup();");
                }
                else
                {
                    divWarningMsg.InnerHtml = status.ErrorMessage;
                    divWarningMsg.Hide = false;
                }
            }
        }
               

    }
}