﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BLL.Manager;
using BLL;

namespace Web.CP
{
    public partial class HRRating : System.Web.UI.Page
    {
        public string SaveMode
        {
            get { return this.hdSaveMode.Value; }
            set { this.hdSaveMode.Value = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                Initialize();
            }
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            if (SaveMode.Equals("0"))
            {
                string errMsg = string.Empty;
                EvalulationRating rating = new EvalulationRating ();
                rating.Name = txtRatingName.Text.Trim();
                rating.Order = int.Parse(txtOrder.Text.Trim() == "" ? "0" : txtOrder.Text.Trim());
                rating.CompanyId = SessionManager.CurrentCompanyId;
                rating.CreatedOn = System.DateTime.Now;
                rating.CreatedBy = SessionManager.CurrentLoggedInEmployeeId;
                if (PositionGradeStepAmountManager.SaveRating(rating, ref errMsg))
                {
                    ClearField();
                    BindPosition();
                }
                else
                {
                    divWarningMsg.InnerHtml = errMsg;
                    divWarningMsg.Hide = false;
                }
            }
            else
            {
                string errMsg = string.Empty;
                EvalulationRating rating = new EvalulationRating();
                rating.RatingId = int.Parse(hdPositionId.Value);
                rating.Name = txtRatingName.Text.Trim();
                rating.Order = int.Parse(txtOrder.Text.Trim() == "" ? "0" : txtOrder.Text.Trim());
                rating.CompanyId = SessionManager.CurrentCompanyId;
                rating.ModifiedOn = System.DateTime.Now;
                rating.ModifiedBy = SessionManager.CurrentLoggedInEmployeeId;
                if (PositionGradeStepAmountManager.UpdateRating(rating, ref errMsg))
                {
                    ClearField();
                    BindPosition();
                }
                else
                {
                    divWarningMsg.InnerHtml = errMsg;
                    divWarningMsg.Hide = false;
                }
            }
        }



        protected void gvPosition_OnRowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            string errMessage = string.Empty;
            int id = (int)gvPosition.DataKeys[e.RowIndex][0];
            if (!PositionGradeStepAmountManager.CanRatingBeDeleted(id, ref errMessage))
            {
                divWarningMsg.InnerHtml = errMessage;
                divWarningMsg.Hide = false;
                return;

            }
            if (PositionGradeStepAmountManager.DeleteRating(id))
            {
                ClearField();
                BindPosition();
            }

        }



        void Initialize()
        {
            BindPosition();

        }

        void BindPosition()
        {
            List<EvalulationRating> list = PositionGradeStepAmountManager.GetAllRatings(SessionManager.CurrentCompanyId);
            gvPosition.DataSource = list;
            gvPosition.DataBind();
            if (list.Count == 0)
                txtOrder.Text = "1";

            else
                txtOrder.Text = (list[list.Count - 1].Order + 1).ToString();
        }

        protected void gvPosition_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvPosition.PageIndex = e.NewPageIndex;
            BindPosition();
        }

        void ClearField()
        {
            hdPositionId.Value = "0";
            SaveMode = "0";
            txtOrder.Text = string.Empty;
            txtRatingName.Text = string.Empty;
            divWarningMsg.Hide = true;
        }
    }
}