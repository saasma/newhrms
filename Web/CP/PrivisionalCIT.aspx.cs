﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using Utils.Helper;
using Utils.Web;
using BLL.Base;
using DAL;
using Utils.Base;

namespace Web
{
    public partial class PrivisionalCIT : BasePage
    {

        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.AdministratorTools;
            }
        }     

        private int _tempCurrentPage;
        private int _tempCount;
        private PayManager payMgr = new PayManager();
        protected void Page_Load(object sender, EventArgs e)
        {

            if(SessionManager.CurrentCompany.IsEnglishDate)
            {
                Response.Redirect("~/cp/PrivisionalCITEng.aspx");
                return;
            }

            //RegisterEmployeeSavedMsg();
            if (!IsPostBack)
            {
                PayrollPeriod period = CommonManager.GetLastPayrollPeriod();
                if (period != null)
                {
                    citList.InnerHtml += period.Name;
                }

                pagingCtl.CurrentPage = 1;
                BindEmployees();

            }
            if (Request.Form["__EVENTTARGET"] != null && Request.Form["__EVENTTARGET"].ToLower().Contains("refresh"))
                BindEmployees();
            JavascriptHelper.AttachNonDialogPopUpCode(Page, "taxImportPopup", "../ExcelWindow/ProvisionalCITExcel.aspx", 450, 500);

         
        
        }
        //void RegisterEmployeeSavedMsg()
        //{
        //    if (SessionManager.EmployeeSaved)
        //    {
        //        JavascriptHelper.DisplayClientMsgWithTimeout("Employee Information Updated.", Page);
        //        SessionManager.EmployeeSaved = false;
        //    }
        //}

        void BindEmployees()
        {
            int totalRecords = 0;
            gvEmployeeIncome.DataSource
                = TaxManager.GetListForPrivisionalCIT
                (txtEmployeeName.Text.Trim(), pagingCtl.CurrentPage - 1, int.Parse(pagingCtl.DDLRecords.SelectedValue), ref totalRecords);
            gvEmployeeIncome.DataBind();
            pagingCtl.UpdatePagingBar(totalRecords);

            btnUpdate.Visible = true;
            btnExport.Visible = btnUpdate.Visible;
        }

        public bool IsTextBoxEnabled(object value)
        {
            return value.ToString().ToLower() != "optimum";
        }

        public string GetCITCurrency(object amount)
        {
            return BaseHelper.GetCurrency(amount, 0);
        }
        protected void btnPrevious_Click()
        {
            pagingCtl.CurrentPage -= 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage -= 1;
            BindEmployees();
        }

        protected void btnNext_Click()
        {
            pagingCtl.CurrentPage += 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage += 1;
            BindEmployees();
        }

        protected void ddlRecords_SelectedIndexChanged()
        {
            //SessionManager.EmployeeDisplayRecodsPerPage = int.Parse(pagintCtl.DDLRecords.SelectedValue);
            //_tempCurrentPage = 1;
            pagingCtl.CurrentPage = 1;
            BindEmployees();
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            Page.Validate("Balance");
            bool isSavedReqd = false;
            decimal provisionAmount = 0, regularCITAmountRate = 0, currentMonthAddition = 0, remProvision = 0, externalCIT = 0;
            decimal m4, m5, m6, m7, m8, m9, m10, m11, m12, m1, m2, m3, rate = 0;
            int isAutoProvision = 0;
            decimal oneTimeAdjustment = 0;

            if (Page.IsValid)
            {
                StringBuilder str = new StringBuilder("<root>");
                StringBuilder strMonthsAmounts = new StringBuilder("<root>");
                int companyId = SessionManager.CurrentCompanyId;

                foreach (GridViewRow row in gvEmployeeIncome.Rows)
                {
                    int employeeId = (int)gvEmployeeIncome.DataKeys[row.RowIndex][0];


                    TextBox txt = row.FindControl("txtProvision") as TextBox;
                    TextBox txtRegularAmountRate = row.FindControl("txtRegularAmountRate") as TextBox;
                    TextBox txtRate = row.FindControl("txtRate") as TextBox;
                    TextBox txtCurrentMonthAddition = row.FindControl("txtCurrentMonthAddition") as TextBox;
                    TextBox txtExternal = row.FindControl("txtExternal") as TextBox;
                    DropDownList ddl = row.FindControl("ddl") as DropDownList;
                    DropDownList ddlAutoPro = row.FindControl("ddlAutoPro") as DropDownList;
                    TextBox txtCurrentMonthAdditionalAdjustment = row.FindControl("txtCurrentMonthAdditionalAdjustment") as TextBox;

                    m4 = Convert.ToDecimal((row.FindControl("txtMonth4") as TextBox).Text);
                    m5 = Convert.ToDecimal((row.FindControl("txtMonth5") as TextBox).Text);
                    m6 = Convert.ToDecimal((row.FindControl("txtMonth6") as TextBox).Text);
                    m7 = Convert.ToDecimal((row.FindControl("txtMonth7") as TextBox).Text);

                    m8 = Convert.ToDecimal((row.FindControl("txtMonth8") as TextBox).Text);
                    m9 = Convert.ToDecimal((row.FindControl("txtMonth9") as TextBox).Text);
                    m10 = Convert.ToDecimal((row.FindControl("txtMonth10") as TextBox).Text);
                    m11 = Convert.ToDecimal((row.FindControl("txtMonth11") as TextBox).Text);

                    m12 = Convert.ToDecimal((row.FindControl("txtMonth12") as TextBox).Text);
                    m1 = Convert.ToDecimal((row.FindControl("txtMonth1") as TextBox).Text);
                    m2 = Convert.ToDecimal((row.FindControl("txtMonth2") as TextBox).Text);
                    m3 = Convert.ToDecimal((row.FindControl("txtMonth3") as TextBox).Text);

                    string type = gvEmployeeIncome.DataKeys[row.RowIndex]["RegularCITType"].ToString().Trim();


                    //if (type == "Optimum")
                    //    continue;

                    if (type != "Optimum" && ddl.SelectedValue == "Optimum")
                    {
                        divWarningMsg.InnerHtml = "Optimum CIT can not be set for the employee " + gvEmployeeIncome.DataKeys[row.RowIndex]["Name"].ToString()
                            + " from this page, please use Optimum CIT page.";
                        divWarningMsg.Hide = false;
                        return;
                    }

                    isSavedReqd = true;

                    provisionAmount = 0;

                    //decimal.TryParse(txt.Text.Trim(), out provisionAmount);
                    decimal.TryParse(txtRegularAmountRate.Text.Trim(), out regularCITAmountRate);
                    decimal.TryParse(txtRate.Text.Trim(), out rate);
                    decimal.TryParse(txtExternal.Text.Trim(), out externalCIT);

                    if (decimal.TryParse(txtCurrentMonthAdditionalAdjustment.Text.Trim(), out oneTimeAdjustment) == false)
                        oneTimeAdjustment = 0;

                    remProvision = provisionAmount - currentMonthAddition;

                    isAutoProvision = 0;
                    if (ddlAutoPro != null && ddlAutoPro.SelectedValue.ToLower() == bool.TrueString.ToLower())
                        isAutoProvision = 1;

                    //DAL.AttendanceMapping map = BLL.BaseBiz.PayrollDataContext.AttendanceMappings
                    //.SingleOrDefault(x => x.DeviceId == deviceId);

                    //if (map.EmployeeId != employeeId)
                    //{
                    //   WarningCtl.InnerHTML = "Device ID " + deviceId + " already exists for the employee
                    //}

                    str.Append(
                        string.Format("<row EmployeeId=\"{0}\" ProvisionAmount=\"{1}\" RegularAmount=\"{2}\" Rate=\"{3}\" CurrentMonthAddition=\"{4}\" RemainingAmount=\"{5}\" AmountType=\"{6}\"  ExternalCIT=\"{7}\" IsAutoProvision=\"{8}\" CurrentMonthAdditionalCIT=\"{9}\"  /> ",
                             employeeId, provisionAmount, regularCITAmountRate,rate, currentMonthAddition, remProvision, ddl.SelectedValue, externalCIT,isAutoProvision,oneTimeAdjustment)
                        );

                    strMonthsAmounts.Append(string.Format("<row EmployeeId=\"{0}\" Month=\"{1}\" Amount=\"{2}\" Order=\"{3}\" /> ", employeeId, 4, m4, 4));
                    strMonthsAmounts.Append(string.Format("<row EmployeeId=\"{0}\" Month=\"{1}\" Amount=\"{2}\" Order=\"{3}\" /> ", employeeId, 5, m5, 5));
                    strMonthsAmounts.Append(string.Format("<row EmployeeId=\"{0}\" Month=\"{1}\" Amount=\"{2}\" Order=\"{3}\" /> ", employeeId, 6, m6, 6));
                    strMonthsAmounts.Append(string.Format("<row EmployeeId=\"{0}\" Month=\"{1}\" Amount=\"{2}\" Order=\"{3}\" /> ", employeeId, 7, m7, 7));

                    strMonthsAmounts.Append(string.Format("<row EmployeeId=\"{0}\" Month=\"{1}\" Amount=\"{2}\" Order=\"{3}\" /> ", employeeId, 8, m8, 8));
                    strMonthsAmounts.Append(string.Format("<row EmployeeId=\"{0}\" Month=\"{1}\" Amount=\"{2}\" Order=\"{3}\" /> ", employeeId, 9, m9, 9));
                    strMonthsAmounts.Append(string.Format("<row EmployeeId=\"{0}\" Month=\"{1}\" Amount=\"{2}\" Order=\"{3}\" /> ", employeeId, 10, m10, 10));
                    strMonthsAmounts.Append(string.Format("<row EmployeeId=\"{0}\" Month=\"{1}\" Amount=\"{2}\" Order=\"{3}\" /> ", employeeId, 11, m11, 11));


                    strMonthsAmounts.Append(string.Format("<row EmployeeId=\"{0}\" Month=\"{1}\" Amount=\"{2}\" Order=\"{3}\" /> ", employeeId, 12, m12, 12));
                    strMonthsAmounts.Append(string.Format("<row EmployeeId=\"{0}\" Month=\"{1}\" Amount=\"{2}\" Order=\"{3}\" /> ", employeeId, 1, m1, 13));
                    strMonthsAmounts.Append(string.Format("<row EmployeeId=\"{0}\" Month=\"{1}\" Amount=\"{2}\" Order=\"{3}\" /> ", employeeId, 2, m2, 14));
                    strMonthsAmounts.Append(string.Format("<row EmployeeId=\"{0}\" Month=\"{1}\" Amount=\"{2}\" Order=\"{3}\" /> ", employeeId, 3, m3, 15));


                }
                str.Append("</root>");
                strMonthsAmounts.Append("</root>");
                if (isSavedReqd)
                {
                    TaxManager.SaveForPrivisionalCIT(str.ToString(), strMonthsAmounts.ToString());

                    divMsgCtl.InnerHtml = "Information saved.";
                    divMsgCtl.Hide = false;

                    //JavascriptHelper.DisplayClientMsg("Information saved.", Page);
                }
            }
        }

        protected void txtEmployeeName_TextChanged(object sender, EventArgs e)
        {
            _tempCurrentPage = 1;
            BindEmployees();

        }
    }
}
