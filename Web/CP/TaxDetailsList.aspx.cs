﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using Utils.Helper;
using Utils.Web;
using BLL.Base;
using DAL;
using Utils.Base;

namespace Web
{


    public partial class TaxDetailsList : BasePage
    {

        int payrollPeriodId;
        /// <summary>
        /// For Excel export to work
        /// </summary>
        /// <param name="control"></param>
        public override void VerifyRenderingInServerForm(Control control)
        {

        }

        void SetPeriod()
        {
            int month = 0;
            int year = 0;

            if (!string.IsNullOrEmpty(ddlPayrollFromMonth.SelectedValue))
            {


                month = int.Parse(ddlPayrollFromMonth.SelectedValue);
                year = int.Parse(ddlPayrollFromYear.SelectedValue);

                PayrollPeriod period = CommonManager.GetPayrollPeriod(month, year);
                if (period != null)
                    payrollPeriodId = period.PayrollPeriodId;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
                gvEmployeeIncome.Columns[20].HeaderText = CommonManager.GetHandicappedName;

           if (!IsPostBack)
            {
                //PayrollPeriod payroll = CommonManager.GetLastPayrollPeriod();
                //if (payroll != null)
                //{
                //    title.InnerHtml += payroll.Name;
                //}

                
             

                CommonManager.SetFirstAndLastFinalSavedPayrollPeriod(ddlPayrollFromYear, ddlPayrollFromMonth);

            }
            SetPeriod();
            AddColumns();
            //if (!IsPostBack)
            {

                BindEmployees();

            }
        }

        public List<CalcGetTaxDetailsHeaderListResult> OrderHeaders(List<CalcGetTaxDetailsHeaderListResult> headers)
        {
            List<CalcGetTaxDetailsHeaderListResult> newHeaders = new List<CalcGetTaxDetailsHeaderListResult>();
            newHeaders.AddRange(headers.Where(x => x.GroupType == (int)TaxDetailHeaderEnum.DefaultIncome).OrderBy(x => x.HeaderName));
            newHeaders.AddRange(headers.Where(x => x.GroupType == (int)TaxDetailHeaderEnum.PastIncome).OrderBy(x => x.HeaderName));
            newHeaders.AddRange(headers.Where(x => x.GroupType == (int)TaxDetailHeaderEnum.CurrentIncome).OrderBy(x => x.HeaderName));
            newHeaders.AddRange(headers.Where(x => x.GroupType == (int)TaxDetailHeaderEnum.ForecastIncome).OrderBy(x => x.HeaderName));

            newHeaders.AddRange(headers.Where(x => x.GroupType == (int)TaxDetailHeaderEnum.OneTimeIncome).OrderBy(x => x.HeaderName));

            newHeaders.AddRange(headers.Where(x => x.GroupType == (int)TaxDetailHeaderEnum.DeemedIncome).OrderBy(x => x.HeaderName));

            return newHeaders;
        }

        protected void gvw_DataBound(object sender, EventArgs e)
        {
            CreateGroupInHeader(sender);
        }

        #region "Group Header"
        public void AddCell(GridViewRow row, bool skipRight)
        {
            TableCell left = new TableHeaderCell();
            //left.ColumnSpan = count;
            //left.HorizontalAlign = HorizontalAlign.Center;
           // left.BackColor = ColorTranslator.FromHtml(color);
           // left.Style.Add("border-bottom", String.Format("1px solid {0}", color));
            //left.Text = Utils.Helper.Util.GetTextResource("CalculationIncomesTitle");
            left.Style["border-top"] = "1px solid white";
            left.Style["border-left"] = "1px solid white";
            if (skipRight == false)
                left.Style["border-right"] = "1px solid white";
            left.Style["background"] = "white";
            row.Cells.Add(left);
        }

        public delegate bool IsRequiredColumn(TaxDetailHeaderEnum type);
        /// <summary>
        /// Provides logic in application if the ColumnType is Income or Not
        /// </summary>
        public static bool IsDefaultIncome(TaxDetailHeaderEnum type)
        {
            return type == TaxDetailHeaderEnum.DefaultIncome;
        }
        public static bool IsPastIncome(TaxDetailHeaderEnum type)
        {
            return type == TaxDetailHeaderEnum.PastIncome;
        }
        public static bool IsCurrentIncome(TaxDetailHeaderEnum type)
        {
            return type == TaxDetailHeaderEnum.CurrentIncome;
        }
        public static bool IsForecastIncome(TaxDetailHeaderEnum type)
        {
            return type == TaxDetailHeaderEnum.ForecastIncome;
        }
        public static bool IsOneTimeIncome(TaxDetailHeaderEnum type)
        {
            return type == TaxDetailHeaderEnum.OneTimeIncome;
        }
        public static bool IsDeemedIncome(TaxDetailHeaderEnum type)
        {
            return type == TaxDetailHeaderEnum.DeemedIncome;
        }
        public  void CreateGroupInHeader(object sender)
        {
            GridView grid = sender as GridView;
            TableHeaderCell cell;
            TableCell left;

            if (grid != null && grid.Rows.Count > 0)
            {
                ///add group table
                GridViewRow row = new GridViewRow(0, -1,
                                                  DataControlRowType.Header, DataControlRowState.Normal);

                GridViewRow headerRow = grid.HeaderRow;

                int i;

                ///1. Empty cell for 7 cell
                for (i = 1; i <= 7; i++)
                {
                    bool skipRight = (i == 7) ? true : false;
                    AddCell(row, skipRight);
                }

                ///2. Incomes, find the column span for Incomes
                int count = 0;
                i = 7;

              
                ///For Default heads
                count = GetSimilarColumnCount(grid, headerRow, i, new IsRequiredColumn(IsDefaultIncome));
                if (count != 0)
                {
                    AddGroupColumn(row, count, "Regular Default Salary");
                    i += count;
                }

                count = GetSimilarColumnCount(grid, headerRow, i, new IsRequiredColumn(IsPastIncome));
                if (count != 0)
                {
                    AddGroupColumn(row, count, "Regular Actual Income in the past");
                    i += count;
                }

                count = GetSimilarColumnCount(grid, headerRow, i, new IsRequiredColumn(IsCurrentIncome));
                if (count != 0)
                {
                    AddGroupColumn(row, count, "Regular Current Month Salary");
                    i += count;
                }

                count = GetSimilarColumnCount(grid, headerRow, i, new IsRequiredColumn(IsForecastIncome));
                if (count != 0)
                {
                    AddGroupColumn(row, count, "Regular Income Forecast");
                    i += count;
                }

                count = GetSimilarColumnCount(grid, headerRow, i, new IsRequiredColumn(IsOneTimeIncome));
                if (count != 0)
                {
                    AddGroupColumn(row, count, "One Time Income");
                    i += count;
                }

                count = GetSimilarColumnCount(grid, headerRow, i, new IsRequiredColumn(IsDeemedIncome));
                if (count != 0)
                {
                    AddGroupColumn(row, count, "Deemed Income");
                    i += count;
                }
                ///3. Column for Deemed income     
                //count = 0;
                //for (; i < grid.Columns.Count; i++)
                //{
                //    Label lbl = GetLabelInHeader(headerRow.Cells[i].Controls);
                //    if (lbl != null)
                //    {
                //        CalculationColumnType columnType = (CalculationColumnType)Int32.Parse(lbl.Attributes["Type"]);
                //        if (CalculationValue.IsColumTypeDeemedIncome(columnType))
                //            count += 1;
                //        else
                //        {
                //            break;
                //        }
                //    }
                //}
                

                Table t = grid.Controls[0] as Table;
                if (t != null)
                {
                    t.Rows.AddAt(0, row);
                }


            }
        }

        private  void AddSingleEmptyGroupColumn(GridViewRow row)
        {
            TableCell cell = new TableHeaderCell();
            //cell.BackColor = ColorTranslator.FromHtml(incomeColor);
            cell.Style.Add("border-bottom", String.Format("1px solid {0}", "black"));
            row.Cells.Add(cell);

        }

        private  void AddGroupColumn(GridViewRow row, int count, string columnGroupText)
        {
            TableHeaderCell left = new TableHeaderCell();
            left.ColumnSpan = count;
            left.HorizontalAlign = HorizontalAlign.Center;
           // left.BackColor = ColorTranslator.FromHtml(incomeColor);
            left.Text = columnGroupText;
            row.Cells.Add(left);

        }

        public static Label GetLabelInHeader(ControlCollection ctls)
        {
            foreach (var ctl in ctls)
            {
                if ((ctl as Label) != null)
                    return ctl as Label;
            }
            return null;
        }

        private  int GetSimilarColumnCount(GridView grid, GridViewRow headerRow, int columnStartIndex, IsRequiredColumn columnCheckingMethod)
        {
            int count = 0;
            for (; columnStartIndex < grid.Columns.Count; columnStartIndex++)
            {
                Label lbl = GetLabelInHeader(headerRow.Cells[columnStartIndex].Controls);
                if (lbl != null)
                {
                    TaxDetailHeaderEnum columnType = (TaxDetailHeaderEnum)Int32.Parse(lbl.Attributes["GroupType"]);
                    //if (CalculationValue.IsHPLUnitColumn(columnType))
                    if (columnCheckingMethod(columnType))
                        count += 1;
                    else
                    {
                        break;
                    }
                }
            }
            return count;
        }


        protected void btnUpdate_Click(object sender, EventArgs e)
        {

            //PayrollPeriod payroll = CommonManager.GetLastPayrollPeriod();
            //if (payroll != null)
            {
                ExportToExcel();
                //GridViewExportUtil.Export(string.Format("Tax Details List for {0}.xls", payroll.Name), gvEmployeeIncome);
            }
        }

        private void AddColumns()
        {


            List<CalcGetTaxDetailsHeaderListResult> headers = EmployeeManager.GetTaxDetailsListHeader(payrollPeriodId);

            // Order the headers


            headers = OrderHeaders(headers);
            int startColumn = 7;// after Tax Status header
            for (int i = 0; i < headers.Count; i++)
            {
                TemplateField field = new TemplateField();
                field.ItemTemplate = new EmployeeTaxDetailsGridViewTemplate(DataControlRowType.DataRow, headers[i]);
                field.HeaderTemplate = new EmployeeTaxDetailsGridViewTemplate(DataControlRowType.Header, headers[i]);
                gvEmployeeIncome.Columns.Insert(startColumn++, field);
            }
        }

        #endregion


        //string value = dataSource.GetValue(header);
        void BindEmployees()
        {
            if (payrollPeriodId == 0)
                return;

            try
            {


                List<Report_GetTaxCalculationListResult> list = EmployeeManager.GetTaxDetailsList(payrollPeriodId);

                CalcGetTaxDetailsHeaderListResult headerPF = new CalcGetTaxDetailsHeaderListResult { Type = (int)CalculationColumnType.DeductionPF, SourceId = (int)CalculationColumnType.DeductionPF, GroupType = 1 };
                CalcGetTaxDetailsHeaderListResult headerCIT = new CalcGetTaxDetailsHeaderListResult { Type = (int)CalculationColumnType.DeductionCIT, SourceId = (int)CalculationColumnType.DeductionCIT, GroupType = 1 };
                string value = "";
                foreach (Report_GetTaxCalculationListResult item in list)
                {
                    value = item.GetValue(headerPF);
                    item.ForecastPF = string.IsNullOrEmpty(value) ? 0 : Convert.ToDecimal(value);

                    value = item.GetValue(headerCIT);
                    item.ForecastCIT = string.IsNullOrEmpty(value) ? 0 : Convert.ToDecimal(value);
                }

                gvEmployeeIncome.DataSource = list;
                gvEmployeeIncome.DataBind();
            }
            catch (Exception exp)
            {
                divWarningMsg.InnerHtml = "Please make sure there is no red mark in current salary for income/deduction adjsutment.";
                divWarningMsg.Hide = false;
            }
        }

        //protected void btnUpdate_Click(object sender, EventArgs e)
        //{

        //    PayrollPeriod payroll = CommonManager.GetLastPayrollPeriod();
        //    if (payroll != null)
        //    {
        //        ExportToExcel();
        //        //GridViewExportUtil.Export(string.Format("Tax Details List for {0}.xls", payroll.Name), gvEmployeeIncome);
        //    }
        //}



        public void ExportToExcel()
        {
            bool gridViewLastColumnVisibility = gvEmployeeIncome.Columns[gvEmployeeIncome.Columns.Count - 1].Visible;
            GridLines gridViewGridLines = gvEmployeeIncome.GridLines;




            Response.Clear();

            Response.AddHeader("content-disposition", "attachment; filename=\"Tax Details List.xls\"");

            //Response.Charset = "";


            Response.ContentType = "application/vnd.xls";
            //Response.ContentType = "application/ms-excel";
            Response.ContentEncoding = System.Text.Encoding.Default;
            //Response.ContentEncoding = Encoding.;
            Response.Charset = "utf-8";

            Response.HeaderEncoding = Encoding.UTF8;
            System.IO.StringWriter stringWrite = new System.IO.StringWriter();

            System.Web.UI.HtmlTextWriter htmlWrite =
            new HtmlTextWriter(stringWrite);
            //  gvw.HeaderStyle.BackColor = System.Drawing.Color.Red;



            //gvw.DataSource = CalculationManager.GetCalculationList(SessionManager.CurrentCompanyId,
            //        this.GetPayrollPeriodId(), 1,
            //        999999, ref _tempCount, chkHasRetiredOrResigned.Checked, null);
            // gvw.DataBind();





            ClearControls(gvEmployeeIncome);


           // gvw.Columns[gvEmployeeIncome.Columns.Count - 1].Visible = false;
            gvEmployeeIncome.GridLines = GridLines.Both;


            foreach (GridViewRow r in gvEmployeeIncome.Rows)
            {
                int columnIndex = 0;
                foreach (TableCell c in r.Cells)
                {
                    //skip two columns
                    if (++columnIndex > 2)
                        c.HorizontalAlign = HorizontalAlign.Right;

                    //c.Attributes.Add("class", "text");
                    // c.Text = r.Cells.ToString();
                    c.Width = 80;

                }

            }


            gvEmployeeIncome.RenderControl(htmlWrite);

            Response.Write(stringWrite.ToString());

            Response.End();

            gvEmployeeIncome.Columns[gvEmployeeIncome.Columns.Count - 1].Visible = gridViewLastColumnVisibility;
            gvEmployeeIncome.GridLines = gridViewGridLines;

            //gvw.DataSource = CalculationManager.GetCalculationList(SessionManager.CurrentCompanyId,
            //        this.GetPayrollPeriodId(), 1,
            //        int.Parse(pagingCtl.DDLRecords.SelectedValue), ref _tempCount, chkHasRetiredOrResigned.Checked, null);

            //gvw.DataBind();


        }

        private void ClearControls(Control control)
        {
            for (int i = control.Controls.Count - 1; i >= 0; i--)
            {
                ClearControls(control.Controls[i]);
            }
            //
            // if(control is Table)

            if (!(control is TableCell))
            {
                if (control.GetType().GetProperty("SelectedItem") != null)
                {
                    LiteralControl literal = new LiteralControl();
                    control.Parent.Controls.Add(literal);
                    try
                    {
                        literal.Text =
                            (string)control.GetType().GetProperty("SelectedItem").
                                GetValue(control, null);
                    }
                    catch
                    { }
                    control.Parent.Controls.Remove(control);
                }
                else if (control.GetType().GetProperty("Text") != null)
                {
                    LiteralControl literal = new LiteralControl();
                    control.Parent.Controls.Add(literal);
                    literal.Text =
                        (string)control.GetType().GetProperty("Text").
                            GetValue(control, null);
                    control.Parent.Controls.Remove(control);
                }
            }
            return;
        }





        /// <summary>
        /// Replace any of the contained controls with literals
        /// </summary>
        /// <param name="control"></param>
        private void PrepareGridViewForExport(Control gv)
        {



            LinkButton lb = new LinkButton();

            Literal l = new Literal();

            string name = String.Empty;

            for (int i = 0; i < gv.Controls.Count; i++)
            {


                if (gv.Controls[i].GetType() == typeof(LinkButton))
                {

                    l.Text = (gv.Controls[i] as LinkButton).Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                else if (gv.Controls[i].GetType() == typeof(TextBox))
                {

                    l.Text = (gv.Controls[i] as TextBox).Text.ToString();

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);


                }

                if (gv.Controls[i].HasControls())
                {

                    PrepareGridViewForExport(gv.Controls[i]);

                }

            }
        }

    }

    public class EmployeeTaxDetailsGridViewTemplate : ITemplate
    {
        private CalcGetTaxDetailsHeaderListResult header; 
        private DataControlRowType rowType;
        private string list = "";

        public EmployeeTaxDetailsGridViewTemplate(DataControlRowType rowType, CalcGetTaxDetailsHeaderListResult header)
        {
            this.header = header;
            this.rowType = rowType;
        }

       

        public void InstantiateIn(Control container)
        {
            switch (rowType)
            {
                case DataControlRowType.Header:
                    //Label lbl = new Label();
                    Label lblHeader = new Label();
                    lblHeader.Text = header.HeaderName;

                    lblHeader.Style["word-wrap"] = "break-word";

                    lblHeader.Attributes.Add("SourceId", header.SourceId.ToString());
                    lblHeader.Attributes.Add("Type", header.Type.ToString());
                    lblHeader.Attributes.Add("HeaderName", header.HeaderName);
                    lblHeader.Attributes.Add("GroupType", header.GroupType.ToString());

                    container.Controls.Add(lblHeader);

                 
                    break;

                case DataControlRowType.DataRow:

                    Literal lbl = new Literal();                  
                    lbl.DataBinding += new EventHandler(lbl_Binding);
                    container.Controls.Add(lbl);
                    
                    break;
            }
        }


        public void lbl_Binding(object sender, EventArgs e)
        {
            GridViewRow row = ((GridViewRow)(((Control)(sender)).NamingContainer));

            Report_GetTaxCalculationListResult dataSource = (Report_GetTaxCalculationListResult)row.DataItem;
            Literal lbl = (Literal)sender;

            string value = dataSource.GetValue(header);

            if (!string.IsNullOrEmpty(value) && !value.Contains("%") && value != "Yes")
                lbl.Text = BaseHelper.GetCurrency(value, SessionManager.DecimalPlaces);
            else
                lbl.Text = value;
           
        }
    }
}
