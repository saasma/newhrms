﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Details.Master" AutoEventWireup="true" CodeBehind="CompanyDelConf.aspx.cs" Inherits="Web.CP.CompanyDelConf" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
<div class="contentArea">
    <div>
Are you sure you want to remove the company?<br />
    If you choose to remove the company, <b>ALL DATA OF THIS COMPANY WILL BE LOST!
    </b>
    <br />
    <br />
    <asp:CheckBox ID="chk" runat="server" Text="I understand the Risk" />
   <div class="buttonsDiv">
    <asp:Button ID="btnDelete" CssClass="delete" runat="server" Enabled="False" Text="Delete" 
        onclick="btnDelete_Click" />
&nbsp;
    &nbsp;<asp:Button ID="btnCancel" CssClass="cancel" runat="server" OnClientClick="window.location= 'ManageCompany.aspx';return false;"
            Text="Cancel" />
</div>
</div>
</div>
</asp:Content>
