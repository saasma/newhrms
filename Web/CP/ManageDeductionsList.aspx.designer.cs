﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Web.CP {
    
    
    public partial class ManageDeductionsList {
        
        /// <summary>
        /// hdnDeductionId control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Ext.Net.Hidden hdnDeductionId;
        
        /// <summary>
        /// hdnDropMode control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Ext.Net.Hidden hdnDropMode;
        
        /// <summary>
        /// hdnSource control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Ext.Net.Hidden hdnSource;
        
        /// <summary>
        /// hdnDest control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Ext.Net.Hidden hdnDest;
        
        /// <summary>
        /// btnChangeOrder control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Ext.Net.LinkButton btnChangeOrder;
        
        /// <summary>
        /// btnDelete control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Ext.Net.LinkButton btnDelete;
        
        /// <summary>
        /// btnEnable control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Ext.Net.LinkButton btnEnable;
        
        /// <summary>
        /// btnDisable control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Ext.Net.LinkButton btnDisable;
        
        /// <summary>
        /// txtDeductionSearch control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Ext.Net.TextField txtDeductionSearch;
        
        /// <summary>
        /// btnLoad control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Ext.Net.Button btnLoad;
        
        /// <summary>
        /// btnAddDeduction control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Ext.Net.Button btnAddDeduction;
        
        /// <summary>
        /// voucherAlsoChange control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl voucherAlsoChange;
        
        /// <summary>
        /// gridDeductions control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Ext.Net.GridPanel gridDeductions;
        
        /// <summary>
        /// storeDeductions control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Ext.Net.Store storeDeductions;
        
        /// <summary>
        /// Model1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Ext.Net.Model Model1;
        
        /// <summary>
        /// colRN control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Ext.Net.RowNumbererColumn colRN;
        
        /// <summary>
        /// colTitle control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Ext.Net.Column colTitle;
        
        /// <summary>
        /// colAbbreviation control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Ext.Net.Column colAbbreviation;
        
        /// <summary>
        /// colCalculationType control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Ext.Net.Column colCalculationType;
        
        /// <summary>
        /// Column3 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Ext.Net.Column Column3;
        
        /// <summary>
        /// Column4 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Ext.Net.Column Column4;
        
        /// <summary>
        /// ContextCommand control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Ext.Net.CommandColumn ContextCommand;
        
        /// <summary>
        /// RowSelectionModel1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Ext.Net.RowSelectionModel RowSelectionModel1;
        
        /// <summary>
        /// GridView1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Ext.Net.GridView GridView1;
        
        /// <summary>
        /// GridDragDrop1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Ext.Net.GridDragDrop GridDragDrop1;
    }
}
