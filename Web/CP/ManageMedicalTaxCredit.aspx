﻿<%@ Page Title="Medical tax credit list" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="ManageMedicalTaxCredit.aspx.cs" Inherits="Web.CP.ManageMedicalTaxCredit" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function medicalTaxCreditPopupCall(medicalTaxId) {
            var ret = medicalTaxCreditPopup("Id=" + medicalTaxId);
            if (ret == "Yes")
                __doPostBack('Reload', '');
        }

        function parentReloadCallbackFunction(childWindow, ret) {
            childWindow.close();
            if (ret == "Yes")
                __doPostBack('Reload', '');

        }

        var skipLoadingCheck = true;
        function refreshWindow() {
            __doPostBack('Reload', '');
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Employee Medical Tax
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <asp:LinkButton runat="server" Visible="false"></asp:LinkButton>
        <asp:Panel runat="server" DefaultButton="btnLoad" class="attribute" Style="padding: 10px">
            <table>
                <tr>
                    <td>
                        <strong>Search </strong>&nbsp;
                        <asp:TextBox Width="160px" ID="txtEmpSearchText" runat="server" OnTextChanged="txtEmpSearchText_TextChanged"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Button ID="btnLoad" CssClass="btn btn-default btn-sect btn-sm" Style="margin-left: 10px; width: 80px" runat="server"
                            OnClick="btnLoad_Click" Text="Show" />
                        <cc1:TextBoxWatermarkExtender ID="TBWE2" runat="server" TargetControlID="txtEmpSearchText"
                            WatermarkText="Employee name" WatermarkCssClass="searchBoxText" />
                    </td>
                    <td>
                        <asp:LinkButton ID="btnExport" runat="server" Text="Import Expenses" OnClientClick="medicalTaxImportPopup();return false;"
                            CssClass=" excel marginRight tiptip" Style="float: left;" />
                    </td>
                    <td>
                     <asp:Button ID="btnUpdate" CssClass="excel" runat="server" Text="Export" OnClick="btnUpdate_Click" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <br />
        <div class="separator clear">
            <cc2:EmptyDisplayGridView CssClass="table table-primary mb30 table-bordered table-hover" UseAccessibleHeader="true" ID="gvwList"
                runat="server" AutoGenerateColumns="False" DataKeyNames="MedicalTaxCreditId,EmployeeId"
                GridLines="None" ShowFooterWhenEmpty="False" ShowHeaderWhenEmpty="True" OnRowCreated="gvwList_RowCreated"
                Width="100%">
                <Columns>
                    <asp:BoundField HeaderStyle-HorizontalAlign="Left" DataField="EmployeeId" HeaderText="EIN"
                        HeaderStyle-Width="40px" />
                    <asp:BoundField HeaderStyle-Width="200px" HeaderStyle-HorizontalAlign="Left" DataField="Name"
                        HeaderText="Name" />
                    <asp:BoundField HeaderStyle-Width="150px" HeaderStyle-HorizontalAlign="Left" DataField="Branch"
                        HeaderText="Branch" />
                    <asp:BoundField HeaderStyle-Width="150px" HeaderStyle-HorizontalAlign="Left" DataField="Department"
                        HeaderText="Department" />
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right"
                        HeaderStyle-Width="80px" HeaderText="Last year amount">
                        <ItemTemplate>
                            <%# GetCurrency( Eval("PYAmount")) %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right"
                        HeaderStyle-Width="80px" HeaderText="Current year expenses">
                        <ItemTemplate>
                            <%# GetCurrency(Eval("CYExpense"))%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right"
                        HeaderStyle-Width="80px" HeaderText="Adjustable Amount">
                        <ItemTemplate>
                            <%# GetCurrency(Eval("AdjustableAmount"))%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right"
                        HeaderStyle-Width="80px" HeaderText="Transferred to next year">
                        <ItemTemplate>
                            <%# GetCurrency(Eval("CarriedFWDToNextYear"))%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:ImageButton ID="ImageButton1" Visible='<%# IsVisible(Eval("MedicalTaxCreditId")) %>'
                                ImageUrl="~/images/edit.gif" OnClientClick='<%# "medicalTaxCreditPopupCall(" + Eval("MedicalTaxCreditId") + ");return false;" %>'
                                runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <%--                <PagerSettings Mode="NumericFirstLast" NextPageText="Next »" PreviousPageText="« Prev"
                    FirstPageText="First" LastPageText="Last" />--%>
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                <EmptyDataTemplate>
                    No records.
                </EmptyDataTemplate>
            </cc2:EmptyDisplayGridView>
            <uc1:PagingCtl ID="pagingCtl" OnNextRecord="btnNext_Click" OnPrevRecord="btnPrevious_Click"
                OnDropDownShowRecordsChanged="ddlRecords_SelectedIndexChanged" runat="server" />
            <div class="buttonsDiv">
                <asp:Button ID="btnSave" Visible="false" CssClass="save" runat="server" OnClick="btnSave_Click"
                    Text="Save" />
            </div>
        </div>
    </div>
</asp:Content>
