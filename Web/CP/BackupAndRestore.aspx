﻿<%@ Page Title="Backup/Restore" Language="C#" MasterPageFile="~/Master/Details.Master" AutoEventWireup="true"
    CodeBehind="BackupAndRestore.aspx.cs" Inherits="Web.CP.BackupAndRestore" %>

<%@ Register Assembly="DevExpress.Web.v16.1, Version=16.1.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningMsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="InfoMsgCtl" TagPrefix="uc2" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="Paging" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%--<script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>--%>



</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <%--<ext:ResourceManager ID="ResourceManager1" runat="server" ScriptMode="Release" />--%>
   <asp:HiddenField ID="hdnFileName" runat="server" />

    <div class="contentArea">
        <h3>
            Backup And Restore</h3>
            <asp:HiddenField ID="hdnValue" runat="server" />
        <uc2:InfoMsgCtl ID="msgInfo" EnableViewState="false" Hide="true" runat="server" />
        <uc2:WarningMsgCtl ID="msgWarning" EnableViewState="false" Hide="true" runat="server" />
        <div style='clear:both'>
           
        
        <h3>Manual Database Backups</h3>
        Take manual backups whenever you perform a major event. These backups are never deleted automatically.
        <br />
        <br />
        

      
         <table>
                <tr>
                    <td valign="top" style="width:270px;">
                    
                        <strong style='display:block'>
                             Type a friendly backup name </strong>                            
                             
                            <asp:TextBox Width="250px" ID="txtFileName" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFileName"
                                Display="None" ErrorMessage="Filename is required." ValidationGroup="backup"></asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <asp:Button runat="server" CssClass="update" Text="Take Backup" ID="btnBackup" OnClientClick="valGroup='backup';return CheckValidation()"
                            OnClick="btnBackup_Click" />
                    </td>
                    <td valign="top" style="display:none;">
                    <strong style='display:block;'>
                            Backup list </strong>
                        <asp:ListBox ID="lstBackups" Width="285px" Height="200px" runat="server"></asp:ListBox>
                        
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="lstBackups"
                                Display="None" ErrorMessage="Please select a backup." ValidationGroup="restore"></asp:RequiredFieldValidator>
                        <br /> <br />
                        <%-- <asp:Button runat="server" CssClass="save" Text="Restore" ID="btnRestore" 
                                OnClientClick="if(!confirm('Confirm restore,current data may be lost?')) return false; valGroup='restore';return CheckValidation()" 
                                onclick="btnRestore_Click" />--%>
                         <asp:Button runat="server" CssClass="save" Text="Download" ID="btnDownload" 
                                OnClientClick="valGroup='restore';return CheckValidation()" 
                                onclick="btnDownload_Click" />
                         <asp:Button runat="server" CssClass="delete" Text="Delete" ID="Button1" 
                                OnClientClick="if(!confirm('Confirm delete backup?')) return false;valGroup='restore';return CheckValidation()" 
                                onclick="Button1_Click" />
                    </td>
                </tr>                
            </table>

         <br />
         <table style="margin-top:10px;">
            <tr>
                <td style="width:200px;">File Name</td>
                <td style="width:130px;">From Date</td>
                <td style="width:130px;">To Date</td>
                <td></td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox Width="180px" ID="txtNameSearchManual" runat="server"></asp:TextBox>
                </td>
                <td>
                    <dx:ASPxDateEdit ID="txtFromDateManual" runat="server" Width="110" />
                </td>
                <td>
                    <dx:ASPxDateEdit ID="txtToDateManual" runat="server" Width="110" />
                </td>
                <td style="padding-bottom:10px;">
                    <asp:Button UseSubmitBehavior="false" ID="btnLoadManual" 
                        style="margin-left:20px;" runat="server" 
                        CssClass="btn btn-default" Text="Load" 
                        onclick="btnLoadManual_Click" /> 
                </td>
            </tr>
         </table>
         <br />
        
         <cc2:EmptyDisplayGridView Width="750px" Style='clear: both; margin-bottom:0px' PagerStyle-HorizontalAlign="Center"
            PagerStyle-CssClass="defaultPagingBar" CssClass="table table-primary mb30 table-bordered table-hover" UseAccessibleHeader="true"
            ShowHeaderWhenEmpty="True" ID="gvcManualFiles" runat="server" DataKeyNames="FileName"
            AutoGenerateColumns="False" CellPadding="4" GridLines="None" ShowFooterWhenEmpty="False"            
            OnRowDeleting="gvcManualFiles_RowDeleting" OnRowCommand="gvcManualFiles_RowCommand"
             OnPageIndexChanged="gvcManualFiles_PageIndexChanged" OnPageIndexChanging="gvcManualFiles_PageIndexChanging">
            <Columns>
                <asp:TemplateField HeaderText="Date" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="150px">
                    <ItemTemplate>
                        <%# Eval("DateString")%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="File Name" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="300px">
                    <ItemTemplate>
                        <%# Eval("FileName")%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="File Size" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="100px">
                    <ItemTemplate>
                        <%# Eval("FileSize")%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Download" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="100px">
                    <ItemTemplate>
                        <asp:LinkButton ID="btnDownload"
                            runat="server" Text="Download" CommandName="Download" CommandArgument='<%# Eval("FileName") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Delete" ItemStyle-HorizontalAlign="Left"
                    HeaderStyle-Width="100px">
                    <ItemTemplate>
                        <asp:LinkButton ID="btnDelete" runat="server" Text="Delete" CommandName="Delete" OnClientClick="return confirm('Confirm delete backup?');"
                             CommandArgument='<%# Eval("FileName") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <PagerSettings Mode="NumericFirstLast" NextPageText="Next »" PreviousPageText="« Prev"
                FirstPageText="First" LastPageText="Last" />
            <RowStyle CssClass="odd" />
            <AlternatingRowStyle CssClass="even" />
            <PagerStyle CssClass="defaultPagingBar" />
            <SelectedRowStyle CssClass="selected" />
            <EmptyDataTemplate>
                <b>No File. </b>
            </EmptyDataTemplate>
        </cc2:EmptyDisplayGridView>
        <uc1:Paging ID="pagingCtl" OnNextRecord="btnNext_Click" OnPrevRecord="btnPrevious_Click"
            OnDropDownShowRecordsChanged="ddlRecords_SelectedIndexChanged" runat="server" />
       

        <br />
        <div id="divAutomaticFiles" runat="server">
        <h3>Automatic Database Backups</h3>

        Backups older than 30 days are automatically deleted.
        <br />
        <table style="margin-top:20px;">
            <tr>
                <td style="width:200px;">File Name</td>
                <td style="width:130px;">From Date</td>
                <td style="width:130px;">To Date</td>
                <td></td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox Width="180px" ID="txtNameSearchAutomatic" runat="server"></asp:TextBox>
                </td>
                <td>
                    <dx:ASPxDateEdit ID="txtFromDateAutomatic" runat="server" Width="110" />
                </td>
                <td>
                    <dx:ASPxDateEdit ID="txtToDateAutomatic" runat="server" Width="110" />
                </td>
                <td style="padding-bottom:10px;">
                    <asp:Button UseSubmitBehavior="false" ID="btnLoadAutomatic" 
                        style="margin-left:20px;" runat="server" 
                        CssClass="btn btn-default" Text="Load" onclick="btnLoadAutomatic_Click" 
                       /> 
                </td>
            </tr>
         </table>
         
         <br />

         <cc2:EmptyDisplayGridView Width="750px" Style='clear: both; margin-bottom:0px' PagerStyle-HorizontalAlign="Center"
            PagerStyle-CssClass="defaultPagingBar" CssClass="table table-primary mb30 table-bordered table-hover" UseAccessibleHeader="true"
            ShowHeaderWhenEmpty="True" ID="gvcAutomaticFiles" runat="server" DataKeyNames="FileName"
            AutoGenerateColumns="False" CellPadding="4" GridLines="None" ShowFooterWhenEmpty="False"            
             OnRowCommand="gvcAutomaticFiles_RowCommand" OnPageIndexChanged="gvcAutomaticFiles_PageIndexChanged" OnPageIndexChanging="gvcAutomaticFiles_PageIndexChanging">
            <Columns>
                <asp:TemplateField HeaderText="Date" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="150px">
                    <ItemTemplate>
                        <%# Eval("DateString")%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="File Name" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="400px">
                    <ItemTemplate>
                        <%# Eval("FileName")%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="File Size" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="100px">
                    <ItemTemplate>
                        <%# Eval("FileSize")%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Download" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="100px">
                    <ItemTemplate>
                        <asp:LinkButton ID="btnDownload"
                            runat="server" Text="Download" CommandName="Download" CommandArgument='<%# Eval("FileName") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <PagerSettings Mode="NumericFirstLast" NextPageText="Next »" PreviousPageText="« Prev"
                FirstPageText="First" LastPageText="Last" />
            <RowStyle CssClass="odd" />
            <AlternatingRowStyle CssClass="even" />
            <PagerStyle CssClass="defaultPagingBar" />
            <SelectedRowStyle CssClass="selected" />
            <EmptyDataTemplate>
                <b>No File. </b>
            </EmptyDataTemplate>
        </cc2:EmptyDisplayGridView>
        <uc1:Paging ID="pagingCtl2" OnNextRecord="btnNext2_Click" OnPrevRecord="btnPrevious2_Click"
            OnDropDownShowRecordsChanged="ddlRecords2_SelectedIndexChanged" runat="server" />

        
        </div>
        </div>
    </div>
</asp:Content>
