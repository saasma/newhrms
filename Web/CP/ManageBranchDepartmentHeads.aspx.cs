﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using System.Xml;

namespace Web.CP
{
    public partial class ManageBranchDepartmentHeads : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.AdministratorTools;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        public void Initialise()
        {
            CommonManager comManager = new CommonManager();
           

            LoadLevels();
        }
        
        private void LoadLevels()
        {

            List<BranchDepartment> newList = DepartmentManager.getBranchDepartmentForHead();
            storeBranchDepartment.DataSource = newList;
            storeBranchDepartment.DataBind();

            storeEmployeeList.DataSource = EmployeeManager.GetAllActiveEmployees();
            storeBranchDepartment.DataBind();

        }


        
        public void ClearLevelFields()
        {
            hiddenValue.Text = "";
            //txtName.Text = "";
            //txtDescription.Text = "";
        }





        public void btnExport_Click(object sender, EventArgs e)
        {
            List<BranchDepartment> list = DepartmentManager.getBranchDepartmentForHead();


            List<string> hiddenList = new List<string>();
           

            Bll.ExcelHelper.ExportToExcel("Department Head List", list,
                new List<String>() { "BranchID", "DepartmentID", "HeadEmployeeId" },
            new List<String>() { },
            new Dictionary<string, string>() { },
            new List<string>() { }
            , new Dictionary<string, string>() { { "Department Head List" ,""} }
            , new List<string> { "BranchName", "DepartmentName","HeadName" });


        }

        

        protected void btnEditLevel_Click(object sender, DirectEventArgs e)
        {

            BranchDepartment instance = new BranchDepartment();
            instance = DepartmentManager.getBranchDepartmentByIDs(int.Parse(hiddenValue.Text), int.Parse(hiddenValueDept.Text));
            if (instance != null)
            {
                LoadEditData(instance);
                Window1.Center();
                Window1.Show();
            }

        }

        protected void LoadAllowanceGrid(int countryID, int? RequestID)
        {
            List<TARequestLine> lines = new List<TARequestLine>();
            lines = TravelAllowanceManager.getAllowanceByEmployeeID(SessionManager.CurrentLoggedInEmployeeId, RequestID, countryID);
            

        }


        private void LoadEditData(BranchDepartment instance)
        {
            txtBranch.Text = instance.Branch.Name;
            txtDepartment.Text = instance.Department.Name;
            if (instance.HeadEmployeeId != null)
            {
                cmbEmployee.SetValue(instance.HeadEmployeeId.ToString());
            }
            else
                cmbEmployee.ClearValue();
            
        }

        


        


        protected void ButtonNext_Click(object sender, DirectEventArgs e)
        {

            BranchDepartment instance = new BranchDepartment();
            instance = DepartmentManager.getBranchDepartmentByIDs(int.Parse(hiddenValue.Text), int.Parse(hiddenValueDept.Text));

            if (cmbEmployee.SelectedItem != null && cmbEmployee.SelectedItem.Value != null)
                instance.HeadEmployeeId = int.Parse(cmbEmployee.SelectedItem.Value);
            else
                instance.HeadEmployeeId = null;
            

            Status status = DepartmentManager.UpdateDepartmentHeads(instance);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Department Head Updated!");
                LoadLevels();
                Window1.Close();
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
           

        }

        protected void BindBranchDept(object s, StoreReadDataEventArgs e)
        {
            LoadLevels();
        }
    
        //#TODO: Group the Branc-Department-Department Head with Branch Column
 
    }
}

