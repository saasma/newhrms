﻿<%@ Page Title="Password Change Rules" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="PasswordChangeRule.aspx.cs" Inherits="Web.CP.PasswordChangeRule" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .divStyle
        {
            background-color: #D9E1F2;
        }
    </style>
    
    <script type="text/javascript">

    function forcePwdChange() {
        var rbForcePswdChange = <%= rbForcePswdChange.ClientID %>;
        var rbRecommendPswdChange = <%= rbRecommendPswdChange.ClientID %>;
       
        if(rbForcePswdChange.checked){
           rbRecommendPswdChange.setValue(false);
        }  
    }

    function recommendPwdChange(){
        var rbForcePswdChange = <%= rbForcePswdChange.ClientID %>;
        var rbRecommendPswdChange = <%= rbRecommendPswdChange.ClientID %>;

        if(rbRecommendPswdChange.checked){
            rbForcePswdChange.setValue(false);
        }
    }

    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Password Change Rules
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="divStyle">
            <br />
            <table style="margin-left: 30px; margin-bottom: 20px;">
                <tr>
                    <td style="width:220px;">
                        User should change password after
                    </td>
                    <td style="width:60px;">
                        <ext:TextField ID="txtDays" Width="50" runat="server" LabelSeparator="" MaskRe="/[0-9]/" />
                        <asp:RequiredFieldValidator ID="rfvDays" runat="server" ControlToValidate="txtDays"
                            Display="None" ErrorMessage="Number of days is required." ValidationGroup="SavePswdDays"></asp:RequiredFieldValidator>
                    </td>
                    <td>
                        days
                    </td>
                </tr>
            </table>
            <table style="margin-left: 30px; margin-bottom: 20px;">
                <tr>
                    <td style="width:200px;">
                        <ext:Radio ID="rbForcePswdChange" runat="server" BoxLabel="Force Password Change">
                            <Listeners>
                                <Change Handler="forcePwdChange()" />
                            </Listeners>
                        </ext:Radio>
                    </td>
                    <td>
                        <ext:Radio ID="rbRecommendPswdChange" runat="server" BoxLabel="Recommend Password Change">
                            <Listeners>
                                <Change Handler="recommendPwdChange()" />
                            </Listeners>
                        </ext:Radio>
                    </td>
                </tr>
            </table>
            <table style="margin-left: 30px;">
                <tr>
                    <td>
                        <ext:Button runat="server" ID="btnSave" Cls="btn btn-primary" Text="<i></i>Save">
                            <DirectEvents>
                                <Click OnEvent="btnSave_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                            <Listeners>
                                <Click Handler="valGroup = 'SavePswdDays'; return CheckValidation();">
                                </Click>
                            </Listeners>
                        </ext:Button>
                    </td>
                </tr>
            </table>
            <br />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
