<%@ Page Title="Deduction deposit list" Language="C#" MasterPageFile="~/Master/ForPopupPage.Master" AutoEventWireup="true"
    CodeBehind="MGDeductionDepositList.aspx.cs" Inherits="Web.CP.MGDeductionDepositList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">
        function ConfirmLibraryTopicDeletion() {
            var ddllist = document.getElementById('<%= ddlDeposits.ClientID %>');
            if (ddllist.selectedIndex == -1) {
                alert('No deposits to delete.');
                return false;
            }
            if (confirm('Do you want to delete the deposit?')) {
                return true;
            }
            else {
                return false;
            }
        }

        function DisplayLibraryTopicInTextBox(dropdown_id) {
            var ddllist = document.getElementById(dropdown_id);
            document.getElementById('<%= txtDepositRename.ClientID %>').value = ddllist.options[ddllist.selectedIndex].text;
        }
       
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">

<div class="popupHeader">
    <h2 class="headlinespop">
         Manage Deposit List</h2>
</div>
  <div class="marginal">
    <div class="bevel ">
        <div class="fields paddpop"  style="padding: 10px 0 20px 10px !important;width:390px;">
            <table cellpadding="0" cellspacing="0" class="alignttable" width="600px">
                <tr>
                    <td class="lf" width="120px">
        New Deposit
        </td>

  
        <td>
        <asp:TextBox ID="txtNewDeposit" runat="server" Width="159px" MaxLength="100" EnableViewState="false"></asp:TextBox>
        <asp:RequiredFieldValidator ValidationGroup="Deposit1" ID="RequiredFieldValidator3" Display="None" runat="server" ErrorMessage="Deposit is Required"
            ControlToValidate="txtNewDeposit">*</asp:RequiredFieldValidator>
        <asp:Button   CssClass="save" Style="margin: 0px 0 0 5px"   ID="btnAddDepsoit" 
            OnClientClick="valGroup='Deposit1';return CheckValidation()" runat="server" 
            Text="Add" onclick="btnAddDepsoit_Click" />
        </td>

    </tr>
   
    <tr>
         <td class="lf" width="120px">
        Deposit List
        </td>

  
        <td>
        <asp:DropDownList ID="ddlDeposits" runat="server" Width="160px" CssClass="rightMargin"
            DataValueField="DeductionDepositListId" DataTextField="Title" onchange="DisplayLibraryTopicInTextBox(this.id);">
        </asp:DropDownList>
        <asp:Button  CssClass="delete" Style="margin: 0px 0 0 5px"  ID="btnDelete" runat="server" Text="Delete" 
            OnClientClick="javascript:return ConfirmLibraryTopicDeletion();" onclick="btnDelete_Click" />
        </td>

    </tr>
    
    <tr>
         <td class="lf" width="120px">
        Rename Deposit
        </td>

        <td>
        <asp:TextBox ID="txtDepositRename" runat="server" Width="159px" MaxLength="100"></asp:TextBox>
        <asp:RequiredFieldValidator ValidationGroup="Deposit2" ID="RequiredFieldValidator4" runat="server" ErrorMessage="Deposit is required."
            ControlToValidate="txtDepositRename" Display="None">*</asp:RequiredFieldValidator>
            <asp:Button ID="btnRename"  
            OnClientClick="valGroup='Deposit2';return CheckValidation()"  runat="server" 
            Text="Rename" ValidationGroup="RenameLibraryTopic" onclick="btnRename_Click"  CssClass="update"
                            Style="margin: 0px 0 0 5px" />
        </td>

    </tr>
    </table>
      </fieldset>
</asp:Content>
