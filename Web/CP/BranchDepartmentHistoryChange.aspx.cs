﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using System.Text;

namespace Web.CP
{
    public partial class BranchDepartmentHistoryChange : BasePage
    {
        private CommonManager comm = new CommonManager();
        private List<BranchDepartmentHistory> list;
        public bool IsBranchTransfer = true;

        protected void Page_Load(object sender, EventArgs e)
        {
            this.CustomId = int.Parse(Request.QueryString["EIN"]);
            IsBranchTransfer = bool.Parse(Request.QueryString["Branch"]);

            if (IsBranchTransfer == false)
            {
                Page.Title = "Department Transfer";
                headerTitle.InnerHtml = "Department Transfer";
            }
            if (!IsPostBack)
            {
                Initialise();               
            }
            Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "clearUnload();");
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (gvw.SelectedIndex == -1)
                RegisterLastFromDate();
            else
            {
                int currentHistory = (int)gvw.DataKeys[gvw.SelectedIndex][0];

                BranchDepartmentHistory prevHistory = CommonManager.GetPreviousBranchDepartmentHistory(currentHistory);

                if (prevHistory != null)
                {
                    RegisterLastFromDateScript(prevHistory);
                }
            }

            //List<BranchDepartmentHistory> list = CommonManager.GetBranchDepartmentHistory(CustomId);
            //int branchID = -1,departmentID=-1;
            //if (list.Count > 0)
            //{
            //    branchID = list[list.Count - 1].BranchId.Value;
            //    departmentID = list[list.Count - 1].DepeartmentId.Value;
            //}
            //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "changedValue", "var ebranchID=" + branchID + ";var edepartmentID=" + departmentID +";", true);

            //if (this.Value == "true")
            //{

            //}

        }
        private void Initialise()
        {

            //ddlGrade.DataSource = comm.GetAllLocations();
            //ddlGrade.DataBind();

            CommonManager.SaveFirstIBranchfNotExists(this.CustomId);

            calFromDate.IsEnglishCalendar = IsEnglish; calFromDate.SelectTodayDate();
            calFromDate.SelectTodayDate();

            LoadBranches();

            BindData();


            if (IsBranchTransfer == false)
            {
                ddlTransferToBranch.Visible = false;
                rowBranch.Style["display"] = "none";
                EEmployee emp = EmployeeManager.GetEmployeeById(this.CustomId);
                UIHelper.SetSelectedInDropDown(ddlTransferToBranch, emp.BranchId.Value);

                LoadDepartments(null, null);
            }
            

        }
        public void LoadSubDepartments(object sender, EventArgs e)
        {
            ListItem item = ddlTransferToDepartment.Items[0];
            ddlSubDepartment.DataSource = DepartmentManager.GetAllSubDepartmentsByDepartment(int.Parse(ddlTransferToDepartment.SelectedValue));
            ddlSubDepartment.DataBind();

            ddlSubDepartment.Items.Insert(0, item);
        }
        public void LoadDepartments(object sender, EventArgs e)
        {
            ListItem item = ddlTransferToDepartment.Items[0];
            ddlTransferToDepartment.DataSource = DepartmentManager.GetAllDepartmentsByBranch(int.Parse(ddlTransferToBranch.SelectedValue));
            ddlTransferToDepartment.DataBind();

            ddlTransferToDepartment.Items.Insert(0, item);
        }
        void LoadBranches()
        {
            //ddlFilterByBranch.DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            //ddlFilterByBranch.DataBind();

            ddlTransferToBranch.DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            ddlTransferToBranch.DataBind();


            //if( ddlFilterByBranch.Items.Count >0)
            //{
            //    ddlFilterByBranch.Items[1].Selected = true;
            //    LoadEmployeesByBranch();
            //}
            //LoadEmployeesByBranch();
        }

        private void RegisterLastFromDate()
        {
            List<BranchDepartmentHistory> list = CommonManager.GetBranchDepartmentHistory(CustomId,false);

            if (list.Count > 0)
            {
                BranchDepartmentHistory lastHistory = list[list.Count - 1];

                RegisterLastFromDateScript(lastHistory);

            }

        }

        public bool IsEditable(int index)
        {
            if (list != null && list.Count>0)
            {
                if (list.Count - 1 == index)
                    return true;
            }
            return false;
        }

        private void RegisterLastFromDateScript(BranchDepartmentHistory lastHistory)
        {
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "regfromdate",
               string.Format("var fromDate = '{0}';", lastHistory.FromDate), true);
        }

        private void BindData()
        {
            PayManager mgr = new PayManager();


            list = CommonManager.GetBranchDepartmentHistory(CustomId,false);
            gvw.DataSource = list;
            gvw.DataBind();


           
        }

       

        protected void gvw_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvw.PageIndex = e.NewPageIndex;
            BindData();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

            if (Page.IsValid)
            {
                this.Value = "true";

                BranchDepartmentHistory history = Process(null);
                if (gvw.SelectedIndex != -1)
                {
                    
                    history.BranchDepartmentId = (int)gvw.DataKeys[gvw.SelectedIndex][0];
                    CommonManager.UpdateBranchDepartmentHistory(history);

                    if (IsBranchTransfer)
                        divMsgCtl.InnerHtml = "Branch transfer updated.";
                    else
                        divMsgCtl.InnerHtml = "Department transfer updated.";
                }
                else
                {
                    CommonManager.SaveBranchDepartmentHistory(history);
                    if (IsBranchTransfer)
                        divMsgCtl.InnerHtml = "Branch transfer saved.";
                    else
                        divMsgCtl.InnerHtml = "Department transfer saved.";
                }
                divMsgCtl.Hide = false;

                // Prepare Sub-Department List
                //StringBuilder str = new StringBuilder("");
                //bool first = true;
                //List<SubDepartment> deps = DepartmentManager.GetAllSubDepartmentsByDepartment(history.DepeartmentId.Value);
                //foreach (SubDepartment obj in deps)
                //{
                //    if (first == false)
                //        str.Append(",");
                //    str.Append("\"" + obj.SubDepartmentId + "$$" + obj.Name + "\"");
                //    first = false;
                //}
                ////Page.ClientScript.RegisterArrayDeclaration("texts", str.ToString());

                // Register to set Branch/Subdepartment from js in Employee page
                string updateJS = string.Format("opener.updateBranchDepartmentFromChangeHistory({0},{1},'{2}','{3}');",
                    history.BranchId, history.DepeartmentId, new DepartmentManager().GetById(history.DepeartmentId.Value).Name, history.SubDepartmentId);
                Page.ClientScript.RegisterClientScriptBlock(GetType(), "sdfdsdfdsfsd", updateJS, true);


                ClearFields();
                gvw.SelectedIndex = -1;
                BindData();
                RegisterLastFromDate();
            }
        }

        private BranchDepartmentHistory Process(BranchDepartmentHistory entity)
        {
            if (entity == null)
            {
                entity = new BranchDepartmentHistory();
                entity.BranchId = int.Parse(ddlTransferToBranch.SelectedValue);
                entity.DepeartmentId = int.Parse(ddlTransferToDepartment.SelectedValue);
                entity.FromDate = calFromDate.SelectedDate.ToString();
                entity.FromDateEng = GetEngDate(entity.FromDate);
                entity.EmployeeId = this.CustomId;
                entity.Note = txtNote.Text.Trim();
                entity.SubDepartmentId = int.Parse(ddlSubDepartment.SelectedValue);
                return entity;
            }
            else
            {
                UIHelper.SetSelectedInDropDown(ddlTransferToBranch, entity.BranchId);

                LoadDepartments(null, null);
                UIHelper.SetSelectedInDropDown(ddlTransferToDepartment, entity.DepeartmentId);

                LoadSubDepartments(null, null);

                calFromDate.SetSelectedDate(entity.FromDate, IsEnglish);
                txtNote.Text = entity.Note;
            }
            return null;
        }

        protected void gvw_SelectedIndexChanged(object sender, EventArgs e)
        {
            //first record 
            int currentGradeHistory = (int)gvw.DataKeys[gvw.SelectedIndex][0];

            //EGradeHistory prevGradeHistory = CommonManager.GetPreviousGradeHistory(currentGradeHistory);

            //if (prevGradeHistory != null)
            //{
            //    RegisterLastFromDateScript(prevGradeHistory);
            //}


            BranchDepartmentHistory history = CommonManager.GetBranchDepartmentHistoryById(currentGradeHistory);
            Process(history);
            btnSave.Text = Resources.Messages.Update;

            if (history.IsFirst.Value)
                calFromDate.Enabled = false;
            else
                calFromDate.Enabled = true;

            //if first one then disable from date as it should be the same of emp first status from date
            if (CommonManager.GetBranchDepartmentHistory(this.CustomId,false)[0].BranchDepartmentId == history.BranchDepartmentId)
            {
                calFromDate.ToolTip = Resources.Messages.HistoryFirstFromDateNotEditable;
                //calFromDate.Enabled = false;
            }
            else
            {
                calFromDate.Enabled = true; ;
                calFromDate.ToolTip = "";
            }

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            gvw.SelectedIndex = -1;
            BindData();
            ClearFields();
        }


        private void ClearFields()
        {
            calFromDate.Enabled = true;
            txtNote.Text = "";
            calFromDate.ToolTip = "";
            btnSave.Text = Resources.Messages.Save;
            UIHelper.SetSelectedInDropDown(ddlTransferToBranch, -1);
            UIHelper.SetSelectedInDropDown(ddlTransferToDepartment, -1);
            calFromDate.SelectTodayDate();
        }

      
    }
}
