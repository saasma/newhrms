﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Entity;
using BLL;
using BLL.BO;
using Utils.Helper;
using Utils.Calendar;
using System.Drawing;

namespace Web.NewHR
{
    public partial class FutureRetirement : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.HumanResource;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
              
                Initialise();

            }
        }
        public int GetEmployeeId()
        {
            return int.Parse(Request.QueryString["Id"]);
        }

       
        public void Initialise()
        {
         

            cmbTypes.Store[0].DataSource = CommonManager.GetServieEventTypes();
            cmbTypes.Store[0].DataBind();

         

            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                
                LoadEmploymentDetails();

                RetirementDisplayValidation();

            }
           
        }


   
        /// <summary>
        /// Displayes emp saved msg, as the page is redirected to itself after save
        /// </summary>
        void RegisterEmployeeSavedMsg()
        {
            if (SessionManager.EmployeeSaved)
            {
                //JavascriptHelper.DisplayClientMsgWithTimeout("Employee information saved.", Page);
                SetMessage(lblMsg, Resources.Messages.EmployeeInformationSaved);
                SessionManager.EmployeeSaved = false;
            }
        }


        protected void RetirementDisplayValidation()
        {

            PayrollPeriod lastPayrollDate = CommonManager.RetiredValidPayrollPeriod(GetEmployeeId());

            if (EmployeeManager.IsRetiredOrResignedIncludedInCalculation(GetEmployeeId()))
            {
                chkRetirement.Disable();
                calRetirementDate.Disable();
                cmbTypes.Disable();
            }
            else if (lastPayrollDate == null)
            {
                chkRetirement.Disable();
                calRetirementDate.Disable();
                cmbTypes.Disable();
                //NewMessage.ShowNormalPopup("Please define a payroll period first, partial saved payroll period not exists.");
            }
            else if (CalculationManager.IsCalculationSavedForEmployee(lastPayrollDate.PayrollPeriodId, GetEmployeeId()))
            {
                chkRetirement.Disable();
                calRetirementDate.Disable();
                cmbTypes.Disable();
                NewMessage.ShowNormalPopup(Resources.Messages.NotEdiableDuringPayrollGeneration);
            }
           
        }

        protected bool RetirementSaveValidation()
        {

            PayrollPeriod lastPayrollDate = CommonManager.RetiredValidPayrollPeriod(GetEmployeeId());

            if (EmployeeManager.IsRetiredOrResignedIncludedInCalculation(GetEmployeeId()))
            {
                if(chkRetirement.Checked==false ||
                    string.IsNullOrEmpty(calRetirementDate.Text.Trim()) ||
                    GetEngDate(calRetirementDate.Text.Trim()) != EmployeeManager.GetEmployeeById(GetEmployeeId()).EHumanResources[0].DateOfRetirementEng)
                {
                    
                    NewMessage.ShowWarningMessage("Retirement can not be changed or removed after the effect of retirement in salary.");
                    return false;
                }
            }
           
            else if (lastPayrollDate != null && CalculationManager.IsCalculationSavedForEmployee(lastPayrollDate.PayrollPeriodId, GetEmployeeId()))
            {
                //DisableRetiredResigned();
                //chkIsResigned.ToolTip = calDateOfResignation.ToolTip = Resources.Messages.NotEdiableDuringPayrollGeneration;
                //chkIsRetired.ToolTip = calDateOfRetirement.ToolTip = Resources.Messages.NotEdiableDuringPayrollGeneration;
            }
            else if(chkRetirement.Checked)
            {
                CustomDate date = CustomDate.GetCustomDateFromString(lastPayrollDate.StartDate, IsEnglish);

                string jsCode = "";



                if (IsEnglish
                    || SessionManager.User.IsEnglishPreferred == null
                    || (SessionManager.User.IsEnglishPreferred != null &&
                        SessionManager.User.IsEnglishPreferred == SessionManager.CurrentCompany.IsEnglishDate)
                    )
                {
                    date = date.DecrementByOneDay();
                }
                // Nepali calendar but english date
                else
                {
                    date = CustomDate.GetCustomDateFromString(lastPayrollDate.StartDate, false);
                    date = CustomDate.ConvertNepToEng(date);
                    date = date.DecrementByOneDay();
                }


                DateTime retDate = GetEngDate(calRetirementDate.Text.Trim()).Date;

                //if(retDate < date.EnglishDate)
                //{
                //    NewMessage.ShowWarningMessage("Retirement can not exceed " + date.ToString() + " date.");
                //    return false;
                //}
                


            }



            //if (chkRetirement.Checked)
            //{
            //    if (string.IsNullOrEmpty(calRetirementDate.Text.Trim()))
            //    {
            //        NewMessage.ShowWarningMessage("Retirement date is required.");
            //        return false;
            //    }
            //    if (cmbTypes.SelectedItem == null || cmbTypes.SelectedItem.Value == null)
            //    {
            //        NewMessage.ShowWarningMessage("Retirement type is required.");
            //        return false;
            //    }


              
            //}



            return true;
        }

        protected void ButtonNext_Click(object sender, DirectEventArgs e)
        {
            EEmployee eEmployee = new EEmployee();

            if (!string.IsNullOrEmpty(Request.QueryString["ID"]) && RetirementSaveValidation() == false)
                return;

            eEmployee = GetEmployeePersonalDetails();

          

           
                eEmployee.EmployeeId = int.Parse(Request.QueryString["ID"]);
           

           


            Status respStatus = NewHRManager.UpdateRetirement(eEmployee);
            if (respStatus.IsSuccess)
            {
               
                    SetMessage(lblMsg, Resources.Messages.EmployeeInformationUpdatedMsg);
                    //NewMessage.ShowNormalPopup(Resources.Messages.EmployeeInformationUpdatedMsg);

                
            }
            else
            {
                SetWarning(lblMsg, respStatus.ErrorMessage);
                //NewMessage.ShowWarningPopup(respStatus.ErrorMessage);
            }
        }

        private EEmployee GetEmployeePersonalDetails()
        {
            EEmployee eEmployee = new EEmployee();
            
            EHumanResource hr = new EHumanResource();


            if (chkRetirement.Checked)
            {
                hr.DateOfRetirement = calRetirementDate.Text.Trim();
                hr.DateOfRetirementEng = GetEngDate(hr.DateOfRetirement);
                eEmployee.IsRetired = true;
                hr.RetirementType = int.Parse(cmbTypes.SelectedItem.Value);
            }

            eEmployee.EHumanResources.Add(hr);

            return eEmployee;
        }

    
        void LoadEmploymentDetails()
        {
            EEmployee employee = EmployeeManager.GetEmployeeById(GetEmployeeId());



            if (employee.IsRetired != null && employee.IsRetired.Value)
            {
                chkRetirement.Checked = employee.IsRetired.Value;
                calRetirementDate.Text = employee.EHumanResources[0].DateOfRetirement;
                calRetirementDate.Enable();
                cmbTypes.Enable();
                if (employee.EHumanResources[0].RetirementType != null)
                {
                    ExtControlHelper.ComboBoxSetSelected(employee.EHumanResources[0].RetirementType.ToString(),
                        cmbTypes);
                }
            }
            if (employee.IsResigned != null && employee.IsResigned.Value)
            {
                chkRetirement.Checked = employee.IsResigned.Value;
                calRetirementDate.Text = employee.EHumanResources[0].DateOfResignation;
                calRetirementDate.Enable();
                cmbTypes.Enable();
                if (employee.EHumanResources[0].RetirementType != null)
                {
                    ExtControlHelper.ComboBoxSetSelected(employee.EHumanResources[0].RetirementType.ToString(),
                        cmbTypes);
                }
            }
        }



    
        protected void Page_PreRender(object sender, EventArgs e)
        {
            bool isEng = false;
            if (UserManager.IsEnglishDatePreferredInNepaliDateSys())
                isEng = true;

            UserControl ctrl = Master.ContentHeaderCtrl();
            ResourceManager rm = ctrl.FindControl("ResourceManager1") as ResourceManager;

            rm.RegisterClientInitScript("Date11111", string.Format("isEnglish = {0};", isEng.ToString().ToLower()));

            rm.RegisterClientInitScript("Date22222", string.Format("todayDate = '{0}';function calendarOnClick(event, sourceElement, e3){{}};",
                                                                      CustomDate.GetTodayDate(isEng).ToString()));
        }

    }
}