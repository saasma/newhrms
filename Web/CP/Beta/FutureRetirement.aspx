﻿<%@ Page Title="Future Retirement" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="FutureRetirement.aspx.cs" Inherits="Web.NewHR.FutureRetirement" %>

<%@ Register Src="~/NewHR/UserControls/EmployeeDetailsCtl.ascx" TagName="EmployeeDetailsCtl"
    TagPrefix="uc1" %>
<%@ Register Src="~/newhr/UserControls/EmployeeWizard.ascx" TagName="EmployeeWizard"
    TagPrefix="uc2" %>
<%@ Register Src="~/NewHR/UserControls/EmpDetailsLeftMenu.ascx" TagName="EmpDetailsWizard"
    TagPrefix="ucEW" %>
<%@ MasterType VirtualPath="~/Master/NewDetails.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .groupLevelUL
        {
            padding: 0px;
            margin: 0px;
        }
        .groupLevelUL li
        {
            float: left;
            width: 180px;
            padding-right: 15px;
            list-style: none;
        }
        .userDetailsClass
        {
            background-color: #D8E7F3;
            height: 50px;
            text-align: center;
            padding-top: 10px;
            margin-bottom: 10px;
        }
    </style>
    <script type="text/javascript">
       
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body" runat="server" id="divTitle" >
                <h4>
                    Employee Personal Details
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="innerLR">
            <%--<uc2:EmployeeWizard runat="server" Id="EmployeeWizard1" />--%>
            <table>
                <tr>
                    
                    <td valign="top">
                        <div class="userDetailsClass" runat="server" id="divEmployeeDetails">
                            <uc1:EmployeeDetailsCtl Id="EmployeeDetailsCtl1" runat="server" />
                        </div>
                        <ext:Container ID="lblMsg" runat="server" />
                        <table runat="server" id="tableRetirement2" style="width: inherit; margin-top: 25px;
                            margin-bottom: 15px;">
                            <tbody>
                                <tr>
                                    <td>
                                        <ext:Checkbox ID="chkRetirement" runat="server" LabelSeparator="" BoxLabel="Retirement Date"
                                            BoxLabelAlign="After" LabelWidth="150">
                                            <Listeners>
                                                <Change Handler="if(this.getValue()==true) {#{calRetirementDate}.enable();#{cmbTypes}.enable();} else {#{calRetirementDate}.disable();#{cmbTypes}.disable();}" />
                                            </Listeners>
                                        </ext:Checkbox>
                                        <pr:CalendarExtControl Disabled="true" Width="180px" ID="calRetirementDate" runat="server"
                                            LabelAlign="Top" LabelSeparator="" />
                                    </td>
                                    <td style="padding-left: 10px">
                                        <ext:ComboBox Disabled="true" FieldLabel="Retirement Type" ID="cmbTypes" Width="180px"
                                            runat="server" ValueField="EventID" ForceSelection="true" DisplayField="Name"
                                            LabelAlign="Top" LabelSeparator="" QueryMode="Local">
                                            <Store>
                                                <ext:Store ID="Store12" runat="server">
                                                    <Model>
                                                        <ext:Model ID="Model13" IDProperty="EventID" runat="server">
                                                            <Fields>
                                                                <ext:ModelField Name="EventID" />
                                                                <ext:ModelField Name="Name" />
                                                            </Fields>
                                                        </ext:Model>
                                                    </Model>
                                                </ext:Store>
                                            </Store>
                                        </ext:ComboBox>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="buttonBlock">
                            <ext:Button ID="btnNext" Cls="btn btn-primary" Width="100px" runat="server" Text="&nbsp;&nbsp;Save&nbsp;&nbsp;"
                                ValidationGroup="InsertUpdate">
                                <DirectEvents>
                                    <Click OnEvent="ButtonNext_Click">
                                        <EventMask ShowMask="true" />
                                    </Click>
                                </DirectEvents>
                                <Listeners>
                                    <Click Handler="valGroup = 'InsertUpdate';  if(CheckValidation()) return this.disable(); else return false;">
                                    </Click>
                                </Listeners>
                            </ext:Button>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
