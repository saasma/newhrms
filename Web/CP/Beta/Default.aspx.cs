﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
//using System.Web.UI.DataVisualization.Charting;
using BLL.Base;
using System.Globalization;
using Utils.Calendar;
using BLL.BO;
namespace Web.CP.Beta
{
    public partial class Default : BasePage
    {      

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public new bool IsAccessible(string url)
        {
            return UserManager.IsPageAccessible(url);

        }
    }
}