﻿<%@ Page Title="Beta Dashboard" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Web.CP.Beta.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Beta Dashboard
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel" style="padding-left: 10px">
        <div class="innerLR">
            <div class="col-sm-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="panel-btns" style="display: none;">
                            <a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel">
                                <i class="fa fa-minus"></i></a>
                        </div>
                        <h3 class="panel-title">
                            Training
                        </h3>
                    </div>
                    <div class="panel-body panel-body-small">
                        <ul role="menu" class="dropdown-menu dropdown-demo-only custommenulist">
                            <li><a id="A11" visible='<%# IsAccessible("newhr/TrainingMod/TrainingPlanning.aspx") %>'
                                target="_blank" runat="server" href="~/newhr/TrainingMod/TrainingPlanning.aspx">
                                <i class="fa fa-bars"></i>Training Planning</a> </li>
                            <li><a id="A1" visible='<%# IsAccessible("newhr/TrainingMod/TrainingAdministration.aspx") %>'
                                target="_blank" runat="server" href="~/newhr/TrainingMod/TrainingAdministration.aspx">
                                <i class="fa fa-bars"></i>Training Administration</a> </li>
                            <li><a id="A19" visible='<%# IsAccessible("newhr/TrainingMod/TrainingEmployee.aspx") %>'
                                target="_blank" runat="server" href="~/newhr/TrainingMod/TrainingEmployee.aspx">
                                <i class="fa fa-bars"></i>Training Employees</a> </li>
                            <li><a id="A20" visible='<%# IsAccessible("newhr/TrainingMod/TrainingAttendance.aspx") %>'
                                target="_blank" runat="server" href="~/newhr/TrainingMod/TrainingAttendance.aspx">
                                <i class="fa fa-bars"></i>Training Attendance</a> </li>
                        </ul>
                        <div style="clear: both">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="panel-btns" style="display: none;">
                            <a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel">
                                <i class="fa fa-minus"></i></a>
                        </div>
                        <h3 class="panel-title">
                            Award
                        </h3>
                    </div>
                    <div class="panel-body panel-body-small">
                        <ul role="menu" class="dropdown-menu dropdown-demo-only custommenulist">
                            <li><a id="A2" visible='<%# IsAccessible("CP/AwardList.aspx") %>' target="_blank"
                                runat="server" href="~/cp/AwardList.aspx"><i class="fa fa-bars"></i>Award List</a>
                            </li>
                            <li><a id="A3" visible='<%# IsAccessible("CP/EmployeeAwardList.aspx") %>' target="_blank"
                                runat="server" href="~/CP/EmployeeAwardList.aspx"><i class="fa fa-bars"></i>Employee
                                Award List</a> </li>
                        </ul>
                        <div style="clear: both">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="panel-btns" style="display: none;">
                            <a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel">
                                <i class="fa fa-minus"></i></a>
                        </div>
                        <h3 class="panel-title">
                            Rigo Activity
                        </h3>
                    </div>
                    <div class="panel-body panel-body-small">
                        <ul role="menu" class="dropdown-menu dropdown-demo-only custommenulist">
                            <li><a id="A6" visible='<%# IsAccessible("NewHR/EmployeeActivityList.aspx") %>' target="_blank"
                                runat="server" href="~/NewHR/EmployeeActivityList.aspx"><i class="fa fa-bars"></i>Acitivity
                                Report</a> </li>
                        </ul>
                        <div style="clear: both">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="panel-btns" style="display: none;">
                            <a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel">
                                <i class="fa fa-minus"></i></a>
                        </div>
                        <h3 class="panel-title">
                            Employee Planning
                        </h3>
                    </div>
                    <div class="panel-body panel-body-small">
                        <ul role="menu" class="dropdown-menu dropdown-demo-only custommenulist">
                            <li><a id="A4" visible='<%# IsAccessible("NewHR/EmployeeHierarchy.aspx") %>' target="_blank"
                                runat="server" href="~/NewHR/EmployeeHierarchy.aspx"><i class="fa fa-bars"></i>Employee
                                Hierarchy</a> </li>
                            <li><a id="A5" visible='<%# IsAccessible("NewHR/EmpPlanning.aspx") %>' target="_blank"
                                runat="server" href="~/NewHR/EmpPlanning.aspx"><i class="fa fa-bars"></i>Employee
                                Planning</a> </li>
                        </ul>
                        <div style="clear: both">
                        </div>
                    </div>
                </div>
            </div>

             <div class="col-sm-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="panel-btns" style="display: none;">
                            <a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel">
                                <i class="fa fa-minus"></i></a>
                        </div>
                        <h3 class="panel-title">
                            Attendance Report
                        </h3>
                    </div>
                    <div class="panel-body panel-body-small">
                        <ul role="menu" class="dropdown-menu dropdown-demo-only custommenulist">
                            <li><a id="A7" visible='<%# IsAccessible("Attendance/HRAttMontlyDetailDevice.aspx") %>' target="_blank"
                                runat="server" href="~/Attendance/HRAttMontlyDetailDevice.aspx"><i class="fa fa-bars"></i>Time Attendance Detail</a> </li>
                            <li><a id="A8" visible='<%# IsAccessible("Attendance/HREmployeeAttSummary.aspx") %>' target="_blank"
                                runat="server" href="~/Attendance/HREmployeeAttSummary.aspx"><i class="fa fa-bars"></i>Employee Attendance Summary</a> </li>
                        </ul>
                        <div style="clear: both">
                        </div>
                    </div>
                </div>
            </div>



             <div class="col-sm-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="panel-btns" style="display: none;">
                            <a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel">
                                <i class="fa fa-minus"></i></a>
                        </div>
                        <h3 class="panel-title">
                             Attendance Request Approval
                        </h3>
                    </div>
                    <div class="panel-body panel-body-small">
                        <ul role="menu" class="dropdown-menu dropdown-demo-only custommenulist">
                            <li><a id="A9" visible='<%# IsAccessible("NewHR/AttRequestsApproval.aspx") %>' target="_blank"
                                runat="server" href="~/NewHR/AttRequestsApproval.aspx"><i class="fa fa-bars"></i>Attendance Requests Approval</a> </li>
                           
                        </ul>
                        <div style="clear: both">
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-sm-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="panel-btns" style="display: none;">
                            <a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel">
                                <i class="fa fa-minus"></i></a>
                        </div>
                        <h3 class="panel-title">
                             Training Attendance Management
                        </h3>
                    </div>
                    <div class="panel-body panel-body-small">
                        <ul role="menu" class="dropdown-menu dropdown-demo-only custommenulist">
                            <li><a id="A10" visible='<%# IsAccessible("NewHR/TrainingMod/TrainingEmployee.aspx") %>' target="_blank"
                                runat="server" href="~/NewHR/TrainingMod/TrainingEmployee.aspx"><i class="fa fa-bars"></i>Training Employee List</a> </li>
                           
                        </ul>
                        <div style="clear: both">
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-sm-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="panel-btns" style="display: none;">
                            <a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel">
                                <i class="fa fa-minus"></i></a>
                        </div>
                        <h3 class="panel-title">
                             HR
                        </h3>
                    </div>
                    <div class="panel-body panel-body-small">
                        <ul role="menu" class="dropdown-menu dropdown-demo-only custommenulist">
                            <li><a id="A12" visible='<%# IsAccessible("NewHR/EmployeeCatalogueList.aspx") %>' target="_blank"
                                runat="server" href="~/NewHR/EmployeeCatalogueList.aspx"><i class="fa fa-bars"></i>Employee Catalogue List</a> </li>
                           
                        </ul>
                        <div style="clear: both">
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="panel-btns" style="display: none;">
                            <a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel">
                                <i class="fa fa-minus"></i></a>
                        </div>
                        <h3 class="panel-title">
                             Backdated Retirement
                        </h3>
                    </div>
                    <div class="panel-body panel-body-small">
                        <ul role="menu" class="dropdown-menu dropdown-demo-only custommenulist">
                            <li><a id="A13" visible='<%# IsAccessible("CP/PowerEdit/BackdatedRetirement.aspx") %>' target="_blank"
                                runat="server" href="~/CP/PowerEdit/BackdatedRetirement.aspx"><i class="fa fa-bars"></i>Backdated Retirement</a> </li>
                           
                        </ul>
                        <div style="clear: both">
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="panel-btns" style="display: none;">
                            <a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel">
                                <i class="fa fa-minus"></i></a>
                        </div>
                        <h3 class="panel-title">
                             Employee Transfers
                        </h3>
                    </div>
                    <div class="panel-body panel-body-small">
                        <ul role="menu" class="dropdown-menu dropdown-demo-only custommenulist">
                            <li><a id="A14" visible='<%# IsAccessible("CP/BranchTransferNew.aspx") %>' target="_blank"
                                runat="server" href="~/CP/BranchTransferNew.aspx"><i class="fa fa-bars"></i>Branch Transfer</a> </li>   
                             <li><a id="A15" visible='<%# IsAccessible("CP/DepartmentTransferNew.aspx") %>' target="_blank"
                                runat="server" href="~/CP/DepartmentTransferNew.aspx"><i class="fa fa-bars"></i>Department Transfer</a> </li>   
                             <li><a id="A16" visible='<%# IsAccessible("CP/DesignationTransferNew.aspx") %>' target="_blank"
                                runat="server" href="~/CP/DesignationTransferNew.aspx"><i class="fa fa-bars"></i>Designation Change</a> </li> 
                              <li><a id="A17" visible='<%# IsAccessible("CP/LocationTransferNew.aspx") %>' target="_blank"
                                runat="server" href="~/CP/LocationTransferNew.aspx"><i class="fa fa-bars"></i>Location Transfer</a> </li>                       
                        </ul>                        
                        </ul>
                        <div style="clear: both">
                        </div>
                    </div>

                </div>
            </div>

            <div class="col-sm-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="panel-btns" style="display: none;">
                            <a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel">
                                <i class="fa fa-minus"></i></a>
                        </div>
                        <h3 class="panel-title">
                             Manage Employee Permission
                        </h3>
                    </div>
                    <div class="panel-body panel-body-small">
                        <ul role="menu" class="dropdown-menu dropdown-demo-only custommenulist">
                            <li><a id="A18" visible='<%# IsAccessible("User/EmpPermission.aspx") %>' target="_blank"
                                runat="server" href="~/User/EmpPermission.aspx"><i class="fa fa-bars"></i>Manage Employee Permission</a> </li>
                           
                        </ul>
                        <div style="clear: both">
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
