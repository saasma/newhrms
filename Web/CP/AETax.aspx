﻿<%@ Page Title="Tax details" Language="C#" MasterPageFile="~/Master/ForPopupPage.Master" AutoEventWireup="true"
    CodeBehind="AETax.aspx.cs" Inherits="Web.CP.AETax" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">
        function closePopup() {
            window.returnValue = "ReloadTax";
            window.close();
        }
    </script>
    <style type="text/css">
    .paddinAll{ padding:10px;}
    table tr td{ padding:0 10px 10px 0 ;}
    </style>
    

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
<div class="popupHeader"></div>
<div class="marginal">
<div class="bevel paddinAll" style="width:400px">    
<table cellpadding="0" cellspacing="0"  width="380px">
    <tr>
    <td  width="90px">Title</td>
    <td width="270px">    <asp:TextBox ID="txtTitle" Width="151px" runat="server"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtTitle"
        Display="None" ErrorMessage="Title is required." ValidationGroup="AETax"></asp:RequiredFieldValidator></td>
    </tr>
    <tr>
    <td width="90px">Abbreviation</td>
    <td width="270px"><asp:TextBox ID="txtAbbreviation" Width="151px" runat="server"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtAbbreviation"
        Display="None" ErrorMessage="Abbreviation is required." ValidationGroup="AETax"></asp:RequiredFieldValidator></td>
    </tr>
    <tr>
    <td width="90px">Paid By</td>
    <td width="270px">    <asp:DropDownList ID="ddlPaidBy" Width="154px" runat="server">
    </asp:DropDownList></td>
    </tr>
    </table>
    </div>
    
   

    <div class="clear">
    <asp:Button ID="btnAdd" runat="server"  CssClass="save" style="margin-top:10px" OnClientClick="valGroup='AETax';return CheckValidation()"
        Text="Save" ValidationGroup="AETax" OnClick="btnAdd_Click" />
        </div>
         </div>
</asp:Content>
