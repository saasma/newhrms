﻿<%@ Page Title="Deduction list" Language="C#" MasterPageFile="~/Master/ForPopupPage.Master"
    AutoEventWireup="true" CodeBehind="DeductionList.aspx.cs" Inherits="Web.CP.DeductionList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">

        function closePopup() {
      

            // alert(window.opener.parentReloadCallbackFunction)
            //if ($.browser.msie == false && typeof (window.opener.reloadDeduction) != 'undefined' && ($.browser.chrome || $.browser.safari)) {
                window.opener.reloadDeduction(window);
//            } else {
//                window.returnValue = "ReloadDeduction";
//                window.close();
//            }
        }

        function popupCreateNewCall() {
            var ret = popupCreateNew();
            if (typeof (ret) != 'undefined') {
                if (ret == "Reload") {
                    __doPostBack('Reload', '');
                }
            }
        }

        function reloadDeduction(closingWindow) {
            closingWindow.close();
            __doPostBack('Reload', '');
        }

        function doubleClickHander(lst) {
            $('#<%= btnOk.ClientID %>').click();
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <div class="popupHeader">
        <h2 class="headlinespop">
            Deduction List
        </h2>
    </div>
    <div class="bevel marginal">
        <div class="fields paddpop">
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td valign="top">
                        <asp:ListBox   ondblclick="doubleClickHander(this)" SelectionMode="Multiple" ID="lstDeductionList" DataTextField="Title" DataValueField="DeductionId"
                            runat="server" Height="238px" Width="217px"></asp:ListBox>
                        <asp:RequiredFieldValidator ID="valReqdName" runat="server" ControlToValidate="lstDeductionList"
                            Display="None" ErrorMessage="Deduction must be selected for adding." ValidationGroup="Add"></asp:RequiredFieldValidator>
                    </td>
                    <td valign="top" style="padding-left: 10px;">
                        <asp:Button ID="btnOk" runat="server" Text="Ok" CssClass="update" ValidationGroup="Add"
                            OnClientClick="valGroup='Add';return CheckValidation()" OnClick="btnOk_Click" />
                            
                        <br />
                        <asp:Button ID="btnCreateNew" runat="server" CssClass="save" Text="Create"
                OnClientClick="popupCreateNewCall();return false;" />
                <br />
                        <asp:Button ID="btnCancel" runat="server" CssClass="cancel" Text="Cancel" OnClientClick="closePopup();return false;" />
                    </td>
                </tr>
            </table>
            
            <br />
            <asp:RadioButtonList ID="rdbListAddTo" runat="server">
                <asp:ListItem Selected="True" Value="1">Add to this employee </asp:ListItem>
                <asp:ListItem Value="2">Add to all employees</asp:ListItem>
            </asp:RadioButtonList>
            <asp:LinkButton ID="btnRefresh" runat="server" Text="Refresh" Style="display: none" />
        </div>
    </div>
</asp:Content>
