﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using Utils.Helper;
using Utils.Web;
using BLL.Base;
using DAL;
using Utils.Base;
using Bll;

namespace Web
{


    public partial class HPLVoucherList : BasePage
    {

        int payrollPeriodId;
        /// <summary>
        /// For Excel export to work
        /// </summary>
        /// <param name="control"></param>
        public override void VerifyRenderingInServerForm(Control control)
        {

        }

        void SetPeriod()
        {
            int month = 0;
            int year = 0;

            if (!string.IsNullOrEmpty(ddlPayrollFromMonth.SelectedValue))
            {


                month = int.Parse(ddlPayrollFromMonth.SelectedValue);
                year = int.Parse(ddlPayrollFromYear.SelectedValue);

                PayrollPeriod period = CommonManager.GetPayrollPeriod(month, year);
                if (period != null)
                    payrollPeriodId = period.PayrollPeriodId;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {


            if (!IsPostBack)
            {
                //PayrollPeriod payroll = CommonManager.GetLastPayrollPeriod();
                //if (payroll != null)
                //{
                //    title.InnerHtml += payroll.Name;
                //}




                CommonManager.SetFirstAndLastFinalSavedPayrollPeriod(ddlPayrollFromYear, ddlPayrollFromMonth);

            }
            SetPeriod();
            

            {

                BindEmployees();

            }
        }

       



       

        //string value = dataSource.GetValue(header);
        void BindEmployees()
        {
            if (payrollPeriodId == 0)
                return;

            List<GetOriginalHPLVoucherReportResult> list = EmployeeManager.GetOriginalHPLVoucherReport(payrollPeriodId);

         

            gvEmployeeIncome.DataSource = list;
            gvEmployeeIncome.DataBind();        
        }



        protected void btnExport_Click(object sender, EventArgs e)
        {

            //GridViewExportUtil.Export("VoucherList.xls", gvEmployeeIncome);
            ExportToExcel();

        }
        public void ExportToExcel()
        {

            List<GetOriginalHPLVoucherReportResult> list = EmployeeManager.GetOriginalHPLVoucherReport(payrollPeriodId);


            ExcelHelper.ExportToExcel<GetOriginalHPLVoucherReportResult>(
                  "Voucher List.xls",
                  list,
                  new List<string> { "EmployeeName" },
                  new List<string> { },
                  new Dictionary<string, string> { }
                  , new List<string> { "Debit", "Credit" }
                  , new Dictionary<string, string> { }
                  , new List<string> { "PostingPeriod", "Date", "Currency", "Account", "Debit", "Credit","Memo","Entity","Department","Class","Location" }

                   );

            return;

           // bool gridViewLastColumnVisibility = gvEmployeeIncome.Columns[gvEmployeeIncome.Columns.Count - 1].Visible;
           // GridLines gridViewGridLines = gvEmployeeIncome.GridLines;




           // Response.Clear();

           // Response.AddHeader("content-disposition", "attachment; filename=\"Voucher List.xls\"");

           // //Response.Charset = "";


           // Response.ContentType = "application/vnd.xls";
           // //Response.ContentType = "application/ms-excel";
           // Response.ContentEncoding = System.Text.Encoding.Default;
           // //Response.ContentEncoding = Encoding.;
           // Response.Charset = "utf-8";

           // Response.HeaderEncoding = Encoding.UTF8;
           // System.IO.StringWriter stringWrite = new System.IO.StringWriter();

           // System.Web.UI.HtmlTextWriter htmlWrite =
           // new HtmlTextWriter(stringWrite);
           // //  gvw.HeaderStyle.BackColor = System.Drawing.Color.Red;



           // //gvw.DataSource = CalculationManager.GetCalculationList(SessionManager.CurrentCompanyId,
           // //        this.GetPayrollPeriodId(), 1,
           // //        999999, ref _tempCount, chkHasRetiredOrResigned.Checked, null);
           // // gvw.DataBind();





           // ClearControls(gvEmployeeIncome);


           //// gvw.Columns[gvEmployeeIncome.Columns.Count - 1].Visible = false;
           // gvEmployeeIncome.GridLines = GridLines.Both;


           // foreach (GridViewRow r in gvEmployeeIncome.Rows)
           // {
           //     int columnIndex = 0;
           //     foreach (TableCell c in r.Cells)
           //     {
           //         //skip two columns
           //         if (++columnIndex > 2)
           //             c.HorizontalAlign = HorizontalAlign.Right;

           //         //c.Attributes.Add("class", "text");
           //         // c.Text = r.Cells.ToString();
           //         c.Width = 80;

           //     }

           // }


           // gvEmployeeIncome.RenderControl(htmlWrite);

           // Response.Write(stringWrite.ToString());

           // Response.End();

           // gvEmployeeIncome.Columns[gvEmployeeIncome.Columns.Count - 1].Visible = gridViewLastColumnVisibility;
           // gvEmployeeIncome.GridLines = gridViewGridLines;

           // //gvw.DataSource = CalculationManager.GetCalculationList(SessionManager.CurrentCompanyId,
           // //        this.GetPayrollPeriodId(), 1,
           // //        int.Parse(pagingCtl.DDLRecords.SelectedValue), ref _tempCount, chkHasRetiredOrResigned.Checked, null);

           // //gvw.DataBind();


        }

        private void ClearControls(Control control)
        {
            for (int i = control.Controls.Count - 1; i >= 0; i--)
            {
                ClearControls(control.Controls[i]);
            }
            //
            // if(control is Table)

            if (!(control is TableCell))
            {
                if (control.GetType().GetProperty("SelectedItem") != null)
                {
                    LiteralControl literal = new LiteralControl();
                    control.Parent.Controls.Add(literal);
                    try
                    {
                        literal.Text =
                            (string)control.GetType().GetProperty("SelectedItem").
                                GetValue(control, null);
                    }
                    catch
                    { }
                    control.Parent.Controls.Remove(control);
                }
                else if (control.GetType().GetProperty("Text") != null)
                {
                    LiteralControl literal = new LiteralControl();
                    control.Parent.Controls.Add(literal);
                    literal.Text =
                        (string)control.GetType().GetProperty("Text").
                            GetValue(control, null);
                    control.Parent.Controls.Remove(control);
                }
            }
            return;
        }





        /// <summary>
        /// Replace any of the contained controls with literals
        /// </summary>
        /// <param name="control"></param>
        private void PrepareGridViewForExport(Control gv)
        {



            LinkButton lb = new LinkButton();

            Literal l = new Literal();

            string name = String.Empty;

            for (int i = 0; i < gv.Controls.Count; i++)
            {


                if (gv.Controls[i].GetType() == typeof(LinkButton))
                {

                    l.Text = (gv.Controls[i] as LinkButton).Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                else if (gv.Controls[i].GetType() == typeof(TextBox))
                {

                    l.Text = (gv.Controls[i] as TextBox).Text.ToString();

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);


                }

                if (gv.Controls[i].HasControls())
                {

                    PrepareGridViewForExport(gv.Controls[i]);

                }

            }
        }

    }

 

}
