﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Base;
using BLL.Manager;
using Utils.Web;
using DAL;
using Utils.Helper;
using BLL.BO;

namespace Web.CP
{
    public partial class AddOnSetting : BasePage
    {
        PayManager payMgr = new PayManager();
        CommonManager commonMgr = new CommonManager();

        int addOnId = 0;
        int payrollperiod = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            addOnId = UrlHelper.GetIdFromQueryString("Id");
            string value = Request.QueryString["PId"];
            if (!string.IsNullOrEmpty(value))
            {
                value = value.Replace(":1", "");
                int.TryParse(value, out payrollperiod);
            }

            if (!IsPostBack)
            {
                Initialise();
            }
        }

        void Initialise()
        {

            List<PayrollPeriodBO> payrollPeriodList = CommonManager.GetCurrentYear(SessionManager.CurrentCompanyId);

            //ddlPayrollPeriods.DataSource = payrollPeriodList;
            //ddlPayrollPeriods.DataBind();

            ddlShowAfterPeriod.DataSource = payrollPeriodList;
            ddlShowAfterPeriod.DataBind();

            ddlShowAfterPeriod.ClearSelection();
            ddlShowAfterPeriod.Items.Insert(0, new ListItem { Value = "-1", Text = "--Select--" });

            //List<PayrollPeriodBO> payrollPeriodList = CommonManager.GetCurrentYear(SessionManager.CurrentCompanyId);

            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.Janata ||
                CommonManager.CompanySetting.WhichCompany == WhichCompany.Prabhu ||
                CommonManager.CompanySetting.WhichCompany == WhichCompany.MBL)
            { }
            else
                ddlType.Items.RemoveAt(1);
            //if (ddlShowAfterPeriod.Items.Count == 0)
            //    ddlShowAfterPeriod.Items.AddRange(CommonManager.AppendMiddleTaxPaidList(payrollPeriodList, false, false).ToArray());
            //CommonManager.AppendMiddleTaxPaidList(payrollPeriodList);



            btnDelete.Visible = false;
            if (addOnId != 0)
            {
                btnDelete.Visible = true;
                AddOn addon = PayManager.GetAddOn(addOnId);
                
                if (addon != null)
                {
                    Process(ref addon);
                    btnSave.Text = Resources.Messages.Update;
                }
            }

        }

        void Process(ref AddOn entity)
        {
            if (entity == null)
            {
                entity = new AddOn();
                entity.PayrollPeriodId = payrollperiod;
                entity.Name = txtName.Text.Trim();
                entity.AddOnType = int.Parse(ddlType.SelectedValue);
                //entity.ShowAfterPayrollPeriodId = int.Parse(ddlShowAfterPeriod.SelectedValue);
                entity.ShowInPaySlip = chkShowInPayslip.Checked;

                if (ddlShowAfterPeriod.SelectedValue == "-1")
                    entity.ShowAfterPayrollPeriodId = null;
                else
                    entity.ShowAfterPayrollPeriodId = int.Parse(ddlShowAfterPeriod.SelectedValue);
            }
            else
            {
                txtName.Text = entity.Name;

                ///UIHelper.SetSelectedInDropDown(ddlShowAfterPeriod, entity.ShowAfterPayrollPeriodId);
                if (entity.ShowInPaySlip != null)
                    chkShowInPayslip.Checked = entity.ShowInPaySlip.Value;

                if (entity.AddOnType != null)
                    UIHelper.SetSelectedInDropDown(ddlType, entity.AddOnType.Value);

                if (entity.ShowAfterPayrollPeriodId != null)
                    UIHelper.SetSelectedInDropDown(ddlShowAfterPeriod, entity.ShowAfterPayrollPeriodId.Value);
            }
        }



        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {

                AddOn entity = null;
                Process(ref entity);


                {
                    entity.AddOnId = addOnId;



                    PayManager.SaveUpdateAddOn(entity);
                    JavascriptHelper.DisplayClientMsg("Add on saved.", Page, "closePopup();");
                }
            }
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {




            Status status = PayManager.DeleteAddOn(addOnId);

            if (status.IsSuccess)
                JavascriptHelper.DisplayClientMsg("Add on deleted.", Page, "closePopup();");
            else
                JavascriptHelper.DisplayClientMsg(status.ErrorMessage, Page);


        }
    }
}
