﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Manager;

namespace Web.CP
{
    public partial class AddOnPayCalculation : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (CommonManager.CompanySetting.WhichCompany == BLL.WhichCompany.NIBL
                || CommonManager.CompanySetting.WhichCompany == BLL.WhichCompany.Citizen
                ||
                (
                CommonManager.Setting.EnableMultipleAddOn != null && CommonManager.Setting.EnableMultipleAddOn.Value
                ))
            {
                Calculation1.Visible = false;
                Calculation1New.Visible = true;
            }
        }

        /// <summary>
        /// For Excel export to work
        /// </summary>
        /// <param name="control"></param>
        public override void VerifyRenderingInServerForm(Control control)
        {

        }

    }
}
