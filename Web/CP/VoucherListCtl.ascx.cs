﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using Utils.Helper;
using Utils.Web;
using BLL.Base;
using DAL;
using Utils.Base;
using Bll;
using Ext.Net;
using BLL.BO;

namespace Web
{


    public partial class VoucherListCtl : BaseUserControl
    {

       

        int payrollPeriodId;
        /// <summary>
        /// For Excel export to work
        /// </summary>
        /// <param name="control"></param>
        /// 

    


        void SetPeriod()
        {
            int month = 0;
            int year = 0;

            if (!string.IsNullOrEmpty(ddlPayrollFromMonth.SelectedValue))
            {


                month = int.Parse(ddlPayrollFromMonth.SelectedValue);
                year = int.Parse(ddlPayrollFromYear.SelectedValue);

                PayrollPeriod period = CommonManager.GetPayrollPeriod(month, year);
                if (period != null)
                    payrollPeriodId = period.PayrollPeriodId;
            }
        }

        protected void LoadAddOn()
        {

            System.Web.UI.WebControls.ListItem item = ddlAddOnName.SelectedItem;

            System.Web.UI.WebControls.ListItem firstIte = ddlAddOnName.Items[0];

            ddlAddOnName.DataSource = PayManager.GetAddOnForPayrollPeriod(payrollPeriodId);
            ddlAddOnName.DataBind();

            ddlAddOnName.Items.Insert(0, firstIte);

            bool selectedAddOnExists = false;

            if (item != null && item.Selected)
            {
                foreach (System.Web.UI.WebControls.ListItem selItem in ddlAddOnName.Items)
                {
                    if (selItem.Value == item.Value)
                    {
                        selItem.Selected = true;
                        selectedAddOnExists = true;
                    }
                }
            }

            if (selectedAddOnExists == false || item.Value == "-1")
            {
                List<TextValue> list = new List<TextValue>();
                ddlAddOnDateFilter.DataSource = list;
                ddlAddOnDateFilter.DataBind();
            }
            else
            {
                int addOnId = int.Parse(ddlAddOnName.SelectedValue);

                List<TextValue> list = PayManager.GetAddOnDateList(addOnId); ;
                list.Insert(0, new TextValue { Text = "--Select Date--", Value = "All" });

                System.Web.UI.WebControls.ListItem selectedAddOnDate = ddlAddOnDateFilter.SelectedItem;

                ddlAddOnDateFilter.DataSource = list;
                ddlAddOnDateFilter.DataBind();

                if (selectedAddOnDate != null && selectedAddOnDate.Selected)
                {
                    foreach (System.Web.UI.WebControls.ListItem selItem in ddlAddOnDateFilter.Items)
                    {
                        if (selItem.Value == selectedAddOnDate.Value)
                        {
                            selItem.Selected = true;
                        }
                    }
                }
            }
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
           

        }
        protected void Page_Load(object sender, EventArgs e)
        {

            if (this.Visible)
            {
                btnExportDeduction.Visible = (CommonManager.CompanySetting.WhichCompany == WhichCompany.Century);

                if (!IsPostBack && !X.IsAjaxRequest)
                {

                    //PayrollPeriod payroll = CommonManager.GetLastPayrollPeriod();
                    //if (payroll != null)
                    //{
                    //    title.InnerHtml += payroll.Name;
                    //}

                    



                    CommonManager.SetFirstAndLastFinalSavedPayrollPeriod(ddlPayrollFromYear, ddlPayrollFromMonth);

                }
                SetPeriod();


                {

                    //BindEmployees();

                }

                LoadAddOn();
            }
        }

        public void btnLoadSelectedEmpVoucher_Click(object sender, EventArgs e)
        {
            window.Hide();

            int addOnId = -1;
            if (!string.IsNullOrEmpty(ddlAddOnName.SelectedValue) && ddlAddOnName.SelectedValue != "-1")
                addOnId = int.Parse(ddlAddOnName.SelectedValue);

            CheckboxSelectionModel selection = grid.GetSelectionModel() as CheckboxSelectionModel;
            string empIDList = "";
            foreach (var item in selection.SelectedRows)
            {
                if (empIDList == "")
                    empIDList = item.RecordID.ToString();
                else
                    empIDList += "," + item.RecordID.ToString();
            }

            hiddenSelectedEmp.Value = empIDList;

            if (hiddenAddOnId.Value != "" && hiddenAddOnId.Value != addOnId.ToString())            
            {
                hiddenSelectedEmp.Value = "";
            }

            hiddenAddOnId.Value = addOnId.ToString();

            BindEmployees();
        }

        public void btnClearSelection_Click(object sender, EventArgs e)
        {
            window.Hide();

            int addOnId = -1;
            if (!string.IsNullOrEmpty(ddlAddOnName.SelectedValue) && ddlAddOnName.SelectedValue != "-1")
                addOnId = int.Parse(ddlAddOnName.SelectedValue);

           

            hiddenSelectedEmp.Value = "";

            

            hiddenAddOnId.Value = addOnId.ToString();

            BindEmployees();
        }
        public void btnLoadEmployeeFilter_Click(object sender, EventArgs e)
        {
            int addOnId = -1;
            if (!string.IsNullOrEmpty(ddlAddOnName.SelectedValue) && ddlAddOnName.SelectedValue != "-1")
                addOnId = int.Parse(ddlAddOnName.SelectedValue);

            if (addOnId == -1)
            {
                NewMessage.ShowWarningMessage("Add on name should be selected for the filter.");
                return;
            }
            //CheckboxSelectionModel

            //if (hiddenAddOnId.Value == addOnId.ToString())
            //{
            //    window.Center();
            //    window.Show();
            //    return;
            //}
            //else
            //{
            //    hiddenSelectedEmp.Value = "";
            //}

            //hiddenAddOnId.Value = addOnId.ToString();

            grid.Store[0].DataSource = PartialAddOnManager.GetAddOnEmpList(addOnId);
            grid.Store[0].DataBind();

            // Select in grid previously selected employees

            RowSelectionModel sm = this.grid.GetSelectionModel() as RowSelectionModel;

            string[] idList = hiddenSelectedEmp.Value.Split(new char[] { ',' });

            sm.SelectedRows.Clear();
            foreach (string id in idList)
            {
                if (!string.IsNullOrEmpty(id))
                    sm.SelectedRows.Add(new SelectedRow(id));
            }

            

            BindEmployees();

            window.Center();
            window.Show();

        }


        protected void btnLoad_Click(object sender, EventArgs e)
        {
            BindEmployees();
        }

        //string value = dataSource.GetValue(header);
        void BindEmployees()
        {
            if (payrollPeriodId == 0)
                return;



            


              //msg.InnerHtml = empIDList;


            // reset selected add on id
            hiddenAddOnId.Value = "";

            bool? showRetiredOnly = null;
            if (ddlRetirementType.SelectedValue != "All")
                showRetiredOnly = bool.Parse(ddlRetirementType.SelectedValue);

            int addOnId = -1;
            if (!string.IsNullOrEmpty(ddlAddOnName.SelectedValue) && ddlAddOnName.SelectedValue != "-1")
                addOnId = int.Parse(ddlAddOnName.SelectedValue);

           
            //if (hiddenAddOnId.Value != addOnId.ToString())           
            //{
            //    hiddenSelectedEmp.Value = "";
            //}
            if (hiddenSelectedEmp.Value == "")
                msg.InnerHtml = "Voucher generated for all employees.";
            else
                msg.InnerHtml = "Voucher generated for " + (hiddenSelectedEmp.Value.Split(new char[] { ',' }).Length) + " employees only.";


            bool applyAddOndateFilter = false;
            DateTime? addOnDate = null;

            if (ddlAddOnDateFilter.SelectedIndex != 0)
            {
                applyAddOndateFilter = true;
                if (ddlAddOnDateFilter.SelectedItem != null && ddlAddOnDateFilter.SelectedItem.Text != "")
                    addOnDate = Convert.ToDateTime(ddlAddOnDateFilter.SelectedItem.Value);
            }


           
            {
               

                if (addOnId != -1)
                    gvEmployeeIncome.DataSource = BLL.BaseBiz.PayrollDataContext.GetHPLVoucherReportForMulitpleAddOn(payrollPeriodId, addOnId, hiddenSelectedEmp.Value, applyAddOndateFilter, addOnDate);
                else if (isAddOn.Checked == false)
                {

                    gvEmployeeIncome.DataSource = EmployeeManager.GetHPLVoucherReport(payrollPeriodId, showRetiredOnly);

                }
                else
                    gvEmployeeIncome.DataSource = EmployeeManager.GetHPLVoucherReportForAddOn(payrollPeriodId);

                gvEmployeeIncome.DataBind();

                gvDeduction.DataSource = EmployeeManager.GetHPLVoucherDeductionReport(payrollPeriodId, isAddOn.Checked, showRetiredOnly);
                gvDeduction.DataBind();
            }

           





            if (CalculationManager.IsAllEmployeeSavedFinally(payrollPeriodId, false))
            {
                salaryNotSaved.Visible=false;
            }
            else
            {
                int totalEmployees = CalculationManager.GetTotalApplicableForSalary(payrollPeriodId, false);
                int initialSave = new CalculationManager().GetTotalEmployeeInitialSave(payrollPeriodId, false);

                if (totalEmployees != initialSave)
                    salaryNotSaved.Visible = true;
                else
                    salaryNotSaved.Visible = false;

            }


            // Warning if any Income/Deduction head is not mapped
            string headerName = CalculationManager.GetIncomeOrDeductionNotMappedForVoucher(payrollPeriodId);
            if (!string.IsNullOrEmpty(headerName))
            {
                spanWarningForIncomeDeductionHead.InnerHtml = "\"" + headerName + "\" is not mapped from Voucher Setting, if this income/deduction has amount then the voucher will not be correct.";
                spanWarningForIncomeDeductionHead.Visible = true;
            }
            else
                spanWarningForIncomeDeductionHead.Visible = false;

        }



        protected void btnExportDeduction_Click(object sender, EventArgs e)
        {

            bool? showRetiredOnly = null;
            if (ddlRetirementType.SelectedValue != "All")
                showRetiredOnly = bool.Parse(ddlRetirementType.SelectedValue);

            List<GetHPLVoucherDeductionReportResult> list = EmployeeManager.GetHPLVoucherDeductionReport(payrollPeriodId, isAddOn.Checked,showRetiredOnly);

            ExcelHelper.ExportToExcel<GetHPLVoucherDeductionReportResult>(
                "VoucherDeductionList.xls",
                list,
                new List<string> { "Class", "Location", "Entity" },
                new List<string> { },
                new Dictionary<string, string> { { "Department", "Branch" }, { "Account", "MainCode" }, { "Debit", "Amount" }, { "Credit", "LCYAmount" }, { "Memo", "Desc1" } }
                , new List<string> { "Debit", "Credit" }
                , new Dictionary<string, string> { }
                , new List<string> { "Department", "Account", "TranCode", "Debit", "Credit", "Memo" }

                 );

           // GridViewExportUtil.Export("VoucherDeductionList.xls", gvDeduction);

        }

       

        protected void ddlAddOnName_Change(object sender, EventArgs e)
        {
            int addOnId = int.Parse(ddlAddOnName.SelectedValue);

            List<TextValue> list = PayManager.GetAddOnDateList(addOnId); ;
            list.Insert(0, new TextValue { Text = "--Select Date--", Value = "All" });
            ddlAddOnDateFilter.DataSource = list;
            ddlAddOnDateFilter.DataBind();


        }

        protected void btnExport_Click(object sender, EventArgs e)
        {

            bool? showRetiredOnly = null;
            if (ddlRetirementType.SelectedValue != "All")
                showRetiredOnly = bool.Parse(ddlRetirementType.SelectedValue);

            int addOnId = -1;
            if (!string.IsNullOrEmpty(ddlAddOnName.SelectedValue) && ddlAddOnName.SelectedValue != "-1")
                addOnId = int.Parse(ddlAddOnName.SelectedValue);

          

           if (isAddOn.Checked == false && addOnId == -1)
            {
                List<GetHPLVoucherReportResult> list = EmployeeManager.GetHPLVoucherReport(payrollPeriodId, showRetiredOnly);

                ExcelHelper.ExportToExcel<GetHPLVoucherReportResult>(
                    "VoucherList.xls",
                    list,
                    new List<string> { "Class", "Location", "Entity" },
                    new List<string> { },
                    new Dictionary<string, string> { { "Department", "Branch" }, { "Account", "MainCode" }, { "Debit", "Amount" }, { "Credit", "LCYAmount" }, { "Memo", "Desc1" } }
                    , new List<string> { "Debit", "Credit" }
                    , new Dictionary<string, string> { }
                    , new List<string> { "Department", "Account", "EmployeeName", "TranCode", "Debit", "Credit", "Memo" }

                     );
            }
            else if (addOnId == -1)
            {
                List<GetHPLVoucherReportForAddOnResult> list = EmployeeManager.GetHPLVoucherReportForAddOn(payrollPeriodId);

                //if (addOnId == -1)
                //    list = EmployeeManager.GetHPLVoucherReportForAddOn(payrollPeriodId);
                //else
                //    list = BLL.BaseBiz.PayrollDataContext.GetHPLVoucherReportForMulitpleAddOn(payrollPeriodId, addOnId);

                ExcelHelper.ExportToExcel<GetHPLVoucherReportForAddOnResult>(
                    "VoucherList.xls",
                    list,
                    new List<string> { "Class", "Location", "Entity" },
                    new List<string> { },
                    new Dictionary<string, string> { { "Department", "Branch" }, { "Account", "MainCode" }, { "Debit", "Amount" }, { "Credit", "LCYAmount" }, { "Memo", "Desc1" } }
                    , new List<string> { "Debit", "Credit" }
                    , new Dictionary<string, string> { }
                    , new List<string> { "Department", "Account", "EmployeeName", "TranCode", "Debit", "Credit", "Memo" }

                     );
            }
            else
            {
                bool applyAddOndateFilter = false;
                DateTime? addOnDate = null;

                if (ddlAddOnDateFilter.SelectedIndex != 0)
                {
                    applyAddOndateFilter = true;
                    if (ddlAddOnDateFilter.SelectedItem != null && ddlAddOnDateFilter.SelectedItem.Text != "")
                        addOnDate = Convert.ToDateTime(ddlAddOnDateFilter.SelectedItem.Value);
                }


                List<GetHPLVoucherReportForMulitpleAddOnResult> list = BLL.BaseBiz.PayrollDataContext.GetHPLVoucherReportForMulitpleAddOn(payrollPeriodId, addOnId, hiddenSelectedEmp.Value
                    , applyAddOndateFilter, addOnDate)
                    .ToList();



                ExcelHelper.ExportToExcel<GetHPLVoucherReportForMulitpleAddOnResult>(
                    "VoucherList.xls",
                    list,
                    new List<string> { "Class", "Location", "Entity" },
                    new List<string> { },
                    new Dictionary<string, string> { { "Department", "Branch" }, { "Account", "MainCode" }, { "Debit", "Amount" }, { "Credit", "LCYAmount" }, { "Memo", "Desc1" } }
                    , new List<string> { "Debit", "Credit" }
                    , new Dictionary<string, string> { }
                    , new List<string> { "Department", "Account", "EmployeeName", "TranCode", "Debit", "Credit", "Memo" }

                     );
            }


        }
        public void ExportToExcel()
        {
            bool gridViewLastColumnVisibility = gvEmployeeIncome.Columns[gvEmployeeIncome.Columns.Count - 1].Visible;
            GridLines gridViewGridLines = gvEmployeeIncome.GridLines;




            Response.Clear();

            Response.AddHeader("content-disposition", "attachment; filename=\"Voucher List.xls\"");

            //Response.Charset = "";


            Response.ContentType = "application/vnd.xls";
            //Response.ContentType = "application/ms-excel";
            Response.ContentEncoding = System.Text.Encoding.Default;
            //Response.ContentEncoding = Encoding.;
            Response.Charset = "utf-8";

            Response.HeaderEncoding = Encoding.UTF8;
            System.IO.StringWriter stringWrite = new System.IO.StringWriter();

            System.Web.UI.HtmlTextWriter htmlWrite =
            new HtmlTextWriter(stringWrite);
            //  gvw.HeaderStyle.BackColor = System.Drawing.Color.Red;



            //gvw.DataSource = CalculationManager.GetCalculationList(SessionManager.CurrentCompanyId,
            //        this.GetPayrollPeriodId(), 1,
            //        999999, ref _tempCount, chkHasRetiredOrResigned.Checked, null);
            // gvw.DataBind();





            ClearControls(gvEmployeeIncome);


           // gvw.Columns[gvEmployeeIncome.Columns.Count - 1].Visible = false;
            gvEmployeeIncome.GridLines = GridLines.Both;


            foreach (GridViewRow r in gvEmployeeIncome.Rows)
            {
                int columnIndex = 0;
                foreach (TableCell c in r.Cells)
                {
                    //skip two columns
                    if (++columnIndex > 2)
                        c.HorizontalAlign = HorizontalAlign.Right;

                    //c.Attributes.Add("class", "text");
                    // c.Text = r.Cells.ToString();
                    c.Width = 80;

                }

            }


            gvEmployeeIncome.RenderControl(htmlWrite);

            Response.Write(stringWrite.ToString());

            Response.End();

            gvEmployeeIncome.Columns[gvEmployeeIncome.Columns.Count - 1].Visible = gridViewLastColumnVisibility;
            gvEmployeeIncome.GridLines = gridViewGridLines;

            //gvw.DataSource = CalculationManager.GetCalculationList(SessionManager.CurrentCompanyId,
            //        this.GetPayrollPeriodId(), 1,
            //        int.Parse(pagingCtl.DDLRecords.SelectedValue), ref _tempCount, chkHasRetiredOrResigned.Checked, null);

            //gvw.DataBind();


        }

        private void ClearControls(Control control)
        {
            for (int i = control.Controls.Count - 1; i >= 0; i--)
            {
                ClearControls(control.Controls[i]);
            }
            //
            // if(control is Table)

            if (!(control is TableCell))
            {
                if (control.GetType().GetProperty("SelectedItem") != null)
                {
                    LiteralControl literal = new LiteralControl();
                    control.Parent.Controls.Add(literal);
                    try
                    {
                        literal.Text =
                            (string)control.GetType().GetProperty("SelectedItem").
                                GetValue(control, null);
                    }
                    catch
                    { }
                    control.Parent.Controls.Remove(control);
                }
                else if (control.GetType().GetProperty("Text") != null)
                {
                    LiteralControl literal = new LiteralControl();
                    control.Parent.Controls.Add(literal);
                    literal.Text =
                        (string)control.GetType().GetProperty("Text").
                            GetValue(control, null);
                    control.Parent.Controls.Remove(control);
                }
            }
            return;
        }





        /// <summary>
        /// Replace any of the contained controls with literals
        /// </summary>
        /// <param name="control"></param>
        private void PrepareGridViewForExport(Control gv)
        {



            System.Web.UI.WebControls.LinkButton lb = new System.Web.UI.WebControls.LinkButton();

            Literal l = new Literal();

            string name = String.Empty;

            for (int i = 0; i < gv.Controls.Count; i++)
            {


                if (gv.Controls[i].GetType() == typeof(System.Web.UI.WebControls.LinkButton))
                {

                    l.Text = (gv.Controls[i] as System.Web.UI.WebControls.LinkButton).Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                else if (gv.Controls[i].GetType() == typeof(TextBox))
                {

                    l.Text = (gv.Controls[i] as TextBox).Text.ToString();

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);


                }

                if (gv.Controls[i].HasControls())
                {

                    PrepareGridViewForExport(gv.Controls[i]);

                }

            }
        }

        protected void isAddOn_CheckedChanged(object sender, EventArgs e)
        {
            if (isAddOn.Checked)
            {
                ddlRetirementType.SelectedValue = "All";
                ddlRetirementType.Enabled = false;
            }
            else
            {
                ddlRetirementType.SelectedValue = "false";
                ddlRetirementType.Enabled = true;
            }
        }

    }

 

}
