<%@ Page Title="PF/CIT Opening" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="OpeningImportPage.aspx.cs" Inherits="Web.OpeningImportPage" %>

<%@ Import Namespace="BLL.Manager" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .tableLightColor input
        {
            width: 120px;
        }
    </style>
    <script src="../Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        function refreshWindow() {

            __doPostBack('Refresh', 0);


        }
        jQuery(document).ready(
    function () {

        setMovementToGrid('#<%= gvEmployeeIncome.ClientID %>');
    }
);

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
<div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Opening Provident Fund and CIT
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
    <div class="attribute" style="height:45px; padding-top:10px;">
        <table>
            <tr>
                <td>
                    Search &nbsp;
                    <asp:TextBox ID="txtEmployeeName" AutoPostBack="true" runat="server" Width="146px" Visible="false"
                        OnTextChanged="txtEmployeeName_TextChanged"></asp:TextBox>
                </td>
                <td>
                    <asp:LinkButton ID="btnExport" runat="server" Text="Excel import" OnClientClick="taxImportPopup();return false;"
                        CssClass=" excel marginRight tiptip" Style="float: left;" />
                </td>
                <td>
                    <asp:Button ID="Button1" CssClass="update"  AutoPostBack="false" runat="server" Text="Search" Visible="false" OnClick="btnSearch_Click" />
                </td>
            </tr>
        </table>
    </div>
    <div class="clear">
        <cc1:TextBoxWatermarkExtender ID="TBWE2" runat="server" TargetControlID="txtEmployeeName"
            WatermarkText="Enter employee name" WatermarkCssClass="watermarked" />
        <cc2:EmptyDisplayGridView CssClass="tableLightColor" UseAccessibleHeader="true" ShowHeaderWhenEmpty="True"
            ID="gvEmployeeIncome" runat="server" DataKeyNames="EmployeeId" AutoGenerateColumns="False"
            CellPadding="4" GridLines="None" AllowSorting="True" ShowFooterWhenEmpty="False">
            <RowStyle BackColor="#E3EAEB" />
            <Columns>
                <asp:BoundField HeaderStyle-Width="20px" DataFormatString="{0:000}" DataField="EmployeeId"
                    HeaderText="EIN"></asp:BoundField>
                <asp:TemplateField HeaderText="Name" HeaderStyle-Width="150px">
                    <ItemTemplate>
                        <asp:Label Text='<%# Eval("Name") %>' Style="width: 100px!important" runat="server" />
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left" Width="150px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Opening PF"
                    HeaderStyle-Width="80px">
                    <ItemTemplate>
                        <asp:TextBox CssClass='calculationInput' data-col='0' data-row='<%# Container.DataItemIndex %>' Style='text-align: right; background:#E0E0E0;'
                            ID="txtAmount" Text='<%# GetCurrency( Eval("OpeningPF")) %>' runat="server" ReadOnly="true"></asp:TextBox>
                        <asp:CompareValidator SetFocusOnError="true" ValueToCompare="0" Display="None" Operator="GreaterThanEqual"
                            Type="Currency" ID="valOpeningBalance" ControlToValidate="txtAmount" ValidationGroup="Balance"
                            runat="server" ErrorMessage="Invalid amount."></asp:CompareValidator>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Opening CIT" HeaderStyle-Width="80px">
                    <ItemTemplate>
                        <asp:TextBox CssClass='calculationInput' data-col='1' data-row='<%# Container.DataItemIndex %>' Style='text-align: right; background:#E0E0E0;'
                            ID="txtPF" Text='<%# GetCurrency( Eval("OpeningCIT")) %>' runat="server" ReadOnly="true"></asp:TextBox>
                        <asp:CompareValidator SetFocusOnError="true" ValueToCompare="0" Display="None" Operator="GreaterThanEqual"
                            Type="Currency" ID="valOpeningBalance1" ControlToValidate="txtPF" ValidationGroup="Balance"
                            runat="server" ErrorMessage="Invalid amount."></asp:CompareValidator>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Opening PF Interest" HeaderStyle-Width="80px">
                    <ItemTemplate>
                        <asp:TextBox CssClass='calculationInput' data-col='2' data-row='<%# Container.DataItemIndex %>' Style='text-align: right; background:#E0E0E0;'
                            ID="txtCIT" Text='<%# GetCurrency( Eval("OpeningPFInterest")) %>' runat="server" ReadOnly="true"></asp:TextBox>
                        <asp:CompareValidator SetFocusOnError="true" ValueToCompare="0" Display="None" Operator="GreaterThanEqual"
                            Type="Currency" ID="valOpeningBalance289" ControlToValidate="txtCIT" ValidationGroup="Balance"
                            runat="server" ErrorMessage="Invalid amount."></asp:CompareValidator>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Opening CIT Interest" HeaderStyle-Width="80px">
                    <ItemTemplate>
                        <asp:TextBox  CssClass='calculationInput' data-col='3' data-row='<%# Container.DataItemIndex %>' Style='text-align: right; background:#E0E0E0;'
                            ID="txtInsurance" Text='<%# GetCurrency( Eval("OpeningCITInterest")) %>' runat="server" ReadOnly="true"></asp:TextBox>
                        <asp:CompareValidator SetFocusOnError="true" ValueToCompare="0" Display="None" Operator="GreaterThanEqual"
                            Type="Currency" ID="valOpeningBalance23" ControlToValidate="txtInsurance" ValidationGroup="Balance"
                            runat="server" ErrorMessage="Invalid amount."></asp:CompareValidator>
                    </ItemTemplate>
                </asp:TemplateField>
                
            </Columns>
            <RowStyle CssClass="odd" />
            <AlternatingRowStyle CssClass="even" />
            <EmptyDataTemplate>
                <b> PF/CIT Opening </b>
            </EmptyDataTemplate>
        </cc2:EmptyDisplayGridView>
        <uc1:PagingCtl ID="pagingCtl" OnNextRecord="btnNext_Click" OnPrevRecord="btnPrevious_Click"
            OnDropDownShowRecordsChanged="ddlRecords_SelectedIndexChanged" runat="server" />
    </div>
    <div class="buttonsDiv">
        <%--    <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Balance" DisplayMode="List"
            ShowSummary="false" ShowMessageBox="true" runat="server"></asp:ValidationSummary>--%>
        <asp:Button ID="btnUpdate" CssClass="update" OnClientClick="valGroup='Balance';return CheckValidation();"
            Visible="true" ValidationGroup="Balance" runat="server" Text="Update" OnClick="btnUpdate_Click" />
    </div>
</div>
</asp:Content>
