﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Manager;
using BLL;
using DAL;

using Utils.Helper;
using BLL.Base;
using Utils.Calendar;
using System.Text;

namespace Web.CP
{
    public partial class AdjustRetrospectIncrement : BasePage
    {
        class LinkBO
        {
            public string Text { get; set; }
            public string Url { get; set; }
            public int IncomeId { get; set; }
        }
        public static int companyId = -1;
        #region "Control state related"
        private string _sortBy = "EmployeeId";
        private SortDirection _sortDirection = SortDirection.Ascending;
        private int periodId = -1;

        protected override void OnInit(EventArgs e)
        {
            Page.RegisterRequiresControlState(this);
            base.OnInit(e);
        }
        protected override void LoadControlState(object savedState)
        {
            object[] rgState = (object[])savedState;
            base.LoadControlState(rgState[0]);
            _sortBy = rgState[1].ToString();
            _sortDirection = (SortDirection)rgState[2];
            //_tempCurrentPage = (int)rgState[2];

        }
        protected override object SaveControlState()
        {
            object[] rgState = new object[3];
            rgState[0] = base.SaveControlState();
            rgState[1] = _sortBy;
            rgState[2] = _sortDirection;
            // rgState[2] = _tempCurrentPage;
            return rgState;
        }
        protected void gvwEmployees_Sorting(object sender, GridViewSortEventArgs e)
        {

            if (e.SortExpression != _sortBy)
            {
                e.SortDirection = SortDirection.Ascending;
                _sortDirection = e.SortDirection;
            }
            else if (_sortDirection == SortDirection.Ascending)
            {
                e.SortDirection = SortDirection.Descending;
            }
            else if (_sortDirection == SortDirection.Descending)
            {
                e.SortDirection = SortDirection.Ascending;
            }

            _sortDirection = e.SortDirection;

            _sortBy = e.SortExpression;


            e.Cancel = true;


            pagintCtl.CurrentPage = 1;
            //_tempCurrentPage = 1;
            BindEmployee();
        }
        #endregion

        public int IncomeId { get; set; }
        public int Type { get; set; }

        List<KeyValue> statusList = new JobStatus().GetMembers();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Request.QueryString["Type"]))
                this.Type = 1;
            else
                this.Type = int.Parse(Request.QueryString["Type"]);
            string incId = Request.QueryString["Id"];
            if (!string.IsNullOrEmpty(incId))
                this.IncomeId = int.Parse(incId);


            string PeriodId = Request.QueryString["PeriodId"];
            if (!string.IsNullOrEmpty(PeriodId))
                this.periodId = int.Parse(PeriodId);

            if (!IsPostBack)
            {
                List<LinkBO> linkList = new List<LinkBO>();
               


                pagintCtl.Visible = false;
              
                //PayrollPeriod payrollPeriod = CommonManager.GetValidLastPayrollPeriod();

                // then hpl allowance
                if (this.Type < 0)
                {
                    //lblIncome.Text = HPLAllowanceManager.GetAllowanceName((CalculationColumnType)this.Type);
                    gvMassIncrement.Columns[11].Visible = false;
                    btnGenerate.Visible = false;

                    gvMassIncrement.Columns[5].HeaderText = "Increase in Basic salary";
                    gvMassIncrement.Columns[6].HeaderText = "Unit";
                    gvMassIncrement.Columns[6].Visible = false;
                    gvMassIncrement.Columns[7].Visible = false;
                    gvMassIncrement.Columns[8].Visible = false;
                    gvMassIncrement.Columns[9].Visible = false;
                }
                else
                {

                    PIncome income = new PayManager().GetIncomeById(this.IncomeId);

                    if (income.IsPFCalculated == false)
                    {
                        gvMassIncrement.Columns[11].Visible = false;
                    }


                    //incomeId = -1;
                    //statusId = -1;
                    //gradeId = -1;
                    //designationId = -1;
                    companyId = SessionManager.CurrentCompanyId;

                    lblIncome.Text = income.Title;
                    lblIncomeDependent.Text += income.Title;
                    if (income.Calculation == IncomeCalculation.PERCENT_OF_INCOME
                        || (income.Calculation == IncomeCalculation.DEEMED_INCOME && Convert.ToBoolean( income.DeemedIsAmount)==false))
                    {

                        // create link
                        //create link for Fixed income with
                        linkList.Add
                            (
                                new LinkBO
                                {
                                    Text = income.Title + " Increment",
                                    Url = "MassIncrement.aspx?Id=" + PayManager.GetPercentOfIncomeFirstOtherIncomeId(income.IncomeId),
                                    IncomeId = income.IncomeId
                                }
                            );
                        linkList.Add
                           (
                               new LinkBO
                               {
                                   Text = income.Title + " Arrear Details",
                                   Url = "AdjustRetrospectIncrement.aspx?Id=" + income.IncomeId,
                                   IncomeId = income.IncomeId
                               }
                           );

                        lblIncome.Text += " % of ";
                        string value = "";

                        gvMassIncrement.Columns[5].HeaderText = "Increase in ";
                        gvMassIncrement.Columns[6].HeaderText = "% of ";

                        foreach (PIncomeIncome inc in income.PIncomeIncomes)
                        {
                            PIncome otherIncome = new PayManager().GetIncomeById(inc.OtherIncomeId);
                            if (value == "")
                                value = otherIncome.Title;
                            else
                                value += " ," + otherIncome.Title;


                            gvMassIncrement.Columns[5].HeaderText += otherIncome.Title + ", ";
                            gvMassIncrement.Columns[6].HeaderText += otherIncome.Title + ", ";

                            gvMassIncrement.Columns[7].Visible = false;
                            gvMassIncrement.Columns[8].Visible = false;
                            gvMassIncrement.Columns[9].Visible = false;
                        }
                        lblIncome.Text += value;

                        //if (PayManager.IfPercentOfIncomeAlreadyRetrospectIncrement(this.IncomeId, payrollPeriod.PayrollPeriodId))
                          //  btnGenerate.Visible = false;
                        // if already retrospecct salary exists then
                    }
                    else
                    {
                        lblIncomeDependent.Visible = true;
                        //create link for Fixed income with
                        linkList.Add
                            (
                                new LinkBO
                                {
                                    Text = income.Title + " Increment",
                                    Url = "MassIncrement.aspx?Id=" + income.IncomeId
                                }
                            );

                        //btnGenerate.Visible = false;
                    }
                    links.DataSource = linkList;
                    links.DataBind();
                    LoadIncomeAndDeduction();
                }

                BindEmployee();

               
            }

            string eventTarget = Request.Form["__EVENTTARGET"];
            if (eventTarget != null && eventTarget.Equals("Reload"))
            {
                BindEmployee();
                ReloadFromSession();
            }

            JavascriptHelper.AttachNonDialogPopUpCode(Page, "importPopup", "../ExcelWindow/IncrementedSalary.aspx", 450, 500);
            JavascriptHelper.AttachNonDialogPopUpCode(Page, "importPopupAdArrInc", "../ExcelWindow/AdjustArrearIncrementExcel.aspx", 450, 500);

            // generate links


        }

        public bool IsDeleteVisible(object incomeId,object type)
        {
            return PayManager.IsRetrospectGeneratedExists((int)incomeId, (int)type);
        }

        /// <summary>
        /// Load percent of Income/Deduction relying on this Income
        /// </summary>
        public void LoadIncomeAndDeduction()
        {
            PIncome inc = new PayManager().GetIncomeById(this.IncomeId);
            if (inc != null)
            {
                if (inc.IsBasicIncome)
                {
                    List<PIncome> otherIncomes = PayManager.GetOtherIncomes(inc.IncomeId);
                    foreach (PIncome income in otherIncomes)
                        income.Type = 1;

                    rptOtherIncomes.DataSource = otherIncomes;
                    rptOtherIncomes.DataBind();

                    //if (CommonManager.CalculationConstant.CompanyIsHPL.Value)
                    //{
                    //    hplAllowancePanel.Visible = true;
                    //    rptAllowances.DataSource = HPLAllowanceManager.GetHPLAllowanceList();
                    //    rptAllowances.DataBind();
                    //}
                    
                    


                }
            }
        }

        public void btnGenerateAllAllowance_Click(object sender, EventArgs e)
        {
            // generate for all HPL allowance depedent on Basic 
            HPLAllowanceManager.GenerateHPLRetrospectAllowance();

            LoadIncomeAndDeduction();

            divMsgCtl.InnerHtml = "Arrear adjustment allowance generated.";
            divMsgCtl.Hide = false;
        }

        public string GetCurrentIncomeTitle()
        {
            PIncome inc = new PayManager().GetIncomeById(this.IncomeId);
            if (inc != null)
            {
                return inc.Title;
            }
            return "";
        }

        //public double GetIncomePercent(int parentIncomeId, int childIncomeId)
        //{

        //}

        public string GetStatus(object value)
        {
            foreach (KeyValue val in new JobStatus().GetMembers())
            {
                if (val.Key == value.ToString())
                    return val.Value;
            }




            return "";
        }

        public void ReloadFromSession()
        {
            if (Session["IncrementList"] != null)
            {
                List<GetEmployeeListForMassIncrementResult> list =
               Session["IncrementList"] as List<GetEmployeeListForMassIncrementResult>;
                if (list != null)
                {
                    int currentRowEmpId = 0;
                    decimal existingAmount = 0;
                    decimal newIncome = 0;
                    decimal incrementAmount = 0;
                    foreach (GridViewRow row in gvMassIncrement.Rows)
                    {
                        currentRowEmpId = (int)gvMassIncrement.DataKeys[row.RowIndex][0];
                        if (currentRowEmpId != 0)
                        {
                            GetEmployeeListForMassIncrementResult increment = 
                                list.SingleOrDefault(x => x.EmployeeId == currentRowEmpId);

                            if (increment != null)
                            {
                                TextBox txtNewIncome = (TextBox)row.FindControl("txtNewIncome");
                                TextBox txtExistingIncome = row.FindControl("txtExistingIncome") as TextBox;
                                TextBox txtIncreasedBy = (TextBox)row.FindControl("txtIncreasedBy");

                                decimal.TryParse(txtExistingIncome.Text.Trim(), out existingAmount);


                                txtIncreasedBy.Text = GetCurrency(increment.IncrementIncome);
                                if (Convert.ToDecimal(increment.IncrementIncome)==0)
                                    txtNewIncome.Text = "0.00";
                                else
                                    txtNewIncome.Text = GetCurrency(existingAmount + Convert.ToDecimal(increment.IncrementIncome));

                            }
                        }
                    }

                    divWarningMsg.InnerHtml = "Besure to save the increment using Save button.";
                    divWarningMsg.Hide = false;

                }
            }
        }

        public void btnExportPopup_Click(object sender, EventArgs e)
        {
            //int? _tempCount = 0;
            //PayrollPeriod validPayrollPeriod = CommonManager.GetValidLastPayrollPeriod();
            //int payrollPeriodId = validPayrollPeriod == null ? 0 : validPayrollPeriod.PayrollPeriodId;
            //decimal filterAmount = 0;
            //decimal.TryParse(txtFilter.Text.Trim(), out filterAmount);

            //List<GetEmployeeListForMassIncrementResult> list = BLL.Manager.PayManager.GetEmployeeListForMassIncrementResult(int.Parse(ddlIncome.SelectedValue),
            //    int.Parse(ddlStatus.SelectedValue), int.Parse(ddlGrade.SelectedValue), int.Parse(ddlDesignation.SelectedValue),
            //    SessionManager.CurrentCompanyId, payrollPeriodId, pagintCtl.CurrentPage,
            //    int.Parse(pagintCtl.DDLRecords.SelectedValue), ref _tempCount, (_sortBy + " " + _sortDirection).ToLower()
            //    , int.Parse(ddlDepartments.SelectedValue), int.Parse(ddlFilter.SelectedValue), filterAmount, int.Parse(ddlFilterIncomes.SelectedValue),txtEmpSearchText.Text.Trim());

            //string template = ("~/App_Data/ExcelTemplate/SalaryIncrementImport.xlsx");



            //int totalRecords = 0;

         
            //{
            //    ExcelGenerator.ExportSalaryIncrement(template, list, ddlIncome.SelectedItem.Text);
            //}
        }

        protected void btnPrevious_Click()
        {
            pagintCtl.CurrentPage -= 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage -= 1;
            BindEmployee();
        }

        protected void btnNext_Click()
        {
            pagintCtl.CurrentPage += 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage += 1;
            BindEmployee();
        }


   


        public void RegisterJsForPopUp()
        {
            JavascriptHelper.AttachPopUpCode(Page, "popupMassIncrement", "UniformIncrease/UniformIncrease.aspx", 500, 260);
        }


        public void txtEmpSearch_TextChanged(object sender, EventArgs e)
        {
            BindEmployee();
        }
       
          protected void BindEmployee()
          {

              //if (!incomeId.Equals(-1) && !statusId.Equals(-1) && !gradeId.Equals(-1) && !designationId.Equals(-1) & !companyId.Equals(-1))

              int empId = -1;

              if (!string.IsNullOrEmpty(hiddenEmployeeID.Value) && !string.IsNullOrEmpty(txtEmpSearch.Text.Trim()))
                  empId = int.Parse(hiddenEmployeeID.Value);

              int? _tempCount = 0;
              PayrollPeriod validPayrollPeriod = CommonManager.GetValidLastPayrollPeriod();
              int payrollPeriodId = validPayrollPeriod == null ? 0 : validPayrollPeriod.PayrollPeriodId;
              decimal filterAmount = 0;

            if (this.periodId != -1)
                payrollPeriodId = periodId;


              List<GetRetrospectIncrementResult> list = BLL.Manager.PayManager.GetRetrospectIncrementList
                  (this.IncomeId,this.Type, pagintCtl.CurrentPage,
                int.Parse(pagintCtl.DDLRecords.SelectedValue), payrollPeriodId, ref _tempCount,empId);

              gvMassIncrement.DataSource = list;
              gvMassIncrement.DataBind();

              pagintCtl.UpdatePagingBar(_tempCount.Value);


              if (_tempCount <= 0)
              {
                  pagintCtl.Visible = false;
              }
              else
              {
                  pagintCtl.Visible = true;
              }
             
          }

 
    
        protected void ddlRecords_SelectedIndexChanged()
        {
            //_tempCurrentPage = 1;
            pagintCtl.CurrentPage = 1;
            BindEmployee();
        }


        protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            pagintCtl.CurrentPage = 1;
            BindEmployee();
        }

        protected void ddlGrade_SelectedIndexChanged(object sender, EventArgs e)
        {
            pagintCtl.CurrentPage = 1;
            BindEmployee();
        }

        protected void ddlDesignation_SelectedIndexChanged(object sender, EventArgs e)
        {
            pagintCtl.CurrentPage = 1;
            BindEmployee();
        }

        protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            pagintCtl.CurrentPage = 1;
            BindEmployee();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            Page.Validate("Balance");

            if(periodId != -1 && periodId != CommonManager.GetLastPayrollPeriod().PayrollPeriodId)
            {
                divWarningMsg.InnerHtml = "Change can be done only for last period.";
            }

            decimal amount, pfAmount;
            //string notes;
            int employeeId, payrollPeriodId, incomeId;
           // int type;
            bool isSavedReqd = false;
            if (Page.IsValid)
            {

                StringBuilder str = new StringBuilder("<root>");


                foreach (GridViewRow row in gvMassIncrement.Rows)
                {


                    employeeId = (int)gvMassIncrement.DataKeys[row.RowIndex]["EmployeeId"];
                    payrollPeriodId = (int)gvMassIncrement.DataKeys[row.RowIndex]["PayrollPeriodId"];
                    incomeId = (int)gvMassIncrement.DataKeys[row.RowIndex]["IncomeId"];


                    TextBox txtTotalIncreased = row.FindControl("txtTotalIncreased") as TextBox;
                    TextBox txtTotalPFIncreased = row.FindControl("txtTotalPFIncreased") as TextBox;
                   


                    //string invalidText = Util.HasInvalidCharacterForXMLParsingAndImport(txtNote.Text.Trim());

                    //if (!string.IsNullOrEmpty(invalidText))
                    //{
                    //    divWarningMsg.InnerHtml = "\"" + invalidText + "\" character can not be set in the note.";
                    //    divWarningMsg.Hide = false;
                    //    return;
                    //}

                    if (txtTotalIncreased.Text.Trim() == "")
                        amount = 0;
                    else
                        decimal.TryParse(txtTotalIncreased.Text.Trim(), out amount);

                    if (txtTotalPFIncreased.Text.Trim() == "")
                        pfAmount = 0;
                    else
                        decimal.TryParse(txtTotalPFIncreased.Text.Trim(), out pfAmount);

                

                    //if ( (adjustmentAmount != 0 || payAmount != 0))
                    {
                        isSavedReqd = true;

                        str.Append(
                            string.Format("<row PayrollPeriodId=\"{0}\" EmployeeId=\"{1}\" IncomeId=\"{2}\" Amount=\"{3}\"  PFAmount=\"{4}\" Type=\"{5}\" /> ",
                            //overtimeId, payrollPeriodId, employeeId, overtimeHours, payPerHour, txtNote.Text.Trim()
                                payrollPeriodId, employeeId, incomeId, amount,pfAmount,this.Type
                            ));
                    }
                }
                str.Append("</root>");

                if (isSavedReqd)
                {
                    PayManager.ReSaveRetrospectIncrement(str.ToString());
                    divMsgCtl.InnerHtml = "Saved";
                    divMsgCtl.Hide = false;
                    BindEmployee();
                }

            }

        }

        public void btnLoad_Click(object sender, EventArgs e)
        {
            BindEmployee();
        }

        protected void btnGenerate_Click(object sender, EventArgs e)
        {
            if (periodId != -1 && periodId != CommonManager.GetLastPayrollPeriod().PayrollPeriodId)
            {
                divWarningMsg.InnerHtml = "Change can be done only for last period.";
            }

            PayrollPeriod payrollPeriod = CommonManager.GetValidLastPayrollPeriod();

            if (payrollPeriod != null)
            {
                //CCalculation calc = CalculationManager.IsCalculationSaved(payrollPeriod.PayrollPeriodId);
                //if (calc != null)
                //{
                //    divWarningMsg.InnerHtml = "Salary already saved for this period " + payrollPeriod.Name + ",so arrear adjustement can not be generated.";
                //    divWarningMsg.Hide = false;
                //    return;
                //}

                PayManager.GenerateRetrospectForPercentOfIncome(
                    this.IncomeId, payrollPeriod.PayrollPeriodId);

                //btnGenerate.Visible = false;

                divMsgCtl.InnerHtml = "Salary has been generated.";
                divMsgCtl.Hide = false;

                BindEmployee();
            }
        }

        protected void rptOtherIncomes_DeleteCommand(object source, DataListCommandEventArgs e)
        {
            int incomeId = int.Parse(e.CommandArgument.ToString());

            int type = (incomeId < 0) ? incomeId : 1;
            bool status = PayManager.DeleteRetrospectGenerated(incomeId, type);

            if (status)
            {
                LoadIncomeAndDeduction();
                string name = "";
                if (type < 0)
                {
                    //name = HPLAllowanceManager.GetAllowanceName((CalculationColumnType)type);
                }
                else
                {
                    name = new PayManager().GetIncomeById(incomeId).Title;
                }

                divMsgCtl.InnerHtml = "Arrear adjustment deleted for \"" + name  + " \".";
                divMsgCtl.Hide = false;
            }
            else
            {
                divWarningMsg.InnerHtml = "Payroll period saved adjustment can not be deleted.";
                divWarningMsg.Hide = false;
            }

        }

        protected void rptOtherIncomes_SelectedIndexChanged(object sender, EventArgs e)
        {

        }


        //public bool IsIncrementMode(object isIncrementMode, object incrementId)
        //{
        //    bool mode = (bool)isIncrementMode;

        //    //if (incrementId == null || (int)(incrementId) == 0)
        //    //    return false;

        //    return mode;
        //}

       

      
    }
}
