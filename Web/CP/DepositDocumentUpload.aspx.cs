﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL;
using BLL.Manager;
using Utils.Helper;
using System.IO;
using Utils;
using DAL;
using System.Drawing;
using Utils.Security;

namespace Web.CP
{
    public partial class DepositDocumentUpload : System.Web.UI.Page
    {
        int DocumentDepositId = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            DocumentDepositId = int.Parse(Request.QueryString["Id"]);

            if (!IsPostBack)
                Initialise();

            
        }

        void Initialise()
        {
           
           
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            if (fup.HasFile)
            {
                string ext = Path.GetExtension(fup.FileName);
                string guidFileName = Guid.NewGuid().ToString() + ext;
                string saveLocation = Path.Combine(Server.MapPath(Config.UploadLocation), guidFileName);


                
                string code = "closeWindow(\"{0}\");";
             
                {
                    try
                    {
                        //

                        guidFileName =  guidFileName.Replace(Path.GetExtension(guidFileName), ".ashx");
                        saveLocation = saveLocation.Replace(Path.GetExtension(saveLocation), ".ashx");

                  

                        EncryptorDecryptor enc = new EncryptorDecryptor();
                        enc.Encrypt(fup.FileContent, saveLocation);

                        DepositDocumentAttachment doc = new DepositDocumentAttachment();
                        doc.DepositDocumentId = DocumentDepositId;
                        doc.Name = Path.GetFileName(fup.FileName);

                        if (!string.IsNullOrEmpty(txtDesc.Text.Trim()))
                        {
                            doc.Description = txtDesc.Text.Trim();
                        }
                        else
                            doc.Description = Path.GetFileNameWithoutExtension(fup.FileName);

                        doc.ContentType = fup.PostedFile.ContentType;
                        doc.Url = guidFileName;
                       

                       
                        HRManager.SaveDocument(doc);


                        code = string.Format(code, HRManager.GetHTMLDepositDocuments(DocumentDepositId));
                        JavascriptHelper.DisplayClientMsg("Document added.", Page,code);
                    }
                    catch (Exception exp1)
                    {
                        JavascriptHelper.DisplayClientMsg(exp1.Message, Page);
                    }
                }
            }
        }

        /// <summary>
        /// Checks if the file has the valid image extension or not
        /// </summary>
        /// <param name="pExtension">Extension type</param>
        /// <returns>Is valid image or not</returns>
        public static bool IsImage(string pExtension)
        {
            if ((pExtension.ToLower() == ".jpg" | pExtension.ToLower() == ".gif" | pExtension.ToLower() == ".bmp" | pExtension.ToLower() == ".jpeg" | pExtension.ToLower() == ".png"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// Helper method to convert Bytes in KiloBytes
        /// </summary>
        /// <param name="byteValue"></param>
        /// <returns></returns>
        public static int ConvertBytesIntoKB(int byteValue)
        {
            double mb = (byteValue / 1024);
            if (mb.ToString("#") == "")
                return 0;
            return Convert.ToInt32(mb.ToString("#"));
        }
        public static string ConvertKBIntoMB(object value)
        {
            long kb = Convert.ToInt64(value);
            double mb = (kb / 1024.0);
            return mb.ToString("#.#");
        }

        public string GetSize(int byteValue)
        {
            if (byteValue < 1024)
                return "1 kb";

            double kb = ConvertBytesIntoKB(byteValue);
            if (kb > 1024)
            {
                return ConvertKBIntoMB(kb) + " mb";
            }
            else
            {
                return kb + " kb";
            }
        }
    }
}
