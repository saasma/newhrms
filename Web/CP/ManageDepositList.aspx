<%@ Page Title="Manage Deposit List" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="ManageDepositList.aspx.cs" Inherits="Web.User.ManageDepositList" %>

<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="Paging" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningMsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="InfoMsgCtl" TagPrefix="uc2" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        function changePasswordCall(userName) {
            var ret = changePassword("isPopup=true&u=" + userName);
            if (typeof (ret) != 'undefined') {
            }
        }
        function refreshWindow() {
            window.location.reload();
        }
     
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Deposit Document List
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="contentArea">
            <div class="attribute" style="padding:10px">
                Year List &nbsp;
                <asp:DropDownList AutoPostBack="true" Width="150" ID="ddlYears" runat="server" DataValueField="FinancialDateId"
                    DataTextField="Name" OnSelectedIndexChanged="ddlYears_Changed" AppendDataBoundItems="true">
                    <asp:ListItem Value="-1">--Select Year--</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ValidationGroup="Inc" ID="RequiredFieldValidator8" Display="None"
                    runat="server" InitialValue="-1" ControlToValidate="ddlYears" ErrorMessage="Year is required."></asp:RequiredFieldValidator>
                &nbsp;
                <asp:LinkButton ID="add" runat="server" OnClientClick='window.location="DepositDocument.aspx";return false;' 
                    Text="Add Deposit Document" />
            </div>
            <cc2:EmptyDisplayGridView Width="100%" Style='clear: both' PagerStyle-HorizontalAlign="Center"
                PagerStyle-CssClass="defaultPagingBar" CssClass="table table-primary mb30 table-bordered table-hover" UseAccessibleHeader="true"
                ShowHeaderWhenEmpty="True" ID="gvwRoles" runat="server" DataKeyNames="DepositDocumentId"
                AutoGenerateColumns="False" CellPadding="4" GridLines="None" ShowFooterWhenEmpty="False"
                OnSelectedIndexChanged="gvwRoles_SelectedIndexChanged" OnRowDeleting="gvwRoles_RowDeleting">
                <Columns>
                    <asp:TemplateField HeaderText="Period" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="60px">
                        <ItemTemplate>
                            <%# GetPeriodName( (int) Eval("PayrollPeriodId"))%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Description" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="150px">
                        <ItemTemplate>
                            <%# GetTypeName(Eval("Type").ToString())%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Bank" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="120px">
                        <ItemTemplate>
                            <%# Eval("Bank")%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Voucher No" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="80px">
                        <ItemTemplate>
                            <%# (Eval("VoucherNo"))%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"
                        HeaderText="Amount" HeaderStyle-Width="100px">
                        <ItemTemplate>
                            <%# (Eval("Amount","{0:N2}"))%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Date Deposited"
                        HeaderStyle-Width="80px">
                        <ItemTemplate>
                            <%# Eval("DateDeposited")%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Deposited By" HeaderStyle-Width="120px">
                        <ItemTemplate>
                            &nbsp;
                            <%# Eval("DepositedBy")%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Submission No"
                        HeaderStyle-Width="120px">
                        <ItemTemplate>
                            <%# Eval("SubmissionNo")%></ItemTemplate>
                    </asp:TemplateField>
                    <%--  <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Attachments" HeaderStyle-Width="50px">
                    <ItemTemplate>
                       </ItemTemplate>
                </asp:TemplateField>--%>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderStyle-Width="50px">
                        <ItemTemplate>
                            <asp:ImageButton ID="ImageButton1" runat="server" CommandName="Select" ImageUrl="~/images/edit.gif" />
                            &nbsp;
                            <asp:ImageButton ID="ImageButton2" OnClientClick="return confirm('Do you really want to delete the document?')"
                                runat="server" CommandName="Delete" ImageUrl="~/images/delet.png" />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                    </asp:TemplateField>
                </Columns>
                <PagerSettings Mode="NumericFirstLast" NextPageText="Next �" PreviousPageText="� Prev"
                    FirstPageText="First" LastPageText="Last" />
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                <PagerStyle CssClass="defaultPagingBar" />
                <SelectedRowStyle CssClass="selected" />
                <EmptyDataTemplate>
                    <b>No List. </b>
                </EmptyDataTemplate>
            </cc2:EmptyDisplayGridView>
            <div style='margin-top: 10px'>
            </div>
            <%-- <div class="buttonsDiv">
        
     <asp:Button ID="btnRole"  CssClass="addbtns" runat="server" Text="Add" />
        
        </div>--%>
            <uc2:InfoMsgCtl ID="msgInfo" EnableViewState="false" Hide="true" runat="server" />
            <uc2:WarningMsgCtl ID="msgWarning" EnableViewState="false" Hide="true" runat="server" />
        </div>
    </div>
</asp:Content>
