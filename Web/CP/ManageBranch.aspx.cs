﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Manager;
using BLL;
using Utils.Helper;
using DAL;
using BLL.Base;

namespace Web.CP
{
    public partial class ManageBranch : BasePage
    {
        BranchManager branchMgr = new BranchManager();

        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.AdministratorTools;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialise();
            }
            else if (Request.Form["__EVENTTARGET"] != null &&  Request.Form["__EVENTTARGET"].Equals("Reload"))
            {
                Initialise();
            }
            JavascriptHelper.AttachPopUpCode(Page, "popupIncome", "AEBranch.aspx", 600, 600);
        }

        public void btnExport_Click(object sender, EventArgs e)
        {
            EmployeeManager mgr = new EmployeeManager();
            List<Branch> list = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            EEmployee emp = null;
            foreach (Branch item in list)
            {
                if (item.BranchHeadId != null)
                {
                    emp = mgr.GetById(item.BranchHeadId.Value);
                    item.Head = emp.Name + " - " + item.BranchHeadId;

                    if (emp.IsRetired != null && emp.IsRetired.Value && emp.EHumanResources[0].DateOfRetirementEng <= DateTime.Now.Date)
                    {
                        item.Head += " (Already retired)";
                    }
                    if (emp.IsResigned != null && emp.IsResigned.Value && emp.EHumanResources[0].DateOfResignationEng <= DateTime.Now.Date)
                    {
                        item.Head += " (Already retired)";
                    }
                }

                if (item.RegionalHeadId != null)
                {
                    emp = mgr.GetById(item.RegionalHeadId.Value);
                    item.RegionHead = emp.Name + " - " + item.RegionalHeadId;

                    if (emp.IsRetired != null && emp.IsRetired.Value && emp.EHumanResources[0].DateOfRetirementEng <= DateTime.Now.Date)
                    {
                        item.RegionHead += " (Already retired)";
                    }
                    if (emp.IsResigned != null && emp.IsResigned.Value && emp.EHumanResources[0].DateOfResignationEng <= DateTime.Now.Date)
                    {
                        item.RegionHead += " (Already retired)";
                    }
                }
            }


            List<string> hiddenList = new List<string>();
            hiddenList.Add("ActingID");
            hiddenList.Add("BranchDepartmentId");
            hiddenList.Add("LetterDateEng");
            hiddenList.Add("LetterDateEng");
            hiddenList.Add("ApplicableTillEng");
            hiddenList.Add("ApplicableFromEng");
            hiddenList.Add("CreatedBy");
            hiddenList.Add("CreatedOn");
            hiddenList.Add("ModifiedBy");
            hiddenList.Add("ModifiedOn");
            hiddenList.Add("FromLevelId");
            hiddenList.Add("ToLevelId");
            hiddenList.Add("DesignationId");
            hiddenList.Add("Amount");
            hiddenList.Add("ActingEmployee2");
            hiddenList.Add("ActingEmployee1");

            Dictionary<string, string> renameList = new Dictionary<string, string>();


            Bll.ExcelHelper.ExportToExcel("Branch List", list,
                hiddenList,
            new List<String>() { },
            renameList,
            new List<string>() { }
            , new Dictionary<string, string>() { { "Branch List", "" } }
            , new List<string> { "Name", "Code", "Address", "Phone", "Head", "RegionHead", "IsHeadOffice", "IsRegionalOffice", "IsInRemoteArea" });


        }
        protected void gvwEmployees_RowCreated(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover",
                "this.originalClass=this.className;this.className='selected'");

                e.Row.Attributes.Add("onmouseout",
                "this.className=this.originalClass;");


            }

        }

        protected void Initialise()
        {
            EmployeeManager mgr = new EmployeeManager();


            string branchSearch = txtBranchName.Text.Trim();
            int activeStatus = int.Parse(ddlStatus.SelectedValue);

            List<Branch> source = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            source = source.Where(x => ((branchSearch == "" || x.Name.ToLower().Contains(branchSearch.ToLower())) && (activeStatus == -1 || (activeStatus == 0 && (x.IsActive == null || x.IsActive == true)) || (activeStatus == 1 && x.IsActive == false)))).ToList();

            EEmployee emp = null;
            foreach (Branch item in source)
            {
                if (item.BranchHeadId != null)
                {
                    emp = mgr.GetById(item.BranchHeadId.Value);
                    if (emp != null)
                    {
                        item.Head = emp.Name + " - " + item.BranchHeadId;

                        if (emp.IsRetired != null && emp.IsRetired.Value && emp.EHumanResources[0].DateOfRetirementEng <= DateTime.Now.Date)
                        {
                            item.Head += " <span style='color:red'> (Already retired) </span>";
                        }
                        if (emp.IsResigned != null && emp.IsResigned.Value && emp.EHumanResources[0].DateOfResignationEng <= DateTime.Now.Date)
                        {
                            item.Head += " <span style='color:red'> (Already retired) </span>";
                        }
                    }
                }

                if (item.RegionalHeadId != null)
                {
                    emp = mgr.GetById(item.RegionalHeadId.Value);
                    item.RegionHead = emp.Name + " - " + item.RegionalHeadId;

                    if (emp.IsRetired != null && emp.IsRetired.Value && emp.EHumanResources[0].DateOfRetirementEng <= DateTime.Now.Date)
                    {
                        item.RegionHead += " <span style='color:red'> (Already retired) </span>";
                    }
                    if (emp.IsResigned != null && emp.IsResigned.Value && emp.EHumanResources[0].DateOfResignationEng <= DateTime.Now.Date)
                    {
                        item.RegionHead += " <span style='color:red'> (Already retired) </span>";
                    }
                }
            }
            gvwBranches.DataSource = source;
            gvwBranches.DataBind();

            //This will add the <thead> and <tbody> elements
            gvwBranches.HeaderRow.TableSection = TableRowSection.TableHeader;

        }

        protected void gvwIncomes_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            CommonManager.ResetCache();
            int id = (int)gvwBranches.DataKeys[e.RowIndex][0];
            Branch branch = branchMgr.GetById(id);
           
            string msg = "";
            if (branchMgr.Delete(branch,ref msg) == 1)
            {
               
                divMsgCtl.InnerHtml = Resources.Messages.BranchDeletedMsg;
                divMsgCtl.Hide = false;

                MyCache.Reset();
                // JavascriptHelper.DisplayClientMsg("Branch information is deleted.", Page);
            }
            else
            {
                divWarningMsg.InnerHtml = msg;
                divWarningMsg.Hide = false;
                //JavascriptHelper.DisplayClientMsg("Branch is in use.", Page);
            }
            Initialise();
        }

        protected void btnLoad_Click(object sender, EventArgs e)
        {
            Initialise();
        }
       
    }
}
