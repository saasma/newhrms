﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;
using Utils.Calendar;
using Utils.Helper;
using Bll;

namespace Web.NewHR
{
    public partial class OvertimeEachDayDetails : BasePage
    {
        int period = -1;

        protected void Page_Load(object sender, EventArgs e)
        {

            period = int.Parse(Request.QueryString["periodiId"]);

            if (!X.IsAjaxRequest)
            {

                Store3.Parameters[2].Value = period.ToString();

                OvertimePeriod ot = OvertimeManager.GetOverTimPeriodById(period);
                title.Html = string.Format("<h3> Overtime Approvals for {0} to {1}</h3>", ot.StartDate.ToString("dd MMM yyyy")
                    , ot.EndDate.ToString("dd MMM yyyy"));

            }
        }

        public void cmbOverTimePeriod_Select(object sender, DirectEventArgs e)
        {
            //int period = int.Parse(cmbOverTimePeriod.SelectedItem.Value);
            OvertimePeriod ot = OvertimeManager.GetOverTimPeriodById(period);
            title.Html = string.Format("<h3> Overtime Approvals for {0} to {1}</h3>",ot.StartDate.ToString("dd MMM yyyy")
                , ot.EndDate.ToString("dd MMM yyyy"));

            X.Js.AddScript("searchList();");

            PayrollPeriod currentPeriod = CommonManager.GetLastPayrollPeriod();
            if (ot.Status == (int)OvertimePeriodStatus.NotEditable 
                || ot.Status == (int)OvertimePeriodStatus.Distributed
                || (ot.PaidPayrollPeriodId != currentPeriod.PayrollPeriodId && ot.PaidPayrollPeriodId != -1))
            {
                btnApprove.Hide();
                btnRejectSelected.Hide();
                //btnRejectall.Hide();
                columnActions.Hide();
            }
            else
            {
                btnApprove.Show();
                btnRejectSelected.Show();
                //btnRejectall.Show();
                columnActions.Show();
            }
        }

        protected void btnRevertAll_Click(object sender, DirectEventArgs e)
        {
            if (SessionManager.User.UserMappedEmployeeId == null ||
               SessionManager.User.UserMappedEmployeeId == 0 ||
               SessionManager.User.UserMappedEmployeeId == -1)
            {
                NewMessage.ShowWarningMessage("User should be mapped to Employee for OT processing.");
                return;
            }

            string gridItemsJson = e.ExtraParams["gridItems"];
            List<GetOvertimeForApprovalResult> list = JSON.Deserialize<List<GetOvertimeForApprovalResult>>(gridItemsJson);
            List<OvertimePeriodDetail> _OvertimePeriodDetail = new List<OvertimePeriodDetail>();
            if (list.Count == 0)
            {
                NewMessage.ShowNormalMessage("Select Overtime to approve.");
                return;
            }

            foreach (var item in list)
            {

                

                OvertimePeriodDetail obj = new OvertimePeriodDetail()
                {
                    EmployeeId = item.EmployeeID,
                    OTDate = item.OTDate

                };
                _OvertimePeriodDetail.Add(obj);
                if (list.Count == 0)
                    return;

               
            }

            Status status = OvertimeManager.RevertOvertime(_OvertimePeriodDetail);
            if (status.IsSuccess)
            {
                CheckboxSelectionModel1.ClearSelection();
                NewMessage.ShowNormalMessage("Overtime reverted successfully.");
                PagingToolbar1.DoRefresh();
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }


        protected void btnApprove_Click(object sender, DirectEventArgs e)
        {
            if (SessionManager.User.UserMappedEmployeeId == null ||
               SessionManager.User.UserMappedEmployeeId == 0 ||
               SessionManager.User.UserMappedEmployeeId == -1)
            {
                NewMessage.ShowWarningMessage("User should be mapped to Employee for OT processing.");
                return;
            }

            string gridItemsJson = e.ExtraParams["gridItems"];
            List<GetOvertimeForApprovalResult> list = JSON.Deserialize<List<GetOvertimeForApprovalResult>>(gridItemsJson);
            List<OvertimePeriodDetail> _OvertimePeriodDetail = new List<OvertimePeriodDetail>();
            if (list.Count == 0)
            {
                NewMessage.ShowNormalMessage("Select Overtime to approve.");
                return;
            }

            foreach (var item in list)
            {

                int ApprovedHrsFormatted = 0;
                int ApprovedMinsFormatted = 0;

                if (!string.IsNullOrEmpty(item.ApprovedHrsFormatted.Trim()))
                    ApprovedHrsFormatted = int.Parse(item.ApprovedHrsFormatted);

                if (!string.IsNullOrEmpty(item.ApprovedMinFormatted.Trim()))
                    ApprovedMinsFormatted = int.Parse(item.ApprovedMinFormatted);

                if (ApprovedHrsFormatted == 0 && ApprovedMinsFormatted == 0)
                {
                    NewMessage.ShowNormalMessage("Approved Hrs or Minute is required.");
                    return;
                }

                int ApprovedHrs = int.Parse(item.ApprovedMinFormatted) + (int.Parse(item.ApprovedHrsFormatted) * 60);

                if (ApprovedHrs > int.Parse(item.OTMinute.ToString()))
                {
                    NewMessage.ShowNormalMessage("Approved Hrs can not exceed OT hours for Employee :" + item.Name + ", OT Date : " + item.OTDate.ToString("MM/dd/yyyy"));
                    return;
                }
                OvertimePeriodDetail obj = new OvertimePeriodDetail()
                {
                    EmployeeId = item.EmployeeID,
                    OTDate = item.OTDate,
                    ApprovedMin = ApprovedHrs,
                };
                _OvertimePeriodDetail.Add(obj);
                if (list.Count == 0)
                    return;

                Status status = OvertimeManager.ApproveOvertimePeriodDetails(_OvertimePeriodDetail);
                if (status.IsSuccess)
                {
                    CheckboxSelectionModel1.ClearSelection();
                    NewMessage.ShowNormalMessage("Overtime approved successfully.");
                    PagingToolbar1.DoRefresh();
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }
            }
        }

          protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            int start = 0;
            int pagesize = 50;
            int EmployeeId = -1;

            if (cmbSearch.SelectedItem != null && cmbSearch.SelectedItem.Value != null)
                EmployeeId = int.Parse(cmbSearch.SelectedItem.Value);

            int OverTimePeriodID = period;

            int status = int.Parse(cmbStatus.SelectedItem.Value);
            DateTime? StartDate = null;
            DateTime? EndDate = null;

           
            OvertimePeriod _OverTimPeriod = OvertimeManager.GetOverTimPeriodById(OverTimePeriodID);
            if (_OverTimPeriod != null)
            {
                StartDate = _OverTimPeriod.StartDate;
                EndDate = _OverTimPeriod.EndDate;
            }

            DateTime? date = null;

            if (calDate.SelectedDate != calDate.EmptyDate)
                date = calDate.SelectedDate;

            List<GetOvertimeForApprovalResult> resultSet = OvertimeManager.GetOvertimelistForApproval(e.Page, e.Limit, EmployeeId, status,
               StartDate, EndDate, date);


            e.Total = 0;
            if (resultSet.Count > 0)
                e.Total = resultSet[0].TotalRows.Value;

            Store3.DataSource = resultSet;
            Store3.DataBind();

        }

          protected void btnExcelPrint_Click(object sender, EventArgs e)
          {
              int EmployeeID = -1;
              int Status = -1;
              int OverTimePeriodID = period;
              DateTime? StartDate = null;
              DateTime? EndDate = null;
              DateTime? date = null;

              if (!string.IsNullOrEmpty(cmbSearch.SelectedItem.Value))
                  EmployeeID = int.Parse(cmbSearch.SelectedItem.Value);

              if (!string.IsNullOrEmpty(cmbStatus.SelectedItem.Value))
                  Status = int.Parse(cmbStatus.SelectedItem.Value);


    


              OvertimePeriod _OverTimPeriod = OvertimeManager.GetOverTimPeriodById(OverTimePeriodID);
              if (_OverTimPeriod != null)
              {
                  StartDate = _OverTimPeriod.StartDate;
                  EndDate = _OverTimPeriod.EndDate;
              }

              if (calDate.SelectedDate != calDate.EmptyDate)
                  date = calDate.SelectedDate;

              List<GetOvertimeForApprovalResult> resultSet = OvertimeManager.GetOvertimelistForApproval(1, 999999, EmployeeID, Status, StartDate, EndDate, date);
              ExcelHelper.ExportToExcel("Overtime Approval List", resultSet,
              new List<String>() { "TotalRows", "RowNumber", "DayType", "OTMinute", "ApprovedMin", "ApprovedBy", "ApprovedOn", "Status" },
              new List<String>() { },
              new Dictionary<string, string>() { { "EmployeeID", "EIN" }, { "Name", "Employee Name" }, { "OTDate", "OT Date" }, { "DayValueText", "Day Type" }, { "ClockIn", "In Time" }
          , { "ClockOut", "Out Time" }, { "OTHours", "OT Hours" }, { "StatusText", "Status" }, { "ApprovedHrsFormatted", "Approved Hrs" }, { "ApprovedMinFormatted", "Approved Min" }, { "AprovedBy", "Approved By" }},
              new List<string>() { }
              , new Dictionary<string, string>() { }
              , new List<string> { "EmployeeID", "Name", "OTDate", "DayValueText", "ClockIn", "ClockOut", "OTHours", "StatusText","AprovedBy", "ApprovedHrsFormatted", "ApprovedMinFormatted" });
          }
        protected void btnRejectSelected_Click(object sender, DirectEventArgs e)
        {
            if (SessionManager.User.UserMappedEmployeeId == null ||
               SessionManager.User.UserMappedEmployeeId == 0 ||
               SessionManager.User.UserMappedEmployeeId == -1)
            {
                NewMessage.ShowWarningMessage("User should be mapped to Employee for OT processing.");
                return;
            }

            string gridItemsJson = e.ExtraParams["gridItems"];
            List<GetOvertimeForApprovalResult> list = JSON.Deserialize<List<GetOvertimeForApprovalResult>>(gridItemsJson);
            List<OvertimePeriodDetail> _OvertimePeriodDetail = new List<OvertimePeriodDetail>();
            if (list.Count == 0)
            {
                NewMessage.ShowNormalMessage("Select Overtime to Reject.");
                return;
            }

            foreach (var item in list)
            {

               
                OvertimePeriodDetail obj = new OvertimePeriodDetail()
                {
                    EmployeeId = item.EmployeeID,
                    OTDate = item.OTDate,
                };
                _OvertimePeriodDetail.Add(obj);
                if (list.Count == 0)
                    return;

                Status status = OvertimeManager.RejectSelectedOvertimePeriodDetails(_OvertimePeriodDetail);
                if (status.IsSuccess)
                {
                    CheckboxSelectionModel1.ClearSelection();
                    NewMessage.ShowNormalMessage("Overtime Rejected successfully.");
                    PagingToolbar1.DoRefresh();
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }
            }
        }


        protected void btnRejectAll_Click(object sender, DirectEventArgs e)
        {
            if (SessionManager.User.UserMappedEmployeeId == null ||
               SessionManager.User.UserMappedEmployeeId == 0 ||
               SessionManager.User.UserMappedEmployeeId == -1)
            {
                NewMessage.ShowWarningMessage("User should be mapped to Employee for OT processing.");
                return;
            }

            Status status = OvertimeManager.RejectAllSavedOvertimePeriodDetails();
            if (status.IsSuccess)
            {
                CheckboxSelectionModel1.ClearSelection();
                NewMessage.ShowNormalMessage("Overtime Rejected successfully.");
                PagingToolbar1.DoRefresh();
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        protected void btnApproveByLine_Click(object sender, DirectEventArgs e)
        {
            if (SessionManager.User.UserMappedEmployeeId == null ||
                  SessionManager.User.UserMappedEmployeeId == 0 ||
                  SessionManager.User.UserMappedEmployeeId == -1)
            {
                NewMessage.ShowWarningMessage("User should be mapped to Employee for OT processing.");
                return;
            }

            int EmployeeID = int.Parse(hdnEmployeeID.Text);
            DateTime OTDate = DateTime.Parse(hdnOTDate.Text);

            if (string.IsNullOrEmpty(hdnApprovedMin.Text) || string.IsNullOrEmpty(hdnApprovedHour.Text))
            {
                NewMessage.ShowNormalMessage("Approved Hrs or Minute is required.");
                return;
            }

            int ApprovedMin = int.Parse(hdnApprovedMin.Text);
            int ApprovedHrs = int.Parse(hdnApprovedHour.Text);

            DAL.OvertimePeriodDetail _OvertimePeriodDetail = OvertimeManager.GetOvertimePeriodDetailsForEmployeOfDate(EmployeeID, OTDate);

            if (_OvertimePeriodDetail != null)
            {
                _OvertimePeriodDetail.ApprovedMin = ApprovedMin + (ApprovedHrs * 60);
                if (_OvertimePeriodDetail.ApprovedMin > int.Parse(_OvertimePeriodDetail.OTMinute.ToString()))

                {
                    NewMessage.ShowNormalMessage("Approved Hrs can not exceed OT hours");
                    return;
                }
                Status status = OvertimeManager.ApproveOvertimePeriodByLine(_OvertimePeriodDetail);
                if (status.IsSuccess)
                {
                    CheckboxSelectionModel1.ClearSelection();
                    NewMessage.ShowNormalMessage("Overtime approved successfully.");
                    PagingToolbar1.DoRefresh();
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }
            }
        }

        protected void btnRevertToSaved_Click(object sender, DirectEventArgs e)
        {
            if (SessionManager.User.UserMappedEmployeeId == null ||
               SessionManager.User.UserMappedEmployeeId == 0 ||
               SessionManager.User.UserMappedEmployeeId == -1)
            {
                NewMessage.ShowWarningMessage("User should be mapped to Employee for OT processing.");
                return;
            }

            int EmployeeID = int.Parse(hdnEmployeeID.Text);
            DateTime OTDate = DateTime.Parse(hdnOTDate.Text);
            DAL.OvertimePeriodDetail _OvertimePeriodDetail = OvertimeManager.GetOvertimePeriodDetailsForEmployeOfDate(EmployeeID, OTDate);

            if (_OvertimePeriodDetail != null)
            {
                Status status = OvertimeManager.RevertToSaved(_OvertimePeriodDetail);
                if (status.IsSuccess)
                {
                    CheckboxSelectionModel1.ClearSelection();
                    NewMessage.ShowNormalMessage("Overtime reverted to saved successfully.");
                    PagingToolbar1.DoRefresh();
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }
            }
        }
        protected void btnReject_Click(object sender, DirectEventArgs e)
        {
            if (SessionManager.User.UserMappedEmployeeId == null ||
                SessionManager.User.UserMappedEmployeeId == 0 ||
                SessionManager.User.UserMappedEmployeeId == -1)
            {
                NewMessage.ShowWarningMessage("User should be mapped to Employee for OT processing.");
                return;
            }

            int EmployeeID = int.Parse(hdnEmployeeID.Text);
            DateTime OTDate = DateTime.Parse(hdnOTDate.Text);
            DAL.OvertimePeriodDetail _OvertimePeriodDetail = OvertimeManager.GetOvertimePeriodDetailsForEmployeOfDate(EmployeeID, OTDate);

            if (_OvertimePeriodDetail != null)
            {
                Status status = OvertimeManager.RejectOvertimePeriodByLine(_OvertimePeriodDetail);
                if (status.IsSuccess)
                {
                    CheckboxSelectionModel1.ClearSelection();
                    NewMessage.ShowNormalMessage("Overtime Rejected successfully.");
                    PagingToolbar1.DoRefresh();
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }
            }
        }
    }
}
