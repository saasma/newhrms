﻿<%@ Page Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" Title="Deposit Document Details" EnableEventValidation="false"
    CodeBehind="DepositDocument.aspx.cs" Inherits="Web.CP.DepositDocument" %>

<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .GridRowHover
        {
            cursor: pointer;
        }
    </style>
    <script type="text/javascript">

        function refreshDocument(returnValue, popupWindow) {
            popupWindow.close();
            __doPostBack('<%= updPanel.ClientID %>', '')
            //process(returnValue);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Deposit Document Details
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <uc2:MsgCtl ID="divMsgCtl" Width="900px" EnableViewState="false" Hide="true" runat="server" />
        <uc2:WarningCtl ID="divWarningMsg" Width="900px" EnableViewState="false" Hide="true"
            runat="server" />
        <table style='clear: both'>
            <tr>
                <td>
                    <table class="dataField" style="height: 280px">
                        <tr>
                            <td class="fieldHeader">
                                Year *
                            </td>
                            <td>
                                <asp:DropDownList AutoPostBack="true" Width="200" ID="ddlYears" runat="server" DataValueField="FinancialDateId"
                                    DataTextField="Name" OnSelectedIndexChanged="ddlYears_Changed" AppendDataBoundItems="true">
                                    <asp:ListItem Value="-1">--Select Year--</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ValidationGroup="Inc" ID="RequiredFieldValidator8" Display="None"
                                    runat="server" InitialValue="-1" ControlToValidate="ddlYears" ErrorMessage="Year is required."></asp:RequiredFieldValidator>
                            </td>
                            <td style="width: 40px">
                            </td>
                            <td style="width: 300" rowspan="10" valign="top">
                                Upload Documents
                                <br />
                                <br />
                                <asp:UpdatePanel ID="updPanel" runat="server">
                                    <ContentTemplate>
                                        <asp:GridView DataKeyNames="DocumentId" PagerStyle-HorizontalAlign="Center" PagerStyle-CssClass="defaultPagingBar"
                                            ID="gvw" OnPageIndexChanging="gvw_PageIndexChanging" AllowPaging="false" runat="server"
                                            AutoGenerateColumns="False" CellPadding="4" GridLines="None">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Name" ItemStyle-Width="300px" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="HyperLink1" runat="server" Text='<%# Eval("Name") %>' NavigateUrl='<%# "~/DocumentHandler.ashx?id=" + System.IO.Path.GetFileNameWithoutExtension(Eval("Url").ToString()) %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Delete" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="30px">
                                                    <ItemTemplate>
                                                        <asp:ImageButton OnCommand="btnDelete_Click" CommandArgument='<%# Eval("DocumentId") %>'
                                                            CausesValidation="False" ID="btnDelete" OnClientClick="return confirm('Do you want to delete the document?')"
                                                            runat="server" CommandName="Delete" ImageUrl="~/images/delet.png" />
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="30px"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:TemplateField>
                                            </Columns>
                                            <RowStyle CssClass="odd" />
                                            <AlternatingRowStyle CssClass="even" />
                                            <SelectedRowStyle CssClass="selected" />
                                            <PagerStyle HorizontalAlign="Center" CssClass="defaultPagingBar"></PagerStyle>
                                            <EmptyDataTemplate>
                                                <b>No document uploaded. </b>
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <input id="btnAddDocument" type="button" runat="server" style='margin-top: 10px'
                                    value="Add" class="addbtns" onclick="popImageUploadCall(4)" />
                                <br />
                                <br />
                                <br />
                                Note<br />
                                <asp:TextBox ID="txtNote" runat="server" TextMode="MultiLine" Width="300" Height="100" />
                            </td>
                        </tr>
                        <tr>
                            <td class="fieldHeader">
                                Payroll Period *
                            </td>
                            <td>
                                <asp:DropDownList Width="200" ID="ddlPayrollPeriods" runat="server" DataValueField="PayrollPeriodId"
                                    DataTextField="Name">
                                    <asp:ListItem Value="-1">--Select Period--</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ValidationGroup="Inc" ID="RequiredFieldValidator3" Display="None"
                                    runat="server" InitialValue="-1" ControlToValidate="ddlPayrollPeriods" ErrorMessage="Payroll period is required."></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="fieldHeader">
                                Payment for *
                            </td>
                            <td>
                                <asp:DropDownList Width="200" ID="ddlPaymentFor" runat="server">
                                    <asp:ListItem Value="-1">--Select Payment for--</asp:ListItem>
                                    <asp:ListItem Value="1" Text="Tax Deducted at Source (TDS)" />
                                    <asp:ListItem Value="2" Text="Social Security Tax (SST)" />
                                    <asp:ListItem Value="3" Text="Provident Fund (PF)" />
                                    <asp:ListItem Value="4" Text="Citizen Investment Trust (CIT)" />
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ValidationGroup="Inc" ID="RequiredFieldValidator2" Display="None"
                                    runat="server" InitialValue="-1" ControlToValidate="ddlPaymentFor" ErrorMessage="Payment for is required."></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="fieldHeader">
                                Bank *
                            </td>
                            <td>
                                <asp:TextBox ID="txtBank" runat="server" Width="200" />
                                <asp:RequiredFieldValidator ValidationGroup="Inc" ID="RequiredFieldValidator1" Display="None"
                                    runat="server" ControlToValidate="txtBank" ErrorMessage="Bank is required."></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="fieldHeader">
                                Voucher No *
                            </td>
                            <td>
                                <asp:TextBox ID="txtVoucherNo" runat="server" Width="200" />
                                <asp:RequiredFieldValidator ValidationGroup="Inc" ID="RequiredFieldValidator4" Display="None"
                                    runat="server" ControlToValidate="txtVoucherNo" ErrorMessage="Voucher No is required."></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="fieldHeader">
                                Amount *
                            </td>
                            <td>
                                <asp:TextBox ID="txtAmount" runat="server" Width="200" />
                                <asp:RequiredFieldValidator ValidationGroup="Inc" ID="RequiredFieldValidator5" Display="None"
                                    runat="server" ControlToValidate="txtAmount" ErrorMessage="Amount is required."></asp:RequiredFieldValidator>
                                <asp:CompareValidator Operator="GreaterThanEqual" ValueToCompare="0" ValidationGroup="Inc"
                                    Type="Currency" ID="valCompareVariableFixedAmount" Display="None" ControlToValidate="txtAmount"
                                    runat="server" ErrorMessage="Invalid amount."></asp:CompareValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="fieldHeader">
                                Date Deposited *
                            </td>
                            <td>
                                <My:Calendar Id="calDateDeposited" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="fieldHeader">
                                Deposited By *
                            </td>
                            <td>
                                <asp:TextBox ID="txtDepositedBy" runat="server" Width="200" />
                                <asp:RequiredFieldValidator ValidationGroup="Inc" ID="RequiredFieldValidator6" Display="None"
                                    runat="server" ControlToValidate="txtDepositedBy" ErrorMessage="Deposited by is required."></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="fieldHeader">
                                Submission No *
                            </td>
                            <td>
                                <asp:TextBox ID="txtSubmissionNo" runat="server" Width="200" />
                                <asp:RequiredFieldValidator ValidationGroup="Inc" ID="RequiredFieldValidator7" Display="None"
                                    runat="server" ControlToValidate="txtSubmissionNo" ErrorMessage="Submission No is required."></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <div class="buttonsDiv" style="text-align: left; padding-left: 20px">
            <asp:Button ID="btnSave" CssClass="save" runat="server" Text="Save" OnClientClick="valGroup='Inc';return CheckValidation()"
                OnClick="btnSave_Click" />
            <asp:Button ID="btnCancel" CssClass="cancel" CausesValidation="false" runat="server"
                Text="Cancel" OnClick="btnCancel_Click" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="pageScriptsContent" runat="server">
    <script type="text/javascript">
        function popImageUploadCall() {


            var depositId1 = parseInt(depositId);

            if (depositId1 == 0) {
                alert("Please save the deposit first for document upload.");
                return;
            }

            var ret = popImageUpload("Id=" + depositId1);
            //process(ret);
        }
    </script>
</asp:Content>
