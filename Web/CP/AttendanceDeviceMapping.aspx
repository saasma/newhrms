<%@ Page Title="Attendance Device Mapping" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="AttendanceDeviceMapping.aspx.cs" Inherits="Web.AttendanceDeviceMapping" %>

<%@ Import Namespace="BLL.Manager" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .tableLightColor input
        {
            width: 120px;
        }
    </style>
    <script src="../Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        function refreshWindow() {

            __doPostBack('Refresh', 0);

        }
        jQuery(document).ready(
    function () {

        setMovementToGrid('#<%= gvEmployeeIncome.ClientID %>');
    }
);

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Attendance Device Mapping
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="attribute" style="padding: 10px">
            <table>
                <tr>
                    <td>
                        Search &nbsp;
                        <asp:TextBox ID="txtEmployeeName" runat="server" OnTextChanged="txtEmployeeName_TextChanged"
                            AutoPostBack="true" Width="146px" />
                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtEmployeeName"
                            WatermarkText="Search Employee" WatermarkCssClass="searchBoxText" />
                        <cc1:AutoCompleteExtender EnableCaching="true" ID="AutoCompleteExtenderOrganization"
                            runat="server" MinimumPrefixLength="2" ServiceMethod="GetEmployeeNames" ServicePath="~/PayrollService.asmx"
                            TargetControlID="txtEmployeeName" CompletionSetCount="10" CompletionInterval="250"
                            CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList"
                            CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
                        </cc1:AutoCompleteExtender>
                    </td>
                    <td>
                        <asp:LinkButton ID="btnExport" runat="server" Text="Excel import" OnClientClick="taxImportPopup();return false;"
                            CssClass=" excel marginRight tiptip" Style="float: left;" />
                    </td>
                </tr>
            </table>
            <div style="color:#FF983C;padding-top: 10px;">
                Note : Device ID must be unique for each employee
            </div>
        </div>
        <div class="clear">
            <cc1:TextBoxWatermarkExtender ID="TBWE2" runat="server" TargetControlID="txtEmployeeName"
                WatermarkText="Enter employee name" WatermarkCssClass="watermarked" />
            <cc2:EmptyDisplayGridView CssClass="tableLightColor" UseAccessibleHeader="true" ShowHeaderWhenEmpty="True"
                ID="gvEmployeeIncome" runat="server" DataKeyNames="EmployeeId" AutoGenerateColumns="False"
                CellPadding="4" GridLines="None" AllowSorting="True" ShowFooterWhenEmpty="False">
                <RowStyle BackColor="#E3EAEB" />
                <Columns>
                    <asp:BoundField HeaderStyle-Width="20px" DataFormatString="{0:000}" DataField="EmployeeId"
                        HeaderText="EIN"></asp:BoundField>
                    <asp:TemplateField HeaderText="Name" HeaderStyle-Width="150px">
                        <ItemTemplate>
                            <asp:Label ID="lblName" Text='<%# Eval("Name") %>' Style="width: 100px!important"
                                runat="server" />
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" Width="150px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Device ID" HeaderStyle-Width="40px">
                        <ItemTemplate>
                            <asp:TextBox CssClass='calculationInput' data-col='5' data-row='<%# Container.DataItemIndex %>'
                                Style='text-align: right' ID="txtTaxPaid" Text='<%#  Eval("DeviceId")%>' runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator SetFocusOnError="true" Display="None" ID="CompareValidator1"
                                ControlToValidate="txtTaxPaid" ValidationGroup="Balance" runat="server" ErrorMessage="Device id is required."></asp:RequiredFieldValidator>
                            <asp:CompareValidator SetFocusOnError="true" ValueToCompare="0" Display="None" Operator="DataTypeCheck"
                                Type="String" ID="valOpeningBalance245" ControlToValidate="txtTaxPaid" ValidationGroup="Balance"
                                runat="server" ErrorMessage="Invalid value."></asp:CompareValidator>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Allow Manual Attendance"
                        HeaderStyle-Width="40px">
                        <ItemTemplate>
                            <asp:CheckBox CssClass='calculationInput' data-col='5' Style='text-align: right'
                                Checked='<%#  Eval("IsEnabledManual")%>' ID="chkManualAttendance" runat="server">
                            </asp:CheckBox>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Skip Late Entry Mail"
                        HeaderStyle-Width="40px">
                        <ItemTemplate>
                            <asp:CheckBox CssClass='calculationInput' data-col='5' Style='text-align: right'
                                Checked='<%#  Eval("IsEnabledSkipLateEntry")==null?false:Eval("IsEnabledSkipLateEntry")%>' ID="chkSkipLateEntry" runat="server">
                            </asp:CheckBox>
                        </ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Skip Absent/Late Entry Deduction"
                        HeaderStyle-Width="40px">
                        <ItemTemplate>
                            <asp:CheckBox CssClass='calculationInput' data-col='5' Style='text-align: right'
                                Checked='<%#  Eval("SkipAbsentAndLateEntryDeduction")==null?false:Eval("SkipAbsentAndLateEntryDeduction")%>' ID="chkSkipAbsentLateProcessing" runat="server">
                            </asp:CheckBox>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <%--Text='<%#  Eval("DeviceId")%>' data-row='<%# Container.DataItemIndex %>'--%>
                    <%--  <asp:TemplateField HeaderText="Exclude Device Attendance" HeaderStyle-Width="150px">
                    <ItemTemplate>
                        <asp:Label ID="Label1" Text='<%# Eval("ExcludeDeviceAttendance") == null ? "" : (Eval("ExcludeDeviceAttendance").ToString().ToLower()=="true" ? "Yes": "") %>' Style="width: 100px!important" runat="server" />
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left" Width="150px" />
                </asp:TemplateField>--%>
                </Columns>
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                <EmptyDataTemplate>
                    <b>No employee list. </b>
                </EmptyDataTemplate>
            </cc2:EmptyDisplayGridView>
            <uc1:PagingCtl ID="pagingCtl" OnNextRecord="btnNext_Click" OnPrevRecord="btnPrevious_Click"
                OnDropDownShowRecordsChanged="ddlRecords_SelectedIndexChanged" runat="server" />
        </div>
        <div class="buttonsDiv">
            <%--    <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Balance" DisplayMode="List"
            ShowSummary="false" ShowMessageBox="true" runat="server"></asp:ValidationSummary>--%>
            <asp:Button ID="btnUpdate" CssClass="update" OnClientClick="valGroup='Balance';return CheckValidation();"
                Visible="true" ValidationGroup="Balance" runat="server" Text="Update" OnClick="btnUpdate_Click" />
        </div>
    </div>
</asp:Content>
