﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using BLL;
using DAL;
using Utils.Web;
using Utils.Helper;
using Utils;
using System.IO;
using Web.ReportDataSetTableAdapters;
using Web.CP.Report.Templates.Pay.Detail;
using BLL.BO;

namespace Web.CP
{
    public partial class EMIAdjustment : BasePage
    {
        CommonManager commonMgr = new CommonManager();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                Initialise();
            
        }

        public void Initialise()
        {

            ddlLoans.DataSource = PayManager.GetEMILoans();
            ddlLoans.DataBind();

            BindEmployees();
        }

        public void BindEmployees()
        {
        }

        protected void ddlPayrollPeriods_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindEmployees();
        }


        protected void btnSendMail_Click(object sender, EventArgs e)
        {

            bool isSaved = false;



            foreach (GridViewRow row in gvwSimpleLoanInstallments.Rows)
            {
                int payrollPeriodId = (int)gvwSimpleLoanInstallments.DataKeys[row.RowIndex]["PayrollPeriodId"];
                int IsCurrentValidPayrollPeriod = (int)gvwSimpleLoanInstallments.DataKeys[row.RowIndex]["IsCurrentValidPayrollPeriod"];
                int employeeId = (int)gvwSimpleLoanInstallments.DataKeys[row.RowIndex]["EmployeeId"];
                int deductionId = (int)gvwSimpleLoanInstallments.DataKeys[row.RowIndex]["DeductionId"];

                PEmployeeDeduction empDeduction = PayManager.GetEmployeeDeduction(employeeId, deductionId);

                if (IsCurrentValidPayrollPeriod == 1)
                {
                    //TextBox txtAdjustmentPPMT = row.FindControl("txtAdjustmentPPMT") as TextBox;
                    //TextBox txtAdjustmentIPMT = row.FindControl("txtAdjustmentIPMT") as TextBox;
                    Label lblPPMT = row.FindControl("lblPPMT") as Label;
                    Label lblIPMT = row.FindControl("lblIPMT") as Label;
                   
                    Label lblOpening = row.FindControl("lblOpening") as Label;

                    TextBox txtInstallmentAdjustment = row.FindControl("txtInstallmentAdjustment") as TextBox;

                    decimal ipmt = Convert.ToDecimal(lblIPMT.Text.Trim());

                    DAL.LoanAdjustment loan = new DAL.LoanAdjustment();


                    loan.PayrollPeriodId = payrollPeriodId;
                    loan.EmployeeId = employeeId;
                    loan.DeductionId = deductionId;
                    loan.DoNotAdjustLoan = false;
                    if (!string.IsNullOrEmpty(txtInstallmentAdjustment.Text.Trim()))
                        loan.InstallmentMonths = int.Parse(txtInstallmentAdjustment.Text.Trim());
                    else
                        loan.InstallmentMonths = 1;

                    if (row.RowIndex == 0 && loan.InstallmentMonths == 0)
                    {
                        divWarningMsg.InnerHtml = "Adjustment can not be done in first month, if needed then please add Loan from next month only.";
                        divWarningMsg.Hide = false;
                        return;
                    }


                    loan.Opening = Convert.ToDecimal(lblOpening.Text.Trim());
                    loan.IsEMI = true;

                    //if first installment
                    if (row.RowIndex == 0)
                    {
                        loan.Opening = empDeduction.AdvanceLoanAmount;
                    }
                    else
                    {

                    }

                    loan.PPMTAdjustment=0;
                    loan.IPMTAdjustment =0;
                    int remainingInstallment = empDeduction.RemainingInstallments;
                    int paidInstallment = empDeduction.NoOfInstallments.Value - remainingInstallment;

                    // calculate total PPMT & IMPT for total installment
                    for (int i = 1; i <= loan.InstallmentMonths; i++)
                    {
                        decimal ppmt = Convert.ToDecimal(BLL.BaseBiz.PayrollDataContext.GetPPMTForAdjustment(
                            empDeduction.EmployeeId, empDeduction.DeductionId, remainingInstallment, paidInstallment));

                        loan.PPMTAdjustment += ppmt;

                        loan.IPMTAdjustment += (empDeduction.InstallmentAmount - ppmt);

                        remainingInstallment -= 1;
                        paidInstallment += 1;

                    }

                    loan.AdjustedClosing = loan.Opening - loan.PPMTAdjustment;// -loan.IPMTAdjustment;

                    //if (txtAdjustmentPPMT.Text.Trim() == "" || txtAdjustmentPPMT.Text.Trim().Equals("0"))
                    //    loan.PPMTAdjustment = null;
                    //else
                    //    loan.PPMTAdjustment = decimal.Parse(txtAdjustmentPPMT.Text);


                    //if (txtAdjustmentIPMT.Text.Trim() == "" || txtAdjustmentIPMT.Text.Trim().Equals("0"))
                    //    loan.IPMTAdjustment = null;
                    //else
                    //    loan.IPMTAdjustment = decimal.Parse(txtAdjustmentIPMT.Text);

                    PayManager.SaveUpdateEMIAdjustment(loan, int.Parse(txtInterestRate.Text.Trim()), decimal.Parse(lblOpening.Text), decimal.Parse(lblPPMT.Text), ipmt
                        ,ddlLoans.SelectedItem.Text);

                    isSaved = true;
                    break;
                }
            }


            if (isSaved)
            {
                btnLoad_Click(null, null);

                divMsgCtl.InnerHtml = "Saved";
                divMsgCtl.Hide = false;
            }

        }

        protected void btnSendMailForAll_Click(object sender, EventArgs e)
        {
         
        }

        protected void gvwEmployees_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
          
        }

        protected void ddlLoans_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListItem firstItem =  ddlEmployees.Items[0];
            ddlEmployees.Items.Clear();
           

            ddlEmployees.DataSource = PayManager.GetSimpleLoansEmployees(int.Parse(ddlLoans.SelectedValue));
            ddlEmployees.DataBind();

            ddlEmployees.Items.Insert(0, firstItem);
        }

        public string GetAmount(object value,bool isLabel)
        {
            string currency = GetCurrency(value);
            string str;
            if (currency.ToString() == "0" || currency.ToString() == "0.00")
                str = "";
            else
                str = currency;

            if (isLabel && str == "")
                return "-";


            return str;
        }

        protected void btnLoad_Click(object sender, EventArgs e)
        {
            if (ddlEmployees.SelectedValue == "-1" || ddlLoans.SelectedValue == "-1")
            {
                pnlDetails.Visible = false;
            }
            else
            {
                pnlDetails.Visible = true;

                int employeeId = int.Parse(ddlEmployees.SelectedValue);
                int deductionId = int.Parse(ddlLoans.SelectedValue);
                // int payrollPeriodId = int.Parse(ddlPayrollPeriods.SelectedValue);

                bool hasCurrentPayrollPeriod = false;
                bool isInstallmentComplete = false;

                PEmployeeDeduction empDeduction = PayManager.GetEmployeeDeduction(employeeId, deductionId);
                // only for Monthly payment terms adjustment enabled, for other in future
                if (empDeduction.PaymentTerms != "1")
                {
                    divWarningMsg.InnerHtml = "Monthly payment terms can only be adjusted.";
                    divWarningMsg.Hide = false;
                    return;
                }

                List<GetLoanAdjustmentInstallmentsResult> loans = PayManager.GetEMIAdjustments(deductionId, employeeId, ref hasCurrentPayrollPeriod, ref isInstallmentComplete);
                
                //set Closing Balance


                for (int i = 1; i < loans.Count; i++)
                {
                    if (loans[i].InstallmentMonths == null)
                        loans[i].InstallmentMonths = 1;

                    decimal upperClosingBalance = 0;
                    for (int j = i - 1; j >= 0; j--)
                    {
                        if (loans[j].InstallmentMonths != 0)
                        {
                            upperClosingBalance = loans[j].ClosingBalance;
                            break;
                        }
                    }

                    if(loans[i].InstallmentMonths != 0)
                        loans[i].ClosingBalance = upperClosingBalance - (loans[i].EMI * loans[i].InstallmentMonths.Value);
                }

                int count = 1;
                for (int i = loans.Count - 1; i >= 0; i--)
                {
                    loans[i].RemainingInstallments = count++;
                }
                for (int i = 0; i < loans.Count; i++)
                {
                    if (loans[i].InstallmentMonths == 0)
                    {
                        loans[i].Opening = null;
                        loans[i].PPMT = null;
                        loans[i].IPMT = null;
                        loans[i].EMI = 0;
                        loans[i].Closing = null;
                        loans[i].ClosingBalance = 0;

                    }
                }
                
                gvwSimpleLoanInstallments.DataSource  = loans;

                gvwSimpleLoanInstallments.DataBind();



             
                //Process to set row separate for paid ones
                foreach (GridViewRow row in gvwSimpleLoanInstallments.Rows)
                {
                    //GetLoanAdjustmentInstallmentsResult loanItem = row.DataItem as GetLoanAdjustmentInstallmentsResult;
                    bool IsPayrollPeriodSaved = (bool)gvwSimpleLoanInstallments.DataKeys[row.RowIndex]["IsPayrollPeriodSaved"];
                    
                    if (IsPayrollPeriodSaved == false)
                    {
                        int columnIndex = 0;
                        foreach (TableCell cell in row.Cells)
                        {
                            if( columnIndex !=6 )                                
                                cell.CssClass = "hightlightCell";

                            columnIndex += 1;
                        }
                        break;
                    }
                }
                

                //set information
                PEmployeeDeduction empDed = PayManager.GetEmployeeDeduction(employeeId, deductionId);
                if (empDed != null)
                {
                    lblLoanAmount.Text = GetCurrency(empDed.AdvanceLoanAmount);
                    lblTakenOn.Text = empDed.TakenOn;
                    txtInterestRate.Text = empDed.InterestRate.ToString();
                    txtInterestRate.Enabled = hasCurrentPayrollPeriod;
                    btnSave.Visible = hasCurrentPayrollPeriod;
                    lblNoOfPayments.Text = empDed.ToBePaidOver.ToString();

                    lblPaymentTerm.Text = new EMILoadPaymentTerm().GetPaymentTerm(int.Parse(empDed.PaymentTerms));

                    lblTotalInterest.Text =
                        GetCurrency((empDed.InstallmentAmount * empDed.NoOfInstallments) -  empDed.AdvanceLoanAmount);
                    lblEMI.Text = GetCurrency(empDed.InstallmentAmount);

                    EEmployee emp = empDed.EEmployee;
                    lblEID.Text = emp.EmployeeId.ToString();
                    lblName.Text = emp.Name;
                    lblDesignation.Text = emp.EDesignation.Name;
                    lblBranch.Text = emp.Branch.Name;
                    lblDepartment.Text = emp.Department.Name;
                }


                //if valid payroll period not exists then show message
                else if (!hasCurrentPayrollPeriod)
                {
                    divMsgCtl.InnerHtml = Resources.ResourceTooltip.LoanAdjustmentNoValidPayrollPeriod;
                    divMsgCtl.Hide = false;
                }

                if (EmployeeManager.IsRetiredOrResignedIncludedInCalculation(employeeId))
                {
                    divWarningMsg.InnerHtml = "Employee already retired.";
                    divWarningMsg.Hide = false;
                    btnSave.Visible = false;
                    return;
                }


                //if all installment paid then show paid msg

                if (loans != null && loans.Count > 0)
                {
                    bool isAllPaid = true;
                    foreach (GetLoanAdjustmentInstallmentsResult item in loans)
                    {
                        if (item.IsPayrollPeriodSaved != null && item.IsPayrollPeriodSaved.Value == false)
                        {
                            isAllPaid = false;
                            break;
                        }
                    }
                    if (isAllPaid)
                    {
                        divMsgCtl.InnerHtml = Resources.Messages.InstallmentComplete;
                        divMsgCtl.Hide = false;
                    }
                }
            }
        }

        protected void gvwSimpleLoanInstallments_RowCommand(object sender, GridViewCommandEventArgs e)
        {

        }

        protected void gvwSimpleLoanInstallments_RowDataBound(object sender, GridViewRowEventArgs e)
        {
           
        }

       

    }
}
