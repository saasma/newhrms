﻿<%@ Page Title="Grade Transfer List" Language="C#" MasterPageFile="~/Master/HROld.Master" AutoEventWireup="true" CodeBehind="GradeTransferList.aspx.cs" Inherits="Web.CP.GradeTransferList" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

<script type="text/javascript">


    function popupAddGradeTransfer() {
        var ret = popGradeTransferDetls();

        if (typeof (ret) != 'undefined') {
            if (ret == 'Reload') {
                __doPostBack('Reload', '');
            }
        }
        return false;
    }

    function reloadGradeTransferList(childWindow) {
        childWindow.close();
        __doPostBack('Reload', '');
    }

    function popupEditGradeTransferDetls(gradeHistoryId) {
        var ret = popGradeTransferDetls('Id=' + gradeHistoryId);

        if (typeof (ret) != 'undefined') {
            if (ret == 'Reload') {
                __doPostBack('Reload', '');
            }
        }
        return false;
    }
    
    </script>


    <style type="text/css">
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
    </style>
    <link href="../css/core.css" rel="stylesheet" type="text/css" />

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">

<div class="contentArea">
        <h4>
            Grade Transfer List</h4>     
      
        <div class="attribute">
            <table>
                <tr>
                    <td>
                        <strong>Search </strong>&nbsp;
                        <asp:TextBox Width="160px" ID="txtEmpSearchText" runat="server" OnTextChanged="txtEmpSearchText_TextChanged"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Button ID="btnLoad" CssClass="load" Style="margin-left: 10px" runat="server"
                            OnClick="btnLoad_Click" Text="Load" />
                        <cc1:TextBoxWatermarkExtender ID="TBWE2" runat="server" TargetControlID="txtEmpSearchText"
                            WatermarkText="Employee name" WatermarkCssClass="searchBoxText" />
                    </td>
                
                </tr>
            </table>
        </div>
        <div style="clear:both;"/>
        <div style="text-align: left">         
                    <asp:Button ID="btnSave" Width="150px" Style="margin-left: 10px;" CausesValidation="false"
                    CssClass="save" runat="server" Text="New Grade Transfer" OnClientClick="return popupAddGradeTransfer();" />
            </div>
    
        <div class="clear" style="width: 1180px;margin-top:10px;">
            <cc2:EmptyDisplayGridView CssClass="tableLightColor" UseAccessibleHeader="true" ID="gvwList"
                runat="server" AutoGenerateColumns="False" DataKeyNames="GradeHistoryId,EmployeeId"
                GridLines="None" ShowFooterWhenEmpty="False" ShowHeaderWhenEmpty="True" 
                Width="100%">
                <Columns>
                    <asp:BoundField HeaderStyle-Width="150px" HeaderStyle-HorizontalAlign="Left" DataField="FromDate"
                        HeaderText="Date" />
                    <asp:BoundField HeaderStyle-Width="150px" HeaderStyle-HorizontalAlign="Left" DataField="FromDateEng" DataFormatString="{0:yyyy-M-dd}"
                        HeaderText="Date(AD)" />
                    <asp:BoundField HeaderStyle-HorizontalAlign="Center" DataField="EmployeeId" HeaderText="EIN" ItemStyle-HorizontalAlign="Center"
                        HeaderStyle-Width="80px" />
                    <asp:BoundField HeaderStyle-Width="200px" HeaderStyle-HorizontalAlign="Left" DataField="EmployeeName"
                        HeaderText="Name" />
                    <asp:BoundField HeaderStyle-Width="150px" HeaderStyle-HorizontalAlign="Left" DataField="FromGrade"
                        HeaderText="From Grade" />
                    <asp:BoundField HeaderStyle-Width="150px" HeaderStyle-HorizontalAlign="Left" DataField="ToGrade"
                        HeaderText="To Grade" />
                    <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>                       
                             <asp:ImageButton ID="btnEdit" ImageUrl="~/images/edit.gif" runat="server" ToolTip="Edit"
                                    OnClientClick='<%# "return popupEditGradeTransferDetls(" +  Eval("GradeHistoryId") + ");" %>'
                                    AlternateText="Edit" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                <EmptyDataTemplate>
                    No records.
                </EmptyDataTemplate>
            </cc2:EmptyDisplayGridView>
            <uc1:PagingCtl ID="pagingCtl" OnNextRecord="btnNext_Click" OnPrevRecord="btnPrevious_Click"
                OnDropDownShowRecordsChanged="ddlRecords_SelectedIndexChanged" runat="server" />
            
        </div>
    </div>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
