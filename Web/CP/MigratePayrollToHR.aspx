<%@ Page Title="About" Language="C#" MasterPageFile="~/Master/NewDetails.Master" AutoEventWireup="true" CodeBehind="MigratePayrollToHR.aspx.cs" Inherits="Web.CP.MigratePayrollToHR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <p>
        <img alt="rigopayroll" src="../images/about.RigoPayroll.jpg" 
            style="width: 362px; height: 127px" /></p>

    <p>
    Version : <span id="lblVersion" runat="server" />
    </p>

    <p>
    <ext:Label ID="lblMsg" runat="server" />
    </p>
    <ext:Button id="btnMigrate" runat="server" Text="Migrate" >
        <DirectEvents>
            <Click OnEvent="btnMigrate_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>

</asp:Content>
