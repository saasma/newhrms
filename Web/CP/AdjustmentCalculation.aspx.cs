﻿using System;
using System.Collections.Generic;
using System.Linq;
using BLL.Base;
using BLL.Manager;
using DAL;
using BLL;
using Utils.Calendar;
using System.IO;

namespace Web.CP
{
    public partial class AdjustmentCalculation : BasePage
    {
        public int PayrollPeriodId
        {
            get
            {
                return int.Parse(Request.QueryString["payrollPeriod"].ToString());
            }
        }

        public int Type
        {
            get
            {
                var value = Request.QueryString["type"].ToString();
                return int.Parse(value.Split(new char[] { ':' })[0]);
            }
        }
        public int SourceID
        {
            get
            {
                var value = Request.QueryString["type"].ToString();
                return int.Parse(value.Split(new char[] { ':' })[1]);
            }
        }
        public int EmployeeId
        {
            get
            {
                var value = Request.QueryString["eid"].ToString();
                return int.Parse(value);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialise();
            }


            Register();
        }

        /// <summary>
        /// Register javascript like current payroll period starting date to validate for reterospect increment
        /// </summary>
        protected void Register()
        {
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
        }

        public string GetDifference(object diff, object amount)
        {
            if (amount != null)
            {
                return GetCurrency(diff);
            }
            else
            {
                if (diff != null)
                {
                    return diff.ToString() + "%";
                }
            }
            return string.Empty;
        }

        public string GetAmountOrRate(object amount, object rate)
        {
            if (amount != null)
            {
                return GetCurrency(amount);
            }
            else
            {
                if (rate != null)
                {
                    return rate.ToString() + "%";
                }
            }
            return string.Empty;
        }

        private void Initialise()
        {
            calPayrollEndDate.IsEnglishCalendar = IsEnglish;
            calStartDate.IsEnglishCalendar = IsEnglish;

            var period = CommonManager.GetPayrollPeriod(PayrollPeriodId);

            calStartDate.SetSelectedDate(period.StartDate, IsEnglish);

            calPayrollEndDate.SetSelectedDate(period.EndDate, IsEnglish);
            calPayrollEndDate.Enabled = false;

            var basicIncome = new PayManager().GetBasicIncome(SessionManager.CurrentCompanyId);

            var list = PayManager.GetIncomeChangeHistory
                    (basicIncome.IncomeId, EmployeeId);
            ;

            foreach (CCalculationIncomeChangeHistory item in list)
            {
                var d = CustomDate.GetCustomDateFromString(item.FromDate, IsEnglish);
                item.FromDate = d.ToStringMonthEng();
            }

            gvwBasicSalary.DataSource = list;
            gvwBasicSalary.DataBind();


            txtExpression.Text = Request.QueryString["exp"];
            txtNote.Text = Request.QueryString["note"];

            if (txtNote.Text.Contains(" - "))
            {
                txtNote.Text = txtNote.Text.Substring(
                    txtNote.Text.IndexOf(" - ") + 3);
            }
            else
            {
                if (txtNote.Text.EndsWith(" -"))
                {
                    txtNote.Text = string.Empty;
                }
            }
            var type = Type;

            //title.InnerHtml +=
            //    (HPLAllowanceManager.GetAllowanceName((CalculationColumnType)type));


            var imageName = "~/images/allowance/" + ((CalculationColumnType)type).ToString() + ".png";

            if (File.Exists(Server.MapPath(imageName)))
            {
                img.ImageUrl = imageName;
            }
            else
            {
                img.Visible = false;
            }
        }

        protected void btnCalculate_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
            }
        }



        protected void btnOk_Click(object sender, EventArgs e)
        {
        }
    }
}
