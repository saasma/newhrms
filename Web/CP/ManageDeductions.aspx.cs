﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Manager;
using BLL;
using Utils.Helper;

namespace Web.CP
{
    public partial class ManageDeductions : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialise();
            }
            else if (Request.Form["__EVENTTARGET"].Equals(this.updPanel.ClientID))
            {
                Initialise();
            }
            JavascriptHelper.AttachPopUpCode(Page, "popupDeduction", "AEDeduction.aspx", 600, 600);
        }

     

        protected void Initialise()
        {
            //load incomes
            if (BLL.BaseBiz.PayrollDataContext.VoucherGroups.Any() == false)
                voucherAlsoChange.Visible = false;

            gvwDeductions.DataSource = PayManager.GetDeductionListByCompany(SessionManager.CurrentCompanyId);
            gvwDeductions.DataBind();
        }

        protected void gvwDeductions_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

            int id = (int)gvwDeductions.DataKeys[e.RowIndex][0];
            PayManager.DeleteDeduction(id);
            Initialise();
        }

        protected void Unnamed1_Click(object sender, EventArgs e)
        {

            foreach (GridViewRow row in this.gvwDeductions.Rows)
            {
                int deductionId = (int)(gvwDeductions.DataKeys[row.RowIndex][0]);
                TextBox txtOrder = row.FindControl("txtOrder") as TextBox;

                int order = 0;

                if (!string.IsNullOrEmpty(txtOrder.Text))
                    order = int.Parse(txtOrder.Text);


                PayManager.UpdateDeductionOrder(deductionId, order);


                divMsgCtl.InnerHtml = "Deduction order updated.";
                divMsgCtl.Hide = false;

            }

        }
    }
}
