<%@ Page Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/Master/ForPopupPage.Master"
    AutoEventWireup="true" CodeBehind="AELeave.aspx.cs" Title="Leave details" Inherits="Web.CP.AELeave" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Assembly="Karpach.WebControls" Namespace="Karpach.WebControls" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/UpDownCtl.ascx" TagName="UpDownCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="InfoMsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .colorStyle td
        {
            padding: 2px !important;
        }
        .transparent
        {
            /* this class makes a window partially transparent */
            opacity: 0; /* Standard style for transparency */
            -moz-opacity: 0; /* Transparency for older Mozillas */
            filter: alpha(opacity=00); /* Transparency for IE */
            width: 0;
        }
        .paddinAll
        {
            padding: 10px;
        }
        div.notify
        {
            margin-left: 0;
            width: 41%;
        }
        .lf
        {
            width: 150px;
            padding: 0 0px 0px 0;
        }
        .lfs
        {
            width: 160px;
            padding: 0 10px 0px 0;
            vertical-align: top;
        }
        table tr td
        {
            padding-bottom: 10px;
        }
        .bevel
        {
            margin-bottom: 20px;
        }
        
        div.notify
        {
            width: 510px;
        }
        .displayInline
        {
            display: inline;
        }
        #linkBranchDepartment
        {
            cursor: pointer;
            color: #62b1e9;
        }
        .tableLightColor1 th{background-color:#F5F5F5;color:#4A90CC;border:1px solid #DDDDDD;}
        .tableLightColor1 td{}
    </style>
    <script type="text/javascript">
        function closePopup() {
            // alert(window.opener.parentReloadCallbackFunction)
            //if ($.browser.msie == false && typeof (window.opener.reloadLeave) != 'undefined' && ($.browser.chrome || $.browser.safari)) {
            window.opener.reloadLeave(window);
            //            } else {
            //                window.returnValue = "Reload";
            //                window.close();
            //            }

        }


        var txtAccured = '<%= txtAccured.ClientID %>';
        var rowManualLeaveShowInLeaveRequest = '<%= rowManualLeaveShowInLeaveRequest.ClientID %>';
        function onChangeEncaseDays(ddl) {
            var upDownYearlyMaxBalance = '#<%= upDownYearlyMaxBalance.ClientID %>';
            if (ddl.value == 13) {
                enableElement(upDownYearlyMaxBalance);
            }
            else {
                disableElement(upDownYearlyMaxBalance);
            }
        }

        function onChangeAccure(ddlAccure) {
            //alert(ddlAccure);
            var rowLeavePerPeriod = '#<%= rowLeavePerPeriod.ClientID %> ';
            var rowWorkdayIncludesLeaves = '#<%=rowWorkdayIncludesLeaves.ClientID  %>';
            var rowWorkDaysPerPeriod = '#<%=rowWorkDaysPerPeriod.ClientID  %>';
            var rowUnusedLeave = '#<%= rowUnusedLeave.ClientID %>'
            var rowUnusedLeaveDetails = '#<%= rowUnusedLeaveDetails.ClientID %>'
            var rowHalfDay = '#<%= rowHalfDay.ClientID %>';
            var rowCompensatoryFulldayValue = '#<%=rowCompensatoryFulldayValue.ClientID %>';
            var rowCompensatoryHalfdayValue = '#<%=rowCompensatoryHalfdayValue.ClientID %>';
            var rowProportionatelyCalculation = '#<%=rowProportionatelyCalculation.ClientID %>';
            var rowMannualMaxPeriod = '#<%= rowMannualMaxPeriod.ClientID %>';
            var accureText = ddlAccure.value;


            $(rowLeavePerPeriod).hide();
            $(rowWorkdayIncludesLeaves).hide();
            $(rowWorkDaysPerPeriod).hide();
            $(rowCompensatoryFulldayValue).hide();
            $(rowCompensatoryHalfdayValue).hide();
            $(rowProportionatelyCalculation).hide();
            $(rowMannualMaxPeriod).hide();

            if (accureText == 'Monthly' || accureText == 'Yearly') {
                $(rowLeavePerPeriod).show();
                disableElement(txtAccured);

                //disableElement(chkClearBalance);
                $('#' + txtAccured).css('background-color', 'lightgrey');
                // $(rowProportionatelyCalculation).show();
            }
            else if (accureText == 'BasedOnWorkDays') {
                $(rowLeavePerPeriod).show();
                $(rowWorkdayIncludesLeaves).show();
                $(rowWorkDaysPerPeriod).show();
                disableElement(txtAccured);

                $('#' + txtAccured).css('background-color', 'lightgrey');
            }

            if (accureText == 'Manually') {
                $(rowMannualMaxPeriod).show();
                //$(rowLeavePerPeriod).show();
                // $(rowHalfDay).hide();
                $(rowUnusedLeave).hide();
                $(rowUnusedLeaveDetails).hide();
                enableElement(txtAccured);
                //  $('#' + rowManualLeaveShowInLeaveRequest).show();
                //$('#' + divClearBalance).show();
                $('#' + txtAccured).css('background-color', '');
            }
            else if (accureText == 'Compensatory') {
                //$(rowHalfDay).hide();
                $(rowUnusedLeave).hide();
                $(rowUnusedLeaveDetails).hide();
                enableElement(txtAccured);

                $('#' + txtAccured).css('background-color', '');

                $(rowCompensatoryFulldayValue).show();
                $(rowCompensatoryHalfdayValue).show();
            }
            else {
                $(rowUnusedLeave).show();
                $(rowUnusedLeaveDetails).show();
                $(rowHalfDay).show();
            }
            onUnusedLeaveChanged(document.getElementById('<%= ddlUnusedLeave.ClientID %>'))
        }

        function onUnusedLeaveChanged(ddlUnusedLeaveChanged) {
            var unUsedLeaveType = ddlUnusedLeaveChanged.value;
            var pnlLapse = '#<%= pnlLapse.ClientID %>';
            //var pnlEncase = 'pnlEncase.ClientID';
            var tdUnusedLeave = '#<%= tdUnusedLeave.ClientID %>';
            var pnlEncashmentIncomes = '#<%= pnlEncashmentIncomes.ClientID %>';
            var pnlLapseEncase = '#<%= pnlLapseEncase.ClientID %>';
            //var reqCblLapseOn = document.getElementById();

            $(pnlLapse).hide();
            //$(pnlEncase).hide();
            $(pnlLapseEncase).hide();
            $(pnlEncashmentIncomes).hide();
            $(tdUnusedLeave).hide();

            //reqCblLapseOn.ValidationGroup="";

            switch (unUsedLeaveType) {
                case "Lapse":
                    {
                        $(pnlLapse).show();
                        $(tdUnusedLeave).show();
                        $(pnlEncashmentIncomes).show();
                        //reqCblLapseOn.ValidationGroup="Leave";
                        break;
                    }
                case "Encase":
                    {
                        $(pnlEncashmentIncomes).show();
                        $(tdUnusedLeave).show();
                        break;
                    }
                case "LapseEncase":
                    {
                        $(pnlLapseEncase).show();
                        $(pnlEncashmentIncomes).show();
                        $(tdUnusedLeave).show();
                        break;
                    }
            }

        }

        function maxEncashChange(element) {

            if (element.checked)
                enableElement('ctl00_mainContent_upMaxEnashBalance_txt');
            else
                disableElement('ctl00_mainContent_upMaxEnashBalance_txt');
        }

        function showHideHolidayOption(value) {

            if (value == "true")
            {
                document.getElementById('<%=rowHolidayDoNotCountWeeklySaturdayAsLeave.ClientID %>').style.display = "";
                document.getElementById('<%=rowHolidayDoNotCountPublicHolidayAsLeave.ClientID %>').style.display = "";
            }
            else
            {
                document.getElementById('<%=rowHolidayDoNotCountWeeklySaturdayAsLeave.ClientID %>').style.display = "none";
                document.getElementById('<%=rowHolidayDoNotCountPublicHolidayAsLeave.ClientID %>').style.display = "none";
            }
            
        }


//        var ret = popupCreateNew('leavetype=' + leaveType);
//        if (typeof (ret) != 'undefined') {
//            if (ret == 'Reload') {
//                //refreshIncomeList(0);

//            }
//        }

        function popupCreateNewCall() {
            var LId = document.getElementById('<%= hdnLeaveTypeID.ClientID %>').value;

            var ret = popupCreateNew('LId=' + LId);
            if (typeof (ret) != 'undefined') {
                if (ret == "Reload") {
                    __doPostBack('Reload', '');
                }
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <asp:HiddenField ID="hdEditSequence" runat="server" />
    <asp:HiddenField ID="hdnLeaveTypeID" runat="server" />
    <asp:HiddenField ID="hdEmployeeEditSequence" runat="server" />
    <div class="popupHeader">
        <h3>
            Leave entry</h3>
    </div>
    <div class="marginal" style='margin-top: 0px'>
        <uc2:InfoMsgCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
        <fieldset class="large-heading">
        </fieldset>
        <div class="bevel paddinAll" style="width: 540px!important; margin-bottom: 5px!important">
            <table cellpadding="0" cellspacing="0" width="540px!important">
                <tr runat="server" id="rowGroupName">
                    <td class="lf">
                        <label runat="server" id="Label4">
                            Group name
                        </label>
                        <label>
                            *</label>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlGroupName" AutoPostBack="true" AppendDataBoundItems="true"
                            DataTextField="Title" DataValueField="LeaveTypeId" runat="server" Width="200px"
                            OnSelectedIndexChanged="ddlGroupName_SelectedIndexChanged">
                            <asp:ListItem Value="-1" Selected="True">--Group Name--</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator61" runat="server" ControlToValidate="ddlGroupName"
                            Display="None" ErrorMessage="Group name is required." ValidationGroup="Leave"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="lf">
                        <label runat="server" id="leaveTitle">
                            Leave name
                        </label>
                        <label>
                            *</label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtTitle" runat="server" Width="200px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="valReqdTitle" runat="server" ControlToValidate="txtTitle"
                            Display="None" ErrorMessage="Title is required." ValidationGroup="Leave"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="lf">
                        Short name (A-Z)<label>*</label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtAbbreviation" Width="200px" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtAbbreviation"
                            Display="None" ErrorMessage="Abbreviation is required." ValidationGroup="Leave"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="lf">
                        Color
                    </td>
                    <td>
                        <div class="colorStyle">
                            <cc2:ColorPicker ID="colorPicker" Width="200px" runat="server" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="lf">
                        Leave Type<label>*</label>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlLeaveType" runat="server" Width="200px">
                            <asp:ListItem Selected="True" Value="-1">-- Select Type --</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlLeaveType"
                            Display="None" ErrorMessage="Please select Leave Type." InitialValue="-1" ValidationGroup="Leave"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="lf">
                        Applies To<label>*</label>
                    </td>
                    <td>
                        <asp:DropDownList AppendDataBoundItems="true" Width="200px" ID="ddlAppliesTo" runat="server"
                            DataValueField="Key" DataTextField="Value">
                            <asp:ListItem Text="---Select---" Value="-1"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="ddlAppliesTo"
                            Display="None" ErrorMessage="Please select applies to." InitialValue="-1" ValidationGroup="Leave"></asp:RequiredFieldValidator>
                        <div id="divStatusList" runat="server" style="overflow-y: scroll; height: 85px; width: 385px;
                            border: 1px solid #C1D6F3; padding-top: 5px; padding: 0px">
                            <asp:CheckBoxList RepeatColumns="4" RepeatDirection="Horizontal" ID="chkStatusList"
                                runat="server" DataTextField="Value" DataValueField="Key">
                            </asp:CheckBoxList>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="lf" runat="server" id="gender">
                        Applies To ({0})
                    </td>
                    <td>
                        <asp:DropDownList AppendDataBoundItems="true" Width="200px" ID="ddlAppliesToGender"
                            runat="server" DataValueField="Key" DataTextField="Value">
                            <asp:ListItem Text="---Select---" Value="-1" />
                            <asp:ListItem Text="Female Only" Value="0" />
                            <asp:ListItem Text="Male Only" Value="1" />
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
        </div>
        <div class="clear">
            <fieldset style='padding-left: 0px!important; padding-bottom: 5px' class="large-heading">
                <h2>
                    Leave Settings</h2>
            </fieldset>
            <div class="bevel paddinAll" style="width: 540px!important; margin-bottom: 5px!important;
                padding-bottom: 0px!important">
                <table cellpadding="0" cellspacing="0" width="540px!important">
                    <tr>
                        <td class="lfs" style='width: 140px!important'>
                            <asp:Label ID="lblFOA" runat="server" Text="Frequency Of Accural" />
                            <asp:Label ID="lblreq" runat="server" Text="*" />
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlAccrue" runat="server" onchange="onChangeAccure(this)" Width="200px">
                                <asp:ListItem Text="-- Select Accural --" Value="-1"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator InitialValue="-1" ID="RequiredFieldValidator2" runat="server"
                                ControlToValidate="ddlAccrue" Display="None" ErrorMessage="Please select Frequency Of Accural."
                                ValidationGroup="Leave"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr runat="server" id="rowWorkdayIncludesLeaves">
                        <td class="lfs" style='width: 140px!important'>
                            <asp:Label ID="lblWI" runat="server" Text="Workday Includes" /><asp:Label ID="lblreqWI"
                                runat="server" Text="*" />
                        </td>
                        <td>
                            <div style="overflow-y: scroll; height: 135px; width: 200px; border: 1px solid #C1D6F3;
                                padding: 0px">
                                <asp:CheckBoxList ID="cblWorkDayInclude" runat="server" />
                            </div>
                        </td>
                    </tr>
                    <tr runat="server" id="rowWorkDaysPerPeriod">
                        <td class="lfs" style='width: 140px!important'>
                            Workdays per period:
                            <label>
                                *</label>
                        </td>
                        <td>
                            <uc1:UpDownCtl ID="upDownWorkDayPeriod" runat="server" />
                        </td>
                    </tr>
                    <tr runat="server" id="rowCompensatoryFulldayValue" style="display: none">
                        <td class="lfs" style='width: 140px!important'>
                            Full day on holiday
                            <label>
                                *</label>
                        </td>
                        <td>
                            <asp:TextBox Width="40" ID="upComensatoryFulldayValue" runat="server" />
                        </td>
                    </tr>
                    <tr runat="server" id="rowCompensatoryHalfdayValue" style="display: none">
                        <td class="lfs" style='width: 140px!important'>
                            Half day on holiday
                            <label>
                                *</label>
                        </td>
                        <td>
                            <asp:TextBox Width="40" ID="upCompensatoryHalfdayValue" runat="server" />
                        </td>
                    </tr>
                    <tr runat="server" id="rowBasedOnServicePeriod" style="display: none">
                        <td class="lfs" style='width: 140px!important'>
                            Based on service period
                        </td>
                        <td>
                            <asp:CheckBox AutoPostBack="true" OnCheckedChanged="chkBasedOnServicePeriod_Click"
                                Width="40" ID="chkBasedOnServicePeriod" runat="server" />
                        </td>
                    </tr>
                    <tr runat="server" id="rowServicePeriod" style="display: none;padding-bottom:10px;">
                        <td colspan='2' style='border: 1px solid lightblue;padding:5px;padding-top:10px;'>
                            <asp:GridView CssClass="tableLightColor1" ShowHeaderWhenEmpty="True" ID="gvwServicePeriods"
                                runat="server"  AutoGenerateColumns="False" OnRowDeleting="gvw_RowDeleting"
                                CellPadding="4" GridLines="None" ShowFooterWhenEmpty="False" 
                                EnableModelValidation="True">
                                <RowStyle BackColor="#E3EAEB" />
                                <Columns>
                                    <asp:TemplateField  HeaderText="Service Period Upto(Days)">
                                        <ItemTemplate>
                                            <asp:TextBox CssClass="item" ID="txtPeriodUpto" Height="16px" Width="90px" runat="server" Text='<%# Eval("PeriodUptoDays") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField  HeaderText="Leave Per Period">
                                        <ItemTemplate>
                                            <asp:TextBox CssClass="item" ID="txtLeavePerPeriod"  Height="16px" Width="90px" runat="server" Text='<%# Eval("LeavePerPeriod") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderStyle-Width="300px" HeaderText="Notes">
                                        <ItemTemplate>
                                            <asp:TextBox CssClass="item" ID="txtNotes" Width="120px"  Height="16px" runat="server" Text='<%# Eval("Notes") %>' />
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left" Width="120px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:CommandField ButtonType="Image" DeleteImageUrl="~/images/retdelete.png" ShowCancelButton="False"
                        ShowDeleteButton="True" />
                                </Columns>
                             
                            </asp:GridView>
                              <asp:Button Text="+Add New Line" runat="server" ID="btnAddRow" CssClass="cancel" style="width:100px;height:24px;"
                            OnClick="btnAddRow_Click"></asp:Button>
                         
                                <br /> <br />
                              <asp:CheckBox TextAlign="Left" style='padding-top:5px;padding-bottom:5px;'   Text="Deduct Prev Year Accural"
                                ID="chkAccuralDeductingPrevYearAccural" runat="server" />
                                 <br /> <br />
                                <span>Carry Forward Lapse month </span> <br />
                                 <asp:DropDownList ID="ddlLapseCarryForwardMonth" runat="server" DataValueField="Key" DataTextField="Value" AppendDataBoundItems="True" BorderColor="#C1D6F3"
                                            BorderStyle="Solid" BorderWidth="1px" Width="150px" 
                                            RepeatDirection="Horizontal">
                                            <asp:ListItem Text="--Select month--" Value="-1" />
                                        </asp:DropDownList>
                                        <br /> <br />
                        </td>
                    </tr>
                    <tr runat="server" id="rowLeavePerPeriod">
                        <td class="lfs" style='width: 140px!important'>
                            Leave Per Period<label>*</label>
                        </td>
                        <td>
                            <uc1:UpDownCtl ID="upDownLeavePerPeriod" AllowDecimal="true" runat="server" />
                        </td>
                    </tr>
                    <tr runat="server" id="rowMannualMaxPeriod">
                        <td class="lfs" style='width: 140px!important'>
                            Total Maximum Period
                        </td>
                        <td>
                            <uc1:UpDownCtl ID="upDownMannualMaxPeriod" Value="0" AllowDecimal="true" runat="server" />
                        </td>
                    </tr>
                    <tr runat="server" id="rowProportionatelyCalculation" style="display: none">
                        <td class="lfs" style='width: 140px!important'>
                            Skip Proportionately for Middle Join
                        </td>
                        <td>
                            <asp:CheckBox ID="chkSkipProportionatelyCalculation" runat="server" />
                        </td>
                    </tr>
                    <tr runat="server" id="rowManualLeaveShowInLeaveRequest" style="display: none">
                        <td class="lfs" style='width: 140px!important'>
                            Show in Leave Request
                        </td>
                        <td>
                            <asp:CheckBox ID="chkManualLeaveShowInLeaveRequest" runat="server" />
                        </td>
                    </tr>
                    <tr id="rowHalfDay" runat="server">
                        <td class="lfs" style='width: 140px!important;padding-top:5px;'>
                            <asp:Label ID="lblHalfDay" runat="server" Text="Half Day Leave" />
                        </td>
                        <td style='padding-top:5px;'>
                            <asp:DropDownList ID="ddlAllowHalfDay" runat="server" Width="200px" AppendDataBoundItems="true">
                                <asp:ListItem Selected="True" Text="--Select Half day--" Value="-1"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="ddlAllowHalfDay"
                                ErrorMessage="Please select for Half day." Display="None" ValidationGroup="Leave"
                                InitialValue="-1" />
                        </td>
                    </tr>
                    <tr runat="server" id="rowUnusedLeave">
                        <td class="lfs" style='width: 140px!important'>
                            Unused Leave<label>*</label>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlUnusedLeave" runat="server" Width="200px" onchange="javascript:onUnusedLeaveChanged(this)">
                                <asp:ListItem Value="-1" Selected="True">-- Select Unused Leave --</asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="reqUnusedLeave" runat="server" ControlToValidate="ddlUnusedLeave"
                                ErrorMessage="Please select Unused Leave" Display="None" ValidationGroup="Leave"
                                InitialValue="-1" />
                        </td>
                    </tr>
                    <tr runat="server" id="rowUnusedLeaveDetails">
                        <td width="350px;" runat="server" id="tdUnusedLeave" colspan="2" style="border: 1px solid lightgray;
                            padding: 5px;">
                            <table cellpadding="0" style="padding-bottom: 0px" cellspacing="0">
                                <tr runat="server" id="pnlLapse" style='padding: 0px;'>
                                    <td style='width: 145px'>
                                        Lapse On<label>*</label>
                                    </td>
                                    <td width="280px">
                                        <asp:CheckBoxList ID="cblLapseOn" runat="server" AppendDataBoundItems="True" BorderColor="#C1D6F3"
                                            BorderStyle="Solid" BorderWidth="1px" Width="350px" Height="80px" RepeatColumns="4"
                                            RepeatDirection="Horizontal">
                                        </asp:CheckBoxList>
                                        <%-- <cc2:RequiredForCheckBoxLists type="checkboxlist" ID="reqdCheckboxlist1" Display="None"
                                            ControlToValidate="cblLapseOn" ErrorMessage="Lapse month(s) must be selected."
                                            ValidationGroup="Leave" runat="server">&nbsp; </cc2:RequiredForCheckBoxLists>--%>
                                            <br />

                                            <asp:CheckBox runat="server" ID="chkEnableContractRenewalSetInStatusList" Text="Enable leave accural on contract renew period for contract status set in Status list" />
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top">
                                        <table id="pnlLapseEncase" runat="server" style='padding: 0px'>
                                            <tr id="Tr1" runat="server" style='padding: 0px'>
                                                <td>
                                                    Total Maximum Balance<label>*</label>
                                                </td>
                                                <td colspan="2">
                                                    <uc1:UpDownCtl ID="upDownMaxBalance" runat="server" />
                                                </td>
                                            </tr>
                                            <tr id="Tr2" runat="server" style='padding: 0px'>
                                                <td>
                                                    Yearly Maximum Balance<label>*</label>
                                                </td>
                                                <td colspan="2">
                                                    <uc1:UpDownCtl ID="upDownYearlyMaxBalance" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Encash<label>*</label>
                                                </td>
                                                <td>
                                                    <uc1:UpDownCtl ID="upDownEncase" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Encash On Retirement
                                                    <label>
                                                        *</label>
                                                </td>
                                                <td>
                                                    <uc1:UpDownCtl ID="upMaxEnashBalance" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    First Priority
                                                    <label>
                                                        *</label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlFirstPriority" runat="server" Width="120px">
                                                        <asp:ListItem Value="1" Selected="True">Encashment</asp:ListItem>
                                                        <asp:ListItem Value="2">Carry Forward</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td valign="top">
                                        <div id="pnlEncashmentIncomes" runat="server" style="overflow-y: scroll; height: 150px;
                                            width: 200px; border: 1px solid #C1D6F3; padding-top: 10px; padding: 0px">
                                            <strong style="padding-left: 5px">Encashment Incomes</strong>
                                            <asp:CheckBoxList ID="chkEncashmentIncomeList" runat="server" DataTextField="Title"
                                                DataValueField="IncomeId">
                                            </asp:CheckBoxList>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr runat="server" id="lr1">
                        <td colspan="2" style="padding-top: 10px">
                            <span style="font-weight: bold">Leave Request Settings</span>
                        </td>
                    </tr>
                    <tr id="lr2" runat="server">
                        <td class="lfs" style='width: 140px!important'>
                            <asp:Label ID="Label1" runat="server" Text="Count Holiday as Leave" />
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlCountHolidayAsLeave" onchange='showHideHolidayOption(this.value);'
                                runat="server" Width="250px" AppendDataBoundItems="true">
                                <asp:ListItem Selected="True" Text="--Select--" Value="-1"></asp:ListItem>
                                <asp:ListItem Text="Yes (Holiday will be counted as leave)" Value="false"></asp:ListItem>
                                <asp:ListItem Text="No (Holiday will be counted as Holiday)" Value="true"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="ddlCountHolidayAsLeave"
                                ErrorMessage="Setting is required." Display="None" ValidationGroup="Leave" InitialValue="-1" />
                        </td>
                    </tr>
                    <tr id="rowHolidayDoNotCountWeeklySaturdayAsLeave" style='display: none' runat="server">
                        <td class="lfs" style='width: 140px!important; padding-bottom: 10px;'>
                            <asp:Label ID="Label6" runat="server" Text="Exclude Public holiday" />
                        </td>
                        <td>
                            <asp:RadioButton  GroupName="holiday"  Width="250px" Text="Only count Weekly Saturday as holiday" runat="server"
                                ID="chkDoNotCountWeeklySaturdayAsLeave" />
                        </td>
                    </tr>
                    <tr id="rowHolidayDoNotCountPublicHolidayAsLeave" style='display: none' runat="server">
                        <td class="lfs" style='width: 140px!important; padding-bottom: 10px;'>
                            <asp:Label ID="Label7" runat="server" Text="Exclude Weekly holiday" />
                        </td>
                        <td>
                            <asp:RadioButton GroupName="holiday" Width="250px" Text="Only count Public Holiday as holiday" runat="server"
                                ID="chkDoNotCountPublicHolidayAsLeave" />
                        </td>
                    </tr>
                    <tr id="lr3" runat="server">
                        <td class="lfs" style='width: 140px!important'>
                            <asp:Label ID="Label3" runat="server" Text="Allow Negative Balance" />
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlAllowNegativeBalance" runat="server" Width="250px" AppendDataBoundItems="true">
                                <asp:ListItem Selected="True" Text="--Select--" Value="-1"></asp:ListItem>
                                <asp:ListItem Text="Yes" Value="true"></asp:ListItem>
                                <asp:ListItem Text="No" Value="false"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="ddlAllowNegativeBalance"
                                ErrorMessage="Setting is required." Display="None" ValidationGroup="Leave" InitialValue="-1" />
                        </td>
                    </tr>
                    <tr id="lr4" runat="server">
                        <td class="lfs" style='width: 140px!important'>
                            <asp:Label ID="Label2" runat="server" Text="Allowable Past days <br/>(Blank for no setting)" />
                        </td>
                        <td>
                            <asp:TextBox ID="txtAllowablePastDays" Width="100px" runat="server"></asp:TextBox>
                            <asp:CompareValidator ID="compPastDays" runat="server" ControlToValidate="txtAllowablePastDays"
                                Display="None" ErrorMessage="Positive integer days is required for allowable past days."
                                Type="Integer" Operator="GreaterThanEqual" ValueToCompare="0" ValidationGroup="Leave" />
                        </td>
                    </tr>
                    <tr id="Tr4" runat="server">
                        <td class="lfs" style='width: 140px!important'>
                            <asp:Label ID="Label5" runat="server" Text="Allowable Future days <br/>(Blank for no setting)" />
                        </td>
                        <td>
                            <asp:TextBox ID="txtAllowableFutureDays" Width="100px" runat="server"></asp:TextBox>
                            <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="txtAllowableFutureDays"
                                Display="None" ErrorMessage="Positive integer days is required for allowable future days."
                                Type="Integer" Operator="GreaterThanEqual" ValueToCompare="0" ValidationGroup="Leave" />
                        </td>
                    </tr>
                    <tr style='display: none'>
                        <td>
                           <%-- <a onclick="popupCreateNewCall();return false;" id="linkBranchDepartment">Apply Branch
                                Department</a>--%>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div runat="server" id="sectionEmployee" style='display: none; clear: both;'>
            <fieldset class="large-heading">
                <h2>
                    Employee details</h2>
            </fieldset>
            <div class="bevel paddinAll" style="width: 540px!important">
                <table>
                    <tr>
                        <td class="lf" style='width: 145px!important'>
                            Employee
                        </td>
                        <td>
                            <asp:TextBox Enabled="false" ID="txtEmployee" Width="200px" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="lf" style='width: 145px!important'>
                            Beginning Balance
                        </td>
                        <td>
                            <asp:TextBox Enabled="false" ID="txtBeginningBalance" Width="200px" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="lf" style='width: 145px!important'>
                            Accured
                        </td>
                        <td>
                            <asp:TextBox ID="txtAccured" Style="background-color: lightgray" runat="server" Width="200px" />
                            <%--  <div id="divClearBalance" runat="server" class='displayInline'>
                                <asp:CheckBox ID="chkClearBalance" Text="Change Balance" runat="server" onchange="document.getElementById('ctl00_mainContent_txtAccured').style.backgroundColor='';" />
                            </div>--%>
                            <asp:CompareValidator Operator="GreaterThanEqual" ValueToCompare="0" Type="Double"
                                ID="CompareValidator1" Display="None" ControlToValidate="txtAccured" runat="server"
                                ErrorMessage="Invalid accured value."></asp:CompareValidator>
                        </td>
                    </tr>
                    <tr id="Tr3" runat="server">
                        <td class="lf" style='width: 140px!important'>
                            Apply Leave without checking Applies to Status
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlApplyLeaveWithOutCheckingStatus" runat="server" Width="200px"
                                AppendDataBoundItems="true">
                                <asp:ListItem Text="Yes" Value="true"></asp:ListItem>
                                <asp:ListItem Text="No" Selected="True" Value="false"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="clear" style='text-align: right; padding-bottom: 20px; width: 565px'>
            <asp:Button ID="btnSave" CssClass="save" runat="server" OnClientClick="valGroup='Leave';return CheckValidation()"
                OnClick="btnOk_Click" Text="Save" ValidationGroup="Leave" />
            <asp:Button ID="btnCancel" runat="server" CssClass="cancel" Text="Cancel" OnClientClick="window.close()" />
        </div>
    </div>
</asp:Content>
