﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Base;
using DAL;
using Utils.Helper;
using Utils;
using Utils.Web;

namespace Web.CP
{
    public partial class ManageBranchDepartment : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.AdministratorTools;
            }
        }
    }
}
