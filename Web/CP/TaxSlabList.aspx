﻿<%@ Page Title="Tax Detail List" Language="C#" MasterPageFile="~/Master/NewDetails.Master" AutoEventWireup="true" CodeBehind="TaxSlabList.aspx.cs" Inherits="Web.CP.TaxSlabList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

<style type="text/css">
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
</style>

<script type="text/javascript">

    var myRenderer = function (value, metadata) {
        if (parseFloat(value) == 0)
            return '';
        else
            return parseFloat(value).toFixed(2);
    };

    var myRenderer1 = function (value, metadata) {
        if (parseFloat(value) == 0)
            return '';
        else
            return parseFloat(value).toFixed(0);
    };


    var beforeEdit = function (e1, e, e2, e3) {
        if ((e.field == "EndingRange") && (e.rowIdx == 2 || e.rowIdx == 5)) {
            e.cancel = true;
            return;
        }
    };

    var afterEdit = function (obj, e, e1, e2, e3) {

        /*
        Properties of 'e' include:
        e.grid - This grid
        e.record - The record being edited
        e.field - The field name being edited
        e.value - The value being set
        e.originalValue - The original value for the field, before the edit.
        e.row - The grid row index
        e.column - The grid column index
        */
        //            e.record.get('Name')

        var storeGrid = e.grid.getStore();

        var i = 0;

        var currentRowIndex = e.rowIdx;
        var nextRowIndex = currentRowIndex + 1;

        for (; i < storeGrid.data.length; i++) {

            if (i == nextRowIndex) {
                var record = storeGrid.data.items[i];
                record.data.StartingRange = e.value + 1;
                record.commit();
            }

        }
    };


</script>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">

<div class="pageheader">
    <div class="media">
        <div class="media-body">
            <h4>
                Tax Details List
            </h4>
        </div>
    </div>
</div>

<div class="contentpanel">


<table>
    <tr>
        
        <td>
            <pr:CalendarExtControl ID="txtEffectiveDate" Disabled="true" LabelSeparator="" runat="server" FieldLabel="Effective Date" MarginSpec="20 0 0 10"
                Width="220" LabelAlign="Left">
            </pr:CalendarExtControl>
        </td>
        <td style='padding-left: 162px; padding-top: 10px;'>
            <ext:Button runat="server" Cls="btn btn-primary"  Icon="Add" ID="btnAdd" Text="<i></i>Add New">
                <DirectEvents>
                    <Click OnEvent="btnAdd_Click">
                        <EventMask ShowMask="true" />
                    </Click>
                </DirectEvents>
            </ext:Button>
        </td>
    </tr>
</table>

<br />

<ext:GridPanel ID="gridTaxDetails" runat="server" Width="500" Cls="itemgrid">
    <Store>
        <ext:Store ID="Store1" runat="server">
            <Model>
                <ext:Model ID="Model4" runat="server">
                    <Fields>
                        <ext:ModelField Name="TypeName" Type="String" />
                        <ext:ModelField Name="StartingRange" Type="Float" />
                        <ext:ModelField Name="EndingRange" Type="Float" />
                        <ext:ModelField Name="Rate" Type="Float" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
    </Store>
    <ColumnModel>
        <Columns>
            <ext:Column ID="colTypeName" Sortable="false" MenuDisabled="true" runat="server" Text="Tax Status"
                Align="Left" Width="100" DataIndex="TypeName" />
            <ext:NumberColumn ID="colRate" runat="server" Sortable="false" Align="Center"
                MenuDisabled="true" Text="Slab" Width="100" DataIndex="Rate">
                <Renderer Fn="getFormattedAmount" />
            </ext:NumberColumn>
            <ext:NumberColumn ID="colStartingRange" runat="server" Sortable="false" Align="Right"
                MenuDisabled="true" Text="Starting Range" Width="150" DataIndex="StartingRange">
                <Renderer Fn="getFormattedAmount" />
            </ext:NumberColumn>
            <ext:NumberColumn ID="colEndingRange" runat="server" Sortable="false" Align="Right"
                MenuDisabled="true" Text="Ending Range" Width="150" DataIndex="EndingRange">
                <Renderer Fn="getFormattedAmount" />
            </ext:NumberColumn>
            

        </Columns>
    </ColumnModel>
    <SelectionModel>
        <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
    </SelectionModel>
</ext:GridPanel>

</div>

<ext:Window ID="WTaxDetails" runat="server" Title="Tax Details" Icon="Application"
        Resizable="false" Width="610" Height="400" BodyPadding="5" Hidden="true" Modal="true">
        <Content>
            <table style="margin-left: 20px; margin-top: 10px;">
                <tr>
                    <td style="width: 850px;">
                        <pr:CalendarExtControl ID="calEffectiveDate" LabelSeparator="" runat="server" FieldLabel="Effective Date *" MarginSpec="20 0 0 0"
                            Width="250" LabelAlign="Left">
                        </pr:CalendarExtControl>
                        <asp:RequiredFieldValidator Display="None" ID="rfvEffectiveDate" runat="server" ValidationGroup="SaveUpdate"
                            ControlToValidate="calEffectiveDate" ErrorMessage="Effective Date is required." />
                    </td>
                </tr>
            </table>

            <ext:GridPanel StyleSpec="margin-top:15px; margin-left:20px;" ID="gridTaxDetailsAdd"
                runat="server" Cls="itemgrid" Scroll="None" Width="500">
                <Store>
                    <ext:Store ID="storeTaxDetls" runat="server">
                        <Model>
                            <ext:Model ID="modelTaxDetls" Name="SettingModel" runat="server" >
                                <Fields>
                                    <ext:ModelField Name="Type" Type="Int" />
                                    <ext:ModelField Name="TypeName" Type="String" />
                                    <ext:ModelField Name="StartingRange" Type="Float" />
                                    <ext:ModelField Name="EndingRange" Type="Float"/>
                                    <ext:ModelField Name="Rate" Type="Float" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:Column ID="Column1" Sortable="false" MenuDisabled="true" runat="server" Text="Tax Status"
                            Align="Left" Width="100" DataIndex="TypeName">
                        </ext:Column>
                        <ext:NumberColumn ID="NumberColumn3" runat="server" Sortable="false" Align="Center"
                            MenuDisabled="true" Text="Slab" Width="100" DataIndex="Rate">
                            <Renderer Fn="myRenderer1" />
                        </ext:NumberColumn>
                        <ext:NumberColumn ID="NumberColumn1" runat="server" Sortable="false" Align="Right"
                            MenuDisabled="true" Text="Starting Range" Width="150" DataIndex="StartingRange">
                            <Renderer Fn="myRenderer" />
                        </ext:NumberColumn>
                        <ext:NumberColumn ID="NumberColumn2" runat="server" Sortable="false" Align="Right"
                            MenuDisabled="true" Text="Ending Range" Width="150" DataIndex="EndingRange">
                            <Renderer Fn="myRenderer" />
                            <Editor>
                                <ext:NumberField ID="txtEndingRage" runat="server" MinValue="0" />
                            </Editor>
                        </ext:NumberColumn>
                        
                    </Columns>
                </ColumnModel>
                <SelectionModel>
                    <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                </SelectionModel>
                <Plugins>
                    <ext:CellEditing ID="CellEditing1" runat="server" ClicksToEdit="1">
                    </ext:CellEditing>
                </Plugins>
                <Listeners>
                    <BeforeEdit Fn="beforeEdit" />
                    <Edit Fn="afterEdit" />
                </Listeners>
            </ext:GridPanel>
            <div class="popupButtonDiv bottomBlock" style="padding-left: 20px;">
                <ext:Button runat="server" ID="btnSave" Cls="btn btn-primary" Text="<i></i>Save">
                    <DirectEvents>
                        <Click OnEvent="btnSave_Click">
                            <EventMask ShowMask="true" />
                            <ExtraParams>
                                <ext:Parameter Name="gridItems" Value="Ext.encode(#{gridTaxDetailsAdd}.getRowsValues({ selectedOnly: false }))"
                                    Mode="Raw" />
                            </ExtraParams>
                        </Click>
                    </DirectEvents>
                    <Listeners>
                        <Click Handler="valGroup = 'SaveUpdate'; if(CheckValidation()) return ''; else return false;">
                        </Click>
                    </Listeners>
                </ext:Button>
                <div class="btnFlatOr">
                </div>
                <ext:Button runat="server" ID="Button1" Cls="btn btn-default" Text="<i></i>Cancel">
                    <Listeners>
                        <Click Handler="#{WTaxDetails}.hide();" />
                    </Listeners>
                </ext:Button>
            </div>
        </Content>
    </ext:Window>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
