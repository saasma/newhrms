<%@ Page MaintainScrollPositionOnPostback="true" Title="Messages" Language="C#"
    MasterPageFile="~/Master/Report.Master" AutoEventWireup="true" CodeBehind="Message1.aspx.cs"
    Inherits="Web.CP.VariableDeductionImport" %>

<%@ Register Src="~/Employee/UserControls/PayrollMessageInbox.ascx" TagName="Inbox"
    TagPrefix="uc1" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <ext:ResourceManager ID="ResourceManager1" DisableViewState="false" runat="server"
        ShowWarningOnAjaxFailure="false" ScriptMode="Release">
    </ext:ResourceManager>
    <uc1:Inbox ID="Inbox1" runat="server" />
</asp:Content>
