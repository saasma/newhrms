﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;
using Utils.Calendar;
using Utils.Helper;

namespace Web.NewHR
{
    public partial class AllowanceForward : BasePage
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!X.IsAjaxRequest && !IsPostBack)
            {
                Initialise();


                JavascriptHelper.AttachNonDialogPopUpCode(Page, "assignovertimePopup", "AllowanceForwardAssignPopup.aspx", 475, 550);
                JavascriptHelper.AttachNonDialogPopUpCode(Page, "positionHistoryPopup", "AllowanceForwardPopup.aspx", 625, 600);

            }
        }

        public void Initialise()
        {
            List<EveningCounterType> list = AllowanceManager.GetAllEveningCounterList();
            list.Insert(0, new EveningCounterType { Name = "All", EveningCounterTypeId = -1 });
            cmbType.Store[0].DataSource = list;
            cmbType.Store[0].DataBind();


            List<Branch> branchList = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            //branchList.Insert(0, new Branch { Name = "All", BranchId = -1 });
            cmbBranch.Store[0].DataSource = branchList;
            cmbBranch.Store[0].DataBind();

            PayrollPeriod period = CommonManager.GetLastPayrollPeriod();
            calPostedMonth.Text = period.EndDate;
        }


        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {


            int type = 1;
            int status = -1;

            status = tabPanel.ActiveTabIndex - 1;


            if (cmbPeriodType.SelectedItem != null && cmbPeriodType.SelectedItem.Value != null)
                type = int.Parse(cmbPeriodType.SelectedItem.Value);

            DateTime? start = null;
            DateTime? end = null;

            DateTime date;
            if (cmbPeriodType.SelectedItem.Value == "-1")
            {
                if (DateTime.TryParse(dateFrom.Text, out date))
                    start = dateFrom.SelectedDate;
                if (DateTime.TryParse(dateTo.Text, out date))
                    end = dateTo.SelectedDate;
            }

            if (start != null && start == DateTime.MinValue)
                start = null;

            if (end != null && end == DateTime.MinValue)
                end = null;

            int? totalRecords = 0;
            int BranchID = -1;

            if (cmbBranch.SelectedItem  != null && cmbBranch.SelectedItem.Value != null)
            {
                BranchID = int.Parse(cmbBranch.SelectedItem.Value);
            }


            int? postedPeriodId = null;
            if (!string.IsNullOrEmpty(calPostedMonth.Text))
            {
                CustomDate postedDate = CustomDate.GetCustomDateFromString(calPostedMonth.Text, IsEnglish);
                PayrollPeriod period = CommonManager.GetPayrollPeriod(postedDate.Month, postedDate.Year);
                if (period != null)
                    postedPeriodId = period.PayrollPeriodId;
            }

            if (e.Sort.Length > 0)
                hdnSortBy.Text = (e.Sort[0].Property + " " + e.Sort[0].Direction);//.ToLower();
            else
                hdnSortBy.Text = "";

            string ein = "";
            if (cmbSearch.SelectedItem != null && cmbSearch.SelectedItem.Value != null)
                ein = cmbSearch.SelectedItem.Value;

            gridList.Store[0].DataSource = OvertimeManager.GetEveningCounterListForAdmin(type, status, start, end, ein
                , e.Page, e.Limit, ref totalRecords, BranchID
                , int.Parse(cmbType.SelectedItem.Value), hdnSortBy.Text, postedPeriodId);
            gridList.Store[0].DataBind();

            


            e.Total = totalRecords.Value;

            

            //List<GetOvertimeRequestForAdminResult> list = EmployeeManager.GetLeaveFareDetails
            //    ((period == null ? 0  : period.PayrollPeriodId),startDate,endDate, employeeId, e.Page - 1, e.Limit, (e.Sort[0].Property + " " + e.Sort[0].Direction).ToLower(), ref totalRecords
            //    , isApprovedOnly, paymentStatus);


            

            ////if (list.Any())
            //gridList.Store[0].DataSource = list;
            //gridList.Store[0].DataBind();

        }


        public void btnExport_Click(object sender, EventArgs e)
        {
            int type = 1;
            int status = -1;

            status = tabPanel.ActiveTabIndex - 1;


            if (cmbPeriodType.SelectedItem != null)
                type = int.Parse(cmbPeriodType.SelectedItem.Value);

            DateTime? start = null;
            DateTime? end = null;

            DateTime date;
            if (cmbPeriodType.SelectedItem.Value == "-1")
            {
                if (DateTime.TryParse(dateFrom.Text, out date))
                    start = dateFrom.SelectedDate;
                if (DateTime.TryParse(dateTo.Text, out date))
                    end = dateTo.SelectedDate;
            }

            if (start != null && start == DateTime.MinValue)
                start = null;

            if (end != null && end == DateTime.MinValue)
                end = null;

            int? totalRecords = 0;
            int BranchID = -1;

            if (cmbBranch.SelectedItem != null && cmbBranch.SelectedItem.Value != null)
            {
                BranchID = int.Parse(cmbBranch.SelectedItem.Value);
            }

            int? postedPeriodId = null;
            if (!string.IsNullOrEmpty(calPostedMonth.Text))
            {
                CustomDate postedDate = CustomDate.GetCustomDateFromString(calPostedMonth.Text, IsEnglish);
                PayrollPeriod period = CommonManager.GetPayrollPeriod(postedDate.Month, postedDate.Year);
                if (period != null)
                    postedPeriodId = period.PayrollPeriodId;
            }

            string ein = "";
            if (cmbSearch.SelectedItem != null && cmbSearch.SelectedItem.Value != null)
                ein = cmbSearch.SelectedItem.Value;

            List<GetEveningCounterRequestForAdminResult> list = OvertimeManager.GetEveningCounterListForAdmin(type, status, start, end, ein
                , 1, 9999999, ref totalRecords, BranchID
                , int.Parse(cmbType.SelectedItem.Value),hdnSortBy.Text,postedPeriodId);
            //foreach (var item in list)
            //{
            //    item.DateEngFormatted = Web.Helper.WebHelper.FormatDateForExcel(item.Date);
            //}

            Bll.ExcelHelper.ExportToExcel("Allowance List", list,
                new List<String>() { "TotalRows","StartDateEngFormatted","EndDateEngFormatted","DurationModified", "RowNumber", "RequestID", "SupervisorID", "Status",  "ApprovedOn", "Date" },
            new List<String>() { },
            new Dictionary<string, string>() { {"EmployeeID","EIN"}, {"EmployeeName","Employee"}, 
                    {"OvertimeType","Type"}, {"NepDate","Nep Date"}, { "StartTime", "Start Date" },
                    { "EndTime", "End Date" }, { "CheckInTime", "In Time" }, { "CheckOutTime", "Out Time" }, 
                    {"Duration" , "Units"},{"ApprovedTime","Approved Hr"}, {"DateEngFormatted", "Date"},
                    { "SupervisorName", "Proccessed By" }, { "StatusModified", "Status" } },
            new List<string>() { }
            , new List<string> { }
            , new List<string> { "StartTime", "EndTime" }
            , new Dictionary<string, string>() { }
            , new List<string> { "EmployeeID", "EmployeeName", "OvertimeType", "DateEngFormatted", "NepDate", "StartTime", "EndTime", "StartDateNep", "EndDateNep", "Duration", "ApprovedTime", "Reason", "SupervisorName", "StatusModified" });

            

        }




        protected void btnPost_Click(object sender, DirectEventArgs e)
        {


            string gridItemsJson = e.ExtraParams["gridItems"];
            List<GetEveningCounterRequestForAdminResult> list = JSON.Deserialize<List<GetEveningCounterRequestForAdminResult>>(gridItemsJson);
            //List<OvertimePeriodDetail> _OvertimePeriodDetail = new List<OvertimePeriodDetail>();
            List<EveningCounterRequest> requestList = new List<EveningCounterRequest>();

            foreach (GetEveningCounterRequestForAdminResult item in list)
            {
                EveningCounterRequest request = new EveningCounterRequest();
                request.CounterRequestID = item.RequestID;
                requestList.Add(request);
            }

            int count = 0;
            AllowanceManager.ForwardRequestInBulk(requestList, out count);

            CheckboxSelectionModel1.ClearSelection();
            NewMessage.ShowNormalMessage(count + " allowance has been forwarded.");
            PagingToolbar1.DoRefresh();


        }
        
        protected void btnApproveByLine_Click(object sender, DirectEventArgs e)
        {
           
        }

        protected void btnReject_Click(object sender, DirectEventArgs e)
        {
            string gridItemsJson = e.ExtraParams["gridItems"];
            List<GetEveningCounterRequestForAdminResult> list = JSON.Deserialize<List<GetEveningCounterRequestForAdminResult>>(gridItemsJson);
            //List<OvertimePeriodDetail> _OvertimePeriodDetail = new List<OvertimePeriodDetail>();
            List<EveningCounterRequest> requestList = new List<EveningCounterRequest>();

            foreach (GetEveningCounterRequestForAdminResult item in list)
            {
                EveningCounterRequest request = new EveningCounterRequest();
                request.CounterRequestID = item.RequestID;
                requestList.Add(request);
            }

            int count = 0;
            AllowanceManager.RejectRequestInBulk(requestList, out count);

            CheckboxSelectionModel1.ClearSelection();
            NewMessage.ShowNormalMessage(count + " allowancce has been rejected.");
            PagingToolbar1.DoRefresh();

        }
    }
}
