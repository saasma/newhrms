﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using DAL;
using BLL;
using Utils.Helper;
using Utils;

namespace Web.User
{
    public partial class ManageDepositList : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.AdministratorTools;
            }
        }     

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialise();
                ddlUserType_SelectedIndex(null, null);
               
            }

        }

        private void Initialise()
        {
            List<FinancialDate> years = new CommonManager().GetAllFinancialDates();
            foreach (FinancialDate date in years)
            {
                date.SetName(IsEnglish);
            }
            ddlYears.DataSource = years;
            ddlYears.DataBind();

            ddlYears.SelectedIndex = years.Count;
        }

        public string GetEmployeeName(object value)
        {
            if (value == null)
                return "";
            return (value as EEmployee).Name;

        }
        public string GetEmployeeId(object value)
        {
            if (value == null)
                return "";
            return (value as EEmployee).EmployeeId.ToString();
        }

        public string prevPeriodName = "";
        public string GetPeriodName(int period)
        {
            PayrollPeriod p =  CommonManager.GetPayrollPeriod(period);

            if (prevPeriodName == "")
                prevPeriodName = p.Name;
            else if (prevPeriodName == p.Name)
                return "";
            
            return p.Name.Remove(p.Name.IndexOf("/"));
        }

        public string GetTypeName(string type)
        {
            if (type == "1") return "Tax Deducted at Source (TDS)";
            if (type == "2") return "Social Security Tax (SST)";
            if (type == "3") return "Provident Fund (PF)";
            if (type == "4") return "Citizen Investment Trust (CIT)";
            return "";
        }

        public void ddlYears_Changed(object sender, EventArgs e)
        {
            BindUsers();
        }
        void BindUsers()
        {
            int totalRecords = 0;

            List<DepositDocument> list = HRManager.GetDepoistDocumentList(int.Parse(ddlYears.SelectedValue));
            gvwRoles.DataSource = list;
            gvwRoles.DataBind();
              
        }

        public bool IsMainAdmin(object UserName)
        {
            if (UserName.ToString().ToLower() == "admin")
                return true;
            return false;
        }

       

      

        public bool CanChange(object value)
        {
            return Convert.ToBoolean(value);
        }


        protected void gvwRoles_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvwRoles.PageIndex = e.NewPageIndex;
            gvwRoles.SelectedIndex = -1;
            BindUsers();
           
        }
        
        protected void ddlUserType_SelectedIndex(object sender, EventArgs e)
        {
            BindUsers();
        }

  

        protected void gvwRoles_SelectedIndexChanged(object sender, EventArgs e)
        {
            string id = gvwRoles.DataKeys[gvwRoles.SelectedIndex]["DepositDocumentId"].ToString();
            Response.Redirect("DepositDocument.aspx?Id=" + id);
            
        }

        protected void gvwRoles_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            string id = gvwRoles.DataKeys[e.RowIndex]["DepositDocumentId"].ToString();

            HRManager.DeleteDepositDocument(int.Parse(id));

            BindUsers();

            msgInfo.InnerHtml = "Deposit deleted.";
            msgInfo.Hide = false;
            e.Cancel = true;

            //if (UserManager.DeleteUser(userName))
            //{
            //    msgInfo.InnerHtml = Resources.Messages.UserDeletedMsg; ;
            //    msgInfo.Hide = false;
            //}
            //else
            //{
            //    msgInfo.InnerHtml = "User can not be deleted, its in use." ;
            //    msgInfo.Hide = false;
            //}

            //gvwRoles.SelectedIndex = -1;
            //BindUsers();
            //Clear();
        }

       
    }
}
