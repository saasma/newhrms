﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;
using Utils.Calendar;
using Utils.Helper;

namespace Web.NewHR
{
    public partial class PeriodicPayDetails : BasePage
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!X.IsAjaxRequest && !IsPostBack)
            {
                Initialise();

              

            }
        }

        public void Initialise()
        {
            List<PayGroup> list = BLL.BaseBiz.PayrollDataContext.PayGroups.ToList();
            cmbGroup.Store[0].DataSource = list;
            cmbGroup.Store[0].DataBind();

            int calculationId = 0;

            if (!string.IsNullOrEmpty(Request.QueryString["id"]))
            {
                calculationId = int.Parse(Request.QueryString["id"]);
                PayGroupCalculation calc = PeriodicPayManager.GetPayGroupCalculation(calculationId);
                hdnCalculationID.Text = calculationId.ToString();

                ExtControlHelper.ComboBoxSetSelected(calc.PayGroupRef_ID.ToString(), cmbGroup);

                 dispDays.Text = ((calc.EndDateEng.Value - calc.StartDateEng.Value).Days + 1) + " days";

                startDate1.Text = calc.StartDateEng.ToString();
                endDate1.Text = calc.EndDateEng.ToString();

                cmbGroup.Disable();


            }
            else
            {

                cmbGroup.Enable();


                //else if (list.Count == 1)
                //{
                //    groupId = list[0].PayGroupID;
                //}

                //ExtControlHelper.ComboBoxSetSelected(groupId, cmbGroup);

                //PayGroupCalculation lastPay = PeriodicPayManager.GetLastPay(groupId);
                //if (lastPay != null)
                //{
                //    dispDays.Text = ((lastPay.EndDateEng.Value - lastPay.StartDateEng.Value).Days + 1) + " days";

                //    hdnCalculationID.Text = lastPay.CalculationID.ToString();

                //    startDate1.Text = lastPay.StartDateEng.ToString();
                //    endDate1.Text = lastPay.EndDateEng.ToString();
                //}



                //if (groupId == 0)
                //{
                //    cmbGroup.Enable();
                //}
            }

            PagingToolbar1.DoRefresh();
        }


        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {

            int groupId = int.Parse(cmbGroup.SelectedItem.Value);
            DateTime start = startDate1.SelectedDate.Date;
            DateTime end = endDate1.SelectedDate.Date;




          

            int employeeId = -1;

            int.TryParse(cmbSearch.SelectedItem.Value, out employeeId);

            if (employeeId == 0)
                employeeId = -1;



            hdnSortBy.Text = (e.Sort[0].Property + " " + e.Sort[0].Direction).ToLower();

            List<GetPeriodicPayListResult> list = BLL.BaseBiz.PayrollDataContext.GetPeriodicPayList
                (int.Parse(hdnCalculationID.Text), e.Page - 1, e.Limit, hdnSortBy.Text, employeeId, groupId).ToList();


            if (list.Count > 0)
                e.Total = list[0].TotalRows.Value;
            else
                e.Total = 0;

            //if (list.Any())
            gridList.Store[0].DataSource = list;
            gridList.Store[0].DataBind();

        }
        public void btnChangePayDays_Click(object sender, DirectEventArgs e)
        {
            int ein = int.Parse(hdnEmployeeID.Text.Trim());
           
            double payDays = 0;

            double.TryParse(txtPayDays.Text.Trim(),out payDays);

            Status status = PeriodicPayManager.UpdatePayDays(ein, int.Parse(hdnCalculationID.Text), payDays);

            if (status.IsSuccess)
            {
                window.Hide();
                NewMessage.ShowNormalPopup("Days has been updated.");
                PagingToolbar1.DoRefresh();
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }

        }
        public void btnEditDays_Click(object sender, DirectEventArgs e)
        {
            window.Show();

            int ein = int.Parse(hdnEmployeeID.Text.Trim());
            PayGroupEmployee emp = PeriodicPayManager.GetPeriodEmployee(ein, int.Parse(hdnCalculationID.Text));
            lblEmployee.Text = EmployeeManager.GetEmployeeName(ein);
            if (emp != null)
            {
               
                lblPresentDays.Text = emp.PresentDays == null ? "0" : emp.PresentDays.ToString();
                txtPayDays.Text = emp.PayDays == null ? "0" : emp.PayDays.ToString();

            }
            else
            {
                lblPresentDays.Text = "0";
                txtPayDays.Text = "";
            }

        }

        public void cmbGroup_Select(object sender, DirectEventArgs e)
        {
            int groupId = int.Parse(cmbGroup.SelectedItem.Value);

            PayGroupCalculation lastPay = PeriodicPayManager.GetLastPay(groupId);
            if (lastPay != null)
            {
                dispDays.Text = ((lastPay.EndDateEng.Value - lastPay.StartDateEng.Value).Days + 1) + " days";

                hdnCalculationID.Text = lastPay.CalculationID.ToString();

                startDate1.Text = lastPay.StartDateEng.ToString();
                endDate1.Text = lastPay.EndDateEng.ToString();
            }
        }
        public void btnExport_Click(object sender, EventArgs e)
        {
            int groupId = int.Parse(cmbGroup.SelectedItem.Value);
            DateTime start = startDate1.SelectedDate.Date;
            DateTime end = endDate1.SelectedDate.Date;






            int employeeId = -1;

            int.TryParse(cmbSearch.SelectedItem.Value, out employeeId);

            if (employeeId == 0)
                employeeId = -1;



            
            List<GetPeriodicPayListResult> list = BLL.BaseBiz.PayrollDataContext.GetPeriodicPayList
                (int.Parse(hdnCalculationID.Text), 0, 99999, hdnSortBy.Text, employeeId, groupId).ToList();

            string title = string.Format("<h4> from {0} to {1}</h4>", start.ToString("dd MMM yyyy")
                , end.ToString("dd MMM yyyy")); ;

            Bll.ExcelHelper.ExportToExcel("Periodic Pay", list,
                new List<string> { "TotalRows", "LeaveStatusName", "LeaveRequestId", "ServiceStatusId", "DesignationId", "BranchId", "DepartmentId" },
            new List<String>() { },
            new Dictionary<string, string>() { { "PayDays", "Pay Days" }, { "GrossAmount", "Gross Amount" }, { "NetPay", "Net Pay" } },
            new List<string>() { "Rate", "GrossAmount", "TDS", "NetPay" }
            , new List<string> { "SN", "EmployeeId" }
            , new List<string> { "StartDate", "EndDate" }
            , new Dictionary<string, string>() { { "Periodic Pay ", title } }
            , new List<string> { "SN", "EIN", "Name", "Designation", "Branch", "Department",
                "PayDays","Rate","GrossAmount","TDS","NetPay" }); 


        }



        protected void btnMenuLockPay_Click(object sender, DirectEventArgs e)
        {
            PeriodicPayManager.LockLastPay(int.Parse(hdnCalculationID.Text));
            NewMessage.ShowNormalMessage("Current period has been locked, reload the page to process for new period.");
        }
        protected void btnGenerate_Click(object sender, DirectEventArgs e)
        {


            int groupId = int.Parse(cmbGroup.SelectedItem.Value);
            DateTime start = startDate1.SelectedDate.Date;
            DateTime end = endDate1.SelectedDate.Date;


            if (end < start)
            {
                NewMessage.ShowWarningMessage("End date must be greater than start date.");
                return;
            }

            PayGroupCalculation calculation = BLL.BaseBiz.PayrollDataContext.PayGroupCalculations.FirstOrDefault(x => x.CalculationID==int.Parse(hdnCalculationID.Text));
            if (calculation != null && calculation.IsFinalSaved != null && calculation.IsFinalSaved.Value)
            {
                NewMessage.ShowWarningMessage("This period has been already locked, reload the page to create another period.");
                return;
            }


            int employeeId = -1;

            int.TryParse(cmbSearch.SelectedItem.Value, out employeeId);

            if (employeeId == 0)
                employeeId = -1;


            BLL.BaseBiz.PayrollDataContext.GeneratePeriodicPay(
                BLL.BaseBiz.GetAppropriateDate(start), start, BLL.BaseBiz.GetAppropriateDate(end), end, groupId,
                SessionManager.CurrentLoggedInUserID,int.Parse(hdnCalculationID.Text));


            dispDays.Text = ((end - start).Days + 1) + " days";

            NewMessage.ShowNormalPopup("Pay has been generated.");
            PagingToolbar1.DoRefresh();

        }
        
        protected void btnApproveByLine_Click(object sender, DirectEventArgs e)
        {
           
        }

        protected void btnReject_Click(object sender, DirectEventArgs e)
        {
            
        }
    }
}
