﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using Utils.Helper;
using Utils.Web;
using BLL.Base;
using DAL;
using Utils.Base;
using Bll;
using Ext.Net;
using BLL.BO;

namespace Web
{


    public partial class VoucherListCtlGlobus : BaseUserControl
    {

       

        int payrollPeriodId;
        /// <summary>
        /// For Excel export to work
        /// </summary>
        /// <param name="control"></param>
        /// 

    


        void SetPeriod()
        {
            int month = 0;
            int year = 0;

            if (!string.IsNullOrEmpty(ddlPayrollFromMonth.SelectedValue))
            {


                month = int.Parse(ddlPayrollFromMonth.SelectedValue);
                year = int.Parse(ddlPayrollFromYear.SelectedValue);

                PayrollPeriod period = CommonManager.GetPayrollPeriod(month, year);
                if (period != null)
                    payrollPeriodId = period.PayrollPeriodId;
            }
        }

        protected void LoadAddOn()
        {

            System.Web.UI.WebControls.ListItem item = ddlAddOnName.SelectedItem;

            System.Web.UI.WebControls.ListItem firstIte = ddlAddOnName.Items[0];

            ddlAddOnName.DataSource = PayManager.GetAddOnForPayrollPeriod(payrollPeriodId);
            ddlAddOnName.DataBind();

            ddlAddOnName.Items.Insert(0, firstIte);

            bool selectedAddOnExists = false;

            if (item != null && item.Selected)
            {
                foreach (System.Web.UI.WebControls.ListItem selItem in ddlAddOnName.Items)
                {
                    if (selItem.Value == item.Value)
                    {
                        selItem.Selected = true;
                        selectedAddOnExists = true;
                    }
                }
            }

            if (selectedAddOnExists == false || item.Value == "-1")
            {
                List<TextValue> list = new List<TextValue>();
                ddlAddOnDateFilter.DataSource = list;
                ddlAddOnDateFilter.DataBind();
            }
            else
            {
                int addOnId = int.Parse(ddlAddOnName.SelectedValue);

                List<TextValue> list = PayManager.GetAddOnDateList(addOnId); ;
                list.Insert(0, new TextValue { Text = "--Select Date--", Value = "All" });

                System.Web.UI.WebControls.ListItem selectedAddOnDate = ddlAddOnDateFilter.SelectedItem;

                ddlAddOnDateFilter.DataSource = list;
                ddlAddOnDateFilter.DataBind();

                if (selectedAddOnDate != null && selectedAddOnDate.Selected)
                {
                    foreach (System.Web.UI.WebControls.ListItem selItem in ddlAddOnDateFilter.Items)
                    {
                        if (selItem.Value == selectedAddOnDate.Value)
                        {
                            selItem.Selected = true;
                        }
                    }
                }
            }
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
           

        }
        protected void Page_Load(object sender, EventArgs e)
        {

            if (this.Visible)
            {
                //btnExportDeduction.Visible = (CommonManager.CompanySetting.WhichCompany == WhichCompany.Century);

                if (!IsPostBack && !X.IsAjaxRequest)
                {

                    //PayrollPeriod payroll = CommonManager.GetLastPayrollPeriod();
                    //if (payroll != null)
                    //{
                    //    title.InnerHtml += payroll.Name;
                    //}

                    ddlCategory.DataSource = VouccherManager.GetVoucherGroupCategoryList();
                    ddlCategory.DataBind();
                    


                    CommonManager.SetFirstAndLastFinalSavedPayrollPeriod(ddlPayrollFromYear, ddlPayrollFromMonth);

                }
                SetPeriod();


                {

                    //BindEmployees();

                }

                LoadAddOn();
            }
        }

        public void btnLoadSelectedEmpVoucher_Click(object sender, EventArgs e)
        {
            window.Hide();

            int addOnId = -1;
            if (!string.IsNullOrEmpty(ddlAddOnName.SelectedValue) && ddlAddOnName.SelectedValue != "-1")
                addOnId = int.Parse(ddlAddOnName.SelectedValue);

            CheckboxSelectionModel selection = grid.GetSelectionModel() as CheckboxSelectionModel;
            string empIDList = "";
            foreach (var item in selection.SelectedRows)
            {
                if (empIDList == "")
                    empIDList = item.RecordID.ToString();
                else
                    empIDList += "," + item.RecordID.ToString();
            }

            hiddenSelectedEmp.Value = empIDList;

            if (hiddenAddOnId.Value != "" && hiddenAddOnId.Value != addOnId.ToString())            
            {
                hiddenSelectedEmp.Value = "";
            }

            hiddenAddOnId.Value = addOnId.ToString();

            BindEmployees();
        }

        public void btnClearSelection_Click(object sender, EventArgs e)
        {
            window.Hide();

            int addOnId = -1;
            if (!string.IsNullOrEmpty(ddlAddOnName.SelectedValue) && ddlAddOnName.SelectedValue != "-1")
                addOnId = int.Parse(ddlAddOnName.SelectedValue);

           

            hiddenSelectedEmp.Value = "";

            

            hiddenAddOnId.Value = addOnId.ToString();

            BindEmployees();
        }
        public void btnLoadEmployeeFilter_Click(object sender, EventArgs e)
        {
            int addOnId = -1;
            if (!string.IsNullOrEmpty(ddlAddOnName.SelectedValue) && ddlAddOnName.SelectedValue != "-1")
                addOnId = int.Parse(ddlAddOnName.SelectedValue);

            if (addOnId == -1)
            {
                NewMessage.ShowWarningMessage("Add on name should be selected for the filter.");
                return;
            }
            //CheckboxSelectionModel

            //if (hiddenAddOnId.Value == addOnId.ToString())
            //{
            //    window.Center();
            //    window.Show();
            //    return;
            //}
            //else
            //{
            //    hiddenSelectedEmp.Value = "";
            //}

            //hiddenAddOnId.Value = addOnId.ToString();

            grid.Store[0].DataSource = PartialAddOnManager.GetAddOnEmpList(addOnId);
            grid.Store[0].DataBind();

            // Select in grid previously selected employees

            RowSelectionModel sm = this.grid.GetSelectionModel() as RowSelectionModel;

            string[] idList = hiddenSelectedEmp.Value.Split(new char[] { ',' });

            sm.SelectedRows.Clear();
            foreach (string id in idList)
            {
                if (!string.IsNullOrEmpty(id))
                    sm.SelectedRows.Add(new SelectedRow(id));
            }

            

            BindEmployees();

            window.Center();
            window.Show();

        }


        protected void btnLoad_Click(object sender, EventArgs e)
        {
            BindEmployees();
        }

        //string value = dataSource.GetValue(header);
        void BindEmployees()
        {
            if (payrollPeriodId == 0)
                return;





            // reset selected add on id
            hiddenAddOnId.Value = "";

            bool? showRetiredOnly = null;
            if (ddlRetirementType.SelectedValue != "All")
                showRetiredOnly = bool.Parse(ddlRetirementType.SelectedValue);

            int addOnId = -1;
            if (!string.IsNullOrEmpty(ddlAddOnName.SelectedValue) && ddlAddOnName.SelectedValue != "-1")
                addOnId = int.Parse(ddlAddOnName.SelectedValue);

            int categoryId = -1;
            if (ddlCategory.SelectedValue != "-1")
                categoryId = int.Parse(ddlCategory.SelectedValue);

            if (categoryId == 1)
                categoryId = -1;

            //if (hiddenAddOnId.Value != addOnId.ToString())           
            //{
            //    hiddenSelectedEmp.Value = "";
            //}
            if (hiddenSelectedEmp.Value == "")
                msg.InnerHtml = "Voucher generated for all employees.";
            else
                msg.InnerHtml = "Voucher generated for " + (hiddenSelectedEmp.Value.Split(new char[] { ',' }).Length) + " employees only.";


            bool applyAddOndateFilter = false;
            DateTime? addOnDate = null;

            if (ddlAddOnDateFilter.SelectedIndex != 0)
            {
                applyAddOndateFilter = true;
                if (ddlAddOnDateFilter.SelectedItem != null && ddlAddOnDateFilter.SelectedItem.Text != "")
                    addOnDate = Convert.ToDateTime(ddlAddOnDateFilter.SelectedItem.Value);
            }




            if (categoryId == -1)
            {

                if (CommonManager.CompanySetting.WhichCompany == WhichCompany.Civil)
                {

                    gridGlobusCivil.DataSource = EmployeeManager.GetGlobusCIVILMainVoucherReport(payrollPeriodId, showRetiredOnly, categoryId,
                        addOnId, hiddenSelectedEmp.Value, applyAddOndateFilter, addOnDate);
                    gridGlobusCivil.DataBind();


                    gridGlobusCivil.Visible = true;
                    gridGlobusVoucher.Visible = false;
                    gridGlobusOtherVoucher.Visible = false;
                }
                else
                {


                    List<GetGlobusSalaryVoucherReportResult> list = new List<GetGlobusSalaryVoucherReportResult>();

                    if (addOnId != -1)
                    {


                        gridGlobusVoucher.DataSource = EmployeeManager.GetGlobusAddOnMainVoucher(payrollPeriodId, showRetiredOnly,
                            categoryId, addOnId, hiddenSelectedEmp.Value, applyAddOndateFilter, addOnDate);

                    }
                    else
                    {
                        list
                            = EmployeeManager.GetGlobusVoucherReport(payrollPeriodId, showRetiredOnly, categoryId);
                        gridGlobusVoucher.DataSource = list;

                    }

                    gridGlobusVoucher.DataBind();
                    gridGlobusVoucher.Visible = true;
                    gridGlobusOtherVoucher.Visible = false;
                }
            }
            else
            {
                List<GetGlobusVoucherOtherThanMainVoucherResult> list = new List<GetGlobusVoucherOtherThanMainVoucherResult>();

                gridGlobusOtherVoucher.DataSource = EmployeeManager.GetGlobusOtherVoucher
                    (payrollPeriodId, showRetiredOnly, categoryId, addOnId, hiddenSelectedEmp.Value, applyAddOndateFilter, addOnDate).ToList();
                     
                     //(payrollPeriodId, addOnId, hiddenSelectedEmp.Value, applyAddOndateFilter, addOnDate, categoryId);

                if (CommonManager.CompanySetting.WhichCompany == WhichCompany.MBL)
                    gridGlobusOtherVoucher.Columns[3].Visible = false;

                gridGlobusOtherVoucher.DataBind();
                gridGlobusVoucher.Visible = false;
                gridGlobusOtherVoucher.Visible = true;

            }





            if (CalculationManager.IsAllEmployeeSavedFinally(payrollPeriodId, false))
            {
                salaryNotSaved.Visible=false;
            }
            else
            {
                int totalEmployees = CalculationManager.GetTotalApplicableForSalary(payrollPeriodId, false);
                int initialSave = new CalculationManager().GetTotalEmployeeInitialSave(payrollPeriodId, false);

                if (totalEmployees != initialSave)
                    salaryNotSaved.Visible = true;
                else
                    salaryNotSaved.Visible = false;

            }


            // Warning if any Income/Deduction head is not mapped
            string headerName = CalculationManager.GetIncomeOrDeductionNotMappedForVoucher(payrollPeriodId);
            if (!string.IsNullOrEmpty(headerName))
            {
                spanWarningForIncomeDeductionHead.InnerHtml = "\"" + headerName + "\" is not mapped from Voucher Setting, if this income/deduction has amount then the voucher will not be correct.";
                spanWarningForIncomeDeductionHead.Visible = true;
            }
            else
                spanWarningForIncomeDeductionHead.Visible = false;

        }



       

        protected void ddlAddOnName_Change(object sender, EventArgs e)
        {
            int addOnId = int.Parse(ddlAddOnName.SelectedValue);

            List<TextValue> list = PayManager.GetAddOnDateList(addOnId); ;
            list.Insert(0, new TextValue { Text = "--Select Date--", Value = "All" });
            ddlAddOnDateFilter.DataSource = list;
            ddlAddOnDateFilter.DataBind();


        }

        protected void btnExport_Click(object sender, EventArgs e)
        {

            bool? showRetiredOnly = null;
            if (ddlRetirementType.SelectedValue != "All")
                showRetiredOnly = bool.Parse(ddlRetirementType.SelectedValue);

            int addOnId = -1;
            if (!string.IsNullOrEmpty(ddlAddOnName.SelectedValue) && ddlAddOnName.SelectedValue != "-1")
                addOnId = int.Parse(ddlAddOnName.SelectedValue);

            int categoryId = -1;
            if (ddlCategory.SelectedValue != "-1")
                categoryId = int.Parse(ddlCategory.SelectedValue);
            if (categoryId == 1)
                categoryId = -1;

            bool applyAddOndateFilter = false;
            DateTime? addOnDate = null;

            if (ddlAddOnDateFilter.SelectedIndex != 0)
            {
                applyAddOndateFilter = true;
                if (ddlAddOnDateFilter.SelectedItem != null && ddlAddOnDateFilter.SelectedItem.Text != "")
                    addOnDate = Convert.ToDateTime(ddlAddOnDateFilter.SelectedItem.Value);
            }

            if (categoryId == -1)
            {

                if (CommonManager.CompanySetting.WhichCompany == WhichCompany.Civil)
                {

                    List<GlobusCivilMainVoucher> list = EmployeeManager.GetGlobusCIVILMainVoucherReport(payrollPeriodId, showRetiredOnly, categoryId,
                        addOnId, hiddenSelectedEmp.Value, applyAddOndateFilter, addOnDate);

                    ExcelHelper.ExportToExcel<GlobusCivilMainVoucher>(
                             "VoucherList.xls",
                             list,
                             new List<string> { },
                             new List<string> { },
                             new Dictionary<string, string> { { "COMPANY", "COMPANY" }, { "DEBITACCTNO", "DEBIT.ACCT.NO" }, { "DEBITCURRENCY", "DEBIT.CURRENCY" }, 
                             { "DEBITAMOUNT", "DEBIT.AMOUNT" }, { "DEBITTHEIRREF", "DEBIT.THEIR.REF" }, { "ORDERINGCUST", "ORDERING.CUST" }, 
                             { "PAYMENTDETAILS", "PAYMENT.DETAILS" }
                             , { "CREDITACCTNO", "CREDIT.ACCT.NO" }, { "CREDITCURRENCY", "CREDIT.CURRENCY" }
                             , { "CREDITTHEIRREF", "CREDIT.THEIR.REF" }, { "PROFITCENTREDEPT", "PROFIT.CENTRE.DEPT" }}
                             , new List<string> { }
                             , new List<string> { "DEBITAMOUNT", "PROFITCENTREDEPT" }
                             , new List<string> { }
                              , new List<string> { }
                             , new Dictionary<string, string> { }
                             , new List<string> { }

                              );
                }
                else
                {

                    List<GetGlobusSalaryVoucherReportResult> list = new List<GetGlobusSalaryVoucherReportResult>();
                    if (addOnId != -1)
                    {



                        List<GetGlobusReportForMulitpleAddOnResult> globusList = EmployeeManager.GetGlobusAddOnMainVoucher(payrollPeriodId, showRetiredOnly,
                            categoryId, addOnId, hiddenSelectedEmp.Value, applyAddOndateFilter, addOnDate);

                        ExcelHelper.ExportToExcel<GetGlobusReportForMulitpleAddOnResult>(
                             "VoucherList.xls",
                             globusList,
                             new List<string> { "Class", "Location", "Entity", "Credit", "EmployeeName","Date" },
                             new List<string> { },
                             new Dictionary<string, string> { { "HeadOfficeCode", "Account" }, { "Department", "Branch" }, { "Account", "MainCode" },{"MainCode","Cr"}, { "TranCode", "Dr" }, { "CrCode", "MainCode" }, { "Debit", "Amount" }, { "Memo", "Desc1" } }
                             , new List<string> { }
                             , new Dictionary<string, string> { }
                             , new List<string> { "HeadOfficeCode","CrCode" , "TranCode", "Department", "Debit", "Date", "Desc1", "MainCode", }

                              );

                    }
                    else
                    {
                        list = EmployeeManager.GetGlobusVoucherReport(payrollPeriodId, showRetiredOnly, categoryId);



                        ExcelHelper.ExportToExcel<GetGlobusSalaryVoucherReportResult>(
                          "VoucherList.xls",
                          list,
                          new List<string> { "Class", "Location", "Entity", "Credit", "EmployeeName", "Date" },
                          new List<string> { },
                          new Dictionary<string, string> { { "HeadOfficeCode", "Account" }, { "Department", "Branch" },{"MainCode","Cr"}, { "Account", "Cr" }, { "TranCode", "Dr" }, { "CrCode", "MainCode" }, { "Debit", "Amount" }, { "Memo", "Desc1" } }
                          , new List<string> { }
                          , new Dictionary<string, string> { }
                          , new List<string> { "HeadOfficeCode", "CrCode", "TranCode", "Department", "Debit", "Date", "Desc1", "MainCode" }

                           );
                    }
                }
            }
            else
            {
                List<GetGlobusVoucherOtherThanMainVoucherResult> list = new List<GetGlobusVoucherOtherThanMainVoucherResult>();

                list = EmployeeManager.GetGlobusOtherVoucher
                    (payrollPeriodId, showRetiredOnly, categoryId, addOnId, hiddenSelectedEmp.Value, applyAddOndateFilter, addOnDate).ToList();

                List<string> hiddenList = new List<string>();

                if (CommonManager.CompanySetting.WhichCompany == WhichCompany.MBL)
                    hiddenList.Add("TransactionCode");

                ExcelHelper.ExportToExcel<GetGlobusVoucherOtherThanMainVoucherResult>(
                     "VoucherList.xls",
                     list,
                     hiddenList,

                     new List<string> { },
                             new Dictionary<string, string> { }
                             , new List<string> { }
                             , new List<string> { "Amount", "TransactionCode" }
                             , new List<string> { }
                              , new List<string> { }
                             , new Dictionary<string, string> { }
                             , new List<string> { }

                      );
            }


        }
        public void ExportToExcel()
        {
            


        }

        private void ClearControls(Control control)
        {
            for (int i = control.Controls.Count - 1; i >= 0; i--)
            {
                ClearControls(control.Controls[i]);
            }
            //
            // if(control is Table)

            if (!(control is TableCell))
            {
                if (control.GetType().GetProperty("SelectedItem") != null)
                {
                    LiteralControl literal = new LiteralControl();
                    control.Parent.Controls.Add(literal);
                    try
                    {
                        literal.Text =
                            (string)control.GetType().GetProperty("SelectedItem").
                                GetValue(control, null);
                    }
                    catch
                    { }
                    control.Parent.Controls.Remove(control);
                }
                else if (control.GetType().GetProperty("Text") != null)
                {
                    LiteralControl literal = new LiteralControl();
                    control.Parent.Controls.Add(literal);
                    literal.Text =
                        (string)control.GetType().GetProperty("Text").
                            GetValue(control, null);
                    control.Parent.Controls.Remove(control);
                }
            }
            return;
        }





        /// <summary>
        /// Replace any of the contained controls with literals
        /// </summary>
        /// <param name="control"></param>
        private void PrepareGridViewForExport(Control gv)
        {



            System.Web.UI.WebControls.LinkButton lb = new System.Web.UI.WebControls.LinkButton();

            Literal l = new Literal();

            string name = String.Empty;

            for (int i = 0; i < gv.Controls.Count; i++)
            {


                if (gv.Controls[i].GetType() == typeof(System.Web.UI.WebControls.LinkButton))
                {

                    l.Text = (gv.Controls[i] as System.Web.UI.WebControls.LinkButton).Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                else if (gv.Controls[i].GetType() == typeof(TextBox))
                {

                    l.Text = (gv.Controls[i] as TextBox).Text.ToString();

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);


                }

                if (gv.Controls[i].HasControls())
                {

                    PrepareGridViewForExport(gv.Controls[i]);

                }

            }
        }

        protected void isAddOn_CheckedChanged(object sender, EventArgs e)
        {
           
        }

    }

 

}
