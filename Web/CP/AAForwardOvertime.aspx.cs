﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;
using Utils.Calendar;
using Utils.Helper;

namespace Web.NewHR
{
    public partial class AAForwardOvertime : BasePage
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!X.IsAjaxRequest && !IsPostBack)
            {
                Initialise();


                JavascriptHelper.AttachNonDialogPopUpCode(Page, "positionHistoryPopup", "AAForwardOvertimePopup.aspx", 600, 625);
                JavascriptHelper.AttachNonDialogPopUpCode(Page, "assignovertimePopup", "OvertimeAssignPopup.aspx", 475, 550);

            }
        }

        public void Initialise()
        {

            if (OvertimeManager.IsRequestAutoGroupType)
            {
                ColumnStartTime.Hide();
                ColumnEndTime.Hide();
            }

            List<OvertimeType> list = OvertimeManager.GetOvertimeList();
            list.Insert(0, new OvertimeType { Name = "All", OvertimeTypeId = -1 });
            cmbType.Store[0].DataSource = list;
            cmbType.Store[0].DataBind();


            List<Branch> branchList = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            //branchList.Insert(0, new Branch { Name = "All", BranchId = -1 });
            cmbBranch.Store[0].DataSource = branchList;
            cmbBranch.Store[0].DataBind();

            PayrollPeriod period = CommonManager.GetLastPayrollPeriod();
            calPostedMonth.Text = period.EndDate;
        }


        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {


            int type = 1;
            int status = -1;

            status = tabPanel.ActiveTabIndex - 1;


            if (cmbPeriodType.SelectedItem != null && cmbPeriodType.SelectedItem.Value != null)
                type = int.Parse(cmbPeriodType.SelectedItem.Value);

            DateTime? start = null;
            DateTime? end = null;

            DateTime date;
            if (cmbPeriodType.SelectedItem.Value == "-1")
            {
                if (DateTime.TryParse(dateFrom.Text, out date))
                    start = dateFrom.SelectedDate;
                if (DateTime.TryParse(dateTo.Text, out date))
                    end = dateTo.SelectedDate;
            }

            if (start != null && start == DateTime.MinValue)
                start = null;

            if (end != null && end == DateTime.MinValue)
                end = null;

            int? totalRecords = 0;
            int BranchID = -1;

            if (cmbBranch.SelectedItem  != null && cmbBranch.SelectedItem.Value != null)
            {
                BranchID = int.Parse(cmbBranch.SelectedItem.Value);
            }


            int? postedPeriodId = null;
            if (!string.IsNullOrEmpty(calPostedMonth.Text))
            {
                CustomDate postedDate = CustomDate.GetCustomDateFromString(calPostedMonth.Text, IsEnglish);
                PayrollPeriod period = CommonManager.GetPayrollPeriod(postedDate.Month, postedDate.Year);
                if (period != null)
                    postedPeriodId = period.PayrollPeriodId;
            }

            if (e.Sort.Length > 0)
                hdnSortBy.Text = (e.Sort[0].Property + " " + e.Sort[0].Direction);//.ToLower();
            else
                hdnSortBy.Text = "";

            string ein = "";
            if (cmbSearch.SelectedItem != null && cmbSearch.SelectedItem.Value != null)
                ein = cmbSearch.SelectedItem.Value;

            gridList.Store[0].DataSource = OvertimeManager.GetOvertimeListForAdmin(type, status, start, end, ein
                , e.Page, e.Limit, ref totalRecords, BranchID
                , int.Parse(cmbType.SelectedItem.Value),hdnSortBy.Text,postedPeriodId);
            gridList.Store[0].DataBind();


            e.Total = totalRecords.Value;

            

            //List<GetOvertimeRequestForAdminResult> list = EmployeeManager.GetLeaveFareDetails
            //    ((period == null ? 0  : period.PayrollPeriodId),startDate,endDate, employeeId, e.Page - 1, e.Limit, (e.Sort[0].Property + " " + e.Sort[0].Direction).ToLower(), ref totalRecords
            //    , isApprovedOnly, paymentStatus);


            

            ////if (list.Any())
            //gridList.Store[0].DataSource = list;
            //gridList.Store[0].DataBind();

        }


        public void btnExport_Click(object sender, EventArgs e)
        {
            int type = 1;
            int status = -1;

            status = tabPanel.ActiveTabIndex - 1;


            if (cmbPeriodType.SelectedItem != null)
                type = int.Parse(cmbPeriodType.SelectedItem.Value);

            DateTime? start = null;
            DateTime? end = null;

            DateTime date;
            if (cmbPeriodType.SelectedItem.Value == "-1")
            {
                if (DateTime.TryParse(dateFrom.Text, out date))
                    start = dateFrom.SelectedDate;
                if (DateTime.TryParse(dateTo.Text, out date))
                    end = dateTo.SelectedDate;
            }

            if (start != null && start == DateTime.MinValue)
                start = null;

            if (end != null && end == DateTime.MinValue)
                end = null;

            int? totalRecords = 0;
            int BranchID = -1;

            if (cmbBranch.SelectedItem != null && cmbBranch.SelectedItem.Value != null)
            {
                BranchID = int.Parse(cmbBranch.SelectedItem.Value);
            }

            int? postedPeriodId = null;
            if (!string.IsNullOrEmpty(calPostedMonth.Text))
            {
                CustomDate postedDate = CustomDate.GetCustomDateFromString(calPostedMonth.Text, IsEnglish);
                PayrollPeriod period = CommonManager.GetPayrollPeriod(postedDate.Month, postedDate.Year);
                if (period != null)
                    postedPeriodId = period.PayrollPeriodId;
            }

            string ein = "";
            if (cmbSearch.SelectedItem != null && cmbSearch.SelectedItem.Value != null)
                ein = cmbSearch.SelectedItem.Value;

            List<GetOvertimeRequestForAdminResult> list = OvertimeManager.GetOvertimeListForAdmin(type, status, start, end, ein
                , 1, 9999999, ref totalRecords, BranchID
                , int.Parse(cmbType.SelectedItem.Value),hdnSortBy.Text,postedPeriodId);
            foreach (var item in list)
            {
                item.DateEngFormatted = Web.Helper.WebHelper.FormatDateForExcel(item.Date);
            }

            Bll.ExcelHelper.ExportToExcel("Overtime List", list,
                new List<String>() { "TotalRows", "RowNumber", "RequestID", "Duration", "SupervisorID", "Status",  "ApprovedOn", "Date" },
            new List<String>() { },
            new Dictionary<string, string>() { {"EmployeeID","EIN"}, {"EmployeeName","Employee"}, 
                    {"OvertimeType","Type"}, {"NepDate","Nep Date"}, { "StartTime", "Start" },
                    { "EndTime", "End" }, { "CheckInTime", "In Time" }, { "CheckOutTime", "Out Time" }, 
                    {"DurationModified" , "Requested Hr"},{"ApprovedTime","Approved Hr"}, {"DateEngFormatted", "Date"},
                    { "SupervisorName", "Proccessed By" }, { "StatusModified", "Status" } },
            new List<string>() { }
            , new List<string> { }
            , new List<string> { "DateEngFormatted" }
            , new Dictionary<string, string>() { }
            , new List<string> { "EmployeeID", "EmployeeName","OvertimeType", "DateEngFormatted", "NepDate", "StartTime", "EndTime", "CheckInTime", "CheckOutTime", "DurationModified","ApprovedTime", "Reason", "SupervisorName", "StatusModified" });

            

        }




        protected void btnPost_Click(object sender, DirectEventArgs e)
        {


            string gridItemsJson = e.ExtraParams["gridItems"];
            List<GetOvertimeRequestForAdminResult> list = JSON.Deserialize<List<GetOvertimeRequestForAdminResult>>(gridItemsJson);
            //List<OvertimePeriodDetail> _OvertimePeriodDetail = new List<OvertimePeriodDetail>();
            List<OvertimeRequest> requestList = new List<OvertimeRequest>();

            foreach (GetOvertimeRequestForAdminResult item in list)
            {
                OvertimeRequest request = new OvertimeRequest();
                request.OvertimeRequestID = item.RequestID;
                requestList.Add(request);
            }

            int count = 0;
            OvertimeManager.ForwardRequestInBulk(requestList, out count);

            CheckboxSelectionModel1.ClearSelection();
            NewMessage.ShowNormalMessage(count + " overtime has been forwarded.");
            PagingToolbar1.DoRefresh();


        }
        
        protected void btnApproveByLine_Click(object sender, DirectEventArgs e)
        {
           
        }

        protected void btnReject_Click(object sender, DirectEventArgs e)
        {
            string gridItemsJson = e.ExtraParams["gridItems"];
            List<GetOvertimeRequestForAdminResult> list = JSON.Deserialize<List<GetOvertimeRequestForAdminResult>>(gridItemsJson);
            //List<OvertimePeriodDetail> _OvertimePeriodDetail = new List<OvertimePeriodDetail>();
            List<OvertimeRequest> requestList = new List<OvertimeRequest>();

            foreach (GetOvertimeRequestForAdminResult item in list)
            {
                OvertimeRequest request = new OvertimeRequest();
                request.OvertimeRequestID = item.RequestID;
                requestList.Add(request);
            }

            int count = 0;
            OvertimeManager.RejectRequestInBulk(requestList, out count);

            CheckboxSelectionModel1.ClearSelection();
            NewMessage.ShowNormalMessage(count + " overtime has been rejected.");
            PagingToolbar1.DoRefresh();

        }
    }
}
