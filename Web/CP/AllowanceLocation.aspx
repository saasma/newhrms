﻿<%@ Page Title="Allowance Location" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="AllowanceLocation.aspx.cs" Inherits="Web.CP.AllowanceLocation" %>

<%--<%@ Register Src="~/NewHR/UserControls/EmployeeDetailsCtl.ascx" TagName="EmployeeDetailsCtl"
    TagPrefix="uc1" %>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        var CommandHandler1 = function(command, record){
            <%= hiddenValue.ClientID %>.setValue(record.data.LocationId);
                if(command=="Edit")
                {
                    <%= btnEditLevel.ClientID %>.fireEvent('click');
                }
                else
                {
                
                    <%= btnDeleteLevel.ClientID %>.fireEvent('click');
                }

             }

            function refreshWindow()
            {
                
                
            }

            function isPayrollSelected(btn) {
                if ($('#' + btn.id).prop('disabled'))
                    return false;
       

                var ret = allowancePopup("ID=" + <%=hiddenValue.ClientID %>.getValue());
                return false;
            }
             
    </script>
    <style type="text/css">
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Travel Allowance Location
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel" style="margin-top:0px !important; padding-top:0px !important; width:410px">
    
    <ext:Hidden runat="server" ID="hiddenValue" />
    <ext:Button Cls="btn btn-primary" runat="server" Hidden="true" ID="btnEditLevel" >
        <DirectEvents>
            <Click OnEvent="btnEditLevel_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>
    
    <ext:Button Cls="btn btn-primary" ID="btnDeleteLevel" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnDeleteLevel_Click">
                <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the Allowance Location?" />
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>
    <div class="separator bottom">
    </div>
    <div class="innerLR">
        <%--<uc1:EmployeeDetailsCtl Id="EmployeeDetailsCtl1" runat="server" />--%>
        <div class="widget">

            <div class="widget-body">
                <ext:GridPanel StyleSpec="margin-top:15px;" ID="GridLevels" runat="server" Cls="itemgrid">
                    <Store>
                        <ext:Store ID="Store3" runat="server">
                            <Model>
                                <ext:Model ID="Model4" runat="server" IDProperty="LocationId">
                                    <Fields>
                                        <ext:ModelField Name="LocationId" Type="String" />
                                        <ext:ModelField Name="LocationName" Type="string" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                    </Store>
                    <ColumnModel>
                        <Columns>
                            <ext:Column ID="Column5" Sortable="false" MenuDisabled="true" runat="server" Text="Location Name"
                                Width="300" Align="Left" DataIndex="LocationName" />

                            <ext:CommandColumn runat="server" Text="" Align="Left" Width="120">
                                <Commands>
                                    <ext:GridCommand Cls="editGridButton" ToolTip-Text="Edit"  Text="<i class='fa fa-pencil'></i>" CommandName="Edit" />
                                
                                    <ext:GridCommand Cls="editGridButton" ToolTip-Text="Delete"  Text="<i class='fa fa-trash-o'></i>"
                                        CommandName="Delete" />
                                  
                                </Commands>
                                <Listeners>
                                    <Command Handler="CommandHandler1(command,record);" />
                                </Listeners>
                            </ext:CommandColumn>
                        </Columns>
                    </ColumnModel>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                    </SelectionModel>
                </ext:GridPanel>
                <div class="buttonBlock">
                    <ext:Button Cls="btn btn-primary" runat="server"  ID="btnAddLevel" Text="<i></i>Add New Location" >
                        <DirectEvents>
                            <Click OnEvent="btnAddLevel_Click">
                                <EventMask ShowMask="true" />
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </div>
            </div>
        </div>
        <ext:Window ID="WindowLevel" runat="server" Title="Allowance Location" Icon="Application"
            Width="475" Height="225" BodyPadding="5" Hidden="true" Modal="true">
            <Content>
                <table class="fieldTable">
                    <tr>
                        <td>
                            <ext:TextField ID="txtName" FieldLabel="Name" runat="server" LabelAlign="Top" Width="350"
                                LabelSeparator="">
                            </ext:TextField>
                        </td>
                    </tr>

                    <tr>
                        <td valign="bottom" colspan="2">
                            <div class="popupButtonDiv">
                                <ext:Button Cls="btn btn-primary" runat="server" ID="btnLevelSaveUpdate" Text="<i></i>Save">
                                    <DirectEvents>
                                        <Click OnEvent="btnLevelSaveUpdate_Click">
                                            <EventMask ShowMask="true" />
                                        </Click>
                                    </DirectEvents>
                                    <Listeners>
                                        <Click Handler="valGroup = 'SaveUpdateLevel'; return CheckValidation();">
                                        </Click>
                                    </Listeners>
                                </ext:Button>
                                <div class="btnFlatOr">
                                    or</div>
                                <ext:LinkButton runat="server" StyleSpec="padding:0px;" Cls="btnFlatLeftGap" ID="LinkButton1"
                                    Text="<i></i>Cancel" runat="server">
                                    <Listeners>
                                        <Click Handler="#{WindowLevel}.hide();">
                                        </Click>
                                    </Listeners>
                                </ext:LinkButton>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtName"
                                    ValidationGroup="SaveUpdateLevel">
                                </asp:RequiredFieldValidator>
                            </div>
                        </td>
                    </tr>
                </table>
            </Content>
        </ext:Window>
    </div>
    </div>
    <br />
</asp:Content>
