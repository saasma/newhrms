﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Manager;
using DAL;
using Utils.Helper;
using Utils.Web;
using System.Text;
using BLL;
using System.Text.RegularExpressions;
using Ext.Net;

namespace Web.CP
{
    public partial class AllowanceForwardPopup : System.Web.UI.Page
    {
        InsuranceManager insMgr = new InsuranceManager();
        List<IInsuranceInstitution> source = new List<IInsuranceInstitution>();
        public bool IsDisplayedAsPopup
        {
            get
            {
                if (Request.QueryString["isPopup"] != null)
                    return true;
                return false;
            }
        }

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                Initialise();

        }

        void Initialise()
        {
            //ddlOvertimeType.DataSource = OvertimeManager.getAllowanceDetailList();
            //ddlOvertimeType.DataBind();

            ddlOvertimeType.DataSource = AllowanceManager.GetAllEveningCounterList();
            ddlOvertimeType.DataBind();

            Hidden_RequestID.Value = Request.QueryString["reqid"];
            Load();
        }

        public void btnHistory_Click(object sender, DirectEventArgs e)
        {
            int RequestID = int.Parse(Hidden_RequestID.Value);

            EveningCounterRequest request = AllowanceManager.getEveningCounterRequestByID(RequestID);

            grid.Store[0].DataSource = AllowanceManager.GetAllowanceHistoryForEmployee(request.EmployeeID.Value, request.CounterRequestID);
            grid.Store[0].DataBind();

            windowNew.Show();

        }

        new void Load()
        {
            string hr;
            string min;
            string hrmin;
            string employeename;


            EveningCounterRequest request = new EveningCounterRequest();
            request = AllowanceManager.getEveningCounterRequestByID(int.Parse(Hidden_RequestID.Value));
            //OvertimeManager.gete(Hidden_RequestID.Value);
            //min = (request.StartTime.Value.Subtract(request.EndTime.Value).TotalMinutes).ToString();
            //Hidden_OldMinute.Value = min;
            employeename = EmployeeManager.GetEmployeeById(request.EmployeeID.Value).Name;

            title.InnerHtml += " " + employeename;


            //lblName.Text = employeename;
            lblReason.Text = request.Reason;


            int? branchId = BLL.BaseBiz.PayrollDataContext.GetCurrentBranch(request.EmployeeID.Value, DateTime.Now.Date);
            if (branchId != null)
                lblBranch.Text = BranchManager.GetBranchById(branchId.Value).Name;

            int? desigId = BLL.BaseBiz.PayrollDataContext.GetEmployeeCurrentPositionForDate(DateTime.Now.Date, request.EmployeeID.Value);
            if (desigId != null)
            {
                EDesignation desig = CommonManager.GetDesigId(desigId.Value);
                lblPosition.Text = desig.LevelAndDesignation;
            }


            if (request.RecommenderForwardedBy != null)
                lblRecommendBy.Text = EmployeeManager.GetEmployeeName(request.RecommenderForwardedBy.Value) + ", ";

            if (request.RecommendedBy != null)
                lblRecommendBy.Text += EmployeeManager.GetEmployeeName(request.RecommendedBy.Value);

            if (request.ApprovalID != null)
                lblApprovedBy.Text = EmployeeManager.GetEmployeeName(request.ApprovalID.Value);

            EveningCounterType allowanceType = BLL.BaseBiz.PayrollDataContext.EveningCounterTypes.FirstOrDefault(x => x.EveningCounterTypeId
                    == request.EveningCounterTypeId);
                         

            if (CommonManager.CompanySetting.WhichCompany != WhichCompany.HPL && allowanceType.DoNowShowDaysWarning != true)
            {
                decimal? rate = BLL.BaseBiz.PayrollDataContext.GetAllowanceRate(request.EmployeeID, request.CounterRequestID);
                if (rate != null)
                    lblRate.Text = BLL.BaseBiz.GetCurrency(rate) + " / Unit";
            }

            if (request.EveningCounterTypeId != null)
            {
                UIHelper.SetSelectedInDropDown(ddlOvertimeType, request.EveningCounterTypeId.Value);
                EveningCounterType type1 = new EveningCounterType();
                type1 = AllowanceManager.GetEveningCounterType(request.EveningCounterTypeId.Value);
                
                if(type1!=null)
                Hidden_CounterTypePrev.Value = type1.Name;

                gridHistory.DataSource = AllowanceManager.GetAllowanceHistory(request.CounterRequestID, (int)RequestHistoryTypeEnum.EveningCounter);
                gridHistory.DataBind();
            }


            //if (request.Status != (int)EveningCounterStatusEnum.Approved)
            //{
            //    divWarningMsg.InnerHtml = "Allowance is in " +
            //        ((EveningCounterStatusEnum)request.Status) + " status, only Approved status allowance can be edited.";
            //    divWarningMsg.Hide = false;

            //    btnUpdate.Visible = false;
            //}


            txtFromDate.SelectedDate = request.StartDate.Value;
            txtToDate.SelectedDate = request.EndDate.Value;

            if (request.Days == null)
                txtDays.Text = "";
            else
                txtDays.Text = request.Days.ToString();

            //txtDays.Text = request.Days.ToString();

            Hidden_FromPrev.Value = txtFromDate.Text;
            Hidden_ToPrev.Value = txtToDate.Text;
            Hidden_DaysPrev.Value = txtDays.Text;


            Date_Change(null, null);

        }






        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "window.onunload = null;");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"), true);


            //output all as js array to be updatable in parent window
            //if (IsDisplayedAsPopup && source != null)
            //{
            //    //Page.ClientScript.
            //    StringBuilder str = new StringBuilder("");
            //    bool first = true;
            //    foreach (IInsuranceInstitution obj in source)
            //    {
            //        if (first == false)
            //            str.Append(",");
            //        str.Append("'" + obj.Id + "$$" + obj.InstitutionName + "'");
            //        first = false;
            //    }
            //    Page.ClientScript.RegisterArrayDeclaration("texts", str.ToString());
            //}
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            Page.Validate("SaveUpdate");
            if (Page.IsValid)
            {
                EveningCounterRequest requestInstance = new EveningCounterRequest();

                requestInstance.CounterRequestID = int.Parse(Hidden_RequestID.Value);
                requestInstance.EveningCounterTypeId = int.Parse(ddlOvertimeType.SelectedValue);
                requestInstance.StartDate = txtFromDate.SelectedDate;
                requestInstance.EndDate = txtToDate.SelectedDate;
                requestInstance.Days = double.Parse(txtDays.Text.Trim());


                if (txtFromDate.SelectedDate == DateTime.MinValue)
                {
                    NewMessage.ShowWarningMessage("Invalid from date.");
                    return;
                }

                if (txtFromDate.SelectedDate != null)
                {
                    requestInstance.StartDate = txtFromDate.SelectedDate;

                }
                else
                {
                    NewMessage.ShowWarningMessage("From Date is required");
                    return;
                }

                if (txtToDate.SelectedDate != null)
                {
                    requestInstance.EndDate = txtToDate.SelectedDate;
                }
                else
                {
                    NewMessage.ShowWarningMessage("To Date is required");
                    return;
                }


                if (ddlOvertimeType.SelectedItem != null )
                    requestInstance.EveningCounterTypeId = int.Parse(ddlOvertimeType.SelectedItem.Value);
                else
                {
                    NewMessage.ShowWarningMessage("Allowance Type is required");
                    return;
                }

              


                string daysMathStr = ((txtToDate.SelectedDate.Date - txtFromDate.SelectedDate.Date).TotalDays + 1).ToString();

                string daysUserStr = txtDays.Text;
                float daysUser;
                float daysMath;
                bool isValidDaysMath = float.TryParse(daysMathStr, out daysMath);
                bool isValidDaysUser = float.TryParse(daysUserStr, out daysUser);

                List<RequestEditHistory> edits = new List<RequestEditHistory>();
                RequestEditHistory edit = new RequestEditHistory();

                if (Hidden_FromPrev.Value.Trim() != txtFromDate.Text.Trim())
                {
                    edit = new RequestEditHistory();
                    edit.Column1Before = Hidden_FromPrev.Value;
                    edit.Column1After = txtFromDate.Text;
                    edits.Add(edit);
                    Hidden_FromPrev.Value = txtFromDate.Text;
                }
                if (Hidden_ToPrev.Value.Trim() != txtToDate.Text.Trim())
                {
                    edit = new RequestEditHistory();
                    edit.Column1Before = Hidden_ToPrev.Value;
                    edit.Column1After = txtToDate.Text;
                    edits.Add(edit);
                    Hidden_ToPrev.Value = txtToDate.Text;
                }

                if (Hidden_DaysPrev.Value.Trim() != txtDays.Text.Trim())
                {
                    edit = new RequestEditHistory();
                    edit.Column1Before = Hidden_DaysPrev.Value;
                    edit.Column1After = txtDays.Text;
                    edits.Add(edit);
                    Hidden_DaysPrev.Value = txtDays.Text;
                }

                if (Hidden_CounterTypePrev.Value.Trim() != ddlOvertimeType.SelectedItem.Text.Trim())
                {
                    edit = new RequestEditHistory();
                    edit.Column1Before = Hidden_DaysPrev.Value;
                    edit.Column1After = ddlOvertimeType.SelectedItem.Text;
                    edits.Add(edit);
                    Hidden_DaysPrev.Value = ddlOvertimeType.SelectedItem.Text;
                }

                EveningCounterType allowanceType = BLL.BaseBiz.PayrollDataContext.EveningCounterTypes.FirstOrDefault(x => x.EveningCounterTypeId
                    == requestInstance.EveningCounterTypeId);

                if (CommonManager.CompanySetting.WhichCompany != WhichCompany.HPL && allowanceType.DoNowShowDaysWarning != true)
                {
                    if (isValidDaysUser && isValidDaysMath)
                    {
                        if (daysUser > daysMath)
                        {
                            NewMessage.ShowWarningMessage("Number of days between start and end date is Higher than Expected");
                            return;
                        }
                        else
                        {
                            requestInstance.Days = daysUser;
                        }
                    }
                    else
                    {
                        NewMessage.ShowWarningMessage("Invalid Days Input");
                        return;
                    }
                }
                else
                    requestInstance.Days = daysUser;

                if (requestInstance.StartDate > requestInstance.EndDate)
                {
                    NewMessage.ShowWarningMessage("From date should not be greater then To date.");
                    return;
                }

                Status status = OvertimeManager.UpdateRequestFromHR(requestInstance, edits);
                if (status.IsSuccess)
                {
                    Initialise();

                    this.HasImport = true;
                    NewMessage.ShowNormalMessage("Allowance has been updated.", "closePopup();\n");
                    //JavascriptHelper.DisplayClientMsg(
                }

                else
                {
                    divWarningMsg.InnerHtml = status.ErrorMessage;
                    divWarningMsg.Hide = false;
                }
            }
            else
            {
                JavascriptHelper.DisplayClientMsg("Validation Failed", this);
            }
        }

        protected void btnReject_Click(object sender, EventArgs e)
        {
            Page.Validate("SaveUpdate");
            if (Page.IsValid)
            {
                EveningCounterRequest requestInstance = new EveningCounterRequest();


                requestInstance.CounterRequestID = int.Parse(Hidden_RequestID.Value);


                Status status = OvertimeManager.RejectAllowanceRequestFromHR(requestInstance);

                if (status.IsSuccess)
                {
                    this.HasImport = true;
                    JavascriptHelper.DisplayClientMsg("Allowance has been rejected.", this, "closePopup();\n");
                }

                else
                {
                    divWarningMsg.InnerHtml = status.ErrorMessage;
                    divWarningMsg.Hide = false;
                }


            }
            else
            {
                JavascriptHelper.DisplayClientMsg("Validation Failed", this);
            }
        }

        protected void Date_Change(object sender, DirectEventArgs e)
        {
            DateTime TodayDate = new DateTime();
            TodayDate = CommonManager.GetCurrentDateAndTime();


            string FromDateNep;
            string EndDateNep;




            if (txtFromDate.SelectedDate != null &&
                txtFromDate.SelectedDate.Date > TodayDate.AddYears(-25).Date && txtFromDate.SelectedDate.Date < TodayDate.AddYears(25).Date)
            {
                FromDateNep = DateManager.GetAppropriateDate(txtFromDate.SelectedDate);
                txtFromDateNep.Text = FromDateNep;
            }
            else
            {
                txtFromDateNep.Text = "";
                txtFromDate.SelectedDate = CommonManager.GetCurrentDateAndTime();
            }



            if (txtToDate.SelectedDate != null &&
                txtToDate.SelectedDate.Date > TodayDate.AddYears(-25).Date && txtToDate.SelectedDate.Date < TodayDate.AddYears(25).Date)
            {
                EndDateNep = DateManager.GetAppropriateDate(txtToDate.SelectedDate);
                txtToDateNep.Text = EndDateNep;

            }
            else
            {
                txtToDateNep.Text = "";
                txtToDate.SelectedDate = CommonManager.GetCurrentDateAndTime();
            }




            //if (txtFromDate.SelectedDate != null && txtToDate.SelectedDate != null &&
            //    txtFromDate.SelectedDate.Date > TodayDate.AddYears(-25).Date && txtFromDate.SelectedDate.Date < TodayDate.AddYears(25).Date &&
            //    txtToDate.SelectedDate.Date > TodayDate.AddYears(-25).Date && txtToDate.SelectedDate.Date < TodayDate.AddYears(25).Date)
            //{
            //    if (txtFromDate.SelectedDate.Date > txtToDate.SelectedDate.Date)
            //    {
            //        txtToDate.SelectedDate = txtFromDate.SelectedDate.Date;
            //    }
            //    //if (txtFromDate.SelectedDate.Date != null && txtToDate.SelectedDate.Date != null)
            //    //{
            //    //    string days = ((txtToDate.SelectedDate.Date - txtFromDate.SelectedDate.Date).TotalDays + 1).ToString();
            //    //    txtDays.Text = Math.Round(decimal.Parse(days), MidpointRounding.ToEven).ToString();
            //    //}
            //}



        }


    }
}
