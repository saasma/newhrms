<%@ Page Title="Family List" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="FamilyList.aspx.cs" Inherits="Web.FamilyList" %>

<%@ Import Namespace="BLL.Manager" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Scripts/ScrollTableHeader103.min.js?v=200" type="text/javascript"></script>
    <script type="text/javascript">

        var skipLoadingCheck = true;

        $(document).ready(
            function () {

                $('.tableLightColor tbody').attr('id', 'scrollMe');
                var sth = new ScrollTableHeader();

                sth.addTbody("scrollMe");
                sth.delayAfterScroll = 150;
                sth.minTableRows = 10;
            }
        );

            function ACE_item_selected(source, eventArgs) {
                var value = eventArgs.get_value();

                document.getElementById('<%= hiddenEmployeeID.ClientID %>').value = value;
            }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Family list
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <table class="fieldTable">
            <tr>
                <td>
                    <strong>Employee</strong>
                </td>
                 <td>
                 <asp:HiddenField ID="hiddenEmployeeID" runat="server" />
                    <asp:TextBox ID="txtEmpSearchText" Width="170px" Style='width: 200px; border-radius: 2px;
                        margin-top: 4px; margin-left: 0px; height: 25px;' runat="server"></asp:TextBox>
                    <cc1:TextBoxWatermarkExtender ID="TBWE2" runat="server" TargetControlID="txtEmpSearchText"
                        WatermarkText="Employee Search" WatermarkCssClass="searchBoxText" />
                    <cc1:AutoCompleteExtender EnableCaching="true" ID="AutoCompleteExtenderOrganization"  OnClientItemSelected="ACE_item_selected"
                        runat="server" MinimumPrefixLength="2" ServiceMethod="GetEmployeeNames" ServicePath="~/PayrollService.asmx"
                        TargetControlID="txtEmpSearchText" CompletionSetCount="10" CompletionInterval="250"
                        CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList"
                        CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
                    </cc1:AutoCompleteExtender>
                </td>
                <td  valign="bottom">
                    <asp:Button ID="btnLoads" CssClass="btn btn-default btn-sect btn-sm" Style="width: 80px"
                        OnClick="btnLoad_Click" runat="server" Text="Load"></asp:Button>
                </td>
                <td valign="bottom">
                        <asp:Button ID="btnUpdate" CssClass="excel" runat="server" Text="Export" OnClick="btnUpdate_Click" />
                </td>
            </tr>
        </table>
        <br />
        <div class="clear gridBlock">
            <cc2:EmptyDisplayGridView CssClass="table table-primary mb30 table-bordered table-hover"
                UseAccessibleHeader="true" ShowHeaderWhenEmpty="True" ID="gvEmployeeIncome" runat="server"
                DataKeyNames="EmployeeId" AutoGenerateColumns="False" CellPadding="4" GridLines="None"
                AllowSorting="True" ShowFooterWhenEmpty="False">
                <Columns>
                    <asp:BoundField DataField="SN" HeaderText="SN"></asp:BoundField>
                    <asp:BoundField DataField="EmployeeId" HeaderText="EIN"></asp:BoundField>
                    <asp:TemplateField HeaderText="Name" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:HyperLink runat="server" Width="140" Text='<%# Eval("Name") %>'  NavigateUrl='<%# "~/newhr/Family.aspx?ID=" + Eval("EmployeeId") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Family1" Visible="false"  HeaderStyle-HorizontalAlign="Left" HeaderText="">
                    </asp:BoundField>
                    <asp:BoundField DataField="Family2" Visible="false"  HeaderStyle-HorizontalAlign="Left" HeaderText="">
                    </asp:BoundField>
                    <asp:BoundField DataField="Family3" Visible="false"  HeaderStyle-HorizontalAlign="Left" HeaderText="">
                    </asp:BoundField>
                    <asp:BoundField DataField="Family4" Visible="false"  HeaderStyle-HorizontalAlign="Left" HeaderText="">
                    </asp:BoundField>
                    <asp:BoundField DataField="Family5" Visible="false"  HeaderStyle-HorizontalAlign="Left" HeaderText="">
                    </asp:BoundField>
                    <asp:BoundField DataField="Family6" Visible="false"  HeaderStyle-HorizontalAlign="Left" HeaderText="">
                    </asp:BoundField>
                    <asp:BoundField DataField="Family7" Visible="false"  HeaderStyle-HorizontalAlign="Left" HeaderText="">
                    </asp:BoundField>
                    <asp:BoundField DataField="Family8" Visible="false"  HeaderStyle-HorizontalAlign="Left" HeaderText="">
                    </asp:BoundField>
                    <asp:BoundField DataField="Family9" Visible="false"  HeaderStyle-HorizontalAlign="Left" HeaderText="">
                    </asp:BoundField>
                    <asp:BoundField DataField="Family10" Visible="false"  HeaderStyle-HorizontalAlign="Left" HeaderText="">
                    </asp:BoundField>
                    <asp:BoundField DataField="Family11" Visible="false"  HeaderStyle-HorizontalAlign="Left" HeaderText="">
                    </asp:BoundField>
                    <asp:BoundField DataField="Family12" Visible="false"  HeaderStyle-HorizontalAlign="Left" HeaderText="">
                    </asp:BoundField>
                     <asp:BoundField DataField="Family13" Visible="false"  HeaderStyle-HorizontalAlign="Left" HeaderText="">
                    </asp:BoundField>
                     <asp:BoundField DataField="Family14" Visible="false"  HeaderStyle-HorizontalAlign="Left" HeaderText="">
                    </asp:BoundField>
                     <asp:BoundField DataField="Family15" Visible="false"  HeaderStyle-HorizontalAlign="Left" HeaderText="">
                    </asp:BoundField>
                     <asp:BoundField DataField="Family16" Visible="false"  HeaderStyle-HorizontalAlign="Left" HeaderText="">
                    </asp:BoundField>
                     <asp:BoundField DataField="Family17" Visible="false"  HeaderStyle-HorizontalAlign="Left" HeaderText="">
                    </asp:BoundField>
                     <asp:BoundField DataField="Family18" Visible="false"  HeaderStyle-HorizontalAlign="Left" HeaderText="">
                    </asp:BoundField>
                     <asp:BoundField DataField="Family19" Visible="false"  HeaderStyle-HorizontalAlign="Left" HeaderText="">
                    </asp:BoundField>
                     <asp:BoundField DataField="Family20" Visible="false"  HeaderStyle-HorizontalAlign="Left" HeaderText="">
                    </asp:BoundField>
                     <asp:BoundField DataField="Family21" Visible="false"  HeaderStyle-HorizontalAlign="Left" HeaderText="">
                    </asp:BoundField>
                     <asp:BoundField DataField="Family22" Visible="false"  HeaderStyle-HorizontalAlign="Left" HeaderText="">
                    </asp:BoundField>
                     <asp:BoundField DataField="Family23" Visible="false"  HeaderStyle-HorizontalAlign="Left" HeaderText="">
                    </asp:BoundField>
                     <asp:BoundField DataField="Family24" Visible="false"  HeaderStyle-HorizontalAlign="Left" HeaderText="">
                    </asp:BoundField>
                     <asp:BoundField DataField="Family25" Visible="false"  HeaderStyle-HorizontalAlign="Left" HeaderText="">
                    </asp:BoundField>
                </Columns>
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                <EmptyDataTemplate>
                    <b>No employee list. </b>
                </EmptyDataTemplate>
            </cc2:EmptyDisplayGridView>
        </div>
        <div class="buttonsDiv">
        
        </div>
    </div>
</asp:Content>
