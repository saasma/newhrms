﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using BLL;
using DAL;
using Utils.Web;
using Utils.Helper;
using System.Text;
using Utils.Calendar;

namespace Web.CP
{



    public partial class LeaveFareSetting : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.AdministratorTools;
            }
        }

        CompanyManager compMgr = new CompanyManager();



        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                Initialise();          

        }

        

        public void Initialise()
        {

            ddlIncome.DataSource = PayManager.GetFestivalIncomeList(SessionManager.CurrentCompanyId);
            ddlIncome.DataBind();

            ddlLeaves.DataSource = LeaveAttendanceManager.GetLeaveListByCompany(SessionManager.CurrentCompanyId);
            ddlLeaves.DataBind();

            

            //if attendancce already saved for the company then hide this button
            Setting setting = OvertimeManager.GetSetting();
            if (setting != null && setting.LeaveFareIncomeId != null)
            {
                UIHelper.SetSelectedInDropDown(ddlIncome, setting.LeaveFareIncomeId);
            }
            if (setting != null && setting.LeaveFareLeaveTypeId != null)
            {
                UIHelper.SetSelectedInDropDown(ddlLeaves, setting.LeaveFareLeaveTypeId);
            }

            if (setting != null && setting.LeaveFareAllowProportionate != null)
                UIHelper.SetSelectedInDropDown(ddlAllowProportionateCalculation, setting.LeaveFareAllowProportionate.ToString().ToLower());

            if (setting != null && setting.LeaveFareFullDays != null)
                txtFullDays.Text = setting.LeaveFareFullDays.ToString();

            if (setting != null && setting.LeaveFareProportionateDays != null)
                txtProportionateValue.Text = setting.LeaveFareProportionateDays.ToString();
            
        }


        protected void btnEdit1_Click(object sender, EventArgs e)
        {
            ddlIncome.Enabled = true;
        }
        protected void btnEdit2_Click(object sender, EventArgs e)
        {
            ddlLeaves.Enabled = true;
        }
        protected void btnEdit3_Click(object sender, EventArgs e)
        {
            txtFullDays.Enabled = true;
        }
        protected void btnEdit4_Click(object sender, EventArgs e)
        {
            ddlAllowProportionateCalculation.Enabled = true;
        }
        protected void btnEdit5_Click(object sender, EventArgs e)
        {
            txtProportionateValue.Enabled = true;
        }
       
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {

                Setting setting = CommonManager.Setting;

                setting.LeaveFareAllowProportionate = bool.Parse(ddlAllowProportionateCalculation.SelectedValue);
                if(string.IsNullOrEmpty(txtFullDays.Text.Trim()))
                    setting.LeaveFareFullDays = 0;
                else
                    setting.LeaveFareFullDays = int.Parse(txtFullDays.Text.Trim());
                setting.LeaveFareIncomeId = int.Parse(ddlIncome.SelectedValue);
                setting.LeaveFareLeaveTypeId = int.Parse(ddlLeaves.SelectedValue);

                if (string.IsNullOrEmpty(txtProportionateValue.Text.Trim()))
                    setting.LeaveFareProportionateDays = 0;
                else
                    setting.LeaveFareProportionateDays = int.Parse(txtProportionateValue.Text);


                OvertimeManager.UpdateLeaveFareSetting(setting);

                divMsgCtl.InnerHtml = "Setting saved.";
                divMsgCtl.Hide = false;
                //MyCache.Reset();
            }
            else
            {
                divWarningMsg.InnerHtml = "All settings must be selected for successfull save.";
                divWarningMsg.Hide = false;
            }
        }

       
    }

}

