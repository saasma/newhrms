﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;
using Utils.Calendar;
using Utils.Helper;

namespace Web.NewHR
{
    public partial class HolidayPayDetails : BasePage
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!X.IsAjaxRequest && !IsPostBack)
            {
                Initialise();

              

            }
        }

        public void Initialise()
        {
            PayrollPeriod period = CommonManager.GetLastPayrollPeriod();

            //calDate.Text = CustomDate.GetTodayDate(IsEnglish).ToString();
            cmbYear.Store[0].DataSource = CommonManager.GetYearList();
            cmbYear.Store[0].DataBind();

            ExtControlHelper.ComboBoxSetSelected(period.Year.ToString(), cmbYear);
            ExtControlHelper.ComboBoxSetSelected(period.Month.ToString(), cmbMonth);

            cmbMonth.Store[0].DataSource = DateManager.GetCurrentMonthList();
            cmbMonth.Store[0].DataBind();

            PagingToolbar1.DoRefresh();
        }


        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
           

            int year = int.Parse(cmbYear.SelectedItem.Value);
            int month = int.Parse(cmbMonth.SelectedItem.Value);

            DateTime startDate = DateTime.Now, endDate = DateTime.Now;

            PayrollPeriod period = CommonManager.GetPayrollPeriod(month, year);


            if (period == null)
            {
                // 
                if (IsEnglish == false)
                {
                    CustomDate start = new CustomDate(1, month, year, false);
                    CustomDate end = new CustomDate(DateHelper.GetTotalDaysInTheMonth(year, month, false), month, year, false);
                    startDate = start.EnglishDate;
                    endDate = end.EnglishDate;
                }
                else
                {

                }
                //NewMessage.ShowWarningMessage("Period does not exists for this date.");
            }
            else
            {
                startDate = period.StartDateEng.Value;
                endDate = period.EndDateEng.Value;
            }


            title.Html = string.Format("<h4> Holiday Pay Details for {0} to {1}, {2}</h4>", startDate.ToString("dd MMM yyyy")
                , endDate.ToString("dd MMM yyyy"), period == null? "" : period.Name);

            int totalRecords = 0;


            bool isApprovedOnly = tabPanel.ActiveTabIndex != 0;

            hdnPeriodId.Text = (period == null ? "0" : period.PayrollPeriodId.ToString());
            hdnIsApprovedOnly.Text = isApprovedOnly.ToString();

            if (period != null && period.PayrollPeriodId == CommonManager.GetLastPayrollPeriod().PayrollPeriodId
                && isApprovedOnly)
            {
                btnPost.Show();
            }
            else
            {
                btnPost.Hide();
            }

            if (period != null && period.PayrollPeriodId == CommonManager.GetLastPayrollPeriod().PayrollPeriodId
                && !isApprovedOnly)
            {
                btnApprove.Show();
            }
            else
            {
                btnApprove.Hide();
            }

            int paymentStatus = int.Parse(cmbStatus.SelectedItem.Value);

            int employeeId = -1;

            int.TryParse(cmbSearch.SelectedItem.Value, out employeeId);

            if (employeeId == 0)
                employeeId = -1;



            hdnSortBy.Text = (e.Sort[0].Property + " " + e.Sort[0].Direction).ToLower();

            List<GetHolidayAdditionalPayListResult> list = EmployeeManager.GetHolidayAdditionalPayDetails
                ((period == null ? 0  : period.PayrollPeriodId),startDate,endDate, employeeId, e.Page - 1, e.Limit, (e.Sort[0].Property + " " + e.Sort[0].Direction).ToLower(), ref totalRecords
                , isApprovedOnly, paymentStatus);


            e.Total = totalRecords;

            //if (list.Any())
            gridList.Store[0].DataSource = list;
            gridList.Store[0].DataBind();

        }


        public void btnExport_Click(object sender, EventArgs e)
        {
            int year = int.Parse(cmbYear.SelectedItem.Value);
            int month = int.Parse(cmbMonth.SelectedItem.Value);

            DateTime startDate = DateTime.Now, endDate = DateTime.Now;

            PayrollPeriod period = CommonManager.GetPayrollPeriod(month, year);

            int employeeId = -1;

            int.TryParse(cmbSearch.SelectedItem.Value, out employeeId);

            if (employeeId == 0)
                employeeId = -1;

            if (period == null)
            {
                // 
                if (IsEnglish == false)
                {
                    CustomDate start = new CustomDate(1, month, year, false);
                    CustomDate end = new CustomDate(DateHelper.GetTotalDaysInTheMonth(year, month, false), month, year, false);
                    startDate = start.EnglishDate;
                    endDate = end.EnglishDate;
                }
                else
                {

                }
                //NewMessage.ShowWarningMessage("Period does not exists for this date.");
            }
            else
            {
                startDate = period.StartDateEng.Value;
                endDate = period.EndDateEng.Value;
            }


            string title = string.Format("<h4> Holiday Pay Details for {0} to {1}, {2}</h4>", startDate.ToString("dd MMM yyyy")
                , endDate.ToString("dd MMM yyyy"), period == null ? "" : period.Name);

            int totalRecords = 0;


            bool isApprovedOnly = tabPanel.ActiveTabIndex != 0;




            List<GetHolidayAdditionalPayListResult> list = EmployeeManager.GetHolidayAdditionalPayDetails
                ((period == null ? 0 : period.PayrollPeriodId), startDate, endDate, employeeId, 0, 99999999, "", ref totalRecords
                , isApprovedOnly, 0);


            string titleText = string.Format("<h4> For {0} to {1}, {2}</h4>", startDate.ToString("dd MMM yyyy")
                , endDate.ToString("dd MMM yyyy"), period == null ? "" : period.Name); ;

            Bll.ExcelHelper.ExportToExcel("Holiday pay details", list,
                new List<string> { "TotalRows", "ClockIn", "ClockOut", "WorkedMin", "DesignationId","DayValueText","ServiceStatusId","Date", "BranchId", "DepartmentId" },
            new List<String>() { },
            new Dictionary<string, string>() { { "EmployeeId", "EIN" }, { "DateEng", "Date" }, { "DayValueTextAdded", "Present Day" },
            {"ClockInText","Clock In"},{"ClockOutText","Clock Out"},{"WorkedMinText","Worked Hrs"},{"SalaryType","Additional Salary"}},
            new List<string>() { "Amount", "BasicAmount" }
            , new List<string> { "SN","EmployeeId"}
            , new List<string> {  }
            , new Dictionary<string, string>() { { "Holiday pay details ", titleText } }
            , new List<string> { "SN", "EmployeeId", "Name", "Designation", "Branch", "Department", "ServiceStatus", 
                 "DateEng","DayValueTextAdded","ClockInText","ClockOutText","WorkedMinText","SalaryType","BasicAmount","Amount" }); 


        }



        protected void btnApprove_Click(object sender, DirectEventArgs e)
        {


            string gridItemsJson = e.ExtraParams["gridItems"];
            List<GetHolidayAdditionalPayListResult> list = JSON.Deserialize<List<GetHolidayAdditionalPayListResult>>(gridItemsJson);
            //List<OvertimePeriodDetail> _OvertimePeriodDetail = new List<OvertimePeriodDetail>();
            if (list.Count == 0)
            {
                NewMessage.ShowWarningMessage("Select list for approval.");
                return;
            }



            Status status = OvertimeManager.SaveUpdateHolidayAdditionalPay(list, int.Parse(hdnPeriodId.Text));
            if (status.IsSuccess)
            {
                CheckboxSelectionModel1.ClearSelection();
                NewMessage.ShowNormalMessage("Pay has been approved successfully.");
                PagingToolbar1.DoRefresh();
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }

        }

        protected void btnPost_Click(object sender, DirectEventArgs e)
        {



            Status status = OvertimeManager.PostHolidayAdditionalPayToSalary(int.Parse(hdnPeriodId.Text));
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Overtime posted successfully.");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }

        }
        
        protected void btnApproveByLine_Click(object sender, DirectEventArgs e)
        {
           
        }

        protected void btnReject_Click(object sender, DirectEventArgs e)
        {
            
        }
    }
}
