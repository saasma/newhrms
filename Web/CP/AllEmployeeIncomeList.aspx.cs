﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using DAL;
using BLL;
using Utils.Helper;
using Utils;

namespace Web.User
{
    public partial class AllEmployeeIncomeList : BasePage
    {
        PayManager mgr = new PayManager();
       
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialise();
            }
            JavascriptHelper.AttachPopUpCode(Page, "popupIncome", "IncomeList.aspx", 480, 450);
            JavascriptHelper.AttachPopUpCode(Page, "popupUpdateIncome", "AEIncome.aspx", 600, 700);

            JavascriptHelper.AttachPopUpCode(Page, "popupDeduction", "DeductionList.aspx", 470, 440);
            JavascriptHelper.AttachPopUpCode(Page, "popupUpdateDeduction", "AEDeduction.aspx", 600, 600);
        }

        public string GetIncomeHTML(int employeeId)
        {
           return mgr.GetHTMLEmployeeIncomeList(employeeId);
        }
        public string GetDeductionHTML(int employeeId)
        {
            return PayManager.GetHTMLEmployeeDeductionList(employeeId);
        }
        private void Initialise()
        {
            BindEmployees();

        }

        private void BindEmployees()
        {
            int totalRecords = 0;

            EmployeeManager empMgr = new EmployeeManager();

            string customRoleDeparmentList = null;
            if (SessionManager.IsCustomRole)
                customRoleDeparmentList = SessionManager.CustomRoleDeparmentIDList;

            list.DataSource
                = empMgr.GetAllEmployees(pagintCtl.CurrentPage, int.Parse(pagintCtl.DDLRecords.SelectedValue), ref totalRecords,
                "",SessionManager.CurrentCompanyId,"","",false,-1,-1,-1,-1,"","",-1,-1,-1,"",-1);


                // "", SessionManager.CurrentCompanyId, txtEmpSearchText.Text.Trim(),
                // "", false,
                //-1, -1, -1, -1, customRoleDeparmentList, -1,-1,0,"");
            list.DataBind();

            pagintCtl.UpdatePagingBar(totalRecords);
        }

        public void txtEmpSearchText_TextChanged(object sender, EventArgs e)
        {
            BindEmployees();
        }
        
        protected void btnPrevious_Click()
        {
            pagintCtl.CurrentPage -= 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage -= 1;
            BindEmployees();
        }

        protected void ChangePageNumber()
        {
            BindEmployees();
        }

        protected void btnNext_Click()
        {
            pagintCtl.CurrentPage += 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage += 1;
            BindEmployees();
        }

        protected void ddlRecords_SelectedIndexChanged()
        {
            SessionManager.EmployeeDisplayRecodsPerPage = int.Parse(pagintCtl.DDLRecords.SelectedValue);
            //_tempCurrentPage = 1;
            pagintCtl.CurrentPage = 1;
            BindEmployees();
        }
  

      
    }
}
