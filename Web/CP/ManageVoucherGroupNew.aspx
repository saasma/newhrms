﻿<%@ Page Title="Manage Voucher" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="ManageVoucherGroupNew.aspx.cs" Inherits="Web.CP.ManageVoucherGroupNew" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function medicalTaxCreditPopupCall(medicalTaxId) {
            var ret = medicalTaxCreditPopup("ID=" + medicalTaxId);
            if (ret == "Yes")
                __doPostBack('Reload', '');
        }
        function voucherTypePopup(medicalTaxId) {
            var ret = voucherType("ID=" + medicalTaxId);
            if (ret == "Yes")
                __doPostBack('Reload', '');
        }

        function voucherPoupCall(type, sourceId) {
            var ret = voucherHead("type=" + type + "&sourceId=" + sourceId);
            if (ret == "Yes")
                __doPostBack('Reload', '');
        }
        function parentReloadCallbackFunction(childWindow, ret) {
            childWindow.close();
            if (ret == "Yes")
                __doPostBack('Reload', '');

        }

        function refreshWindow() {
            __doPostBack('Reload', '');
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Account Groups
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div>
            <asp:LinkButton runat="server" Visible="false"></asp:LinkButton>
            <h3 style="margin-top: 10px!important;">
                Voucher Types</h3>
            <div class="separator clear">
                <cc2:EmptyDisplayGridView CssClass="table table-primary mb30 table-bordered table-hover"
                    UseAccessibleHeader="true" ID="gridVoucerType" runat="server" AutoGenerateColumns="False"
                    DataKeyNames="VoucherTypeID" GridLines="None" ShowFooterWhenEmpty="False" ShowHeaderWhenEmpty="True"
                    Width="100%">
                    <Columns>
                        <asp:BoundField HeaderStyle-HorizontalAlign="Left" DataField="Name" HeaderText="Name"
                            HeaderStyle-Width="100px" />
                         <asp:BoundField HeaderStyle-HorizontalAlign="Left" DataField="Description" HeaderText="Description"
                            HeaderStyle-Width="120px" />
                        <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:ImageButton ID="ImageButton1" ImageUrl="~/images/edit.gif" OnClientClick='<%# "voucherTypePopup(" + Eval("VoucherTypeID") + ");return false;" %>'
                                    runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="odd" />
                    <AlternatingRowStyle CssClass="even" />
                    <EmptyDataTemplate>
                        No records.
                    </EmptyDataTemplate>
                </cc2:EmptyDisplayGridView>
            </div>
            <div class="buttonsDivSection">
                <asp:Button ID="Button1" CssClass="btn btn-primary btn-sm btn-sect" runat="server"
                    Text="Add New Type" OnClientClick="voucherTypePopup(0);return false;" />
            </div>
            <h3 style="margin-top: 10px!important;">
                Voucher Group</h3>
            <div class="separator clear">
                <cc2:EmptyDisplayGridView CssClass="table table-primary mb30 table-bordered table-hover"
                    UseAccessibleHeader="true" ID="gvwList" runat="server" AutoGenerateColumns="False"
                    DataKeyNames="VoucherGroupID" GridLines="None" ShowFooterWhenEmpty="False" ShowHeaderWhenEmpty="True"
                    Width="100%">
                    <Columns>
                        <asp:BoundField HeaderStyle-HorizontalAlign="Left" DataField="VoucherType" HeaderText="Voucher Type"
                            HeaderStyle-Width="40px" />
                        <asp:BoundField HeaderStyle-HorizontalAlign="Left" DataField="GroupCode" HeaderText="Code"
                            HeaderStyle-Width="40px" />
                        <asp:BoundField HeaderStyle-HorizontalAlign="Left" DataField="NIBLCardDepartmentGroupCode" HeaderText="Card Dep Code"
                            HeaderStyle-Width="100px" />
                        <asp:BoundField HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left" DataField="GroupName"
                            HeaderText="GroupName" />
                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-Width="80px" HeaderText="Group">
                            <ItemTemplate>
                                <%# GetVoucherGroup(Eval("Group"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderStyle-HorizontalAlign="Left" DataField="Status" HeaderText="Status"
                            HeaderStyle-Width="120px" />
                        <asp:BoundField HeaderStyle-HorizontalAlign="Left" DataField="Level" HeaderText="Level"
                            HeaderStyle-Width="120px" />
                        <asp:BoundField HeaderStyle-HorizontalAlign="Left" DataField="IncomeDeduction" HeaderText="Income Deduction"
                            HeaderStyle-Width="300px" />
                        
                        <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:ImageButton ID="ImageButton1" ImageUrl="~/images/edit.gif" OnClientClick='<%# "medicalTaxCreditPopupCall(" + Eval("VoucherGroupID") + ");return false;" %>'
                                    runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="odd" />
                    <AlternatingRowStyle CssClass="even" />
                    <EmptyDataTemplate>
                        No records.
                    </EmptyDataTemplate>
                </cc2:EmptyDisplayGridView>
            </div>
            <div class="buttonsDivSection">
                <asp:Button ID="btnSave" CssClass="btn btn-primary btn-sm btn-sect" runat="server"
                    Text="Add New Group" OnClientClick="medicalTaxCreditPopupCall(0);return false;" />
            </div>
        </div>
    </div>
</asp:Content>
