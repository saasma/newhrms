<%@ Page Title="Festival Payment Details" MaintainScrollPositionOnPostback="true"
    Language="C#" MasterPageFile="~/Master/HROld.Master" AutoEventWireup="true" CodeBehind="FestivalPaymentDetails.aspx.cs"
    Inherits="Web.CP.FestivalPaymentDetails" %>

<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../_assets/themes/yui/tabs.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">

        function insertUpdateGratuityRuleCall(ein, festivalid) {
            var ret = insertUpdateGratuityRule('fid=' + festivalid + '&ein=' + ein);

            if (typeof (ret) != 'undefined') {
                if (ret == 'Reload') {
                    __doPostBack('Reload', '');
                }
            }

            return false;
        }


        function refresh(popupWindow) {
            popupWindow.close();
            __doPostBack('Reload', '');
        }

        function changeStates(chk) {
            var isChecked = $(chkAll).is(':checked');

            $("INPUT[type='checkbox']").attr('checked', isChecked);

        }

        function updateBonusPopup(bonusId, empId) {
            var ret = updateBonus("bonusId=" + bonusId + "&empid=" + empId);


            return false;
        }
        function importPopupProcess() {


            var ret = importPopup("id=" + overtimeId);


            return false;
        }
        var skipLoadingCheck = true;
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4 runat="server" id="title">
                    Dashain Calculation :
                </h4>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hdnBonusId" runat="server" />
    <div class="contentpanel">
        <div class="contentArea">
            <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
            <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
            <div runat="server" style="color: #F49D25" id="voucherAlsoChange">
                If already posted to salary then amount need to clear from Add On before Re-Generating
                calculation</div>
            <div runat="server" style="color: #F49D25" id="Div1">
                Apply Dashain Allowance to all Employees before proceeding</div>
            <asp:Panel runat="server" ID="pnl" DefaultButton="btnLoad" class="attribute">
                <table class="fieldTable">
                    <tr>
                        <td>
                            <asp:Button OnClick="btnGenerate_Click" CssClass="save" runat="server" ID="btnCalculate" Width="150px"
                                Text="Generate Calculation" OnClientClick='return confirm("Are you sure,you want to generate the calculation?");' />
                        </td>
                        <td>
                            <asp:Button OnClick="btnPostToAddOn_Click" CssClass="save" runat="server" ID="btnPostToAddOn"
                                Width="150px" Text="Post To Add-On" OnClientClick='return confirm("Are you sure,you want to post amount?");' />
                        </td>
                        <td>
                            Employee
                            <br />
                            <asp:TextBox Width="180px" ID="txtEmpSearchText" runat="server"></asp:TextBox>
                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtEmpSearchText"
                                WatermarkText="Employee Name" WatermarkCssClass="searchBoxText" />
                            <cc1:AutoCompleteExtender EnableCaching="true" ID="AutoCompleteExtenderOrganization"
                                runat="server" MinimumPrefixLength="2" ServiceMethod="GetEmployeeNames" ServicePath="~/PayrollService.asmx"
                                TargetControlID="txtEmpSearchText" CompletionSetCount="10" CompletionInterval="250"
                                CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList"
                                CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
                            </cc1:AutoCompleteExtender>
                        </td>
                        <td style="padding-left: 10px">
                            Branch
                            <br />
                            <asp:DropDownList ID="ddlBranch" AppendDataBoundItems="true" DataValueField="BranchId"
                                DataTextField="Name" runat="server" Width="150px">
                                <asp:ListItem Text="--Select Branch--" Value="-1" />
                            </asp:DropDownList>
                        </td>
                        <td style="padding-left: 10px">
                            Designation
                            <br />
                            <asp:DropDownList ID="ddlDesignation" DataValueField="Text" DataTextField="Text"
                                runat="server" Width="150px">
                            </asp:DropDownList>
                        </td>
                        <td style="padding-left: 10px; margin-top: 5px;">
                            <asp:Button ID="btnLoad" CssClass="update" OnClick="btnLoad_Click" runat="server"
                                CommandName="Load" Width="90px" Text="Load" />
                        </td>
                        <td style="padding-left: 20px; margin-top: 5px;">
                            <asp:Button ID="Button4" CssClass="update" OnClick="btnExport_Click" runat="server"
                                CommandName="Export" Width="90px" Text="Export" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <cc2:EmptyDisplayGridView CssClass="table table-primary mb30 table-bordered table-hover"
                Style="margin-bottom: -3px;" UseAccessibleHeader="true" ShowHeaderWhenEmpty="True"
                ID="gvwEmployees" runat="server" DataKeyNames="EIN" AutoGenerateColumns="False"
                AllowSorting="True" OnSorting="gvwEmployees_Sorting" ShowFooterWhenEmpty="False"
                Width="100%" GridLines="None">
                <Columns>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                        HeaderStyle-Width="30px" HeaderText="SN">
                        <ItemTemplate>
                            <%#     (Eval("SN"))%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                        HeaderStyle-Width="30px" HeaderText="EIN">
                        <ItemTemplate>
                            <%#     (Eval("EIN"))%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                        HeaderStyle-Width="50px" HeaderText="Account No">
                        <ItemTemplate>
                            <%#     (Eval("AccountNo"))%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                        HeaderStyle-Width="120px" HeaderText="Employee">
                        <ItemTemplate>
                            <%#     (Eval("Name"))%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                        HeaderStyle-Width="100px" HeaderText="Designation">
                        <ItemTemplate>
                            <%#     (Eval("Designation"))%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                        HeaderStyle-Width="100px" HeaderText="Branch">
                        <ItemTemplate>
                            <%#     (Eval("Branch"))%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                        HeaderStyle-Width="90px" HeaderText="Join Date">
                        <ItemTemplate>
                            <%#     (Eval("JoinDateText"))%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"
                        HeaderStyle-Width="60px" HeaderText="LWP / UPL">
                        <ItemTemplate>
                            <%#     (Eval("UPLAbsent"))%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"
                        HeaderStyle-Width="60px" HeaderText="Stop Pay">
                        <ItemTemplate>
                            <%#     (Eval("StopPay"))%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"
                        HeaderStyle-Width="60px" HeaderText="Work Days">
                        <ItemTemplate>
                            <%#  (Eval("TotalWorkDays"))%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"
                        HeaderStyle-Width="60px" HeaderText="Basic Salary">
                        <ItemTemplate>
                            <%#     GetCurrency(Eval("Amount1"))%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"
                        HeaderStyle-Width="60px" HeaderText="Allowance">
                        <ItemTemplate>
                            <%#     GetCurrency(Eval("Amount2"))%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"
                        HeaderStyle-Width="60px" HeaderText="Incentive Allowance">
                        <ItemTemplate>
                            <%#     GetCurrency(Eval("Amount3"))%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"
                        HeaderStyle-Width="60px" HeaderText="Dashain Amount">
                        <ItemTemplate>
                            <%#     GetCurrency(Eval("Amount"))%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"
                        HeaderStyle-Width="60px" HeaderText="SST">
                        <ItemTemplate>
                            <%#     GetCurrency(Eval("SST"))%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"
                        HeaderStyle-Width="60px" HeaderText="TDS">
                        <ItemTemplate>
                            <%#     GetCurrency(Eval("TDS"))%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"
                        HeaderStyle-Width="60px" HeaderText="Net Paid">
                        <ItemTemplate>
                            <%#     GetCurrency(Eval("NetPay"))%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                        HeaderStyle-Width="100px" HeaderText="Remarks">
                        <ItemTemplate>
                            <%#     (Eval("Remarks"))%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                        HeaderStyle-Width="60px" HeaderText="Skip in ReCalculate">
                        <ItemTemplate>
                            <%#     ( Convert.ToBoolean( Eval("PreventReset")) == true ? "Yes" : "" )%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="60px">
                        <ItemTemplate>
                            <input type='image' style='border-width: 0px; vertical-align: sub;' onclick='return insertUpdateGratuityRuleCall(<%# Eval("EIN") %>,<%# GetFestival() %>);'
                                src='../images/edit.gif' />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:TemplateField>
                </Columns>
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                <EmptyDataTemplate>
                    <b>No employee records found. </b>
                </EmptyDataTemplate>
            </cc2:EmptyDisplayGridView>
            <uc1:PagingCtl ID="pagingCtl" OnNextRecord="btnNext_Click" OnPrevRecord="btnPrevious_Click"
                OnDropDownShowRecordsChanged="ddlRecords_SelectedIndexChanged" runat="server" />
        </div>
    </div>
</asp:Content>
