﻿<%@ Page Title="Income list" Language="C#" MasterPageFile="~/Master/ForPopupPage.Master"
    AutoEventWireup="true" CodeBehind="IncomeList.aspx.cs" Inherits="Web.CP.IncomeList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        function closePopup() {


            // alert(window.opener.parentReloadCallbackFunction)
            //            if ($.browser.msie == false && typeof (window.opener.parentIncomeCallbackFunction) != 'undefined' && ($.browser.chrome || $.browser.safari)) {
            window.opener.parentIncomeCallbackFunction("ReloadIncome", window);
            //            } else {
            //                window.returnValue = "ReloadIncome";
            //                window.close();
            //            }
        }

        function popupCreateNewCall() {
            var ret = popupCreateNew();
            if (typeof (ret) != 'undefined') {
                if (ret == "ReloadIncome") {
                    __doPostBack('Reload', '');
                }
            }
        }

        function parentIncomeCallbackFunction(text, closingWindow) {
            closingWindow.close();
            __doPostBack('Reload', '');
        }
        function doubleClickHander(lst) {
            $('#<%= btnOk.ClientID %>').click();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <div class="popupHeader">
        <h2 class="headlinespop">
            IncomeList
        </h2>
    </div>
    <div class="bevel marginal">
        <div class="fields paddpop">
            <table>
                <tr>
                    <td valign="top">
                        <asp:ListBox ondblclick="doubleClickHander(this)" ID="lstIncomeList" SelectionMode="Multiple"
                            DataTextField="Title" DataValueField="IncomeId" runat="server" Height="238px"
                            Width="217px"></asp:ListBox>
                        <asp:RequiredFieldValidator ID="valReqdName" runat="server" ControlToValidate="lstIncomeList"
                            Display="None" ErrorMessage="Income must be selected for adding." ValidationGroup="AddIncome"></asp:RequiredFieldValidator>
                    </td>
                    <td valign="top" style='padding-left: 10px'>
                        <asp:Button ID="btnOk" runat="server" Text="Ok" CssClass="update" ValidationGroup="AddIncome"
                            OnClientClick="valGroup='AddIncome';return CheckValidation()" OnClick="btnOk_Click" />
                        <br />
                        <asp:Button ID="btnCreateNew" runat="server" Text="Create" CssClass="save" OnClientClick="popupCreateNewCall();return false;" />
                        <br />
                        <asp:Button ID="btnCancel" runat="server" CssClass="cancel" Text="Cancel" OnClientClick="closePopup();return false;" />
                    </td>
                </tr>
            </table>
            <br />
            <asp:RadioButtonList ID="rdbListAddTo" runat="server" AutoPostBack="true" OnSelectedIndexChanged="rdbListAddTo_Change">
                <asp:ListItem Selected="True" Value="1">Add to this employee</asp:ListItem>
                <asp:ListItem Value="2">Add to these employees</asp:ListItem>
            </asp:RadioButtonList>
            <div style="padding-left: 10px; padding-top: 10px;">
                <table>
                    <tr>
                        <td>
                            Designation
                        </td>
                        <td>
                            <asp:DropDownList Enabled="false"  ID="ddlDesig" Width="150px" runat="server"
                                DataValueField="DesignationId" DataTextField="CodeAndName" AppendDataBoundItems="true">
                                <asp:ListItem Value="-1" Text="--All Position--"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Status
                        </td>
                        <td>
                            <asp:DropDownList Enabled="false"  Style="margin-top: 10px" ID="ddlStatus" runat="server" AppendDataBoundItems="true"
                                Width="150px">
                                <asp:ListItem Selected="true" Value="-1" Text="--All Status--"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Branch
                        </td>
                        <td>
                            <asp:DropDownList Enabled="false"  Style="margin-top: 10px" ID="ddlBranch" DataTextField="Name" DataValueField="BranchId" runat="server" AppendDataBoundItems="true"
                                Width="150px">
                                <asp:ListItem Selected="true" Value="-1" Text="--All Branch--"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </div>
            <asp:LinkButton ID="btnRefresh" runat="server" Text="Refresh" Style="display: none" />
        </div>
    </div>
</asp:Content>
