<%@ Page MaintainScrollPositionOnPostback="true" Title="Advance Adjustment" Language="C#"
    MasterPageFile="~/Master/HROld.Master" AutoEventWireup="true" CodeBehind="AdvanceAdjustment.aspx.cs"
    Inherits="Web.CP.AdvanceAdjustment" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        function selectDeselectAll(chk) {
            $('input[type=checkbox]').each(
                    function (index) {
                        if ($(this).prop('disabled') == false && this.id.indexOf('chkSelect') > 0)
                            this.checked = chk.checked;
                    }
                );
        }

        function validateAdjustment(source, args) {

            var adjustmentAmount = getNumber(document.getElementById(source.controltovalidate).value);
            var ppmt = getNumber(source.getAttribute('PPMTorIPMT'));


            if (adjustmentAmount < 0) {
                if ((ppmt + adjustmentAmount) < 0)
                    args.IsValid = false;
                else
                    args.IsValid = true;
            }

            else
                args.IsValid = true;
        }

    </script>
    <style type="text/css">
        .blankColorCell
        {
            background-color: #fff !important;
            border-top: #fff !important;
            border-left: #fff !important;
            border-bottom: #fff !important;
            border-right: 1px solid #A5DEFA !important;
        }
        .hightlightCell
        {
            border-top: 2px solid lightblue !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Advance Adjustment
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <asp:Panel ID="pnl" DefaultButton="btnLoad" runat="server" class="attribute" Style="padding: 10px">
            <table>
                <tr>
                    <td>
                        <%-- <strong>Payroll Period</strong>
                        <asp:DropDownList AutoPostBack="true" ID="ddlPayrollPeriods" DataTextField="Name"
                            DataValueField="PayrollPeriodId" AppendDataBoundItems="true" Width="150px" runat="server"
                            OnSelectedIndexChanged="ddlPayrollPeriods_SelectedIndexChanged">
                            <asp:ListItem Value="-1" Text=""></asp:ListItem>
                        </asp:DropDownList>--%>
                        <%--<asp:RequiredFieldValidator InitialValue="-1" Display="None" ControlToValidate="ddlPayrollPeriods"
                        ValidationGroup="Overtime" ID="RequiredFieldValidator2" runat="server" ErrorMessage="Payroll period is required."></asp:RequiredFieldValidator>--%>
                        <%--   &nbsp; &nbsp; --%>
                        <strong>Advances</strong>
                        <asp:DropDownList AutoPostBack="true" ID="ddlLoans" DataTextField="Title" DataValueField="DeductionId"
                            AppendDataBoundItems="true" Width="150px" runat="server" OnSelectedIndexChanged="ddlLoans_SelectedIndexChanged">
                            <asp:ListItem Value="-1" Text=""></asp:ListItem>
                        </asp:DropDownList>
                        &nbsp; &nbsp; <strong>Employee</strong>
                        <asp:DropDownList ID="ddlEmployees" DataTextField="Name" DataValueField="EmployeeId"
                            AppendDataBoundItems="true" Width="150px" runat="server">
                            <asp:ListItem Value="-1" Text=""></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <asp:Button ID="btnLoad" runat="server" CssClass="btn btn-default btn-sect btn-sm" Text="Show" Width="80px" OnClick="btnLoad_Click" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <br />
        <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
        <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
        <%--   <div cssclass="clear buttonsDiv" id="divButtons" runat="server" style='text-align: right;
            margin-top: 15px'>
          
        </div>--%>
        <asp:Panel ID="pnlDetails" runat="server" Visible="false">
            <div style="width: 656px">
                <table style='clear: both; float: left; padding-bottom: 10px'>
                    <tr>
                        <td class="fieldHeader">
                            Loan Amount
                        </td>
                        <td>
                            <asp:Label ID="lblLoanAmount" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="fieldHeader">
                            Taken On
                        </td>
                        <td>
                            <asp:Label ID="lblTakenOn" runat="server" />
                        </td>
                    </tr>
                    <%-- <tr>
                        <td class="fieldHeader">
                            Interest Rate
                        </td>
                        <td>
                            <asp:TextBox Width="100px" ID="txtInterestRate" runat="server" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtInterestRate"
                                Display="None" SetFocusOnError="true" ErrorMessage="Interest rate is required."
                                ValidationGroup="Balance"></asp:RequiredFieldValidator>
                            <asp:CompareValidator Operator="GreaterThan" ValueToCompare="0" ValidationGroup="Balance"
                                Type="Integer" SetFocusOnError="true" ID="CompareValidator1" Display="None" ControlToValidate="txtInterestRate"
                                runat="server" ErrorMessage="Invalid interest rate."></asp:CompareValidator>
                        </td>
                    </tr>--%>
                    <tr>
                        <td class="fieldHeader">
                            No of Payments
                        </td>
                        <td>
                            <asp:Label ID="lblNoOfPayments" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="fieldHeader">
                            Starting From
                        </td>
                        <td>
                            <asp:Label ID="lblStartingFrom" runat="server" Text="Monthly" />
                        </td>
                    </tr>
                </table>
                <table style='float: right; padding-bottom: 10px'>
                    <tr>
                        <td class="fieldHeader">
                            EID
                        </td>
                        <td>
                            <asp:Label ID="lblEID" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="fieldHeader">
                            Name
                        </td>
                        <td>
                            <asp:Label ID="lblName" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="fieldHeader">
                            Designation
                        </td>
                        <td>
                            <asp:Label ID="lblDesignation" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="fieldHeader">
                            Branch
                        </td>
                        <td>
                            <asp:Label ID="lblBranch" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="fieldHeader">
                            Department
                        </td>
                        <td>
                            <asp:Label ID="lblDepartment" runat="server" />
                        </td>
                    </tr>
                </table>
            </div>
            <cc2:EmptyDisplayGridView CssClass="table table-primary mb30 table-bordered table-hover" Style='clear: both' UseAccessibleHeader="true"
                GridLines="None" ID="gvwSimpleLoanInstallments" DataKeyNames="PayrollPeriodId,IsCurrentValidPayrollPeriod,EmployeeId,DeductionId,IsPayrollPeriodSaved,RemainingInstallments"
                runat="server" ShowFooterWhenEmpty="False" ShowHeaderWhenEmpty="True" AutoGenerateColumns="False"
                OnRowCommand="gvwSimpleLoanInstallments_RowCommand" OnRowDataBound="gvwSimpleLoanInstallments_RowDataBound">
                <Columns>
                    <asp:BoundField HeaderStyle-Width="30px" DataField="RemainingInstallments" ItemStyle-HorizontalAlign="Center"
                        HeaderText="Remaining Installments"></asp:BoundField>
                    <asp:TemplateField HeaderText="Month" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <%# Utils.Calendar.DateHelper.GetMonthName( (int) Eval("Month"),IsEnglish) %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Opening" ItemStyle-HorizontalAlign="Right">
                        <ItemTemplate>
                            <asp:Label ID="lblOpening" runat="server" Text='<%# GetCurrency(Eval("Opening")) %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Installment" ItemStyle-HorizontalAlign="Right">
                        <ItemTemplate>
                            <asp:Label ID="lblInstallment" runat="server" Text='<%# GetCurrency(Eval("InstallmentAmount"))%>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <%--  <asp:TemplateField HeaderText="IPMT" ItemStyle-HorizontalAlign="Right">
                        <ItemTemplate>
                            <asp:Label ID="lblIPMT" runat="server" Text='<%# GetCurrency(Eval("IPMT")) %>' />
                        </ItemTemplate>
                    </asp:TemplateField>--%>
                    <asp:TemplateField HeaderText="Closing" ItemStyle-HorizontalAlign="Right">
                        <ItemTemplate>
                            <asp:Label ID="lblClosing" runat="server" Text='<%# GetCurrency(Eval("Closing")) %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-CssClass="blankColorCell" HeaderText="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;">
                        <ItemTemplate>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </ItemTemplate>
                        <ItemStyle CssClass="blankColorCell" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Installment Adjustment" ItemStyle-HorizontalAlign="Right">
                        <ItemTemplate>
                            <asp:Label runat="server" Visible='<%# ((int)Eval("IsCurrentValidPayrollPeriod"))==0 %>'
                                Text='<%#GetAmount( Eval("Adjustment"),true) %>' />
                            <asp:TextBox Width="100px" Style='text-align: right' ID="txtAdjustment" runat="server"
                                Visible='<%# ((int)Eval("IsCurrentValidPayrollPeriod"))==1 %>' Text='<%#GetAmount( Eval("Adjustment"),false) %>' />
                            <asp:CompareValidator SetFocusOnError="true" ValueToCompare='<%# GetCurrency( Convert.ToDecimal(Eval("Opening")) - Convert.ToDecimal(Eval("InstallmentAmount"))) %>'
                                Display="None" Operator="LessThanEqual" Type="Currency" ID="valOpeningBalance245"
                                ControlToValidate="txtAdjustment" ValidationGroup="Balance" runat="server" ErrorMessage="Invalid amount, should not be greater than opening minus installment amount."></asp:CompareValidator>
                            <asp:CustomValidator ID="CustomValidator1" runat="server" PPMTorIPMT='<%# GetCurrency(Eval("InstallmentAmount")) %>'
                                SetFocusOnError="true" ControlToValidate="txtAdjustment" ValidationGroup="Balance"
                                ErrorMessage="Invalid adjustment , sum of installment and adjustment  can not be negative."
                                Display="None" ClientValidationFunction="validateAdjustment" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <%--<asp:TemplateField HeaderText="Adjustment IPMT" ItemStyle-HorizontalAlign="Right">
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Visible='<%# ((int)Eval("IsCurrentValidPayrollPeriod"))==0 %>'
                                Text='<%# GetAmount( Eval("AdjustmentIPMT"),true) %>' />
                            <asp:TextBox Width="100px" Style='text-align: right' ID="txtAdjustmentIPMT" runat="server"
                                Visible='<%# ((int)Eval("IsCurrentValidPayrollPeriod"))==1 %>' Text='<%# GetAmount(Eval("AdjustmentIPMT"),false) %>' />
                            <asp:CompareValidator SetFocusOnError="true" ValueToCompare='<%# GetCurrency(Eval("PPMT")) %>'
                                Display="None" Operator="LessThanEqual" Type="Currency" ID="valOpeningBalance2451"
                                ControlToValidate="txtAdjustmentIPMT" ValidationGroup="Balance" runat="server"
                                ErrorMessage="Invalid amount, should not be greater than PPMT."></asp:CompareValidator>

                            <asp:CustomValidator ID="CustomValidator12" runat="server" PPMTorIPMT='<%# GetCurrency(Eval("IPMT")) %>' SetFocusOnError="true" ControlToValidate="txtAdjustmentIPMT" ValidationGroup="Balance"
                                ErrorMessage="Invalid adjustment ipmt, sum of ipmt and adjustment ipmt can not be negative." Display="None"  ClientValidationFunction="validateAdjustment" />
                        </ItemTemplate>
                    </asp:TemplateField>--%>
                    <asp:TemplateField HeaderText="Adjusted Installment" ItemStyle-HorizontalAlign="Right">
                        <ItemTemplate>
                            <asp:Label ID="lblAdjustedPPMT" runat="server" Text='<%# GetCurrency(Eval("Adjusted")) %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <%-- <asp:TemplateField HeaderText="Adjusted Interest" ItemStyle-HorizontalAlign="Right">
                        <ItemTemplate>
                            <asp:Label ID="lblAdjustedInterest" runat="server" Text='<%# GetCurrency(Eval("AdjustedInterest")) %>' />
                        </ItemTemplate>
                    </asp:TemplateField>--%>
                    <asp:TemplateField HeaderText="Adjusted Closing" ItemStyle-HorizontalAlign="Right">
                        <ItemTemplate>
                            <asp:Label ID="lblAdjustedClosing" runat="server" Text='<%# GetCurrency(Eval("AdjustedClosing")) %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Skip This Month">
                        <ItemTemplate>
                            <asp:CheckBox ID="chkSkipThisMonth" Enabled='<%# ((int)Eval("IsCurrentValidPayrollPeriod"))==1 %>'
                                runat="server" Checked='<%# (Eval("IsSkipThisMonth") != null) ? Eval("IsSkipThisMonth") : false %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                <EmptyDataTemplate>
                    <b>No installments</b>
                </EmptyDataTemplate>
            </cc2:EmptyDisplayGridView>
            <br />
            <%--  <asp:ValidationSummary ID="ValidationSummary1" DisplayMode="List" ShowSummary="false"
                ShowMessageBox="true" ValidationGroup="Balance" runat="server"></asp:ValidationSummary>--%>
            <asp:Button ID="btnSave" runat="server" OnClientClick="valGroup='Balance';return CheckValidation();"
                ValidationGroup="Balance" Style='float: left' CssClass="save" Text="Save" OnClick="btnSendMail_Click" />
        </asp:Panel>
    </div>
</asp:Content>
