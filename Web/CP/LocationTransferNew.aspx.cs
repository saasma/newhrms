﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using Utils.Helper;
using System.IO;
using Utils;
using Utils.Calendar;

namespace Web.CP
{
    public partial class LocationTransferNew : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.HumanResource;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest && !IsPostBack)
            {
                Initialize();
            }
        }

        private void Initialize()
        {
            X.Js.Call("searchList");

            cmbLocation.GetStore().DataSource = ListManager.GetELocationList();
            cmbLocation.GetStore().DataBind();

            WLocation.Hide();
            if (SessionManager.CurrentCompany.IsEnglishDate)
                colFromDate.Hide();
        }

        private void Clear()
        {
            calFromDate.Text = "";
            txtNote.Text = "";
            cmbLocation.Clear();
            cmbEmployee.Clear();
            cmbEmployee.Enable();
            lblEName.Text = "";
            lblEBranch.Text = "";
            lblEDepartment.Text = "";
            lblESince.Text = "";
            lblInTime.Text = "";

            image1.ImageUrl = Page.ResolveUrl(Web.Helper.WebHelper.GetEmployeeNoImage(0));
        }

        protected void btnNewTransfer_Click(object sender, DirectEventArgs e)
        {
            Clear();
            hdnLocationHistoryId.Text = "";
            hdnEmployeeId.Text = "";
            WLocation.Show();
        }

        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            int totalRecords = 0;
            DateTime fromDate = new DateTime();
            DateTime toDate = new DateTime();

            if (!string.IsNullOrEmpty(txtFromDate.Text.Trim()))
                fromDate = txtFromDate.SelectedDate;

            if (!string.IsNullOrEmpty(txtToDate.Text.Trim()))
                toDate = txtToDate.SelectedDate;

            int employeeId = -1;
            string employeeSearch = "";

            int.TryParse(cmbEmpSearch.SelectedItem.Value, out employeeId);

            if (employeeId == 0)
                employeeId = -1;

            if (employeeId == -1)
            {
                if (!string.IsNullOrEmpty(cmbEmpSearch.Text))
                    employeeSearch = cmbEmpSearch.Text.Trim();
            }

            List<GetLocationTransfersResult> list = NewHRManager.GetLocationTransfersNew(e.Start, int.Parse(cmbPageSize.SelectedItem.Value), employeeSearch, employeeId, ref totalRecords);

            list = list.Where(x => ((fromDate == new DateTime() || x.FromDateEng >= fromDate) && (toDate == new DateTime() || x.FromDateEng <= toDate))).ToList();

            e.Total = totalRecords;

            if (list.Any())
                storeEmpList.DataSource = list;
            storeEmpList.DataBind();

        }

        protected Field OnCreateFilterableField(object sender, ColumnBase column, Field defaultField)
        {
            if (column.DataIndex == "LocationHistoryId")
            {
                ((TextField)defaultField).Icon = Icon.Magnifier;
            }

            return defaultField;
        }


        private void GetElapsedTime(DateTime from_date, DateTime to_date,
            out int years, out int months, out int days, out int hours,
            out int minutes, out int seconds, out int milliseconds)
        {
            // If from_date > to_date, switch them around.
            if (from_date > to_date)
            {
                GetElapsedTime(to_date, from_date,
                    out years, out months, out days, out hours,
                    out minutes, out seconds, out milliseconds);
                years = -years;
                months = -months;
                days = -days;
                hours = -hours;
                minutes = -minutes;
                seconds = -seconds;
                milliseconds = -milliseconds;
            }
            else
            {
                // Handle the years.
                years = to_date.Year - from_date.Year;

                // See if we went too far.
                DateTime test_date = from_date.AddMonths(12 * years);
                if (test_date > to_date)
                {
                    years--;
                    test_date = from_date.AddMonths(12 * years);
                }

                // Add months until we go too far.
                months = 0;
                while (test_date <= to_date)
                {
                    months++;
                    test_date = from_date.AddMonths(12 * years + months);
                }
                months--;

                // Subtract to see how many more days,
                // hours, minutes, etc. we need.
                from_date = from_date.AddMonths(12 * years + months);
                TimeSpan remainder = to_date - from_date;
                days = remainder.Days;
                hours = remainder.Hours;
                minutes = remainder.Minutes;
                seconds = remainder.Seconds;
                milliseconds = remainder.Milliseconds;
            }
        }

        private void EmployeeDetails(int employeeId)
        {
            EEmployee emp = EmployeeManager.GetEmployeeById(employeeId);

            bool hasPhoto = false;
            if (emp.HHumanResources.Count > 0)
            {
                if (!string.IsNullOrEmpty(emp.HHumanResources[0].UrlPhotoThumbnail))
                {
                    image1.ImageUrl = Page.ResolveUrl(Config.UploadLocation + "/" + emp.HHumanResources[0].UrlPhotoThumbnail);
                    if (File.Exists(Server.MapPath(image1.ImageUrl)))
                        hasPhoto = true;
                }
            }
            if (!hasPhoto)
            {
                image1.ImageUrl = Page.ResolveUrl(Web.Helper.WebHelper.GetEmployeeNoImage(emp.EmployeeId));
            }

            lblEName.Text = emp.Title + " " + emp.Name;
            lblEBranch.Text = emp.Branch.Name;
            lblEDepartment.Text = emp.Department.Name;

            int years, months, days, hours;
            int minutes, seconds, milliseconds;
            ServicePeroid firstStatus = EmployeeManager.GetServicePeroidDate(emp.EmployeeId);
            GetElapsedTime(firstStatus.FromDateEng, BLL.BaseBiz.GetCurrentDateAndTime(), out years, out months, out days, out hours, out minutes, out seconds, out milliseconds);

            lblESince.Text = "Since " +
                (IsEnglish ? firstStatus.FromDateEng.ToShortDateString() : firstStatus.FromDate + " (" + firstStatus.FromDateEng.ToShortDateString() + ")");
            lblInTime.Text = string.Format("For {0} years {1} months {2} days",
                years, months, days);
        }

        protected void btnSelectEmp_Click(object sender, DirectEventArgs e)
        {
            int employeeId = int.Parse(cmbEmployee.SelectedItem.Value);
            // hdnEmployeeId.Text = employeeId.ToString();

            EEmployee emp = EmployeeManager.GetEmployeeById(employeeId);

            if (emp == null)
            {
                buttonsBar.Visible = false;
                NewMessage.ShowWarningMessage("Please select an employee.");
                return;
            }
            else
                buttonsBar.Visible = true;

            //ClearFields();
            CommonManager.SaveFirstIBranchfNotExists(employeeId);

            block.Visible = true;

            EmployeeDetails(employeeId);


        }

        protected void btnEdit_Click(object sender, DirectEventArgs e)
        {
            int employeeDesignationId = int.Parse(hdnLocationHistoryId.Text);
            ELocationHistory obj = NewHRManager.GetELocationHistoryById(employeeDesignationId);
            int employeeId = obj.EmployeeId.Value;
            EEmployee emp = EmployeeManager.GetEmployeeById(employeeId);

            EmployeeDetails(employeeId);

            cmbEmployee.Text = emp.Name;
            cmbEmployee.Disable();

            cmbLocation.SetValue(obj.LocationId.Value.ToString());

            if (IsEnglish == false && SessionManager.User.IsEnglishPreferred != null && SessionManager.User.IsEnglishPreferred)
            {

                if (obj.FromDateEng != null)
                    calFromDate.Text = obj.FromDateEng.Value.ToString("yyyy/MM/dd");
            }
            else
            {
                calFromDate.Text = obj.FromDate;
            }

            txtNote.Text = obj.Notes;

            btnSave.Text = Resources.Messages.Update;
            WLocation.Show();
        }


        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveLocTranfer");
            if (Page.IsValid)
            {
                ELocationHistory obj = new ELocationHistory();

                if (!string.IsNullOrEmpty(hdnLocationHistoryId.Text))
                    obj.LocationHistoryId = int.Parse(hdnLocationHistoryId.Text);

                obj.EmployeeId = int.Parse(hdnEmployeeId.Text);
                obj.LocationId = int.Parse(cmbLocation.SelectedItem.Value);

                if (IsEnglish == false && SessionManager.User.IsEnglishPreferred != null && SessionManager.User.IsEnglishPreferred)
                {
                    obj.FromDateEng = BLL.BaseBiz.GetEngDate(calFromDate.Text.Trim(), true);
                    obj.FromDate = BLL.BaseBiz.GetAppropriateDate(obj.FromDateEng.Value);
                }
                else
                {
                    obj.FromDate = calFromDate.Text.Trim();
                    obj.FromDateEng = GetEngDate(obj.FromDate);
                }

                if (!string.IsNullOrEmpty(txtNote.Text.Trim()))
                    obj.Notes = txtNote.Text.Trim();

                Status status = NewHRManager.SaveUpdateELocationHistory(obj);

                if (status.IsSuccess)
                {
                    WLocation.Close();
                    NewMessage.ShowNormalMessage("Location transfer saved successfully.");
                    X.Js.Call("searchList");
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }


            }

        }

        public void btnExport_Click(object sender, EventArgs e)
        {
            int totalRecords = 0;
            DateTime fromDate = new DateTime();
            DateTime toDate = new DateTime();

            if (!string.IsNullOrEmpty(txtFromDate.Text.Trim()))
                fromDate = txtFromDate.SelectedDate;

            if (!string.IsNullOrEmpty(txtToDate.Text.Trim()))
                toDate = txtToDate.SelectedDate;

            int employeeId = -1;
            string employeeSearch = "";

            int.TryParse(cmbEmpSearch.SelectedItem.Value, out employeeId);

            if (employeeId == 0)
                employeeId = -1;

            if (employeeId == -1)
            {
                if (!string.IsNullOrEmpty(cmbEmpSearch.Text))
                    employeeSearch = cmbEmpSearch.Text.Trim();
            }

            List<GetLocationTransfersResult> list = NewHRManager.GetLocationTransfers(0 , 9999999, employeeSearch, employeeId, ref totalRecords);

            list = list.Where(x => ((fromDate == new DateTime() || x.FromDateEng >= fromDate) && (toDate == new DateTime() || x.FromDateEng <= toDate))).ToList();

            foreach (var item in list)
            {
                item.FromDateEngFormatted = item.FromDateEng.Value.ToShortDateString();
            }

            Bll.ExcelHelper.ExportToExcel("Location Transfer Report", list,
                new List<String>() { "LocationHistoryId", "FromDateEng", "TotalRows", "RowNumber" },
            new List<String>() { },
            new Dictionary<string, string>() { { "FromDate", "Date" }, { "FromDateEngFormatted", "Date(AD)" }, { "EmployeeId", "EIN" }, { "EmployeeName", "Employee Name" }, { "FromLocation", "From Location" }, { "ToLocation", "To Location" } },
            new List<string>() { }
            , new Dictionary<string, string>() { } 
            , new List<string> { "FromDate", "FromDateEngFormatted", "EmployeeId", "EmployeeName", "FromLocation", "ToLocation" });



        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            bool isEng = false;
            if (UserManager.IsEnglishDatePreferredInNepaliDateSys())
                isEng = true;

            UserControl ctrl = Master.ContentHeaderCtrl();
            ResourceManager rm = ctrl.FindControl("ResourceManager1") as ResourceManager;

            rm.RegisterClientInitScript("Date11111", string.Format("isEnglish = {0};", isEng.ToString().ToLower()));

            rm.RegisterClientInitScript("Date22222", string.Format("todayDate = '{0}';function calendarOnClick(event, sourceElement, e3){{}};",
                                                                      CustomDate.GetTodayDate(isEng).ToString()));
        }

        protected void btnDelete_Click(object sender, DirectEventArgs e)
        {
            int locationHistoryId = int.Parse(hdnLocationHistoryId.Text);

            Status status = NewHRManager.DeleteLocationTransfer(locationHistoryId);

            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Record deleted successfully.");
                X.Js.Call("searchList");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }


    }
}