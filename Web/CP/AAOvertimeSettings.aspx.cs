﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using DAL;
using Utils.Web;
using BLL.Manager;
using BLL.Base;

namespace Web.CP
{
    public partial class AAOvertimeSettings : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.AdministratorTools;
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            BindCheckBox();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                OvertimeRounding roundingInstance = new OvertimeRounding();
                roundingInstance = OvertimeManager.getTopRounding();
                txtMinTime.Text = roundingInstance.MinTime.ToString();
                txtMaxTime.Text = roundingInstance.MaxTime.ToString();
                txtRounding.Text = roundingInstance.rounding.ToString();

                if (roundingInstance.AllowFutureDaysRequestAlso != null)
                    chkAllowFutureDays.Checked = roundingInstance.AllowFutureDaysRequestAlso.Value;

                if (roundingInstance.OvertimePermitedDays != null)
                    txtPastPerimittedDays.Text = roundingInstance.OvertimePermitedDays.ToString();
                else
                    txtPastPerimittedDays.Text = "0";

                if (roundingInstance.OvertimeHoursInTheMonth != null)
                    txtMonthHours.Text = roundingInstance.OvertimeHoursInTheMonth.ToString();
                else
                    txtMonthHours.Text = "0";

                if (roundingInstance.MaxMinInADay != null)
                    txtMaxMinInADay.Text = roundingInstance.MaxMinInADay.ToString();
                else
                    txtMaxMinInADay.Text = "0";

                if (roundingInstance.MaxMinInAWeek != null)
                    txtMaxMinInAWeek.Text = roundingInstance.MaxMinInAWeek.ToString();
                else
                    txtMaxMinInAWeek.Text = "0";

                //if (roundingInstance.MinBufferTimeAfterRegularShift != null)
                //    txtMinBufferTimeAfterRegShift.Text = roundingInstance.MinBufferTimeAfterRegularShift.ToString();
                //else
                //    txtMinBufferTimeAfterRegShift.Text = "0";

                ddlIncomes.DataSource = PayManager.GetVariableIncomeList(SessionManager.CurrentCompanyId, false);
                ddlIncomes.DataBind();

                ddlTADAIncomeList.DataSource = PayManager.GetVariableIncomeList(SessionManager.CurrentCompanyId, false);
                ddlTADAIncomeList.DataBind();

                Setting setting = OvertimeManager.GetSetting();
                if (setting != null && setting.OvertimeIncomeId != null)
                    UIHelper.SetSelectedInDropDown(ddlIncomes, setting.OvertimeIncomeId.Value);

                if (setting != null && setting.TADAIncomeId != null)
                    UIHelper.SetSelectedInDropDown(ddlTADAIncomeList, setting.TADAIncomeId.Value);

                //if (setting != null && setting.IsLeaveEncashmentRoundingInRetirement != null)
                //    isRounding.Checked = setting.IsLeaveEncashmentRoundingInRetirement.Value;

                if (setting != null && setting.AllowToChangeJoiningDate != null)
                    chkEnableJoiningDate.Checked = setting.AllowToChangeJoiningDate.Value;

                if (setting.EnableMultipleAddOn != null && setting.EnableMultipleAddOn.Value)
                    chkMultiAddOn.Checked = setting.EnableMultipleAddOn.Value;

                if (setting.SkipRetDayForCalculation != null && setting.SkipRetDayForCalculation.Value)
                    chkExcludeRetDayInCalculation.Checked = setting.SkipRetDayForCalculation.Value;

                if (setting != null && setting.HideTeamHRDetails != null)
                    chkHideTeamHRDetails.Checked = setting.HideTeamHRDetails.Value;

                if (setting.HidePaySlip != null && setting.HidePaySlip.Value)
                    chkHidePaySlip.Checked = setting.HidePaySlip.Value;

                if (setting.HideAllMonthPayslip != null && setting.HideAllMonthPayslip.Value)
                    chkHideAllMonthPaySlip.Checked = setting.HideAllMonthPayslip.Value;

                if (setting.ProcessApprovedOTAllowanceInAddOnOnly != null)
                    chkProcessOTAllowanceInAddOn.Checked = setting.ProcessApprovedOTAllowanceInAddOnOnly.Value;

                if (setting.EnableHealthInsurance != null)
                    chkEnableHealthInsurance.Checked = setting.EnableHealthInsurance.Value;


                if (setting.BringGratuityInRetirement != null)
                    chkBringGratuityInRetirement.Checked = setting.BringGratuityInRetirement.Value;


                if (setting.IsEINEditable != null)
                    chkEINEditable.Checked = setting.IsEINEditable.Value;

                if (setting.ShowAppraisalDashboard != null)
                    chkShowReviewDashboard.Checked = setting.ShowAppraisalDashboard.Value;

                //if (setting.AllowNetSalaryNegative != null)
                chkAllowNegativeNetSalarySaving.Checked = CommonManager.CompanySetting.PayrollAllowNegativeSalarySave;

                LoadCheckedItemsInCheckBox();
            }

            

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            Page.Validate("SaveUpdate");
            if (Page.IsValid)
            {
                OvertimeRounding roundingInstance = new OvertimeRounding();
                roundingInstance.MinTime = int.Parse(txtMinTime.Text);
                roundingInstance.MaxTime = int.Parse(txtMaxTime.Text);
                roundingInstance.rounding = int.Parse(txtRounding.Text);
                roundingInstance.OvertimeHoursInTheMonth = int.Parse(txtMonthHours.Text);
                roundingInstance.OvertimePermitedDays = int.Parse(txtPastPerimittedDays.Text);
                roundingInstance.MaxMinInADay = int.Parse(txtMaxMinInADay.Text);
                roundingInstance.MaxMinInAWeek = int.Parse(txtMaxMinInAWeek.Text);
                roundingInstance.AllowFutureDaysRequestAlso = chkAllowFutureDays.Checked;
                //roundingInstance.MinBufferTimeAfterRegularShift = int.Parse(txtMinBufferTimeAfterRegShift.Text.Trim());

                //List<OvertimeRuleMinTimeBufferNotApplicableTo> listOB = new List<OvertimeRuleMinTimeBufferNotApplicableTo>();
                
                //foreach (ListItem item in cbLevelException.Items)
                //{
                //    if (item.Selected)
                //    {
                //        OvertimeRuleMinTimeBufferNotApplicableTo obj = new OvertimeRuleMinTimeBufferNotApplicableTo() { LevelId = int.Parse(item.Value) };
                //        listOB.Add(obj);
                //    }
                //}
                
                //roundingInstance.OvertimeRuleMinTimeBufferNotApplicableTos.AddRange(listOB);

                bool isSuccess = OvertimeManager.UpdateOvertimeRounding(roundingInstance,int.Parse(ddlIncomes.SelectedValue));
                if (isSuccess)
                {
                    msgInfo.InnerHtml = "Setting saved.";
                    msgInfo.Hide = false;
                }

               
            }
        }

       

        protected void btnSaveJoiningDate_Click(object sender, EventArgs e)
        {



            bool isSuccess = OvertimeManager.UpdateJoiningDate(chkEnableJoiningDate.Checked);
            if (isSuccess)
            {
                msgInfo.InnerHtml = "Joining date setting changed.";
                msgInfo.Hide = false;
            }

        }

        protected void btnSavePaySetting_Click(object sender, EventArgs e)
        {



            bool isSuccess = OvertimeManager.UpdateMultipleAddOn(chkMultiAddOn.Checked,
                chkExcludeRetDayInCalculation.Checked, chkHidePaySlip.Checked, chkHideAllMonthPaySlip.Checked, chkProcessOTAllowanceInAddOn.Checked,
                chkAllowNegativeNetSalarySaving.Checked,chkEnableHealthInsurance.Checked,chkBringGratuityInRetirement.Checked);
            if (isSuccess)
            {
                msgInfo.InnerHtml = "Setting updated.";
                msgInfo.Hide = false;
            }

        }

        protected void btnSaveTADA_Click(object sender, EventArgs e)
        {



            bool isSuccess = OvertimeManager.UpdateTADAIncome(int.Parse(ddlTADAIncomeList.SelectedValue));
            if (isSuccess)
            {
                msgInfo.InnerHtml = "TADA setting saved.";
                msgInfo.Hide = false;
            }

        }
              

        private void BindCheckBox()
        {
            //List<BLevel> list = NewPayrollManager.GetAllParentLevelList();

            //List<KeyValue> listKV = new List<KeyValue>();

            //foreach (var item in list)
            //{
            //    KeyValue obj = new KeyValue();

            //    obj.Key = item.Name + " - " + item.BLevelGroup.Name;
            //    obj.Value = item.LevelId.ToString();
            //    listKV.Add(obj);
            //}

            //cbLevelException.DataSource = listKV;
            //cbLevelException.DataBind();            

        }

        private void LoadCheckedItemsInCheckBox()
        {
            //List<BLevel> list = NewPayrollManager.GetAllParentLevelList();

            //List<KeyValue> listKV = new List<KeyValue>();

            //foreach (var item in list)
            //{
            //    KeyValue obj = new KeyValue();

            //    obj.Key = item.Name + " - " + item.BLevelGroup.Name;
            //    obj.Value = item.LevelId.ToString();
            //    listKV.Add(obj);
            //}

            //cbLevelException.DataSource = listKV;
            //cbLevelException.DataBind();

            OvertimeRounding roundingInstance = new OvertimeRounding();
            roundingInstance = OvertimeManager.getTopRounding();

            List<OvertimeRuleMinTimeBufferNotApplicableTo> listOT = roundingInstance.OvertimeRuleMinTimeBufferNotApplicableTos.ToList();
            //foreach (ListItem item in cbLevelException.Items)
            //{
            //    if (listOT.Count > 0 && listOT.Any(x => x.LevelId == int.Parse(item.Value)))
            //    {
            //        item.Selected = true;
            //    }
            //}
        }

        protected void btnHideTeamHRDetails_Click(object sender, EventArgs e)
        {
            bool isSuccess = OvertimeManager.UpdateHideTeamHRDetails(chkHideTeamHRDetails.Checked, chkEnableJoiningDate.Checked,chkEINEditable.Checked
                ,chkShowReviewDashboard.Checked);
            if (isSuccess)
            {
                msgInfo.InnerHtml = "HR setting changed.";
                msgInfo.Hide = false;
            }

        }
    }

   
}
