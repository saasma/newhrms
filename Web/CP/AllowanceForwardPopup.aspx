﻿<%@ Page Language="C#" AutoEventWireup="true" Title="Edit Allowance" MasterPageFile="~/Master/ForPopupPage.Master"
    CodeBehind="AllowanceForwardPopup.aspx.cs" Inherits="Web.CP.AllowanceForwardPopup" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<%@ Register Assembly="DevExpress.Web.v16.1, Version=16.1.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function ConfirmInstitutionDeletion() {
            if (ddllist.selectedIndex == -1) {
                alert('No Insurance company to delete.');
                clearUnload();
                return false;
            }
            if (confirm('Do you want to delete the insurance company?')) {
                clearUnload();
                return true;
            }
            else {
                return false;
            }
        }

        function DisplayInstitutionInTextBox(dropdown_id) {

        }



        //capture window closing event


        function closePopup() {

            window.opener.refreshEventList(window);

        }

      
    </script>
    <style type="text/css">
    #ctl00_mainContent_btnHistory_Container a{color:Blue;}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <ext:ResourceManager DisableViewState="false" ScriptMode="Release" Namespace="" runat="server" />
    <asp:HiddenField ID="Hidden_CounterTypePrev" runat="server" />
    <asp:HiddenField ID="Hidden_FromPrev" runat="server" />
    <asp:HiddenField ID="Hidden_ToPrev" runat="server" />
    <asp:HiddenField ID="Hidden_DaysPrev" runat="server" />
    <div class="popupHeader">
        <span style='margin-left: 20px; padding-top: 8px; display: block' runat="server"
            id="title">Allowance : </span>
    </div>
    <div class="marginal" style="margin-top: 5px;">
        <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" Width="490px" runat="server" />
        <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Width="490px" Hide="true"
            runat="server" />
        <asp:HiddenField ID="Hidden_OldMinute" runat="server" />
        <asp:HiddenField ID="Hidden_RequestID" runat="server" />
        <table style="margin-bottom: 25px;" class="tbl">
            <tbody>
                <tr>
                    <tr>
                        <td colspan="2">
                            <ext:DisplayField FieldLabel="Position" LabelSeparator="" LabelAlign="Left" ID="lblPosition"
                                runat="server" Width="400" LabelWidth="100">
                            </ext:DisplayField>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <ext:DisplayField FieldLabel="Branch" LabelSeparator="" LabelAlign="Left" ID="lblBranch"
                                runat="server" Width="400" LabelWidth="100">
                            </ext:DisplayField>
                        </td>
                    </tr>
                    <td colspan="2">
                        <table>
                            <tr>
                                <td>
                                    <span style="margin-left: -3px">Allowance type</span>
                                </td>
                                <td style="padding-left: 19px;">
                                    <asp:DropDownList DataTextField="Name" DataValueField="EveningCounterTypeId" ID="ddlOvertimeType"
                                        runat="server" Style="width: 180px; margin-right: 25px;">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator5" ValidationGroup="SaveUpdate"
                                        Display="None" ErrorMessage="Overtime type is required." ControlToValidate="ddlOvertimeType"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:DateField ID="txtFromDate" runat="server" FieldLabel="From Date *" LabelAlign="Left"
                            LabelSeparator="" Width="250" LabelWidth="100" AllowBlank="false">
                            <DirectEvents>
                                <Select OnEvent="Date_Change">
                                </Select>
                            </DirectEvents>
                        </ext:DateField>
                        <asp:RequiredFieldValidator Enabled="true" Display="None" ID="valtxtPublicationName"
                            runat="server" ValidationGroup="SaveUpdate" ControlToValidate="txtFromDate" ErrorMessage="From date is required." />
                    </td>
                    <td style="padding-left: 40px">
                        <ext:DisplayField ID="txtFromDateNep" runat="server" FieldLabel=" " LabelAlign="top"
                            LabelSeparator="" Width="175" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:DateField ID="txtToDate" runat="server" FieldLabel="To Date *" LabelAlign="Left"
                            LabelSeparator="" Width="250" LabelWidth="100" Hidden="false" AllowBlank="false">
                            <DirectEvents>
                                <Select OnEvent="Date_Change">
                                </Select>
                            </DirectEvents>
                        </ext:DateField>
                        <asp:RequiredFieldValidator Enabled="true" Display="None" ID="RequiredFieldValidator111"
                            runat="server" ValidationGroup="SaveUpdate" ControlToValidate="txtToDate" ErrorMessage="To Date is required." />
                    </td>
                    <td style="padding-left: 40px">
                        <ext:DisplayField ID="txtToDateNep" runat="server" FieldLabel=" " LabelAlign="top"
                            LabelSeparator="" Width="175" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:NumberField ID="txtDays" AllowDecimals="false" runat="server" FieldLabel="Units *"
                            LabelAlign="Left" LabelSeparator=" " Width="250" LabelWidth="100" Hidden="false"
                            AllowBlank="false" MinValue="0">
                        </ext:NumberField>
                        <asp:RequiredFieldValidator Enabled="true" Display="None" ID="RequiredFieldValidator11"
                            runat="server" ValidationGroup="SaveUpdate" ControlToValidate="txtDays" ErrorMessage="Unit(s) is required." />
                    </td>
                    <td style="padding-left: 40px">
                        <ext:DisplayField Text="" LabelAlign="Top" ID="lblRate" runat="server" Width="250"
                            LabelWidth="100">
                        </ext:DisplayField>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <ext:DisplayField FieldLabel="Reason" LabelSeparator="" LabelAlign="Left" ID="lblReason"
                            runat="server" Width="400" LabelWidth="100">
                        </ext:DisplayField>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <ext:DisplayField FieldLabel="Recommender" LabelSeparator="" LabelAlign="Left" ID="lblRecommendBy"
                            runat="server" Width="500" LabelWidth="100">
                        </ext:DisplayField>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <ext:DisplayField FieldLabel="Approval" LabelSeparator="" LabelAlign="Left" ID="lblApprovedBy"
                            runat="server" Width="250" LabelWidth="100">
                        </ext:DisplayField>
                    </td>
                </tr>
                <tr>
                    <td style="padding-top: 10px;">
                        <ext:LinkButton runat="server"  ID="btnHistory" Text="Allowance History">
                            <DirectEvents>
                                <Click OnEvent="btnHistory_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                        </ext:LinkButton>
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="tbl">
            <tr>
                <td>
                    <asp:Button ID="btnUpdate" ValidationGroup="SaveUpdate" runat="server" OnClientClick="valGroup='SaveUpdate';  if(confirm('Are you sure you want to update this request?')) return CheckValidation(); else return false;"
                        Text="Update" Width="100" Height="30" Style="margin-top: 15px;" OnClick="btnUpdate_Click"
                        CssClass="save" />
                </td>
                <td>
                    <asp:Button ID="btnReject" ValidationGroup="SaveUpdate" runat="server" OnClientClick="valGroup='SaveUpdate';  if(confirm('Are you sure you want to reject this request?')) return CheckValidation(); else return false; "
                        Text="Reject" Width="100" Height="30" Style="margin-top: 15px;" OnClick="btnReject_Click"
                        CssClass="save" />
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td colspan="10">
                    <cc2:EmptyDisplayGridView CssClass="tableLightColor" UseAccessibleHeader="true" ShowHeaderWhenEmpty="True"
                        ID="gridHistory" runat="server" AutoGenerateColumns="False" CellPadding="4" GridLines="None"
                        ShowFooterWhenEmpty="False">
                        <RowStyle BackColor="#E3EAEB" />
                        <Columns>
                            <%--<asp:TemplateField HeaderText="Select" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                <HeaderTemplate>
                                    <asp:CheckBox ID="chk12Delete1" onclick="selectDeselectAll(this)" runat="server" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox Visible='<%# Eval("Status").ToString() == "2" ? true : false %>' ID="chkDelete"
                                        runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                            <asp:BoundField HeaderStyle-Width="160px" HeaderStyle-HorizontalAlign="Left" HeaderText="Modified by"
                                DataField="ModifiedByName"></asp:BoundField>
                            <asp:BoundField DataField="ModifiedOn" DataFormatString="{0:dd-M-yyyy}" HeaderStyle-Width="125px"
                                HeaderStyle-HorizontalAlign="Left" HeaderText="Modified on"></asp:BoundField>
                            <asp:BoundField DataField="Column1Before" HeaderStyle-Width="160px" HeaderStyle-HorizontalAlign="Left"
                                HeaderText="Before"></asp:BoundField>
                            <asp:BoundField DataField="Column1After" HeaderStyle-Width="160px" HeaderStyle-HorizontalAlign="Left"
                                HeaderText="After"></asp:BoundField>
                        </Columns>
                        <RowStyle CssClass="odd" />
                        <AlternatingRowStyle CssClass="even" />
                        <EmptyDataTemplate>
                            <b>No list. </b>
                        </EmptyDataTemplate>
                    </cc2:EmptyDisplayGridView>
                </td>
            </tr>
        </table>
    </div>
    <ext:Window ButtonAlign="Left" runat="server" Layout="BorderLayout" ID="windowNew"
        Title="History" Width="580" Height="300" Hidden="true">
        <Items>
            <ext:GridPanel Region="Center" ID="grid" runat="server" Cls="itemgrid" Scroll="Both">
                <Store>
                    <ext:Store ID="Store2" runat="server" AutoLoad="true">
                        <Model>
                            <ext:Model ID="Model4" runat="server">
                                <Fields>
                                    <ext:ModelField Name="AllowanceName" Type="String" />
                                    <ext:ModelField Name="StartDate" Type="String" />
                                    <ext:ModelField Name="EndDate" Type="String" />
                                    <ext:ModelField Name="Units" Type="String" />
                                    <ext:ModelField Name="Status" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <Features>
                    <ext:Grouping StartCollapsed="false" IsDynamic="true" ID="Group1" runat="server"
                        HideGroupedHeader="false" EnableGroupingMenu="true" />
                </Features>
                <ColumnModel>
                    <Columns>
                        <ext:Column ID="Column4" Sortable="true" MenuDisabled="false" runat="server" Text="Type"
                            Width="130" Align="Left" DataIndex="AllowanceName">
                        </ext:Column>
                        <ext:Column ID="colEmployeeName" Sortable="true" MenuDisabled="false" runat="server"
                            Text="Start Date" Width="150" Align="Left" DataIndex="StartDate">
                        </ext:Column>
                        <ext:Column ID="Column1" Sortable="true" MenuDisabled="false" runat="server" Text="End Date"
                            Width="150" Align="Left" DataIndex="EndDate">
                        </ext:Column>
                        <ext:Column ID="Column5" Sortable="true" MenuDisabled="false" runat="server" Text="Units"
                            Width="60" Align="Center" DataIndex="Units">
                        </ext:Column>
                        <ext:Column ID="Column6" Sortable="true" MenuDisabled="false" runat="server" Text="Status"
                            Width="120" Align="Left" DataIndex="Status">
                        </ext:Column>
                    </Columns>
                </ColumnModel>
                <View>
                    <ext:GridView ID="GridView1" runat="server" StripeRows="true" />
                </View>
            </ext:GridPanel>
        </Items>
        <%--<Buttons>
                <ext:Button ID="btnClearSelection" StyleSpec="margin-left:10px" runat="server" Cls="btn btn-primary"
                    Text="Cancel" Width="120">
                    <Listeners>
                        <Click Handler="#{windowNew}.hide();" />
                    </Listeners>
                </ext:Button>
            </Buttons>--%>
    </ext:Window>
</asp:Content>
