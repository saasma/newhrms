﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;

namespace Web.CP
{
    public partial class WorkShiftHistory : BasePage
    {
        private CommonManager comm = new CommonManager();
        private List<EWorkShiftHistory> list;

     
        protected void Page_Load(object sender, EventArgs e)
        {
            this.CustomId = int.Parse(Request.QueryString["EIN"]);          
           
            if (!IsPostBack)
            {
                Initialise();               
            }
            Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "clearUnload();");
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (gvw.SelectedIndex == -1)
                RegisterLastFromDate();
            else
            {
                int currentHistory = (int)gvw.DataKeys[gvw.SelectedIndex][0];

                EWorkShiftHistory prevHistory = CommonManager.GetPreviousWorkShiftHistory(currentHistory);

                if (prevHistory != null)
                {
                    RegisterLastFromDateScript(prevHistory);
                }
            }

            List<EWorkShiftHistory> list = CommonManager.GetWorkShiftHistory(CustomId);
            int id = -1;
            if (list.Count > 0)
                id = list[list.Count - 1].WorkShiftId.Value;

            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "changedValue", "var value=" + id + ";", true);

        }
        private void Initialise()
        {

            ddlGrade.DataSource = comm.GetAllWorkShift();
            ddlGrade.DataBind();

            calFromDate.IsEnglishCalendar = IsEnglish; calFromDate.SelectTodayDate();

            BindData();

           

            

        }

        private void RegisterLastFromDate()
        {
            List<EWorkShiftHistory> list = CommonManager.GetWorkShiftHistory(CustomId);

            if (list.Count > 0)
            {
                EWorkShiftHistory lastHistory = list[list.Count - 1];

                RegisterLastFromDateScript(lastHistory);

            }

        }

        public bool IsEditable(int index)
        {
            if (list != null && list.Count>0)
            {
                if (list.Count - 1 == index)
                    return true;
            }
            return false;
        }

        private void RegisterLastFromDateScript(EWorkShiftHistory lastHistory)
        {
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "regfromdate",
               string.Format("var fromDate = '{0}';", lastHistory.FromDate), true);
        }

        private void BindData()
        {
            PayManager mgr = new PayManager();


            list = CommonManager.GetWorkShiftHistory(CustomId);
            gvw.DataSource = list;
            gvw.DataBind();


           
        }

       

        protected void gvw_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvw.PageIndex = e.NewPageIndex;
            BindData();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

            if (Page.IsValid)
            {
                EWorkShiftHistory history = Process(null);
                if (gvw.SelectedIndex != -1)
                {
                    
                    history.WorkShiftHistoryId = (int)gvw.DataKeys[gvw.SelectedIndex][0];
                    CommonManager.UpdateWorkShiftHistory(history, gvw.Rows.Count == 1);
                    
                    divMsgCtl.InnerHtml = Resources.Messages.WorkShiftHistoryUpdated;
                }
                else
                {
                    CommonManager.SaveWorkShiftHistory(history);
                    divMsgCtl.InnerHtml = Resources.Messages.WorkShiftHistorySaved;
                }
                divMsgCtl.Hide = false;

                ClearFields();
                gvw.SelectedIndex = -1;
                BindData();
                RegisterLastFromDate();
            }
        }

        private EWorkShiftHistory Process(EWorkShiftHistory entity)
        {
            if (entity == null)
            {
                entity = new EWorkShiftHistory();
                entity.WorkShiftId = int.Parse(ddlGrade.SelectedValue);
                entity.FromDate = calFromDate.SelectedDate.ToString();
                entity.FromDateEng = GetEngDate(entity.FromDate);
                entity.EmployeeId = this.CustomId;
                return entity;
            }
            else
            {
                UIHelper.SetSelectedInDropDown(ddlGrade, entity.WorkShiftId);
                calFromDate.SetSelectedDate(entity.FromDate, IsEnglish);
            }
            return null;
        }

        protected void gvw_SelectedIndexChanged(object sender, EventArgs e)
        {
            //first record 
            int currentGradeHistory = (int)gvw.DataKeys[gvw.SelectedIndex][0];

            //EGradeHistory prevGradeHistory = CommonManager.GetPreviousGradeHistory(currentGradeHistory);

            //if (prevGradeHistory != null)
            //{
            //    RegisterLastFromDateScript(prevGradeHistory);
            //}


            EWorkShiftHistory history = CommonManager.GetWorkShiftHistoryById(currentGradeHistory);
            Process(history);
            btnSave.Text = Resources.Messages.Update;

            //if first one then disable from date as it should be the same of emp first status from date
            if (CommonManager.GetWorkShiftHistory(this.CustomId)[0].WorkShiftHistoryId == history.WorkShiftHistoryId)
            {
                calFromDate.ToolTip = Resources.Messages.HistoryFirstFromDateNotEditable;
                calFromDate.Enabled = false;
            }
            else
            {
                calFromDate.Enabled = true; ;
                calFromDate.ToolTip = "";
            }

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            gvw.SelectedIndex = -1;
            BindData();
            ClearFields();
        }


        private void ClearFields()
        {
            calFromDate.Enabled = true;
            calFromDate.ToolTip = "";
            btnSave.Text = Resources.Messages.Save;
            UIHelper.SetSelectedInDropDown(ddlGrade, -1);
            calFromDate.SelectTodayDate();
        }

      
    }
}
