<%@ Page Title="Approve/Signoff Payroll" Language="C#" MasterPageFile="~/Master/Details.Master"
    AutoEventWireup="true" CodeBehind="PayrollSignOff.aspx.cs" Inherits="Web.User.PayrollSignOff" %>

<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningMsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="InfoMsgCtl" TagPrefix="uc2" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        skipLoadingCheck = true;
       
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <div class="contentArea">
        <h3>
            Payroll Approval/Sign Off
        </h3>
        <div>
            <div style="color: #D95911">
                You are about to authorize Payroll Processing for this month.
            </div>
            <div style="color: #5BACEA; padding-top: 5px; padding-bottom: 5px">
                Please review the following changes made before you approve the Payroll Processing.
            </div>
        </div>
        <uc2:MsgCtl ID="msgCtl" EnableViewState="false" Hide="true" runat="server" />
        <asp:Panel runat="server" DefaultButton="btnLoad" class="attribute">
            Show only &nbsp;
            <asp:DropDownList ID="ddlType" runat="server">
                <asp:ListItem Text="--Select Type--" Value="-1" Selected="True" />
                <asp:ListItem Text="Advance" Value="15" />
                <asp:ListItem Text="CIT" Value="11" />
                <asp:ListItem Text="Deduction" Value="2" />
                <asp:ListItem Text="Employee Added" Value="22" />
                <asp:ListItem Text="Gender" Value="23" />
                <asp:ListItem Text="Income" Value="1" />
                <asp:ListItem Text="Increment" Value="4" />
                <asp:ListItem Text="Insurance" Value="3" />
                <asp:ListItem Text="Loan" Value="14" />
                <asp:ListItem Text="Medical Tax Credit" Value="13" />
                <asp:ListItem Text="No CIT" Value="12" />
                <asp:ListItem Text="Optimum CIT" Value="17" />
                <asp:ListItem Text="PF" Value="7" />
                <asp:ListItem Text="Status" Value="10" />
                <asp:ListItem Text="Tax Status" Value="24" />
            </asp:DropDownList>
            &nbsp; From &nbsp;
            <My:Calendar Id="calFilterFrom" runat="server" />
            Time &nbsp;
            <asp:TextBox ID="txtTime" runat="server" Width="60px"></asp:TextBox>
            <cc1:MaskedEditExtender ID="meeStartTime" runat="server" AcceptAMPM="true" MaskType="Time"
                Mask="99:99" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                ErrorTooltipEnabled="true" UserTimeFormat="None" TargetControlID="txtTime" InputDirection="LeftToRight"
                AcceptNegative="Left">
            </cc1:MaskedEditExtender>
            <cc1:MaskedEditValidator ID="mevStartTime" runat="server" ControlExtender="meeStartTime"
                ControlToValidate="txtTime" Display="Dynamic" ValidationGroup="MKE" />
            Search &nbsp;
            <asp:TextBox ID="txtSearch" runat="server" />
            &nbsp;
            <asp:LinkButton ID="btnLoad" runat="server" OnClick="btnLoad_Click" Text="Load" CssClass="load"
                Style='float: right; padding-right: 30px' />
        </asp:Panel>
        <div style="clear: both">
            <%--Employee/Monetory change log--%>
            <cc2:EmptyDisplayGridView Width="100%" Style='clear: both' PagerStyle-HorizontalAlign="Center"
                PagerStyle-CssClass="defaultPagingBar" CssClass="tableLightColor" UseAccessibleHeader="true"
                ShowHeaderWhenEmpty="True" ID="gvwProjects" runat="server" AutoGenerateColumns="False"
                CellPadding="4" GridLines="None" ShowFooterWhenEmpty="False" OnPageIndexChanged="gvwRoles_PageIndexChanged"
                OnPageIndexChanging="gvwRoles_PageIndexChanging" OnSelectedIndexChanged="gvwRoles_SelectedIndexChanged"
                OnRowDeleting="gvwRoles_RowDeleting">
                <Columns>
                    <asp:TemplateField HeaderText="EIN" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="20px">
                        <ItemTemplate>
                            <%# Eval("EmployeeName").ToString()=="" ? "" : Eval("EIN")%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Employee Name" HeaderStyle-HorizontalAlign="Left"
                        HeaderStyle-Width="100px">
                        <ItemTemplate>
                            <%# Eval("EmployeeName")%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Action" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="30px">
                        <ItemTemplate>
                            <%# GetAction(Eval("Action")).ToString()%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Type" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="120px">
                        <ItemTemplate>
                            <%# GetType(Eval("Type"),Eval("SubType"))%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Before" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"
                        HeaderStyle-Width="100px">
                        <ItemTemplate>
                            <%# Eval("Before") == null ? "-" : (Eval("Before"))%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="After" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"
                        HeaderStyle-Width="100px">
                        <ItemTemplate>
                            <%# Eval("After") == null ? "-" : (Eval("After"))%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Name" FooterStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Left"
                        HeaderStyle-Width="100px">
                        <ItemTemplate>
                            <%# Eval("Name")%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Date:Time" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="90px">
                        <ItemTemplate>
                            <%# Eval("Date")%></ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <PagerSettings Mode="NumericFirstLast" NextPageText="Next �" PreviousPageText="� Prev"
                    FirstPageText="First" LastPageText="Last" />
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                <PagerStyle CssClass="defaultPagingBar" />
                <SelectedRowStyle CssClass="selected" />
                <EmptyDataTemplate>
                    <b>No change logs. </b>
                </EmptyDataTemplate>
            </cc2:EmptyDisplayGridView>
            <uc1:PagingCtl ID="pagintCtl" OnNextRecord="btnNext_Click" OnPrevRecord="btnPrevious_Click"
                OnDropDownShowRecordsChanged="ddlRecords_SelectedIndexChanged" runat="server" />
        </div>
        <div style='margin-top: 15px'>
            <asp:Button ID="btnRole" CssClass="save" runat="server" Text="Export" OnClick="btnExport_Click" />
            <%--<asp:Button ID="btnSignOff" CssClass="update" runat="server" OnClientClick='return confirm("Confirm payroll signoff/approval?");'
                Text="Sign off/Approve" Width="150px" OnClick="btnSignOff_Click" />--%>
        </div>
    </div>
</asp:Content>
