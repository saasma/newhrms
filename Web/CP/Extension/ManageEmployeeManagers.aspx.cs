﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using DAL;
using BLL;
using Utils.Helper;

namespace Web.User
{
    public partial class ManageEmployeeManagers : BasePage
    {

        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.HumanResource;
            }
        }

        //double percentTotal = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            BindEmployeeList();
            BindProjectInDDL();
            if (!IsPostBack)
            {
                Initialise();

            }

            JavascriptHelper.AttachNonDialogPopUpCode(Page, "shiftPopup", "../../ExcelWindow/AssociateEmployeeManagerExcel.aspx", 450, 500);
            SetPagePermission();
        }
        /// <summary>
        /// 
        /// </summary>
        public void SetPagePermission()
        {
            if (SessionManager.IsCustomRole && SessionManager.CustomRole.IsReadOnly != null && SessionManager.CustomRole.IsReadOnly.Value)
            {
                btnExport.Visible = false;
                //btnInsertUpdate.Visible = false;
                gvwProjects.Columns[4].Visible = false;
               // fieldset.Visible = false;

            }
        }
        private void Initialise()
        {

            ddlBranch.DataSource
             = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            ddlBranch.DataBind();

            //ddlProjectOrTeam.DataSource = LeaveRequestManager.GetAllLeaveProjectList().ToList();

            //ddlProjectOrTeam.DataBind();


            //ddlEmployee.DataSource
           
            //ddlEmployee.DataBind();

            //BindProjectInDDL();

            BindProjects();
        }

        private void BindEmployeeList()
        {
          //  storeEmployee.DataSource = EmployeeManager.GetAllEmployees().OrderBy(x => x.Name);
           // storeEmployee.DataBind();
        }

        private void BindProjectInDDL()
        {
            //ListItem item = ddlProject.Items[0];

            //ddlProjectOrTeam.SelectedItem.Text = "";

            //int total = 0;
            //storeTeamOrProject.DataSource = LeaveRequestManager.GetProjectList
            //    ("", 1, 9999, ref total).OrderBy(x => x.Name);
            //storeTeamOrProject.DataBind();

            //ddlProject.Items.Insert(0, item);
        }

        protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListItem firstItem = ddlDepartment.Items[0];
            ddlDepartment.Items.Clear();



            ddlDepartment.DataSource
                = DepartmentManager.GetAllDepartmentsByBranch(int.Parse(ddlBranch.SelectedValue));

            ddlDepartment.DataBind();
            ddlDepartment.Items.Insert(0, firstItem);



        }
    

        void BindProjects()
        {


             

            int totalRecords = 0;

            List<GetEmployeesManagersListResult> list =
                LeaveRequestManager.GetEmployeesManagersList1(
                txtSearch.Text.Trim(),pagintCtl.CurrentPage,int.Parse(pagintCtl.DDLRecords.SelectedValue), ref totalRecords,
                int.Parse(ddlDepartment.SelectedValue));



            gvwProjects.DataSource = list;       
            gvwProjects.DataBind();

          
            pagintCtl.UpdatePagingBar(totalRecords);
          
        }

        protected void gvwRoles_PageIndexChanged(object sender, EventArgs e)
        {

        }

        protected void btnLoad_Click(object sender, EventArgs e)
        {
            pagintCtl.CurrentPage = 1;

            gvwProjects.SelectedIndex = -1;
            BindProjectInDDL();
            Clear();
          

            BindProjects();
        }


        protected void btnPrevious_Click()
        {
            pagintCtl.CurrentPage -= 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage -= 1;
            BindProjects();
        }

        protected void btnNext_Click()
        {
            pagintCtl.CurrentPage += 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage += 1;
            BindProjects();
        }


        protected void ddlRecords_SelectedIndexChanged()
        {
            //SessionManager.EmployeeDisplayRecodsPerPage = int.Parse(pagintCtl.DDLRecords.SelectedValue);
            //_tempCurrentPage = 1;
            pagintCtl.CurrentPage = 1;
            BindProjects();
        }

        public bool CanChange(object value)
        {
            return Convert.ToBoolean(value);
        }


        protected void gvwRoles_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            //gvwRoles.PageIndex = e.NewPageIndex;
            //gvwRoles.SelectedIndex = -1;
            BindProjects();
            Clear();
        }
        
        protected void ddlUserType_SelectedIndex(object sender, EventArgs e)
        {
           
        }


        protected void btnInsertUpdate_Click(object sender, EventArgs e)
        {
            if (this.IsValid)
            {
                //LeaveProjectEmployee project = new LeaveProjectEmployee();
               
                //project.LeaveProjectId = int.Parse(ddlProjectOrTeam.SelectedItem.Value);
                //project.EmployeeId = int.Parse(ddlProjectManagerEmployee.SelectedItem.Value);

             

                //if (LeaveRequestManager.SaveUpdateEmployeeProject(project))
                //{
                //    if (gvwProjects.SelectedIndex == -1)
                //        msgInfo.InnerHtml = "Information saved.";
                //    else
                //        msgInfo.InnerHtml = "Information updated.";
                //    msgInfo.Hide = false;
                //    btnCancel_Click(null, null);
                //    BindProjects();
                //}


            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            //txtUserName.Enabled = ddlEmployeeList.Enabled = true;
            //ddlEmployeeList.ClearSelection();
            //ddlEmployeeList.Items[0].Selected = true;
            gvwProjects.SelectedIndex = -1;
            BindProjects();
            Clear();
        }

        void Clear()
        {
         //   ddlProjectManagerEmployee.SelectedItem.Value = "";
            //ddlEmployee.ClearSelection();
            //ddlEmployee.SelectedValue = "-1";
           // ddlProjectOrTeam.SelectedItem.Value = "";
            //txtPercent.Text = "";
            //txtUserName.Text = "";
            //txtEmail.Text = "";
            //chkIsActive.Checked = true; 

            FinancialDate currentYear = SessionManager.CurrentCompanyFinancialDate;
            //ddlEmployee.Enabled = true;


          //  btnInsertUpdate.Text = Resources.Messages.Save;
            //this.CustomId = 0;
        }

        protected void gvwRoles_SelectedIndexChanged(object sender, EventArgs e)
        {
            int projectId = (int)gvwProjects.DataKeys[gvwProjects.SelectedIndex]["LeaveProjectEmployeeId"];
            int employeeId = (int)gvwProjects.DataKeys[gvwProjects.SelectedIndex]["EmployeeId"];

           

            LeaveProjectEmployee project = LeaveRequestManager.GetEmployeeLeaveProject(projectId);

            if (project != null)
            {

                //ddlProject.SelectedValue = project.LeaveProjectId.ToString();
           //     ddlProjectOrTeam.SetValue(project.LeaveProjectId);
                //ddlEmployee.SelectedValue = project.EmployeeId.ToString();
              //  ddlProjectManagerEmployee.SetValue(project.EmployeeId);

                //ddlEmployee.Enabled = false;

                //if (project.Percent.HasValue)
                //    txtPercent.Text = project.Percent.Value.ToString();

              //  btnInsertUpdate.Text = Resources.Messages.Update;


            }
            else
            {
                //ddlEmployee.SelectedValue = employeeId.ToString();
              //  ddlProjectManagerEmployee.SetValue(employeeId);

               // ddlEmployee.Enabled = false;

             //   btnInsertUpdate.Text = Resources.Messages.Update;
            }

        }
        protected void gvwEmployees_RowCreated(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover",
                "this.originalClass=this.className;this.className='selected'");

                e.Row.Attributes.Add("onmouseout",
                "this.className=this.originalClass;");


            }

        }
        protected void gv_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if ((e.Row.RowState & DataControlRowState.Edit) > 0)
                {

                    int employeeId = (int)gvwProjects.DataKeys[e.Row.RowIndex]["EmployeeId"];

                    DropDownList ddList = (DropDownList)e.Row.FindControl("ddlProjectOrTeam1");

                    ddList.DataSource = EmployeeManager.GetAllActiveEmployees().Where(x => x.EmployeeId != employeeId).ToList();
                    ddList.DataBind();

                    EEmployee emp = new EmployeeManager().GetById(employeeId);
                    if (emp != null && emp.EHumanResources.FirstOrDefault()!=null && emp.EHumanResources.FirstOrDefault().ReportToEmployeeId!=null)
                    {
                        ListItem item = ddList.Items.FindByValue(emp.EHumanResources.FirstOrDefault().ReportToEmployeeId.ToString());
                        if (item != null)
                            item.Selected = true;
                    }
                }
            }
        }

        protected void gvwProjects_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvwProjects.EditIndex = e.NewEditIndex;
            int employeeId= (int)gvwProjects.DataKeys[e.NewEditIndex]["EmployeeId"];


            BindProjects();
            //DropDownList ddl = gvwProjects.Rows[e.NewEditIndex].FindControl("ddlProjectOrTeam1") as DropDownList;

           
        }

        protected void gvwProjects_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            
            
            int employeeId = (int)gvwProjects.DataKeys[e.RowIndex]["EmployeeId"];

            DropDownList ddList = gvwProjects.Rows[e.RowIndex].FindControl("ddlProjectOrTeam1") as DropDownList;

            if (ddList.SelectedItem == null)
            {
                msgWarning.InnerHtml = "Team should be selected.";
                msgWarning.Hide = false;
                return;
            }

            int ManagerID = int.Parse(ddList.SelectedItem.Value);




            if (EmployeeManager.UpdateReportsTo(employeeId, ManagerID).IsSuccess)
            {
                //if (gvwProjects.SelectedIndex == -1)
                //    msgInfo.InnerHtml = "Information saved.";
                //else
                    msgInfo.InnerHtml = "Information updated.";
                msgInfo.Hide = false;
               // btnCancel_Click(null, null);
                gvwProjects.EditIndex = -1;
                BindProjects();
            }

        }

    
      
    }
}
