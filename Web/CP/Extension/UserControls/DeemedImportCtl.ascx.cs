﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using BLL;
using DAL;
using Ext.Net;
using Utils.Web;
using Utils.Helper;
using System.Text;
using Newtonsoft.Json.Linq;
using BLL.BO;
using BLL.Entity;

namespace Web.CP.Extension.UserControls
{
    public partial class DeemedImportCtl : BaseUserControl
    {
        CommonManager commonMgr = new CommonManager();
        public bool isNsetDashainAmountImport = true;
        public bool IsDeduction
        {
            get
            {
                return isDeduction;
            }
            set
            {
                isDeduction = value;
            }
        }
        public bool isDeduction = false;

        protected void Page_Load(object sender, EventArgs e)
        {

           

            if (!IsPostBack && !X.IsAjaxRequest)
                Initialise();

            JavascriptHelper.AttachNonDialogPopUpCode(Page, "shiftPopup", "../../ExcelWindow/DeemedSalaryExcel.aspx", 450, 500);

            
            // programatic client-side calls to __doPostBack() can bypass this
            Page.ClientScript.RegisterOnSubmitStatement(GetType(), "ServerForm",
                                                    "if (this.submitted) return false; this.submitted = true; return true;");

          
        }

        public void Initialise()
        {
            cmbDeemedIncomeList.Store[0].DataSource = PayManager.GetDeemedIncomeList();
            cmbDeemedIncomeList.Store[0].DataBind();

            //int totalRecords = 0;
            //BindList(pagingCtl.CurrentPage - 1, int.Parse(pagingCtl.DDLRecords.SelectedValue), ref totalRecords);
            storeProjects.Reload();
        }

        [DirectMethod]
        public object SaveRow(string EmployeeID, bool isPhantom, JsonObject values)
        {
            var myJsonString = values.ToString();
            var strJObject = JObject.Parse(myJsonString);
            List<GetEmployeesIncomeForVariableIncomesResult> projectPays = new List<GetEmployeesIncomeForVariableIncomesResult>();
            //int indx = 0;
            //foreach (System.Web.UI.WebControls.ListItem item in chkVariableIncomes.Items)
            //{
            //    if (chkVariableIncomes.Items[indx].Selected == true)
            //    {
            //        GetEmployeesIncomeForVariableIncomesResult projectPay = new GetEmployeesIncomeForVariableIncomesResult();
            //        projectPay.IncomeId = int.Parse(item.Value);
            //        projectPay.EmployeeId = int.Parse(EmployeeID);
            //        if (values["P_" + item.Value] != null)
            //            projectPay.Amount = decimal.Parse(values["P_" + item.Value].ToString());
            //        projectPays.Add(projectPay);
            //    }
            //    indx++;
            //}
                       var list = new List<PEmployeeIncome>();
                       var income = new PayManager().GetIncomeById(int.Parse(cmbDeemedIncomeList.SelectedItem.Value));
                       var empIncome = new PEmployeeIncome();
                        empIncome.EmployeeId = int.Parse(EmployeeID);
                        empIncome.Name = "";//name;

                        if (income.DeemedIsAmount.Value)
                        {
                            empIncome.MarketValue = decimal.Parse(values["DeemedAmount"].ToString());
                            //ExcelGenerator.GetAmountValueFromCell("Amount", row, employeeId);
                            empIncome.Amount = 0;
                            empIncome.MarketValue = decimal.Parse(empIncome.MarketValue.Value.ToString("n2"));
                        }
                        else
                        {
                            var rate = values["RatePercent"].ToString();//ExcelGenerator.GetAmountValueFromCell("Rate %", row, employeeId);
                            if (rate == null)
                            {
                                empIncome.RatePercent = 0;
                            }
                            else
                            {
                                empIncome.RatePercent = double.Parse(rate);
                            }
                        }
                    list.Add(empIncome);
                    var count = 0;
                    ResponseStatus status = PayManager.SaveDeemedIncome(list,  ref count, income);

            //int count = 0;
            
            if (status.IsSuccessType == true)
            {
                // NewMessage.ShowNormalMessage("The Information has been updated");
                Notification.Show(new NotificationConfig
                {
                    Title = "Notification",
                    Icon = Icon.Information,
                    AutoHide = true,
                    Html = "The Information has been updated",
                });
                return new { valid = true };
            }
            else
                return new { valid = false };
        }

        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            List<PEmployeeIncome> data = new List<PEmployeeIncome>();
            if (cmbDeemedIncomeList.SelectedItem == null || string.IsNullOrEmpty(cmbDeemedIncomeList.SelectedItem.Value))
            {
            }
            else
            {
                PIncome income = new PayManager().GetIncomeById(int.Parse(cmbDeemedIncomeList.SelectedItem.Value));

                if (income.DeemedIsAmount.Value)
                {
                    gridProjects.ColumnModel.Columns[3].Hidden = false;
                    gridProjects.ColumnModel.Columns[4].Hidden = true;
                }
                else
                {
                    gridProjects.ColumnModel.Columns[3].Hidden = true;
                    gridProjects.ColumnModel.Columns[4].Hidden = false;
                }

                int? employeeid = null;
                if (!string.IsNullOrEmpty(cmbEmpSearch.SelectedItem.Value))
                    employeeid = int.Parse(cmbEmpSearch.SelectedItem.Value);
                if (employeeid != null)
                    data = PayManager.GetPEmployeeIncomes(int.Parse(cmbDeemedIncomeList.SelectedItem.Value)).Where(x => x.EmployeeId == employeeid).ToList();
                else
                    data = PayManager.GetPEmployeeIncomes(int.Parse(cmbDeemedIncomeList.SelectedItem.Value));
            }

            if (data.Any())
                e.Total = data.Count();
            else
                e.Total = 0;

            data = data.Skip((e.Page - 1) * int.Parse(cmbPageSize.SelectedItem.Value)).Take(int.Parse(cmbPageSize.SelectedItem.Value)).ToList();
            storeProjects.DataSource = data;
            storeProjects.DataBind();
        }

        public void BindList(int start, int limit, ref int totalrows)
        {

            List<PEmployeeIncome> data = new List<PEmployeeIncome>();

            if (cmbDeemedIncomeList.SelectedItem == null || string.IsNullOrEmpty( cmbDeemedIncomeList.SelectedItem.Value))
            {
               
            }
            else
            {
                PIncome income = new PayManager().GetIncomeById(int.Parse(cmbDeemedIncomeList.SelectedItem.Value));
                
                if (income.DeemedIsAmount.Value)
                {
                    gridProjects.ColumnModel.Columns[3].Hidden = false;
                    gridProjects.ColumnModel.Columns[4].Hidden = true;
                }
                else
                {
                    gridProjects.ColumnModel.Columns[3].Hidden = true;
                    gridProjects.ColumnModel.Columns[4].Hidden = false;
                }

                int? employeeid = null;
                if (!string.IsNullOrEmpty(cmbEmpSearch.SelectedItem.Value))
                    employeeid = int.Parse(cmbEmpSearch.SelectedItem.Value);
                if(employeeid!=null)
                data = PayManager.GetPEmployeeIncomes(int.Parse(cmbDeemedIncomeList.SelectedItem.Value)).Where(x=>x.EmployeeId==employeeid).ToList();
                else
                    data = PayManager.GetPEmployeeIncomes(int.Parse(cmbDeemedIncomeList.SelectedItem.Value));
            }



            start = (start * limit);

            totalrows = data.Count;
            //Paging code
            if (start >= 0 && limit > 0)
            {
                data = data.ToList().Skip(start).Take(limit).ToList();
            }

            storeProjects.DataSource = data;
            storeProjects.DataBind();

            //if (totalrows == 0)
            //    pagingCtl.Visible = false;
            //else
            //    pagingCtl.Visible = true;
            //pagingCtl.UpdatePagingBar(totalrows);

        }

        public void ddlDeemedIncomeList_Changed(object sender,EventArgs e)
        {
            
            //BindList(pagingCtl.CurrentPage - 1, int.Parse(pagingCtl.DDLRecords.SelectedValue), ref totalRecords);
            storeProjects.Reload();
        }

        public bool IsEnable(object ein)
        {
            if (IsPayrollSaved)
                return false;
            return !LeaveAttendanceManager.IsAttendanceSavedForEmployee(payrollPeriodId, Convert.ToInt32(ein));
        }


        public bool IsPayrollSaved = false;
        public int payrollPeriodId;
        public void BindOvertimes()
        {
            //int totalRecords = 0;
            //BindList(pagingCtl.CurrentPage - 1, int.Parse(pagingCtl.DDLRecords.SelectedValue), ref totalRecords);
            storeProjects.Reload();

            if (IsPayrollSaved)
            {
                btnExport.Visible = false;
                divWarningMsg.InnerHtml = Resources.ResourceTooltip.HPLAllowanceSalaryAlreadyGenerated;
                divWarningMsg.Hide = false;
            }
            else
            {
                btnExport.Visible = true;
            }

        }

        protected void ddlPayrollPeriods_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindOvertimes();
        }

        protected void gvwOvertimes_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            BindOvertimes();
        }


        protected void btnSave_Click1(object sender, EventArgs e)
        {
        }

        public void btnSave_DirectClick(object sender, DirectEventArgs e)
        {
            //int totalRecords = 0;
            //BindList(pagingCtl.CurrentPage - 1, int.Parse(pagingCtl.DDLRecords.SelectedValue), ref totalRecords);
            storeProjects.Reload();
        }

     
    }
}