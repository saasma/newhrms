﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using BLL;
using Ext.Net;
using Utils.Helper;
using DAL;
using BLL.BO;

namespace Web.CP.Extension.UserControls
{
    public partial class TargetsImportCtl : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack && !X.IsAjaxRequest)
            {
                Initialise();
            }

            JavascriptHelper.AttachNonDialogPopUpCode(Page, "shiftPopup", "../../ExcelWindow/TargetsImportExcel.aspx", 450, 500);

            var code = string.Format("var totalHoursOrDayInAMonth = {0}; ",
                CommonManager.CompanySetting.IsProjectInputType ? ProjectManager.TotalHourOrDaysInAMonth : 100);
            code = string.Format("var isDeduction = {0}; ", false);
            Page.ClientScript.RegisterStartupScript(GetType(), "sdffd", code, true);
        }

        public void Initialise()
        {
            //calDate.Text = CustomDate.GetTodayDate(IsEnglish).ToString();
            cmbYear.Store[0].DataSource = CommonManager.GetYearList();
            cmbYear.Store[0].DataBind();

            PayrollPeriod period = CommonManager.GetLastPayrollPeriod();

            ExtControlHelper.ComboBoxSetSelected(period.Year.ToString(), cmbYear);
            ExtControlHelper.ComboBoxSetSelected(period.Month.ToString(), cmbMonth);

            cmbMonth.Store[0].DataSource = DateManager.GetCurrentMonthList();
            cmbMonth.Store[0].DataBind();

            CreateAppraisalTargetColumns();
        }

        protected void CreateAppraisalTargetColumns()
        {
            var _AppraisalTargetsNames = AppraisalManager.GetAllAppraisalTargets();
            var columnId = "{0}";
            int col = 6;
            foreach (var item in _AppraisalTargetsNames)
            {

                var gridIndexId = string.Format(columnId, col);
                var field = new ModelField(gridIndexId, ModelFieldType.Float);
                field.UseNull = true;
                ModelProject.Fields.Add(gridIndexId, ModelFieldType.Float);
                gridProjects.ColumnModel.Columns.Add(GetExtGridPanelColumn(gridIndexId, item.Name));
                col++;
            }
        }

        private Column GetExtGridPanelColumn(string indexId, string headerName)
        {
            var column = new Column();
            column.DataIndex = indexId;
            column.DataIndex = indexId;
            column.Text = headerName;
            column.Align = Alignment.Center;
            column.ToolTip = headerName;
            column.MenuDisabled = true;
            column.Sortable = false;
            column.Width = new Unit(120);
            column.Renderer.Fn = "columnRenderer";
            column.Renderer.Fn = "amountRenderer";
            return column;
        }


        protected void Store_ReadDataDynamic(object sender, StoreReadDataEventArgs e)
        {
            this.storeProjects.RebuildMeta();
            List<EEmployee> employees = new List<EEmployee>();
            var data = new object[employees.Count];
            var _AppraisalTargetsNames = AppraisalManager.GetAllAppraisalTargets();
            employees = EmployeeManager.GetAllActiveEmployees();

            if (!string.IsNullOrEmpty(cmbEmpSearch.SelectedItem.Value))
            {
                employees = employees.Where(X => X.EmployeeId == int.Parse(cmbEmpSearch.SelectedItem.Value)).ToList();
            }

            int month = int.Parse(cmbMonth.SelectedItem.Value);
            int year = int.Parse(cmbYear.SelectedItem.Value);

            var _AppraisalEmployeeTargets = AppraisalManager.GetAppraisalEmployeeTargets(month,year);

            if (!string.IsNullOrEmpty(cmbEmpSearch.SelectedItem.Value))
            {
                _AppraisalEmployeeTargets = _AppraisalEmployeeTargets.Where(x => x.EmployeeId == int.Parse(cmbEmpSearch.SelectedItem.Value)).ToList();

            }

            var selection = new Dictionary<string, double?>();
            foreach (var item in _AppraisalEmployeeTargets)
            {
                selection.Add(item.EmployeeId + "_" + item.TargetID, item.Value);
            }

            data = new object[employees.Count];

            List<EDesignationBO> designationList = CommonManager.GetDesignationLevelList();
            List<BLevel> levelList = NewPayrollManager.GetAllParentLevelList();

            for (var i = 0; i < employees.Count; i++)
            {
                var rowData = new object[_AppraisalTargetsNames.Count + 6];
                var columnIndex = 0;
                rowData[columnIndex++] = employees[i].EmployeeId;
                rowData[columnIndex++] = employees[i].Name;
                //  int position = 6;
                EDesignationBO designation = designationList.FirstOrDefault(x => x.DesignationId == employees[i].DesignationId);
                rowData[columnIndex++] = designation.Name;

                BLevel _BLevel1 = levelList.First(x => x.LevelId == designation.LevelId);
                if (_BLevel1 != null)
                    rowData[columnIndex++] = _BLevel1.Name;
                else
                    rowData[columnIndex++] = "";
                if (employees[i].Branch != null)
                    rowData[columnIndex++] = employees[i].Branch.Name;//Branch
                else
                    rowData[columnIndex++] = "";

                if (employees[i].Department != null)
                    rowData[columnIndex++] = employees[i].Department.Name;//Department
                else
                    rowData[columnIndex++] = "";

                int k = 0;
                for (var j = 6; j < (_AppraisalTargetsNames.Count + 6); j++)
                {
                    if (selection.ContainsKey(employees[i].EmployeeId + "_" + _AppraisalTargetsNames[k].TargetID))
                    {
                        rowData[j] = selection[employees[i].EmployeeId + "_" + _AppraisalTargetsNames[k].TargetID];
                    }
                    else
                    {
                        rowData[j] = null;
                    }
                    k++;
                }

                data[i] = rowData;


                int eStartingPaging = 0;
                int PagingStart = 1;
                if (e.Start > 0)
                {
                    PagingStart = int.Parse(e.Parameters["page"]);
                    eStartingPaging = (storeProjects.PageSize * (PagingStart - 1));
                }

                //  this.storeProjects.RebuildMeta();
                if (data.Any())
                {
                    e.Total = data.Count();
                    if (e.Sort.Any())
                        if (X.IsAjaxRequest)
                        {
                            // this.GridPanel1.Features.Add(filterList);
                            this.gridProjects.Reconfigure();
                            X.AddScript(" if(storeProjects!=null) storeProjects.currentPage =" + PagingStart + ";");
                        }
                }
                storeProjects.DataSource = data;
                storeProjects.DataBind();
            }
        }
        protected void btnSave_Click1(object sender, EventArgs e)
        {
        }
        public void btnLoad_DirectClick(object sender, DirectEventArgs e)
        {
            storeProjects.Reload();
        }

    }
}
