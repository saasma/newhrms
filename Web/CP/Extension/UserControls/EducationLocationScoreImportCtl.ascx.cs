﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using BLL;
using Ext.Net;
using Utils.Helper;
using DAL;
using BLL.BO;

namespace Web.CP.Extension.UserControls
{
    public partial class EducationLocationScoreImportCtl : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack && !X.IsAjaxRequest)
            {
                Initialise();
            }

            JavascriptHelper.AttachNonDialogPopUpCode(Page, "shiftPopup", "../../ExcelWindow/EducationLocationScoreExcel.aspx", 450, 500);

            var code = string.Format("var totalHoursOrDayInAMonth = {0}; ",
                CommonManager.CompanySetting.IsProjectInputType ? ProjectManager.TotalHourOrDaysInAMonth : 100);
            code = string.Format("var isDeduction = {0}; ", false);
            Page.ClientScript.RegisterStartupScript(GetType(), "sdffd", code, true);
        }

        public void Initialise()
        {

            //CreateAppraisalTargetColumns();
            cmbPeriod.Store[0].DataSource = AppraisalManager.GetAllPeriods();
            cmbPeriod.Store[0].DataBind();
        }

        //protected void CreateAppraisalTargetColumns()
        //{
        //    var _AppraisalTargetsNames = AppraisalManager.GetAllAppraisalTargets();
        //    var columnId = "{0}";
        //    int col = 6;
        //    foreach (var item in _AppraisalTargetsNames)
        //    {

        //        var gridIndexId = string.Format(columnId, col);
        //        var field = new ModelField(gridIndexId, ModelFieldType.Float);
        //        field.UseNull = true;
        //        ModelProject.Fields.Add(gridIndexId, ModelFieldType.Float);
        //        gridProjects.ColumnModel.Columns.Add(GetExtGridPanelColumn(gridIndexId, item.Name));
        //        col++;
        //    }
        //}

        //private Column GetExtGridPanelColumn(string indexId, string headerName)
        //{
        //    var column = new Column();
        //    column.DataIndex = indexId;
        //    column.DataIndex = indexId;
        //    column.Text = headerName;
        //    column.Align = Alignment.Center;
        //    column.ToolTip = headerName;
        //    column.MenuDisabled = true;
        //    column.Sortable = false;
        //    column.Width = new Unit(120);
        //    column.Renderer.Fn = "columnRenderer";
        //    column.Renderer.Fn = "amountRenderer";
        //    return column;
        //}


        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
         
            int periodID = -1;
            int employeeId = -1;

            int.TryParse(cmbEmpSearch.SelectedItem.Value, out employeeId);

            if (employeeId == 0)
                employeeId = -1;

            List<GetAppraisalEducationLocationScoreResult> list = AppraisalManager.GetAppraisalEducationLocationScore(e.Start, e.Limit, employeeId, periodID);
            e.Total = list[0].TotalRows.Value;
            //if (list.Any())
            storeScore.DataSource = list;
            storeScore.DataBind();

        }
        protected void btnSave_Click1(object sender, EventArgs e)
        {
        }
        public void btnLoad_DirectClick(object sender, DirectEventArgs e)
        {
            storeScore.Reload();
        }

    }
}
