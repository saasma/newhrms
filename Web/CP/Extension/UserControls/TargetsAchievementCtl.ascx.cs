﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using BLL;
using Ext.Net;
using Utils.Helper;
using DAL;
using BLL.BO;

namespace Web.CP.Extension.UserControls
{
    public partial class TargetsAchievementCtl : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack && !X.IsAjaxRequest)
            {
                Initialise();
            }

            JavascriptHelper.AttachNonDialogPopUpCode(Page, "shiftPopup", "../../ExcelWindow/TargetAchievementExcel.aspx", 450, 500);

            var code = string.Format("var totalHoursOrDayInAMonth = {0}; ",
                CommonManager.CompanySetting.IsProjectInputType ? ProjectManager.TotalHourOrDaysInAMonth : 100);
            code = string.Format("var isDeduction = {0}; ", false.ToString().ToLower());
            Page.ClientScript.RegisterStartupScript(GetType(), "sdffd", code, true);
        }

        public void Initialise()
        {
            cmbTarget.Store[0].DataSource = AppraisalManager.GetAllTargets();
            cmbTarget.Store[0].DataBind();

            //calDate.Text = CustomDate.GetTodayDate(IsEnglish).ToString();
            cmbYear.Store[0].DataSource = CommonManager.GetYearList();
            cmbYear.Store[0].DataBind();

            PayrollPeriod period = CommonManager.GetLastPayrollPeriod();

            ExtControlHelper.ComboBoxSetSelected(period.Year.ToString(), cmbYear);
            //ExtControlHelper.ComboBoxSetSelected(period.Month.ToString(), cmbStartMonth);
            //ExtControlHelper.ComboBoxSetSelected(period.Month.ToString(), cmbEndMonth);

            cmbStartMonth.Store[0].DataSource = DateManager.GetCurrentMonthList();
            cmbStartMonth.Store[0].DataBind();

            cmbEndMonth.Store[0].DataSource = DateManager.GetCurrentMonthList();
            cmbEndMonth.Store[0].DataBind();

           
        }


        [DirectMethod(Namespace = "CompanyX")]
        public void Edit(int id, string field, string oldValue, string newValue, object customer)
        {
            string message = "<b>Property:</b> {0}<br /><b>Field:</b> {1}<br /><b>Old Value:</b> {2}<br /><b>New Value:</b> {3}";

            // Send Message...
            X.Msg.Notify(new NotificationConfig()
            {
                Title = "Edit Record #" + id.ToString(),
                Html = string.Format(message, id, field, oldValue, newValue),
                Width = 250
            }).Show();

            //this.GridPanel1.GetStore().GetById(id).Commit();
        }

        protected void Store_ReadDataDynamic(object sender, StoreReadDataEventArgs e)
        {

            int year = int.Parse(cmbYear.SelectedItem.Value);

            int empId = -1;
            if(cmbEmpSearch.SelectedItem != null && cmbEmpSearch.SelectedItem.Value != null)
                empId = int.Parse(cmbEmpSearch.SelectedItem.Value);

            int targetId = -1;
            if(cmbTarget.SelectedItem != null && cmbTarget.SelectedItem.Value != null)
                targetId = int.Parse(cmbTarget.SelectedItem.Value);

            List<TargetAchievmentBO> targetList = AppraisalManager.GetEmpTargetAchievments
                (e.Page - 1, e.Limit, empId, targetId, year).ToList();


            if (targetList.Count > 0)
                e.Total = targetList[0].TotalRows;

            storeProjects.DataSource = targetList;
            storeProjects.DataBind();
        }
        protected void btnSave_Click1(object sender, EventArgs e)
        {
        }
        public void btnLoad_DirectClick(object sender, DirectEventArgs e)
        {
           
        }

    }
}
