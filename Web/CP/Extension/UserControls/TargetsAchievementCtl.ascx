﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TargetsAchievementCtl.ascx.cs"
    Inherits="Web.CP.Extension.UserControls.TargetsAchievementCtl" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<script type="text/javascript">

    var btnLoad = null;
    var blockMultipleFormSubmit = true;
     var PagingToolbar_ListItems = null;

    Ext.onReady(
        function () {
            btnLoad = <%= btnLoad.ClientID %>;
              var PagingToolbar_ListItems=<%= PagingToolbar_ListItems.ClientID %>;
        }
    );

    function refreshWindow() {
       
        btnLoad.fireEvent('click');
    }

    function PassQueryString(btn) {
        var year =-1;
        var target = -1;
        var empId = -1;

        if(<%= cmbYear.ClientID %>.getValue() != null)
            year = <%= cmbYear.ClientID %>.getValue();
         if(<%= cmbTarget.ClientID %>.getValue() != null)
            target = <%= cmbTarget.ClientID %>.getValue();
         if(<%= cmbEmpSearch.ClientID %>.getValue() != null)
            empId = <%= cmbEmpSearch.ClientID %>.getValue();
      
        var ret = shiftPopup("empid=" + empId + "&year=" + year + "&target=" + target);
        return false;
    }

    var columnRenderer = function (value, meta, record) {
        if (value == null)
            return "";
        if (value == "")
            return "";
        return value.toFixed(2);
    }

    var afterEdit = function (e) {
    };

    var amountRenderer = function (value, meta, record) {

        if (typeof (value) == 'undefined' || value == null)
            return "";

        var value = parseFloat(value);

        if (value < 0) {

            meta.attr = 'style="border: 1px solid red;"';
            meta.attr += ('title="Negative value."');
        }

        return value.toFixed(2);
    };
        function searchList()
        {
            PagingToolbar_ListItems.doRefresh();
        }

        function valueRender(value,r1,r2)
        {
            if(parseFloat(value)==0)
                return "";
            return value;
        }
        var edit = function (editor, e) {
            /*
                "e" is an edit event with the following properties:

                    grid - The grid
                    record - The record that was edited
                    field - The field name that was edited
                    value - The value being set
                    originalValue - The original value for the field, before the edit.
                    row - The grid table row
                    column - The grid Column defining the column that was edited.
                    rowIdx - The row index that was edited
                    colIdx - The column index that was edited
            */
            
            // Call DirectMethod
            if (!(e.value === e.originalValue )) {
                CompanyX.Edit(e.record.data.ID, e.field, e.originalValue, e.value, e.record.data);
            }
        };

</script>
<style type="text/css">
    .selectCell
    {
        border: 1px splid red !important;
    }
    label
    {
        padding-left: 5px;
        padding-right: 10px;
    }
</style>
<div class="pageheader">
    <div class="media">
        <div class="media-body">
            <h4 runat="server" id="title">
                Target and Achievement List</h4>
        </div>
    </div>
</div>
<div class="contentpanel">
    <div class="contentArea">
        <div class="attribute">
            <table>
                <tr>
                    <td>
                        <ext:ComboBox Width="150" MarginSpec="0 5 0 5" LabelSeparator="" LabelAlign="Top"
                            QueryMode="Local" ID="cmbTarget" ForceSelection="true" DisplayField="Name" ValueField="TargetID"
                            runat="server" FieldLabel="Target">
                            <Store>
                                <ext:Store ID="Store4" runat="server">
                                    <Model>
                                        <ext:Model ID="Model4" IDProperty="TargetID" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="TargetID" Type="String" />
                                                <ext:ModelField Name="Name" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <Triggers>
                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                            </Triggers>
                            <Listeners>
                                <Select Handler="this.getTrigger(0).show();" />
                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                            </Listeners>
                        </ext:ComboBox>
                    </td>
                    <td>
                        <ext:ComboBox Width="100" MarginSpec="0 5 0 5" LabelSeparator="" LabelAlign="Top"
                            QueryMode="Local" ID="cmbYear" ForceSelection="true" DisplayField="Year" ValueField="Year"
                            runat="server" FieldLabel="Year">
                            <Store>
                                <ext:Store ID="Store1" runat="server">
                                    <Model>
                                        <ext:Model ID="Model2" IDProperty="Year" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="Year" Type="String" />
                                                <ext:ModelField Name="Year" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                    </td>
                    <td style="display: none">
                        <ext:ComboBox Width="100" MarginSpec="0 5 0 5" LabelSeparator="" LabelAlign="Top"
                            QueryMode="Local" ID="cmbStartMonth" ForceSelection="true" DisplayField="Value"
                            ValueField="Key" runat="server" FieldLabel="Month">
                            <Store>
                                <ext:Store ID="Store2" runat="server">
                                    <Model>
                                        <ext:Model ID="Model3" IDProperty="Key" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="Key" Type="String" />
                                                <ext:ModelField Name="Value" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                    </td>
                    <td style="display: none">
                        <ext:ComboBox Width="100" MarginSpec="0 5 0 5" LabelSeparator="" LabelAlign="Top"
                            QueryMode="Local" ID="cmbEndMonth" ForceSelection="true" DisplayField="Value"
                            ValueField="Key" runat="server" FieldLabel="Month">
                            <Store>
                                <ext:Store ID="Store3" runat="server">
                                    <Model>
                                        <ext:Model ID="Model1" IDProperty="Key" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="Key" Type="String" />
                                                <ext:ModelField Name="Value" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                    </td>
                    <td style="width: 250px">
                        <ext:Store runat="server" ID="storeEmpSearch" AutoLoad="false">
                            <Proxy>
                                <ext:AjaxProxy Url="../../../Handler/EmpSearchID.ashx">
                                    <ActionMethods Read="GET" />
                                    <Reader>
                                        <ext:JsonReader Root="plants" TotalProperty="total" />
                                    </Reader>
                                </ext:AjaxProxy>
                            </Proxy>
                            <Model>
                                <ext:Model ID="Model9" IDProperty="Name" Name="ItemLineModel" runat="server">
                                    <Fields>
                                        <ext:ModelField Name="Name" Type="String" />
                                        <ext:ModelField Name="EmployeeId" Type="String" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                        <ext:ComboBox LabelSeparator="" ID="cmbEmpSearch" FieldLabel="Employee" LabelWidth="70"
                            EmptyText="Search Employee" LabelAlign="Top" runat="server" DisplayField="Name"
                            ValueField="EmployeeId" StoreID="storeEmpSearch" TypeAhead="false" Width="200"
                            PageSize="9999" HideBaseTrigger="true" MinChars="1" Hidden="false" TriggerAction="All"
                            ForceSelection="true">
                            <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                                <ItemTpl ID="ItemTpl2" runat="server">
                                    <Html>
                                        <tpl>
                                                <div class="search-item">
                                                                <span>{Name}</span>  
                                                 </div>
					                    </tpl>
                                    </Html>
                                </ItemTpl>
                            </ListConfig>
                            <Triggers>
                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                            </Triggers>
                            <Listeners>
                                <Select Handler="this.getTrigger(0).show();" />
                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                <TriggerClick Handler="if (index == 0) { 
                                               this.clearValue(); 
                                               this.getTrigger(0).hide();
                                           }" />
                            </Listeners>
                        </ext:ComboBox>
                    </td>
                    <td valign="bottom">
                        <ext:Button ID="btnLoad" runat="server" Cls="btn btn-primary btn-sect" Width="100"
                            Text="Load">
                            <Listeners>
                                <Click Handler="#{storeProjects}.reload();">
                                </Click>
                            </Listeners>
                        </ext:Button>
                    </td>
                    <td style='padding-left: 10px'>
                        <asp:LinkButton ID="btnExport" runat="server" Text="Import from Excel" OnClientClick="return PassQueryString(this)"
                            CssClass=" excel marginRight tiptip" Style="float: left;" />
                    </td>
                </tr>
            </table>
        </div>
        <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
        <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
        <div style="clear: both; padding-top: 30px" runat="server" id="gridContainer">
        </div>
        <ext:GridPanel ID="gridProjects" Scroll="Both" ClicksToEdit="1" Border="true" StripeRows="true"
            Header="false" runat="server" Title="Title" Cls="gridtbl">
            <Store>
                <ext:Store runat="server" ID="storeProjects" OnReadData="Store_ReadDataDynamic" AutoLoad="false">
                    <Proxy>
                        <ext:PageProxy>
                        </ext:PageProxy>
                    </Proxy>
                    <Model>
                        <ext:Model ID="ModelProject" runat="server">
                            <Fields>
                                <ext:ModelField Name="EIN" Type="Int" />
                                <ext:ModelField Name="Name" Type="String" />
                                <ext:ModelField Name="TargetID" Type="String" />
                                <ext:ModelField Name="TargetName" Type="String" />
                                <ext:ModelField Name="Target1" Type="String" />
                                <ext:ModelField Name="Achievment1" Type="String" />
                                <ext:ModelField Name="Target2" Type="String" />
                                <ext:ModelField Name="Achievment2" Type="String" />
                                <ext:ModelField Name="Target3" Type="String" />
                                <ext:ModelField Name="Achievment3" Type="String" />
                                <ext:ModelField Name="Target4" Type="String" />
                                <ext:ModelField Name="Achievment4" Type="String" />
                                <ext:ModelField Name="Target5" Type="String" />
                                <ext:ModelField Name="Achievment5" Type="String" />
                                <ext:ModelField Name="Target6" Type="String" />
                                <ext:ModelField Name="Achievment6" Type="String" />
                                <ext:ModelField Name="Target7" Type="String" />
                                <ext:ModelField Name="Achievment7" Type="String" />
                                <ext:ModelField Name="Target8" Type="String" />
                                <ext:ModelField Name="Achievment8" Type="String" />
                                <ext:ModelField Name="Target9" Type="String" />
                                <ext:ModelField Name="Achievment9" Type="String" />
                                <ext:ModelField Name="Target10" Type="String" />
                                <ext:ModelField Name="Achievment10" Type="String" />
                                <ext:ModelField Name="Target11" Type="String" />
                                <ext:ModelField Name="Achievment11" Type="String" />
                                <ext:ModelField Name="Target12" Type="String" />
                                <ext:ModelField Name="Achievment12" Type="String" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>
            <Listeners>
            </Listeners>
            <SelectionModel>
                <ext:CellSelectionModel ID="CellSelectionModel1" runat="server" />
            </SelectionModel>
            <ColumnModel runat="server" ID="columnModelProjects">
                <Columns>
                    <ext:Column runat="server" Sortable="false" MenuDisabled="true" Header="EIN" Width="50"
                        ColumnID="EmployeeId" Locked="true" DataIndex="EIN">
                    </ext:Column>
                    <ext:Column runat="server" Sortable="false" MenuDisabled="true" Header="Name" Width="150"
                        ColumnID="Name" Locked="true" DataIndex="Name">
                    </ext:Column>
                    <ext:Column runat="server" Sortable="false" MenuDisabled="true" Header="Target" Width="150"
                        Locked="true" ColumnID="asdf" DataIndex="TargetName">
                    </ext:Column>
                    <ext:Column ID="Column1" Text="Baisakh" StyleSpec="background-color:#FCE4D6" runat="server">
                        <Columns>
                            <ext:Column ID="ColumnAIncome1" runat="server" Text="Target" StyleSpec="background-color:#FCE4D6"
                                DataIndex="Target1" MenuDisabled="false" Sortable="false" Align="Center" Width="60">
                                <Renderer Fn="valueRender" />
                                <Editor>
                                    <ext:NumberField ID="NumberField13" runat="server" />
                                </Editor>
                            </ext:Column>
                            <ext:Column ID="ColumnAIncome2" runat="server" Text="Achievement" StyleSpec="background-color:#FCE4D6"
                                DataIndex="Achievment1" MenuDisabled="false" Sortable="false" Align="Center"
                                Width="90">
                                <Renderer Fn="valueRender" />
                                <Editor>
                                    <ext:NumberField ID="NumberField1" runat="server" />
                                </Editor>
                            </ext:Column>
                        </Columns>
                    </ext:Column>
                    <ext:Column ID="Column2" Text="Jeshta" StyleSpec="background-color:#DDEBF7" runat="server">
                        <Columns>
                            <ext:Column ID="Column4" runat="server" Text="Target" StyleSpec="background-color:#DDEBF7"
                                DataIndex="Target2" MenuDisabled="false" Sortable="false" Align="Center" Width="60">
                                <Renderer Fn="valueRender" />
                                <Editor>
                                    <ext:NumberField ID="NumberField14" runat="server" />
                                </Editor>
                            </ext:Column>
                            <ext:Column ID="Column5" runat="server" Text="Achievement" StyleSpec="background-color:#DDEBF7"
                                DataIndex="Achievment2" MenuDisabled="false" Sortable="false" Align="Center"
                                Width="90">
                                <Renderer Fn="valueRender" />
                                <Editor>
                                    <ext:NumberField ID="NumberField2" runat="server" />
                                </Editor>
                            </ext:Column>
                        </Columns>
                    </ext:Column>
                    <ext:Column ID="Column3" Text="Ashadh" StyleSpec="background-color:#FCE4D6" runat="server">
                        <Columns>
                            <ext:Column ID="Column6" runat="server" Text="Target" StyleSpec="background-color:#FCE4D6"
                                DataIndex="Target3" MenuDisabled="false" Sortable="false" Align="Center" Width="60">
                                <Renderer Fn="valueRender" />
                                <Editor>
                                    <ext:NumberField ID="NumberField15" runat="server" />
                                </Editor>
                            </ext:Column>
                            <ext:Column ID="Column7" runat="server" Text="Achievement" StyleSpec="background-color:#FCE4D6"
                                DataIndex="Achievment3" MenuDisabled="false" Sortable="false" Align="Center"
                                Width="90">
                                <Renderer Fn="valueRender" />
                                <Editor>
                                    <ext:NumberField ID="NumberField3" runat="server" />
                                </Editor>
                            </ext:Column>
                        </Columns>
                    </ext:Column>
                    <ext:Column ID="Column8" Text="Sharwan" StyleSpec="background-color:#DDEBF7" runat="server">
                        <Columns>
                            <ext:Column ID="Column9" runat="server" Text="Target" StyleSpec="background-color:#DDEBF7"
                                DataIndex="Target4" MenuDisabled="false" Sortable="false" Align="Center" Width="60">
                                <Renderer Fn="valueRender" />
                                <Editor>
                                    <ext:NumberField ID="NumberField16" runat="server" />
                                </Editor>
                            </ext:Column>
                            <ext:Column ID="Column10" runat="server" Text="Achievement" StyleSpec="background-color:#DDEBF7"
                                DataIndex="Achievment4" MenuDisabled="false" Sortable="false" Align="Center"
                                Width="90">
                                <Renderer Fn="valueRender" />
                                <Editor>
                                    <ext:NumberField ID="NumberField4" runat="server" />
                                </Editor>
                            </ext:Column>
                        </Columns>
                    </ext:Column>
                    <ext:Column ID="Column11" Text="Bhadra" StyleSpec="background-color:#FCE4D6" runat="server">
                        <Columns>
                            <ext:Column ID="Column12" runat="server" Text="Target" StyleSpec="background-color:#FCE4D6"
                                DataIndex="Target5" MenuDisabled="false" Sortable="false" Align="Center" Width="60">
                                <Renderer Fn="valueRender" />
                                <Editor>
                                    <ext:NumberField ID="NumberField17" runat="server" />
                                </Editor>
                            </ext:Column>
                            <ext:Column ID="Column13" runat="server" Text="Achievement" StyleSpec="background-color:#FCE4D6"
                                DataIndex="Achievment5" MenuDisabled="false" Sortable="false" Align="Center"
                                Width="90">
                                <Renderer Fn="valueRender" />
                                <Editor>
                                    <ext:NumberField ID="NumberField5" runat="server" />
                                </Editor>
                            </ext:Column>
                        </Columns>
                    </ext:Column>
                    <ext:Column ID="Column14" Text="Ashwin" StyleSpec="background-color:#DDEBF7" runat="server">
                        <Columns>
                            <ext:Column ID="Column15" runat="server" Text="Target" StyleSpec="background-color:#DDEBF7"
                                DataIndex="Target6" MenuDisabled="false" Sortable="false" Align="Center" Width="60">
                                <Renderer Fn="valueRender" />
                                <Editor>
                                    <ext:NumberField ID="NumberField18" runat="server" />
                                </Editor>
                            </ext:Column>
                            <ext:Column ID="Column16" runat="server" Text="Achievement" StyleSpec="background-color:#DDEBF7"
                                DataIndex="Achievment6" MenuDisabled="false" Sortable="false" Align="Center"
                                Width="90">
                                <Renderer Fn="valueRender" />
                                <Editor>
                                    <ext:NumberField ID="NumberField6" runat="server" />
                                </Editor>
                            </ext:Column>
                        </Columns>
                    </ext:Column>
                    <ext:Column ID="Column17" Text="Kartki" StyleSpec="background-color:#FCE4D6" runat="server">
                        <Columns>
                            <ext:Column ID="Column18" runat="server" Text="Target" StyleSpec="background-color:#FCE4D6"
                                DataIndex="Target7" MenuDisabled="false" Sortable="false" Align="Center" Width="60">
                                <Renderer Fn="valueRender" />
                                <Editor>
                                    <ext:NumberField ID="NumberField19" runat="server" />
                                </Editor>
                            </ext:Column>
                            <ext:Column ID="Column19" runat="server" Text="Achievement" StyleSpec="background-color:#FCE4D6"
                                DataIndex="Achievment7" MenuDisabled="false" Sortable="false" Align="Center"
                                Width="90">
                                <Renderer Fn="valueRender" />
                                <Editor>
                                    <ext:NumberField ID="NumberField7" runat="server" />
                                </Editor>
                            </ext:Column>
                        </Columns>
                    </ext:Column>
                    <ext:Column ID="Column20" Text="Mangsir" StyleSpec="background-color:#DDEBF7" runat="server">
                        <Columns>
                            <ext:Column ID="Column21" runat="server" Text="Target" StyleSpec="background-color:#DDEBF7"
                                DataIndex="Target8" MenuDisabled="false" Sortable="false" Align="Center" Width="60">
                                <Renderer Fn="valueRender" />
                                <Editor>
                                    <ext:NumberField ID="NumberField20" runat="server" />
                                </Editor>
                            </ext:Column>
                            <ext:Column ID="Column22" runat="server" Text="Achievement" StyleSpec="background-color:#DDEBF7"
                                DataIndex="Achievment8" MenuDisabled="false" Sortable="false" Align="Center"
                                Width="90">
                                <Renderer Fn="valueRender" />
                                <Editor>
                                    <ext:NumberField ID="NumberField8" runat="server" />
                                </Editor>
                            </ext:Column>
                        </Columns>
                    </ext:Column>
                    <ext:Column ID="Column23" Text="Poush" StyleSpec="background-color:#FCE4D6" runat="server">
                        <Columns>
                            <ext:Column ID="Column24" runat="server" Text="Target" StyleSpec="background-color:#FCE4D6"
                                DataIndex="Target9" MenuDisabled="false" Sortable="false" Align="Center" Width="60">
                                <Renderer Fn="valueRender" />
                                <Editor>
                                    <ext:NumberField ID="NumberField21" runat="server" />
                                </Editor>
                            </ext:Column>
                            <ext:Column ID="Column25" runat="server" Text="Achievement" StyleSpec="background-color:#FCE4D6"
                                DataIndex="Achievment9" MenuDisabled="false" Sortable="false" Align="Center"
                                Width="90">
                                <Renderer Fn="valueRender" />
                                <Editor>
                                    <ext:NumberField ID="NumberField9" runat="server" />
                                </Editor>
                            </ext:Column>
                        </Columns>
                    </ext:Column>
                    <ext:Column ID="Column26" Text="Magh" StyleSpec="background-color:#DDEBF7" runat="server">
                        <Columns>
                            <ext:Column ID="Column27" runat="server" Text="Target" StyleSpec="background-color:#DDEBF7"
                                DataIndex="Target10" MenuDisabled="false" Sortable="false" Align="Center" Width="60">
                                <Renderer Fn="valueRender" />
                                <Editor>
                                    <ext:NumberField ID="NumberField22" runat="server" />
                                </Editor>
                            </ext:Column>
                            <ext:Column ID="Column28" runat="server" Text="Achievement" StyleSpec="background-color:#DDEBF7"
                                DataIndex="Achievment10" MenuDisabled="false" Sortable="false" Align="Center"
                                Width="90">
                                <Renderer Fn="valueRender" />
                                <Editor>
                                    <ext:NumberField ID="NumberField10" runat="server" />
                                </Editor>
                            </ext:Column>
                        </Columns>
                    </ext:Column>
                    <ext:Column ID="Column29" Text="Falgun" StyleSpec="background-color:#FCE4D6" runat="server">
                        <Columns>
                            <ext:Column ID="Column30" runat="server" Text="Target" StyleSpec="background-color:#FCE4D6"
                                DataIndex="Target11" MenuDisabled="false" Sortable="false" Align="Center" Width="60">
                                <Renderer Fn="valueRender" />
                                <Editor>
                                    <ext:NumberField ID="NumberField23" runat="server" />
                                </Editor>
                            </ext:Column>
                            <ext:Column ID="Column31" runat="server" Text="Achievement" StyleSpec="background-color:#FCE4D6"
                                DataIndex="Achievment11" MenuDisabled="false" Sortable="false" Align="Center"
                                Width="90">
                                <Renderer Fn="valueRender" />
                                <Editor>
                                    <ext:NumberField ID="NumberField11" runat="server" />
                                </Editor>
                            </ext:Column>
                        </Columns>
                    </ext:Column>
                    <ext:Column ID="Column32" Text="Chaitra" StyleSpec="background-color:#DDEBF7" runat="server">
                        <Columns>
                            <ext:Column ID="Column33" runat="server" Text="Target" StyleSpec="background-color:#DDEBF7"
                                DataIndex="Target12" MenuDisabled="false" Sortable="false" Align="Center" Width="60">
                                <Renderer Fn="valueRender" />
                                <Editor>
                                    <ext:NumberField ID="NumberField24" runat="server" />
                                </Editor>
                            </ext:Column>
                            <ext:Column ID="Column34" runat="server" Text="Achievement" StyleSpec="background-color:#DDEBF7"
                                DataIndex="Achievment12" MenuDisabled="false" Sortable="false" Align="Center"
                                Width="90">
                                <Renderer Fn="valueRender" />
                                <Editor>
                                    <ext:NumberField ID="NumberField12" runat="server" />
                                </Editor>
                            </ext:Column>
                        </Columns>
                    </ext:Column>
                </Columns>
            </ColumnModel>
            <%--  <Plugins>
                <ext:CellEditing ID="CellEditing1" runat="server">
                     <Listeners>
                        <Edit Fn="edit" />
                    </Listeners>
                </ext:CellEditing>
            </Plugins>--%>
            <BottomBar>
                <ext:PagingToolbar ID="PagingToolbar_ListItems" runat="server" StoreID="storeProjects"
                    DisplayInfo="true">
                    <Items>
                        <ext:Label ID="lblPageSize" runat="server" Text="Page size:" />
                        <ext:ToolbarSpacer ID="ToolbarSpacer_listItems" runat="server" Width="10" />
                        <ext:ComboBox ID="ddlListItemPageSize" runat="server" Width="80" SelectOnFocus="true"
                            Selectable="true" ValueField="Value" DisplayField="Text">
                            <Listeners>
                                <Select Handler="#{storeProjects}.pageSize = this.getValue();#{PagingToolbar_ListItems}.moveFirst(); searchList()" />
                            </Listeners>
                            <Items>
                                <ext:ListItem Value="20" Text="20" />
                                <ext:ListItem Value="50" Text="50" />
                                <ext:ListItem Value="100" Text="100" />
                            </Items>
                            <SelectedItems>
                                <ext:ListItem Index="0">
                                </ext:ListItem>
                            </SelectedItems>
                        </ext:ComboBox>
                    </Items>
                </ext:PagingToolbar>
            </BottomBar>
        </ext:GridPanel>
    </div>
</div>
