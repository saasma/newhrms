﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EducationLocationScoreImportCtl.ascx.cs"
    Inherits="Web.CP.Extension.UserControls.EducationLocationScoreImportCtl" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<script type="text/javascript">

    var btnLoad = null;
    var blockMultipleFormSubmit = true;
     var PagingToolbar_ListItems = null;

    Ext.onReady(
        function () {
            btnLoad = <%= btnLoad.ClientID %>;
              var PagingToolbar_ListItems=<%= PagingToolbar_ListItems.ClientID %>;
        }
    );

    function refreshWindow() {
       
        btnLoad.fireEvent('click');
    }

    function PassQueryString(btn) {
        var PeriodId = <%=cmbPeriod.ClientID %>.getValue();
        if(PeriodId==""||PeriodId==null)
        {
        alert("Select Appraisal Period");
        return;
        }
        var ret = shiftPopup("PeriodId=" + PeriodId );
        return false;
    }

    var columnRenderer = function (value, meta, record) {
        if (value == null)
            return "";
        if (value == "")
            return "";
        return value.toFixed(2);
    }

    var afterEdit = function (e) {
    };

    var amountRenderer = function (value, meta, record) {

        if (typeof (value) == 'undefined' || value == null)
            return "";

        var value = parseFloat(value);

        if (value < 0) {

            meta.attr = 'style="border: 1px solid red;"';
            meta.attr += ('title="Negative value."');
        }

        return value.toFixed(2);
    };
        function searchList()
        {
            PagingToolbar_ListItems.doRefresh();
        }


</script>
<style type="text/css">
    .selectCell
    {
        border: 1px splid red !important;
    }
    label
    {
        padding-left: 5px;
        padding-right: 10px;
    }
</style>
<div class="pageheader">
    <div class="media">
        <div class="media-body">
            <h4 runat="server" id="title">
                Education,Location and Seniority Score</h4>
        </div>
    </div>
</div>
<div class="contentpanel">
    <div class="contentArea">
        <div class="attribute">
            <table>
                <tr>
                    <td style="padding-right:10px">
                        <ext:ComboBox ForceSelection="true" QueryMode="Local" ID="cmbPeriod" FieldLabel="Appraisal Period" 
                            runat="server" LabelAlign="Top" DisplayField="Name" Width="200px" LabelSeparator=""
                            ValueField="PeriodId">
                            <Store>
                                <ext:Store ID="store4" runat="server">
                                    <Fields>
                                        <ext:ModelField Name="PeriodId" Type="String" />
                                        <ext:ModelField Name="Name" />
                                    </Fields>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator9" runat="server"
                            ValidationGroup="SaveUpdateLevel" ControlToValidate="cmbPeriod" ErrorMessage="Period is required." />
                    </td>
                    <td style="width: 250px">
                        <ext:Store runat="server" ID="storeEmpSearch" AutoLoad="false">
                            <Proxy>
                                <ext:AjaxProxy Url="../../../Handler/EmpSearchID.ashx">
                                    <ActionMethods Read="GET" />
                                    <Reader>
                                        <ext:JsonReader Root="plants" TotalProperty="total" />
                                    </Reader>
                                </ext:AjaxProxy>
                            </Proxy>
                            <Model>
                                <ext:Model ID="Model9" IDProperty="Name" Name="ItemLineModel" runat="server">
                                    <Fields>
                                        <ext:ModelField Name="Name" Type="String" />
                                        <ext:ModelField Name="EmployeeId" Type="String" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                        <ext:ComboBox LabelSeparator="" ID="cmbEmpSearch" FieldLabel="Employee" LabelWidth="70"
                            EmptyText="Search Employee" LabelAlign="Top" runat="server" DisplayField="Name"
                            ValueField="EmployeeId" StoreID="storeEmpSearch" TypeAhead="false" Width="200"
                            PageSize="9999" HideBaseTrigger="true" MinChars="1" Hidden="false" TriggerAction="All"
                            ForceSelection="true">
                            <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                                <ItemTpl ID="ItemTpl2" runat="server">
                                    <Html>
                                        <tpl>
                                                <div class="search-item">
                                                                <span>{Name}</span>  
                                                 </div>
					                    </tpl>
                                    </Html>
                                </ItemTpl>
                            </ListConfig>
                            <Triggers>
                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                            </Triggers>
                            <Listeners>
                                <Select Handler="this.getTrigger(0).show();" />
                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                <TriggerClick Handler="if (index == 0) { 
                                               this.clearValue(); 
                                               this.getTrigger(0).hide();
                                           }" />
                            </Listeners>
                        </ext:ComboBox>
                    </td>
                    <td valign="bottom">
                        <ext:Button ID="btnLoad" runat="server" Cls="btn btn-primary btn-sect" Width="100"
                            Text="Load">
                            <DirectEvents>
                                <Click OnEvent="btnLoad_DirectClick">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                            <Listeners>
                                <Click Handler="">
                                </Click>
                            </Listeners>
                        </ext:Button>
                    </td>
                    <td style='padding-left: 10px'>
                        <asp:LinkButton ID="btnExport" runat="server" Text="Import from Excel" OnClientClick="return PassQueryString(this)"
                            CssClass=" excel marginRight tiptip" Style="float: left;" />
                    </td>
                </tr>
            </table>
        </div>
        <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
        <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
        <div style="clear: both; padding-top: 30px" runat="server" id="gridContainer">
        </div>
        <ext:GridPanel ID="gridProjects" Scroll="Both" ClicksToEdit="1" Border="true" StripeRows="true"
            Header="false" runat="server" Width="1150" Title="Title" Cls="gridtbl">
            <Store>
                <ext:Store runat="server" ID="storeScore" OnReadData="Store_ReadData" AutoLoad="false">
                    <Proxy>
                        <ext:PageProxy />
                    </Proxy>
                    <Model>
                        <ext:Model ID="ModelProject" runat="server">
                            <Fields>
                                <ext:ModelField Name="RowNumber" Type="String" />
                                <ext:ModelField Name="EmployeeId" Type="String" />
                                <ext:ModelField Name="EmployeeName" Type="String" />
                                <ext:ModelField Name="LocationScore" Type="String" />
                                <ext:ModelField Name="EducationScore" Type="String" />
                                <ext:ModelField Name="SeniorityScore" Type="String" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>
            <Listeners>
            </Listeners>
            <SelectionModel>
                <ext:RowSelectionModel ID="RowSelectionModel11" runat="server">
                </ext:RowSelectionModel>
            </SelectionModel>
            <ColumnModel runat="server" ID="columnModelProjects">
                <Columns>
                    <ext:Column Sortable="false" MenuDisabled="true" Header="EIN" Width="50" ColumnID="EmployeeId"
                        Locked="true" DataIndex="EmployeeId">
                    </ext:Column>
                    <ext:Column Sortable="false" MenuDisabled="true" Header="Employee Name" Width="150"
                        ColumnID="Namasdfe" Locked="true" DataIndex="EmployeeName">
                    </ext:Column>
                    <ext:Column Sortable="false" MenuDisabled="true" Header="Location Score" Width="120"
                        Locked="true" ColumnID="asdf" DataIndex="LocationScore">
                    </ext:Column>
                    <ext:Column Sortable="false" MenuDisabled="true" Header="Education Score" Width="120"
                        ColumnID="Leasdvel" Locked="true" DataIndex="EducationScore">
                    </ext:Column>
                    <ext:Column Sortable="false" MenuDisabled="true" Header="SeniorityScore" Width="120"
                        ColumnID="asd" Locked="true" DataIndex="SeniorityScore">
                    </ext:Column>
                </Columns>
            </ColumnModel>
            <BottomBar>
                <ext:PagingToolbar ID="PagingToolbar_ListItems" runat="server" StoreID="storeProjects"
                    DisplayInfo="true">
                    <Items>
                        <ext:Label ID="lblPageSize" runat="server" Text="Page size:" />
                        <ext:ToolbarSpacer ID="ToolbarSpacer_listItems" runat="server" Width="10" />
                        <ext:ComboBox ID="ddlListItemPageSize" runat="server" Width="80" SelectOnFocus="true"
                            Selectable="true" ValueField="Value" DisplayField="Text">
                            <Listeners>
                                <Select Handler="#{storeScore}.pageSize = this.getValue();#{PagingToolbar_ListItems}.moveFirst(); searchList()" />
                            </Listeners>
                            <Items>
                                <ext:ListItem Value="20" Text="20" />
                                <ext:ListItem Value="50" Text="50" />
                                <ext:ListItem Value="100" Text="100" />
                            </Items>
                            <SelectedItems>
                                <ext:ListItem Index="0">
                                </ext:ListItem>
                            </SelectedItems>
                        </ext:ComboBox>
                    </Items>
                </ext:PagingToolbar>
            </BottomBar>
        </ext:GridPanel>
    </div>
</div>
