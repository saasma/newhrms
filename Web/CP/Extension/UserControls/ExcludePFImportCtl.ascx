﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ExcludePFImportCtl.ascx.cs"
    Inherits="Web.CP.Extension.UserControls.ExcludePFImportCtl" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<script type="text/javascript">

    var btnLoad = null;
    var blockMultipleFormSubmit = true;
    var gridProjects =null;
    
    Ext.onReady(
        function () {
            btnLoad = <%= btnLoad.ClientID %>;
            gridProjects=<%=gridProjects.ClientID%>;
            
        }

    );
     function searchList() {
              <%=PagingToolbar1.ClientID %>.doRefresh();
        }
    function refreshWindow() {
       // alert('h');
        btnLoad.fireEvent('click');
    }

    function isPayrollSelected(btn) {
        if ($('#' + btn.id).prop('disabled'))
            return false;
        var str = "";
        if (typeof (strIncomes) != 'undefined')
            str = strIncomes;
        var ret = shiftPopup("IncomeIDList=" + str + "&IsDeduction=" + isDeduction);
        return false;
    }

    var columnRenderer = function (value, meta, record) {
        if (value == null)
            return "";

        if (value == "")
            return "";
        return value.toFixed(2);
    }

   var afterEdit = function (editor, e) {
            e.record.commit();
        }

    var amountRenderer = function (value, meta, record) {

        if (typeof (value) == 'undefined' || value == null)
            return "";

        var value = parseFloat(value);

        if (value < 0) {

            meta.attr = 'style="border: 1px solid red;"';
            meta.attr += ('title="Negative value."');
        }

        return value.toFixed(2);
    };

     function selectDeselectAll(chk) {
            $('input[type=checkbox]').each(
                    function (index) {
                        if ($(this).prop('disabled') == false && this.id.indexOf('chkVariableIncomes') > 0)
                            this.checked = chk.checked;
                    }
                );

                __doPostBack("","");
                }

                   function searchList() {
             gridProjects.getStore().pageSize = <%=cmbPageSize.ClientID %>.getValue();
            <%=PagingToolbar1.ClientID %>.doRefresh();
        }
</script>
<ext:XScript ID="XScript1" runat="server">
        <script>
            var UpdateRow = function () {                
                var plugin = this.editingPlugin;
                VarEmployeeID=gridProjects.getSelectionModel().getSelection()[0].data.EmployeeId;
                if (this.getForm().isValid()) { // local validation                    
                   #{DirectMethods}.SaveRow(VarEmployeeID,plugin.context.record.phantom, this.getValues(false, false, false, true), {
                        success : function (result) {
                            if (!result.valid) {
                                return;
                            }

                            plugin.completeEdit();
                        }
                    });
                }
            };
        </script>
</ext:XScript>
<style type="text/css">
    .selectCell
    {
        border: 1px splid red !important;
    }
    label
    {
        padding-left: 5px;
        padding-right: 10px;
    }
    
    p.note
    {
        color: #8C8C19;
       <%-- border: solid 1px #BFD62F;
        background-color: #DAE691;
        -moz-border-radius: 6px;
        -webkit-border-radius: 6px;
        border-radius: 6px;
        padding: 10px 20px;
        mc-auto-number-format: '{b}Note: {/b}';
        width: 650px;--%>
    }
</style>
<div class="pageheader">
    <div class="media">
        <div class="media-body">
            <h4 runat="server" id="title">
                Exclude PF Income</h4>
        </div>
    </div>
</div>
<div class="contentpanel">
    <div class="contentArea">
        <div class="attribute">
            <table>
                <tr>
                   
                    <td>
                        <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                            <Proxy>
                                <ext:AjaxProxy Url="../../../Handler/EmpSearchID.ashx">
                                    <ActionMethods Read="GET" />
                                    <Reader>
                                        <ext:JsonReader Root="plants" TotalProperty="total" />
                                    </Reader>
                                </ext:AjaxProxy>
                            </Proxy>
                            <Model>
                                <ext:Model ID="Model5" IDProperty="Name" Name="ItemLineModel" runat="server">
                                    <Fields>
                                        <ext:ModelField Name="Name" Type="String" />
                                        <ext:ModelField Name="EmployeeId" Type="String" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                        <ext:ComboBox LabelSeparator="" ID="cmbEmpSearch" FieldLabel="Search Employee" EmptyText="Employee Name"
                            LabelWidth="120" LabelAlign="Top" runat="server" DisplayField="Name" ValueField="EmployeeId"
                            StoreID="storeSearch" TypeAhead="false" Width="300" PageSize="9999" HideBaseTrigger="true"
                            MinChars="1" TriggerAction="All" ForceSelection="false">
                            <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                                <ItemTpl ID="ItemTpl2" runat="server">
                                    <Html>
                                        <tpl>
                                                        <div class="search-item">
                                                                        <span>{Name}</span>  
                                                         </div>
					                            </tpl>
                                    </Html>
                                </ItemTpl>
                            </ListConfig>
                            <Triggers>
                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                            </Triggers>
                            <Listeners>
                                <Select Handler="this.getTrigger(0).show(); searchList();" />
                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                <TriggerClick Handler="if (index == 0) { 
                                                       this.clearValue(); searchList();
                                                       this.getTrigger(0).hide();
                                                   }" />
                            </Listeners>
                        </ext:ComboBox>
                    </td>
                    <td style="padding-left: 10px; padding-top: 20px">
                        <ext:Button ID="btnLoad" runat="server" Cls="btn btn-primary btn-sect" Width="100"
                            Text="Load">
                            <DirectEvents>
                                <Click OnEvent="btnSave_DirectClick">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                            <Listeners>
                                <Click Handler="">
                                </Click>
                            </Listeners>
                        </ext:Button>
                    </td>
                    <td style='padding-left: 10px; padding-top: 20px'>
                        <ext:Button ID="btnShowFixedIncomeAlso" Visible="false" runat="server" Cls="btn btn-primary btn-sect" Hidden="true"
                            Width="150" Text="Load Fixed Incomes">
                            <Listeners>
                                <Click Handler="window.location = 'VariableSalaryImport.aspx?ShowFixedIncomeAlso=true';">
                                </Click>
                            </Listeners>
                        </ext:Button>
                    </td>
                    <td style='padding-left: 10px; padding-top: 20px'>
                        <asp:LinkButton ID="btnExport" runat="server" Text="Import from Excel" OnClientClick="return isPayrollSelected(this)"
                            CssClass=" excel marginRight tiptip" Style="float: left;" />
                    </td>
                </tr>
            </table>
        </div>
        <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
        <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
        <div style="clear: both" runat="server" id="gridContainer">
        </div>
        <%--<div>
            <p class="note">
                Note: double click to edit values</p>
        </div>--%>
        <br />
        <ext:GridPanel ID="gridProjects" Scroll="Horizontal" ClicksToEdit="1" Border="true"
            StripeRows="true" Header="false" runat="server" Title="Title" Cls="gridtbl">
            <Store>
                <ext:Store ID="storeProjects" runat="server" AutoLoad="false" OnReadData="Store_ReadData"
                    PageSize="20">
                    <Reader>
                        <ext:ArrayReader>
                        </ext:ArrayReader>
                    </Reader>
                    <Model>
                        <ext:Model ID="ModelProject" runat="server" IDProperty="EmployeeId" Name="LicenceBaseGridModel">
                            <Fields>
                                <ext:ModelField Name="EmployeeId" Type="Int" />
                                <ext:ModelField Name="Name" Type="String" />
                                <ext:ModelField Name="level" Type="String" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>
            <Listeners>
            </Listeners>
            <SelectionModel>
                <ext:RowSelectionModel ID="RowSelectionModel11" runat="server">
                </ext:RowSelectionModel>
            </SelectionModel>
            <ColumnModel runat="server" ID="columnModelProjects">
                <Columns>
                    <ext:Column Sortable="false" MenuDisabled="true" Header="EIN" Width="50" ColumnID="EmployadseeId"
                        Locked="true" runat="server" DataIndex="EmployeeId">
                    </ext:Column>
                    <ext:Column Sortable="false" MenuDisabled="true" Header="Name" Width="200" ColumnID="Namasdfe"
                        Locked="true" runat="server" DataIndex="Name">
                    </ext:Column>
                    <ext:Column Sortable="false" MenuDisabled="true" Header="level" Width="100"
                        runat="server" ColumnID="asdfa" DataIndex="level" Locked="true">
                    </ext:Column>
                  
                </Columns>
            </ColumnModel>
            <%--<Plugins>
                <ext:RowEditing ID="RowEditing1dd" runat="server" ClicksToMoveEditor="1" AutoCancel="false" 
                    SaveHandler="UpdateRow">
                  
                    <Listeners>
                        <Edit Fn="afterEdit" />
                    </Listeners>
                </ext:RowEditing>
            </Plugins>--%>
            <BottomBar>
                <ext:PagingToolbar ID="PagingToolbar1" runat="server" StoreID="storeProjects" DisplayInfo="true">
                    <Items>
                        <ext:Label ID="lblPageSize" runat="server" Text="Page size:" />
                        <ext:ToolbarSpacer ID="ToolbarSpacer_listItems" runat="server" Width="10" />
                        <ext:ComboBox ID="cmbPageSize" runat="server" Width="80" ValueField="Value" DisplayField="Text">
                            <Listeners>
                                <%-- <Select Handler="searchList()" />--%>
                                <Select Handler="#{gridProjects}.store.pageSize = parseInt(this.getValue(), 10); #{gridProjects}.store.reload();" />
                                <%--#{cmbPageSize}.pageSize = this.getValue();#{PagingToolbar1}.moveFirst();--%>
                            </Listeners>
                            <Items>
                                <ext:ListItem Value="20" Text="20" />
                                <ext:ListItem Value="50" Text="50" />
                                <ext:ListItem Value="100" Text="100" />
                            </Items>
                            <SelectedItems>
                                <ext:ListItem Value="20" />
                            </SelectedItems>
                        </ext:ComboBox>
                    </Items>
                    <Plugins>
                        <ext:ProgressBarPager ID="ProgressBarPager1" runat="server" />
                    </Plugins>
                </ext:PagingToolbar>
            </BottomBar>
        </ext:GridPanel>
    </div>
</div>
