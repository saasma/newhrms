<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployeeLeaveBalance.ascx.cs"
    Inherits="Web.Extension.UserControls.EmployeeLeaveBalance" %>
<%@ Import Namespace="BLL.Manager" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<script src="../../Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>
<script type="text/javascript">
    var loadingButtonWhenSubmitting = false;
    var skipLoadingCheck = true;
    function ChangeGrade(employeeId) {
        positionHistoryPopup("isPopup=true&EIN=" + employeeId);
    }
       
</script>
<style type="text/css">
    .tableLightColor a:hover
    {
        color: #048FC2;
        text-decoration: none;
    }
</style>
<div class="contentArea">
    <h3>
        Leave Balance</h3>
    <div class="attribute" style='padding-top: 10px;'>
        <table>
            <tr>
                <td valign="top">
                    <asp:Button ID="btnLoad" CssClass="load" OnClientClick="valGroup='AEEmployee';return CheckValidation();"
                        OnClick="btnLoad_Click" runat="server" Text="Load" />
                </td>
                <td valign="top" style='padding-left: 20px;'>
                    <asp:Button ID="btnExport" CssClass="save" OnClick="btnExport_Click" Visible="true"
                        CausesValidation="false" runat="server" Text="Export" />
                </td>
            </tr>
        </table>
        <%--</ContentTemplate>
            </asp:UpdatePanel>--%>
    </div>
    <div class="clear">
        <cc2:emptydisplaygridview cssclass="table table-primary mb30 table-bordered table-hover"
            useaccessibleheader="true" showheaderwhenempty="True" id="gvw" runat="server"
            datakeynames="" autogeneratecolumns="False" cellpadding="4" gridlines="None"
            showfooterwhenempty="False" onrowcreated="gvwEmployees_RowCreated">
            <columns>
                <asp:BoundField HeaderStyle-Width="20px" DataField="EmployeeId" HeaderText="EIN">
                </asp:BoundField>
                <asp:TemplateField HeaderText="Name" HeaderStyle-Width="140px" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <asp:Label runat="server" Text='<%# Eval("Name") %>' Width="140px" />
                    </ItemTemplate>
                </asp:TemplateField>
                
                <asp:BoundField HeaderText="Leave Requested" DataField="LeaveRequestCount" HeaderStyle-Width="100px"
                    HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                <asp:BoundField HeaderText="Leave Recommended" DataField="LeaveRecommendedCount"
                    HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                </asp:BoundField>
                <asp:BoundField HeaderText="Attendance Time Requested" DataField="TimeRequestCount"
                    HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                </asp:BoundField>
                <asp:BoundField HeaderText="Attendance Time Recommended" DataField="TimeRecommendedCount"
                    HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                </asp:BoundField>
                <asp:BoundField HeaderText="Recommender" DataField="Recommender" HeaderStyle-Width="100px"
                    HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                <asp:BoundField HeaderText="Approval" DataField="Approval" HeaderStyle-Width="100px"
                    HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                    <asp:BoundField HeaderText="" Visible="false" DataField="LeaveBalance1" HeaderStyle-HorizontalAlign="Right"
                    ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                <asp:BoundField HeaderText="" Visible="false" DataField="LeaveBalance2" HeaderStyle-HorizontalAlign="Right"
                    ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                <asp:BoundField HeaderText="" Visible="false" DataField="LeaveBalance3" HeaderStyle-HorizontalAlign="Right"
                    ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                <asp:BoundField HeaderText="" Visible="false" DataField="LeaveBalance4" HeaderStyle-HorizontalAlign="Right"
                    ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                <asp:BoundField HeaderText="" Visible="false" DataField="LeaveBalance5" HeaderStyle-HorizontalAlign="Right"
                    ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                <asp:BoundField HeaderText="" Visible="false" DataField="LeaveBalance6" HeaderStyle-HorizontalAlign="Right"
                    ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                <asp:BoundField HeaderText="" Visible="false" DataField="LeaveBalance7" HeaderStyle-HorizontalAlign="Right"
                    ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                <asp:BoundField HeaderText="" Visible="false" DataField="LeaveBalance8" HeaderStyle-HorizontalAlign="Right"
                    ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                <asp:BoundField HeaderText="" Visible="false" DataField="LeaveBalance9" HeaderStyle-HorizontalAlign="Right"
                    ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                <asp:BoundField HeaderText="" Visible="false" DataField="LeaveBalance10" HeaderStyle-HorizontalAlign="Right"
                    ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                <asp:BoundField HeaderText="" Visible="false" DataField="LeaveBalance11" HeaderStyle-HorizontalAlign="Right"
                    ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                <asp:BoundField HeaderText="" Visible="false" DataField="LeaveBalance12" HeaderStyle-HorizontalAlign="Right"
                    ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                <asp:BoundField HeaderText="" Visible="false" DataField="LeaveBalance13" HeaderStyle-HorizontalAlign="Right"
                    ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                <asp:BoundField HeaderText="" Visible="false" DataField="LeaveBalance14" HeaderStyle-HorizontalAlign="Right"
                    ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                <asp:BoundField HeaderText="" Visible="false" DataField="LeaveBalance15" HeaderStyle-HorizontalAlign="Right"
                    ItemStyle-HorizontalAlign="Right"></asp:BoundField>
            </columns>
            <emptydatatemplate>
                <b>No list. </b>
            </emptydatatemplate>
        </cc2:emptydisplaygridview>
    </div>
</div>
