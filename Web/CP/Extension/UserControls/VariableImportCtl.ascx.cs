﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using BLL;
using Ext.Net;
using Utils.Helper;
using DAL;
using Newtonsoft.Json.Linq;
using BLL.Entity;
using System.Threading;

namespace Web.CP.Extension.UserControls
{
    public partial class VariableImportCtl : BaseUserControl
    {
        public bool isNsetDashainAmountImport = true;
        public bool IsDeduction
        {
            get
            {
                return isDeduction;
            }
            set
            {
                isDeduction = value;
            }
        }


        public bool displayData { get; set; }

        public bool isDeduction = false;

        public bool ShowFixedIncomeAlso = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (isDeduction)
            {
                title.InnerHtml = "Variable Deduction Import";
                subTitle.Text = "All Deductions";
            }

          

            if (!string.IsNullOrEmpty(Request.QueryString["ShowFixedIncomeAlso"]))
                ShowFixedIncomeAlso = bool.Parse(Request.QueryString["ShowFixedIncomeAlso"]);

            if (!IsPostBack && !X.IsAjaxRequest)
            {
                Initialise();
            }
            JavascriptHelper.AttachNonDialogPopUpCode(Page, "shiftPopup", "../../ExcelWindow/VariableSalaryExcel.aspx", 450, 500);

            var code = string.Format("var totalHoursOrDayInAMonth = {0}; ",
                CommonManager.CompanySetting.IsProjectInputType ? ProjectManager.TotalHourOrDaysInAMonth : 100);
            code = string.Format("var isDeduction = {0}; ", isDeduction.ToString().ToLower());

            Page.ClientScript.RegisterStartupScript(GetType(), "sdffd", code, true);
            Page.ClientScript.RegisterOnSubmitStatement(GetType(), "ServerForm",
                                                    "if (this.submitted) return false; this.submitted = true; return true;");
            btnShowFixedIncomeAlso.Visible = !this.isDeduction;

            Page.ClientScript.RegisterClientScriptBlock(GetType(), "sffdsf", string.Format("var IsProjectInputType = {0};", CommonManager.CompanySetting.IsProjectInputType ? "true" : "false"), true);
        }

        public void Initialise()
        {
            //if (isDeduction == false && CommonManager.CompanySetting.HasLevelGradeSalary==false)
            //    btnShowFixedIncomeAlso.Visible = true;

            List<PIncome> list = PayManager.GetIncomeListForSalaryExportImport(SessionManager.CurrentCompanyId, isDeduction, ShowFixedIncomeAlso);
            chkVariableIncomes.DataSource = list;
            chkVariableIncomes.DataBind();

            foreach (System.Web.UI.WebControls.ListItem item in chkVariableIncomes.Items)
            {
                if (isDeduction == false)
                {
                    PIncome income = list.FirstOrDefault(x => x.IncomeId.ToString().Equals(item.Value));
                    // do not selected Fixed Income
                    if (income.Calculation == IncomeCalculation.FIXED_AMOUNT)
                    {
                    }
                    else
                        item.Selected = true;
                }
                else
                    item.Selected = true;
            }
            subTitle.Checked = true;
            var strIncomes = string.Empty;
            foreach (System.Web.UI.WebControls.ListItem item in chkVariableIncomes.Items)
            {
                if (item.Selected)
                {
                    if (strIncomes == string.Empty)
                    {
                        strIncomes = item.Value;
                    }
                    else
                    {
                        strIncomes += "," + item.Value;
                    }
                }
            }
            Page.ClientScript.RegisterClientScriptBlock(GetType(), "sffdsf111", string.Format("strIncomes ='{0}';", strIncomes), true);
          //  int totalRecords = 0;
            //BindList(false, pagingCtl.CurrentPage - 1, int.Parse(pagingCtl.DDLRecords.SelectedValue), ref totalRecords);
            displayData = false;
            BindList(displayData);
           // storeProjects.Reload();
        }

        private Column GetExtGridPanelColumn(string indexId, string headerName)
        {
            var column = new Column();
            column.DataIndex = indexId;
            column.DataIndex = indexId;
            column.Text = headerName;
            column.Align = Alignment.Center;
            column.ToolTip = headerName;
            column.MenuDisabled = true;
            column.Sortable = false;
            column.Width = new Unit(120);
            column.Renderer.Fn = "columnRenderer";
            column.Renderer.Fn = "amountRenderer";
            NumberField _txtNumberFieldfield = new NumberField();
            //_txtNumberFieldfield.MinValue = 0;
            //_txtNumberFieldfield.MaxValue = int.MaxValue;
            column.Editor.Add(_txtNumberFieldfield);
            return column;
        }


        [DirectMethod]
        public object SaveRow(string EmployeeID, bool isPhantom, JsonObject values)
        {
            var myJsonString = values.ToString();
            var strJObject = JObject.Parse(myJsonString);
            List<GetEmployeesIncomeForVariableIncomesResult> projectPays = new List<GetEmployeesIncomeForVariableIncomesResult>();
            int indx = 0;
            foreach (System.Web.UI.WebControls.ListItem item in chkVariableIncomes.Items)
            {
                if (chkVariableIncomes.Items[indx].Selected == true)
                {
                    GetEmployeesIncomeForVariableIncomesResult projectPay = new GetEmployeesIncomeForVariableIncomesResult();
                    projectPay.IncomeId = int.Parse(item.Value);
                    projectPay.EmployeeId = int.Parse(EmployeeID);
                    if (values["P_" + item.Value] != null)
                        projectPay.Amount = decimal.Parse(values["P_" + item.Value].ToString());
                    projectPays.Add(projectPay);
                }
                indx++;
            }

            int count = 0;
            ResponseStatus status = PayManager.SaveVariableIncome(projectPays, isDeduction, ref count);
            if (status.IsSuccessType == true)
            {
               // NewMessage.ShowNormalMessage("The Information has been updated");
                Notification.Show(new NotificationConfig
                {
                    Title = "Notification",
                    Icon = Icon.Information,
                    AutoHide = true,
                    Html = "The Information has been updated",
                });
                return new { valid = true };
            }
            else
                return new { valid = false,Message = status.ErrorMessage };
        }

        public void BindList(bool displayData)
        {
            int? employeeid = null;
            if (!string.IsNullOrEmpty(cmbEmpSearch.SelectedItem.Value))
                employeeid = int.Parse(cmbEmpSearch.SelectedItem.Value);
            var ids = new List<int>();
            var strIncomes = string.Empty;
            foreach (System.Web.UI.WebControls.ListItem item in chkVariableIncomes.Items)
            {
                if (item.Selected)
                {
                    if (strIncomes == string.Empty)
                    {
                        strIncomes = item.Value;
                    }
                    else
                    {
                        strIncomes += "," + item.Value;
                    }
                    ids.Add(int.Parse(item.Value));
                }
            }

            var incomes = PayManager.GetVariableIncomes(ids, isDeduction);


            var columnId = "P_{0}";
            foreach (var project in incomes)
            {
                var gridIndexId = string.Format(columnId, project.IncomeId);
                var field = new ModelField(gridIndexId, ModelFieldType.Float);
                field.UseNull = true;
                ModelProject.Fields.Add(gridIndexId, ModelFieldType.Float);
                gridProjects.ColumnModel.Columns.Add(GetExtGridPanelColumn(gridIndexId, project.Title));
            }


            List<GetEmployeesForVariableIncomesResult> employees = new List<GetEmployeesForVariableIncomesResult>();
            var data = new object[employees.Count];
            
            
            if (displayData)
            {
                List<GetEmployeesIncomeForVariableIncomesResult> incomeList = new List<GetEmployeesIncomeForVariableIncomesResult>();
                if (employeeid != null)
                {
                    gridProjects.MinHeight = (150);
                    employees = PayManager.GetEmployeeForVariableIncomes(strIncomes, isDeduction).Where(x => x.EmployeeId == employeeid).ToList();
                    incomeList = PayManager.GetEmployeesIncomeForVariableIncomes(strIncomes, isDeduction).Where(x=>x.EmployeeId==employeeid).ToList();
                }
                else
                {
                    employees = PayManager.GetEmployeeForVariableIncomes(strIncomes, isDeduction);
                    incomeList = PayManager.GetEmployeesIncomeForVariableIncomes(strIncomes, isDeduction);
                }


                var selection = new Dictionary<string, double?>();
                foreach (var item in incomeList)
                {
                    selection.Add(item.EmployeeId + "_" + item.IncomeId, item.Amount == null ? 0 : (double)item.Amount);
                }

                data = new object[employees.Count];

                for (var i = 0; i < employees.Count; i++)
                {
                    var rowData = new object[incomes.Count + 4];

                    var columnIndex = 0;

                    rowData[columnIndex++] = employees[i].EmployeeId;
                    rowData[columnIndex++] = employees[i].IdCardNo;
                    rowData[columnIndex++] = employees[i].AccountNO;
                    rowData[columnIndex++] = employees[i].Name;
                    for (var j = 0; j < incomes.Count; j++)
                    {
                        if (selection.ContainsKey(employees[i].EmployeeId + "_" + incomes[j].IncomeId))
                        {
                            rowData[columnIndex++] = selection[employees[i].EmployeeId + "_" + incomes[j].IncomeId];
                        }
                        else
                        {
                            rowData[columnIndex++] = null;
                        }
                    }

                    data[i] = rowData;
                }
            }


            storeProjects.DataSource = data;
            storeProjects.DataBind();

            //if (totalrows == 0)
            //    pagingCtl.Visible = false;
            //else
            //    pagingCtl.Visible = true;
           // pagingCtl.UpdatePagingBar(totalrows);
        }


        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {

            var ids = new List<int>();
            var strIncomes = string.Empty;
            foreach (System.Web.UI.WebControls.ListItem item in chkVariableIncomes.Items)
            {
                if (item.Selected)
                {
                    if (strIncomes == string.Empty)
                    {
                        strIncomes = item.Value;
                    }
                    else
                    {
                        strIncomes += "," + item.Value;
                    }
                    ids.Add(int.Parse(item.Value));
                }
            }

            var incomes = PayManager.GetVariableIncomes(ids, isDeduction);


            var columnId = "P_{0}";
            foreach (var project in incomes)
            {
                var gridIndexId = string.Format(columnId, project.IncomeId);
                var field = new ModelField(gridIndexId, ModelFieldType.Float);
                field.UseNull = true;
                ModelProject.Fields.Add(gridIndexId, ModelFieldType.Float);
                gridProjects.ColumnModel.Columns.Add(GetExtGridPanelColumn(gridIndexId, project.Title));
            }

            List<GetEmployeesForVariableIncomesResult> employees = new List<GetEmployeesForVariableIncomesResult>();
            var data = new object[employees.Count];

            displayData = true;
            if (displayData)
            {
                int? employeeid = null;
                if (!string.IsNullOrEmpty(cmbEmpSearch.SelectedItem.Value))
                    employeeid = int.Parse(cmbEmpSearch.SelectedItem.Value);
                List<GetEmployeesIncomeForVariableIncomesResult> incomeList = new List<GetEmployeesIncomeForVariableIncomesResult>();
                if (employeeid != null)
                {
                    gridProjects.MinHeight = (150);
                    employees = PayManager.GetEmployeeForVariableIncomes(strIncomes, isDeduction).Where(x => x.EmployeeId == employeeid).ToList();
                    incomeList = PayManager.GetEmployeesIncomeForVariableIncomes(strIncomes, isDeduction).Where(x => x.EmployeeId == employeeid).ToList();
                }
                else
                {
                    employees = PayManager.GetEmployeeForVariableIncomes(strIncomes, isDeduction);
                    incomeList = PayManager.GetEmployeesIncomeForVariableIncomes(strIncomes, isDeduction);
                }

                var selection = new Dictionary<string, double?>();
                foreach (var item in incomeList)
                {
                    selection.Add(item.EmployeeId + "_" + item.IncomeId, item.Amount == null ? 0 : (double)item.Amount);
                }

                data = new object[employees.Count];

                for (var i = 0; i < employees.Count; i++)
                {
                    var rowData = new object[incomes.Count + 4];

                    var columnIndex = 0;

                    rowData[columnIndex++] = employees[i].EmployeeId;
                    rowData[columnIndex++] = employees[i].IdCardNo;
                    rowData[columnIndex++] = employees[i].AccountNO;
                    rowData[columnIndex++] = employees[i].Name;
                    for (var j = 0; j < incomes.Count; j++)
                    {
                        if (selection.ContainsKey(employees[i].EmployeeId + "_" + incomes[j].IncomeId))
                        {
                            rowData[columnIndex++] = selection[employees[i].EmployeeId + "_" + incomes[j].IncomeId];
                        }
                        else
                        {
                            rowData[columnIndex++] = null;
                        }
                    }

                    data[i] = rowData;
                }
            }


            List<object> _list = new List<object>();
            _list = data.ToList();
            //if (_list.Any())
            //    e.Total = _list.Count();
            //else
            //    e.Total = 0;

            //_list = _list.Skip((e.Page - 1) * int.Parse(cmbPageSize.SelectedItem.Value)).Take(int.Parse(cmbPageSize.SelectedItem.Value)).ToList();
            storeProjects.DataSource = _list;
            storeProjects.DataBind();

           // X.AddScript(" if(storeProjects!=null) storeProjects.currentPage =" + e.Page + ";");

            //int milliseconds = 2000;
            //Thread.Sleep(milliseconds);

            //if (X.IsAjaxRequest)
            //{
            ////    // this.GridPanel1.Features.Add(filterList);
            ////    this.gridProjects.Reconfigure();
            //     X.AddScript(" if(storeProjects!=null) storeProjects.currentPage =" + e.Page + ";");
            //}

        }

        public bool IsEnable(object ein)
        {
            if (IsPayrollSaved)
            {
                return false;
            }
            return !LeaveAttendanceManager.IsAttendanceSavedForEmployee(payrollPeriodId, Convert.ToInt32(ein));
        }

       

        public bool IsPayrollSaved = false;
        public int payrollPeriodId;
        public void BindOvertimes()
        {
            //int totalRecords = 0;
            this.displayData = false;
            storeProjects.Reload();
            //BindList(false,pagingCtl.CurrentPage - 1, int.Parse(pagingCtl.DDLRecords.SelectedValue), ref totalRecords);

            if (IsPayrollSaved)
            {
                btnExport.Visible = false;
                divWarningMsg.InnerHtml = Resources.ResourceTooltip.HPLAllowanceSalaryAlreadyGenerated;
                divWarningMsg.Hide = false;
            }
            else
            {
                btnExport.Visible = true;
            }
        }

        protected void ddlPayrollPeriods_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindOvertimes();
        }

        protected void gvwOvertimes_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            BindOvertimes();
        }


        protected void btnSave_Click1(object sender, EventArgs e)
        {
        }

        public void btnSave_DirectClick(object sender, DirectEventArgs e)
        {
            //int totalRecords = 0;
            //BindList(true, pagingCtl.CurrentPage - 1, int.Parse(pagingCtl.DDLRecords.SelectedValue), ref totalRecords);
           BindList(true);
           // this.displayData = true;
           //storeProjects.Reload();
        }

        protected void chkVariableIncomes_SelectedIndexChanged(object sender, EventArgs e)
        {
            var strIncomes = string.Empty;
            foreach (System.Web.UI.WebControls.ListItem item in chkVariableIncomes.Items)
            {
                if (item.Selected)
                {
                    if (strIncomes == string.Empty)
                    {
                        strIncomes = item.Value;
                    }
                    else
                    {
                        strIncomes += "," + item.Value;
                    }
                }
            }

            X.AddScript(string.Format("strIncomes ='{0}';", strIncomes));
           // int totalRecords = 0;
            //BindList(false, pagingCtl.CurrentPage - 1, int.Parse(pagingCtl.DDLRecords.SelectedValue), ref totalRecords);
            BindList(false);
           // displayData = true;
            //storeProjects.Reload();
        }
    }
}
