﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using BLL;
using Ext.Net;
using Utils.Helper;
using DAL;
using BLL.BO;

namespace Web.CP.Extension.UserControls
{
    public partial class TargetsImportOtherDetailCtl : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack && !X.IsAjaxRequest)
            {
                Initialise();
            }

            JavascriptHelper.AttachNonDialogPopUpCode(Page, "shiftPopup", "../../ExcelWindow/WeightTargetAchImportExcel.aspx", 450, 500);

            var code = string.Format("var totalHoursOrDayInAMonth = {0}; ",
                CommonManager.CompanySetting.IsProjectInputType ? ProjectManager.TotalHourOrDaysInAMonth : 100);
            code = string.Format("var isDeduction = {0}; ", false);
            Page.ClientScript.RegisterStartupScript(GetType(), "sdffd", code, true);
        }

        public void Initialise()
        {
            //calDate.Text = CustomDate.GetTodayDate(IsEnglish).ToString();
            cmbTarget.Store[0].DataSource = AppraisalManager.GetAllTargets();
            cmbTarget.Store[0].DataBind();


            cmbForm.Store[0].DataSource = AppraisalManager.GetAllForms();
            cmbForm.Store[0].DataBind();

            ///CreateAppraisalTargetColumns();
        }




        protected void Store_ReadDataDynamic(object sender, StoreReadDataEventArgs e)
        {
            int empId = -1;

            if (cmbEmpSearch.SelectedItem != null && cmbEmpSearch.SelectedItem.Value != null)
                empId = int.Parse(cmbEmpSearch.SelectedItem.Value);

            int formid = -1;

            if (cmbForm.SelectedItem != null && cmbForm.SelectedItem.Value != null)
                formid = int.Parse(cmbForm.SelectedItem.Value);

            int target = -1;

            if (cmbTarget.SelectedItem != null && cmbTarget.SelectedItem.Value != null)
                target = int.Parse(cmbTarget.SelectedItem.Value);


            storeProjects.DataSource = AppraisalManager.GetTargetOtherDetails(empId, formid, target);
            storeProjects.DataBind();


        }

        protected void btnSave_Click1(object sender, EventArgs e)
        {
        }
        public void btnLoad_DirectClick(object sender, DirectEventArgs e)
        {
            storeProjects.Reload();
        }

    }
}
