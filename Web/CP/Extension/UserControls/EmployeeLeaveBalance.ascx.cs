﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using Utils.Helper;
using Utils.Web;
using BLL.Base;
using DAL;
using Utils.Calendar;
using BLL.BO;
using Bll;

namespace Web.Extension.UserControls
{
    public partial class EmployeeLeaveBalance : BaseUserControl
    {


        private int _tempCurrentPage;
        private int _tempCount;
        private PayManager payMgr = new PayManager();

        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!IsPostBack)
            {
              
                Initialise();

                btnLoad_Click(null, null);
            }

      
   
        }

     


        void Initialise()
        {



            List<LLeaveType> leaveList = BLL.BaseBiz.PayrollDataContext.LLeaveTypes
                .Where(x => x.FreqOfAccrual != LeaveAccrue.MANUALLY).OrderBy(x => x.Order).ToList();

            int index = 8;
            foreach (var item in leaveList)
            {

                gvw.Columns[index].HeaderText = item.Title;
                gvw.Columns[index].Visible = true;

                index += 1;
            }


        }
        protected void btnLoad_Click(object sender, EventArgs e)
        {
        
            LoadEmployees();
        }
        protected void gvwEmployees_RowCreated(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover",
                "this.originalClass=this.className;this.className='selected'");

                e.Row.Attributes.Add("onmouseout",
                "this.className=this.originalClass;");


            }

        }
        protected void LoadEmployees()
        {

           
            //gvw.DataSource = list;                
            //gvw.DataBind();

            //bool? type=null;



            gvw.DataSource = ReportManager.GetEmployeeLeaveBalance(2,-1);

            gvw.DataBind();

           
            
        }

        public string GetEngOrNepDate(object value)
        {
            if (value == null)
                return "";

            DateTime date = Convert.ToDateTime(value);

            CustomDate c = new CustomDate(date.Day, date.Month, date.Year, true);

            if (IsEnglish)
                return c.ToString();

            else
                return CustomDate.ConvertEngToNep(c).ToString();
        }

       
        protected void btnExport_Click(object sender, EventArgs e)
        {
            GridViewExportUtil.Export("Leave Balance.xls", gvw);
        }

        //protected void btnPostToSalary_Click(object sender, EventArgs e)
        //{

        //    foreach (GridViewRow row in gvw.Rows)
        //    {


        //        int payrollPeriodId = (int)gvw.DataKeys[row.RowIndex]["PayrollPeriodId"];

        //        PayManager.PostOvertimePay(payrollPeriodId);
        //        msgCtl.InnerHtml = Resources.Messages.OvertimePostedMessage;
        //        msgCtl.Hide = false;
        //        break;
        //    }


        //}
        protected void txtEmployeeName_TextChanged(object sender, EventArgs e)
        {
            

        }

      
       

    }
}
