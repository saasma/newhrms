﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MGAllowanceCtl.ascx.cs"
    Inherits="Web.CP.Extension.UserControls.MGAllowanceCtl" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<script type="text/javascript">

    var btnLoad = null;
    var blockMultipleFormSubmit = true;
    Ext.onReady(
        function () {
            btnLoad = <%= btnLoad.ClientID %>;
        }

    );
    function refreshWindow() {
       // alert('h');
        btnLoad.fireEvent('click');
    }

    function isPayrollSelected(btn) {
      
        var ret = shiftPopup("IncomeID=" + <%=cmbMGAllowanceIncomeList.ClientID %>.value);
        return false;
    }

    var columnRenderer = function (value, meta, record) {
        if (value == null)
            return "";

        if (value == "")
            return "";
        return value.toFixed(2);
    }

    var afterEdit = function (e) {
    };

    var amountRenderer = function (value, meta, record) {

        if (typeof (value) == 'undefined' || value == null)
            return "";

        var value = parseFloat(value);

        if (value < 0) {

            meta.attr = 'style="border: 1px solid red;"';
            meta.attr += ('title="Negative value."');
        }

        return value.toFixed(2);
    };

     function selectDeselectAll(chk) {
            $('input[type=checkbox]').each(
                    function (index) {
                        if ($(this).prop('disabled') == false && this.id.indexOf('chkVariableIncomes') > 0)
                            this.checked = chk.checked;
                    }
                );

                __doPostBack("","");
                }
</script>
<style type="text/css">
    .selectCell
    {
        border: 1px splid red !important;
    }
    label
    {
        padding-left: 5px;
        padding-right: 10px;
    }
</style>
<div class="contentArea">
    <h4 runat="server" id="title">
        Allowance Import</h4>
    <div class="attribute">
        <table>
            <tr>
                <td style='width: 800px'>
                    <ext:ComboBox ValueField="IncomeId" FieldLabel="Income" Width="300" LabelWidth="50"
                        DisplayField="Title" ID="cmbMGAllowanceIncomeList" runat="server">
                        <Store>
                            <ext:Store runat="server">
                                <Model>
                                    <ext:Model ID="ModelProject" runat="server">
                                        <Fields>
                                            <ext:ModelField runat="server" Name="IncomeId" Type="Int" />
                                            <ext:ModelField runat="server" Name="Title" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                </td>
                <td>
                    <ext:Button ID="btnLoad" runat="server" Icon="ApplicationAdd" Width="100" Text="Load">
                        <DirectEvents>
                            <Click OnEvent="btnSave_DirectClick">
                                <EventMask ShowMask="true" />
                            </Click>
                        </DirectEvents>
                        <Listeners>
                            <Click Handler="">
                            </Click>
                        </Listeners>
                    </ext:Button>
                </td>
                <td style='padding-left: 10px'>
                    <asp:LinkButton ID="btnExport" runat="server" Text="Import from Excel" OnClientClick="return isPayrollSelected(this)"
                        CssClass=" excel marginRight tiptip" Style="float: left;" />
                </td>
            </tr>
        </table>
    </div>
    <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
    <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
    <div style="clear: both" runat="server" id="gridContainer">
    </div>
    <ext:GridPanel ID="gridProjects" ClicksToEdit="1" Border="true" StripeRows="true"
        Header="false" runat="server" Height="500" Width="1177px" Title="Title" Cls="gridtbl">
        <Store>
            <ext:Store ID="storeProjects" runat="server">
                <Model>
                    <ext:Model ID="Model1" runat="server">
                        <Fields>
                            <ext:ModelField Name="EmployeeId" Type="Int" />
                            <ext:ModelField Name="Name" Type="String" />
                            <ext:ModelField Name="Amount" Type="Float" />
                        </Fields>
                    </ext:Model>
                </Model>
            </ext:Store>
        </Store>
        <Listeners>
        </Listeners>
        <SelectionModel>
            <ext:RowSelectionModel ID="RowSelectionModel11" runat="server">
            </ext:RowSelectionModel>
        </SelectionModel>
        <ColumnModel runat="server" ID="columnModel">
            <Columns>
                <ext:Column Sortable="false" MenuDisabled="true" Header="EIN" Width="50" DataIndex="EmployeeId">
                </ext:Column>
                <ext:Column Sortable="false" MenuDisabled="true" Header="Name" Width="180" ColumnID="Name"
                    DataIndex="Name">
                </ext:Column>
                <ext:Column Sortable="false" Align="Right" MenuDisabled="true" Header="Bill Received"
                    Width="120" ColumnID="Amount" DataIndex="Amount">
                    <Renderer Fn="getFormattedAmount" />
                </ext:Column>
            </Columns>
        </ColumnModel>
    </ext:GridPanel>
</div>
