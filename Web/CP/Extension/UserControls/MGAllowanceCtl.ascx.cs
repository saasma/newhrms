﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using BLL;
using DAL;
using Ext.Net;
using Utils.Web;
using Utils.Helper;
using System.Text;
using Newtonsoft.Json.Linq;
using BLL.BO;

namespace Web.CP.Extension.UserControls
{
    public partial class MGAllowanceCtl : BaseUserControl
    {
        CommonManager commonMgr = new CommonManager();
        public bool isNsetDashainAmountImport = true;
        public bool IsDeduction
        {
            get
            {
                return isDeduction;
            }
            set
            {
                isDeduction = value;
            }
        }
        public bool isDeduction = false;

        protected void Page_Load(object sender, EventArgs e)
        {

           

            if (!IsPostBack && !X.IsAjaxRequest)
                Initialise();

            JavascriptHelper.AttachNonDialogPopUpCode(Page, "shiftPopup", "../../ExcelWindow/MGAllowanceSalaryExcel.aspx", 450, 500);

            
            // programatic client-side calls to __doPostBack() can bypass this
            Page.ClientScript.RegisterOnSubmitStatement(GetType(), "ServerForm",
                                                    "if (this.submitted) return false; this.submitted = true; return true;");

          
        }

        public void Initialise()
        {
            cmbMGAllowanceIncomeList.Store[0].DataSource = AllowanceManager.GetAllowanceIncomes();
            cmbMGAllowanceIncomeList.Store[0].DataBind();

           
            BindList();
        }

       

        public void BindList()
        {

            List<PEmployeeIncome> data = new List<PEmployeeIncome>();

            if (cmbMGAllowanceIncomeList.SelectedItem == null || string.IsNullOrEmpty(cmbMGAllowanceIncomeList.SelectedItem.Value))
            {
               
            }
            else
            {
                PIncome income = new PayManager().GetIncomeById(int.Parse(cmbMGAllowanceIncomeList.SelectedItem.Value));



                data = PayManager.GetPEmployeeIncomes(int.Parse(cmbMGAllowanceIncomeList.SelectedItem.Value));
            }

            
           
            storeProjects.DataSource = data;
            storeProjects.DataBind();
        }

        public void ddlDeemedIncomeList_Changed(object sender,EventArgs e)
        {
            BindList();
        }

        //public bool IsEnable(object ein)
        //{
        //    if (IsPayrollSaved)
        //        return false;
        //    return !LeaveAttendanceManager.IsAttendanceSavedForEmployee(payrollPeriodId, Convert.ToInt32(ein));
        //}


        public bool IsPayrollSaved = false;
        public int payrollPeriodId;
        public void BindOvertimes()
        {
            BindList();

            if (IsPayrollSaved)
            {
                btnExport.Visible = false;
                divWarningMsg.InnerHtml = Resources.ResourceTooltip.HPLAllowanceSalaryAlreadyGenerated;
                divWarningMsg.Hide = false;
            }
            else
            {
                btnExport.Visible = true;
            }

        }

        protected void ddlPayrollPeriods_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindOvertimes();
        }

        protected void gvwOvertimes_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            BindOvertimes();
        }


        protected void btnSave_Click1(object sender, EventArgs e)
        {
        }

        public void btnSave_DirectClick(object sender, DirectEventArgs e)
        {
            BindList();
        }

     
    }
}