﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using BLL;
using DAL;
using Ext.Net;
using Utils.Web;
using Utils.Helper;
using Utils;
using System.IO;
using Web.ReportDataSetTableAdapters;
using Web.CP.Report.Templates.Pay.Detail;
using BLL.BO;
using Newtonsoft.Json.Linq;

namespace Web.CP
{
    public partial class ProjectContributon : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.AdministratorTools;
            }
        }     

        private CommonManager commonMgr = new CommonManager();
        private const  string Income_Key = "I_{0}";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
               // calFrom.IsEnglishCalendar = IsEnglish; 
                Initialise();

            }
            JavascriptHelper.AttachNonDialogPopUpCode(Page, "popupCreateNew", "ProjReallocation.aspx", 600, 650);
        }


        public void Initialise()
        {
            BindEmployees();


        }
        
        /// <summary>
        /// Creates Array of Object for each Project
        /// </summary>
        /// <returns></returns>
        public object[] GetData()
        {
            List<Project> projects = ProjectManager.GetProjectsForLatestPayroll();
            List<PIncome> incomes = ProjectManager.GetIncomesForProjects();
            List<ProjectIncome> projectIncomes = ProjectManager.GetSelectedProjectIncomes();
            // Add each project/income id as key
            Dictionary<string, string> selection = new Dictionary<string, string>();
            foreach (var projectIncome in projectIncomes)
            {
                selection.Add(projectIncome.ProjectId + "_" + projectIncome.IncomeId, "");
            }


            object[] data = new object[projects.Count];
           
            
            // Iterate for each Project & Skip first as it represents All Project
            for (int i = 0; i < projects.Count; i++)
            {
                object[] rowData = new object[incomes.Count + 2];
                int columnIndex = 0;
              

                rowData[columnIndex++] = projects[i].ProjectId;
                rowData[columnIndex++] = projects[i].Name;
                // Iterate for each Income
                for (int j = 0; j < incomes.Count; j++)
                {
                    if (selection.ContainsKey(projects[i].ProjectId + "_" + incomes[j].IncomeId))
                        rowData[columnIndex++] = true;
                    else
                    {
                        rowData[columnIndex++] = false;

                    }
                }
              
                data[i] = rowData;
            }

            // Process for All Project Selection by check each columns
            //for (int i = 0; i < data.Length; i++)
            //{
            //    object[] array = data[i] as object[];
            //    for (int j = 0; j < array.Length; j++)
            //    {
                   
            //    }
            //}

            return data;
        }

        public void BindEmployees()
        {


            List<PIncome> incomes = ProjectManager.GetIncomesForProjects();

            gridProjects.Store[0].DataSource = GetData();
            gridProjects.Store[0].DataBind();

            Store store = gridProjects.Store[0];
            //ArrayReader reader = (store.Reader[0] as ArrayReader);
            GridHeaderContainer columnModelProjects = gridProjects.ColumnModel;

           
            string columnId;
            foreach (var income in incomes)
            {
                
                columnId = string.Format(Income_Key, income.IncomeId);
                ModelProject.Fields.Add(columnId, ModelFieldType.Boolean);
                

                // Add Checkbox
                CheckColumn column = new CheckColumn();
                //column. = columnId;
                column.DataIndex = columnId;
                column.Text = income.Title;
                column.Align = Alignment.Center;
                column.ToolTip = income.Title;
                column.MenuDisabled = true;
                column.Sortable = false;
                column.Editable = true;
                column.Width = new Unit(130);


                //Checkbox chk = new Checkbox();
                //chk.Listeners.Change.Fn = "checkUncheckAll";
                //column.Editor.Add(chk);

                columnModelProjects.Columns.Add(column);


                // Add TextField percent
                //Column colTextField = new Column();
                //colTextField.ColumnID = "";
                //colTextField.DataIndex = "";
                //colTextField.Header = "%";
                //colTextField.Align = Alignment.Center;
                //colTextField.MenuDisabled = true;
                //colTextField.Sortable = false;
                //colTextField.Width = new Unit(40);

                //NumberField txt = new NumberField();
                //txt.PreventMark = true;
                //colTextField.Editor.Add(txt);
                //columnModelProjects.Columns.Add(colTextField);

            }



            // Load ProjectIncomes


        }



        protected void ddlPayrollPeriods_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindEmployees();
        }




        protected void btnSave_DirectClick(object sender, DirectEventArgs e)
        {
            //JSON representation
            string gridJSON = e.ExtraParams["gridItems"];
            
            //lbl.Text = gridJSON;



            List<Project> projectList = ProjectManager.GetProjects();
            List<PIncome> incomesList = ProjectManager.GetIncomesForProjects();

            JArray jsonArray = Ext.Net.JSON.Deserialize(gridJSON) as JArray;


            List<ProjectIncome> projectIncomes = new List<ProjectIncome>();

            // Skip first row as it represent All Projects
            for (int i = 1; i < jsonArray.Count; i++)
            {
                //Project project = projectList[i];
                JToken row = jsonArray[i];

                int projectId = int.Parse(row["ProjectId"].Value<string>());

                for (int j = 0; j < incomesList.Count; j++)
                {
                    string key = string.Format(Income_Key, incomesList[j].IncomeId);

                    if (row[key] != null && row[key].ToString().ToLower().Contains("true"))//row[key].ToString()=="true")
                    {
                        ProjectIncome projectIncome = new ProjectIncome();
                        projectIncome.IncomeId = incomesList[j].IncomeId;
                        projectIncome.ProjectId = projectId;

                        projectIncomes.Add(projectIncome);
                    }
                }
            }


            ProjectManager.SaveProjectIncomeList(projectIncomes);

            if (projectIncomes.Count > 0)
            {

                X.MessageBox.Show(
                        new MessageBoxConfig
                        {
                            Message = "Project contribution saved.",
                            Buttons = MessageBox.Button.OK,
                            Title = "Information",
                            Icon = MessageBox.Icon.INFO,
                            MinWidth = 300
                        });

            }

        }

    }
}
