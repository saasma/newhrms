﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using DAL;
using BLL;
using Utils.Helper;
using Ext.Net;
using Utils.Web;

namespace Web.User
{
    public partial class ManageLeaveProjects : BasePage
    {

        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.AdministratorTools;
            }
        }     


        //double percentTotal = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialise();
              //  ddlUserType_SelectedIndex(null, null);
            }

        //    storeEmployee.DataSource
        //= EmployeeManager.GetAllEmployees().OrderBy(x => x.Name).ToList();
        //    storeEmployee.DataBind();

            JavascriptHelper.AttachNonDialogPopUpCode(Page, "shiftPopup", "../../ExcelWindow/TeamExcel.aspx", 450, 500);
         
        }

        private void Initialise()
        {

            List<DAL.Department> list = new DepartmentManager().GetDepartmentsWithBranchByCompany(
                SessionManager.CurrentCompanyId).ToList();


            ddlDepartment.DataSource = list;
            ddlDepartment.DataBind();

            ddlBranch.DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            ddlBranch.DataBind();

            BindUsers();
        }

      

        void BindUsers()
        {


             

            int totalRecords = 0;

            List<GetLeaveProjectsResult> list = 
                LeaveRequestManager.GetProjectList(
                txtSearch.Text.Trim(),pagintCtl.CurrentPage,int.Parse(pagintCtl.DDLRecords.SelectedValue), ref totalRecords);



            gvwProjects.DataSource = list.OrderBy(x=>x.Name);       
            gvwProjects.DataBind();

          
            pagintCtl.UpdatePagingBar(totalRecords);
          
        }

        protected void gvwRoles_PageIndexChanged(object sender, EventArgs e)
        {

        }

        protected void btnLoad_Click(object sender, EventArgs e)
        {
            pagintCtl.CurrentPage = 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage -= 1;
            BindUsers();
        }


        protected void btnPrevious_Click()
        {
            pagintCtl.CurrentPage -= 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage -= 1;
            BindUsers();
        }

        protected void btnNext_Click()
        {
            pagintCtl.CurrentPage += 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage += 1;
            BindUsers();
        }


        protected void ddlRecords_SelectedIndexChanged()
        {
            //SessionManager.EmployeeDisplayRecodsPerPage = int.Parse(pagintCtl.DDLRecords.SelectedValue);
            //_tempCurrentPage = 1;
            pagintCtl.CurrentPage = 1;
            BindUsers();
        }

        public bool CanChange(object value)
        {
            return Convert.ToBoolean(value);
        }


        protected void gvwRoles_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            //gvwRoles.PageIndex = e.NewPageIndex;
            //gvwRoles.SelectedIndex = -1;
            BindUsers();
            Clear();
        }
        
        protected void ddlUserType_SelectedIndex(object sender, EventArgs e)
        {
            ////clear & load roles for User type
            //ListItem firstItem = ddlRoles.Items[0];
            //firstItem.Selected = false;
            //ddlRoles.Items.Clear();
            //ddlRoles.ClearSelection();
            ////user type
            //if (!Convert.ToBoolean(ddlUserType.SelectedValue))
            //{
            //    ddlRoles.DataSource = UserManager.GetRoles((int)Role.Employee);
            //    ddlRoles.DataBind();
            //    rowEmployee.Visible = false;
            //}
            //else
            //{
            //    ListItem item = new ListItem(Role.Employee.ToString(), ((int)Role.Employee).ToString());
            //    ddlRoles.Items.Insert(0, item);                
            //    ddlRoles.Items[0].Selected = true;
            //    rowEmployee.Visible = true;
            //}
            //ddlRoles.Items.Insert(0, firstItem);
            //BindUsers();
            //Clear();

        }

        protected void btnInsertUpdate_Click(object sender, EventArgs e)
        {
            int? employeeProjectManger = 0;
            employeeProjectManger = GetProjectManager();
            //if (employeeProjectManger == null)
            //{
            //    msgWarning.InnerHtml = "Manager is required.";
            //    msgWarning.Hide = false;
            //    return;
            //}

            if (this.IsValid)
            {
                LeaveProject project = new LeaveProject();
                project.Name = txtName.Text.Trim();
                project.DepartmentId = int.Parse(ddlDepartment.SelectedValue);
                
                if (ddlBranch.SelectedValue != "-1")
                    project.BranchId = int.Parse(ddlBranch.SelectedValue);

                project.ProjectManagerEmployeeId = employeeProjectManger;

                if (gvwProjects.SelectedIndex != -1)
                {

                    project.LeaveProjectId = (int)gvwProjects.DataKeys[gvwProjects.SelectedIndex]["LeaveProjectId"];
                }

                //if (LeaveRequestManager.IsDepartmentAlreadyExists(project))
                //{
                //    msgWarning.InnerHtml = "Department already exists.";
                //    msgWarning.Hide = false;
                //    return;
                //}

                if (LeaveRequestManager.IsProjectNameAlreadyExists(project))
                {
                    msgWarning.InnerHtml = "Team name already exists.";
                    msgWarning.Hide = false;
                    return;
                }

                if (LeaveRequestManager.SaveUpdateProject(project))
                {
                    if (gvwProjects.SelectedIndex == -1)
                        msgInfo.InnerHtml = "Information saved.";
                    else
                        msgInfo.InnerHtml = "Information updated.";
                    msgInfo.Hide = false;
                    btnCancel_Click(null, null);
                    BindUsers();
                }


            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            //txtUserName.Enabled = ddlEmployeeList.Enabled = true;
            //ddlEmployeeList.ClearSelection();
            //ddlEmployeeList.Items[0].Selected = true;
            gvwProjects.SelectedIndex = -1;
            BindUsers();
            Clear();
        }

        void Clear()
        {
            txtName.Text = "";
            //txtPercent.Text = "";
            //txtUserName.Text = "";
            //txtEmail.Text = "";
            //chkIsActive.Checked = true; 
            ddlBranch.SelectedValue = "-1";
            ddlDepartment.SelectedValue = "-1";
            //ddlProjectManagerEmployee.SelectedValue = "-1";
            ddlProjectManagerEmployee.ClearValue();
            FinancialDate currentYear = SessionManager.CurrentCompanyFinancialDate;
       

          
            btnInsertUpdate.Text = Resources.Messages.Save;
            //this.CustomId = 0;
        }

        public int? GetProjectManager()
        {
            int employeeId = 0;


            if (ddlProjectManagerEmployee.SelectedItem == null || ddlProjectManagerEmployee.SelectedItem.Value == "" || ddlProjectManagerEmployee.SelectedItem.Value == "-1")
                return null;
            else
            {
                if (int.TryParse(ddlProjectManagerEmployee.SelectedItem.Value, out employeeId))
                    return int.Parse(ddlProjectManagerEmployee.SelectedItem.Value);
                else
                    return null;
            }

           
        }

        protected void gvwRoles_SelectedIndexChanged(object sender, EventArgs e)
        {
            int projectId = (int)gvwProjects.DataKeys[gvwProjects.SelectedIndex]["LeaveProjectId"];

            //popup('popUpDiv')
            X.Js.Call("popup","popUpDiv");
            LeaveProject project = LeaveRequestManager.GetLeaveProject(projectId);

            if (project != null)
            {
                txtName.Text = project.Name;
                UIHelper.SetSelectedInDropDown(ddlDepartment, project.DepartmentId.ToString());
                //ddlDepartment.SelectedValue = project.DepartmentId.ToString();

                if (project.BranchId == null)
                    ddlBranch.SelectedValue = "-1";
                else
                    ddlBranch.SelectedValue = project.BranchId.ToString();
                //ddlProjectManagerEmployee.SelectedValue = project.ProjectManagerEmployeeId.ToString();
                //ddlProjectManagerEmployee.SetValue(project.ProjectManagerEmployeeId.ToString());

                // Postback do ComboBoxSetSelected instead of SetValue
                ExtControlHelper.ComboBoxSetSelected(project.ProjectManagerEmployeeId.ToString(), ddlProjectManagerEmployee);
                //if (project.Percent.HasValue)
                //    txtPercent.Text = project.Percent.Value.ToString();

                btnInsertUpdate.Text = Resources.Messages.Update;

              
            }
            
        }

        protected void gvwRoles_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            //string userName = gvwRoles.DataKeys[e.RowIndex]["UserName"].ToString();

            int projectId = (int)gvwProjects.DataKeys[e.RowIndex]["LeaveProjectId"];

            if (LeaveRequestManager.DeleteProject(projectId))
            {
                msgInfo.InnerHtml = "Team deleted.";
                msgInfo.Hide = false;

                gvwProjects.SelectedIndex = -1;
                BindUsers();
                Clear();
            }
            else
            {
                msgWarning.InnerHtml = "Team is in use, can't be deleted.";
                msgWarning.Hide = false;
            }
           
        }

     

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (SessionManager.IsReadOnlyUser)
            {
                //btnInsertUpdate.Visible = false;
                gvwProjects.Columns[3].Visible=false;
                //fieldset.Visible = false;
            }
        }

        //protected void gvwProjects_RowDataBound(object sender, GridViewRowEventArgs e)
        //{

        //    if (e.Row.RowType == DataControlRowType.Footer)
        //    {
        //        Label lbl = e.Row.FindControl("lblPercentTotal") as Label;

        //        lbl.Text = percentTotal.ToString();
        //    }
        //}
    }
}
