﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using DAL;
using BLL;
using Utils.Helper;

namespace Web.CP.Extension
{
    public partial class ManageProjectEmployee : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.AdministratorTools;
            }
        }     
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialise();
            }

            if (Request.Form["__EVENTTARGET"] != null && Request.Form["__EVENTTARGET"].Equals("Reload"))
            {
                BindProjectEmployeeGrid();
            }

            JavascriptHelper.AttachPopUpCode(Page, "popup", "ProjectEmployeeDetails.aspx", 700, 500);
        }

        private void Initialise()
        {
            hdnEmpId.Value = "";
            BindProjectEmployeeGrid();
        }

        private void BindProjectEmployeeGrid()
        {
            int totalRecords = 0;

            int employeeId = 0;
            if (!string.IsNullOrEmpty(hdnEmpId.Value))
                employeeId = int.Parse(hdnEmpId.Value);

            gvwProjectEmployee.DataSource = ProjectManager.GetEmployeeListForPrjEmp(pagintCtl.CurrentPage, int.Parse(pagintCtl.DDLRecords.SelectedValue), employeeId, ref totalRecords);
            gvwProjectEmployee.DataBind();

            pagintCtl.UpdatePagingBar(totalRecords);
        }

        protected void gvwProjectEmployee_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover",
                "this.originalClass=this.className;this.className='selected'");

                e.Row.Attributes.Add("onmouseout",
                "this.className=this.originalClass;");
            }
        }

        protected void gvwProjectEmployee_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void gvwProjectEmployee_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            BindProjectEmployeeGrid();
        }

        protected void btnLoadEmpDetls_Click(object sender, EventArgs e)
        {
            BindProjectEmployeeGrid();
        }

        protected void btnNext_Click()
        {
            pagintCtl.CurrentPage += 1;
            BindProjectEmployeeGrid();
        }

        protected void btnPrevious_Click()
        {
            pagintCtl.CurrentPage -= 1;
            BindProjectEmployeeGrid();
        }

        protected void ddlRecords_SelectedIndexChanged()
        {
            pagintCtl.CurrentPage = 1;
            BindProjectEmployeeGrid();
        }
    }
}