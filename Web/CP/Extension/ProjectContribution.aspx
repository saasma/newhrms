<%@ Page MaintainScrollPositionOnPostback="true" Title="Project Contribution" Language="C#"
    MasterPageFile="~/Master/NewDetails.Master" AutoEventWireup="true" CodeBehind="ProjectContribution.aspx.cs"
    Inherits="Web.CP.ProjectContributon" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ExtMsgCtl.ascx" TagName="ExtMsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>
    <%--  <script src="../Scripts/jquery.sexy-combo.pack.js" type="text/javascript"></script>

   <script src="../Scripts/jquery.sexy-combo.min.js" type="tet/javascript"></script>

    <script src="../Scripts/jquery.sexy-combo.js" type="text/javascript"></script>--%>
    <script type="text/javascript">

        //        function selectDeselectAll(chk) {
        //            $('input[type=checkbox]').each(
        //                    function (index) {
        //                        if ($(this).attr('disabled') == false && this.id.indexOf('chkSelect') > 0)
        //                            this.checked = chk.checked;
        //                    }
        //                );
        //        }



        function popupCreateNewCall() {
            var ret = popupCreateNew();
            //            if (typeof (ret) != 'undefined') {
            //                if (ret == "ReloadIncome") {
            //                    __doPostBack('Reload', '');
            //                }
            //            }
        }

        var checkUncheckAll = function (e, value) {


            e.grid.record.data[e.grid.dataIndex] = value;
            var store = gridProjects.getStore();
            var items = store.data.items;
            var dataIndex = e.grid.dataIndex;
            //if first row then check/un-check other rows
            if (e.grid.row == 0) {

                var length = items.length;
                for (var i = 1; i < length; i++) {
                    items[i].data[dataIndex] = value;
                }

            } else {
                //if first row has checked then don't change to uncheck
                if (items[0].data[e.grid.dataIndex] == true) {
                    e.grid.record.data[e.grid.dataIndex] = true;
                }
            }

            store.commitChanges();
            gridProjects.getView().refresh();
        };


       
    </script>
    <style type="text/css">
        .x-grid3 table
        {
            table-layout: inherit;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
 <%--   <ext:ResourceManager runat="server" DisableViewState="false" ShowWarningOnAjaxFailure="false">
    </ext:ResourceManager>--%>
    <script type="text/javascript">

        var gridProjects = null;
        Ext.onReady(
                function() {
                    gridProjects = <%= gridProjects.ClientID %>;
                }
        );

    </script>

     <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                   Project Contribution
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
    <div class="contentArea">
      
        <div class="attribute" style='text-align:left;padding-bottom:10px'>
            <input type="button" id="btn1" runat="server" onclick='popupCreateNewCall();return false;'
                value="Reallocate" class="btn btn-default btn-sm btn-sect" />
            <%--   <table>
                <tr>
                    <td>
                      From &nbsp;
                         <My:Calendar Id="calFrom" runat="server" />
                    </td>
                </tr>
            </table>--%>
        </div>
        <div style="clear: both">
            <ext:GridPanel ID="gridProjects" Border="true" StripeRows="true" Header="false" runat="server"
                Height="400" Width="1176" Title="Title" Cls="gridtbl">
                <Store>
                    <%-- <ext:Store runat="server" ID="storeProjects">
                        <Reader>
                            <ext:ArrayReader>
                                <Fields>
                                    <ext:RecordField Name="ProjectId" Type="String" />
                                    <ext:RecordField Name="Name" Type="String" />
                                </Fields>
                            </ext:ArrayReader>
                        </Reader>
                    </ext:Store>--%>
                    <ext:Store runat="server" ID="storeProjects">
                        <Reader>
                            <ext:ArrayReader>
                            </ext:ArrayReader>
                        </Reader>
                        <Model>
                            <ext:Model ID="ModelProject" runat="server">
                                <Fields>
                                    <ext:ModelField Name="ProjectId" Type="Int" />
                                    <ext:ModelField Name="Name" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <%-- <Plugins>
                    <ext:EditableGrid ID="EditableGrid1" runat="server" />
                </Plugins>--%>
                <View>
                    <ext:GridView ID="GridView1" runat="server" />
                </View>
                <SelectionModel>
                    <ext:RowSelectionModel runat="server">
                    </ext:RowSelectionModel>
                </SelectionModel>
                <ColumnModel runat="server" ID="columnModelProjects">
                    <Columns>

                        <ext:Column Sortable="false" MenuDisabled="true" Header="Project" Width="180" ColumnID="Name"
                            DataIndex="Name">
                        </ext:Column>
                        <%--<ext:Column Sortable="false" Align="Center" MenuDisabled="true" Header="%" Width="40"
                            ColumnID="ProjectPercent" DataIndex="ProjectPercent">
                            <Editor>
                               <ext:NumberField runat="server"></ext:NumberField>
                            </Editor>
                        </ext:Column>--%>
                    </Columns>
                </ColumnModel>
            </ext:GridPanel>
        </div>
        <%-- <uc2:ExtMsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
        <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />--%>
        <div cssclass="clear buttonsDiv" id="divButtons" runat="server" style='text-align: left;
            margin-top: 15px'>
            <ext:Button ID="btnSave" cls="btn btn-primary btn-sect" runat="server" Width="120" Text="Save Changes">
                <DirectEvents>
                    <Click OnEvent="btnSave_DirectClick">
                        <ExtraParams>
                            <ext:Parameter Name="gridItems" Value="Ext.encode(gridProjects.getRowsValues({ selectedOnly: false }))"
                                Mode="Raw" />
                        </ExtraParams>
                        <%--server side event with mask/loading showing--%>
                        <EventMask ShowMask="true" />
                    </Click>
                </DirectEvents>
                <Listeners>
                    <Click Handler="">
                    </Click>
                </Listeners>
            </ext:Button>
            <ext:Label runat="server" ID="lbl">
            </ext:Label>
        </div>
    </div>
    </div>
</asp:Content>
