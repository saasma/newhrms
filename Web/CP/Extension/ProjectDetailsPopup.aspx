﻿<%@ Page Title="Project Details" Language="C#" MasterPageFile="~/Master/ForPopupPage.Master"
    AutoEventWireup="true" CodeBehind="ProjectDetailsPopup.aspx.cs" Inherits="Web.CP.Extension.ProjectDetailsPopup" %>

<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningMsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="InfoMsgCtl" TagPrefix="uc2" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        table tr td
        {
            padding: 0 10px 8px 0;
        }
        h3
        {
            font: bold 13px Arial;
        }
        .marginTopss
        {
            margin-top: 10px;
            display: block;
        }
        .loanAdvTable
        {
            background-color: white;
            width: 60px;
            border: 1px solid white !important;
            padding: 0px;
        }
        .tableLightColor th
        {
            text-align: left;
        }
        strong
        {
            display: block;
            padding-bottom: 3px;
        }
        .tableLightColor input[type=text]
        {
            height: 20px;
        }
        .clsColor
        {
            background-color: #e8f1ff;
            height: 460px;
        }
    </style>
    <script type="text/javascript">

        function validateDOJToDate(source, args) {
            //debugger;

            args.IsValid = isSecondCalendarCtlDateGreater(getCalendarSelectedDate('<%= calFrom.ClientID %>'),
         getCalendarSelectedDate('<%= calTo.ClientID %>'));

        }

        function closePopup() {
            window.opener.reloadProject(window);
        }

    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">
    <div class="clsColor">
        <div class="popupHeader">
            <h3>
                Add/Edit Project</h3>
        </div>
        <asp:HiddenField ID="hdnProjectId" runat="server" />
        <div class=" marginal" style="margin: 5px 20px 0 10px;">
            <uc2:InfoMsgCtl ID="msgInfo" EnableViewState="false" Hide="true" runat="server" />
            <uc2:WarningMsgCtl ID="msgWarning" EnableViewState="false" Hide="true" runat="server" />
        </div>
        <div class=" marginal" style="margin: 5px 20px 0 10px;">
            <table class="fieldTable" cellpadding="0" style='padding-left: 10px' cellspacing="5">
                <tr runat="server" id="rowEmployee">
                    <td class="fieldHeader">
                        Name *
                    </td>
                    <td>
                        <asp:TextBox ID="txtName" MaxLength="64" Width="400px" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" SetFocusOnError="true" ValidationGroup="SaveUpdateProject"
                            ControlToValidate="txtName" Display="None" ErrorMessage="Project name is required."
                            runat="server" />
                    </td>
                </tr>
                <tr runat="server" id="Tr1">
                    <td class="fieldHeader">
                        Code
                    </td>
                    <td>
                        <asp:TextBox ID="txtCode" Width="182" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="fieldHeader">
                        Start date *
                    </td>
                    <td style='width: 300px'>
                        <My:Calendar Id="calFrom" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="fieldHeader">
                        End date *
                    </td>
                    <td>
                        <My:Calendar Id="calTo" runat="server" />
                        <asp:TextBox Style='display: none' ID="txt" runat="server" />
                        <asp:CustomValidator IsCalendar="true" ID="valCustomeToDate2" runat="server" ValidateEmptyText="true"
                            ValidationGroup="SaveUpdateProject" ControlToValidate="txt" Display="None" ErrorMessage="Ending date must be greater than starting date."
                            ClientValidationFunction="validateDOJToDate" />
                        <asp:CheckBox ID="chkNoEndDate" runat="server" AutoPostBack="true" OnCheckedChanged="CheckNoEndDate_Change"
                            Checked="false" Text="No End Date" />
                    </td>
                </tr>
                <tr>
                    <td class="fieldHeader">
                        Donor
                    </td>
                    <td>
                        <asp:TextBox ID="txtDonor" Width="182" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="fieldHeader">
                        Fund Code
                    </td>
                    <td>
                        <asp:TextBox ID="txtFundCode" Width="182" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="fieldHeader">
                        Project ID
                    </td>
                    <td>
                        <asp:TextBox ID="txtProjectID" Width="182" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="fieldHeader">
                        Project Analysis Code
                    </td>
                    <td>
                        <asp:TextBox ID="txtProjectAnalysisCode" Width="182" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="fieldHeader">
                        Department ID
                    </td>
                    <td>
                        <asp:TextBox ID="txtDepID" Width="182" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="fieldHeader">
                        Remarks
                    </td>
                    <td>
                        <asp:TextBox ID="txtRemarks" Width="400px" Height="60px" TextMode="MultiLine" runat="server"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </div>
        <br />
        <div style='padding-left: 12px; text-align: right; padding-right: 22px; width: 550px;'
            id="buttonsBar" runat="server">
            &nbsp; &nbsp;
            <asp:Button ID="btnInsertUpdate" CssClass="save" runat="server" Text="Save" OnClientClick="valGroup='SaveUpdateProject';return CheckValidation()"
                OnClick="btnInsertUpdate_Click" />
            <asp:Button ID="btnCancel" CssClass="cancel" CausesValidation="false" runat="server"
                Text="Cancel" OnClientClick="window.close();" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
