<%@ Page MaintainScrollPositionOnPostback="true" Title="Project Pay" Language="C#"
    MasterPageFile="~/Master/HROld.Master" AutoEventWireup="true" CodeBehind="ProjectPayInput.aspx.cs"
    Inherits="Web.CP.ProjectPayInput" %>


<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%--  <script src="../Scripts/jquery.sexy-combo.pack.js" type="text/javascript"></script>

   <script src="../Scripts/jquery.sexy-combo.min.js" type="tet/javascript"></script>

    <script src="../Scripts/jquery.sexy-combo.js" type="text/javascript"></script>--%>
    <script type="text/javascript">
        var blockMultipleFormSubmit = true;
        function refreshWindow() {
            __doPostBack('<%= ddlPayrollPeriods.ClientID %>', 0);
        }


        function isPayrollSelected(btn) {


            if ($('#' + btn.id).prop('disabled'))
                return false;

            var payrollPeriodId = document.getElementById('<%= ddlPayrollPeriods.ClientID %>').value;
            var incomeid = document.getElementById('<%= ddlIncomes.ClientID %>').value;


            if (IsProjectInputType)
                if (payrollPeriodId == 0)
                    return false;

            var ret = shiftPopup("payrollPeriod=" + payrollPeriodId + "&incomeid=" + incomeid);
            //            if (typeof (ret) != 'undefined') {
            //                if (ret == 'Reload') {
            //                    //refreshIncomeList(0);


            //                }
            //            }

            return false;
        }


        //        function totalRenderer(e1,e2,e3,e4) {
        //  
        //        }


        var totalRenderer = function (value, meta, record) {
            // alert(totalHoursOrDayInAMonth);

            //            if (typeof (totalHoursOrDayInAMonth) == 'undefined')
            //                return value;

            var total = parseFloat(record.data.Total);


            if (parseFloat(total).toFixed(2) != parseFloat(totalHoursOrDayInAMonth)) {
                // meta.css = 'selectCell';

                meta.attr = 'style="border: 1px solid orange;"';

                if (IsProjectInputType)
                    meta.attr += ('title="Total hours in the month is ' + totalHoursOrDayInAMonth + '."');
                else
                    meta.attr += ('title="Total percentage is ' + totalHoursOrDayInAMonth + '."');
            }


            return value.toFixed(2);
        };

        var columnRenderer = function (value, meta, record) {

            if (value == "")
                return "0";
            return value.toFixed(2);
        }
        var afterEdit = function (editor, e, e1, e2, e3, e4) {
            //            e.record.commit();
            /*
            Properties of 'e' include:
            e.grid - This grid
            e.record - The record being edited
            e.field - The field name being edited
            e.value - The value being set
            e.originalValue - The original value for the field, before the edit.
            e.row - The grid row index
            e.column - The grid column index
            */

            var value = 0;
            if (e.value != "")
                value = e.value;
         //   e.record.set(e.field, value);
            var total = 0;
            for (var i = 2; i < e.record.raw.length - 1; i++) {
//                if (e.record.raw[i] != null) {
                    var fieldName = e.record.fields.items[i].name;
                    total += parseFloat(e.record.data[fieldName]);
                   // total += parseFloat(e.record.raw[i]);
//                }
            }
            e.record.set('Total', total);
            e.record.commit();
        }


    </script>
    <ext:XScript ID="XScript1" runat="server">
        <script>
            var UpdateRow = function () {                
                var plugin = this.editingPlugin;
                VarEmployeeID=gridProjects.getSelectionModel().getSelection()[0].data.EmployeeId;
                if (this.getForm().isValid()) { // local validation                    
                   #{DirectMethods}.SaveRow(VarEmployeeID,plugin.context.record.phantom, this.getValues(false, false, false, true), {
                        success : function (result) {
                            if (!result.valid) {
                                return;
                            }

                            plugin.completeEdit();
                        }
                    });
                }
            };
        </script>
    </ext:XScript>
    <style type="text/css">
        .selectCell
        {
            border: 1px splid orange !important;
        }
        p.note
    {
        color: #8C8C19;
       <%-- border: solid 1px #BFD62F;
        background-color: #DAE691;
        -moz-border-radius: 6px;
        -webkit-border-radius: 6px;
        border-radius: 6px;
        padding: 10px 20px;
        mc-auto-number-format: '{b}Note: {/b}';
        width: 650px;--%>
    }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <script type="text/jscript">
 var gridProjects =null;
    Ext.onReady(
        function () {
            gridProjects=<%=gridProjects.ClientID%>;
        }

    );
    </script>
    <ext:ResourceManager ID="ResourceManager1" DisableViewState="false" runat="server"
        ShowWarningOnAjaxFailure="false" ScriptMode="Release">
    </ext:ResourceManager>
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Project Pay
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="contentArea">
            <div class="attribute" style="padding: 10px">
                <table>
                    <tr>
                        <td style='width: 170px'>
                            <strong runat="server" id="payrollText">Payroll Period</strong>
                            <asp:DropDownList AutoPostBack="true" ID="ddlPayrollPeriods" DataTextField="Name"
                                DataValueField="PayrollPeriodId" AppendDataBoundItems="true" Width="150px" runat="server"
                                OnSelectedIndexChanged="ddlPayrollPeriods_SelectedIndexChanged">
                                <asp:ListItem Value="0" Text=""></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator InitialValue="0" Display="None" ControlToValidate="ddlPayrollPeriods"
                                ValidationGroup="Overtime" ID="RequiredFieldValidator2" runat="server" ErrorMessage="Payroll period is required."></asp:RequiredFieldValidator>
                        </td>
                        <td style='width: 170px;display:none' >
                            <strong>Income</strong>
                            <asp:DropDownList AutoPostBack="true" ID="ddlIncomes" DataTextField="Title" DataValueField="IncomeId"
                                OnSelectedIndexChanged="ddlPayrollPeriods_SelectedIndexChanged" AppendDataBoundItems="true"
                                Width="150px" runat="server">
                                <asp:ListItem Value="-1" Text=""></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td style='padding-left: 10px'>
                            <asp:LinkButton ID="btnExport" runat="server" Text="Excel import" OnClientClick="return isPayrollSelected(this)"
                                CssClass=" excel marginRight tiptip" Style="float: left;" />
                        </td>
                        <td>
                            <strong runat="server" id="hoursText">Project Hours In Month</strong>
                        </td>
                        <td style='padding-left: 10px'>
                            <asp:TextBox ID="txtHours" runat="server" Width="80" Height="22" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtHours"
                                Display="None" ErrorMessage="Hours in a month is required." ValidationGroup="setting"></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="val" Type="Integer" Operator="GreaterThan" ValueToCompare="0"
                                runat="server" ControlToValidate="txtHours" Display="None" ErrorMessage="Invalid hours in a month."
                                ValidationGroup="setting">
                            </asp:CompareValidator>
                        </td>
                        <td style='padding-left: 10px'>
                            <asp:Button ID="btnSave" OnClientClick="valGroup='setting';if( CheckValidation() ) {this.value='';this.className='save spinner';;return true;}"
                                Text="Save Changes" CssClass="btn btn-primary btn-sect btn-sm" runat="server"
                                ValidationGroup="setting" OnClick="btnSave_Click" />
                        </td>
                    </tr>
                </table>
            </div>
            <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
            <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
            <div style="clear: both">
            </div>
            <div>
                <p class="note">
                    Note: double click to edit values</p>
            </div>
            <ext:GridPanel ID="gridProjects" ClicksToEdit="1" Border="true" StripeRows="true"
                Header="false" runat="server" Height="550" Title="Title" Cls="gridtbl">
                <Store>
                    <ext:Store runat="server" ID="storeProjects">
                        <Reader>
                            <ext:ArrayReader>
                            </ext:ArrayReader>
                        </Reader>
                        <Model>
                            <ext:Model ID="ModelProject" runat="server">
                                <Fields>
                                    <ext:ModelField Name="EmployeeId" Type="Int" />
                                    <ext:ModelField Name="Name" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <Listeners>
                </Listeners>
                <SelectionModel>
                    <ext:RowSelectionModel ID="RowSelectionModelasdfasdfa11" runat="server">
                    </ext:RowSelectionModel>
                </SelectionModel>
                <ColumnModel runat="server" ID="columnModelProjects">
                    <Columns>
                        <ext:Column Sortable="false" runat="server" MenuDisabled="true" Header="EIN" Width="50"
                            ColumnID="EmployeeId" DataIndex="EmployeeId">
                        </ext:Column>
                        <ext:Column Sortable="false" runat="server" MenuDisabled="true" Header="Name" Width="180"
                            ColumnID="Name" DataIndex="Name">
                        </ext:Column>
                    </Columns>
                </ColumnModel>
                <Plugins>
                    <ext:RowEditing ID="RowEditing1ddd" runat="server" ClicksToMoveEditor="1" AutoCancel="false"
                        SaveHandler="UpdateRow">
                        <Listeners>
                            <Edit Fn="afterEdit" />
                        </Listeners>
                    </ext:RowEditing>
                </Plugins>
            </ext:GridPanel>
           
        </div>
        <div cssclass="clear buttonsDiv" id="divButtons" runat="server" style='text-align: right;
            margin-top: 15px; margin-left: 10px'>
            <ext:Button ID="Button1" Cls="btn btn-primary btn-sm btn-sect" Hidden="true" runat="server"
                Icon="ApplicationAdd" Width="100" Text="Save Changes">
                <DirectEvents>
                    <Click OnEvent="btnSave_DirectClick">
                        <Confirmation ConfirmRequest="true" Message="Are you sure, you want to save the changes?" />
                        <ExtraParams>
                            <ext:Parameter Name="gridItems" Value="Ext.encode(#{gridProjects}.getRowsValues({ selectedOnly: false }))"
                                Mode="Raw" />
                        </ExtraParams>
                        <%--server side event with mask/loading showing--%>
                        <EventMask ShowMask="true" />
                    </Click>
                </DirectEvents>
                <Listeners>
                    <Click Handler="">
                    </Click>
                </Listeners>
            </ext:Button>
            <ext:Label runat="server" ID="lbl">
            </ext:Label>
        </div>
    </div>
</asp:Content>
