<%@ Page Title="Manage Leave Teams" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="ManageLeaveProjects.aspx.cs" Inherits="Web.User.ManageLeaveProjects" %>

<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningMsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="InfoMsgCtl" TagPrefix="uc2" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%-- <script src="../../Scripts/jquery-1.9.1.js"></script>
    <script src="../../Scripts/jquery-ui.js"></script>--%>
    <%-- TODO: jquery draggable has side effect on control validation --%>
    <script src="../../Scripts/popup.js"></script>
    <script type="text/javascript">


        function filterCallback(chk) {

        }
        function validateDOJToDate(source, args) {
            //debugger;
        }

        function refreshWindow() {
            window.location.reload();
        }

        function isPayrollSelected(btn) {


            var ret = shiftPopup();
            return false;
        }
  


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <ext:ResourceManager ID="ResourceManager1" runat="server" DisableViewState="false"
        ShowWarningOnAjaxFailure="false" ScriptMode="Release" Namespace="Rigo" />
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Set up Teams for Leave Request and Approval
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div>
            <!-- panel-heading -->
            <div>
                <div class="contentArea">
                    <asp:Panel runat="server" DefaultButton="btnLoad" Style="padding: 10px" class="attribute">
                        <table>
                            <tr>
                                <td style="width: 155px; padding-top: 0px;">
                                    <strong>Team Name</strong><asp:TextBox ID="txtSearch" runat="server" />
                                </td>
                                <td style="padding-top: 10px;">
                                    <asp:LinkButton ID="btnLoad" runat="server" OnClick="btnLoad_Click" Text="Load" CssClass="btn btn-default btn-sm"
                                        Style='width: 80px' />
                                </td>
                                <td style='padding-left: 10px'>
                                    <asp:LinkButton ID="btnExport" runat="server" Text="Excel import" OnClientClick="return isPayrollSelected(this)"
                                        CssClass=" excel marginRight tiptip" Style="float: left;" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <uc2:InfoMsgCtl ID="msgInfo" EnableViewState="false" Hide="true" runat="server" />
                    <uc2:WarningMsgCtl ID="msgWarning" EnableViewState="false" Hide="true" runat="server" />
                    <div style="clear: both">
                        <cc2:EmptyDisplayGridView Width="100%" Style='clear: both; margin-bottom:0px' PagerStyle-HorizontalAlign="Center"
                            PagerStyle-CssClass="defaultPagingBar" CssClass="table table-primary mb30 table-bordered table-hover" UseAccessibleHeader="true"
                            ShowHeaderWhenEmpty="True" ID="gvwProjects" runat="server" DataKeyNames="LeaveProjectId"
                            AutoGenerateColumns="False" CellPadding="4" GridLines="None" ShowFooterWhenEmpty="False"
                            OnPageIndexChanged="gvwRoles_PageIndexChanged" OnPageIndexChanging="gvwRoles_PageIndexChanging"
                            OnSelectedIndexChanged="gvwRoles_SelectedIndexChanged" OnRowDeleting="gvwRoles_RowDeleting">
                            <Columns>
                                <%--<asp:BoundField HeaderText="SN" DataField="" />--%>
                                <asp:TemplateField HeaderText="Branch" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="150px">
                                    <ItemTemplate>
                                        <%# Eval("BranchName")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                             <%--   <asp:TemplateField HeaderText="Department" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="150px">
                                    <ItemTemplate>
                                        <%# Eval("DepartmentName")%>
                                    </ItemTemplate>
                                </asp:TemplateField>--%>
                                <asp:TemplateField HeaderText="Team Name" FooterStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Left"
                                    HeaderStyle-Width="250px">
                                    <ItemTemplate>
                                        <%# Eval("Name")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                              <%--  <asp:TemplateField HeaderText="Team Manager" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="300px">
                                    <ItemTemplate>
                                        <%# Eval("ProjectManagerName")%>
                                    </ItemTemplate>
                                </asp:TemplateField>--%>
                                <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderStyle-Width="50px">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="ImageButton1" runat="server" CommandName="Select" ImageUrl="~/images/edit.gif" />
                                        &nbsp;
                                        <asp:ImageButton ID="ImageButton2" OnClientClick="return confirm('Do you really want to delete the team?')"
                                            runat="server" CommandName="Delete" ImageUrl="~/images/delet.png" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                </asp:TemplateField>
                            </Columns>
                            <PagerSettings Mode="NumericFirstLast" NextPageText="Next �" PreviousPageText="� Prev"
                                FirstPageText="First" LastPageText="Last" />
                            <RowStyle CssClass="odd" />
                            <AlternatingRowStyle CssClass="even" />
                            <PagerStyle CssClass="defaultPagingBar" />
                            <SelectedRowStyle CssClass="selected" />
                            <EmptyDataTemplate>
                                <b>No Teams. </b>
                            </EmptyDataTemplate>
                        </cc2:EmptyDisplayGridView>
                        <uc1:PagingCtl ID="pagintCtl" OnNextRecord="btnNext_Click" OnPrevRecord="btnPrevious_Click"
                            OnDropDownShowRecordsChanged="ddlRecords_SelectedIndexChanged" runat="server" />
                    </div>
                    <div style='margin-top: 15px'>
                    </div>
                    <%-- <div class="buttonsDiv">
        
     <asp:Button ID="btnRole"  CssClass="addbtns" runat="server" Text="Add" />
        
        </div>--%>
                    <!--POPUP-->
                    <div id="blanket" style="display: none;">
                    </div>
                    <div id="popUpDiv" style="display: none; height: 250px">
                        <div id="titleBar">
                            <span id="title">ADD/EDIT TEAM</span> <span id="close"><a href="#" onclick="popup('popUpDiv')">
                            </a></span>
                        </div>
                        <asp:Panel runat="server" DefaultButton="btnInsertUpdate" Style="padding: 0px 0px 0px 10px;
                            clear: both;">
                            <table>
                                <tr runat="server" id="Tr3">
                                    <td class="fieldHeader">
                                        Team Name *
                                    </td>
                                    <td style="padding-top: 10px;">
                                        <asp:TextBox ID="txtName" Width="198" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" SetFocusOnError="true" ValidationGroup="user"
                                            ControlToValidate="txtName" Display="None" ErrorMessage="Team name is required."
                                            runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="Tr1">
                                    <td class="fieldHeader">
                                        Branch *
                                    </td>
                                    <td style="padding-top: 10px;">
                                        <asp:DropDownList ID="ddlBranch" Width="200px" DataTextField="Name" DataValueField="BranchId"
                                            runat="server" AppendDataBoundItems="true">
                                            <asp:ListItem Text="" Value="-1" />
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator InitialValue="-1" ID="RequiredFieldValidator1" SetFocusOnError="true"
                                            ValidationGroup="user" ControlToValidate="ddlBranch" Display="None" ErrorMessage="Branch is required."
                                            runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="Tr4" style="display: none">
                                    <td class="fieldHeader">
                                        Department *
                                    </td>
                                    <td style="padding-top: 10px;">
                                        <asp:DropDownList ID="ddlDepartment" Width="200px" DataTextField="Name" DataValueField="DepartmentId"
                                            runat="server" AppendDataBoundItems="true">
                                            <asp:ListItem Text="" Value="-1" />
                                        </asp:DropDownList>
                                        <%-- <asp:RequiredFieldValidator InitialValue="-1" ID="RequiredFieldValidator4" SetFocusOnError="true"
                                ValidationGroup="user" ControlToValidate="ddlDepartment" Display="None" ErrorMessage="Department name is required."
                                runat="server" />--%>
                                    </td>
                                </tr>
                                <tr runat="server" id="Tr5" style="display:none">
                                    <td class="fieldHeader">
                                        Manager
                                    </td>
                                    <td style="padding-top: 10px;">
                                        <ext:Store ID="storeEmployee" runat="server">
                                            <Model>
                                                <ext:Model runat="server">
                                                    <Fields>
                                                        <ext:ModelField Name="Name" />
                                                        <ext:ModelField Name="EmployeeId" Type="String" />
                                                    </Fields>
                                                </ext:Model>
                                            </Model>
                                        </ext:Store>
                                        <ext:ComboBox ID="ddlProjectManagerEmployee" Width="200" ForceSelection="false" QueryMode="Local"
                                            runat="server" StoreID="storeEmployee" Editable="true" DisplayField="Name" ValueField="EmployeeId"
                                            Mode="Local" EmptyText="">
                                            <Listeners>
                                                <Blur Handler="if( this.getValue() == this.getText()) this.clearValue();" />
                                            </Listeners>
                                        </ext:ComboBox>
                                        <%-- <asp:DropDownList ID="ddlProjectManagerEmployee" Width="180px" DataTextField="Name" DataValueField="EmployeeId"
                                runat="server" AppendDataBoundItems="true">
                                <asp:ListItem Text="" Value="-1" />
                            </asp:DropDownList>--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-top: 20px;" colspan="2">
                                        <%-- <asp:ValidationSummary ValidationGroup="save" DisplayMode="SingleParagraph" ShowSummary="false"
                                ShowMessageBox="true" runat="server"></asp:ValidationSummary>--%>
                                        <asp:Button ValidationGroup="save" OnClientClick="valGroup='user';return CheckValidation()"
                                            CssClass="save" ID="btnInsertUpdate" runat="server" Text="Save" OnClick="btnInsertUpdate_Click" />
                                        <asp:Button ID="btnCancel" CssClass="cancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </div>
                    <a href="#" onclick="popup('popUpDiv')" style="height: 30px" class="btn btn-success btn-sm">
                        Create New</a>
                    <!-- / POPUP-->
                </div>
            </div>
        </div>
    </div>
</asp:Content>
