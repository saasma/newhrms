<%@ Page MaintainScrollPositionOnPostback="true" Title="Change Email Setting" Language="C#"
    MasterPageFile="~/Master/HROld.Master" AutoEventWireup="true" CodeBehind="DeviceAttenanceSync.aspx.cs"
    Inherits="Web.CP.DeviceAttenanceSync" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <ext:ResourceManager ID="ResourceManager1" DisableViewState="false" runat="server"
        ShowWarningOnAjaxFailure="false" ScriptMode="Release">
    </ext:ResourceManager>
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Attendance Data Upload
                </h4>
            </div>
        </div>
    </div>
    
        <div class="contentArea">
            <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
            <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
            Upload Device attendnace of your branch
         
        </div>
        <div cssclass="clear buttonsDiv" id="divButtons" runat="server" style='margin-left: 10px'>
            <ext:Button Text="Upload" runat="server" ID="btnUpload" Cls="btn btn-primary btn-sm btn-sect">
                <DirectEvents>
                    <Click OnEvent="btnUpload_Click">
                        <EventMask ShowMask="true">
                        </EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </div>
</asp:Content>
