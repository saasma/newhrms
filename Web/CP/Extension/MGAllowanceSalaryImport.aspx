<%@ Page MaintainScrollPositionOnPostback="true" Title="Allowance Salary" Language="C#"
    MasterPageFile="~/Master/Details.Master" AutoEventWireup="true" CodeBehind="MGAllowanceSalaryImport.aspx.cs"
    Inherits="Web.CP.MGAllowanceSalaryImport" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<%@ Register src="UserControls/MGAllowanceCtl.ascx" tagname="VariableImportCtl" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <ext:ResourceManager ID="ResourceManager1"  DisableViewState="false"  runat="server" ShowWarningOnAjaxFailure="false"
        ScriptMode="Release">
    </ext:ResourceManager>
  
    <uc1:VariableImportCtl ID="VariableImportCtl1" runat="server" />
  
</asp:Content>
