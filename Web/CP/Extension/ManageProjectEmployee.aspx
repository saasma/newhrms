﻿<%@ Page Title="Manage Project Employee" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="ManageProjectEmployee.aspx.cs" Inherits="Web.CP.Extension.ManageProjectEmployee" %>

<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningMsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="InfoMsgCtl" TagPrefix="uc2" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        function popupUpdateProject(employeeId) {

            var ret = popup('Id=' + employeeId);

            if (typeof (ret) != 'undefined') {
                if (ret == 'Reload') {
                    __doPostBack('Reload', '');
                }

            }
        }

        function reloadProjectEmployee(childWindow) {
            childWindow.close();
            __doPostBack('Reload', '');
        }


        function ACE_item_selected(source, eventArgs) {
            var val = eventArgs.get_value();
            if (val != null) {
                var hdnEmpId = document.getElementById('<%= hdnEmpId.ClientID %>');
                hdnEmpId.value = val;
            } else {
                alert("First select the employee from the list.");
            }
        }

        function clearEmp() {
            var hdnEmpId = document.getElementById('<%= hdnEmpId.ClientID %>');
            hdnEmpId.value = '';
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <uc2:InfoMsgCtl ID="msgInfo" EnableViewState="false" Hide="true" runat="server" />
    <uc2:WarningMsgCtl ID="msgWarning" EnableViewState="false" Hide="true" runat="server" />
    <asp:HiddenField ID="hdnEmpId" runat="server" />
    <div style="display: none;">
    </div>
     <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Add Project to Employees
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
    <div class="contentArea">
        
        <div class="attribute">
            <table class="fieldTable" cellpadding="0" style='padding-left: 3px' cellspacing="5">
                <tr>
                    <td>
                        Search Employee
                    </td>
                    <td>
                        <asp:TextBox ID="txtEmpSearchText" Width="170px" Style='width: 200px; border-radius: 5px;
                            margin-top: 4px; margin-left: 10px; height: 20px;' runat="server" onchange="if(this.value=='') clearEmp();"></asp:TextBox>
                        <cc1:TextBoxWatermarkExtender ID="TBWE2" runat="server" TargetControlID="txtEmpSearchText"
                            WatermarkText="Employee Search" WatermarkCssClass="searchBoxText" />
                        <cc1:AutoCompleteExtender EnableCaching="true" ID="AutoCompleteExtenderOrganization"
                            runat="server" MinimumPrefixLength="2" ServiceMethod="GetEmployeeNames" ServicePath="~/PayrollService.asmx"
                            TargetControlID="txtEmpSearchText" OnClientItemSelected="ACE_item_selected" CompletionSetCount="10"
                            CompletionInterval="250" CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList"
                            CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
                        </cc1:AutoCompleteExtender>
                        <asp:RequiredFieldValidator ValidationGroup="AEEmployee" ID="valEmployeeReq" ControlToValidate="txtEmpSearchText"
                            Display="None" runat="server" ErrorMessage="Please select an Employee."></asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 20px;">
                    </td>
                    <td>
                        <%--<asp:Button ID="btnLoadEmpDetls" OnClick="" Text="Load" runat="server" CssClass="save" />--%>
                        <asp:LinkButton ID="btnLoad" runat="server" OnClick="btnLoadEmpDetls_Click" Text="Load"
                            CssClass="load" Style='float: right; padding-right: 30px' />
                    </td>
                </tr>
            </table>
        </div>
        <br />
        <div style="clear: both">
            <cc2:EmptyDisplayGridView Width="100%" Style='clear: both' PagerStyle-HorizontalAlign="Center"
                PagerStyle-CssClass="defaultPagingBar" CssClass="tableLightColor" UseAccessibleHeader="true"
                ShowHeaderWhenEmpty="True" ID="gvwProjectEmployee" runat="server" DataKeyNames="EmployeeId"
                AutoGenerateColumns="False" CellPadding="4" GridLines="None" ShowFooterWhenEmpty="False"
                OnPageIndexChanging="gvwProjectEmployee_PageIndexChanging" OnRowCreated="gvwProjectEmployee_RowCreated">
                <Columns>
                    <asp:TemplateField HeaderText="EIN" FooterStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Left"
                        HeaderStyle-Width="70px">
                        <ItemTemplate>
                            <%# Eval("EmployeeId")%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="I No" FooterStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Left"
                        HeaderStyle-Width="100px">
                        <ItemTemplate>
                            <%# Eval("IdCardNo")%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Employee Name" FooterStyle-HorizontalAlign="Right"
                        HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="150px">
                        <ItemTemplate>
                            <%# Eval("Name")%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Project Name" FooterStyle-HorizontalAlign="Right"
                        HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="350px">
                        <ItemTemplate>
                            <%# Eval("ProjectNameList")%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left"></asp:TemplateField>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderStyle-Width="50px">
                        <ItemTemplate>
                            <asp:ImageButton ID="ImageButton1" runat="server" CommandName="Select" ImageUrl="~/images/edit.gif"
                                ToolTip="Edit" OnClientClick='<%# "return popupUpdateProject(" + Eval("EmployeeId") + ");" %>' />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                    </asp:TemplateField>
                </Columns>
                <PagerSettings Mode="NumericFirstLast" NextPageText="Next »" PreviousPageText="« Prev"
                    FirstPageText="First" LastPageText="Last" />
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                <PagerStyle CssClass="defaultPagingBar" />
                <SelectedRowStyle CssClass="selected" />
                <EmptyDataTemplate>
                    <b>No employee. </b>
                </EmptyDataTemplate>
            </cc2:EmptyDisplayGridView>
            <uc1:PagingCtl ID="pagintCtl" OnNextRecord="btnNext_Click" OnPrevRecord="btnPrevious_Click"
                OnDropDownShowRecordsChanged="ddlRecords_SelectedIndexChanged" runat="server" />
        </div>
        <div style='margin-top: 15px'>
        </div>
    </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
