﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using BLL;
using DAL;
using Ext.Net;
using Utils.Web;
using Utils.Helper;
using System.Text;
using Newtonsoft.Json.Linq;
using BLL.BO;

namespace Web.CP
{
    public partial class MailMerge : BasePage
    {
        CommonManager commonMgr = new CommonManager();

        public bool isNsetDashainAmountImport = true;

        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.HumanResource;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                Initialise();





        }


        public void btnRefresh_Click(object sender, DirectEventArgs e)
        {
            LoadEmployeeGrid();
        }

        public void cboBranch_Select(object sender, DirectEventArgs e)
        {
            
        }

        public void btnHistory_Click(object sender, DirectEventArgs e)
        {
            List<DAL.MailMerge> HistoryList = new List<DAL.MailMerge>();
            HistoryList = MailMergeManager.getAllEmailHistory();
            storeHistory.DataSource = HistoryList;
            storeHistory.DataBind();

            Window1.Center();
            Window1.Show();
        }


        public void btnSave_Click(object sender, DirectEventArgs e)
        {

            String orisubject;
            String oribody;

            orisubject = txtSubject.Text.Trim();
            oribody = HtmlEditor1.Text.Trim();
            List<EmailBody> EmailList = new List<EmailBody>();
            EmailBody EmailInstance;

            List<MailMergeExcelTemp> tempExcelData = new List<MailMergeExcelTemp>();
            tempExcelData = MailMergeManager.getMailMergeExcelTempRows();
            EEmployee eEmp;

            List<DAL.MailMerge> mailMergeList = new List<DAL.MailMerge>();
            List<DAL.PayrollMessage> payrollMsg = new List<DAL.PayrollMessage>();

            foreach (var row in tempExcelData)
            {

                int EmpID = int.Parse(row.EmployeeID);
                eEmp = new EEmployee();
                EmailInstance = new EmailBody();

                eEmp = EmployeeManager.GetEmployeeById(EmpID);
                if (eEmp != null)
                {
                    
                    EmailInstance.ToAddress = eEmp.EAddresses.FirstOrDefault().CIEmail;

                    EmailInstance.EmployeeID = eEmp.EmployeeId;
                    UUser user = eEmp.UUsers.FirstOrDefault();

                    string subject = orisubject;
                    string body = oribody;

                    if (user != null)
                        EmailInstance.EmpUserID = user.UserName;

                    List<MailMergeTemplateHeader> headers = new List<MailMergeTemplateHeader>();
                    headers = MailMergeManager.GetAllHeaderList(1);

                    foreach (var val1 in headers)
                    {
                        int HeaderIndex = val1.Type;
                        string replacingVlaue = "";

                        if (HeaderIndex == 1)
                            replacingVlaue = row.Column1;
                        if (HeaderIndex == 2)
                            replacingVlaue = row.Column2;
                        if (HeaderIndex == 3)
                            replacingVlaue = row.Column3;
                        if (HeaderIndex == 4)
                            replacingVlaue = row.Column4;
                        if (HeaderIndex == 5)
                            replacingVlaue = row.Column5;
                        if (HeaderIndex == 6)
                            replacingVlaue = row.Column6;
                        if (HeaderIndex == 7)
                            replacingVlaue = row.Column7;
                        if (HeaderIndex == 8)
                            replacingVlaue = row.Column8;
                        if (HeaderIndex == 9)
                            replacingVlaue = row.Column9;
                        if (HeaderIndex == 10)
                            replacingVlaue = row.Column10;
                        if (HeaderIndex == -2)
                            replacingVlaue = eEmp.Name;

                        string repValue = "{" + val1.HeaderText + "}";
                        EmailInstance.Subject = subject.Replace(repValue, replacingVlaue);
                        subject = EmailInstance.Subject;

                        EmailInstance.Body = body.Replace(repValue, replacingVlaue);
                        body = EmailInstance.Body;

                        
                    }

                    EmailList.Add(EmailInstance);
                    
                }
                
                
               
            }

            foreach (var mailInst in EmailList)
            {
                if (!string.IsNullOrEmpty(mailInst.ToAddress))
                {
                    bool isSendSuccess = SMTPHelper.SendMail(mailInst.ToAddress, mailInst.Body, mailInst.Subject, "",txtFromName.Text.Trim(),txtFromEmail.Text.Trim());

                    if (isSendSuccess)
                    {
                        if (!string.IsNullOrEmpty(mailInst.EmpUserID))
                        {
                            DAL.PayrollMessage msg = new DAL.PayrollMessage();
                            msg.Subject = mailInst.Subject;
                            msg.Body = mailInst.Body;
                            //message.DueDateEng = inst.SubmissionDate;
                            msg.IsRead = false;
                            DateTime date = SessionManager.GetCurrentDateAndTime();
                            msg.ReceivedDate = date.ToShortDateString();
                            msg.ReceivedDateEng = date;
                            msg.ReceivedTo = mailInst.EmpUserID;
                            msg.SendBy = SessionManager.UserName;
                            payrollMsg.Add(msg);

                        }


                        mailMergeList.Add(new DAL.MailMerge
                        {
                            Subject = mailInst.Subject,
                            Body = mailInst.Body,
                            ToAddress = mailInst.ToAddress,
                            SentDate = CommonManager.GetCurrentDateAndTime(),
                            CreatedBy = SessionManager.CurrentLoggedInUserID,
                            EmployeeId = mailInst.EmployeeID
                        });


                    }

                }

            }


            MailMergeManager.InsertAllPayrollMessage(payrollMsg);
            MailMergeManager.InsertMailHistory(mailMergeList);
            X.Msg.Show(new MessageBoxConfig { Icon = MessageBox.Icon.INFO, Title = "Message", Message = "Email Sent to " + mailMergeList.Count().ToString() + " Employees", Buttons = MessageBox.Button.OK });
                

            /*
                EmailContent content = new EmailContent();
                content.CompayId = SessionManager.CurrentCompanyId;
                content.Subject = txtSubject.Text.Trim();
                content.Body = HtmlEditor1.Text.Trim();

                CommonManager.SaveOrUpdateEmailContent(content);
                X.Msg.Show(new MessageBoxConfig { Icon = MessageBox.Icon.INFO, Title = "Message", Message = "Data saved successfully.", Buttons = MessageBox.Button.OK });
            */
            
        }

        public void Initialise()
        {

            storeTemplateHeader.DataSource = MailMergeManager.GetAllHeaderList(null);

           
            LoadEmployeeGrid();

        }



        public void LoadEmployeeGrid()
        {
            List<EEmployee> empList = new List<EEmployee>();
            List<MailMergeExcelTemp> tempList = new List<MailMergeExcelTemp>();

            tempList = MailMergeManager.getMailMergeExcelTempRows().OrderBy(x=>x.EmployeeName).ToList();
            EEmployee eEmp;

            int sn = 0;
            foreach (var temp1 in tempList)
            {
                temp1.SN = ++sn;

                //int EmployeeID;
                //bool isEmpID = int.TryParse(temp1.EmployeeID, out EmployeeID);
                //if (isEmpID)
                //{
                //    eEmp = EmployeeManager.GetEmployeeById(EmployeeID);
                //    if (eEmp != null)
                //    {
                //        empList.Add(eEmp);
                //    }
                //}

            }


            storeEmployee.DataSource = tempList;
            storeEmployee.DataBind();

            

        }


        protected void btnImport_Click(object sender, DirectEventArgs e)
        {
            string script = string.Format("window.open('../../ExcelWindow/MailMergeImport.aspx', 'Import', 'menubar=no,width=430,height=550,toolbar=no');");

            X.Js.AddScript(script);
        }


        protected void btnDetailSaveUpdate_Click(object sender, DirectEventArgs e)
        {
            List<MailMergeTemplateHeader> lines = new List<MailMergeTemplateHeader>();
            string json = e.ExtraParams["Values"];

            if (string.IsNullOrEmpty(json))
            {
                return;
            }

            lines = JSON.Deserialize<List<MailMergeTemplateHeader>>(json);
            
            Status status = MailMergeManager.UpdateHeaders(lines);
            if (status.IsSuccess)
            {
                Window1.Hide();
                //LoadLevels();
                NewMessage.ShowNormalPopup("Template Headers saved.");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
            
        }

    }

}

