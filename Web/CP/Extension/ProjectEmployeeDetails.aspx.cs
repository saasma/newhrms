﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using DAL;
using BLL;
using Utils.Helper;

namespace Web.CP.Extension
{
    public partial class ProjectEmployeeDetails : BasePage
    {
        List<Project> projectList = new List<Project>();
        protected void Page_Load(object sender, EventArgs e)
        {
            List<Project> prjList = ProjectManager.GetProjects();
            prjList.RemoveAt(0);
            projectList = prjList;

            if (!IsPostBack)
            {
                Initialise();
            }
        }

        private void Initialise()
        {
            int employeeId = int.Parse(Request.QueryString["Id"].ToString());
            hdnEmpId.Value = Request.QueryString["Id"];

            List<Project> source = projectList;
            List<Project> dest = ProjectManager.GetDestPrjListByEmpId(employeeId);

            foreach (var item in dest)
            {
                source.Remove(item);
            }

            lstSource.DataSource = source;
            lstSource.DataBind();

            lstDest.DataSource = dest;
            lstDest.DataBind();

            btnSave.Text = Resources.Messages.Update;

            branchTitle.InnerHtml += new EmployeeManager().GetById(employeeId).Name;
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            for (int i = lstSource.Items.Count - 1; i >= 0; i--)
            {
                ListItem item = lstSource.Items[i];
                if (item.Selected)
                {
                    lstSource.Items.Remove(item);

                    lstDest.Items.Add(item);
                }
            }
        }

        protected void btnRemove_Click(object sender, EventArgs e)
        {
            for (int i = lstDest.Items.Count - 1; i >= 0; i--)
            {
                ListItem item = lstDest.Items[i];
                if (item.Selected)
                {
                    lstDest.Items.Remove(item);

                    lstSource.Items.Add(item);
                }
            }
        }

        protected void btnAddAll_Click(object sender, EventArgs e)
        {
            lstSource.DataSource = new List<GetDepartmentListResult>();
            lstSource.DataBind();

            lstDest.DataSource = projectList;
            lstDest.DataBind();
        }

        protected void btnRemoveAll_Click(object sender, EventArgs e)
        {
            lstSource.DataSource = projectList;
            lstSource.DataBind();

            lstDest.DataSource = new List<GetDepartmentListResult>();
            lstDest.DataBind();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            EEmployee emp = new EEmployee();
            emp.EmployeeId = int.Parse(hdnEmpId.Value);           

            foreach (ListItem item in lstDest.Items)
            {
                emp.ProjectEmployees.Add(new ProjectEmployee { ProjectId = int.Parse(item.Value) });
            }

            string result = ProjectManager.AddProjectToEmployee(emp, chkToAll.Checked);
            if (result == string.Empty)
            {
                JavascriptHelper.DisplayClientMsg("Projects added to the employee successfully.", Page, "closePopup();");
            }
            else
            {
                divWarningMsg.InnerHtml = result;
                divWarningMsg.Hide = false;
                return;
            }
        }
    }
}