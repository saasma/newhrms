﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using BLL;
using DAL;
using Ext.Net;
using Utils.Web;
using Utils.Helper;
using System.Text;
using Newtonsoft.Json.Linq;
using BLL.BO;
using System.Web.Configuration;
using System.Net.Configuration;

namespace Web.CP
{
    public partial class ManageCheckList : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.AdministratorTools;
            }
        }     


        CommonManager commonMgr = new CommonManager();

        public bool isNsetDashainAmountImport = true;



        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                Initialise();


           

                     
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

            if (Page.IsValid)
            {

                
            }
        }

        public void btnSaveUpdate_Click(object sender, EventArgs e)
        {
            string action = txtDesc.Text.Trim();
            int order = 0;
            int.TryParse(txtOrder.Text.Trim(), out order);
            int checkListId = 0;
            if (list.SelectedIndex != -1)
                checkListId = (int)list.DataKeys[list.SelectedIndex][0];

            if (action != "")
            {
                CheckList entity = new CheckList();
                entity.Order = order;
                entity.Description = action;
                entity.IsCompleted = false;
                entity.CheckListId = checkListId;

                CommonManager.SaveOrUpdate(entity);

                txtDesc.Text = "";
                txtOrder.Text = "";

                list.SelectedIndex = -1;
                Initialise();
            }
        }

        protected void btnTestMail_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {

               
            }
        }

        public void Initialise()
        {

            //Load Content Type
            list.DataSource = CommonManager.GetAllCheckList();
            list.DataBind();

        }

        protected void list_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            int checkListId = (int)list.DataKeys[list.SelectedIndex][0];
            CheckList entity = CommonManager.GetCheckListById(checkListId);
            if (entity != null)
            {
                if (entity.Order != 0)
                    txtOrder.Text = entity.Order.ToString();
                else
                    txtOrder.Text = "";
                txtDesc.Text = entity.Description.Trim();
                windowPopup.Show();
            }
        }

        protected void btnTestMail_Click1(object sender, EventArgs e)
        {
            txtDesc.Text = "";
            txtOrder.Text = "";

            list.SelectedIndex = -1;
            Initialise();

            windowPopup.Show();
        }

        protected void btnMarkAllCompleted_Click(object sender, EventArgs e)
        {
            CommonManager.MarkAll(true);
            Initialise();
        }

        protected void btnAllRemining_Click(object sender, EventArgs e)
        {
            CommonManager.MarkAll(false);
            Initialise();
        }

        protected void list_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int id = (int)list.DataKeys[e.RowIndex][0];

            CommonManager.DeleteCheckList(id);
            Initialise();
        }
    }

}

