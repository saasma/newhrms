<%@ Page Title="Manage Employee Projects" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="ManageEmployeeProjects.aspx.cs" Inherits="Web.User.ManageEmployeeProjects" %>

<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningMsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="InfoMsgCtl" TagPrefix="uc2" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">


        function filterCallback(chk) {

        }
        function validateDOJToDate(source, args) {
            //debugger;



        }
        function refreshWindow() {
            window.location.reload();
        }

        function isPayrollSelected(btn) {

            var value = document.getElementById('<%= ddlDepartment.ClientID %>').value;

            if (value == null)
                value = -1;
            var ret = shiftPopup("DepartmentId=" + value);
            return false;
        }
  
    </script>
     <style type="text/css">
    .toptableGap tr td
    {
    	padding-right:5px;
    }
   </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
     <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Associate Employees with Teams for Leave Request and Approval
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel" style="">
    
    <ext:ResourceManager ID="ResourceManager1"  DisableViewState="false" runat="server" ShowWarningOnAjaxFailure="false"
        ScriptMode="Release" />
    <div class="contentArea">
        
        <asp:Panel runat="server" DefaultButton="btnLoad" style="padding:10px" class="attribute">
            <table class="toptableGap">
                <tr>
                    <td runat="server" id="rowBranch1">
                        <strong>Branch</strong>
                    </td>
                    <td runat="server" id="rowDep1">
                        <strong>Department</strong>
                    </td>
                     <td runat="server" id="Td1">
                        <strong>Designation</strong>
                    </td>
                    <td>
                        <strong>Search</strong>
                    </td>
                    <td>
                        <strong>Team</strong>
                    </td>
                    <td rowspan="2" valign="middle" style="padding-top: 8px;">
                        <asp:LinkButton ID="btnLoad" runat="server" OnClick="btnLoad_Click" Text="Load" CssClass="btn btn-default btn-sm"
                            Style='width: 80px' />
                    </td>
                </tr>
                <tr>
                    <td runat="server" id="rowBranch2">
                        <asp:DropDownList ID="ddlBranch"
                            Width="160px" DataTextField="Name" DataValueField="BranchId" AutoPostBack="true"
                            AppendDataBoundItems="true" runat="server">
                            <asp:ListItem Text="" Value="-1" />
                        </asp:DropDownList>
                    </td>
                    <td runat="server" id="rowDep2">
                        <asp:DropDownList ID="ddlDepartment" Width="160px" DataTextField="Name" DataValueField="DepartmentId"
                            runat="server" AppendDataBoundItems="true">
                            <asp:ListItem Text="" Value="-1" />
                        </asp:DropDownList>
                    </td>
                     <td runat="server" id="Td2">
                        <asp:DropDownList ID="ddlDesignation" Width="160px" DataTextField="LevelAndDesignation" DataValueField="DesignationId"
                            runat="server" AppendDataBoundItems="true">
                            <asp:ListItem Text="" Value="-1" />
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:TextBox Width="180px" ID="txtSearch" runat="server" />
                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtSearch"
                            WatermarkText="Employee Name" WatermarkCssClass="searchBoxText" />
                        <cc1:AutoCompleteExtender EnableCaching="true" ID="AutoCompleteExtenderOrganization"
                            runat="server" MinimumPrefixLength="2" ServiceMethod="GetEmployeeNames" ServicePath="~/PayrollService.asmx"
                            TargetControlID="txtSearch" CompletionSetCount="10" CompletionInterval="250"
                            CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList"
                            CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
                        </cc1:AutoCompleteExtender>
                    </td>
                    <td>
                     <asp:DropDownList ID="ddlProjectOrTeam" AppendDataBoundItems="true" Width="200" runat="server" DataTextField="Name" DataValueField="LeaveProjectId">
                          <asp:ListItem Text="--Select Team--" Value="-1"></asp:ListItem>
                            </asp:DropDownList>
                    </td>
                    <td style='padding-left: 10px'>
                        <asp:LinkButton ID="btnExport" runat="server" Text="Excel import" OnClientClick="return isPayrollSelected(this)"
                            CssClass=" excel marginRight tiptip" Style="float: left;" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
                <uc2:InfoMsgCtl ID="msgInfo" EnableViewState="false" Hide="true" runat="server" />
        <uc2:WarningMsgCtl ID="msgWarning" EnableViewState="false" Hide="true" runat="server" />
        <div style="clear: both">
            <p style="padding-bottom: 5px;">
                <strong style='color: #EE6B57'>(Please make sure a Manager has been assigned the Team,
                    can be any team if multiple team under the same manager exists.)</strong>
            </p>
            <cc2:EmptyDisplayGridView Width="100%" Style='clear: both; margin-bottom:0px;' PagerStyle-HorizontalAlign="Center"
                PagerStyle-CssClass="defaultPagingBar" CssClass="table table-primary mb30 table-bordered table-hover
" UseAccessibleHeader="true"
                ShowHeaderWhenEmpty="True" ID="gvwProjects" runat="server" DataKeyNames="LeaveProjectEmployeeId,EmployeeId"
                AutoGenerateColumns="False" OnRowCreated="gvwEmployees_RowCreated" CellPadding="4" GridLines="None" ShowFooterWhenEmpty="False"
                OnPageIndexChanged="gvwRoles_PageIndexChanged" OnRowDataBound="gv_RowDataBound"
                OnPageIndexChanging="gvwRoles_PageIndexChanging" OnSelectedIndexChanged="gvwRoles_SelectedIndexChanged"
                OnRowEditing="gvwProjects_RowEditing" 
                onrowupdating="gvwProjects_RowUpdating">
                <Columns>
                    <%--<asp:BoundField HeaderText="SN" DataField="" />--%>
                    <asp:TemplateField HeaderText="I No" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="150px">
                        <ItemTemplate>
                            <%# Eval("IdCardNo")%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Department" FooterStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Left"
                        HeaderStyle-Width="300px">
                        <ItemTemplate>
                            <%# Eval("DepartmentName")%></ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Designation" FooterStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Left"
                        HeaderStyle-Width="300px">
                        <ItemTemplate>
                            <%# Eval("Designation")%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Employee Name" HeaderStyle-HorizontalAlign="Left"
                        HeaderStyle-Width="300px">
                        <ItemTemplate>
                            <%# Eval("Name")%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Team" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="300px">
                        <ItemTemplate>
                            <%# Eval("ProjectName")%></ItemTemplate>
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlProjectOrTeam1" Width="200" runat="server" DataTextField="Name" DataValueField="LeaveProjectId">
                            </asp:DropDownList>
                        </EditItemTemplate>
                    </asp:TemplateField>
                      <asp:TemplateField HeaderText="Team 2" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="300px">
                        <ItemTemplate>
                            <%# Eval("ProjectName2")%></ItemTemplate>
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlProjectOrTeam2" Width="200" runat="server" DataTextField="Name" DataValueField="LeaveProjectId">
                            </asp:DropDownList>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Manager" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="300px">
                        <ItemTemplate>
                            <%# Eval("Manager")%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderStyle-Width="50px">
                        <ItemTemplate>
                            <asp:ImageButton ID="ImageButton1" runat="server" CommandName="Edit" ToolTip="Edit" ImageUrl="~/images/edit.gif" />
                            
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ImageButton1" runat="server" ToolTip="Save" CommandName="Update" ImageUrl="~/css/images/save.png" />
                            
                        </EditItemTemplate>
                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                    </asp:TemplateField>
                </Columns>
                <PagerSettings Mode="NumericFirstLast" NextPageText="Next �" PreviousPageText="� Prev"
                    FirstPageText="First" LastPageText="Last" />
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                <PagerStyle CssClass="defaultPagingBar" />
                <SelectedRowStyle CssClass="selected" />
                
                <EditRowStyle CssClass="selected" BackColor="LightYellow" />
                <EmptyDataTemplate>
                    <b>No list. </b>
                </EmptyDataTemplate>
            </cc2:EmptyDisplayGridView>
            <uc1:PagingCtl ID="pagintCtl" OnNextRecord="btnNext_Click" OnPrevRecord="btnPrevious_Click"
                OnDropDownShowRecordsChanged="ddlRecords_SelectedIndexChanged" runat="server" />
        </div>
        <div style='margin-top: 15px'>
        </div>

   
    </div>

    </div>
</asp:Content>
