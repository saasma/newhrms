﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;

namespace Web.CP
{
    public partial class CITImportExcel : BasePage
    {

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.CustomId = UrlHelper.GetIdFromQueryString("payrollPeriod");                
            }

            
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "window.onunload = null;");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"),true);
        }
       
       


        protected void btnExport_Click(object sender, EventArgs e)
        {

            string template = ("~/App_Data/ExcelTemplate/CITImport.xlsx");        
                 
            // Retrieve data from SQL Server table.
            List<Nset_GetEmployeeListForCITResult> list = PayManager.GetNsetCITList();

            ExcelGenerator.ExportNsetCIT(template, list);
        }


        

        private List<int> GetEmployeeIdList()
        {
            List<Nset_GetEmployeeListForCITResult> list = PayManager.GetNsetCITList();
            List<int> ids = new List<int>();
            foreach (Nset_GetEmployeeListForCITResult item in list)
            {
                ids.Add(item.EIN);
            }
            return ids;
        }

        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");


            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);


                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }

                StringBuilder xml = new StringBuilder();
                xml.Append("<root>");

                int employeeId;
                decimal? Month6, Month7, Month8, Month9, Month10, Month11, Month12, Month1, Month2, Month3, Month4, Month5;
               
                List<int> employeeIDList = GetEmployeeIdList();

                int importedCount = 0;

                try
                {

                    foreach (DataRow row in datatable.Rows)
                    {

                        int.TryParse(row["EIN"].ToString(), out employeeId);
                        
                        if (!employeeIDList.Contains(employeeId))
                        {
                            divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_EmployeeIDNotFound, employeeId);
                            divWarningMsg.Hide = false;
                            return;
                        }

                        importedCount += 1;

                        Month6 = ExcelGenerator.GetAmountValueFromCell("July-2011", row, employeeId);
                        //Days_At_Kirne = ExcelGenerator.GetValueFromCell("Days_At_Kirne", row, employeeId);

                        Month7 = ExcelGenerator.GetAmountValueFromCell("July-2011", row, employeeId);

                        Month8 = ExcelGenerator.GetAmountValueFromCell("Sep-2011", row, employeeId);
                        Month9 = ExcelGenerator.GetAmountValueFromCell("Oct-2011", row, employeeId);
                        Month10 = ExcelGenerator.GetAmountValueFromCell("Nov-2011", row, employeeId);
                        Month11 = ExcelGenerator.GetAmountValueFromCell("Dec-2011", row, employeeId);

                        Month12 = ExcelGenerator.GetAmountValueFromCell("Jan-2012", row, employeeId);
                        Month1 = ExcelGenerator.GetAmountValueFromCell("Feb-2012", row, employeeId);
                        Month2 =ExcelGenerator.GetAmountValueFromCell("March-2012", row, employeeId);
                        Month3 = ExcelGenerator.GetAmountValueFromCell("April-2012", row, employeeId);

                        Month4 = ExcelGenerator.GetAmountValueFromCell("May-2012", row, employeeId);

                        Month5 = ExcelGenerator.GetAmountValueFromCell("June-2012", row, employeeId);
                        

                        xml.AppendFormat(
                           @"<row PayrollPeriodId='{0}' EmployeeId='{1}' 
                                        {2} {3} 
                                        {4}

                                        {5} {6} {7} {8}

                                        {9} {10} {11} {12} 
                                        {13}
                                        />",
                                        this.CustomId, employeeId,

                                   ExcelGenerator.GetColumnValue("Month6", Month6),
                                        ExcelGenerator.GetColumnValue("Month7", Month7),

                                        ExcelGenerator.GetColumnValue("Month8", Month8),

                                        ExcelGenerator.GetColumnValue("Month9", Month9),
                                        ExcelGenerator.GetColumnValue("Month10", Month10),
                                        ExcelGenerator.GetColumnValue("Month11", Month11),
                                        ExcelGenerator.GetColumnValue("Month12", Month12),

                                        ExcelGenerator.GetColumnValue("Month1", Month1),
                                        ExcelGenerator.GetColumnValue("Month2", Month2),
                                        ExcelGenerator.GetColumnValue("Month3", Month3),
                                        ExcelGenerator.GetColumnValue("Month4", Month4),

                                        ExcelGenerator.GetColumnValue("Month5", Month5)

                                        );

                    }



                    xml.Append("</root>");

                  PayManager.SaveNsetCIT(xml.ToString());

                    divMsgCtl.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ImportedEmployees, importedCount);
                    divMsgCtl.Hide = false;

                    this.HasImport = true;  

                }
                //if invalid value like string found
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                        exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                    //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }              
            }
        }
        
     
       
    }

   
}

