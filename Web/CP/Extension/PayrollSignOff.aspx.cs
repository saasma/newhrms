﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using DAL;
using BLL;
using Utils.Helper;
using Utils.Calendar;
using Utils.Web;

namespace Web.User
{
    public partial class PayrollSignOff : BasePage
    {
        //double percentTotal = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialise();
              //  ddlUserType_SelectedIndex(null, null);
            }         
        }

        private void Initialise()
        {

            if (SessionManager.User.EnablePayrollSignOff != null && SessionManager.User.EnablePayrollSignOff.Value)
            { }
            else
            {
                Response.Redirect("~/CP/Dashboard.aspx");
            }

            calFilterFrom.IsEnglishCalendar = true;


            DateTime date = DateTime.Now;

            PayManager.HasSignOffNotBeingDoneForLatestChanges(ref date);


            string today = date.Day + "/" + date.Month + "/" + date.Year;

            //DateTime startingDate = SessionManager.CurrentCompanyFinancialDate.StartingDateEng.Value;
            //string starging = startingDate.Day + "/" + startingDate.Month + "/" + startingDate.Year;

            calFilterFrom.SetSelectedDate(today, true);

            txtTime.Text = date.ToString("t");


            BindUsers();
        }

      

        void BindUsers()
        {

            

            DateTime start = new DateTime(calFilterFrom.SelectedDate.Year,calFilterFrom.SelectedDateEng.Month, calFilterFrom.SelectedDateEng.Day,0,0,0);
            try
            {
                DateTime time = DateTime.Parse(txtTime.Text.Trim());
                start = new DateTime(start.Year, start.Month, start.Day, time.Hour, time.Minute, time.Second);
            }
            catch
            {
            }

            int totalRecords = 0;



            
                gvwProjects.DataSource =
                    CommonManager.GetMonetoryChangeLogsForSignOff(
                    start, null,
                    txtSearch.Text.Trim(), pagintCtl.CurrentPage, int.Parse(pagintCtl.DDLRecords.SelectedValue), ref totalRecords,int.Parse(ddlType.SelectedValue));
                gvwProjects.DataBind();
          


            pagintCtl.UpdatePagingBar(totalRecords);
          
        }

        protected void gvwRoles_PageIndexChanged(object sender, EventArgs e)
        {

        }

        protected void btnLoad_Click(object sender, EventArgs e)
        {
            pagintCtl.CurrentPage = 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage -= 1;
            BindUsers();
        }


        protected void btnPrevious_Click()
        {
            pagintCtl.CurrentPage -= 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage -= 1;
            BindUsers();
        }

        protected void btnNext_Click()
        {
            pagintCtl.CurrentPage += 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage += 1;
            BindUsers();
        }


        protected void ddlRecords_SelectedIndexChanged()
        {
            //SessionManager.EmployeeDisplayRecodsPerPage = int.Parse(pagintCtl.DDLRecords.SelectedValue);
            //_tempCurrentPage = 1;
            pagintCtl.CurrentPage = 1;
            BindUsers();
        }

        public bool CanChange(object value)
        {
            return Convert.ToBoolean(value);
        }


        protected void gvwRoles_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            //gvwRoles.PageIndex = e.NewPageIndex;
            //gvwRoles.SelectedIndex = -1;
            BindUsers();
          
        }
        
        protected void ddlUserType_SelectedIndex(object sender, EventArgs e)
        {
          
        }

        protected void btnInsertUpdate_Click(object sender, EventArgs e)
        {
         
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {


            DateTime start = new DateTime(calFilterFrom.SelectedDate.Year, calFilterFrom.SelectedDateEng.Month, calFilterFrom.SelectedDateEng.Day, 0, 0, 0);



            int totalRecords = 0;


            List<GetMonetoryChangeLogsForSignOffResult> list = CommonManager.GetMonetoryChangeLogsForSignOff(start, null,
                txtSearch.Text.Trim(), 1, null, ref totalRecords,int.Parse(ddlType.SelectedValue));
            GridViewExportUtil.ExportToExcel<GetMonetoryChangeLogsForSignOffResult>("Payroll Change Logs.xls", list,
                new List<string> { "TotalRows", "RowNumber", "Action", "Type","SubType" }, new List<string> { "EIN" }
                , new Dictionary<string, string> {{"EmployeeName","Employee Name" },{"ActionValue","Action"},{"TypeValue","Type"}}, new List<string> { }, new Dictionary<string, string> { },
                new List<string> { "EIN","EmployeeName","ActionValue","TypeValue","Before","After","Name","Date"});






        }


        public void btnSignOff_Click(object sender, EventArgs e)
        {
            //PayrollPeriodSignOff entity = new PayrollPeriodSignOff();
            //entity.SignOffDate = DateTime.Now;
            //entity.SignOffUserID = SessionManager.User.UserID;

            //PayManager.SavePayrollSignOff(entity);

            //msgCtl.InnerHtml = "Payroll has been successfully signoff/approved.";
            //msgCtl.Hide = false;
        }
       


        public string GetAction(object value)
        {
            LogActionEnum log = (LogActionEnum)int.Parse(value.ToString());
            return log.ToString();
        }


        public string GetType(object valueType,object valueSubType)
        {

            MonetoryChangeLogTypeEnum log = (MonetoryChangeLogTypeEnum)int.Parse(valueType.ToString());
            if (valueSubType.ToString() == "")
                return log.ToString();
            return log.ToString() + " : " + valueSubType.ToString();
        }

        protected void gvwRoles_SelectedIndexChanged(object sender, EventArgs e)
        {
          
            
        }

        protected void gvwRoles_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
          
        }

     

        protected void Page_PreRender(object sender, EventArgs e)
        {
          
        }

     
    }
}
