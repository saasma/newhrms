﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using BLL;
using DAL;
using Ext.Net;
using Utils.Web;
using Utils.Helper;
using System.Text;
using Newtonsoft.Json.Linq;
using BLL.BO;

namespace Web.CP
{
    public partial class EmailContentDisplay : BasePage
    {

        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.AdministratorTools;
            }
        }

        CommonManager commonMgr = new CommonManager();

        public bool isNsetDashainAmountImport = true;



        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                Initialise();





        }


        public void cboBranch_Select(object sender, DirectEventArgs e)
        {
            if (cmbContentType.SelectedItem != null)
            {
                int type = int.Parse(cmbContentType.SelectedItem.Value);
                EmailContent content = CommonManager.GetMailContent(type);
                if (content != null)
                {
                    txtSubject.Text = content.Subject;
                    HtmlEditor1.Text = content.Body;
                    dispPlaceHolders.Text = GetPlaceHolders((EmailContentType)type);
                }
                else
                {
                    txtSubject.Text = "";
                    HtmlEditor1.Text = "";
                    dispPlaceHolders.Text = "";
                }
            }
        }

        public string GetPlaceHolders(EmailContentType type)
        {
            switch (type)
            {
                case EmailContentType.DayTypeLeaveRequest:

                    return "#RequesterName#  #StartDate#  #EndDate#  #Days#  #Reason#  #LeaveType#  #Substitute#  #RecommendedBy#  #Url#  {Organizations Name}";

                    //break;
                case EmailContentType.TrainingEmail:

                    return "#TrainingName#  #Period#  #Time#  #Days#  #Venue#  #ResourccePerson#  #Notes#";

                    //break;
            }

            return "";
        }


        public void btnSave_Click(object sender, DirectEventArgs e)
        {
            if (cmbContentType.SelectedItem != null)
            {
                int type = int.Parse(cmbContentType.SelectedItem.Value);
                EmailContent content = new EmailContent();
                content.ContentType = type;
                content.CompayId = SessionManager.CurrentCompanyId;
                content.Subject = txtSubject.Text.Trim();
                content.Body = HtmlEditor1.Text.Trim();

                CommonManager.SaveOrUpdateEmailContent(content);
                X.Msg.Show(new MessageBoxConfig { Icon = MessageBox.Icon.INFO, Title = "Message", Message = "Data saved successfully.", Buttons = MessageBox.Button.OK });

            }
        }

        public void Initialise()
        {

            // Day type Leave request & approval
            if (CommonManager.CalculationConstant.CompanyHasHourlyLeave == false)
            {
                cmbContentType.Items.Add(new Ext.Net.ListItem { Text = "Leave Request Content", 
                    Value =  ((int)EmailContentType.DayTypeLeaveRequest).ToString() });

                //cmbContentType.Items.Add(new Ext.Net.ListItem
                //{
                //    Text = "Leave Request Forward Content",
                //    Value = ((int)EmailContentType.DayTypeLeaveRequestForward).ToString()
                //});

                cmbContentType.Items.Add(new Ext.Net.ListItem
                {
                    Text = "Leave Approval Content",
                    Value = ((int)EmailContentType.DayTypeLeaveApproval).ToString()
                });

                cmbContentType.Items.Add(new Ext.Net.ListItem
                {
                    Text = "Cancel Leave Request Content",
                    Value = ((int)EmailContentType.CancelLeaveRequest).ToString()
                });
            }

        }


    }

}

