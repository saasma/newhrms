﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;
using System.Text;
using Utils.Calendar;

namespace Web.CP
{
    public partial class MonthlyProjectAssociation : BasePage
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        private void Initialise()
        {


            List<Project> projectList = ProjectManager.GetAllProjectList();
            storeProjects.DataSource = projectList;
            storeProjects.DataBind();

            CustomDate date1 = CustomDate.GetTodayDate(true);
            txtFromDate.SelectedValue = date1.GetFirstDateOfThisMonth().EnglishDate;
            txtToDate.SelectedValue = date1.GetLastDateOfThisMonth().EnglishDate;

            storeEmployeeList.DataSource = EmployeeManager.GetAllEmployeesForEmployeeImportExport().OrderBy(x => x.Name).ToList();
            storeEmployeeList.DataBind();

            cmbFilterEmployee.GetStore().DataSource = EmployeeManager.GetAllEmployeesForEmployeeImportExport().OrderBy(x => x.Name).ToList();
            cmbFilterEmployee.GetStore().DataBind();

            hiddenValue.Text = "";
        }

        protected void btnAddNew_Click(object sender, DirectEventArgs e)
        {
            ClearControls();
            WPublication.Show();
        }

        protected void ClearControls()
        {
            cmbProjects.Clear();
            cmbEmployee.Clear();
            txtStartDate.Clear();
            txtEndDate.Clear();
            cmbEmployee.Disabled = false;
            cmbProjects.Disabled = false;
            hiddenValue.Text = "";

        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveUpdProjectAssociation");
            if (Page.IsValid)
            {
                INGOProjectEmployee obj = new INGOProjectEmployee();

                obj.EmployeeId = int.Parse(cmbEmployee.SelectedItem.Value);
                obj.ProjectId = int.Parse(cmbProjects.SelectedItem.Value);
                 if (txtStartDate.SelectedValue!= null)
                 obj.StartDate  = DateTime.Parse(txtStartDate.Text.Trim());
                 if (txtEndDate.SelectedValue != null)
                     obj.EndDate = DateTime.Parse(txtEndDate.Text.Trim());
               

                if (!string.IsNullOrEmpty(hiddenValue.Text))
                    obj.ID = int.Parse(hiddenValue.Text);

                Status status = ProjectManager.SaveUpdateProjectAssocation(obj);
                if (status.IsSuccess)
                {
                    if (!string.IsNullOrEmpty(hiddenValue.Text))
                        NewMessage.ShowNormalMessage("Record updated successfully.");
                    else
                        NewMessage.ShowNormalMessage("Record saved successfully.");
                    WPublication.Close();
                    storeGrid.Reload();

                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }

            }
        }

        protected void btnSearch_Click(object sender, DirectEventArgs e)
        {

            if (string.IsNullOrEmpty(txtFromDate.Text))
            {
                NewMessage.ShowNormalMessage("From is required.");
                return;
            }

            if (string.IsNullOrEmpty(txtToDate.Text))
            {
                NewMessage.ShowNormalMessage("To is required.");
                return;
            }

            storeGrid.Reload();
        }

        protected void btnEdit_Click(object sender, DirectEventArgs e)
        {
            
            int id = int.Parse(hiddenValue.Text);
            cmbEmployee.Disabled = true;
            cmbProjects.Disabled = true;

            INGOProjectEmployee obj = ProjectManager.GetMonthlyProjectAssociationByID(id);
            if (obj != null)
            {
                cmbEmployee.SetValue(obj.EmployeeId.ToString());
                cmbProjects.SetValue(obj.ProjectId.ToString());
                txtStartDate.Text = obj.StartDate.ToString();

                if(obj.EndDate!=null)
                txtEndDate.Text = obj.EndDate.ToString();

             

                WPublication.Show();
            }
        }

        protected void btnDelete_Click(object sender, DirectEventArgs e)
        {
            int id = int.Parse(hiddenValue.Text);

            Status status =ProjectManager.DeleteProjectAssociation(id);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Record deleted successfully.");
                storeGrid.Reload();
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }


        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            string sortBy = "";
            if (e.Sort.Any())
                sortBy = e.Sort[0].Property + " " + e.Sort[0].Direction;

            int? ProjectID = null;
            int? EmployeeID = null;
            DateTime? startDate = null;
            DateTime? EndDate = null;

            if (!string.IsNullOrEmpty(txtFromDate.Text.Trim()))
                startDate = txtFromDate.SelectedDate;

            if (!string.IsNullOrEmpty(txtToDate.Text.Trim()))
                EndDate = txtToDate.SelectedDate;

            if (!string.IsNullOrEmpty(cmbFilterEmployee.SelectedItem.Value))
                EmployeeID = int.Parse(cmbFilterEmployee.SelectedItem.Value);

            if (!string.IsNullOrEmpty(cmbFilterProject.SelectedItem.Value))
                ProjectID = int.Parse(cmbFilterProject.SelectedItem.Value);

            List<GetMonthlyProjectAssociationResult> ProjectAssociationList = ProjectManager
                .GetMonthlyProjectAssociation(e.Start, e.Limit, sortBy, EmployeeID, ProjectID, startDate, EndDate).ToList();
           
            if (ProjectAssociationList.Any())
            {
                storeGrid.DataSource = ProjectAssociationList;

                e.Total = ProjectAssociationList[0].TotalRows == null ? 0 : ProjectAssociationList[0].TotalRows.Value;
            }

            storeGrid.DataBind();

        }

    }
}