﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using Utils.Base;
using BLL;
using DAL;
using Utils.Web;
using Utils.Helper;
using System.Text.RegularExpressions;
using Ext.Net;
using Utils.Calendar;

namespace Web.CP
{
    public partial class HREvalulation : BasePage
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
            JavascriptHelper.AttachNonDialogPopUpCode(Page, "PopUpPosition", "../HRRating.aspx", 625, 600);
            JavascriptHelper.AttachNonDialogPopUpCode(Page, "PopUpPosition1", "../HREvaluationPeriod.aspx", 625, 600);
            if (!X.IsAjaxRequest)
            {
                Initialize();
            }
            Register();

            LinkButton_Rating.Visible = !SessionManager.IsReadOnlyUser;
            LinkButton_Period.Visible = !SessionManager.IsReadOnlyUser;
            if (SessionManager.IsReadOnlyUser)
                gvwEvalulations.Listeners.ClearListeners();
        }

        private void Initialize()
        {
            storeEvaluationPeriod.DataSource = PositionGradeStepAmountManager.GetEvaluationPeriodList();
            storeEvaluationPeriod.DataBind();

            storeRatings.DataSource = PositionGradeStepAmountManager.GetAllRatings(SessionManager.CurrentCompanyId);
            storeRatings.DataBind();

            List<Branch> listBranch = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            cmbBranch.Store[0].DataSource = listBranch;
            cmbBranch.Store[0].DataBind();

            //gvwEvalulations.Store[0].BaseParams["companyId"] = SessionManager.CurrentCompanyId.ToString();

            CommonManager mgr = new CommonManager();
            cmbDesignation.Store[0].DataSource = mgr.GetAllDesignations();
            cmbDesignation.Store[0].DataBind();
        }

        public void cboBranch_Select(object sender, DirectEventArgs e)
        {
            cmbDepartment.ClearValue();

            int branchId = (cmbBranch.SelectedItem == null || cmbBranch.SelectedItem.Value==null) ? -1 : int.Parse(cmbBranch.SelectedItem.Value);
            List<GetDepartmentListResult> listDepartment = DepartmentManager.GetAllDepartmentsByBranch(branchId);
            cmbDepartment.Store[0].DataSource = listDepartment;
            cmbDepartment.Store[0].DataBind();
        }

        public void cmbDepartment_Select(object sender, DirectEventArgs e)
        {
            cmbSubDepartment.ClearValue();

            int depId = (cmbDepartment.SelectedItem == null || cmbDepartment.SelectedItem.Value == null) ? -1 : int.Parse(cmbDepartment.SelectedItem.Value);
            List<SubDepartment> listDepartment = DepartmentManager.GetAllSubDepartmentsByDepartment(depId);
            cmbSubDepartment.Store[0].DataSource = listDepartment;
            cmbSubDepartment.Store[0].DataBind();
        }

        public GetEvaluationInfoResult GetEvalulation(int evalulationPeriodId, int employeeId)
        {
              List<GetEvaluationInfoResult> list =   PositionGradeStepAmountManager.GetEvaluationPeriod(0, 10,
                 SessionManager.CurrentCompanyId, evalulationPeriodId, -1, -1, -1, -1, "", employeeId);

              if (list.Count > 0)
              {
                  return list[0];
              }

              return null;
        }

        public void btnEdit_Click(object sender, DirectEventArgs e)
        {
            string[] values = Hidden_Selection.Text.Split(new char[] { ':' });
            int employeeId = int.Parse(values[0]);
            int evalulationPeriodId = int.Parse(values[1]);
            //int evalulationId = int.Parse(values[2]);


            GetEvaluationInfoResult info = GetEvalulation(evalulationPeriodId, employeeId);
            if (info != null)
            {
                lblEmployee.Text = info.Name;
                lblEvaluationPeriod.Text = info.PeriodDateName;
                if (info.RatingId != null)
                    cmbRating.SetValue(info.RatingId.Value);
                else
                    cmbRating.ClearValue();
                if (!string.IsNullOrEmpty(info.EvalulationDate))
                    calDate.Text = info.EvalulationDate;
                else
                    calDate.Text = "";
                if (info.Comment != null)
                    txtComment.Text = info.Comment;
                else
                    txtComment.Text = "";

                window.Show();

            }

        }

        public void btnHistory_Click(object sender, DirectEventArgs e)
        {
            string[] values = hdnHistory.Text.Split(new char[] { ':' });
            int employeeId = int.Parse(values[0]);
            int evaluationPeriodId = int.Parse(values[1]);

            gridHistory.Store[0].DataSource = PositionGradeStepAmountManager.GetEvaluationHistory
                (employeeId, evaluationPeriodId);
            gridHistory.Store[0].DataBind();

            winHistory.Show();

        }

        public void btnSave_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("AEEvaluation");

            if (Page.IsValid)
            {
                string[] values = Hidden_Selection.Text.Split(new char[] { ':' });
                int employeeId = int.Parse(values[0]);
                int evalulationPeriodId = int.Parse(values[1]);

                GetEvaluationInfoResult info = GetEvalulation(evalulationPeriodId, employeeId);
                info.EmployeeId = employeeId;
                info.EvaluationPeriodId = evalulationPeriodId;
                info.RatingId = int.Parse(cmbRating.SelectedItem.Value);
                info.EvalulationDate = calDate.Text.Trim();
                info.Comment = txtComment.Text.Trim();

                PositionGradeStepAmountManager.SaveUpdateEvalulation(info);
                window.Hide();
                X.Js.AddScript("searchList();");
            }
        }

        public void Register()
        {
            ResourceManager1.RegisterClientInitScript("Date1", string.Format("isEnglish = {0};", true ? "true" : "false"));



            ResourceManager1.RegisterClientInitScript("Date2", string.Format("todayDate = '{0}';function calendarOnClick(event, sourceElement, e3){{}};",
                                                                      CustomDate.GetTodayDate(true).ToString()));

          
            
           

        }

        

    }
}
