﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using BLL;
using DAL;
using Ext.Net;
using Utils.Web;
using Utils.Helper;
using System.Text;
using Newtonsoft.Json.Linq;
using BLL.BO;
using Utils.Calendar;

namespace Web.CP
{
    public partial class ProjectRateInput : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.ProjectAndTimesheet;
            }
        }     

        CommonManager commonMgr = new CommonManager();

        public bool isNsetDashainAmountImport = true;



        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack && !X.IsAjaxRequest)
                Initialise();

            if (Request.Form["__EVENTTARGET"] != null && Request.Form["__EVENTTARGET"].ToString().ToLower().Equals("reload"))
            {
               

                BindList();
            }


            //if (Request.Form["__EVENTTARGET"] != null && Request.Form["__EVENTTARGET"].Contains(ddlPayrollPeriods.ClientID))
            //{
            //    BindOvertimes();
            //}


            JavascriptHelper.AttachNonDialogPopUpCode(Page, "shiftPopup", "../../ExcelWindow/ProjectEmployeeRateExcel.aspx", 450, 500);


            string code = string.Format("var totalHoursOrDayInAMonth = {0}; ", 
                
                CommonManager.CompanySetting.IsProjectInputType ? ProjectManager.TotalHourOrDaysInAMonth : 100);

            Page.ClientScript.RegisterStartupScript(this.GetType(),"sdffd",code,true); 

          
            // programatic client-side calls to __doPostBack() can bypass this
            Page.ClientScript.RegisterOnSubmitStatement(GetType(), "ServerForm",
                                                    "if (this.submitted) return false; this.submitted = true; return true;");

            Page.ClientScript.RegisterClientScriptBlock(GetType(), "sffdsf", string.Format("var IsProjectInputType = {0};", CommonManager.CompanySetting.IsProjectInputType ? "true": "false"),true);
               
        }






        public void Initialise()
        {
            CustomDate date = CustomDate.GetTodayDate(true).GetFirstDateOfThisMonth();
            CustomDate date2 = CustomDate.GetTodayDate(true).GetLastDateOfThisMonth();
            txtFromDate.Text = date.ToString();
            txtToDate.Text = date2.ToString();
          
            //txtHours.Text = ProjectManager.TotalHourOrDaysInAMonth.ToString();

            //BindList();
            
            //For Project Percent case
            if (!CommonManager.CompanySetting.IsProjectInputType)
            {
              
                //txtHours.Style["display"] = "none";
               // btnSave.Style["display"] = "none";
                
            }

            //BindOvertimes();

         
                btnExport.Enabled = true;


        }

        Column GetExtGridPanelColumn(string indexId,string headerName)
        {
            Column column = new Column();
            //column.ColumnID = indexId;
            column.DataIndex = indexId;
            column.Text = headerName;
            column.Align = Alignment.Center;
            column.ToolTip = headerName;
            column.MenuDisabled = true;
            column.Sortable = false;
            column.Width = new Unit(120);
           

            if (headerName == "Total")
                column.Renderer.Fn = "totalRenderer";
            else
            {

                NumberField field = new NumberField();
                field.MinValue = 0;
                column.Editor.Add(field);

                column.Renderer.Fn = "columnRenderer";
            }
            return column;
        }

        public void BindList()
        {

            int? employeeID =null;
            if (!string.IsNullOrEmpty(hdnFilterEmployee.Text))
                employeeID = int.Parse(hdnFilterEmployee.Text);

            List<EEmployee> employees = EmployeeManager.GetAllEmployeesForEmployeeImportExport().Where(x => x.EmployeeId == employeeID || employeeID==null).OrderBy(x => x.Name).ToList();
            List<Project> projects = ProjectManager.GetAllProjectList()
                .Where(x => 
                    (x.StartDateEng >= txtFromDate.SelectedDate && x.StartDateEng <= txtToDate.SelectedDate)
                    ||
                    (x.EndDateEng >= txtFromDate.SelectedDate && x.EndDateEng <= txtToDate.SelectedDate)
                    ||
                    (x.StartDateEng <= txtFromDate.SelectedDate && x.EndDateEng >= txtToDate.SelectedDate)
                    
                    ).ToList();
            if (!projects.Any())
                return;

            //List<EEmployee> employees = ProjectManager.GetProjectPaysEmployeeList(payrollPeroidId
            //     , int.Parse(ddlIncomes.SelectedValue));

            //List<Project> projects = ProjectManager.GetProjectForPays(payrollPeroidId);
           

           // List<ProjectPayBO> projectPays = ProjectManager.GetProjectRateInputs();


            // Create Columns for the Projects
            //ArrayReader reader = (storeProjects.Reader[0] as ArrayReader);
            //ColumnModel columnModelProjects = gridProjects.ColumnModel;
            string columnId = "P_{0}";
            foreach (var project in projects)
            {

                string gridIndexId = string.Format(columnId, project.ProjectId);

                ModelField field = new ModelField(gridIndexId, ModelFieldType.Float);
                field.UseNull = true;
                //field. = true;
                ModelProject.Fields.Add(gridIndexId, ModelFieldType.Float);


                // Add Checkbox

                gridProjects.ColumnModel.Columns.Add(GetExtGridPanelColumn(gridIndexId, project.Name));

            }

            ModelProject.Fields.Add("Total", ModelFieldType.Float);
            gridProjects.ColumnModel.Columns.Add(GetExtGridPanelColumn("Total", "Total"));


            // Generate data array & bind to Store
            // Add each  id as key
            Dictionary<string, double?> selection = new Dictionary<string, double?>();

            //foreach (var item in projectPays)
            //{
            //    selection.Add(item.EmployeeId + "_" + item.ProjectId, item.HoursOrDays);
            //}

            object[] data = new object[employees.Count];

            // Iterate for each Project & Skip first as it represents All Project
            for (int i = 0; i < employees.Count; i++)
            {
                //first one for EIN, second for Name & last for Total
                object[] rowData = new object[projects.Count + 3];
                double total = 0;
                int columnIndex = 0;

                rowData[columnIndex++] = employees[i].EmployeeId;
                rowData[columnIndex++] = employees[i].Name;



                 //Iterate for each Project
                for (int j = 0; j < projects.Count; j++)
                {
                    //if (selection.ContainsKey(employees[i].EmployeeId + "_" + projects[j].ProjectId))
                    //{
                        double Rate =  ProjectManager.GetProjectRateByEmployeeID(employees[i].EmployeeId, projects[j].ProjectId);

                        //rowData[columnIndex++] = selection[employees[i].EmployeeId + "_" + projects[j].ProjectId];
                        rowData[columnIndex++] = Rate;
                        total += Convert.ToDouble(rowData[columnIndex - 1]);
                    //}
                    //else
                    //{
                    //    rowData[columnIndex++] = null;
                    //}
                }


                rowData[rowData.Length - 1] = (total);
                data[i] = rowData;
            }

            //int gridwidth = gridProjects.ColumnModel.Columns.Count * 120;
            //gridProjects.Width = gridwidth;

            storeProjects.DataSource = data;
            storeProjects.DataBind();
            
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {

            if (string.IsNullOrEmpty(txtFromDate.Text))
            {
                NewMessage.ShowNormalMessage("From is required.");
                return;
            }

            if (string.IsNullOrEmpty(txtToDate.Text))
            {
                NewMessage.ShowNormalMessage("To is required.");
                return;
            }

            //storeGrid.Reload();

            BindList();

        }



        public bool IsEnable(object ein)
        {
            if (IsPayrollSaved)
                return false;

            return !IsAttendanceSavedForEmployee(payrollPeriodId, Convert.ToInt32(ein));
        }


        public bool IsPayrollSaved = false;
        public int payrollPeriodId;
     
      


       
      
       

        protected void btnSave_DirectClick(object sender, DirectEventArgs e)
        {
            //JSON representation
            string gridJSON = e.ExtraParams["gridItems"];

            //lbl.Text = gridJSON;


            //List<Project> projects = ProjectManager.GetProjectsForLatestPayroll();

            DateTime startdate = Convert.ToDateTime(txtFromDate.Text.Trim());
            DateTime enddate = Convert.ToDateTime(txtToDate.Text.Trim());

            List<Project> projects = ProjectManager.GetAllProjectList()
                       .Where(x =>
                       (x.StartDateEng >= Convert.ToDateTime(startdate) && x.StartDateEng <= Convert.ToDateTime(enddate))
                       ||
                       (x.EndDateEng >= Convert.ToDateTime(startdate) && x.EndDateEng <= Convert.ToDateTime(enddate))
                       ||
                       (x.StartDateEng <= Convert.ToDateTime(startdate) && x.EndDateEng >= Convert.ToDateTime(enddate))
                       )
                       .ToList();

            //List<PIncome> incomesList = ProjectManager.GetIncomesForProjects();

            JArray jsonArray = Ext.Net.JSON.Deserialize(gridJSON) as JArray;

            double value=0;

            List<ProjectEmployeeContribution> projectPays = new List<ProjectEmployeeContribution>();

            // Skip first row as it represent All Projects
            for (int i = 0; i < jsonArray.Count; i++)
            {
                //Project project = projectList[i];
                JToken row = jsonArray[i];

                int employeeId = int.Parse(row["EmployeeId"].Value<string>());

                for (int j = 0; j < projects.Count; j++)
                {
                    string key = string.Format("P_{0}", projects[j].ProjectId);

                    if (row[key] != null)
                    {

                        if( !double.TryParse ( row[key].ToString(),out value))
                            value= 0;

                        ProjectEmployeeContribution pay = new ProjectEmployeeContribution();
                        pay.EmployeeId = employeeId;
                       
                        pay.ProjectId = projects[j].ProjectId;
                        pay.Rate=value;
                        projectPays.Add(pay);
                    }
                }
            }


            ProjectManager.SaveProjectRateContribution(projectPays);

            if (projectPays.Count > 0)
            {

                X.MessageBox.Show(
                        new MessageBoxConfig
                        {
                            Message = "Project value saved.",
                            Buttons = MessageBox.Button.OK,
                            Title = "Information",
                            Icon = MessageBox.Icon.INFO,
                            MinWidth = 300
                        });

            }
        }
    }

}

