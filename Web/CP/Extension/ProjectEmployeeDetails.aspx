﻿<%@ Page Title="Project Employee Details" Language="C#" MasterPageFile="~/Master/ForPopupPage.Master" AutoEventWireup="true" CodeBehind="ProjectEmployeeDetails.aspx.cs" Inherits="Web.CP.Extension.ProjectEmployeeDetails" %>


<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningMsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="InfoMsgCtl" TagPrefix="uc2" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

<script type="text/javascript">

    function closePopup() {
        window.opener.reloadProjectEmployee(window);
    }

</script>

<style type="text/css">
    .clsColor
    {
        background-color: #e8f1ff;
    }
</style>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">

 <div style="clear: both">
        </div>

        <asp:HiddenField ID="hdnEmpId" runat="server" />
    <div class="clsColor">


        <div class="popupHeader">
            <h3 id="branchTitle" runat="server">
                                Add Projects to
                            </h3>
        </div>   

        <uc2:InfoMsgCtl ID="divMsgCtl" Width="900px" EnableViewState="false" Hide="true" runat="server" />
        <uc2:WarningMsgCtl ID="divWarningMsg" Width="900px" EnableViewState="false" Hide="true"
            runat="server" />

        

            <div class=" marginal" style="margin: 5px 20px 0 10px;">
                <table class="fieldTable" cellpadding="0" style='padding-left: 10px' cellspacing="4">               
                    <tr>
                        <td class="fieldHeader">
                            <strong>Available Projects</strong>
                            <asp:ListBox ID="lstSource" runat="server" SelectionMode="Multiple" Width="300" Height="300"
                                DataValueField="ProjectId" DataTextField="Name"></asp:ListBox>
                            <asp:RequiredFieldValidator ID="test" runat="server" ValidationGroup="add" ControlToValidate="lstSource"
                                Display="None" ErrorMessage="Project should be selected for addition." />
                        </td>
                        <td style="text-align: center">
                            <asp:Button Text=">" runat="server" Width="50" ToolTip="Add" ID="btnAdd" OnClientClick="valGroup='add';return CheckValidation()"
                                OnClick="btnAdd_Click" />
                            <br />
                            <br />
                            <asp:Button Text="<" runat="server" OnClientClick="valGroup='remove';return CheckValidation()"
                                Width="50" ToolTip="Remove" ID="btnRemove" OnClick="btnRemove_Click" />
                            <br />
                            <br />
                            <asp:Button Text=">>" runat="server" Width="50" CausesValidation="false" ToolTip="Add All"
                                ID="btnAddAll" OnClick="btnAddAll_Click" />
                            <br />
                            <br />
                            <asp:Button Text="<<" runat="server" Width="50" CausesValidation="false" ToolTip="Remove All"
                                ID="btnRemoveAll" OnClick="btnRemoveAll_Click" />
                            <br />
                            <br />
                            <asp:CheckBox ID="chkToAll" runat="server" Text="To All Employee" />
                        </td>
                        <td>
                            <strong>Selected Projects</strong>
                            <br />
                            <asp:ListBox ID="lstDest" SelectionMode="Multiple" runat="server" Width="300" Height="300"
                                DataValueField="ProjectId" DataTextField="Name"></asp:ListBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="remove"
                                ControlToValidate="lstDest" Display="None" ErrorMessage="Project should be selected for removal." />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="btnSave" CssClass="update" runat="server" Text="Save" ValidationGroup="AEBranch"
                                OnClick="btnSave_Click" />
                            <asp:Button ID="btnCancel" OnClientClick="window.close();" CssClass="cancel" runat="server"
                                Text="Cancel" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
