﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using BLL;
using DAL;
using Ext.Net;
using Utils.Web;
using Utils.Helper;
using System.Text;
using Newtonsoft.Json.Linq;
using BLL.BO;
using System.Web.Configuration;
using System.Net.Configuration;
using System.Diagnostics;

namespace Web.CP
{
    public partial class DeviceAttenanceSync : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.AdministratorTools;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                Initialise();
        }

        protected void btnUpload_Click(object sender, DirectEventArgs e)
        {
            try
            {
                ProcessStartInfo startinfo = new ProcessStartInfo();
                var currentDirectory = System.IO.Directory.GetCurrentDirectory();
                startinfo.FileName = @"RigoAttendanceService.exe";
                startinfo.Arguments = "1";
                startinfo.CreateNoWindow = false;
                startinfo.UseShellExecute = false;
                Process myProcess = Process.Start(startinfo);
                myProcess.WaitForExit(120000);
                //process ended
                NewMessage.ShowNormalMessage("Process end");
            }

            catch (Exception ex)
            {
                NewMessage.ShowWarningMessage(ex.Message.ToString());
            }
        }

        public void Initialise()
        {

            //Load Content Type

       

        }

       
    }

}

