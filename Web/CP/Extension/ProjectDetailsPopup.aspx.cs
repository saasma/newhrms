﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using DAL;
using BLL;
using Utils.Helper;

namespace Web.CP.Extension
{
    public partial class ProjectDetailsPopup : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialise();
            }
        }

        private void Initialise()
        {
            
            
            calFrom.IsEnglishCalendar = IsEnglish;
            calTo.IsEnglishCalendar = IsEnglish;

            Clear();

            if (Request.QueryString["Id"] != null)
            {
                hdnProjectId.Value = Request.QueryString["Id"];
                LoadProjectDataById(int.Parse(hdnProjectId.Value));
            }
        }

        private void Clear()
        {
            txtName.Text = string.Empty;
            txtCode.Text = string.Empty;
            txtDonor.Text = string.Empty;
            txtRemarks.Text = string.Empty;
            txtDepID.Text = "";

            var currentYear = SessionManager.CurrentCompanyFinancialDate;

            calFrom.SetSelectedDate(currentYear.StartingDate, IsEnglish);
            calTo.SetSelectedDate(currentYear.EndingDate, IsEnglish);

            btnInsertUpdate.Text = Resources.Messages.Save;
        }

        public void CheckNoEndDate_Change(Object sender, EventArgs e)
        {

            if (chkNoEndDate.Checked)
            {
                calTo.Enabled = false;
                valCustomeToDate2.Visible = false;
            }
            else
            {
                calTo.Enabled = true;
                valCustomeToDate2.Visible = true;
            }

        }

        private void LoadProjectDataById(int ProjectId)
        {
            var project = ProjectManager.GetProjectById(ProjectId);

            if (project != null)
            {
                txtName.Text = project.Name;
                if (project.Code != null)
                {
                    txtCode.Text = project.Code;
                }
                else
                {
                    txtCode.Text = string.Empty;
                }

                if (project.Donor != null)
                    txtDonor.Text = project.Donor;                

                if (project.FundCode != null)
                    txtFundCode.Text = project.FundCode;

                if (project.PID != null)
                    txtProjectID.Text = project.PID;

                if (project.Remarks != null)
                    txtRemarks.Text = project.Remarks;

                txtDepID.Text = project.DepartmentID;

                btnInsertUpdate.Text = Resources.Messages.Update;

                calFrom.SetSelectedDate(project.StartDate, IsEnglish);
                calTo.SetSelectedDate(project.EndDate, IsEnglish);

                if (!string.IsNullOrEmpty(project.ProjectAnalysisCode))
                txtProjectAnalysisCode.Text=project.ProjectAnalysisCode;

                if (string.IsNullOrEmpty(project.EndDate))
                {
                    chkNoEndDate.Checked = true;
                   // calTo.Enabled = false;
                    CheckNoEndDate_Change(null, null);
                }


            }
        }

        protected void btnInsertUpdate_Click(object sender, EventArgs e)
        {
            if (IsValid)
            {
                var project = new Project();

                if (!string.IsNullOrEmpty(hdnProjectId.Value))
                {
                    project.ProjectId = int.Parse(hdnProjectId.Value);
                }

                project.Name = txtName.Text.Trim();
                project.Code = txtCode.Text.Trim();
                project.CompanyId = SessionManager.CurrentCompanyId;
                project.StartDate = calFrom.SelectedDate.ToString();
                if (chkNoEndDate.Checked == false)
                    project.EndDate = calTo.SelectedDate.ToString();
                else
                {
                    project.EndDate = "";
                }
                project.DepartmentID = txtDepID.Text.Trim();
                project.ProjectAnalysisCode = txtProjectAnalysisCode.Text.Trim();

                if (!string.IsNullOrEmpty(txtDonor.Text.Trim()))
                    project.Donor = txtDonor.Text.Trim();                

                if (!string.IsNullOrEmpty(txtFundCode.Text.Trim()))
                    project.FundCode = txtFundCode.Text.Trim();

                if (!string.IsNullOrEmpty(txtProjectID.Text.Trim()))
                    project.PID = txtProjectID.Text.Trim();

                if (!string.IsNullOrEmpty(txtRemarks.Text.Trim()))
                    project.Remarks = txtRemarks.Text.Trim();

                if (ProjectManager.SaveUpdateProject(project))
                {
                    if (string.IsNullOrEmpty(hdnProjectId.Value))
                    {
                        JavascriptHelper.DisplayClientMsg("Information saved.", Page, "closePopup();");
                    }
                    else
                    {
                        JavascriptHelper.DisplayClientMsg("Information updated.", Page, "closePopup();");
                    }                                    
                }
                else
                {
                    msgWarning.InnerHtml = "Project name already exists.";
                    msgWarning.Hide = false;
                }
            }
        }

    }
}