<%@ Page Title="Monetory Change Logs" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="MonetoryChangeLog.aspx.cs" Inherits="Web.User.MonetoryChangeLog" %>

<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningMsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="InfoMsgCtl" TagPrefix="uc2" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        skipLoadingCheck = true;
       
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Change Logs
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="contentArea">
            <asp:Panel runat="server" DefaultButton="btnLoad" class="attribute" Style="padding: 10px">
                Type &nbsp;
                <asp:DropDownList AutoPostBack="true" OnSelectedIndexChanged="ddlType_Changed" ID="ddlType"
                    runat="server">
                    <asp:ListItem Value="1" Text="Employee" />
                    <asp:ListItem Value="2" Text="Setting" Selected="True" />
                    <asp:ListItem Value="3" Text="User Activity" />
                </asp:DropDownList>
                &nbsp; From &nbsp;
                <My:Calendar Id="calFilterFrom" runat="server" />
                To &nbsp;
                <My:Calendar Id="calFilterTo" runat="server" />
                &nbsp; Search: &nbsp;
                <asp:TextBox ID="txtSearch" runat="server" />
                &nbsp;
                <asp:LinkButton ID="btnLoad" runat="server" OnClick="btnLoad_Click" Text="Show" CssClass="btn btn-default btn-sm btn-sect"
                    Style="width: 100px" />
            </asp:Panel>
            <div style="clear: both">
                <%--Employee/Monetory change log--%>
                <cc2:EmptyDisplayGridView Width="100%" Style='clear: both' PagerStyle-HorizontalAlign="Center"
                    PagerStyle-CssClass="defaultPagingBar" CssClass="table table-primary mb30 table-bordered table-hover"
                    UseAccessibleHeader="true" ShowHeaderWhenEmpty="True" ID="gvwProjects" runat="server"
                    AutoGenerateColumns="False" CellPadding="4" GridLines="None" ShowFooterWhenEmpty="False"
                    OnPageIndexChanged="gvwRoles_PageIndexChanged" OnPageIndexChanging="gvwRoles_PageIndexChanging"
                    OnSelectedIndexChanged="gvwRoles_SelectedIndexChanged" OnRowDeleting="gvwRoles_RowDeleting">
                    <Columns>
                        <asp:TemplateField HeaderText="Name" FooterStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Left"
                            HeaderStyle-Width="100px">
                            <ItemTemplate>
                                <%# Eval("Name")%></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Date:Time" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="90px">
                            <ItemTemplate>
                                <%# Eval("Date")%></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="EIN" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="20px">
                            <ItemTemplate>
                                <%# Eval("EIN")%></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Employee Name" HeaderStyle-HorizontalAlign="Left"
                            HeaderStyle-Width="100px">
                            <ItemTemplate>
                                <%# Eval("EmployeeName")%></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Action" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="30px">
                            <ItemTemplate>
                                <%# GetAction(Eval("Action")).ToString()%></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Type" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="120px">
                            <ItemTemplate>
                                <%# GetType(Eval("Type"),Eval("SubType"))%></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Before" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"
                            HeaderStyle-Width="100px">
                            <ItemTemplate>
                                <%# Eval("Before") == null ? "-" : (Eval("Before"))%></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="After" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"
                            HeaderStyle-Width="100px">
                            <ItemTemplate>
                                <%# Eval("After") == null ? "-" : (Eval("After"))%></ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <PagerSettings Mode="NumericFirstLast" NextPageText="Next �" PreviousPageText="� Prev"
                        FirstPageText="First" LastPageText="Last" />
                    <RowStyle CssClass="odd" />
                    <AlternatingRowStyle CssClass="even" />
                    <PagerStyle CssClass="defaultPagingBar" />
                    <SelectedRowStyle CssClass="selected" />
                    <EmptyDataTemplate>
                        <b>No change logs. </b>
                    </EmptyDataTemplate>
                </cc2:EmptyDisplayGridView>
                <%--Setting change log--%>
                <cc2:EmptyDisplayGridView Width="100%" Style='clear: both; margin-bottom: 0px;' PagerStyle-HorizontalAlign="Center"
                    PagerStyle-CssClass="defaultPagingBar" CssClass="table table-primary mb30 table-bordered table-hover"
                    UseAccessibleHeader="true" ShowHeaderWhenEmpty="True" ID="gvwSettingChangeLogs"
                    runat="server" AutoGenerateColumns="False" CellPadding="4" GridLines="None" ShowFooterWhenEmpty="False"
                    OnPageIndexChanged="gvwRoles_PageIndexChanged" OnPageIndexChanging="gvwRoles_PageIndexChanging"
                    OnSelectedIndexChanged="gvwRoles_SelectedIndexChanged" OnRowDeleting="gvwRoles_RowDeleting">
                    <Columns>
                        <asp:TemplateField HeaderText="Name" FooterStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Left"
                            HeaderStyle-Width="100px">
                            <ItemTemplate>
                                <%# Eval("Name")%></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Date:Time" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="90px">
                            <ItemTemplate>
                                <%# Eval("Date")%></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Action" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="30px">
                            <ItemTemplate>
                                <%# GetAction(Eval("Action")).ToString()%></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Type" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="130px">
                            <ItemTemplate>
                                <%# GetType(Eval("Type"),Eval("SubType"))%></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Before" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-Width="100px">
                            <ItemTemplate>
                                <%# Eval("Before") %></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="After" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-Width="100px">
                            <ItemTemplate>
                                <%# Eval("After") %></ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <PagerSettings Mode="NumericFirstLast" NextPageText="Next �" PreviousPageText="� Prev"
                        FirstPageText="First" LastPageText="Last" />
                    <RowStyle CssClass="odd" />
                    <AlternatingRowStyle CssClass="even" />
                    <PagerStyle CssClass="defaultPagingBar" />
                    <SelectedRowStyle CssClass="selected" />
                    <EmptyDataTemplate>
                        <b>No change logs. </b>
                    </EmptyDataTemplate>
                </cc2:EmptyDisplayGridView>
                <%--Email Log--%>
                <cc2:EmptyDisplayGridView Width="100%" Style='clear: both; margin-bottom: 0px;' PagerStyle-HorizontalAlign="Center"
                    PagerStyle-CssClass="defaultPagingBar" CssClass="table table-primary mb30 table-bordered table-hover"
                    UseAccessibleHeader="true" ShowHeaderWhenEmpty="True" ID="gvwEmailLogs"
                    runat="server" AutoGenerateColumns="False" CellPadding="4" GridLines="None" ShowFooterWhenEmpty="False"
                    OnPageIndexChanged="gvwRoles_PageIndexChanged" OnPageIndexChanging="gvwRoles_PageIndexChanging"
                    OnSelectedIndexChanged="gvwRoles_SelectedIndexChanged" OnRowDeleting="gvwRoles_RowDeleting">
                    <Columns>
                        <asp:TemplateField HeaderText="User" FooterStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Left"
                            HeaderStyle-Width="100px">
                            <ItemTemplate>
                                <%# Eval("User")%></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Date:Time" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="90px">
                            <ItemTemplate>
                                <%# Eval("DateTime")%></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="EmployeeName" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="30px">
                            <ItemTemplate>
                                <%# (Eval("EmployeeName")).ToString()%></ItemTemplate>
                        </asp:TemplateField>
                      
                        <asp:TemplateField HeaderText="EIN" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-Width="100px">
                            <ItemTemplate>
                                <%# Eval("EIN")%></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Action" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-Width="100px">
                            <ItemTemplate>
                                <%# Eval("Action")%></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ToEmail" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-Width="100px">
                            <ItemTemplate>
                                <%# Eval("ToEmail")%></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Subject" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-Width="100px">
                            <ItemTemplate>
                                <%# Eval("Subject")%></ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Success" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-Width="100px">
                            <ItemTemplate>
                                <%# Eval("Success")%></ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="ErrorMessage" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-Width="100px">
                            <ItemTemplate>
                                <%# Eval("ErrorMessage")%></ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <PagerSettings Mode="NumericFirstLast" NextPageText="Next �" PreviousPageText="� Prev"
                        FirstPageText="First" LastPageText="Last" />
                    <RowStyle CssClass="odd" />
                    <AlternatingRowStyle CssClass="even" />
                    <PagerStyle CssClass="defaultPagingBar" />
                    <SelectedRowStyle CssClass="selected" />
                    <EmptyDataTemplate>
                        <b>No change logs. </b>
                    </EmptyDataTemplate>
                </cc2:EmptyDisplayGridView>
                <%--User Activity change log--%>
                <cc2:EmptyDisplayGridView Width="100%" Style='clear: both' PagerStyle-HorizontalAlign="Center"
                    PagerStyle-CssClass="defaultPagingBar" CssClass="tableLightColor" UseAccessibleHeader="true"
                    ShowHeaderWhenEmpty="True" ID="gvwUserActivity" runat="server" AutoGenerateColumns="False"
                    CellPadding="4" GridLines="None" ShowFooterWhenEmpty="False" OnPageIndexChanged="gvwRoles_PageIndexChanged"
                    OnPageIndexChanging="gvwRoles_PageIndexChanging" OnSelectedIndexChanged="gvwRoles_SelectedIndexChanged"
                    OnRowDeleting="gvwRoles_RowDeleting">
                    <Columns>
                        <asp:TemplateField HeaderText="Name" FooterStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Left"
                            HeaderStyle-Width="100px">
                            <ItemTemplate>
                                <%# Eval("Name")%></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Date:Time" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="90px">
                            <ItemTemplate>
                                <%# Eval("Date")%></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Remarks" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-Width="300px">
                            <ItemTemplate>
                                <%# Eval("Remarks")%></ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <PagerSettings Mode="NumericFirstLast" NextPageText="Next �" PreviousPageText="� Prev"
                        FirstPageText="First" LastPageText="Last" />
                    <RowStyle CssClass="odd" />
                    <AlternatingRowStyle CssClass="even" />
                    <PagerStyle CssClass="defaultPagingBar" />
                    <SelectedRowStyle CssClass="selected" />
                    <EmptyDataTemplate>
                        <b>No User change logs. </b>
                    </EmptyDataTemplate>
                </cc2:EmptyDisplayGridView>
                <uc1:PagingCtl ID="pagintCtl" OnNextRecord="btnNext_Click" OnPrevRecord="btnPrevious_Click"
                    OnDropDownShowRecordsChanged="ddlRecords_SelectedIndexChanged" runat="server" />
            </div>
            <div style='margin-top: 15px'>
                <asp:Button ID="btnRole" CssClass="btn btn-info btn-sect btn-sm" runat="server" Text="Export to Excel"
                    OnClick="btnExport_Click" />
            </div>
        </div>
    </div>
</asp:Content>
