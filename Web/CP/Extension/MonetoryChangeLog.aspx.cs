﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using DAL;
using BLL;
using Utils.Helper;
using Utils.Calendar;
using Utils.Web;

namespace Web.User
{
    public partial class MonetoryChangeLog : BasePage
    {
        //double percentTotal = 0;
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.AdministratorTools;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialise();
              //  ddlUserType_SelectedIndex(null, null);
            }

         
        }

        private void Initialise()
        {
            calFilterFrom.IsEnglishCalendar = true;
            calFilterTo.IsEnglishCalendar = true;

            DateTime date = BLL.BaseBiz.GetCurrentDateAndTime();

            string today = new CustomDate(date.Day, date.Month, date.Year, true).ToString();

            DateTime startingDate = SessionManager.CurrentCompanyFinancialDate.StartingDateEng.Value;
            string starging = new CustomDate(startingDate.Day, startingDate.Month, startingDate.Year, true).ToString(); 

            calFilterFrom.SetSelectedDate(starging, true);
            calFilterTo.SetSelectedDate(today, true);

            if (CommonManager.Setting.EnableEmailLog != null && CommonManager.Setting.EnableEmailLog.Value)
                ddlType.Items.Add(new ListItem { Text="Email Logs", Value = "4" });
           
            //BindUsers();
        }

      

        void BindUsers()
        {

            DateTime start = new DateTime(calFilterFrom.SelectedDate.Year,calFilterFrom.SelectedDateEng.Month, calFilterFrom.SelectedDateEng.Day,0,0,0);
            DateTime end = new DateTime(calFilterTo.SelectedDate.Year, calFilterTo.SelectedDateEng.Month, calFilterTo.SelectedDateEng.Day, 23, 59, 59);



            int totalRecords = 0;



            if (ddlType.SelectedValue == "1")
            {
                gvwProjects.DataSource =
                    CommonManager.GetMonetoryChangeLogs(
                    start, end,
                    txtSearch.Text.Trim(), pagintCtl.CurrentPage, int.Parse(pagintCtl.DDLRecords.SelectedValue), ref totalRecords);
                gvwProjects.DataBind();
            }
            else if (ddlType.SelectedValue == "2")
            {
                gvwSettingChangeLogs.DataSource =
                    CommonManager.GetSettingChangeLogs(
                    start, end,
                    txtSearch.Text.Trim(), pagintCtl.CurrentPage, int.Parse(pagintCtl.DDLRecords.SelectedValue), ref totalRecords);
                gvwSettingChangeLogs.DataBind();

            }
            else if (ddlType.SelectedValue == "3")
            {
                gvwUserActivity.DataSource =
                 CommonManager.GetUserChangeLogs(
                 start, end,
                 txtSearch.Text.Trim(), pagintCtl.CurrentPage, int.Parse(pagintCtl.DDLRecords.SelectedValue), ref totalRecords);
                gvwUserActivity.DataBind();
            }
            else 
            {
                gvwEmailLogs.DataSource =
                 CommonManager.GetEmailLogs(
                 start, end,
                 txtSearch.Text.Trim(), pagintCtl.CurrentPage, int.Parse(pagintCtl.DDLRecords.SelectedValue), ref totalRecords);
                gvwEmailLogs.DataBind();
            }


            pagintCtl.UpdatePagingBar(totalRecords);
          
        }

        protected void gvwRoles_PageIndexChanged(object sender, EventArgs e)
        {

        }

        protected void btnLoad_Click(object sender, EventArgs e)
        {
            pagintCtl.CurrentPage = 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage -= 1;
            BindUsers();
        }


        protected void btnPrevious_Click()
        {
            pagintCtl.CurrentPage -= 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage -= 1;
            BindUsers();
        }

        protected void btnNext_Click()
        {
            pagintCtl.CurrentPage += 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage += 1;
            BindUsers();
        }


        protected void ddlRecords_SelectedIndexChanged()
        {
            //SessionManager.EmployeeDisplayRecodsPerPage = int.Parse(pagintCtl.DDLRecords.SelectedValue);
            //_tempCurrentPage = 1;
            pagintCtl.CurrentPage = 1;
            BindUsers();
        }

        public bool CanChange(object value)
        {
            return Convert.ToBoolean(value);
        }


        protected void gvwRoles_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            //gvwRoles.PageIndex = e.NewPageIndex;
            //gvwRoles.SelectedIndex = -1;
            BindUsers();
          
        }
        
        protected void ddlUserType_SelectedIndex(object sender, EventArgs e)
        {
          
        }

        protected void btnInsertUpdate_Click(object sender, EventArgs e)
        {
         
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {


            DateTime start = new DateTime(calFilterFrom.SelectedDate.Year, calFilterFrom.SelectedDateEng.Month, calFilterFrom.SelectedDateEng.Day, 0, 0, 0);
            DateTime end = new DateTime(calFilterTo.SelectedDate.Year, calFilterTo.SelectedDateEng.Month, calFilterTo.SelectedDateEng.Day, 23, 59, 59);



            int totalRecords = 0;

            switch (ddlType.SelectedValue)
            {
                case "1":
                    List<GetMonetoryChangeLogsResult> list = CommonManager.GetMonetoryChangeLogs(start, end,
                        txtSearch.Text.Trim(), 1, null, ref totalRecords);

                    foreach(var item in list)
                    {
                        item.TypeName = GetType(item.Type, item.SubType);
                        item.ActionName = GetAction(item.Action);
                    }

                    GridViewExportUtil.ExportToExcel<GetMonetoryChangeLogsResult>("Monetory Change Logs.xls", list,
                        new List<string> { "TotalRows", "RowNumber","Type","Action","SubType" }, new List<string> { "EIN" }
                        , new Dictionary<string, string> { { "TypeName","Type" }, { "ActionName","Action"} }
                        , new List<string> { }, new Dictionary<string, string> { }, 
                        new List<string> {"Name","Date","EIN","EmployeeName","ActionName","TypeName","Before","After" });

                    break;
                case "2":

                      List<GetSettingChangeLogsResult> list1 = CommonManager.GetSettingChangeLogs(start, end,
                        txtSearch.Text.Trim(), 1, null, ref totalRecords);
                      GridViewExportUtil.ExportToExcel<GetSettingChangeLogsResult>("Setting Change Logs.xls", list1,
                        new List<string> { "TotalRows", "RowNumber" }, new List<string> { "EIN" });
                    break;
                case "3":
                       List<GetUserActivityChangeLogsResult> list2= CommonManager.GetUserChangeLogs(start, end,
                        txtSearch.Text.Trim(), 1, null, ref totalRecords);
                       GridViewExportUtil.ExportToExcel<GetUserActivityChangeLogsResult>("Setting Change Logs.xls", list2,
                        new List<string> { "TotalRows", "RowNumber" }, new List<string> { "EIN" });
                    break;
                case "4":
                    List<GetEmailLogsResult> list22 = CommonManager.GetEmailLogs(start, end,
                     txtSearch.Text.Trim(), 1, null, ref totalRecords);
                    GridViewExportUtil.ExportToExcel<GetEmailLogsResult>("Email Logs.xls", list22,
                     new List<string> { "TotalRows", "RowNumber" }, new List<string> { "EIN" });
                    break;
            }


           
        }

        public void ddlType_Changed(object sender, EventArgs e)
        {
            gvwProjects.Visible=false;
            gvwSettingChangeLogs.Visible=false;
            gvwUserActivity.Visible=false;
            gvwEmailLogs.Visible = false;

            switch (ddlType.SelectedValue)
            {
                case "1":
                    gvwProjects.Visible = true;
                    
                    break;
                case "2":
                   
                    gvwSettingChangeLogs.Visible = true;
                    break;
                case "3":
                    gvwUserActivity.Visible = true;
                    break;
                case "4":
                    gvwEmailLogs.Visible = true;
                    break;
            }

            btnLoad_Click(null,null);
        }

        public string GetAction(object value)
        {
            LogActionEnum log = (LogActionEnum)int.Parse(value.ToString());
            return log.ToString();
        }


        public string GetType(object valueType,object valueSubType)
        {

            MonetoryChangeLogTypeEnum log = (MonetoryChangeLogTypeEnum)int.Parse(valueType.ToString());
            if (valueSubType.ToString() == "")
                return log.ToString();
            return log.ToString() + " : " + valueSubType.ToString();
        }

        protected void gvwRoles_SelectedIndexChanged(object sender, EventArgs e)
        {
          
            
        }

        protected void gvwRoles_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
          
        }

     

        protected void Page_PreRender(object sender, EventArgs e)
        {
          
        }

     
    }
}
