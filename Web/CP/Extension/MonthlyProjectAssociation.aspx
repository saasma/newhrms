﻿<%@ Page Title="Project Association" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="MonthlyProjectAssociation.aspx.cs" Inherits="Web.CP.MonthlyProjectAssociation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
   
    var CommandHandler = function(command, record){
            <%= hiddenValue.ClientID %>.setValue(record.data.ID);
            
                if(command=="Edit")
                {
                    <%= btnEdit.ClientID %>.fireEvent('click');
                }
                else
                {
                    <%= btnDelete.ClientID %>.fireEvent('click');
                }

             }

    

        function searchList()
        {
            PagingToolbar_ListItems.doRefresh();
        }



    </script>
    <style type="text/css">
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
        #content
        {
            margin: inherit;
        }
        .fieldTable > tr > td
        {
            padding-top: 0px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <ext:ResourceManager ID="ResourceManager1" Namespace="" runat="server" />
    <script type="text/jscript">
 var PagingToolbar_ListItems=null;
      Ext.onReady(
            function () {
                    PagingToolbar_ListItems = <%= PagingToolbar_ListItems.ClientID %>;
            }
        );

    </script>
    <ext:Hidden ID="hiddenValue" runat="server" />
    <ext:Store runat="server" ID="storeProjects">
        <Model>
            <ext:Model ID="Model2" runat="server">
                <Fields>
                    <ext:ModelField Name="ProjectId" Type="String" />
                    <ext:ModelField Name="Name" Type="String" />
                </Fields>
            </ext:Model>
        </Model>
    </ext:Store>
    <ext:LinkButton ID="btnEdit" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnEdit_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:LinkButton ID="btnDelete" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnDelete_Click">
                <EventMask ShowMask="true" />
                <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete ?" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h3>
                    Employee and Project Association
                </h3>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="innerLR" style="clear: both">
            <ext:Container ID="Container3" Region="None" runat="server">
                <Content>
                    <div class="attribute" style="margin-top: 0px;">
                        <table>
                            <tr>
                                <td style="width: 160px">
                                    <ext:DateField ID="txtFromDate" runat="server" FieldLabel="From" EmptyText="" LabelAlign="Top"
                                        LabelSeparator="">
                                    </ext:DateField>
                                </td>
                                <td style="width: 160px">
                                    <ext:DateField ID="txtToDate" runat="server" FieldLabel="To" EmptyText="" LabelAlign="Top"
                                        LabelSeparator="">
                                    </ext:DateField>
                                </td>
                                <td style="width: 210px">
                                    <ext:ComboBox ID="cmbFilterProject" ForceSelection="true" QueryMode="Local" runat="server"
                                        Width="200" LabelAlign="Top" LabelSeparator="" FieldLabel="Project" DisplayField="Name"
                                        ValueField="ProjectId" StoreID="storeProjects">
                                        <Triggers>
                                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                        </Triggers>
                                        <Listeners>
                                            <Select Handler="this.getTrigger(0).show();" />
                                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                            <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                                        </Listeners>
                                    </ext:ComboBox>
                                </td>
                                <td style="width: 210px">
                                    <ext:ComboBox ID="cmbFilterEmployee" ForceSelection="true" runat="server" Width="200"
                                        FieldLabel="Employee" LabelAlign="Top" LabelSeparator="" QueryMode="Local" DisplayField="Name"
                                        ValueField="EmployeeId">
                                        <Store>
                                            <ext:Store ID="store1" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="EmployeeId" Type="String" />
                                                    <ext:ModelField Name="Name" />
                                                </Fields>
                                            </ext:Store>
                                        </Store>
                                        <Triggers>
                                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                        </Triggers>
                                        <Listeners>
                                            <Select Handler="this.getTrigger(0).show();" />
                                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                            <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                                        </Listeners>
                                    </ext:ComboBox>
                                </td>
                                <td style="padding-top: 25px; width: 100px">
                                    <ext:Button Width="70" ID="btnSearch" Cls="btn btn-default btn-sm btn-sect" runat="server"
                                        Text="Search" Height="30">
                                        <DirectEvents>
                                            <Click OnEvent="btnSearch_Click">
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </td>
                            </tr>
                        </table>
                    </div>
                </Content>
            </ext:Container>
            <ext:GridPanel ID="gridProjectAssociation" Border="false" runat="server" Width="900"
                Cls="itemgrid">
                <Store>
                    <ext:Store ID="storeGrid" runat="server" PageSize="50" AutoLoad="true" RemoteSort="true"
                        OnReadData="Store_ReadData">
                        <Model>
                            <ext:Model ID="Model4" runat="server" IDProperty="ID">
                                <Fields>
                                    <ext:ModelField Name="EID" Type="String" />
                                    <ext:ModelField Name="EmployeeName" Type="string" />
                                    <ext:ModelField Name="ProjectId" Type="string" />
                                    <ext:ModelField Name="ProjectName" Type="string" />
                                    <ext:ModelField Name="StartDate" Type="string" />
                                    <ext:ModelField Name="EndDate" Type="string" />
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Proxy>
                            <ext:PageProxy />
                        </Proxy>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:Column ID="colID" Sortable="false" MenuDisabled="true" runat="server" Text="EID"
                            Align="Center" Width="50" DataIndex="EID" />
                        <ext:Column ID="Column5" Sortable="false" MenuDisabled="true" runat="server" Text="EmployeeName"
                            Align="Left" Width="200" DataIndex="EmployeeName" />
                        <ext:Column ID="Column3" Sortable="false" MenuDisabled="true" runat="server" Text="Project Name"
                            Align="Left" Width="200" DataIndex="ProjectName" />
                        <ext:DateColumn ID="Column4" Sortable="false" MenuDisabled="true" runat="server"
                            Text="Start Date" Align="Left" Width="100" DataIndex="StartDate" />
                        <ext:DateColumn ID="Column6" Sortable="false" MenuDisabled="true" runat="server"
                            Text="End Date" Align="Left" Width="100" DataIndex="EndDate" />
                        <ext:CommandColumn ID="CommandColumn1" runat="server" Width="140" Text="Actions"
                            Align="Center">
                            <Commands>
                                <ext:CommandSeparator />
                                <ext:GridCommand Cls="editGridButton" ToolTip-Text="Edit" Text="Edit" Icon="ApplicationEdit"
                                    CommandName="Edit" />
                                <ext:GridCommand Cls="editGridButton" ToolTip-Text="Delete" Text="Delete" Icon="ApplicationDelete"
                                    CommandName="Delete" />
                            </Commands>
                            <Listeners>
                                <Command Handler="CommandHandler(command,record);" />
                            </Listeners>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
                <SelectionModel>
                    <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                </SelectionModel>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar_ListItems" runat="server" StoreID="storeGrid"
                        DisplayInfo="true">
                        <Items>
                            <ext:Label ID="lblPageSize" runat="server" Text="Page size:" />
                            <ext:ToolbarSpacer ID="ToolbarSpacer_listItems" runat="server" Width="10" />
                            <ext:ComboBox ID="ddlListItemPageSize" runat="server" Width="80" SelectOnFocus="true"
                                Selectable="true" ValueField="Value" DisplayField="Text">
                                <Listeners>
                                    <Select Handler="#{storeGrid}.pageSize = this.getValue();#{PagingToolbar_ListItems}.moveFirst(); searchList()" />
                                </Listeners>
                                <Items>
                                    <ext:ListItem Value="50" Text="50" />
                                    <ext:ListItem Value="100" Text="100" />
                                    <ext:ListItem Value="100" Text="200" />
                                </Items>
                                <SelectedItems>
                                    <ext:ListItem Index="0">
                                    </ext:ListItem>
                                </SelectedItems>
                            </ext:ComboBox>
                        </Items>
                    </ext:PagingToolbar>
                </BottomBar>
            </ext:GridPanel>
            <div class="buttonBlockSection" style="margin-top: 10px" runat="server" id="buttonBlock">
                <ext:Button runat="server" Cls="btn btn-primary btn-sect" Height="30px" ID="btnAddNew"
                    Text="<i></i>Add New Association">
                    <DirectEvents>
                        <Click OnEvent="btnAddNew_Click">
                            <EventMask ShowMask="true" />
                        </Click>
                    </DirectEvents>
                </ext:Button>
            </div>
            <br />
        </div>
    </div>
    <br />
    <ext:Window ID="WPublication" runat="server" Title="Add/Edit Project Association"
        Icon="Application" Width="400" Height="300" BodyPadding="5" Hidden="true" Modal="true">
        <Content>
            <table class="fieldTable">
                <tr>
                    <td>
                        <ext:ComboBox ID="cmbEmployee" ForceSelection="true" runat="server" Width="300" FieldLabel="Employee *"
                            LabelAlign="Left" LabelSeparator="" QueryMode="Local" DisplayField="Name" ValueField="EmployeeId">
                            <Store>
                                <ext:Store ID="storeEmployeeList" runat="server">
                                    <Fields>
                                        <ext:ModelField Name="EmployeeId" Type="String" />
                                        <ext:ModelField Name="Name" />
                                    </Fields>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator1" runat="server"
                            ValidationGroup="SaveUpdProjectAssociation" ControlToValidate="cmbEmployee" ErrorMessage="Employee is required." />
                    </td>
                </tr>
               
                <tr>
                    <td>
                        <ext:ComboBox ID="cmbProjects" ForceSelection="true" QueryMode="Local" runat="server"
                            Width="300" LabelAlign="Left" LabelSeparator="" FieldLabel="Project *" DisplayField="Name"
                            ValueField="ProjectId" StoreID="storeProjects">
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator Display="None" ID="rfvName" runat="server" ValidationGroup="SaveUpdProjectAssociation"
                            ControlToValidate="cmbProjects" ErrorMessage="Project is required." />
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:DateField ID="txtStartDate" runat="server" FieldLabel="Start Date *" EmptyText=""
                            LabelAlign="Left" LabelSeparator="" Width="250" StyleSpec="margin-top:7px;">
                        </ext:DateField>
                        <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator2" runat="server"
                            ValidationGroup="SaveUpdProjectAssociation" ControlToValidate="txtStartDate"
                            ErrorMessage="Start Date is required." />
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:DateField ID="txtEndDate" runat="server" FieldLabel="End Date" EmptyText=""
                            Width="250" LabelAlign="Left" LabelSeparator="" StyleSpec="margin-top:7px;">
                        </ext:DateField>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td valign="bottom">
                                    <div class="popupButtonDiv">
                                        <ext:Button runat="server" ID="btnSave" Height="30px" Width="80px" Cls="btn btn-primary"
                                            Text="<i></i>Save">
                                            <DirectEvents>
                                                <Click OnEvent="btnSave_Click">
                                                    <EventMask ShowMask="true" />
                                                </Click>
                                            </DirectEvents>
                                            <Listeners>
                                                <Click Handler="valGroup = 'SaveUpdProjectAssociation'; if(CheckValidation()) return true;">
                                                </Click>
                                            </Listeners>
                                        </ext:Button>
                                </td>
                                <td>
                                    <div class="btnFlatOr">
                                        or</div>
                                </td>
                                <td>
                                    <ext:LinkButton runat="server" Cls="btnFlatLeftGap" ID="LinkButton1" Text="<i></i>Cancel">
                                        <Listeners>
                                            <Click Handler="#{WPublication}.hide();">
                                            </Click>
                                        </Listeners>
                                    </ext:LinkButton>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </Content>
    </ext:Window>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
