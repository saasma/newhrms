<%@ Page MaintainScrollPositionOnPostback="true" Title="Exclude PF Income" Language="C#"
    MasterPageFile="~/Master/NewDetails.Master" AutoEventWireup="true" CodeBehind="ExculdePFImport.aspx.cs"
    Inherits="Web.CP.ExculdePFImport" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<%@ Register Src="UserControls/ExcludePFImportCtl.ascx" TagName="ExcludePFImportCtl"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <% if (BLL.SessionManager.IsCustomRole)
       { %>
    <style type="text/css">
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
        #content
        {
            margin: 0px;
        }
    </style>
    <%} %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <uc1:ExcludePFImportCtl Id="ExcludePFImportCtl1" runat="server" />
</asp:Content>
