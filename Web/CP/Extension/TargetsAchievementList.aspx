<%@ Page MaintainScrollPositionOnPostback="true" Title="Appraisal Target Import" Language="C#"
    MasterPageFile="~/Master/NewDetails.Master" AutoEventWireup="true" CodeBehind="TargetsAchievementList.aspx.cs"
    Inherits="Web.CP.TargetsAchievementList" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<%@ Register src="UserControls/TargetsAchievementCtl.ascx" tagname="TargetsImportCtl" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   
     <% if (BLL.SessionManager.IsCustomRole)
        { %>
    <style type="text/css">
       
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
        #content{margin:0px;}
    </style>
    <%} %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <uc1:TargetsImportCtl ID="TargetsImportCtl1" runat="server" />
</asp:Content>
