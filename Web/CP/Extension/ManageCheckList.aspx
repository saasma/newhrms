<%@ Page MaintainScrollPositionOnPostback="true" Title="CheckList" Language="C#"
    MasterPageFile="~/Master/HROld.Master" AutoEventWireup="true" CodeBehind="ManageCheckList.aspx.cs"
    Inherits="Web.CP.ManageCheckList" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
     .x-toolbar{padding:inherit;}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <ext:ResourceManager DisableViewState="false" ID="ResourceManager1" runat="server"
        ShowWarningOnAjaxFailure="false" ScriptMode="Release">
    </ext:ResourceManager>
     <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Payroll Processing Checklist
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
    <div class="contentArea">
      
        <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
        <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
        <div style="clear: both;">
            <cc2:EmptyDisplayGridView Style='clear: both;' AllowPaging="false" PagerStyle-HorizontalAlign="Center"
                PagerStyle-CssClass="defaultPagingBar" CssClass="table table-primary mb30 table-bordered table-hover" UseAccessibleHeader="true"
                ShowHeaderWhenEmpty="True" ID="list" runat="server" DataKeyNames="CheckListId"
                AutoGenerateColumns="False" ShowHeader="false" CellPadding="4" GridLines="None"
                ShowFooterWhenEmpty="False" OnSelectedIndexChanged="list_OnSelectedIndexChanged"
                OnRowDeleting="list_RowDeleting">
                <Columns>
                    <%--<asp:BoundField HeaderText="SN" DataField="" />--%>
                    <asp:TemplateField HeaderText="Description" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="600px">
                        <ItemTemplate>
                            <div runat="server" id="div" style='<%# "font-size: 14px; color:  " + ( Convert.ToBoolean( Eval("IsCompleted")) ? "green": "red" ) %>'>
                                <%# Convert.ToInt32( DataBinder.Eval(Container, "DataItemIndex")) +  1%>. &nbsp;
                                <%# Eval("Description")%>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                        <ItemTemplate>
                            <asp:Image ID="image" Style='cursor: pointer; display: block' State='<%# Eval("IsCompleted").ToString().ToLower() %>'
                                onclick='<%# "toggle(" + Eval("CheckListId") + ",this)" %>' Width="20" Height="20"
                                runat="server" ToolTip='<%# Convert.ToBoolean( Eval("IsCompleted")) ? "Completed" : "Remaining  " %>'
                                ImageUrl='<%# Convert.ToBoolean( Eval("IsCompleted")) ? "~/images/yes.png" : "~/images/no.png" %>' />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50px"
                        ItemStyle-Width="50px">
                        <ItemTemplate>
                            <asp:ImageButton ID="ImageButton1" runat="server" CommandName="Select" ImageUrl="~/images/edit.gif" />&nbsp;
                            <asp:ImageButton ID="ImageButton2" runat="server" OnClientClick="return confirm('Confirm delete?');"
                                CommandName="Delete" ImageUrl="~/images/delete.gif" />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:TemplateField>
                </Columns>
                <PagerSettings Mode="NumericFirstLast" NextPageText="Next �" PreviousPageText="� Prev"
                    FirstPageText="First" LastPageText="Last" />
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                <PagerStyle CssClass="defaultPagingBar" />
                <SelectedRowStyle CssClass="selected" />
                <EmptyDataTemplate>
                    <b>No CheckList. </b>
                </EmptyDataTemplate>
            </cc2:EmptyDisplayGridView>
        </div>
        <asp:Button ID="btnTestMail" Style='margin-top: 15px' runat="server" CssClass="save"
            Text="Add New" OnClick="btnTestMail_Click1" />
        <asp:Button ID="btnMarkAllCompleted" Style='margin-top: 15px;width:100px' runat="server" CssClass="update"
            Text="All Completed" OnClick="btnMarkAllCompleted_Click" />
        <asp:Button ID="btnAllRemining" Style='margin-top: 15px;width:100px' runat="server" CssClass="update"
            Text="All Remaining" OnClick="btnAllRemining_Click" />
        <ext:Window runat="server" ID="windowPopup" Modal="true" Icon="Application" PaddingSummary="10"
            Hidden="true" Width="550" Resizable="false" Height="430" Title="CheckList Details">
            <Content>
                <ext:TextField ID="txtOrder" Width="300" FieldLabel="CheckList Order" runat="server" LabelAlign="Top" />
                <ext:HtmlEditor Height="150" FieldBodyCls="htmlBody" ID="txtDesc" runat="server"
                    FieldLabel="CheckList action *" Width="455" LabelAlign="Top" />
            </Content>
            <Buttons>
                <ext:Button AutoPostBack="true" OnClick="btnSaveUpdate_Click" ID="btnSaveUpdate"
                    runat="server" Text="Save">
                    <Listeners>
                        <Click Handler="if(#{txtDesc}.getValue().trim() == '') {alert('Action is required.');#{txtDesc}.focus();return false;}" />
                    </Listeners>
                </ext:Button>
                <ext:Button ID="Button2" runat="server" Text="Close">
                    <Listeners>
                        <Click Handler="#{windowPopup}.hide();">
                        </Click>
                    </Listeners>
                </ext:Button>
            </Buttons>
        </ext:Window>
        <script type="text/javascript">

        var windowPopup = null;

        function toggle(checkListId,imageRef)
        {
            var divRef = document.getElementById( imageRef.id.replace("image","div"));

            Web.PayrollService.ToggleCheckList(checkListId);

            if(imageRef.getAttribute('State').toString() == "true")
            {
                imageRef.setAttribute('State','false');
                divRef.style.color = "red";
                imageRef.src = '../../images/no.png';
            }
            else{
                 imageRef.setAttribute('State','true');
                 divRef.style.color = "green";
                 imageRef.src = '../../images/yes.png';
            }
        }
        
        Ext.onReady(
            function () {
                windowPopup = <%= windowPopup.ClientID %>;
            }
        );
       
    
        </script>
    </div>
    </div>
</asp:Content>
