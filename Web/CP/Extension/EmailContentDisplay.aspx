<%@ Page MaintainScrollPositionOnPostback="true" Title="Change Email Content" Language="C#"
    MasterPageFile="~/Master/HROld.Master" AutoEventWireup="true" ValidateRequest="false"
    CodeBehind="EmailContentDisplay.aspx.cs" Inherits="Web.CP.EmailContentDisplay" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .x-toolbar
        {
            padding: inherit;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <ext:ResourceManager ID="ResourceManager1" DisableViewState="false" runat="server"
        ShowWarningOnAjaxFailure="false" ScriptMode="Release">
    </ext:ResourceManager>
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Change Email Content
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="contentArea">
            <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
            <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
            <div style="clear: both">
                <table cellspacing="10" class="fieldTable">
                    <tbody>
                        <tr>
                            <td>
                                <strong>Content Type</strong>
                            </td>
                            <td style="width: 200px">
                                <ext:ComboBox ID="cmbContentType" runat="server" ForceSelection="true" Editable="true"
                                    Width="300px" EmptyText="" SelectOnFocus="true">
                                    <Items>
                                        <ext:ListItem Value="1" Text="Salary Mail" />
                                        <ext:ListItem Value="2" Text="New Password" />
                                        <ext:ListItem Value="5" Text="New UserName" />
                                        <ext:ListItem Value="6" Text="Payroll Signoff/Approval" />
                                        <ext:ListItem Value="10" Text="Appraisal Employee Rollout" />
                                        <ext:ListItem Value="11" Text="Appraisal Supervisor" />
                                        <ext:ListItem Value="12" Text="Appraisal Employee Comment" />
                                        <ext:ListItem Value="13" Text="Appraisal Higher Level Comment" />
                                        <ext:ListItem Value="15" Text="Travel Request Review Comment" />
                                        <ext:ListItem Value="21" Text="Travel Advance Settlement Notification" />
                                        <ext:ListItem Value="16" Text="Timesheet not Filled" />
                                        <ext:ListItem Value="17" Text="Timesheet Approval Request" />
                                        <ext:ListItem Value="18" Text="Timesheet Approved/Rejected" />
                                        <ext:ListItem Value="30" Text="Timesheet Draft " />
                                        <ext:ListItem Value="31" Text="Attendance Late In" />
                                        <ext:ListItem Value="32" Text="Attendance Absent" />
                                        <ext:ListItem Value="33" Text="Attendance Absent Weekly for Employee" />
                                        <ext:ListItem Value="34" Text="Attendance Absent Weekly for Superviser" />
                                        <ext:ListItem Value="35" Text="Time Attendance Comment Approved/Reject" />
                                        <ext:ListItem Value="36" Text="Time Request Approved/Reject" />
                                       
                                        <ext:ListItem Value="38" Text="Birthday Wish" />
                                        <ext:ListItem Value="40" Text="Training" />
                                        <ext:ListItem Value="41" Text="Attendance Absent daily for Superviser" />
                                    </Items>
                                    <DirectEvents>
                                        <Select OnEvent="cboBranch_Select">
                                            <EventMask ShowMask="true" />
                                        </Select>
                                    </DirectEvents>
                                </ext:ComboBox>
                            </td>
                        </tr>
                         <tr>
                            <td>
                                <strong>Placeholders</strong>
                            </td>
                            <td>
                                <ext:DisplayField ID="dispPlaceHolders" runat="server" Width="800" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Subject</strong>
                            </td>
                            <td>
                                <ext:TextField ID="txtSubject" runat="server" Width="600" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Body</strong>
                            </td>
                            <td>
                                <ext:HtmlEditor FieldBodyCls="htmlBody" Height="400" Width="800" ID="HtmlEditor1"
                                    runat="server" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div id="divButtons" runat="server" style='text-align: left; margin-top: 15px; margin-left: 102px'>
            <ext:Button ID="btnSave" runat="server" Cls="btn btn-primary btn-sect" Text="Save Template">
                <DirectEvents>
                    <Click OnEvent="btnSave_Click">
                        <EventMask ShowMask="true" />
                    </Click>
                </DirectEvents>
                <Listeners>
                    <Click Handler="">
                    </Click>
                </Listeners>
            </ext:Button>
            <ext:Label runat="server" ID="lbl">
            </ext:Label>
        </div>
    </div>
</asp:Content>
