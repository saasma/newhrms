<%@ Page Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/Master/Details.Master"
    AutoEventWireup="true" CodeBehind="HREvalulation.aspx.cs" Title="HR Evaluation"
    Inherits="Web.CP.HREvalulation" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Assembly="Karpach.WebControls" Namespace="Karpach.WebControls" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/UpDownCtl.ascx" TagName="UpDownCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="InfoMsgCtl" TagPrefix="uc2" %>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <ext:ResourceManager  DisableViewState="false" ID="ResourceManager1" runat="server" GZip="true" ShowWarningOnAjaxFailure="false"
        Namespace="" ScriptMode="Release">
    </ext:ResourceManager>
    <ext:Hidden runat="server" ID="Hidden_Selection">
    </ext:Hidden>
    <ext:Hidden runat="server" ID="Hidden_EvaluationId">
    </ext:Hidden>
    <ext:Button ID="btnEdit" runat="server" StyleSpec="display:none">
        <DirectEvents>
            <Click OnEvent="btnEdit_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>
    <div class="contentArea">
        <div class="attribute">
            <table class="tblexp">
                <tr>
                    <td>
                        <ext:Store ID="storeEvaluationPeriod" runat="server">
                            <Reader>
                                <ext:JsonReader IDProperty="EvaluationPeriodId">
                                    <Fields>
                                        <ext:RecordField Name="Name" />
                                        <ext:RecordField Name="EvaluationPeriodId" />
                                        <ext:RecordField Name="PeriodDateName" />
                                    </Fields>
                                </ext:JsonReader>
                            </Reader>
                        </ext:Store>
                        <ext:ComboBox Width="140" ID="cmbEvaluationPeriod" ForceSelection="true" FieldLabel="Evalulation Period"
                            LabelAlign="Top" LabelSeparator="" StoreID="storeEvaluationPeriod" runat="server"
                            DisplayField="Name" ValueField="EvaluationPeriodId">
                            <Listeners>
                                <Select Handler="searchList();" />
                            </Listeners>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator runat="server" Display="None" ID="valEval" ErrorMessage="Evaluation period is required."
                            ValidationGroup="Evaluation" ControlToValidate="cmbEvaluationPeriod" />
                    </td>
                    <td>
                        <ext:ComboBox Width="120" ID="cmbBranch" FieldLabel="Branch" LabelAlign="Top" LabelSeparator=""
                            runat="server" DisplayField="Name" ValueField="BranchId" Mode="Local">
                            <DirectEvents>
                                <Select OnEvent="cboBranch_Select">
                                    <EventMask ShowMask="true" />
                                </Select>
                            </DirectEvents>
                            <Store>
                                <ext:Store runat="server">
                                    <Reader > 
                                        <ext:JsonReader IDProperty="BranchId">
                                            <Fields>
                                                <ext:RecordField Name="Name" />
                                                <ext:RecordField Name="BranchId" />
                                            </Fields>
                                        </ext:JsonReader>
                                    </Reader>
                                </ext:Store>
                            </Store>
                            <Listeners>
                                <Focus Handler="if( this.getValue()=='-1' ) this.clearValue();" />
                                <Select Handler="if(this.getValue()=='-1') this.clearValue();" />
                                  <Blur Handler="if(this.getStore().getById(this.getValue())==null) this.clearValue(); " />
                            </Listeners>
                            <Items>
                                <ext:ListItem Value="-1" Text="&nbsp;" />
                            </Items>
                        </ext:ComboBox>
                    </td>
                    <td>
                        <ext:ComboBox Width="120" ID="cmbDepartment" FieldLabel="Department" LabelAlign="Top"
                            LabelSeparator="" runat="server" DisplayField="Name" ValueField="DepartmentId" Mode="Local">
                               <DirectEvents>
                                <Select OnEvent="cmbDepartment_Select">
                                    <EventMask ShowMask="true" />
                                </Select>
                            </DirectEvents>
                            <Store>
                                <ext:Store runat="server">
                                    <Reader>
                                        <ext:JsonReader IDProperty="DepartmentId">
                                            <Fields>
                                                <ext:RecordField Name="Name" />
                                                <ext:RecordField Name="DepartmentId" />
                                            </Fields>
                                        </ext:JsonReader>
                                    </Reader>
                                </ext:Store>
                            </Store>
                            <Listeners>
                                <Focus Handler="if( this.getValue()=='-1' ) this.clearValue();" />
                                <Select Handler="if(this.getValue()=='-1') this.clearValue();" />
                                  <Blur Handler="if(this.getStore().getById(this.getValue())==null) this.clearValue(); " />
                            </Listeners>
                            <Items>
                                <ext:ListItem Value="-1" Text="&nbsp;" />
                            </Items>
                        </ext:ComboBox>
                    </td>
                     <td>
                        <ext:ComboBox Width="120" ID="cmbSubDepartment" FieldLabel="Sub-Department" LabelAlign="Top"
                            LabelSeparator="" runat="server" DisplayField="Name" ValueField="SubDepartmentId" Mode="Local">
                            <Store>
                                <ext:Store ID="Store3" runat="server">
                                    <Reader>
                                        <ext:JsonReader IDProperty="SubDepartmentId">
                                            <Fields>
                                                <ext:RecordField Name="Name" />
                                                <ext:RecordField Name="SubDepartmentId" />
                                            </Fields>
                                        </ext:JsonReader>
                                    </Reader>
                                </ext:Store>
                            </Store>
                            <Listeners>
                                <Focus Handler="if( this.getValue()=='-1' ) this.clearValue();" />
                                <Select Handler="if(this.getValue()=='-1') this.clearValue();" />
                                  <Blur Handler="if(this.getStore().getById(this.getValue())==null) this.clearValue(); " />
                            </Listeners>
                            <Items>
                                <ext:ListItem Value="-1" Text="&nbsp;" />
                            </Items>
                        </ext:ComboBox>
                    </td>
                    <td>
                        <ext:ComboBox Width="150" ID="cmbDesignation" FieldLabel="Designation" LabelAlign="Top"
                            LabelSeparator="" runat="server" EmptyText="" AllowBlank="true" DisplayField="Name" Mode="Local"
                            ValueField="DesignationId">
                            <Store>
                                <ext:Store runat="server">
                                    <Reader>
                                        <ext:JsonReader IDProperty="DesignationId">
                                            <Fields>
                                                <ext:RecordField Name="Name" />
                                                <ext:RecordField Name="DesignationId" />
                                            </Fields>
                                        </ext:JsonReader>
                                    </Reader>
                                </ext:Store>
                            </Store>
                            <Listeners>
                                <Focus Handler="if( this.getValue()=='-1' ) this.clearValue();" />
                                <Select Handler="if(this.getValue()=='-1') this.clearValue();" />
                                  <Blur Handler="if(this.getStore().getById(this.getValue())==null) this.clearValue(); " />
                            </Listeners>
                            <Items>
                                <ext:ListItem Value="-1" Text="&nbsp;" />

                            </Items>
                        </ext:ComboBox>
                    </td>
                    <td>
                        <ext:TextField ID="txtEmpSearch" runat="server" FieldLabel="Employee Search" LabelAlign="Top"
                            LabelSeparator="" Width="120" />
                    </td>
                    <td style='vertical-align: bottom; padding-bottom: 14px;'>
                        <ext:Button ID="btnLoad" runat="server" Height="22" Text="Load" Icon="ArrowRefresh"
                            Width="80">
                            <Listeners>
                                <Click Handler="return searchList();" />
                            </Listeners>
                        </ext:Button>
                    </td>
                    <td style='vertical-align: bottom; padding-left: 5px; width: 100px'>
                        <%--  <pr:CalendarExtControl ID="cal" runat="server" />--%>
                        <asp:LinkButton ID="LinkButton_Rating" runat="server" OnClientClick="PopUpPositionCall();return false;"
                            Text="Define Rating"></asp:LinkButton>
                        &nbsp;
                        <asp:LinkButton ID="LinkButton_Period" runat="server" OnClientClick="PopUpPositionCall1();return false;"
                            Text="Evaluation Period"></asp:LinkButton>
                    </td>
                </tr>
            </table>
        </div>
        <ext:Store ID="storeRatings" runat="server">
            <Reader>
                <ext:JsonReader IDProperty="RatingId">
                    <Fields>
                        <ext:RecordField Name="RatingId" Type="Int" />
                        <ext:RecordField Name="Name" />
                    </Fields>
                </ext:JsonReader>
            </Reader>
        </ext:Store>
        <br />
        <div style="clear: both" id="divGrid" runat="server">
            <ext:GridPanel ID="gvwEvalulations" runat="server" StripeRows="true" ClicksToEdit="1"
                Title="Evaluation List" TrackMouseOver="true" Cls="gridtbl" Width="980" AutoHeight="true">
                <Listeners>
                    <RowDblClick Fn="rowDoubleClick" />
                </Listeners>
                <SelectionModel>
                    <ext:RowSelectionModel runat="server">
                    </ext:RowSelectionModel>
                </SelectionModel>
                <Listeners>
                    <AfterEdit Fn="AfterEditHandler" />
                    <BeforeEdit Fn="BeforeEditHandler" />
                </Listeners>
                <Store>
                    <ext:Store ID="Store2" runat="server" AutoLoad="false">
                        <Proxy>
                            <ext:HttpProxy Method="GET" Url="~/Handler/HREvaluationInfoHandler.ashx" />
                        </Proxy>
                        <AutoLoadParams>
                            <ext:Parameter Name="start" Value="={0}" />
                            <ext:Parameter Name="limit" Value="20" />
                        </AutoLoadParams>
                        <BaseParams>
                            <ext:Parameter Name="start" Value="={0}" />
                            <ext:Parameter Name="limit" Value="#{cmbPageSize}.getValue()" Mode="Raw" />
                            <ext:Parameter Name="evaluationId" Value="#{cmbEvaluationPeriod}.getValue()" Mode="Raw" />
                            <ext:Parameter Name="companyId" Value="" />
                            <ext:Parameter Name="branchId" Value="#{cmbBranch}.getValue()" Mode="Raw" />
                            <ext:Parameter Name="departmentId" Value="#{cmbDepartment}.getValue()" Mode="Raw" />
                            
                            <ext:Parameter Name="subDepartmentId" Value="#{cmbSubDepartment}.getValue()" Mode="Raw" />
                            <ext:Parameter Name="designationId" Value="#{cmbDesignation}.getValue()" Mode="Raw" />
                            <ext:Parameter Name="empSearch" Value="#{txtEmpSearch}.getValue()" Mode="Raw" />
                        </BaseParams>
                        <Reader>
                            <ext:JsonReader Root="Data" TotalProperty="TotalRecords" IDProperty="EmployeeId">
                                <Fields>
                                    <ext:RecordField Name="EvaluationId" />
                                    <ext:RecordField Name="PeriodDateName" />
                                    <ext:RecordField Name="EvaluationPeriodId" />
                                    <ext:RecordField Name="IdCardNo" />
                                    <ext:RecordField Name="Name" />
                                    <ext:RecordField Name="EmployeeId" />
                                    <ext:RecordField Name="EvalulationDate" Type="String" />
                                    <ext:RecordField Name="RatingId" />
                                    <ext:RecordField Name="Comment" />
                                    <ext:RecordField Name="History" />
                                </Fields>
                            </ext:JsonReader>
                        </Reader>
                    </ext:Store>
                </Store>
                <ColumnModel ID="ColumnModel2" runat="server">
                    <Columns>
                        <ext:Column Width="40" ColumnID="EID" Header="EIN" DataIndex="EmployeeId" />
                        <ext:Column Width="80" Header="I No" DataIndex="IdCardNo">
                        </ext:Column>
                        <ext:Column Width="180" ColumnID="Name" Header="Name" DataIndex="Name">
                        </ext:Column>
                        <ext:Column Width="140" Header="Evaluation Period" DataIndex="PeriodDateName">
                        </ext:Column>
                        <ext:Column Header="Evaluation Date" ColumnID="EvalulationDate" DataIndex="EvalulationDate">
                            <Renderer Fn="dateRenderer" />
                        </ext:Column>
                        <ext:Column Header="Rating" DataIndex="RatingId" Width="100">
                            <Renderer Fn="ratingRenderer" />
                        </ext:Column>
                        <ext:Column Width="230" Header="Comment" DataIndex="Comment">
                            <%--   <Editor>
                                <ext:TextArea runat="server" />
                            </Editor>--%>
                        </ext:Column>
                        <ext:Column Width="80" Header="History" DataIndex="History">
                            <Renderer Fn="historyRenderer" />
                        </ext:Column>
                    </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar1" runat="server" PageSize="20" DisplayInfo="true"
                        DisplayMsg="(Double click to edit) Displaying employees {0} - {1} of {2}" EmptyMsg="No Evaluation to display">
                        <Items>
                            <ext:ComboBox runat="server" SelectedIndex="0" Width="80" ID="cmbPageSize">
                                <Items>
                                    <ext:ListItem Text="20" Value="20" />
                                    <ext:ListItem Text="30" Value="30" />
                                    <ext:ListItem Text="50" Value="50" />
                                </Items>
                                <Listeners>
                                    <Select Handler="#{PagingToolbar1}.pageSize = parseInt( this.getValue()); searchList();" />
                                </Listeners>
                            </ext:ComboBox>
                        </Items>
                    </ext:PagingToolbar>
                </BottomBar>
                <LoadMask ShowMask="true" />
            </ext:GridPanel>
        </div>
    </div>
    <ext:Window runat="server" ID="window" Icon="Application" Hidden="true" Width="400"
        Resizable="true" Height="300" Title="Evaluation">
        <Content>
            <table class="tableField">
                <tr>
                    <td>
                        <ext:Label ID="lblEmployee" Width="300" LabelWidth="120" runat="server" FieldLabel="Employee" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:Label ID="lblEvaluationPeriod" Width="300" LabelWidth="120" runat="server" FieldLabel="Evaluation Period" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:ComboBox ID="cmbRating" Width="300" runat="server" LabelWidth="120" FieldLabel="Rating *"
                            StoreID="storeRatings" DisplayField="Name" ValueField="RatingId" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <pr:CalendarExtControl LabelWidth="120" Width="300" FieldLabel="Evaluation Date *"
                            ID="calDate" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:TextArea ID="txtComment" Width="300" LabelWidth="120" runat="server" FieldLabel="Comment" />
                    </td>
                </tr>
            </table>
            <asp:RequiredFieldValidator runat="server" Display="None" ID="RequiredFieldValidator1"
                ErrorMessage="Rating is required." ValidationGroup="AEEvaluation" ControlToValidate="cmbRating" />
            <asp:RequiredFieldValidator runat="server" Display="None" ID="RequiredFieldValidator2"
                ErrorMessage="Date is required." ValidationGroup="AEEvaluation" ControlToValidate="calDate" />
        </Content>
        <Buttons>
            <ext:Button ID="btnSave" runat="server" Text="Save">
                <DirectEvents>
                    <Click OnEvent="btnSave_Click">
                        <EventMask ShowMask="true" />
                    </Click>
                </DirectEvents>
                <Listeners>
                    <Click Handler="valGroup = 'AEEvaluation'; return CheckValidation();" />
                </Listeners>
            </ext:Button>
            <ext:Button ID="btnCancel" runat="server" Text="Cancel">
                <Listeners>
                    <Click Handler=" #{window}.hide();"></Click>
                </Listeners>
            </ext:Button>
        </Buttons>
    </ext:Window>
    <ext:Hidden ID="hdnHistory" runat="server" />
    <ext:Button ID="btnHistory" runat="server" StyleSpec="display:none">
        <DirectEvents>
            <Click OnEvent="btnHistory_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>
    <ext:Window runat="server" ID="winHistory" Icon="Application" Hidden="true" Width="580"
        Resizable="true" Height="300" Title="Evaluation History">
        <Content>
            <ext:GridPanel ID="gridHistory" runat="server" StripeRows="true" ClicksToEdit="1"
                Header="false" TrackMouseOver="true" Cls="gridtbl" Height="250">
                <Store>
                    <ext:Store ID="Store1" runat="server" AutoLoad="false">
                        <Reader>
                            <ext:JsonReader>
                                <Fields>
                                    <ext:RecordField Name="EvalulationPeriodId" />
                                    <ext:RecordField Name="EvaluationPeriodName" />
                                    <ext:RecordField Name="EvaluationPeriodDateName" />
                                    <ext:RecordField Name="RatingId" />
                                    <ext:RecordField Name="EvalulationDate" />
                                    <ext:RecordField Name="Comment" />
                                    <ext:RecordField Name="History" />
                                </Fields>
                            </ext:JsonReader>
                        </Reader>
                    </ext:Store>
                </Store>
                <ColumnModel ID="ColumnModel1" runat="server">
                    <Columns>
                        <ext:Column Width="100" Header="Period Name" DataIndex="EvalulationPeriodId">
                            <Renderer Fn="evaluationName" />
                        </ext:Column>
                        <ext:Column Width="130" Header="Evaluation Period" DataIndex="EvalulationPeriodId">
                            <Renderer Fn="evaluationPeriodDateName" />
                        </ext:Column>
                        <ext:Column Header="Evaluation Date" Width="100" DataIndex="EvalulationDate">
                        </ext:Column>
                        <ext:Column Header="Rating" DataIndex="RatingId" Width="80">
                            <Renderer Fn="ratingRenderer" />
                        </ext:Column>
                        <ext:Column Width="140" Header="Comment" DataIndex="Comment">
                        </ext:Column>
                    </Columns>
                </ColumnModel>
            </ext:GridPanel>
        </Content>
        <Buttons>
            <ext:Button ID="Button2" runat="server" Text="Close">
                <Listeners>
                    <Click Handler=" #{winHistory}.hide();"></Click>
                </Listeners>
            </ext:Button>
        </Buttons>
    </ext:Window>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="pageScriptsContent" runat="server">
    <ext:ResourcePlaceHolder ID="ResourcePlaceHolder1" Mode="Script" runat="server">
    </ext:ResourcePlaceHolder>
    <script type="text/javascript">

    var EvaluationCombo = null;
    var hdnEvaluationId = null;
    function PopUpPositionCall() {
        var ret = PopUpPosition("isPopup=true");
        if (typeof (ret) != 'undefined') {
            //ChangeDropDownList('ddlDepartments.ClientID ', ret);
        }
    }
    function PopUpPositionCall1() {
        var ret = PopUpPosition1("isPopup=true");
        if (typeof (ret) != 'undefined') {
            //ChangeDropDownList('ddlDepartments.ClientID ', ret);
        }
    }


    var cmb = function (e1, e2, e3) {
        
    }

    Ext.onReady(function () { 

     EvaluationCombo = <%= cmbEvaluationPeriod.ClientID%>;
     hdnEvaluationId =<%=Hidden_EvaluationId.ClientID %>

    });

    function searchList()
    {
        valGroup='Evaluation';

        if( CheckValidation()==true)
       

        <%= PagingToolbar1.ClientID %>.moveFirst();


    }

    var dateRenderer = function(value,e1,e2,e3)
    {
        return value;
    }
    
        var record;
        var BeforeEditHandler = function(e) {
            record = null;
            isDateColumnChanged = false;
            switch(e.field.toLowerCase())
            {
                case "evalulationdate":
                    isDateColumnChanged = true;
                    record = e.record;
                break;
            }       
        }
        var AfterEditHandler = function(e)
        {
           e.record.commit();
        }
        function refreshGridViewForDateInGrid(date)
        {
            if( date == "" )
                return;
          
            record.set('EvalulationDate',date);
            record.commit();
            //record.store.commit();
        }

        var ratingRenderer = function(value)
        {
            var r = <%= storeRatings.ClientID %>.getById(value);

            if( Ext.isEmpty(r))
                return "";

            return r.data.Name;
        }

        var rowDoubleClick = function(e)
        {
            var employeeId = e.selModel.selections.items[0].data.EmployeeId;
            var evalulationPeriodId = e.selModel.selections.items[0].data.EvaluationPeriodId;
            var evalulationId =  e.selModel.selections.items[0].data.EvaluationId;


            <%= Hidden_Selection.ClientID %>.setValue( employeeId +":"+ evalulationPeriodId+":"+ evalulationId);

            <%= btnEdit.ClientID %>.fireEvent('click');
        }
        var historyRenderer = function(value, meta, record)
        {
       
            return "<a href='#'  onclick=\"DisplayHistory("+ record.data.EvaluationId + "," + record.data.EmployeeId +");\">History</a>";
        }

        function DisplayHistory(evaluationId,employeeId)
        {
            <%= hdnHistory.ClientID %>.setValue(employeeId + ":" + <%= cmbEvaluationPeriod.ClientID %>.getValue());
            <%= btnHistory.ClientID %>.fireEvent('click');
        }

        var evaluationName = function(value)
        {
            var r = <%= storeEvaluationPeriod.ClientID %>.getById(value);
            if( Ext.isEmpty(r))
                return "";

            return r.data.Name;
        }
        var evaluationPeriodDateName = function(value)
        {
            var r = <%= storeEvaluationPeriod.ClientID %>.getById(value);
            if( Ext.isEmpty(r))
                return "";

            return r.data.PeriodDateName;
        }
    </script>
    <style>
        .tableField
        {
            padding: 20px;
        }
        .tableField td
        {
            padding-bottom: 5px;
        }
    </style>
</asp:Content>
