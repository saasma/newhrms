<%@ Page MaintainScrollPositionOnPostback="true" Title="Manage Letters" Language="C#"
    MasterPageFile="~/Master/Details.Master" AutoEventWireup="true" ValidateRequest="false"
    CodeBehind="ManageLetter.aspx.cs" Inherits="Web.CP.ManageLetter" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <ext:ResourceManager ID="ResourceManager1" DisableViewState="false" runat="server"
        ShowWarningOnAjaxFailure="false" ScriptMode="Release">
    </ext:ResourceManager>
    
    <script type="text/javascript">


        var placeholderItemsArray = [['Employee ID'], ['Employee Name']];
        
    </script>
    <div class="contentArea">
        <h3>
            Manage Letters Content</h3>
        <%-- <div class="attribute">
            <table>
                <tr>
                    <td style='width: 170px'>
                        <strong runat="server" id="payrollText">Content Type</strong>
                        <asp:DropDownList AutoPostBack="true" ID="ddlPayrollPeriods" DataTextField="Text"
                            DataValueField="Value" AppendDataBoundItems="true" Width="150px" runat="server"
                            OnSelectedIndexChanged="ddlPayrollPeriods_SelectedIndexChanged">
                            <asp:ListItem Value="0" Text=""></asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator InitialValue="0" Display="None" ControlToValidate="ddlPayrollPeriods"
                            ValidationGroup="Overtime" ID="RequiredFieldValidator2" runat="server" ErrorMessage="Content Type is required."></asp:RequiredFieldValidator>
                    </td>
                </tr>
            </table>
        </div>--%>
        <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
        <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
        <div style="clear: both">
            <table cellspacing="10">
                <tr>
                    <td>
                        <strong>Letter Module</strong>
                    </td>
                    <td style="width: 200px">
                        <ext:ComboBox ID="cmbContentType" runat="server" ForceSelection="true" Editable="true"
                            Width="300px" EmptyText="" SelectOnFocus="true">
                            <Items>
                                <ext:ListItem Value="1" Text="Salary Mail" />
                                <ext:ListItem Value="2" Text="New Password" />
                                <ext:ListItem Value="5" Text="New UserName" />
                                <ext:ListItem Value="6" Text="Payroll Signoff/Approval" />
                            </Items>
                            <DirectEvents>
                                <Select OnEvent="cboBranch_Select">
                                    <EventMask ShowMask="true" />
                                </Select>
                            </DirectEvents>
                        </ext:ComboBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>Subject</strong>
                    </td>
                    <td>
                        <ext:TextField ID="txtSubject" runat="server" Width="500" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>Body</strong>
                    </td>
                    <td>
                        <CKEditor:CKEditorControl ID="CKEditor" Width="700" Height="300" runat="server" BasePath ="~/ckeditor">
                        </CKEditor:CKEditorControl>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="buttonsDiv" id="divButtons" runat="server" style='text-align: right;
        margin-top: 15px'>
        <ext:Button ID="btnSave" runat="server" Icon="ApplicationAdd" Width="100" Text="Save">
            <DirectEvents>
                <Click OnEvent="btnSave_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
            <Listeners>
                <Click Handler="">
                </Click>
            </Listeners>
        </ext:Button>
        <ext:Label runat="server" ID="lbl">
        </ext:Label>
    </div>
    
</asp:Content>
