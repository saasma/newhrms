﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using Utils.Helper;
using Utils.Web;
using BLL.Base;
using DAL;
using Utils.Calendar;
using BLL.BO;

namespace Web
{
    public partial class ServiceYearReport : BasePage
    {


        private int _tempCurrentPage;
        private int _tempCount;
        private PayManager payMgr = new PayManager();

        protected void Page_PreRender(object sender, EventArgs e)
        {
               //skip for some child master pages like popup

            bool isEnglish = IsEnglish;

            if (chkEnglishDate.Checked)
                isEnglish = true;

            contentHeader.ExtResourceManager.
                RegisterClientInitScript("DatePage122", string.Format("isEnglish = {0};", isEnglish.ToString().ToLower()));



            contentHeader.ExtResourceManager.RegisterClientInitScript("DatePage12", string.Format("todayDate = '{0}';function calendarOnClick(event, sourceElement, e3){{}};",
                                                                      CustomDate.GetTodayDate(isEnglish).ToString()));

        }
        

        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!IsPostBack)
            {
              
                Initialise();

                
            }

            JavascriptHelper.AttachNonDialogPopUpCode(Page, "positionHistoryPopup", "EmployeeGradeStepPopup.aspx", 1050, 350);
        
        }

     
        protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListItem firstItem = ddlDepartment.Items[0];
            ddlDepartment.Items.Clear();



            ddlDepartment.DataSource
                = DepartmentManager.GetAllDepartmentsByBranch(int.Parse(ddlBranch.SelectedValue));

            ddlDepartment.DataBind();

            ddlDepartment.Items.Insert(0, firstItem);
        }

        void Initialise()
        {

            

        

            ddlBranch.DataSource
              = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            ddlBranch.DataBind();


            List<KeyValue> statues = new JobStatus().GetMembers();

            statues.RemoveAt(0);
            statues.Insert(0, new KeyValue { Key = "-1", Value = "Join date" });

            ddlDateFrom.DataSource = statues;
            ddlDateFrom.DataBind();

        }
        protected void btnLoad_Click(object sender, EventArgs e)
        {
        
            LoadEmployees();
           
        }
        protected void gvwEmployees_RowCreated(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover",
                "this.originalClass=this.className;this.className='selected'");

                e.Row.Attributes.Add("onmouseout",
                "this.className=this.originalClass;");


            }

        }
        protected void LoadEmployees()
        {

           
            //gvw.DataSource = list;                
            //gvw.DataBind();

            bool? type=null;


            if( ddlType.SelectedIndex != 0)
                type = bool.Parse(ddlType.SelectedValue);

            DateTime? from = null, to = null;

            if (IsEnglish || chkEnglishDate.Checked)
            {

                if (!string.IsNullOrEmpty(calFrom.Text))
                    from = Convert.ToDateTime(calFrom.Text);

                if (!string.IsNullOrEmpty(calTo.Text))
                    to = Convert.ToDateTime(calTo.Text);
            }
            else
            {
                from = GetEngDate(calFrom.Text);
                to = GetEngDate(calTo.Text);
            }

            double? value = null;

            if (!string.IsNullOrEmpty(txtDayOrYear.Text))
                value = double.Parse(txtDayOrYear.Text.Trim());

            gvw.DataSource = ReportManager.GetServiceYearList
                (from,to, int.Parse(ddlBranch.SelectedValue), int.Parse(ddlDepartment.SelectedValue),
                type,value
                ,int.Parse(ddlDateFrom.SelectedValue));

            gvw.DataBind();

            if (IsEnglish)
            {
                gvw.Columns[5].Visible = false;

            }

           
            
        }

        public string GetEngOrNepDate(object value)
        {
            if (value == null)
                return "";

            DateTime date = Convert.ToDateTime(value);

            if (date == DateTime.MinValue)
                return "";

            CustomDate c = new CustomDate(date.Day, date.Month, date.Year, true);

            if (IsEnglish)
                return c.ToString();

            else
                return CustomDate.ConvertEngToNep(c).ToString();
        }

       
        protected void btnExport_Click(object sender, EventArgs e)
        {


            bool? type = null;


            if (ddlType.SelectedIndex != 0)
                type = bool.Parse(ddlType.SelectedValue);

            DateTime? from = null, to = null;

            if (IsEnglish || chkEnglishDate.Checked)
            {

                if (!string.IsNullOrEmpty(calFrom.Text))
                    from = Convert.ToDateTime(calFrom.Text);

                if (!string.IsNullOrEmpty(calTo.Text))
                    to = Convert.ToDateTime(calTo.Text);
            }
            else
            {
                from = GetEngDate(calFrom.Text);
                to = GetEngDate(calTo.Text);
            }

            double? value = null;

            if (!string.IsNullOrEmpty(txtDayOrYear.Text))
                value = double.Parse(txtDayOrYear.Text.Trim());


           List<GetServiceYearReportResult> _ListResult =  ReportManager.GetServiceYearList
                (from, to, int.Parse(ddlBranch.SelectedValue), int.Parse(ddlDepartment.SelectedValue),
                type, value
                , int.Parse(ddlDateFrom.SelectedValue));


            List<string> _hideColumnList = new List<string>();


            string[] _hideColumn = {"Designation","Status" };
            _hideColumnList.AddRange(_hideColumn.ToList());
            if (IsEnglish)
                _hideColumnList.Add("JoinDate");


            Bll.ExcelHelper.ExportToExcel("Service Year Report", _ListResult,
            _hideColumnList,
            new List<String>() { },
            new Dictionary<string, string>() { { "JoinDateEng", "Join Date(A.D)" }, { "JoinDate", "Join Date(B.S)" }, { "ServicePeriod", "Service Period" }, { "StatusOrJoinDate", "Period Start Date" }, { "CompletionDateEng", "Period Completion Date" }, { "StatusName", "Staus" } },
            new List<string>() { }
            , new Dictionary<string, string>() { }
            , new List<string> { "EIN", "NAME", "Branch", "Department", "StatusName", "JoinDateEng", "JoinDateNepali", "StatusOrJoinDate", "CompletionDateEng", "ServicePeriod" });

          // GridViewExportUtil.Export("Service Year.xls", gvw);
        }

        //protected void btnPostToSalary_Click(object sender, EventArgs e)
        //{

        //    foreach (GridViewRow row in gvw.Rows)
        //    {


        //        int payrollPeriodId = (int)gvw.DataKeys[row.RowIndex]["PayrollPeriodId"];

        //        PayManager.PostOvertimePay(payrollPeriodId);
        //        msgCtl.InnerHtml = Resources.Messages.OvertimePostedMessage;
        //        msgCtl.Hide = false;
        //        break;
        //    }


        //}
        protected void txtEmployeeName_TextChanged(object sender, EventArgs e)
        {
            

        }

      
       

    }
}
