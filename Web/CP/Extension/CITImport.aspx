<%@ Page MaintainScrollPositionOnPostback="true" Title="CIT Import" Language="C#"
    MasterPageFile="~/Master/Details.Master" AutoEventWireup="true" CodeBehind="CITImport.aspx.cs"
    Inherits="Web.CP.CITImport" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   
   
  
    <style type="text/css">
        .excelLikeTable
        {
            /*border: AppWorkspace solid 1px;*/
            font-size: 0.8em;
            text-align: center;
            border-collapse: collapse !important;
        }
        .excelLikeTable th
        {
            font-size: 11px;
        }
        .excelLikeTable tr
        {
        }
        .excelLikeTable tr.header
        {
            font-weight: bold;
            background-color: #ffffef;
        }
        .excelLikeTable td
        {
            border: 1px solid AppWorkspace;
            margin: 0 !important;
            padding: 0px !important;
            height: 19px;
        }
        .excelLikeTable tr td input
        {
            padding: 0;
            margin: 0;
            border: 0px;
            padding-left: 2px;
            padding-right: 2px;
            text-align: right;
            font-size: 11px;
            width: 60px;
            height: 19px !important;
            margin: 0px !important;
            vertical-align: middle !important;
        }
        .inputFocus
        {
            background-color: lightgreen;
        }
    </style>
  
    <style type="text/css">
        .empCell
        {
            width: 140px !important;
        }
        .empCell input
        {
            width: 140px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <div class="contentArea">
        <h3>
            Import CIT</h3>
        <div class="attribute">
            <table>
                <tr>
                   
                    <td style='padding-left: 10px'>
                       
                        <asp:LinkButton ID="btnExport" runat="server" Text="Excel import" OnClientClick="shiftPopup();return false;" 
                            CssClass=" excel marginRight tiptip" Style="float: left;" />
                    </td>
                </tr>
            </table>
        </div>
        <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
        <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
        <div class="gridBlock1" style='clear:both'>

        <cc2:EmptyDisplayGridView Style='clear: both' GridLines="None" PagerStyle-CssClass="defaultPagingBar"
            CssClass="excelLikeTable" ShowHeaderWhenEmpty="True" ID="gvwOvertimes" runat="server"
            DataKeyNames="EIN" AutoGenerateColumns="False" ShowFooterWhenEmpty="False" AllowPaging="True"
            PageSize="15" OnPageIndexChanging="gvwOvertimes_PageIndexChanging">
            <Columns>
                <asp:TemplateField HeaderText="EIN" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# "&nbsp;" + Eval("EIN")%>' Width="30px" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Employee" HeaderStyle-CssClass="empCell" ItemStyle-CssClass="empCell"
                    ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <asp:Label runat="server" Text='<%# "&nbsp;" + Eval("Employee")%>' Width="140px" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="July/2011">
                    <ItemTemplate>
                        <asp:TextBox id="Month6" runat="server"
                            data-col='0' data-row='<%# Container.DataItemIndex %>' ReadOnly="true"  Text='<%# Eval("Month6","{0:N2}")%>' />
                    </ItemTemplate>
                </asp:TemplateField>
             
             <asp:TemplateField HeaderText="August/2011">
                    <ItemTemplate>
                        <asp:TextBox id="Month7" runat="server"
                            data-col='0' data-row='<%# Container.DataItemIndex %>'  ReadOnly="true" Text='<%# Eval("Month7","{0:N2}")%>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Sep/2011">
                    <ItemTemplate>
                        <asp:TextBox id="Month8" runat="server"
                            data-col='0' data-row='<%# Container.DataItemIndex %>' ReadOnly="true"  Text='<%# Eval("Month8","{0:N2}")%>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Oct/2011">
                    <ItemTemplate>
                        <asp:TextBox id="Month9" runat="server"
                            data-col='0' data-row='<%# Container.DataItemIndex %>'  ReadOnly="true" Text='<%# Eval("Month9","{0:N2}")%>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Nov/2011">
                    <ItemTemplate>
                        <asp:TextBox id="Month10" runat="server"
                            data-col='0' data-row='<%# Container.DataItemIndex %>' ReadOnly="true"  Text='<%# Eval("Month10","{0:N2}")%>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Dec/2011">
                    <ItemTemplate>
                        <asp:TextBox id="Month11" runat="server"
                            data-col='0' data-row='<%# Container.DataItemIndex %>' ReadOnly="true"  Text='<%# Eval("Month11","{0:N2}")%>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Jan/2012">
                    <ItemTemplate>
                        <asp:TextBox id="Month12" runat="server"
                            data-col='0' data-row='<%# Container.DataItemIndex %>' ReadOnly="true"  Text='<%# Eval("Month12","{0:N2}")%>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Feb/2012">
                    <ItemTemplate>
                        <asp:TextBox id="Month1" runat="server"
                            data-col='0' data-row='<%# Container.DataItemIndex %>' ReadOnly="true"  Text='<%# Eval("Month1","{0:N2}")%>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="March/2012">
                    <ItemTemplate>
                        <asp:TextBox id="Month2" runat="server"
                            data-col='0' data-row='<%# Container.DataItemIndex %>' ReadOnly="true"  Text='<%# Eval("Month2","{0:N2}")%>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="April/2012">
                    <ItemTemplate>
                        <asp:TextBox id="Month3" runat="server"
                            data-col='0' data-row='<%# Container.DataItemIndex %>'  ReadOnly="true" Text='<%# Eval("Month3","{0:N2}")%>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="May/2012">
                    <ItemTemplate>
                        <asp:TextBox id="Month4" runat="server"
                            data-col='0' data-row='<%# Container.DataItemIndex %>'  ReadOnly="true" Text='<%# Eval("Month4","{0:N2}")%>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="June/2012">
                    <ItemTemplate>
                        <asp:TextBox id="Month5" runat="server"
                            data-col='0' data-row='<%# Container.DataItemIndex %>' ReadOnly="true"  Text='<%# Eval("Month5","{0:N2}")%>' />
                    </ItemTemplate>
                </asp:TemplateField>
<%--
                  <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Delete" HeaderStyle-Width="20px">
                        <ItemTemplate>
                            <asp:CheckBox ID="chkDelete" runat="server"  />
                        </ItemTemplate>
                        <HeaderTemplate>
                            Delete
                            <asp:CheckBox ID="chkSelect" onclick="selectDeselectAll(this)" runat="server" />
                        </HeaderTemplate>
                    </asp:TemplateField>--%>
            </Columns>
            <PagerSettings Mode="NumericFirstLast" NextPageText="Next �" PreviousPageText="� Prev"
                FirstPageText="First" LastPageText="Last" />
            <RowStyle CssClass="odd" />
            <AlternatingRowStyle CssClass="even" />
            <PagerStyle CssClass="defaultPagingBar" />
            <SelectedRowStyle CssClass="selected" />
            <EmptyDataTemplate>
                <strong style='padding-top: 6px; padding-left: 3px'>No CIT. </strong>
            </EmptyDataTemplate>
        </cc2:EmptyDisplayGridView>
        </div>

        <asp:Panel ID="pnlButtons" CssClass="clear buttonsDiv" Style='text-align: right'
            runat="server">
            <asp:Button ID="btnSave" runat="server" Text="Save" Visible="false" CssClass="save" OnClick="btnSave_Click1" />

             <asp:Button ID="btnDelete" runat="server" Text="Delete"  Visible="false"  CssClass="save" OnClick="btnDelete_Click" />
          
        </asp:Panel>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="pageScriptsContent" runat="server">
 <script type="text/javascript">
     function refreshWindow() {
         __doPostBack('<%= this.ClientID %>', 0);
     }
     function selectDeselectAll(chk) {
         $('input[type=checkbox]').each(
                    function (index) {
                        if ($(this).prop('disabled') == false && this.id.indexOf('chkDelete') > 0)
                            this.checked = chk.checked;
                    }
                );
     }

     $(document).ready(function () {
         setMovementToGrid('#<%= gvwOvertimes.ClientID %>');
     });
        
            
    </script>
</asp:Content>