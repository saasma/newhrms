﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using BLL;
using DAL;
using Ext.Net;
using Utils.Web;
using Utils.Helper;
using System.Text;
using Newtonsoft.Json.Linq;
using BLL.BO;

namespace Web.CP
{
    public partial class ProjectPayInput : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.AdministratorTools;
            }
        }     

        CommonManager commonMgr = new CommonManager();

        public bool isNsetDashainAmountImport = true;



        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                Initialise();


            if (Request.Form["__EVENTTARGET"] != null && Request.Form["__EVENTTARGET"].Contains(ddlPayrollPeriods.ClientID))
            {
                BindOvertimes();
            }


            JavascriptHelper.AttachNonDialogPopUpCode(Page, "shiftPopup", "../../ExcelWindow/ProjectExcel.aspx", 450, 500);


            string code = string.Format("var totalHoursOrDayInAMonth = {0}; ", 
                
                CommonManager.CompanySetting.IsProjectInputType ? ProjectManager.TotalHourOrDaysInAMonth : 100);

            Page.ClientScript.RegisterStartupScript(this.GetType(),"sdffd",code,true); 

          
            // programatic client-side calls to __doPostBack() can bypass this
            Page.ClientScript.RegisterOnSubmitStatement(GetType(), "ServerForm",
                                                    "if (this.submitted) return false; this.submitted = true; return true;");

            Page.ClientScript.RegisterClientScriptBlock(GetType(), "sffdsf", string.Format("var IsProjectInputType = {0};", CommonManager.CompanySetting.IsProjectInputType ? "true": "false"),true);
               
        }






        public void Initialise()
        {

          
            txtHours.Text = ProjectManager.TotalHourOrDaysInAMonth.ToString();


            ddlPayrollPeriods.DataSource = CommonManager.GetCurrentYear(SessionManager.CurrentCompanyId);
            ddlPayrollPeriods.DataBind();

            //For Project Percent case
            if (!CommonManager.CompanySetting.IsProjectInputType)
            {
                payrollText.Style["display"] = "none";
                ddlPayrollPeriods.Style["display"] = "none";
                txtHours.Style["display"] = "none";
                btnSave.Style["display"] = "none";
                hoursText.Style["display"] = "none";
            }
            else
                UIHelper.SelectListItem(ddlPayrollPeriods);

            BindOvertimes();

            if ( IsPayrollFinalSaved(int.Parse(ddlPayrollPeriods.SelectedValue)))
            {
                btnExport.Enabled = false;
                //btnExport.ToolTip = Resources.ResourceTooltip.HPLAllowanceAttendanceSaved;
            }
            else
                btnExport.Enabled = true;


            List<PIncome> incomeList = ProjectManager.GetIncomesForProjects();
            // Remove PF income value as it is not working
            for (int i = incomeList.Count - 1; i >= 0; i--)
            {
                if (incomeList[i].IncomeId == 0)
                    incomeList.RemoveAt(i);
            }
            ddlIncomes.DataSource = incomeList;
            ddlIncomes.DataBind();
        }

        Column GetExtGridPanelColumn(string indexId,string headerName)
        {
            Column column = new Column();
            //column.ColumnID = indexId;
            column.DataIndex = indexId;
            column.Text = headerName;
            column.Align = Alignment.Center;
            column.ToolTip = headerName;
            column.MenuDisabled = true;
            column.Sortable = false;
            column.Width = new Unit(120);

            if (headerName == "Total")
                column.Renderer.Fn = "totalRenderer";
            else
            {

                NumberField field = new NumberField();
                field.MinValue = 0;
                column.Editor.Add(field);

                column.Renderer.Fn = "columnRenderer";
            }

            return column;
        }

        public void BindList(int payrollPeroidId)
        {
            List<EEmployee> employees = ProjectManager.GetProjectPaysEmployeeList(payrollPeroidId
                 , int.Parse(ddlIncomes.SelectedValue));
            List<Project> projects = ProjectManager.GetProjectForPays(payrollPeroidId);
            List<ProjectPayBO> projectPays = ProjectManager.GetProjectPays(payrollPeroidId
                , int.Parse(ddlIncomes.SelectedValue));


            // Create Columns for the Projects
            //ArrayReader reader = (storeProjects.Reader[0] as ArrayReader);
            //ColumnModel columnModelProjects = gridProjects.ColumnModel;
            string columnId = "P_{0}";
            foreach (var project in projects)
            {
                string gridIndexId = string.Format(columnId, project.ProjectId);
                ModelField field = new ModelField(gridIndexId, ModelFieldType.Float);
                field.UseNull = true;
                //field. = true;
                ModelProject.Fields.Add(gridIndexId, ModelFieldType.Float);
                // Add Checkbox
                gridProjects.ColumnModel.Columns.Add(GetExtGridPanelColumn(gridIndexId, project.Name));

            }

            ModelProject.Fields.Add("Total", ModelFieldType.Float);
            gridProjects.ColumnModel.Columns.Add(GetExtGridPanelColumn("Total", "Total"));


            // Generate data array & bind to Store
            // Add each  id as key
            Dictionary<string, double?> selection = new Dictionary<string, double?>();
            foreach (var item in projectPays)
            {
                selection.Add(item.EmployeeId + "_" + item.ProjectId, item.HoursOrDays);
            }
            
            object[] data = new object[employees.Count];
            
            // Iterate for each Project & Skip first as it represents All Project
            for (int i = 0; i < employees.Count; i++)
            {
                //first one for EIN, second for Name & last for Total
                object[] rowData = new object[projects.Count + 3];
                double total = 0;
                int columnIndex = 0;


                rowData[columnIndex++] = employees[i].EmployeeId;
                rowData[columnIndex++] = employees[i].Name;
                // Iterate for each Project
                for (int j = 0; j < projects.Count; j++)
                {
                    if (selection.ContainsKey(employees[i].EmployeeId + "_" + projects[j].ProjectId))
                    {
                        rowData[columnIndex++] = selection[employees[i].EmployeeId + "_" + projects[j].ProjectId];
                        total += Convert.ToDouble(rowData[columnIndex - 1]);
                    }
                    else
                    {
                        rowData[columnIndex++] = null;
                    }
                }

                rowData[rowData.Length - 1] = (total);  

                data[i] = rowData;
            }


            storeProjects.DataSource = data;
            storeProjects.DataBind();

      

        }

        public bool IsEnable(object ein)
        {
            if (IsPayrollSaved)
                return false;

            return !IsAttendanceSavedForEmployee(payrollPeriodId, Convert.ToInt32(ein));
        }


        public bool IsPayrollSaved = false;
        public int payrollPeriodId;
        public void BindOvertimes()
        {
            this.IsPayrollSaved = false;
            payrollPeriodId = int.Parse(ddlPayrollPeriods.SelectedValue);
            IsPayrollSaved = IsPayrollFinalSaved(payrollPeriodId);

            //gvwOvertimes.SelectedIndex = -1;
            //gvwOvertimes.DataSource = HPLAllowanceManager.HPL_GetEmployeeForAllowance(payrollPeriodId);
            //gvwOvertimes.DataBind();
            int totalRecords = 0;
            BindList(payrollPeriodId);

            if (IsPayrollSaved)
            {
                btnExport.Visible = false;

                divWarningMsg.InnerHtml = Resources.ResourceTooltip.HPLAllowanceSalaryAlreadyGenerated;
                divWarningMsg.Hide = false;

                
            }
            else
            {
                btnExport.Visible = true;
            }
            //else
            //{
            //    //pnlButtons.Visible = true;

            //    if (LeaveAttendanceManager.IsAttendanceSaved(int.Parse(ddlPayrollPeriods.SelectedValue)))
            //    {
            //        divWarningMsg.InnerHtml = Resources.ResourceTooltip.HPLAllowanceAttendanceSaved;
            //        divWarningMsg.Hide = false;
            //    }
            //}
        }


       

        


        protected void ddlPayrollPeriods_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindOvertimes();
        }

        protected void gvwOvertimes_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            //gvwOvertimes.PageIndex = e.NewPageIndex;
            BindOvertimes();
        }

        

       
        protected void btnSave_Click1(object sender, EventArgs e)
        {
         
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            Page.Validate("setting");
            if (Page.IsValid)
            {
                ProjectManager.UpdateProjectSetting(int.Parse(txtHours.Text.Trim()));
                divMsgCtl.InnerHtml = "Updated hours in a month.";
                divMsgCtl.Hide = false;
            }

            BindOvertimes();
        }

        [DirectMethod]
        public object SaveRow(string EmployeeID, bool isPhantom, JsonObject values)
        {
            var myJsonString = values.ToString();
            var strJObject = JObject.Parse(myJsonString);

            List<Project> projects = ProjectManager.GetProjectsForLatestPayroll();
            List<ProjectPay> projectPays = new List<ProjectPay>();
            foreach (Project item in projects)
            {
                //for (int j = 0; j < projects.Count; j++)
                //{
                //    string key = string.Format("P_{0}", projects[j].ProjectId);
                //}
                if (item.ProjectId != 0)//for all Project value
                {
                    ProjectPay pay = new ProjectPay();
                    pay.EmployeeId = int.Parse(EmployeeID);
                    pay.PayrollPeriodId = int.Parse(ddlPayrollPeriods.SelectedValue);
                    pay.ProjectId = item.ProjectId;
                    string key = string.Format("P_{0}", item.ProjectId);
                    pay.HoursOrDays = double.Parse(values[key].ToString()); ;
                    projectPays.Add(pay);
                }
            }

            try
            {
                ProjectManager.UpdateProjectPay(projectPays, int.Parse(ddlPayrollPeriods.SelectedValue), int.Parse(ddlIncomes.SelectedValue));
                    Notification.Show(new NotificationConfig
                    {
                        Title = "Notification",
                        Icon = Icon.Information,
                        AutoHide = true,
                        Html = "The Information has been updated",
                    });
                    return new { valid = true };
            }
            catch
            {
                return new { valid = false };
            }
        }


        protected void btnSave_DirectClick(object sender, DirectEventArgs e)
        {
            //JSON representation
            string gridJSON = e.ExtraParams["gridItems"];

            //lbl.Text = gridJSON;



            List<Project> projects = ProjectManager.GetProjectsForLatestPayroll();
            //List<PIncome> incomesList = ProjectManager.GetIncomesForProjects();

            JArray jsonArray = Ext.Net.JSON.Deserialize(gridJSON) as JArray;

            double value=0;

            List<ProjectPay> projectPays = new List<ProjectPay>();

            // Skip first row as it represent All Projects
            for (int i = 0; i < jsonArray.Count; i++)
            {
                //Project project = projectList[i];
                JToken row = jsonArray[i];

                int employeeId = int.Parse(row["EmployeeId"].Value<string>());

                for (int j = 0; j < projects.Count; j++)
                {
                    string key = string.Format("P_{0}", projects[j].ProjectId);

                    if (row[key] != null)
                    {

                        if( !double.TryParse ( row[key].ToString(),out value))
                            value= 0;

                        ProjectPay pay = new ProjectPay();
                        pay.EmployeeId = employeeId;
                        pay.PayrollPeriodId = int.Parse(ddlPayrollPeriods.SelectedValue);
                        pay.ProjectId = projects[j].ProjectId;

                            pay.HoursOrDays=value;

                            projectPays.Add(pay);
                    }
                }
            }



            ProjectManager.SaveProjectPay(projectPays
                                                      , int.Parse(ddlPayrollPeriods.SelectedValue), int.Parse(ddlIncomes.SelectedValue));


            if (projectPays.Count > 0)
            {

                X.MessageBox.Show(
                        new MessageBoxConfig
                        {
                            Message = "Project value saved.",
                            Buttons = MessageBox.Button.OK,
                            Title = "Information",
                            Icon = MessageBox.Icon.INFO,
                            MinWidth = 300
                        });

            }
        }
    }

}

