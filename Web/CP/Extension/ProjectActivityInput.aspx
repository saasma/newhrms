<%@ Page MaintainScrollPositionOnPostback="true" Title="Project Activity Input" Language="C#"
    MasterPageFile="~/Master/HROld.Master" AutoEventWireup="true" CodeBehind="ProjectActivityInput.aspx.cs"
    Inherits="Web.CP.ProjectActivityInput" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%--  <script src="../Scripts/jquery.sexy-combo.pack.js" type="text/javascript"></script>

   <script src="../Scripts/jquery.sexy-combo.min.js" type="tet/javascript"></script>

    <script src="../Scripts/jquery.sexy-combo.js" type="text/javascript"></script>--%>
    <script type="text/javascript">

        function ACE_item_selected_ProjectRate(source, eventArgs) {
            var value = eventArgs.get_value();
            if (value != null) 
            <%=hdnFilterEmployee.ClientID %>.setValue(value);
           
        }
        function clearEmp()
        {
            <%=hdnFilterEmployee.ClientID %>.setValue('');
        }

        var blockMultipleFormSubmit = true;

        function refreshWindow() {
          __doPostBack('Reload', 'Reload');

        }

        function isPayrollSelected(btn) {

            var sartdate = <%=txtFromDate.ClientID%>.getRawValue();
            var enddate = <%=txtToDate.ClientID%>.getRawValue();
            var ret = shiftPopup("startdate=" + sartdate + "&enddate=" + enddate);
            return false;
        }

        //        function totalRenderer(e1,e2,e3,e4) {
        //  
        //        }


        var totalRenderer = function (value, meta, record) {
       
            // alert(totalHoursOrDayInAMonth);

            //            if (typeof (totalHoursOrDayInAMonth) == 'undefined')
            //                return value;


            var total = parseFloat(record.data.Total);


            if (parseFloat(total).toFixed(2) != parseFloat(totalHoursOrDayInAMonth)) {
                // meta.css = 'selectCell';

                meta.attr = 'style="border: 1px solid orange;"';

                if (IsProjectInputType)
                    meta.attr += ('title="Total hours in the month is ' + totalHoursOrDayInAMonth + '."');
                else
                    meta.attr += ('title="Total percentage is ' + totalHoursOrDayInAMonth + '."');
            }


            //return value.toFixed(2);
            return value;
        };

        var columnRenderer = function (value, meta, record) {

            if (value == "")
                return "0";
                return value;
            //return value.toFixed(2);
        }


        var afterEdit = function (obj,e,e1,e2,e3) {
      
            /*
            Properties of 'e' include:
            e.grid - This grid
            e.record - The record being edited
            e.field - The field name being edited
            e.value - The value being set
            e.originalValue - The original value for the field, before the edit.
            e.row - The grid row index
            e.column - The grid column index
            */
//            e.record.get('Name')
                        var value = 0;
                        if (e.value != "")
                            value = e.value;
                        e.record.set(e.field, value);
                        e.record.commit();

                        var total = 0;

                        var i = 1;
                        e.grid.columns.forEach( function( col ) {
                        
                        if(i<((e.grid.columns.length)))
                        {
                        
                        var value = e.record.get(col.dataIndex);
                        total += parseFloat(value);
                        }
                        i++;

                        });

                    e.record.set('Total', total);
                    e.record.commit();

        };

    </script>
    <style type="text/css">
        .selectCell
        {
            border: 1px splid orange !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <ext:ResourceManager ID="ResourceManager1" DisableViewState="false" runat="server"
        ShowWarningOnAjaxFailure="false" ScriptMode="Release">
    </ext:ResourceManager>
    <ext:Hidden runat="server" ID="hdnFilterEmployee">
    </ext:Hidden>
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Project Activity Input
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="contentArea">
            <div class="attribute">
                <table class="fieldTable">
                    <tr>
                        <td style="width: 160px">
                            <ext:DateField Width="150px" FieldLabel="From" ID="txtFromDate" runat="server" LabelAlign="Top"
                                LabelSeparator="">
                            </ext:DateField>
                        </td>
                        <td style="width: 160px">
                            <ext:DateField Width="150px" FieldLabel="To" ID="txtToDate" runat="server" LabelAlign="Top"
                                LabelSeparator="">
                            </ext:DateField>
                        </td>
                        <td style="width: 165px; padding-top: 25px">
                            <div class="form form-search">
                                <asp:TextBox ID="txtEmpSearchText" onblur="if(this.value=='') clearEmp();" Width="180px"  placeholder="Search Employee"
                                    runat="server"></asp:TextBox>
                            </div>
                            <cc1:AutoCompleteExtender EnableCaching="true" ID="AutoCompleteExtenderOrganization"
                                runat="server" MinimumPrefixLength="2" ServiceMethod="GetEmployeeNamesWithIDAndINo"
                                ServicePath="~/PayrollService.asmx" TargetControlID="txtEmpSearchText" OnClientItemSelected="ACE_item_selected_ProjectRate"
                                CompletionSetCount="10" CompletionInterval="250" CompletionListCssClass="AutoExtender"
                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
                            </cc1:AutoCompleteExtender>
                        </td>
                        <td>
                            <ext:Button Width="70" ID="btnSearch" runat="server" Text="Search" Height="30" MarginSpec="15 0 0 0"
                                AutoPostBack="true" OnClick="btnSearch_Click">
                                <%-- <DirectEvents>
                                    <Click OnEvent="btnSearch_Click">
                                    </Click>
                                </DirectEvents>--%>
                            </ext:Button>
                        </td>
                        <td style='padding-left: 10px'>
                            <asp:LinkButton ID="btnExport" runat="server" Text="Excel import" OnClientClick="return isPayrollSelected(this)"
                                CssClass=" excel tiptip" Style="float: left; margin-top: 15px" />
                        </td>
                    </tr>
                </table>
            </div>
            <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
            <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
            <div style="clear: both">
            </div>
                
            <ext:GridPanel ID="gridProjects" ClicksToEdit="1" Border="true" StripeRows="true" ForceFit="false"  AutoScroll="true" 
                Header="false" runat="server"   Title="Title" Cls="gridtbl">
                <Store>
                    <ext:Store runat="server" ID="storeProjects">
                        <Reader>
                            <ext:ArrayReader>
                            </ext:ArrayReader>
                        </Reader>
                        <Model>
                            <ext:Model ID="ModelProject" runat="server">
                                <Fields>
                                    <ext:ModelField Name="EmployeeId" Type="Int" />
                                    <ext:ModelField Name="Name" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <Plugins>
                    <ext:CellEditing ID="CellEditing1" ClicksToEdit="1" runat="server">
                        <Listeners>
                            <%--  <BeforeEdit Fn="beforeEdit" />--%>
                            <Edit Fn="afterEdit" />
                        </Listeners>
                    </ext:CellEditing>
                </Plugins>
                <SelectionModel>
                    <ext:CellSelectionModel ID="RowSelectionModel1" runat="server">
                    </ext:CellSelectionModel>
                </SelectionModel>
                <ColumnModel runat="server" ID="columnModelProjects">
                    <Columns>
                     
                        <ext:Column Sortable="false" MenuDisabled="true" Text="EIN" Width="50" ColumnID="EmployeeId" Locked="true"
                            DataIndex="EmployeeId">
                        </ext:Column>
                        <ext:Column Sortable="false" MenuDisabled="true" Text="Name" Width="180" ColumnID="Name" Locked="true"
                            DataIndex="Name">
                        </ext:Column>
                    
                                                
                                               
                    </Columns>
                </ColumnModel>
            </ext:GridPanel>
            
        </div>
        <div cssclass="clear buttonsDiv" id="divButtons" runat="server" style='text-align: left;
            margin-top: 15px; margin-left: 0px'>
            <ext:Button ID="Button2" runat="server" Width="100" Height="30" MarginSpec="15 0 0 0"
                Text="Save">
                <DirectEvents>
                    <Click OnEvent="btnSave_DirectClick">
                        <Confirmation ConfirmRequest="true" Message="Are you sure, you want to save the changes?" />
                        <ExtraParams>
                            <ext:Parameter Name="gridItems" Value="Ext.encode(#{gridProjects}.getRowsValues({ selectedOnly: false }))"
                                Mode="Raw" />
                        </ExtraParams>
                        <%--server side event with mask/loading showing--%>
                        <EventMask ShowMask="true" />
                    </Click>
                </DirectEvents>
                <Listeners>
                    <Click Handler="">
                    </Click>
                </Listeners>
            </ext:Button>
            <ext:Label runat="server" ID="lbl">
            </ext:Label>
        </div>
    </div>
</asp:Content>
