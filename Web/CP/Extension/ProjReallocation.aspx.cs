﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using Utils.Helper;
using DAL;

namespace Web.CP.Extension
{
    public partial class ProjReallocation : System.Web.UI.Page
    {
       
        protected void Page_Load(object sender, EventArgs e)
        {
           
            if (!IsPostBack)
            {
                Initialise();
            }
          
        }

        void Initialise()
        {

          
            List<PIncome> incomes = ProjectManager.GetIncomesForProjects();

            ddlIncomes.DataSource = incomes;
            ddlIncomes.DataBind();

            BindIncomeProjectList();
           

        }

        protected void ddlIncome_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlIncomes.SelectedValue != "")
            {

                List<Project> projects = ProjectManager.GetProjectsAssociatedWithIncome(int.Parse(ddlIncomes.SelectedValue));
               // projects.RemoveAt(0);//remove All Project

                ddlFromProjects.DataSource = projects;
                ddlFromProjects.DataBind();

                ddlToProjects.DataSource = projects;
                ddlToProjects.DataBind();
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {

            if (Page.IsValid)
            {
                if (ddlIncomes.SelectedValue != "-1" && ddlFromProjects.SelectedValue != "-1" && ddlToProjects.SelectedValue != "-1"
                    && ddlFromProjects.SelectedValue != ddlToProjects.SelectedValue)
                {

                    ProjectReallocation entity = new ProjectReallocation();
                    entity.IncomeId = int.Parse(ddlIncomes.SelectedValue);
                    entity.FromProjectId = int.Parse(ddlFromProjects.SelectedValue);
                    entity.ToProjectId = int.Parse(ddlToProjects.SelectedValue);


                    if (ProjectManager.SaveProjectReallocation(entity))
                    {
                        this.msgInfo.InnerHtml = "Project reallocation saved.";
                        this.msgInfo.Hide = false;
                    }

                    BindIncomeProjectList();

                }
               
            }
        }


        void BindIncomeProjectList()
        {
            gvwProjects.DataSource = ProjectManager.GetProjectReallocation();
            gvwProjects.DataBind();
        }

        protected void gvwRoles_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvwProjects.PageIndex = e.NewPageIndex;
            gvwProjects.SelectedIndex = -1;
            BindIncomeProjectList();
           
        }

        protected void gvwRoles_PageIndexChanged(object sender, EventArgs e)
        {
            //gvwRoles.PageIndex = e.NewPageIndex;
            //gvwRoles.SelectedIndex = -1;
          //  BindIncomeProjectList();

        }

        protected void ddlUserType_SelectedIndex(object sender, EventArgs e)
        {
            ////clear & load roles for User type
            //ListItem firstItem = ddlRoles.Items[0];
            //firstItem.Selected = false;
            //ddlRoles.Items.Clear();
            //ddlRoles.ClearSelection();
            ////user type
            //if (!Convert.ToBoolean(ddlUserType.SelectedValue))
            //{
            //    ddlRoles.DataSource = UserManager.GetRoles((int)Role.Employee);
            //    ddlRoles.DataBind();
            //    rowEmployee.Visible = false;
            //}
            //else
            //{
            //    ListItem item = new ListItem(Role.Employee.ToString(), ((int)Role.Employee).ToString());
            //    ddlRoles.Items.Insert(0, item);                
            //    ddlRoles.Items[0].Selected = true;
            //    rowEmployee.Visible = true;
            //}
            //ddlRoles.Items.Insert(0, firstItem);
            //BindUsers();
            //Clear();

        }

        protected void gvwRoles_SelectedIndexChanged(object sender, EventArgs e)
        {
          

        }

        protected void gvwRoles_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            //string userName = gvwRoles.DataKeys[e.RowIndex]["UserName"].ToString();

            int FromProjectId = (int)gvwProjects.DataKeys[e.RowIndex]["FromProjectId"];
            int IncomeId = (int)gvwProjects.DataKeys[e.RowIndex]["IncomeId"];
            int ToProjectId = (int)gvwProjects.DataKeys[e.RowIndex]["ToProjectId"];

            ProjectReallocation entity = new ProjectReallocation();
            entity.IncomeId = IncomeId;
            entity.FromProjectId = FromProjectId;
            entity.ToProjectId = ToProjectId;

            if (ProjectManager.DeleteProjectReallocation(entity))
            {
                msgInfo.InnerHtml = "Project reallocation deleted.";
                msgInfo.Hide = false;

                gvwProjects.SelectedIndex = -1;
                BindIncomeProjectList();
               
            }
          

        }

     
    }
}
