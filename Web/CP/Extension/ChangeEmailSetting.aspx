<%@ Page MaintainScrollPositionOnPostback="true" Title="Change Email Setting" Language="C#"
    MasterPageFile="~/Master/HROld.Master" AutoEventWireup="true" CodeBehind="ChangeEmailSetting.aspx.cs"
    Inherits="Web.CP.ChangeEmailSetting" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <ext:ResourceManager ID="ResourceManager1" DisableViewState="false" runat="server"
        ShowWarningOnAjaxFailure="false" ScriptMode="Release">
    </ext:ResourceManager>
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Change Email Settings
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="contentArea">
            <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
            <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
            From local computer, mail will be sent to @rigonepal.com domain only, so if you want to try for other domain,please
            access the page from remote computer
            <div style="clear: both">
                <table cellspacing="10" class="fieldTable">
                    <tr>
                        <td>
                            <strong>From Email (Can be non existing,
                                <br />
                                like payroll@rigonepal.com)</strong>
                        </td>
                        <td>
                            <asp:TextBox ID="txtFromEmail" Width="200" runat="server" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="user" ControlToValidate="txtFromEmail"
                                Display="None" ErrorMessage="From email is required." runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Host</strong>
                        </td>
                        <td>
                            <asp:TextBox ID="txtHost" Width="200" runat="server" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="user" ControlToValidate="txtHost"
                                Display="None" ErrorMessage="Host is required." runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Port (Default port 25)</strong>
                        </td>
                        <td>
                            <asp:TextBox ID="txtPort" Width="200" runat="server" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="user" ControlToValidate="txtPort"
                                Display="None" ErrorMessage="Port is required." runat="server" />
                            <asp:CompareValidator ID="valComp" runat="server" ValidationGroup="user" Display="None"
                                ControlToValidate="txtPort" ErrorMessage="Must be integer." Type="Integer" Operator="DataTypeCheck" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>User Name (Leave if no authentication reqd)</strong>
                        </td>
                        <td>
                            <asp:TextBox ID="txtUserName" Width="200" runat="server"  />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Password (Leave if no authentication reqd)</strong>
                        </td>
                        <td>
                            <asp:TextBox ID="txtPassword" Width="200" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                           <asp:CheckBox Text="Enable SSL" runat="server" ID="chkIsSSLEnabled" />
                        </td>
                        
                    </tr>
                    <tr>
                        <td>
                           <asp:CheckBox Text="Exclude Server Certificate in case of SSL Enabled" runat="server" ID="chkExcludeServerCertificate" />
                        </td>
                        
                    </tr>
                </table>
            </div>
        </div>
        <div cssclass="clear buttonsDiv" id="divButtons" runat="server" style='margin-left: 10px'>
            <asp:Button ID="btnSave" Visible="false" runat="server" ValidationGroup="save" OnClientClick="valGroup='user';return CheckValidation()"
                Text="Save Changes" OnClick="btnSave_Click" CssClass="btn btn-primary btn-sm btn-sect" />
            <ext:Label runat="server" ID="lbl">
            </ext:Label>
        </div>
        <table cellspacing="10" style='margin-top: 15px' class="fieldTable">
            <tr>
                <td>
                    <strong>Email for Test</strong>
                </td>
                <td>
                    <asp:TextBox ID="txtTestToEmail" Width="200" runat="server" />
                </td>
            </tr>
        </table>
        <asp:Button ID="btnTestMail" runat="server" OnClick="btnTestMail_Click" CssClass="btn btn-warning btn-sect btn-sm"
            Text="Send Test Mail" Style='margin-left: 10px; width: 102px; margin-top: 10px' />
    </div>
</asp:Content>
