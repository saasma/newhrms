﻿<%@ Page Title="Reallocate" Language="C#" MasterPageFile="~/Master/ForPopupPage.Master"
    AutoEventWireup="true" CodeBehind="ProjReallocation.aspx.cs" Inherits="Web.CP.Extension.ProjReallocation" %>

     <%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningMsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="InfoMsgCtl" TagPrefix="uc2" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../../Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>
    <script type="text/javascript">

        function closePopup() {



            window.close();

        }



     


    </script>
    <style type="text/css">
        div.bevel div.fields
        {
            width: 340px !important;
            padding-bottom: 10px !important;
            overflow: hidden;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <div class="popupHeader">
        <h2 class="headlinespop">
            Project Reallocate</h2>
    </div>
    <div style='padding-left: 20px; padding-top: 10px'>
        <table>
            <tr>
                <td colspan="3">
                    Income
                    <asp:DropDownList ID="ddlIncomes" AutoPostBack="true" OnSelectedIndexChanged="ddlIncome_SelectedIndexChanged"
                        runat="server" DataValueField="IncomeId" DataTextField="Title" AppendDataBoundItems="true" >
                        <asp:ListItem Text="Select Income" Value="-1"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    From &nbsp;&nbsp;&nbsp;
                    <asp:DropDownList ID="ddlFromProjects" runat="server" DataValueField="ProjectId"
                        DataTextField="Name"   >
                        <asp:ListItem Text="Select Project" Value="-1"></asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    To
                    <asp:DropDownList ID="ddlToProjects" runat="server" DataValueField="ProjectId" DataTextField="Name">
                        <asp:ListItem Text="Select Project" Value="-1"  ></asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="update" OnClick="btnAdd_Click" />
                </td>
            </tr>
        </table>
          <uc2:InfoMsgCtl Width="450px" ID="msgInfo" EnableViewState="false" Hide="true" runat="server" />
        <uc2:WarningMsgCtl ID="msgWarning" EnableViewState="false" Hide="true" runat="server" />
        <div style='clear:both;padding-top:15px;'></div>

        <cc2:emptydisplaygridview style='clear: both' pagerstyle-horizontalalign="Center"
            pagerstyle-cssclass="defaultPagingBar" cssclass="tableLightColor" useaccessibleheader="true"
            showheaderwhenempty="True" id="gvwProjects" runat="server" datakeynames="IncomeId,FromProjectId,ToProjectId" autogeneratecolumns="False"
            cellpadding="4" gridlines="None" showfooterwhenempty="False" allowpaging="True"
            pagesize="20" onpageindexchanged="gvwRoles_PageIndexChanged" onpageindexchanging="gvwRoles_PageIndexChanging"
            onselectedindexchanged="gvwRoles_SelectedIndexChanged" onrowdeleting="gvwRoles_RowDeleting">
                <Columns>
                    <asp:TemplateField HeaderText="Income" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="150px">
                        <ItemTemplate>  
                            <%# Eval("IncomeText")%></ItemTemplate>
                    </asp:TemplateField>
                  
                   <asp:TemplateField HeaderText="From Project" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="150px">
                        <ItemTemplate>
                            <%# Eval("FromProjectText")%></ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="To Project" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="150px">
                        <ItemTemplate>
                            <%# Eval("ToProjectText")%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderStyle-Width="30px">
                        <ItemTemplate>
                                    <asp:ImageButton ID="ImageButton2" OnClientClick="return confirm('Do you really want to delete the project reallocation?')"
                                runat="server" CommandName="Delete" ImageUrl="~/images/delet.png" />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                    </asp:TemplateField>
                </Columns>
                <PagerSettings Mode="NumericFirstLast" NextPageText="Next »" PreviousPageText="« Prev"
                    FirstPageText="First" LastPageText="Last" />
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                <PagerStyle CssClass="defaultPagingBar" />
                <SelectedRowStyle CssClass="selected" />
                <EmptyDataTemplate>
                    <b>No projects. </b>
                </EmptyDataTemplate>
            </cc2:emptydisplaygridview>
    </div>
</asp:Content>
