﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using BLL;
using DAL;
using Ext.Net;
using Utils.Web;
using Utils.Helper;
using System.Text;
using Newtonsoft.Json.Linq;
using BLL.BO;
using System.Web.Configuration;
using System.Net.Configuration;
using System.Net.Mail;
using Utils;
using System.Net;

namespace Web.CP
{
    public partial class ChangeEmailSetting : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.AdministratorTools;
            }
        }

        CommonManager commonMgr = new CommonManager();

        public bool isNsetDashainAmountImport = true;



        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                Initialise();





        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

            if (Page.IsValid)
            {

                var config = WebConfigurationManager.OpenWebConfiguration(HttpContext.Current.Request.ApplicationPath);
                var settings = config.GetSectionGroup("system.net/mailSettings") as MailSettingsSectionGroup;

                settings.Smtp.From = txtFromEmail.Text.Trim();
                settings.Smtp.Network.Host = txtHost.Text.Trim();
                settings.Smtp.Network.Port = int.Parse(txtPort.Text.Trim());
                settings.Smtp.Network.UserName = txtUserName.Text.Trim();
                settings.Smtp.Network.Password = txtPassword.Text.Trim();


                config.Save(System.Configuration.ConfigurationSaveMode.Modified);
                divMsgCtl.InnerHtml = "Information saved.";
                divMsgCtl.Hide = false;
            }
        }

        protected void btnTestMail_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {

                try
                {
                    SmtpClient smtpClient = new SmtpClient(txtHost.Text.Trim(), int.Parse(txtPort.Text));

                    // if has autentication
                    if (!string.IsNullOrEmpty(txtUserName.Text))
                    {
                        smtpClient.UseDefaultCredentials = false;
                        smtpClient.Credentials = new NetworkCredential(txtUserName.Text.Trim(), txtPassword.Text.Trim());
                    }

                    if (chkIsSSLEnabled.Checked)
                    {
                        smtpClient.EnableSsl = true;


                        smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;

                        if (chkExcludeServerCertificate.Checked == false)
                        {
                            //Add this line to bypass the certificate validation
                            System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate (object s,
                                    System.Security.Cryptography.X509Certificates.X509Certificate certificate,
                                    System.Security.Cryptography.X509Certificates.X509Chain chain,
                                    System.Net.Security.SslPolicyErrors sslPolicyErrors)
                            {
                                return true;
                            };
                        }

                    }

                    string testMail = "Test Mail";

                    //MailAddress from = new MailAddress(Config.NoReplyEmail, subject);
                    MailAddress from = new MailAddress(txtFromEmail.Text, testMail);
                    using (MailMessage mailMessage = new MailMessage(from, new MailAddress(txtTestToEmail.Text)))//, subject, body))
                    {

                        //mailMessage.Bcc.Add("sailendra@rigonepal.com,sailendra@rigonepal.com");

                        mailMessage.IsBodyHtml = true;

                        mailMessage.Subject = testMail;
                        mailMessage.Body = testMail;

                        smtpClient.Send(mailMessage);

                    }



                    divMsgCtl.InnerHtml = "Sent mail successfully.";
                    divMsgCtl.Hide = false;
                }
                catch (Exception exp)
                {
                    // here must be ToString to identity email sending error
                    divWarningMsg.InnerHtml = exp.ToString(); ;
                    divWarningMsg.Hide = false;
                    //return false;
                    // MarketBrick.Helpers.ErrorLogger.LogError(exp, "Email setup error.");
                    //Log.log("Error mail sending to : " + to, exp);
                }
                finally
                {
                    //excelAttachment.Close();
                }



            }
        }

        public void Initialise()
        {

            //Load Content Type

            var config = WebConfigurationManager.OpenWebConfiguration(HttpContext.Current.Request.ApplicationPath);
            var settings = config.GetSectionGroup("system.net/mailSettings") as MailSettingsSectionGroup;

            txtFromEmail.Text = settings.Smtp.From;
            txtHost.Text = settings.Smtp.Network.Host;
            txtPort.Text = settings.Smtp.Network.Port.ToString();
            txtUserName.Text = settings.Smtp.Network.UserName;
            txtPassword.Text = settings.Smtp.Network.Password;

            if (!string.IsNullOrEmpty(Config.EnableSsl))
            {
                if (bool.Parse(Config.EnableSsl.ToString()))
                    chkIsSSLEnabled.Checked = true;

            }


        }

    }
}

