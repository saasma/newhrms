<%@ Page MaintainScrollPositionOnPostback="true" Title="Mail Merge" Language="C#"
    MasterPageFile="~/Master/NewDetails.Master" AutoEventWireup="true" ValidateRequest="false"
    CodeBehind="MailMerge.aspx.cs" Inherits="Web.CP.MailMerge" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .x-toolbar
        {
            padding: inherit;
        }
        .wrap
        {
            overflow-y: inherit !important;
        }
    </style>
    <script type="text/javascript">
        var refreshWindow = function () {
        <%=btnRefresh.ClientID %>.fireEvent('click');
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <%--<ext:ResourceManager ID="ResourceManager1" DisableViewState="false" runat="server"
        ShowWarningOnAjaxFailure="false" ScriptMode="Release">
    </ext:ResourceManager>--%>
    <ext:LinkButton runat="server" StyleSpec="padding:0px;" Cls="btnFlatLeftGap" ID="btnRefresh"
        Hidden="true" Text="<i></i>Cancel" runat="server">
        <DirectEvents>
            <Click OnEvent="btnRefresh_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
       <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                     Mail Merge
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
       
        <%-- <div class="attribute">
            <table>
                <tr>
                    <td style='width: 170px'>
                        <strong runat="server" id="payrollText">Content Type</strong>
                        <asp:DropDownList AutoPostBack="true" ID="ddlPayrollPeriods" DataTextField="Text"
                            DataValueField="Value" AppendDataBoundItems="true" Width="150px" runat="server"
                            OnSelectedIndexChanged="ddlPayrollPeriods_SelectedIndexChanged">
                            <asp:ListItem Value="0" Text=""></asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator InitialValue="0" Display="None" ControlToValidate="ddlPayrollPeriods"
                            ValidationGroup="Overtime" ID="RequiredFieldValidator2" runat="server" ErrorMessage="Content Type is required."></asp:RequiredFieldValidator>
                    </td>
                </tr>
            </table>
        </div>--%>
        <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
        <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />

        <div style="clear: both;">
            <table>
                <tr>
                    <td style="width:948px;"></td>
                    <td style="width:100px;">
                        <ext:Button ID="btnHistory" runat="server" Text="History">
                                             <DirectEvents>
                                                <Click OnEvent="btnHistory_Click">
                                                    <EventMask ShowMask="true" />
                                                </Click>
                                            </DirectEvents>
                                            <%--<Listeners>
                                                <Click Handler="#{Window1}.show();">
                                                </Click>
                                            </Listeners>--%>
                                        </ext:Button>
                    </td>
                    <td>
                        <ext:Button ID="btnImport" runat="server" Text="Import">
                                            <DirectEvents>
                                                <Click OnEvent="btnImport_Click">
                                                </Click>
                                            </DirectEvents>
                                        </ext:Button>
                    </td>
                </tr>
            </table>
        </div>


        <div style="clear: both">
            <table>
                <tr>
                    <td>

                        <table cellspacing="10" class="">
                            <tbody>
                                <tr>
                                    <td style="padding-top: 10px; width:110px;">
                                        <strong>Sender's Name</strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: top">
                                        <ext:TextField ID="txtFromName" runat="server" Width="500" />
                                    </td>
                                </tr>

                                <tr>
                                     <td style="padding-top: 10px">
                                        <strong>Sender's Email Address</strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: top">
                                        <ext:TextField ID="txtFromEmail" runat="server" Width="500" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-top: 10px">
                                        <strong>E-Mail Subject Line</strong>
                                    </td>
                                </tr>
                                <tr>
                                      
                                    <td style="vertical-align: top">
                                        <ext:TextField ID="txtSubject" runat="server" Width="500" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-top: 10px; vertical-align: top">
                                        <strong>E-Mail Message</strong>
                                    </td>
                                </tr>
                                <tr>                                    
                                    <td>
                                        <ext:HtmlEditor FieldBodyCls="htmlBody" Height="300" Width="500" ID="HtmlEditor1"
                                            runat="server" />
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                       
                    </td>
                    <td valign="top">
                        <table class="fieldTable">
                            <tr>
                                <td>
                                    <ext:GridPanel StyleSpec="margin-top:11px;" ID="GridPanel1" runat="server" Cls="itemgrid"
                                        Width="160" ClicksToEdit="1">
                                        <Store>
                                            <ext:Store ID="storeTemplateHeader" runat="server" AutoLoad="true">
                                                <Model>
                                                    <ext:Model ID="Model1" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="HeaderText" Type="string" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <Plugins>
                                            <ext:CellEditing ID="CellEditing1" ClicksToEdit="1" runat="server">
                                                <Listeners>
                                                </Listeners>
                                            </ext:CellEditing>
                                        </Plugins>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:Column ID="Column_Score" TdCls="wrap" runat="server" Text="Custom Fields" Sortable="false"
                                                    MenuDisabled="true" Width="150" DataIndex="HeaderText" Border="false">
                                                    <Editor>
                                                        <ext:TextField ID="txtHeader" runat="server" />
                                                    </Editor>
                                                </ext:Column>
                                            </Columns>
                                        </ColumnModel>
                                        <SelectionModel>
                                            <ext:CellSelectionModel ID="CellSelectionModel1" runat="server" Mode="Single" />
                                        </SelectionModel>
                                    </ext:GridPanel>
                                </td>
                            </tr>
                            <tr>
                                <td valign="bottom">
                                    <div class="popupButtonDiv">
                                        <ext:Button runat="server" ID="LinkButton2" Cls="btn btn-primary" StyleSpec="margin-top:10px" Height="30" Text="<i></i>Save" runat="server">
                                            <DirectEvents>
                                                <Click OnEvent="btnDetailSaveUpdate_Click">
                                                    <EventMask ShowMask="true" />
                                                    <ExtraParams>
                                                        <ext:Parameter Name="Values" Value="Ext.encode(#{GridPanel1}.getRowsValues({selectedOnly : false}))"
                                                            Mode="Raw" />
                                                    </ExtraParams>
                                                </Click>
                                            </DirectEvents>
                                        </ext:Button>
                                        <%--<div class="btnFlatOr">
                                    or</div>--%>
                                        <ext:LinkButton runat="server" StyleSpec="padding:0px;" Cls="btnFlatLeftGap" ID="LinkButton3"
                                            Hidden="true" Text="<i></i>Cancel" runat="server">
                                            <Listeners>
                                                <Click Handler="#{Window1}.hide();">
                                                </Click>
                                            </Listeners>
                                        </ext:LinkButton>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td valign="top">
                        <table class="fieldTable">
                            <tr>
                                <td>
                                    <ext:GridPanel ID="GridPanelEmployee" runat="server" StyleSpec="margin-top:11px;" 
                                        Width="420" Height="375" ClicksToEdit="1">
                                        <Store>
                                            <ext:Store ID="storeEmployee" runat="server" AutoLoad="true">
                                                <Model>
                                                    <ext:Model ID="Model3" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="SN" Type="string" />
                                                            <ext:ModelField Name="EmployeeID" Type="string" />
                                                            <ext:ModelField Name="EmployeeName" Type="string" />
                                                            <ext:ModelField Name="Email" Type="string" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:Column ID="Column6" runat="server" Text="SN" Sortable="false" MenuDisabled="true"
                                                    Width="50" DataIndex="SN" Border="false">
                                                </ext:Column>
                                                <ext:Column ID="Column7" runat="server" Text="EIN" Sortable="false" MenuDisabled="true"
                                                    Width="50" Align="Right" DataIndex="EmployeeID" Border="false">
                                                </ext:Column>
                                                <ext:Column ID="Column5" runat="server" Text="Employee Name" Sortable="false" MenuDisabled="true"
                                                    Width="180" DataIndex="EmployeeName" Border="false">
                                                </ext:Column>
                                                <ext:Column ID="Column8" runat="server" Text="Email" Sortable="false" MenuDisabled="true"
                                                    Width="100" DataIndex="Email" Border="false">
                                                </ext:Column>
                                            </Columns>
                                        </ColumnModel>
                                        <SelectionModel>
                                            <ext:CellSelectionModel ID="CellSelectionModel3" runat="server" Mode="Single" />
                                        </SelectionModel>
                                    </ext:GridPanel>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="buttonsDiv" id="divButtons" runat="server" style="text-align: left; margin-top: 15px; margin-left:10px;">
        <%-- <ext:LinkButton runat="server" StyleSpec="padding:0px;
                                " ID="LinkButton1" Cls="btnFlat" BaseCls="btnFlat" Text="<i></i>Send" runat="server">
                                            <DirectEvents>
                                                <Click OnEvent="btnDetailSaveUpdate_Click">
                                                    <EventMask ShowMask="true" />
                                                    <ExtraParams>
                                                        <ext:Parameter Name="Values" Value="Ext.encode(#{GridPanel1}.getRowsValues({selectedOnly : false}))"
                                                            Mode="Raw" />
                                                    </ExtraParams>
                                                </Click>
                                            </DirectEvents>
                                        </ext:LinkButton>--%>
        <ext:Button ID="btnSave" Cls="btn btn-primary" runat="server" StyleSpec="margin-top:10px" Height="30" Text="<i></i>Send" runat="server">
            <DirectEvents>
                <Click OnEvent="btnSave_Click" Timeout="99999999">
                    <Confirmation ConfirmRequest="true" Message="Confirm send the message?" />
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
            <Listeners>
                <Click Handler="">
                </Click>
            </Listeners>
        </ext:Button>
        <ext:Label runat="server" ID="lbl">
        </ext:Label>
    </div>
    <ext:Window ID="Window1" runat="server" AutoScroll="true" Title="History" Icon="Application"
        Width="740" Height="600" BodyPadding="5" Hidden="true" Modal="true">
        <Content>
            <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridHistory" runat="server" Cls="itemgrid"
                Width="640" ClicksToEdit="1">
                <Store>
                    <ext:Store ID="storeHistory" runat="server" AutoLoad="true">
                        <Model>
                            <ext:Model ID="Model2" runat="server">
                                <Fields>
                                    <ext:ModelField Name="EmployeeName" Type="String" />
                                    <ext:ModelField Name="Subject" Type="string" />
                                    <ext:ModelField Name="SentDate" Type="Date" />
                                    <ext:ModelField Name="CreatedByName" Type="string" />
                                    <ext:ModelField Name="ToAddress" Type="string" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <Plugins>
                    <ext:CellEditing ID="CellEditing2" ClicksToEdit="1" runat="server">
                        <Listeners>
                        </Listeners>
                    </ext:CellEditing>
                </Plugins>
                <ColumnModel>
                    <Columns>
                         <ext:Column ID="Column9" TdCls="wrap" runat="server" Text="Employee" Sortable="false"
                            MenuDisabled="true" Width="150" DataIndex="EmployeeName" Border="false">
                        </ext:Column>
                        <ext:Column ID="Column1" TdCls="wrap" runat="server" Text="Subject" Sortable="false"
                            MenuDisabled="true" Width="150" DataIndex="Subject" Border="false">
                        </ext:Column>
                        <ext:Column ID="Column2" TdCls="wrap" runat="server" Text="ToAddress" Sortable="false"
                            MenuDisabled="true" Width="150" DataIndex="ToAddress" Border="false">
                        </ext:Column>
                        <ext:Column ID="Column3" TdCls="wrap" runat="server" Text="Sent By" Sortable="false"
                            MenuDisabled="true" Width="100" DataIndex="CreatedByName" Border="false">
                        </ext:Column>
                        <ext:DateColumn ID="Column4" TdCls="wrap" runat="server" Text="SentDate" Sortable="false"
                            Format="d-m-Y" MenuDisabled="true" Width="100" DataIndex="SentDate" Border="false">
                        </ext:DateColumn>
                    </Columns>
                </ColumnModel>
                <SelectionModel>
                    <ext:CellSelectionModel ID="CellSelectionModel2" runat="server" Mode="Single" />
                </SelectionModel>
            </ext:GridPanel>
        </Content>
    </ext:Window>
</asp:Content>
