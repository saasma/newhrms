﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using BLL;
using DAL;
using Ext.Net;
using Utils.Web;
using Utils.Helper;
using System.Text;
using Newtonsoft.Json.Linq;
using BLL.BO;

namespace Web.CP
{
    public partial class ManageLetter : BasePage
    {
        CommonManager commonMgr = new CommonManager();

        public bool isNsetDashainAmountImport = true;



        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                Initialise();




           

        }


        public void cboBranch_Select(object sender, DirectEventArgs e)
        {
            if (cmbContentType.SelectedItem != null)
            {
                int type = int.Parse(cmbContentType.SelectedItem.Value);
                EmailContent content = CommonManager.GetMailContent(type);
                if (content != null)
                {
                    txtSubject.Text = content.Subject;
                    //HtmlEditor1.Text = content.Body;
                }
            }
        }


        public void btnSave_Click(object sender, DirectEventArgs e)
        {
            if (cmbContentType.SelectedItem != null)
            {
                int type = int.Parse(cmbContentType.SelectedItem.Value);
                EmailContent content = new EmailContent();
                content.ContentType = type;
                content.CompayId = SessionManager.CurrentCompanyId;
                content.Subject = txtSubject.Text.Trim();
                //content.Body = HtmlEditor1.Text.Trim();

                CommonManager.SaveOrUpdateEmailContent(content);
                X.Msg.Show(new MessageBoxConfig { Icon = MessageBox.Icon.INFO, Title = "Message", Message = "Data saved successfully.", Buttons = MessageBox.Button.OK });

            }
        }

        void CustomizeEditor()
        {

            //CKEditor.config.toolbar = new object[]
            //{
            //    new object[] { "Source", "-", "Save", "NewPage", "Preview", "-", "Templates" },
            //    new object[] { "Cut", "Copy", "Paste", "PasteText", "PasteFromWord", "-", "Print", "SpellChecker", "Scayt" },
            //    new object[] { "Undo", "Redo", "-", "Find", "Replace", "-", "SelectAll", "RemoveFormat" },
            //    new object[] { "Form", "Checkbox", "Radio", "TextField", "Textarea", "Select", "Button", "ImageButton", "HiddenField" },
            //    "/",
            //    new object[] { "Bold", "Italic", "Underline", "Strike", "-", "Subscript", "Superscript" },
            //    new object[] { "NumberedList", "BulletedList", "-", "Outdent", "Indent", "Blockquote", "CreateDiv" },
            //    new object[] { "JustifyLeft", "JustifyCenter", "JustifyRight", "JustifyBlock" },
            //    new object[] { "BidiLtr", "BidiRtl" },
            //    new object[] { "Link", "Unlink", "Anchor" },
            //    new object[] { "Image", "Flash", "Table", "HorizontalRule", "Smiley", "SpecialChar", "PageBreak", "Iframe" },
            //    "/",
            //    new object[] { "Styles", "Format", "Font", "FontSize" },
            //    new object[] { "TextColor", "BGColor" },
            //    new object[] { "Maximize", "ShowBlocks", "-", "About" },
            //    "/",
            //    new object[] {"language","Save"}
            //};
        }
        public void Initialise()
        {
            CustomizeEditor();

            // Day type Leave request & approval
            if (CommonManager.CalculationConstant.CompanyHasHourlyLeave == false)
            {
                cmbContentType.Items.Add(new Ext.Net.ListItem { Text = "Leave Request Content", 
                    Value =  ((int)EmailContentType.DayTypeLeaveRequest).ToString() });

                cmbContentType.Items.Add(new Ext.Net.ListItem
                {
                    Text = "Leave Approval Content",
                    Value = ((int)EmailContentType.DayTypeLeaveApproval).ToString()
                });
            }

        }


    }

}

