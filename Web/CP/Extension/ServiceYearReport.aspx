<%@ Page Title="Service Year Report" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="ServiceYearReport.aspx.cs" Inherits="Web.ServiceYearReport" %>

<%@ Import Namespace="BLL.Manager" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/ContentHeader.ascx" TagName="ContentHeader" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../../Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        var loadingButtonWhenSubmitting = false;
        var skipLoadingCheck = true;
        function ChangeGrade(employeeId) {
            positionHistoryPopup("isPopup=true&EIN=" + employeeId);
        }
       

    </script>
    <style type="text/css">
        .tableLightColor a:hover
        {
            color: #048FC2;
            text-decoration: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <uc1:ContentHeader runat="server" Id="contentHeader" />
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Check Service Period
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="contentArea">
            <div class="attribute" style="padding:10px">
                <table class="fieldTable">
                    <tr>    
                        <td>
                            
                         
                        </td>
                        <td>
                            <strong>Date Range To *</strong>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                            <strong>Count Period From</strong>
                        </td>
                        <td>
                            <strong>Branch</strong>
                        </td>
                        <td>
                            <strong>Department</strong>
                        </td>
                        <td>
                            Service Period
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                           <asp:CheckBox runat="server" ID="chkEnglishDate" Text="Use English Date" AutoPostBack="true" />
                        </td>
                        <td>
                            <pr:CalendarExtControl ID="calFrom" runat="server" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="calFrom"
                                Display="None" ErrorMessage="From date is required." ValidationGroup="AEEmployee"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            to
                        </td>
                        <td>
                            <pr:CalendarExtControl ID="calTo" runat="server" />
                            <asp:RequiredFieldValidator ID="valReqdStatus" runat="server" ControlToValidate="calTo"
                                Display="None" ErrorMessage="To date is required." ValidationGroup="AEEmployee"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlDateFrom" DataTextField="Value" DataValueField="Key" AppendDataBoundItems="true"
                                Width="130px" runat="server">
                                <%--  <asp:ListItem Value="-1" Text="Joining"></asp:ListItem>
                            <asp:ListItem Value="-2" Text="Latest Grade Change"></asp:ListItem>--%>
                                <%-- <asp:ListItem Value="2" Text="Permanent"></asp:ListItem>--%>
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:DropDownList AutoPostBack="true" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged"
                                ID="ddlBranch" DataTextField="Name" DataValueField="BranchId" AppendDataBoundItems="true"
                                Width="130px" runat="server">
                                <asp:ListItem Value="-1" Text=""></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlDepartment" DataTextField="Name" DataValueField="DepartmentId"
                                runat="server" AppendDataBoundItems="true" Width="130px">
                                <asp:ListItem Value="-1" Text=""></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlType" runat="server">
                                <asp:ListItem Value="">Day</asp:ListItem>
                                <asp:ListItem Value="true" Selected="True">Month</asp:ListItem>
                                <asp:ListItem Value="false">Year</asp:ListItem>
                            </asp:DropDownList>
                            <asp:TextBox Width="60" ID="txtDayOrYear" runat="server" />
                            <asp:CompareValidator Operator="GreaterThanEqual" ValueToCompare="0" ValidationGroup="AEEmployee"
                                Type="Double" ID="PFCompVal222" Display="None" ControlToValidate="txtDayOrYear"
                                runat="server" ErrorMessage="Invalid value."></asp:CompareValidator>
                        </td>
                        <td>
                        </td>
                        <td valign="top">
                            <asp:Button ID="btnLoad" CssClass="btn btn-default btn-sect btn-sm" style="width:80px" OnClientClick="valGroup='AEEmployee';return CheckValidation();"
                                OnClick="btnLoad_Click" runat="server" Text="Load" />
                        </td>
                    </tr>
                </table>
                <%--</ContentTemplate>
            </asp:UpdatePanel>--%>
            </div>
            <div class="clear">
                <cc2:EmptyDisplayGridView CssClass="table table-primary mb30 table-bordered table-hover" UseAccessibleHeader="true" ShowHeaderWhenEmpty="True"
                    ID="gvw" runat="server" DataKeyNames="" AutoGenerateColumns="False" CellPadding="4"
                    GridLines="None" ShowFooterWhenEmpty="False" OnRowCreated="gvwEmployees_RowCreated">
<%--                    <RowStyle BackColor="#E3EAEB" />--%>
                    <Columns>
                        <asp:BoundField HeaderStyle-Width="20px" DataField="EIN" HeaderText="EIN"></asp:BoundField>
                        <asp:TemplateField HeaderText="Name" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:HyperLink CssClass="employeeClass" Target="_blank" Width="150px" ID="Label2"
                                    runat="server" NavigateUrl='javascript:void(0)' Text='<%# Eval("Name") %>'></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--<asp:BoundField DataField="Name" HeaderStyle-Width="180px" HeaderStyle-HorizontalAlign="Left" HeaderText="Name"></asp:BoundField>--%>
                        <asp:BoundField DataField="Branch" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left"
                            HeaderText="Branch"></asp:BoundField>
                        <asp:BoundField DataField="Department" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left"
                            HeaderText="Department"></asp:BoundField>
                        <asp:TemplateField HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left" HeaderText="Status">
                            <ItemTemplate>
                                <%#   DAL.JobStatus.GetStatusForDisplay( Eval("Status"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="JoinDate" HeaderStyle-Width="90px" HeaderStyle-HorizontalAlign="Left"
                            HeaderText="Join Date(BS)"></asp:BoundField>
                        <asp:BoundField DataField="JoinDateEng" DataFormatString="{0:d}" HeaderStyle-Width="90px"
                            HeaderStyle-HorizontalAlign="Left" HeaderText="Join Date(AD)"></asp:BoundField>
                        <asp:BoundField DataField="StatusOrJoinDate" DataFormatString="{0:d}" HeaderStyle-Width="90px"
                            HeaderStyle-HorizontalAlign="Left" HeaderText="Period Start Date"></asp:BoundField>
                        <asp:BoundField DataField="CompletionDateEng" DataFormatString="{0:d}" HeaderStyle-Width="90px"
                            HeaderStyle-HorizontalAlign="Left" HeaderText="Period Completion Date"></asp:BoundField>
                        <asp:BoundField DataField="ServicePeriod" HeaderStyle-Width="120" HeaderStyle-HorizontalAlign="Left"
                            HeaderText="Service Period"></asp:BoundField>
                    </Columns>
                    <RowStyle CssClass="odd" />
                    <AlternatingRowStyle CssClass="even" />
                    <EmptyDataTemplate>
                        <b>No list. </b>
                    </EmptyDataTemplate>
                </cc2:EmptyDisplayGridView>
            </div>
            <div class="buttonsDivSect">
                <asp:Button ID="btnExport" CssClass="btn btn-info btn-sm btn-sect" OnClick="btnExport_Click" Visible="true"
                    CausesValidation="false" runat="server" Text="Export to Excel" />
            </div>
        </div>
</asp:Content>
