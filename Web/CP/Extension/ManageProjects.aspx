<%@ Page Title="Manage Projects" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="ManageProjects.aspx.cs" Inherits="Web.User.ManageProjects" %>

<%@ Register Src="~/NewHR/UserControls/ManageProjectsCtrl.ascx" TagName="ManagePrj" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">

<uc2:ManagePrj ID="ManagePrjCtrl" runat="server" />

</asp:Content>
