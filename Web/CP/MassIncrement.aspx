<%@ Page Title="Mass Increment" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="MassIncrement.aspx.cs" Inherits="Web.CP.MassIncrement" %>

<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/CalendarCtl.ascx" TagPrefix="uc" TagName="Calendar" %>
<%@ Register Src="../Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .increment
        {
        }
        
        .table thead a:hover
        {
            color: #000;
        }
        
        table td
        {
            padding-top: 4px;
        }
    </style>
    <script type="text/javascript">
        var incomeTypeIsRate = false;
        function refreshWindow() {
            __doPostBack('Reload', '');
        }

        function refreshLoadButton()
        {
        
             __doPostBack('LoadButton', '');
        }
        function exportimportPopupProcess(btn) {

            var ret = exportimportPopup("incomeid=" + <%= ddlIncome.ClientID %>.value 
                + "&status=" +  <%= ddlStatus.ClientID %>.value
                + "&designationId=" +  <%= ddlDesignation.ClientID %>.value
                + "&groupId=" +  <%= ddlGroups.ClientID %>.value
                + "&levelId=" +  <%= ddlLevel.ClientID %>.value
                + "&eventId=" +  <%= ddlEvents.ClientID %>.value
                + "&date=" +  getCalendarSelectedDate('<%= Calendar1.ClientID %>')
                );
        }
        function importPopupProcess(btn) {




            var ret = importPopup();


            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <asp:HiddenField ID="hdIncreaseType" runat="server" />
    <asp:HiddenField ID="hdIncomeType" runat="server" />
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Mass Increment
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="attribute" style="padding: 10px">
            <table class='fieldTable'>
                <tr>
                    <td>
                        <strong>Select Income *</strong>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlIncome" AutoPostBack="true" AppendDataBoundItems="true"
                            DataTextField="Title" DataValueField="IncomeId" runat="server" Width="180px"
                            OnSelectedIndexChanged="ddlIncome_SelectedIndexChanged">
                            <asp:ListItem Text="--Select Income--" Value="-1" Selected="True" />
                        </asp:DropDownList>
                    </td>
                    <td>
                        <strong>Employee</strong>
                    </td>
                    <td>
                        <asp:TextBox AutoPostBack="true" Width="150px" ID="txtEmpSearchText" runat="server"
                            OnTextChanged="btnLoad_Click"></asp:TextBox>
                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtEmpSearchText"
                            WatermarkText="Employee Name" WatermarkCssClass="searchBoxText" />
                        <cc1:AutoCompleteExtender EnableCaching="true" ID="AutoCompleteExtenderOrganization"
                            runat="server" MinimumPrefixLength="2" ServiceMethod="GetEmployeeNames" ServicePath="~/PayrollService.asmx"
                            TargetControlID="txtEmpSearchText" CompletionSetCount="10" CompletionInterval="250"
                            CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList"
                            CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
                        </cc1:AutoCompleteExtender>
                    </td>
                    <td style="padding-left: 10px; display: none">
                        <strong>Select Grade</strong>
                    </td>
                    <td style="display: none">
                        <asp:DropDownList ID="ddlGrade" AutoPostBack="true" AppendDataBoundItems="true" DataTextField="Name"
                            DataValueField="GradeId" runat="server" Width="150px" OnSelectedIndexChanged="ddlGrade_SelectedIndexChanged">
                            <asp:ListItem Text="--Select Grade--" Value="-1" Selected="True" />
                        </asp:DropDownList>
                    </td>
                    <td style="padding-left: 10px; display: none">
                        <strong>Select Department</strong>
                    </td>
                    <td style="padding-left: 10px; display: none">
                        <asp:DropDownList ID="ddlDepartments" AutoPostBack="true" AppendDataBoundItems="true"
                            DataTextField="Name" DataValueField="DepartmentId" runat="server" Width="150px"
                            OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged">
                            <asp:ListItem Text="--Select Department--" Value="-1" Selected="True" />
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>Select Status</strong>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlStatus" AutoPostBack="true" AppendDataBoundItems="true"
                            DataTextField="Value" DataValueField="Key" runat="server" Width="180px" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged">
                            <asp:ListItem Text="--Select Status--" Value="-1" Selected="True" />
                        </asp:DropDownList>
                    </td>
                    <td>
                        <strong>Select Group</strong>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlGroups" Width="150px" DataTextField="Name" DataValueField="LevelGroupId"
                            runat="server" AppendDataBoundItems="true">
                            <asp:ListItem Text="--Select--" Value="-1" />
                        </asp:DropDownList>
                    </td>
                    <td style="padding-left:10px;">
                        <strong>Select Level/Position</strong>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlLevel" Width="150px" DataTextField="GroupLevel" DataValueField="LevelId"
                            runat="server" AppendDataBoundItems="true">
                            <asp:ListItem Text="--Select--" Value="-1" />
                        </asp:DropDownList>
                    </td>
                    <td style="padding-left:10px;">
                        <strong>Select Designation</strong>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlDesignation" AutoPostBack="true" AppendDataBoundItems="true"
                            DataTextField="CodeAndName" DataValueField="DesignationId" runat="server" Width="150px"
                            OnSelectedIndexChanged="ddlDesignation_SelectedIndexChanged">
                            <asp:ListItem Text="--Select Designation--" Value="-1" Selected="True" />
                        </asp:DropDownList>
                    </td>
                    <td style="padding-left: 10px; display: none">
                        <strong>Filter</strong>
                    </td>
                    <td style="display: none">
                        <asp:DropDownList ID="ddlFilterIncomes" AppendDataBoundItems="true" DataTextField="Title"
                            DataValueField="IncomeId" runat="server" Width="170px">
                            <asp:ListItem Text="--Select Income--" Value="-1" Selected="True" />
                        </asp:DropDownList>
                    </td>
                    <td style="padding-left: 10px; display: none">
                        <asp:DropDownList ID="ddlFilter" AppendDataBoundItems="true" runat="server" Width="150px">
                            <asp:ListItem Text="--Select Filter--" Value="-1" Selected="True" />
                            <asp:ListItem Text="Less Than Equals" Value="1" />
                            <asp:ListItem Text="Greater Than Equals" Value="2" />
                        </asp:DropDownList>
                    </td>
                    <td style="padding-left: 10px; display: none">
                        <asp:TextBox ID="txtFilter" runat="server" Width="150px" />
                        <asp:CompareValidator ID="compFilter" runat="server" Display="None" ErrorMessage="Invalid amount."
                            Text="" ControlToValidate="txtFilter" ValidationGroup="value1" Type="Currency"
                            Operator="DataTypeCheck" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>Effective Date *</strong>
                    </td>
                    <td>
                        <uc:Calendar Id="Calendar1" runat="server" />
                    </td>
                    <td>
                    </td>
                    <td>
                        <asp:Button ID="btnUniformIncrease" CssClass="btn btn-default btn-sm" Style="height: 30px;
                            cursor: pointer" runat="server" Text="Uniform Increase" Width="150px" OnClientClick="CallPopUp();return false;" />
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td style='vertical-align: bottom; text-align: right; padding-left: 20px;' colspan="4">
                        <table>
                            <tr>
                                <td>
                                    <asp:Button ID="btnLoads" CssClass="btn btn-default btn-sm" Style="width: 100px;
                                        height: 30px;" OnClientClick="valGroup='value1';return CheckValidation();" OnClick="btnLoad_Click"
                                        runat="server" Text="Load"></asp:Button>
                                </td>
                                <td style='display:none'>
                                    <asp:LinkButton ID="btnExportPopup" OnClick="btnExportPopup_Click" runat="server"
                                        Text="Export Salary" CssClass=" excel marginRight tiptip" Style="float: left;
                                        width: 120px" />
                                </td>
                               <td style='display:none'>
                                    <asp:LinkButton ID="btnImportPopup" runat="server" Text="Import Salary" CssClass=" excel marginRight tiptip"
                                        Style="float: left; width: 120px" OnClientClick="importPopupProcess();return false;" />
                                </td>
                                <td>
                                    <asp:LinkButton ID="LinkButton1" runat="server" Text="Import Salary" CssClass=" excel marginRight tiptip"
                                        Style="float: left; width: 150px" OnClientClick="exportimportPopupProcess();return false;" />
                                </td>
                            </tr>
                        </table>
                        <asp:Button Style="display: none" ID="btnReload" CssClass="load" runat="server" Text="Load">
                        </asp:Button>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <strong>Saving Event Type *</strong>
                    </td>
                    <td  valign="top">
                        <asp:DropDownList ID="ddlEvents" AppendDataBoundItems="true" DataTextField="Name"
                            DataValueField="EventID" runat="server" Width="180px">
                            <asp:ListItem Text="--Select Event--" Value="-1" Selected="True" />
                        </asp:DropDownList>
                       
                    </td>
                     <td valign="top">
                        <strong>Filter Event Type</strong>
                    </td>
                    <td  valign="top">
                        <asp:DropDownList ID="ddlFilterEvents" AutoPostBack="true" AppendDataBoundItems="true" DataTextField="Name"
                            DataValueField="EventID" runat="server" Width="180px"  OnSelectedIndexChanged="ddlIncome_SelectedIndexChanged">
                            <asp:ListItem Text="--Select Event--" Value="-1" Selected="True" />
                        </asp:DropDownList>
                      
                    </td>
                </tr>
            </table>
             <br />
            <div style="margin-left:10px">
                        Note : For promotion salary change, please first change the Designation of Employee
                        <br />
                        If multiple salary change like Annual increment and Promotion on same month then
                        import in sequence
                </div>
        </div>
        <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
        <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
        <div class="clear" style="margin-top: 10px; margin-bottom: 0px">
            <cc2:EmptyDisplayGridView AllowSorting="true" OnSorting="gvwEmployees_Sorting" CssClass="table table-primary mb30 table-bordered table-hover"
                UseAccessibleHeader="true" DataKeyNames="EmployeeId,IncrementId,EmployeeIncomeId,IsIncrementMode"
                GridLines="None" HeaderStyle-CssClass="sortLink" ID="gvMassIncrement" runat="server"
                ShowFooterWhenEmpty="False" ShowHeaderWhenEmpty="True" AutoGenerateColumns="False">
                <Columns>
                    <asp:BoundField DataField="EmployeeId" HeaderText="EIN" ItemStyle-HorizontalAlign="Right"
                        HeaderStyle-Width="30px" />
                    <asp:BoundField DataField="EmployeeIncomeId" HeaderText="EmployeeIncomeId" Visible="false" />
                    <asp:TemplateField HeaderText="&nbsp;Name" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:HyperLink CssClass="employeeClass" Target="_blank" Width="120px" ID="Label2"
                                runat="server" NavigateUrl='<%# "~/newhr/EmployeePayroll.aspx?Id=" +  Eval("EmployeeId") %>'
                                Text='<%# "&nbsp;" + Eval("Name") %>'></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <%-- <asp:BoundField DataField="Grade" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="80px"
                        HeaderText="Grade" />--%>
                    <asp:BoundField DataField="Designation" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="170px"
                        HeaderText="Designation" />
                    <asp:TemplateField HeaderText="Status" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <%# GetStatus( Eval("Status")) %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Existing Income" ItemStyle-HorizontalAlign="Right"
                        HeaderStyle-HorizontalAlign="Right">
                        <ItemTemplate>
                            <asp:TextBox data-col='0' CssClass="readonlyTextBox" ReadOnly="true" data-row='<%# Container.DataItemIndex %>'
                                ID="txtExistingIncome" AutoCompleteType="None" Width="70px" Style='<%# "text-align: right" + (GetExistingIncome( Convert.ToString(Eval("ExistingIncome")), Convert.ToString(Eval("ExistingIncomeRate")) ) == "" ? ";border:1px solid red": "")%>'
                                runat="server" Text='<%# GetExistingIncome( Convert.ToString(Eval("ExistingIncome")), Convert.ToString(Eval("ExistingIncomeRate")) )%>' />
                            <asp:RequiredFieldValidator ID="valReqdExistingAmount" runat="server" Display="None"
                                ValidationGroup="Balance" ErrorMessage="Amount not assinged, please use employee -> pay calculation tab to assign the income."
                                ControlToValidate="txtExistingIncome" />
                            <%-- <asp:CompareValidator SetFocusOnError="true" ValueToCompare="0" Display="None" Operator="GreaterThan"
                                Type="Currency" ID="valOpeningBalance134" ControlToValidate="txtExistingIncome"
                                ValidationGroup="Balance" runat="server" ErrorMessage="Invalid amount, please use employee -> pay information tab to change the income."></asp:CompareValidator>--%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Increased By">
                        <ItemTemplate>
                            <asp:TextBox data-col='1' class="increment" increment="true" data-row='<%# Container.DataItemIndex %>'
                                ID="txtIncreasedBy" AutoCompleteType="None" Width="70px" Style='text-align: right'
                                runat="server" onChange='<%# "DisplayIncome( this," + ((GridViewRow)Container).RowIndex + ","+"\"\""+")"%>'
                                Text='<%#GetCurrency(Eval("IncrementIncome"))%>' />
                            <asp:CompareValidator SetFocusOnError="true" ValueToCompare="0" Display="None" Operator="DataTypeCheck"
                                Type="Currency" ID="valOpeningBalance1" ControlToValidate="txtIncreasedBy" ValidationGroup="Balance"
                                runat="server" ErrorMessage="Invalid amount."></asp:CompareValidator>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="New Income">
                        <ItemTemplate>
                            <asp:TextBox data-col='2' class="increment" increment="true" data-row='<%# Container.DataItemIndex %>'
                                ID="txtNewIncome" Width="70px" AutoCompleteType="None" Style='text-align: right'
                                onChange='<%# "DisplayIncome(this," + ((GridViewRow)Container).RowIndex + ","+"\"\""+")"%>'
                                runat="server" Text='<%#GetNewIncome(Convert.ToDouble(Eval("IncrementIncome")), Convert.ToDouble(Eval("ExistingIncome")),Eval("IncrementId")) %>' />
                            <asp:CompareValidator SetFocusOnError="true" ValueToCompare="0" Display="None" Operator="DataTypeCheck"
                                Type="Currency" ID="valOpeningBalance12" ControlToValidate="txtNewIncome" ValidationGroup="Balance"
                                runat="server" ErrorMessage="Invalid amount, value must be greater than zero."></asp:CompareValidator>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Arrear Increase">
                        <ItemTemplate>
                            <asp:TextBox Text='<%#GetCurrency(Eval("RetrospectIncrement"))%>' ID="txtReterospectiveIncrease"
                                AutoCompleteType="None" Style='text-align: right' onkeydown="return false;" Width="70px"
                                ReadOnly="True" BackColor="LightGray" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="EffectiveDate" HeaderText="Effective Date" ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-Width="100px" />
                    <asp:BoundField DataField="EffectiveDateEng" DataFormatString="{0:yyyy-MMM-dd}" HeaderText="Eng Date"
                        ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="90px" />
                    <asp:TemplateField HeaderText="Event" HeaderStyle-Width="150px">
                        <ItemTemplate>
                            <span>
                                <%#GetEventName((int)Eval("EventID"))%></span>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField runat="server" HeaderText="">
                        <HeaderTemplate>
                            <asp:Button ID="link" runat="server" CssClass="btn btn-info btn-sm" Style="height: 24px;
                                cursor: pointer" Text="Clear All" OnClientClick='return clearAll(this);' />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Button ID="link" runat="server" Style="border: 1px solid gray; height: 24px;
                                cursor: pointer" Text="Clear" OnClientClick='return clear1(this);' />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                <EmptyDataTemplate>
                    <b>No Employees are found</b>
                </EmptyDataTemplate>
            </cc2:EmptyDisplayGridView>
        </div>
        <uc1:PagingCtl ID="pagintCtl" OnNextRecord="btnNext_Click" OnPrevRecord="btnPrevious_Click"
            OnDropDownShowRecordsChanged="ddlRecords_SelectedIndexChanged" runat="server" />
        <div>
            <strong>Please clear & save the salary & re-enter for updating the increment.</strong>
        </div>
        <div class="buttonsDiv">
            <asp:LinkButton ID="btnSaveAndAddNew" OnClientClick="if( document.getElementById('ctl00_ContentPlaceHolderMain_ddlIncome').value !='-1' ) {window.location = 'AdjustRetrospectIncrement.aspx?Id=' + document.getElementById('ctl00_ContentPlaceHolderMain_ddlIncome').value;  }return false;;"
                CssClass="btn btn-warning" Style="float: left; margin-left: 10px; margin-right: 10px;"
                runat="server" Text="Arrear Details" Height="35px" Width="120px" />
            <asp:Button ID="btnSave" OnClientClick="valGroup='Balance';if(CheckValidation())  return ValidateIncrement();"
                CssClass="btn btn-primary" runat="server" Width="100px" Text="save" ValidationGroup="Balance"
                OnClick="btnSave_Click" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="pageScriptsContent" runat="server">
    <script type="text/javascript">
        function clearAll(sourceControl) {
            $('input[increment=true]').val('0');
            return false;
        }

        function clear1(sourceControl) {
            var increaseType = document.getElementById('<%=hdIncreaseType.ClientID%>');
            var income, iIncome, nIncome;
            var rootId = sourceControl.id.substring(0, sourceControl.id.lastIndexOf("_"));

            var txtExistingIncome = document.getElementById(rootId + '_txtExistingIncome');
            var txtIncreasedBy = document.getElementById(rootId + '_txtIncreasedBy');
            var txtNewIncome = document.getElementById(rootId + '_txtNewIncome');

            txtIncreasedBy.value = "0";
            txtNewIncome.value = "0";
            return false;
        }

        function DisplayIncome(sourceControl, rowIndex, returnIncreaseType) {

            var increaseType = document.getElementById('<%=hdIncreaseType.ClientID%>');
            var income, iIncome, nIncome;
            var rootId = sourceControl.id.substring(0, sourceControl.id.lastIndexOf("_"));
            var txtExistingIncome = document.getElementById(rootId + '_txtExistingIncome');
            var txtIncreasedBy = document.getElementById(rootId + '_txtIncreasedBy');
            var txtNewIncome = document.getElementById(rootId + '_txtNewIncome');

            income = txtExistingIncome.value.toString().replaceAll(",", "");
            iIncome = txtIncreasedBy.value.toString().replaceAll(",", "");
            nIncome = txtNewIncome.value.toString().replaceAll(",", "");

            if (sourceControl.id.indexOf("txtIncreasedBy") != -1) {

                if (iIncome == "")
                    iIncome = "0";

                if (isNaN(parseFloat(iIncome)))
                    return false;

                var newIncome;
                if (increaseType.value == "BYAMOUNT" && returnIncreaseType == "BYPERCENT") {

                    newIncome = parseFloat(income) + parseFloat(income) * parseFloat(iIncome) / 100;
                } else {
                    newIncome = parseFloat(income) + parseFloat(iIncome);
                }

                if (isNaN(newIncome))
                    txtNewIncome.value = "0";
                else {
                    txtNewIncome.value = (getNumber(newIncome.toFixed(2)));
                }
            } else if (sourceControl.id.indexOf("txtNewIncome") != -1) {

                if (nIncome == "")
                    nIncome = "0";

                if (isNaN(parseFloat(nIncome)))
                    return false;

                txtIncreasedBy.value = (getNumber((parseFloat(nIncome) - parseFloat(income)).toFixed(2)));
            }
            //if zero or not increment then set 0
            if (typeof (incomeTypeIsRate) != 'undefined' && incomeTypeIsRate == false) {
                if (parseFloat(income) == parseFloat(txtNewIncome.value)) {
                    txtNewIncome.value = "0";
                }
            }
        }

        function CallPopUp() {

            var IsPopUp = false;
            var incomeType = document.getElementById('<%=hdIncomeType.ClientID%>');
            var controls = document.getElementsByTagName("input");

            for (var i = 0; i < controls.length; i++) {
                if (controls[i].type == "text") {
                    if (controls[i].id.indexOf("txtIncreasedBy") > 0) {
                        IsPopUp = true;
                        break;
                    }
                }
            }
            if (!IsPopUp) {
                return;
            }
            var retValue = popupMassIncrement("Itype=" + incomeType.value);

            setUniformIncrement(retValue);

            return;
        }

        function setUniformIncrement(retValue) {
            if (typeof (retValue) != 'undefined' || retValue != null) {

                controls = document.getElementsByTagName("input");
                var retTypeArray;
                if (retValue == null || typeof (retValue) == 'undefined' || typeof (retValue.split) == 'undefined')
                    return;
                retTypeArray = retValue.split(":");

                for (var i = 0; i < controls.length; i++) {

                    if (controls[i].type == "text") {
                        if (controls[i].id.indexOf("gvMassIncrement") > 0 && controls[i].id.indexOf("txtIncreasedBy") > 0) {

                            if (controls[i].readOnly)
                                continue;

                            var startRowIndex = parseInt(controls[i].id.lastIndexOf("ctl0")) + 4;
                            var endRowIndex = controls[i].id.indexOf("_txtIncreasedBy");
                            var rowIndex = controls[i].id.substr(startRowIndex, endRowIndex - startRowIndex);
                            controls[i].value = retTypeArray[1];
                            DisplayIncome(controls[i], rowIndex - 2, retTypeArray[0]);
                        }
                    }

                }
            }
        }

        function ValidateIncrement() {
            var incrementStartingDate = getCalendarSelectedDate('<%= Calendar1.ClientID %>');

            if (isDateSame(startingDate, incrementStartingDate) ||
                isSecondCalendarCtlDateGreater(startingDate, incrementStartingDate))
                return true;

            //logic for reterospect
            if (confirm("<%= Resources.Messages.IncomeRetrospectIncrementSettingWarningMsg %>")) {
                return true;
            }
            else {
                alert("<%=  Resources.Messages.IncomeRetropsectIncrementNotWantedMsg %>");
                return false;
            }

            return false;

        }

        $(document).ready(function () {
            setMovementToGrid('#<%= gvMassIncrement.ClientID %>');

        });

        function selectAllText(textbox) {

            if (typeof (textbox.getAttribute) != 'undefined') {
                if (textbox.getAttribute("readonly") != null || textbox.getAttribute("disabled") != null)
                    return;
            }
            else if (textbox.length > 0) {
                if (textbox[0].getAttribute("readonly") != null || textbox[0].getAttribute("disabled") != null)
                    return;
            }

            textbox.select();
        }      
        
    </script>
</asp:Content>
