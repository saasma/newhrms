﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Manager;
using Utils.Helper;
using BLL;
using DAL;
using Utils.Web;
using BLL.Base;

namespace Web.CP
{
    public partial class ManageAutoOTGroup : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.AdministratorTools;
            }
        }

        CommonManager commonMgr = new CommonManager();
        PayManager payMgr = new PayManager();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialise();
            }
            if (Request.Form["__EVENTTARGET"] != null && Request.Form["__EVENTTARGET"].Equals("Reload"))
            {
                LoadGratuity();               
            }

            JavascriptHelper.AttachPopUpCode(Page, "insertUpdateGratuityRule", "AEAutoOTGroup.aspx", 900, 600);
        }

        void Initialise()
        {
            IncomeList();
            LoadGratuity();



            divWarningMsg.InnerHtml = CommonManager.GetLevelListNotDefinedInOTGroup() + " levels are not defined in any groups.";
            divWarningMsg.Hide = false;



        }
        void LoadGratuity()
        {
            gvwGratuityRules.DataSource = commonMgr.GetAutoOvertimeGroups(
                SessionManager.CurrentCompanyId);
            gvwGratuityRules.DataBind();

        }
       
        protected void gvwClass_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int GratuityClassId = (int)gvwGratuityRules.DataKeys[e.RowIndex][0];

            if (GratuityClassId != 0)
            {
                bool deleted = CommonManager.DeleteOTGroup(GratuityClassId);

                if (deleted)
                {

                    divMsgCtl.InnerHtml = "Group deleted.";
                    divMsgCtl.Hide = false;
                }
                else
                {
                    divMsgCtl.InnerHtml = "Group is being used, can not be deleted.";
                    divMsgCtl.Hide = false;
                }

            }
            LoadGratuity();
        }

        void IncomeList()
        {
          
        }

       
        protected void btnAddNew_Click(object sender, EventArgs e)
        {
           
        }
    }
}
