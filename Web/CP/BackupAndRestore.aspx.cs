﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Manager;
using Utils;
using System.Data.SqlClient;
using System.IO;
using BLL;

namespace Web.CP
{
    public partial class BackupAndRestore : System.Web.UI.Page
    {
       
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (User.Identity.IsAuthenticated == false 
            //    ||
            //    SessionManager.CurrentLoggedInEmployeeId != 0 
            //    ||
            //    SessionManager.User.RoleId!= (int)Role.Administrator)
            //{
            //    Response.Write("No Permission");
            //    return;
            //}

            if(!IsPostBack)
                LoadBackups();
        }

        void LoadBackups()
        {
            BindManualFilesGrid();
            BindAutomaticFilesGrid();

            //lstBackups.Items.Clear();
            //foreach (string file in Directory.GetFiles(CommonManager. GetLocation()))
            //{
            //    lstBackups.Items.Add(Path.GetFileName(file));
            //}
            DateTime now = DateTime.Now;
            txtFileName.Text
                = string.Format("backup-{0}-{1}-{2}({3}.{4}).bak", now.Day,now.Month,now.Year,now.Hour,now.Minute );
        }

        protected void btnBackup_Click(object sender, EventArgs e)
        {
            string location = CommonManager.GetLocation();

            location += "\\" + txtFileName.Text.Trim();

            string msg = CommonManager.TakeBacup(txtFileName.Text.Trim());
            if (msg == "")
            {
                msgInfo.InnerHtml = "Backup successfull";
                msgInfo.Hide = false;
                BindManualFilesGrid();

                txtFileName.Text = "";
            }
            else
            {
                msgWarning.InnerHtml = "Error : " +
                                       msg;
                msgWarning.Hide = false;
            }

        }



        protected void btnRestore_Click(object sender, EventArgs e)
        {
            //string location = CommonManager. GetLocation();

            //location += "\\" + lstBackups.SelectedValue;


            

            //string backupSql =
            //    string.Format("RESTORE DATABASE {0} FROM DISK = '{1}'", CommonManager.GetDatabaseName(), location);

            //try
            //{
            //    string connStr = Config.ConnectionString.Replace(CommonManager.GetDatabaseName(), "Master");




            //    SqlConnection conn = new SqlConnection(connStr);

            //    using (conn)
            //    {
            //        conn.Open();
            //        //SqlCommand dropCmd = new SqlCommand(string.Format("drop database {0}",GetDatabaseName()), conn);

            //        SqlCommand dropCmd = new SqlCommand(string.Format(" alter database {0} set single_user with rollback immediate", CommonManager.GetDatabaseName()), conn);
            //        dropCmd.ExecuteNonQuery();

            //        SqlCommand cmd = new SqlCommand(backupSql, conn);
            //        cmd.ExecuteNonQuery();

            //        SqlCommand multiUserCmd = new SqlCommand(string.Format(" alter database {0} set MULTI_USER with NO_WAIT", CommonManager.GetDatabaseName()), conn);
            //        multiUserCmd.ExecuteNonQuery();
            //    }

            //    msgInfo.InnerHtml = "Restore successfull";
            //    msgInfo.Hide = false;

                
            //}
            //catch (Exception exp)
            //{
            //    msgWarning.InnerHtml = "Error : " +
            //        exp.Message;
            //    msgWarning.Hide = false;
            //}
        }

        protected void btnDownload_Click(object sender, EventArgs e)
        {
            string location = CommonManager. GetLocation();

            location += "\\" + lstBackups.SelectedValue;

            string filename = lstBackups.SelectedValue;
            FileInfo info = new FileInfo(location);
            Response.Clear();
            Response.ClearHeaders();
            Response.AppendHeader("Content-Disposition", "attachment; filename=\"" + filename + "\"");
            Response.AppendHeader("Content-Length", info.Length.ToString());
            //Response.ContentType = document.MimeType;
            //Response.BinaryWrite(bytes);
            Response.WriteFile(location);
            Response.End();

            
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                string location = CommonManager. GetLocation();

                location += "\\" + lstBackups.SelectedValue;

                File.Delete(location);

                LoadBackups();

                msgInfo.InnerHtml = string.Format("Backup \"{0}\" deleted successfully.",Path.GetFileName(location));
                msgInfo.Hide = false;
            }
            catch (Exception exp)
            {
                msgWarning.InnerHtml = "Error : " +
                    exp.Message;
                msgWarning.Hide = false;
            }

        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            //if (fuBackup.HasFile)
            //{
            //    try
            //    {
            //        string loc = CommonManager.GetLocation() + "\\" + fuBackup.FileName;
            //        fuBackup.SaveAs(loc);
            //        msgInfo.InnerHtml = string.Format("Upload of \"{0}\" successfull.", fuBackup.FileName);
            //        msgInfo.Hide = false;
            //    }
            //    catch (Exception exp)
            //    {
            //        msgWarning.InnerHtml = "Error : " +
            //            exp.Message;
            //        msgWarning.Hide = false;
            //    }

            //    LoadBackups();
            //}
        }

        private void BindManualFilesGrid()
        {
            List<FileClass> list = new List<FileClass>();

            int i = 0;

            foreach (string file in Directory.GetFiles(CommonManager.GetLocation()))
            {
                FileClass obj = new FileClass();
                FileInfo objFileInfo = new FileInfo(file);
                obj.FileName = objFileInfo.Name;
                obj.CreatedDate = objFileInfo.CreationTime; 
                obj.DateString = obj.CreatedDate.ToString("MMM dd yyyy hh:mm tt");
                obj.FileSize = FileSizeToString(objFileInfo.Length);
                list.Add(obj);
            }

            int start = 0, end = 0, pageSize = 0, totalRecords = 0;
            pageSize = int.Parse(pagingCtl.DDLRecords.SelectedValue);
            start = (pagingCtl.CurrentPage - 1) * pageSize;
            end = start + pageSize;

            string searchName = "";
            DateTime? fromDate = null, toDate = null;

            if (!string.IsNullOrEmpty(txtNameSearchManual.Text.Trim()))
                searchName = txtNameSearchManual.Text.Trim().ToLower();

            if (!string.IsNullOrEmpty(txtFromDateManual.Text.Trim()) && txtFromDateManual.Date != new DateTime())
                fromDate = txtFromDateManual.Date;

            if (!string.IsNullOrEmpty(txtToDateManual.Text.Trim()) && txtToDateManual.Date != null)
                toDate = txtToDateManual.Date;

            list = list.Where(x => (x.FileName.ToLower().Contains(searchName)) && (fromDate == null || x.CreatedDate.Date >= fromDate.Value.Date) && (toDate == null || x.CreatedDate.Date <= toDate.Value.Date)).OrderByDescending(x => x.CreatedDate).ToList();
            totalRecords = list.Count();

            foreach (var item in list)
            {
                i++;
                item.Id = i;
            }

            gvcManualFiles.DataSource = list.Where(x => x.Id > start && x.Id <= end).ToList();

            pagingCtl.UpdatePagingBar(totalRecords);
            gvcManualFiles.DataBind();
        }

        protected void gvcManualFiles_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            string fileName = gvcManualFiles.DataKeys[e.RowIndex]["FileName"].ToString();

            try
            {
                string location = CommonManager.GetLocation();

                location += "\\" + fileName;

                File.Delete(location);

                gvcManualFiles.SelectedIndex = -1;
                BindManualFilesGrid();

                msgInfo.InnerHtml = string.Format("Backup \"{0}\" deleted successfully.", Path.GetFileName(location));
                msgInfo.Hide = false;
            }
            catch (Exception exp)
            {
                msgWarning.InnerHtml = "Error : " +
                    exp.Message;
                msgWarning.Hide = false;
            }
        }

        protected void gvcManualFiles_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Download")
            {
                string filename = e.CommandArgument.ToString();

                string location = CommonManager.GetLocation();

                location += "\\" + filename;
                
                FileInfo info = new FileInfo(location);
                Response.Clear();
                Response.ClearHeaders();
                Response.AppendHeader("Content-Disposition", "attachment; filename=\"" + filename + "\"");
                Response.AppendHeader("Content-Length", info.Length.ToString());
                Response.WriteFile(location);
                Response.End();
            }
        }

        private string FileSizeToString(double bytes)
        {

            string[] SizeSuffixes = { "bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };
            int mag = (int)Math.Log(bytes, 1024);
            decimal adjustedSize = (decimal)bytes / (1 << (mag * 10));
            return string.Format("{0:n1} {1}", adjustedSize, SizeSuffixes[mag]);
        }

        private void BindAutomaticFilesGrid()
        {
            List<FileClass> list = new List<FileClass>();

            string automaticFileLocationName = CommonManager.GetAutomaticFileLocation();

            if (string.IsNullOrEmpty(automaticFileLocationName))
            {
                divAutomaticFiles.Visible = false;
                return;
            }

            try
            {               
                foreach (string file in Directory.GetFiles(automaticFileLocationName))
                {
                    FileClass obj = new FileClass();
                    FileInfo objFileInfo = new FileInfo(file);
                    obj.FileName = objFileInfo.Name;
                    obj.CreatedDate = objFileInfo.CreationTime;
                    obj.DateString = obj.CreatedDate.ToString("MMM dd yyyy hh:mm tt");
                    obj.FileSize = FileSizeToString(objFileInfo.Length);
                    list.Add(obj);
                }
            }
            catch (Exception ex)
            {
                msgWarning.InnerHtml = "Error : " + ex.Message + " for Automatic Database Backups";
                msgWarning.Hide = false;
            }

            int start = 0, end = 0, pageSize = 0, totalRecords = 0;
            pageSize = int.Parse(pagingCtl2.DDLRecords.SelectedValue);
            start = (pagingCtl2.CurrentPage - 1) * pageSize;
            end = start + pageSize;

            string searchName = "";
            DateTime? fromDate = null, toDate = null;

            if (!string.IsNullOrEmpty(txtNameSearchAutomatic .Text.Trim()))
                searchName = txtNameSearchAutomatic.Text.Trim().ToLower();

            if (!string.IsNullOrEmpty(txtFromDateAutomatic.Text.Trim()) && txtFromDateAutomatic.Date != new DateTime())
                fromDate = txtFromDateAutomatic.Date;

            if (!string.IsNullOrEmpty(txtToDateAutomatic.Text.Trim()) && txtToDateAutomatic.Date != new DateTime())
                toDate = txtToDateAutomatic.Date;

            list = list.Where(x => (x.FileName.ToLower().Contains(searchName)) && (fromDate == null || x.CreatedDate.Date >= fromDate.Value.Date) && (toDate == null || x.CreatedDate.Date <= toDate.Value.Date)).OrderByDescending(x => x.CreatedDate).ToList();

            totalRecords = list.Count();

            int i = 0;
            foreach (var item in list)
            {
                i++;
                item.Id = i;
            }

            gvcAutomaticFiles.DataSource = list.Where(x => x.Id > start && x.Id <= end).ToList();
            pagingCtl2.UpdatePagingBar(totalRecords);
            gvcAutomaticFiles.DataBind();
        }

        protected void gvcAutomaticFiles_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Download")
            {
                string filename = e.CommandArgument.ToString();

                string location = CommonManager.GetAutomaticFileLocation();

                location += "\\" + filename;

                FileInfo info = new FileInfo(location);
                Response.Clear();
                Response.ClearHeaders();
                Response.AppendHeader("Content-Disposition", "attachment; filename=\"" + filename + "\"");
                Response.AppendHeader("Content-Length", info.Length.ToString());
                Response.WriteFile(location);
                Response.End();
            }
        }

        protected void btnNext_Click()
        {
            pagingCtl.CurrentPage += 1;
            BindManualFilesGrid();
        }

        protected void btnPrevious_Click()
        {
            pagingCtl.CurrentPage -= 1;
            BindManualFilesGrid();
        }

        protected void ddlRecords_SelectedIndexChanged()
        {
            pagingCtl.CurrentPage = 1;
            BindManualFilesGrid();
        }

        protected void gvcManualFiles_PageIndexChanged(object sender, EventArgs e)
        {

        }

        protected void gvcManualFiles_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvcManualFiles.PageIndex = e.NewPageIndex;
            gvcManualFiles.SelectedIndex = -1;
            BindManualFilesGrid();
        }

        protected void btnNext2_Click()
        {
            pagingCtl2.CurrentPage += 1;
            BindAutomaticFilesGrid();
        }

        protected void btnPrevious2_Click()
        {
            pagingCtl2.CurrentPage -= 1;
            BindAutomaticFilesGrid();
        }

        protected void ddlRecords2_SelectedIndexChanged()
        {
            pagingCtl2.CurrentPage = 1;
            BindAutomaticFilesGrid();
        }

        protected void gvcAutomaticFiles_PageIndexChanged(object sender, EventArgs e)
        {

        }

        protected void gvcAutomaticFiles_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvcAutomaticFiles.PageIndex = e.NewPageIndex;
            gvcAutomaticFiles.SelectedIndex = -1;
            BindAutomaticFilesGrid();
        }

        protected void btnLoadManual_Click(object sender, EventArgs e)
        {
            BindManualFilesGrid();
        }

        protected void btnLoadAutomatic_Click(object sender, EventArgs e)
        {
            BindAutomaticFilesGrid();
        }

        
    }

    public class FileClass
    {
        public string FileName { get; set; }
        public string DateString { get; set; }
        public string FileSize { get; set; }
        public DateTime CreatedDate { get; set; }
        public int Id { get; set; }
    }
}
