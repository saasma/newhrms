<%@ Page MaintainScrollPositionOnPostback="true" Title="Leave Request Settings" Language="C#"
    MasterPageFile="~/Master/HROld.Master" AutoEventWireup="true" CodeBehind="LeaveApprovalNotification.aspx.cs"
    Inherits="Web.CP.LeaveApprovalNotification" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .x-grid3-hd
        {
            background-color: #C5E2FF !important;
        }
        .x-grid3-hd-row
        {
            border-left-color: #A5DEFA;
            border-right-color: #A5DEFA;
        }
        .x-grid3-hd-inner
        {
            color: Black;
        }
        tbody .odd td
        {
            padding: 0px !important;
        }
        tbody .even td
        {
            padding: 0px !important;
        }
        .tableLightColor > tbody > tr > td
        {
            padding: 3px !important;
        }
        .itemList
        {
            margin: 0px;
        }
        .itemList li
        {
            float: left;
            list-style-type: none;
        }
        .bodypart
        {
            width: 1400px;
        }
    </style>
    <%--  <script src="../Scripts/jquery.sexy-combo.pack.js" type="text/javascript"></script>

   <script src="../Scripts/jquery.sexy-combo.min.js" type="tet/javascript"></script>

    <script src="../Scripts/jquery.sexy-combo.js" type="text/javascript"></script>--%>
    <script type="text/javascript">
        var DisplayEmail = function (store,value,email) {
            //var value = cmb.getValue();
           
            var r = store.getById(value);
//            if (Ext.isEmpty(r)) {
//                return "";
//            }

            switch (store.storeId.toLowerCase()) {
                case "ctl00_maincontent_storeappliesto":
                    //email = Ext.get(this.store.storeId.replace("storeAppliesTo", "lblEmail1"));
                    if (Ext.isEmpty(r)) {
                        email.setText("");
                        return "";
                    }
                    email.setText(r.data.Email);
                    
                    break;
                case "ctl00_maincontent_storecc1":
                    //email = Ext.get(this.store.storeId.replace("cboCC1", "lblEmail2"));
                    if (Ext.isEmpty(r)) {
                        email.setText("");
                        return "";
                    }
                    email.setText(r.data.Email);
                    break;
                case "ctl00_maincontent_storecc2":
                    //email = Ext.get(this.store.storeId.replace("cboCC2", "lblEmail3"));
                    if (Ext.isEmpty(r)) {
                        email.setText("");
                        return "";
                    }
                    email.setText(r.data.Email);
                    break;
                 case "ctl00_maincontent_storecc3":                   
                    if (Ext.isEmpty(r)) {
                        email.setText("");
                        return "";
                    }
                    email.setText(r.data.Email);
                    break;
                  case "ctl00_maincontent_storecc4":                   
                    if (Ext.isEmpty(r)) {
                        email.setText("");
                        return "";
                    }
                    email.setText(r.data.Email);
                    break;
                 case "ctl00_maincontent_storecc5":                   
                    if (Ext.isEmpty(r)) {
                        email.setText("");
                        return "";
                    }
                    email.setText(r.data.Email);
                    break;
                default: break;
            }
            

            //alert(r.data.Email);


        };

        var valGroup;
        var valControl;
        // validate all controls of the current validation group &
        //show message
        function Validate() {

            if (typeof (Page_Validators) == 'undefined')
                return true;

            for (var ii = 0; ii < Page_Validators.length; ii++) {
                var val = Page_Validators[ii];
                valControl = null;
                if (val.validationGroup == valGroup) {

                    ValidatorEnable(val);          
                        if (val.isvalid == false) {

                            valControl = val.controltovalidate;
                            alert(val.errormessage);
                            return false;
                        }
                }
            }
            return true;
        }

            function ValidationInAction()
            {
                if(confirm('Do you really want to save data?'))
                { 
                    valGroup='LeaveApproval';                    
                    return Validate();                    
                }
                else 
                {                    
                    return false;
                }

            }
             var CommandHandler = function(command, record){

                if(command=="Delete")
                {
                    
                    <%= hiddenID.ClientID %>.setValue(record.data.ID);
                    <%= btnDelete.ClientID %>.fireEvent('click');
                }
                else
                {
                    var projectId = <%= hiddenProjectId.ClientID %>;
                    projectId.setValue("");

                    if(record.data.LeaveProjectId != null)
                        projectId.setValue(parseInt(record.data.LeaveProjectId));

                    <%= hiddenDepartmentId.ClientID  %>.setValue( parseInt( record.data.DepartmentId));

                    <%= btnEdit.ClientID %>.fireEvent('click');
                }

             }

        function refreshWindow() {
            <%=PagingToolbar1.ClientID %>.doRefresh();
        }

        function isPayrollSelected(btn) {

           
            var ret = shiftPopup();
            return false;
        }
  
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <ext:ResourceManager ID="ResourceManager1" DisableViewState="false" ShowWarningOnAjaxFailure="false"
        runat="server" />
    <ext:Hidden ID="hiddenDepartmentId" runat="server" />
    <ext:Hidden ID="hiddenProjectId" runat="server" />
    <ext:Hidden ID="hiddenID" runat="server" />
    <ext:Button ID="btnEdit" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnEdit_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>
    <ext:Button ID="btnDelete" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnDelete_Click">
                <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the approval?" />
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Leave Request Settings
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
        <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
        <table style="clear: both;">
            <tr runat="server" id="rowBranch">
                <td colspan="2">
                    <ext:Store ID="storeBranch" runat="server">
                        <Model>
                            <ext:Model>
                                <Fields>
                                    <ext:ModelField Name="BranchId" />
                                    <ext:ModelField Name="Name" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                    <ext:ComboBox ID="cboBranch" runat="server" QueryMode="Local" DisplayField="Name"
                        ValueField="BranchId" Width="200" ForceSelection="true" StoreID="storeBranch"
                        FieldLabel="Branch" LabelAlign="Top" Editable="true">
                        <%--<DirectEvents>
                            <Select OnEvent="cboBranch_Select">
                                <EventMask ShowMask="true" />
                            </Select>
                        </DirectEvents>--%>
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                        </Triggers>
                        <Listeners>
                            <Select Handler="this.getTrigger(0).show();#{PagingToolbar1}.doRefresh();" />
                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                            <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                           #{PagingToolbar1}.doRefresh();
                                       }" />
                        </Listeners>
                    </ext:ComboBox>
                </td>
                <td style="width: 210px; display: none">
                    <ext:Store ID="storeMainDepartment" runat="server">
                        <Model>
                            <ext:Model>
                                <Fields>
                                    <ext:ModelField Name="DepartmentId" />
                                    <ext:ModelField Name="Name" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                    <ext:Store ID="storeDepartment" runat="server">
                        <Model>
                            <ext:Model>
                                <Fields>
                                    <ext:ModelField Name="DepartmentId" />
                                    <ext:ModelField Name="Name" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                    <ext:ComboBox ID="cboDepartment" runat="server" DisplayField="Name" ValueField="DepartmentId"
                        FieldLabel="Department" QueryMode="Local" LabelAlign="Top" Width="200" ForceSelection="true"
                        StoreID="storeMainDepartment" Editable="false">
                        <DirectEvents>
                            <Select OnEvent="cboDepartment_Select">
                                <EventMask ShowMask="true" />
                            </Select>
                        </DirectEvents>
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                        </Triggers>
                        <Listeners>
                            <Select Handler="this.getTrigger(0).show();" />
                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                            <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                            #{PagingToolbar1}.doRefresh();  
                                       }" />
                        </Listeners>
                    </ext:ComboBox>
                </td>
                <td colspan="2">
                    <ext:Store ID="storeSearchTeam" runat="server">
                        <Model>
                            <ext:Model runat="server">
                                <Fields>
                                    <ext:ModelField Name="LeaveProjectId" />
                                    <ext:ModelField Name="Name" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                    <ext:ComboBox ID="cmbTeam" runat="server" StyleSpec="margin-left:15px;" QueryMode="Local"
                        DisplayField="Name" ValueField="LeaveProjectId" Width="200" ForceSelection="true"
                        StoreID="storeSearchTeam" FieldLabel="Search Team" LabelAlign="Top" Editable="true">
                        <%-- <DirectEvents>
                            <Select OnEvent="cboBranch_Select">
                                <EventMask ShowMask="true" />
                            </Select>
                        </DirectEvents>--%>
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                        </Triggers>
                        <Listeners>
                            <Select Handler="this.getTrigger(0).show();#{PagingToolbar1}.doRefresh();" />
                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                            <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                           #{PagingToolbar1}.doRefresh();
                                       }" />
                        </Listeners>
                    </ext:ComboBox>
                </td>
                <td>
                    <td style='padding-left: 10px; padding-top: 20px;'>
                        <asp:LinkButton ID="btnExport" runat="server" Text="Excel import" OnClientClick="return isPayrollSelected(this)"
                            CssClass=" excel marginRight tiptip" Style="float: left;" />
                    </td>
                </td>
            </tr>
            <tr>
            </tr>
            <tr>
                <td colspan="2">
                </td>
            </tr>
            <tr>
                <td colspan="3">
                </td>
            </tr>
        </table>
    </div>
    <div style="clear: both; margin-left: 20px;" id="divGrid" runat="server">
        <ext:GridPanel ID="gvList" runat="server" Width="1200" AutoHeight="true" Cls="gridtbl">
            <Store>
                <ext:Store ID="storeLeaveApproval" WarningOnDirty="false" PageSize="50" runat="server"
                    RemoteSort="false">
                    <Proxy>
                        <ext:AjaxProxy Json="true" Url="../Handler/EmployeeGradeStepJsonService.asmx/GetApprovalList">
                            <ActionMethods Read="POST" />
                            <Reader>
                                <ext:JsonReader Root="d.Data" TotalProperty="d.TotalRecords" />
                            </Reader>
                        </ext:AjaxProxy>
                    </Proxy>
                    <AutoLoadParams>
                        <ext:Parameter Name="start" Value="={0}" />
                        <ext:Parameter Name="limit" Value="50" />
                    </AutoLoadParams>
                    <Parameters>
                        <ext:StoreParameter Name="departmentId" Value="#{cboDepartment}.getValue()" Mode="Raw" />
                        <ext:StoreParameter Name="branchId" Value="#{cboBranch}.getValue()" Mode="Raw" />
                        <ext:StoreParameter Name="teamIdStr" Value="#{cmbTeam}.getValue()" Mode="Raw" />
                    </Parameters>
                    <Model>
                        <ext:Model runat="server">
                            <Fields>
                                <ext:ModelField Name="ID" Type="Int" />
                                <ext:ModelField Name="DepartmentId" Type="Int" />
                                <ext:ModelField Name="LeaveProjectId" Type="Int" UseNull="true" />
                                <ext:ModelField Name="Department" />
                                <ext:ModelField Name="Branch" />
                                <ext:ModelField Name="Project" />
                                <ext:ModelField Name="ApplyTo" />
                                <ext:ModelField Name="CC1" />
                                <ext:ModelField Name="CC2" />
                                <ext:ModelField Name="CC3" />
                                <ext:ModelField Name="CC4" />
                                <ext:ModelField Name="CC5" />
                            </Fields>
                        </ext:Model>
                    </Model>
                    <%--<SortInfo Field="EmployeeId" Direction="ASC" />--%>
                </ext:Store>
            </Store>
            <ColumnModel ID="ColumnModel1" runat="server">
                <Columns>
                    <ext:Column ID="Column3" MenuDisabled="true" runat="server" Text="Branch" DataIndex="Branch"
                        Width="150px" Sortable="false" Align="Left" />
                    <ext:Column MenuDisabled="true" runat="server" Text="Team" DataIndex="Project" Width="150px"
                        Sortable="false" Align="Left" />
                    <ext:Column ID="ColumnR1" MenuDisabled="true" runat="server" Text="Recommend 1" DataIndex="CC4"
                        Width="150px" Sortable="false" Align="Left" />
                    <ext:Column ID="ColumnR2" MenuDisabled="true" runat="server" Text="Recommend 2" DataIndex="CC5"
                        Width="150px" Sortable="false" Align="Left" />
                    <ext:Column MenuDisabled="true" runat="server" Text="Approve 1" DataIndex="ApplyTo"
                        Width="150px" Sortable="false" Align="Left" />
                    <ext:Column MenuDisabled="true" runat="server" Text="Approve 2" DataIndex="CC1" Width="150px"
                        Sortable="false" Align="Left" />
                    <ext:Column MenuDisabled="true" Hidden="true" runat="server" Text="Approve 3" DataIndex="CC2"
                        Width="100px" Sortable="false" Align="Left" />
                    <ext:Column MenuDisabled="true" Hidden="true" runat="server" Text="Approve 4" DataIndex="CC3"
                        Width="100px" Sortable="false" Align="Left" />
                    <ext:CommandColumn Width="100" runat="server">
                        <Commands>
                            <ext:GridCommand Icon="ApplicationEdit" ToolTip-Text="Edit" CommandName="Save" />
                            <ext:CommandSeparator />
                            <ext:GridCommand Icon="Delete" CommandName="Delete" ToolTip-Text="Delete" />
                        </Commands>
                        <Listeners>
                            <Command Handler="CommandHandler(command,record);" />
                        </Listeners>
                    </ext:CommandColumn>
                </Columns>
            </ColumnModel>
            <SelectionModel>
                <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" />
            </SelectionModel>
            <BottomBar>
                <ext:PagingToolbar ID="PagingToolbar1" runat="server" PageSize="50" DisplayInfo="true"
                    DisplayMsg="Displaying list {0} - {1} of {2}" EmptyMsg="No list to display" />
            </BottomBar>
        </ext:GridPanel>
        <ext:Button runat="server" StyleSpec="margin-top:10px;" Text="Add" Height="30" Width="100">
            <%-- <Listeners>
                <Click Handler="#{LeaveApprovalWindow}.show()" />
            </Listeners>--%>
            <DirectEvents>
                <Click OnEvent="LeaveApprovalAddBtn_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:Button>
        <!--  -->
        <ext:Store ID="storeAppliesTo" runat="server">
            <Model>
                <ext:Model IDProperty="EmployeeId">
                    <Fields>
                        <ext:ModelField Name="EmployeeId" Type="String" />
                        <ext:ModelField Name="Name" />
                        <ext:ModelField Name="Email" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
        <ext:Window ID="LeaveApprovalWindow" runat="server" Icon="House" Title="Leave Approval Settings"
            Hidden="true" Width="700" Height="600" Modal="true">
            <Content>
                <table style="margin-left: 15px!important;" class="tableLightColor">
                    <tr>
                        <td colspan="6">
                            <ext:Store ID="storeLeaveProject" runat="server">
                                <Model>
                                    <ext:Model runat="server">
                                        <Fields>
                                            <ext:ModelField Name="LeaveProjectId" Type="String" />
                                            <ext:ModelField Name="Name" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                            <ext:ComboBox ID="cboProject" runat="server" DisplayField="Name" ValueField="LeaveProjectId"
                                Margins="0 0 10 0" FieldLabel="Select a team" QueryMode="Local" LabelAlign="Top"
                                Width="350" StoreID="storeLeaveProject" ForceSelection="true">
                                <DirectEvents>
                                    <Select OnEvent="cboProject_Select">
                                        <EventMask ShowMask="true" />
                                    </Select>
                                </DirectEvents>
                            </ext:ComboBox>
                            <ext:ComboBox ID="cmbApprovalNotificationType" runat="server" Margins="0 0 10 0"
                                ForceSelection="true" FieldLabel="When leave is approved, notify to" LabelWidth="350"
                                LabelAlign="Top" Width="350" Editable="false">
                                <Items>
                                    <ext:ListItem Value="0" Text="None" />
                                    <ext:ListItem Value="1" Text="All Employees" />
                                    <ext:ListItem Value="2" Text="Branch Employees" />
                                    <ext:ListItem Value="3" Text="Department Employees" />
                                    <ext:ListItem Value="4" Text="Team Members" />
                                    <ext:ListItem Value="5" Text="Team, Branch and Department Employees" />
                                </Items>
                            </ext:ComboBox>
                            <ext:TextField ID="ccEmailList" Width="350" LabelSeparator="" FieldLabel="Additional Notify to these Emails, separated by comma(,)"
                                LabelAlign="Top" runat="server" />
                        </td>
                    </tr>
                    
                    <tr runat="server" id="rowReview1">
                        <td colspan="6">
                            <div style="font-size: 15px; padding-top: 15px; padding-bottom: 5px;">
                                Team member's leave will be recommended by</div>
                        </td>
                    </tr>
                    <tr runat="server" id="rowReview2">
                        <th style="width: 100px">
                            Person
                        </th>
                        <th style="width: 100px;">
                            Name
                        </th>
                        <th style="text-align: center; display: none">
                            Allow
                            <br />
                            Leave Approval
                        </th>
                        <th>
                            <span runat="server" id="Span1" style="display: none">Timesheet HR Approve </span>
                        </th>
                        <th style="display: none">
                            Level
                        </th>
                        <th style="width: 250px;">
                            Email
                        </th>
                    </tr>
                    <tr class="odd"  runat="server" id="rowReview3">
                        <td>
                            Main Person
                        </td>
                        <td>
                            <ext:ComboBox QueryMode="Local" ID="cboCC4" runat="server" DisplayField="Name" ValueField="EmployeeId"
                                Width="200" ForceSelection="false" StoreID="storeAppliesTo" Mode="Local">
                                <Triggers>
                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                </Triggers>
                                <Listeners>
                                    <Focus Handler="this.lastQuery = '';this.store.clearFilter();" />
                                    <Blur Handler="if(this.getStore().getById(this.getValue())==null) this.clearValue(); " />
                                    <AfterRender Handler="this.getTrigger(0).show();" />
                                    <Select Handler="this.getTrigger(0).show();DisplayEmail(this.getStore(),this.getValue(),#{lblEmail5});" />
                                    <BeforeQuery Handler="this.getTrigger(0)[ this.getRawValue().toString().length == 0 ? 'show' : 'show']();" />
                                    <TriggerClick Handler="if (index == 0) { this.clearValue();DisplayEmail(this.getStore(),this.getValue(),#{lblEmail5});} " />
                                </Listeners>
                            </ext:ComboBox>
                        </td>
                        <td align="center" style="display: none">
                            <ext:Checkbox ID="chkCC4" Checked="true" runat="server" />
                        </td>
                        <td align="center">
                            &nbsp;
                        </td>
                        <td align="center" style="display: none">
                            <ext:TextField Width="50" ID="txtLevelCC4" Text="5" runat="server" />
                        </td>
                        <td>
                            <ext:Label ID="lblEmail5" runat="server" />
                        </td>
                    </tr>
                    <tr class="even"  runat="server" id="rowReview4">
                        <td>
                            Alternate person
                        </td>
                        <td>
                            <ext:ComboBox QueryMode="Local" ID="cboCC5" runat="server" DisplayField="Name" ValueField="EmployeeId"
                                Width="200" ForceSelection="false" StoreID="storeAppliesTo" Mode="Local">
                                <Triggers>
                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                </Triggers>
                                <Listeners>
                                    <Focus Handler="this.lastQuery = '';this.store.clearFilter();" />
                                    <Blur Handler="if(this.getStore().getById(this.getValue())==null) this.clearValue(); " />
                                    <AfterRender Handler="this.getTrigger(0).show();" />
                                    <Select Handler="this.getTrigger(0).show();DisplayEmail(this.getStore(),this.getValue(),#{lblEmail6});" />
                                    <BeforeQuery Handler="this.getTrigger(0)[ this.getRawValue().toString().length == 0 ? 'show' : 'show']();" />
                                    <TriggerClick Handler="if (index == 0) { this.clearValue();DisplayEmail(this.getStore(),this.getValue(),#{lblEmail6});} " />
                                </Listeners>
                            </ext:ComboBox>
                        </td>
                        <td align="center" style="display: none">
                            <ext:Checkbox ID="chkCC5" Checked="true" runat="server" />
                        </td>
                        <td align="center">
                            &nbsp;
                        </td>
                        <td align="center" style="display: none">
                            <ext:TextField Width="50" ID="txtLevelCC5" Text="6" runat="server" />
                        </td>
                        <td>
                            <ext:Label ID="lblEmail6" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            <div style="font-size: 15px; padding-top: 15px; padding-bottom: 5px;">
                                Recommended leaves will be approved by</div>
                        </td>
                    </tr>
                    <tr>
                        <th style="width: 100px">
                            Action Person
                        </th>
                        <th style="width: 100px;">
                            Person Name
                        </th>
                        <th style="text-align: center; display: none">
                            Allow
                            <br />
                            Leave Approval
                        </th>
                        <th>
                            <span runat="server" id="timesheetHRHeader1">Timesheet HR Approve </span>
                        </th>
                        <th style="display: none">
                            Level
                        </th>
                        <th style="width: 150px;">
                            Email
                        </th>
                    </tr>
                    <tr class="odd">
                        <td style='width: 150px'>
                            Main person
                        </td>
                        <td>
                            <ext:ComboBox ID="cboAppliesTo" runat="server" DisplayField="Name" ValueField="EmployeeId"
                                Width="200" QueryMode="Local" StoreID="storeAppliesTo" AllowBlank="true" EmptyText=""
                                ForceSelection="false" Mode="Local" TypeAhead="false">
                                <%--<Listeners>
                                                    <Select Fn="DisplayEmail" />
                                                </Listeners>--%>
                                <Triggers>
                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                </Triggers>
                                <Listeners>
                                    <Focus Handler="this.lastQuery = '';this.store.clearFilter();" />
                                    <Blur Handler="if(this.getStore().getById(this.getValue())==null) this.clearValue(); " />
                                    <AfterRender Handler="this.getTrigger(0).show();" />
                                    <Select Handler="this.getTrigger(0).show();DisplayEmail(this.getStore(),this.getValue(),#{lblEmail1});" />
                                    <BeforeQuery Handler="this.getTrigger(0)[ this.getRawValue().toString().length == 0 ? 'show' : 'show']();" />
                                    <TriggerClick Handler="if (index == 0) { this.clearValue();DisplayEmail(this.getStore(),this.getValue(),#{lblEmail1});} " />
                                </Listeners>
                            </ext:ComboBox>
                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="cboAppliesTo"
                                Display="None" ErrorMessage="Applies to is required." ValidationGroup="LeaveApproval" />--%>
                        </td>
                        </td>
                        <td align="center" style="display: none">
                            &nbsp;
                        </td>
                        <td align="center">
                            <span runat="server" id="timesheetInfo" style="font-size: 10px">If selected skipped
                                for Leave </span>
                        </td>
                        <td align="center" style="display: none">
                            <ext:TextField ID="txtLevelApplyTo" Text="1" Width="50" runat="server" />
                        </td>
                        <td>
                            <ext:Label ID="lblEmail1" runat="server" />
                        </td>
                    </tr>
                    <tr class="even">
                        <td>
                            Alternate person 1
                        </td>
                        <td>
                            <ext:ComboBox QueryMode="Local" ID="cboCC1" runat="server" DisplayField="Name" ValueField="EmployeeId"
                                Width="200" ForceSelection="false" StoreID="storeAppliesTo" Mode="Local">
                                <Triggers>
                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                </Triggers>
                                <Listeners>
                                    <Focus Handler="this.lastQuery = '';this.store.clearFilter();" />
                                    <Blur Handler="if(this.getStore().getById(this.getValue())==null) this.clearValue(); " />
                                    <AfterRender Handler="this.getTrigger(0).show();" />
                                    <Select Handler="this.getTrigger(0).show();DisplayEmail(this.getStore(),this.getValue(),#{lblEmail2});" />
                                    <BeforeQuery Handler="this.getTrigger(0)[ this.getRawValue().toString().length == 0 ? 'show' : 'show']();" />
                                    <TriggerClick Handler="if (index == 0) { this.clearValue();DisplayEmail(this.getStore(),this.getValue(),#{lblEmail2});} " />
                                </Listeners>
                            </ext:ComboBox>
                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="cboCC1"
                                    Display="None" ErrorMessage="Notified to 1 is required" ValidationGroup="LeaveApproval" />--%>
                        </td>
                        <td align="center" style="display: none">
                            <ext:Checkbox ID="chkCC1" Checked="true" runat="server" />
                        </td>
                        <td align="center">
                            &nbsp;
                        </td>
                        <td align="center" style="display: none">
                            <ext:TextField ID="txtLevelCC1" Text="2" Width="50" runat="server" />
                        </td>
                        <td>
                            <ext:Label ID="lblEmail2" runat="server" />
                        </td>
                    </tr>
                    <tr class="odd" style="display: none">
                        <td>
                            Alternate person 2
                        </td>
                        <td>
                            <ext:ComboBox QueryMode="Local" ID="cboCC2" runat="server" DisplayField="Name" ValueField="EmployeeId"
                                Width="200" ForceSelection="false" StoreID="storeAppliesTo" Mode="Local">
                                <Triggers>
                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                </Triggers>
                                <Listeners>
                                    <Focus Handler="this.lastQuery = '';this.store.clearFilter();" />
                                    <Blur Handler="if(this.getStore().getById(this.getValue())==null) this.clearValue(); " />
                                    <AfterRender Handler="this.getTrigger(0).show();" />
                                    <Select Handler="this.getTrigger(0).show();DisplayEmail(this.getStore(),this.getValue(),#{lblEmail3});" />
                                    <BeforeQuery Handler="this.getTrigger(0)[ this.getRawValue().toString().length == 0 ? 'show' : 'show']();" />
                                    <TriggerClick Handler="if (index == 0) { this.clearValue();DisplayEmail(this.getStore(),this.getValue(),#{lblEmail3});} " />
                                </Listeners>
                            </ext:ComboBox>
                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="cboCC2"3
                                    Display="None" ErrorMessage="Notified to 2 is required" ValidationGroup="LeaveApproval" />--%>
                        </td>
                        <td align="center" style="display: none">
                            <ext:Checkbox ID="chkCC2" Checked="true" runat="server" />
                        </td>
                        <td align="center" runat="server" id="timesheetHRRow">
                            <ext:Checkbox ID="chkTimeSheetHRApproval" Checked="false" runat="server" />
                        </td>
                        <td align="center" style="display: none">
                            <ext:TextField Width="50" ID="txtLevelCC2" Text="3" runat="server" />
                        </td>
                        <td>
                            <ext:Label ID="lblEmail3" runat="server" />
                        </td>
                    </tr>
                    <tr class="even" style="display: none">
                        <td>
                            Alternate person 3
                        </td>
                        <td>
                            <ext:ComboBox QueryMode="Local" ID="cboCC3" runat="server" DisplayField="Name" ValueField="EmployeeId"
                                Width="200" ForceSelection="false" StoreID="storeAppliesTo" Mode="Local">
                                <Triggers>
                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                </Triggers>
                                <Listeners>
                                    <Focus Handler="this.lastQuery = '';this.store.clearFilter();" />
                                    <Blur Handler="if(this.getStore().getById(this.getValue())==null) this.clearValue(); " />
                                    <AfterRender Handler="this.getTrigger(0).show();" />
                                    <Select Handler="this.getTrigger(0).show();DisplayEmail(this.getStore(),this.getValue(),#{lblEmail4});" />
                                    <BeforeQuery Handler="this.getTrigger(0)[ this.getRawValue().toString().length == 0 ? 'show' : 'show']();" />
                                    <TriggerClick Handler="if (index == 0) { this.clearValue();DisplayEmail(this.getStore(),this.getValue(),#{lblEmail4});} " />
                                </Listeners>
                            </ext:ComboBox>
                        </td>
                        <td align="center" style="display: none">
                            <ext:Checkbox ID="chkCC3" Checked="true" runat="server" />
                        </td>
                        <td align="center">
                            &nbsp;
                        </td>
                        <td align="center" style="display: none">
                            <ext:TextField Width="50" ID="txtLevelCC3" Text="4" runat="server" />
                        </td>
                        <td>
                            <ext:Label ID="lblEmail4" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-top: 30px" colspan="6">
                            <ul class="itemList">
                                <li>
                                    <ext:Button ID="btnExtSave" StyleSpec="margin-top:10px" runat="server" Text="Save"
                                        Height="30" Width="100" ValidationGroup="LeaveApproval">
                                        <DirectEvents>
                                            <Click OnEvent="btnExtSave_Click">
                                                <EventMask ShowMask="true" />
                                            </Click>
                                        </DirectEvents>
                                        <Listeners>
                                            <Click Handler=" return ValidationInAction();" />
                                        </Listeners>
                                    </ext:Button>
                                </li>
                                <li style="margin-left: 10px; padding-top: 15px;">or </li>
                                <li>
                                    <ext:LinkButton ID="btnClose" StyleSpec="margin-left:10px;padding-top:16px;" Width="100"
                                        Height="30" runat="server" Text="Cancel" ValidationGroup="InsertUpdate">
                                        <Listeners>
                                            <Click Handler="#{LeaveApprovalWindow}.hide();">
                                            </Click>
                                        </Listeners>
                                    </ext:LinkButton>
                                </li>
                            </ul>
                        </td>
                    </tr>
                </table>
            </Content>
        </ext:Window>
    </div>
</asp:Content>
