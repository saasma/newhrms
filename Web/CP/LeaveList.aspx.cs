﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using Utils.Helper;
using DAL;

namespace Web.CP
{
    public partial class LeaveList : System.Web.UI.Page
    {
        LeaveAttendanceManager leaveMgr = new LeaveAttendanceManager();
        int employeeId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            string eventTarget = Request.Form["__EVENTTARGET"];
            employeeId = UrlHelper.GetIdFromQueryString("EId");
            if (!IsPostBack)
            {
                Initialise();

                BizHelper.Load(new JobStatus(), ddlStatus);
            }
            else if (eventTarget != null && eventTarget.Equals("Reload"))
            {
                Initialise();
            }

            JavascriptHelper.AttachPopUpCode(Page, "popupCreateNew", "AELeave.aspx", 600, 650);
        }

        public void rdbListAddTo_Change(object sender, EventArgs e)
        {
            if (rdbListAddTo.Items[0].Selected)
            {
                ddlStatus.Enabled = false;
            }
            else
            {
                ddlStatus.Enabled = true;
            }
        }


        void Initialise()
        {

            int empId = employeeId;
            List<DAL.GetLeaveListForEmployeeResult> leaveMgrGetLeaveListForEmp = leaveMgr.GetLeaveListForEmp(empId);

            foreach (DAL.GetLeaveListForEmployeeResult result in leaveMgrGetLeaveListForEmp)
            {
                if (result.IsActive != null && result.IsActive.Value == false)
                {
                    result.Title += " (Deleted)";
                }
            }

            lstLeaveList.DataSource = leaveMgrGetLeaveListForEmp;
            lstLeaveList.DataBind();



        }

        protected void btnOk_Click(object sender, EventArgs e)
        {

            if (Page.IsValid)
            {

                bool result = false;

                foreach (ListItem item in lstLeaveList.Items)
                {
                    if (item.Selected)
                    {
                        result = leaveMgr.AddLeaveToEmployee(int.Parse(item.Value),
                    employeeId, rdbListAddTo.Items[1].Selected, SessionManager.CurrentCompanyId, int.Parse(ddlStatus.SelectedValue));
                    }
                }


                if (result)
                {
                    string msg;
                    if (rdbListAddTo.Items[1].Selected)
                        msg = "Leave added to all employees.";
                    else
                        msg = "Leave added to the employee.";

                    JavascriptHelper.DisplayClientMsg(msg, this, "closePopup();");
                }
                else
                {
                    JavascriptHelper.DisplayClientMsg("Leave may not be applicable for this employee.", this);
                }
            }
        }
    }
}
