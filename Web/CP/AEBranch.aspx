﻿<%@ Page Title="Branch Details" Language="C#" ValidateRequest="false" EnableEventValidation="false"
    MasterPageFile="~/Master/ForPopupPage.Master" AutoEventWireup="true" CodeBehind="AEBranch.aspx.cs"
    Inherits="Web.CP.AEBranch" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningMsgCtl" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">



        $(document).ready(
            function () {
            }
        );

        function closePopup() {


            // alert(window.opener.parentReloadCallbackFunction)
            //if ($.browser.msie == false && typeof (window.opener.reloadDeduction) != 'undefined' && ($.browser.chrome || $.browser.safari)) {
            window.opener.parentIncomeCallbackFunction('ReloadIncome', window);
            //            } else {
            //                window.returnValue = "Reload";
            //                window.close();
            //            }

        }





        function loadCalculation(ret, closingWindow) {
            closingWindow.close();
            processReturnValue(ret);
        }
        var divRemoteArea = '#<%= divRemoteArea.ClientID %>';

        var districtId = '<%= ddlDistricts.ClientID %>';
        var vdcId = '<%= ddlVDCs.ClientID %>';
        function DisplayRemoteArea() {

            Web.PayrollService.GetRemoveAreaValue($('#' + vdcId).val(), DisplayRemoteAreaCallback);
        }

        function cmbBranchHeadChange() {

            var hid = document.getElementById('Hidden_BranchHeadID');
            var cmbHead = document.getElementById('cmbBranchHead');
            hid.value = cmbHead.value;
        }

        function DisplayRemoteAreaCallback(result) {
            var ctl = document.getElementById('<%= lblRemoteAreaValue.ClientID %>');
            if (result == "")
                ctl.style.display = 'none';
            else
                ctl.style.display = '';

            ctl.innerHTML = result;
        }

        function regionChange(isRegionalOffice) {
            var trRegionHead = document.getElementById('<%=trRegionHead.ClientID %>');
            var trRegionalBranch = document.getElementById('<%=trRegionalBranch.ClientID %>');
            if (isRegionalOffice) {
                trRegionHead.style.display = '';
                trRegionalBranch.style.display = 'none';
            }
            else {
                trRegionHead.style.display = 'none';
                trRegionalBranch.style.display = '';
            }
        }


        function onChangeDistrict(ddl) {
            Web.PayrollService.GetVDCByDistrict(ddl.value, onChangeDistrictCallback);
        }

        function onChangeDistrictCallback(result) {

            var list = result;
            document.getElementById(vdcId).options.length = 1;
            for (i = 0; i < list.length; i++) {
                var menu = list[i];
                var htmlContent = String.format("<option value='{0}'>{1}</option>", menu.Key, menu.Value);
                $('#' + vdcId).append(htmlContent);
            }
            DisplayRemoteArea();
        }

        function showDetails() {

        }
        function hideDetails() {
        }
        
        function IsActiveCheckChange(value) {
            if (value) {
                <%= calInActiveDate.ClientID %>.hide();
            }
            else
            {
                <%= calInActiveDate.ClientID %>.show();
            }
        }

    </script>
    <style type="text/css">
        .paddinAll
        {
            padding: 10px;
        }
        
        h3
        {
            font: bold 13px Arial;
        }
        .marginTopss
        {
            margin-top: 10px;
            display: block;
        }
        .loanAdvTable
        {
            background-color: white;
            width: 60px;
            border: 1px solid white !important;
            padding: 0px;
        }
        .tableLightColor th
        {
            text-align: left;
        }
        strong
        {
            display: block;
            padding-bottom: 3px;
        }
        .tableLightColor input[type=text]
        {
            height: 20px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <asp:HiddenField ID="hdEditSequence" runat="server" />
    <asp:HiddenField ID="hdEEditSequence" runat="server" />
    <asp:HiddenField ID="hiddenEmployeeId" Value="0" runat="server" />
    <asp:HiddenField ID="hdnBranchId" runat="server" />
    <ext:ResourceManager ID="ResourceManager1" DisableViewState="false" runat="server" />
    <div class="popupHeader">
        <h3>
            Branch Details</h3>
    </div>
    <div class=" marginal" style="margin: 5px 20px 0 10px;">
        <uc1:WarningMsgCtl ID="WarningMsgCtl1" runat="server" Width="550px" EnableViewState="false"
            Hide="true" />
        <table cellpadding="4px" style="margin-left: 10px;">
            <tr>
                <td class="fieldHeader">
                    <My:Label ID="Label2" Text="Branch Name" runat="server" ShowAstrick="true" />
                </td>
                <td>
                    <asp:TextBox ID="txtBranchName" runat="server" Width="180px" />
                    <asp:RequiredFieldValidator ID="valReqdName" runat="server" ControlToValidate="txtBranchName"
                        Display="None" ErrorMessage="Branch name is required." ValidationGroup="AEBranch"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="fieldHeader">
                    <My:Label ID="Label4" Text="Branch Code" runat="server" ShowAstrick="false" />
                </td>
                <td>
                    <asp:TextBox ID="txtCode" runat="server" Width="180px" />
                </td>
            </tr>
            <tr>
                <td class="fieldHeader">
                    <My:Label ID="lblBranchID" Text="Branch ID" runat="server" ShowAstrick="false" />
                </td>
                <td>
                    <asp:TextBox ID="txtBranchID" runat="server" Width="180px" />
                </td>
            </tr>
            <tr id="Tr1" class="seperatoring" runat="server" visible="false">
                <td class="fieldHeader">
                    Responsible Person
                </td>
                <td>
                    <asp:TextBox ID="txtResponsiblePerson" runat="server" Width="180px" />
                </td>
            </tr>
            <tr>
                <td class="fieldHeader">
                    <My:Label ID="Label1" Text="Address" runat="server" ShowAstrick="false" />
                </td>
                <td>
                    <asp:TextBox ID="txtAddress" runat="server" Width="180px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="fieldHeader">
                    <My:Label ID="Label3" Text="Branch Head" runat="server" ShowAstrick="false" />
                </td>
                <td>
                    <asp:HiddenField ID="Hidden_BranchHeadID" runat="server" />
                    <asp:DropDownList ID="cmbBranchHead" onchange="cmbBranchHeadChange()" DataTextField="NameEIN"
                        Width="185px" CssClass="marginbtns" DataValueField="EmployeeId" runat="server"
                        AppendDataBoundItems="true">
                        <asp:ListItem Value="-1">--Select a Employee--</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="fieldHeader">
                    Area Code
                </td>
                <td>
                    <asp:TextBox ID="txtAreaCode" runat="server" Width="180px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="fieldHeader">
                    Phone
                </td>
                <td>
                    <asp:TextBox ID="txtPhone" runat="server" Width="180px"></asp:TextBox>
                  <%--  <asp:RegularExpressionValidator ID="valRegPhone" runat="server" ControlToValidate="txtPhone"
                        ErrorMessage="Phone number is invalid." ValidationGroup="AEBranch" Display="None"></asp:RegularExpressionValidator>--%>
                </td>
            </tr>
            <tr id="Tr2" runat="server" visible="true">
                <td class="fieldHeader">
                    Fax
                </td>
                <td>
                    <asp:TextBox ID="txtFax" runat="server" Width="180px"></asp:TextBox>
                <%--    <asp:RegularExpressionValidator ID="valRegFax" runat="server" ControlToValidate="txtFax"
                        ErrorMessage="Fax is invalid." ValidationGroup="AEBranch" Display="None"></asp:RegularExpressionValidator>--%>
                </td>
            </tr>
            <tr class="seperatoring">
                <td class="fieldHeader">
                    Email
                </td>
                <td>
                    <asp:TextBox ID="txtEmail" runat="server" Width="180px"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                        ControlToValidate="txtEmail" ErrorMessage="Email is invalid." ValidationGroup="AEBranch"
                        Display="None"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr class="up">
                <td class="fieldHeader">
                    Is Regional Office
                </td>
                <td>
                    <asp:CheckBox ID="chkIsRegionalOffice" onclick='regionChange(this.checked)' runat="server" />
                </td>
            </tr>
            <tr class="seperatoring" runat="server" id="trRegionHead" style="display: none">
                <td class="fieldHeader">
                    <My:Label ID="Label31" Text="Region Head" runat="server" ShowAstrick="false" />
                </td>
                <td>
                    <asp:DropDownList ID="cmbRegionHead" DataTextField="NameEIN" Width="185px" CssClass="marginbtns"
                        DataValueField="EmployeeId" runat="server" AppendDataBoundItems="true">
                        <asp:ListItem Value="-1">--Select Employee--</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr class="seperatoring" runat="server" id="trRegionalBranch">
                <td class="fieldHeader">
                    <My:Label ID="Label3111" Text="Region" runat="server" ShowAstrick="false" />
                </td>
                <td>
                    <asp:DropDownList ID="cmbRegionBranchList" DataTextField="Name" Width="185px" CssClass="marginbtns"
                        DataValueField="BranchId" runat="server" AppendDataBoundItems="true">
                        <asp:ListItem Value="-1">--Select Regional Branch--</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr class="up" style="display: none">
                <td class="fieldHeader">
                    Checking Bank Name
                </td>
                <td>
                    <asp:TextBox ID="txtBankName" runat="server" Width="180px"></asp:TextBox>
                </td>
            </tr>
            <tr class="seperatoring" style="display: none">
                <td class="fieldHeader">
                    Account No
                </td>
                <td>
                    <asp:TextBox ID="txtBankAccountNo" runat="server" Width="180px"></asp:TextBox>
                </td>
            </tr>
            <tr class="up">
                <td class="fieldHeader" valign="top">
                    <asp:CheckBox ID="chkIsInRemoteArea" runat="server" Text="Remote Area" />
                </td>
                <td id="divRemoteArea" style='padding-left: 0px' runat="server">
                    <table>
                        <tr>
                            <td align="left">
                                <asp:DropDownList EnableViewState="false" onchange="onChangeDistrict(this)" disabled='disabled'
                                    ID="ddlDistricts" DataTextField="District" DataValueField="DistrictId" runat="server"
                                    AppendDataBoundItems="true" Width="185px" CssClass="marginbtns">
                                    <asp:ListItem Value="-1">--Select District--</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator InitialValue="-1" Display="None" ControlToValidate="ddlDistricts"
                                    ValidationGroup="AEBranch" ID="valCustDistrict" runat="server" ErrorMessage="Please select a district."></asp:RequiredFieldValidator>
                            </td>
                            <td valign="top" style='padding: 2px; padding-top: 7px'>
                                <asp:Label ID="lblRemoteAreaValue" EnableViewState="false" runat="server" Style='border: 1px solid blue' />
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:DropDownList EnableViewState="false" onchange="DisplayRemoteArea()" disabled='disabled'
                                    ID="ddlVDCs" DataTextField="Name" Width="185px" CssClass="marginbtns" DataValueField="VDCId"
                                    runat="server" AppendDataBoundItems="true">
                                    <asp:ListItem Value="-1">--Select VDC--</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator InitialValue="-1" Display="None" ControlToValidate="ddlVDCs"
                                    ValidationGroup="AEBranch" ID="valCustVDC" runat="server" ErrorMessage="Please select a VDC."></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                    </table>
                    <%--</div>--%>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:CheckBox ID="chkIsManualAttendance" runat="server" Text="Enable Manual Attendance" />
                </td>
            </tr>
            <tr class="up">
                <td class="fieldHeader" valign="top">
                    <asp:CheckBox ID="chkIsActive" runat="server" Text="Active" onclick='IsActiveCheckChange(this.checked)'  />
                </td>
                <td style='padding-left: 0px'>
                    <table>
                        <tr>
                            <td align="left">
                                <pr:CalendarExtControl Width="180px" FieldLabel="" ID="calInActiveDate" Hidden="true"
                                    runat="server" LabelAlign="Top" LabelSeparator="">
                                </pr:CalendarExtControl>
                            </td>
                        </tr>

                    </table>
                    <%--</div>--%>
                </td>
            </tr>

        </table>
    </div>
    <br />
    <div style='padding-left: 12px; text-align: right; padding-right: 22px'>
        <asp:Button ID="btnSave" runat="server" CssClass="save" OnClientClick="valGroup='AEBranch';return CheckValidation()"
            Text="Save" ValidationGroup="AEBranch" OnClick="btnSave_Click" />
        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="cancel" OnClientClick="window.close()" />
    </div>
</asp:Content>
