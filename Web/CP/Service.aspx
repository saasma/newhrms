﻿<%@ Page Title="Change Service provider information" Language="C#" MasterPageFile="~/Master/Details.Master"
    AutoEventWireup="true" CodeBehind="Service.aspx.cs" Inherits="Web.CP.Service" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
     <fieldset class="mainFieldset">
        <legend>Service</legend>
        
        
        
        
    <div align="left" style="margin-left:20px">
        <%-- <fieldset>--%>
        <%-- <legend>Payroll Service Provider</legend>--%>
     
     
        <table  cellpadding="4px">
            <%-- class="smallContentTable" width="360px" cellpadding="4px" >--%>
            <tr>
                <td class="fieldHeader">
                    Company Name
                </td>
                <td>
                    <asp:TextBox ID="txtCompanyName" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="valReqdName" runat="server" ControlToValidate="txtCompanyName"
                        Display="None" ErrorMessage="Company name is required." ValidationGroup="Service"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="fieldHeader">
                    Address
                </td>
                <td>
                    <asp:TextBox ID="txtAddress" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="fieldHeader">
                    Vat No
                </td>
                <td>
                    <asp:TextBox ID="txtVatPanNo" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="fieldHeader">
                    Phone No
                </td>
                <td>
                    <asp:TextBox ID="txtPhoneNo" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="valRegPhone" runat="server" ControlToValidate="txtPhoneNo"
                        ErrorMessage="Phone number is invalid." ValidationGroup="Service" Display="None"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td class="fieldHeader">
                    Fax No
                </td>
                <td>
                    <asp:TextBox ID="txtFaxNo" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="valRegFax" runat="server" ControlToValidate="txtFaxNo"
                        ErrorMessage="Fax number is invalid." ValidationGroup="Service" Display="None"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td class="fieldHeader">
                    Email
                </td>
                <td>
                    <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="valRegEmail" runat="server" ControlToValidate="txtEmail"
                        ErrorMessage="Email is invalid." ValidationGroup="Service" Display="None" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td class="fieldHeader">
                    Website
                </td>
                <td>
                    <asp:TextBox ID="txtWebsite" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="valRegWebsite" runat="server" ControlToValidate="txtWebsite"
                        ErrorMessage="Website is invalid." ValidationGroup="Service" Display="None" ValidationExpression="http(s)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&amp;=]*)?"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <asp:LinkButton  ID="btnSave" CssClass="buttonGreen" OnClientClick="valGroup='Service';return CheckValidation();"
                        ValidationGroup="Service" runat="server" Text="Save" OnClick="btnSave_Click" />
                    &nbsp;<asp:Button ID="btnCancel" Visible="false" runat="server" OnClientClick="window.location= 'ManageCompany.aspx';return false;"
                        Text="Cancel" />
                </td>
            </tr>
        </table>
        <%--</fieldset>--%>
    </div>
</fieldset>
</asp:Content>
