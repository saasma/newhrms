﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using Utils.Base;
using BLL;
using DAL;
using Utils.Web;
using Utils.Helper;
using System.Text.RegularExpressions;
using Ext.Net;

namespace Web.CP
{
    public partial class AELeaveBranchDepartments : BasePage
    {
        /// <summary>
        /// 0-> Data Insert Mode
        /// 1-> Data Update Mode
        /// </summary>


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                Initialize();
        }

        private void Initialize()
        {
            List<Branch> list = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId); ;
            storeGridBranch.DataSource = list;
            storeGridBranch.DataBind();

            storeDepartment.DataSource = DepartmentManager.GetAllDepartments().OrderBy(x => x.Name).ToList();
            storeDepartment.DataBind();
            BindSavedBranchDepartment();
        }

        protected void btnSaveUpdate_Click(object sender, DirectEventArgs e)
        {
            string entryLineJsonDepartment = e.ExtraParams["DepartmentList"];
            string entryLineJsonBranch = e.ExtraParams["BranchList"];
            Status myStatus = new Status();

            List<Branch> _LineListBranch = JSON.Deserialize<List<Branch>>(entryLineJsonBranch);
            List<Department> _LineListDepartment = JSON.Deserialize<List<Department>>(entryLineJsonDepartment);

            if (!_LineListDepartment.Any() && !_LineListBranch.Any())
            {
                NewMessage.ShowNormalMessage("No any branch/Department is selected");
                return;
            }


            int LeaveTypeId = int.Parse(Request.QueryString["LId"]);

            RowSelectionModel smBranch = this.gridBranch.GetSelectionModel() as RowSelectionModel;
            List<LeaveBranchDepartment> _listLeaveBranchDepartment = new List<LeaveBranchDepartment>();
            int CountBranch = _LineListBranch.Count();
            int countDept = _LineListDepartment.Count();

            
            int TotalCount = 0;
            if(countDept>CountBranch)
                TotalCount= countDept;
            else
                TotalCount= CountBranch;

            TotalCount = TotalCount - 1;
            for (int index = 0; index <= TotalCount; index++)
            {

                LeaveBranchDepartment _objLeaveBranchDepartment = new LeaveBranchDepartment();
                _objLeaveBranchDepartment.ID = Guid.NewGuid();
                _objLeaveBranchDepartment.LeaveTypeID = LeaveTypeId;

                if (index <= CountBranch-1)
                    _objLeaveBranchDepartment.BranchId = _LineListBranch[index].BranchId;
                if (index <= countDept-1)
                    _objLeaveBranchDepartment.DepartmentId = _LineListDepartment[index].DepartmentId;
                _listLeaveBranchDepartment.Add(_objLeaveBranchDepartment);
            }

            myStatus = LeaveAttendanceManager.InsertUpdateLeveBranchDepartment(LeaveTypeId, _listLeaveBranchDepartment);
            if (myStatus.IsSuccess)
                NewMessage.ShowNormalMessage("The information has been updated.");
            else
                NewMessage.ShowWarningMessage("unable to update record.");
        }

        protected void BindSavedBranchDepartment()
        {
            int LeaveTypeId = int.Parse(Request.QueryString["LId"]);
            List<LeaveBranchDepartment> _ListDepartments = LeaveAttendanceManager.GetLeaveBranchDepartment(LeaveTypeId);
            RowSelectionModel smBranch = this.gridBranch.GetSelectionModel() as RowSelectionModel;
            smBranch.ClearSelection();
            smBranch.SelectedRows.Clear();
            smBranch.UpdateSelection();

            RowSelectionModel smDepartment = this.gridDepartment.GetSelectionModel() as RowSelectionModel;
            smDepartment.ClearSelection();
            smDepartment.SelectedRows.Clear();
            smDepartment.UpdateSelection();

            foreach (LeaveBranchDepartment _list in _ListDepartments)
            {
                smBranch.SelectedRows.Add(new SelectedRow(_list.BranchId.ToString().ToLower()));
                smDepartment.SelectedRows.Add(new SelectedRow(_list.DepartmentId.ToString().ToLower()));
            }

            smBranch.UpdateSelection();
            smDepartment.UpdateSelection();
        }
    }
}

