﻿<%@ Page Title="Department Transfer List" Language="C#" MasterPageFile="~/Master/NewDetails.Master" AutoEventWireup="true" CodeBehind="DepartmentTransferNew.aspx.cs" Inherits="Web.CP.DepartmentTransferNew" %>

<%@ MasterType VirtualPath="~/Master/NewDetails.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

<style type="text/css">
        .empName
        {
            font-size: 20px;
            color: #439EC6;
            font-weight: bold;
        }
        .titleDesign
        {
            font-size: 15px;
            color: #000000;
            text-decoration: underline;
            padding-top: 10px;
        }
        
        .items span
        {
            display: block;
            padding-top: 3px;
            font-weight: bold;
        }
        
        .paddinAll
        {
            padding: 10px;
        }
       <%-- table tr td
        {
            padding: 0 10px 8px 0;
        }--%>
        h3
        {
            font: bold 13px Arial;
        }
        .marginTopss
        {
            margin-top: 10px;
            display: block;
        }
        .loanAdvTable
        {
            background-color: white;
            width: 60px;
            border: 1px solid white !important;
            padding: 0px;
        }
        .tableLightColor th
        {
            text-align: left;
        }
        strong
        {
            display: block;
            padding-bottom: 3px;
        }
        .tableLightColor input[type=text]
        {
            height: 20px;
        }
        .clsColor
        {
            background-color: #e8f1ff;
        }
         .BlueCls
        {
            color:Blue;
        }
    </style>

    <script type="text/javascript">

    function searchList() {
             <%=gridDepartmentTransfer.ClientID %>.getStore().pageSize = <%=cmbPageSize.ClientID %>.getValue();
            <%=PagingToolbar1.ClientID %>.doRefresh();
        }

    function EmpSelect()
    {
        <%=hdnEmployeeId.ClientID %>.setValue(<%=cmbEmployee.ClientID %>.getValue());
        <%=btnSelectEmp.ClientID %>.fireEvent('click');
    }

    var CommandHandler = function(command, record){
            <%= hdnBranchDepartmentId.ClientID %>.setValue(record.data.BranchDepartmentId);
            <%=hdnEmployeeId.ClientID %>.setValue(record.data.EmployeeId);
                if(command=="Edit")
                {
                    <%= btnEdit.ClientID %>.fireEvent('click');
                }
             };

  

   </script>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">

<ext:Hidden ID="hdnEmployeeId" runat="server" />
<ext:Hidden ID="hdnBranchDepartmentId" runat="server" />
<ext:Hidden ID="hdnMsg" runat="server" />

<ext:LinkButton ID="btnSelectEmp" runat="server" Hidden="true">
    <DirectEvents>
        <Click OnEvent="btnSelectEmp_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:LinkButton>

<ext:LinkButton ID="btnEdit" runat="server" Hidden="true">
    <DirectEvents>
        <Click OnEvent="btnEdit_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:LinkButton>



 <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Department Transfer List
                </h4>
            </div>
        </div>
    </div>


    <div class="contentpanel" style="margin-top: 0px !important; padding-top: 0px !important;">

<div class="innerLR">
<br />

                <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                            <Proxy>
                                <ext:AjaxProxy Url="../Handler/EmpSearch.ashx">
                                    <ActionMethods Read="GET" />
                                    <Reader>
                                        <ext:JsonReader Root="plants" TotalProperty="total" />
                                    </Reader>
                                </ext:AjaxProxy>
                            </Proxy>
                            <Model>
                                <ext:Model ID="Model5" IDProperty="Name" Name="ItemLineModel" runat="server">
                                    <Fields>
                                        <ext:ModelField Name="Name" Type="String" />
                                        <ext:ModelField Name="EmployeeId" Type="String" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>

    <ext:Button ID="btnNewTransfer" runat ="server" Text = "New Transfer">
            <DirectEvents>
                <Click OnEvent = "btnNewTransfer_Click" />
            </DirectEvents>
        </ext:Button>
    
    
     <div class="alert alert-info" style="margin-top: 25px;">
                <ext:Container ID="listingContainer" runat="server">
                    <Content>
                        <table>
                            <tr>                                
                                <td style="width:160px;">
                                    <ext:DateField ID="txtFromDate" runat="server" FieldLabel="From" LabelSeparator="" LabelAlign="Top" Width="150" /> 
                                </td>
                                <td style="width:160px;">
                                    <ext:DateField ID="txtToDate" runat="server" FieldLabel="To" LabelSeparator="" LabelAlign="Top" Width="150" /> 
                                </td>

                                <td style="width:310px;">
                                    <ext:ComboBox LabelSeparator="" ID="cmbEmpSearch" FieldLabel="Search Employee" EmptyText="Employee Name" LabelWidth="80"
                                        LabelAlign="Top" runat="server" DisplayField="Name" ValueField="EmployeeId" StoreID="storeSearch"
                                        TypeAhead="false" Width="300" PageSize="9999" HideBaseTrigger="true" MinChars="1"
                                        TriggerAction="All" ForceSelection="false">
                                        <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                                            <ItemTpl ID="ItemTpl2" runat="server">
                                                <Html>
                                                    <tpl>
                                                        <div class="search-item">
                                                                        <span>{Name}</span>  
                                                         </div>
					                            </tpl>
                                                </Html>
                                            </ItemTpl>
                                        </ListConfig>
                                        <Triggers>
                                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                        </Triggers>
                                        <Listeners>
                                            <Select Handler="this.getTrigger(0).show();"  />
                                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                            <TriggerClick Handler="if (index == 0) { 
                                                       this.clearValue(); 
                                                       this.getTrigger(0).hide();
                                                   }" />
                                        </Listeners>
                                    </ext:ComboBox>   
                                </td>
                                
                                <td>
                                    <ext:Button ID="btnLoad" runat="server" Text="Load" Width="100" Cls="btn btn-default" MarginSpec="25 10 10 10">
                                        
                                        <Listeners>
                                               
                                            <Click Fn="searchList">
                                            </Click>
                                        </Listeners>
                                    </ext:Button>
                                </td>
                            </tr>
                   
                        </table>
                    </Content>
                </ext:Container>
            </div>

            <ext:KeyMap ID="KeyMap1" runat="server" Target="={#{listingContainer}.getBody()}">
                <Binding>
                    <ext:KeyBinding Handler="#{btnLoad}.fireEvent('click');">
                        <Keys>
                            <ext:Key Code="ENTER" />
                        </Keys>
                    </ext:KeyBinding>
                </Binding>
            </ext:KeyMap>



        <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridDepartmentTransfer" runat="server" Cls="itemgrid"
                OnReadData="Store_ReadData">
                <Store>
                    <ext:Store ID="storeEmpList" runat="server" AutoLoad="true" OnReadData="Store_ReadData"
                        RemotePaging="true" RemoteSort="true">
                        <Proxy>
                            <ext:PageProxy />
                        </Proxy>
                            <Model>
                                <ext:Model ID="Model1" runat="server" IDProperty="BranchDepartmentId">
                                    <Fields>
                                        <ext:ModelField Name="BranchDepartmentId" Type="Int" />
                                        <ext:ModelField Name="FromDate" Type="String" />
                                        <ext:ModelField Name="FromDateEng" Type="Date" />
                                        <ext:ModelField Name="EmployeeId" Type="String" />
                                        <ext:ModelField Name="EmployeeName" Type="String" />
                                        <ext:ModelField Name="FromDepartment" Type="String" />
                                        <ext:ModelField Name="ToDepartment" Type="String" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                            <Sorters>
                                <ext:DataSorter Property="FromDateEng" Direction="ASC" />
                            </Sorters>        
                        </ext:Store>
                    </Store>
                    <ColumnModel ID="ColumnModel1" runat="server">
                        <Columns>
                            <ext:Column ID="colFromDate" runat="server" Text="Date" DataIndex="FromDate" MenuDisabled="true" Sortable="false" Align="Left" Width="150" />
                            <ext:DateColumn ID="colFromDateEng" runat="server" Text="Date(AD)" DataIndex="FromDateEng" Format="yyyy-MM-dd" MenuDisabled="true" Sortable="false" Align="Left" Width="150" />

                            <ext:Column ID="colEmployeeId" runat="server" Text="EIN" DataIndex="EmployeeId" MenuDisabled="true" Sortable="false" Align="Center" Width="80" />

                            <ext:Column ID="colEmployeeName" runat="server" Text="Name" DataIndex="EmployeeName" MenuDisabled="true" Sortable="false" Align="Left" Width="250" />
                            
                            <ext:Column ID="colFromBranch" runat="server" Text="From Department" DataIndex="FromDepartment" MenuDisabled="true" Sortable="false" Align="Left" Width="200" />
                            <ext:Column ID="colToBranch" runat="server" Text="To Department" DataIndex="ToDepartment" MenuDisabled="true" Sortable="false" Align="Left" Width="200" />
                                                         
                            <ext:CommandColumn ID="CommandColumn1" runat="server" Width="45" Text="" Align="Center">
                                <Commands>
                                    <ext:CommandSeparator />
                                    <ext:GridCommand Cls="editGridButton" ToolTip-Text="Edit" Text="<i class='fa fa-pencil'></i>"
                                        CommandName="Edit" />
                                </Commands>
                                <Listeners>
                                    <Command Handler="CommandHandler(command,record);" />
                                </Listeners>
                            </ext:CommandColumn>
                        </Columns>
                    </ColumnModel>
                    <Plugins>
                        <ext:FilterHeader ID="FilterHeader1" runat="server" OnCreateFilterableField="OnCreateFilterableField" />
                    </Plugins>
                    <BottomBar>                    
                    <ext:PagingToolbar ID="PagingToolbar1" runat="server" StoreID="storeEmpList" DisplayInfo="true">
                        <Items>
                            <ext:Label ID="lblPageSize" runat="server" Text="Page size:" />
                            <ext:ToolbarSpacer ID="ToolbarSpacer_listItems" runat="server" Width="10" />
                            <ext:ComboBox ID="cmbPageSize" runat="server" Width="80" SelectOnFocus="true" Selectable="true"
                                ValueField="Value" DisplayField="Text" ForceSelection="true" AllowBlank="false">
                                <Listeners>
                                    <Select Handler="searchList()" />
                                    <%--#{cmbPageSize}.pageSize = this.getValue();#{PagingToolbar1}.moveFirst();--%>
                                </Listeners>
                                <Items>
                                    <ext:ListItem Value="20" Text="20" />
                                    <ext:ListItem Value="50" Text="50" />
                                    <ext:ListItem Value="100" Text="100" />
                                </Items>
                                <SelectedItems>
                                    <ext:ListItem Index="0">
                                    </ext:ListItem>
                                </SelectedItems>
                            </ext:ComboBox>
                        </Items>
                    </ext:PagingToolbar>
                </BottomBar>
                                  
                </ext:GridPanel>

</div>

    
 <div class="alert alert-info" style="margin-top: 15px; height:50px;">
            <div style="float:right;">
                <ext:Button runat="server" AutoPostBack="true" OnClick="btnExport_Click" 
                            ID="btnExport"  Text="<i></i>Export To Excel">
                        </ext:Button>
            </div>
       </div>

</div>




<ext:Window ID="WDepartment" runat="server" Title="Department Transfer Details" Icon="Application"
        Width="650" Height="600" BodyPadding="5" Hidden="true" Modal="true">
        <Content>

            <table class="fieldTable" style="margin-left:20px;">
                <tr> 
                    <td>                    
                        <ext:ComboBox LabelSeparator="" ID="cmbEmployee" FieldLabel="Search Employee *" EmptyText="Search Employee" LabelWidth="118"
                            LabelAlign="Left" runat="server" DisplayField="Name" ValueField="EmployeeId" StoreID="storeSearch"
                            TypeAhead="false" Width="300" PageSize="9999" HideBaseTrigger="true" MinChars="1"
                            TriggerAction="All" ForceSelection="false">
                            <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                                <ItemTpl ID="ItemTpl1" runat="server">
                                    <Html>
                                        <tpl>
                                            <div class="search-item">
                                                            <span>{Name}</span>  
                                             </div>
					                </tpl>
                                    </Html>
                                </ItemTpl>
                            </ListConfig>
                            <Triggers>
                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                            </Triggers>
                            <Listeners>
                                <Select Handler="this.getTrigger(0).show(); EmpSelect();"  />
                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                            </Listeners>
                        </ext:ComboBox>                  
                        
                        <asp:RequiredFieldValidator ValidationGroup="SaveDptTransfer" ID="valEmployeeReq" ControlToValidate="cmbEmployee"
                            Display="None" runat="server" ErrorMessage="Please select an Employee."></asp:RequiredFieldValidator>
                    </td>
                </tr>
            </table>

            <div id="block" runat="server">
                <table style='clear: both; padding: 10px; margin-top: 10px;'>
                    <tr>
                        <td valign="top" style="padding-left: 10px;">                            
                            <ext:Image ID="image1" runat="server" ImageUrl="~/images/sample.jpg" Width="120" Height="120" />
                        </td>
                        <td valign="top" style="padding-left: 15px" class="items">                            
                            <ext:Label ID="lblEName" StyleHtmlCls="empName" runat="server" /><br />
                            <ext:Label ID="lblEWorkingIn" runat="server" Text="Working In" StyleHtmlCls="titleDesign" /><br />
                            <ext:Label ID="lblEBranch" runat="server" /><br />
                            <ext:Label ID="lblEDepartment" runat="server" /><br />
                            <ext:Label ID="lblESince" runat="server" /><br />
                            <ext:Label ID="lblInTime" runat="server" /><br />

                        </td>
                    </tr>
                </table>
            </div>
      

        <table class="fieldTable" style="margin-left:20px;">
            <tr>
                <td style="width:120px;">Letter Number</td>

                <td style="width:160px;">
                     <ext:TextField ID="txtLetterNumber" Width="150" runat="server" LabelAlign="Left" LabelSeparator="">
                    </ext:TextField>
                </td>

                <td style="width:120px;">Letter Date *</td>

                <td>
                     <pr:CalendarExtControl Width="150px" ID="calLetterDate" runat="server" LabelSeparator="" />

                    <asp:RequiredFieldValidator ID="rfvLetterDate" runat="server" ControlToValidate="calLetterDate"
                        Display="None" ErrorMessage="Letter date is required." ValidationGroup="SaveDptTransfer"></asp:RequiredFieldValidator>
                </td>

               
            </tr>

            <tr>
                <td style="width:120px;">Department *</td>

                <td style="width:160px;">
                    <ext:ComboBox ID="cmbDepartment" runat="server" ValueField="DepartmentId" DisplayField="Name" Width="150"
                          LabelSeparator="" ForceSelection="true" QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store1" runat="server">
                                    <Model>
                                        <ext:Model ID="Model3" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="DepartmentId" Type="String" />
                                                <ext:ModelField Name="Name" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                    <asp:RequiredFieldValidator ID="rfvDepartment" runat="server" ControlToValidate="cmbDepartment"
                            Display="None" ErrorMessage="Department is required." ValidationGroup="SaveDptTransfer"></asp:RequiredFieldValidator>
                </td>

                 <td style="width:120px;">Sub Department</td>

                <td>
                    <ext:ComboBox ID="cmbSubDepartment" runat="server" ValueField="SubDepartmentId" DisplayField="Name" Width="150"
                           LabelSeparator="" ForceSelection="true" QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store3" runat="server">
                                    <Model>
                                        <ext:Model ID="Model4" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="SubDepartmentId" Type="String" />
                                                <ext:ModelField Name="Name" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                </td>
            </tr>



            <tr>
                 <td style="width:120px;">Special Responsibility</td>

                <td style="width:160px;">
                    <ext:TextField ID="txtSpecialResponsibility" Width="150"
                        runat="server" LabelSeparator="">
                    </ext:TextField>
                </td>

                <td style="width:120px;">Applicable Date *</td>

                <td>
                    <pr:CalendarExtControl Width="150px" ID="calFromDate" runat="server" LabelSeparator="" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="calFromDate"
                                    Display="None" ErrorMessage="Applicable date is required." ValidationGroup="SaveDptTransfer"></asp:RequiredFieldValidator>
                </td>
            </tr>                      

            <tr>
                <td style="width:120px;">Note</td>

                <td colspan="3"> 
                    <ext:TextArea ID="txtNote" runat="server" LabelSeparator="" Rows="3" Width="430" Height="80" />
                </td>
            </tr>
        
            <tr>
               <td valign="bottom" colspan="4">
                        <div id="buttonsBar" runat="server" class="popupButtonDiv" style="float:right; margin-right:2px;">

                    <div style="width:100px;">
                    <ext:Button runat="server" ID="btnSave" Cls="btn btn-primary" Width="90" Text="<i></i>Save">
                        <DirectEvents>
                            <Click OnEvent="btnSave_Click">
                                <EventMask ShowMask="true" />
                            </Click>
                        </DirectEvents>
                        <Listeners>
                            <Click Handler="valGroup = 'SaveDptTransfer'; if(CheckValidation()) return this.disable(); else return false;">
                            </Click>
                        </Listeners>
                    </ext:Button>
                   </div>               

                    <ext:Button runat="server" ID="btnCancel" Cls="btn btn-default" Text="Cancel">
                                <DirectEvents>
                                    <Click OnEvent="btnCancel_Click">
                                        <EventMask ShowMask="true" />
                                    </Click>
                                </DirectEvents>
                                
                            </ext:Button>
                            </div>
                </td>
            </tr>

        </table>



        </Content>
</ext:Window>



</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
