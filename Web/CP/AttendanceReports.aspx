<%@ Page Title="AttendanceReports List" Language="C#" MasterPageFile="~/Master/Details.Master"
    AutoEventWireup="true" CodeBehind="AttendanceReports.aspx.cs" Inherits="Web.CP.AttendanceReports" %>

<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .itemStatus
        {
            /*            -webkit-border-radius: 4px 4px 0px 0px;
            -moz-border-radius: 4px 4px 0px 0px;
            border-radius: 4px 4px 0px 0px;
            background-image: none !important; */
            background-color: #EBF3FF;
            color: #048FC2;
            padding: 7px 10px 7px 5px;
            border-bottom: 1px solid #048FC2 !important;
            font-size: 14px;
            font-family: Open Sans;
            font-weight: bold;
        }
        .x-toolbar-default a
        {
            color: White;
            font-family: Arial, 'DejaVu Sans' , 'Liberation Sans' ,Freesans,sans-serif !important;
            font-size: 13px;
        }
        .itemStatus .x-panel-header-text-default
        {
            color: White !important;
        }
        .dbPanelBodyStyle
        {
            background-color: White !important;
        }
        .tdStyle
        {
            width: 480px;
        }
        .description
        {
            font-family: Open Sans;
            font-size: 12px;
            margin-top: 6px;
            margin-bottom: 6px;
            margin-left: 4px;
            font-style: italic;
        }
        .innerTable
        {
            width: 100%;
            margin-bottom: 20px;
        }
        .innerTable .maintd
        {
            width: 50%;
            padding: 4px;
            margin-bottom: 4px;
            border-bottom: solid 1px #DDD;
            cursor: pointer;
        }
        .innerTable a
        {
            display: block;
            height: 15px; /*background-image: url(../Styles/images/report.png);*/
            background-repeat: no-repeat;
            padding-left: 25px;
            padding-top: 5px;
        }
        td:hover
        {
            background-color: ##EBF3FF !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <ext:ResourceManager ID="ResourceManager2"  DisableViewState="false"  ShowWarningOnAjaxFailure="false" runat="server">
    </ext:ResourceManager>
    <asp:Panel ID="pnlContainer" runat="server" class="contentArea">
        <%-- <h2>Company List</h2>--%>
        <div>
            <span style="float: left">
                <h3>
                    Attendance Reports
                </h3>
            </span>
        </div>
        <div style='clear: both'>
        </div>
        <table class="contentTable">
            <tr>
                <td class="tdStyle" valign="top">
                    <div class="itemStatus">
                        Attendance Reports
                    </div>
                    <%--  <div class="description">
                        Show your financial reports
                    </div>--%>
                    <table class="innerTable" border="0">
                        <tr runat="server">
                            <td class="maintd" runat="server">
                                <span style="float: left;">
                                    <ext:ImageButton ID="A28" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                        OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                        PressedImageUrl="~/images/favEnable.gif">
                                    </ext:ImageButton>
                                </span>
                                <asp:HyperLink runat="server" Text="Daily Attendance Details" NavigateUrl='~/Attendance/AttendanceDailyReport.aspx' />
                            </td>
                            <td id="Td1" class="maintd" runat="server">
                                <span style="float: left;">
                                    <ext:ImageButton ID="ImageButton1" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                        OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                        PressedImageUrl="~/images/favEnable.gif">
                                    </ext:ImageButton>
                                </span>
                                <asp:HyperLink ID="HyperLink1" runat="server" Text="Monthly Attendance Details" NavigateUrl='~/Attendance/HRAttendanceEmpMonthDetails.aspx' />
                            </td>
                        </tr>
                        <tr class="maintd" runat="server">
                            <td id="Td2" class="maintd" runat="server">
                                <span style="float: left;">
                                    <ext:ImageButton ID="ImageButton2" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                        OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                        PressedImageUrl="~/images/favEnable.gif">
                                    </ext:ImageButton>
                                </span>
                                <asp:HyperLink ID="HyperLink2" runat="server" Text="Attendance Device Mapping" NavigateUrl='~/CP/AttendanceDeviceMapping.aspx' />
                            </td>
                            <td id="Td3" class="maintd" runat="server">
                                <span style="float: left;">
                                    <ext:ImageButton ID="ImageButton3" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                        OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                        PressedImageUrl="~/images/favEnable.gif">
                                    </ext:ImageButton>
                                </span>
                                <asp:HyperLink ID="HyperLink3" runat="server" Text="Attendance Summary" NavigateUrl='~/Attendance/HRAttendanceEmpMontlySummary.aspx' />
                            </td>
                        </tr>
                         <tr id="Tr1" class="maintd" runat="server">
                            <td id="Td4" class="maintd" runat="server">
                                <span style="float: left;">
                                    <ext:ImageButton ID="ImageButton4" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                        OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                        PressedImageUrl="~/images/favEnable.gif">
                                    </ext:ImageButton>
                                </span>
                                <asp:HyperLink ID="HyperLink4" runat="server" Text="Attendance Exception Entry" NavigateUrl='~/Attendance/AttendanceExceptionReport.aspx' />
                            </td>
                          
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
