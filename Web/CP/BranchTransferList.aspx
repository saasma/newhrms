﻿<%@ Page Title="Branch Transfer" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="BranchTransferList.aspx.cs" Inherits="Web.CP.BranchTransferList" %>

<%@ MasterType VirtualPath="~/Master/NewDetails.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .empName
        {
            font-size: 20px;
            color: #439EC6;
            font-weight: bold;
        }
        .titleDesign
        {
            font-size: 15px;
            color: #000000;
            text-decoration: underline;
            padding-top: 10px;
        }
        
        .items span
        {
            display: block;
            padding-top: 3px;
            font-weight: bold;
        }
        
        .paddinAll
        {
            padding: 10px;
        }
       <%-- table tr td
        {
            padding: 0 10px 8px 0;
        }--%>
        h3
        {
            font: bold 13px Arial;
        }
        .marginTopss
        {
            margin-top: 10px;
            display: block;
        }
        .loanAdvTable
        {
            background-color: white;
            width: 60px;
            border: 1px solid white !important;
            padding: 0px;
        }
        .tableLightColor th
        {
            text-align: left;
        }
        strong
        {
            display: block;
            padding-bottom: 3px;
        }
        .tableLightColor input[type=text]
        {
            height: 20px;
        }
        .clsColor
        {
            background-color: #e8f1ff;
        }
         .BlueCls
        {
            color:Blue;
        }
          .x-grid-cell-inner{padding: 5px 5px 5px 5px;}
.x-column-header-inner {padding: 7px 5px 7px 5px;}
    </style>
     <link href="../css/core.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>
    <script type="text/javascript">

    function searchList() {
             <%=gridBranchTransfer.ClientID %>.getStore().pageSize = <%=cmbPageSize.ClientID %>.getValue();
            <%=PagingToolbar1.ClientID %>.doRefresh();
        }

    function EmpSelect()
    {
        <%=hdnEmployeeId.ClientID %>.setValue(<%=cmbEmployee.ClientID %>.getValue());
        <%=btnSelectEmp.ClientID %>.fireEvent('click');
    }

    var CommandHandler = function(command, record){
            <%= hdnBranchDepartmentId.ClientID %>.setValue(record.data.BranchDepartmentId);
            <%=hdnEmployeeId.ClientID %>.setValue(record.data.EmployeeId);
                if(command=="Edit")
                {
                    <%= btnEdit.ClientID %>.fireEvent('click');
                }
                else
                {
                    <%= btnDelete.ClientID %>.fireEvent('click');
                }

             };

    //ImageCommandHandler
    
    var ImageCommandHandler = function(value, command,record,p2) {            
            if(command == 'View')
            { 
                 popup('Id=' + record.data.BranchDepartmentId);
            }
       };

    function SelEmpSearch()
    {
        <%=hdnEmpSearch.ClientID %>.setValue(<%=cmbEmpSearch.ClientID %>.getValue());
    }

    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <ext:Hidden ID="hdnEmployeeId" runat="server" />
    <ext:Hidden ID="hdnBranchDepartmentId" runat="server" />
    <ext:Hidden ID="hdnMsg" runat="server" />
    <ext:Hidden ID="hdnEmpSearch" runat="server" />
    <ext:Hidden ID="hdnIsEng" runat="server" />
    <ext:LinkButton ID="btnSelectEmp" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnSelectEmp_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:LinkButton ID="btnEdit" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnEdit_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:LinkButton ID="btnDelete" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnDelete_Click">
                <Confirmation ConfirmRequest="true" Message="Are you sure,you want to delete the transfer?" />
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Branch Transfer
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel" style="margin-top: 0px !important; padding-top: 0px !important;">
        <div class="innerLR">
            <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                <Proxy>
                    <ext:AjaxProxy Url="../Handler/EmpSearchID.ashx">
                        <ActionMethods Read="GET" />
                        <Reader>
                            <ext:JsonReader Root="plants" TotalProperty="total" />
                        </Reader>
                    </ext:AjaxProxy>
                </Proxy>
                <Model>
                    <ext:Model ID="Model5" IDProperty="Name" Name="ItemLineModel" runat="server">
                        <Fields>
                            <ext:ModelField Name="Name" Type="String" />
                            <ext:ModelField Name="EmployeeId" Type="String" />
                        </Fields>
                    </ext:Model>
                </Model>
            </ext:Store>
            <div class="alert alert-info" style="margin-top: 10px; padding-top: 0px!important;">
                <ext:Container ID="listingContainer" runat="server">
                    <Content>
                        <table>
                            <tr>
                                <td>
                                    <ext:DateField ID="txtFromDate" runat="server" FieldLabel="From" LabelSeparator=""
                                        LabelAlign="Top" Width="110">
                                        <Plugins>
                                            <ext:GenericPlugin ID="HighlightDates1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                                        </Plugins>
                                    </ext:DateField>
                                </td>
                                <td>
                                    <pr:CalendarExtControl Width="110px" FieldLabel="&nbsp;" StyleSpec="margin-left:15px;"
                                        ID="txtFromDateNep" runat="server" LabelAlign="Top" LabelSeparator="" />
                                </td>
                                <td>
                                    <pr:CalendarExtControl Width="110px" FieldLabel="To" StyleSpec="margin-left:15px;"
                                        ID="txtToDateNep" runat="server" LabelAlign="Top" LabelSeparator="" />
                                </td>
                                <td>
                                    <ext:DateField ID="txtToDate" StyleSpec="margin-left:15px;" runat="server" FieldLabel="&nbsp;"
                                        LabelSeparator="" LabelAlign="Top" Width="110">
                                        <Plugins>
                                            <ext:GenericPlugin ID="GenericPlugin1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                                        </Plugins>
                                    </ext:DateField>
                                </td>
                                <td style='padding-left: 15px;'>
                                    <ext:ComboBox LabelSeparator="" ID="cmbEmpSearch" FieldLabel="Search Employee" EmptyText="Employee Name"
                                        LabelWidth="80" LabelAlign="Top" runat="server" DisplayField="Name" ValueField="EmployeeId"
                                        StoreID="storeSearch" TypeAhead="false" Width="200" PageSize="9999" HideBaseTrigger="true"
                                        MinChars="1" TriggerAction="All" ForceSelection="false">
                                        <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                                            <ItemTpl ID="ItemTpl2" runat="server">
                                                <Html>
                                                    <tpl>
                                                        <div class="search-item">
                                                                        <span>{Name}</span>  
                                                         </div>
					                            </tpl>
                                                </Html>
                                            </ItemTpl>
                                        </ListConfig>
                                        <Triggers>
                                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                        </Triggers>
                                        <Listeners>
                                            <Select Handler="this.getTrigger(0).show(); SelEmpSearch();" />
                                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                            <TriggerClick Handler="if (index == 0) { 
                                                       this.clearValue(); 
                                                       this.getTrigger(0).hide();
                                                   }" />
                                        </Listeners>
                                    </ext:ComboBox>
                                </td>
                                <td style="padding-left: 10px">
                                    <ext:ComboBox LabelAlign="Top" ID="cmbEventFilter" runat="server" ValueField="EventID"
                                        DisplayField="Name" Width="150" LabelSeparator="" FieldLabel="Event Type" ForceSelection="true"
                                        QueryMode="Local">
                                        <Store>
                                            <ext:Store ID="Store5" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model7" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="EventID" Type="String" />
                                                            <ext:ModelField Name="Name" Type="String" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <Triggers>
                                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                        </Triggers>
                                        <Listeners>
                                            <Select Handler="this.getTrigger(0).show();" />
                                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                            <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                                        </Listeners>
                                    </ext:ComboBox>
                                </td>
                                <td style="padding-left: 10px">
                                    <ext:ComboBox LabelAlign="Top" ID="cmbShowJoinBranchAlso" runat="server" Width="150"
                                        LabelSeparator="" FieldLabel="Show Joined Branch" ForceSelection="true" QueryMode="Local">
                                        <Items>
                                            <ext:ListItem Text="Yes" Value="true" />
                                            <ext:ListItem Text="No" Value="false" />
                                        </Items>
                                        <SelectedItems>
                                            <ext:ListItem Index="0" />
                                        </SelectedItems>
                                    </ext:ComboBox>
                                </td>
                                <td>
                                    <ext:Button ID="btnLoad" runat="server" Text="Load" Width="100" Cls="btn btn-default"
                                        MarginSpec="25 10 10 10">
                                        <Listeners>
                                            <Click Fn="searchList">
                                            </Click>
                                        </Listeners>
                                    </ext:Button>
                                </td>
                                <td>
                                    <ext:Button ID="btnNewTransfer" MarginSpec="25 10 10 10" Width="100" Height="30"
                                        runat="server" Text="New Transfer">
                                        <DirectEvents>
                                            <Click OnEvent="btnNewTransfer_Click">
                                                <EventMask ShowMask="true" />
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </td>
                            </tr>
                        </table>
                    </Content>
                </ext:Container>
            </div>
            <ext:KeyMap ID="KeyMap1" runat="server" Target="={#{listingContainer}.getBody()}">
                <Binding>
                    <ext:KeyBinding Handler="#{btnLoad}.fireEvent('click');">
                        <Keys>
                            <ext:Key Code="ENTER" />
                        </Keys>
                    </ext:KeyBinding>
                </Binding>
            </ext:KeyMap>
            <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridBranchTransfer" runat="server"
                Cls="itemgrid" OnReadData="Store_ReadData"  Scroll="Horizontal">
                <Store>
                    <ext:Store ID="storeEmpList" runat="server" AutoLoad="true" OnReadData="Store_ReadData"
                        RemotePaging="true" RemoteSort="true">
                        <Proxy>
                            <ext:PageProxy />
                        </Proxy>
                        <Model>
                            <ext:Model ID="Model1" runat="server" IDProperty="BranchDepartmentId">
                                <Fields>
                                    <ext:ModelField Name="BranchDepartmentId" Type="Int" />
                                    <ext:ModelField Name="FromDate" Type="String" />
                                    <ext:ModelField Name="FromDateEng" Type="Date" />
                                    <ext:ModelField Name="EmployeeId" Type="String" />
                                    <ext:ModelField Name="Name" Type="String" />
                                    <ext:ModelField Name="FromBranch" Type="String" />
                                    <ext:ModelField Name="ToBranch" Type="String" />
                                    <ext:ModelField Name="FromDepartment" Type="String" />
                                    <ext:ModelField Name="ToDepartment" Type="String" />
                                    <ext:ModelField Name="IdCardNo" Type="String" />
                                    <ext:ModelField Name="AppointmentEngDate" Type="Date" />
                                    <ext:ModelField Name="LevelPosition" Type="String" />
                                    <ext:ModelField Name="RowNumber" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Sorters>
                            <ext:DataSorter Property="FromDateEng" Direction="ASC" />
                        </Sorters>
                    </ext:Store>
                </Store>
                <ColumnModel ID="ColumnModel1" runat="server">
                    <Columns>
                        <ext:Column Locked="true"  ID="Column4" runat="server" Text="SN" DataIndex="RowNumber" MenuDisabled="true"
                            Sortable="false" Align="Right" Width="40" />
                        <ext:Column Locked="true"  ID="colFromDate" runat="server" Text="Transfer Date" DataIndex="FromDate"
                            MenuDisabled="true" Sortable="false" Align="Right" Width="90" />
                        <ext:DateColumn Locked="true"  ID="colFromDateEng" runat="server" Text="Eng Date" DataIndex="FromDateEng"
                            Format="dd-MMM-yyyy" MenuDisabled="true" Sortable="false" Align="Right" Width="100" />
                        <ext:Column Locked="true"  ID="colEmployeeId" runat="server" Text="EIN" DataIndex="EmployeeId" MenuDisabled="true"
                            Sortable="false" Align="Right" Width="50" />
                        <ext:Column Locked="true"  ID="Column1" runat="server" Text="I No" DataIndex="IdCardNo" MenuDisabled="true"
                            Sortable="false" Align="Right" Width="50" />
                        <ext:Column Locked="true"  ID="colEmployeeName" runat="server" Text="Name" DataIndex="Name" MenuDisabled="true"
                            Sortable="false" Align="Left" Width="150" />
                        <ext:Column ID="colFromBranch" runat="server" Text="From Branch" DataIndex="FromBranch"
                            MenuDisabled="true" Sortable="false" Align="Left" Width="150" />
                        <ext:Column ID="Column2" runat="server" Text="From Department" DataIndex="FromDepartment"
                            MenuDisabled="true" Sortable="false" Align="Left" Width="150" />
                        <ext:Column ID="colToBranch" runat="server" Text="To Branch" DataIndex="ToBranch"
                            MenuDisabled="true" Sortable="false" Align="Left" Width="150" />
                        <ext:Column ID="Column211" runat="server" Text="To Department" DataIndex="ToDepartment"
                            MenuDisabled="true" Sortable="false" Align="Left" Width="150" />
                        <ext:ImageCommandColumn ID="ColumnTeam" runat="server" Width="120" Text="Action"
                            MenuDisabled="true" Sortable="false" Align="Center">
                            <Commands>
                                <ext:ImageCommand CommandName="View" Text="Review Team" Cls="BlueCls">
                                </ext:ImageCommand>
                            </Commands>
                            <Listeners>
                                <Command Fn="ImageCommandHandler">
                                </Command>
                            </Listeners>
                        </ext:ImageCommandColumn>
                        <ext:CommandColumn ID="CommandColumn1" runat="server" Width="80" Text="" Align="Center">
                            <Commands>
                                <ext:CommandSeparator />
                                <ext:GridCommand Cls="editGridButton" ToolTip-Text="Edit" Text="<i class='fa fa-pencil'></i>"
                                    CommandName="Edit" />
                                <ext:GridCommand Cls="editGridButton" ToolTip-Text="Delete" Text="<i class='fa fa-trash-o'></i>"
                                    CommandName="Delete" />
                            </Commands>
                            <Listeners>
                                <Command Handler="CommandHandler(command,record);" />
                            </Listeners>
                        </ext:CommandColumn>
                        <ext:DateColumn ID="DateColumn1" runat="server" Text="Appoint Date" DataIndex="AppointmentEngDate"
                            Format="dd-MMM-yyyy" MenuDisabled="true" Sortable="false" Align="Right" Width="90" />
                        <ext:Column ID="Column3" runat="server" Text="Curr Position" DataIndex="LevelPosition"
                            MenuDisabled="true" Sortable="false" Align="Left" Width="100" />
                    </Columns>
                </ColumnModel>
                <%-- <Plugins>
                    <ext:FilterHeader ID="FilterHeader1" runat="server" OnCreateFilterableField="OnCreateFilterableField" />
                </Plugins>--%>
                <BottomBar>
                    <ext:PagingToolbar Height="40" ID="PagingToolbar1" runat="server" StoreID="storeEmpList"
                        DisplayInfo="true">
                        <Items>
                            <ext:Label ID="lblPageSize" runat="server" Text="Page size:" />
                            <ext:ToolbarSpacer ID="ToolbarSpacer_listItems" runat="server" Width="10" />
                            <ext:ComboBox ID="cmbPageSize" runat="server" Width="80" SelectOnFocus="true" Selectable="true"
                                ValueField="Value" DisplayField="Text" ForceSelection="true" AllowBlank="false">
                                <Listeners>
                                    <Select Handler="searchList()" />
                                    <%--#{cmbPageSize}.pageSize = this.getValue();#{PagingToolbar1}.moveFirst();--%>
                                </Listeners>
                                <Items>
                                    <ext:ListItem Value="20" Text="20" />
                                    <ext:ListItem Value="50" Text="50" />
                                    <ext:ListItem Value="100" Text="100" />
                                </Items>
                                <SelectedItems>
                                    <ext:ListItem Index="0">
                                    </ext:ListItem>
                                </SelectedItems>
                            </ext:ComboBox>
                        </Items>
                    </ext:PagingToolbar>
                </BottomBar>
            </ext:GridPanel>
        </div>
        <div class="alert alert-info" style="margin-top: 15px; height: 50px;">
            <div style="float: right;">
                <table>
                    <tr>
                        <td>
                            <ext:Button runat="server" AutoPostBack="true" OnClick="btnExport_Click" ID="btnExport"
                         Text="<i></i>Export To Excel">
                        </ext:Button>
                        </td>
                        <td>
                             <asp:LinkButton ID="btnImportExcel"  runat="server" Text="Import" OnClientClick="BranchTransferHistoryImport();return false;"
                            CssClass=" excel tiptip" Style="float: left;width: 84px;" />
                            <%-- <ext:Button ID="btnImportExcel" runat="server" OnClientClick="BranchTransferHistoryImport();return false;" Text="<i></i>Import From Excel" MarginSpec="0 0 0 10">
                          </ext:Button>--%>
                        </td>
                    </tr>
                </table>   
            </div>
        </div>
    </div>
    <ext:Window ID="WBranchTransfer" runat="server" Title="Branch Transfer Details" Icon="Application"
        Width="730" Height="600" BodyPadding="5" Hidden="true" Modal="true">
        <Content>
            <table class="fieldTable" style="margin-left: 20px;">
                <tr>
                    <td>
                        <ext:ComboBox LabelSeparator="" ID="cmbEmployee" FieldLabel="Search Employee *" EmptyText="Search Employee"
                            LabelWidth="120" LabelAlign="Left" runat="server" DisplayField="Name" ValueField="EmployeeId"
                            StoreID="storeSearch" TypeAhead="false" Width="350" PageSize="9999" HideBaseTrigger="true"
                            MinChars="1" TriggerAction="All" ForceSelection="false">
                            <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                                <ItemTpl ID="ItemTpl1" runat="server">
                                    <Html>
                                        <tpl>
                                            <div class="search-item">
                                                            <span>{Name}</span>  
                                             </div>
					                </tpl>
                                    </Html>
                                </ItemTpl>
                            </ListConfig>
                            <Triggers>
                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                            </Triggers>
                            <Listeners>
                                <Select Handler="this.getTrigger(0).show(); EmpSelect();" />
                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                            </Listeners>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator ValidationGroup="SaveBrTran" ID="valEmployeeReq" ControlToValidate="cmbEmployee"
                            Display="None" runat="server" ErrorMessage="Please select an Employee."></asp:RequiredFieldValidator>
                    </td>
                </tr>
            </table>
            <div id="block" runat="server">
                <table style='clear: both; padding: 10px; margin-top: 10px;'>
                    <tr>
                        <td valign="top" style="padding-left: 10px;">
                            <ext:Image ID="image1" runat="server" ImageUrl="~/images/sample.jpg" Width="120"
                                Height="120" />
                        </td>
                        <td valign="top" style="padding-left: 20px" class="items">
                            <ext:Label ID="lblEName" StyleHtmlCls="empName" runat="server" />
                            <br />
                            <ext:Label ID="lblEININo" runat="server" Text="" StyleHtmlCls="titleDesign" />
                            <br />
                            <ext:Label ID="lblEBranch" runat="server" />
                            <br />
                            <ext:Label ID="lblEDepartment" runat="server" />
                            <br />
                            <ext:Label ID="lblESince" runat="server" />
                            <br />
                            <ext:Label ID="lblInTime" runat="server" />
                            <br />
                        </td>
                    </tr>
                </table>
            </div>
            <table class="fieldTable" style="margin-left: 20px;">
                <tr>
                    <td colspan="4">
                        <ext:ComboBox ID="cmbEventType" runat="server" ValueField="EventID" DisplayField="Name"
                            Width="300" LabelWidth="115" LabelSeparator="" FieldLabel="Event Type" ForceSelection="true"
                            QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store4" runat="server">
                                    <Model>
                                        <ext:Model ID="Model6" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="EventID" Type="String" />
                                                <ext:ModelField Name="Name" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator ID="reqdEventType" runat="server" ControlToValidate="cmbEventType"
                            Display="None" ErrorMessage="Event is required." ValidationGroup="SaveBrTran"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td style="width: 120px;">
                        Letter Number
                    </td>
                    <td style="width: 160px;">
                        <ext:TextField ID="txtLetterNumber" Width="180" runat="server" LabelAlign="Left"
                            LabelSeparator="">
                        </ext:TextField>
                    </td>
                    <td style="width: 120px;">
                        Letter Date *
                    </td>
                    <td>
                        <pr:CalendarExtControl Width="180" ID="calLetterDate" runat="server" LabelSeparator="" />
                        <asp:RequiredFieldValidator ID="rfvLetterDate" runat="server" ControlToValidate="calLetterDate"
                            Display="None" ErrorMessage="Letter date is required." ValidationGroup="SaveBrTran"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td style="width: 120px;">
                        Transferred Branch *
                    </td>
                    <td style="width: 160px;">
                        <ext:ComboBox ID="cmbBranch" runat="server" ValueField="BranchId" DisplayField="BranchName"
                            Width="180" LabelSeparator="" ForceSelection="true" QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store2" runat="server">
                                    <Model>
                                        <ext:Model ID="Model2" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="BranchId" Type="String" />
                                                <ext:ModelField Name="BranchName" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <DirectEvents>
                                <Select OnEvent="Branch_Select" />
                            </DirectEvents>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator ID="rfvBranch" runat="server" ControlToValidate="cmbBranch"
                            Display="None" ErrorMessage="Branch is required." ValidationGroup="SaveBrTran"></asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 120px;">
                        Department *
                    </td>
                    <td>
                        <ext:ComboBox ID="cmbDepartment" runat="server" ValueField="DepartmentId" DisplayField="Name"
                            Width="180" LabelSeparator="" ForceSelection="true" QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store1" runat="server">
                                    <Model>
                                        <ext:Model ID="Model3" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="DepartmentId" Type="String" />
                                                <ext:ModelField Name="Name" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator ID="rfvDepartment" runat="server" ControlToValidate="cmbDepartment"
                            Display="None" ErrorMessage="Department is required." ValidationGroup="SaveBrTran"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td style="width: 120px;">
                        Sub Department
                    </td>
                    <td style="width: 160px;">
                        <ext:ComboBox ID="cmbSubDepartment" runat="server" ValueField="SubDepartmentId" DisplayField="Name"
                            Width="180" LabelSeparator="" ForceSelection="true" QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store3" runat="server">
                                    <Model>
                                        <ext:Model ID="Model4" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="SubDepartmentId" Type="String" />
                                                <ext:ModelField Name="Name" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                    </td>
                    <td style="width: 120px;">
                        Special Responsibility
                    </td>
                    <td>
                        <ext:TextField ID="txtSpecialResponsibility" Width="180" runat="server" LabelSeparator="">
                        </ext:TextField>
                    </td>
                </tr>
                <tr>
                    <td style="width: 120px;">
                        Departure Date *
                    </td>
                    <td style="width: 160px;">
                        <pr:CalendarExtControl Width="180" ID="calDepartureDate" runat="server" LabelSeparator="" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="calDepartureDate"
                            Display="None" ErrorMessage="Departure date is required." ValidationGroup="SaveBrTran"></asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 120px;">
                        Applicable Date *
                    </td>
                    <td style="width: 160px;">
                        <pr:CalendarExtControl Width="180" ID="calFromDate" runat="server" LabelSeparator="" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="calFromDate"
                            Display="None" ErrorMessage="Applicable date is required." ValidationGroup="SaveBrTran"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td style="width: 120px;">
                        Note
                    </td>
                    <td colspan="3">
                        <ext:TextArea ID="txtNote" runat="server" LabelSeparator="" Rows="3" Width="510"
                            Height="80" />
                    </td>
                </tr>
                <tr>
                    <td valign="bottom" colspan="4">
                        <div id="buttonsBar" runat="server" class="popupButtonDiv" style="float: right;">
                            <div style="width: 100px;">
                                <ext:Button runat="server" ID="btnSave" Cls="btn btn-primary" Width="90" Text="<i></i>Save">
                                    <DirectEvents>
                                        <Click OnEvent="btnSave_Click">
                                            <EventMask ShowMask="true" />
                                        </Click>
                                    </DirectEvents>
                                    <Listeners>
                                        <Click Handler="valGroup = 'SaveBrTran'; if(CheckValidation()) return this.disable(); else return false;">
                                        </Click>
                                    </Listeners>
                                </ext:Button>
                            </div>
                            <ext:Button runat="server" ID="btnCancel" Cls="btn btn-default" Text="Cancel">
                                <Listeners>
                                    <Click Handler="#{WBranchTransfer}.hide();" />
                                </Listeners>
                            </ext:Button>
                        </div>
                    </td>
                </tr>
            </table>
        </Content>
    </ext:Window>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
