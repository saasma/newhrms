<%@ Page Title="Opening Workdays" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="OpeningWorkdaysEntry.aspx.cs" Inherits="Web.OpeningWorkdaysEntry" %>

<%@ Import Namespace="BLL.Manager" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function refreshWindow() {



            __doPostBack('Refresh', 0);


        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Opening workdays
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="attribute" style="padding: 10px">
            Search &nbsp;
            <asp:TextBox ID="txtEmployeeName" AutoPostBack="true" runat="server" Width="146px"
                OnTextChanged="txtEmployeeName_TextChanged"></asp:TextBox>
            <td>
                <asp:LinkButton ID="btnExport" runat="server" Text="Excel import" OnClientClick="taxImportPopup();return false;"
                    CssClass=" excel marginRight tiptip" Style="float: left;" />
            </td>
        </div>
        <div class="clear">
            <cc1:TextBoxWatermarkExtender ID="TBWE2" runat="server" TargetControlID="txtEmployeeName"
                WatermarkText="Enter employee name" WatermarkCssClass="watermarked" />
            <cc2:emptydisplaygridview width="100%" cssclass="table table-primary mb30 table-bordered table-hover"
                style="margin-bottom: 0px" useaccessibleheader="true" showheaderwhenempty="True"
                id="gvw" runat="server" datakeynames="EmployeeId" autogeneratecolumns="False"
                cellpadding="4" gridlines="None" allowsorting="True" showfooterwhenempty="False">
                <%--                <RowStyle BackColor="#E3EAEB" />--%>
                <columns>
                    <asp:BoundField HeaderStyle-Width="30px" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:000}"
                        DataField="EmployeeId" HeaderText="EIN"></asp:BoundField>
                    <asp:TemplateField HeaderText="Name" HeaderStyle-Width="100px">
                        <ItemTemplate>
                            <%# Eval("Name") %>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" Width="150px" />
                    </asp:TemplateField>
                    <asp:TemplateField Visible="false" HeaderStyle-HorizontalAlign="Left" HeaderText="Opening workdays for Gratuity"
                        HeaderStyle-Width="120px">
                        <ItemTemplate>
                            <asp:TextBox Width="100px" CssClass='calculationInput' Enabled='<%# !CommonManager.IsFirstSalaryGenerated() %>'
                                Style='text-align: right' ID="txtWorkdays" Text='<%# Eval("Workdays") %>' runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator SetFocusOnError="true" Display="None" ID="valOpeningWorkdays1"
                                ControlToValidate="txtWorkdays" ValidationGroup="Balance" runat="server" ErrorMessage="Workdays is required." />
                            <asp:CompareValidator SetFocusOnError="true" ValueToCompare="0" Display="None" Operator="DataTypeCheck"
                                Type="Double" ID="valOpeningBalance" ControlToValidate="txtWorkdays" ValidationGroup="Balance"
                                runat="server" ErrorMessage="Invalid workdays."></asp:CompareValidator>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Workdays Adjustment"
                        HeaderStyle-Width="120px">
                        <ItemTemplate>
                            <asp:TextBox Width="100px" CssClass='calculationInput' Style='text-align: right'
                                ID="txtWorkdaysAdjustment" Text='<%# Eval("WorkdayAdjustment") %>' runat="server"></asp:TextBox>
                            <asp:CompareValidator SetFocusOnError="true" Display="None" Operator="DataTypeCheck"
                                Type="Double" ID="valOpeningBalance11" ControlToValidate="txtWorkdaysAdjustment"
                                ValidationGroup="Balance" runat="server" ErrorMessage="Invalid workdays adjustment."></asp:CompareValidator>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Unpaid leaves for Gratuity/Festival income"
                        HeaderStyle-Width="120px">
                        <ItemTemplate>
                            <asp:TextBox Width="100px" CssClass='calculationInput' 
                                Style='text-align: right' ID="txtUnpaidLeavesForGratuity" Text='<%# Eval("UnpaidLeavesForGratuity") %>'
                                runat="server"></asp:TextBox>
                      
                            <asp:CompareValidator SetFocusOnError="true" ValueToCompare="0" Display="None" Operator="DataTypeCheck"
                                Type="Integer" ID="valOpeningBalance1" ControlToValidate="txtUnpaidLeavesForGratuity"
                                ValidationGroup="Balance" runat="server" ErrorMessage="Invalid unpaid leave."></asp:CompareValidator>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Opening Gratuity"
                        HeaderStyle-Width="120px">
                        <ItemTemplate>
                            <asp:TextBox Width="100px" CssClass='calculationInput' Enabled='<%# Convert.ToBoolean(Eval("IsEditable")) %>'
                                Style='text-align: right' ID="txtGratuity" Text='<%# GetCurrency( Eval("OpeningGratuityAmount") )%>'
                                runat="server"></asp:TextBox>
                           <%-- <asp:RequiredFieldValidator SetFocusOnError="true" Display="None" ID="valOpeningWorkdays22"
                                ControlToValidate="txtGratuity" ValidationGroup="Balance" runat="server"
                                ErrorMessage="Gratuity is required." />--%>
                            <asp:CompareValidator SetFocusOnError="true" ValueToCompare="0" Display="None" Operator="DataTypeCheck"
                                Type="Double" ID="valOpeningBalance111" ControlToValidate="txtGratuity"
                                ValidationGroup="Balance" runat="server" ErrorMessage="Invalid gratuity value."></asp:CompareValidator>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Gratuity Amount Adjustment"
                        HeaderStyle-Width="120px">
                        <ItemTemplate>
                            <asp:TextBox Width="100px" CssClass='calculationInput'
                                Style='text-align: right' ID="txtGratuityAdjustment" Text='<%# GetCurrency( Eval("GratuityAmountAdjustment ") )%>'
                                runat="server"></asp:TextBox>
                         
                           <%-- <asp:CompareValidator SetFocusOnError="true" ValueToCompare="0" Display="None" Operator="DataTypeCheck"
                                Type="Currency" ID="valOpeningBalance11111" ControlToValidate="txtGratuityAdjustment"
                                ValidationGroup="Balance" runat="server" ErrorMessage="Invalid adjustment value."></asp:CompareValidator>--%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="200px" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                        </ItemTemplate>
                    </asp:TemplateField>
                </columns>
                <pagersettings mode="NumericFirstLast" nextpagetext="Next �" previouspagetext="� Prev"
                    firstpagetext="First" lastpagetext="Last" />
                <rowstyle cssclass="odd" />
                <alternatingrowstyle cssclass="even" />
                <emptydatatemplate>
                    <b>No list </b>
                </emptydatatemplate>
            </cc2:emptydisplaygridview>
            <uc1:PagingCtl ID="pagintCtl" OnNextRecord="btnNext_Click" OnPrevRecord="btnPrevious_Click"
                OnDropDownShowRecordsChanged="ddlRecords_SelectedIndexChanged" runat="server" />
        </div>
        <div class="buttonsDivSection" style="margin-top: 10px">
            <%--  <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Balance" DisplayMode="List"
            ShowSummary="false" ShowMessageBox="true" runat="server"></asp:ValidationSummary>--%>
            <asp:Button ID="btnUpdate" CssClass="btn btn-primary btn-sect btn-sm" Visible="true"
                OnClientClick="valGroup='Balance';return CheckValidation();" ValidationGroup="Balance"
                runat="server" Text="Save Changes" OnClick="btnUpdate_Click" />
        </div>
    </div>
</asp:Content>
