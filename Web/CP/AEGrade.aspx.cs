﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using Utils.Base;
using BLL;
using DAL;
using Utils.Web;
using Utils.Helper;
using System.Text.RegularExpressions;

namespace Web.CP
{
    public partial class AEGrade : BasePage
    {
        /// <summary>
        /// 0-> Data Insert Mode
        /// 1-> Data Update Mode
        /// </summary>
        public string SaveMode
        {
            get { return this.hdSaveMode.Value; }
            set { this.hdSaveMode.Value = value; }
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                
                Initialize();
            }
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            if (SaveMode.Equals("0"))
            {
                string errMsg = string.Empty;
                Grade grade = new Grade();
                grade.Name = txtGradeName.Text.Trim();
                grade.Order = int.Parse(txtOrder.Text.Trim() == "" ? "0" : txtOrder.Text.Trim());
                grade.CompanyId = SessionManager.CurrentCompanyId;
                grade.CreatedOn = System.DateTime.Now;
                grade.CreatedBy = SessionManager.CurrentLoggedInEmployeeId;
                if (PositionGradeStepAmountManager.SaveGrade(grade, ref errMsg))
                {
                    ClearField();
                    BindGrade();
                }
                else
                {
                    divWarningMsg.InnerHtml = errMsg;
                    divWarningMsg.Hide = false;
                }
            }
            else
            {
                string errMsg = string.Empty;
                Grade grade = new Grade();
                grade.GradeId = int.Parse(hdGradeId.Value);
                grade.Name = txtGradeName.Text.Trim();
                grade.Order = int.Parse(txtOrder.Text.Trim() == "" ? "0" : txtOrder.Text.Trim());
                grade.CompanyId = SessionManager.CurrentCompanyId;
                grade.ModifiedOn = System.DateTime.Now;
                grade.ModifiedBy = SessionManager.CurrentLoggedInEmployeeId;
                if (PositionGradeStepAmountManager.UpdateGrade(grade, ref errMsg))
                {                    
                    ClearField();
                    BindGrade();
                    hdGradeId.Value = "0";
                    SaveMode = "0";
                }
                else
                {
                    divWarningMsg.InnerHtml = errMsg;
                    divWarningMsg.Hide = false;
                }
            }
        }



        protected void gvGrade_OnRowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            string errMessage =string.Empty;
            int id = (int)gvGrade.DataKeys[e.RowIndex][0];
            if (!PositionGradeStepAmountManager.CanItemBeDeleted(id,ref errMessage))
            {
                divWarningMsg.InnerHtml = errMessage;
                divWarningMsg.Hide = false;
                return;

            }
            if (PositionGradeStepAmountManager.DeleteGrade(id))
            {
                ClearField();
                BindGrade();
            }

        }

        protected void gvGrade_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvGrade.PageIndex = e.NewPageIndex;
            BindGrade();
        }

        void Initialize()
        {
            BindGrade();

        }

        void BindGrade()
        {
            List<Grade> list = PositionGradeStepAmountManager.GetAllGrades(SessionManager.CurrentCompanyId);
            gvGrade.DataSource = list;
            gvGrade.DataBind();
            if (list.Count == 0)
                txtOrder.Text = "1";
            else
                txtOrder.Text = (list[list.Count - 1].Order + 1).ToString();
        }

        void ClearField()
        {
            hdGradeId.Value = "0";
            SaveMode = "0";
            txtOrder.Text = string.Empty;
            txtGradeName.Text = string.Empty;
            divWarningMsg.Hide = true;
        }
    }
}
