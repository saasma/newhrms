﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Manager;
using BLL;
using Utils.Helper;
using DAL;

namespace Web.CP
{
    public partial class Listing : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialise();
            }
            else if (Request.Form["__EVENTTARGET"].Equals(this.updPanel.ClientID))
            {
                Initialise();
            }
            JavascriptHelper.AttachPopUpCode(Page, "popupIncome", "AEIncome.aspx", 600, 600);
        }

        protected void gvwEmployees_RowCreated(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover",
                "this.originalClass=this.className;this.className='selected'");

                e.Row.Attributes.Add("onmouseout",
                "this.className=this.originalClass;");


            }

        }

        protected void Initialise()
        {
            if (BLL.BaseBiz.PayrollDataContext.VoucherGroups.Any() == false)
                voucherAlsoChange.Visible = false;

            //load incomes
            List<PIncome> list = PayManager.GetIncomeListByCompany(SessionManager.CurrentCompanyId);

            int count = 1;
            foreach (PIncome item in list)
            {
                item.SN = count;
                count += 1;
            }

            list.Add(new PIncome
            {
                IncomeId = 0,
                Title = "Provident Fund",
                Abbreviation = "PF",
                Calculation = IncomeCalculation.FIXED_AMOUNT,
                IsTaxable=true,
                IsEnabled=true,
                Order =
                    SessionManager.CurrentCompany.PFOrder
                    ,SN = count
            });

            gvwIncomes.DataSource = list;
            gvwIncomes.DataBind();
        }

        protected void gvwIncomes_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

            int id = (int)gvwIncomes.DataKeys[e.RowIndex][0];
            PayManager.DeleteIncome(id);
            Initialise();
        }

        protected void Unnamed1_Click(object sender, EventArgs e)
        {

            foreach (GridViewRow row in this.gvwIncomes.Rows)
            {
                int incomeId = (int) (gvwIncomes.DataKeys[row.RowIndex][0]);
                TextBox txtOrder = row.FindControl("txtOrder") as TextBox;

                int order = 0;

                if (!string.IsNullOrEmpty(txtOrder.Text))
                    order = int.Parse(txtOrder.Text);


                PayManager.UpdateIncomeOrder(incomeId,order);


                divMsgCtl.InnerHtml = "Income order updated.";
                divMsgCtl.Hide = false;

            }

        }
    }
}
