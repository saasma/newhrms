﻿<%@ Page Title="Department Transfer list" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="DepartmentTransferList.aspx.cs" Inherits="Web.CP.DepartmentTransferList" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function medicalTaxCreditPopupCall(medicalTaxId) {
            var ret = medicalTaxCreditPopup("Id=" + medicalTaxId);
            if (ret == "Yes")
                __doPostBack('Reload', '');
        }

        function parentReloadCallbackFunction(childWindow, ret) {
            childWindow.close();
            if (ret == "Yes")
                __doPostBack('Reload', '');

        }

        function refreshWindow() {
            __doPostBack('Reload', '');
        }

        function popupAddDeptTransferCall() {
            var ret = popupDeptTransferDetails();

            if (typeof (ret) != 'undefined') {
                if (ret == 'Reload') {
                    __doPostBack('Reload', '');
                }
            }

            return false;
        }

        function reloadDeptTransfer(childWindow) {
            childWindow.close();
            __doPostBack('Reload', '');
        }

        function popupUpdateDeptTransferCall(BranchDepartmentId) {

            var ret = popupDeptTransferDetails('Id=' + BranchDepartmentId);

            if (typeof (ret) != 'undefined') {
                if (ret == 'Reload') {
                    __doPostBack('Reload', '');
                }
            }

            return false;
        }

        function popupcall(id) {
            popup('Id=' + id);
            return false;
        }
    </script>
    <style type="text/css">
        .hideLeftBlockCssInPage {
            margin: 0px !important;
            padding-left: 20px !important;
        }

        #menu {
            display: none;
        }
    </style>
    <link href="../css/core.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>Department Transfer List
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <%--to generate __PostBack function--%>
        <asp:LinkButton runat="server" Visible="false"></asp:LinkButton>
        <div class="attribute"  style="padding: 10px; width:1180px;">
            <table>
                <tr>
                    <td>
                        <strong>Search </strong>&nbsp;
                        <asp:TextBox Width="160px" ID="txtEmpSearchText" runat="server" OnTextChanged="txtEmpSearchText_TextChanged"></asp:TextBox>
                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtEmpSearchText"
                            WatermarkText="Employee Search" WatermarkCssClass="searchBoxText" />
                        <cc1:AutoCompleteExtender EnableCaching="true" ID="AutoCompleteExtenderOrganization"
                            runat="server" MinimumPrefixLength="2" ServiceMethod="GetEmployeeNames" ServicePath="~/PayrollService.asmx"
                            TargetControlID="txtEmpSearchText" CompletionSetCount="10" CompletionInterval="250"
                            CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList"
                            CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
                        </cc1:AutoCompleteExtender>
                    </td>
                    <td>
                        <asp:Button ID="btnLoad" CssClass="btn btn-default btn-sm" Style="margin-left: 10px; width: 100px;"
                            runat="server" OnClick="btnLoad_Click" Text="Load" />
                        <cc1:TextBoxWatermarkExtender ID="TBWE2" runat="server" TargetControlID="txtEmpSearchText"
                            WatermarkText="Employee name" WatermarkCssClass="searchBoxText" />
                    </td>
                    <td>
                        <asp:LinkButton ID="btnExport" runat="server" Text="Export" OnClick="btnExport_Click"
                            CssClass=" excel tiptip" Style="float: left;" />
                    </td>
                    <td>
                        <asp:LinkButton ID="btnImport" Visible="false" runat="server" Text="Import From Excel" OnClientClick="DepartmentTransferHistoryImport();return false;"
                            CssClass=" excel tiptip" Style="float: left;" />
                    </td>
                </tr>
            </table>
        </div>
        <br />
        <div style="text-align: left; clear: both">
            <%--<asp:Button ID="btnSave" Width="172px" Style="margin-left: 10px;" CausesValidation="false"
                    CssClass="save" runat="server" Text="Add/Edit Department Transfer" OnClientClick="window.location='DepartmentTransferDetails.aspx';return false;" />--%>
            <asp:Button ID="btnSave" Width="128px" Style="margin-left: 0px;" CausesValidation="false"
                CssClass="btn btn-primary btn-sm" runat="server" Text="New Transfer" OnClientClick="return popupAddDeptTransferCall();" />
        </div>
        <%--<div class="separator clear">--%>
        <div class="clear" style="width: 1180px; margin-top: 10px;">
            <cc2:EmptyDisplayGridView CssClass="tableLightColor" UseAccessibleHeader="true" ID="gvwList"
                runat="server" AutoGenerateColumns="False" DataKeyNames="BranchDepartmentId,EmployeeId"
                GridLines="None" ShowFooterWhenEmpty="False" ShowHeaderWhenEmpty="True" OnRowCreated="gvwList_RowCreated"
                Width="100%">
                <Columns>
                    <asp:BoundField HeaderStyle-Width="150px" HeaderStyle-HorizontalAlign="Left" DataField="FromDate"
                        HeaderText="Date" />
                    <asp:BoundField HeaderStyle-Width="150px" HeaderStyle-HorizontalAlign="Left" DataField="FromDateEng"
                        DataFormatString="{0:yyyy-M-dd}" HeaderText="Date(AD)" />
                    <asp:BoundField HeaderStyle-HorizontalAlign="Left" DataField="EmployeeId" HeaderText="EIN"
                        HeaderStyle-Width="40px" />
                    <asp:BoundField HeaderStyle-HorizontalAlign="Left" DataField="INo" HeaderText="I No"
                        HeaderStyle-Width="40px" />
                    <asp:BoundField HeaderStyle-Width="200px" HeaderStyle-HorizontalAlign="Left" DataField="EmployeeName"
                        HeaderText="Name" />
                    <asp:BoundField HeaderStyle-Width="150px" HeaderStyle-HorizontalAlign="Left" DataField="FromDepartment"
                        HeaderText="From Department" />
                    <asp:BoundField HeaderStyle-Width="150px" HeaderStyle-HorizontalAlign="Left" DataField="ToDepartment"
                        HeaderText="To Department" />
                    <asp:TemplateField HeaderStyle-Width="100px" HeaderText="Review" ItemStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <%--<asp:HyperLink ID="linkEdit" ImageUrl="~/images/edit.gif" NavigateUrl='<%# "~/CP/DepartmentTransferDetails.aspx?Id=" + Eval("EmployeeId") %>'
                                runat="server"></asp:HyperLink>--%>
                            <asp:HyperLink ID="btnEdit1" Text="Review Team" runat="server"
                                onclick='<%# "return popupcall(" + Eval("BranchDepartmentId") + ");return false;" %>' NavigateUrl='javascript:void(0)' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <%--<asp:HyperLink ID="linkEdit" ImageUrl="~/images/edit.gif" NavigateUrl='<%# "~/CP/DepartmentTransferDetails.aspx?Id=" + Eval("EmployeeId") %>'
                                runat="server"></asp:HyperLink>--%>
                            <asp:ImageButton ID="btnEdit" CommandName="Delete" ImageUrl="~/images/edit.gif" runat="server"
                                OnClientClick='<%# "return popupUpdateDeptTransferCall(" +  Eval("BranchDepartmentId") + ");" %>'
                                AlternateText="Edit" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                <EmptyDataTemplate>
                    No records.
                </EmptyDataTemplate>
            </cc2:EmptyDisplayGridView>
            <uc1:PagingCtl ID="pagingCtl" OnNextRecord="btnNext_Click" OnPrevRecord="btnPrevious_Click"
                OnDropDownShowRecordsChanged="ddlRecords_SelectedIndexChanged" runat="server" />
        </div>
    </div>
</asp:Content>
