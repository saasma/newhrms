﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using BLL;
using DAL;
using Ext.Net;
using Utils.Web;
using Utils.Helper;
using Utils;
using System.IO;
using Web.ReportDataSetTableAdapters;
using Web.CP.Report.Templates.Pay.Detail;
using BLL.BO;
using Newtonsoft.Json.Linq;

namespace Web.Employee
{
    public partial class ComposeMessage : BasePage
    {
        string userName = string.Empty;

        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.HumanResource;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
             userName = SessionManager.UserName;
            if (!X.IsAjaxRequest)
            {
                Initialize();
            }
        }

        [DirectMethod]
        public void reloadSpecificEmployee()
        {
            storeEmployee.DataSource = EmployeeManager.GetEmployeeUserNamesByCompany(SessionManager.CurrentCompanyId)
                .OrderBy(x => x.Text);
            storeEmployee.DataBind();
        }

        protected void cboBranch1_Select(object sender, DirectEventArgs e)
        {
            storeDepartment.DataSource = DepartmentManager.GetAllDepartmentsByBranch(int.Parse(cboBranch1.SelectedItem.Value));
            storeDepartment.DataBind();
            cboDepartment.SetRawValue("");
        }

        void Initialize()
        {
            //storeBranch.DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            //storeBranch.DataBind();

            storeBranch1.DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            storeBranch1.DataBind();

            storeEmployee.DataSource = EmployeeManager.GetEmployeeUserNamesByCompany(SessionManager.CurrentCompanyId);
            storeEmployee.DataBind();
        }

        protected void btnSend_Click(object sender, DirectEventArgs e)
        {

            if (string.IsNullOrEmpty(txtSubject.Text.Trim()))
            {
                NewMessage.ShowWarningMessage("Subject is required.");
                return;
            }

            if (string.IsNullOrEmpty(txtBody.Text.Trim()))
            {
                NewMessage.ShowWarningMessage("Message is required.");
                return;
            }

            List<string> distinctEmployee = new List<string>();
            if (rdoSpecificEmployee.Checked)
            {
                if (hdEmployees.Text == string.Empty)
                    return;

                string[] employees = hdEmployees.Text.Split(';');

                foreach (string st in employees)
                {
                    if (!distinctEmployee.Contains(st) && !st.Equals(userName))
                        distinctEmployee.Add(st);
                }

                
            }
            else if (rdoAll.Checked)
            {
                distinctEmployee = EmployeeManager.GetEmployeeUserNameList(SessionManager.CurrentCompanyId);
                if (distinctEmployee.Contains(userName))
                {
                    distinctEmployee.Remove(userName);
                }
            }
            //else if (rdoBranch.Checked)
            //{
            //    distinctEmployee = EmployeeManager.GetEmployeeUserNameListByBranch(int.Parse(cboBranch.SelectedItem.Value));
            //}
            //else if (rdoDepartment.Checked)
            //{
            //    distinctEmployee = EmployeeManager.GetEmployeeUserNameListByDepartment(int.Parse(cboBranch.SelectedItem.Value));
            //}

            else if (rdoDepartment.Checked)
            {
                if (cboBranch1.SelectedItem != null && cboDepartment.SelectedItem == null)
                {
                    distinctEmployee = EmployeeManager.GetEmployeeUserNameListByBranch(int.Parse(cboBranch1.SelectedItem.Value));
                }
                else if(cboBranch1.SelectedItem != null && cboDepartment.SelectedItem != null)
                    distinctEmployee = EmployeeManager.GetEmployeeUserNameListByDepartment(int.Parse(cboDepartment.SelectedItem.Value));
            }
            else
            {
                return;
            }


            if (chkSendMeAlso.Checked)
                distinctEmployee.Add(userName);
            DAL.PayrollMessage msg = new DAL.PayrollMessage();
            msg.Subject = txtSubject.Text.Trim();
            msg.Body = txtBody.Text.Trim();
            msg.SendBy = SessionManager.UserName;
            msg.ReceivedDate = BLL.BaseBiz. GetCurrentDateAndTime().ToShortDateString();
            msg.ReceivedDateEng = BLL.BaseBiz.GetCurrentDateAndTime();
            msg.IsRead = false;
            if (!string.IsNullOrEmpty(dateDueDate.RawText))
                msg.DueDateEng = dateDueDate.SelectedDate;
            MessageBoxButtonsConfig button = new MessageBoxButtonsConfig();

            if (PayrollMessageManager.SendMessages(msg, distinctEmployee))
            {
                NewMessage.ShowNormalMessage("Message successfully send.");

            }

        }

        protected void bntLoadEmp_Click(object sender, DirectEventArgs e)
        {
            storeEmployee.DataSource = EmployeeManager.GetEmployeeUserNamesByCompany(SessionManager.CurrentCompanyId)
               .OrderBy(x => x.Text);
            storeEmployee.DataBind();
        }
    }
}
