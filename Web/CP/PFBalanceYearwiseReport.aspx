<%@ Page Title="PF Balance Yearly Report" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="PFBalanceYearwiseReport.aspx.cs" Inherits="Web.CP.PFBalanceYearwiseReport" %>

<%@ Register Src="../UserControls/PFBalanceYearwiseReportUC.ascx" TagName="PFBalanceYearwiseReportUC" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
<%--<ext:ResourceManager ID="ResourceManager1" DisableViewState="false"  runat="server" ShowWarningOnAjaxFailure="false"
    ScriptMode="Release" Namespace="Rigo" />--%>

<div class="pageheader">
    <div class="media">
        <div class="media-body">
            <h4>
                Yearly Employee PF Balance
            </h4>
        </div>
    </div>
</div>
<div class="contentpanel">

    <div style='margin-top:15px'>
    <uc1:PFBalanceYearwiseReportUC Id="PFBalanceYearwiseReportUC1" runat="server" />
    </div>
</div>
</asp:Content>
