<%@ Page MaintainScrollPositionOnPostback="true" Title="Loan Adjustment" Language="C#"
    MasterPageFile="~/Master/HROld.Master" AutoEventWireup="true" CodeBehind="LoanAdjustment.aspx.cs"
    Inherits="Web.CP.LoanAdjustment" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ContentHeader.ascx" TagName="ContentHeader" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>
    <script type="text/javascript">
        var skipLoadingCheck = true;

        function selectDeselectAll(chk) {
            $('input[type=checkbox]').each(
                    function (index) {
                        if ($(this).prop('disabled') == false && this.id.indexOf('chkSelect') > 0)
                            this.checked = chk.checked;
                    }
                );
        }

        function validateAdjustment(source, args) {

            var adjustmentAmount = getNumber(document.getElementById(source.controltovalidate).value);
            var ppmt = getNumber(source.getAttribute('PPMTorIPMT'));


            if (adjustmentAmount < 0) {
                if ((ppmt + adjustmentAmount) < 0)
                    args.IsValid = false;
                else
                    args.IsValid = true;
            }

            else
                args.IsValid = true;
        }

        function reloadDeduction(win) {
            win.close();
            __doPostBack('Reload', '');
        }
        function popupUpdateDeductionCall(deductionId) {

            var ret = popupUpdateDeduction('Id=' + deductionId);
//            if (typeof (ret) != 'undefined') {
//                if (ret == 'Reload') {
//                    refreshDeductionList(0, EmployeeId);
//                }
//            }
            return false;
        }

        function showHistory() {
            Web.PayrollService.GetLoanAdjustmentHistory(document.getElementById('<%=ddlEmployees.ClientID %>').value, document.getElementById('<%=ddlLoans.ClientID %>').value, showHistoryCallback);
        }

        function showHistoryCallback(result) {
            document.getElementById("divHistory").innerHTML = result;
        }

        function realoadTable() {
            __doPostBack('Reload', '');
        }

        function adjustmentTypeChange(cmbAdjustmentType, cmbPaymentOption, cmbInstallment) {
            if (cmbAdjustmentType.getValue() == "1") {
                //cmbPaymentOption.setDisabled(false);

                cmbInstallment.clearValue();
                cmbInstallment.setDisabled(true);
            }
            else {
                //cmbPaymentOption.setValue("1"); //set Deduct from salary
                //cmbPaymentOption.setDisabled(true);

                cmbInstallment.setDisabled(false);
            }
        }

        function installmentChange(cmbInstallment, txtNoOfInstallment) {
            if (cmbInstallment.getValue() == "3" || cmbInstallment.getValue() == "4")
                txtNoOfInstallment.setDisabled(false);
            else
                txtNoOfInstallment.setDisabled(true);
        }

    </script>
    <style type="text/css">
        .blankColorCell
        {
            background-color: #fff !important;
            border-top: #fff !important;
            border-left: #fff !important;
            border-bottom: #fff !important;
            border-right: 1px solid #A5DEFA !important;
        }
        .hightlightCell
        {
            border-top: 2px solid lightblue !important;
            border-bottom: 2px solid lightblue !important;
            background-color: lightgray !important;
        }
        .tdGap td
        {
            padding-bottom: 8px;
            padding-right: 10px;
        }
        
        .viewhistory td
        {
            padding: 2px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Simple Loan Adjustment
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <uc1:ContentHeader Id="ContentHeader1" runat="server" />
        <asp:Panel ID="pnl" DefaultButton="btnLoad" runat="server" class="attribute" Style="padding: 10px">
            <table>
                <tr>
                    <td>
                        <%-- <strong>Payroll Period</strong>
                        <asp:DropDownList AutoPostBack="true" ID="ddlPayrollPeriods" DataTextField="Name"
                            DataValueField="PayrollPeriodId" AppendDataBoundItems="true" Width="150px" runat="server"
                            OnSelectedIndexChanged="ddlPayrollPeriods_SelectedIndexChanged">
                            <asp:ListItem Value="-1" Text=""></asp:ListItem>
                        </asp:DropDownList>--%>
                        <%--<asp:RequiredFieldValidator InitialValue="-1" Display="None" ControlToValidate="ddlPayrollPeriods"
                        ValidationGroup="Overtime" ID="RequiredFieldValidator2" runat="server" ErrorMessage="Payroll period is required."></asp:RequiredFieldValidator>--%>
                        <%--   &nbsp; &nbsp; --%>
                        <strong>Simple Loan</strong>
                        <asp:DropDownList AutoPostBack="true" ID="ddlLoans" DataTextField="Title" DataValueField="DeductionId"
                            AppendDataBoundItems="true" Width="150px" runat="server" OnSelectedIndexChanged="ddlLoans_SelectedIndexChanged">
                            <asp:ListItem Value="-1" Text=""></asp:ListItem>
                        </asp:DropDownList>
                        &nbsp; &nbsp; <strong>Employee</strong>
                        <asp:DropDownList ID="ddlEmployees" DataTextField="NameEIN" DataValueField="EmployeeId"
                            AppendDataBoundItems="true" Width="180px" runat="server">
                            <asp:ListItem Value="-1" Text=""></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <asp:Button ID="btnLoad" runat="server" CssClass="btn btn-default btn-sect btn-sm"
                            Style="width: 80px;" Text="Show" OnClick="btnLoad_Click" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <br />
        <ext:Hidden ID="currentPayrollPeriod" runat="server" />
        <ext:Hidden ID="currentEmployeeId" runat="server" />
        <ext:Hidden ID="currentDeductionId" runat="server" />
        <ext:Hidden ID="currentOpening" runat="server" />
        <ext:Hidden ID="currentRemInstallment" runat="server" />
        <ext:Hidden ID="currentPaidInstallment" runat="server" />
        <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
        <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
        <%--   <div cssclass="clear buttonsDiv" id="divButtons" runat="server" style='text-align: right;
            margin-top: 15px'>
          
        </div>--%>
        <asp:Panel ID="pnlDetails" Style="clear: both" runat="server" Visible="false">
            <div style="">
                <table>
                    <tr>
                        <td valign="top">
                            <table style='clear: both; padding-bottom: 10px' class="tdGap">
                                <tr>
                                    <td class="fieldHeader">
                                        Loan Amount
                                    </td>
                                    <td>
                                        <asp:Label ID="lblLoanAmount" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="fieldHeader">
                                        Taken On
                                    </td>
                                    <td>
                                        <asp:Label ID="lblTakenOn" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="fieldHeader">
                                        Interest Rate
                                    </td>
                                    <td>
                                        <asp:TextBox Width="100px" Enabled="false" ID="txtInterestRate" AutoPostBack="true"
                                            OnTextChanged="value_Change" runat="server" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtInterestRate"
                                            Display="None" SetFocusOnError="true" ErrorMessage="Interest rate is required."
                                            ValidationGroup="Balance"></asp:RequiredFieldValidator>
                                        <asp:CompareValidator Operator="GreaterThanEqual" ValueToCompare="0" ValidationGroup="Balance"
                                            Type="Double" SetFocusOnError="true" ID="CompareValidator1" Display="None" ControlToValidate="txtInterestRate"
                                            runat="server" ErrorMessage="Invalid interest rate."></asp:CompareValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="fieldHeader">
                                        Fixed PPMT
                                    </td>
                                    <td>
                                        <asp:TextBox Width="100px" Enabled="false" ID="txtNewPPMT" AutoPostBack="true" OnTextChanged="value_Change"
                                            runat="server" />
                                        <asp:CompareValidator Operator="GreaterThanEqual" ValueToCompare="0" ValidationGroup="Balance"
                                            Type="Currency" SetFocusOnError="true" ID="CompareValidator2" Display="None"
                                            ControlToValidate="txtNewPPMT" runat="server" ErrorMessage="Invalid Amount."></asp:CompareValidator>
                                    </td>
                                </tr>
                                <%--  <tr>
                        <td class="fieldHeader">
                            
                        </td>
                        <td>
                            <asp:Label ID="lblNoOfPayments" runat="server" />
                        </td>
                    </tr>--%>
                                <tr>
                                    <td class="fieldHeader">
                                        No of Payments
                                    </td>
                                    <td>
                                        <asp:TextBox Width="100px" Enabled="false" ID="txtNoOfPayment" AutoPostBack="true"
                                            OnTextChanged="value_Change" runat="server" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtNoOfPayment"
                                            Display="None" SetFocusOnError="true" ErrorMessage="No of payments is required."
                                            ValidationGroup="Balance"></asp:RequiredFieldValidator>
                                        <asp:CompareValidator ValidationGroup="Balance" Type="Integer" ValueToCompare="0"
                                            Operator="GreaterThan" SetFocusOnError="true" ID="CompareValidator3" Display="None"
                                            ControlToValidate="txtNoOfPayment" runat="server" ErrorMessage="Invalid no of payments"></asp:CompareValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="fieldHeader">
                                        Payment Term
                                    </td>
                                    <td>
                                        <asp:Label ID="Label2" runat="server" Text="Monthly" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td valign="top" style="padding-left: 30px">
                            <table style='padding-bottom: 10px' class="tdGap">
                                <tr>
                                    <td class="fieldHeader">
                                        EIN
                                    </td>
                                    <td>
                                        <asp:Label ID="lblEID" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="fieldHeader">
                                        Name
                                    </td>
                                    <td>
                                        <asp:Label ID="lblName" Style="width: 200px" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="fieldHeader">
                                        Designation
                                    </td>
                                    <td>
                                        <asp:Label ID="lblDesignation" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="fieldHeader">
                                        Branch
                                    </td>
                                    <td>
                                        <asp:Label ID="lblBranch" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="fieldHeader">
                                        Department
                                    </td>
                                    <td>
                                        <asp:Label ID="lblDepartment" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Button ID="btnGenerateTable" runat="server" Width="120px" OnClientClick="valGroup='Balance';return CheckValidation();"
                                            ValidationGroup="Balance" Style='float: left' CssClass="update" Text="Generate List"
                                            OnClick="btnGenerateTable_Click" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td valign="top" style="padding-left: 30px">
                            <table style='padding-bottom: 10px' class="tdGap">
                                <tr>
                                    <td>
                                        <ext:Button  Width="130px"  runat="server" ID="Button1" Cls="btn btn-primary" Text="<i></i>Adjust Loan">
                                            <Listeners>
                                                <Click Handler="#{windowAdjustment}.show();">
                                                </Click>
                                            </Listeners>
                                        </ext:Button>
                                    </td>
                                    <td rowspan="4" valign="top">
                                        <asp:Button ID="btnExportTable" runat="server" Width="120px" CausesValidation="false"
                                            ValidationGroup="Balance" Style='' CssClass="update" Text="Export Table" OnClick="btnExportTable_Click" />
                                        <asp:Button ID="btnExportLastAdjustmentTable" runat="server" Width="150px" CausesValidation="false"
                                            ValidationGroup="Balance" Style='padding-top: 5px;' CssClass="update" Text="Export Last Adjustment"
                                            OnClick="btnExportLastAdjustmentTable_Click" />
                                        <ext:Button runat="server" ID="btnEditLoan" StyleSpec="margin-top:10px;" Width="130px" Cls="btn btn-primary" Text="<i></i>Edit Loan">
                                            <Listeners>
                                                <Click Handler="return popupUpdateDeductionCall(6843);">
                                                </Click>
                                            </Listeners>
                                        </ext:Button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ext:Button runat="server" ID="Button3" Width="130px"  Cls="btn btn-primary" Text="<i></i>Add Loan">
                                            <Listeners>
                                                <Click Handler="#{windowAddLoan}.show();">
                                                </Click>
                                            </Listeners>
                                        </ext:Button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ext:Button runat="server" ID="Button2" Width="130px"  Cls="btn btn-primary" Text="<i></i>Change Interest">
                                            <Listeners>
                                                <Click Handler="#{windowInterest}.show();">
                                                </Click>
                                            </Listeners>
                                        </ext:Button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ext:Button runat="server" ID="Button4" Width="130px"  Cls="btn btn-primary" Text="<i></i>Adjust Interest">
                                            <Listeners>
                                                <Click Handler="#{windowAdjustInterest}.show();">
                                                </Click>
                                            </Listeners>
                                        </ext:Button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ext:Button runat="server" ID="btnDeleteAdjustment" Cls="btn btn-primary" Text="<i></i>Delete Current Adjustment">
                                            <DirectEvents>
                                                <Click OnEvent="btnDeleteAdjustment_Click">
                                                    <EventMask ShowMask="true">
                                                    </EventMask>
                                                    <Confirmation ConfirmRequest="true" Message="Confirm delete the current month adjustment?" />
                                                </Click>
                                            </DirectEvents>
                                        </ext:Button>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <cc2:EmptyDisplayGridView EnableViewState="false" CssClass="table table-primary mb30 table-bordered table-hover"
                Style='clear: both; margin-top: 20px;' UseAccessibleHeader="true" GridLines="None"
                ID="gvwSimpleLoanInstallments" DataKeyNames="PayrollPeriodId,IsCurrentValidPayrollPeriod,EmployeeId,DeductionId,IsPayrollPeriodSaved,Opening,RemainingInstallments"
                runat="server" ShowFooterWhenEmpty="False" ShowHeaderWhenEmpty="True" AutoGenerateColumns="False"
                OnRowCommand="gvwSimpleLoanInstallments_RowCommand" OnRowDataBound="gvwSimpleLoanInstallments_RowDataBound">
                <Columns>
                    <asp:TemplateField HeaderStyle-Width="30px" HeaderText="Remaining Installments" HeaderStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:Label ID="lblRemInstallment" runat="server" Text='<%# Eval("RemainingInstallments") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Year" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <%# Eval("Year")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Month" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <%# Utils.Calendar.DateHelper.GetMonthName( (int) Eval("Month"),IsEnglish) %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Opening" ItemStyle-HorizontalAlign="Right">
                        <ItemTemplate>
                            <asp:Label ID="lblOpening" runat="server" Text='<%# GetCurrency(Eval("Opening")) %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="PPMT" ItemStyle-HorizontalAlign="Right">
                        <ItemTemplate>
                            <asp:Label ID="lblPPMT" runat="server" Text='<%# GetCurrency(Eval("PPMT"))%>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="IPMT" ItemStyle-HorizontalAlign="Right">
                        <ItemTemplate>
                            <asp:Label ID="lblIPMT" runat="server" Text='<%# GetCurrency(Eval("IPMT")) %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Closing" ItemStyle-HorizontalAlign="Right">
                        <ItemTemplate>
                            <asp:Label ID="lblClosing" runat="server" Text='<%# GetCurrency(Eval("Closing")) %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-CssClass="blankColorCell" HeaderText="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;">
                        <ItemTemplate>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </ItemTemplate>
                        <ItemStyle CssClass="blankColorCell" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Addition" ItemStyle-HorizontalAlign="Right">
                        <ItemTemplate>
                            <asp:Label Width="100px" Style='text-align: right' ID="txtAddition" runat="server"
                                Text='<%#GetAmount( Eval("Addition"),false) %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Adjustment PPMT" ItemStyle-HorizontalAlign="Right">
                        <ItemTemplate>
                            <asp:Label runat="server" Text='<%#GetAmount( Eval("AdjustmentPPMT"),true) %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Adjustment IPMT" ItemStyle-HorizontalAlign="Right">
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# GetAmount( Eval("AdjustmentIPMT"),true) %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Adjusted PPMT" ItemStyle-HorizontalAlign="Right">
                        <ItemTemplate>
                            <asp:Label ID="lblAdjustedPPMT" runat="server" Text='<%# GetCurrency(Eval("AdjustedPPMT")) %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Adjusted Interest" ItemStyle-HorizontalAlign="Right">
                        <ItemTemplate>
                            <asp:Label ID="lblAdjustedInterest" runat="server" Text='<%# GetCurrency(Eval("AdjustedInterest")) %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Adjusted Closing" ItemStyle-HorizontalAlign="Right">
                        <ItemTemplate>
                            <asp:Label ID="lblAdjustedClosing" runat="server" Text='<%# GetCurrency(Eval("AdjustedClosing")) %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField Visible="false" ItemStyle-HorizontalAlign="Center" HeaderText="Do Not Adjust Loan">
                        <ItemTemplate>
                            <asp:CheckBox ID="chkDoNotAdjustLoan" Enabled='<%# ((int)Eval("IsCurrentValidPayrollPeriod"))==1 %>'
                                runat="server" Checked='<%# (Eval("DoNotAdjustLoan") != null) ? Eval("DoNotAdjustLoan") : false %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                <EmptyDataTemplate>
                    <b>No installments</b>
                </EmptyDataTemplate>
            </cc2:EmptyDisplayGridView>
            <br />
            <%-- <asp:ValidationSummary ID="ValidationSummary1" DisplayMode="List" ShowSummary="false"
                ShowMessageBox="true" ValidationGroup="Balance" runat="server"></asp:ValidationSummary>--%>
            <%-- <asp:Button Visible="false" ID="btnSave" runat="server" OnClientClick="valGroup='Balance';return CheckValidation();"
                ValidationGroup="Balance" Style='float: left; padding-bottom: 20px' CssClass="save"
                Text="Save" OnClick="btnSendMail_Click" />--%>
        </asp:Panel>
        <div class="viewhistory" style="margin-bottom: 40px">
            <div class="viewhistorydiv">
                <table>
                    <tr>
                        <td>
                            <h2 style="font-size: 12px">
                                Changed History
                            </h2>
                        </td>
                        <td style="padding-top: 9px; padding-left: 10px">
                            <asp:LinkButton ID="btnHistoryExport" runat="server" Width="120px" CausesValidation="false"
                                ValidationGroup="Balance" Style='float: left' Text="Export" OnClick="btnHistoryExport_Click" />
                        </td>
                    </tr>
                </table>
            </div>
            <asp:LinkButton ID="btnDocumentHistory" OnClientClick="showHistory();return false;"
                runat="server" Text="View History" Icon="BulletArrowDown" Cls="viewbtn">
                <%--<DirectEvents>
                    <Click OnEvent="btnDocumentHistory_Click">
                        <EventMask ShowMask="true">
                        </EventMask>
                    </Click>
                </DirectEvents>--%>
            </asp:LinkButton>
            <div id="divHistory">
            </div>
            <%--<uc1:ViewDocumentHistory ID="ViewDocumentHistory1" runat="server" />--%>
        </div>
    </div>
    <ext:Window ID="windowAdjustment" runat="server" Title="Loan Adjustment" Icon="Application"
        Height="280" Width="770" BodyPadding="5" Hidden="true" Modal="true">
        <Content>
            <table class="fieldTable">
                <tr>
                    <td>
                        <ext:TextField ID="txtWinLoanAdjustmentAmount" runat="server" FieldLabel="Reduce Loan By"
                            LabelAlign="Top" LabelSeparator="" Width="150px">
                        </ext:TextField>
                        <asp:RequiredFieldValidator Display="None" ID="valLoanAdjust" runat="server" ValidationGroup="SaveUpdateAdjustment"
                            ControlToValidate="txtWinLoanAdjustmentAmount" ErrorMessage="Adjustment amount is required." />
                        <asp:CompareValidator SetFocusOnError="true" ValueToCompare='0' Display="None" Operator="GreaterThanEqual"
                            Type="Currency" ID="valOpeningBalance245" ControlToValidate="txtWinLoanAdjustmentAmount"
                            ValidationGroup="SaveUpdateAdjustment" runat="server" ErrorMessage="Invalid amount, should  be greater than 0."></asp:CompareValidator>
                    </td>
                    <td>
                        <ext:DateField ID="dateAdjustmentPeriod" LabelSeparator="" LabelAlign="Top" runat="server"
                            LabelWidth="120" FieldLabel="Adjustment Period">
                            <Plugins>
                                <ext:GenericPlugin ID="HighlightDates1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                            </Plugins>
                        </ext:DateField>
                        <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator3" runat="server"
                            ValidationGroup="SaveUpdateAdjustment" ControlToValidate="dateAdjustmentPeriod"
                            ErrorMessage="Adjustment date is required." />
                    </td>
                    <td>
                        <ext:DisplayField ID="dispEligiblePeriod" LabelSeparator="" LabelAlign="Top" runat="server"
                            LabelWidth="120" FieldLabel="Current Period" />
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:ComboBox ID="cmbWinAdjustmentType" LabelSeparator="" FieldLabel="Adjustment Type"
                            LabelWidth="70" LabelAlign="Top" runat="server">
                            <Items>
                                <ext:ListItem Text="Full Settlement" Value="1" />
                                <ext:ListItem Text="Partial Settlement" Value="2" />
                            </Items>
                            <Listeners>
                                <Change Handler="adjustmentTypeChange(#{cmbWinAdjustmentType},#{cmbWinAdjustmentPayment},#{cmbWinAdjustmentInstallmentOption});" />
                            </Listeners>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator9" runat="server"
                            ValidationGroup="SaveUpdateAdjustment" ControlToValidate="cmbWinAdjustmentType"
                            ErrorMessage="Adjustment type is required." />
                    </td>
                    <td>
                        <ext:ComboBox ID="cmbWinAdjustmentPayment" LabelSeparator="" FieldLabel="Adjustment Payment"
                            LabelWidth="70" LabelAlign="Top" runat="server">
                            <Items>
                                <ext:ListItem Text="Deduct from Salary" Value="1" />
                                <ext:ListItem Text="External Settlement" Value="2" />
                            </Items>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator10" runat="server"
                            ValidationGroup="SaveUpdateAdjustment" ControlToValidate="cmbWinAdjustmentPayment"
                            ErrorMessage="Adjustment Payment is required." />
                    </td>
                    <td>
                        <ext:ComboBox ID="cmbWinAdjustmentInstallmentOption" LabelSeparator="" FieldLabel="Installment Option"
                            LabelWidth="70" LabelAlign="Top" Width="200" runat="server">
                            <Items>
                                <ext:ListItem Text="Reduce Installment Amount" Value="1" />
                                <ext:ListItem Text="Reduce Installment Period" Value="2" />
                                <ext:ListItem Text="New Installment Period" Value="3" />
                                <ext:ListItem Text="New Installment with Fixed Amount" Value="4" />
                            </Items>
                            <Listeners>
                                <Select Handler="if(this.getValue()=='3' || this.getValue()=='4') #{txtAdjustmentNoOfInstallment}.setDisabled(false); else #{txtAdjustmentNoOfInstallment}.setDisabled(true);" />
                            </Listeners>
                        </ext:ComboBox>
                        <%--  <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator11" runat="server"
                            ValidationGroup="SaveUpdateAdjustment" ControlToValidate="cmbWinAdjustmentInstallmentOption"
                            ErrorMessage="Installment Option is required." />--%>
                    </td>
                    <td>
                        <ext:TextField ID="txtAdjustmentNoOfInstallment" runat="server" FieldLabel="Installment Period / Amount"
                            LabelAlign="Top" LabelSeparator="" Width="170px">
                        </ext:TextField>
                        <asp:CompareValidator SetFocusOnError="true" ValueToCompare='0' Display="None" Operator="GreaterThan"
                            Type="Integer" ID="CompareValidator9" ControlToValidate="txtAdjustmentNoOfInstallment"
                            ValidationGroup="SaveUpdateAdjustment" runat="server" ErrorMessage="Invalid installment, should  be greater than 0."></asp:CompareValidator>
                    </td>
                </tr>
                <tr>
                    <td valign="bottom" colspan="2">
                        <div class="popupButtonDiv">
                            <ext:Button runat="server" ID="btnLoanAdjustment" Cls="btn btn-primary" Text="<i></i>Save">
                                <DirectEvents>
                                    <Click OnEvent="btnLoanSaveAdjustment_Click">
                                        <EventMask ShowMask="true" />
                                    </Click>
                                </DirectEvents>
                                <Listeners>
                                    <Click Handler="valGroup = 'SaveUpdateAdjustment'; return CheckValidation();">
                                    </Click>
                                </Listeners>
                            </ext:Button>
                            <div class="btnFlatOr">
                                or</div>
                            <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="LinkButton2" Cls="btnFlatLeftGap"
                                Text="<i></i>Cancel">
                                <Listeners>
                                    <Click Handler="#{windowAdjustment}.hide();">
                                    </Click>
                                </Listeners>
                            </ext:LinkButton>
                        </div>
                    </td>
                </tr>
            </table>
        </Content>
    </ext:Window>
    <ext:Window ID="windowAddLoan" runat="server" Title="Add Loan" Icon="Application"
        Height="280" Width="650" BodyPadding="5" Hidden="true" Modal="true">
        <Content>
            <table class="fieldTable">
                <tr>
                    <td>
                        <ext:TextField ID="txtAddLoanAmount" runat="server" FieldLabel="Additional Loan Amount"
                            LabelAlign="Top" LabelSeparator="" Width="150px">
                        </ext:TextField>
                        <%-- <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator4" runat="server"
                            ValidationGroup="SaveUpdateAddLoan" ControlToValidate="txtAddLoanAmount" ErrorMessage="Amount is required." />--%>
                        <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator11" runat="server"
                            ValidationGroup="SaveUpdateAddLoan" ControlToValidate="txtAddLoanAmount" ErrorMessage="Amount is required." />
                        <asp:CompareValidator SetFocusOnError="true" ValueToCompare='0' Display="None" Operator="GreaterThanEqual"
                            Type="Currency" ID="CompareValidator4" ControlToValidate="txtAddLoanAmount" ValidationGroup="SaveUpdateAddLoan"
                            runat="server" ErrorMessage="Invalid amount, should be greater than 0."></asp:CompareValidator>
                    </td>
                    <td>
                        <ext:DateField ID="dateAddLoan" LabelSeparator="" LabelAlign="Top" runat="server"
                            LabelWidth="120" FieldLabel="Addition Date">
                            <Plugins>
                                <ext:GenericPlugin ID="GenericPlugin1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                            </Plugins>
                        </ext:DateField>
                        <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator5" runat="server"
                            ValidationGroup="SaveUpdateAddLoan" ControlToValidate="dateAddLoan" ErrorMessage="Date is required." />
                    </td>
                    <td>
                        <ext:DisplayField ID="dispAddLoan" LabelSeparator="" LabelAlign="Top" runat="server"
                            LabelWidth="120" FieldLabel="Current Period" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:ComboBox ID="cmbAddLoanInstallationOption" LabelSeparator="" FieldLabel="Installment Option"
                            Width="180" LabelAlign="Top" runat="server">
                            <Items>
                                <ext:ListItem Text="Same Installment Amount" Value="1" />
                                <ext:ListItem Text="Same Installment Period" Value="2" />
                                <ext:ListItem Text="Define New Installment Period" Value="3" />
                                <ext:ListItem Text="Define New Installment with Fixed Amount" Value="4" />
                            </Items>
                            <Listeners>
                                <Change Handler="installmentChange(#{cmbAddLoanInstallationOption},#{txtAddLoanNoOfInstallment});" />
                            </Listeners>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator4" runat="server"
                            ValidationGroup="SaveUpdateAddLoan" ControlToValidate="cmbAddLoanInstallationOption"
                            ErrorMessage="Installment Option is required." />
                    </td>
                    <td>
                        <ext:TextField ID="txtAddLoanNoOfInstallment" runat="server" FieldLabel="Installment Period / Amount"
                            LabelAlign="Top" LabelSeparator="" Width="170px">
                        </ext:TextField>
                        <asp:CompareValidator SetFocusOnError="true" ValueToCompare='0' Display="None" Operator="GreaterThan"
                            Type="Integer" ID="CompareValidator5" ControlToValidate="txtAddLoanNoOfInstallment"
                            ValidationGroup="SaveUpdateAddLoan" runat="server" ErrorMessage="Invalid installment, should  be greater than 0."></asp:CompareValidator>
                    </td>
                </tr>
                <tr>
                    <td valign="bottom" colspan="2">
                        <div class="popupButtonDiv">
                            <ext:Button runat="server" ID="btnAddLoanSave" Cls="btn btn-primary" Text="<i></i>Save">
                                <DirectEvents>
                                    <Click OnEvent="btnAddLoanSave_Click1">
                                        <EventMask ShowMask="true" />
                                    </Click>
                                </DirectEvents>
                                <Listeners>
                                    <Click Handler="valGroup = 'SaveUpdateAddLoan'; return CheckValidation();">
                                    </Click>
                                </Listeners>
                            </ext:Button>
                            <div class="btnFlatOr">
                                or</div>
                            <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="LinkButton1" Cls="btnFlatLeftGap"
                                Text="<i></i>Cancel">
                                <Listeners>
                                    <Click Handler="#{windowAddLoan}.hide();">
                                    </Click>
                                </Listeners>
                            </ext:LinkButton>
                        </div>
                    </td>
                </tr>
            </table>
        </Content>
    </ext:Window>
    <ext:Window ID="windowAdjustInterest" runat="server" Title="Adjust Interest" Icon="Application"
        Height="280" Width="650" BodyPadding="5" Hidden="true" Modal="true">
        <Content>
            <table class="fieldTable">
                <tr>
                    <td>
                        <ext:TextField ID="txtInterestAdjustmentAmount" runat="server" FieldLabel="Adjust Interest by"
                            LabelAlign="Top" LabelSeparator="" Width="150px">
                        </ext:TextField>
                        <%-- <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator4" runat="server"
                            ValidationGroup="SaveUpdateAddLoan" ControlToValidate="txtAddLoanAmount" ErrorMessage="Amount is required." />--%>
                        <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator12" runat="server"
                            ValidationGroup="SaveUpdateAdjustInterest" ControlToValidate="txtInterestAdjustmentAmount"
                            ErrorMessage="Interest adjustment is required." />
                        <asp:CompareValidator SetFocusOnError="true" ValueToCompare='0' Display="None" Operator="DataTypeCheck"
                            Type="Currency" ID="CompareValidator8" ControlToValidate="txtInterestAdjustmentAmount"
                            ValidationGroup="SaveUpdateAdjustInterest" runat="server" ErrorMessage="Invalid amount, should be greater than 0."></asp:CompareValidator>
                    </td>
                    <td>
                        <ext:DateField ID="dateInterestAdjustment" LabelSeparator="" LabelAlign="Top" runat="server"
                            LabelWidth="120" FieldLabel="Adjustment Date">
                            <Plugins>
                                <ext:GenericPlugin ID="GenericPlugin3" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                            </Plugins>
                        </ext:DateField>
                        <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator13" runat="server"
                            ValidationGroup="SaveUpdateAdjustInterest" ControlToValidate="dateInterestAdjustment"
                            ErrorMessage="Date is required." />
                    </td>
                    <td>
                        <ext:DisplayField ID="dispInterestAdjustment" LabelSeparator="" LabelAlign="Top"
                            runat="server" LabelWidth="120" FieldLabel="Current Period" />
                    </td>
                </tr>
                <tr>
                    <td valign="bottom" colspan="2">
                        <div class="popupButtonDiv">
                            <ext:Button runat="server" ID="btnSaveInterestAdjustment" Cls="btn btn-primary" Text="<i></i>Save">
                                <DirectEvents>
                                    <Click OnEvent="btnSaveInterestAdjustment_Click">
                                        <EventMask ShowMask="true" />
                                    </Click>
                                </DirectEvents>
                                <Listeners>
                                    <Click Handler="valGroup = 'SaveUpdateAdjustInterest'; return CheckValidation();">
                                    </Click>
                                </Listeners>
                            </ext:Button>
                            <div class="btnFlatOr">
                                or</div>
                            <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="LinkButton4" Cls="btnFlatLeftGap"
                                Text="<i></i>Cancel">
                                <Listeners>
                                    <Click Handler="#{windowAdjustInterest}.hide();">
                                    </Click>
                                </Listeners>
                            </ext:LinkButton>
                        </div>
                    </td>
                </tr>
            </table>
        </Content>
    </ext:Window>
    <ext:Window ID="windowInterest" runat="server" Title="Change Interest" Icon="Application"
        Height="280" Width="550" BodyPadding="5" Hidden="true" Modal="true">
        <Content>
            <table class="fieldTable">
                <tr>
                    <td>
                        <ext:DateField ID="dateInterest" LabelSeparator="" LabelAlign="Top" runat="server"
                            LabelWidth="120" FieldLabel="Change Date">
                            <Plugins>
                                <ext:GenericPlugin ID="GenericPlugin2" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                            </Plugins>
                        </ext:DateField>
                        <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator6" runat="server"
                            ValidationGroup="SaveUpdateInterest" ControlToValidate="dateInterest" ErrorMessage="Date is required." />
                    </td>
                    <td>
                        <ext:DisplayField ID="dispInterestCurrentPeriod" LabelSeparator="" LabelAlign="Top"
                            runat="server" LabelWidth="120" FieldLabel="Current Period" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:TextField ID="txtInterest" runat="server" FieldLabel="New Interest Rate" LabelAlign="Top"
                            LabelSeparator="" Width="150px">
                        </ext:TextField>
                        <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator7" runat="server"
                            ValidationGroup="SaveUpdateInterest" ControlToValidate="txtInterest" ErrorMessage="Rate is required." />
                        <asp:CompareValidator SetFocusOnError="true" ValueToCompare='0' Display="None" Operator="GreaterThan"
                            Type="Double" ID="CompareValidator6" ControlToValidate="txtInterest" ValidationGroup="SaveUpdateInterest"
                            runat="server" ErrorMessage="Invalid rate."></asp:CompareValidator>
                    </td>
                    <td>
                        <ext:TextField ID="txtMarketRate" runat="server" FieldLabel="New Market Rate" LabelAlign="Top"
                            LabelSeparator="" Width="150px">
                        </ext:TextField>
                        <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator8" runat="server"
                            ValidationGroup="SaveUpdateInterest" ControlToValidate="txtMarketRate" ErrorMessage="Market rate is required." />
                        <asp:CompareValidator SetFocusOnError="true" ValueToCompare='0' Display="None" Operator="GreaterThan"
                            Type="Double" ID="CompareValidator7" ControlToValidate="txtMarketRate" ValidationGroup="SaveUpdateInterest"
                            runat="server" ErrorMessage="Invalid rate."></asp:CompareValidator>
                    </td>
                    <tr>
                        <td valign="bottom" colspan="2">
                            <div class="popupButtonDiv">
                                <ext:Button runat="server" ID="btnSaveInterest" Cls="btn btn-primary" Text="<i></i>Save">
                                    <DirectEvents>
                                        <Click OnEvent="btnSaveInterest_Click">
                                            <EventMask ShowMask="true" />
                                        </Click>
                                    </DirectEvents>
                                    <Listeners>
                                        <Click Handler="valGroup = 'SaveUpdateInterest'; return CheckValidation();">
                                        </Click>
                                    </Listeners>
                                </ext:Button>
                                <div class="btnFlatOr">
                                    or</div>
                                <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="LinkButton3" Cls="btnFlatLeftGap"
                                    Text="<i></i>Cancel">
                                    <Listeners>
                                        <Click Handler="#{windowInterest}.hide();">
                                        </Click>
                                    </Listeners>
                                </ext:LinkButton>
                            </div>
                        </td>
                    </tr>
            </table>
        </Content>
    </ext:Window>
</asp:Content>
