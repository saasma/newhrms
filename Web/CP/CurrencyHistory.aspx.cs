﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using BLL;
using DAL;
using Utils.Web;
using Utils.Helper;
using System.Text;

namespace Web.CP
{



    public partial class CurrencyHistory : BasePage
    {
        CompanyManager compMgr = new CompanyManager();



        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                Initialise();          

        }

        

        public void Initialise()
        {
            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.BritishSchool || CommonManager.CompanySetting.IsAllIncomeInForeignCurrency == true)
                gvw.Columns[2].Visible = false;

            gvw.DataSource = CommonManager.GetCurrencyHistory();
            gvw.DataBind();
        }


     

      
    }

}

