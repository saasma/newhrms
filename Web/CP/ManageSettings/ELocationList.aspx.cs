﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;
using System.Text;

namespace Web.CP.ManageSettings
{
    public partial class ELocationList : BasePage
    {

        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.AdministratorTools;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        private void Initialise()
        {
            BindELocations();
            hiddenValue.Text = "";
        }

        private void BindELocations()
        {
            gridELocation.GetStore().DataSource = ListManager.GetELocationListWithRemoteArea();
            gridELocation.GetStore().DataBind();

            cmbRemoteArea.Store[0].DataSource = ListManager.GetRemoteAreaWithNameAndAmount();
            cmbRemoteArea.Store[0].DataBind();
        }

        protected void btnAddNew_Click(object sender, DirectEventArgs e)
        {
            txtName.Text = "";
            hiddenValue.Text = "";
            WELocation.Center();
            WELocation.Show();
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveUpdELoc");
            if (Page.IsValid)
            {
                ELocation obj = new ELocation();

                obj.Name = txtName.Text.Trim();

                if (!string.IsNullOrEmpty(hiddenValue.Text))
                    obj.LocationId = int.Parse(hiddenValue.Text);

                if (cmbRemoteArea.SelectedItem != null && cmbRemoteArea.SelectedItem.Value != null)
                {
                    obj.RemoteAreaId = int.Parse(cmbRemoteArea.SelectedItem.Value);
                }

                obj.CompanyId = SessionManager.CurrentCompanyId;

                Status status = ListManager.SaveUpdateELocation(obj);
                if (status.IsSuccess)
                {
                    if (!string.IsNullOrEmpty(hiddenValue.Text))
                        NewMessage.ShowNormalMessage("Record updated successfully.");
                    else
                        NewMessage.ShowNormalMessage("Record saved successfully.");
                    WELocation.Close();
                    BindELocations();
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }

            }
        }

        protected void btnEdit_Click(object sender, DirectEventArgs e)
        {
            int id = int.Parse(hiddenValue.Text);
            ELocation obj = ListManager.GetELocationById(id);
            if (obj != null)
            {
                txtName.Text = obj.Name;
                if (obj.RemoteAreaId != null)
                    cmbRemoteArea.SetValue(obj.RemoteAreaId.ToString());
                else
                    cmbRemoteArea.ClearValue();
                WELocation.Center();
                WELocation.Show();
            }
        }

        protected void btnDelete_Click(object sender, DirectEventArgs e)
        {
            int id = int.Parse(hiddenValue.Text);

            Status status = ListManager.DeleteELocation(id);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Record deleted successfully.");
                BindELocations();
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }
    }
}