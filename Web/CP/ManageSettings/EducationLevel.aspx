﻿<%@ Page Title="Education Level List" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="EducationLevel.aspx.cs" Inherits="Web.CP.ManageSettings.EducationLevel" %>

<%@ Register Src="~/NewHR/UserControls/ListWizard.ascx" TagName="listWizard" TagPrefix="uc" %>
<%@ Register Src="~/Controls/ContentHeader.ascx" TagName="ContentHeader" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
    
    var CommandHandler = function(command, record){
            <%= hiddenValue.ClientID %>.setValue(record.data.ID);
            
                if(command=="Edit")
                {
                    <%= btnEdit.ClientID %>.fireEvent('click');
                }
                else
                {
                    <%= btnDelete.ClientID %>.fireEvent('click');
                }

             }

    </script>
    <style type="text/css">
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
        #content
        {
            margin: inherit;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <%--<uc1:ContentHeader Id="ContentHeader1" runat="server" />--%>
    <ext:Hidden ID="hiddenValue" runat="server" />
    <ext:LinkButton ID="btnEdit" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnEdit_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:LinkButton ID="btnDelete" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnDelete_Click">
                <EventMask ShowMask="true" />
                <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the level?" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Education Level List
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <table>
            <tr>
                <td valign="top">
                    <div style="float: left; margin-right: 10px;">
                        <uc:listWizard Id="ListWizard1" runat="server" />
                    </div>
                </td>
                <td valign="top">
                    <div class="panel panel-default">
                        <!-- panel-heading -->
                        <div class="panel-body">
                            <div class="innerLR">
                                <ext:GridPanel ID="gridEducationLevel" runat="server" Width="280" Cls="itemgrid">
                                    <Store>
                                        <ext:Store ID="Store1" runat="server">
                                            <Model>
                                                <ext:Model ID="Model4" runat="server" IDProperty="ID">
                                                    <Fields>
                                                        <ext:ModelField Name="ID" Type="String" />
                                                        <ext:ModelField Name="Name" Type="string" />
                                                    </Fields>
                                                </ext:Model>
                                            </Model>
                                        </ext:Store>
                                    </Store>
                                    <ColumnModel>
                                        <Columns>
                                            <ext:Column ID="Column5" Sortable="false" MenuDisabled="true" runat="server" Text="Level Name"
                                                Align="Left" Width="200" DataIndex="Name" />
                                            <ext:CommandColumn ID="CommandColumn1" runat="server" Width="80" Text="" Align="Center">
                                                <Commands>
                                                    <ext:CommandSeparator />
                                                    <ext:GridCommand Cls="editGridButton" ToolTip-Text="Edit" Text="<i class='fa fa-pencil'></i>"
                                                        CommandName="Edit" />
                                                    <ext:GridCommand Cls="editGridButton" ToolTip-Text="Delete" Text="<i class='fa fa-trash-o'></i>"
                                                        CommandName="Delete" />
                                                </Commands>
                                                <Listeners>
                                                    <Command Handler="CommandHandler(command,record);" />
                                                </Listeners>
                                            </ext:CommandColumn>
                                        </Columns>
                                    </ColumnModel>
                                    <SelectionModel>
                                        <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                                    </SelectionModel>
                                </ext:GridPanel>
                                <div class="buttonBlock" runat="server" id="buttonBlock">
                                    <ext:Button runat="server" Cls="btn btn-primary" ID="btnAddNew" Text="<i></i>Add New">
                                        <DirectEvents>
                                            <Click OnEvent="btnAddNew_Click">
                                                <EventMask ShowMask="true" />
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </div>
                                <br />
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <br />
    <ext:Window ID="WLevel" runat="server" Title="Add/Edit Education Level" Icon="Application"
        Width="300" Height="200" BodyPadding="5" Hidden="true" Modal="true">
        <Content>
            <table class="fieldTable">
                <tr>
                    <td>
                        <ext:TextField ID="txtName" LabelSeparator="" runat="server" FieldLabel="Name *"
                            LabelWidth="60" Width="250" LabelAlign="Left">
                        </ext:TextField>
                        <asp:RequiredFieldValidator Display="None" ID="rfvName" runat="server" ValidationGroup="SaveUpd"
                            ControlToValidate="txtName" ErrorMessage="Name is required." />
                    </td>
                </tr>
                <tr>
                    <td valign="bottom">
                        <div class="popupButtonDiv">
                            <ext:Button runat="server" Cls="btn btn-primary" Text="<i></i>Save">
                                <DirectEvents>
                                    <Click OnEvent="btnSave_Click">
                                        <EventMask ShowMask="true" />
                                    </Click>
                                </DirectEvents>
                                <Listeners>
                                    <Click Handler="valGroup = 'SaveUpd'; if(CheckValidation()) return this.disable(); else return false;">
                                    </Click>
                                </Listeners>
                            </ext:Button>
                            <div class="btnFlatOr">
                                or</div>
                            <ext:LinkButton runat="server" Cls="btnFlatLeftGap" ID="LinkButton1" Text="<i></i>Cancel">
                                <Listeners>
                                    <Click Handler="#{WLevel}.hide();">
                                    </Click>
                                </Listeners>
                            </ext:LinkButton>
                        </div>
                    </td>
                </tr>
            </table>
        </Content>
    </ext:Window>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
