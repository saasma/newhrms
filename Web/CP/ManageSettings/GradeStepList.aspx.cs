﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;
using System.Text;

namespace Web.CP.ManageSettings
{
    public partial class GradeStepList : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        private void Initialise()
        {
            BindGrades();
            hiddenValue.Text = "";
        }

        private void BindGrades()
        {
            gridGrade.GetStore().DataSource = ListManager.GetGrades();
            gridGrade.GetStore().DataBind();
        }

        private void Clear()
        {
            txtName.Text = "";
            txtOrder.Text = "0";                
        }

        protected void btnAddNew_Click(object sender, DirectEventArgs e)
        {            
            hiddenValue.Text = "";
            Clear();
            WGrade.Show();
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveUpdateGrade");
            if (Page.IsValid)
            {
                EGrade obj = new EGrade();

                obj.Name = txtName.Text.Trim();
                obj.CompanyId = SessionManager.CurrentCompanyId;

                if(!string.IsNullOrEmpty(txtOrder.Text.Trim()) && txtOrder.Text.Trim() != "0")
                    obj.Order = int.Parse(txtOrder.Text.Trim());

                if (!string.IsNullOrEmpty(hiddenValue.Text))
                    obj.GradeId = int.Parse(hiddenValue.Text);

                Status status = ListManager.SaveUpdateGrade(obj);
                if (status.IsSuccess)
                {
                    if (!string.IsNullOrEmpty(hiddenValue.Text))
                        NewMessage.ShowNormalMessage("Record updated successfully.");
                    else
                        NewMessage.ShowNormalMessage("Record saved successfully.");
                    WGrade.Close();
                    BindGrades();
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }

            }
        }

        protected void btnEdit_Click(object sender, DirectEventArgs e)
        {
            int id = int.Parse(hiddenValue.Text);
            Clear();
            EGrade obj = ListManager.GetEGradeById(id);
            if (obj != null)
            {
                txtName.Text = obj.Name;
                if (obj.Order != null)
                    txtOrder.Text = obj.Order.ToString();
                WGrade.Show();
            }
        }

        protected void btnDelete_Click(object sender, DirectEventArgs e)
        {
            int id = int.Parse(hiddenValue.Text);

            Status status = ListManager.DeleteGrade(id);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Record deleted successfully.");
                BindGrades();
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }
    }
}