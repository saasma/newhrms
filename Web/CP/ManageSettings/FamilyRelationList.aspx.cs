﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;
using System.Text;

namespace Web.CP.ManageSettings
{
    public partial class FamilyRelationList : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.AdministratorTools;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        private void Initialise()
        {
            BindRelations();
            hiddenValue.Text = "";
        }

        private void BindRelations()
        {
            gridFamilyRelation.GetStore().DataSource = ListManager.GetFamilyRelations();
            gridFamilyRelation.GetStore().DataBind();
        }

        protected void btnAddNew_Click(object sender, DirectEventArgs e)
        {
            txtName.Text = "";
            hiddenValue.Text = "";
            WFRelation.Show();
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveUpdReln");
            if (Page.IsValid)
            {
                FixedValueFamilyRelation obj = new FixedValueFamilyRelation();

                obj.Name = txtName.Text.Trim();

                if (!string.IsNullOrEmpty(hiddenValue.Text))
                    obj.ID = int.Parse(hiddenValue.Text);

                obj.ShowBirthdayInFamilyListReport = chkShowBirthday.Checked;

                Status status = ListManager.SaveUpdateRelation(obj);
                if (status.IsSuccess)
                {
                    if (!string.IsNullOrEmpty(hiddenValue.Text))
                        NewMessage.ShowNormalMessage("Record updated successfully.");
                    else
                        NewMessage.ShowNormalMessage("Record saved successfully.");
                    WFRelation.Close();
                    BindRelations();
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }

            }
        }

        protected void btnEdit_Click(object sender, DirectEventArgs e)
        {
            int id = int.Parse(hiddenValue.Text);
            FixedValueFamilyRelation obj = ListManager.GetFamilyRelationById(id);
            if (obj != null)
            {
                txtName.Text = obj.Name;
                if (obj.ShowBirthdayInFamilyListReport != null)
                    chkShowBirthday.Checked = obj.ShowBirthdayInFamilyListReport.Value;
                else
                    chkShowBirthday.Checked = false;
                WFRelation.Show();
            }
        }

        protected void btnDelete_Click(object sender, DirectEventArgs e)
        {
            int id = int.Parse(hiddenValue.Text);

            Status status = ListManager.DeleteFixedValueFamilyReln(id);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Record deleted successfully.");
                BindRelations();
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }
    }
}