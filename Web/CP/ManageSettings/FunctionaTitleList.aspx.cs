﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;
using System.Text;

namespace Web.CP.ManageSettings
{
    public partial class FunctionaTitleList : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.AdministratorTools;
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        private void Initialise()
        {
            BindTrainingType();
            hiddenValue.Text = "";
        }

        private void BindTrainingType()
        {
            gridTrainingType.GetStore().DataSource = ListManager.GetFunctionalTitleType();
            gridTrainingType.GetStore().DataBind();
        }

        protected void btnAddNew_Click(object sender, DirectEventArgs e)
        {
            txtName.Text = "";
            hiddenValue.Text = "";
            WTrainingType.Show();
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveUpdTrainingType");
            if (Page.IsValid)
            {
                FunctionalTitle obj = new FunctionalTitle();

                obj.Name = txtName.Text.Trim();

                if (!string.IsNullOrEmpty(hiddenValue.Text))
                    obj.FunctionalTitleId = int.Parse(hiddenValue.Text);

                Status status = ListManager.SaveUpdateFunctionTitle(obj);
                if (status.IsSuccess)
                {
                    if (!string.IsNullOrEmpty(hiddenValue.Text))
                        NewMessage.ShowNormalMessage("Record updated successfully.");
                    else
                        NewMessage.ShowNormalMessage("Record saved successfully.");
                    WTrainingType.Close();
                    BindTrainingType();
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }

            }
        }

        protected void btnEdit_Click(object sender, DirectEventArgs e)
        {
            int id = int.Parse(hiddenValue.Text);
            FunctionalTitle obj = ListManager.GetTypeById(id);
            if (obj != null)
            {
                txtName.Text = obj.Name;
                WTrainingType.Show();
            }
        }

        protected void btnDelete_Click(object sender, DirectEventArgs e)
        {
            int id = int.Parse(hiddenValue.Text);

            Status status = ListManager.DeleteFunctionTitleType(id);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Record deleted successfully.");
                BindTrainingType();
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }
    }
}