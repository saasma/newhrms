﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;
using System.Text;

namespace Web.CP.ManageSettings
{
    public partial class EducationLevel : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.AdministratorTools;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        private void Initialise()
        {
            BindLevel();
            hiddenValue.Text = "";
        }

        private void BindLevel()
        {
            gridEducationLevel.GetStore().DataSource = NewHRManager.GetAddEducationLevel();
            gridEducationLevel.GetStore().DataBind();
        }

        protected void btnAddNew_Click(object sender, DirectEventArgs e)
        {
            txtName.Text = "";
            hiddenValue.Text = "";
            WLevel.Show();
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveUpd");
            if (Page.IsValid)
            {
                FixedValueEducationLevel obj = new FixedValueEducationLevel();

                obj.Name = txtName.Text.Trim();

                if (!string.IsNullOrEmpty(hiddenValue.Text))
                    obj.ID = int.Parse(hiddenValue.Text);

                Status status = NewHRManager.SaveUpdateFixedValueEducationLevel(obj);
                if (status.IsSuccess)
                {
                    if (!string.IsNullOrEmpty(hiddenValue.Text))
                        NewMessage.ShowNormalMessage("Record updated successfully.");
                    else
                        NewMessage.ShowNormalMessage("Record saved successfully.");
                    WLevel.Close();
                    BindLevel();
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }

            }
        }

        protected void btnEdit_Click(object sender, DirectEventArgs e)
        {
            int id = int.Parse(hiddenValue.Text);
            FixedValueEducationLevel obj = NewHRManager.GetFixedValueEducationLevelyById(id);
            if (obj != null)
            {
                txtName.Text = obj.Name;
                WLevel.Show();
            }
        }

        protected void btnDelete_Click(object sender, DirectEventArgs e)
        {
            int id = int.Parse(hiddenValue.Text);

            Status status = NewHRManager.DeleteFixedValueEducationLevel(id);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Record deleted successfully.");
                BindLevel();
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }
    }
}