﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;
using System.Text;

namespace Web.CP.ManageSettings
{
    public partial class SubDepartmentList : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        private void Initialise()
        {
            Clear();
            BindSubDepartments();
        }

        private void Clear()
        {
            txtName.Text = "";
            txtDescription.Text = "";
        }

        private void BindSubDepartments()
        {
            gridSubDepartment.GetStore().DataSource = ListManager.GetSubDepartments();
            gridSubDepartment.GetStore().DataBind();
        }

        protected void btnAddNew_Click(object sender, DirectEventArgs e)
        {
            Clear();
            hiddenValue.Text = "";
            WSubDepartment.Show();
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveUpdateSubDept");
            if (Page.IsValid)
            {
                SubDepartment obj = new SubDepartment();

                if (!string.IsNullOrEmpty(hiddenValue.Text))
                    obj.SubDepartmentId = int.Parse(hiddenValue.Text);

                obj.Name = txtName.Text.Trim();

                if (!string.IsNullOrEmpty(txtDescription.Text.Trim()))
                    obj.Description = txtDescription.Text.Trim();

                Status status = ListManager.SaveUpdateSubDepartment(obj);
                if (status.IsSuccess)
                {
                    if (string.IsNullOrEmpty(hiddenValue.Text))
                        NewMessage.ShowNormalMessage("Record saved successfully.");
                    else
                        NewMessage.ShowNormalMessage("Record updated successfully.");

                    BindSubDepartments();
                    WSubDepartment.Close();
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }
                
            }
        }

        protected void btnEdit_Click(object sender, DirectEventArgs e)
        {
            Clear();
            int subDepartmentId = int.Parse(hiddenValue.Text);

            SubDepartment obj = ListManager.GetSubDepartmentById(subDepartmentId);
            if (obj != null)
            {
                txtName.Text = obj.Name;

                if (obj.Description != null)
                    txtDescription.Text = obj.Description;

                WSubDepartment.Show();
            }
        }

        protected void btnDelete_Click(object sender, DirectEventArgs e)
        {
            int subDepartmentId = int.Parse(hiddenValue.Text);
            Status status = ListManager.DeleteSubDepartment(subDepartmentId);

            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Record deleted successfully.");
                BindSubDepartments();
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        

    }
}