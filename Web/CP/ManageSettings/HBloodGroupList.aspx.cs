﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;
using System.Text;


namespace Web.CP.ManageSettings
{
    public partial class HBloodGroupList : BasePage
    {

        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.AdministratorTools;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        private void Initialise()
        {
            BindBloodGroups();
            hiddenValue.Text = "";
        }

        private void BindBloodGroups()
        {
            gridBloodGroup.GetStore().DataSource = ListManager.GetHBloodGroups();
            gridBloodGroup.GetStore().DataBind();
        }

        protected void btnAddNew_Click(object sender, DirectEventArgs e)
        {
            txtName.Text = "";
            hiddenValue.Text = "";
            WBloodGroup.Show();
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveUpdBG");
            if (Page.IsValid)
            {
                HBloodGroup obj = new HBloodGroup();

                obj.BloodGroupName = txtName.Text.Trim();

                if (!string.IsNullOrEmpty(hiddenValue.Text))
                    obj.Id = int.Parse(hiddenValue.Text);

                Status status = ListManager.SaveUpdateHBloodGroup(obj);
                if (status.IsSuccess)
                {
                    if (!string.IsNullOrEmpty(hiddenValue.Text))
                        NewMessage.ShowNormalMessage("Record updated successfully.");
                    else
                        NewMessage.ShowNormalMessage("Record saved successfully.");
                    WBloodGroup.Close();
                    BindBloodGroups();
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }

            }
        }

        protected void btnEdit_Click(object sender, DirectEventArgs e)
        {
            int id = int.Parse(hiddenValue.Text);
            HBloodGroup obj = ListManager.GetHBloodGroupById(id);
            if (obj != null)
            {
                txtName.Text = obj.BloodGroupName;
                WBloodGroup.Show();
            }
        }

        protected void btnDelete_Click(object sender, DirectEventArgs e)
        {
            int id = int.Parse(hiddenValue.Text);

            Status status = ListManager.DeleteHBloodGroupById(id);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Record deleted successfully.");
                BindBloodGroups();
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }
    }
}