﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;
using System.Text;

namespace Web.CP.ManageSettings
{
    public partial class RetirementTypes : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        private void Initialise()
        {

            StoreTypes.DataSource = NewHelper.GetTextValues(typeof(ServiceEventType)).OrderBy(x => x.Text);
            StoreTypes.DataBind();


            BindFaculty();
            hiddenValue.Text = "";
        }

        private void BindFaculty()
        {
            gridEducationFaculty.GetStore().DataSource = CommonManager.GetServieEventTypes();
            gridEducationFaculty.GetStore().DataBind();
        }

        protected void btnAddNew_Click(object sender, DirectEventArgs e)
        {
            txtName.Text = "";
            hiddenValue.Text = "";
            WFaculty.Show();
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveUpd");
            if (Page.IsValid)
            {
                HR_Service_Event obj = new HR_Service_Event();

                obj.Name = txtName.Text.Trim();

                if (!string.IsNullOrEmpty(hiddenValue.Text))
                    obj.EventID = int.Parse(hiddenValue.Text);

                obj.GroupID = int.Parse(cmbEventTypes.SelectedItem.Value);

                Status status = NewHRManager.SaveUpdateRetirementType(obj);
                if (status.IsSuccess)
                {
                    if (!string.IsNullOrEmpty(hiddenValue.Text))
                        NewMessage.ShowNormalMessage("Record updated successfully.");
                    else
                        NewMessage.ShowNormalMessage("Record saved successfully.");
                    WFaculty.Close();
                    BindFaculty();
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }

            }
        }

        protected void btnEdit_Click(object sender, DirectEventArgs e)
        {
            int id = int.Parse(hiddenValue.Text);
            HR_Service_Event obj = NewHRManager.GetRetirementType(id);
            if (obj != null)
            {
                txtName.Text = obj.Name;
                if (obj.GroupID != null)
                    cmbEventTypes.SetValue(obj.GroupID.ToString());
                else
                    cmbEventTypes.ClearValue();
                WFaculty.Show();
            }
        }

        protected void btnDelete_Click(object sender, DirectEventArgs e)
        {
            int id = int.Parse(hiddenValue.Text);

            Status status = NewHRManager.DeleteRetirementType(id);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Record deleted successfully.");
                BindFaculty();
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }
    }
}