﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;
using System.Text;

namespace Web.CP.ManageSettings
{
    public partial class DocumentSubCategory : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.DocumentManagement;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        private void Initialise()
        {
            BindDocSubCategories();
            BindCategoryComboBox();
        }

        private void BindCategoryComboBox()
        {
            cmbCategory.Store[0].DataSource = DocumentManager.GetDocCategories();
            cmbCategory.Store[0].DataBind();
        }

        private void BindDocSubCategories()
        {
            gridDocSubCategory.GetStore().DataSource = DocumentManager.GetDocSubCategories();
            gridDocSubCategory.GetStore().DataBind();
        }

        protected void btnAddNew_Click(object sender, DirectEventArgs e)
        {
            btnSave.Text = "Save";
            txtName.Text = "";
            txtCode.Text = "";
            cmbCategory.ClearValue();
            hiddenValue.Text = "";
            WDocumentSubCategory.Center();
            WDocumentSubCategory.Show();
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveUpd");
            if (Page.IsValid)
            {
                bool isSave = true;
                DocSubCategory obj = new DocSubCategory();

                obj.Name = txtName.Text.Trim();
                obj.Code = txtCode.Text.Trim();

                if (!string.IsNullOrEmpty(hiddenValue.Text))
                {
                    obj.SubCategoryID = new Guid(hiddenValue.Text);
                    isSave = false;
                }
                else
                    obj.SubCategoryID = Guid.NewGuid();

                obj.CategoryID = new Guid(cmbCategory.SelectedItem.Value);

                Status status = DocumentManager.SaveUpdateDocSubCategory(obj, isSave);
                if (status.IsSuccess)
                {
                    if (!string.IsNullOrEmpty(hiddenValue.Text))
                        NewMessage.ShowNormalMessage("Record updated successfully.");
                    else
                        NewMessage.ShowNormalMessage("Record saved successfully.");

                    WDocumentSubCategory.Center();
                    WDocumentSubCategory.Close();
                    BindDocSubCategories();
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }

            }
        }

        protected void btnEdit_Click(object sender, DirectEventArgs e)
        {
            Guid subCategoryId = new Guid(hiddenValue.Text);
            DocSubCategory objDocSubCategory = DocumentManager.GetDocSubCategoryById(subCategoryId);
            if (objDocSubCategory != null)
            {
                txtName.Text = objDocSubCategory.Name;
                txtCode.Text = objDocSubCategory.Code;
                cmbCategory.SetValue(objDocSubCategory.CategoryID.Value);
                btnSave.Text = "Update";
                WDocumentSubCategory.Center();
                WDocumentSubCategory.Show();
            }
        }

        protected void btnDelete_Click(object sender, DirectEventArgs e)
        {
            Guid subCategoryId = new Guid(hiddenValue.Text);

            Status status = DocumentManager.DeleteDocSubCategories(subCategoryId);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Record deleted successfully.");
                BindDocSubCategories();
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }
    }
}