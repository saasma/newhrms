﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;
using System.Text;

namespace Web.CP.ManageSettings
{
    public partial class DocumentCategory : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.DocumentManagement;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        private void Initialise()
        {
            BindDocCategoryGrid();
        }

        private void BindDocCategoryGrid()
        {
            gridDocCategory.GetStore().DataSource = DocumentManager.GetDocCategories();
            gridDocCategory.GetStore().DataBind();
        }

        protected void btnAddNew_Click(object sender, DirectEventArgs e)
        {
            btnSave.Text = "Save";
            txtName.Text = "";
            txtCode.Clear();
            hiddenValue.Text = "";
            WDocumentCategory.Center();
            WDocumentCategory.Show();
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveUpd");
            if (Page.IsValid)
            {
                bool isSave = true;

                DocCategory obj = new DocCategory();

                obj.Name = txtName.Text.Trim();
                obj.Code = txtCode.Text.Trim();

                if (!string.IsNullOrEmpty(hiddenValue.Text))
                {
                    obj.CategoryID = new Guid(hiddenValue.Text);
                    isSave = false;
                }
                else
                    obj.CategoryID = Guid.NewGuid();

                Status status = DocumentManager.SaveUpdateDocCategory(obj, isSave);
                if (status.IsSuccess)
                {
                    if (!string.IsNullOrEmpty(hiddenValue.Text))
                        NewMessage.ShowNormalMessage("Record updated successfully.");
                    else
                        NewMessage.ShowNormalMessage("Record saved successfully.");

                    WDocumentCategory.Close();
                    BindDocCategoryGrid();
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }

            }
        }

        protected void btnEdit_Click(object sender, DirectEventArgs e)
        {
            Guid categoryId = new Guid(hiddenValue.Text);
            DocCategory objDocCategory = DocumentManager.GetDocCategoryById(categoryId);
            if (objDocCategory != null)
            {
                txtName.Text = objDocCategory.Name;
                txtCode.Text = objDocCategory.Code;
                btnSave.Text = "Update";
                WDocumentCategory.Center();
                WDocumentCategory.Show();
            }
        }

        protected void btnDelete_Click(object sender, DirectEventArgs e)
        {
            Guid categoryId = new Guid(hiddenValue.Text);

            Status status = DocumentManager.DeleteDocCategories(categoryId);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Record deleted successfully.");
                BindDocCategoryGrid();
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }
    }
}