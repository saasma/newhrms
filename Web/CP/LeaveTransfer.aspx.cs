﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Manager;
using BLL;
using DAL;

using Utils.Helper;
using BLL.Base;

namespace Web.CP
{
    public partial class LeaveTransfer : BasePage
    {
        public bool isPayrollEditable = true;
        private static int payrollPeriodId = -1;
        private static int leaveTypeId = -1;
        private static int currentPayrollPeriodId = -1;

        protected void Page_Load(object sender, EventArgs e)
        {
            if( !IsPostBack)
            {

             

                //currentPayrollPeriodId = -1;

                ddlLeaves.DataSource = LeaveAttendanceManager.GetYearlyMonthlyLeaves(SessionManager.CurrentCompanyId);
                ddlLeaves.DataTextField = "Title";
                ddlLeaves.DataValueField = "LeaveTypeId";
                ddlLeaves.DataBind();

                ddlPayrollPeriods.DataSource = CommonManager.GetCurrentYear(SessionManager.CurrentCompanyId);
                ddlPayrollPeriods.DataBind();

                if (ddlPayrollPeriods.Items.Count > 0)
                {
                    ListItem selItem = ddlPayrollPeriods.Items[ddlPayrollPeriods.Items.Count - 1];
                    if (selItem != null)
                        selItem.Selected = true;
                }


                payrollPeriodId = int.Parse(ddlPayrollPeriods.SelectedItem.Value);
                leaveTypeId = int.Parse(ddlLeaves.SelectedItem.Value);
                currentPayrollPeriodId = int.Parse(ddlPayrollPeriods.SelectedItem.Value);

                divLeaveTransfer.Visible = false;

            }

            //string code=string.Format( "var diffMsg = '{0}';", Resources.Messages.LeaveAdjustmentDiffMsg);
            //RegisterJSCode(code);
        }

        protected void LoadDonorReceiverEmployee()
        {
            
            txtDonatingHours.Text = string.Empty;
            txtDonorBalance.Text = string.Empty;
            txtReveiverBalance.Text = string.Empty;

            if (ddlLeaves.SelectedIndex != -1 && ddlPayrollPeriods.SelectedIndex != -1)
            {
                // payrollPeriodId = int.Parse(ddlPayrollPeriods.SelectedItem.Value);
                // leaveTypeId = int.Parse(ddlLeaves.SelectedItem.Value);

                ddlDonor.Items.Clear();
                ddlDonor.DataSource = LeaveAttendanceManager.GetEmployeesForLeaveBalance(SessionManager.CurrentCompanyId, leaveTypeId, payrollPeriodId );
                ddlDonor.DataTextField = "EmployeeName";
                ddlDonor.DataValueField = "Value";
                ddlDonor.DataBind();
                ddlDonor.Items.Insert(0, new ListItem( "--Select donor--","-1"));
                ddlDonor.SelectedIndex = 0;

                ddlReceiver.DataSource = LeaveAttendanceManager.GetEmployeesForLeaveBalance(SessionManager.CurrentCompanyId, leaveTypeId, payrollPeriodId);
                ddlReceiver.DataTextField = "EmployeeName";
                ddlReceiver.DataValueField = "Value";
                ddlReceiver.DataBind();
               
                ddlReceiver.Items.Insert(0, new ListItem( "--Select reveiver--","-1"));
                ddlReceiver.SelectedIndex = 0;

            }
            else
            {
                ddlDonor.Items.Clear();
                ddlDonor.DataSource = null;
                ddlDonor.DataBind();
                //ddlDonor.Items.Insert(0, "--select donor--");
                ddlDonor.SelectedIndex = -1;

                ddlReceiver.DataSource = null;
                ddlReceiver.DataBind();
                //ddlReceiver.Items.Insert(0, "--select reveiver--");
                ddlReceiver.SelectedIndex = -1;
            }
            
        }



        protected void LoadLeaveTransferDetails()
        {
            if (LeaveAttendanceManager.IsPayrollPeriodValidForLeaveTransfer(payrollPeriodId))
            {

                divLeaveTransfer.Visible = true;
                LoadDonorReceiverEmployee();


            }
            else
            {
                divLeaveTransfer.Visible = false;
                divWarningMsg.InnerHtml = "Invalid payroll period for leave transfer. For transfering leave, <br> salary generated payroll period with attendance not saved in future payroll period is required.";
                divWarningMsg.Hide = false;
            }
            BindLeaveTransfer();
        }

        protected void ddlLeaves_SelectedIndexChanged(object sender, EventArgs e)
        {
            leaveTypeId = int.Parse(ddlLeaves.SelectedItem.Value);
            LoadLeaveTransferDetails();
        }

        protected void ddlPayrollPeriodss_SelectedIndexChanged(object sender, EventArgs e)
        {
            payrollPeriodId = int.Parse(ddlPayrollPeriods.SelectedItem.Value);
            LoadLeaveTransferDetails();
        }

        protected void btnDonate_Click(object sender, EventArgs e)
        {
            LeaveAdjustment LvAdj = null;
            if (ddlDonor.SelectedItem == null || ddlReceiver.SelectedItem == null)
                return;
            if (ddlDonor.SelectedItem.Text.ToLower() == ddlReceiver.SelectedItem.Text.ToLower())
            {
                divWarningMsg.InnerHtml = "Donor and Receiver are same.\\nLeave can not be transfered to same employee.\\nPlease select different Donor or Receiver.";
                divWarningMsg.Hide = false;
                return;
            }

            DAL.LeaveTransfer leavetransfer = new DAL.LeaveTransfer();
            leavetransfer.CompanyId = SessionManager.CurrentCompanyId;
            leavetransfer.DonorEmployeeId = int.Parse(ddlDonor.SelectedItem.Value.Split(':')[0].ToString());
            leavetransfer.ReveiverEmployeeId = int.Parse(ddlReceiver.SelectedItem.Value.Split(':')[0].ToString());
            leavetransfer.PayrollPeriodId = int.Parse(ddlPayrollPeriods.SelectedItem.Value);
            leavetransfer.LeaveTypeId = int.Parse(ddlLeaves.SelectedItem.Value);
            leavetransfer.LeaveHours = decimal.Parse(txtDonatingHours.Text.Trim());
            List<LeaveAdjustment> listLvAdj = new List<LeaveAdjustment>();
            LvAdj = new LeaveAdjustment();
            LvAdj.EmployeeId = leavetransfer.DonorEmployeeId;
            LvAdj.PayrollPeriodId = leavetransfer.PayrollPeriodId;
            LvAdj.LeaveTypeId = leavetransfer.LeaveTypeId;
            LvAdj.NewBalance = decimal.Parse(ddlDonor.SelectedItem.Value.Split(':')[1].ToString()) - leavetransfer.LeaveHours;
            listLvAdj.Add(LvAdj);
            
            LvAdj = new LeaveAdjustment();
            LvAdj.EmployeeId = leavetransfer.ReveiverEmployeeId;
            LvAdj.PayrollPeriodId = leavetransfer.PayrollPeriodId;
            LvAdj.LeaveTypeId = leavetransfer.LeaveTypeId;
            LvAdj.NewBalance = decimal.Parse(ddlReceiver.SelectedItem.Value.Split(':')[1].ToString()) + leavetransfer.LeaveHours;
            listLvAdj.Add(LvAdj);

            if (LeaveAttendanceManager.SaveEmployeeLeaveTransfer(leavetransfer))
            {
                if (LeaveAttendanceManager.UpdateNewBalance(listLvAdj))
                {
                    LoadLeaveTransferDetails();
                    divMsgCtl.InnerHtml = "Leave is successfully transfered.";
                }
            }
            
        }

        protected void BindLeaveTransfer()
        {
            if (ddlLeaves.SelectedIndex == -1 || ddlPayrollPeriods.SelectedIndex == -1)
                return;
            List<GetLeaveTrasferEmployeeResult> leavetransfers = LeaveAttendanceManager.GetLeaveTrasferEmployee(SessionManager.CurrentCompanyId, leaveTypeId, payrollPeriodId);
            //if ((leavetransfers != null) && (leavetransfers.Count > 0))
            //{
                gvwLeaveTransfer.DataSource = leavetransfers;
                gvwLeaveTransfer.DataBind();
            //}
            //else
            //{
            //    gvwLeaveTransfer.DataSource = null;
            //    gvwLeaveTransfer.DataBind();
            //}
            //Page.ClientScript.RegisterClientScriptBlock(GetType(), "key", "alert('" + leaveTypeId + ":" + payrollPeriodId + "')", true);

        }
    }
}
