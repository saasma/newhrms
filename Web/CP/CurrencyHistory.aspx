<%@ Page MaintainScrollPositionOnPostback="true" Title="History" Language="C#"
    MasterPageFile="~/Master/ForPopupPage.Master" AutoEventWireup="true" CodeBehind="CurrencyHistory.aspx.cs"
    Inherits="Web.CP.CurrencyHistory" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.numeric.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.bgiframe.min.js" type="text/javascript"></script>
    <link href="../css-combo/sexy-combo.css" rel="stylesheet" type="text/css" />
    <link href="../css-combo/custom/custom.css" rel="stylesheet" type="text/css" />
    <link href="../css-combo/sexy/sexy.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        tr
        {
            padding-top: 10px;
        }
    </style>
    <script type="text/javascript">

        function closePopup() {



            window.opener.setCurrency(document.getElementById('ctl00_mainContent_hdn').value);
        }


        window.onunload = closePopup;   
        
    </script>
    <%--  <script src="../Scripts/jquery.sexy-combo.pack.js" type="text/javascript"></script>

   <script src="../Scripts/jquery.sexy-combo.min.js" type="tet/javascript"></script>

    <script src="../Scripts/jquery.sexy-combo.js" type="text/javascript"></script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <div class="popupHeader">
        <h3>
            Currency History</h3>
    </div>
    <asp:HiddenField ID="hdn" runat="server" />
    <div class=" marginal" style='margin-top: 15px'>
        <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
        <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Width="370px"  Hide="true" runat="server" />
      <cc2:EmptyDisplayGridView Style='clear: both;' GridLines="None" PagerStyle-CssClass="defaultPagingBar"
            CssClass="tableLightColor" ShowHeaderWhenEmpty="True" ID="gvw" runat="server"
            DataKeyNames="" AutoGenerateColumns="False" ShowFooterWhenEmpty="False"
            >
            <Columns>
                <asp:TemplateField HeaderText="Month" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="100px">
                    <ItemTemplate>
                        <%# Eval("Month")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Fixed Rate" ItemStyle-HorizontalAlign="Right"  HeaderStyle-HorizontalAlign="Right" HeaderStyle-Width="80px">
                    <ItemTemplate>
                        <%# Eval("FixedRate")%></ItemTemplate>
                </asp:TemplateField>
                    <asp:TemplateField HeaderText="Current Rate" ItemStyle-HorizontalAlign="Right"  HeaderStyle-HorizontalAlign="Right" HeaderStyle-Width="80px">
                    <ItemTemplate>
                        <%# Eval("CurrentRate")%></ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <PagerSettings Mode="NumericFirstLast" NextPageText="Next �" PreviousPageText="� Prev"
                FirstPageText="First" LastPageText="Last" />
            <RowStyle CssClass="odd" />
            <AlternatingRowStyle CssClass="even" />
            <PagerStyle CssClass="defaultPagingBar" />
            <SelectedRowStyle CssClass="selected" />
        </cc2:EmptyDisplayGridView>
    </div>
</asp:Content>
