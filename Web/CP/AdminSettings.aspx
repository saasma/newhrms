<%@ Page MaintainScrollPositionOnPostback="true" Title="Leave Settings" Language="C#"
    MasterPageFile="~/Master/Details.Master" AutoEventWireup="true" CodeBehind="AdminSettings.aspx.cs"
    Inherits="Web.CP.AdminSettings" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <div class="contentArea">
        <h3>
            Admin Settings</h3>
        <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
        <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
        <table style='clear: both;'>
            <tr>
                <td class="fieldHeader" style='width: 180px;'>
                    <asp:Label ID="Label4" Text="Company Monthly Hours" runat="server" />
                </td>
                <td>
                    <asp:TextBox ID="txtCompanyMonthlyHours" runat="server" Enabled="false" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtCompanyMonthlyHours"
                        Display="None" ErrorMessage="Please select Company Monthly Hours." ValidationGroup="AdminSetting"></asp:RequiredFieldValidator>
                </td>
                <td>
                    <asp:Button runat="server" ID="btnEdit1" Text="Edit" OnClick="btnEdit1_Click" />
                </td>
            </tr>
            <tr>
                <td class="fieldHeader" style='width: 180px;'>
                    <asp:Label ID="Label3" Text="Company Has Hourly Leave " runat="server" />
                </td>
                <td>
                    <asp:DropDownList Enabled="false" AppendDataBoundItems="true" Width="183px" ID="ddlCompanyHasHourlyLeave"
                        runat="server" DataValueField="Key" DataTextField="Value" >
                        <asp:ListItem Text="" Value="-1"></asp:ListItem>
                        <asp:ListItem Text="True" Value="1"></asp:ListItem>
                        <asp:ListItem Text="False" Value="0"></asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="ddlCompanyHasHourlyLeave"
                        InitialValue="-1" Display="None" ErrorMessage="Please select Company Has Hourly Leave."
                        ValidationGroup="AdminSetting"></asp:RequiredFieldValidator>
                </td>
                <td>
                    <asp:Button runat="server" ID="btnEdit2" Text="Edit" OnClick="btnEdit2_Click" />
                </td>
            </tr>
            <tr>
                <td class="fieldHeader" style='width: 180px;'>
                    <asp:Label ID="Label5" Text="Company Leave Hours In Days" runat="server" />
                </td>
                <td>
                    <asp:TextBox Enabled="false" ID="txtCompanyLeaveHoursInDays" runat="server" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtCompanyLeaveHoursInDays"
                        Display="None" ErrorMessage="Please select Company Leave Hours In Days." ValidationGroup="AdminSetting"></asp:RequiredFieldValidator>
                </td>
                <td>
                    <asp:Button runat="server" ID="btnEdit3" Text="Edit" OnClick="btnEdit3_Click" />
                </td>
            </tr>
            <tr>
                <td class="fieldHeader" style='width: 180px;'>
                    <asp:Label ID="Label6" Text="Company Is Hpl" runat="server" />
                </td>
                <td>
                    <asp:DropDownList Enabled="false" AppendDataBoundItems="true" Width="183px" ID="ddlCompanyIsHPL"
                        runat="server" DataValueField="Key" DataTextField="Value" >
                        <asp:ListItem Text="" Value="-1"></asp:ListItem>
                        <asp:ListItem Text="True" Value="1"></asp:ListItem>
                        <asp:ListItem Text="False" Value="0"></asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="ddlCompanyIsHPL"
                        InitialValue="-1" Display="None" ErrorMessage="Please select Company Is Hpl."
                        ValidationGroup="AdminSetting"></asp:RequiredFieldValidator>
                </td>
                <td>
                    <asp:Button ID="btnEdit4" Text="Edit" runat="server" OnClick="btnEdit4_Click" />
                </td>
            </tr>
            <tr>
                <td class="fieldHeader" style='width: 180px;'>
                    <asp:Label ID="Label7" Text="Gratuity Days In Year" runat="server" />
                </td>
                <td>
                    <asp:TextBox Enabled="false" ID="txtGratuityDaysInYear" runat="server" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtGratuityDaysInYear"
                        Display="None" ErrorMessage="Please select Gratuity Days In Year." ValidationGroup="AdminSetting"></asp:RequiredFieldValidator>
                </td>
                <td>
                    <asp:Button ID="btnEdit5" Text="Edit" runat="server" OnClick="btnEdit5_Click" />
                </td>
            </tr>
            <tr>
                <td class="fieldHeader" style='width: 180px;'>
                    <asp:Label ID="Label8" Text="Gratuity Permanent In Years" runat="server" />
                </td>
                <td>
                    <asp:TextBox Enabled="false" ID="txtGratuityPermanentYears" runat="server" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtGratuityPermanentYears"
                        Display="None" ErrorMessage="Please enter Gratuity Permanent In Years." ValidationGroup="AdminSetting"></asp:RequiredFieldValidator>
                </td>
                <td>
                    <asp:Button ID="btnEdit6" Text="Edit" runat="server" OnClick="btnEdit6_Click" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save"  ValidationGroup="AdminSetting"
                        OnClick="btnSave_Click" />
                </td>
            </tr>
        </table>
        <table style='clear: both;' width="100%">
            <tr>
                <td>
                    <uc2:MsgCtl ID="MsgCtl1" EnableViewState="false" Hide="true" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <cc2:EmptyDisplayGridView CssClass="tableLightColor" UseAccessibleHeader="true" ShowHeaderWhenEmpty="True"
                        ID="gvwCompanySetting" runat="server" DataKeyNames="Key" GridLines="None" AutoGenerateColumns="False"
                        AllowSorting="True" ShowFooterWhenEmpty="False">
                        <Columns>
                            <asp:TemplateField HeaderText="Name"  HeaderStyle-Width="300px">
                                <ItemTemplate>
                                    <asp:Label ID="lblKey" runat="server" Text='<%#Eval("Key")%>' />
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Value"  HeaderStyle-Width="200px">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtValue" runat="server" Text='<%#Eval("Value") %>' enabled="false"/>
                                    <asp:RequiredFieldValidator ID="required" runat="server" ControlToValidate="txtValue" ValidationGroup="AdminSetting"
                                     ErrorMessage="Please enter a value field." Display="Dynamic" SetFocusOnError="true" />
                                    <%--^\d+(\.\d\d)?$--%>
                                    <asp:RegularExpressionValidator style='display:block' ID="Regular1" Display="Dynamic" runat="server" ControlToValidate="txtValue"
                                     ValidationExpression='<%#GetValidationExpression(Eval("DataType"))%>' ErrorMessage='<%#GetErrorMessage(Eval("DataType"))%>'
                                     ValidationGroup="AdminSetting"  />
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                
                            </asp:TemplateField>

                            <asp:TemplateField Visible="false" HeaderStyle-Width="120px">
                                <ItemTemplate>
                                    <asp:Label ID="lblDataType" runat="server" Text='<%#Eval("DataType")%>' />
                                </ItemTemplate>
                                  <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderStyle-Width="50px">
                                <ItemTemplate>
                                    <asp:Button ID="btnEditCompanySetting" runat="server" Text="Edit" 
                                        onclick="btnEditCompanySetting_Click" />
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left" Width="50px" />
                            </asp:TemplateField>
                        </Columns>
                        <RowStyle CssClass="odd" />
                        <AlternatingRowStyle CssClass="even" />
                        <EmptyDataTemplate>
                            <b>No employee records found. </b>
                        </EmptyDataTemplate>
                    </cc2:EmptyDisplayGridView>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnSaveCompanySetting" Text="Save"  CssClass="save"  runat="server" OnClick="btnSaveCompanySetting_Click"
                        ValidationGroup="AdminSetting"/>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
