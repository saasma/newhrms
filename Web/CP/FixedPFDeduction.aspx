<%@ Page Title="Fixed PF" Language="C#" MasterPageFile="~/Master/HROld.Master" AutoEventWireup="true"
    CodeBehind="FixedPFDeduction.aspx.cs" Inherits="Web.FixedPFDeduction" %>

<%@ Import Namespace="BLL.Manager" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Scripts/currency.js" type="text/javascript"></script>
    <style type="text/css">
        .tableLightColor input
        {
            width: 120px;
        }
        #content{overflow:auto;;}
    </style>
    <script src="../Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        var decimalPlaces = 0;
        function refreshWindow() {

            __doPostBack('Refresh', 0);

        }
        jQuery(document).ready(
    function () {

        setMovementToGrid('#<%= gvEmployeeIncome.ClientID %>');

        $("select").each(function (index) {
            if (this.id.indexOf("gvEmployeeIncome") >= 0) {
                citTypeChange(document.getElementById(this.id));
            }
        });


    }
);

    function citTypeChange(ddl) {
        var id = ddl.id;
       
        var txtAmount = document.getElementById(id.replace("ddlType", "txtAmount"));
        var type = ddl.value;

        if (type == "True") {
            txtAmount.readOnly = false;
            txtAmount.style.backgroundColor = '';
        }
        else {
            txtAmount.readOnly = true;
            txtAmount.style.backgroundColor = '#EBEBE4';
        }


    }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4 id="citList" runat="server">
                    Fixed Provident Fund
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="attribute" style="padding: 10px">
            <table>
                <tr>
                    <td>
                        Search &nbsp;
                        <asp:TextBox ID="txtEmployeeName" runat="server" OnTextChanged="txtEmployeeName_TextChanged"
                            AutoPostBack="true" Width="146px" />
                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtEmployeeName"
                            WatermarkText="Search Employee" WatermarkCssClass="searchBoxText" />
                        <cc1:AutoCompleteExtender EnableCaching="true" ID="AutoCompleteExtenderOrganization"
                            runat="server" MinimumPrefixLength="2" ServiceMethod="GetEmployeeNames" ServicePath="~/PayrollService.asmx"
                            TargetControlID="txtEmployeeName" CompletionSetCount="10" CompletionInterval="250"
                            CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList"
                            CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
                        </cc1:AutoCompleteExtender>
                    </td>
                    <td>
                        <asp:LinkButton ID="btnExport" runat="server" Text="Excel import" OnClientClick="taxImportPopup();return false;"
                            CssClass=" excel marginRight tiptip" Style="float: left;" />
                    </td>
                </tr>
            </table>
        </div>
        <div class="clear" style="width: 1200px; overflow-x: scroll">
            <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" Width="390px" runat="server" />
            <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
            <cc1:TextBoxWatermarkExtender ID="TBWE2" runat="server" TargetControlID="txtEmployeeName"
                WatermarkText="Enter employee name" WatermarkCssClass="watermarked" />
            <cc2:EmptyDisplayGridView CssClass="table table-primary mb30 table-bordered table-hover"
                Style="margin-bottom: 0px" UseAccessibleHeader="true" ShowHeaderWhenEmpty="True"
                ID="gvEmployeeIncome" runat="server" DataKeyNames="EmployeeId,Name"
                AutoGenerateColumns="False" CellPadding="4" GridLines="None" AllowSorting="True"
                ShowFooterWhenEmpty="False">
                <%--            <RowStyle BackColor="#E3EAEB" />--%>
                <Columns>
                    <asp:BoundField HeaderStyle-Width="20px" DataFormatString="{0:000}" DataField="EmployeeId"
                        HeaderText="EIN"></asp:BoundField>
                    <asp:BoundField HeaderStyle-Width="40px" DataFormatString="{0:000}" DataField="IdCardNo"
                        HeaderText="I No"></asp:BoundField>
                    <asp:TemplateField HeaderText="Name" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="120px">
                        <ItemTemplate>
                            <asp:HyperLink CssClass="employeeClass" Target="_blank" Width="150px" ID="Label2"
                                runat="server" NavigateUrl='<%# "~/newhr/EmployeePayroll.aspx?Id=" +  Eval("EmployeeId") %>'
                                Text='<%# "&nbsp;" + Eval("Name") %>'></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                   
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Right" HeaderText="Status" HeaderStyle-Width="80px">
                        <ItemTemplate>
                            <asp:Label Width="80px" runat="server" ID="lbl" Text='<%# (Eval("StatusName")) %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="PF Deduction"
                        HeaderStyle-Width="100px">
                        <ItemTemplate>
                            <asp:DropDownList  onchange='citTypeChange(this);'  ID="ddlType" 
                                SelectedValue='<%#  Eval("IsPFDeductionFixed")%>' runat="server" Width="120px">
                                <asp:ListItem Text="Default PF" Value="False" />
                                <asp:ListItem Text="Fixed Amount" Value="True" />
                            </asp:DropDownList>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Right" HeaderText="PF Contribution" HeaderStyle-Width="80px">
                        <ItemTemplate>
                            <asp:Label Width="80px" style='text-align:right' runat="server" ID="lbl2" Text='<%# GetCurrency(Eval("PFContribution")) %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="PF Deduction" HeaderStyle-Width="80px">
                        <ItemTemplate>
                            <asp:TextBox Width="80px" CssClass='calculationInput' data-col='5' data-row='<%# Container.DataItemIndex %>'
                                Style='text-align: right' 
                                ID="txtAmount" Text='<%#  GetCITCurrency( Eval("FixedPFDeductionAmount"))%>' runat="server"></asp:TextBox>
                            <asp:CompareValidator SetFocusOnError="true" ValueToCompare="0" Display="None" Operator="GreaterThanEqual"
                                Type="Currency" ID="valOpeningBalance2511" ControlToValidate="txtAmount" ValidationGroup="Balance"
                                runat="server" ErrorMessage="Invalid amount, must be greater than or equal to zero."></asp:CompareValidator>
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                </Columns>
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                <EmptyDataTemplate>
                    <b>No employee list. </b>
                </EmptyDataTemplate>
            </cc2:EmptyDisplayGridView>
            <uc1:PagingCtl ID="pagingCtl" OnNextRecord="btnNext_Click" OnPrevRecord="btnPrevious_Click"
                OnDropDownShowRecordsChanged="ddlRecords_SelectedIndexChanged" runat="server" />
        </div>
        <div class="buttonsDiv">
            <%--    <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Balance" DisplayMode="List"
            ShowSummary="false" ShowMessageBox="true" runat="server"></asp:ValidationSummary>--%>
            <asp:Button ID="btnUpdate" CssClass="update" OnClientClick="valGroup='Balance';return CheckValidation();"
                Visible="true" ValidationGroup="Balance" runat="server" Text="Update" OnClick="btnUpdate_Click" />
        </div>
    </div>
</asp:Content>
