<%@ Page Title="Employee Income List" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="AllEmployeeIncomeList.aspx.cs" Inherits="Web.User.AllEmployeeIncomeList" %>

<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="Paging" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningMsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="InfoMsgCtl" TagPrefix="uc2" %>
<%@ Register Src="../Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function changePasswordCall(userName) {
            var ret = changePassword("isPopup=true&u=" + userName);
            if (typeof (ret) != 'undefined') {
            }
        }
        function refreshWindow() {
            window.location.reload();
        }
        /*Incomes */
        function popupIncomeCall() {
            if (typeof (EmployeeId) != 'undefined') {
                var ret = popupIncome("EId=" + parseInt(EmployeeId));
                if (typeof (ret) != 'undefined') {
                    if (ret == 'ReloadIncome') {
                        refreshIncomeList(0);
                    }
                }
            }
            return false;
        }


        function popupUpdateIncomeCall(empIncomeId) {
            var ret = popupUpdateIncome('Id=' + empIncomeId);
            if (typeof (ret) != 'undefined') {
                if (ret == 'ReloadIncome') {
                    refreshIncomeList(0);
                }
            }
            return false;
        }

        function parentIncomeCallbackFunction(text, closingWindow, EmployeeId) {
            closingWindow.close();
            refreshIncomeList(0, EmployeeId);
        }

        //call to delete income
        function deleteIncomeCall(empIncomeId, employeeId) {
            if (confirm("Do you want to delete the income for the employee?")) {
                refreshIncomeList(empIncomeId, employeeId);
            }
            return false;
        }

        var incomeDiv = null;
        function refreshIncomeList(empIncomeId, EmployeeId) {
            if (typeof (EmployeeId) != 'undefined') {
                showLoading();
                incomeDiv = document.getElementById('income' + EmployeeId);
                Web.PayrollService.GetIncomeList(empIncomeId, parseInt(EmployeeId), refreshIncomeListCallback);
            }
        }

        function refreshIncomeListCallback(result) {
            if (result) {
                incomeDiv.innerHTML
                = result;
            }
            hideLoading();
        }

        /*Deduction*/

        function popupDeductionCall() {

            if (typeof (EmployeeId) != 'undefined') {
                var ret = popupDeduction("EId=" + parseInt(EmployeeId));
                if (typeof (ret) != 'undefined') {
                    if (ret == 'ReloadDeduction') {
                        refreshDeductionList(0);
                    }
                }
            }
            return false;
        }

        function reloadDeduction(closingWindow, EmployeeId) {
            closingWindow.close();
            refreshDeductionList(0, EmployeeId);
        }

        function popupUpdateDeductionCall(deductionId) {

            var ret = popupUpdateDeduction('Id=' + deductionId);
            if (typeof (ret) != 'undefined') {
                if (ret == 'Reload') {
                    refreshDeductionList(0);
                }
            }
            return false;
        }
        //call to delete income
        function deleteDeductionCall(deductionId, employeeId) {
            if (confirm("Do you want to delete the deduction for the employee?")) {
                refreshDeductionList(deductionId, employeeId);
            }
            return false;
        }
        var deductionDiv = null;
        function refreshDeductionList(deductionId, EmployeeId) {
            if (typeof (EmployeeId) != 'undefined') {
                showLoading();
                deductionDiv = document.getElementById('deduction' + EmployeeId);
                Web.PayrollService.GetDeductionList(deductionId, parseInt(EmployeeId), refreshDeductionListCallback);
            }
        }

        function refreshDeductionListCallback(result) {
            if (result) {
                deductionDiv.innerHTML
                = result;
            }
            hideLoading();
        }
    </script>
    <style type="text/css">
        .income th
        {
            background: #DDEBF7;
        }
        .deduction th
        {
            background: #FCE4D6;
        }
        .bodypart
        {
            width: 1300px !important;
        }
        .editDeleteTH
        {
            width: 80px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Income / Deduction List
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <strong>Search Employee : </strong>
        <asp:TextBox AutoPostBack="true" Width="160px" ID="txtEmpSearchText" runat="server"
            OnTextChanged="txtEmpSearchText_TextChanged"></asp:TextBox>
        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtEmpSearchText"
            WatermarkText="Employee Name" WatermarkCssClass="searchBoxText" />
        <cc1:AutoCompleteExtender EnableCaching="true" ID="AutoCompleteExtenderOrganization"
            runat="server" MinimumPrefixLength="2" ServiceMethod="GetEmployeeNames" ServicePath="~/PayrollService.asmx"
            TargetControlID="txtEmpSearchText" CompletionSetCount="10" CompletionInterval="250"
            CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList"
            CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
        </cc1:AutoCompleteExtender>
        <div style='margin-top: 10px'>
            <asp:DataList RepeatColumns="1" ID="list" runat="server" DataKeyField="EmployeeId">
                <HeaderTemplate>
                    <table>
                        <tr>
                            <th>
                                EIN
                            </th>
                            <th style="width: 130px">
                                Name
                            </th>
                            <th>
                                Income
                            </th>
                            <th>
                                Deduction
                            </th>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td valign="top" style="padding-top: 4px">
                            <%# Eval("EmployeeId") %>
                        </td>
                        <td valign="top" style="padding-top: 4px">
                            <%# Eval("Name") %>
                        </td>
                        <td valign="top">
                            <div class="income" id='<%# "income" + Eval("EmployeeId") %>'>
                                <%# GetIncomeHTML((int)Eval("EmployeeId")) %>
                            </div>
                        </td>
                        <td valign="top">
                            <div class="deduction" id='<%# "deduction" + Eval("EmployeeId") %>'>
                                <%# GetDeductionHTML((int)Eval("EmployeeId"))%>
                            </div>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:DataList>
            <uc1:PagingCtl ID="pagintCtl" ShowDropDown="true" OnNextRecord="btnNext_Click" OnChangePage="ChangePageNumber"
                OnPrevRecord="btnPrevious_Click" OnDropDownShowRecordsChanged="ddlRecords_SelectedIndexChanged"
                runat="server" />
        </div>
        <uc2:InfoMsgCtl ID="msgInfo" EnableViewState="false" Hide="true" runat="server" />
        <uc2:WarningMsgCtl ID="msgWarning" EnableViewState="false" Hide="true" runat="server" />
    </div>
</asp:Content>
