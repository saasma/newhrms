﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using System.Xml;

namespace Web.Appraisal
{
    public partial class TravelRequestAdminView : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        public void Initialise()
        {
            CommonManager comManager = new CommonManager();
            cmbCountry.Store[0].DataSource = comManager.GetAllCountries();
            cmbCountry.Store[0].DataBind();

            cmbTravelBy.Store[0].DataSource = TravelAllowanceManager.getTravelByList();
            cmbTravelBy.Store[0].DataBind();

            cmbExpensePaidBy.Store[0].DataSource = TravelAllowanceManager.getExpensePaidByList();
            cmbExpensePaidBy.Store[0].DataBind();

            LoadLevels();
        }
        
        private void LoadLevels()
        {
            //HardCoded Date Just incase we need date in future. Just pass the desired date to the function and Date filter works fine.
            //GridLevels.GetStore().DataSource = TravelAllowanceManager.GetAllTravelRequestForAdmin(new DateTime(DateTime.Now.Year - 3, 1, 1), DateTime.Now);
            //GridLevels.GetStore().DataBind();
        }


        
        public void ClearLevelFields()
        {
            hiddenValue.Text = "";
            //txtName.Text = "";
            //txtDescription.Text = "";
        }

      
        
        protected void btnAddLevel_Click(object sender, DirectEventArgs e)
        {
            
            ClearLevelFields();
            //WindowLevel.Show();
        }


        protected void btnDeleteLevel_Click(object sender, DirectEventArgs e)
        {
            int TAllowance = int.Parse(hiddenValue.Text.Trim());
            Status status = TravelAllowanceManager.DeleteTravelAllowance(TAllowance);
            if (status.IsSuccess)
            {
                LoadLevels();
                NewMessage.ShowNormalMessage("Travel Allowance deleted.");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        

        protected void btnEditLevel_Click(object sender, DirectEventArgs e)
        {

            TARequest request = new TARequest();
            request = TravelAllowanceManager.getRequestByID(int.Parse(hiddenValue.Text));
            LoadEmployeeInfo(request.EmployeeId.Value);
            LoadAllowanceGrid(request.CountryId.Value, request.RequestID);
            LoadEditData(request);
            Window1.Show();

        }

        protected void LoadAllowanceGrid(int countryID, int? RequestID)
        {
            List<TARequestLine> lines = new List<TARequestLine>();
            lines = TravelAllowanceManager.getAllowanceByEmployeeID(SessionManager.CurrentLoggedInEmployeeId, RequestID, countryID);
            storeAllowances.DataSource = lines;
            storeAllowances.DataBind();

        }


        private void LoadEditData(TARequest loadRequest)
        {
            

            txtPlaceToTravel.Text = loadRequest.PlaceOfTravel;
            //cmbCountry.SelectedItem.Value = loadRequest.CountryId.Value.ToString();
            //ExtControlHelper.ComboBoxSetSelected(loadRequest.CountryId.Value.ToString(), cmbCountry);
            cmbCountry.SetValue(loadRequest.CountryId.ToString());

            txtPurposeOfTravel.Text = loadRequest.PurposeOfTravel;
            txtFromDate.Text = loadRequest.TravellingFromEng.Value.ToShortDateString();
            txtToDate.Text = loadRequest.TravellingToEng.Value.ToShortDateString();

            //cmbTravelBy.SelectedItem.Value = loadRequest.TravelBy.Value.ToString();
            //ExtControlHelper.ComboBoxSetSelected(loadRequest.TravelBy.Value.ToString(), cmbTravelBy);
            cmbTravelBy.SetValue(loadRequest.TravelBy.Value.ToString());
            //cmbExpensePaidBy.SelectedItem.Value = loadRequest.ExpensePaidBy.Value.ToString();
            //ExtControlHelper.ComboBoxSetSelected(loadRequest.ExpensePaidBy.Value.ToString(), cmbExpensePaidBy);
            cmbExpensePaidBy.SetValue(loadRequest.ExpensePaidBy.Value.ToString());

            txtDayCount.Text = (Math.Abs((loadRequest.TravellingFromEng.Value - loadRequest.TravellingToEng.Value).TotalDays) + 1).ToString();
            txtNightCount.Text = (Math.Abs((loadRequest.TravellingFromEng.Value - loadRequest.TravellingToEng.Value).TotalDays)).ToString();



        }

        protected void LoadEmployeeInfo(int EmployeeID)
        {
            EEmployee eemployee = EmployeeManager.GetEmployeeById(EmployeeID);
            lblName.Text = eemployee.Name;
            lblDepartment.Text = eemployee.Department.Name;
            lblDesignation.Text = eemployee.EDesignation.Name;
            lblBranch.Text = eemployee.Branch.Name;
            List<TARequestStatusHistory> statusHistory = TravelAllowanceManager.getTravelRequestForwardHistory(hiddenValue.Text);
            
            string body = "";
            foreach (var status in statusHistory)
            {
                body = body + "<tr><td  colspan='2' style='font-weight:bold'>" + status.whoseComment + "</td></tr><tr><td colspan='2' style='color:blue'>" + status.ApprovalRemarks + "</td></tr><tr><td>" + status.ApprovedByName + "</td><td>" + status.ApprovedOn.Value.ToShortDateString() + "</td></tr>";
            }
            body = "<table>" + body + "</table>";
            txtCommentSection.Html = body;

        }


        protected void Country_Change(object sender, DirectEventArgs e)
        {
            LoadAllowanceGrid(int.Parse(cmbCountry.SelectedItem.Value));
        }


        protected void CheckInOutSelectChange(object sender, DirectEventArgs e)
        {
            string actualInDate = "";
            string actualOutDate = "";
            if (txtFromDate.Text != "0001-01-01 12:00:00 AM")
                actualInDate = txtFromDate.Text;
            if (txtToDate.Text != "0001-01-01 12:00:00 AM")
                actualOutDate = txtToDate.Text;
            DateTime englishInDate;
            DateTime englishOutDate;
            if (!string.IsNullOrEmpty(actualInDate) && !string.IsNullOrEmpty(actualOutDate))
            {
                englishInDate = DateTime.Parse(actualInDate);
                englishOutDate = DateTime.Parse(actualOutDate);

                txtDayCount.Text = (Math.Abs((englishInDate - englishOutDate).TotalDays) + 1).ToString();
                txtNightCount.Text = (Math.Abs((englishInDate - englishOutDate).TotalDays)).ToString();

                if ((englishInDate - englishOutDate).TotalDays == 0)
                {
                    txtDayCount.Text = "1";
                    txtNightCount.Text = "1";
                }

            }
            else
            {
                txtDayCount.Text = "0";
                txtNightCount.Text = "0";
            }
        }


        protected void LoadAllowanceGrid(int countryID)
        {
            List<TARequestLine> lines = new List<TARequestLine>();
            int? RequestID = null;
            if (!string.IsNullOrEmpty(hiddenValue.Text))
            {
                RequestID = int.Parse(hiddenValue.Text);
            }
            lines = TravelAllowanceManager.getAllowanceByEmployeeID(SessionManager.CurrentLoggedInEmployeeId, RequestID, countryID);
            storeAllowances.DataSource = lines;
            storeAllowances.DataBind();

        }

        protected void btnDetailLevel_Click(object sender, DirectEventArgs e)
        {
            
        }


        protected void btnDetailSaveUpdate_Click(object sender, DirectEventArgs e)
        {
            List<TAAllowanceRate> lines = new List<TAAllowanceRate>();
            string json = e.ExtraParams["Values"];

            if (string.IsNullOrEmpty(json))
            {
                return;
            }
            int TAllowance = int.Parse(hiddenValue.Text.Trim());

            lines = JSON.Deserialize<List<TAAllowanceRate>>(json);

            Status status = TravelAllowanceManager.InsertUpdateAllowanceDetail(lines, TAllowance);
            if (status.IsSuccess)
            {
                Window1.Hide();
                LoadLevels();
                NewMessage.ShowNormalMessage("Rating Scale saved.");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }


        }


        protected void ButtonNext_Click(object sender, DirectEventArgs e)
        {

            TARequest request = new TARequest();
            TARequestStatusHistory hist = new TARequestStatusHistory();
            bool isEdit;
            bool isStatusIncrease=true;

            isEdit = string.IsNullOrEmpty(hiddenValue.Text);
            if (!isEdit)
            {
                request.RequestID = int.Parse(hiddenValue.Text);
                request.Status = TravelAllowanceManager.getRequestByID(int.Parse(hiddenValue.Text)).Status;
            }

            if (!string.IsNullOrEmpty(txtDayCount.Text))
            {
                request.Days = int.Parse(txtDayCount.Text);
            }
            if (!string.IsNullOrEmpty(txtNightCount.Text))
            {
                request.Night = int.Parse(txtNightCount.Text);
            }

            hist.ApprovalRemarks = txtHRComment.Text;
            request.PlaceOfTravel = txtPlaceToTravel.Text;
            request.CountryId = int.Parse(cmbCountry.SelectedItem.Value);
            request.CountryName = cmbCountry.SelectedItem.Text;
            request.PurposeOfTravel = txtPurposeOfTravel.Text;
            request.TravellingFromEng = DateTime.Parse(txtFromDate.Text);
            request.TravellingToEng = DateTime.Parse(txtToDate.Text);
            request.TravelBy = int.Parse(cmbTravelBy.SelectedItem.Value);
            request.TravelByText = cmbTravelBy.SelectedItem.Text;
            request.ExpensePaidBy = int.Parse(cmbExpensePaidBy.SelectedItem.Value);
            request.ExpensePaidByText = cmbExpensePaidBy.SelectedItem.Value;
            request.EmployeeId = SessionManager.CurrentLoggedInEmployeeId;

            List<TARequestLine> lines = new List<TARequestLine>();
            string json = e.ExtraParams["AllowanceValues"];
            if (string.IsNullOrEmpty(json))
            {
                return;
            }


            lines = JSON.Deserialize<List<TARequestLine>>(json);

            Status status = TravelAllowanceManager.InsertUpdateTravelRequests(request, lines, hist, isStatusIncrease);
            if (status.IsSuccess)
            {

                NewMessage.ShowNormalMessage("Allowance Forwarded");
                Window1.Close();
                LoadLevels();
                //Response.Redirect("TravelRequestHRView.aspx");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }


        }

 
    }
}