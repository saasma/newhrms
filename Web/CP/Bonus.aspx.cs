﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using Web.Controls;
using BLL.Manager;
using DAL;

namespace Web.CP
{
    public partial class Bonus : System.Web.UI.Page
    {

        protected void Page_PreRender(object sender, EventArgs e)
        {
            for (int i = 0; i < gvwEligibleEmployees.Rows.Count; i++)
            {
                GridViewRow row = gvwEligibleEmployees.Rows[i];

                TextBox txtAdjustment = row.FindControl("txtActualWorkdays") as TextBox;
                Label lblActualWorkdays = row.FindControl("lblActualWorkdays") as Label;
                Label lblAdjustedWorkdays = row.FindControl("lblAdjustedWorkdays") as Label;
                Label lblEligibility = row.FindControl("lblEligibility") as Label;

                string id = lblActualWorkdays.ClientID + ":" + lblAdjustedWorkdays.ClientID + ":" +
                            lblEligibility.ClientID;

                txtAdjustment.Attributes.Add("idList", id);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);

            if( !IsPostBack)
                Initialise();
        }

        public HeaderCtl HeaderCtl
        {
            get
            {
                return Page.Master.FindControl("HeaderCtl1") as HeaderCtl; 
            }
        }

        //Load handler
        public void filter_Changed()
        {

            gvwEligibleEmployees
                .DataSource = CommonManager.GetEmployeeForEligibility(filter.DepartmentId, filter.BranchId);
            gvwEligibleEmployees.DataBind();
        }

        void Initialise()
        {
           
            //HeaderCtl.DisplayBonus(1);

            //Load payroll period
            PayrollPeriod payrollPeriod = CommonManager.GetValidLastPayrollPeriod();
            if( payrollPeriod == null)
            {
                warning.InnerHtml = Resources.Messages.ValidPayrollPeriodNotDefined;
                warning.Hide = false;

                btnNext.Enabled = false;
            }
            else
            {
                lblPayrollPeriod.Text = payrollPeriod.Name;
            }

            //load incomes
            chkListSalaries.DataSource = PayManager.GetIncomeListByCompany(SessionManager.CurrentCompanyId);
            chkListSalaries.DataBind();
        }

        protected void btnPrevious_Click(object sender, EventArgs e)
        {
            int currentIndex = multiView.ActiveViewIndex;
            btnNext.Enabled = true;

            switch (currentIndex)
            {
                case 0:
                    break;
                case 1:
                    btnPrevious.Enabled = false;
                    break;
                case 2:
                    break;
                case 3:
                    
                   
                    break;
                case 4:
                    break;

            }

            currentIndex -= 1;
            multiView.ActiveViewIndex = currentIndex;

            //HeaderCtl.DisplayBonus(currentIndex + 1);
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            int currentIndex = multiView.ActiveViewIndex;
            btnPrevious.Enabled = true;

            switch (currentIndex)
            {
                case 0:
                    break;
                case 1:
                    break;
                case 2:
                    break;
                case 3:
                    //temporary disable it
                    return;
                    //break;
                case 4:
                    break;
                    
            }

            currentIndex += 1;
            multiView.ActiveViewIndex = currentIndex;

            //HeaderCtl.DisplayBonus(currentIndex + 1);

        }
    }
}
