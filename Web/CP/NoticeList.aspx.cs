﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using BLL;
using DAL;
using Ext.Net;
using Utils.Web;
using Utils.Helper;
using Utils;
using System.IO;
using Web.ReportDataSetTableAdapters;
using Web.CP.Report.Templates.Pay.Detail;
using BLL.BO;
using Newtonsoft.Json.Linq;

namespace Web.CP
{
    public partial class NoticeList : BasePage
    {
        string userName = string.Empty;

        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.HumanResource;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
             userName = SessionManager.UserName;
            if (!X.IsAjaxRequest)
            {
                Initialize();
            }
        }

        

        

        void Initialize()
        {
            LoadGrid(1);
        }


        public void LoadGrid(int status)
        {
            //StatusStr
            List<NoticeBoard> noticeList = new List<NoticeBoard>();
            noticeList = NoticeManager.getAllNotice((int)NoticeBoardStatusEnum.Published);
            storeNoticeList.DataSource = noticeList;
            storeNoticeList.DataBind();

            List<NoticeBoard> unpublishedList = new List<NoticeBoard>();
            unpublishedList = NoticeManager.getAllNotice((int)NoticeBoardStatusEnum.Unpublished);
            storeUnpublished.DataSource = unpublishedList;
            storeUnpublished.DataBind();

            List<NoticeBoard> draftList = new List<NoticeBoard>();
            draftList = NoticeManager.getAllNotice((int)NoticeBoardStatusEnum.Draft);
            storeDraft.DataSource = draftList;
            storeDraft.DataBind();
        }



        protected void btnChangeStatus_Click(object s, DirectEventArgs e)
        {
            if (!string.IsNullOrEmpty(HiddenRowID.Text))
            {
                int NoticeID = int.Parse(HiddenRowID.Text);
                Status status = new Status();
                status = NoticeManager.ChangeNoticeStatus(NoticeID);
                if (status.IsSuccess)
                {
                    X.Msg.Alert("", "Status of Notice Changed").Show();
                    LoadGrid(1);
                }
                else
                {
                    X.Msg.Alert("", status.ErrorMessage).Show();
                }
            }
        }

        protected void btnEdit_Click(object s, DirectEventArgs e)
        {
            if (!string.IsNullOrEmpty(HiddenRowID.Text))
            {
                int NoticeID = int.Parse(HiddenRowID.Text);
                Response.Redirect("NoticeCompose.aspx?id=" + NoticeID.ToString());
            }
        }

        protected void btnStatus_Click(object s, DirectEventArgs e)
        {
            Ext.Net.Button button = (Ext.Net.Button)s;
            if (button.ID == "btnPublished")
            {
                LoadGrid((int)NoticeBoardStatusEnum.Published);
            }
            if (button.ID == "btnDraft")
            {
                LoadGrid((int)NoticeBoardStatusEnum.Draft);
            }
            else
            {
                LoadGrid((int)NoticeBoardStatusEnum.Unpublished);
            }

        }

        protected void btnDraft_Click(object sender, DirectEventArgs e)
        {
 
        }
        

        public static int ConvertBytesIntoKB(int byteValue)
        {
            double mb = (byteValue / 1024);
            if (mb.ToString("#") == "")
                return 0;
            return Convert.ToInt32(mb.ToString("#"));
        }
        public static string ConvertKBIntoMB(object value)
        {
            long kb = Convert.ToInt64(value);
            double mb = (kb / 1024.0);
            return mb.ToString("#.#");
        }

        public string GetSize(int byteValue)
        {
            if (byteValue < 1024)
                return "1 kb";

            double kb = ConvertBytesIntoKB(byteValue);
            if (kb > 1024)
            {
                return ConvertKBIntoMB(kb) + " mb";
            }
            else
            {
                return kb + " kb";
            }
        }

        protected void btnDelete_Click(object sender, DirectEventArgs e)
        {
            int noticeId = int.Parse(HiddenRowID.Text);

            Status status = NoticeManager.DeleteNotice(noticeId);
            if (status.IsSuccess)
            {
                X.Msg.Alert("", "Notice deleted successfully.").Show();
                LoadGrid(1);
            }
            else
            {
                X.Msg.Alert("", status.ErrorMessage).Show();
            }
        }

    }
}
