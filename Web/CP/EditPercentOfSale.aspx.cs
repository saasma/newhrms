﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BLL;
using BLL.Manager;
using Utils.Web;
using System.Collections.Generic;
using DAL;
using Utils.Helper;

namespace Web.CP
{
    public partial class EditPercentOfSale : System.Web.UI.Page
    {
        private string _sortBy = "Name Asc";
        private int _tempCurrentPage;
        private int _tempCount;
        private PayManager payMgr = new PayManager();
        protected void Page_Load(object sender, EventArgs e)
        {
            //RegisterEmployeeSavedMsg();
            if (!IsPostBack)
            {
                if (SessionManager.EmployeeDisplayRecodsPerPage != 0)
                {
                    UIHelper.SetSelectedInDropDown(pagingCtl.DDLRecords, SessionManager.EmployeeDisplayRecodsPerPage.ToString());
                }
                _tempCurrentPage = 1;
                BindEmployees();

            }
        }
        //void RegisterEmployeeSavedMsg()
        //{
        //    if (SessionManager.EmployeeSaved)
        //    {
        //        JavascriptHelper.DisplayClientMsgWithTimeout("Employee Information Updated.", Page);
        //        SessionManager.EmployeeSaved = false;
        //    }
        //}

        void BindEmployees()
        {
            gvEmployeeIncome.DataSource
                = payMgr.GetPercentOfSaleIncome(_tempCurrentPage,int.Parse(pagingCtl.DDLRecords.SelectedValue), ref _tempCount, 
                SessionManager.CurrentCompanyId,_sortBy, txtEmployeeName.Text.Trim());
            gvEmployeeIncome.DataBind();
            
            if (_tempCount <= 0)
            {
                pagingCtl.Visible = false;
            }
            else
            {
                pagingCtl.Visible = true;
            }
            int totalPages = (int)CalculateTotalPages(_tempCount);
            pagingCtl.LabelTotalPage.Text = (totalPages).ToString();
            pagingCtl.LabelCurrentPage.Text = _tempCurrentPage.ToString();

            if (_tempCurrentPage <= 1)
            {
                pagingCtl.ButtonPrev.Enabled = false;
            }
            else
            {
                pagingCtl.ButtonPrev.Enabled = true;
            }

            if (_tempCurrentPage < totalPages)
                pagingCtl.ButtonNext.Enabled = true;
            else
                pagingCtl.ButtonNext.Enabled = false;

        }
        public string GetName(DAL.EEmployee obEmp)
        {
            return obEmp.Name;
        }
        private int CalculateTotalPages(double totalRows)
        {
            int totalPages = (int)Math.Ceiling(totalRows / int.Parse(pagingCtl.DDLRecords.SelectedValue));

            return totalPages;
        }
        protected void ddlRecords_SelectedIndexChanged()
        {
            SessionManager.EmployeeDisplayRecodsPerPage = int.Parse(pagingCtl.DDLRecords.SelectedValue);
            _tempCurrentPage = 1;
            BindEmployees();
        }
        protected void btnNext_Click()
        {
            _tempCurrentPage = int.Parse(pagingCtl.LabelCurrentPage.Text);
            _tempCurrentPage += 1;
            BindEmployees();
        }
        protected void btnPrevious_Click()
        {
            _tempCurrentPage = int.Parse(pagingCtl.LabelCurrentPage.Text);
            _tempCurrentPage -= 1;
            BindEmployees();
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            List<PEmployeeIncome> lstPEIncome = new List<PEmployeeIncome>();
            PEmployeeIncome obPEmpInc;
            foreach(GridViewRow gRow in gvEmployeeIncome.Rows)
            {
                //gRow = gvEmployeeIncome.Rows[gIndex];
                obPEmpInc = new PEmployeeIncome();
                obPEmpInc.EmployeeId = (int)gvEmployeeIncome.DataKeys[gRow.RowIndex][0];
                obPEmpInc.EmployeeIncomeId = (int)gvEmployeeIncome.DataKeys[gRow.RowIndex][1];
                obPEmpInc.Amount = decimal.Parse(((TextBox)((GridViewRow)
                    gvEmployeeIncome.Rows[gRow.RowIndex]).FindControl("txtAmount")).Text.Trim());
                lstPEIncome.Add(obPEmpInc);
            }
            if (payMgr.UpdateEmployeeIncome(lstPEIncome))
            {
                JavascriptHelper.DisplayClientMsgWithTimeout("Employee information updated.", Page);
                BindEmployees();
            }
            else
                JavascriptHelper.DisplayClientMsgWithTimeout("Employee information update failed.", Page);
            
        }

        protected void txtEmployeeName_TextChanged(object sender, EventArgs e)
        {
            _tempCurrentPage = 1;
            BindEmployees();

        }


        //protected void gvEmployeeIncome_RowEditing(object sender, GridViewEditEventArgs e)
        //{
        //    gvEmployeeIncome.EditIndex = e.NewEditIndex;
        //    BindEmployees();
        //}

        //protected void gvEmployeeIncome_RowUpdating(object sender, GridViewUpdateEventArgs e)
        //{
        //    PEmployeeIncome obPEmpInc = new PEmployeeIncome();
        //    obPEmpInc.EmployeeId = (int)gvEmployeeIncome.DataKeys[e.RowIndex][0];
        //    obPEmpInc.EmployeeIncomeId = (int)gvEmployeeIncome.DataKeys[e.RowIndex][1];
        //    GridViewRow row = gvEmployeeIncome.Rows[e.RowIndex];
        //    obPEmpInc.Amount = decimal.Parse(((TextBox)((GridViewRow)gvEmployeeIncome.Rows[e.RowIndex]).FindControl("txtAmount")).Text.Trim());
            
        //    payMgr.UpdateEmployeeIncome(obPEmpInc);
        //    gvEmployeeIncome.EditIndex = -1;
        //    BindEmployees();
        //}

        //protected void gvEmployeeIncome_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        //{

        //    gvEmployeeIncome.EditIndex = -1;
        //    BindEmployees();

        //}



    }
}
