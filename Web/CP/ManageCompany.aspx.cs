﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using BLL;
using BLL.Base;
using BLL.Manager;
using DAL;

namespace Web.CP
{
    public partial class ManageCompany : BasePage
    {
        CompanyManager compMgr = new CompanyManager();

        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.HumanResource;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindCompanies();
            }

            if (gvwCompanies.HeaderRow != null)
                gvwCompanies.HeaderRow.TableSection = TableRowSection.TableHeader;

        }


        void BindCompanies()
        {
            gvwCompanies.DataSource = compMgr.GetAllCompanies();
            gvwCompanies.DataBind();
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
                       

            //foreach (GridViewRow row in gvwCompanies.Rows)
            //{                
            //    int cid =(int) gvwCompanies.DataKeys[row.RowIndex][0];

            //    //row.Attributes.Add("onclick", string.Format("redirectToOverview({0})", cid));
            //    //skip for edit column
            //    for (int i = 0; i < row.Cells.Count - 1; i++)
            //    {
            //        TableCell cell = row.Cells[i];
            //        cell.Attributes.Add("onclick", string.Format("redirectToOverview({0})", cid));
            //    }


                
            //}
        }

        protected void gvwCompanies_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover",
                "this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#ebebeb'");

                e.Row.Attributes.Add("onmouseout",
                "this.style.backgroundColor=this.originalstyle;");
            }
        }

        protected void gvwCompanies_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int cid = (int)gvwCompanies.DataKeys[e.RowIndex][0];
            SessionManager.CompanyDeleteId = cid;
            Response.Redirect("~/CP/CompanyDelConf.aspx");
        }

        public bool IsDefault(object val)
        {
            if (val == null)
                return false;
            return Convert.ToBoolean(val);
        }
    }
}
