﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using Utils.Base;
using BLL;
using DAL;
using Utils.Web;
using Utils.Helper;
using System.Text.RegularExpressions;

namespace Web.CP
{
    public partial class CurrentPayIncomeSelection : BasePage
    {
        /// <summary>
        /// 0-> Data Insert Mode
        /// 1-> Data Update Mode
        /// </summary>
        public string SaveMode
        {
            get { return this.hdSaveMode.Value; }
            set { this.hdSaveMode.Value = value; }
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                
                Initialize();
            }
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            List<CurrentPayIncome> incomes = new List<CurrentPayIncome>();
            foreach (ListItem item in chkListSalaries.Items)
            {
                if (item.Selected)
                {
                    incomes.Add(new CurrentPayIncome { IncomeId = int.Parse(item.Value) });
                }
            }


            List<CurrentPayIncome> list = BLL.BaseBiz.PayrollDataContext.CurrentPayIncomes.ToList();
            BLL.BaseBiz.PayrollDataContext.CurrentPayIncomes.DeleteAllOnSubmit(list);
            BLL.BaseBiz.PayrollDataContext.CurrentPayIncomes.InsertAllOnSubmit(incomes);
            BLL.BaseBiz.PayrollDataContext.SubmitChanges();

            JavascriptHelper.DisplayClientMsg("Income updated, please reload the page for new income list.", Page, "closePopup();");

        }



        


        void Initialize()
        {

            chkListSalaries.DataSource =
                PayManager.GetFixedAndPercentIncomeListByCompany(SessionManager.CurrentCompanyId);
            chkListSalaries.DataBind();


            List<CurrentPayIncome> list = BLL.BaseBiz.PayrollDataContext.CurrentPayIncomes.ToList();

            foreach (var item in list)
            {
                ListItem listItem = chkListSalaries.Items.FindByValue(item.IncomeId.ToString());
                if (listItem != null)
                    listItem.Selected = true;
            }
        }

       
      
    }
}
