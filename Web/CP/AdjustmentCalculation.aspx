<%@ Page Title="Adjustment calculation" Language="C#" MasterPageFile="~/Master/ForPopupPage.Master"
    AutoEventWireup="true" CodeBehind="AdjustmentCalculation.aspx.cs" Inherits="Web.CP.AdjustmentCalculation" %>

<%@ Register Src="../Controls/InfoMsgCtl.ascx" TagName="InfoMsgCtl" TagPrefix="uc1" %>
<%@ Register Src="../Controls/WarningMsgCtl.ascx" TagName="WarningMsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Scripts/jquery.caret.1.02.min.js" type="text/javascript"></script>
    <script type="text/javascript">



        function ReturnValues() {

            var obj = new Array();
            obj["Advance"] = $(txtAmountId).val();
            obj["TakenOn"] = getCalendarSelectedDate(takenOn);




            // alert(window.opener.parentReloadCallbackFunction)
            if ($.browser.msie == false && typeof (window.opener.loadCalculation) != 'undefined' && ($.browser.chrome || $.browser.safari)) {
                window.opener.loadCalculation(obj, window);
            } else {
                window.returnValue = obj;
                window.close();
            }

        }
        function okClick() {

            valGroup = 'Calculation';
            if (CheckValidation() == false)
                return false;
            //            if( calculated==false)
            {
                ReturnValues();
                return false;
            }
        }



        var txtResult = null;
        var txtNote = null;
        $(document).ready
        (
            function () {
                txtNote = '<%=txtNote.ClientID %>';
                txtResult = '<%=txtResult.ClientID %>';

                calculateResult();
            }
        );

        function insert(strValue) {



            var start = $('#ctl00_mainContent_txtExpression').caret().start;

            var value = $('#ctl00_mainContent_txtExpression').val();

            var before = value.substring(0, start);
            var after = value.substring(start, value.length);
            $('#ctl00_mainContent_txtExpression').focus();
            $('#ctl00_mainContent_txtExpression').val(before + strValue + after);
            // $('#ctl00_mainContent_txtExpression').caretTo((before + strValue).length);

        }

        function calculateResult() {
            try {
                var result = $('#ctl00_mainContent_txtExpression').val();
                if (result != "") {
                    if (result.length > 0 && result.charAt(0) == '=')
                        result = result.substring(1, result.length);
                    var result = parseFloat(eval(result)).toFixed(2);


                    $('#' + txtResult).val(result.toString());
                }
            }
            catch (ee) {
                $('#' + txtResult).val("");
            }
        }

        function setValue() {
            opener.expressionCallback(
                $('#ctl00_mainContent_txtExpression').val(), $('#' + txtResult).val()
                , $('#' + txtNote).val());
            window.close();
        }
        

    </script>
    <style type="text/css">
        .style1
        {
            height: 20px;
        }
        table tr td
        {
            padding: 0 10px 8px 0;
        }
        div.notify
        {
            width: 510px;
        }
        .expression a
        {
            width: 32px !important;
            background-color: lightgray;
            display: inline-block;
            height: 18px;
            vertical-align: middle !important;
            padding-top: 4px;
            font-size: 14px;
            text-align: center;
            cursor: pointer;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <div class="popupHeader" style="padding-left: 18px; padding-top: 5px">
        <h1 style="color: White" runat="server" id="title">
            Manual Salary Review :
        </h1>
    </div>
    <div style='width: 400!important; margin-left: 10px'>
        <uc2:WarningMsgCtl ID="warningMsg" Hide="true" runat="server" />
        <uc1:InfoMsgCtl ID="infoMsg" runat="server" Hide="true" />
    </div>
    <div class="marginal">
        <div class="bevel" style="width: 700px">
            <table>
                <tr>
                    <td>
                        <div class="fields paddpop" style="width: 350px!important">
                            <table cellpadding="0" cellspacing="0" style="display: none">
                                <tr>
                                    <td class="lf">
                                        Starting date
                                    </td>
                                    <td>
                                        <My:Calendar Id="calStartDate" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="lf">
                                        Current Pay Period
                                    </td>
                                    <td>
                                        <My:Calendar Id="calPayrollEndDate" runat="server" />
                                    </td>
                                </tr>
                            </table>
                            <div style="padding-top: 5px; padding-bottom: 5px">
                                <b>Basic Salary Change History</b>
                            </div>
                            <asp:GridView ID="gvwBasicSalary" PagerStyle-HorizontalAlign="Center" PagerStyle-CssClass="defaultPagingBar"
                                AllowPaging="true" runat="server" AutoGenerateColumns="False" CellPadding="4"
                                GridLines="None" PageSize="10">
                                <Columns>
                                    <asp:BoundField DataField="FromDate" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left"
                                        HeaderText="From" />
                                    <asp:TemplateField HeaderStyle-Width="100px" HeaderText="Amount/Rate" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <a id="A1" runat="server" style="cursor: pointer" onclick='<%# "insert(" + Eval("Amount","{0:00}")  + ");"%>'>
                                                <%# GetAmountOrRate(Eval("Amount"), Eval("Rate"))%>
                                            </a>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-Width="100px" HeaderText="Difference" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <a id="A1" runat="server" style="cursor: pointer" onclick='<%# "insert(" + Eval("Difference","{0:00}")  + ");"%>'>
                                                <%# GetDifference(Eval("Difference"), Eval("Amount"))%></a>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle CssClass="odd" />
                                <AlternatingRowStyle CssClass="even" />
                                <SelectedRowStyle CssClass="selected" />
                                <PagerStyle HorizontalAlign="Center" CssClass="defaultPagingBar"></PagerStyle>
                                <EmptyDataTemplate>
                                    <b>No change history. </b>
                                </EmptyDataTemplate>
                            </asp:GridView>
                            <div style="padding-top: 10px">
                                <b>Equation Editor</b>
                            </div>
                            <div class="expression" style="padding-top: 10px">
                                <a onclick="insert('+')">+</a> <a onclick="insert('-')">-</a> <a onclick="insert('*')">
                                    *</a> <a onclick="insert('/')">/</a> <a onclick="insert('(')">(</a> <a onclick="insert(')')">
                                        )</a> <a onclick="insert('30')">30</a> <a onclick="insert('174')">174</a>
                            </div>
                            <div style="padding-top: 10px;">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtExpression" Width="320px" runat="server" TextMode="MultiLine"
                                                Height="70">                    
                                            </asp:TextBox>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div style="padding-top: 10px; padding-bottom: 10px;">
                                <table>
                                    <tr>
                                        <td style="padding-left: 107px">
                                            <input type="button" onclick="calculateResult();" class="save" value="Calculate" />
                                        </td>
                                        <td>
                                            =
                                            <asp:TextBox ID="txtResult" Style="font-size: 14px" runat="server" Width="100px"
                                                ReadOnly="true" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div style="padding-top: 10px">
                                <b>Note</b>
                            </div>
                            <asp:TextBox ID="txtNote" Width="320px" runat="server" TextMode="MultiLine" Height="50">                    
                            </asp:TextBox>
                            <br />
                            <br />
                        </div>
                    </td>
                    <td>
                        <div>
                            <asp:Image ID="img" runat="server" />
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div class="clear">
            <asp:Button ID="btnOk" CssClass="update" runat="server" Text="Save" OnClientClick="setValue(); ;return false;" />
            &nbsp;
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="cancel" OnClientClick="window.close();return false;" />
        </div>
    </div>
</asp:Content>
