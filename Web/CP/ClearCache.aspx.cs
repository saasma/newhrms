﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;

namespace Web.CP
{
    public partial class ClearCache : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
            
            }
        }

       

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            // for janata revert in appraisal for two days
            //if (CommonManager.CompanySetting.WhichCompany == WhichCompany.Janata
            //    && DateTime.Now.Date.Month==7 
            //    && (DateTime.Now.Date.Day == 28 || DateTime.Now.Date.Day==29))
            //{
            //    int[] einList ={431,419,307,107,343,326,367,123,258,447,446,116,463,311,397,318,317,124
            //        ,135,436,224,298,140,47,429,371,345,359,432,474,336,479,262,490,443,477,283,313
            //        ,131,216,129,132,162,265,234,458,177,417,503,94,195,143
            //    };

            //    foreach (var item in einList)
            //    {
            //        AppraisalEmployeeForm form = BLL.BaseBiz.PayrollDataContext.AppraisalEmployeeForms
            //            .FirstOrDefault(x=>x.EmployeeId == item);
            //        if (form != null)
            //        {
            //            AppraisalManager.ReverseAppraisalStatus(form.AppraisalEmployeeFormID, 2, "admin reverted");
            //        }
            //    }
            //    NewMessage.ShowNormalMessage("Appraisal reverted.");
            //}
            //else
            {
                MyCache.Reset();



                NewMessage.ShowNormalMessage("Cache has been cleared.");
            }
        }
    }
}