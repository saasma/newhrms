<%@ Page MaintainScrollPositionOnPostback="true" Title="Payroll Message" Language="C#"
    AutoEventWireup="true" CodeBehind="PayrollMessage.aspx.cs" Inherits="Web.CP.PayrollMessage" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Employee/UserControls/PayrollMessageInbox.ascx" TagName="Inbox"
    TagPrefix="uc1" %>
<%@ Register Src="~/Controls/FooterCtl.ascx" TagName="FooterCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/HeaderCtl.ascx" TagName="HeaderCtl" TagPrefix="uc3" %>
<html>
<head runat="server">
    <title>Message</title>

   <link id="Link5" runat="server" rel="stylesheet" href="~/payrollmenu_files/mbcsmbmcp.css" type="text/css" />
  
    <link id="Link1" runat="server" rel="stylesheet" type="text/css" href="~/css/core.css" />
    <link id="Link2" runat="server" rel="Stylesheet" type="text/css" href="~/Styles/calendar/calendar.css" />
    <link id="Link3" runat="server" rel="stylesheet" href="~/mega_menu/payroll_menu.css" />
     <link id="Link4" runat="server" rel="stylesheet" href="~/Styles/design.css" />
     
    
    <style type="text/css">
        .details
        {
            padding-top: 10px;
        }
        .details tr td
        {
            padding-top: 4px;
        }
        
        a.x-datepicker-eventdate
        {
            border: 1px solid #a3bad9;
            padding: 1px; /* background-color: Cyan;*/
        }
        
        .icon-delete
        {
            background-image: url("../images/delet.png") !important;
        }
    </style>
    <%--<script type="text/javascript" src="common.js"></script>--%>
    <%--<script type="text/javascript" src="override.js"></script>--%>
</head>
<body>
    <form id="Form1" runat="server">
    <ext:ResourceManager ID="ResourceManager1"  DisableViewState="false" ShowWarningOnAjaxFailure="false"  runat="server" ScriptMode="Release" Namespace="CompanyX" />
    <uc3:HeaderCtl ID="HeaderCtl1" runat="server" />
    <div id="bodypart">
        <div class="bodypart">
            <ext:Hidden ID="hdUserName" runat="server" />
            <ext:Hidden ID="hdBody" runat="server" />
            <uc1:Inbox ID="Inbox1" runat="server" />
        </div>
    </div>
    <uc1:FooterCtl ID="FooterCtl1" runat="server" />
    </form>
</body>
</html>
