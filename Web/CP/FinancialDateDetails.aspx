﻿<%@ Page Title="Financial Dates" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="FinancialDateDetails.aspx.cs" Inherits="Web.CP.FinancialDateDetails" %>

<%@ Register Src="../UserControls/FinancialDateCtl.ascx" TagName="FinancialDateCtl"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Financial Years
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <uc1:FinancialDateCtl Id="FinancialDateCtl1" runat="server" />
    </div>
</asp:Content>
