﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using Utils.Helper;
using Utils.Calendar;
using BLL.Base;

namespace Web.CP
{
    public partial class HolidayOld : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.LeaveAndAttendance;
            }
        }
        CommonManager cmnMgr = new CommonManager();
        HolidayManager holidayMgr = new HolidayManager();
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialise();
            }         
            
        }

        public string GetNature(object nature)
        {
            bool nt = Convert.ToBoolean(nature);
            if (nt)
                return "Full day";
            else
                return "Second half";
        }

        void Initialise()
        {

            if (IsEnglish)
                gvwHoliday.Columns[0].Visible = false;

            //ddlBranch.DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            //ddlBranch.DataBind();
            List<Branch> list = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);

            chkBranchList.DataSource = list;
            chkBranchList.DataBind();


            if (list.Count <= 1)
                trBranch.Visible = false;

            LoadCasteList();
            Calendar1.IsEnglishCalendar = SessionManager.CurrentCompany.IsEnglishDate;
            Calendar1.SelectTodayDate();

            CalendarToDate.IsEnglishCalendar = SessionManager.CurrentCompany.IsEnglishDate;
            CalendarToDate.SelectTodayDate();

            BindHolidayList();
            BindPayrollPeriods();
            BindDaysOfWeek();
            BindHolidayType();
            BindWeeklyHoliday();

            BindFinancialYears();

            ddlFinancialYear.SelectedValue = "-1";
        }

        private void LoadCasteList()
        {
            //ddlAppliesTo.DataSource = cmnMgr.GetAllCastes();
            //ddlAppliesTo.DataBind();
        }
        private void BindHolidayList()
        {

            List<GetHolidayListResult> holidayList = holidayMgr.GetHolidayList();


            if (ddlFinancialYear.SelectedValue != "-1")
            {
                FinancialDate objFD = HolidayManager.GetFinancialDateById(int.Parse(ddlFinancialYear.SelectedValue));
                holidayList = holidayList.Where(x => x.EngDate.Value.Date >= objFD.StartingDateEng.Value.Date && x.EngDate.Value.Date <= objFD.EndingDateEng.Value.Date).ToList();
            }

            gvwHoliday.DataSource = holidayList;
            gvwHoliday.DataBind();
        }

        private void BindPayrollPeriods()
        {
            ddlPayrollPeriod.DataSource = CommonManager.GetCurrentYear(SessionManager.CurrentCompanyId);
            //payrollMgr.GetPayrollPeriodsByCompanyID(SessionManager.CurrentCompanyId);
            ddlPayrollPeriod.DataBind();
        }
        private void BindDaysOfWeek()
        {           
            ListItem[] items= BizHelper.GetMembersAsListItem(new DAL.DaysOfWeek());
            //int start = 1;
            //foreach (ListItem item in items)
            //{
            //    item.Value = (start++).ToString();
            //}

            ddlDayOfWeek.DataSource = items;
            ddlDayOfWeek.DataBind();
        }
        private void BindHolidayType()
        {
            rdbFullOrHalf.DataSource =  BizHelper.GetMembersAsListItem(new DAL.HolidayType());
            rdbFullOrHalf.DataBind();
            rdbFullOrHalf.SelectedIndex = 0;
        }
        private void BindWeeklyHoliday()
        {
            DaysOfWeek w = new DaysOfWeek();
            HolidayType t = new HolidayType();
            List<GetWeeklyHolidayResult> lstWeeklyHolidays = holidayMgr.GetWeeklyHoliday();
            //foreach (GetWeeklyHolidayResult item in lstWeeklyHolidays)
            //{                            
            //    item.DayofWeek  =  ((KeyValue)w.GetMembers().Find(i=>i.Key == item.DayofWeek)).Value;
            //    item.FullOrHalf = ((KeyValue)t.GetMembers().Find(j => j.Key == item.FullOrHalf)).Value;
            //}

            gvwWeeklyHoliday.DataSource =  lstWeeklyHolidays;
            gvwWeeklyHoliday.DataBind();
        }
        protected void AddHoliday(object sender,EventArgs e)
        {
            if (string.IsNullOrEmpty(txtDescription.Text))
                return;

            if (ddlAppliesTo.SelectedValue != "-2" && chkIsNationalHoliday.Checked)
                return;



            if (holidayMgr.IsHolidayExists(Calendar1.SelectedDate.ToString(), txtDescription.Text.Trim()))
                return;

            List<HolidayList> listHolidays = new List<HolidayList>();

            if ((chkToDate.Checked) && (Calendar1.SelectedDate.ToString() != CalendarToDate.SelectedDate.ToString()))
            {
                CustomDate startDate, endDate;
                DateTime startDateEng, endDateEng;

                if (SessionManager.CurrentCompany.IsEnglishDate)
                {
                    startDate = Calendar1.SelectedDate;
                    endDate = CalendarToDate.SelectedDate;
                }
                else
                {
                    startDate = CustomDate.ConvertNepToEng(Calendar1.SelectedDate);
                    endDate = CustomDate.ConvertNepToEng(CalendarToDate.SelectedDate);
                }

                startDateEng = new DateTime(startDate.Year, startDate.Month, startDate.Day);
                endDateEng = new DateTime(endDate.Year, endDate.Month, endDate.Day);

                if (startDateEng > endDateEng)
                {
                    JavascriptHelper.DisplayClientMsg("Date cannot be greater than To Date.", Page);
                    CalendarToDate.Focus();
                    return;
                }

                TimeSpan ts = endDateEng - startDateEng;
                int totalDays = ts.Days + 1;
                for (int i = 0; i < totalDays; i++)
                {
                    HolidayList entity = new HolidayList();
                    entity.CompanyId = SessionManager.CurrentCompanyId;
                    entity.EngDate = startDateEng.AddDays(i);
                    entity.Date = BLL.BaseBiz.GetAppropriateDate(entity.EngDate.Value);
                    entity.Description = txtDescription.Text.Trim();
                    if (chkIsNationalHoliday.Checked)
                        entity.AppliesTo = -1;
                    else
                        entity.AppliesTo = int.Parse(ddlAppliesTo.SelectedValue);
                    entity.IsNationalHoliday = chkIsNationalHoliday.Checked;
                    entity.BranchIDList = GetSelectedBranch();

                    holidayMgr.Save(entity);
                }
            }
            else
            {

                HolidayList entity = new HolidayList();
                entity.CompanyId = SessionManager.CurrentCompanyId;
                entity.Date = Calendar1.SelectedDate.ToString();
                //set coverted eng version of date from nepali
                CustomDate date;
                if (SessionManager.CurrentCompany.IsEnglishDate)
                    date = Calendar1.SelectedDate;
                else
                    date = CustomDate.ConvertNepToEng(Calendar1.SelectedDate);
                entity.EngDate = new DateTime(date.Year, date.Month, date.Day);

                entity.Description = txtDescription.Text.Trim();
                if (chkIsNationalHoliday.Checked)
                    entity.AppliesTo = -1;
                else
                    entity.AppliesTo = int.Parse(ddlAppliesTo.SelectedValue);
                entity.IsNationalHoliday = chkIsNationalHoliday.Checked;
                entity.BranchIDList = GetSelectedBranch();

                holidayMgr.Save(entity);
            }


            BindHolidayList();
            Clear();
        }

        public void BindFinancialYears()
        {
            List<FinancialDate> financialYearList = new CommonManager().GetAllFinancialDates();
            foreach (FinancialDate date in financialYearList)
            {
                date.SetName(IsEnglish);
            }

            ddlFinancialYear.DataSource = financialYearList;
            ddlFinancialYear.DataBind();
        }

        public string GetSelectedBranch()
        {
            string list = "";
            foreach (ListItem item in chkBranchList.Items)
            {
                if (item.Selected && item.Value != "-1")
                {
                    if (list == "")
                        list = item.Value;
                    else
                        list += "," + item.Value;
                }
            }
            return list.Trim();
        }

        void Clear()
        {
            Calendar1.SelectTodayDate();
            txtDescription.Text = "";
            chkIsNationalHoliday.Checked = false;
            ddlAppliesTo.ClearSelection();
            ddlAppliesTo.SelectedIndex = 0;
            chkBranchList.ClearSelection();
            chkToDate.Checked = false;
            CalendarToDate.SelectTodayDate();
            CalendarToDate.Enabled = false;
        }

        protected void AddWeeklyHoliday(object sender, EventArgs e)
        {
            if (ddlPayrollPeriod.SelectedIndex < 0)
                return;

            if (ddlDayOfWeek.SelectedValue == "-1")
                return;

            if (holidayMgr.IsWeeklyHolidayExists(int.Parse(ddlPayrollPeriod.SelectedValue), int.Parse(ddlDayOfWeek.SelectedValue)))
            {
                JavascriptHelper.DisplayClientMsg("Weekly holiday for this day of week already exists.", Page);
                return;
            }

            WeeklyHoliday weekly = new WeeklyHoliday();
            weekly.EffectiveFrom = int.Parse(ddlPayrollPeriod.SelectedValue);
            weekly.DayOfWeek = int.Parse(ddlDayOfWeek.SelectedValue);
            weekly.IsFullDay = rdbFullOrHalf.Items[0].Selected;
            weekly.CompanyId = SessionManager.CurrentCompanyId;
            holidayMgr.Save(weekly);

            BindWeeklyHoliday();
        }
        protected void gvwHoliday_RowCreated(object sender, GridViewRowEventArgs e)
        {
          
        }
        protected void gvwWeeklyHoliday_RowCreated(object sender, GridViewRowEventArgs e)
        {

        }
        protected void gvwWeeklyHoliday_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
         
            int id = (int)gvwWeeklyHoliday.DataKeys[e.RowIndex]["ID"];

            if (holidayMgr.DeleteWeeklyHoliday(id))
                BindWeeklyHoliday();
            else
                JavascriptHelper.DisplayClientMsg("Weekly holiday is associated with the attendance, so can not be deleted.", Page);
        }

        protected void gvwHoliday_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvwHoliday.PageIndex = e.NewPageIndex;
            BindHolidayList();
        }

        protected void gvwHoliday_Sorting(object sender, GridViewSortEventArgs e)
        {

        }

        protected void gvwHoliday_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int id = (int)gvwHoliday.DataKeys[e.RowIndex]["ID"];

            if (holidayMgr.DeleteHoliday(id))
                BindHolidayList();
            else
                JavascriptHelper.DisplayClientMsg("Holiday is associated with the attendance, so can not be deleted.", Page);
        }

        protected void gvwWeeklyHoliday_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        protected void chkToDate_CheckedChanged(object sender, EventArgs e)
        {
            if (chkToDate.Checked)
                CalendarToDate.Enabled = true;
            else
            {
                CalendarToDate.SelectTodayDate();
                CalendarToDate.Enabled = false;
            }
        }

        protected void ddlFinancialYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindHolidayList();
        }
    }
}

