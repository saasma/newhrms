<%@ Page Title="Voucher Report" Language="C#" EnableViewState="true" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="HPLVoucherList.aspx.cs" Inherits="Web.HPLVoucherList" %>

<%@ Import Namespace="BLL.Manager" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        var skipLoadingCheck = true;
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Voucher Details
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
       
        <div class="attribute" style="padding:10px;">
            <table cellpadding="3" cellspacing="0">
                <tr>
                    <td class="filterHeader" runat="server" id="rowPayrollFrom1">
                        <strong>Payroll</strong>
                    </td>
                    <td rowspan="2" valign="bottom">
                        <asp:Button ID="btnLoad" Style='float: left' CssClass="load" runat="server" Text="Load" />
                    </td>
                </tr>
                <tr>
                    <td runat="server" id="rowPayrollFrom2">
                        <asp:DropDownList ID="ddlPayrollFromMonth" runat="server">
                        </asp:DropDownList>
                        <asp:DropDownList ID="ddlPayrollFromYear" runat="server">
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
        </div>
        <div class="clear gridBlock">
            <cc2:EmptyDisplayGridView EnableViewState="false" CssClass="tableLightColor" UseAccessibleHeader="true"
                ShowHeaderWhenEmpty="True" ID="gvEmployeeIncome" runat="server" AutoGenerateColumns="False"
                CellPadding="4" GridLines="None" ShowFooterWhenEmpty="False">
                <RowStyle BackColor="#E3EAEB" />
                <Columns>
                    <asp:BoundField DataField="PostingPeriod" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="PostingPeriod"></asp:BoundField>
                    <asp:BoundField DataField="Date" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="Date"></asp:BoundField>
                    <asp:BoundField DataField="Currency" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="Currency"></asp:BoundField>
                    <asp:BoundField DataField="Account" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="Account"></asp:BoundField>
                    <asp:BoundField DataField="Debit" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                        HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" HeaderText="Debit"
                        DataFormatString="{0:N2}" />
                    <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="Credit"
                        DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"
                        HeaderText="Credit" />
                    <asp:BoundField DataField="Memo" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="Memo"></asp:BoundField>
                    <asp:BoundField DataField="Entity" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="Entity"></asp:BoundField>
                    <asp:BoundField DataField="Department" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="Department"></asp:BoundField>
                    <asp:BoundField DataField="Class" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="Class"></asp:BoundField>
                    <asp:BoundField DataField="Location" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="Location"></asp:BoundField>
                </Columns>
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                <EmptyDataTemplate>
                    <b>No employee list. </b>
                </EmptyDataTemplate>
            </cc2:EmptyDisplayGridView>
        </div>
        <div class="buttonsDiv">
            <asp:Button ID="btnExport" CssClass="excel" runat="server" Text="Export" OnClick="btnExport_Click" />
        </div>
    </div>
</asp:Content>
