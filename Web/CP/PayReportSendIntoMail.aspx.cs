﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using BLL;
using DAL;
using Utils.Web;
using Utils.Helper;
using Utils;
using System.IO;
using Web.ReportDataSetTableAdapters;
using Web.CP.Report.Templates.Pay.Detail;
using BLL.BO;
using System.Text;
using System.Drawing;
using Utils.Calendar;
using Web.CP.Report;

namespace Web.CP
{
    public partial class PayReportSendIntoMail : BasePage
    {
        CommonManager commonMgr = new CommonManager();
        System.Drawing.Bitmap bitmap = null;



        protected void Page_Load(object sender, EventArgs e)
        {
           
            if (!IsPostBack)
                Initialise();
            
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (bitmap != null)
                bitmap.Dispose();
        }

        public void Initialise()
        {

            btnSendMail.OnClientClick = string.Format(btnSendMail.OnClientClick,
                Resources.Messages.SentMailForSelectedEmpConfirmationMsg);

            //btnSendMailForAll.OnClientClick = string.Format(btnSendMailForAll.OnClientClick,
            //    Resources.Messages.SentMailForAllEmployeesMsg);

            ddlPayrollPeriods.DataSource = CommonManager.GetCurrentYear(SessionManager.CurrentCompanyId);
            ddlPayrollPeriods.DataBind();

            ddlBranch.DataSource
              = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            ddlBranch.DataBind();

            UIHelper.SelectListItem(ddlPayrollPeriods);

            BindEmployees();


            
        }


        protected void btnLoad_Click(object sender, EventArgs e)
        {
            BindEmployees();
            }
        public void BindEmployees()
        {
           
            
            if (ddlPayrollPeriods.SelectedValue.Equals("-1"))
            {
                gvwEmployees.DataSource = null;
                gvwEmployees.DataBind();

                divButtons.Visible = false;
                pagingCtl.Visible = false;
            }
            else
            {
                int addOnId = -1;
                if (!string.IsNullOrEmpty(ddlAddOnName.SelectedValue)
                   && ddlAddOnName.SelectedValue != "-1"
                   && ddlAddOnName.SelectedValue != "0")
                {
                    addOnId = int.Parse(ddlAddOnName.SelectedValue);
                }


                int BranchID = -1;
                if (!string.IsNullOrEmpty(ddlBranch.SelectedItem.Value))
                    BranchID = int.Parse(ddlBranch.SelectedItem.Value);
                divButtons.Visible = true;
                int totalRecords = 0;
                gvwEmployees.SelectedIndex = -1;
                gvwEmployees.DataSource = CalculationManager.GetCalculationEmployeeList(int.Parse(ddlPayrollPeriods.SelectedValue), txtEmployeeName.Text.Trim(), pagingCtl.CurrentPage - 1, int.Parse(pagingCtl.DDLRecords.SelectedValue),
                    ref totalRecords, BranchID, addOnId);
                gvwEmployees.DataBind();
                if(totalRecords == 0)
                    pagingCtl.Visible = false;
                pagingCtl.UpdatePagingBar(totalRecords);
            }

            
        }


        protected void ddlBranch_Selected(object sender, EventArgs e)
        {
            BindEmployees();
        }



        protected void ddlAddOnName_Change(object sender, EventArgs e)
        {
            

            BindEmployees();
        }

        protected void ddlPayrollPeriods_SelectedIndexChanged(object sender, EventArgs e)
        {
            int peroidId = int.Parse(ddlPayrollPeriods.SelectedValue);

            int selectedAddOnId = 0;
            if (!string.IsNullOrEmpty(ddlAddOnName.SelectedValue))
                selectedAddOnId = int.Parse(ddlAddOnName.SelectedValue);

            List<AddOn> list = PayManager.GetAddOnForPayrollPeriod(peroidId);

            list.Insert(0, new AddOn { Name = "--Select Add on--", AddOnId = 0 });

            ddlAddOnName.DataSource = list;
            ddlAddOnName.DataBind();



            BindEmployees();
        }

        private MemoryStream GetReportAsStreamWithTaxDetails(PayrollPeriod payrollPeriod, int employeeId)
        {
            Report_Pay_PaySlipDetailTableAdapter mainAdapter = new Report_Pay_PaySlipDetailTableAdapter();



            ReportPaySlipWithTaxDetails mainReport = new ReportPaySlipWithTaxDetails();


            mainReport.labelCompanyName.Text = new CompanyManager().GetById(SessionManager.CurrentCompanyId).Name;
          

            BLL.BaseBiz.SetConnectionPwd(mainAdapter.Connection);
            ReportDataSet.Report_Pay_PaySlipDetailDataTable mainTable =
                mainAdapter.GetData(payrollPeriod.Month, payrollPeriod.Year,
                                                                      SessionManager.CurrentCompanyId, employeeId, "", "", 1, null, -1);

            mainReport.labelMonth.Text = string.Format("{0} ({1} {2})", payrollPeriod.Name,
                                                         DateHelper.GetMonthsForOtherPayrollPeriod(payrollPeriod.Month,
                                                                                                   IsEnglish)
                                                                                                   , DateManager.GetOtherYearForPayrollPeriod(payrollPeriod, IsEnglish));
            
            mainReport.Month = payrollPeriod.Month;
            mainReport.Year = payrollPeriod.Year.Value;
            mainReport.IsIncome = true;
            mainReport.CompanyId = SessionManager.CurrentCompanyId;

            mainReport.DataSource = mainTable;
            mainReport.DataMember = "Report";

            MemoryStream stream = new MemoryStream();

            mainReport.ExportToImage(stream, System.Drawing.Imaging.ImageFormat.MemoryBmp);
            stream.Position = 0;

            Bitmap b = new Bitmap(stream);

            ImageConverter ic = new ImageConverter();
            Byte[] ba = (Byte[])ic.ConvertTo(b, typeof(Byte[]));

            MemoryStream logo = new MemoryStream(ba);
            logo.Position = 0;


            return logo;
        }
        private MemoryStream GetReportAsStream(PayrollPeriod payrollPeriod, int employeeId,int addOnId)
        {
            Report_Pay_PaySlipDetailTableAdapter mainAdapter = new Report_Pay_PaySlipDetailTableAdapter();


            if (CommonManager.CompanySetting.AppendTaxDetailsInPayslip)
            {
                return GetReportAsStreamWithTaxDetails(payrollPeriod, employeeId);
            }

            ReportPaySlip mainReport = new ReportPaySlip();

           
            mainReport.labelCompanyName.Text = new CompanyManager().GetById(SessionManager.CurrentCompanyId).Name;
            if (bitmap != null)
            {

                mainReport.logo.Image = bitmap;
                mainReport.logo.Visible = true;
            }

            PayslipType paysliptype = PayslipType.NormalTotal;

            if (addOnId != -1)
            {
                paysliptype = PayslipType.NewAddOn;

                mainReport.att1.Visible = false;
                mainReport.att2.Visible = false;
                mainReport.att3.Visible = false;

                mainReport.total1.Visible = false;
                mainReport.total2.Visible = false;
                mainReport.total3.Visible = false;
                mainReport.total4.Visible = false;

                mainReport.subReportAttendace.Visible = false;
            }

            BLL.BaseBiz.SetConnectionPwd(mainAdapter.Connection);
            ReportDataSet.Report_Pay_PaySlipDetailDataTable mainTable =
                mainAdapter.GetData(payrollPeriod.Month, payrollPeriod.Year,
                                                                      SessionManager.CurrentCompanyId, employeeId, "", "", (int)paysliptype, addOnId, -1);

            mainReport.labelMonth.Text = string.Format("{0} ({1} {2})", payrollPeriod.Name,
                                                          DateHelper.GetMonthsForOtherPayrollPeriod(payrollPeriod.Month,
                                                                                                    IsEnglish)
                                                                                                    , DateManager.GetOtherYearForPayrollPeriod(payrollPeriod, IsEnglish));
            mainReport.Month = payrollPeriod.Month;
            mainReport.Year = payrollPeriod.Year.Value;
            mainReport.IsIncome = true;
            mainReport.CompanyId = SessionManager.CurrentCompanyId;

            mainReport.DataSource = mainTable;
            mainReport.DataMember = "Report";

            MemoryStream stream  = new MemoryStream();

            mainReport.ExportToImage(stream, System.Drawing.Imaging.ImageFormat.MemoryBmp);
            stream.Position = 0;

            Bitmap b = new Bitmap(stream);

            ImageConverter ic = new ImageConverter();
            Byte[] ba = (Byte[])ic.ConvertTo(b, typeof(Byte[]));
            
            MemoryStream logo = new MemoryStream(ba);            
            logo.Position = 0;
          

            return logo;
        }


        protected void btnSendMail_Click(object sender, EventArgs e)
        {

            int addOnId = -1;
            if (!string.IsNullOrEmpty(ddlAddOnName.SelectedValue)
               && ddlAddOnName.SelectedValue != "-1"
               && ddlAddOnName.SelectedValue != "0")
            {
                addOnId = int.Parse(ddlAddOnName.SelectedValue);
            }


            SMTPHelper mailSender = new SMTPHelper();
            PayrollPeriod payroll = CommonManager.GetPayrollPeriod(int.Parse(ddlPayrollPeriods.SelectedValue));

            EmailContent dbMailContent = CommonManager.GetMailContent((int)EmailContentType.SalaryMail);

            string subject = dbMailContent == null ?
                string.Format(Config.SalaryMailSubject, payroll.Name) : string.Format(dbMailContent.Subject, payroll.Name);

            string body =
                dbMailContent == null ?
                File.ReadAllText(Server.MapPath("~/App_Data/EmailTemplates/SalaryEmailContent.txt")) : dbMailContent.Body;

            body = string.Format(body, payroll.Name);

            int count = 0;
            StringBuilder str = new StringBuilder();
            for (int i = 0; i < this.gvwEmployees.Rows.Count; i++)
            {
                GridViewRow row = gvwEmployees.Rows[i];
                int employeeId = (int)gvwEmployees.DataKeys[row.RowIndex]["EmployeeId"];

                if (str.Length == 0)
                    str.Append(employeeId);
                else
                    str.Append("," + employeeId);
            }

            GenerateData(CommonManager.GetPayrollPeriod(int.Parse(ddlPayrollPeriods.SelectedValue)), str.ToString(),addOnId);

            string logoLoc = ReportManager.PaySlipLogoAbsolutePath; //Server.MapPath("~/Images/PaySlip_Company_Logo-80x55.png");
            if (File.Exists(logoLoc))
            {

                bitmap = new System.Drawing.Bitmap(logoLoc);
            }
            string errorMsg = "";
            for (int i = 0; i < this.gvwEmployees.Rows.Count; i++)
            {
                GridViewRow row = gvwEmployees.Rows[i];
                int employeeId = (int)gvwEmployees.DataKeys[row.RowIndex]["EmployeeId"];
                int calculationEmployeeId = (int)gvwEmployees.DataKeys[row.RowIndex]["CalculationEmployeeId"];
                string email = gvwEmployees.DataKeys[row.RowIndex]["Email"].ToString();

                if (!string.IsNullOrEmpty(email))
                {
                    if (row.FindControl("chkSelect") == null)
                        continue;
                    CheckBox chkSelect = (CheckBox)row.FindControl("chkSelect");
                    if (chkSelect.Checked)
                    {
                        if (mailSender.SendSalary(email, body, subject, GetReportAsStream(payroll, employeeId,addOnId), ref errorMsg))
                        {
                            if (addOnId == -1)
                                CalculationManager.UpdateCalculationEmployeeForMail(calculationEmployeeId);
                            count += 1;
                        }
                        else
                        {
                            divWarningMsg.InnerHtml = errorMsg;
                            divWarningMsg.Hide = false;
                            return;
                        }

                    }
                }
            }

            if (count > 0)
            {
                BindEmployees();
            }


            divMsgCtl.InnerHtml =
                string.Format(Resources.Messages.SentMailSuccessMsg, count);
            divMsgCtl.Hide = false;
        }

        protected void GenerateData(PayrollPeriod payrollPeriod,string employeeListID,int addOnId)
        {
            bool readingSumForMiddleFiscalYearStartedReqd = false;
            int startingPayrollPeriodId = 0, endingPayrollPeriodId = 0;
            PayrollPeriod startingMonthParollPeriodInFiscalYear =
               CalculationManager.GenerateForPastIncome(
                    payrollPeriod.PayrollPeriodId, ref readingSumForMiddleFiscalYearStartedReqd,
                    ref startingPayrollPeriodId, ref endingPayrollPeriodId);

            #region "Income/Deduction/Atte List"

            // Code to retrieve all emp once & save the DataTable in the request Cache so that multiple db request can be prevented
            // due to large no of employees
            Report_Pay_PaySlipDetail_EmpHeaderTableAdapter incomeAdapter = new Report_Pay_PaySlipDetail_EmpHeaderTableAdapter();
            BLL.BaseBiz.SetConnectionPwd(incomeAdapter.Connection);

            Report_Pay_PaySlip_AttendanceTableAdapter attendacDetailAdapter = new Report_Pay_PaySlip_AttendanceTableAdapter();
            BLL.BaseBiz.SetConnectionPwd(attendacDetailAdapter.Connection);

            PayslipType paysliptype = PayslipType.NormalTotal;

            if (addOnId != -1)
                paysliptype = PayslipType.NewAddOn;

            //incomes
            ReportDataSet.Report_Pay_PaySlipDetail_EmpHeaderDataTable incomesTable = incomeAdapter.GetData(payrollPeriod.Month, payrollPeriod.Year,
                                              SessionManager.CurrentCompanyId, null, true, employeeListID, startingPayrollPeriodId, endingPayrollPeriodId,(int) paysliptype, addOnId);

            ReportDataSet.Report_Pay_PaySlipDetail_EmpHeaderDataTable deductionTable = incomeAdapter.GetData(payrollPeriod.Month, payrollPeriod.Year,
                                              SessionManager.CurrentCompanyId, null, false, employeeListID, startingPayrollPeriodId, endingPayrollPeriodId, (int)paysliptype, addOnId);


            ReportDataSet.Report_Pay_PaySlip_AttendanceDataTable attendanceTable =
                    attendacDetailAdapter.GetData(payrollPeriod.Month, payrollPeriod.Year, SessionManager.CurrentCompanyId, null,employeeListID);


            try
            {
                //to prevent db timeout due to 100 connection full
                incomeAdapter.Connection.Close();
                attendacDetailAdapter.Connection.Close();
               
            }
            catch (Exception exp)
            {
                Log.log("Error", exp);
            }
            HttpContext.Current.Items["PaySlipIncomeList"] = incomesTable;
            HttpContext.Current.Items["PaySlipDeductionList"] = deductionTable;
            HttpContext.Current.Items["PaySlipAttendanceList"] = attendanceTable;
            #endregion
        }

        //protected void btnSendMailForAll_Click(object sender, EventArgs e)
        //{
        //    SMTPHelper mailSender = new SMTPHelper();
        //    PayrollPeriod payroll = CommonManager.GetPayrollPeriod(int.Parse(ddlPayrollPeriods.SelectedValue));

        //    EmailContent dbMailContent = CommonManager.GetMailContent((int)EmailContentType.SalaryMail);

        //    string subject = dbMailContent == null ?
        //        string.Format(Config.SalaryMailSubject, payroll.Name) : string.Format(dbMailContent.Subject, payroll.Name);

        //    string body =
        //        dbMailContent == null ?
        //        File.ReadAllText(Server.MapPath("~/App_Data/EmailTemplates/SalaryEmailContent.txt")) : dbMailContent.Body;

        //    body = string.Format(body, payroll.Name);


        //    int totalRecords = 0;
        //    List<GetCalculationEmployeeListResult> employees = CalculationManager.GetCalculationEmployeeList(int.Parse(ddlPayrollPeriods.SelectedValue), txtEmployeeName.Text.Trim(), pagingCtl.CurrentPage - 1, int.Parse(pagingCtl.DDLRecords.SelectedValue), ref totalRecords).ToList();
        //    if (employees == null)
        //        return;

        //    PayrollPeriod period = CommonManager.GetPayrollPeriod(int.Parse(ddlPayrollPeriods.SelectedValue));

        //    GenerateData(period,null);

        //    int count = 0;
        //    string logoLoc = ReportManager.PaySlipLogoAbsolutePath; //Server.MapPath("~/Images/PaySlip_Company_Logo-80x55.png");
        //    if (File.Exists(logoLoc))
        //    {

        //        bitmap = new System.Drawing.Bitmap(logoLoc);
        //    }

        //    foreach (GetCalculationEmployeeListResult emp in employees)
        //    {
        //        if (!string.IsNullOrEmpty(emp.Email))
        //        {
        //            if (mailSender.SendSalary(emp.Email, body, subject, GetReportAsStream(payroll, emp.EmployeeId)))
        //            {
        //                CalculationManager.UpdateCalculationEmployeeForMail(emp.CalculationEmployeeId);
        //                count += 1;
        //            }

        //        }
        //    }
        //    if (count > 0)
        //    {
        //        BindEmployees();
        //    }


        //    divMsgCtl.InnerHtml =
        //        string.Format(Resources.Messages.SentMailSuccessMsg, count);
        //    divMsgCtl.Hide = false;
        //}

        protected void gvwEmployees_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvwEmployees.PageIndex = e.NewPageIndex;
            BindEmployees();
        }

        protected void btnPrevious_Click()
        {
            pagingCtl.CurrentPage -= 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage -= 1;
            BindEmployees();
        }

        protected void btnNext_Click()
        {
            pagingCtl.CurrentPage += 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage += 1;
            BindEmployees();
        }

        protected void ddlRecords_SelectedIndexChanged()
        {
            //SessionManager.EmployeeDisplayRecodsPerPage = int.Parse(pagintCtl.DDLRecords.SelectedValue);
            //_tempCurrentPage = 1;
            pagingCtl.CurrentPage = 1;
            BindEmployees();
        }

    }
}
