<%@ Page Title="Tax History" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="HistoryForTax.aspx.cs" Inherits="Web.HistoryForTax" %>

<%@ Import Namespace="BLL.Manager" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .tableLightColor input
        {
            width: 120px;
        }
    </style>
    <script src="../Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        function refreshWindow() {



            __doPostBack('Refresh', 0);


        }
        jQuery(document).ready(
    function () {

        setMovementToGrid('#<%= gvEmployeeIncome.ClientID %>');
    }
);

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Tax history list
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="attribute" style="padding:10px">
            <table>
                <tr>
                    <td>
                        Search &nbsp;
                        <asp:TextBox ID="txtEmployeeName" AutoPostBack="true" runat="server" Width="146px"
                            OnTextChanged="txtEmployeeName_TextChanged"></asp:TextBox>
                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtEmployeeName"
                            WatermarkText="Employee" WatermarkCssClass="searchBoxText" />
                        <cc1:AutoCompleteExtender EnableCaching="true" ID="AutoCompleteExtenderOrganization"
                            runat="server" MinimumPrefixLength="1" ServiceMethod="GetEmployeeNames" ServicePath="~/PayrollService.asmx"
                            TargetControlID="txtEmployeeName" CompletionSetCount="10" CompletionInterval="250"
                            CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList"
                            CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
                        </cc1:AutoCompleteExtender>
                    </td>
                    <td>
                        <asp:LinkButton ID="btnExport" runat="server" Text="Excel import" OnClientClick="taxImportPopup();return false;"
                            CssClass=" excel marginRight tiptip" Style="float: left;" />
                    </td>
                </tr>
            </table>
        </div>
        <div class="clear">
            <cc1:TextBoxWatermarkExtender ID="TBWE2" runat="server" TargetControlID="txtEmployeeName"
                WatermarkText="Enter employee name" WatermarkCssClass="watermarked" />
            <cc2:EmptyDisplayGridView CssClass="table table-primary mb30 table-bordered table-hover" style="margin-bottom:0px" UseAccessibleHeader="true" ShowHeaderWhenEmpty="True"
                ID="gvEmployeeIncome" runat="server" DataKeyNames="EmployeeId" AutoGenerateColumns="False"
                CellPadding="4" GridLines="None" AllowSorting="True" ShowFooterWhenEmpty="False">
<%--                <RowStyle BackColor="#E3EAEB" />--%>
                <Columns>
                    <asp:BoundField HeaderStyle-Width="30px" DataFormatString="{0:000}" DataField="EmployeeId"
                        HeaderText="EIN"></asp:BoundField>
                     <asp:BoundField HeaderStyle-Width="80px"  DataField="IdCardNo"
                        HeaderText="I No"></asp:BoundField>
                    <asp:TemplateField HeaderText="Name" HeaderStyle-Width="150px">
                        <ItemTemplate>
                            <asp:Label Text='<%# Eval("Name") %>' Style="width: 100px!important" runat="server" />
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" Width="150px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Opening Gross Amt. including PF Income"
                        HeaderStyle-Width="80px">
                        <ItemTemplate>
                            <asp:TextBox CssClass='calculationInput' data-col='0' data-row='<%# Container.DataItemIndex %>'
                                Enabled='<%# !CommonManager.IsFirstSalaryGenerated() %>' Style='text-align: right'
                                ID="txtAmount" Text='<%# GetCurrency( Eval("OpeningTaxableAmount")) %>' runat="server"></asp:TextBox>
                            <asp:CompareValidator SetFocusOnError="true" ValueToCompare="0" Display="None" Operator="GreaterThanEqual"
                                Type="Currency" ID="valOpeningBalance" ControlToValidate="txtAmount" ValidationGroup="Balance"
                                runat="server" ErrorMessage="Invalid amount."></asp:CompareValidator>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Opening PF Deduction" HeaderStyle-Width="80px">
                        <ItemTemplate>
                            <asp:TextBox CssClass='calculationInput' data-col='1' data-row='<%# Container.DataItemIndex %>'
                                Enabled='<%# !CommonManager.IsFirstSalaryGenerated() %>' Style='text-align: right'
                                ID="txtPF" Text='<%# GetCurrency( Eval("OpeningPF")) %>' runat="server"></asp:TextBox>
                            <asp:CompareValidator SetFocusOnError="true" ValueToCompare="0" Display="None" Operator="GreaterThanEqual"
                                Type="Currency" ID="valOpeningBalance1" ControlToValidate="txtPF" ValidationGroup="Balance"
                                runat="server" ErrorMessage="Invalid amount."></asp:CompareValidator>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Opening CIT" HeaderStyle-Width="80px">
                        <ItemTemplate>
                            <asp:TextBox CssClass='calculationInput' data-col='2' data-row='<%# Container.DataItemIndex %>'
                                Enabled='<%# !CommonManager.IsFirstSalaryGenerated() %>' Style='text-align: right'
                                ID="txtCIT" Text='<%# GetCurrency( Eval("OpeningCIT")) %>' runat="server"></asp:TextBox>
                            <asp:CompareValidator SetFocusOnError="true" ValueToCompare="0" Display="None" Operator="GreaterThanEqual"
                                Type="Currency" ID="valOpeningBalance289" ControlToValidate="txtCIT" ValidationGroup="Balance"
                                runat="server" ErrorMessage="Invalid amount."></asp:CompareValidator>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" Visible="false" HeaderText="Opening Insurance"
                        HeaderStyle-Width="80px">
                        <ItemTemplate>
                            <asp:TextBox CssClass='calculationInput' data-col='3' data-row='<%# Container.DataItemIndex %>'
                                Enabled='<%# !CommonManager.IsFirstSalaryGenerated() %>' Style='text-align: right'
                                ID="txtInsurance" Text='<%# GetCurrency( Eval("OpeningInsurance")) %>' runat="server"></asp:TextBox>
                            <asp:CompareValidator SetFocusOnError="true" ValueToCompare="0" Display="None" Operator="GreaterThanEqual"
                                Type="Currency" ID="valOpeningBalance23" ControlToValidate="txtInsurance" ValidationGroup="Balance"
                                runat="server" ErrorMessage="Invalid amount."></asp:CompareValidator>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Opening SST" HeaderStyle-Width="80px">
                        <ItemTemplate>
                            <asp:TextBox CssClass='calculationInput' data-col='4' data-row='<%# Container.DataItemIndex %>'
                                Enabled='<%# !CommonManager.IsFirstSalaryGenerated() %>' Style='text-align: right'
                                ID="txtOpeningSST" Text='<%# GetCurrency( Eval("OpeningSST")) %>' runat="server"></asp:TextBox>
                            <asp:CompareValidator SetFocusOnError="true" ValueToCompare="0" Display="None" Operator="GreaterThanEqual"
                                Type="Currency" ID="valOpeningBalance2451" ControlToValidate="txtOpeningSST"
                                ValidationGroup="Balance" runat="server" ErrorMessage="Invalid amount."></asp:CompareValidator>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Opening Tax Paid"
                        HeaderStyle-Width="80px">
                        <ItemTemplate>
                            <asp:TextBox CssClass='calculationInput' data-col='5' data-row='<%# Container.DataItemIndex %>'
                                Enabled='<%# !CommonManager.IsFirstSalaryGenerated() %>' Style='text-align: right'
                                ID="txtTaxPaid" Text='<%# GetCurrency( Eval("OpeningTaxPaid")) %>' runat="server"></asp:TextBox>
                            <asp:CompareValidator SetFocusOnError="true" ValueToCompare="0" Display="None" Operator="GreaterThanEqual"
                                Type="Currency" ID="valOpeningBalance245" ControlToValidate="txtTaxPaid" ValidationGroup="Balance"
                                runat="server" ErrorMessage="Invalid amount."></asp:CompareValidator>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Opening Remote Area"
                        HeaderStyle-Width="80px">
                        <ItemTemplate>
                            <asp:TextBox CssClass='calculationInput' data-col='5' data-row='<%# Container.DataItemIndex %>'
                                Enabled='<%# !CommonManager.IsFirstSalaryGenerated()  && IsOpeningRemoteAreaEnabled()%>'
                                Style='text-align: right' ID="txtRemoteArea" Text='<%# GetCurrency( Eval("OpeningRemoteArea")) %>'
                                runat="server"></asp:TextBox>
                            <asp:CompareValidator SetFocusOnError="true" ValueToCompare="0" Display="None" Operator="GreaterThanEqual"
                                Type="Currency" ID="valOpeningBalance24511" ControlToValidate="txtRemoteArea"
                                ValidationGroup="Balance" runat="server" ErrorMessage="Invalid amount."></asp:CompareValidator>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                <EmptyDataTemplate>
                    <b>No tax history. </b>
                </EmptyDataTemplate>
            </cc2:EmptyDisplayGridView>
            <uc1:PagingCtl ID="pagingCtl" OnNextRecord="btnNext_Click" OnPrevRecord="btnPrevious_Click"
                OnDropDownShowRecordsChanged="ddlRecords_SelectedIndexChanged" runat="server" />
        </div>
        <div class="buttonsDiv">
            <%--    <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Balance" DisplayMode="List"
            ShowSummary="false" ShowMessageBox="true" runat="server"></asp:ValidationSummary>--%>
            <asp:Button ID="btnUpdate" CssClass="update" OnClientClick="valGroup='Balance';return CheckValidation();"
                Visible="true" ValidationGroup="Balance" runat="server" Text="Update" OnClick="btnUpdate_Click" />
        </div>
    </div>
</asp:Content>
