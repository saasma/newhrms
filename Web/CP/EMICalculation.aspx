<%@ Page Title="EMI calculation" Language="C#" MasterPageFile="~/Master/ForPopupPage.Master"
    AutoEventWireup="true" CodeBehind="EMICalculation.aspx.cs" Inherits="Web.CP.EMICalculation" %>

<%@ Register Src="../Controls/InfoMsgCtl.ascx" TagName="InfoMsgCtl" TagPrefix="uc1" %>
<%@ Register Src="../Controls/WarningMsgCtl.ascx" TagName="WarningMsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        var calculated = false;
        var txtAmountId = '#<%= txtAmount.ClientID %>';
        var takenOn = '<%= calTakenOn.ClientID %>';
        var interestRate = '#<%= txtInterestRate.ClientID %>';
        var marketRate = '#<%= txtMarketRate.ClientID %>';
        var toBePaidOver = '#<%= txtToBePaidOver.ClientID %>';
        var hfRemainingInstallment = '#<%= hfRemainingInstallment.ClientID %>';
        var paymentTerms = '#<%= ddlPaymentTerms.ClientID %>';
        var ddlMonths = '#<%= ddlMonths.ClientID %>';
        var years = '#<%= ddlYears.ClientID %>';
        var monthlyPayment = '#<%= lblMonthlyPayment.ClientID %>';
        var lastPayment = '#<%= lblLastPayment.ClientID %>';
        var totalInterest = '#<%= lblTotalInterest.ClientID %>';
        var hfPaid = '#<%= hfPaid.ClientID %>'; ;
        var chkIsFixedInstallmentAmount = '#<%= chkIsFixedInstallmentAmount.ClientID %>'; ;
        function ReturnValues() {

            var obj = new Array();
            obj["Advance"] = $(txtAmountId).val();
            obj["TakenOn"] = getCalendarSelectedDate(takenOn);

            if (hasVariableAmount) {
                obj["InterestRate"] = "";
                obj["MarketRate"] = "";
                obj["ToBePaidOver"] = "";
                obj["RemainingInstallment"] = "";
                obj["PaymentTerms"] = "";
                obj["Months"] = "";
                obj["Years"] = "";
                obj["MonthlyPayment"] = "";
                obj["Balance"] = "";
                obj["Paid"] = "";
                obj["IsFixedInstallment"] = "";
            }
            else {
                obj["Balance"] = $('#<%= hfBalance.ClientID %>').val();
                obj["InterestRate"] = $(interestRate).val();
                obj["MarketRate"] = $(marketRate).val();
                obj["ToBePaidOver"] = $(toBePaidOver).val();
                obj["RemainingInstallment"] = $(hfRemainingInstallment).val(); ;
                obj["PaymentTerms"] = $(paymentTerms).val();
                obj["Months"] = $(ddlMonths).val();
                obj["Years"] = $(years).val();
                obj["MonthlyPayment"] = $(monthlyPayment).text();
                var isFixed1 = document.getElementById('<%= chkIsFixedInstallmentAmount.ClientID %>');
                if (typeof (isFixed1) == 'undefined' || isFixed1 == null)
                    obj["IsFixedInstallment"] = "";
                else
                    obj["IsFixedInstallment"] = isFixed1.checked;
                //$(chkIsFixedInstallmentAmount).val();
                obj["Paid"] = $(hfPaid).val();
            }


            // alert(window.opener.parentReloadCallbackFunction)
            //if ($.browser.msie == false && typeof (window.opener.loadCalculation) != 'undefined' && ($.browser.chrome || $.browser.safari)) {
            window.opener.loadCalculation(obj, window);
            //            } else {
            //                window.returnValue = obj;
            //                window.close();
            //            }

        }
        function okClick() {

            valGroup = 'Calculation';
            if (CheckValidation() == false)
                return false;
            //            if( calculated==false)
            {
                ReturnValues();
                return false;
            }
        }



        function setFixedInstallment(chk) {

            var txtToBePaidOver = '<%= txtToBePaidOver.ClientID %>';
            var txtFixedInstallmentAmount = '<%= txtFixedInstallmentAmount.ClientID %>';
            if (chk.checked) {
                $('#<%= txtToBePaidOver.ClientID %>').addClass('disabledTxt');
                $('#<%= txtFixedInstallmentAmount.ClientID %>').removeClass('disabledTxt');
            }
            else {
                $('#<%= txtFixedInstallmentAmount.ClientID %>').addClass('disabledTxt');
                $('#<%= txtToBePaidOver.ClientID %>').removeClass('disabledTxt');
            }
        }

        function onchangeFixedInstallment() {

            var chkIsFixedInstallmentAmount = document.getElementById('<%= chkIsFixedInstallmentAmount.ClientID %>');
            var txtToBePaidOver = '#<%= txtToBePaidOver.ClientID %>';
            var txtFixedInstallmentAmount = '#<%= txtFixedInstallmentAmount.ClientID %>';
            var txtAmount = '#<%= txtAmount.ClientID %>';
            if (chkIsFixedInstallmentAmount.checked) {
                var tobepaidover = parseFloat($(txtAmount).val().replaceAll(",", "")) / parseFloat($(txtFixedInstallmentAmount).val().replaceAll(",", ""));
                tobepaidover = Number((tobepaidover).toFixed());
                $(txtToBePaidOver).val(tobepaidover);
            }
            else {

            }




        }

        function validateStartingDate(source, args) {

            args.IsValid = false;

            //first check if starting date >= payroll period
            var ddlMonths = '#<%= ddlMonths.ClientID %>';
            var ddlYears = '#<%= ddlYears.ClientID %>';

            var startingDate = $(ddlYears).val() + "/" + leftPad($(ddlMonths).val(), 2) + "/01";

            if (!isDateSame(startingDate, payrollStartDate)
                && !isSecondCalendarCtlDateGreater(payrollStartDate, startingDate)) {
                args.IsValid = false;
                alert("Starting date must be greater than or equal to the current payroll period " +
                    payrollStartDate + ".");
                return;
            }


            var takenOn = getCalendarSelectedDateAsArray('<%= calTakenOn.ClientID %>');
            var takenOnDate = takenOn['Year'] + "/" + leftPad(takenOn['Month'], 2) + "/01";

            if (!isDateSame(startingDate, takenOnDate)
                && !isSecondCalendarCtlDateGreater(takenOnDate, startingDate)) {
                args.IsValid = false;
                alert("Starting date must be greater than or equal to taken on date.");
                return;
            }

            args.IsValid = true;

        }


        var txtAmount = '<%= txtAmount.ClientID %>';
        $(document).ready
        (
            function () {
                $('#' + txtAmount).blur(
                    function () {
                        processCalcuatorOnBlur(this);
                    }
                );
            }
        );

        function processCalcuatorOnBlur(element) {
            var originalValue = element.value;
            try {
                var value = element.value;

                if (value.indexOf("+") > 0 || value.indexOf("-") > 0 || value.indexOf("*") > 0 || value.indexOf("/") > 0) {
                    value = value.replaceAll(",", "");
                    element.value = eval(value);
                }
            }
            catch (ee) { element.value = originalValue; }
        }



        function onchangeText() {
            $('#<%= btnOk.ClientID %>').attr('disabled', 'disabled');
        }
    </script>
    <style type="text/css">
        .style1
        {
            height: 20px;
        }
        table tr td
        {
            padding: 0 10px 8px 0;
        }
        div.notify
        {
            width: 510px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <%--<div id="divWarningMsg" runat="server" class='msgwarn' style='display: none'>
    Installment remaining for this deduction.Change on your own risk.</div>--%>
    <asp:HiddenField ID="hfBalance" runat="server" />
    <asp:HiddenField ID="hfRemainingInstallment" runat="server" />
    <asp:HiddenField ID="hfPaid" runat="server" />
    <div class="popupHeader">
    </div>
    <div style='width: 400!important; margin-left: 10px'>
        <uc2:WarningMsgCtl ID="warningMsg" Hide="true" runat="server" />
        <uc1:InfoMsgCtl ID="infoMsg" runat="server" Hide="true" />
    </div>
    <div class="marginal">
        <div class="bevel" style="width: 550px">
            <div class="fields paddpop" style="width: 530px!important">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="lf">
                            <asp:Label ID="lblAmountText" runat="server" Text="Label"></asp:Label>*
                        </td>
                        <td>
                            <asp:TextBox ID="txtAmount" onchange="onchangeText()" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtAmount"
                                Display="None" ErrorMessage="Amount is required." ValidationGroup="Calculation"></asp:RequiredFieldValidator>
                            <asp:CompareValidator Operator="GreaterThanEqual" ValueToCompare="0" ValidationGroup="Calculation"
                                Type="Currency" ID="CompareValidator121" Display="None" ControlToValidate="txtAmount"
                                runat="server" ErrorMessage="Invalid amount."></asp:CompareValidator>
                        </td>
                    </tr>
                    <tr runat="server" id="rowTakenOn">
                        <td class="lf">
                            Taken On*
                        </td>
                        <td>
                            <My:Calendar Id="calTakenOn" runat="server" />
                        </td>
                    </tr>
                    <tr runat="server" id="rowInterestRate">
                        <td class="lf">
                            Interest Rate
                        </td>
                        <td>
                            <asp:TextBox onchange="onchangeText()" Width="80px" ID="txtInterestRate" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtInterestRate"
                                Display="None" ErrorMessage="Interest rate is required." ValidationGroup="Calculation"></asp:RequiredFieldValidator>
                            <asp:CompareValidator Operator="GreaterThanEqual" ValueToCompare="0" ValidationGroup="Calculation"
                                Type="Double" ID="CompareValidator1" Display="None" ControlToValidate="txtInterestRate"
                                runat="server" ErrorMessage="Invalid interest rate."></asp:CompareValidator>
                            % per year
                        </td>
                    </tr>
                    <tr runat="server" id="rowMarketRate">
                        <td class="lf">
                            Market Rate
                        </td>
                        <td>
                            <asp:TextBox ID="txtMarketRate" Width="80px" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtMarketRate"
                                Display="None" ErrorMessage="Market rate is required." ValidationGroup="Calculation"></asp:RequiredFieldValidator>
                            <asp:CompareValidator Operator="GreaterThanEqual" ValueToCompare="0" ValidationGroup="Calculation"
                                Type="Double" ID="CompareValidator2" Display="None" ControlToValidate="txtMarketRate"
                                runat="server" ErrorMessage="Invalid market rate."></asp:CompareValidator>
                            % per year
                        </td>
                    </tr>
                    <tr runat="server" id="rowToBePaidOver">
                        <td class="lf" style='padding-top: 15px;'>
                            To be Paid Over
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td valign="middle">
                                        <asp:TextBox onchange="onchangeText()" Width="80px" ID="txtToBePaidOver" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="valReqdPaidOver" runat="server" ControlToValidate="txtToBePaidOver"
                                            Display="None" ErrorMessage="Paid over {0} is required." ValidationGroup="Calculation"></asp:RequiredFieldValidator>
                                        <asp:CompareValidator Operator="GreaterThan" ValueToCompare="0" ValidationGroup="Calculation"
                                            Type="Integer" ID="valCompPaidOver" Display="None" ControlToValidate="txtToBePaidOver"
                                            runat="server" ErrorMessage="Invalid paid over {0}."></asp:CompareValidator>
                                        <asp:Label ID="lblMonthsOrInstallments" runat="server" Text="Label"></asp:Label>
                                    </td>
                                    <td valign="middle">
                                        <table runat="server" id="divFixedInstallment">
                                            <tr>
                                                <td>
                                                    <asp:CheckBox onclick='setFixedInstallment(this);' Style='' runat="server" ID="chkIsFixedInstallmentAmount" Text="Fixed Installment" />
                                                </td>
                                                <td>
                                                    <asp:TextBox  CssClass="disabledTxt"  onchange="onchangeFixedInstallment()" Width="100px"
                                                        ID="txtFixedInstallmentAmount" runat="server"></asp:TextBox>
                                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtFixedInstallmentAmount"
                                                        Display="None" ErrorMessage="Fixed installment amount is required." ValidationGroup="Calculation"></asp:RequiredFieldValidator>
                                                    <asp:CompareValidator Operator="GreaterThan" ValueToCompare="0" ValidationGroup="Calculation"
                                                        Type="Currency" ID="CompareValidator3" Display="None" ControlToValidate="txtFixedInstallmentAmount"
                                                        runat="server" ErrorMessage="Invalid Fixed installment amount."></asp:CompareValidator>--%>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <%--</td>
                        <td>--%>
                        </td>
                    </tr>
                    <tr runat="server" id="rowPaymentTerms">
                        <td class="lf">
                            Payment Terms
                        </td>
                        <td>
                            <asp:DropDownList onchange="onchangeText()" ID="ddlPaymentTerms" AppendDataBoundItems="true"
                                DataTextField="Value" DataValueField="Key" runat="server">
                                <asp:ListItem Text="--Select payment term--" Value="-1"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator InitialValue="-1" ID="RequiredFieldValidator3" runat="server"
                                ControlToValidate="ddlPaymentTerms" Display="None" ErrorMessage="Please select a payment term."
                                ValidationGroup="Calculation"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr runat="server" id="rowStartingFrom">
                        <td class="lf">
                            Starting From
                        </td>
                        <td>
                            <asp:DropDownList onchange="onchangeText()" ID="ddlMonths" DataTextField="Value"
                                DataValueField="Key" runat="server">
                            </asp:DropDownList>
                            <asp:DropDownList onchange="onchangeText()" ID="ddlYears" DataTextField="Value" DataValueField="Key"
                                runat="server">
                            </asp:DropDownList>
                            <asp:TextBox Style='display: none' Text="sdf" ID="txtForEmptyValidate" runat="server" />
                            <asp:CustomValidator IsCalendar="true" ID="valCustomeToDate2" runat="server" ValidateEmptyText="true"
                                ControlToValidate="txtForEmptyValidate" ValidationGroup="Calculation" Display="None"
                                ErrorMessage="" ClientValidationFunction="validateStartingDate" />
                        </td>
                    </tr>
                </table>
                <asp:Button Style='margin-top: 10px; margin-bottom: 10px' ID="btnCalculate" OnClientClick="valGroup='Calculation';return CheckValidation()"
                    CssClass="save" runat="server" Text="Calculate" OnClick="btnCalculate_Click" />
                <table class="tableLightColor" style='margin-bottom: 10px' runat="server" id="tbl">
                    <tr>
                        <th style='width: 100px'>
                            Monthly Payment
                        </th>
                        <th style='width: 100px'>
                            Last Payment
                        </th>
                        <th style='width: 100px' runat="server" id="cellInterest1">
                            Total Interest
                        </th>
                    </tr>
                    <tr class="even">
                        <td>
                            <asp:Label ID="lblMonthlyPayment" runat="server"></asp:Label>
                        </td>
                        <td style="text-align: center">
                            <asp:Label ID="lblLastPayment" runat="server"></asp:Label>
                        </td>
                        <td style="text-align: right" runat="server" id="cellInterest2">
                            <asp:Label ID="lblTotalInterest" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="clear">
            <asp:Button ID="btnOk" Enabled="false" CssClass="update" runat="server" Text="Ok"
                ValidationGroup="Calculation" OnClientClick="valGroup='Calculation';if( CheckValidation()) okClick(); else return false;"
                OnClick="btnOk_Click" />
            &nbsp;
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="cancel" OnClientClick="window.close();return false;" />
        </div>
    </div>
</asp:Content>
