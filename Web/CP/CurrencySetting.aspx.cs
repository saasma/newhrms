﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using BLL;
using DAL;
using Utils.Web;
using Utils.Helper;
using System.Text;

namespace Web.CP
{



    public partial class CurrencySetting : BasePage
    {
        CompanyManager compMgr = new CompanyManager();



        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                Initialise();          

        }

        

        public void Initialise()
        {

            CurrencyRate rate = CommonManager.GetCurrencyRate();

            if (rate != null)
            {

                txtCurrentRate.Text = rate.CurrentRateDollar.ToString();
                txtFixedRate.Text = rate.FixedRateDollar.ToString();

            }

            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.BritishSchool || CommonManager.CompanySetting.IsAllIncomeInForeignCurrency)
                trCurrentRate.Visible = false;
        }


       
        protected void btnSave_Click1(object sender, EventArgs e)
        {
          
        }

        protected void gvw_RowEditing(object sender, GridViewEditEventArgs e)
        {
            //gvw.EditIndex = e.NewEditIndex;
            //Initialise();
        }

        protected void gvw_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {

            //string key = gvw.DataKeys[e.RowIndex]["Key"].ToString();
            //double value = double.Parse((gvw.Rows[e.RowIndex].FindControl("txt") as TextBox).Text);

            //HPLAllowanceManager.UpdateAllowanceRate(key, value);

            //gvw.EditIndex = -1;
            //Initialise();


            //divMsgCtl.InnerHtml = "Saved";
            //divMsgCtl.Hide = false;

        }

        protected void gvw_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            //gvw.EditIndex = -1;
            //Initialise();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {

                CurrencyRate company = CommonManager.GetCurrencyRate();
                if (company == null)
                    company = new CurrencyRate();
                company.FixedRateDollar = double.Parse(txtFixedRate.Text);
                company.CurrentRateDollar = double.Parse(txtCurrentRate.Text);

                CommonManager.UpdateCurrencyRate(company);


                divMsgCtl.InnerHtml = "Currency rate saved.";
                divMsgCtl.Hide = false;

                SetDefault(btnEdit1, btnCancel1, txtFixedRate);
                SetDefault(btnEdit2, btnCancel2, txtCurrentRate);

                hdn.Value = string.Format("Fixed Rate: Rs. {0}/$  Current Rate: Rs. {1}/$",
               CommonManager.GetCurrencyRate().FixedRateDollar, CommonManager.GetCurrencyRate().CurrentRateDollar);

              
            }
            else
            {
                divWarningMsg.InnerHtml = "All settings must be selected for successfull save.";
                divWarningMsg.Hide = false;
            }
        }

        void SetDefault(Button btnEdit, Button btnCancel, TextBox ddl)
        {
            btnEdit.Visible = true;
            btnCancel.Visible = false;
            ddl.Enabled = false;
        }

        protected void btnEdit1_Click(object sender, EventArgs e)
        {
            btnEdit1.Visible = false;
            btnCancel1.Visible = true;
            txtFixedRate.Enabled = true;
        }

        protected void btnCancel1_Click(object sender, EventArgs e)
        {
            btnEdit1.Visible = true;
            btnCancel1.Visible = false;
            txtFixedRate.Enabled = false;

            txtFixedRate.Text = CommonManager.GetCurrencyRate().FixedRateDollar.ToString();
           
        }

        protected void btnEdit2_Click(object sender, EventArgs e)
        {
            btnEdit2.Visible = false;
            btnCancel2.Visible = true;
            txtCurrentRate.Enabled = true;
        }

        protected void btnCancel2_Click(object sender, EventArgs e)
        {
            btnEdit2.Visible = true;
            btnCancel2.Visible = false;
            txtCurrentRate.Enabled = false;

            txtCurrentRate.Text = CommonManager.GetCurrencyRate().CurrentRateDollar.ToString();

          
        }

    

      
    }

}

