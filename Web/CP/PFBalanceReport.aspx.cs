﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using Utils.Helper;
using Utils.Web;
using BLL.Base;
using DAL;
using Utils.Base;

namespace Web
{


    public partial class PFBalanceReport : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.AdministratorTools;
            }
        }
    
        protected void Page_Load(object sender, EventArgs e)
        {
           
                AddColumns();
            //if (!IsPostBack)
            {
              
                BindEmployees();

            }
        }



        private void AddColumns()
        {
            List<GetPFCITBalanceReportHeaderResult> headers = PFCITManager.GetPFHeader();

            for (int i = 0; i < headers.Count; i++)
            {
                TemplateField field = new TemplateField();
                field.ItemTemplate = new PFBalanceReportTemplate(DataControlRowType.DataRow, headers[i]);
                field.HeaderTemplate = new PFBalanceReportTemplate(DataControlRowType.Header, headers[i]);
                field.ItemStyle.HorizontalAlign = HorizontalAlign.Right;
                gvEmployeeIncome.Columns.Add(field);
                //gvEmployeeIncome.Columns.Insert(gvEmployeeIncome.Columns.Count - 2, field);
            }
        }


        void BindEmployees()
        {
            string EmpName = "";
            EmpName = txtEmp.Text;
            gvEmployeeIncome.DataSource = PFCITManager.GetEmployeeForPFCIT(EmpName.Trim());
            gvEmployeeIncome.DataBind();
            
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            GridViewExportUtil.Export("PFBalanceReport.xls", gvEmployeeIncome);
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindEmployees();
        }
    }

    public class PFBalanceReportTemplate : ITemplate
    {
        private GetPFCITBalanceReportHeaderResult header; 
        private DataControlRowType rowType;
        private string list = "";

        List<GetAllPFContributionResult> innerGridList = new List<GetAllPFContributionResult>();
        public List<GetAllPFContributionResult> InnerGridList
        {
            get
            {
                return innerGridList;
            }
            set
            {
                innerGridList = value;
            }
        }

        public PFBalanceReportTemplate(DataControlRowType rowType, GetPFCITBalanceReportHeaderResult header)
        {
            this.header = header;
            this.rowType = rowType;

            InnerGridList = PFCITManager.BindInnerGridListForPF();
        }


        public void InstantiateIn(Control container)
        {
            switch (rowType)
            {
                case DataControlRowType.Header:

                    Literal lblHeader = new Literal();
                    lblHeader.Text = header.HeaderName;
              

                    container.Controls.Add(lblHeader);

                    break;

                case DataControlRowType.DataRow:

                    Literal lbl = new Literal();                  
                    lbl.DataBinding += new EventHandler(lbl_Binding);
                    container.Controls.Add(lbl);
                    
                    break;
            }
        }

       
        public void lbl_Binding(object sender, EventArgs e)
        {
            GridViewRow row = ((GridViewRow)(((Control)(sender)).NamingContainer));

            GetEmployeeForPFCITResult dataSource = (GetEmployeeForPFCITResult)row.DataItem;
            Literal lbl = (Literal)sender;

            string value = GetCellValueForPFGrid(dataSource.EmployeeId, header.SourceId, header.HeaderName);
            // dataSource.EmployeeId.ToString() + " , " + header.SourceId.ToString(); //dataSource.GetValue(header);     //header.SourceId.ToString(); // "0";// dataSource.GetValue(header);


            lbl.Text = value;
            
            
            /*
            if (!string.IsNullOrEmpty(value) && (value == "Y" || value=="Ye" ))
                lbl.Text = "Yes";
            else if (!string.IsNullOrEmpty(value) && !value.Contains("%") && value != "Yes")
                */
             //   lbl.Text = BaseHelper.GetCurrency(value, SessionManager.DecimalPlaces);
            //else
            //    lbl.Text = value;

        }


        public string GetCellValueForPFGrid(int empID, int yearID, string HeaderName)
        {
            
           
            if(InnerGridList.Any(x=>x.EmployeeId == empID && x.FinancialDateId== yearID))
            {
                if (HeaderName.StartsWith("Contribution"))
                {
                    //return BaseHelper.GetCurrency(GetPFAmount(empID, YearID), SessionManager.DecimalPlaces);
                    decimal PFTotal = 0;
                    if (InnerGridList.FirstOrDefault(x => x.EmployeeId == empID && x.FinancialDateId == yearID).PFTotal != null)
                    {
                        PFTotal = InnerGridList.FirstOrDefault(x => x.EmployeeId == empID && x.FinancialDateId == yearID).PFTotal.Value;
                    }
                    return BaseHelper.GetCurrency(PFTotal, SessionManager.DecimalPlaces);
                }
                else if (HeaderName.StartsWith("Interest"))
                {
                    //return BaseHelper.GetCurrency(GetPFInterest(empID, YearID), SessionManager.DecimalPlaces);
                    decimal PFInterest = 0;
                    if (InnerGridList.FirstOrDefault(x => x.EmployeeId == empID && x.FinancialDateId == yearID).PFInterest != null)
                    {
                        PFInterest = InnerGridList.FirstOrDefault(x => x.EmployeeId == empID && x.FinancialDateId == yearID).PFInterest.Value;
                    }
                    //return CITTotal.ToString();
                    return BaseHelper.GetCurrency(PFInterest, SessionManager.DecimalPlaces);
                }
            }

            return BaseHelper.GetCurrency(0, SessionManager.DecimalPlaces);
            //return "";
        }
        
    }
}
