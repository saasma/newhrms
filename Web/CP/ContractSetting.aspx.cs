﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using BLL;
using DAL;
using Utils.Web;
using Utils.Helper;
using System.Text;
using Utils.Calendar;

namespace Web.CP
{



    public partial class ContractSetting : BasePage
    {
        CompanyManager compMgr = new CompanyManager();

        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.AdministratorTools;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                Initialise();          

        }
        protected void btnEdit1_Click(object sender, EventArgs e)
        {
            btnEdit1.Visible = false;
            btnCancel1.Visible = true;
            txtContractDay.Enabled = true;
        }

        protected void btnCancel1_Click(object sender, EventArgs e)
        {
            btnEdit1.Visible = true;
            btnCancel1.Visible = false;
            txtContractDay.Enabled = false;

           
        }

        public void Initialise()
        {


            Setting setting = CalculationManager.GetSetting();


            if (setting != null && setting.ContractUptoDay != null)
                txtContractDay.Text = setting.ContractUptoDay.ToString();

            //if attendancce already saved for the company then hide this button


            //btnEdit1.Visible =! LeaveAttendanceManager.IsAttendanceSavedForCompany();
        }


       
        protected void btnSave_Click1(object sender, EventArgs e)
        {
          
        }

        protected void gvw_RowEditing(object sender, GridViewEditEventArgs e)
        {
            //gvw.EditIndex = e.NewEditIndex;
            //Initialise();
        }

        protected void gvw_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {

            //string key = gvw.DataKeys[e.RowIndex]["Key"].ToString();
            //double value = double.Parse((gvw.Rows[e.RowIndex].FindControl("txt") as TextBox).Text);

            //HPLAllowanceManager.UpdateAllowanceRate(key, value);

            //gvw.EditIndex = -1;
            //Initialise();


            //divMsgCtl.InnerHtml = "Saved";
            //divMsgCtl.Hide = false;

        }

        protected void gvw_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            //gvw.EditIndex = -1;
            //Initialise();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {

                Setting setting = CalculationManager.GetSetting();


                //#region "Change Logs"
                //if( company.LeaveStartMonth.ToString() != ddlLeaveStartMonth.SelectedValue)
                //    BLL.BaseBiz.PayrollDataContext.ChangeSettings.InsertOnSubmit(BLL.BaseBiz.GetSettingChangeLog(
                //        (byte)MonetoryChangeLogTypeEnum.Leave,"Leave Start Month",
                //         DateHelper.GetMonthName( company.LeaveStartMonth.Value,IsEnglish),
                //         DateHelper.GetMonthName(int.Parse(ddlLeaveStartMonth.SelectedValue), IsEnglish), LogActionEnum.Update));

                //if (company.LeaveEncashTypePaysOn != null)
                //{
                //    if (company.LeaveEncashTypePaysOn.ToString() != ddlEncashLeavePaysOn.SelectedValue)
                //        BLL.BaseBiz.PayrollDataContext.ChangeSettings.InsertOnSubmit(BLL.BaseBiz.GetSettingChangeLog(
                //            (byte)MonetoryChangeLogTypeEnum.Leave, "Encash Pays On",
                //            DateHelper.GetMonthName(company.LeaveEncashTypePaysOn.Value, IsEnglish),
                //            DateHelper.GetMonthName(int.Parse(ddlEncashLeavePaysOn.SelectedValue), IsEnglish), LogActionEnum.Update));
                //}

                //if (company.LeaveLapseEncashTypePaysOn != null)
                //{
                //    if (company.LeaveLapseEncashTypePaysOn.ToString() != ddlLapseEncashLeavePaysOn.SelectedValue)
                //        BLL.BaseBiz.PayrollDataContext.ChangeSettings.InsertOnSubmit(BLL.BaseBiz.GetSettingChangeLog(
                //            (byte)MonetoryChangeLogTypeEnum.Leave, "Encash Lapse Pays On",
                //             DateHelper.GetMonthName(company.LeaveLapseEncashTypePaysOn.Value, IsEnglish),
                //             DateHelper.GetMonthName(int.Parse(ddlLapseEncashLeavePaysOn.SelectedValue), IsEnglish), LogActionEnum.Update));
                //}
                //#endregion

                setting.ContractUptoDay = int.Parse(txtContractDay.Text.Trim());



                CalculationManager.SaveUpdateSetting(setting);

                divMsgCtl.InnerHtml = "Setting updated.";
                divMsgCtl.Hide = false;

                SetDefault(btnEdit1, btnCancel1, txtContractDay);

            }
            else
            {
                divWarningMsg.InnerHtml = "All settings must be selected for successfull save.";
                divWarningMsg.Hide = false;
            }
        }

        void SetDefault(Button btnEdit, Button btnCancel, DropDownList ddl)
        {
            btnEdit.Visible = true;
            btnCancel.Visible = false;
            ddl.Enabled = false;
        }
        void SetDefault(Button btnEdit, Button btnCancel, TextBox ddl)
        {
            btnEdit.Visible = true;
            btnCancel.Visible = false;
            ddl.Enabled = false;
        }
   

    }

}

