<%@ Page Title="Optimum PF and CIT" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="OptimumPFAndCIT.aspx.cs" Inherits="Web.CP.OptimumPFAndCIT" %>

<%@ Register Src="~/Controls/FilterCtl.ascx" TagName="FilterCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/CalendarCtl.ascx" TagPrefix="uc" TagName="Calendar" %>
<%@ Register Src="../Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="../Scripts/jquery-1.3.2.min.js"></script>
    <script src="../Scripts/currency.js" type="text/javascript"></script>
    <script type="text/javascript">




        $(document).ready(function () {


        });


        function selectDeselectAll(chk) {
            $('input[type=checkbox]').each(
                    function (index) {
                        if ($(this).prop('disabled') == false && this.id.indexOf('chkApply') > 0)
                            this.checked = chk.checked;
                    }
                );
        }
        
    </script>
    <style type="text/css">
        .increment
        {
        }
        .tableLightColor a:hover
        {
            color: #048FC2;
            text-decoration: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <asp:HiddenField ID="hdIncreaseType" runat="server" />
    <asp:HiddenField ID="hdIncomeType" runat="server" />
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Optimum CIT
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <uc2:FilterCtl Designation="false" WorkShift="false" SearchEmployeeTextboxAutoPostback="false"
            Id="filterCtl" OnFilterChanged="ReBindEmployees" EmployeeName="true" CITRoundOff="false"
            runat="server" />
        <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
        <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
        <div class="clear" style="margin-top: 10px; margin-bottom: 0px;">
            <cc2:EmptyDisplayGridView CssClass="table table-primary mb30 table-bordered table-hover" style="margin-bottom:0px" UseAccessibleHeader="true" DataKeyNames="OptimumId,EID,PayrollPeriodId,AdjustedPF,AdjustedCIT"
                GridLines="None" ID="gvwEmployeeList" runat="server" ShowFooterWhenEmpty="False"
                ShowHeaderWhenEmpty="True" AutoGenerateColumns="False" OnPageIndexChanging="gvwDepartments_PageIndexChanging">
                <Columns>
                    <asp:BoundField DataField="EID" HeaderText="EIN" ItemStyle-HorizontalAlign="Right"
                        HeaderStyle-Width="30px" />
                     <asp:BoundField DataField="IdCardNo" HeaderText="I No" ItemStyle-HorizontalAlign="Right"
                        HeaderStyle-Width="60px" />
                    <asp:TemplateField HeaderText="&nbsp;Name" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:HyperLink CssClass="employeeClass" Target="_blank" Width="150px" ID="Label2"
                                runat="server" NavigateUrl='<%# "~/newhr/EmployeePayroll.aspx?Id=" +  Eval("EID") %>'
                                Text='<%# "&nbsp;" + Eval("Name") %>'></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="ReferencePayroll" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="80px"
                        HeaderText="Ref Month" />
                    <asp:TemplateField HeaderText="Employee Monthy PF" ItemStyle-HorizontalAlign="Right"
                        HeaderStyle-Width="70px">
                        <ItemTemplate>
                            <asp:Label ID="lblEmpPF" runat="server" Text='<%# Eval("EmployeePFAmount","{0:N2}") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Employee Monthy CIT" ItemStyle-HorizontalAlign="Right"
                        HeaderStyle-Width="70px">
                        <ItemTemplate>
                            <asp:Label ID="lblCIT" runat="server" Text='<%# Eval("CITAmount","{0:N2}") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Yearly Forecasted Income" ItemStyle-HorizontalAlign="Right"
                        HeaderStyle-Width="80px">
                        <ItemTemplate>
                            <asp:Label ID="lblYearlyForecast" runat="server" Text='<%# Eval("YearlyForecast","{0:N2}") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="1/3 of Yearly Income" ItemStyle-HorizontalAlign="Right"
                        HeaderStyle-Width="80px">
                        <ItemTemplate>
                            <asp:Label ID="lblOneThird" runat="server" Text='<%# Eval("OneThird","{0:N2}") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Current Actual Retirement Fund" ItemStyle-HorizontalAlign="Right"
                        HeaderStyle-Width="80px">
                        <ItemTemplate>
                            <asp:Label ID="lblActual" runat="server" Text='<%# Eval("Actual","{0:N2}") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="CIT Provision" ItemStyle-HorizontalAlign="Right" HeaderStyle-Width="80px">
                        <ItemTemplate>
                            <asp:Label ID="lblProvision" runat="server" Text='<%# Eval("PrivisionalCIT","{0:N2}") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Max Allowed" ItemStyle-HorizontalAlign="Right" HeaderStyle-Width="80px">
                        <ItemTemplate>
                            <asp:Label ID="lblAllowed" runat="server" Text='<%# Eval("Allowed","{0:N2}") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Possible Additional CIT" ItemStyle-HorizontalAlign="Right"
                        HeaderStyle-Width="80px">
                        <ItemTemplate>
                            <asp:Label ID="lblAdditional" runat="server" Text='<%# Eval("AdditionalCIT","{0:N2}") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Remaining Months" ItemStyle-HorizontalAlign="Right"
                        HeaderStyle-Width="80px">
                        <ItemTemplate>
                            <asp:Label ID="lblRem" runat="server" Text='<%# Eval("RemainingMonths","{0:N2}") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <%-- <asp:TemplateField HeaderText="Opt Emp PF" ItemStyle-HorizontalAlign="Right" HeaderStyle-Width="80px">
                        <ItemTemplate>
                            <asp:Label ID="lblOptPF" runat="server" Text='<%# Eval("OptimumPF","{0:N2}") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>--%>
                    <%--<asp:TemplateField HeaderText="Adjusted PF" ItemStyle-HorizontalAlign="Right" HeaderStyle-Width="80px">
                        <ItemTemplate>
                            <asp:TextBox ID="txtAdjustedPF" Width="80" Style='text-align: right' runat="server"
                                Text='<%# Eval("AdjustedPF","{0:N2}") %>' />
                            <asp:RequiredFieldValidator SetFocusOnError="true" Display="None" ID="val2" ControlToValidate="txtAdjustedPF"
                                ValidationGroup="Balance" runat="server" ErrorMessage="Adjusted PF is required." />
                            <asp:CompareValidator SetFocusOnError="true" ValueToCompare="0" Display="None" Operator="GreaterThanEqual"
                                Type="Currency" ID="valOpeningBalance1" ControlToValidate="txtAdjustedPF" ValidationGroup="Balance"
                                runat="server" ErrorMessage="Invalid adjusted PF."></asp:CompareValidator>
                        </ItemTemplate>
                    </asp:TemplateField>--%>
                    <asp:TemplateField HeaderText="Additional Monthly CIT" ItemStyle-HorizontalAlign="Right"
                        HeaderStyle-Width="80px">
                        <ItemTemplate>
                            <asp:Label ID="lblOptCIT" runat="server" Text='<%# Eval("OptimumCIT","{0:N2}") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Total Opt CIT" ItemStyle-HorizontalAlign="Right" Visible="false"
                        HeaderStyle-Width="80px">
                        <ItemTemplate>
                            <asp:Label ID="lblTotalOptCIT" runat="server" Text='<%# Eval("TotalOptimumCIT","{0:N2}") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Adjusted CIT" Visible="false" ItemStyle-HorizontalAlign="Right"
                        HeaderStyle-Width="80px">
                        <ItemTemplate>
                            <asp:TextBox ID="txtAdjustedCIT" ReadOnly="true" CssClass="calculationInput disabled"
                                Width="80" Style='text-align: right' runat="server" Text='<%# Eval("AdjustedCIT","{0:N2}") %>' />
                            <asp:RequiredFieldValidator SetFocusOnError="true" Display="None" ID="RequiredFieldValidator1"
                                ControlToValidate="txtAdjustedCIT" ValidationGroup="Balance" runat="server" ErrorMessage="Adjusted CIT is required." />
                            <asp:CompareValidator SetFocusOnError="true" ValueToCompare="0" Display="None" Operator="DataTypeCheck"
                                Type="Currency" ID="CompareValidator1" ControlToValidate="txtAdjustedCIT" ValidationGroup="Balance"
                                runat="server" ErrorMessage="Invalid adjusted CIT."></asp:CompareValidator>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Total Monthly Optimum CIT" ItemStyle-HorizontalAlign="Right"
                        Visible="false" HeaderStyle-Width="80px">
                        <ItemTemplate>
                            <asp:Label ID="lblTotalMonthlyOptimumCIT" runat="server" Text='<%# Eval("TotalOptimumCIT","{0:N2}") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Apply" HeaderStyle-Width="20px">
                        <ItemTemplate>
                            <asp:CheckBox ID="chkApply" runat="server" Checked='<%# ((int) Eval("OptimumId")) == 0 ? false : true %>' />
                        </ItemTemplate>
                        <HeaderTemplate>
                            Apply
                            <asp:CheckBox ID="chkSelect" onclick="selectDeselectAll(this)" runat="server" />
                        </HeaderTemplate>
                    </asp:TemplateField>
                </Columns>
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                <EmptyDataTemplate>
                    <b>No Employees are found</b>
                </EmptyDataTemplate>
            </cc2:EmptyDisplayGridView>
            <uc1:PagingCtl ID="pagingCtl" ShowDropDown="true" OnChangePage="ChangePageNumber"
                OnNextRecord="btnNext_Click" OnPrevRecord="btnPrevious_Click" OnDropDownShowRecordsChanged="ddlRecords_SelectedIndexChanged"
                runat="server" />
        </div>
        <div class="buttonsDiv">
            <asp:LinkButton ID="btnExport" runat="server" Text="Export to Excel" OnClick="btnExport_Click"
                CssClass=" excel marginRight" Style='float: right; margin-right: 0px' />
            <asp:Button ID="btnSave" OnClientClick="valGroup='Balance';if(CheckValidation())  return ValidateIncrement();"
                CssClass="btn btn-warning btn-sect btn-sm" runat="server" Text="Save Changes" ValidationGroup="Balance" OnClick="btnSave_Click" />
            <%--    <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Balance" DisplayMode="List"
                ShowSummary="false" ShowMessageBox="true" runat="server"></asp:ValidationSummary>--%>
        </div>
    </div>
</asp:Content>
