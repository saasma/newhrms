﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Manager;
using DAL;
using Utils.Helper;
using Utils.Web;
using System.Text;
using BLL;
using System.Text.RegularExpressions;

namespace Web.CP
{
    public partial class AAForwardOvertimePopup : System.Web.UI.Page
    {
        InsuranceManager insMgr = new InsuranceManager();
        List<IInsuranceInstitution> source = new List<IInsuranceInstitution>();
        public bool IsDisplayedAsPopup
        {
            get
            {
                if (Request.QueryString["isPopup"] != null)
                    return true;
                return false;
            }
        }

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                Initialise();

        }

        void Initialise()
        {
            ddlOvertimeType.DataSource = OvertimeManager.GetOvertimeList();
            ddlOvertimeType.DataBind();

            Hidden_RequestID.Value = Request.QueryString["reqid"];
            Load();

            if (OvertimeManager.IsRequestAutoGroupType)
            {
                rowRecommender.Visible = false;
                colMin.Visible = false;
            }
        }

        new void Load()
        {
            string hr;
            string min;
            string hrmin;
            string employeename;


            OvertimeRequest request = new OvertimeRequest();
            request = OvertimeManager.getRequestByID(Hidden_RequestID.Value);
            if (request.StartTime != null)
                min = (request.StartTime.Value.Subtract(request.EndTime.Value).TotalMinutes).ToString();
            else
                min = "0";
            Hidden_OldMinute.Value = min;
            employeename = EmployeeManager.GetEmployeeById(request.EmployeeID.Value).Name;

            title.InnerHtml += " " + employeename;

            lblDate.Text = request.Date.Value.ToShortDateString();
            lblName.Text = employeename;
            lblReason.Text = request.Reason;
            DateTime? date = null;
            if (AttendanceManager.GetEmployeeCheckInOutTime(request.EmployeeID.Value, request.Date.Value, "CheckIn") != null)
            {
                date = AttendanceManager.GetEmployeeCheckInOutTime(request.EmployeeID.Value, request.Date.Value, "CheckIn");
                if (date != null)
                    lblInTime.Text = date.Value.ToShortTimeString();
            }

            if (AttendanceManager.GetEmployeeCheckInOutTime(request.EmployeeID.Value, request.Date.Value, "CheckOut") != null)
            {
                date = AttendanceManager.GetEmployeeCheckInOutTime(request.EmployeeID.Value, request.Date.Value, "CheckOut");
                if (date != null)
                    lblOutTime.Text = date.Value.ToShortTimeString();
            }


            if (request.OvertimeTypeId != null)
            {
                UIHelper.SetSelectedInDropDown(ddlOvertimeType, request.OvertimeTypeId.Value);
                gridHistory.DataSource = AllowanceManager.GetAllowanceHistory(request.OvertimeRequestID, (int)RequestHistoryTypeEnum.Overtime);
                gridHistory.DataBind();
             
            }

           
            if (request.RecommendedBy != null)
                lblRecommender.Text += EmployeeManager.GetEmployeeName(request.RecommendedBy.Value);

            if (request.ApprovalID != null)
                lblApproval.Text = EmployeeManager.GetEmployeeName(request.ApprovalID.Value);

            lblStatus.Text = ((OvertimeStatusEnum)(request.Status)).ToString();

            int minute = Math.Abs(int.Parse(min));
            
            if (request.RequestedTimeInMin != null)
                minute = request.RequestedTimeInMin.Value;

            hrmin = new AttendanceManager().MinuteToHourMinuteConverter(minute);


            txtRequested.Text = hrmin;
            Hidden_DurationPrev.Value = hrmin;

            if (request.OvertimeTypeId != null)
            {
                DAL.OvertimeType overtimetype = OvertimeManager.GetOvertimeType(request.OvertimeTypeId.Value);
                if (overtimetype != null)
                {
                    Hidden_OvertimeTypePrev.Value = overtimetype.Name;
                }
            }

            hr = hrmin.Split(':').First();
            min = hrmin.Split(':').Last();


            if (request.ApprovedTimeInMinute != null)
            {
                var span = System.TimeSpan.FromMinutes(request.ApprovedTimeInMinute.Value);
                var hours = ((int)span.TotalHours).ToString();
                var minutes = span.Minutes.ToString();

                txtHour.Text = hours;
                txtMinute.Text = minutes;
                
            }
            else
            {
                txtHour.Text = Regex.Match(hr, @"\d+").Value;
                txtMinute.Text = Regex.Match(min, @"\d+").Value;
            }

          

        }






        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "window.onunload = null;");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"), true);


            //output all as js array to be updatable in parent window
            //if (IsDisplayedAsPopup && source != null)
            //{
            //    //Page.ClientScript.
            //    StringBuilder str = new StringBuilder("");
            //    bool first = true;
            //    foreach (IInsuranceInstitution obj in source)
            //    {
            //        if (first == false)
            //            str.Append(",");
            //        str.Append("'" + obj.Id + "$$" + obj.InstitutionName + "'");
            //        first = false;
            //    }
            //    Page.ClientScript.RegisterArrayDeclaration("texts", str.ToString());
            //}
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            Page.Validate("SaveUpdate");
            if (Page.IsValid)
            {
                OvertimeRequest requestInstance = new OvertimeRequest();
                List<RequestEditHistory> edits = new List<RequestEditHistory>();
                RequestEditHistory editHist = new RequestEditHistory();

                editHist.RequestType = (int)RequestHistoryTypeEnum.Overtime;
                requestInstance.OvertimeRequestID = int.Parse(Hidden_RequestID.Value);
                requestInstance.OvertimeTypeId = int.Parse(ddlOvertimeType.SelectedValue);
                requestInstance.ApprovedTimeInMinute = (int.Parse(txtHour.Text) * 60) + int.Parse(txtMinute.Text);
                requestInstance.ApprovedTime = txtHour.Text + ":" + txtMinute.Text + "";

                double totalHour = TimeSpan.FromMinutes(requestInstance.ApprovedTimeInMinute.Value).TotalHours;
                if (totalHour >= 24)
                {
                    JavascriptHelper.DisplayClientMsg("Approval hour should be under 24 hrs, current total hour is "
                         + totalHour.ToString("n2") + ".", this.Page);
                    return;
                }

                if (Hidden_OvertimeTypePrev.Value.Trim().ToLower() != ddlOvertimeType.SelectedItem.Text.Trim().ToLower())
                {
                    editHist = new RequestEditHistory();
                    editHist.Column1Before = Hidden_OvertimeTypePrev.Value;
                    editHist.Column1After = ddlOvertimeType.SelectedItem.Text;
                    edits.Add(editHist);

                    Hidden_OvertimeTypePrev.Value = ddlOvertimeType.SelectedItem.Text;
                }

                if (Hidden_DurationPrev.Value.Trim().ToLower() != requestInstance.ApprovedTime.Trim().ToLower())
                {
                    editHist = new RequestEditHistory();
                    editHist.Column1Before = Hidden_DurationPrev.Value;
                    editHist.Column1After = requestInstance.ApprovedTime;
                    edits.Add(editHist);
                    Hidden_DurationPrev.Value = requestInstance.ApprovedTime;

                }


                Status status = OvertimeManager.UpdateRequestFromHR(requestInstance, edits);
               
                if (status.IsSuccess)
                {
                    this.HasImport = true;
                    JavascriptHelper.DisplayClientMsg("Overtime has been updated.", this, "closePopup();\n");
                }

                else
                {
                    divWarningMsg.InnerHtml = status.ErrorMessage;
                    divWarningMsg.Hide = false;
                }
                 
                
            }
            else
            {
                JavascriptHelper.DisplayClientMsg("Validation Failed", this);
            }
        }

        protected void btnReject_Click(object sender, EventArgs e)
        {
            Page.Validate("SaveUpdate");
            if (Page.IsValid)
            {
                OvertimeRequest requestInstance = new OvertimeRequest();
               

                requestInstance.OvertimeRequestID = int.Parse(Hidden_RequestID.Value);


                Status status = OvertimeManager.RejectOTRequestFromHR(requestInstance);

                if (status.IsSuccess)
                {
                    this.HasImport = true;
                    JavascriptHelper.DisplayClientMsg("Overtime has been rejected.", this, "closePopup();\n");
                }

                else
                {
                    divWarningMsg.InnerHtml = status.ErrorMessage;
                    divWarningMsg.Hide = false;
                }


            }
            else
            {
                JavascriptHelper.DisplayClientMsg("Validation Failed", this);
            }
        }
    }
}
