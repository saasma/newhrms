﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;
using System.Text;

namespace Web.CP
{
    public partial class AwardList : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.AdministratorTools;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        private void Initialise()
        {
            cmbShowAwardIn.Store[0].DataSource = AwardManager.GetAllIncomeHead();
            cmbShowAwardIn.Store[0].DataBind();

            BindGrid();
        }

        private void ClearFields()
        {
            txtAwardName.Text = "";
            cmbShowAwardIn.ClearValue();
        }

        private void BindGrid()
        {
            gridAward.Store[0].DataSource = AwardManager.GetAllEmployeeCashAwardType();
            gridAward.Store[0].DataBind();
        }

        protected void btnAddNew_Click(object sender, DirectEventArgs e)
        {
            ClearFields();
            hdnAwardId.Text = "";
            WAward.Show();
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveUpdateAward");
            if (Page.IsValid)
            {
                EmployeeCashAwardType obj = new EmployeeCashAwardType();

                if (!string.IsNullOrEmpty(hdnAwardId.Text))
                    obj.CashAwardTypeId = int.Parse(hdnAwardId.Text);

                obj.Name = txtAwardName.Text.Trim();
                obj.IncomeId = int.Parse(cmbShowAwardIn.SelectedItem.Value);

                Status status = AwardManager.SaveUpdateEmployeeCashAwardType(obj);
                if (status.IsSuccess)
                {
                    WAward.Close();
                    BindGrid();
                    if(!string.IsNullOrEmpty(hdnAwardId.Text))
                        NewMessage.ShowNormalMessage("Record updated successfully.");
                    else
                        NewMessage.ShowNormalMessage("Record saved successfully.");
                }
                else
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        protected void btnEdit_Click(object sender, DirectEventArgs e)
        {
            ClearFields();
            int cashAwardTypeId = int.Parse(hdnAwardId.Text);

            EmployeeCashAwardType obj = AwardManager.GetEmployeeCashAwardTypeById(cashAwardTypeId);
            if (obj != null)
            {
                txtAwardName.Text = obj.Name;
                cmbShowAwardIn.SetValue(obj.IncomeId.Value);
                WAward.Show();
            }
        }

        protected void btnDelete_Click(object sender, DirectEventArgs e)
        {
            int cashAwardTypeId = int.Parse(hdnAwardId.Text);

            Status status = AwardManager.DeleteEmployeeCashAwardType(cashAwardTypeId);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Record deleted successfully.");
                BindGrid();
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }

        }

    }
}