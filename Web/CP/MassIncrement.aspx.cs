﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Manager;
using BLL;
using DAL;

using Utils.Helper;
using BLL.Base;
using Utils.Calendar;

namespace Web.CP
{
    public partial class MassIncrement : BasePage
    {
        public static int companyId = -1;
        #region "Control state related"
        private string _sortBy = "EmployeeId";
        private SortDirection _sortDirection = SortDirection.Ascending;

        protected override void OnInit(EventArgs e)
        {
            Page.RegisterRequiresControlState(this);
            base.OnInit(e);
        }
        protected override void LoadControlState(object savedState)
        {
            object[] rgState = (object[])savedState;
            base.LoadControlState(rgState[0]);
            _sortBy = rgState[1].ToString();
            _sortDirection = (SortDirection)rgState[2];
            //_tempCurrentPage = (int)rgState[2];

        }
        protected override object SaveControlState()
        {
            object[] rgState = new object[3];
            rgState[0] = base.SaveControlState();
            rgState[1] = _sortBy;
            rgState[2] = _sortDirection;
            // rgState[2] = _tempCurrentPage;
            return rgState;
        }
        protected void gvwEmployees_Sorting(object sender, GridViewSortEventArgs e)
        {

            if (e.SortExpression != _sortBy)
            {
                e.SortDirection = SortDirection.Ascending;
                _sortDirection = e.SortDirection;
            }
            else if (_sortDirection == SortDirection.Ascending)
            {
                e.SortDirection = SortDirection.Descending;
            }
            else if (_sortDirection == SortDirection.Descending)
            {
                e.SortDirection = SortDirection.Ascending;
            }

            _sortDirection = e.SortDirection;

            _sortBy = e.SortExpression;


            e.Cancel = true;


            pagintCtl.CurrentPage = 1;
            //_tempCurrentPage = 1;
            BindEmployee();
        }
        #endregion
        List<KeyValue> statusList = new JobStatus().GetMembers();
        List<HR_Service_Event> eventList = new List<HR_Service_Event>();

        protected void Page_PreRender(object sender, EventArgs e)
        {

            if (ddlIncome.SelectedValue != "-1")
            {
                PIncome inc = new PayManager().GetIncomeById(int.Parse(ddlIncome.SelectedValue));
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(),"sdfdsfdsf",
                    "incomeTypeIsRate = " + (inc.Calculation==IncomeCalculation.PERCENT_OF_INCOME ? "true" : "false"),true);
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            RegisterJsForPopUp();

            eventList = CommonManager.GetServieEventTypes();

            if (!IsPostBack)
            {
                if (CommonManager.CompanySetting.WhichCompany == WhichCompany.NIBL)
                {
                    btnExportPopup.Visible = false;
                    btnImportPopup.Visible = false;
                }

                pagintCtl.Visible = false;
                Calendar1.IsEnglishCalendar = IsEnglish; 
                Calendar1.SelectTodayDate();
                PayrollPeriod payrollPeriod = CommonManager.GetValidLastPayrollPeriod();
                if (payrollPeriod != null)
                {
                    Calendar1.SetSelectedDate(payrollPeriod.StartDate, IsEnglish);
                }
                

                //incomeId = -1;
                //statusId = -1;
                //gradeId = -1;
                //designationId = -1;
                companyId = SessionManager.CurrentCompanyId;
                ddlIncome.DataSource = PayManager.GetIncomeListForMassIncrement(SessionManager.CurrentCompanyId);
                ddlIncome.DataBind();

                ddlFilterIncomes.DataSource = ddlIncome.DataSource;
                ddlFilterIncomes.DataBind();

                JobStatus jobstatus = new JobStatus();
                List<KeyValue> list = jobstatus.GetMembers();
                list.RemoveAt(0);
                ddlStatus.DataSource = list;
                ddlStatus.DataBind();

                CommonManager commonmanager = new CommonManager();
                ddlGrade.DataSource = commonmanager.GetAllGrades();
                ddlGrade.DataBind();

                ddlDesignation.DataSource = commonmanager.GetAllDesignations();
                ddlDesignation.DataBind();

                ddlLevel.DataSource = NewPayrollManager.GetAllParentLevelList()
                    .OrderBy(x => x.Name).ToList();
                ddlLevel.DataBind();

                ddlGroups.DataSource = NewPayrollManager.GetLevelGroup();
                ddlGroups.DataBind();

                ddlDepartments.DataSource = new DepartmentManager().GetDepartmentsByCompany(SessionManager.CurrentCompanyId);
                ddlDepartments.DataBind();

                ddlEvents.DataSource = CommonManager.GetServieEventTypes();
                ddlEvents.DataBind();

                ddlFilterEvents.DataSource = CommonManager.GetServieEventTypes();
                ddlFilterEvents.DataBind();

            }

            string eventTarget = Request.Form["__EVENTTARGET"];
            if (eventTarget != null && eventTarget.Equals("Reload"))
            {
                ReloadFromSession();
            }
            if (eventTarget != null && eventTarget.Equals("LoadButton"))
            {
                btnLoad_Click(null, null);
            }
            IsIncrementValid();
            JavascriptHelper.AttachNonDialogPopUpCode(Page, "importPopup", "../ExcelWindow/IncrementedSalary.aspx", 450, 500);
            JavascriptHelper.AttachNonDialogPopUpCode(Page, "exportimportPopup", "../ExcelWindow/MassIncrementSalary.aspx", 450, 500);

        }

        public string GetEventName(int type)
        {
            if (type == 0)
                return "";
            HR_Service_Event eve = eventList.FirstOrDefault(x => x.EventID == type);
            if (eve == null)
                return "";
            return eve.Name;
        }

        //protected void txtEmpSearchText_TextChanged(object sender, EventArgs e)
        //{
        //    _tempCurrentPage = 1;
        //    LoadAttedance();
        //}


        public void ReloadFromSession()
        {
            if (Session["IncrementList"] != null)
            {
                List<GetEmployeeListForMassIncrementResult> list =
               Session["IncrementList"] as List<GetEmployeeListForMassIncrementResult>;
                if (list != null)
                {
                    int currentRowEmpId = 0;
                    decimal existingAmount = 0;
                    decimal newIncome = 0;
                    decimal incrementAmount = 0;
                    foreach (GridViewRow row in gvMassIncrement.Rows)
                    {
                        currentRowEmpId = (int)gvMassIncrement.DataKeys[row.RowIndex][0];
                        if (currentRowEmpId != 0)
                        {
                            GetEmployeeListForMassIncrementResult increment = 
                                list.SingleOrDefault(x => x.EmployeeId == currentRowEmpId);

                            if (increment != null)
                            {
                                TextBox txtNewIncome = (TextBox)row.FindControl("txtNewIncome");
                                TextBox txtExistingIncome = row.FindControl("txtExistingIncome") as TextBox;
                                TextBox txtIncreasedBy = (TextBox)row.FindControl("txtIncreasedBy");

                                decimal.TryParse(txtExistingIncome.Text.Trim(), out existingAmount);


                                txtIncreasedBy.Text = GetCurrency(increment.IncrementIncome);
                                if (Convert.ToDecimal(increment.IncrementIncome)==0)
                                    txtNewIncome.Text = "0.00";
                                else
                                    txtNewIncome.Text = GetCurrency(existingAmount + Convert.ToDecimal(increment.IncrementIncome));

                            }
                        }
                    }

                    divWarningMsg.InnerHtml = "Besure to save the increment using Save button.";
                    divWarningMsg.Hide = false;

                }
            }
        }

        public void btnExportPopup_Click(object sender, EventArgs e)
        {
            int? _tempCount = 0;
            PayrollPeriod validPayrollPeriod = CommonManager.GetValidLastPayrollPeriod();
            int payrollPeriodId = validPayrollPeriod == null ? 0 : validPayrollPeriod.PayrollPeriodId;
            decimal filterAmount = 0;
            decimal.TryParse(txtFilter.Text.Trim(), out filterAmount);

            List<GetEmployeeListForMassIncrementResult> list = BLL.Manager.PayManager.GetEmployeeListForMassIncrementResult(int.Parse(ddlIncome.SelectedValue),
                int.Parse(ddlStatus.SelectedValue), int.Parse(ddlGrade.SelectedValue), int.Parse(ddlDesignation.SelectedValue),
                SessionManager.CurrentCompanyId, payrollPeriodId, pagintCtl.CurrentPage,
                int.Parse(pagintCtl.DDLRecords.SelectedValue), ref _tempCount, (_sortBy + " " + _sortDirection).ToLower()
                , int.Parse(ddlDepartments.SelectedValue), int.Parse(ddlFilter.SelectedValue), filterAmount,
                int.Parse(ddlFilterIncomes.SelectedValue), txtEmpSearchText.Text.Trim(), int.Parse(ddlGroups.SelectedValue), int.Parse(ddlLevel.SelectedValue),
                int.Parse(ddlFilterEvents.SelectedValue));

            string template = ("~/App_Data/ExcelTemplate/SalaryIncrementImport.xlsx");



            int totalRecords = 0;

         
            {
                ExcelGenerator.ExportSalaryIncrement(template, list, ddlIncome.SelectedItem.Text);
            }
        }

        protected void btnPrevious_Click()
        {
            pagintCtl.CurrentPage -= 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage -= 1;
            BindEmployee();
        }

        protected void btnNext_Click()
        {
            pagintCtl.CurrentPage += 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage += 1;
            BindEmployee();
        }


        private void IsIncrementValid()
        {
            PayrollPeriod payrollPeriod = CommonManager.GetValidLastPayrollPeriod();




            if (payrollPeriod == null  )
                
            {
                //DisableChildControls(rowIncrement);

                divWarningMsg.InnerHtml = Resources.Messages.ValidPayrollPeriodNotDefined;
                divWarningMsg.Hide = false;

                btnSave.Visible = false;
                //calEligibilityDate.Enabled = false;

                //rowIncrement.Attributes.Add("title", Resources.Messages.ValidPayrollPeriodNotDefined);
            }
            else
            {
                string code = string.Format("var startingDate = '{0}';", payrollPeriod.StartDate);

                //PEmployeeIncrement lastIncludedIncrement =
                //    PayManager.GetLastIncludedIncrement(this.CustomId);

                ////If has last included increment set in javascript for the validation of current increment
                //if (lastIncludedIncrement != null)
                //    code += string.Format("var lastIncludedIncrementDate = '{0}';",
                //                          lastIncludedIncrement.EffectiveFromDate);




                Page.ClientScript.RegisterClientScriptBlock(
                    this.GetType(), "DateReg123", code, true);

            }


            //string addModeCode = "var isAddMode = " + (this.CustomId == 0 ? "true;" : "false;");
            //Page.ClientScript.RegisterClientScriptBlock(
            //    this.GetType(), "DateReg123AddMode", addModeCode, true);


        }



        public void RegisterJsForPopUp()
        {
            JavascriptHelper.AttachPopUpCode(Page, "popupMassIncrement", "UniformIncrease/UniformIncrease.aspx", 500, 260);
        }

        public string GetExistingIncome(string existingIncome, string existingIncomeRate)
        {
            if (!string.IsNullOrEmpty(existingIncome.Trim()))
            {
               // if (!double.Parse(existingIncome).Equals(0))
                    return GetCurrency(existingIncome);
               // else
                   // return string.Empty;
            }
            else if (!string.IsNullOrEmpty(existingIncomeRate.Trim()))
              //  if (!double.Parse(existingIncomeRate).Equals(0))
                    return GetCurrency(existingIncomeRate);
               // else
                 //   return string.Empty;
            else
                return string.Empty;

        }


        protected void BindEmployee()
        {

            //if (!incomeId.Equals(-1) && !statusId.Equals(-1) && !gradeId.Equals(-1) && !designationId.Equals(-1) & !companyId.Equals(-1))

            int? _tempCount = 0;
            PayrollPeriod validPayrollPeriod = CommonManager.GetValidLastPayrollPeriod();
            int payrollPeriodId = validPayrollPeriod == null ? 0 : validPayrollPeriod.PayrollPeriodId;
            decimal filterAmount = 0;
            decimal.TryParse(txtFilter.Text.Trim(), out filterAmount);

            List<GetEmployeeListForMassIncrementResult> list = BLL.Manager.PayManager.GetEmployeeListForMassIncrementResult(int.Parse(ddlIncome.SelectedValue),
                int.Parse(ddlStatus.SelectedValue), int.Parse(ddlGrade.SelectedValue), int.Parse(ddlDesignation.SelectedValue),
                SessionManager.CurrentCompanyId, payrollPeriodId, pagintCtl.CurrentPage,
                int.Parse(pagintCtl.DDLRecords.SelectedValue), ref _tempCount, (_sortBy + " " + _sortDirection).ToLower()
                , int.Parse(ddlDepartments.SelectedValue), int.Parse(ddlFilter.SelectedValue), filterAmount, int.Parse(ddlFilterIncomes.SelectedValue)
                , txtEmpSearchText.Text.Trim(),int.Parse(ddlGroups.SelectedValue),int.Parse(ddlLevel.SelectedValue), int.Parse(ddlFilterEvents.SelectedValue));
            gvMassIncrement.DataSource = list;
            gvMassIncrement.DataBind();

            pagintCtl.UpdatePagingBar(_tempCount.Value);

            

            if (_tempCount <= 0)
            {
                pagintCtl.Visible = false;
            }
            else
            {
                pagintCtl.Visible = true;
            }

        }

 
        protected void ddlIncome_SelectedIndexChanged(object sender, EventArgs e)
        {
            //incomeId = int.Parse(ddlIncome.SelectedItem.Value);
            pagintCtl.CurrentPage = 1;
            BindEmployee();
            hdIncomeType.Value = PayManager.GetIncomeTypeByIncomeId(int.Parse(ddlIncome.SelectedValue));
            if (hdIncomeType.Value.ToUpper().Equals("PERCENTOFINCOME"))
                hdIncreaseType.Value = "BYPERCENT";
            else
                hdIncreaseType.Value = "BYAMOUNT";

            RegisterJsForPopUp();
        }

        protected void ddlRecords_SelectedIndexChanged()
        {
            //_tempCurrentPage = 1;
            pagintCtl.CurrentPage = 1;
            BindEmployee();
        }


        protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            pagintCtl.CurrentPage = 1;
            BindEmployee();
        }

        protected void ddlGrade_SelectedIndexChanged(object sender, EventArgs e)
        {
            pagintCtl.CurrentPage = 1;
            BindEmployee();
        }

        protected void ddlDesignation_SelectedIndexChanged(object sender, EventArgs e)
        {
            pagintCtl.CurrentPage = 1;
            BindEmployee();
        }

        protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            pagintCtl.CurrentPage = 1;
            BindEmployee();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

            if( ddlIncome.SelectedIndex==0)
                return;
            ;

            if (ddlEvents.Items.Count >= 2 && ddlEvents.SelectedValue=="-1")
            {
                divWarningMsg.InnerHtml = "Event type selection is required.";
                divWarningMsg.Hide = false;
                return;
            }

            bool saveStatus = true;
            List<PEmployeeIncrement> empIncrements = new List<PEmployeeIncrement>();
            List<PEmployeeIncome> empIncomes = new List<PEmployeeIncome>();

            PEmployeeIncrement empInc = null;
            PEmployeeIncome empIncome = null;
            TextBox txtNewIncome = null;
            TextBox txtExistingIncome = null;
            TextBox txtIncreasedBy = null;

            PayManager payMgr = new PayManager();
            PIncome income = payMgr.GetIncomeById(int.Parse(ddlIncome.SelectedValue));
            bool isPercentTypeIncome = income.Calculation == IncomeCalculation.PERCENT_OF_INCOME;
            PayrollPeriod payrollPeriod = CommonManager.GetLastPayrollPeriod();
            int employeeId, incrementId = 0; bool isIncrementMode = false;
            bool saved = false;
            decimal newIncomeAmount = 0, existingAmount = 0,increasedBy = 0;
            int count = 0;
           
           
            foreach (GridViewRow grow in gvMassIncrement.Rows)
            {
                txtNewIncome = (TextBox)grow.FindControl("txtNewIncome");
                txtExistingIncome = grow.FindControl("txtExistingIncome") as TextBox;
                txtIncreasedBy = grow.FindControl("txtIncreasedBy") as TextBox;

                incrementId = (int)(gvMassIncrement.DataKeys[grow.RowIndex]["IncrementId"]);
                isIncrementMode = (bool)gvMassIncrement.DataKeys[grow.RowIndex]["IsIncrementMode"];

                decimal.TryParse(txtNewIncome.Text.Trim(),out newIncomeAmount);
                decimal.TryParse(txtExistingIncome.Text.Trim(), out existingAmount);
                decimal.TryParse(txtIncreasedBy.Text.Trim(), out increasedBy);


                //if already incremented & new income is not empty, then don't do anything, as for incremented
                // income, it can only be deleted
                // as saved increment date can be changed so we are not allowing to directly update 
                if (incrementId != 0 && newIncomeAmount != 0)
                    continue;   

                // if not already incremented & no amount for increment then don't do anything
                if (incrementId == 0 && newIncomeAmount == 0 && increasedBy == 0)
                    continue;

             
                employeeId = (int)(gvMassIncrement.DataKeys[grow.RowIndex]["EmployeeId"]);
                int empIncId = (int)(gvMassIncrement.DataKeys[grow.RowIndex]["EmployeeIncomeId"]);
                //empIncome = payMgr.GetEmployeeIncome(empIncId);
                empIncome = new PEmployeeIncome();
                empIncome.EmployeeIncomeId = empIncId;
                empIncome.EmployeeId = employeeId;
                empIncome.IncomeId = income.IncomeId;

                // TODO : preserver this one
                // if not Retrospect increment but salary is already saved for this PayrollPeriod then show warning
                if (CalculationManager.IsCalculationSavedForEmployee(payrollPeriod.PayrollPeriodId, employeeId))
                {
                    divWarningMsg.InnerHtml = string.Format("Salary is already saved for the employee \"{0}\" for current period.", EmployeeManager.GetEmployeeById(employeeId).Name);
                    divWarningMsg.Hide = false;

                    return;
                }

                // reset increment as if increment is being deleted then newIncomeAmount is 0 and previously created PEmployeeIncrement will be added in the list twice
                empInc = null;

                //for Increment
                if (newIncomeAmount != 0 || (newIncomeAmount == 0 && increasedBy!= 0))
                {

                    empInc = GetIncrement(payrollPeriod);
                    empInc.EmployeeIncomeId = empIncId;
                    empInc.Id = incrementId;

                   

                    if (isPercentTypeIncome)
                        empIncome.RatePercent = double.Parse(txtNewIncome.Text.Trim());
                    else
                        empIncome.Amount = decimal.Parse(txtNewIncome.Text.Trim());

                    empInc.AmountNew = empIncome.Amount;
                    empInc.RatePercentNew = empIncome.RatePercent;

                }
                //for first time or not incrementable income value setting
                else
                {
                    if (txtExistingIncome.Text.Trim() == "")
                        txtExistingIncome.Text = "0";

                    if (isPercentTypeIncome)
                        empIncome.RatePercent = double.Parse(txtExistingIncome.Text.Trim());
                    else
                        empIncome.Amount = decimal.Parse(txtExistingIncome.Text.Trim());

                    empIncome.IsValid = true;
                }
                count += 1;

                //save Incomes to be updated 
                empIncomes.Add(empIncome);
                if (empInc != null)
                {
                    if (ddlEvents.SelectedValue != "-1")
                        empInc.EventID = int.Parse(ddlEvents.SelectedValue);

                    empIncrements.Add(empInc);
                }
            }
            
            //call to force to detach from cache as in update method also it has been retrieved
            //if (empIncome != null)
            //{
            //    PIncome test = empIncome.PIncome;
            //    empIncome.PIncome.PIncomeIncomes.Count();
            //    BLL.BaseBiz.Dispose();
            //}

            if (empIncomes.Count > 0)
            {
                string msg = payMgr.DoMassIncrement(empIncomes, empIncrements, payrollPeriod);

                if (!string.IsNullOrEmpty(msg))
                {
                    divWarningMsg.InnerHtml = msg;
                    divWarningMsg.Hide = false;
                    return;
                }

                saved = true;
                
            }

            if (saved)
            {
                divMsgCtl.InnerHtml = string.Format("Income changed for {0} employees.", count);
                divMsgCtl.Hide = false;
            }
            BindEmployee();
        }

        public void btnLoad_Click(object sender, EventArgs e)
        {
            BindEmployee();
        }


        //public bool IsIncrementMode(object isIncrementMode, object incrementId)
        //{
        //    bool mode = (bool)isIncrementMode;

        //    //if (incrementId == null || (int)(incrementId) == 0)
        //    //    return false;

        //    return mode;
        //}

        public string GetStatus(object value)
        {
            foreach (KeyValue val in new JobStatus().GetMembers())
            {
                if (val.Key == value.ToString())
                    return val.Value;
            }


            return "";
        }

        private PEmployeeIncrement GetIncrement(PayrollPeriod payrollPeriod)
        {
            PEmployeeIncrement inc = new PEmployeeIncrement();
            //inc.IsIncluded = false;
            inc.EffectiveFromDate = Calendar1.SelectedDate.ToString();

            //check if resterospect or not
            if (payrollPeriod != null)
            {
                CustomDate date1 = CustomDate.GetCustomDateFromString(payrollPeriod.StartDate,
                                                                      SessionManager.IsEnglish);
                CustomDate date2 = CustomDate.GetCustomDateFromString(inc.EffectiveFromDate, IsEnglish);

                if (payrollPeriod.StartDate != inc.EffectiveFromDate
                    && DateManager.IsSecondDateGreaterThan(date1, date2) == false)
                {
                    inc.IsReterospect = true;
                }
            }
            return inc;
        }

        public string GetNewIncome(double IncrementIncome, double ExistingIncomeRate, object IncrementId)
        {
            int incrementIDValue = Convert.ToInt32(IncrementId);

            double NewIncome = 0;

            if (IncrementIncome.Equals(0) && incrementIDValue== 0)
                return string.Empty;


            NewIncome = IncrementIncome + ExistingIncomeRate;

            //if (NewIncome == 0)
            //    return GetCurrency(IncrementIncome);

            return GetCurrency( NewIncome);

        }
    }
}
