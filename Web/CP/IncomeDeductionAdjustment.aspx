<%@ Page Title="Income/Deduction Adjustment" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="IncomeDeductionAdjustment.aspx.cs" Inherits="Web.IncomeDeductionAdjustment" %>

<%@ Import Namespace="BLL.Manager" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>
    <script type="text/javascript">

        function calculatePayFromAdjustedAmount(txt1, txtExistingAmount, txtAdjustmentAmount, txtPay) {

            var txtAdjustedAmountElement = document.getElementById(txt1.id.replace(txtPay, txtPay)); ;
            var txtExistingAmountElement = document.getElementById(txt1.id.replace(txtPay, txtExistingAmount));
            var txtAdjustmentAmountElement = document.getElementById(txt1.id.replace(txtPay, txtAdjustmentAmount));

            //var txtPaidAmountElement = document.getElementById(txt1.id.replace(txtPay, "txtPaidAmount"));

            if (parseFloat(txtAdjustedAmountElement.value) == 0)
                return;

            txtAdjustmentAmountElement.value = getMoney(getNumber(txtAdjustedAmountElement.value) - getNumber(txtExistingAmountElement.value));

        }

        function calculatePay(txt1, txtFirstId, txtSecondId, txtPay) {
            var txt2 = document.getElementById(txt1.id.replace(txtFirstId, txtSecondId));
            var result = document.getElementById(txt1.id.replace(txtFirstId, txtPay));


            if (parseFloat(txt1.value) == 0)
                result.value = "0.00";
            else
                result.value = getMoney(getNumber(txt1.value) + getNumber(txt2.value));

        }

        function validateAdjustment(source, args) {


            var txtAdjustmentAmount = document.getElementById(source.controltovalidate);
            var txtExistingAmount = document.getElementById(source.controltovalidate.replace('txtAdjustmentAmount', 'txtExistingAmount'));

            var existingAmount = getNumber(txtExistingAmount.value);

            var value = getNumber(txtAdjustmentAmount.value);

            if (typeof (isNegativeValueAllowed) == 'undefined')
                isNegativeValueAllowed = false;

            if (isNegativeValueAllowed == false) {
                if (value < 0) {
                    if ((value + existingAmount) < 0)
                        args.IsValid = false;
                    else
                        args.IsValid = true;
                }
            }
            else
                args.IsValid = true;
        }

        $(document).ready(function () {

            setMovementToGrid('#<%= gvw.ClientID %>', false);



        });


        function importPopupProcess(btn) {
            var payrollPeriodId = document.getElementById('<%= ddlPayrollPeriods.ClientID %>').value;
            var typeID = document.getElementById('<%= ddlIncomeOrDeduction.ClientID %>').value;


            // if (IsProjectInputType)
            if (payrollPeriodId == 0)
                return false;

            var ret = importPopup("payrollPeriod=" + payrollPeriodId + "&type=" + typeID);


            return false;
        }


        function refreshWindow() {
        }

        function expressionCallback(expression, value, note) {
            txtExpressionRef.value = (expression);
            txtValueRef.value = value;

            var newNote = txtExpressionRef.value;




            var tempNote = (note);
            if (tempNote.indexOf(' - ') >= 0) {
                tempNote = tempNote.substring(tempNote.indexOf(' - '), tempNote.length);
            }

            //if (newNote != "" && tempNote != "")
            newNote += ' - ';

            newNote += tempNote;

            txtNoteRef.value = newNote;

            calculatePay(txtValueRef, "txtAdjustmentAmount", "txtExistingAmount", "txtPay");
        }

        var txtExpressionRef = null;
        var txtValueRef = null;
        var txtNoteRef = null;
        function showRateCalculate(txtExpressionCtl, employeeId) {
            //AdjustmentCalculation.aspx
            txtExpressionRef = document.getElementById(txtExpressionCtl.id.replace("imgCalculate", "txtExpression"));
            txtValueRef = document.getElementById(txtExpressionCtl.id.replace("imgCalculate", "txtAdjustmentAmount"));
            txtNoteRef = document.getElementById(txtExpressionCtl.id.replace("imgCalculate", "txtNote"));

            var payrollPeriodId = document.getElementById('<%= ddlPayrollPeriods.ClientID %>').value;
            var typeID = document.getElementById('<%= ddlIncomeOrDeduction.ClientID %>').value;


            // if (IsProjectInputType)
            if (payrollPeriodId == 0)
                return false;

            var ret = adjustmentPopup("payrollPeriod=" + payrollPeriodId + "&type=" + typeID + "&eid=" + employeeId + "&exp="
                + encodeURIComponent(txtExpressionRef.value) + "&note=" + encodeURIComponent(txtNoteRef.value));

        }
        
    </script>
    <script src="../Scripts/currency.js" type="text/javascript"></script>
    <style type="text/css">
        .calculate
        {
            background-image: url(../images/calculate.png);
            vertical-align: middle;
            cursor: pointer;
        }
        td
        {
            padding: auto !important;
        }
        .fieldTable td, table tr.up td
        {
            padding-top: inherit;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <ext:ResourceManager ID="ResourceManager1" DisableViewState="false" runat="server"
        ShowWarningOnAjaxFailure="false" ScriptMode="Release" Namespace="Rigo" />
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4 runat="server" id="title">
                    Income or Deduction Adjustment</h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <asp:Panel runat="server" class="attribute" Style="padding: 10px" DefaultButton="btnLoad">
            <%--  <asp:UpdatePanel ID="upd" runat="server" UpdateMode="Conditional">
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnLoad" />
                </Triggers>
                <ContentTemplate>--%>
            <table class="fieldTable firsttdskip">
                <tr>
                    <td>
                        <strong>Payroll Period</strong>
                    </td>
                    <td>
                        <strong>Branch</strong>
                    </td>
                    <td>
                        <strong>Department</strong>
                    </td>
                    <td>
                        <strong>Employee</strong>
                    </td>
                    <%-- <td>
                                <strong>Type</strong>
                            </td>--%>
                    <td>
                        <strong>Income/Deduction/Tax</strong>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:DropDownList AutoPostBack="true" ID="ddlPayrollPeriods" DataTextField="Name"
                            DataValueField="PayrollPeriodId" AppendDataBoundItems="true" Width="120px" runat="server"
                            OnSelectedIndexChanged="ddlPayrollPeriods_SelectedIndexChanged">
                            <asp:ListItem Value="-1" Text=""></asp:ListItem>
                        </asp:DropDownList>
                        <%-- <asp:RequiredFieldValidator ID="valReqdStatus" runat="server" ControlToValidate="ddlPayrollPeriods"
                    Display="None" ErrorMessage="PayrollPeriod is required." InitialValue="-1" ValidationGroup="Balance"></asp:RequiredFieldValidator>--%>
                    </td>
                    <td>
                        <asp:DropDownList AutoPostBack="true" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged"
                            ID="ddlBranch" DataTextField="Name" DataValueField="BranchId" AppendDataBoundItems="true"
                            Width="120px" runat="server">
                            <asp:ListItem Value="-1" Text=""></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:DropDownList AutoPostBack="true" ID="ddlDepartment" DataTextField="Name" DataValueField="DepartmentId"
                            runat="server" AppendDataBoundItems="true" Width="120px" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged1">
                            <asp:ListItem Value="-1" Text=""></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <%--<asp:DropDownList ID="ddlEmployee" DataTextField="Name" DataValueField="EmployeeId"
                            runat="server" AppendDataBoundItems="true" Width="200px">
                            <asp:ListItem Value="-1" Text=""></asp:ListItem>
                        </asp:DropDownList>--%>
                        <ext:Store ID="storeEmployee" runat="server">
                            <Model>
                                <ext:Model IDProperty="EmployeeId">
                                    <Fields>
                                        <ext:ModelField Name="NameEIN" />
                                        <ext:ModelField Name="EmployeeId" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                        <ext:ComboBox QueryMode="Local" ID="ddlEmployee" Width="200px" ForceSelection="false"
                            runat="server" StoreID="storeEmployee" Editable="true" DisplayField="NameEIN" ValueField="EmployeeId"
                            Mode="Local" EmptyText="">
                            <Items>
                                <ext:ListItem Value="-1" Text="&nbsp;" />
                            </Items>
                            <Listeners>
                                <Focus Handler="if( this.getValue()=='-1' ) this.clearValue();" />
                                <Select Handler="if( this.getValue()=='-1') this.clearValue(); " />
                                <Blur Handler="if(this.getStore().getById(this.getValue())==null) this.clearValue(); " />
                                <BeforeQuery Handler="var q = queryEvent.query;if(q=='') return;
                                      queryEvent.query = new RegExp('\\b' + q, 'i');
                                      queryEvent.query.length = q.length;" />
                            </Listeners>
                        </ext:ComboBox>
                    </td>
                    <%--  <td>
                                <asp:DropDownList ID="ddlIncomeOrDeductionType" runat="server" AppendDataBoundItems="true"
                                    Width="120px">
                                    <asp:ListItem Value="-1" Text=""></asp:ListItem>
                                    <asp:ListItem Value="true" Text="Income"></asp:ListItem>
                                    <asp:ListItem Value="false" Text="Deduction"></asp:ListItem>
                                </asp:DropDownList>
                            </td>--%>
                    <td>
                        <asp:DropDownList ID="ddlIncomeOrDeduction" DataTextField="Key" DataValueField="Value"
                            runat="server" AppendDataBoundItems="true" Width="200px">
                            <asp:ListItem Value="-1" Text=""></asp:ListItem>
                        </asp:DropDownList>
                        <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlIncomeOrDeduction"
                    Display="None" ErrorMessage="Income or Deduction is required." InitialValue="-1" ValidationGroup="Balance"></asp:RequiredFieldValidator>--%>
                    </td>
                    <td valign="top">
                        <asp:Button ID="btnLoad" CssClass="btn btn-default btn-sect btn-sm" OnClick="btnLoad_Click" width="80px" runat="server" Text="Show" />
                    </td>
                    <td>
                        &nbsp;&nbsp;
                        <asp:LinkButton ID="LinkButton1" Style='text-decoration: underline' runat="server"
                            Text="Import" OnClientClick="importPopupProcess();return false;" />
                    </td>
                </tr>
            </table>
            <%--</ContentTemplate>
            </asp:UpdatePanel>--%>
            <span style='color: #F4B5A6'>Note : If the head doesn't appear for the employee, then
                add the head or click on change and save the head to the employee from payroll.</span>
        </asp:Panel>
        <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
        <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
        <asp:Panel runat="server" DefaultButton="btnUpdate">
            <div class="clear">
                <cc2:EmptyDisplayGridView CssClass="table table-primary mb30 table-bordered table-hover" UseAccessibleHeader="true" ShowHeaderWhenEmpty="True"
                    ID="gvw" runat="server" DataKeyNames="EmployeeId,PayrollPeriodId,IncomeDeductionId,Type"
                    AutoGenerateColumns="False" CellPadding="4" GridLines="None" AllowSorting="True"
                    ShowFooterWhenEmpty="False">
<%--                    <RowStyle BackColor="#E3EAEB" />--%>
                    <Columns>
                        <asp:BoundField HeaderStyle-Width="30px" DataFormatString="{0:000}" DataField="EmployeeId"
                            HeaderText="EIN"></asp:BoundField>
                        <asp:TemplateField HeaderText="Name" HeaderStyle-Width="200px">
                            <ItemTemplate>
                                <%# Eval("Name") %>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" Width="200px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Existing Amount"
                            HeaderStyle-Width="120px">
                            <ItemTemplate>
                                <asp:TextBox data-col='0' Width="100px" data-row='<%# Container.DataItemIndex %>'
                                    Enabled='<%#((int) Eval("IsCalculationSaved")) == 0 %>' onblur='calculatePay(this,"txtExistingAmount","txtAdjustmentAmount","txtPay")'
                                    CssClass='calculationInput disabled' Style='text-align: right' ID="txtExistingAmount"
                                    Text='<%#GetCurrency( Eval("ExistingAmount") ) %>' runat="server" ReadOnly="true"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField Visible="false" HeaderStyle-HorizontalAlign="Left" HeaderText="Paid Amount"
                            HeaderStyle-Width="100px">
                            <ItemTemplate>
                                <asp:TextBox data-col='1' Width="100px" data-row='<%# Container.DataItemIndex %>'
                                    Enabled='<%#((int) Eval("IsCalculationSaved")) == 0 %>' onblur='calculatePay( document.getElementById( this.id.replace("txtPaidAmount","txtExistingAmount")),"txtExistingAmount","txtAdjustmentAmount","txtPay")'
                                    CssClass='calculationInput' Style='text-align: right' ID="txtPaidAmount" Text='<%# GetCurrency( Eval("PaidAmount")) %>'
                                    runat="server"></asp:TextBox>
                                <asp:CompareValidator SetFocusOnError="true" ValueToCompare="0" Display="None" Operator="DataTypeCheck"
                                    Type="Currency" ID="valPaidAmount" ControlToValidate="txtPaidAmount" ValidationGroup="Balance"
                                    runat="server" ErrorMessage="Invalid paid amount."></asp:CompareValidator>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Adjustment Amount"
                            HeaderStyle-Width="140px">
                            <ItemTemplate>
                                +
                                <asp:TextBox data-col='2' Width="90px" data-row='<%# Container.DataItemIndex %>'
                                    Enabled='<%#((int) Eval("IsCalculationSaved")) == 0 %>' onblur='calculatePay(this,"txtAdjustmentAmount","txtExistingAmount","txtPay")'
                                    CssClass='calculationInput' Style='text-align: right' ID="txtAdjustmentAmount"
                                    Text='<%# GetCurrency( Eval("AdjustmentAmount")) %>' runat="server"></asp:TextBox>
                                <asp:HiddenField ID="txtExpression" Value='<%#  Eval("Expression")%>' runat="server" />
                                <asp:RequiredFieldValidator SetFocusOnError="true" Display="None" ID="val2" ControlToValidate="txtAdjustmentAmount"
                                    ValidationGroup="Balance" runat="server" ErrorMessage="Adjustment Amount is required." />
                                <asp:CompareValidator SetFocusOnError="true" ValueToCompare="0" Display="None" Operator="DataTypeCheck"
                                    Type="Currency" ID="valOpeningBalance1" ControlToValidate="txtAdjustmentAmount"
                                    ValidationGroup="Balance" runat="server" ErrorMessage="Invalid adjustment amount."></asp:CompareValidator>
                                <asp:CustomValidator runat="server" SetFocusOnError="true" ControlToValidate="txtAdjustmentAmount"
                                    ValidationGroup="Balance" ErrorMessage="Invalid adjustment amount, adjustment amount can not be negative."
                                    Display="None" ClientValidationFunction="validateAdjustment" />
                                <img id="imgCalculate" visible='<%# IsCalculateVisible( Eval("Type") ) %>' class="calculate"
                                    runat="server" src="~/images/calculate.png" alt="Calculate" title="Calculate"
                                    onclick='<%# "showRateCalculate(this," + Eval("EmployeeId") + ")" %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Adjusted Amount"
                            HeaderStyle-Width="120px">
                            <ItemTemplate>
                                =
                                <asp:TextBox data-col='3' Width="85px" data-row='<%# Container.DataItemIndex %>'
                                    Enabled='<%#((int) Eval("IsCalculationSaved")) == 0 %>' onblur='calculatePayFromAdjustedAmount(this,"txtExistingAmount","txtAdjustmentAmount","txtPay")'
                                    Style='text-align: right' CssClass='calculationInput' Text='<%# GetCurrency( Eval("AdjustedAmount")) %>'
                                    ID="txtPay" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator SetFocusOnError="true" Display="None" ID="val21" ControlToValidate="txtPay"
                                    ValidationGroup="Balance" runat="server" ErrorMessage="Adjusted Amount is required." />
                                <asp:CompareValidator SetFocusOnError="true" ValueToCompare="0" Display="None" Operator="DataTypeCheck"
                                    Type="Currency" ID="valOpeningBalance11" ControlToValidate="txtPay" ValidationGroup="Balance"
                                    runat="server" ErrorMessage="Invalid adjusted amount."></asp:CompareValidator>
                                <asp:CustomValidator ID="CustomValidator11" runat="server" SetFocusOnError="true"
                                    ControlToValidate="txtPay" ValidationGroup="Balance" ErrorMessage="Invalid adjusted amount, adjusted amount can not be negative."
                                    Display="None" ClientValidationFunction="validateAdjustment" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Note" HeaderStyle-Width="350px">
                            <ItemTemplate>
                                <asp:TextBox data-col='4' data-row='<%# Container.DataItemIndex %>' Enabled='<%#((int) Eval("IsCalculationSaved")) == 0 %>'
                                    Style='width: 350px; text-align: left;' CssClass='calculationInput' ID="txtNote"
                                    Text='<%#  Eval("Note") %>' runat="server"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Delete" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                            <HeaderTemplate>
                                <img alt="Delete" title="Delete" src="../images/delet.png" />
                                <br />
                                <asp:CheckBox ID="chkDelete" onclick="selectDeselectAll(this)" runat="server" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox Enabled='<%#((int) Eval("IsCalculationSaved")) == 0 %>' ID="chkDelete"
                                    Visible='<%# ((bool) Eval("IsSaved"))!=false %>' runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="odd" />
                    <AlternatingRowStyle CssClass="even" />
                    <EmptyDataTemplate>
                        <b>No adjustments. </b>
                    </EmptyDataTemplate>
                </cc2:EmptyDisplayGridView>
                <uc1:PagingCtl ID="pagingCtl" OnNextRecord="btnNext_Click" OnPrevRecord="btnPrevious_Click"
                    OnDropDownShowRecordsChanged="ddlRecords_SelectedIndexChanged" runat="server" />
            </div>
            <div class="buttonsDiv">
                <%--  <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Balance" DisplayMode="List"
                ShowSummary="false" ShowMessageBox="true" runat="server"></asp:ValidationSummary>--%>
                <%--  <asp:Button Style='text-align: left' ID="btnPostToSalary" CssClass="update" Visible="true"
                runat="server" Text="Post Salary" OnClick="btnPostToSalary_Click" />
            &nbsp;&nbsp;&nbsp;--%>
                <asp:Button OnClientClick="valGroup='Balance';return CheckValidation();" ID="btnUpdate"
                    CssClass="btn btn-primary btn-sect btn-sm" Visible="true" ValidationGroup="Balance" runat="server" Text="Apply"
                    OnClick="btnUpdate_Click" />
                <asp:Button ID="btnCancel" CssClass="btn btn-default btn-sect btn-sm" Visible="true" CausesValidation="false"
                    runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                <asp:Button ID="btnDelete" CssClass="btn btn-danger btn-sect btn-sm" OnClick="btnDelete_Click" Visible="true"
                    CausesValidation="false" runat="server" OnClientClick="return confirm('Confirm delete the adjustments?');"
                    Text="Delete" />
            </div>
        </asp:Panel>
    </div>
</asp:Content>
