﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using System.Xml;
using BLL.BO;

namespace Web.CP
{
    public partial class AllowanceType : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.HumanResource;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
              
            }
        }

        public void Initialise()
        {
            storeIncomes.DataSource = PayManager.GetVariableIncomeList(SessionManager.CurrentCompanyId, false)
                .OrderBy(x => x.Title).ToList();
            storeIncomes.DataBind();

            List<GetIncomeForIncomesResult> list = new PayManager().GetIncomeListForIncome(SessionManager.CurrentCompanyId, 0);
            gridIncomes.Store[0].DataSource = list;
            gridIncomes.Store[0].DataBind();

            Load();

            LoadLevels();

            if (CommonManager.CompanySetting.WhichCompany != WhichCompany.HPL)
            {
                cmbCalculationType.Items.RemoveAt(1);
                cmbCalculationType.Items.RemoveAt(1);
            }
        }

        protected void btnCopy_Click(object sender, DirectEventArgs e)
        {
            List<EveningCounterTypeLevel> lines = new List<EveningCounterTypeLevel>();
            string json = e.ExtraParams["CopyValue"];

            if (string.IsNullOrEmpty(json))
            {
                return;
            }


            lines = JSON.Deserialize<List<EveningCounterTypeLevel>>(json);

            if (lines == null)
            {
                X.Msg.Alert("", "No Value Selected").Show();
                return;
            }


            if (lines != null && lines.Count > 0)
            {
                if (lines.First().Rate != null)
                {
                    Hidden_CopyValue.Text = lines.First().Rate.ToString();
                    //X.Msg.Alert("", Hidden_CopyValue.Text).Show();


                }
                else
                    Hidden_CopyValue.Text = "0";

                txtValueToBeCopied.Text = Hidden_CopyValue.Text;
                WindowCopy.Show();
            }
            else
            {
                X.Msg.Alert("", "No Value Selected").Show();
                return;
            }



        }


        protected void btnCopyTo_Click(object sender, DirectEventArgs e)
        {
            List<OvertimeTypeLevel> lines = new List<OvertimeTypeLevel>();
            string json = e.ExtraParams["SelectedLevels"];

            if (string.IsNullOrEmpty(json))
            {
                X.Msg.Alert("", "Please select atleast one Level.").Show();
                return;
            }


            lines = JSON.Deserialize<List<OvertimeTypeLevel>>(json);
            //X.Msg.Alert("", lines.Count()).Show();



            foreach (var val1 in lines)
            {
                if (val1 != null)
                {
                    //grid.store.getById(id).set('field_name', value)
                    storeAllowanceDetail.GetById(val1.LevelId.ToString()).Set("Rate", Hidden_CopyValue.Text);
                    storeAllowanceDetail.CommitChanges();
                }
            }

            if (lines == null || lines.Count < 1)
            {
                X.Msg.Alert("", "Please select atleast one Level.").Show();
            }
            else
                WindowCopy.Close();

        }

        public new void Load()
        {
            List<PIncome> list = PayManager.GetFixedIncomeListByCompany(SessionManager.CurrentCompanyId);

            Node rootNode = new Node();

            foreach (PIncome module in list)
            {
                Node moduleChild = new Node();
                moduleChild.Checked = false;
                moduleChild.Expanded = true;


                moduleChild.CustomAttributes.Add(new ConfigItem { Name = "Title", Value = module.Title });
                moduleChild.CustomAttributes.Add(new ConfigItem { Name = "IncomeId", Value = module.IncomeId.ToString() });

                rootNode.Children.Add(moduleChild);


            }

            //treePanel.SetRootNode(rootNode);
        }

        public bool IsContainsPage(int incomeId, List<OvertimeTypeIncome> pages)
        {
            foreach (OvertimeTypeIncome page in pages)
            {
                if(page.IncomeId == incomeId)
                    return true;
            }
            return false;
        }


        public void LoadNodes(List<OvertimeTypeIncome> pages)
        {
      

            Node rootNode = new Node();

            List<PIncome> list = PayManager.GetFixedIncomeListByCompany(SessionManager.CurrentCompanyId);

            foreach (PIncome module in list)
            {
                Node moduleChild = new Node();
                moduleChild.Checked = false;
                moduleChild.Expanded = true;
                moduleChild.Leaf = true;


                if (pages != null && IsContainsPage(module.IncomeId,pages))
                {
                    moduleChild.Checked = true;
                }
                else
                {
                    moduleChild.Checked = false;
                }

                moduleChild.CustomAttributes.Add(new ConfigItem { Name = "Title", Value = module.Title });
                moduleChild.CustomAttributes.Add(new ConfigItem { Name = "IncomeId", Value = module.IncomeId.ToString() });


                rootNode.Children.Add(moduleChild);

            }

            //treePanel.SetRootNode(rootNode);
        }

        private void LoadLevels()
        {
            GridLevels.GetStore().DataSource = AllowanceManager.GetAllowanceTypes();
            GridLevels.GetStore().DataBind();

        }



        public void ClearLevelFields()
        {
            hiddenValue.Text = "";
            txtName.Text = "";

            selectionModel.ClearSelection();

            selectionModel.UpdateSelection();

        }

      
        
        protected void btnAddLevel_Click(object sender, DirectEventArgs e)
        {
            
            ClearLevelFields();
            
        }


        protected void btnDeleteLevel_Click(object sender, DirectEventArgs e)
        {
            int overtimeTypeId = int.Parse(hiddenValue.Text.Trim());
            Status status = AllowanceManager.DeleteEveningCounter(overtimeTypeId);
            if (status.IsSuccess)
            {
                LoadLevels();
                NewMessage.ShowNormalPopup("Allowance type deleted.");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        

        protected void btnEditLevel_Click(object sender, DirectEventArgs e)
        {
            //int levelId = int.Parse(hiddenValue.Text.Trim());
            //TAAllowance entity = TravelAllowanceManager.GetTravelAllowanceById(levelId);
            //txtName.Text = entity.Name;
            //txtDescription.Text = entity.Description;

            //WindowLevel.Show();

            int overtimeId = int.Parse(hiddenValue.Text.Trim());
            DAL.EveningCounterType counter = AllowanceManager.GetEveningCounterType(overtimeId);
            if (counter != null)
            {
                cmbIncomes.ClearValue();
                storeIncomes.ClearFilter();
                if (counter.IncomeId != null)
                    cmbIncomes.SetValue(counter.IncomeId.ToString());

                if (counter.IsProportionOnTotalMonthDays != null)
                    chkIsMonthlyProportionate.Checked = counter.IsProportionOnTotalMonthDays.Value;
                else
                    chkIsMonthlyProportionate.Checked = false;

                if (counter.DoNowShowDaysWarning != null)
                    chkDoNotShowDaysWarning.Checked = counter.DoNowShowDaysWarning.Value;
                else
                    chkDoNotShowDaysWarning.Checked = false;

                if (counter.DoNotShowInEmployeePortal != null)
                    chkDoNotShowInEmployeePortal.Checked = counter.DoNotShowInEmployeePortal.Value;
                else
                    chkDoNotShowInEmployeePortal.Checked = false;

                if (counter.CalculationType != null)
                    cmbCalculationType.SetValue(counter.CalculationType.ToString());
                else
                    cmbCalculationType.ClearValue();

                if (counter.CalculationType == (int)AllowanceEveningCounterCalculationType.BasedOnSalary)
                {
                    gridIncomes.Show();



                    foreach (EveningCounterTypeIncome item in counter.EveningCounterTypeIncomes)
                    {
                        selectionModel.SelectedRows.Add(new SelectedRow(item.IncomeId.ToString()));
                    }

                    selectionModel.UpdateSelection();
                }
                else
                {
                    gridIncomes.Hide();

                    selectionModel.ClearSelection();

                    selectionModel.UpdateSelection();
                }
                txtName.Text = counter.Name;
                txtAllowanceRate.Text = counter.Rate.ToString();
            }
            windowAddEditOvertime.Show();

        }



        protected void btnDetailLevel_Click(object sender, DirectEventArgs e)
        {
            int overtypeId = int.Parse(hiddenValue.Text.Trim());
            List<EveningCounterTypeLevel> detailGridList = OvertimeManager.GetAllowanceLevelList(overtypeId);
            storeAllowanceDetail.DataSource = detailGridList;
            storeAllowanceDetail.DataBind();

            storeCopyList.DataSource = detailGridList;
            storeCopyList.DataBind();

            Window1.Show();
        }


        public void btnOvertimeType_Click(object sender, DirectEventArgs e)
        {
            DAL.EveningCounterType counter = new DAL.EveningCounterType();
            bool isInsert = string.IsNullOrEmpty(hiddenValue.Text.Trim());
            if (!isInsert)
                counter.EveningCounterTypeId = int.Parse(hiddenValue.Text.Trim());

            counter.Name = txtName.Text.Trim();
            counter.IsProportionOnTotalMonthDays = chkIsMonthlyProportionate.Checked;
            counter.DoNowShowDaysWarning = chkDoNotShowDaysWarning.Checked;
            counter.DoNotShowInEmployeePortal = chkDoNotShowInEmployeePortal.Checked;
            if (!string.IsNullOrEmpty(txtAllowanceRate.Text))
                counter.Rate = double.Parse(txtAllowanceRate.Text);

            if (cmbIncomes.SelectedItem != null && cmbIncomes.SelectedItem.Value != null)
                counter.IncomeId = int.Parse(cmbIncomes.SelectedItem.Value);

            if (cmbCalculationType.SelectedItem != null && cmbCalculationType.SelectedItem.Value != null)
                counter.CalculationType = int.Parse(cmbCalculationType.SelectedItem.Value);


            if (counter.CalculationType == (int)AllowanceEveningCounterCalculationType.BasedOnSalary)
            {
                CheckboxSelectionModel sm = this.gridIncomes.GetSelectionModel() as CheckboxSelectionModel;
                foreach (var item in sm.SelectedRows)
                {
                    counter.EveningCounterTypeIncomes.Add(new EveningCounterTypeIncome { IncomeId = int.Parse(item.RecordID) });
                }
            }

            Status status = AllowanceManager.InsertUpdateEveningCounterType(counter, isInsert);
            if (status.IsSuccess)
            {
                windowAddEditOvertime.Hide();
                LoadLevels();
                NewMessage.ShowNormalPopup("Allowance Type saved.");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }

        }

        protected void btnDetailSaveUpdate_Click(object sender, DirectEventArgs e)
        {
            List<EveningCounterTypeLevel> lines = new List<EveningCounterTypeLevel>();
            string json = e.ExtraParams["Values"];

            if (string.IsNullOrEmpty(json))
            {
                return;
            }


            lines = JSON.Deserialize<List<EveningCounterTypeLevel>>(json);

            Status status = OvertimeManager.InsertUpdateAllowanceLevelDetail(lines);
            if (status.IsSuccess)
            {
                Window1.Hide();
                LoadLevels();
                NewMessage.ShowNormalPopup("Allowance rate saved.");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }


        }


        protected void btnLevelSaveUpdate_Click(object sender, DirectEventArgs e)
        {

             
        }
 
    }
}

/*
  
 
 
*/