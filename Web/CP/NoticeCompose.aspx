<%@ Page MaintainScrollPositionOnPostback="true" Title="Notice" Language="C#" ValidateRequest="false"
    MasterPageFile="~/Master/NewDetails.Master" AutoEventWireup="true" CodeBehind="NoticeCompose.aspx.cs"
    Inherits="Web.Employee.NoticeCompose" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ExtMsgCtl.ascx" TagName="ExtMsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
    <%--  <script src="../Scripts/jquery.sexy-combo.pack.js" type="text/javascript"></script>

   <script src="../Scripts/jquery.sexy-combo.min.js" type="tet/javascript"></script>

    <script src="../Scripts/jquery.sexy-combo.js" type="text/javascript"></script>--%>
    <script type="text/javascript">

        var cboBranch = null;
        
        var store;

        Ext.onReady(
            function () {
                
            }
        );
        
 function SetEditorValue()
        {
            <%=hdnEditorDescription.ClientID%>.setValue(CKEDITOR.instances.ctl00_ContentPlaceHolder_Main_txtBody.getData());
        }
     
               
        
    </script>
    <style type="text/css">
        .dirty-row
        {
            background: #FFFDD8;
        }
        
        .new-row
        {
            background: #c8ffc8;
        }
        .divDueDate
        {
            background: silver;
        }
        .x-panel-header
        {
            background-image: none;
        }
        .x-panel-header-text-container-default
        {
            color: White !important;
        }
        .fieldTable td, table tr.up td
        {
            padding-top: 0px;
        }
        .x-toolbar
        {
            padding: inherit;
        }
        #ctl00_mainContent_txtBody-inputCmp-iframeEl
        {
            border: 1px solid lightgray;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <%--<ext:ResourceManager runat="server" ScriptMode="Debug" DisableViewState="false" ShowWarningOnAjaxFailure="false"
        Namespace="Rigo">--%>
    </ext:ResourceManager>
    <script type="text/javascript">
        Ext.onReady(
                function () {
                }
        );
    </script>

<div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Compose Notice
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <ext:Hidden ID="hdEmployees" runat="server" />
       
        <%-- <uc2:ExtMsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
        <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />--%>
        <table width="800px" style="padding-top: 10px; padding-left: 10px; clear: both">
            <tr>
                <td colspan="4" style="width: 600px !important;">
                    <ext:TextField ID="txtSubject" runat="server" LabelAlign="Top" Width="600px" FieldLabel="Notice Title *"
                        LabelSeparator="" />
                    <asp:RequiredFieldValidator ID="valSub1" Enabled="true" ControlToValidate="txtSubject"
                        runat="server" Display="None" ErrorMessage="Subject is required." ValidationGroup="message" />
                </td>
            </tr>
            <tr>
                <td colspan="1">
                    <div>
                        <table>
                            <tr>
                                <td>
                                    <ext:ComboBox ID="cmbNoticeCategory" runat="server" QueryMode="Local" ForceSelection="true"
                                        DisplayField="Name" ValueField="CategoryID" FieldLabel="Category *" LabelSeparator=""
                                        Width="200" LabelAlign="Top" Style="margin-bottom: 15px;">
                                        <Store>
                                            <ext:Store runat="server" ID="storeNoticeCate">
                                                <Model>
                                                    <ext:Model>
                                                        <Fields>
                                                            <ext:ModelField Name="Name" />
                                                            <ext:ModelField Name="CategoryID" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                    </ext:ComboBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Enabled="true" ControlToValidate="cmbNoticeCategory"
                                        runat="server" Display="None" ErrorMessage="Category is required" ValidationGroup="message" />
                                </td>
                                <td>
                                    <ext:LinkButton ID="btnAddCategory" runat="server" Text="Add Category" MarginSpec="0 0 0 25">
                                        <Listeners>
                                            <Click Handler="#{WinCategory}.show();" />
                                        </Listeners>
                                    </ext:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <%--<ext:HtmlEditor ID="txtBody" Border="true" runat="server" LabelAlign="Top" Width="600"
                        FieldLabel="Notice Body" Height="200">
                    </ext:HtmlEditor>--%>
                    <CKEditor:CKEditorControl ID="txtBody" Width="800px" Height="200px" runat="server"
                        BasePath="~/ckeditor/">
                    </CKEditor:CKEditorControl>
                    <ext:Hidden runat="server" ID="hdnEditorDescription" />
                </td>
            </tr>
            <tr>
                <td>
                    <ext:FileUploadField ID="fup" runat="server" ButtonText="Upload File" Icon="Add"
                        Width="300" Visible="true" ButtonOffset="1" MarginSpec="12 7 7 0">
                    </ext:FileUploadField>
                </td>
            </tr>
            <tr>
                <%-- <td align="right">--%>
                <td>
                    <p style="margin: 3px;">
                        Expiry Date</p>
                    <ext:DateField ID="dateDueDate" runat="server" LabelSeparator="">
                    </ext:DateField>
                </td>
            </tr>
            <tr>
                <td>
                    <ext:Button ID="btnSend" runat="server" Width="150" Height="30" Text="Publish Notice"
                        MarginSpec="25 0 0 0">
                        <Listeners>
                            <Click Handler="SetEditorValue(); valGroup='message';return CheckValidation();" />
                        </Listeners>
                        <DirectEvents>
                            <Click OnEvent="btnSend_Click">
                                <Confirmation Message="Are you sure you want to Publish this Notice?" ConfirmRequest="true">
                                </Confirmation>
                                <EventMask ShowMask="true" Msg="Publishing..." />
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </td>
                <td>
                    <ext:Button ID="btnDraft" runat="server" Width="150" Height="30" Text="Save as Draft"
                        MarginSpec="25 0 0 0">
                        <Listeners>
                            <Click Handler="SetEditorValue(); valGroup='message';return CheckValidation();" />
                        </Listeners>
                        <DirectEvents>
                            <Click OnEvent="btnSend_Click">
                                <EventMask ShowMask="true" Msg="Saving As Draft..." />
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </td>
            </tr>
        </table>
    </div>
    <ext:Window ID="WinCategory" MinWidth="450" MinHeight="200" Title="Add Category"
        Closable="true" Resizable="false" Hidden="true" runat="server" AutoScroll="true">
        <Content>
            <table>
                <tr>
                    <td colspan="2">
                        <ext:TextField runat="server" ID="txtCategoryName" FieldLabel="Category Name" LabelSeparator=""
                            LabelAlign="Top" MarginSpec="25 25 25 25" Width="350">
                        </ext:TextField>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Enabled="true" ControlToValidate="txtCategoryName"
                            runat="server" Display="None" ErrorMessage="Category Name is required." ValidationGroup="Cate" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td>
                                    <ext:Button ID="btnSaveCate" runat="server" Width="100" Height="30" Text="Send" MarginSpec="25 5 25 25">
                                        <Listeners>
                                            <Click Handler="valGroup='Cate';return CheckValidation();" />
                                        </Listeners>
                                        <DirectEvents>
                                            <Click OnEvent="btnSaveCategory_Click">
                                                <EventMask ShowMask="true" Msg="sending message..." />
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </td>
                                <td>
                                    <ext:LinkButton ID="btnCancel" runat="server" Text="Cancel" Cls="btnFlatLeftGap"
                                        BaseCls="btnFlatLeftGap" MarginSpec="25 25 25 25">
                                        <Listeners>
                                            <Click Handler="#{WinCategory}.hide();" />
                                        </Listeners>
                                    </ext:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </Content>
    </ext:Window>
</asp:Content>
