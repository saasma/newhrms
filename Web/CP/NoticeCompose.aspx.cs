﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using BLL;
using DAL;
using Ext.Net;
using Utils.Web;
using Utils.Helper;
using Utils;
using System.IO;
using Web.ReportDataSetTableAdapters;
using Web.CP.Report.Templates.Pay.Detail;
using BLL.BO;
using Newtonsoft.Json.Linq;

namespace Web.Employee
{
    public partial class NoticeCompose : BasePage
    {
        string userName = string.Empty;
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.HumanResource;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
             userName = SessionManager.UserName;
            if (!X.IsAjaxRequest)
            {
                Initialize();
            }
        }


        void Initialize()
        {
            List<NoticeCategory> categories = new List<NoticeCategory>();
            categories = NoticeManager.getAllNoticeCategories();

            storeNoticeCate.DataSource = categories;
            storeNoticeCate.DataBind();

            if(!string.IsNullOrEmpty(Request.QueryString["id"]))
            {
                int NoticeID = int.Parse(Request.QueryString["id"]);
                LoadEditData(NoticeID);

            }

        }


        public void LoadEditData(int NoticeID)
        {
            NoticeBoard notice = new NoticeBoard();
            notice = NoticeManager.getNoticeByID(NoticeID);
            if (notice != null)
            {
                txtSubject.Text = notice.Title;

                if (notice.CategoryID != null)
                {
                    //cmbNoticeCategory.SetValue(notice.CategoryID);
                    cmbNoticeCategory.SelectedItem.Value = notice.CategoryID.ToString();
                }

                txtBody.Text = notice.Body;
                if (notice.ExpiryDate != null)
                {
                    dateDueDate.SelectedDate = notice.ExpiryDate.Value;
                }

                fup.Text = notice.FileName;

                if (notice.Status > 0)
                {
                    btnDraft.Hidden = true;
                }
            }
        }

        protected void btnSend_Click(object sender, DirectEventArgs e)
        {

            if (string.IsNullOrEmpty(txtSubject.Text.Trim()))
            {
                NewMessage.ShowWarningMessage("Notice Title is required.");
                return;
            }

            if (string.IsNullOrEmpty(hdnEditorDescription.Text.Trim()))
            {
                NewMessage.ShowWarningMessage("Notice Body is required.");
                return;
            }

            if (cmbNoticeCategory.SelectedItem == null || cmbNoticeCategory.SelectedItem.Index == -1 || cmbNoticeCategory.SelectedItem.Value==null)
            {
                NewMessage.ShowWarningMessage("Category is required.");
                return;
            }

            NoticeBoard noticeboard = new NoticeBoard();

            if (!string.IsNullOrEmpty(Request.QueryString["id"]))
            {
                noticeboard.NoticeID = int.Parse(Request.QueryString["id"]);
            }

            noticeboard.Body = hdnEditorDescription.Text.Trim();
            if(cmbNoticeCategory.SelectedItem!=null && cmbNoticeCategory.SelectedItem.Index!=-1)
                noticeboard.CategoryID = int.Parse(cmbNoticeCategory.SelectedItem.Value);

            noticeboard.CreatedBy = SessionManager.CurrentLoggedInUserID;
            noticeboard.CreatedOn = CommonManager.GetCurrentDateAndTime();

            if (dateDueDate.SelectedDate != DateTime.MinValue)
            {
                noticeboard.ExpiryDate = dateDueDate.SelectedDate;
            }

            Ext.Net.Button button = (Ext.Net.Button)sender;
            if (button.ID == "btnSend")
            {
                noticeboard.Status = (int)NoticeBoardStatusEnum.Published;
                noticeboard.PublishDate = SessionManager.GetCurrentDateAndTime();
            }
            else
                noticeboard.Status = (int)NoticeBoardStatusEnum.Draft;

            noticeboard.Title = txtSubject.Text;


            if (fup.HasFile )
            {
                string ext = Path.GetExtension(fup.FileName);
                string guidFileName = Guid.NewGuid().ToString() + ext;
                string saveLocation = Path.Combine(Server.MapPath(Config.NoticeLocation), guidFileName);
                string thumbnailLocation = Path.Combine(Server.MapPath(Config.NoticeLocation), Guid.NewGuid().ToString() + ext);

                //UploadType type = (UploadType)1;
                //string code = "closeWindow(\"{0}\");";
                NewHRManager mgr = new NewHRManager();

                try
                {
                    //

                    guidFileName = guidFileName.Replace(Path.GetExtension(guidFileName), ".ashx");
                    saveLocation = saveLocation.Replace(Path.GetExtension(saveLocation), ".ashx");



                    Utils.Security.EncryptorDecryptor enc = new Utils.Security.EncryptorDecryptor();
                    enc.Encrypt(fup.FileContent, saveLocation);

                    noticeboard.FileName = Path.GetFileName(fup.FileName);


                    noticeboard.Description = Path.GetFileNameWithoutExtension(fup.FileName);

                    noticeboard.FileType = fup.PostedFile.ContentType;
                    noticeboard.URL = guidFileName;
                    noticeboard.Size = GetSize(fup.PostedFile.ContentLength);


                }
                catch (Exception exp1)
                {
                    X.Msg.Alert("", "There was Problem While Uploading File").Show();
                }

            }
            else
            {
 
            }


            Status status = new Status();
            status = NoticeManager.InsertUpdateNotice(noticeboard);
            if (status.IsSuccess)
            {

                if (noticeboard.Status > (int)NoticeBoardStatusEnum.Draft)
                {
                    Response.Redirect("NoticeList.aspx");
                }
                else
                {
                    Response.Redirect("NoticeCompose.aspx?id="+noticeboard.NoticeID.ToString());
                }
                //X.Msg.Alert("", "Notice Saved").Show();
                //ClearFields();
            }
            else
            {
                X.Msg.Alert("", status.ErrorMessage).Show();
            }

            

        }

        void ClearFields()
        {
            txtSubject.Clear();
            cmbNoticeCategory.Clear();
            txtBody.Text = "";
            //txtBody.Clear();
            fup.Clear();
            dateDueDate.Clear();
        }

        protected void btnSaveCategory_Click(object sender, DirectEventArgs e)
        {
            string CategoryName = txtCategoryName.Text.Trim();

            NoticeCategory noticeCate = new NoticeCategory();
            if (!string.IsNullOrEmpty(CategoryName))
            {
                noticeCate.Name = CategoryName;

                Status status = new Status();
                status = NoticeManager.AddUpdateNoticeCategory(noticeCate);
                if (status.IsSuccess)
                {
                    List<NoticeCategory> categories = new List<NoticeCategory>();
                    categories = NoticeManager.getAllNoticeCategories();

                    storeNoticeCate.DataSource = categories;
                    storeNoticeCate.DataBind();

                    X.Msg.Alert("", "Category Added").Show();
                    WinCategory.Hide();

                    //cmbNoticeCategory.SelectedItem.Value = noticeCate.CategoryID.ToString();
                    cmbNoticeCategory.SetValue(noticeCate.CategoryID);

                }
                else
                {
                    X.Msg.Alert("", status.ErrorMessage).Show();
                }

            }
        }




        public static int ConvertBytesIntoKB(int byteValue)
        {
            double mb = (byteValue / 1024);
            if (mb.ToString("#") == "")
                return 0;
            return Convert.ToInt32(mb.ToString("#"));
        }
        public static string ConvertKBIntoMB(object value)
        {
            long kb = Convert.ToInt64(value);
            double mb = (kb / 1024.0);
            return mb.ToString("#.#");
        }

        public string GetSize(int byteValue)
        {
            if (byteValue < 1024)
                return "1 kb";

            double kb = ConvertBytesIntoKB(byteValue);
            if (kb > 1024)
            {
                return ConvertKBIntoMB(kb) + " mb";
            }
            else
            {
                return kb + " kb";
            }
        }
    }
}
