<%@ Page Title="Optimum PF and CIT" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="OxfamOptimumPFAndCIT.aspx.cs" Inherits="Web.CP.OxfamOptimumPFAndCIT" %>

<%@ Register Src="~/Controls/FilterCtl.ascx" TagName="FilterCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/CalendarCtl.ascx" TagPrefix="uc" TagName="Calendar" %>
<%@ Register Src="../Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="../Scripts/jquery-1.3.2.min.js"></script>
    <script src="../Scripts/currency.js" type="text/javascript"></script>
    <script type="text/javascript">




        $(document).ready(function () {


        });


        function selectDeselectAll(chk) {
            $('input[type=checkbox]').each(
                    function (index) {
                        if ($(this).prop('disabled') == false && this.id.indexOf('chkApply') > 0)
                            this.checked = chk.checked;
                    }
                );
        }

        function refreshWindow() {
            __doPostBack('__EVENTTARGENT', 'ctl00$ContentPlaceHolderMain$ctl00$btnLoads');
        }
        function importPopupProcess(btn) {




            var ret = importPopup();


            return false;
        }

           function processEdit(ein,desired) {
        <%=hdnEmployeeId.ClientID %>.setValue(ein);
        
        <%=hiddenMaxAllowed.ClientID %>.setValue(desired);
        <%= btnLoadHead.ClientID %>.fireEvent('click');
    }

    </script>
    <style type="text/css">
        .increment
        {
        }
        .tableLightColor a:hover
        {
            color: #048FC2;
            text-decoration: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <ext:ResourceManager runat="server" ID="res" DisableViewState="false" />
    <asp:HiddenField ID="hdIncreaseType" runat="server" />
    <asp:HiddenField ID="hdIncomeType" runat="server" />
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Optimum PF And CIT
                </h4>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hiddenEmployeeID" runat="server" />
    <ext:Hidden ID="hiddenMaxAllowed" runat="server" />
    <ext:Button ID="btnLoadHead" Hidden="true" runat="server" Cls="btn btn-success" Text="LoadHead"
        Width="120">
        <DirectEvents>
            <Click OnEvent="btnLoadHead_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>
    <div class="contentpanel">
        <uc2:FilterCtl Designation="false" WorkShift="false" SearchEmployeeTextboxAutoPostback="false"
            Id="filterCtl" OnFilterChanged="ReBindEmployees" EmployeeName="true" CITRoundOff="false"
            runat="server" />

            <asp:Button ID="btnLoad" Visible="false" 
                CssClass="btn btn-warning btn-sect btn-sm" runat="server" Text="Load"
                ValidationGroup="Balance" OnClick="btnLoad_Click" />

        <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
        <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
        <div class="clear" style="margin-top: 10px; margin-bottom: 0px;">
            <cc2:EmptyDisplayGridView CssClass="table table-primary mb30 table-bordered table-hover"
                Style="margin-bottom: 0px" UseAccessibleHeader="true" DataKeyNames="OptimumId,EID,PayrollPeriodId"
                GridLines="None" ID="gvwEmployeeList" runat="server" ShowFooterWhenEmpty="False"
                ShowHeaderWhenEmpty="True" AutoGenerateColumns="False" OnPageIndexChanging="gvwDepartments_PageIndexChanging">
                <Columns>
                    <asp:BoundField DataField="EID" HeaderText="EIN" ItemStyle-HorizontalAlign="Right"
                        HeaderStyle-Width="30px" />
                    <asp:BoundField DataField="IdCardNo" HeaderText="I No" ItemStyle-HorizontalAlign="Right"
                        HeaderStyle-Width="60px" />
                    <asp:TemplateField HeaderText="&nbsp;Name" HeaderStyle-Width="180px" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:HyperLink CssClass="employeeClass" Target="_blank" Width="150px" ID="Label2"
                                runat="server" NavigateUrl='<%# "~/newhr/EmployeePayroll.aspx?Id=" +  Eval("EID") %>'
                                Text='<%# "&nbsp;" + Eval("Name") %>'></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Income PF" ItemStyle-HorizontalAlign="Right" HeaderStyle-Width="70px">
                        <ItemTemplate>
                            <asp:Label ID="lblEmpPF" runat="server" Text='<%# Eval("EmployeePFAmount","{0:N2}") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Current PF Deduction" ItemStyle-HorizontalAlign="Right"
                        HeaderStyle-Width="70px">
                        <ItemTemplate>
                            <asp:Label ID="lblCIT" runat="server" Text='<%# Eval("CurrentDeductionPF","{0:N2}") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Current CIT" ItemStyle-HorizontalAlign="Right" HeaderStyle-Width="70px">
                        <ItemTemplate>
                            <asp:Label ID="lblCIT2" runat="server" Text='<%# Eval("CITAmount","{0:N2}") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Max Allowed" ItemStyle-HorizontalAlign="Right" HeaderStyle-Width="80px">
                        <ItemTemplate>
                            <asp:Label ID="lblAllowed" runat="server" Text='<%# Eval("Allowed","{0:N2}") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Desired Contribution" ItemStyle-HorizontalAlign="Right"
                        HeaderStyle-Width="80px">
                        <ItemTemplate>
                            <asp:Label ID="lblAllowed1" runat="server" Style='<%# Convert.ToDecimal(Eval("Allowed").ToString()) == Convert.ToDecimal(Eval("DesiredAllowed").ToString())  ? "": "color:#E88117" %>'
                                Text='<%# Eval("DesiredAllowed","{0:N2}") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Past RF Contribution" ItemStyle-HorizontalAlign="Right"
                        HeaderStyle-Width="80px">
                        <ItemTemplate>
                            <asp:Label ID="lblAdditional" runat="server" Text='<%# Eval("PastPFAndCIT","{0:N2}") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Remaining Allowed" ItemStyle-HorizontalAlign="Right"
                        HeaderStyle-Width="80px">
                        <ItemTemplate>
                            <asp:Label ID="lblAdditional" runat="server" Text='<%# Eval("RemainingAllowed","{0:N2}") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Remaining Months" ItemStyle-HorizontalAlign="Right"
                        HeaderStyle-Width="80px">
                        <ItemTemplate>
                            <asp:Label ID="lblRem" runat="server" Text='<%# Eval("RemainingMonths","{0:N2}") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Monthly Optimum RF" ItemStyle-HorizontalAlign="Right"
                        HeaderStyle-Width="80px">
                        <ItemTemplate>
                            <asp:Label ID="lblRem" runat="server" Text='<%# Eval("MonthlyOptimumRF","{0:N2}") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="PF Percentage" ItemStyle-HorizontalAlign="Right" HeaderStyle-Width="80px">
                        <ItemTemplate>
                            

                             <asp:Label ID="lblRem12" runat="server" Style='<%# Convert.ToDecimal(Eval("PFPercentage")) ==60  ? "": "color:#E88117" %>'
                                Text='<%# Eval("PFPercentage","{0:N2}") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="PF Amount" ItemStyle-HorizontalAlign="Right" HeaderStyle-Width="80px">
                        <ItemTemplate>
                           

                            <asp:Label ID="lblNewPF" runat="server" Style='<%# Convert.ToDecimal(Eval("NewPF").ToString()) == Convert.ToDecimal(Eval("CurrentDeductionPF").ToString())  ? "": "color:#E88117" %>'
                                Text='<%# Eval("NewPF","{0:N2}") %>' />

                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="CIT Amount" ItemStyle-HorizontalAlign="Right" HeaderStyle-Width="80px">
                        <ItemTemplate>
                        

                            <asp:Label ID="lblNewCIT" runat="server" Style='<%# Convert.ToDecimal(Eval("NewCIT").ToString()) == Convert.ToDecimal(Eval("CITAmount").ToString())  ? "": "color:#E88117" %>'
                                Text='<%# Eval("NewCIT","{0:N2}") %>' />

                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="&nbsp;"  ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:HyperLink CssClass="employeeClass" Target="_blank" Width="50px" ID="Label21"
                                Style='border-right: 1px solid #DDDDDD' runat="server" NavigateUrl='javascript:void(0)'
                                onclick='<%# "processEdit(" + Eval("EID") + "," + Eval("Allowed")  + ");return false;"%>' Text='Edit'></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                <EmptyDataTemplate>
                    <b>No Employees are found</b>
                </EmptyDataTemplate>
            </cc2:EmptyDisplayGridView>
            <uc1:PagingCtl ID="pagingCtl" ShowDropDown="true" OnChangePage="ChangePageNumber"
                OnNextRecord="btnNext_Click" OnPrevRecord="btnPrevious_Click" OnDropDownShowRecordsChanged="ddlRecords_SelectedIndexChanged"
                runat="server" />
        </div>
        <div class="buttonsDiv">
           <%--  <asp:LinkButton ID="LinkButton1" runat="server" Text="Import PF" OnClientClick="pfImportPopup();return false;"
                            CssClass=" excel marginRight tiptip" Style="float: right;" />--%>
            <asp:LinkButton ID="btnImportPopup" runat="server" Text="Import Desired Contribution"
                CssClass=" excel marginRight tiptip" Style="float: right; width: 200px" OnClientClick="importPopupProcess();return false;" />
            <asp:LinkButton ID="btnExport" runat="server" Text="Export to Excel" OnClick="btnExport_Click"
                CssClass=" excel marginRight" Style='float: right; margin-right: 0px' />
            <asp:Button ID="btnUpdatePFAndCIT" style='float:left' OnClientClick="return confirm('Confirm update PF and CIT?');"
                CssClass="btn btn-warning btn-sect btn-sm" runat="server" Text="Update PF and CIT"
                ValidationGroup="Balance" OnClick="btnUpdatePFAndCIT_Click" />
        </div>
    </div>
    <ext:Window ID="window" runat="server" ButtonAlign="Left" Title="Edit Amount" Icon="Application"
        Width="480" Height="250" BodyPadding="15" Hidden="true" Modal="true">
        <Content>
            <table class="fieldTable">
                <tr>
                    <td>
                        <ext:DisplayField ID="lblEmployee" LabelSeparator="" FieldStyle='font-size:16px;color:#428BCA;'
                            FieldLabel="" LabelAlign="Top" runat="server" Width="200px" />
                        <ext:Hidden runat="server" ID="hdnEmployeeId" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:DisplayField ID="dispMaxAllowed" LabelSeparator=""  LabelWidth="130" FieldLabel="Max Allowed"
                            LabelAlign="Left" runat="server" Width="300px" />

                    </td>
                </tr>
                <tr>
                    <td style='padding-top: 10px'>
                        <ext:TextField ID="txtNewAmount" LabelSeparator="" LabelWidth="130"  FieldLabel="Desired Contribution" LabelAlign="Left"
                            runat="server" Width="300px">
                        </ext:TextField>
                        <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator1" runat="server"
                            ValidationGroup="SaveAmount" ControlToValidate="txtNewAmount" ErrorMessage="Please place amount." />
                        <asp:CompareValidator Display="None" ID="RequiredFieldValidator22" runat="server"
                            ValidationGroup="SaveAmount" ControlToValidate="txtNewAmount" ErrorMessage="Please place valid amount."
                            Type="Double" Operator="DataTypeCheck" />
                    </td>
                </tr>
                <tr>
                    <td style='padding-top: 10px'>
                        <ext:TextField ID="txtNewPFPercent" LabelSeparator="" LabelWidth="130"  FieldLabel="PF Percent" LabelAlign="Left"
                            runat="server" Width="300px" >
                        </ext:TextField>
                       
                        <asp:RangeValidator Display="None" ID="CompareValidator1" runat="server"
                            ValidationGroup="SaveAmount" ControlToValidate="txtNewPFPercent" ErrorMessage="Please place pf percent."
                            Type="Double"  MinimumValue="0" MaximumValue="100"/>
                    </td>
                </tr>
            </table>
        </Content>
        <Buttons>
            <ext:Button runat="server" AutoPostBack="true" OnClick="btnChangeAmount_Click" ID="btnChangeAmount" Cls="btn btn-primary" Text="<i></i>Change"
                runat="server">
              
                <Listeners>
                    <Click Handler="valGroup = 'SaveAmount'; return CheckValidation();">
                    </Click>
                </Listeners>
            </ext:Button>
            <ext:LinkButton runat="server" Cls="btnFlatLeftGap" ID="LinkButton4" Text="<i></i>Cancel"
                runat="server">
                <Listeners>
                    <Click Handler="#{window}.hide();">
                    </Click>
                </Listeners>
            </ext:LinkButton>
        </Buttons>
    </ext:Window>
</asp:Content>
