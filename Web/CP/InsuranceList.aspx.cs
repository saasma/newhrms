﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using System.Text;
using BLL.Manager;
using Utils.Helper;
using DAL;
using System.Drawing;
using BLL.Base;


namespace Web.CP
{
    public partial class InsuranceList : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.Payroll;
            }
        }    

        TaxManager taxMgr = new TaxManager();
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
                {
                    int empId = int.Parse(Request.QueryString["ID"]);
                    string name = EmployeeManager.GetEmployeeName(empId);
                    txtEmpSearchText.Text = name;
                }

                pagingCtl.Visible = false;
                Initialise();

                SessionManager.CurrentCompanyFinancialDate.SetName(IsEnglish);
            }


            if (Request.Form["__EVENTTARGET"] != null && Request.Form["__EVENTTARGET"].Equals("Reload"))
                LoadTaxCreditList();
            
            //JavascriptHelper.AttachPopUpCode(Page, "medicalTaxCreditPopup", "AEMedicalTax.aspx", 600, 590);
            //JavascriptHelper.AttachNonDialogPopUpCode(Page, "medicalTaxImportPopup", "../ExcelWindow/MedicalTaxDetailExcel.aspx", 450, 500);
            JavascriptHelper.AttachNonDialogPopUpCode(Page, "shiftPopup", "../ExcelWindow/InsuranceExcel.aspx", 450, 500);
       
        }

        void Initialise()
        {
            List<FinancialDate> financialYearList = new CommonManager().GetAllFinancialDates();
            foreach (FinancialDate date in financialYearList)
            {
                date.SetName(IsEnglish);
            }

            ddlFinancialYear.DataSource = financialYearList;
            ddlFinancialYear.DataBind();

            ddlFinancialYear.SelectedIndex = financialYearList.Count - 1;

            int empId = UrlHelper.GetIdFromQueryString("EmpId");
            if (empId != 0)
            {
                EEmployee emp = EmployeeManager.GetEmployeeById(empId);
                if (emp != null)
                {
                    txtEmpSearchText.Text = emp.Name;
                    btnLoad_Click(null, null);
                }
            }
            else
            {
                btnLoad_Click(null, null);
            }

            
        }

        void LoadTaxCreditList()
        {
            int totalRecords = 0;
            List<IIndividualInsurance> list = InsuranceManager.GetCurrentYearInsuranceList
                (int.Parse(ddlFinancialYear.SelectedValue), pagingCtl.CurrentPage - 1, int.Parse(pagingCtl.DDLRecords.SelectedValue), ref totalRecords,txtEmpSearchText.Text.Trim(),0);
                
            //    taxMgr.GetMedicalTaxCreditList(
            //    SessionManager.CurrentCompanyId, txtEmpSearchText.Text.Trim(), pagingCtl.CurrentPage-1, int.Parse(pagingCtl.DDLRecords.SelectedValue), ref totalRecords);
            gvwList.DataSource = list;
            gvwList.DataBind();
            if (totalRecords == 0)
                pagingCtl.Visible = false;
            else
                pagingCtl.Visible = true;
            pagingCtl.UpdatePagingBar(totalRecords);

            //int totalRecords = 0;
            //gvwList.DataSource = ProjectManager.GetProjectList(
            //    GetEngDate(calFilterFrom.SelectedDate.ToString()), GetEngDate(calFilterTo.SelectedDate.ToString()),
            //    chkAllProjects.Checked, txtSearch.Text.Trim(), pagintCtl.CurrentPage, int.Parse(pagintCtl.DDLRecords.SelectedValue), ref totalRecords);


            //gvwList.DataBind();


            //if (list.Count > 0)
            //{
                
            //    bool newFound = false;
            //    //foreach (GetMedicalTaxCreditListResult item in list)
            //    //{
            //    //    if (item.MedicalTaxCreditId == 0)
            //    //    {
            //    //        newFound = true;
            //    //        break;
            //    //    }
            //    //}
            //    //if (newFound)
            //    //    btnSave.Visible = true;
            //    //else
            //    //    btnSave.Visible = false;
            //}
            //else
            //    btnSave.Visible = false;


        }

        protected void btnLoad_Click(object sender, EventArgs e)
        {
            pagingCtl.CurrentPage = 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage -= 1;
            LoadTaxCreditList();;
        }

       


        public bool IsVisible(object val)
        {
            if (val == null)
                return false;
            int mId = int.Parse(val.ToString());
            if (mId == 0)
                return false;
            return true;
        }

        protected void gvwList_RowCreated(object sender, GridViewRowEventArgs e)        
        {
           

        }

        protected void txtEmpSearchText_TextChanged(object sender, EventArgs e)
        {
            LoadTaxCreditList();
        }

        protected void btnPrevious_Click()
        {
            pagingCtl.CurrentPage -= 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage -= 1;
            LoadTaxCreditList();

        }

        protected void btnNext_Click()
        {
            pagingCtl.CurrentPage += 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage += 1;
            LoadTaxCreditList();
        }

        protected void ddlRecords_SelectedIndexChanged()
        {
            //SessionManager.EmployeeDisplayRecodsPerPage = int.Parse(pagintCtl.DDLRecords.SelectedValue);
            //_tempCurrentPage = 1;
            pagingCtl.CurrentPage = 1;
            LoadTaxCreditList();
        }
    }
}
