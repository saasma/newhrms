<%@ Page Title="Manage departments" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="ManageDep.aspx.cs" Inherits="Web.CP.ManageDep" %>

<%@ Register Src="../UserControls/MGDepartment.ascx" TagName="MGDepartment" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Departments in your organization
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <ext:ResourceManager ID="ResourceManager1" DisableViewState="false" runat="server"
            ShowWarningOnAjaxFailure="false" ScriptMode="Release" Namespace="Rigo" />
        <div>
            <uc1:MGDepartment Id="MGDepartment1" runat="server" />
        </div>
    </div>
</asp:Content>
