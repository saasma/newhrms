﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;
using System.Text;
using Utils.Helper;
using Utils.Calendar;

namespace Web.CP
{
    public partial class EmployeeAwardList : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.AdministratorTools;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }

            if (Request.Form["__EVENTTARGET"] != null && Request.Form["__EVENTTARGET"].ToLower().Contains("refresh"))
            {
                BindGrid();
            }

            JavascriptHelper.AttachNonDialogPopUpCode(Page, "awardImportPopup", "../ExcelWindow/EmployeeAwardExcel.aspx", 450, 500);

            
        }

        private void Initialise()
        {
            cmbAwardName.Store[0].DataSource = AwardManager.GetAllEmployeeCashAwardType();
            cmbAwardName.Store[0].DataBind();

            cmbAwardImport.Store[0].DataSource = AwardManager.GetAllEmployeeCashAwardType();
            cmbAwardImport.Store[0].DataBind();

            BindGrid();
        }

        private void ClearFields()
        {
            cmbAwardName.Text = "";
            cmbAwardCalcaulation.Text = "";
            txtMonthsBasicSalary.Text = "";
            txtPercentageOfBasicSalary.Text = "";
            txtPayMonths.Text = "";
            txtFixedAmount.Text = "";
            txtMonthsBasicSalary.Hide();
            txtPercentageOfBasicSalary.Hide();
            txtFixedAmount.Hide();
            lblMonths.Hide();
            txtEmployeeName.Text = "";
            txtEmployeeName.Hide();
            txtNote.Text = "";
            cmbSearch.Show();
            btnSave.Text = Resources.Messages.Save;
        }

        private void BindGrid()
        {
            gridAward.Store[0].DataSource = AwardManager.GetEmployeeCashAwardList();
            gridAward.Store[0].DataBind();
        }

        protected void btnAddNew_Click(object sender, DirectEventArgs e)
        {
            ClearFields();
            hdnCashAwardTypeId.Text = "";
            hdnEmployeeId.Text = "";
            hdnMonth.Text = "";
            hdnYear.Text = "";
            WEmployeeAward.Show();
        }
     
        protected void cmbAwardCalcaulation_Change(object sender, DirectEventArgs e)
        {
            if (string.IsNullOrEmpty(cmbAwardCalcaulation.Text))
                return;
            int awardCalculationType = int.Parse(cmbAwardCalcaulation.SelectedItem.Value);
            if (awardCalculationType == (int)AwardCalculationEnum.MonthsBasicSalary)
            {
                txtMonthsBasicSalary.Show();
                lblMonths.Show();
                txtPercentageOfBasicSalary.Hide();
                txtFixedAmount.Hide();
            }
            else if (awardCalculationType == (int)AwardCalculationEnum.PercentageOfBasicSalary)
            {
                txtMonthsBasicSalary.Hide();
                lblMonths.Hide();
                txtPercentageOfBasicSalary.Show();
                txtFixedAmount.Hide();
            }
            else if (awardCalculationType == (int)AwardCalculationEnum.FixedAmount)
            {
                txtMonthsBasicSalary.Hide();
                lblMonths.Hide();
                txtPercentageOfBasicSalary.Hide();
                txtFixedAmount.Show();
            }
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveEmpAward");
            if (Page.IsValid)
            {
                List<EmployeeCashAward> list = new List<EmployeeCashAward>();

                EmployeeCashAward obj = new EmployeeCashAward();
                obj.CashAwardTypeId = int.Parse(cmbAwardName.SelectedItem.Value);

                if (!string.IsNullOrEmpty(hdnEmployeeId.Text))
                    obj.EmployeeId = int.Parse(hdnEmployeeId.Text);
                else
                {
                    if (string.IsNullOrEmpty(cmbSearch.Text))
                    {
                        NewMessage.ShowWarningMessage("Employee is required.");
                        return;
                    }
                    obj.EmployeeId = int.Parse(cmbSearch.SelectedItem.Value);
                }

                EmployeeCashAwardType obdbEmpCashAwardType = AwardManager.GetEmployeeCashAwardTypeById(obj.CashAwardTypeId);


                int calculationType = int.Parse(cmbAwardCalcaulation.SelectedItem.Value);
                obj.CalculationType = calculationType;

                if (calculationType == (int)AwardCalculationEnum.MonthsBasicSalary)
                {
                    if (string.IsNullOrEmpty(txtMonthsBasicSalary.Text.Trim()))
                    {
                        NewMessage.ShowWarningMessage("Months Basic Salary is required.");
                        txtMonthsBasicSalary.Focus();
                        return;
                    }

                    obj.AmountOrRate = decimal.Parse(txtMonthsBasicSalary.Text.Trim());
                }
                else if (calculationType == (int)AwardCalculationEnum.PercentageOfBasicSalary)
                {
                    if (string.IsNullOrEmpty(txtPercentageOfBasicSalary.Text.Trim()))
                    {
                        NewMessage.ShowWarningMessage("Percentage of Basic Salary is required.");
                        txtPercentageOfBasicSalary.Focus();
                        return;
                    }

                    obj.AmountOrRate = decimal.Parse(txtPercentageOfBasicSalary.Text.Trim());
                }
                else if (calculationType == (int)AwardCalculationEnum.FixedAmount)
                {
                    if (string.IsNullOrEmpty(txtFixedAmount.Text.Trim()))
                    {
                        NewMessage.ShowWarningMessage("Fixed Amount is required.");
                        txtFixedAmount.Focus();
                        return;
                    }
                    obj.AmountOrRate = decimal.Parse(txtFixedAmount.Text.Trim());
                }

                if (string.IsNullOrEmpty(txtPayMonths.Text.Trim()))
                {
                    NewMessage.ShowWarningMessage("Pay Month is required.");
                    txtPayMonths.Focus();
                    return;
                }

                CustomDate date = CustomDate.GetCustomDateFromString(txtPayMonths.Text.Trim(), IsEnglish);
                obj.Month = date.Month;
                obj.Year = date.Year;
                obj.Date = date.ToString();
                obj.DateEng = BLL.BaseBiz.GetEngDate(obj.Date, IsEnglish);
                obj.CreatedBy = SessionManager.CurrentLoggedInUserID;
                obj.CreatedOn = DateTime.Now;
                obj.Notes = txtNote.Text.Trim();

                list.Add(obj);

                int count = 0;
                Status status = AwardManager.SaveUpdateEmployeeAward(list, ref count);
                if (status.IsSuccess)
                {
                    if (string.IsNullOrEmpty(hdnEmployeeId.Text))
                        NewMessage.ShowNormalMessage("Record saved successfully.");
                    else
                        NewMessage.ShowNormalMessage("Record updated successfully.");
                    BindGrid();
                    WEmployeeAward.Close();
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }
            }
        }

        protected void btnImport_Click(object sender, DirectEventArgs e)
        {
            cmbAwardImport.ClearValue();
            cmbCalculationTypeImport.ClearValue();
            WAwardImport.Show();
        }

        protected void btnEdit_Click(object sender, DirectEventArgs e)
        {
            int employeeId = int.Parse(hdnEmployeeId.Text);
            int cashAwardTypeId = int.Parse(hdnCashAwardTypeId.Text);
            int month = int.Parse(hdnMonth.Text);
            int year = int.Parse(hdnYear.Text);

            ClearFields();

            EmployeeCashAward obj = AwardManager.GetEmployeeCashAwardById(employeeId, cashAwardTypeId, month, year);
            if (obj != null)
            {

                //cmbSearch.Disable(true);

                //cmbSearch.ClearValue();

                //Ext.Net.ListItem cItem = new Ext.Net.ListItem();
                //cItem.Text = EmployeeManager.GetEmployeeById(obj.EmployeeId).Name;
                //cItem.Value = obj.EmployeeId.ToString();
                //cItem.Index = 0;

                //cmbSearch.Items.Add(cItem);
                //cmbSearch.SelectedItems.Add(cItem);
                //cmbSearch.SetValue(obj.EmployeeId.ToString());
                //cmbSearch.Text = cItem.Text;

                cmbSearch.Hide();
                txtEmployeeName.Text = EmployeeManager.GetEmployeeById(obj.EmployeeId).Name;
                txtEmployeeName.Show();

                cmbAwardName.SetValue(obj.CashAwardTypeId.ToString());

                cmbAwardCalcaulation.SetValue(obj.CalculationType.ToString());

                if (obj.CalculationType == (int)AwardCalculationEnum.MonthsBasicSalary)
                {
                    txtMonthsBasicSalary.Show();
                    txtMonthsBasicSalary.Text = decimal.Parse(obj.AmountOrRate.ToString()).ToString("N2");
                    lblMonths.Show();
                }
                else if (obj.CalculationType == (int)AwardCalculationEnum.PercentageOfBasicSalary)
                {
                    txtPercentageOfBasicSalary.Show();
                    txtPercentageOfBasicSalary.Text = decimal.Parse(obj.AmountOrRate.ToString()).ToString("N2");
                }
                else if (obj.CalculationType == (int)AwardCalculationEnum.FixedAmount)
                {
                    txtFixedAmount.Show();
                    txtFixedAmount.Text = decimal.Parse(obj.AmountOrRate.ToString()).ToString("N2");
                }

                txtPayMonths.Text = obj.Date;

                txtNote.Text = obj.Notes;

                btnSave.Text = Resources.Messages.Update;
                WEmployeeAward.Show();

            }
        }

        protected void btnDelete_Click(object sender, DirectEventArgs e)
        {
            int employeeId = int.Parse(hdnEmployeeId.Text);
            int cashAwardTypeId = int.Parse(hdnCashAwardTypeId.Text);
            int month = int.Parse(hdnMonth.Text);
            int year = int.Parse(hdnYear.Text);

            Status status = AwardManager.DeleteEmployeeCashAward(employeeId, cashAwardTypeId, month, year);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Record deleted successfully.");
                BindGrid();
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }
    }
}