<%@ Page MaintainScrollPositionOnPostback="true" Title="Branch Transfer Leave Settings"
    Language="C#" MasterPageFile="~/Master/Details.Master" AutoEventWireup="true"
    CodeBehind="BranchTransferLeaveSetting.aspx.cs" Inherits="Web.CP.BranchTransferLeaveSetting" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .x-grid3-hd
        {
            background-color: #C5E2FF !important;
        }
        .x-grid3-hd-row
        {
            border-left-color: #A5DEFA;
            border-right-color: #A5DEFA;
        }
        .x-grid3-hd-inner
        {
            color: Black;
        }
        tbody .odd td
        {
            padding: 0px !important;
        }
        tbody .even td
        {
            padding: 0px !important;
        }
        .tableLightColor > tbody > tr > td
        {
            padding: 3px !important;
        }
        .itemList
        {
            margin: 0px;
        }
        .itemList li
        {
            float: left;
            list-style-type: none;
        }
        .bodypart
        {
            width: 1400px;
        }
        .labelBold
        {
            font-weight: bold !important;
            font-size: 14px !important;
        }
    </style>
    <%--  <script src="../Scripts/jquery.sexy-combo.pack.js" type="text/javascript"></script>

   <script src="../Scripts/jquery.sexy-combo.min.js" type="tet/javascript"></script>

    <script src="../Scripts/jquery.sexy-combo.js" type="text/javascript"></script>--%>
    <script type="text/javascript">
      
      var DisplayEmail = function (store,value,email) {
            //var value = cmb.getValue();
           
            var r = store.getById(value);
//            if (Ext.isEmpty(r)) {
//                return "";
//            }

            switch (store.storeId.toLowerCase()) {
                case "ctl00_maincontent_storeappliesto":
                    //email = Ext.get(this.store.storeId.replace("storeAppliesTo", "lblEmail1"));
                    if (Ext.isEmpty(r)) {
                        email.setText("");
                        return "";
                    }
                    email.setText(r.data.Email);
                    
                    break;
                case "ctl00_maincontent_storecc1":
                    //email = Ext.get(this.store.storeId.replace("cboCC1", "lblEmail2"));
                    if (Ext.isEmpty(r)) {
                        email.setText("");
                        return "";
                    }
                    email.setText(r.data.Email);
                    break;
                case "ctl00_maincontent_storecc2":
                    //email = Ext.get(this.store.storeId.replace("cboCC2", "lblEmail3"));
                    if (Ext.isEmpty(r)) {
                        email.setText("");
                        return "";
                    }
                    email.setText(r.data.Email);
                    break;
                 case "ctl00_maincontent_storecc3":                   
                    if (Ext.isEmpty(r)) {
                        email.setText("");
                        return "";
                    }
                    email.setText(r.data.Email);
                    break;
                  case "ctl00_maincontent_storecc4":                   
                    if (Ext.isEmpty(r)) {
                        email.setText("");
                        return "";
                    }
                    email.setText(r.data.Email);
                    break;
                 case "ctl00_maincontent_storecc5":                   
                    if (Ext.isEmpty(r)) {
                        email.setText("");
                        return "";
                    }
                    email.setText(r.data.Email);
                    break;
                default: break;
            }
            

            //alert(r.data.Email);


        };

        var valGroup;
        var valControl;
        // validate all controls of the current validation group &
        //show message
        function Validate() {

            if (typeof (Page_Validators) == 'undefined')
                return true;

            for (var ii = 0; ii < Page_Validators.length; ii++) {
                var val = Page_Validators[ii];
                valControl = null;
                if (val.validationGroup == valGroup) {

                    ValidatorEnable(val);          
                        if (val.isvalid == false) {

                            valControl = val.controltovalidate;
                            alert(val.errormessage);
                            return false;
                        }
                }
            }
            return true;
        }

            function ValidationInAction(group)
            {
                if(confirm('Do you really want to save data?'))
                { 
                    valGroup=group;                    
                    return Validate();                    
                }
                else 
                {                    
                    return false;
                }

            }
             var CommandHandler = function(command, record){

                if(command=="Delete")
                {
                    
                    <%= hiddenID.ClientID %>.setValue(record.data.ID);
                    <%= btnDelete.ClientID %>.fireEvent('click');
                }
                else
                {
                    var projectId = <%= hiddenProjectId.ClientID %>;
                    projectId.setValue("");

                    if(record.data.LeaveProjectId != null)
                        projectId.setValue(parseInt(record.data.LeaveProjectId));

                    <%= hiddenDepartmentId.ClientID  %>.setValue( parseInt( record.data.DepartmentId));

                    <%= btnEdit.ClientID %>.fireEvent('click');
                }

             }

             var CommandHandler2 = function(command, record){

                if(command=="Delete")
                {
                    
                    <%= hiddenID.ClientID %>.setValue(record.data.ID);
                    <%= btnDelete2.ClientID %>.fireEvent('click');
                }
                else
                {
                    var projectId = <%= hiddenProjectId.ClientID %>;
                    projectId.setValue("");

                    if(record.data.LeaveProjectId != null)
                        projectId.setValue(parseInt(record.data.LeaveProjectId));

                    <%= hiddenDepartmentId.ClientID  %>.setValue( parseInt( record.data.DepartmentId));

                    <%= btnEdit2.ClientID %>.fireEvent('click');
                }

             }

             //
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <ext:ResourceManager ID="ResourceManager1" DisableViewState="false" ShowWarningOnAjaxFailure="false"
        runat="server" />
    <ext:Hidden ID="hiddenDepartmentId" runat="server" />
    <ext:Hidden ID="hiddenProjectId" runat="server" />
    <ext:Hidden ID="hiddenID" runat="server" />
    <ext:Button ID="btnEdit" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnEdit_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>
    <ext:Hidden ID="hdnIsFirstGrid" runat="server" />
    <ext:Button ID="btnDelete" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnDelete_Click">
                <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the approval?" />
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>
    <ext:Button ID="btnEdit2" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnEdit2_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>
    <ext:Button ID="btnDelete2" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnDelete2_Click">
                <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the approval?" />
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>
    <div class="contentArea">
        <h3>
            Branch Transfer Leave Request Settings</h3>
        <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
        <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
        <ext:Store ID="storeBranch" runat="server">
            <Model>
                <ext:Model runat="server">
                    <Fields>
                        <ext:ModelField Name="BranchId" Type="String" />
                        <ext:ModelField Name="Name" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
        <ext:Store ID="storeLeaveProject" runat="server">
            <Model>
                <ext:Model ID="Model2" runat="server">
                    <Fields>
                        <ext:ModelField Name="LeaveProjectId" Type="String" />
                        <ext:ModelField Name="Name" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
    </div>
    <div style="clear: both; margin-left: 16px;" id="divGrid" runat="server">
        <table>
            <tr>
                <td valign="top">
                    <table>
                        <tbody>
                            <tr>
                                <td class="labelBold">
                                    Old Branch
                                </td>
                                <td style="width: 10px">
                                </td>
                                <td>
                                    <ext:DisplayField ID="dispOldBranch" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="labelBold">
                                    Current Team
                                </td>
                                <td style="width: 10px">
                                </td>
                                <td>
                                    <ext:DisplayField ID="dispTeam1" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="labelBold">
                                    Review
                                </td>
                                <td style="width: 10px">
                                </td>
                                <td>
                                    <ext:DisplayField ID="dispReview1" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td style="width: 10px">
                                </td>
                                <td>
                                    <ext:DisplayField ID="dispReview2" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="labelBold">
                                    Approval
                                </td>
                                <td style="width: 10px">
                                </td>
                                <td>
                                    <ext:DisplayField ID="dispApprove1" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td style="width: 10px">
                                </td>
                                <td>
                                    <ext:DisplayField ID="dispApprove2" runat="server" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
                <td style="width: 10px">
                </td>
                <td valign="top" style="width: 850px">
                    <ext:ComboBox ID="cboBranch" LabelCls="labelBold" runat="server" QueryMode="Local"
                        DisplayField="Name" ValueField="BranchId" Width="250" ForceSelection="true" StoreID="storeBranch"
                        FieldLabel="Employee left Branch" LabelSeparator="" LabelAlign="Top">
                        <Listeners>
                            <Select Handler="#{PagingToolbar1}.doRefresh();" />
                        </Listeners>
                    </ext:ComboBox>
                    <ext:GridPanel ID="gvList" runat="server" StyleSpec="margin-top:10px;" AutoHeight="true"
                        Cls="gridtbl">
                        <Store>
                            <ext:Store ID="storeLeaveApproval" WarningOnDirty="false" PageSize="100" runat="server"
                                RemoteSort="false">
                                <Proxy>
                                    <ext:AjaxProxy Json="true" Url="../Handler/EmployeeGradeStepJsonService.asmx/GetApprovalList">
                                        <ActionMethods Read="POST" />
                                        <Reader>
                                            <ext:JsonReader Root="d.Data" TotalProperty="d.TotalRecords" />
                                        </Reader>
                                    </ext:AjaxProxy>
                                </Proxy>
                                <AutoLoadParams>
                                    <ext:Parameter Name="start" Value="={0}" />
                                    <ext:Parameter Name="limit" Value="100" />
                                </AutoLoadParams>
                                <Parameters>
                                    <ext:StoreParameter Name="departmentId" Value="-1" Mode="Raw" />
                                    <ext:StoreParameter Name="branchId" Value="#{cboBranch}.getValue()" Mode="Raw" />
                                    <ext:StoreParameter Name="teamIdStr" Value="-1" Mode="Raw" />
                                </Parameters>
                                <Model>
                                    <ext:Model ID="Model1" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="ID" Type="Int" />
                                            <ext:ModelField Name="DepartmentId" Type="Int" />
                                            <ext:ModelField Name="LeaveProjectId" Type="Int" UseNull="true" />
                                            <ext:ModelField Name="Department" />
                                            <ext:ModelField Name="Branch" />
                                            <ext:ModelField Name="Project" />
                                            <ext:ModelField Name="ApplyTo" />
                                            <ext:ModelField Name="CC1" />
                                            <ext:ModelField Name="CC2" />
                                            <ext:ModelField Name="CC3" />
                                            <ext:ModelField Name="CC4" />
                                            <ext:ModelField Name="CC5" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <%--<SortInfo Field="EmployeeId" Direction="ASC" />--%>
                            </ext:Store>
                        </Store>
                        <ColumnModel ID="ColumnModel1" runat="server">
                            <Columns>
                                <ext:Column ID="Column2" MenuDisabled="true" runat="server" Text="Team" DataIndex="Project"
                                    Width="150px" Sortable="false" Align="Left" />
                                <ext:Column ID="ColumnR1" MenuDisabled="true" runat="server" Text="Recommend 1" DataIndex="CC4"
                                    Width="150px" Sortable="false" Align="Left" />
                                <ext:Column ID="ColumnR2" MenuDisabled="true" runat="server" Text="Recommend 2" DataIndex="CC5"
                                    Width="150px" Sortable="false" Align="Left" />
                                <ext:Column ID="Column5" MenuDisabled="true" runat="server" Text="Approve 1" DataIndex="ApplyTo"
                                    Width="150px" Sortable="false" Align="Left" />
                                <ext:Column ID="Column6" MenuDisabled="true" runat="server" Text="Approve 2" DataIndex="CC1"
                                    Width="150px" Sortable="false" Align="Left" />
                                <ext:Column ID="Column7" MenuDisabled="true" Hidden="true" runat="server" Text="Approve 3"
                                    DataIndex="CC2" Width="100px" Sortable="false" Align="Left" />
                                <ext:Column ID="Column8" MenuDisabled="true" Hidden="true" runat="server" Text="Approve 4"
                                    DataIndex="CC3" Width="100px" Sortable="false" Align="Left" />
                                <ext:CommandColumn ID="CommandColumn1" Width="100" runat="server">
                                    <Commands>
                                        <ext:GridCommand Icon="ApplicationEdit" ToolTip-Text="Edit" CommandName="Save" />
                                        <ext:CommandSeparator />
                                        <ext:GridCommand Icon="Delete" CommandName="Delete" ToolTip-Text="Delete" />
                                    </Commands>
                                    <Listeners>
                                        <Command Handler="CommandHandler(command,record);" />
                                    </Listeners>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <SelectionModel>
                            <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" />
                        </SelectionModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar1" runat="server" PageSize="100" DisplayInfo="true"
                                DisplayMsg="Displaying list {0} - {1} of {2}" EmptyMsg="No list to display" />
                        </BottomBar>
                    </ext:GridPanel>
                </td>
            </tr>
            <tr>
                <td style="height: 10px">
                </td>
            </tr>
            <tr>
                <td valign="top">
                    <table>
                        <tbody>
                            <tr>
                                <td class="labelBold">
                                    New Branch
                                </td>
                                <td style="width: 10px">
                                </td>
                                <td>
                                    <ext:DisplayField ID="dispNewBranch" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="labelBold">
                                    New Team
                                </td>
                                <td style="width: 10px">
                                </td>
                                <td>
                                    <ext:ComboBox ID="cmbNewTeam" QueryMode="Local" runat="server" DisplayField="Name"
                                        ValueField="LeaveProjectId" Margins="0 0 10 0" LabelAlign="Top" Width="200" ForceSelection="true"
                                        StoreID="storeLeaveProject">
                                        <DirectEvents>
                                            <Select OnEvent="cmbNewTeam_Select">
                                                <EventMask ShowMask="true" />
                                            </Select>
                                        </DirectEvents>
                                    </ext:ComboBox>
                                    <asp:RequiredFieldValidator ID="valNewTeam" runat="server" ControlToValidate="cmbNewTeam"
                                        Text="" ErrorMessage="New Team is required." ValidationGroup="NewTeam" />
                                </td>
                            </tr>
                            <tr>
                                <td class="labelBold">
                                    Review
                                </td>
                                <td style="width: 10px">
                                </td>
                                <td>
                                    <ext:DisplayField ID="dispNewReview1" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td style="width: 10px">
                                </td>
                                <td>
                                    <ext:DisplayField ID="dispNewReview2" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="labelBold">
                                    Approval
                                </td>
                                <td style="width: 10px">
                                </td>
                                <td>
                                    <ext:DisplayField ID="dispNewApprove1" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td style="width: 10px">
                                </td>
                                <td>
                                    <ext:DisplayField ID="dispNewApprove2" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <ext:Button ID="btnSaveNewTeam" StyleSpec="margin-top:10px" runat="server" Text="Save"
                                        Height="30" Width="100" ValidationGroup="NewTeam">
                                        <DirectEvents>
                                            <Click OnEvent="btnSaveNewTeam_Click">
                                                <EventMask ShowMask="true" />
                                            </Click>
                                        </DirectEvents>
                                        <%-- <Listeners>
                                            <Click Handler=" return ValidationInAction('NewTeam');" />
                                        </Listeners>--%>
                                    </ext:Button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
                <td style="width: 10px">
                </td>
                <td valign="top">
                    <ext:ComboBox ID="cmbNewBranch" LabelCls="labelBold" runat="server" QueryMode="Local"
                        DisplayField="Name" ValueField="BranchId" Width="250" ForceSelection="true" StoreID="storeBranch"
                        FieldLabel="Employee joined Branch" LabelSeparator="" LabelAlign="Top">
                        <Listeners>
                            <Select Handler="#{PagingToolbar2}.doRefresh();" />
                        </Listeners>
                    </ext:ComboBox>
                    <ext:GridPanel ID="gvList2" runat="server" StyleSpec="margin-top:10px;" AutoHeight="true"
                        Cls="gridtbl">
                        <Store>
                            <ext:Store ID="store2" WarningOnDirty="false" PageSize="100" runat="server" RemoteSort="false">
                                <Proxy>
                                    <ext:AjaxProxy Json="true" Url="../Handler/EmployeeGradeStepJsonService.asmx/GetApprovalList">
                                        <ActionMethods Read="POST" />
                                        <Reader>
                                            <ext:JsonReader Root="d.Data" TotalProperty="d.TotalRecords" />
                                        </Reader>
                                    </ext:AjaxProxy>
                                </Proxy>
                                <AutoLoadParams>
                                    <ext:Parameter Name="start" Value="={0}" />
                                    <ext:Parameter Name="limit" Value="100" />
                                </AutoLoadParams>
                                <Parameters>
                                    <ext:StoreParameter Name="departmentId" Value="-1" Mode="Raw" />
                                    <ext:StoreParameter Name="branchId" Value="#{cmbNewBranch}.getValue()" Mode="Raw" />
                                    <ext:StoreParameter Name="teamIdStr" Value="-1" Mode="Raw" />
                                </Parameters>
                                <Model>
                                    <ext:Model ID="Model3" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="ID" Type="Int" />
                                            <ext:ModelField Name="DepartmentId" Type="Int" />
                                            <ext:ModelField Name="LeaveProjectId" Type="Int" UseNull="true" />
                                            <ext:ModelField Name="Department" />
                                            <ext:ModelField Name="Branch" />
                                            <ext:ModelField Name="Project" />
                                            <ext:ModelField Name="ApplyTo" />
                                            <ext:ModelField Name="CC1" />
                                            <ext:ModelField Name="CC2" />
                                            <ext:ModelField Name="CC3" />
                                            <ext:ModelField Name="CC4" />
                                            <ext:ModelField Name="CC5" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <%--<SortInfo Field="EmployeeId" Direction="ASC" />--%>
                            </ext:Store>
                        </Store>
                        <ColumnModel ID="ColumnModel2" runat="server">
                            <Columns>
                                <ext:Column ID="Column1" MenuDisabled="true" runat="server" Text="Team" DataIndex="Project"
                                    Width="150px" Sortable="false" Align="Left" />
                                <ext:Column ID="ColumnR3" MenuDisabled="true" runat="server" Text="Recommend 1" DataIndex="CC4"
                                    Width="150px" Sortable="false" Align="Left" />
                                <ext:Column ID="ColumnR4" MenuDisabled="true" runat="server" Text="Recommend 2" DataIndex="CC5"
                                    Width="150px" Sortable="false" Align="Left" />
                                <ext:Column ID="Column11" MenuDisabled="true" runat="server" Text="Approve 1" DataIndex="ApplyTo"
                                    Width="150px" Sortable="false" Align="Left" />
                                <ext:Column ID="Column12" MenuDisabled="true" runat="server" Text="Approve 2" DataIndex="CC1"
                                    Width="150px" Sortable="false" Align="Left" />
                                <ext:Column ID="Column13" MenuDisabled="true" Hidden="true" runat="server" Text="Approve 3"
                                    DataIndex="CC2" Width="100px" Sortable="false" Align="Left" />
                                <ext:Column ID="Column14" MenuDisabled="true" Hidden="true" runat="server" Text="Approve 4"
                                    DataIndex="CC3" Width="100px" Sortable="false" Align="Left" />
                                <ext:CommandColumn ID="CommandColumn2" Width="100" runat="server">
                                    <Commands>
                                        <ext:GridCommand Icon="ApplicationEdit" ToolTip-Text="Edit" CommandName="Save" />
                                        <ext:CommandSeparator />
                                        <ext:GridCommand Icon="Delete" CommandName="Delete" ToolTip-Text="Delete" />
                                    </Commands>
                                    <Listeners>
                                        <Command Handler="CommandHandler2(command,record);" />
                                    </Listeners>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <SelectionModel>
                            <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" />
                        </SelectionModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar2" runat="server" PageSize="100" DisplayInfo="true"
                                DisplayMsg="Displaying list {0} - {1} of {2}" EmptyMsg="No list to display" />
                        </BottomBar>
                    </ext:GridPanel>
                </td>
            </tr>
        </table>
      <%--  <ext:Button runat="server" StyleSpec="margin-top:10px;" Text="Add" Width="100">
            <DirectEvents>
                <Click OnEvent="LeaveApprovalAddBtn_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:Button>--%>
        <!--  -->
        <ext:Store ID="storeAppliesTo" runat="server">
            <Model>
                <ext:Model IDProperty="EmployeeId">
                    <Fields>
                        <ext:ModelField Name="EmployeeId" Type="String" />
                        <ext:ModelField Name="Name" />
                        <ext:ModelField Name="Email" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
        <ext:Window ID="LeaveApprovalWindow" runat="server" Icon="House" Title="Leave Approval Settings"
            Hidden="true" Width="700" Height="550" Modal="true">
            <Content>
                <table style="margin-left: 15px!important;" class="tableLightColor">
                    <tr>
                        <td colspan="6">
                            <ext:ComboBox ID="cboProject" runat="server" DisplayField="Name" ValueField="LeaveProjectId"
                                Margins="0 0 10 0" FieldLabel="Select a team" LabelAlign="Top" Width="250" ForceSelection="true"
                                StoreID="storeLeaveProject" Editable="false">
                                <DirectEvents>
                                    <Select OnEvent="cboProject_Select">
                                        <EventMask ShowMask="true" />
                                    </Select>
                                </DirectEvents>
                            </ext:ComboBox>
                            <%--  </td>
                        <td colspan="4">--%>
                            <ext:ComboBox ID="cmbApprovalNotificationType" runat="server" Margins="0 0 10 0"
                                ForceSelection="true" FieldLabel="Approval notification" LabelWidth="250" LabelAlign="Top"
                                Width="250" Editable="false">
                                <Items>
                                    <ext:ListItem Value="0" Text="None" />
                                    <ext:ListItem Value="1" Text="All Employees" />
                                    <ext:ListItem Value="2" Text="Branch Employees" />
                                    <ext:ListItem Value="3" Text="Department Employees" />
                                    <ext:ListItem Value="4" Text="Team Members" />
                                </Items>
                            </ext:ComboBox>
                              <ext:TextField ID="ccEmailList" LabelWidth="250" Width="250" LabelSeparator="" FieldLabel="Approval CC Emails separated by comma(,)"
                                LabelAlign="Top" runat="server" />
                        </td>
                    </tr>
                    <tr runat="server" id="rowReview1">
                        <td colspan="6">
                            <div style="font-size: 15px; padding-top: 15px; padding-bottom: 5px;">
                                Team member's leave will be recommended by</div>
                        </td>
                    </tr>
                   <tr runat="server" id="rowReview2">
                        <th style="width: 100px">
                            Person
                        </th>
                        <th style="width: 100px;">
                            Name
                        </th>
                        <th style="text-align: center; display: none">
                            Allow
                            <br />
                            Leave Approval
                        </th>
                        <th>
                            <span runat="server" id="Span1" style="display: none">Timesheet HR Approve </span>
                        </th>
                        <th style="display: none">
                            Level
                        </th>
                        <th style="width: 250px;">
                            Email
                        </th>
                    </tr>
                    <tr class="odd" runat="server" id="rowReview3">
                        <td>
                            Main Person
                        </td>
                        <td>
                            <ext:ComboBox QueryMode="Local" ID="cboCC4" runat="server" DisplayField="Name" ValueField="EmployeeId"
                                Width="200" ForceSelection="false" StoreID="storeAppliesTo" Mode="Local">
                                <Triggers>
                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                </Triggers>
                                <Listeners>
                                    <Focus Handler="this.lastQuery = '';this.store.clearFilter();" />
                                    <Blur Handler="if(this.getStore().getById(this.getValue())==null) this.clearValue(); " />
                                    <AfterRender Handler="this.getTrigger(0).show();" />
                                    <Select Handler="this.getTrigger(0).show();DisplayEmail(this.getStore(),this.getValue(),#{lblEmail5});" />
                                    <BeforeQuery Handler="this.getTrigger(0)[ this.getRawValue().toString().length == 0 ? 'show' : 'show']();" />
                                    <TriggerClick Handler="if (index == 0) { this.clearValue();DisplayEmail(this.getStore(),this.getValue(),#{lblEmail5});} " />
                                </Listeners>
                            </ext:ComboBox>
                        </td>
                        <td align="center" style="display: none">
                            <ext:Checkbox ID="chkCC4" Checked="true" runat="server" />
                        </td>
                        <td align="center">
                            &nbsp;
                        </td>
                        <td align="center" style="display: none">
                            <ext:TextField Width="50" ID="txtLevelCC4" Text="5" runat="server" />
                        </td>
                        <td>
                            <ext:Label ID="lblEmail5" runat="server" />
                        </td>
                    </tr>
                    <tr class="even" runat="server" id="rowReview4">
                        <td>
                            Alternate person
                        </td>
                        <td>
                            <ext:ComboBox QueryMode="Local" ID="cboCC5" runat="server" DisplayField="Name" ValueField="EmployeeId"
                                Width="200" ForceSelection="false" StoreID="storeAppliesTo" Mode="Local">
                                <Triggers>
                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                </Triggers>
                                <Listeners>
                                    <Focus Handler="this.lastQuery = '';this.store.clearFilter();" />
                                    <Blur Handler="if(this.getStore().getById(this.getValue())==null) this.clearValue(); " />
                                    <AfterRender Handler="this.getTrigger(0).show();" />
                                    <Select Handler="this.getTrigger(0).show();DisplayEmail(this.getStore(),this.getValue(),#{lblEmail6});" />
                                    <BeforeQuery Handler="this.getTrigger(0)[ this.getRawValue().toString().length == 0 ? 'show' : 'show']();" />
                                    <TriggerClick Handler="if (index == 0) { this.clearValue();DisplayEmail(this.getStore(),this.getValue(),#{lblEmail6});} " />
                                </Listeners>
                            </ext:ComboBox>
                        </td>
                        <td align="center" style="display: none">
                            <ext:Checkbox ID="chkCC5" Checked="true" runat="server" />
                        </td>
                        <td align="center">
                            &nbsp;
                        </td>
                        <td align="center" style="display: none">
                            <ext:TextField Width="50" ID="txtLevelCC5" Text="6" runat="server" />
                        </td>
                        <td>
                            <ext:Label ID="lblEmail6" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            <div style="font-size: 15px; padding-top: 15px; padding-bottom: 5px;">
                                Recommended leaves will be approved by</div>
                        </td>
                    </tr>
                    <tr>
                        <th style="width: 100px">
                            Action Person
                        </th>
                        <th style="width: 100px;">
                            Person Name
                        </th>
                        <th style="text-align: center; display: none">
                            Allow
                            <br />
                            Leave Approval
                        </th>
                        <th>
                            <span runat="server" id="timesheetHRHeader1">Timesheet HR Approve </span>
                        </th>
                        <th style="display: none">
                            Level
                        </th>
                        <th style="width: 150px;">
                            Email
                        </th>
                    </tr>
                    <tr class="odd">
                        <td style='width: 150px'>
                            Main person
                        </td>
                        <td>
                            <ext:ComboBox ID="cboAppliesTo" runat="server" DisplayField="Name" ValueField="EmployeeId"
                                Width="200" QueryMode="Local" StoreID="storeAppliesTo" AllowBlank="true" EmptyText=""
                                ForceSelection="false" Mode="Local" TypeAhead="false">
                                <%--<Listeners>
                                                    <Select Fn="DisplayEmail" />
                                                </Listeners>--%>
                                <Triggers>
                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                </Triggers>
                                <Listeners>
                                    <Focus Handler="this.lastQuery = '';this.store.clearFilter();" />
                                    <Blur Handler="if(this.getStore().getById(this.getValue())==null) this.clearValue(); " />
                                    <AfterRender Handler="this.getTrigger(0).show();" />
                                    <Select Handler="this.getTrigger(0).show();DisplayEmail(this.getStore(),this.getValue(),#{lblEmail1});" />
                                    <BeforeQuery Handler="this.getTrigger(0)[ this.getRawValue().toString().length == 0 ? 'show' : 'show']();" />
                                    <TriggerClick Handler="if (index == 0) { this.clearValue();DisplayEmail(this.getStore(),this.getValue(),#{lblEmail1});} " />
                                </Listeners>
                            </ext:ComboBox>
                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="cboAppliesTo"
                                Display="None" ErrorMessage="Applies to is required." ValidationGroup="LeaveApproval" />--%>
                        </td>
                        </td>
                        <td align="center" style="display: none">
                            &nbsp;
                        </td>
                        <td align="center">
                            <span runat="server" id="timesheetInfo" style="font-size: 10px">If selected skipped
                                for Leave </span>
                        </td>
                        <td align="center" style="display: none">
                            <ext:TextField ID="txtLevelApplyTo" Text="1" Width="50" runat="server" />
                        </td>
                        <td>
                            <ext:Label ID="lblEmail1" runat="server" />
                        </td>
                    </tr>
                    <tr class="even">
                        <td>
                            Alternate person 1
                        </td>
                        <td>
                            <ext:ComboBox QueryMode="Local" ID="cboCC1" runat="server" DisplayField="Name" ValueField="EmployeeId"
                                Width="200" ForceSelection="false" StoreID="storeAppliesTo" Mode="Local">
                                <Triggers>
                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                </Triggers>
                                <Listeners>
                                    <Focus Handler="this.lastQuery = '';this.store.clearFilter();" />
                                    <Blur Handler="if(this.getStore().getById(this.getValue())==null) this.clearValue(); " />
                                    <AfterRender Handler="this.getTrigger(0).show();" />
                                    <Select Handler="this.getTrigger(0).show();DisplayEmail(this.getStore(),this.getValue(),#{lblEmail2});" />
                                    <BeforeQuery Handler="this.getTrigger(0)[ this.getRawValue().toString().length == 0 ? 'show' : 'show']();" />
                                    <TriggerClick Handler="if (index == 0) { this.clearValue();DisplayEmail(this.getStore(),this.getValue(),#{lblEmail2});} " />
                                </Listeners>
                            </ext:ComboBox>
                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="cboCC1"
                                    Display="None" ErrorMessage="Notified to 1 is required" ValidationGroup="LeaveApproval" />--%>
                        </td>
                        <td align="center" style="display: none">
                            <ext:Checkbox ID="chkCC1" Checked="true" runat="server" />
                        </td>
                        <td align="center">
                            &nbsp;
                        </td>
                        <td align="center" style="display: none">
                            <ext:TextField ID="txtLevelCC1" Text="2" Width="50" runat="server" />
                        </td>
                        <td>
                            <ext:Label ID="lblEmail2" runat="server" />
                        </td>
                    </tr>
                    <tr class="odd" style="display: none">
                        <td>
                            Alternate person 2
                        </td>
                        <td>
                            <ext:ComboBox QueryMode="Local" ID="cboCC2" runat="server" DisplayField="Name" ValueField="EmployeeId"
                                Width="200" ForceSelection="false" StoreID="storeAppliesTo" Mode="Local">
                                <Triggers>
                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                </Triggers>
                                <Listeners>
                                    <Focus Handler="this.lastQuery = '';this.store.clearFilter();" />
                                    <Blur Handler="if(this.getStore().getById(this.getValue())==null) this.clearValue(); " />
                                    <AfterRender Handler="this.getTrigger(0).show();" />
                                    <Select Handler="this.getTrigger(0).show();DisplayEmail(this.getStore(),this.getValue(),#{lblEmail3});" />
                                    <BeforeQuery Handler="this.getTrigger(0)[ this.getRawValue().toString().length == 0 ? 'show' : 'show']();" />
                                    <TriggerClick Handler="if (index == 0) { this.clearValue();DisplayEmail(this.getStore(),this.getValue(),#{lblEmail3});} " />
                                </Listeners>
                            </ext:ComboBox>
                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="cboCC2"3
                                    Display="None" ErrorMessage="Notified to 2 is required" ValidationGroup="LeaveApproval" />--%>
                        </td>
                        <td align="center" style="display: none">
                            <ext:Checkbox ID="chkCC2" Checked="true" runat="server" />
                        </td>
                        <td align="center" runat="server" id="timesheetHRRow">
                            <ext:Checkbox ID="chkTimeSheetHRApproval" Checked="false" runat="server" />
                        </td>
                        <td align="center" style="display: none">
                            <ext:TextField Width="50" ID="txtLevelCC2" Text="3" runat="server" />
                        </td>
                        <td>
                            <ext:Label ID="lblEmail3" runat="server" />
                        </td>
                    </tr>
                    <tr class="even" style="display: none">
                        <td>
                            Alternate person 3
                        </td>
                        <td>
                            <ext:ComboBox QueryMode="Local" ID="cboCC3" runat="server" DisplayField="Name" ValueField="EmployeeId"
                                Width="200" ForceSelection="false" StoreID="storeAppliesTo" Mode="Local">
                                <Triggers>
                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                </Triggers>
                                <Listeners>
                                    <Focus Handler="this.lastQuery = '';this.store.clearFilter();" />
                                    <Blur Handler="if(this.getStore().getById(this.getValue())==null) this.clearValue(); " />
                                    <AfterRender Handler="this.getTrigger(0).show();" />
                                    <Select Handler="this.getTrigger(0).show();DisplayEmail(this.getStore(),this.getValue(),#{lblEmail4});" />
                                    <BeforeQuery Handler="this.getTrigger(0)[ this.getRawValue().toString().length == 0 ? 'show' : 'show']();" />
                                    <TriggerClick Handler="if (index == 0) { this.clearValue();DisplayEmail(this.getStore(),this.getValue(),#{lblEmail4});} " />
                                </Listeners>
                            </ext:ComboBox>
                        </td>
                        <td align="center" style="display: none">
                            <ext:Checkbox ID="chkCC3" Checked="true" runat="server" />
                        </td>
                        <td align="center">
                            &nbsp;
                        </td>
                        <td align="center" style="display: none">
                            <ext:TextField Width="50" ID="txtLevelCC3" Text="4" runat="server" />
                        </td>
                        <td>
                            <ext:Label ID="lblEmail4" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-top: 30px" colspan="6">
                            <ul class="itemList">
                                <li>
                                    <ext:Button ID="btnExtSave" StyleSpec="margin-top:10px" runat="server" Text="Save"
                                        Height="30" Width="100" ValidationGroup="LeaveApproval">
                                        <DirectEvents>
                                            <Click OnEvent="btnExtSave_Click">
                                                <EventMask ShowMask="true" />
                                            </Click>
                                        </DirectEvents>
                                        <Listeners>
                                            <Click Handler=" return ValidationInAction('LeaveApproval');" />
                                        </Listeners>
                                    </ext:Button>
                                </li>
                                <li style="margin-left: 10px; padding-top: 15px;">or </li>
                                <li>
                                    <ext:LinkButton ID="btnClose" StyleSpec="margin-left:10px;padding-top:16px;" Width="100"
                                        Height="30" runat="server" Text="Cancel" ValidationGroup="InsertUpdate">
                                        <Listeners>
                                            <Click Handler="#{LeaveApprovalWindow}.hide();">
                                            </Click>
                                        </Listeners>
                                    </ext:LinkButton>
                                </li>
                            </ul>
                        </td>
                    </tr>
                </table>
            </Content>
        </ext:Window>
    </div>
</asp:Content>
