﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Details.Master" AutoEventWireup="true"
    CodeBehind="CompanyDel.aspx.cs" Inherits="Web.CP.CompanyDel" %>

<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <div class="contentArea">
    
                      <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
    <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
    
    
        <asp:Panel runat="server" DefaultButton="btnDelete">
            <table>
                <tr>
                    <td class="lf">
                        Password
                    </td>
                    <td>
                        <asp:TextBox ID="txtPwd" TextMode="Password" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="CD" ControlToValidate="txtPwd"
                            ErrorMessage="Password is required." runat="server" Display="None"></asp:RequiredFieldValidator>
                    </td>
                </tr>
            </table>
            <div class="buttonsDiv">
                <asp:Button ID="btnDelete" CssClass="save" runat="server" OnClientClick="valGroup='CD';return CheckValidation();"
                    Text="Delete" OnClick="btnDelete_Click" />
                &nbsp;<asp:Button ID="btnCancel" CssClass="cancel" runat="server" OnClientClick="window.location= 'ManageCompany.aspx';return false;"
                    Text="Cancel" />
            </div>
            <%--<br />
            <br />
            <asp:Label ID="lblMsg" runat="server" EnableViewState="false" ForeColor="Red"></asp:Label>--%>
        </asp:Panel>
    </div>
</asp:Content>
