﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using System.Text;
using BLL.Manager;
using Utils.Helper;
using DAL;
using System.Drawing;
using BLL.Base;


namespace Web.CP
{
    public partial class ManageVoucherGroupNew : BasePage
    {
        TaxManager taxMgr = new TaxManager();

        List<CalcGetHeaderListResult> headerList = new List<CalcGetHeaderListResult>();

        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.AdministratorTools;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
               
                Initialise();

            }

            if (Request.Form["__EVENTTARGET"] != null && Request.Form["__EVENTTARGET"].Equals("Reload"))
                LoadGroup();


            JavascriptHelper.AttachPopUpCode(Page, "voucherType", "Popup/AEVoucherType.aspx", 500, 400);
            JavascriptHelper.AttachPopUpCode(Page, "medicalTaxCreditPopup", "Popup/AEVoucherGroupNew.aspx", 750, 600);
        }

        void Initialise()
        {

            LoadGroup();

        }

        public string GetVoucherGroupName(object value)
        {
            if (value == null)
                return "";
            if (int.Parse(value.ToString()) == 0)
                return "";
            return VouccherManagerNew.GetVoucher(int.Parse(value.ToString())).GroupName;
        }

        public string GetType(int type)
        {
            if (type == (int)CalculationColumnType.Income
                || type == (int)CalculationColumnType.DeemedIncome
            || type == (int)CalculationColumnType.IncomeLeaveEncasement
                || type == (int)CalculationColumnType.IncomePF)
                return "Pay";

            return "Deduction";
        }

        public string GetVoucherType(object value)
        {
            return ((VoucherGroupTypeEnum)int.Parse(value.ToString())).ToString();
        }

        public string GetVoucherGroup(object value)
        {
            return ((VoucherGroupEnum)int.Parse(value.ToString())).ToString();
        }

        void LoadGroup()
        {
          


            gvwList.DataSource = VouccherManagerNew.GetVoucherGroupList();
            gvwList.DataBind();


            gridVoucerType.DataSource = VouccherManagerNew.GetVoucherTypeList();
            gridVoucerType.DataBind();


        }

        public string GetHeaderName(object type, object value)
        {
            int typev = int.Parse(type.ToString());
            int valuev = int.Parse(value.ToString());
            foreach (CalcGetHeaderListResult item in headerList)
            {

                if (item.Type == typev && item.SourceId == valuev)
                    return item.HeaderName;
            }

            return "";
        }

        protected void btnLoad_Click(object sender, EventArgs e)
        {
            

            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage -= 1;
            LoadGroup();;
        }

       



    }
}
