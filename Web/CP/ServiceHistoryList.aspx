﻿<%@ Page Title="Service History List" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="ServiceHistoryList.aspx.cs" Inherits="Web.CP.ServiceHistoryList" %>

<%@ Register Src="~/newhr/UserControls/EmployeeSummaryCtl.ascx" TagName="EmployeeSummaryCtl"
    TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/Master/NewDetails.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .empName
        {
            font-size: 20px;
            color: #439EC6;
            font-weight: bold;
        }
        .titleDesign
        {
            font-size: 15px;
            color: #000000;
            text-decoration: underline;
            padding-top: 10px;
        }
        
        .items span
        {
            display: block;
            padding-top: 3px;
            font-weight: bold;
        }
        
        .paddinAll
        {
            padding: 10px;
        }
       <%-- table tr td
        {
            padding: 0 10px 8px 0;
        }--%>
        h3
        {
            font: bold 13px Arial;
        }
        .marginTopss
        {
            margin-top: 10px;
            display: block;
        }
        .loanAdvTable
        {
            background-color: white;
            width: 60px;
            border: 1px solid white !important;
            padding: 0px;
        }
        .tableLightColor th
        {
            text-align: left;
        }
        strong
        {
            display: block;
            padding-bottom: 3px;
        }
        .tableLightColor input[type=text]
        {
            height: 20px;
        }
        .clsColor
        {
            background-color: #e8f1ff;
        }
         .BlueCls
        {
            color:Blue;
        }
        .tblTD
        {
            padding-top:10px!important;
        }
       .x-grid-cell-inner{padding: 5px 5px 5px 5px;}
.x-column-header-inner {padding: 7px 5px 7px 5px;}
    </style>
    <script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>
    <script type="text/javascript">

    function searchList() {
             <%=gridBranchTransfer.ClientID %>.getStore().pageSize = <%=cmbPageSize.ClientID %>.getValue();
            <%=PagingToolbar1.ClientID %>.doRefresh();
        }

    function EmpSelect()
    {
        <%=hdnEmployeeId.ClientID %>.setValue(<%=cmbEmployee.ClientID %>.getValue());
        <%=btnSelectEmp.ClientID %>.fireEvent('click');
    }

    var CommandHandler = function(command, record){
            <%= hdnBranchDepartmentId.ClientID %>.setValue(record.data.EventSourceID);
            <%=hdnEmployeeId.ClientID %>.setValue(record.data.EmployeeId);
                if(command=="Edit")
                {
                    <%= btnEdit.ClientID %>.fireEvent('click');
                }
                else
                {
                    <%= btnDelete.ClientID %>.fireEvent('click');
                }

             };

    //ImageCommandHandler
    
    var ImageCommandHandler = function(value, command,record,p2) {            
            if(command == 'View')
            { 
                 popup('BranchId=' + record.data.BranchId + '&EmployeeId=' + record.data.EmployeeId);
            }
       };

    function SelEmpSearch()
    {
        <%=hdnEmpSearch.ClientID %>.setValue(<%=cmbEmpSearch.ClientID %>.getValue());
    }

    function isPayrollSelected(btn) {


            var ret = shiftPopup();
            return false;
    }

    function OpeningHistoryServices(btn){
        var red=GetPopUp();
        return false;
    }

    var onAfterRender = function () {
            this.mon(this.el, {
                dblclick : function () {
                    this.setReadOnly(false);
                },
                click : function (e) {
                    this.setReadOnly(false);
                },
                scope : this
            });
        };

        var refreshWindow = function()
        {
            searchList();
        }
        var setLink = function(e1,e2,e3)
        {
            return '<a href="javscript:void(0)" onclick="showDetailsNew(' + e3.data.EventSourceID + ',0);">Details</a>' 
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <ext:Hidden ID="hdnEmployeeId" runat="server" />
    <ext:Hidden ID="hdnBranchDepartmentId" runat="server" />
    <ext:Hidden ID="hdnMsg" runat="server" />
    <ext:Hidden ID="hdnEmpSearch" runat="server" />
    <ext:Hidden ID="hdnIsEng" runat="server" />
    <ext:LinkButton ID="btnSelectEmp" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnSelectEmp_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:LinkButton ID="btnEdit" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnEdit_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:LinkButton ID="btnDelete" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnDelete_Click">
                <Confirmation ConfirmRequest="true" Message="Are you sure,you want to delete the evn?" />
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Service History List
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel" style="margin-top: 0px !important; padding-top: 0px !important;">
        <div class="innerLR">
            <uc1:EmployeeSummaryCtl Id="empSummaryDetails" runat="server" />
            <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                <Proxy>
                    <ext:AjaxProxy Url="../Handler/EmpSearchID.ashx">
                        <ActionMethods Read="GET" />
                        <Reader>
                            <ext:JsonReader Root="plants" TotalProperty="total" />
                        </Reader>
                    </ext:AjaxProxy>
                </Proxy>
                <Model>
                    <ext:Model ID="Model5" IDProperty="Name" Name="ItemLineModel" runat="server">
                        <Fields>
                            <ext:ModelField Name="Name" Type="String" />
                            <ext:ModelField Name="EmployeeId" Type="String" />
                        </Fields>
                    </ext:Model>
                </Model>
            </ext:Store>
            <div class="alert alert-info" style="margin-top: 10px; padding-top: 0px!important;">
                <ext:Container ID="listingContainer" runat="server">
                    <Content>
                        <table border="0">
                            <tr>
                                <td style="width: 160px;">
                                    <ext:DateField ID="txtFromDate" runat="server" FieldLabel="From" LabelSeparator=""
                                        LabelAlign="Top" Width="150">
                                        <Plugins>
                                            <ext:GenericPlugin ID="HighlightDates1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                                        </Plugins>
                                    </ext:DateField>
                                </td>
                                <td style="width: 160px;">
                                    <ext:DateField ID="txtToDate" runat="server" FieldLabel="To" LabelSeparator="" LabelAlign="Top"
                                        Width="150">
                                        <Plugins>
                                            <ext:GenericPlugin ID="GenericPlugin1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                                        </Plugins>
                                    </ext:DateField>
                                </td>
                                <td>
                                    <ext:ComboBox LabelSeparator="" ID="cmbEmpSearch" FieldLabel="Search Employee" EmptyText="Employee Name"
                                        LabelWidth="80" LabelAlign="Top" runat="server" DisplayField="Name" ValueField="EmployeeId"
                                        StoreID="storeSearch" TypeAhead="false" Width="200" PageSize="9999" HideBaseTrigger="true"
                                        MinChars="1" TriggerAction="All" ForceSelection="false">
                                        <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                                            <ItemTpl ID="ItemTpl2" runat="server">
                                                <Html>
                                                    <tpl>
                                                        <div class="search-item">
                                                                        <span>{Name}</span>  
                                                         </div>
					                            </tpl>
                                                </Html>
                                            </ItemTpl>
                                        </ListConfig>
                                        <Triggers>
                                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                        </Triggers>
                                        <Listeners>
                                            <Select Handler="this.getTrigger(0).show(); SelEmpSearch();" />
                                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                            <TriggerClick Handler="if (index == 0) { 
                                                       this.clearValue(); 
                                                       this.getTrigger(0).hide();
                                                   }" />
                                        </Listeners>
                                    </ext:ComboBox>
                                </td>
                                <td style="padding-left: 10px">
                                    <ext:ComboBox LabelAlign="Top" ID="cmbEventFilter" runat="server" ValueField="EventID"
                                        DisplayField="Name" Width="150" LabelSeparator="" FieldLabel="Event Type *" ForceSelection="true"
                                        QueryMode="Local">
                                        <Store>
                                            <ext:Store ID="Store5" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model7" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="EventID" Type="String" />
                                                            <ext:ModelField Name="Name" Type="String" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <Triggers>
                                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                        </Triggers>
                                        <Listeners>
                                            <Select Handler="this.getTrigger(0).show();" />
                                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                            <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                                        </Listeners>
                                    </ext:ComboBox>
                                </td>
                                <td>
                                    <ext:Button ID="btnLoad" runat="server" Text="Load" Width="100" Cls="btn btn-default"
                                        MarginSpec="25 10 10 10">
                                        <Listeners>
                                            <Click Fn="searchList">
                                            </Click>
                                        </Listeners>
                                    </ext:Button>
                                </td>
                                <td style="padding-left:10px">
                                    <ext:Button ID="btnNewTransfer" MarginSpec="25 10 10 0" Width="100" Height="30"
                                        runat="server" Text="New Event">
                                        <DirectEvents>
                                            <Click OnEvent="btnNewTransfer_Click">
                                                <EventMask ShowMask="true" />
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </td>
                                 <td style="padding-left:10px;">
                            <asp:LinkButton ID="LinkButton1" runat="server"  Text="Excel import" OnClientClick="return isPayrollSelected(this)"
                                CssClass=" excel marginRight tiptip" Style="float: left;" />
                        </td>
                                <td style="padding-left:10px">
                                    <asp:LinkButton runat="server" ID="lnkOpeningServicesExcelImport" Text="| Import History Opening " Visible="false" OnClientClick="return OpeningHistoryServices(this)" CssClass="excel marginRight tiptip"  Style="float: left;">
                                    </asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </Content>
                </ext:Container>
            </div>
            <ext:KeyMap ID="KeyMap1" runat="server" Target="={#{listingContainer}.getBody()}">
                <Binding>
                    <ext:KeyBinding Handler="#{btnLoad}.fireEvent('click');">
                        <Keys>
                            <ext:Key Code="ENTER" />
                        </Keys>
                    </ext:KeyBinding>
                </Binding>
            </ext:KeyMap>
            <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridBranchTransfer" runat="server"
                Cls="itemgrid" OnReadData="Store_ReadData" Scroll="Horizontal">
                <Store>
                    <ext:Store ID="storeEmpList" runat="server" AutoLoad="true" OnReadData="Store_ReadData"
                        RemotePaging="true" RemoteSort="true">
                        <Proxy>
                            <ext:PageProxy />
                        </Proxy>
                        <Model>
                            <ext:Model ID="Model1" runat="server" IDProperty="EventSourceID">
                                <Fields>
                                    <ext:ModelField Name="RowNumber" Type="Int" />
                                    <ext:ModelField Name="EventSourceID" Type="Int" />
                                    <ext:ModelField Name="BranchId" Type="Int" />
                                    <ext:ModelField Name="Date" Type="String" />
                                    <ext:ModelField Name="DateEng" Type="Date" />
                                    <ext:ModelField Name="EmployeeId" Type="String" />
                                    <ext:ModelField Name="Name" Type="String" />
                                    <ext:ModelField Name="EventName" Type="String" />
                                    <ext:ModelField Name="Branch" Type="String" />
                                    <ext:ModelField Name="Department" Type="String" />
                                    <ext:ModelField Name="ServiceStatus" Type="String" />
                                    <ext:ModelField Name="Designation" Type="String" />
                                    <ext:ModelField Name="Level" Type="String" />
                                    <ext:ModelField Name="IdCardNo" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Sorters>
                            <ext:DataSorter Property="DateEng" Direction="ASC" />
                        </Sorters>
                    </ext:Store>
                </Store>
                <ColumnModel ID="ColumnModel1" runat="server">
                    <Columns>
                        <ext:Column ID="Column5" runat="server" Text="SN" DataIndex="RowNumber" MenuDisabled="true"
                            Sortable="false" Align="Left" Width="40" />
                        <ext:Column ID="Column3" runat="server" Text="Event" DataIndex="EventName" MenuDisabled="true"
                            Sortable="false" Align="Left" Width="90" />
                        <ext:Column ID="colFromDate" runat="server" Text="Date" DataIndex="Date" MenuDisabled="true"
                            Sortable="false" Align="Left" Width="80" />
                        <ext:DateColumn ID="colFromDateEng" runat="server" Text="Eng Date" DataIndex="DateEng"
                            Format="yyyy-MMM-dd" MenuDisabled="true" Sortable="false" Align="Left" Width="90" />
                        <ext:Column ID="colEmployeeId" runat="server" Text="EIN" DataIndex="EmployeeId" MenuDisabled="true"
                            Sortable="false" Align="Center" Width="40" />
                        <ext:Column ID="Column1" runat="server" Text="I No" DataIndex="IdCardNo" MenuDisabled="true"
                            Sortable="false" Align="Center" Width="40" />
                        <ext:Column ID="colEmployeeName" runat="server" Text="Name" DataIndex="Name" MenuDisabled="true"
                            Sortable="false" Align="Left" Width="120" />
                        <ext:Column ID="colFromBranch" runat="server" Text="Branch" DataIndex="Branch" MenuDisabled="true"
                            Sortable="false" Align="Left" Width="120" />
                        <ext:Column ID="Column2" runat="server" Text="Department" DataIndex="Department"
                            MenuDisabled="true" Sortable="false" Align="Left" Width="120" />
                        <ext:Column ID="colToBranch" runat="server" Text="Service Status" DataIndex="ServiceStatus"
                            MenuDisabled="true" Sortable="false" Align="Left" Width="90" />
                        <ext:Column ID="Column211" runat="server" Text="Designation" DataIndex="Designation"
                            MenuDisabled="true" Sortable="false" Align="Left" Width="120" />
                        <ext:ImageCommandColumn ID="ColumnTeam" runat="server" Width="120" Text="Action"
                            MenuDisabled="true" Sortable="false" Align="Center">
                            <Commands>
                                <ext:ImageCommand CommandName="View" Text="Review Team" Cls="BlueCls">
                                </ext:ImageCommand>
                            </Commands>
                            <Listeners>
                                <Command Fn="ImageCommandHandler">
                                </Command>
                            </Listeners>
                        </ext:ImageCommandColumn>
                        <ext:CommandColumn ID="CommandColumn1" runat="server" Width="80" Text="" Align="Center">
                            <Commands>
                                <ext:CommandSeparator />
                                <ext:GridCommand Cls="editGridButton" ToolTip-Text="Edit" Text="<i class='fa fa-pencil'></i>"
                                    CommandName="Edit" />
                                <ext:GridCommand Cls="editGridButton" ToolTip-Text="Delete" Text="<i class='fa fa-trash-o'></i>"
                                    CommandName="Delete" />
                            </Commands>
                            <Listeners>
                                <Command Handler="CommandHandler(command,record);" />
                            </Listeners>
                        </ext:CommandColumn>
                        <ext:Column ID="Column4" runat="server" Text="Details" MenuDisabled="true" Sortable="false"
                            Align="Left" Width="100">
                            <Renderer Fn="setLink" />
                        </ext:Column>
                        <%-- <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:HyperLink NavigateUrl='javscript:void(0)' onclick='<%# "showDetails(" + Eval("EventSourceID") + "," + Eval("RowNumber") + ");" %>'
                                                            runat="server" Text="Details">
                                                            
                                                        </asp:HyperLink>
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>
                    </Columns>
                </ColumnModel>
                <%--<Plugins>
                    <ext:FilterHeader ID="FilterHeader1" runat="server" OnCreateFilterableField="OnCreateFilterableField" />
                </Plugins>--%>
                <BottomBar>
                    <ext:PagingToolbar Height="40" ID="PagingToolbar1" runat="server" StoreID="storeEmpList"
                        DisplayInfo="true">
                        <Items>
                            <ext:Label ID="lblPageSize" runat="server" Text="Page size:" />
                            <ext:ToolbarSpacer ID="ToolbarSpacer_listItems" runat="server" Width="10" />
                            <ext:ComboBox ID="cmbPageSize" runat="server" Width="80" SelectOnFocus="true" Selectable="true"
                                ValueField="Value" DisplayField="Text" ForceSelection="true" AllowBlank="false">
                                <Listeners>
                                    <Select Handler="searchList()" />
                                    <%--#{cmbPageSize}.pageSize = this.getValue();#{PagingToolbar1}.moveFirst();--%>
                                </Listeners>
                                <Items>
                                    <ext:ListItem Value="20" Text="20" />
                                    <ext:ListItem Value="50" Text="50" />
                                    <ext:ListItem Value="100" Text="100" />
                                </Items>
                                <SelectedItems>
                                    <ext:ListItem Index="0">
                                    </ext:ListItem>
                                </SelectedItems>
                            </ext:ComboBox>
                        </Items>
                    </ext:PagingToolbar>
                </BottomBar>
            </ext:GridPanel>
        </div>
        <div class="alert alert-info" style="margin-top: 15px; height: 50px;">
            <div style="float: right;">
                <ext:Button runat="server" AutoPostBack="true" OnClick="btnExport_Click" ID="btnExport"
                    Text="<i></i>Export To Excel">
                </ext:Button>
            </div>
        </div>
    </div>
    <ext:Window ID="WBranchTransfer" runat="server" Title="Event Details" Icon="Application"
        Width="800" Height="650" BodyPadding="5" Hidden="true" Modal="true">
        <Content>
            <table class="fieldTable" style="margin-left: 20px;">
                <tr>
                    <td>
                        <ext:ComboBox LabelSeparator="" ID="cmbEmployee" FieldLabel="Search Employee *" EmptyText="Search Employee"
                            LabelWidth="120" LabelAlign="Left" runat="server" DisplayField="Name" ValueField="EmployeeId"
                            StoreID="storeSearch" TypeAhead="false" Width="350" PageSize="9999" HideBaseTrigger="true"
                            MinChars="1" TriggerAction="All" ForceSelection="false">
                            <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                                <ItemTpl ID="ItemTpl1" runat="server">
                                    <Html>
                                        <tpl>
                                            <div class="search-item">
                                                            <span>{Name}</span>  
                                             </div>
					                </tpl>
                                    </Html>
                                </ItemTpl>
                            </ListConfig>
                            <Triggers>
                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                            </Triggers>
                            <Listeners>
                                <Select Handler="this.getTrigger(0).show(); EmpSelect();" />
                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                            </Listeners>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator ValidationGroup="SaveBrTran" ID="valEmployeeReq" ControlToValidate="cmbEmployee"
                            Display="None" runat="server" ErrorMessage="Please select an Employee."></asp:RequiredFieldValidator>
                    </td>
                </tr>
            </table>
            <div id="block" runat="server">
                <table style='clear: both; padding: 10px; margin-top: 10px;'>
                    <tr>
                        <td valign="top" style="padding-left: 10px;">
                            <ext:Image ID="image1" runat="server" ImageUrl="~/images/sample.jpg" Width="120"
                                Height="120" />
                        </td>
                        <td valign="top" style="padding-left: 20px" class="items">
                            <ext:Label ID="lblEName" StyleHtmlCls="empName" runat="server" />
                            <br />
                            <%--  <ext:Label ID="lblEININo" runat="server" Text="" StyleHtmlCls="titleDesign" />
                            <br />--%>
                            <ext:Label ID="lblEBranch" runat="server" />
                            <br />
                            <%--<ext:Label ID="lblEDepartment" runat="server" />
                            <br />--%>
                            <ext:Label ID="lblESince" runat="server" />
                            <br />
                            <ext:Label ID="lblInTime" runat="server" />
                            <br />
                        </td>
                    </tr>
                </table>
            </div>
            <table class="fieldTable" style="margin-left: 20px;">
                <tr>
                    <td class="tblTD">
                        <ext:ComboBox ID="cmbEventType" runat="server" LabelStyle="font-weight:bold;text-align:right"
                            ValueField="EventID" DisplayField="Name" Width="320" LabelWidth="120" LabelSeparator=""
                            FieldLabel="Event Type *" ForceSelection="true" QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store4" runat="server">
                                    <Model>
                                        <ext:Model ID="Model6" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="EventID" Type="String" />
                                                <ext:ModelField Name="Name" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator ID="reqdEventType" runat="server" ControlToValidate="cmbEventType"
                            Display="None" ErrorMessage="Event is required." ValidationGroup="SaveBrTran"></asp:RequiredFieldValidator>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td class="tblTD">
                        <pr:CalendarExtControl LabelStyle="font-weight:bold;text-align:right" FieldLabel="Applicable Date *"
                            Width="320" LabelWidth="120" ID="calFromDate" runat="server" LabelSeparator="" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="calFromDate"
                            Display="None" ErrorMessage="Applicable date is required." ValidationGroup="SaveBrTran"></asp:RequiredFieldValidator>
                    </td>
                    <td class="tblTD">
                        <pr:CalendarExtControl LabelStyle="font-weight:bold;text-align:right" FieldLabel="To Date" 
                            Width="320" LabelWidth="120" ID="calToDate" runat="server" LabelSeparator="" />
                    </td>
                </tr>
                <tr>
                    <%-- <td style="width: 120px;">
                        Letter Number
                    </td>--%>
                    <td class="tblTD">
                        <ext:TextField FieldLabel="Letter Number" LabelStyle="font-weight:bold;text-align:right"
                            Width="320" LabelWidth="120" ID="txtLetterNumber" runat="server" LabelAlign="Left"
                            LabelSeparator="">
                        </ext:TextField>
                    </td>
                    <%--<td style="width: 120px;">
                        
                    </td>--%>
                    <td class="tblTD">
                        <pr:CalendarExtControl FieldLabel="Letter Date *" LabelStyle="font-weight:bold;text-align:right"
                            Width="320" LabelWidth="120" ID="calLetterDate" runat="server" LabelSeparator="" />
                        <asp:RequiredFieldValidator ID="rfvLetterDate" runat="server" ControlToValidate="calLetterDate"
                            Display="None" ErrorMessage="Letter date is required." ValidationGroup="SaveBrTran"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <%-- <td style="width: 120px;">
                        Branch *
                    </td>--%>
                    <td class="tblTD">
                        <ext:ComboBox ID="cmbBranch" LabelStyle="font-weight:bold;text-align:right" runat="server"
                            ValueField="BranchId" DisplayField="Name" Width="320" LabelWidth="120" FieldLabel="Branch *"
                            LabelSeparator="" ForceSelection="true" QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store2" runat="server">
                                    <Model>
                                        <ext:Model ID="Model2" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="BranchId" Type="String" />
                                                <ext:ModelField Name="Name" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <DirectEvents>
                                <Select OnEvent="Branch_Select" />
                            </DirectEvents>
                            <%-- <Listeners>
                                <AfterRender Fn="onAfterRender" />
                            </Listeners>--%>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator ID="rfvBranch" runat="server" ControlToValidate="cmbBranch"
                            Display="None" ErrorMessage="Branch is required." ValidationGroup="SaveBrTran"></asp:RequiredFieldValidator>
                    </td>
                    <%-- <td style="width: 120px;">
                        Department *
                    </td>--%>
                    <td class="tblTD">
                        <ext:ComboBox ID="cmbDepartment" LabelStyle="font-weight:bold;text-align:right" runat="server"
                            ValueField="DepartmentId" DisplayField="Name" Width="320" LabelWidth="120" FieldLabel="Department *"
                            LabelSeparator="" ForceSelection="true" QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store1" runat="server">
                                    <Model>
                                        <ext:Model ID="Model3" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="DepartmentId" Type="String" />
                                                <ext:ModelField Name="Name" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator ID="rfvDepartment" runat="server" ControlToValidate="cmbDepartment"
                            Display="None" ErrorMessage="Department is required." ValidationGroup="SaveBrTran"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <%--  <td style="width: 120px;">
                        Sub Department
                    </td>--%>
                    <td class="tblTD">
                        <ext:ComboBox ID="cmbSubDepartment" LabelStyle="font-weight:bold;text-align:right"
                            runat="server" ValueField="SubDepartmentId" DisplayField="Name" Width="320" LabelWidth="120"
                            FieldLabel="Sub Department" LabelSeparator="" ForceSelection="true" QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store3" runat="server">
                                    <Model>
                                        <ext:Model ID="Model4" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="SubDepartmentId" Type="String" />
                                                <ext:ModelField Name="Name" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                    </td>
                    <td class="tblTD">
                    </td>
                    <%--<td style="width: 120px;">
                        Special Responsibility
                    </td>--%>
                    <%--<td>
                        <ext:TextField ID="txtSpecialResponsibility" LabelWidth=" Special Responsibility"
                            Width="320"  runat="server" LabelSeparator="">
                        </ext:TextField>
                    </td>--%>
                </tr>
                <tr>
                    <td class="tblTD">
                        <ext:ComboBox ID="cmbLevel" LabelStyle="font-weight:bold;text-align:right" runat="server"
                            LabelAlign="Left" FieldLabel="Level/Position *" ValueField="LevelId" DisplayField="GroupLevel"
                            Width="320" LabelWidth="120" LabelSeparator="" ForceSelection="true" QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store6" runat="server">
                                    <Model>
                                        <ext:Model ID="Model8" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="LevelId" Type="String" />
                                                <ext:ModelField Name="GroupLevel" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <DirectEvents>
                                <Select OnEvent="cmbLevel_Select">
                                    <EventMask ShowMask="true" />
                                </Select>
                            </DirectEvents>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="cmbLevel"
                            Display="None" ErrorMessage="Level/Position is required." ValidationGroup="SaveBrTran"></asp:RequiredFieldValidator>
                    </td>
                    <td class="tblTD">
                        <ext:ComboBox ID="cmbDesignation" LabelStyle="font-weight:bold;text-align:right"
                            runat="server" LabelAlign="Left" FieldLabel=" Designation *" ValueField="DesignationId"
                            DisplayField="Name" Width="320" LabelWidth="120" LabelSeparator="" ForceSelection="true"
                            QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store7" runat="server">
                                    <Model>
                                        <ext:Model ID="Model9" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="DesignationId" Type="String" />
                                                <ext:ModelField Name="Name" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="cmbDesignation"
                            Display="None" ErrorMessage="Designation is required." ValidationGroup="SaveBrTran"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:ComboBox ID="cmbStatus" LabelStyle="font-weight:bold;text-align:right" runat="server"
                            LabelAlign="Left" FieldLabel=" Service Status *" ValueField="Key" DisplayField="Value"
                            Width="320" LabelWidth="120" LabelSeparator="" ForceSelection="true" QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store8" runat="server">
                                    <Model>
                                        <ext:Model ID="Model10" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="Value" Type="String" />
                                                <ext:ModelField Name="Key" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="cmbStatus"
                            Display="None" ErrorMessage="Service status is required." ValidationGroup="SaveBrTran"></asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <%--<asp:DropDownList Style="margin-top: 10px" CssClass='statusWidth' ID="ddlStatus"
                            runat="server" AppendDataBoundItems="true" Width="150px">
                            <asp:ListItem Selected="true" Value="-1" Text="--Status--"></asp:ListItem>
                        </asp:DropDownList>--%>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="tblTD">
                        <ext:TextArea LabelWidth="120" LabelStyle="font-weight:bold;text-align:right" FieldLabel="Note"
                            ID="txtNote" runat="server" LabelSeparator="" Rows="3" Width="650px" Height="80" />
                    </td>
                </tr>
                <tr>
                    <td valign="bottom" colspan="2">
                        <div id="buttonsBar" runat="server" class="popupButtonDiv" style="float: right;">
                            <div style="width: 100px;">
                                <ext:Button runat="server" ID="btnSave" Cls="btn btn-primary" Width="90" Text="<i></i>Save">
                                    <DirectEvents>
                                        <Click OnEvent="btnSave_Click">
                                            <EventMask ShowMask="true" />
                                        </Click>
                                    </DirectEvents>
                                    <Listeners>
                                        <Click Handler="valGroup = 'SaveBrTran'; if(CheckValidation()) return this.disable(); else return false;">
                                        </Click>
                                    </Listeners>
                                </ext:Button>
                            </div>
                            <ext:Button runat="server" ID="btnCancel" Cls="btn btn-default" Text="Cancel">
                                <Listeners>
                                    <Click Handler="#{WBranchTransfer}.hide();" />
                                </Listeners>
                            </ext:Button>
                        </div>
                    </td>
                </tr>
            </table>
        </Content>
    </ext:Window>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
