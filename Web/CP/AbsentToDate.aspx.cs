﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using BLL;
using DAL;
using Utils.Web;
using Utils.Helper;
using System.Text;

namespace Web.CP
{



    public partial class AbsentToDate : BasePage
    {
        CompanyManager compMgr = new CompanyManager();



        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                Initialise();          

        }

        

        public void Initialise()
        {

            calHireDate.Text = CommonManager.Setting.AttendanceUpto;
        }


        public void btnSave_Click(object sender, EventArgs e)
        {
            if(CommonManager.CompanySetting.WhichCompany==WhichCompany.NIBL)
            {
                if(string.IsNullOrEmpty(calHireDate.Text.Trim()))
                {
                    NewMessage.ShowWarningMessage("To date is required.");
                    return;
                }
            }

            CompanyManager.SaveAbsentToDate(calHireDate.Text.Trim());
            divMsgCtl.InnerHtml = "Date saved.";
            divMsgCtl.Hide = false;

            hdn.Value =
                "Absent processing upto date : <br>" + CommonManager.Setting.AttendanceUpto +
                " (" + (CommonManager.Setting.AttendanceUptoDate == null ? "" :
                 CommonManager.Setting.AttendanceUptoDate.Value.ToShortDateString()) + ")";
        }
     

      
    }

}

