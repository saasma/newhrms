﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Manager;
using Utils.Helper;
using BLL;
using DAL;
using Utils.Web;
using BLL.Base;

namespace Web.CP
{
    public partial class ManageGratuityRule : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.AdministratorTools;
            }
        }

        CommonManager commonMgr = new CommonManager();
        PayManager payMgr = new PayManager();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialise();
            }
            if (Request.Form["__EVENTTARGET"] != null && Request.Form["__EVENTTARGET"].Equals("Reload"))
            {
                LoadGratuity();               
            }

            JavascriptHelper.AttachPopUpCode(Page, "insertUpdateGratuityRule", "AEGratuity.aspx", 400, 300);
            JavascriptHelper.AttachPopUpCode(Page, "insertUpdateGratuityClass", "AEGratuityClass.aspx", 400, 300);
        }

        void Initialise()
        {
            IncomeList();
            LoadGratuity();


        


        }
        void LoadGratuity()
        {
            gvwGratuityRules.DataSource = commonMgr.GetAllGratuity(
                SessionManager.CurrentCompanyId);
            gvwGratuityRules.DataBind();

            gridClassList.DataSource = commonMgr.GetAllGratuityClass(
                SessionManager.CurrentCompanyId);
            gridClassList.DataBind();
        }
        public string GetApplicableTo(object val)
        {
            if (val == null)
                return "";

            List<GratuityClass> list = 
                new CommonManager().GetAllGratuityClass(SessionManager.CurrentCompanyId);

            int value = int.Parse(val.ToString());

            if (list.Any(x => x.GratuityClassID == value))
                return list.FirstOrDefault(x => x.GratuityClassID == value).Name;

            return "";
        }

        protected void gvwClass_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int GratuityClassId = (int)gvwGratuityRules.DataKeys[e.RowIndex][0];

            if (GratuityClassId != 0)
            {
                bool deleted = CommonManager.DeleteGratuityClass(GratuityClassId);

                if (deleted)
                {

                    divMsgCtl.InnerHtml = "Class deleted.";
                    divMsgCtl.Hide = false;
                }
                else
                {
                    divMsgCtl.InnerHtml = "Class is being used, can not be deleted.";
                    divMsgCtl.Hide = false;
                }

            }
            LoadGratuity();
        }

        protected void gvwDepartments_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int GratuityRuleId = (int)gvwGratuityRules.DataKeys[e.RowIndex][0];

            if (GratuityRuleId != 0)
            {
                CommonManager.DeleteGratuityRule(GratuityRuleId);

                divMsgCtl.InnerHtml = Resources.Messages.GratutiyRuleDeletedMsg;
                divMsgCtl.Hide = false;
                
                
            }
            LoadGratuity();
        }

        void IncomeList()
        {
            List<PIncome> list = PayManager.GetIncomeListByCompany(SessionManager.CurrentCompanyId);

            list.Add(new PIncome { Title = "PF", IncomeId = 0 });
            chkListSalaries.DataSource = list.OrderBy(x => x.Title).ToList();
            chkListSalaries.DataBind();

            


            foreach (GetGratuityIncomeForCompanyResult income in commonMgr.GetGratuityIncomeListForCompany(
                SessionManager.CurrentCompanyId))
            {
                ListItem item = chkListSalaries.Items.FindByValue(income.IncomeId.ToString());
                if (item != null)
                    item.Selected = true;
            }

            bool allChecked = true;
            foreach (ListItem item in chkListSalaries.Items)
            {
                item.Attributes.Add("onclick", string.Format("stateChanged(this,{0})", item.Value));
                if (item.Selected == false)
                    allChecked = false;
            }

            chkAll.Checked = allChecked;



            JobStatus obJs = new JobStatus();
            List<KeyValue> statuslist = obJs.GetMembers();
            statuslist.RemoveAt(0);
            chkStatusList.DataSource = statuslist;
            chkStatusList.DataBind();

            List<GratuityStatus> selectedStatus = commonMgr.GetGratuityStatus();
            foreach (GratuityStatus item in selectedStatus)
            {
                ListItem selitem = chkStatusList.Items.FindByValue(item.Status.ToString());
                if (selitem != null)
                    selitem.Selected = true;
            }

            if (CommonManager.Setting.GratuityFromStatusChange != null
                && CommonManager.Setting.GratuityFromStatusChange.Value)
            {
                rdbGratutiyFromJoining.Checked = false;
                rdbGratutiyFromStatusChange.Checked = true;
            }

            //if (CommonManager.Setting.GratuityDeductStopPayment != null)
            //    chkDeductStopPayment.Checked = CommonManager.Setting.GratuityDeductStopPayment.Value;
            if (CommonManager.Setting.GratuityDeductLWPUPL != null)
                chkDeductUPLLWP.Checked = CommonManager.Setting.GratuityDeductLWPUPL.Value;
            if (CommonManager.Setting.GratuityDeductAbsent != null)
                chkDeductAbsent.Checked = CommonManager.Setting.GratuityDeductAbsent.Value;
            

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

            string str="";

            foreach(ListItem item in chkListSalaries.Items)
            {
                if (item.Selected)
                {
                    if (str == "")
                        str += item.Value;
                    else
                        str += "," + item.Value;
                }
            }

            List<GratuityStatus> status = new List<GratuityStatus>();
            foreach (ListItem item in chkStatusList.Items)
            {
                if (item.Selected)
                    status.Add(new GratuityStatus { Status = int.Parse(item.Value) });
            }

            bool fromStatusChange = rdbGratutiyFromStatusChange.Checked;

            CommonManager.DeleteAndSaveAllGratuityIncomes(str, status, fromStatusChange, true,
                chkDeductAbsent.Checked, chkDeductUPLLWP.Checked);

            divMsgCtl.InnerHtml = Resources.Messages.GratuityIncomeListSaved;
            divMsgCtl.Hide = false;

        }

        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            section.Visible = true;
            //LoadDepartments();
            //ClearDepartmentFields();
            //details.Visible = true;
            //txtDepartmentName.Focus();
        }
    }
}
