<%@ Control Language="C#" EnableViewState="true" AutoEventWireup="true" CodeBehind="VoucherListCtlGlobus.ascx.cs"
    Inherits="Web.VoucherListCtlGlobus" %>
<%@ Import Namespace="BLL.Manager" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<div class="pageheader">
    <div class="media">
        <div class="media-body">
            <h4 id="title" runat="server">
                Accounting Voucher
            </h4>
        </div>
    </div>
</div>
<div class="alert alert-warning" style="margin: 20px 20px 0px 20px;">
    <span runat="server" visible="false" id="salaryNotSaved" style="color: #FF3400">Note
        : All employees salary not saved for this period, so voucher report is not complete.</span>
    <span runat="server" visible="false" id="spanWarningForIncomeDeductionHead"></span>
</div>
<div class="contentpanel">
    <asp:HiddenField runat="server" ID="hiddenAddOnId" />
    <asp:HiddenField runat="server" ID="hiddenSelectedEmp" />
    <div class="attribute" style="padding: 10px">
        <table class="fieldTable">
            <tr>
                <td runat="server" id="rowPayrollFrom2">
                    <strong>Payroll</strong>
                    <br />
                    <asp:DropDownList ID="ddlPayrollFromMonth" runat="server">
                    </asp:DropDownList>
                    <asp:DropDownList ID="ddlPayrollFromYear" runat="server">
                    </asp:DropDownList>
                </td>
                <td>
                    Category<br />
                    <asp:DropDownList runat="server" ID="ddlCategory" AppendDataBoundItems="true" DataValueField="CategoryID"
                        DataTextField="Name" Width="100px">
                        <asp:ListItem Text="--Select Category--" Value="-1"></asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    Retire Filter<br />
                    <asp:DropDownList runat="server" ID="ddlRetirementType" Width="100px">
                        <asp:ListItem Text="Active only" Value="false" />
                        <asp:ListItem Text="All" Value="All" />
                        <asp:ListItem Text="Retired only" Value="true" />
                    </asp:DropDownList>
                </td>
                <td>
                    Multi Add On<br />
                    <asp:DropDownList ID="ddlAddOnName" runat="server" Width="120px" DataTextField="Name"
                        DataValueField="AddOnId">
                        <asp:ListItem Text="--Select Add On--" Value="-1" />
                    </asp:DropDownList>
                </td>
                <td>
                    <br />
                    <asp:DropDownList ID="ddlAddOnDateFilter" runat="server" Width="160px" DataTextField="Text"
                        DataValueField="Text">
                    </asp:DropDownList>
                </td>
                <td valign="bottom">
                    <asp:Button ID="btnLoad" OnClick="btnLoad_Click" Style='float: left; width: 100px;'
                        CssClass="btn btn-default btn-sm btn-sect" runat="server" Text="View" />
                </td>
                <td valign="bottom">
                    <ext:Button ID="btnLoadEmployeeFilter" ToolTip="Employee Filter works only for Retired list and Add-On list"
                        OnClick="btnLoadEmployeeFilter_Click" AutoPostBack="true" runat="server" Cls="btn btn-success"
                        Text="Employee Filter" Width="120">
                        <%--<DirectEvents>
                            <Click OnEvent="btnLoadEmployeeFilter_Click">
                                <EventMask ShowMask="true" />
                            </Click>
                        </DirectEvents>--%>
                    </ext:Button>
                </td>
                <td style="padding-left: 20px" valign="bottom">
                    <asp:Button ID="btnExport" CssClass="excel" runat="server" Text="Export" OnClick="btnExport_Click" />
                    <br />
                    <%-- </td>
                <td rowspan="2" style="padding-left: 20px" valign="bottom">--%>
                    
                </td>
            </tr>
        </table>
    </div>
    <div class="alert alert-warning" runat="server" id="msg">
        <span runat="server" visible="false" id="Span1" style="color: #FF3400">Note : All employees
            salary not saved for this period, so voucher report is not complete.</span>
        <span runat="server" visible="false" id="span2"></span>
    </div>
    <div class="clear gridBlock">
        <table>
            <tr>
                <td valign="top">
                    <cc2:EmptyDisplayGridView EnableViewState="false" CssClass="table table-primary mb30 table-bordered table-hover"
                        UseAccessibleHeader="true" ShowHeaderWhenEmpty="True" ID="gridGlobusCivil"
                        runat="server" AutoGenerateColumns="False" CellPadding="4" GridLines="None" ShowFooterWhenEmpty="False">
                        <RowStyle BackColor="#E3EAEB" />
                        <Columns>
                            <asp:BoundField DataField="COMPANY" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                                HeaderStyle-HorizontalAlign="Left" HeaderText="COMPANY"></asp:BoundField>
                            <asp:BoundField DataField="DEBITACCTNO" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                                HeaderStyle-HorizontalAlign="Left" HeaderText="DEBIT.ACCT.NO"></asp:BoundField>
                            <asp:BoundField DataField="DEBITCURRENCY" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                                HeaderStyle-HorizontalAlign="Left" HeaderText="DEBIT.CURRENCY"></asp:BoundField>
                            <asp:BoundField DataField="DEBITAMOUNT" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                                HeaderStyle-HorizontalAlign="Left" HeaderText="DEBIT.AMOUNT"></asp:BoundField>
                            <asp:BoundField DataField="DEBITTHEIRREF" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                                HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" HeaderText="DEBIT.THEIR.REF"
                             />
                            <asp:BoundField DataField="ORDERINGCUST" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                                HeaderStyle-HorizontalAlign="Left" HeaderText="ORDERING.CUST"></asp:BoundField>
                            <asp:BoundField DataField="PAYMENTDETAILS" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                                HeaderStyle-HorizontalAlign="Left" HeaderText="PAYMENT.DETAILS"></asp:BoundField>
                            <asp:BoundField DataField="CREDITACCTNO" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                                HeaderStyle-HorizontalAlign="Left" HeaderText="CREDIT.ACCT.NO"></asp:BoundField>

                                <asp:BoundField DataField="CREDITCURRENCY" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                                HeaderStyle-HorizontalAlign="Left" HeaderText="CREDIT.CURRENCY"></asp:BoundField>
                                <asp:BoundField DataField="CREDITTHEIRREF" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                                HeaderStyle-HorizontalAlign="Left" HeaderText="CREDIT.THEIR.REF"></asp:BoundField>
                                <asp:BoundField DataField="PROFITCENTREDEPT" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                                HeaderStyle-HorizontalAlign="Left" HeaderText="PROFIT.CENTRE.DEPT"></asp:BoundField>
                        </Columns>
                        <RowStyle CssClass="odd" />
                        <AlternatingRowStyle CssClass="even" />
                        <EmptyDataTemplate>
                            <b>No employee list. </b>
                        </EmptyDataTemplate>
                    </cc2:EmptyDisplayGridView>
                    <cc2:EmptyDisplayGridView EnableViewState="false" CssClass="table table-primary mb30 table-bordered table-hover"
                        UseAccessibleHeader="true" ShowHeaderWhenEmpty="True" ID="gridGlobusVoucher"
                        runat="server" AutoGenerateColumns="False" CellPadding="4" GridLines="None" ShowFooterWhenEmpty="False">
                        <RowStyle BackColor="#E3EAEB" />
                        <Columns>
                            <asp:BoundField DataField="HeadOfficeCode" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                                HeaderStyle-HorizontalAlign="Left" HeaderText="Account"></asp:BoundField>
                            <asp:BoundField DataField="CrCode" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                                HeaderStyle-HorizontalAlign="Left" HeaderText="MainCode"></asp:BoundField>
                            <asp:BoundField DataField="TranCode" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                                HeaderStyle-HorizontalAlign="Left" HeaderText="Dr"></asp:BoundField>
                            <asp:BoundField DataField="Department" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                                HeaderStyle-HorizontalAlign="Left" HeaderText="Branch"></asp:BoundField>
                            <asp:BoundField DataField="Debit" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                                HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" HeaderText="Amount"
                                DataFormatString="{0:N2}" />
                            <asp:BoundField DataField="Desc1" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                                HeaderStyle-HorizontalAlign="Left" HeaderText="Desc1"></asp:BoundField>
                            <asp:BoundField DataField="MainCode" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                                HeaderStyle-HorizontalAlign="Left" HeaderText="Cr"></asp:BoundField>
                        </Columns>
                        <RowStyle CssClass="odd" />
                        <AlternatingRowStyle CssClass="even" />
                        <EmptyDataTemplate>
                            <b>No employee list. </b>
                        </EmptyDataTemplate>
                    </cc2:EmptyDisplayGridView>

                    <cc2:EmptyDisplayGridView Visible="false" EnableViewState="false" CssClass="table table-primary mb30 table-bordered table-hover"
                        UseAccessibleHeader="true" ShowHeaderWhenEmpty="True" ID="gridGlobusOtherVoucher"
                        runat="server" AutoGenerateColumns="False" CellPadding="4" GridLines="None" ShowFooterWhenEmpty="False">
                        <RowStyle BackColor="#E3EAEB" />
                        <Columns>
                            <asp:BoundField DataField="Account" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                                HeaderStyle-HorizontalAlign="Left" HeaderText="Account number"></asp:BoundField>
                            <asp:BoundField DataField="Sign" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                                HeaderStyle-HorizontalAlign="Left" HeaderText="Sign"></asp:BoundField>
                             <asp:BoundField DataField="Amount" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                                HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" HeaderText="Amount"
                                DataFormatString="{0:N2}" />
                            <asp:BoundField DataField="TransactionCode" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                                HeaderStyle-HorizontalAlign="Left" HeaderText="Transaction Code"></asp:BoundField>
                            <asp:BoundField DataField="Narrative" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                                HeaderStyle-HorizontalAlign="Left" HeaderText="Narrative"></asp:BoundField>
                           

                        </Columns>
                        <RowStyle CssClass="odd" />
                        <AlternatingRowStyle CssClass="even" />
                        <EmptyDataTemplate>
                            <b>No employee list. </b>
                        </EmptyDataTemplate>
                    </cc2:EmptyDisplayGridView>
                </td>
                <td valign="top" style="padding-left: 15px">
                </td>
            </tr>
        </table>
    </div>
    <ext:Window ButtonAlign="Left" runat="server" Layout="BorderLayout" ID="window" Title="Select employees for Add-On filter : "
        Width="500" Height="600" Hidden="true">
        <Items>
            <ext:GridPanel Region="Center" ID="grid" runat="server" Cls="itemgrid" Scroll="Both">
                <Store>
                    <ext:Store ID="Store1" runat="server" AutoLoad="true">
                        <%-- <Proxy>
                            <ext:PageProxy />
                        </Proxy>--%>
                        <Model>
                            <ext:Model ID="Model4" runat="server" IDProperty="ID">
                                <Fields>
                                    <ext:ModelField Name="ID" Type="Int" />
                                    <ext:ModelField Name="Text" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:Column ID="Column4" Sortable="true" MenuDisabled="true" runat="server" Text="EIN"
                            Width="60" Align="Center" DataIndex="ID">
                        </ext:Column>
                        <ext:Column ID="colEmployeeName" Sortable="true" MenuDisabled="true" runat="server"
                            Text="Name" Width="160" Align="Left" DataIndex="Text">
                        </ext:Column>
                    </Columns>
                </ColumnModel>
                <Listeners>
                    <SelectionChange Handler="#{window}.setTitle('Select employees for Add-On filter : ' + this.getSelectionModel().selected.items.length + ' employees selected');" />
                </Listeners>
                <SelectionModel>
                    <ext:CheckboxSelectionModel ID="CheckboxSelectionModel1" runat="server" Mode="Simple">
                        <%--<CustomConfig>
                            <ext:ConfigItem Name="renderer" Value="checkboxRenderer" Mode="Raw" />
                        </CustomConfig>
                        <Listeners>
                            <BeforeSelect Handler="return (record.data.Status == 0 || record.data.Status == 3);" />
                        </Listeners>--%>
                    </ext:CheckboxSelectionModel>
                </SelectionModel>
                <View>
                    <ext:GridView ID="GridView1" runat="server" StripeRows="true" />
                </View>
            </ext:GridPanel>
        </Items>
        <Buttons>
            <ext:Button ID="btnLoadSelectedEmpVoucher" AutoPostBack="true" OnClick="btnLoadSelectedEmpVoucher_Click"
                runat="server" Cls="btn btn-primary" Text="Select Employees" Width="120">
            </ext:Button>
            <ext:Button ID="btnClearSelection" StyleSpec="margin-left:10px" AutoPostBack="true"
                OnClick="btnClearSelection_Click" runat="server" Cls="btn btn-primary" Text="Clear Selection"
                Width="120">
            </ext:Button>
        </Buttons>
    </ext:Window>
    <div class="buttonsDiv">
    </div>
</div>
