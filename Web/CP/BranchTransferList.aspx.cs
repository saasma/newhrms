﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using Utils.Helper;
using System.IO;
using Utils;
using Utils.Security;
using Utils.Calendar;
using Web.Helper;

namespace Web.CP
{
    public partial class BranchTransferList : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.HumanResource;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest && !IsPostBack)
            {
                ColumnTeam.Visible = IsTeamColumnVisible();
                Initialize();

                hdnIsEng.Text = UserManager.IsEnglishDatePreferredInNepaliDateSys().ToString().ToLower();

            }

            if (Request.QueryString["IsEnableImport"] == "true")
            {
                btnImportExcel.Visible = true;
            }

            JavascriptHelper.AttachPopUpCode(Page, "popup", "BranchTransferLeaveSetting.aspx", 1200, 700);
            JavascriptHelper.AttachNonDialogPopUpCode(Page, "BranchTransferHistoryImport", "../ExcelWindow/BranchTranferHistoryImport.aspx", 450, 500);

        }

        private void Initialize()
        {
            if (IsEnglish)
            {
                txtFromDateNep.Hide();
                txtToDateNep.Hide();
            }

            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                int empId = int.Parse(Request.QueryString["ID"]);
                string name = EmployeeManager.GetEmployeeName(empId);

                cmbEmpSearch.GetStore().RemoveAll();
                cmbEmpSearch.InsertItem(0, name, empId);
                cmbEmpSearch.SelectedItem.Value = empId.ToString();
                cmbEmpSearch.SelectedItem.Text = name;
                cmbEmpSearch.UpdateSelectedItems();

                hdnEmployeeId.Text = empId.ToString();

            }

            X.Js.Call("searchList");

            if (CommonManager.IsServiceHistoryEnabled)
            {
                btnNewTransfer.Visible = false;
                CommandColumn1.Visible = false;
            }

            List<HR_Service_Event> eventList = CommonManager.GetServieEventTypes();
            if (eventList.Count > 0)
            {
                cmbEventType.Store[0].DataSource = eventList;
                cmbEventType.Store[0].DataBind();
            }
            else
                reqdEventType.Enabled = false;

            cmbEventFilter.Store[0].DataSource = eventList;
            cmbEventFilter.Store[0].DataBind();

            cmbBranch.Store[0].DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            cmbBranch.Store[0].DataBind();

            cmbSubDepartment.GetStore().DataSource = ListManager.GetSubDepartments();
            cmbSubDepartment.GetStore().DataBind();


            WBranchTransfer.Hide();
            if (SessionManager.CurrentCompany.IsEnglishDate)
                colFromDate.Hide();



        }

        private void Clear()
        {
            txtLetterNumber.Text = "";
            calLetterDate.Text = "";
            cmbBranch.Clear();
            cmbDepartment.Clear();
            cmbSubDepartment.Clear();
            txtSpecialResponsibility.Text = "";
            calDepartureDate.Text = "";
            calFromDate.Text = "";
            txtNote.Text = "";

            cmbEmployee.Clear();
            cmbEmployee.Enable();
            lblEName.Text = "";
            lblEBranch.Text = "";
            lblEDepartment.Text = "";
            lblESince.Text = "";
            lblInTime.Text = "";

            image1.ImageUrl = Page.ResolveUrl(Web.Helper.WebHelper.GetEmployeeNoImage(0));
        }

        private void LoadDepartment(int branchId)
        {
            cmbDepartment.GetStore().DataSource = DepartmentManager.GetAllDepartmentsByBranch(branchId);
            cmbDepartment.GetStore().DataBind();
        }

        protected void Branch_Select(object sender, DirectEventArgs e)
        {
            cmbDepartment.ClearValue();
            LoadDepartment(int.Parse(cmbBranch.SelectedItem.Value));
        }

        protected void btnNewTransfer_Click(object sender, DirectEventArgs e)
        {
            Clear();
            hdnBranchDepartmentId.Text = "";
            hdnEmployeeId.Text = "";
            WBranchTransfer.Show();
        }

        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            int totalRecords = 0;
            int? total = 0;
            DateTime? fromDate = null;
            DateTime? toDate = null;

            int? eventType = null;

            if (txtFromDate.SelectedDate != DateTime.MinValue)
                fromDate = txtFromDate.SelectedDate;

            if (txtToDate.SelectedDate != DateTime.MinValue)
                toDate = txtToDate.SelectedDate;

            if (!string.IsNullOrEmpty(txtFromDateNep.Text.Trim()))
            {
                fromDate = GetEngDate(txtFromDateNep.Text.Trim());
            }
            if (!string.IsNullOrEmpty(txtToDateNep.Text.Trim()))
            {
                toDate = GetEngDate(txtToDateNep.Text.Trim());
            }

            int employeeId = -1;
            string employeeSearch = "";

            int.TryParse(cmbEmpSearch.SelectedItem.Value, out employeeId);

            if (employeeId == 0)
                employeeId = -1;

            if (employeeId == -1)
            {
                if (!string.IsNullOrEmpty(cmbEmpSearch.Text))
                    employeeSearch = cmbEmpSearch.Text.Trim();
            }

            if (cmbEventFilter.SelectedItem != null && cmbEventFilter.SelectedItem.Value != null)
                eventType = int.Parse(cmbEventFilter.SelectedItem.Value);

            bool showJoinedDetails = bool.Parse(cmbShowJoinBranchAlso.SelectedItem.Value);

            List<GetTransferListResult> list = BranchManager.GetTranserList
                (e.Start, int.Parse(cmbPageSize.SelectedItem.Value), ref totalRecords, employeeSearch, employeeId, fromDate, toDate, eventType, showJoinedDetails);

            e.Total = totalRecords;

            //if (list.Any())
            storeEmpList.DataSource = list;
            storeEmpList.DataBind();

        }

        protected Field OnCreateFilterableField(object sender, ColumnBase column, Field defaultField)
        {
            if (column.DataIndex == "BranchDepartmentId")
            {
                ((TextField)defaultField).Icon = Icon.Magnifier;
            }

            return defaultField;
        }


        // Return the number of years, months, days, hours, minutes, seconds,
        // and milliseconds you need to add to from_date to get to_date.
        private void GetElapsedTime(DateTime from_date, DateTime to_date,
            out int years, out int months, out int days, out int hours,
            out int minutes, out int seconds, out int milliseconds)
        {
            // If from_date > to_date, switch them around.
            if (from_date > to_date)
            {
                GetElapsedTime(to_date, from_date,
                    out years, out months, out days, out hours,
                    out minutes, out seconds, out milliseconds);
                years = -years;
                months = -months;
                days = -days;
                hours = -hours;
                minutes = -minutes;
                seconds = -seconds;
                milliseconds = -milliseconds;
            }
            else
            {
                // Handle the years.
                years = to_date.Year - from_date.Year;

                // See if we went too far.
                DateTime test_date = from_date.AddMonths(12 * years);
                if (test_date > to_date)
                {
                    years--;
                    test_date = from_date.AddMonths(12 * years);
                }

                // Add months until we go too far.
                months = 0;
                while (test_date <= to_date)
                {
                    months++;
                    test_date = from_date.AddMonths(12 * years + months);
                }
                months--;

                // Subtract to see how many more days,
                // hours, minutes, etc. we need.
                from_date = from_date.AddMonths(12 * years + months);
                TimeSpan remainder = to_date - from_date;
                days = remainder.Days;
                hours = remainder.Hours;
                minutes = remainder.Minutes;
                seconds = remainder.Seconds;
                milliseconds = remainder.Milliseconds;
            }
        }

        private void EmployeeDetails(int employeeId)
        {
            EEmployee emp = EmployeeManager.GetEmployeeById(employeeId);

            bool hasPhoto = false;
            if (emp.HHumanResources.Count > 0)
            {
                if (!string.IsNullOrEmpty(emp.HHumanResources[0].UrlPhotoThumbnail))
                {
                    image1.ImageUrl = Page.ResolveUrl(Config.UploadLocation + "/" + emp.HHumanResources[0].UrlPhotoThumbnail);
                    if (File.Exists(Server.MapPath(image1.ImageUrl)))
                        hasPhoto = true;
                }
            }
            if (!hasPhoto)
            {
                image1.ImageUrl = Page.ResolveUrl(Web.Helper.WebHelper.GetEmployeeNoImage(employeeId));
            }

            lblEININo.Text = "EIN : " + emp.EmployeeId + ", I No : " + emp.EHumanResources[0].IdCardNo;

            lblEName.Text = emp.Title + " " + emp.Name;
            lblEBranch.Text = emp.Branch.Name;
            lblEDepartment.Text = emp.Department.Name;

            int years, months, days, hours;
            int minutes, seconds, milliseconds;
            ServicePeroid firstStatus = EmployeeManager.GetServicePeroidDate(emp.EmployeeId);
            GetElapsedTime(firstStatus.FromDateEng, BLL.BaseBiz.GetCurrentDateAndTime(), out years, out months, out days, out hours, out minutes, out seconds, out milliseconds);



            lblESince.Text = "Since " +
                (IsEnglish ? firstStatus.FromDateEng.ToString("yyyy-MMM-dd") : CustomDate.GetCustomDateFromString(firstStatus.FromDate, IsEnglish).ToStringMonthEng() + " (" + firstStatus.FromDateEng.ToString("yyyy-MMM-dd") + ")");
            lblInTime.Text = string.Format("For {0} years {1} months {2} days",
                years, months, days);
        }

        protected void btnSelectEmp_Click(object sender, DirectEventArgs e)
        {
            int employeeId = int.Parse(cmbEmployee.SelectedItem.Value);
            // hdnEmployeeId.Text = employeeId.ToString();

            EEmployee emp = EmployeeManager.GetEmployeeById(employeeId);

            if (emp == null)
            {
                buttonsBar.Visible = false;
                NewMessage.ShowWarningMessage("Please select an employee.");
                return;
            }
            else
                buttonsBar.Visible = true;

            //ClearFields();
            CommonManager.SaveFirstIBranchfNotExists(employeeId);

            block.Visible = true;

            EmployeeDetails(employeeId);


        }

        protected void btnEdit_Click(object sender, DirectEventArgs e)
        {
            int branchDepartmentId = int.Parse(hdnBranchDepartmentId.Text);
            int employeeId = CommonManager.GetBranchDepartmentHistoryById(branchDepartmentId).EmployeeId.Value;
            EEmployee emp = EmployeeManager.GetEmployeeById(employeeId);

            EmployeeDetails(employeeId);

            BranchDepartmentHistory history = CommonManager.GetBranchDepartmentHistoryById(branchDepartmentId);

            cmbEmployee.Text = emp.Name;
            cmbEmployee.Disable();

            if (history.EventID != null)
                cmbEventType.SetValue(history.EventID.ToString());
            else
                cmbEventType.ClearValue();

            cmbBranch.GetStore().ClearFilter();
            cmbBranch.SetValue(history.BranchId.Value.ToString());

            LoadDepartment(history.BranchId.Value);

            cmbDepartment.GetStore().ClearFilter();
            cmbDepartment.SetValue(history.DepeartmentId.Value.ToString());
            txtLetterNumber.Text = history.LetterNumber;
            txtSpecialResponsibility.Text = history.SpecialResponsibility;


            if (IsEnglish == false && SessionManager.User.IsEnglishPreferred != null && SessionManager.User.IsEnglishPreferred)
            {

                if (history.LetterDateEng != null)
                    calLetterDate.Text = history.LetterDateEng.Value.ToString("yyyy/MM/dd");

                if (history.FromDateEng != null)
                    calFromDate.Text = history.FromDateEng.Value.ToString("yyyy/MM/dd");

                if (history.DepartureDateEng != null)
                    calDepartureDate.Text = history.DepartureDateEng.Value.ToString("yyyy/MM/dd");

            }
            else
            {
                calLetterDate.Text = history.LetterDate;
                calFromDate.Text = history.FromDate;
                calDepartureDate.Text = history.DepartureDate;
            }

            txtNote.Text = history.Note;

            if (history.SubDepartmentId != null)
                cmbSubDepartment.SetValue(history.SubDepartmentId.Value.ToString());

            btnSave.Text = Resources.Messages.Update;
            WBranchTransfer.Show();
        }

        protected void btnDelete_Click(object sender, DirectEventArgs e)
        {
            int branchDepartmentId = int.Parse(hdnBranchDepartmentId.Text);

            Status status = CommonManager.DeleteBranchTransfer(branchDepartmentId);

            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Branch transfer deleted.");
                X.Js.Call("searchList");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }


        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveBrTran");
            if (Page.IsValid)
            {
                BranchDepartmentHistory obj = new BranchDepartmentHistory();

                if (!string.IsNullOrEmpty(txtLetterNumber.Text.Trim()))
                    obj.LetterNumber = txtLetterNumber.Text.Trim();

                if (IsEnglish == false && SessionManager.User.IsEnglishPreferred != null && SessionManager.User.IsEnglishPreferred)
                {
                    obj.LetterDateEng = BLL.BaseBiz.GetEngDate(calLetterDate.Text.Trim(), true);
                    obj.LetterDate = BLL.BaseBiz.GetAppropriateDate(obj.LetterDateEng.Value);

                    obj.DepartureDateEng = BLL.BaseBiz.GetEngDate(calDepartureDate.Text.Trim(), true);
                    obj.DepartureDate = BLL.BaseBiz.GetAppropriateDate(obj.DepartureDateEng.Value);

                    obj.FromDateEng = BLL.BaseBiz.GetEngDate(calFromDate.Text.Trim(), true);
                    obj.FromDate = BLL.BaseBiz.GetAppropriateDate(obj.FromDateEng.Value);
                }
                else
                {
                    obj.FromDate = calFromDate.Text.Trim();
                    obj.FromDateEng = GetEngDate(obj.FromDate);

                    obj.LetterDate = calLetterDate.Text.Trim();
                    obj.LetterDateEng = GetEngDate(obj.LetterDate);

                    obj.DepartureDate = calDepartureDate.Text.Trim();
                    obj.DepartureDateEng = GetEngDate(obj.DepartureDate);
                }


                obj.BranchId = int.Parse(cmbBranch.SelectedItem.Value);
                obj.DepeartmentId = int.Parse(cmbDepartment.SelectedItem.Value);

                if (!string.IsNullOrEmpty(txtSpecialResponsibility.Text.Trim()))
                    obj.SpecialResponsibility = txtSpecialResponsibility.Text.Trim();



                if (cmbEventType.SelectedItem != null && cmbEventType.SelectedItem.Value != null)
                    obj.EventID = int.Parse(cmbEventType.SelectedItem.Value);


                if (!string.IsNullOrEmpty(txtNote.Text.Trim()))
                    obj.Note = txtNote.Text.Trim();

                if (!string.IsNullOrEmpty(cmbSubDepartment.Text))
                    obj.SubDepartmentId = int.Parse(cmbSubDepartment.SelectedItem.Value);

                obj.EmployeeId = int.Parse(hdnEmployeeId.Text);

                if (!string.IsNullOrEmpty(hdnBranchDepartmentId.Text))
                {
                    obj.BranchDepartmentId = int.Parse(hdnBranchDepartmentId.Text);

                    //// If the it is last branch transfer then only allow to update
                    //if (CommonManager.IsLastBranchTransfer(obj.BranchDepartmentId, obj.EmployeeId.Value) == false)
                    //{
                    //    NewMessage.ShowWarningMessage("Only latest branch transfer can be edited.");
                    //    return;
                    //}

                    CommonManager.UpdateBranchDepartmentHistory(obj);

                    hdnMsg.Text = "Branch transfer updated.";
                }
                else
                {
                    CommonManager.SaveBranchDepartmentHistory(obj);
                    hdnMsg.Text = "Branch transfer saved.";
                }

                string updateJS = string.Format("opener.updateBranchDepartmentFromChangeHistory({0},{1},'{2}','{3}');",
                    obj.BranchId, obj.DepeartmentId, new DepartmentManager().GetById(obj.DepeartmentId.Value).Name, obj.SubDepartmentId);
                Page.ClientScript.RegisterClientScriptBlock(GetType(), "sdfdsdfdsfsd", updateJS, true);


                btnCancel_Click(null, null);

            }
        }

        public bool IsTeamColumnVisible()
        {
            if (
                (CommonManager.Setting.LeaveRequestSettingUsingPreDefinedList != null &&
                CommonManager.Setting.LeaveRequestSettingUsingPreDefinedList.Value)

                 ||
                (CommonManager.Setting.LeaveRequestSettngUsingOneToOne != null &&
                CommonManager.Setting.LeaveRequestSettngUsingOneToOne.Value)
                )
                return false;

            return true;
        }

        protected void btnCancel_Click(object sender, DirectEventArgs e)
        {

            btnSave.Text = "Save";

            if (string.IsNullOrEmpty(hdnEmployeeId.Text))
            {
                WBranchTransfer.Close();
                return;
            }

            int employeeId = int.Parse(hdnEmployeeId.Text);

            if (employeeId != -1)
            {
                CommonManager.SaveFirstIBranchfNotExists(employeeId);
                Clear();
            }

            if (e == null)
                NewMessage.ShowNormalMessage(hdnMsg.Text);

            WBranchTransfer.Close();

            X.Js.Call("searchList");

        }

        public void btnExport_Click(object sender, EventArgs e)
        {
            int totalRecords = 0;
            DateTime? fromDate = null;
            DateTime? toDate = null;


            if (txtFromDate.SelectedDate != DateTime.MinValue)
                fromDate = txtFromDate.SelectedDate;

            if (txtToDate.SelectedDate != DateTime.MinValue)
                toDate = txtToDate.SelectedDate;

            if (!string.IsNullOrEmpty(txtFromDateNep.Text.Trim()))
            {
                fromDate = GetEngDate(txtFromDateNep.Text.Trim());
            }
            if (!string.IsNullOrEmpty(txtToDateNep.Text.Trim()))
            {
                toDate = GetEngDate(txtToDateNep.Text.Trim());
            }

            int employeeId = -1;
            string employeeSearch = "";

            int.TryParse(cmbEmpSearch.SelectedItem.Value, out employeeId);

            if (employeeId == 0)
                employeeId = -1;

            if (employeeId == -1)
            {
                if (!string.IsNullOrEmpty(cmbEmpSearch.Text))
                    employeeSearch = cmbEmpSearch.Text.Trim();
            }

            bool showJoinedDetails = bool.Parse(cmbShowJoinBranchAlso.SelectedItem.Value);

            int? eventType = null;

            if (cmbEventFilter.SelectedItem != null && cmbEventFilter.SelectedItem.Value != null)
                eventType = int.Parse(cmbEventFilter.SelectedItem.Value);

            List<GetTransferListResult> list = BranchManager.GetTranserList
                (0, 999999, ref totalRecords, employeeSearch, employeeId, fromDate, toDate, eventType, showJoinedDetails);

            foreach (var item in list)
            {
                item.FromDateEngFormatted = WebHelper.FormatDateForExcel(item.FromDateEng);
                item.AppointmentEngDateText = WebHelper.FormatDateForExcel(item.AppointmentEngDate);
            }

            Bll.ExcelHelper.ExportToExcel("Branch Transfer Report", list,
                new List<String>() {"AppointmentEngDate","TotalRows","BranchDepartmentId", "FromDateEng","Branch","Department","Type","EventID", "TimeElapsed", "FromBranchId", "BranchId", "DepeartmentId", "Note", "IsFirst", "CreatedBy", "CreatedOn",
                "ModifiedBy", "ModifiedOn", "FromDepartmentId", "LetterNumber", "LetterDate", "LetterDateEng", "SpecialResponsibility", "DepartureDate", "DepartureDateEng", "Status", "AttendanceDate", "AttendanceDateEng",
                "IsDepartmentTransfer", "SubDepartmentId", "FromSubDepartmentId", "ActionType"},
            new List<String>() { },
            new Dictionary<string, string>() { {"RowNumber","SN"}, {"AppointmentEngDateText","Appoint Date"},
                    {"IdCardNo","I No"}, {"LevelPosition","Position"}, { "FromDate", "Transfer Date" },
                    { "FromDateEngFormatted", "Transfer Date(AD)" }, { "EmployeeId", "EIN" }, { "EmployeeName", "Employee Name" },
                    { "FromBranch", "From Branch" }, { "ToBranch", "To Branch" } },
            new List<string>() { }
            , new List<string> { }
            , new List<string> { "FromDateEngFormatted", "AppointmentEngDateText" }
            , new Dictionary<string, string>() { }
            , new List<string> { "RowNumber", "FromDate", "FromDateEngFormatted", "EmployeeId", "IdCardNo", "Name", "FromBranch", "FromDepartment", "ToBranch", "ToDepartment", "AppointmentEngDateText", "LevelPosition" });


        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            bool isEng = false;
            if (UserManager.IsEnglishDatePreferredInNepaliDateSys())
                isEng = true;

            UserControl ctrl = Master.ContentHeaderCtrl();
            ResourceManager rm = ctrl.FindControl("ResourceManager1") as ResourceManager;

            rm.RegisterClientInitScript("Date11111", string.Format("isEnglish = {0};", isEng.ToString().ToLower()));

            rm.RegisterClientInitScript("Date22222", string.Format("todayDate = '{0}';function calendarOnClick(event, sourceElement, e3){{}};",
                                                                      CustomDate.GetTodayDate(isEng).ToString()));
        }


    }
}