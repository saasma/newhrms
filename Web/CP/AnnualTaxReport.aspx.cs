﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using Utils.Helper;
using Utils.Web;
using BLL.Base;
using DAL;
using Utils.Base;
using BLL.BO;

namespace Web
{


    public partial class AnnualTaxReport : BasePage
    {


        protected void Page_Load(object sender, EventArgs e)
        {

          
           

            if (!IsPostBack)
            {
                List<FinancialDate> list = new CommonManager().GetAllFinancialDates(); ;

                foreach (FinancialDate item in list)
                    item.SetName(IsEnglish);

                ddlYear.DataSource = list; 
                ddlYear.DataBind();

                ddlYear.SelectedIndex = list.Count;


            }

            {

                BindEmployees();

            }
        }

        public void btnLoad_Click(object sender, EventArgs e)
        {
            BindEmployees();
        }

        public void BindEmployees()
        {



            int yearid = int.Parse(ddlYear.SelectedValue);

            if (string.IsNullOrEmpty(hiddenEmployeeID.Value))
                return;
            int empId = int.Parse(hiddenEmployeeID.Value);


            int periodId = CalculationManager.GetLastPeriodForYear(yearid);
                //CommonManager.GetLastPayrollPeriod().PayrollPeriodId;


            string data = CalculationManager.GetSalaryCalcForTaxDetails(empId,
                SessionManager.CurrentCompanyId, periodId);

            bool readingSumForMiddleFiscalYearStartedReqd = false;
            int startingPayrollPeriodId = 0, endingPayrollPeriodId = 0;
            CalculationManager.GenerateForPastIncome(
                    periodId, ref readingSumForMiddleFiscalYearStartedReqd,
                    ref startingPayrollPeriodId, ref endingPayrollPeriodId);


            

            List<AnnualTaxBO> items = new List<AnnualTaxBO>();
            List<GetSalaryListResult> salary = BLL.BaseBiz.PayrollDataContext.GetSalaryList(empId, startingPayrollPeriodId, periodId).ToList();
            List<GetAddOnListResult> addonList = BLL.BaseBiz.PayrollDataContext.GetAddOnList(empId, startingPayrollPeriodId, periodId).ToList();

            List<PayrollPeriod> yearPeriods = BLL.BaseBiz.PayrollDataContext
                .PayrollPeriods.Where(x => x.FinancialDateId == yearid).OrderBy(x => x.PayrollPeriodId).ToList();

            // tax data
            string[] rows = data.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

            AnnualTaxBO total = new AnnualTaxBO { Name = "Total" };

            List<PIncome> incomes = PayManager.GetIncomeList().OrderBy(x => x.Order).ToList();
            incomes.Add(new PIncome { IncomeId = 0, Title = "Employee PF" });
            incomes.Add(new PIncome { IncomeId = -1, Title = "Leave Encashment" });
            incomes.Add(new PIncome { IncomeId = -2, Title = "Insurance Income" });

            foreach (PIncome income in incomes)
            {

                int type = 1;
                int sourceid = income.IncomeId;
                if (income.Calculation == IncomeCalculation.DEEMED_INCOME)
                {
                    type = 25;
                    sourceid = income.IncomeId;
                }
                else if (income.IncomeId == 0)
                {
                    type = (int)CalculationColumnType.IncomePF;
                    sourceid = (int)CalculationColumnType.IncomePF;
                }
                else if (income.IncomeId == -1)
                {
                    type = (int)CalculationColumnType.IncomeLeaveEncasement;
                    sourceid = (int)CalculationColumnType.IncomeLeaveEncasement;
                }
                else if (income.IncomeId == -2)
                {
                    type = (int)CalculationColumnType.Insurance;
                    sourceid = (int)CalculationColumnType.Insurance;
                }


                AnnualTaxBO item = new AnnualTaxBO();
                item.Name = income.Title;

                decimal currentAmount = 0;
                decimal futureAmount = 0;
                decimal futureRem = 0;
                decimal noneCashLikeInsuranceOrAddFromBeginning = 0;

                SetForecast(rows, type, sourceid, ref currentAmount, ref futureAmount, ref futureRem, ref noneCashLikeInsuranceOrAddFromBeginning);

                item.OneTime = noneCashLikeInsuranceOrAddFromBeginning;

                item.M4Amount = GetAmount(salary, yearPeriods, MonthEnum.Sharwan, income, currentAmount, futureAmount, periodId,type,sourceid);
                item.M5Amount = GetAmount(salary, yearPeriods, MonthEnum.Bhadra, income, currentAmount, futureAmount, periodId, type, sourceid);
                item.M6Amount = GetAmount(salary, yearPeriods, MonthEnum.Ashwin, income, currentAmount, futureAmount, periodId, type, sourceid);
                item.M7Amount = GetAmount(salary, yearPeriods, MonthEnum.Kartik, income, currentAmount, futureAmount, periodId, type, sourceid);
                item.M8Amount = GetAmount(salary, yearPeriods, MonthEnum.Mangsir, income, currentAmount, futureAmount, periodId, type, sourceid);
                item.M9Amount = GetAmount(salary, yearPeriods, MonthEnum.Poush, income, currentAmount, futureAmount, periodId, type, sourceid);
                item.M10Amount = GetAmount(salary, yearPeriods, MonthEnum.Magh, income, currentAmount, futureAmount, periodId, type, sourceid);
                item.M11Amount = GetAmount(salary, yearPeriods, MonthEnum.Falgun, income, currentAmount, futureAmount, periodId, type, sourceid);
                item.M12Amount = GetAmount(salary, yearPeriods, MonthEnum.Chaitra, income, currentAmount, futureAmount, periodId, type, sourceid);
                item.M1Amount = GetAmount(salary, yearPeriods, MonthEnum.Baisakh, income, currentAmount, futureAmount, periodId, type, sourceid);
                item.M2Amount = GetAmount(salary, yearPeriods, MonthEnum.Jestha, income, currentAmount, futureAmount, periodId, type, sourceid);
                item.M3Amount = GetAmount(salary, yearPeriods, MonthEnum.Ashadh, income, currentAmount, futureAmount, periodId, type, sourceid);

                item.M4AddOn = GetAddOnAmount(addonList, yearPeriods, MonthEnum.Sharwan, income, type, sourceid);
                item.M5AddOn = GetAddOnAmount(addonList, yearPeriods, MonthEnum.Bhadra, income, type, sourceid);
                item.M6AddOn = GetAddOnAmount(addonList, yearPeriods, MonthEnum.Ashwin, income, type, sourceid);
                item.M7AddOn = GetAddOnAmount(addonList, yearPeriods, MonthEnum.Kartik, income, type, sourceid);
                item.M8AddOn = GetAddOnAmount(addonList, yearPeriods, MonthEnum.Mangsir, income, type, sourceid);
                item.M9AddOn = GetAddOnAmount(addonList, yearPeriods, MonthEnum.Poush, income, type, sourceid);
                item.M10AddOn = GetAddOnAmount(addonList, yearPeriods, MonthEnum.Magh, income, type, sourceid);
                item.M11AddOn = GetAddOnAmount(addonList, yearPeriods, MonthEnum.Falgun, income, type, sourceid);
                item.M12AddOn = GetAddOnAmount(addonList, yearPeriods, MonthEnum.Chaitra, income, type, sourceid);
                item.M1AddOn = GetAddOnAmount(addonList, yearPeriods, MonthEnum.Baisakh, income, type, sourceid);
                item.M2AddOn = GetAddOnAmount(addonList, yearPeriods, MonthEnum.Jestha, income, type, sourceid);
                item.M3AddOn = GetAddOnAmount(addonList, yearPeriods, MonthEnum.Ashadh, income, type, sourceid);


                item.Total = item.M1Amount + item.M2Amount + item.M3Amount + item.M4Amount + item.M5Amount + item.M6Amount +
                            item.M7Amount + item.M8Amount + item.M9Amount + item.M10Amount + item.M11Amount + item.M12Amount +
                            item.M1AddOn + item.M2AddOn + item.M3AddOn + item.M4AddOn + item.M5AddOn + item.M6AddOn +
                             item.M7AddOn + item.M8AddOn + item.M9AddOn + item.M10AddOn + item.M11AddOn + item.M12AddOn + 
                             item.OneTime;


                // add for grand total
                total.Total += item.Total;
                total.M1Amount += item.M1Amount;
                total.M2Amount += item.M2Amount;
                total.M3Amount += item.M3Amount;
                total.M4Amount += item.M4Amount;
                total.M5Amount += item.M5Amount;
                total.M6Amount += item.M6Amount;
                total.M7Amount += item.M7Amount;
                total.M8Amount += item.M8Amount;
                total.M9Amount += item.M9Amount;
                total.M10Amount += item.M10Amount;
                total.M11Amount += item.M11Amount;
                total.M12Amount += item.M12Amount;

                total.M1AddOn += item.M1AddOn;
                total.M2AddOn += item.M2AddOn;
                total.M3AddOn += item.M3AddOn;
                total.M4AddOn += item.M4AddOn;
                total.M5AddOn += item.M5AddOn;
                total.M6AddOn += item.M6AddOn;
                total.M7AddOn += item.M7AddOn;
                total.M8AddOn += item.M8AddOn;
                total.M9AddOn += item.M9AddOn;
                total.M10AddOn += item.M10AddOn;
                total.M11AddOn += item.M11AddOn;
                total.M12AddOn += item.M12AddOn;

                total.OneTime += item.OneTime;

                if (item.M1Amount != 0 || item.M2Amount != 0 || item.M3Amount != 0 || item.M4Amount != 0 ||
                    item.M5Amount != 0 || item.M6Amount != 0 || item.M7Amount != 0 || item.M8Amount != 0 ||
                    item.M9Amount != 0 || item.M10Amount != 0 || item.M11Amount != 0 || item.M12Amount != 0 ||                    
                    item.M1AddOn != 0 || item.M2AddOn != 0 || item.M3AddOn != 0 || item.M4AddOn != 0 ||
                    item.M5AddOn != 0 || item.M6AddOn != 0 || item.M7AddOn != 0 || item.M8AddOn != 0 ||
                    item.M9AddOn != 0 || item.M10AddOn != 0 || item.M11AddOn != 0 || item.M12AddOn != 0 ||
                    item.OneTime != 0
                    
                    )
                    items.Add(item);
            }

            // add total 
            items.Add(total);


            
            List<AnnualTaxBO> pfCITDeductionsitems = new List<AnnualTaxBO>();


            // Retirement fund proecssing, cit and deduction pf
            List<PIncome> retirementFundDeductions = new List<PIncome>();
            retirementFundDeductions.Add(new PIncome { IncomeId = (int)CalculationColumnType.DeductionPF, Title = "PF Deduction" });
            retirementFundDeductions.Add(new PIncome { IncomeId = (int)CalculationColumnType.DeductionCIT, Title = "	CIT Deduction" });

            AnnualTaxBO pfCITTotal = new AnnualTaxBO { Name = "a) Sum of CIT And PF " };

            foreach (PIncome income in retirementFundDeductions)
            {

                int type = income.IncomeId;
                int sourceid = income.IncomeId;                


                AnnualTaxBO item = new AnnualTaxBO();
                item.Name = income.Title;

                decimal currentAmount = 0;
                decimal futureAmount = 0;
                decimal futureRem = 0;
                decimal noneCashLikeInsuranceOrAddFromBeginning = 0;

                SetForecast(rows, type, sourceid, ref currentAmount, ref futureAmount, ref futureRem, ref noneCashLikeInsuranceOrAddFromBeginning);

                item.OneTime = noneCashLikeInsuranceOrAddFromBeginning;

                item.M4Amount = GetAmount(salary, yearPeriods, MonthEnum.Sharwan, income, currentAmount, futureAmount, periodId, type, sourceid);
                item.M5Amount = GetAmount(salary, yearPeriods, MonthEnum.Bhadra, income, currentAmount, futureAmount, periodId, type, sourceid);
                item.M6Amount = GetAmount(salary, yearPeriods, MonthEnum.Ashwin, income, currentAmount, futureAmount, periodId, type, sourceid);
                item.M7Amount = GetAmount(salary, yearPeriods, MonthEnum.Kartik, income, currentAmount, futureAmount, periodId, type, sourceid);
                item.M8Amount = GetAmount(salary, yearPeriods, MonthEnum.Mangsir, income, currentAmount, futureAmount, periodId, type, sourceid);
                item.M9Amount = GetAmount(salary, yearPeriods, MonthEnum.Poush, income, currentAmount, futureAmount, periodId, type, sourceid);
                item.M10Amount = GetAmount(salary, yearPeriods, MonthEnum.Magh, income, currentAmount, futureAmount, periodId, type, sourceid);
                item.M11Amount = GetAmount(salary, yearPeriods, MonthEnum.Falgun, income, currentAmount, futureAmount, periodId, type, sourceid);
                item.M12Amount = GetAmount(salary, yearPeriods, MonthEnum.Chaitra, income, currentAmount, futureAmount, periodId, type, sourceid);
                item.M1Amount = GetAmount(salary, yearPeriods, MonthEnum.Baisakh, income, currentAmount, futureAmount, periodId, type, sourceid);
                item.M2Amount = GetAmount(salary, yearPeriods, MonthEnum.Jestha, income, currentAmount, futureAmount, periodId, type, sourceid);
                item.M3Amount = GetAmount(salary, yearPeriods, MonthEnum.Ashadh, income, currentAmount, futureAmount, periodId, type, sourceid);

                item.M4AddOn = GetAddOnAmount(addonList, yearPeriods, MonthEnum.Sharwan, income, type, sourceid);
                item.M5AddOn = GetAddOnAmount(addonList, yearPeriods, MonthEnum.Bhadra, income, type, sourceid);
                item.M6AddOn = GetAddOnAmount(addonList, yearPeriods, MonthEnum.Ashwin, income, type, sourceid);
                item.M7AddOn = GetAddOnAmount(addonList, yearPeriods, MonthEnum.Kartik, income, type, sourceid);
                item.M8AddOn = GetAddOnAmount(addonList, yearPeriods, MonthEnum.Mangsir, income, type, sourceid);
                item.M9AddOn = GetAddOnAmount(addonList, yearPeriods, MonthEnum.Poush, income, type, sourceid);
                item.M10AddOn = GetAddOnAmount(addonList, yearPeriods, MonthEnum.Magh, income, type, sourceid);
                item.M11AddOn = GetAddOnAmount(addonList, yearPeriods, MonthEnum.Falgun, income, type, sourceid);
                item.M12AddOn = GetAddOnAmount(addonList, yearPeriods, MonthEnum.Chaitra, income, type, sourceid);
                item.M1AddOn = GetAddOnAmount(addonList, yearPeriods, MonthEnum.Baisakh, income, type, sourceid);
                item.M2AddOn = GetAddOnAmount(addonList, yearPeriods, MonthEnum.Jestha, income, type, sourceid);
                item.M3AddOn = GetAddOnAmount(addonList, yearPeriods, MonthEnum.Ashadh, income, type, sourceid);


                item.Total = item.M1Amount + item.M2Amount + item.M3Amount + item.M4Amount + item.M5Amount + item.M6Amount +
                            item.M7Amount + item.M8Amount + item.M9Amount + item.M10Amount + item.M11Amount + item.M12Amount +
                            item.M1AddOn + item.M2AddOn + item.M3AddOn + item.M4AddOn + item.M5AddOn + item.M6AddOn +
                             item.M7AddOn + item.M8AddOn + item.M9AddOn + item.M10AddOn + item.M11AddOn + item.M12AddOn +
                             item.OneTime;


                // add for grand total
                pfCITTotal.Total += item.Total;
                pfCITTotal.M1Amount += item.M1Amount;
                pfCITTotal.M2Amount += item.M2Amount;
                pfCITTotal.M3Amount += item.M3Amount;
                pfCITTotal.M4Amount += item.M4Amount;
                pfCITTotal.M5Amount += item.M5Amount;
                pfCITTotal.M6Amount += item.M6Amount;
                pfCITTotal.M7Amount += item.M7Amount;
                pfCITTotal.M8Amount += item.M8Amount;
                pfCITTotal.M9Amount += item.M9Amount;
                pfCITTotal.M10Amount += item.M10Amount;
                pfCITTotal.M11Amount += item.M11Amount;
                pfCITTotal.M12Amount += item.M12Amount;

                pfCITTotal.M1AddOn += item.M1AddOn;
                pfCITTotal.M2AddOn += item.M2AddOn;
                pfCITTotal.M3AddOn += item.M3AddOn;
                pfCITTotal.M4AddOn += item.M4AddOn;
                pfCITTotal.M5AddOn += item.M5AddOn;
                pfCITTotal.M6AddOn += item.M6AddOn;
                pfCITTotal.M7AddOn += item.M7AddOn;
                pfCITTotal.M8AddOn += item.M8AddOn;
                pfCITTotal.M9AddOn += item.M9AddOn;
                pfCITTotal.M10AddOn += item.M10AddOn;
                pfCITTotal.M11AddOn += item.M11AddOn;
                pfCITTotal.M12AddOn += item.M12AddOn;

                pfCITTotal.OneTime += item.OneTime;

                if (item.M1Amount != 0 || item.M2Amount != 0 || item.M3Amount != 0 || item.M4Amount != 0 ||
                    item.M5Amount != 0 || item.M6Amount != 0 || item.M7Amount != 0 || item.M8Amount != 0 ||
                    item.M9Amount != 0 || item.M10Amount != 0 || item.M11Amount != 0 || item.M12Amount != 0 ||
                    item.M1AddOn != 0 || item.M2AddOn != 0 || item.M3AddOn != 0 || item.M4AddOn != 0 ||
                    item.M5AddOn != 0 || item.M6AddOn != 0 || item.M7AddOn != 0 || item.M8AddOn != 0 ||
                    item.M9AddOn != 0 || item.M10AddOn != 0 || item.M11AddOn != 0 || item.M12AddOn != 0 ||
                    item.OneTime != 0

                    )
                    pfCITDeductionsitems.Add(item);
            }



            /* 
              calculate 
               a) Sum of CIT And PF 
                b) Limit 
                c) 1/3rd of Taxable Income  
                Min of a, b and c 
            */
            //pfCITDeductionsitems.Add(pfCITTotal);

            // now append below values after PF and CIT deductions rows
            bool isAfterPFAndCIT = false;

            foreach (string strRow in rows)
            {
                string[] values = strRow.Split(new char[] { ':' });

                if (values.Length <= 1)
                    continue;

                int dtype = 0, dsourceid = 0;

                // if has more than 5 then only the line is valid as other has not Type/SourceId info
                if (values.Length >= 5)
                {

                    dtype = int.Parse(values[4]);
                    dsourceid = int.Parse(values[5]);

                    if (dtype == (int)CalculationColumnType.DeductionCIT)
                    {
                        isAfterPFAndCIT = true;
                        continue;
                    }

                    
                }


                if (isAfterPFAndCIT)
                {
                    string val = values[1];
                    decimal amount = 0;
                    decimal.TryParse(val, out amount);
                    pfCITDeductionsitems.Add(new AnnualTaxBO { Name = values[0], Total = amount });
                }
            }


            items.AddRange(pfCITDeductionsitems);

            gvEmployeeIncome.DataSource = items;
            gvEmployeeIncome.DataBind();

        }

        private static void SetForecast(string[] rows, int type, int sourceid, ref decimal currentAmount, ref decimal futureAmount
            , ref decimal futureRem, ref decimal noneCashLikeInsuranceOrAddFromBeginning)
        {
            foreach (string strRow in rows)
            {
                string[] values = strRow.Split(new char[] { ':' });

                if (values.Length <= 1)
                    continue;

                int dtype = 0, dsourceid = 0;
             

                // if has more than 5 then only the line is valid as other has not Type/SourceId info
                if (values.Length >= 5)
                {

                    dtype = int.Parse(values[4]);
                    dsourceid = int.Parse(values[5]);

                    // company insurance
                    if (dtype == (int)CalculationColumnType.Insurance && dsourceid == (int)CalculationColumnType.Insurance
                        && dtype == type && dsourceid == sourceid)
                    {
                        decimal.TryParse(values[1], out noneCashLikeInsuranceOrAddFromBeginning);
                    }
                    else if (dtype == type && dsourceid == sourceid)
                    {

                        string forecast = values[2];

                        if (forecast.Contains("("))// && forecast.Contains(","))
                        {

                            string[] amounts = forecast.Split(new char[] { '+' }, StringSplitOptions.RemoveEmptyEntries);

                            if (amounts.Length >= 2)
                            {


                                string current = amounts[1].Trim();
                                decimal.TryParse(current, out currentAmount);

                                //if has futur section only
                                if (amounts.Length >= 3)
                                {
                                    string future = amounts[2].Trim().Replace("(", "").Replace(")", "");

                                    string[] futureSplited = future.Split(new char[] { '*' });

                                    if (futureSplited.Length >= 1)
                                    {
                                        decimal.TryParse(futureSplited[0], out futureAmount);
                                        decimal.TryParse(futureSplited[1], out futureRem);
                                    }
                                    else
                                    {
                                        futureAmount = 0;
                                        futureRem = 0;
                                    }
                                }
                                else
                                {
                                    futureAmount = 0;
                                    futureRem = 0;
                                }
                            }
                        }
                    }
                }
            }
        }

        public new string GetCurrency(object value)
        {
            if (value == null)
                return "-";

            decimal amount = Convert.ToDecimal(value.ToString());

            if (amount == 0)
                return "-";

            return base.GetCurrency(amount);
        }

        public decimal GetAmount(List<GetSalaryListResult> salary, List<PayrollPeriod> periods
            , MonthEnum month, PIncome income, decimal currentAmount, decimal futureAmount, int currentPeriodId,int type,int sourceid)
        {

            PayrollPeriod period = null;

           

            switch (month)
            {
                case MonthEnum.Sharwan:
                    period = periods.FirstOrDefault(x => x.Month == (int)MonthEnum.Sharwan);
                    break;
                case MonthEnum.Bhadra:
                    period = periods.FirstOrDefault(x => x.Month == (int)MonthEnum.Bhadra);
                    break;
                case MonthEnum.Ashwin:
                    period = periods.FirstOrDefault(x => x.Month == (int)MonthEnum.Ashwin);
                    break;
                case MonthEnum.Kartik:
                    period = periods.FirstOrDefault(x => x.Month == (int)MonthEnum.Kartik);
                    break;
                case MonthEnum.Mangsir:
                    period = periods.FirstOrDefault(x => x.Month == (int)MonthEnum.Mangsir);
                    break;
                case MonthEnum.Poush:
                    period = periods.FirstOrDefault(x => x.Month == (int)MonthEnum.Poush);
                    break;
                case MonthEnum.Magh:
                    period = periods.FirstOrDefault(x => x.Month == (int)MonthEnum.Magh);
                    break;
                case MonthEnum.Falgun:
                    period = periods.FirstOrDefault(x => x.Month == (int)MonthEnum.Falgun);
                    break;
                case MonthEnum.Chaitra:
                    period = periods.FirstOrDefault(x => x.Month == (int)MonthEnum.Chaitra);
                    break;
                case MonthEnum.Baisakh:
                    period = periods.FirstOrDefault(x => x.Month == (int)MonthEnum.Baisakh);
                    break;
                case MonthEnum.Jestha:
                    period = periods.FirstOrDefault(x => x.Month == (int)MonthEnum.Jestha);
                    break;
                case MonthEnum.Ashadh:
                    period = periods.FirstOrDefault(x => x.Month == (int)MonthEnum.Ashadh);
                    break;
            }

            if (period != null)
            {
                GetSalaryListResult item = salary.FirstOrDefault(x => x.Type == type && x.SourceId == sourceid
                    && x.PayrollPeriodId == period.PayrollPeriodId);
                if (item == null)
                {
                    if (period.PayrollPeriodId == currentPeriodId)
                        return currentAmount;
                }
                else
                    return item.Amount;
            }

            return futureAmount;
        }

        public decimal GetAddOnAmount(List<GetAddOnListResult> salary, List<PayrollPeriod> periods
            , MonthEnum month, PIncome income,int type,int sourceid)
        {

            PayrollPeriod period = null;

            //int type = 1;
            //int sourceid = income.IncomeId;
            //if (income.Calculation == IncomeCalculation.DEEMED_INCOME)
            //{
            //    type = 25;
            //    sourceid = income.IncomeId;
            //}
            //else if (income.IncomeId == 0)
            //{
            //    type = (int)CalculationColumnType.IncomePF;
            //    sourceid = (int)CalculationColumnType.IncomePF;
            //}
            //else if (income.IncomeId == -1)
            //{
            //    type = (int)CalculationColumnType.IncomeLeaveEncasement;
            //    sourceid = (int)CalculationColumnType.IncomeLeaveEncasement;
            //}

            switch (month)
            {
                case MonthEnum.Sharwan:
                    period = periods.FirstOrDefault(x => x.Month == (int)MonthEnum.Sharwan);
                    break;
                case MonthEnum.Bhadra:
                    period = periods.FirstOrDefault(x => x.Month == (int)MonthEnum.Bhadra);
                    break;
                case MonthEnum.Ashwin:
                    period = periods.FirstOrDefault(x => x.Month == (int)MonthEnum.Ashwin);
                    break;
                case MonthEnum.Kartik:
                    period = periods.FirstOrDefault(x => x.Month == (int)MonthEnum.Kartik);
                    break;
                case MonthEnum.Mangsir:
                    period = periods.FirstOrDefault(x => x.Month == (int)MonthEnum.Mangsir);
                    break;
                case MonthEnum.Poush:
                    period = periods.FirstOrDefault(x => x.Month == (int)MonthEnum.Poush);
                    break;
                case MonthEnum.Magh:
                    period = periods.FirstOrDefault(x => x.Month == (int)MonthEnum.Magh);
                    break;
                case MonthEnum.Falgun:
                    period = periods.FirstOrDefault(x => x.Month == (int)MonthEnum.Falgun);
                    break;
                case MonthEnum.Chaitra:
                    period = periods.FirstOrDefault(x => x.Month == (int)MonthEnum.Chaitra);
                    break;
                case MonthEnum.Baisakh:
                    period = periods.FirstOrDefault(x => x.Month == (int)MonthEnum.Baisakh);
                    break;
                case MonthEnum.Jestha:
                    period = periods.FirstOrDefault(x => x.Month == (int)MonthEnum.Jestha);
                    break;
                case MonthEnum.Ashadh:
                    period = periods.FirstOrDefault(x => x.Month == (int)MonthEnum.Ashadh);
                    break;
            }

            if (period != null)
            {
                GetAddOnListResult item = salary.FirstOrDefault(x => x.Type == type && x.SourceId == sourceid
                    && x.PayrollPeriodId == period.PayrollPeriodId);
                if (item == null)
                {
                    return 0;
                }
                else
                    return item.Amount;
            }

            return 0;
        }

        public enum MonthEnum
        {
            Sharwan = 4,
            Bhadra = 5,
            Ashwin = 6,
            Kartik = 7,
            Mangsir = 8,
            Poush = 9,
            Magh = 10,
            Falgun = 11,
            Chaitra = 12,
            Baisakh = 1,
            Jestha = 2,
            Ashadh = 3
        }

        
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
           
                GridViewExportUtil.Export("Month Wise Yearly Tax Details.xls", gvEmployeeIncome);
          
        }
     
    }

  

}
