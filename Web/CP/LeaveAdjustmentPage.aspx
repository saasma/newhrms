<%@ Page Title="Monthly Leave Adjustment" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="LeaveAdjustmentPage.aspx.cs" Inherits="Web.CP.LeaveAdjustmentPage" %>

<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="Paging" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="../Scripts/jquery-1.3.2.min.js"></script>
    <script type="text/javascript">


        function validateDifferenceFunc(source, args) {

            if (!validateDifference(document.getElementById(source.controltovalidate))) {
                args.IsValid = false;
                return;
            }
            else
                args.IsValid = true;
        }

        function checkIsManualLeave() {
            if (typeof (isManualLeave) == 'undefined')
                return false;

            return isManualLeave;
        }

        function validateDifference(txt) {
            //adjustment can be any difference

            //        if (checkIsManualLeave())
            //            return true;


            //        var isEmployeeRetiring = txt.getAttribute("IsEmployeeRetiring");

            //        if (isEmployeeRetiring == "true")
            //            return true;

            //        var currentValue = getNumber(txt.value);       

            //        var closing = getNumber(txt.getAttribute("closing"));
            //        
            //        if (Math.abs(currentValue - closing) > 1) {
            //            
            //            alert(diffMsg);
            //            txt.focus();
            //            //stopPropagation();
            //            return false;
            //        }


            return true;
        }

        function refreshWindow() {
            __doPostBack('Refresh', 0);

        }
        function isPayrollSelected(btn) {



            if ($('#' + btn.id).prop('disabled'))
                return false;

            var payrollPeriodId = document.getElementById('<%= ddlPayrollPeriods.ClientID %>').value;
            var leaveTypeId = document.getElementById('<%=  ddlLeaves.ClientID %>').value;

            if (payrollPeriodId == -1) {
                alert("Payroll period must be selected.");
                return false;
            }
            if (leaveTypeId == -1) {
                alert("Leave type must be selected.");
                return false;
            }

            var ret = shiftPopup("PayrollPeriodId=" + payrollPeriodId + "&LeaveTypeId=" + leaveTypeId);
            //            if (typeof (ret) != 'undefined') {
            //                if (ret == 'Reload') {
            //                    //refreshIncomeList(0);


            //                }
            //            }

            return false;
        }

        $(document).ready(
        function () {
            addArrowMovementToGrid('<%= gvwLeaveAdjustmentList.ClientID %>', true);
        }
);
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
<ext:ResourceManager runat="server" ScriptMode="Release" DisableViewState="false" />
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Monthly Leave Adjustment
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel" >
    

    <div class="contentArea" style="margin-top:-10px;">

        <asp:Panel runat="server" ID="pnl" class="attribute" style="padding:10px" DefaultButton="btnLoad">
            <table>
                <tr>
                    <td>
                        <strong>Leave </strong>
                        <asp:DropDownList AppendDataBoundItems="true" Width="180px" ID="ddlLeaves" runat="server"
                            DataTextField="Title" DataValueField="LeaveTypeId">
                            <asp:ListItem Text="--Select Leave--" Value="-1"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlLeaves"
                            InitialValue="-1" Display="None" ErrorMessage="Please select leave." ValidationGroup="Load"></asp:RequiredFieldValidator>
                        &nbsp;&nbsp;
                    </td>
                    <td>
                        <strong>Payroll Period</strong>
                        <asp:DropDownList AppendDataBoundItems="true" ID="ddlPayrollPeriods" runat="server"
                            DataTextField="Name" DataValueField="PayrollPeriodId">
                            <asp:ListItem Text="--Select Payroll Period--" Value="-1"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlPayrollPeriods"
                            InitialValue="-1" Display="None" ErrorMessage="Please select payroll period."
                            ValidationGroup="Load"></asp:RequiredFieldValidator>
                    </td>
                    <td class="style1" width="200px" style="padding-left: 10px">
                        <asp:TextBox Width="160px" ID="txtEmpSearchText" runat="server"></asp:TextBox>
                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtEmpSearchText"
                            WatermarkText="Employee Name" WatermarkCssClass="searchBoxText" />
                        <cc1:AutoCompleteExtender EnableCaching="true" ID="AutoCompleteExtenderOrganization"
                            runat="server" MinimumPrefixLength="2" ServiceMethod="GetEmployeeNames" ServicePath="~/PayrollService.asmx"
                            TargetControlID="txtEmpSearchText" CompletionSetCount="10" CompletionInterval="250"
                            CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList"
                            CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
                        </cc1:AutoCompleteExtender>
                    </td>
                    <td>
                        <asp:Button ID="btnLoad" CssClass="load" runat="server" OnClientClick="valGroup='Load';return CheckValidation()"
                            Text="Load" OnClick="btnLoad_Click"  />
                    </td>
                    <td>
                        <asp:LinkButton ID="btnExport" runat="server" Text="Excel import" OnClientClick="return isPayrollSelected(this)"
                            CssClass=" excel marginRight tiptip" Style="float: left;" />
                    </td>
                </tr>
            </table>
            <div style="color:#CE7A14;padding-top:10px">
                Note : Attendance register should be saved for the selected month for adjustment. If attendance register is deleted then adjustment will be auto deleted.
            </div>
        </asp:Panel>
        <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
        <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
        <div class="clear" style="margin-top: 10px">
            <cc2:EmptyDisplayGridView CssClass="tableLightColor" UseAccessibleHeader="true" GridLines="None"
                DataKeyNames="EmployeeId,LeaveTypeId,PayrollPeriodId,IsUsedInCalculation" ID="gvwLeaveAdjustmentList"
                runat="server" ShowFooterWhenEmpty="False" ShowHeaderWhenEmpty="True" AutoGenerateColumns="False">
                <Columns>
                    <asp:BoundField HeaderStyle-Width="30px" DataField="EmployeeId" ItemStyle-HorizontalAlign="Right"
                        HeaderText="&nbsp;&nbsp;&nbsp;EIN">
                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField HeaderStyle-Width="200px" DataField="Name" HeaderStyle-HorizontalAlign="Left"
                        ItemStyle-HorizontalAlign="Left" HeaderText="Name"></asp:BoundField>
                    <%--   <asp:BoundField HeaderStyle-Width="80px" DataFormatString="{0:0.###}" DataField="BeginningBalance"
                        HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" HeaderText="Balance">
                    </asp:BoundField>--%>
                    <asp:TemplateField HeaderStyle-Width="80px" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"
                        HeaderText="Balance">
                        <ItemTemplate>
                            <%# GetFormattedValue(Eval("BeginningBalance"))%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <%-- <asp:BoundField HeaderStyle-Width="80px" DataFormatString="{0:0.###}" DataField="Accured"
                        HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" HeaderText="Accured">
                    </asp:BoundField>--%>
                    <asp:TemplateField HeaderStyle-Width="80px" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"
                        HeaderText="Accured">
                        <ItemTemplate>
                            <%# GetFormattedValue(Eval("Accured"))%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <%-- <asp:BoundField HeaderStyle-Width="80px" DataFormatString="{0:0.###}" DataField="Taken"
                        HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" HeaderText="Taken">
                    </asp:BoundField>--%>
                    <asp:TemplateField HeaderStyle-Width="80px" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"
                        HeaderText="Taken">
                        <ItemTemplate>
                            <%# GetFormattedValue(Eval("Taken"))%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="115px" HeaderStyle-HorizontalAlign="Right"
                        ItemStyle-HorizontalAlign="Right" HeaderText="Adjustment&nbsp;">
                        <ItemTemplate>
                            <asp:TextBox IsEmployeeRetiring='<%# Eval("IsRetiring").ToString().ToLower() %>'
                                data-row='<%# Container.DataItemIndex %>' data-col='0' closing='<%# Eval("Adjusted") %>'
                                Style='text-align: right; width: 70px' ID="txtAdjustment" Enabled='<%# isPayrollEditable || Convert.ToBoolean(Eval("IsEditable")) %>'
                                Text='<%# GetFormattedValue( Eval("Adjusted") ) %>' runat="server" />
                            <asp:RequiredFieldValidator ID="rfTxtAmount1" ValidationGroup="Balance" runat="server"
                                ControlToValidate="txtAdjustment" Display="None" ErrorMessage="Invalid balance." />
                            <asp:CompareValidator ID="reTxtAmount2" runat="server" ValidationGroup="Balance"
                                ControlToValidate="txtAdjustment" Display="None" ErrorMessage="Adjustment must be greater than or equal to zero."
                                Type="Double" Operator="DataTypeCheck" ValueToCompare="0" />
                            <asp:CustomValidator ID="valCust" runat="server" ValidationGroup="Balance" ControlToValidate="txtAdjustment"
                                Display="None" ClientValidationFunction="validateDifferenceFunc" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="80px" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"
                        HeaderText="Lapsed">
                        <ItemTemplate>
                            <%# GetFormattedValue(Eval("Lapsed"))%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="80px" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"
                        HeaderText="Encashed">
                        <ItemTemplate>
                            <%# GetFormattedValue(Eval("Encased"))%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <%-- <asp:BoundField DataField="Encased" HeaderText="Encased" />--%>
                    <asp:BoundField HeaderStyle-Width="90px" DataFormatString="{0:0.###}" HeaderStyle-HorizontalAlign="Right"
                        ItemStyle-HorizontalAlign="Right" DataField="NewBalance" HeaderText="Closing">
                    </asp:BoundField>
                    <%--<asp:BoundField HeaderStyle-Width="90px" DataFormatString="{0:0.###}" HeaderStyle-HorizontalAlign="Right"
                        ItemStyle-HorizontalAlign="Right" DataField="Adjusted" HeaderText="Adjustment">
                    </asp:BoundField>--%>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Note" HeaderStyle-Width="300px">
                        <ItemTemplate>
                            <asp:TextBox data-col='4' data-row='<%# Container.DataItemIndex %>' Style='width: 300px;
                                text-align: left;' CssClass='calculationInput' ID="txtNote" Text='<%#  Eval("Notes") %>'
                                runat="server"></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                <EmptyDataTemplate>
                    <b>Leave not used in the current payroll period.</b>
                </EmptyDataTemplate>
            </cc2:EmptyDisplayGridView>
            <uc1:Paging ID="pagingCtl" OnNextRecord="btnNext_Click" OnPrevRecord="btnPrevious_Click"
                OnDropDownShowRecordsChanged="ddlRecords_SelectedIndexChanged" runat="server" />
            <%--            <cc2:EmptyDisplayGridView CssClass="tableLightColor" UseAccessibleHeader="true" ID="gvwLeaveFixedList"
                runat="server" ShowFooterWhenEmpty="False" ShowHeaderWhenEmpty="True" Width="100%"
                AutoGenerateColumns="False" GridLines="None">
                <Columns>
                    <asp:BoundField DataField="EmployeeId" HeaderText="Employee" />
                    <asp:BoundField DataField="Name" HeaderText="Name" />
                    <asp:BoundField DataField="Total" HeaderText="Taken" />
                </Columns>
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                <EmptyDataTemplate>
                    <b>Leave not used in the current payroll period.</b>
                </EmptyDataTemplate>
            </cc2:EmptyDisplayGridView>
            <uc1:Paging ID="pagingCtl1" OnNextRecord="btnNext1_Click" OnPrevRecord="btnPrevious1_Click"
                OnDropDownShowRecordsChanged="ddlRecords1_SelectedIndexChanged" runat="server" />--%>
            <div class="buttonsDiv">
                <asp:Button UseSubmitBehavior="true" CssClass="save" OnClientClick="if(confirm('Are you sure, you want to save the adjustment, if there is encashment and new adjusted balance is 0 then encashment will be 0?')) { valGroup='Balance';return CheckValidation(); } else return false;"
                    ID="btnSave" Visible="false" runat="server" Text="Save" OnClick="btnSave_Click" />
            </div>
        </div>
    </div>
    </div>
</asp:Content>
