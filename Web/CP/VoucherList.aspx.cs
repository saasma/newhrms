﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Manager;
using BLL.Base;
using BLL;

namespace Web.CP
{
    public partial class VoucherList : BasePage
    {

        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.Payroll;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (CommonManager.Setting.AccountingVoucherEnum != null && CommonManager.Setting.AccountingVoucherEnum == (int)AccountingVoucherTypeEnum.GLOBUS)
                {
                    voucherCtlNew.Visible = false;
                    voucherCtlOld.Visible = false;
                    VoucherListNewGlobus.Visible = true;
                }

                else if (CommonManager.CompanySetting.WhichCompany == BLL.WhichCompany.NIBL
                    || CommonManager.CompanySetting.WhichCompany==WhichCompany.Janata
                    || CommonManager.CompanySetting.WhichCompany == WhichCompany.NCC)
                {
                    voucherCtlNew.Visible = true;
                    voucherCtlOld.Visible = false;
                }

            }
        }
    }
}