﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Reflection;
using Ext.Net;
using DAL;
using BLL.Manager;
using Utils.Helper;
using BLL;
using Utils.Calendar;

namespace Web.CP
{
    public partial class LateEntrySendMail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
         
        }

        protected void btn_SendMail(object sender, DirectEventArgs e)
        {
            List<AttendanceAllEmployeeReportResult> ListLateEmployes = AttendanceManager.GetAttendanceDailyReport(CommonManager.GetCurrentDateAndTime().Date, 11, "")
                .Where(x=>x.InState==2||x.InState==3).ToList();

            if (ListLateEmployes.Count == 0)
            {
                NewMessage.ShowNormalMessage("Late In employee not found.");
                return;
            }
 
            SendMail(ListLateEmployes);

        }


        protected void SendMail(List<AttendanceAllEmployeeReportResult> ListLateEmployes)
        {

            SMTPHelper mailSender = new SMTPHelper();
            EmailContent dbMailContent = CommonManager.GetMailContent((int)EmailContentType.LateIn);

            if (dbMailContent == null)
            {
                NewMessage.ShowNormalMessage("Template not defined.");
                return;
            }

            string todayDate = CommonManager.GetCurrentDateAndTime().ToString("yyyy/MMM/d");
            string subject = dbMailContent.Subject.Replace("#TodayDate#", todayDate);
            int count = 0;



            foreach (AttendanceAllEmployeeReportResult _item in ListLateEmployes)
            {
                string body = dbMailContent.Body.Replace("#LateHour#", _item.InRemarksText.Replace("Late In","").Trim());
                body = body.Replace("#TodayDate#",todayDate);

                EEmployee _Employeee = EmployeeManager.GetEmployeeById(_item.EmployeeId.Value);

                body = body.Replace("#Employee#", _item.EmplooyeeName);


                if (string.IsNullOrEmpty(_Employeee.EAddresses[0].CIEmail))
                {
                    NewMessage.ShowNormalMessage("Email address not found for Employee " + _Employeee.Name);
                    return;
                }

                if(!string.IsNullOrEmpty(_Employeee.EAddresses[0].CIEmail))
                {
                bool isSendSuccess = SMTPHelper.SendAsyncMail(_Employeee.EAddresses[0].CIEmail, body, subject, "");
                if (isSendSuccess)
                       count++;
                }

            }

            if (count > 0)
            {
                NewMessage.ShowNormalMessage(string.Format(Resources.Messages.SentMailSuccessMsg, count));
                return;
            }
            else
            {
                NewMessage.ShowWarningMessage("error while sending email");
                return;
            }


        }


    }
}
