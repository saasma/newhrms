<%@ Page Title="Income Change History" Language="C#" MasterPageFile="~/Master/ForPopupPage.Master" AutoEventWireup="true"
    CodeBehind="IncomeIncrementHistory.aspx.cs" Inherits="Web.CP.IncomeIncrementHistory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">
        function closePopup() {
            window.returnValue = "Reload";
            window.close();
        }

        function closePopupFromPay() {
            window.returnValue = "ReloadIncome";
            window.close();
        }         
        
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <div class="popupHeader">
        <h3>
            Income change history</h3>
    </div>
    <div class=" marginal">
        <%--<asp:GridView ID="gvw" runat="server" AutoGenerateColumns="False" CellPadding="4"
            ForeColor="#333333" GridLines="None">
            <RowStyle BackColor="#EFF3FB" />
            <Columns>
                <asp:BoundField DataField="EffectiveFromDate" HeaderText="Effective From" />
                <asp:TemplateField HeaderText="Amount">
                    <ItemTemplate>
                        <%# GetCurrency(Eval("AmountOld")) %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Rate">
                    <ItemTemplate>
                        <%# GetRate(Eval("RatePercentOld"))%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Market value">
                    <ItemTemplate>
                        <%# GetCurrency(Eval("MarketValueOld"))%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Is Current">
                    <ItemTemplate>
                        <asp:CheckBox runat="server" Enabled="false" Checked='<%# IsCurrent(Eval("Id")) %>' />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <EditRowStyle BackColor="#2461BF" />
            <AlternatingRowStyle BackColor="White" />
            <EmptyDataTemplate>
                <b>No increments. </b>
            </EmptyDataTemplate>
        </asp:GridView>--%>
        
        <asp:GridView ID="gvw"  PagerStyle-HorizontalAlign="Center" PagerStyle-CssClass="defaultPagingBar"  AllowPaging="true" runat="server" AutoGenerateColumns="False" CellPadding="4"
            GridLines="None" PageSize="10" 
            onpageindexchanging="gvw_PageIndexChanging">
           
            <Columns>
                <asp:BoundField DataField="FromDate" HeaderStyle-HorizontalAlign="Left" HeaderText="From" />
                  <asp:BoundField DataField="ToDate" HeaderStyle-HorizontalAlign="Left"  HeaderText="Upto" />
                <asp:TemplateField HeaderText="Amount/Rate" ItemStyle-HorizontalAlign="Right" >
                    <ItemTemplate>
                        <%# GetAmountOrRate(Eval("Amount"), Eval("Rate"))%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Difference" ItemStyle-HorizontalAlign="Right" >
                    <ItemTemplate>
                        <%# GetDifference(Eval("Difference"), Eval("Amount"))%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Is Retrospect" ItemStyle-HorizontalAlign="Center" >
                    <ItemTemplate>
                        <%# Utils.Web.UIHelper.GetYesNoImageTag(Eval("IsReterospect"), Page)%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="PF" ItemStyle-HorizontalAlign="Center" >
                    <ItemTemplate>
                        <%# Utils.Web.UIHelper.GetYesNoImageTag(Eval("HasPF"),Page)%>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField HeaderText="Leave encash" ItemStyle-HorizontalAlign="Center" >
                    <ItemTemplate>
                        <%# Utils.Web.UIHelper.GetYesNoImageTag(Eval("IsLeaveEncash"), Page)%>
                    </ItemTemplate>
                </asp:TemplateField>
               
                 <asp:TemplateField HeaderText="No Leave Effect" ItemStyle-HorizontalAlign="Center" >
                    <ItemTemplate>
                        <%# Utils.Web.UIHelper.GetYesNoImageTag(Eval("HasLeaveEffect"), Page)%>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField HeaderText="Income Tax" ItemStyle-HorizontalAlign="Center" >
                    <ItemTemplate>
                        <%# Utils.Web.UIHelper.GetYesNoImageTag(Eval("HasIncomeTax"),Page)%>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
                                        <RowStyle CssClass="odd" />
            <AlternatingRowStyle CssClass="even" />
            <SelectedRowStyle CssClass="selected" />
            <PagerStyle HorizontalAlign="Center" CssClass="defaultPagingBar"></PagerStyle>
            <EmptyDataTemplate>
                <b>No change history. </b>
            </EmptyDataTemplate>
        </asp:GridView>
    </div>
</asp:Content>
