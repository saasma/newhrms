﻿<%@ Page Title="Manage Voucher" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="ManageVoucherGroup.aspx.cs" Inherits="Web.CP.ManageVoucherGroup" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function medicalTaxCreditPopupCall(medicalTaxId) {
            var ret = medicalTaxCreditPopup("ID=" + medicalTaxId);
            if (ret == "Yes")
                __doPostBack('Reload', '');
        }


        function voucherPoupCall(type, sourceId) {
            var ret = voucherHead("type=" + type + "&sourceId=" + sourceId);
            if (ret == "Yes")
                __doPostBack('Reload', '');
        }
        function parentReloadCallbackFunction(childWindow, ret) {
            childWindow.close();
            if (ret == "Yes")
                __doPostBack('Reload', '');

        }

        function refreshWindow() {
            __doPostBack('Reload', '');
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Account Groups
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div>
            <asp:LinkButton runat="server" Visible="false"></asp:LinkButton>
              <div class="separator clear" runat="server" visible="false" id="divCategory">
                <h3 style="margin-top: 10px!important;">
                Group Category</h3>
                <cc2:EmptyDisplayGridView CssClass="table table-primary mb30 table-bordered table-hover" UseAccessibleHeader="true" ID="gridCategory"
                    runat="server" AutoGenerateColumns="False" DataKeyNames="CategoryID" GridLines="None"
                    ShowFooterWhenEmpty="False" ShowHeaderWhenEmpty="True" Width="100%">
                    <Columns>
                        <asp:BoundField HeaderStyle-HorizontalAlign="Left" DataField="Name" HeaderText="Name"
                            HeaderStyle-Width="140px" />
                       
                        <%--<asp:TemplateField HeaderStyle-Width="50px" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:ImageButton ID="ImageButton1" ImageUrl="~/images/edit.gif" OnClientClick='<%# "medicalTaxCreditPopupCall(" + Eval("VoucherGroupID") + ");return false;" %>'
                                    runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                    </Columns>
                   
                    <RowStyle CssClass="odd" />
                    <AlternatingRowStyle CssClass="even" />
                    <EmptyDataTemplate>
                        No records.
                    </EmptyDataTemplate>
                </cc2:EmptyDisplayGridView>
            </div>
            <div class="separator clear">
                <h3 style="margin-top: 10px!important;">
                Groups</h3>
                <cc2:EmptyDisplayGridView CssClass="table table-primary mb30 table-bordered table-hover" UseAccessibleHeader="true" ID="gvwList"
                    runat="server" AutoGenerateColumns="False" DataKeyNames="VoucherGroupID" GridLines="None"
                    ShowFooterWhenEmpty="False" ShowHeaderWhenEmpty="True" Width="100%">
                    <Columns>
                        <asp:BoundField HeaderStyle-HorizontalAlign="Left" DataField="Code" HeaderText="Group Code"
                            HeaderStyle-Width="40px" />
                        <asp:BoundField HeaderStyle-Width="200px" HeaderStyle-HorizontalAlign="Left" DataField="AccountGroupName"
                            HeaderText="Group Name" />
                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" Visible="false" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-Width="80px" HeaderText="Category">
                            <ItemTemplate>
                                <%# GetVoucherCategory(Eval("CategoryRef_ID"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-Width="80px" HeaderText="Type">
                            <ItemTemplate>
                                <%# GetVoucherType(Eval("Type"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-Width="80px" HeaderText="Group">
                            <ItemTemplate>
                                <%# GetVoucherGroup(Eval("Group"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:ImageButton ID="ImageButton1" ImageUrl="~/images/edit.gif" OnClientClick='<%# "medicalTaxCreditPopupCall(" + Eval("VoucherGroupID") + ");return false;" %>'
                                    runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <%--                <PagerSettings Mode="NumericFirstLast" NextPageText="Next »" PreviousPageText="« Prev"
                    FirstPageText="First" LastPageText="Last" />--%>
                    <RowStyle CssClass="odd" />
                    <AlternatingRowStyle CssClass="even" />
                    <EmptyDataTemplate>
                        No records.
                    </EmptyDataTemplate>
                </cc2:EmptyDisplayGridView>
            </div>
            <div class="buttonsDivSection">
                <asp:Button ID="btnSave" CssClass="btn btn-primary btn-sm btn-sect" runat="server" Text="Add New Group" OnClientClick="medicalTaxCreditPopupCall(0);return false;" />
            </div>
            <div style="clear: both;">
            </div>
            <h3 style="margin-top: 10px!important;">
                Income/Deduction Association</h3>
            <div class="separator clear">
                <cc2:EmptyDisplayGridView CssClass="table table-primary mb30 table-bordered table-hover" UseAccessibleHeader="true" ID="gridIncomeDeductionList"
                    runat="server" AutoGenerateColumns="False" DataKeyNames="ID" GridLines="None"
                    ShowFooterWhenEmpty="False" ShowHeaderWhenEmpty="True" Width="100%">
                    <Columns>
                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-Width="50px" HeaderText="SN">
                            <ItemTemplate>
                                <%# Eval("SN")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-Width="80px" HeaderText="Income and Deductions">
                            <ItemTemplate>
                                <%# GetHeaderName(Eval("Type"),Eval("SourceId"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-Width="120px" HeaderText="Calculation">
                            <ItemTemplate>
                                <%# Eval("CalculationText")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-Width="80px" HeaderText="Type">
                            <ItemTemplate>
                                <%# GetType((int)Eval("Type"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-Width="80px" HeaderText="Group">
                            <ItemTemplate>
                                <%# GetVoucherGroupName(Eval("VoucherGroupID"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:ImageButton ID="ImageButton1" ImageUrl="~/images/edit.gif" OnClientClick='<%# "voucherPoupCall(" + Eval("Type") + "," + Eval("SourceId") + ");return false;" %>'
                                    runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <%--                <PagerSettings Mode="NumericFirstLast" NextPageText="Next »" PreviousPageText="« Prev"
                    FirstPageText="First" LastPageText="Last" />--%>
                    <RowStyle CssClass="odd" />
                    <AlternatingRowStyle CssClass="even" />
                    <EmptyDataTemplate>
                        No records.
                    </EmptyDataTemplate>
                </cc2:EmptyDisplayGridView>
                <div class="buttonsDiv" style="display: none">
                    <asp:Button ID="Button1" CssClass="save" runat="server" Text="Associate Salary head to Group"
                        OnClientClick="voucherPoupCall('');return false;" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
