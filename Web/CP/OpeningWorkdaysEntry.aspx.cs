﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using Utils.Helper;
using Utils.Web;
using BLL.Base;

namespace Web
{
    public partial class OpeningWorkdaysEntry : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.AdministratorTools;
            }
        }
        private int _tempCurrentPage;
        private int _tempCount;
        private PayManager payMgr = new PayManager();
        protected void Page_Load(object sender, EventArgs e)
        {
            //RegisterEmployeeSavedMsg();
            if (!IsPostBack)
            {
                pagintCtl.CurrentPage = 1;
                BindEmployees();

            }

            JavascriptHelper.AttachNonDialogPopUpCode(Page, "taxImportPopup", "../ExcelWindow/GratuityOpeningImportExcel.aspx", 450, 500);
        }
        //void RegisterEmployeeSavedMsg()
        //{
        //    if (SessionManager.EmployeeSaved)
        //    {
        //        JavascriptHelper.DisplayClientMsgWithTimeout("Employee Information Updated.", Page);
        //        SessionManager.EmployeeSaved = false;
        //    }
        //}

        protected void btnPrevious_Click()
        {
            pagintCtl.CurrentPage -= 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage -= 1;
            BindEmployees();
        }

        protected void btnNext_Click()
        {
            pagintCtl.CurrentPage += 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage += 1;
            BindEmployees();
        }

        protected void ddlRecords_SelectedIndexChanged()
        {
            //SessionManager.EmployeeDisplayRecodsPerPage = int.Parse(pagintCtl.DDLRecords.SelectedValue);
            //_tempCurrentPage = 1;
            pagintCtl.CurrentPage = 1;
            BindEmployees();
        }

        void BindEmployees()
        {
            int totalRecords = 0;
            gvw.DataSource
                = EmployeeManager.GetWorkdays(txtEmployeeName.Text.Trim(), pagintCtl.CurrentPage-1, int.Parse(pagintCtl.DDLRecords.SelectedValue), ref totalRecords);
            gvw.DataBind();
            pagintCtl.UpdatePagingBar(totalRecords);


           // btnUpdate.Visible = !CommonManager.IsFirstSalaryGenerated();

        }
     
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            Page.Validate("Balance");
            bool isSavedReqd = false;
            decimal workdays, workdaysAdjustment; int unpaidLeavesForGratuity; decimal gratuity = 0;
            decimal adjustment = 0;
            //if (Page.IsValid)
            {
                StringBuilder str = new StringBuilder("<root>");
              
                foreach (GridViewRow row in gvw.Rows)
                {
                    int employeeId = (int)gvw.DataKeys[row.RowIndex][0];
                   
                   
                    TextBox txt = row.FindControl("txtWorkdays") as TextBox;
                    TextBox txtUnpaidLeavesForGratuity = row.FindControl("txtUnpaidLeavesForGratuity") as TextBox;
                    TextBox txtWorkdaysAdjustment = row.FindControl("txtWorkdaysAdjustment") as TextBox;
                    TextBox txtGratuity = row.FindControl("txtGratuity") as TextBox;
                    TextBox txtGratuityAdjustment = row.FindControl("txtGratuityAdjustment") as TextBox;

                    isSavedReqd = true;

                    if (!decimal.TryParse(txt.Text, out workdays))
                        workdays = 0;

                    if (!decimal.TryParse(txtWorkdaysAdjustment.Text, out workdaysAdjustment))
                        workdaysAdjustment = 0;

                    if (!decimal.TryParse(txtGratuityAdjustment.Text, out adjustment))
                        adjustment = 0;

                    int.TryParse(txtUnpaidLeavesForGratuity.Text, out unpaidLeavesForGratuity);

                    decimal.TryParse(txtGratuity.Text.Trim(), out gratuity);
                    
                    str.Append(
                        string.Format("<row EmployeeId=\"{0}\" Workdays=\"{1}\"  UnpaidLeavesForGratuity=\"{2}\"  WorkdayAdjustment=\"{3}\" Gratuity=\"{4}\" GratuityAmountAdjustment=\"{5}\" /> ",
                             employeeId, workdays,unpaidLeavesForGratuity,workdaysAdjustment,gratuity,adjustment)
                        );
                }
                str.Append("</root>");

                if (isSavedReqd)
                {
                   EmployeeManager.SaveForOpeningWorkdays(str.ToString());

                    JavascriptHelper.DisplayClientMsg("Information saved.", Page);
                }
            }
        }

        protected void txtEmployeeName_TextChanged(object sender, EventArgs e)
        {
            _tempCurrentPage = 1;
            BindEmployees();

        }
    }
}
