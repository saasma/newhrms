﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using Utils.Helper;
using System.IO;
using Utils;
using Utils.Security;
using Utils.Calendar;

namespace Web.CP
{
    public partial class DesignationTransferList : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.HumanResource;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest && !IsPostBack)
            {
                Initialize();
            }
        }


        protected void Page_PreRender(object sender, EventArgs e)
        {
            bool isEng = false;
            if (UserManager.IsEnglishDatePreferredInNepaliDateSys())
                isEng = true;

            UserControl ctrl = Master.ContentHeaderCtrl();
            ResourceManager rm = ctrl.FindControl("ResourceManager1") as ResourceManager;

            rm.RegisterClientInitScript("Date11111", string.Format("isEnglish = {0};", isEng.ToString().ToLower()));

            rm.RegisterClientInitScript("Date22222", string.Format("todayDate = '{0}';function calendarOnClick(event, sourceElement, e3){{}};",
                                                                      CustomDate.GetTodayDate(isEng).ToString()));
        }

        private void Initialize()
        {
            if (CommonManager.IsServiceHistoryEnabled)
            {
                btnNew.Visible = false;
                CommandColumn1.Visible = false;
            }

            X.Js.Call("searchList");

            cmbLevel.Store[0].DataSource = NewPayrollManager.GetAllParentLevelList();
            cmbLevel.Store[0].DataBind();

            List<HR_Service_Event> eventList = CommonManager.GetServieEventTypes();
            if (eventList.Count > 0)
            {
                cmbEventType.Store[0].DataSource = eventList;
                cmbEventType.Store[0].DataBind();
            }

            WDesignation.Hide();
            if (SessionManager.CurrentCompany.IsEnglishDate)
                colFromDate.Hide();

            JavascriptHelper.AttachNonDialogPopUpCode(Page, "DesignationHistImport", "../ExcelWindow/DesignationImport.aspx", 450, 500);
        }

        protected void cmbLevel_Select(object sender, DirectEventArgs e)
        {
            cmbDesignation.GetStore().DataSource = NewHRManager.GetDesignationsByLevel(
                int.Parse(cmbLevel.SelectedItem.Value)).ToList();
            cmbDesignation.GetStore().DataBind();
        }

        private void Clear()
        {
            calFromDate.Text = "";
            txtNote.Text = "";
            cmbDesignation.Clear();
            cmbEmployee.Clear();
            cmbEmployee.Enable();
            lblEName.Text = "";
            lblEBranch.Text = "";
            lblEDepartment.Text = "";
            lblESince.Text = "";
            lblInTime.Text = "";
            lblLevelDesignation.Text = "";


            image1.ImageUrl = Page.ResolveUrl(Web.Helper.WebHelper.GetEmployeeNoImage(0));
        }

        protected void btnNewTransfer_Click(object sender, DirectEventArgs e)
        {
            Clear();
            hdnEmployeeDesignationId.Text = "";
            hdnEmployeeId.Text = "";
            WDesignation.Show();


            if (cmbEmpSearch.SelectedItem != null && cmbEmpSearch.SelectedItem.Value != null)
            {
                EEmployee emp = EmployeeManager.GetEmployeeById(int.Parse(cmbEmpSearch.SelectedItem.Value));
                EmployeeDetails(emp.EmployeeId);
                cmbEmployee.Text = emp.Name;
                hdnEmployeeId.Text = emp.EmployeeId.ToString();

                CommonManager.SaveFirstIfDesignationNotExists(emp.EmployeeId);
            }
        }

        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            int totalRecords = 0;
            DateTime? fromDate = null;
            DateTime? toDate = null;

            if (!string.IsNullOrEmpty(txtFromDate.Text.Trim()) && txtFromDate.SelectedDate != DateTime.MinValue)
                fromDate = txtFromDate.SelectedDate;

            if (!string.IsNullOrEmpty(txtToDate.Text.Trim()) && txtToDate.SelectedDate != DateTime.MinValue)
                toDate = txtToDate.SelectedDate;

            int employeeId = -1;
            string employeeSearch = "";

            int.TryParse(cmbEmpSearch.SelectedItem.Value, out employeeId);

            if (employeeId == 0)
                employeeId = -1;

            if (employeeId == -1)
            {
                if (!string.IsNullOrEmpty(cmbEmpSearch.Text))
                    employeeSearch = cmbEmpSearch.Text.Trim();
            }

            List<GetDesignationTransfersResult> list = NewHRManager.GetDesignationTransfersNew(e.Start, int.Parse(cmbPageSize.SelectedItem.Value), employeeSearch, ref totalRecords, employeeId
                , fromDate, toDate);


            e.Total = totalRecords;

            //if (list.Any())
            storeEmpList.DataSource = list;
            storeEmpList.DataBind();

        }

        protected Field OnCreateFilterableField(object sender, ColumnBase column, Field defaultField)
        {
            if (column.DataIndex == "DesignationId")
            {
                ((TextField)defaultField).Icon = Icon.Magnifier;
            }

            return defaultField;
        }

        // Return the number of years, months, days, hours, minutes, seconds,
        // and milliseconds you need to add to from_date to get to_date.
        private void GetElapsedTime(DateTime from_date, DateTime to_date,
            out int years, out int months, out int days, out int hours,
            out int minutes, out int seconds, out int milliseconds)
        {
            // If from_date > to_date, switch them around.
            if (from_date > to_date)
            {
                GetElapsedTime(to_date, from_date,
                    out years, out months, out days, out hours,
                    out minutes, out seconds, out milliseconds);
                years = -years;
                months = -months;
                days = -days;
                hours = -hours;
                minutes = -minutes;
                seconds = -seconds;
                milliseconds = -milliseconds;
            }
            else
            {
                // Handle the years.
                years = to_date.Year - from_date.Year;

                // See if we went too far.
                DateTime test_date = from_date.AddMonths(12 * years);
                if (test_date > to_date)
                {
                    years--;
                    test_date = from_date.AddMonths(12 * years);
                }

                // Add months until we go too far.
                months = 0;
                while (test_date <= to_date)
                {
                    months++;
                    test_date = from_date.AddMonths(12 * years + months);
                }
                months--;

                // Subtract to see how many more days,
                // hours, minutes, etc. we need.
                from_date = from_date.AddMonths(12 * years + months);
                TimeSpan remainder = to_date - from_date;
                days = remainder.Days;
                hours = remainder.Hours;
                minutes = remainder.Minutes;
                seconds = remainder.Seconds;
                milliseconds = remainder.Milliseconds;
            }
        }

        private void EmployeeDetails(int employeeId)
        {
            EEmployee emp = EmployeeManager.GetEmployeeById(employeeId);

            bool hasPhoto = false;
            if (emp.HHumanResources.Count > 0)
            {
                if (!string.IsNullOrEmpty(emp.HHumanResources[0].UrlPhotoThumbnail))
                {
                    image1.ImageUrl = Page.ResolveUrl(Config.UploadLocation + "/" + emp.HHumanResources[0].UrlPhotoThumbnail);
                    if (File.Exists(Server.MapPath(image1.ImageUrl)))
                        hasPhoto = true;
                }
            }
            if (!hasPhoto)
            {
                image1.ImageUrl = Page.ResolveUrl(Web.Helper.WebHelper.GetEmployeeNoImage(employeeId));
            }

            lblEWorkingIn.Text = "EIN : " + emp.EmployeeId + ", I No : " + emp.EHumanResources[0].IdCardNo;
            lblEName.Text = emp.Title + " " + emp.Name;
            lblEBranch.Text = emp.Branch.Name;
            lblEDepartment.Text = emp.Department.Name;
            string level = emp.EDesignation.BLevel.Name;
            string designation = emp.EDesignation.Name;

            lblLevelDesignation.Text = (designation == level) ? designation : (level + " - " + designation);

            int years, months, days, hours;
            int minutes, seconds, milliseconds;
            ServicePeroid firstStatus = EmployeeManager.GetServicePeroidDate(emp.EmployeeId);
            GetElapsedTime(firstStatus.FromDateEng, BLL.BaseBiz.GetCurrentDateAndTime(), out years, out months, out days, out hours, out minutes, out seconds, out milliseconds);

            lblESince.Text = "Since " +
                (IsEnglish ? firstStatus.FromDateEng.ToShortDateString() : firstStatus.FromDate + " (" + firstStatus.FromDateEng.ToShortDateString() + ")");
            lblInTime.Text = string.Format("For {0} years {1} months {2} days",
                years, months, days);
        }

        protected void btnSelectEmp_Click(object sender, DirectEventArgs e)
        {
            int employeeId = int.Parse(cmbEmployee.SelectedItem.Value);
            // hdnEmployeeId.Text = employeeId.ToString();

            EEmployee emp = EmployeeManager.GetEmployeeById(employeeId);

            if (emp == null)
            {
                buttonsBar.Visible = false;
                NewMessage.ShowWarningMessage("Please select an employee.");
                return;
            }
            else
                buttonsBar.Visible = true;

            //ClearFields();
            CommonManager.SaveFirstIfDesignationNotExists(employeeId);

            block.Visible = true;

            EmployeeDetails(employeeId);


        }

        protected void btnDelete_Click(object sender, DirectEventArgs e)
        {
            int designationChangeID = int.Parse(hdnEmployeeDesignationId.Text);

            Status status = CommonManager.DeleteDesignatioChange(designationChangeID);

            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Designation change has been deleted.");
                X.Js.Call("searchList");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        protected void btnEdit_Click(object sender, DirectEventArgs e)
        {
            int employeeDesignationId = int.Parse(hdnEmployeeDesignationId.Text);
            DesignationHistory obj = NewHRManager.GetDesignationHistoryById(employeeDesignationId);
            int employeeId = obj.EmployeeId.Value;
            EEmployee emp = EmployeeManager.GetEmployeeById(employeeId);

            EmployeeDetails(employeeId);            

            cmbEmployee.Text = emp.Name;
            cmbEmployee.Disable();


            EDesignation desig = new CommonManager().GetDesignationById(obj.DesignationId.Value);
            cmbLevel.SetValue(desig.LevelId.ToString());
            cmbDesignation.GetStore().DataSource = NewHRManager.GetDesignationsByLevel(desig.LevelId.Value).ToList();
            cmbDesignation.GetStore().DataBind();
            cmbDesignation.SetValue(obj.DesignationId.Value.ToString());

            if (IsEnglish == false && SessionManager.User.IsEnglishPreferred != null && SessionManager.User.IsEnglishPreferred)
            {


                if (obj.FromDateEng != null)
                    calFromDate.Text = obj.FromDateEng.Value.ToString("yyyy/MM/dd");


            }
            else
            {
                calFromDate.Text = obj.FromDate;
            }
            txtNote.Text = obj.Note;

            if (obj.EventID != null)
                cmbEventType.SetValue(obj.EventID.ToString());

            btnSave.Text = Resources.Messages.Update;
            WDesignation.Center();
            WDesignation.Show();
        }


        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveDesTransfer");
            if (Page.IsValid)
            {
                DesignationHistory obj = new DesignationHistory();

                obj.EmployeeId = int.Parse(hdnEmployeeId.Text);
                obj.DesignationId = int.Parse(cmbDesignation.SelectedItem.Value);

                if (IsEnglish == false && SessionManager.User.IsEnglishPreferred != null && SessionManager.User.IsEnglishPreferred)
                {
                    obj.FromDateEng = BLL.BaseBiz.GetEngDate(calFromDate.Text.Trim(), true);
                    obj.FromDate = BLL.BaseBiz.GetAppropriateDate(obj.FromDateEng.Value);
                }
                else
                {
                    obj.FromDate = calFromDate.Text.Trim();
                    obj.FromDateEng = BLL.BaseBiz.GetEngDate(obj.FromDate, IsEnglish);
                }

                if (!string.IsNullOrEmpty(txtNote.Text.Trim()))
                    obj.Note = txtNote.Text.Trim();

                obj.EventID = int.Parse(cmbEventType.SelectedItem.Value);

                // Validate specific Designation under the Level
                //if (NewHRManager.HasEmployeeLevelGrade(obj.EmployeeId.Value))
                //{
                //    EEmployee emp = EmployeeManager.GetEmployeeById(obj.EmployeeId.Value);
                //    if (emp != null)
                //    {
                //        BLevel level = NewPayrollManager.GetLevelById(emp.PEmployeeIncomes[0].LevelId.Value);
                //        EDesignation designationLevel = new CommonManager().GetDesignationById(obj.DesignationId.Value);

                //        if (level.LevelId != designationLevel.LevelId)
                //        {
                //            NewMessage.ShowWarningMessage("Employee level is " + level.Name + ", so desingation applied should be of level " + level.Name + ".");
                //            return;
                //        }
                //    }
                //}


                if (!string.IsNullOrEmpty(hdnEmployeeDesignationId.Text))
                {
                    obj.EmployeeDesignationId = int.Parse(hdnEmployeeDesignationId.Text);

                    NewHRManager.UpdateDesignationHistory(obj);

                    hdnMsg.Text = "Designation transfer updated.";
                }
                else
                {
                    Status status = NewHRManager.SaveDesignationHistory(obj);

                    if (status.IsSuccess == false)
                    {
                        NewMessage.ShowWarningMessage(status.ErrorMessage);
                        return;
                    }
                    else
                        hdnMsg.Text = "Designation transfer saved.";
                }

                btnCancel_Click(null, null);

            }

        }

        protected void btnCancel_Click(object sender, DirectEventArgs e)
        {
            btnSave.Text = "Save";

            if (string.IsNullOrEmpty(hdnEmployeeId.Text))
            {
                WDesignation.Close();
                return;
            }

            int employeeId = int.Parse(hdnEmployeeId.Text);

            if (employeeId != -1)
            {
                NewHRManager.SaveFirstDesignHistoryIfNotExists(employeeId);
                Clear();
            }

            if (e == null)
                NewMessage.ShowNormalMessage(hdnMsg.Text);

            WDesignation.Close();

            X.Js.Call("searchList");
        }

        public void btnExport_Click(object sender, EventArgs e)
        {
            int totalRecords = 0;
            DateTime? fromDate = null;
            DateTime? toDate = null;

            if (!string.IsNullOrEmpty(txtFromDate.Text.Trim()) && txtFromDate.SelectedDate != DateTime.MinValue)
                fromDate = txtFromDate.SelectedDate;

            if (!string.IsNullOrEmpty(txtToDate.Text.Trim()) && txtToDate.SelectedDate != DateTime.MinValue)
                toDate = txtToDate.SelectedDate;

            int employeeId = -1;
            string employeeSearch = "";

            int.TryParse(cmbEmpSearch.SelectedItem.Value, out employeeId);

            if (employeeId == 0)
                employeeId = -1;

            if (employeeId == -1)
            {
                if (!string.IsNullOrEmpty(cmbEmpSearch.Text))
                    employeeSearch = cmbEmpSearch.Text.Trim();
            }

            List<GetDesignationTransfersResult> list = NewHRManager.GetDesignationTransfers(0, 999999, employeeSearch, ref totalRecords, employeeId,fromDate,toDate);

           
            foreach (var item in list)
            {
                item.FromDateEngFormatted = item.FromDateEng.Value.ToShortDateString();
            }

            Bll.ExcelHelper.ExportToExcel("Designation Change Report", list,
                new List<String>() {"EmployeeDesignationId", "FromDateEng", "TotalRows", "RowNumber" },
            new List<String>() { },
            new Dictionary<string, string>() { { "FromDate", "Date" }, { "FromDateEngFormatted", "Date(AD)" }, { "EmployeeId", "EIN" }, { "EmployeeName", "Employee Name" }, { "FromDesignation", "From Designation" }, { "ToDesignation", "To Designation" } },
            new List<string>() { }
            , new Dictionary<string, string>() { } 
            , new List<string> { "FromDate", "FromDateEngFormatted", "EmployeeId", "EmployeeName", "FromDesignation", "ToDesignation" });



        }





    }
}