﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Master/Details.Master"
    CodeBehind="GroupInsurance.aspx.cs" Inherits="Web.CP.GroupInsurance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">
        function InsuranceNamePopupCall() {
            var ret = insuranceNamePopup("isPopup=true");
            if (typeof (ret) != 'undefined') {
                ChangeDropDownList('<%= ddlInsuranceName.ClientID %>', ret);
            }
        }
        function InstitutionPopupCall() {
            var ret = InstitutionPopup("isPopup=true");
            if (typeof (ret) != 'undefined') {
                ChangeDropDownList('<%= ddlInstitution.ClientID %>', ret);
            }
        }

        function PremiumLessThanPolicyAmount(source, args) {
            var premium = document.getElementById('<%= txtPremium.ClientID %>');
            var policyAmount = document.getElementById('<%= txtPolicyAmount.ClientID %>');

            if (premium.value < policyAmount.value) {
                args.IsValid = true;
            }
            else {
                args.IsValid = false;
                alert('Premium must be less than the Policy Amount.');
                policyAmount.focus();
            }
        }

        function EndDateGreaterThanStartDate(source, args) {

            var calStartingDate = '<%= calStartDate.ClientID %>';
            var calEndingDate = '<%= calEndDate.ClientID %>';

            if (isSecondCalendarGreater(calStartingDate, calEndingDate) == true) {
                args.IsValid = true;
            }
            else {
                alert('End Date must be greater than Start Date.');
                args.IsValid = false;
            }

            //            var start = new Date(document.getElementById('<%= calStartDate.ClientID %>' + '_datey').value,
            //        document.getElementById('<%= calStartDate.ClientID %>' + '_datem').value,
            //        document.getElementById('<%= calStartDate.ClientID %>' + '_dated').value);
            //            var end = new Date(document.getElementById('<%= calEndDate.ClientID %>' + '_datey').value,
            //        document.getElementById('<%= calEndDate.ClientID %>' + '_datem').value,
            //        document.getElementById('<%= calEndDate.ClientID %>' + '_dated').value);

            //            if (start < end) {
            //                args.IsValid = true;
            //            }
            //            else {
            //                args.IsValid = false;
            //                alert('End Date must be greater than Start Date.');
            //            }
        }

        function AllowOnlyNumbers(evt) {
            var e = event || evt;
            // for trans-browser compatibility    
            var charCode = e.which || e.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
        function handleDelete() {
            if (confirm('Do you want to delete insurance?')) {
                clearUnload();
                return true;
            }
            else
                return false;
        }
        function clearUnload() {
            window.onunload = null;
        }
    
    
    </script>

    <style type="text/css">
        .GridRowHover
        {
            cursor: pointer;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <fieldset>
        Insurance Name*:
        <asp:DropDownList ID="ddlInsuranceName" runat="server" DataValueField="Id" DataTextField="InsuranceName"
            AppendDataBoundItems="true">
            <asp:ListItem Value="-1">--Select One--</asp:ListItem>
        </asp:DropDownList>
        <input onclick="InsuranceNamePopupCall()" id="btnInsuranceNamePopup" type="button"
            value="..." />
        <asp:RequiredFieldValidator ID="valInsuranceNameReq" ControlToValidate="ddlInsuranceName"
            InitialValue="-1" Display="None" runat="server" ErrorMessage="Insurance Name is required."></asp:RequiredFieldValidator>
        <br />
        Institution Name*:
        <asp:DropDownList ID="ddlInstitution" runat="server" DataValueField="Id" DataTextField="InstitutionName"
            AppendDataBoundItems="true">
            <asp:ListItem Value="-1">--Select One--</asp:ListItem>
        </asp:DropDownList>
        <input onclick="InstitutionPopupCall()" id="btnInstitutionPopup" type="button" value="..." />
        <asp:RequiredFieldValidator ID="valInstituteNameReq" ControlToValidate="ddlInstitution"
            InitialValue="-1" Display="None" runat="server" ErrorMessage="Institution is required."></asp:RequiredFieldValidator>
        <br />
        Policy No*:
        <asp:TextBox ID="txtPolicyNo" runat="server" MaxLength="25"></asp:TextBox>
        <asp:RequiredFieldValidator ID="valPolicyReq" ControlToValidate="txtPolicyNo" Display="None"
            runat="server" ErrorMessage="Policy No is required."></asp:RequiredFieldValidator>
        <br />
        Start Month*:
        <My:Calendar Id="calStartDate" runat="server" />
        End Month*:
        <My:Calendar Id="calEndDate" runat="server" />
        <br />
        Apply To:
        <asp:DropDownList ID="ddlBranch" runat="server" DataValueField="BranchId" DataTextField="Name"
            AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlBranch_SelectedIndexChanged">
            <asp:ListItem Value="-1">--Select Branch--</asp:ListItem>
        </asp:DropDownList>
        <asp:RequiredFieldValidator ID="ReqVal" ControlToValidate="ddlBranch" InitialValue="-1"
            Display="None" runat="server" ErrorMessage="Branch name is required."></asp:RequiredFieldValidator>
    </fieldset>
    <fieldset>
        Policy Amount*:
        <asp:TextBox ID="txtPolicyAmount" runat="server" onkeypress="return AllowOnlyNumbers(event);"></asp:TextBox>
        <asp:RequiredFieldValidator ID="valPolicyAmt" ControlToValidate="txtPolicyAmount"
            Display="None" runat="server" ErrorMessage="Policy Amount is required."></asp:RequiredFieldValidator>
        <asp:RangeValidator ID="valPolicyAmtDataType" Display="None" ControlToValidate="txtPolicyAmount"
            Type="Double" runat="server" ErrorMessage="Policy Amount must be number."></asp:RangeValidator>
        <br />
        Premium*:
        <asp:TextBox ID="txtPremium" runat="server" onkeypress="return AllowOnlyNumbers(event);"></asp:TextBox>
        <asp:RequiredFieldValidator ID="valPremium" ControlToValidate="txtPremium" Display="None"
            runat="server" ErrorMessage="Premium Amount is required."></asp:RequiredFieldValidator>
        <asp:RangeValidator ID="valPremiumDataType" ControlToValidate="txtPremium" Display="None"
            Type="Double" runat="server" ErrorMessage="Policy Amount must be number."></asp:RangeValidator>
        <br />
        Premium Payment:
        <asp:RadioButtonList ID="rdbPaymentPeriod" runat="server" RepeatColumns="2" RepeatDirection="Vertical"
            RepeatLayout="Table" DataTextField="Text" DataValueField="Value">
        </asp:RadioButtonList>
        <br />
        Note:
        <asp:TextBox ID="txtNote" runat="server" TextMode="MultiLine" Rows="3" Columns="30"></asp:TextBox>
        <asp:CustomValidator ID="CustomValidator2" runat="server" ErrorMessage="Premium must be less than the Policy Amount."
            Display="None" ClientValidationFunction="PremiumLessThanPolicyAmount" ControlToValidate="txtPolicyAmount"
            ValidateEmptyText="true"></asp:CustomValidator>
            <asp:TextBox  style="display:none"  ID="hf2" runat="server" />
        <asp:CustomValidator ID="CustomValidator3" runat="server" ErrorMessage="End Date must be greater than Start Date."
            Display="None" ClientValidationFunction="EndDateGreaterThanStartDate"  ControlToValidate="hf2"
            ValidateEmptyText="true" ></asp:CustomValidator>
    </fieldset>
    <fieldset>
        <asp:GridView ID="gvInsuranceDetails" runat="server" DataKeyNames="Id" AutoGenerateColumns="False"
            CellPadding="4" ForeColor="#333333" GridLines="None" Width="713px" OnRowCreated="gvInsuranceDetails_RowCreated"
            OnRowDeleting="gvInsuranceDetails_RowDeleting" OnSelectedIndexChanged="gvInsuranceDetails_SelectedIndexChanged">
            <RowStyle BackColor="#EFF3FB" CssClass="GridRowHover" />
            <Columns>
                <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                    DataField="InsuranceName" HeaderText="Insurance Name">
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                    DataField="PolicyNo" HeaderText="Policy No">
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                    DataField="BranchName" HeaderText="Applies to">
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                    DataField="PremiumAmount" HeaderText="Premium Amount">
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                    DataField="StartDate" HeaderText="Start Month">
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                    DataField="EndDate" HeaderText="End Month">
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundField>
                <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                    HeaderText="">
                    <ItemTemplate>
                        <asp:ImageButton ID="btnDelete" OnClientClick="return handleDelete()" runat="server"
                            CommandName="Delete" ImageUrl="~/images/delete.gif" CausesValidation="false" />
                        &nbsp;
                        <asp:ImageButton ID="btnEdit" CommandName="Select" ImageUrl="~/images/edit.gif" runat="server"
                            CausesValidation="false" />
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:TemplateField>
            </Columns>
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
            <EmptyDataTemplate>
                This Employee has no insurance records.
            </EmptyDataTemplate>
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <EditRowStyle BackColor="#2461BF" />
            <AlternatingRowStyle BackColor="White" />
        </asp:GridView>
        <asp:Button ID="btnSave" runat="server" Text="Save" OnClientClick="return CheckValidation()"
            OnClick="btnSave_Click" />
    </fieldset>
</asp:Content>
