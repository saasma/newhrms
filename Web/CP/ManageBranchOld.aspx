<%@ Page Title="Manage branches" Language="C#" ValidateRequest="false" EnableEventValidation="false"
    MasterPageFile="~/Master/Details.Master" AutoEventWireup="true" CodeBehind="ManageBranchOld.aspx.cs"
    Inherits="Web.CP.ManageBranchOld" %>

<%@ Register Src="~/UserControls/MGBranch.ascx" TagName="MGBranch" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        h2.popstitle
        {
            color: #333333;
            font: bold 13px Arial;
            padding: 0 !important;
            margin: 3px 0 0;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <div class="contentArea">
        <h4 style="margin-left:20px">
            Branch List</h4>
        <%-- <h2>Manage Branches</h2>--%>
        <%--  <div class="bodypart bodypartsmall">--%>
        <%-- <div class="bodypart">--%>
        <uc1:MGBranch Id="MGBranch1" runat="server" />
        <%--   </div>--%>
    </div>
</asp:Content>
<%--edid by sudab--%>