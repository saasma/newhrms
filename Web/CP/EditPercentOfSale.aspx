﻿<%@ Page Language="C#" MasterPageFile="~/Master/Details.Master" AutoEventWireup="true"
    CodeBehind="EditPercentOfSale.aspx.cs" Inherits="Web.CP.EditPercentOfSale" Title="Edit Percent of Sales" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="../Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <h2>
        Employee List</h2>
    <div>
        <div>
            <div style="float: left;">
                Search:</div>
            <div>
                <asp:TextBox ID="txtEmployeeName" AutoPostBack="true" runat="server" Width="146px"
                    OnTextChanged="txtEmployeeName_TextChanged"></asp:TextBox>
                <cc1:TextBoxWatermarkExtender ID="TBWE2" runat="server" TargetControlID="txtEmployeeName"
                    WatermarkText="Enter employee name" WatermarkCssClass="watermarked" />
            </div>
        </div>
    </div>
    <cc2:EmptyDisplayGridView  CssClass="tableLightColor" UseAccessibleHeader="true"  ShowHeaderWhenEmpty="True" ID="gvEmployeeIncome" runat="server"
        DataKeyNames="EmployeeId,EmployeeIncomeId" GridLines="None" AutoGenerateColumns="False" 
        AllowSorting="True" ShowFooterWhenEmpty="False">
        <Columns>
            <asp:BoundField SortExpression="EmployeeId" HeaderStyle-Width="20px" DataFormatString="{0:000}"
                HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" DataField="EmployeeId"
                HeaderText="Id" Visible="false">
                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                <ItemStyle HorizontalAlign="Left"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField DataField="EmployeeIncomeId" HeaderText="EmployeeIncomeId" Visible="false" />
            <asp:TemplateField HeaderText="Name" HeaderStyle-Width="150px">
                <ItemTemplate>
                    <%# ((DAL.EEmployee)Eval("EEmployee")).Name %>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" Width="150px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Income Title" HeaderStyle-Width="150px">
                <ItemTemplate>
                    <%# ((DAL.PIncome)Eval("PIncome")).Title %>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" Width="150px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Rate" HeaderStyle-Width="150px">
                <HeaderStyle HorizontalAlign="Left" />
                <ItemTemplate>
                    <asp:Literal ID="ltRetePercent" runat="server" Text='<%# Eval("RatePercent")%>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Amount" HeaderStyle-Width="150px">
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" />
                <ItemTemplate>
                    <asp:TextBox ID="txtAmount" runat="server" Text='<%#Eval("Amount") %>' />
                    <asp:RequiredFieldValidator ID="rfTxtAmount" runat="server" ControlToValidate="txtAmount"
                        Display="None" ErrorMessage="*" ValidationGroup="grpUpdate" />
                    <asp:RegularExpressionValidator ID="reTxtAmount" runat="server" ControlToValidate="txtAmount"
                        Display="None" ErrorMessage="*" ValidationExpression="d[0-9]" ValidationGroup="grpUpdate" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
         <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
        <EmptyDataTemplate>
            <b>No employee records found. </b>
        </EmptyDataTemplate>
    </cc2:EmptyDisplayGridView>
    <%--<br />
    <asp:Panel ID="pnlPageInfo" runat="server">
        Page
        <asp:Label ID="lblCurrentPage" runat="server"></asp:Label>
        of
        <asp:Label ID="lblTotalPage" runat="server"></asp:Label>
        <br />
    </asp:Panel>
    <div>
        Show
        <asp:DropDownList AutoPostBack="true" ID="ddlRecords" runat="server" OnSelectedIndexChanged="ddlRecords_SelectedIndexChanged">
            <asp:ListItem Text="10">10</asp:ListItem>
            <asp:ListItem Text="20">20</asp:ListItem>
            <asp:ListItem Text="50">50</asp:ListItem>
        </asp:DropDownList>
        records per page
    </div>
    <br />
    <div>
        <asp:Button ID="btnPrevious" runat="server" Text="<" OnClick="btnPrevious_Click" />
        <asp:Button ID="btnNext" runat="server" Text=">" OnClick="btnNext_Click" />
    </div>--%>
    <uc1:PagingCtl ID="pagingCtl" OnNextRecord="btnNext_Click" OnPrevRecord="btnPrevious_Click"
        OnDropDownShowRecordsChanged="ddlRecords_SelectedIndexChanged" runat="server" />
    <div>
        
        <asp:Button ID="btnUpdate" Visible="true" runat="server" Text="Update" OnClick="btnUpdate_Click" />
    </div>
</asp:Content>
