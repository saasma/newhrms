﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using BLL;
using DAL;
using Utils.Web;
using Utils.Helper;
using Utils;
using System.IO;
using Web.ReportDataSetTableAdapters;
using Web.CP.Report.Templates.Pay.Detail;
using BLL.BO;
using Utils.Calendar;
using Ext.Net;

namespace Web.CP
{
    public partial class LoanAdjustment : BasePage
    {
        CommonManager commonMgr = new CommonManager();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                Initialise();


            string eventTarget = Request.Form["__EVENTTARGET"];
            //employeeId = UrlHelper.GetIdFromQueryString("EId");
            if (eventTarget != null && eventTarget.Equals("Reload"))
            {
                btnLoad_Click(null, null);
            }

            JavascriptHelper.AttachPopUpCode(Page, "popupUpdateDeduction", "../cp/AEDeduction.aspx", 600, 600);
          
        }

        public void Initialise()
        {

            ddlLoans.DataSource = PayManager.GetSimpleLoans();
            ddlLoans.DataBind();


            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                int empId = int.Parse(Request.QueryString["ID"]);
                string name = EmployeeManager.GetEmployeeName(empId);

                int? siLoanId = EmployeeManager.GetAnyLoanIDForEmp(empId);
                if (siLoanId != null)
                {
                    UIHelper.SetSelectedInDropDown(ddlLoans, siLoanId);

                    ddlLoans_SelectedIndexChanged(null, null);

                    UIHelper.SetSelectedInDropDown(ddlEmployees, empId);

                    btnLoad_Click(null, null);
                }

            }
            //BindEmployees();
        }

        //public void BindEmployees()
        //{
        //}

        protected void value_Change(object sender, EventArgs e)
        {
         
        }
        protected void ddlPayrollPeriods_SelectedIndexChanged(object sender, EventArgs e)
        {
            //BindEmployees();
        }

        protected void btnHistoryExport_Click(object sender, EventArgs e)
        {
            if (ddlEmployees.SelectedValue == "" || ddlLoans.SelectedValue == "")
                return;

            double interestRate = double.Parse(txtInterestRate.Text.Trim());
            decimal? newPPMT = 0;
            int noOfPayments = int.Parse(txtNoOfPayment.Text.Trim());
            if (!string.IsNullOrEmpty(txtNewPPMT.Text.Trim()))
                newPPMT = decimal.Parse(txtNewPPMT.Text.Trim());


            int employeeId = int.Parse(ddlEmployees.SelectedValue);
            int deductionId = int.Parse(ddlLoans.SelectedValue);


            PEmployeeDeduction ded = PayManager.GetEmployeeDeduction(employeeId, deductionId);

            List<HistoryExport> list = PayManager.GetLoanAdjustmentHistoryExport(employeeId, deductionId);


            List<string> hiddenList = new List<string>();
            hiddenList.Add("EMI");

            Dictionary<string, string> renameList = new Dictionary<string, string>();


            Dictionary<string, string> title = new Dictionary<string, string>();
            title["Loan History for "] = EmployeeManager.GetEmployeeName(employeeId) + " - " + employeeId;

            Bll.ExcelHelper.ExportToExcel("Loan History", list,
                hiddenList,
            new List<String>() { },
            renameList,
            new List<string>() { "Amount" }
            , title
            , new List<string> { });


        }

        protected void btnExportLastAdjustmentTable_Click(object sender, EventArgs e)
        {
            double interestRate = double.Parse(txtInterestRate.Text.Trim());
            decimal? newPPMT = 0;
            int noOfPayments = int.Parse(txtNoOfPayment.Text.Trim());
            if (!string.IsNullOrEmpty(txtNewPPMT.Text.Trim()))
                newPPMT = decimal.Parse(txtNewPPMT.Text.Trim());


            int employeeId = int.Parse(ddlEmployees.SelectedValue);
            int deductionId = int.Parse(ddlLoans.SelectedValue);

            bool hasCurrentPayrollPeriod = false;
            bool isInstallmentComplete = false;

            DAL.LoanAdjustment lastAdjustment = BLL.BaseBiz.PayrollDataContext.LoanAdjustments
                .Where(x => x.EmployeeId == employeeId && x.DeductionId == deductionId)
                .OrderByDescending(x => x.PayrollPeriodId).FirstOrDefault();

            GetLoanAdjustmentInstallmentsResult adjustmentRow = null;

            List<GetLoanAdjustmentInstallmentsResult> loans = PayManager.GetLoanAdjustments(
                deductionId, employeeId, ref hasCurrentPayrollPeriod, ref isInstallmentComplete, interestRate, newPPMT, noOfPayments);


            List<string> hiddenList = new List<string>();
            hiddenList.Add("EMI");
            hiddenList.Add("ClosingBalance");
            hiddenList.Add("Month");
            hiddenList.Add("PayrollPeriodId");
            hiddenList.Add("IsCurrentValidPayrollPeriod");
            hiddenList.Add("EmployeeId");
            hiddenList.Add("DeductionId");
            hiddenList.Add("IsPayrollPeriodSaved");
            hiddenList.Add("DoNotAdjustLoan");
            hiddenList.Add("InstallmentMonths");
           
            Dictionary<string, string> renameList = new Dictionary<string, string>();

            renameList.Add("RemainingInstallments", "Remaining Installments");
            renameList.Add("MonthName", "Month");
            renameList.Add("AdjustmentPPMT", "Adjustment PPMT");
            renameList.Add("AdjustmentIPMT", "Adjustment IPMT");
            renameList.Add("AdjustedPPMT", "Adjusted PPMT");
            renameList.Add("AdjustedInterest", "Adjusted Interest");
            renameList.Add("AdjustedClosing", "Adjusted Closing");


            PEmployeeDeduction empDed = PayManager.GetEmployeeDeduction(employeeId, deductionId);
            EEmployee emp = EmployeeManager.GetEmployeeById(employeeId);
           
            Dictionary<string, string> title = new Dictionary<string, string>();
            title["Loan Adjustment For "] = EmployeeManager.GetEmployeeName(employeeId) + " - " + employeeId;
            if (emp.PPays.Count > 0)
                title["Account Number"] = emp.PPays[0].BankACNo;
           
            List<GetLoanAdjustmentInstallmentsResult> newloans = new List<GetLoanAdjustmentInstallmentsResult>();


            if (lastAdjustment != null)
            {
                // exclude prev loan installments
                if ((lastAdjustment.AdjustmentType != null && lastAdjustment.AdjustmentType == (int)LoanAdjustmentType.LoanAdded)
                    ||
                    lastAdjustment.AddLoanNewInstallment != null
                    )
                {
                    foreach (GetLoanAdjustmentInstallmentsResult item in loans)
                    {
                        if (lastAdjustment.AddLoanNewInstallment == null)
                            lastAdjustment.AddLoanNewInstallment = 0;

                        if (lastAdjustment.AddLoanNewInstallment == null 
                            || item.RemainingInstallments <= lastAdjustment.AddLoanNewInstallment.Value)
                            newloans.Add(item);


                        if (item.PayrollPeriodId == lastAdjustment.PayrollPeriodId)
                            adjustmentRow = item;
                    }

                }

                title["Loan Taken Date"] = lastAdjustment.AdjustmentDateEng == null ? "" : lastAdjustment.AdjustmentDateEng.Value.ToString("dd-MMM-yyyy");
                title[""] = "";
                title["Retirement Fund account"] = emp.EFundDetails[0].PFRFNo == null ? "" : emp.EFundDetails[0].PFRFNo;
                title["Interest Rate"] = lastAdjustment.InterestNew != null ? lastAdjustment.InterestNew.ToString() + "%" :
                    (empDed.InterestRate == null ? "" : empDed.InterestRate.ToString() + "%");
                title[" "] = "";
                title["Total PF Contribution"] = "";
                title["Maximum Permissable Loan 90%"] = "";
                title["Loan Taken"] = ""; ;
                title["Repayment Period (Installment)"] = "";
                title["Repayment amount(fixed Installment)"] = "";

                title["  "] = "";
                title["Total Outstanding Loan"] = GetCurrency(Convert.ToDecimal(adjustmentRow == null ? 0 : adjustmentRow.Opening));
                title["Partial Payment"] = GetCurrency(Convert.ToDecimal(adjustmentRow == null ? 0 : adjustmentRow.AdjustmentPPMT));
                title["Remaining Loan"] = GetCurrency(Convert.ToDecimal(adjustmentRow == null ? 0 : adjustmentRow.Opening) -
                                   Convert.ToDecimal(adjustmentRow == null ? 0 : adjustmentRow.AdjustmentPPMT));
                title["Loan Partial Payment (Installment)"] = lastAdjustment == null || lastAdjustment.AddLoanNewInstallment == null ? "" :
                        lastAdjustment.AddLoanNewInstallment.ToString();
                title["Repayment amount(fixed Installment)  "] = "";
                
            }
                // loan is being just started
            else
            {
                title["Loan Taken Date"] = empDed.TakenOnEng == null ? "" : empDed.TakenOnEng.Value.ToString("dd-MMM-yyyy");
                title[""] = "";
                title["Retirement Fund account"] = emp.EFundDetails[0].PFRFNo;
                title["Interest Rate"] = (empDed.InterestRate == null ? "" : empDed.InterestRate.ToString() + "%");
                title[" "] = "";
                title["Total PF Contribution"] = "";
                title["Maximum Permissable Loan 90%"] = "";
                title["Loan Taken"] = ""; ;
                title["Repayment Period (Installment)"] = empDed.ToBePaidOver == null ? "" : empDed.ToBePaidOver.ToString();
                title["Repayment amount(fixed Installment)"] = "";

                //title["  "] = "";
                //title["Total Outstanding Loan"] = GetCurrency(Convert.ToDecimal(adjustmentRow == null ? 0 : adjustmentRow.Opening));
                //title["Partial Payment"] = GetCurrency(Convert.ToDecimal(adjustmentRow == null ? 0 : adjustmentRow.AdjustmentPPMT));
                //title["Remaining Loan"] = GetCurrency(Convert.ToDecimal(adjustmentRow == null ? 0 : adjustmentRow.Opening) -
                //                   Convert.ToDecimal(adjustmentRow == null ? 0 : adjustmentRow.AdjustmentPPMT));
                //title["Loan Partial Payment (Installment)"] = lastAdjustment == null || lastAdjustment.AddLoanNewInstallment == null ? "" :
                //        lastAdjustment.AddLoanNewInstallment.ToString();
                //title["Repayment amount(fixed Installment)  "] = "";
            }

            title["   "] = "";
            title["    "] = "";
            if (lastAdjustment != null)
            {
                PayrollPeriod period = CommonManager.GetPayrollPeriod(lastAdjustment.PayrollPeriodId);
                if (period != null)
                    title[string.Format("Repayment schedule starting {0}", period.Name)] = "";
            }

            Bll.ExcelHelper.ExportToExcel("Loan Adjustment List", (newloans.Count>0? newloans : loans),
                hiddenList,
            new List<String>() { },
            renameList,
            new List<string>() { "Opening","PPMT","IPMT","Closing","Addition",
                "AdjustmentPPMT","AdjustmentIPMT","AdjustedPPMT","AdjustedInterest","AdjustedClosing" }
            , title
            , new List<string> { "RemainingInstallments","Year","MonthName","Opening","PPMT","IPMT","Closing","Addition",
                "AdjustmentPPMT","AdjustmentIPMT","AdjustedPPMT","AdjustedInterest","AdjustedClosing"});


        }

        protected void btnExportTable_Click(object sender, EventArgs e)
        {
            double interestRate = double.Parse(txtInterestRate.Text.Trim());
            decimal? newPPMT = 0;
            int noOfPayments = int.Parse(txtNoOfPayment.Text.Trim());
            if (!string.IsNullOrEmpty(txtNewPPMT.Text.Trim()))
                newPPMT = decimal.Parse(txtNewPPMT.Text.Trim());


            int employeeId = int.Parse(ddlEmployees.SelectedValue);
            int deductionId = int.Parse(ddlLoans.SelectedValue);

            bool hasCurrentPayrollPeriod = false;
            bool isInstallmentComplete = false;

            List<GetLoanAdjustmentInstallmentsResult> loans = PayManager.GetLoanAdjustments(
                deductionId, employeeId, ref hasCurrentPayrollPeriod, ref isInstallmentComplete, interestRate, newPPMT, noOfPayments);


            List<string> hiddenList = new List<string>();
            hiddenList.Add("EMI");
            hiddenList.Add("ClosingBalance");
            hiddenList.Add("Month");
            hiddenList.Add("PayrollPeriodId");
            hiddenList.Add("IsCurrentValidPayrollPeriod");
            hiddenList.Add("EmployeeId");
            hiddenList.Add("DeductionId");
            hiddenList.Add("IsPayrollPeriodSaved");
            hiddenList.Add("DoNotAdjustLoan");
            hiddenList.Add("InstallmentMonths");
           
            Dictionary<string, string> renameList = new Dictionary<string, string>();

            renameList.Add("RemainingInstallments", "Remaining Installments");
            renameList.Add("MonthName", "Month");
            renameList.Add("AdjustmentPPMT", "Adjustment PPMT");
            renameList.Add("AdjustmentIPMT", "Adjustment IPMT");
            renameList.Add("AdjustedPPMT", "Adjusted PPMT");
            renameList.Add("AdjustedInterest", "Adjusted Interest");
            renameList.Add("AdjustedClosing", "Adjusted Closing");


            PEmployeeDeduction empDed = PayManager.GetEmployeeDeduction(employeeId, deductionId);

            Dictionary<string, string> title = new Dictionary<string, string>();
            title["Loan Adjustment For "] = EmployeeManager.GetEmployeeName(employeeId) + " - " + employeeId;
            title["Account Number"] = empDed.LoanAccount;


            Bll.ExcelHelper.ExportToExcel("Loan Adjustment List", loans,
                hiddenList,
            new List<String>() { },
            renameList,
            new List<string>() { "Opening","PPMT","IPMT","Closing","Addition",
                "AdjustmentPPMT","AdjustmentIPMT","AdjustedPPMT","AdjustedInterest","AdjustedClosing" }
            , title
            , new List<string> { "RemainingInstallments","Year","MonthName","Opening","PPMT","IPMT","Closing","Addition",
                "AdjustmentPPMT","AdjustmentIPMT","AdjustedPPMT","AdjustedInterest","AdjustedClosing"});


        }

        protected void btnGenerateTable_Click(object sender, EventArgs e)
        {
         


            int employeeId = int.Parse(ddlEmployees.SelectedValue);
            int deductionId = int.Parse(ddlLoans.SelectedValue);
            // int payrollPeriodId = int.Parse(ddlPayrollPeriods.SelectedValue);


            double interestRate = double.Parse(txtInterestRate.Text.Trim());
            decimal? newPPMT = 0;
            int noOfPayments = int.Parse(txtNoOfPayment.Text.Trim());
            if (!string.IsNullOrEmpty(txtNewPPMT.Text.Trim()))
                newPPMT = decimal.Parse(txtNewPPMT.Text.Trim());

            // first delete current month adjustment if any
            int validPayrollPeriodId = 0;
            PayrollPeriod validPayrollPeriod = CommonManager.GetValidLastPayrollPeriod();
            if (validPayrollPeriod != null)
            {
                if (CalculationManager.IsCalculationSavedForEmployee(validPayrollPeriod.PayrollPeriodId, employeeId))
                    validPayrollPeriod = null;
                else
                    validPayrollPeriodId = validPayrollPeriod.PayrollPeriodId;
            }
        

            bool hasCurrentPayrollPeriod = false;

            PEmployeeDeduction empDed = PayManager.GetEmployeeDeduction(employeeId, deductionId);

            //if (validPayrollPeriodId != 0 &&
            //    ( (interestRate != empDed.InterestRate) || (newPPMT != empDed.SINewPPMTAmount) || (noOfPayments != empDed.ToBePaidOver) ))
            //{
            //    PayManager.DeleteCurrentMonthLoanAdustment(validPayrollPeriodId, employeeId, deductionId);
            //}

            LoadGrid(employeeId, deductionId, ref hasCurrentPayrollPeriod);

            if (empDed != null)
            {
                //txtInterestRate.Enabled = hasCurrentPayrollPeriod;
                //btnSave.Visible = hasCurrentPayrollPeriod;
            }
            //if valid payroll period not exists then show message
            else if (!hasCurrentPayrollPeriod)
            {
                divMsgCtl.InnerHtml = Resources.ResourceTooltip.LoanAdjustmentNoValidPayrollPeriod;
                divMsgCtl.Hide = false;
            }

            if (EmployeeManager.IsRetiredOrResignedIncludedInCalculation(employeeId))
            {
                divWarningMsg.InnerHtml = "Employee already retired.";
                divWarningMsg.Hide = false;
                //btnSave.Visible = false;
                return;
            }
        }

        //protected void btnSendMail_Click(object sender, EventArgs e)
        //{

        //    bool isSaved = false;

        //    foreach (GridViewRow row in gvwSimpleLoanInstallments.Rows)
        //    {
        //        int payrollPeriodId = (int)gvwSimpleLoanInstallments.DataKeys[row.RowIndex]["PayrollPeriodId"];
        //        int IsCurrentValidPayrollPeriod = (int)gvwSimpleLoanInstallments.DataKeys[row.RowIndex]["IsCurrentValidPayrollPeriod"];
        //        int employeeId = (int)gvwSimpleLoanInstallments.DataKeys[row.RowIndex]["EmployeeId"];
        //        int deductionId = (int)gvwSimpleLoanInstallments.DataKeys[row.RowIndex]["DeductionId"];

        //        if (IsCurrentValidPayrollPeriod == 1)
        //        {
        //            TextBox txtAdjustmentPPMT = row.FindControl("txtAdjustmentPPMT") as TextBox;
        //            TextBox txtAdjustmentIPMT = row.FindControl("txtAdjustmentIPMT") as TextBox;

        //            System.Web.UI.WebControls.Label lblPPMT = row.FindControl("lblPPMT") as System.Web.UI.WebControls.Label;
        //            System.Web.UI.WebControls.Label lblRemInstallment = row.FindControl("lblRemInstallment") as System.Web.UI.WebControls.Label;

        //            System.Web.UI.WebControls.Label lblOpening = row.FindControl("lblOpening") as System.Web.UI.WebControls.Label;
        //            CheckBox chkDoNotAdjustLoan = row.FindControl("chkDoNotAdjustLoan") as CheckBox;

        //            decimal ipmt = PayManager.CalculateSimpleInterestRate(decimal.Parse(lblOpening.Text.Trim())
        //                , float.Parse(txtInterestRate.Text.Trim()));

        //            if (payrollPeriodId != 0)
        //            {
        //                PayrollPeriod validPayrollPeriod = CommonManager.GetPayrollPeriod(payrollPeriodId);
        //                PEmployeeDeduction empDeduction = PayManager.GetEmployeeDeduction(employeeId, deductionId);

        //                CustomDate takenOn = CustomDate.GetCustomDateFromString(empDeduction.TakenOn, IsEnglish);
        //                CustomDate startFrom = CustomDate.GetCustomDateFromString(empDeduction.StartingFrom, IsEnglish);

        //                // For first month, if Taken On and Starting From lies in same month and Taken on is not the first day then calculate proprotinately 
        //                if (validPayrollPeriod != null && validPayrollPeriod.StartDateEng == empDeduction.StartingFromEng
        //                    && takenOn.Day != 1
        //                    && takenOn.Month == startFrom.Month
        //                    && takenOn.Year == startFrom.Year)
        //                {
        //                    //firstItem.IPMT = empDeduction.AdvanceLoanAmount.Value * ((decimal)empDeduction.InterestRate.Value / 100 / 12);

        //                    int totayDaysInStartingMonth = DateHelper.GetTotalDaysInTheMonth(takenOn.Year, takenOn.Month, IsEnglish);
        //                    int daysDiffBetTakenOnStartingFrom = totayDaysInStartingMonth - (empDeduction.TakenOnEng.Value - empDeduction.StartingFromEng.Value).Days;
        //                    ipmt = empDeduction.AdvanceLoanAmount.Value * (decimal)(empDeduction.InterestRate.Value / 100.0) * (daysDiffBetTakenOnStartingFrom / (decimal)365.0);
        //                }
        //            }


        //            DAL.LoanAdjustment loan = new DAL.LoanAdjustment();


        //            loan.PayrollPeriodId = payrollPeriodId;
        //            loan.EmployeeId = employeeId;
        //            loan.DeductionId = deductionId;
        //            loan.DoNotAdjustLoan = chkDoNotAdjustLoan.Checked;

        //            if (txtAdjustmentPPMT.Text.Trim() == "" || txtAdjustmentPPMT.Text.Trim().Equals("0"))
        //                loan.PPMTAdjustment = null;
        //            else
        //                loan.PPMTAdjustment = decimal.Parse(txtAdjustmentPPMT.Text);


        //            if (txtAdjustmentIPMT.Text.Trim() == "" || txtAdjustmentIPMT.Text.Trim().Equals("0"))
        //                loan.IPMTAdjustment = null;
        //            else
        //                loan.IPMTAdjustment = decimal.Parse(txtAdjustmentIPMT.Text);


        //            decimal newPPMT = 0;

        //            if(!string.IsNullOrEmpty(txtNewPPMT.Text.Trim()))
        //                newPPMT = decimal.Parse(txtNewPPMT.Text.Trim());

        //            decimal ppmt = newPPMT;// +(loan.PPMTAdjustment == null ? 0 : loan.PPMTAdjustment.Value);  //decimal.Parse(lblPPMT.Text);

        //            // then value is reset so revert ppmt to old value
        //            if (newPPMT == 0)
        //            {
        //                ppmt = decimal.Parse(lblOpening.Text.Trim()) / decimal.Parse(lblRemInstallment.Text.Trim());
        //            }

        //            int noOfPayments = int.Parse(txtNoOfPayment.Text.Trim());

        //            //PEmployeeDeduction empDed = PayManager.GetEmployeeDeduction(employeeId, deductionId);
        //            //if (empDed.ToBePaidOver != noOfPayments)
        //            //{
        //            //    BLL.BaseBiz.PayrollDataContext.ChangeMonetories.InsertOnSubmit(BLL.BaseBiz.GetMonetoryChangeLog(loan.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Loan,
        //            //         empDed.PDeduction.Title + " : No of Payments", empDed.ToBePaidOver,
        //            //         noOfPayments, LogActionEnum.Update));

        //            //    empDed.ToBePaidOver = noOfPayments;
        //            //    empDed.NoOfInstallments = noOfPayments;

        //            //    BLL.BaseBiz.PayrollDataContext.SubmitChanges();

        //            //    btnLoad_Click(null, null);
        //            //}

        //            Status status = PayManager.SaveUpdate(loan, double.Parse(txtInterestRate.Text.Trim()), decimal.Parse(lblOpening.Text), ppmt, ipmt
        //                ,ddlLoans.SelectedItem.Text,newPPMT,noOfPayments);

        //            isSaved = true;
        //            break;
        //        }
        //    }


        //    if (isSaved)
        //    {
        //        btnLoad_Click(null, null);

        //        divMsgCtl.InnerHtml = "Saved";
        //        divMsgCtl.Hide = false;
        //    }

        //}

        protected void btnSendMailForAll_Click(object sender, EventArgs e)
        {
         
        }

        protected void gvwEmployees_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
          
        }

        #region New Events 

        public void btnDeleteAdjustment_Click(object sender, DirectEventArgs e)
        {
            int employeeId = int.Parse(currentEmployeeId.Text);
            int payrollPeriodId = int.Parse(currentPayrollPeriod.Text);
            int deductionId = int.Parse(currentDeductionId.Text);

            Status status = PayManager.DeleteCurrentMonthSimpleInterestLoan(employeeId, deductionId, payrollPeriodId);

            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Loan adjustment has been deleted.", "realoadTable");
            }
            else
                NewMessage.ShowWarningMessage(status.ErrorMessage);
        }

        public void btnAddLoanSave_Click1(object sender, DirectEventArgs e)
        {
            decimal opening = decimal.Parse(currentOpening.Text.Trim());
            float interestRate = float.Parse(txtInterestRate.Text.Trim());
            int noOfPayments = int.Parse(txtNoOfPayment.Text.Trim());

           


            int employeeId = int.Parse(currentEmployeeId.Text);
            int payrollPeriodId = int.Parse(currentPayrollPeriod.Text);
            int deductionId = int.Parse(currentDeductionId.Text);

            if (BLL.BaseBiz.PayrollDataContext.LoanAdjustments.Any(x => x.EmployeeId == employeeId && x.PayrollPeriodId == payrollPeriodId
                && x.DeductionId == deductionId))
            {
                NewMessage.ShowWarningMessage("Adjustment already exists for current month, please delete the prevoius adjustment to place new adjustment.");
                return;
            }

            DAL.LoanAdjustment loan = new DAL.LoanAdjustment();

            loan.AdjustmentDateEng = dateAddLoan.SelectedDate;
            loan.AdjustmentDate = BLL.BaseBiz.GetAppropriateDate(loan.AdjustmentDateEng.Value);

            //if (payrollPeriodId != 0)
            //{
            PayrollPeriod validPayrollPeriod = CommonManager.GetPayrollPeriod(payrollPeriodId);

            if (loan.AdjustmentDateEng < validPayrollPeriod.StartDateEng || loan.AdjustmentDateEng > validPayrollPeriod.EndDateEng)
            {
                NewMessage.ShowWarningMessage("Adjustment date should lie in current period.");
                return;
            }

            PEmployeeDeduction empDeduction = PayManager.GetEmployeeDeduction(employeeId, deductionId);

            CustomDate takenOn = CustomDate.GetCustomDateFromString(loan.AdjustmentDate, IsEnglish);
            //CustomDate startFrom = CustomDate.GetCustomDateFromString(empDeduction.StartingFrom, IsEnglish);

            decimal ipmt = 0;

            //// For first month, if Taken On and Starting From lies in same month and Taken on is not the first day then calculate proprotinately 
            //if (takenOn.Day != 1)
            //{
            //    //firstItem.IPMT = empDeduction.AdvanceLoanAmount.Value * ((decimal)empDeduction.InterestRate.Value / 100 / 12);

            //    //int totayDaysInStartingMonth = DateHelper.GetTotalDaysInTheMonth(takenOn.Year, takenOn.Month, IsEnglish);
            //    int daysDiffBetTakenOnStartingFrom = (loan.AdjustmentDateEng.Value - validPayrollPeriod.StartDateEng.Value).Days;
            //    ipmt = opening * (decimal)(empDeduction.InterestRate.Value / 100.0) * (daysDiffBetTakenOnStartingFrom / (decimal)365.0);
            //}
            //else
            //{
            //    ipmt = PayManager.CalculateSimpleInterestRate(opening, interestRate
            //        ,DateHelper.GetTotalDaysInTheMonth(takenOn.Year,takenOn.Month,IsEnglish));
            //}

            loan.PayrollPeriodId = payrollPeriodId;
            loan.EmployeeId = employeeId;
            loan.DeductionId = deductionId;
            loan.DoNotAdjustLoan = false;


            loan.AdjustmentType = (int)LoanAdjustmentType.LoanAdded;
            loan.AdjustmentInstallmentOption = int.Parse(cmbAddLoanInstallationOption.SelectedItem.Value);

            decimal newPPMT = 0;
            decimal ppmt = 0;


            // do not update this field as needed to preserve when adjustment deleted
            loan.ToBePaidPrev = noOfPayments;
            loan.SINewPPMTAmountOrNewPPMTPrev = empDeduction.SINewPPMTAmount;


            loan.Addition = decimal.Parse(txtAddLoanAmount.Text);
            loan.AdjustmentAmount = loan.Addition;

            ppmt = opening / decimal.Parse(currentRemInstallment.Text.Trim());
            newPPMT = ppmt;
            loan.AdjustedPPMT = ppmt;
            loan.AdjustedClosing = opening + loan.Addition - ppmt;


            int beforeDays = (loan.AdjustmentDateEng.Value - validPayrollPeriod.StartDateEng.Value).Days;
            int totayDaysInStartingMonth = DateHelper.GetTotalDaysInTheMonth(takenOn.Year, takenOn.Month, IsEnglish);
            int afterDays = totayDaysInStartingMonth - beforeDays;

            if (afterDays < 0)
                afterDays = 0;

            // before/after days is correct for this case, do not change for this one
            ipmt =
                ( (opening ) * beforeDays * (decimal)(interestRate / 100.0) / (decimal)365.0) +
                ( (opening  +loan.Addition.Value) * afterDays * (decimal)(interestRate / 100.0) / (decimal)365.0);


            if (loan.AdjustmentInstallmentOption == (int)AddLoanInstallmentOption.SameInstallmentAmount)
            {
                // preserve same installment amount
                if (empDeduction.SINewPPMTAmount != null && empDeduction.SINewPPMTAmount != 0)
                {
                    ppmt = empDeduction.SINewPPMTAmount.Value;
                    newPPMT = ppmt;
                    loan.AdjustedPPMT = ppmt;
                    loan.AdjustedClosing = opening + loan.Addition - ppmt;
                }

                // no rem installment or period will be change so recalculate
                noOfPayments = (int)Math.Ceiling((double)((loan.AdjustedClosing) / ppmt));
                // add paid installment period
                noOfPayments += int.Parse(currentPaidInstallment.Text);
                loan.ToBePaidNew = noOfPayments;
            }
            else if (loan.AdjustmentInstallmentOption == (int)AddLoanInstallmentOption.SameInstallmentPeriod)
            {
                ppmt = (opening + loan.Addition.Value) / decimal.Parse(currentRemInstallment.Text.Trim());
                newPPMT = 0;
            }
                // define new installment
            else if (loan.AdjustmentInstallmentOption == (int)AddLoanInstallmentOption.DefineNewInstallmentWithFixedAmount)
            {
                decimal data;
                if (decimal.TryParse(txtAddLoanNoOfInstallment.Text, out data) == false)
                {
                    NewMessage.ShowWarningMessage("New Installment amount is required.");
                    return;
                }

                ppmt = data;
                newPPMT = data;

                noOfPayments = (int)Math.Ceiling((opening + loan.Addition.Value) / data);

                loan.NewFixedInstallment = newPPMT;    
                

                //ppmt = (opening + loan.Addition.Value) / noOfPayments;
                //newPPMT = ppmt;
                // make -1 here as it do not count current
                noOfPayments += int.Parse(currentPaidInstallment.Text) - 1;
                loan.ToBePaidNew = noOfPayments;
            }
            else
            {
                int data;
                if (int.TryParse(txtAddLoanNoOfInstallment.Text, out data) == false)
                {
                    NewMessage.ShowWarningMessage("No of Installment is required.");
                    return;
                }


                noOfPayments = int.Parse(txtAddLoanNoOfInstallment.Text);
                loan.AddLoanNewInstallment = noOfPayments;

                ppmt = (opening + loan.Addition.Value) / noOfPayments;
                newPPMT = ppmt;
                // make -1 here as it do not count current
                noOfPayments += int.Parse(currentPaidInstallment.Text) - 1;
                loan.ToBePaidNew = noOfPayments;
            }




            Status status =   PayManager.SaveUpdate(loan, double.Parse(txtInterestRate.Text.Trim()), opening, ppmt, ipmt
                , ddlLoans.SelectedItem.Text, newPPMT, noOfPayments);



            if (status.IsSuccess)
                NewMessage.ShowNormalMessage("Loan has been added.", "realoadTable");
            else
                NewMessage.ShowWarningMessage(status.ErrorMessage);



        }

        public void btnSaveInterest_Click(object sender, DirectEventArgs e)
        {
            decimal opening = decimal.Parse(currentOpening.Text.Trim());
            float interestRate = float.Parse(txtInterest.Text.Trim());
            int noOfPayments = int.Parse(txtNoOfPayment.Text.Trim());
            decimal ipmt = 0;

            int employeeId = int.Parse(currentEmployeeId.Text);
            int payrollPeriodId = int.Parse(currentPayrollPeriod.Text);
            int deductionId = int.Parse(currentDeductionId.Text);

            //if (BLL.BaseBiz.PayrollDataContext.LoanAdjustments.Any(x => x.EmployeeId == employeeId && x.PayrollPeriodId == payrollPeriodId
            //    && x.DeductionId == deductionId))
            //{
            //    NewMessage.ShowWarningMessage("Adjustment already exists for current month, please delete the prevoius adjustment to place new adjustment.");
            //    return;
            //}
            DAL.LoanAdjustment adjustment =
                        BLL.BaseBiz.PayrollDataContext.LoanAdjustments.FirstOrDefault(x => x.EmployeeId == employeeId && x.PayrollPeriodId == payrollPeriodId
                        && x.DeductionId == deductionId);


            DAL.LoanAdjustment loan = new DAL.LoanAdjustment();
            loan.AdjustmentDateEng = dateInterest.SelectedDate;
            loan.AdjustmentDate = BLL.BaseBiz.GetAppropriateDate(loan.AdjustmentDateEng.Value);            
            PayrollPeriod validPayrollPeriod = CommonManager.GetPayrollPeriod(payrollPeriodId);

            if (loan.AdjustmentDateEng < validPayrollPeriod.StartDateEng || loan.AdjustmentDateEng > validPayrollPeriod.EndDateEng)
            {
                NewMessage.ShowWarningMessage("Adjustment date should lie in current period.");
                return;
            }

            PEmployeeDeduction empDeduction = PayManager.GetEmployeeDeduction(employeeId, deductionId);
            CustomDate takenOn = CustomDate.GetCustomDateFromString(loan.AdjustmentDate, IsEnglish);
           
            
            loan.PayrollPeriodId = payrollPeriodId;
            loan.EmployeeId = employeeId;
            loan.DeductionId = deductionId;
            loan.DoNotAdjustLoan = false;
            loan.AdjustmentType = (int)LoanAdjustmentType.InterestRateChange;
            decimal newPPMT = 0;
            decimal ppmt = 0;
            
            // do not update this field as needed to preserve when adjustment deleted
            loan.InterestPrev = empDeduction.InterestRate;
            loan.InterestNew = interestRate;
            loan.MarketRatePrev = empDeduction.MarketRate;
            loan.MarketRateNew = double.Parse(txtMarketRate.Text);
            // get loan deduction amount
            loan.PPMTAdjustment = (adjustment != null && adjustment.PPMTAdjustment != null ? adjustment.PPMTAdjustment.Value : 0);
            // get addition amount
            loan.Addition = (adjustment != null && adjustment.Addition != null ? adjustment.Addition.Value : 0);

            loan.AdjustedPPMT = opening - loan.PPMTAdjustment + loan.Addition;
            ppmt = loan.AdjustedPPMT.Value / decimal.Parse(currentRemInstallment.Text.Trim());


            int beforeDays = (loan.AdjustmentDateEng.Value - validPayrollPeriod.StartDateEng.Value).Days;
            int totayDaysInStartingMonth = DateHelper.GetTotalDaysInTheMonth(takenOn.Year, takenOn.Month, IsEnglish);
            int afterDays = totayDaysInStartingMonth - beforeDays;

            if (afterDays < 0)
                afterDays = 0;
            ipmt =
                
                // old interest calculation for opening amount    
                (opening * beforeDays * (decimal)(empDeduction.InterestRate / 100.0) / (decimal)365.0) +
                
                // new interest calculation for the deuction/addition amount
                ((opening - loan.PPMTAdjustment.Value + loan.Addition.Value) * afterDays * (decimal)(interestRate / 100.0) / (decimal)365.0);




            Status status = new Status();


            if (BLL.BaseBiz.PayrollDataContext.LoanAdjustments.Any(x => x.EmployeeId == employeeId && x.PayrollPeriodId == payrollPeriodId
                        && x.DeductionId == deductionId))
            {
                // for now if prev adjustment exists from day 1 then only interest rate change will be correct
                // for middle case, we need to divide the period in to multiple blocks as loan addition/deduction could be on one date and
                // interest rate change could be on another date

                status = PayManager.ChangeInterestRateAfterOtherAdjustmentUpdate(loan, interestRate, opening, ppmt, ipmt
                   , ddlLoans.SelectedItem.Text, newPPMT, noOfPayments);
            }
            else
            {
                status = PayManager.SaveUpdate(loan, interestRate, opening, ppmt, ipmt
                    , ddlLoans.SelectedItem.Text, newPPMT, noOfPayments);
            }

            if (status.IsSuccess)
                NewMessage.ShowNormalMessage("Interest rate changed.", "realoadTable");
            else
                NewMessage.ShowWarningMessage(status.ErrorMessage);
        }


        public void btnSaveInterestAdjustment_Click(object sender, DirectEventArgs e)
        {
            decimal opening = decimal.Parse(currentOpening.Text.Trim());
            float interestRate = float.Parse(txtInterestRate.Text.Trim());
            int noOfPayments = int.Parse(txtNoOfPayment.Text.Trim());
            
            int employeeId = int.Parse(currentEmployeeId.Text);
            int payrollPeriodId = int.Parse(currentPayrollPeriod.Text);
            int deductionId = int.Parse(currentDeductionId.Text);            
            DAL.LoanAdjustment loan = new DAL.LoanAdjustment();
            loan.AdjustmentDateEng = dateInterestAdjustment.SelectedDate;
            loan.AdjustmentDate = BLL.BaseBiz.GetAppropriateDate(loan.AdjustmentDateEng.Value);
            
            PayrollPeriod validPayrollPeriod = CommonManager.GetPayrollPeriod(payrollPeriodId);

            if (loan.AdjustmentDateEng < validPayrollPeriod.StartDateEng || loan.AdjustmentDateEng > validPayrollPeriod.EndDateEng)
            {
                NewMessage.ShowWarningMessage("Adjustment date should lie in current period.");
                return;
            }

            PEmployeeDeduction empDeduction = PayManager.GetEmployeeDeduction(employeeId, deductionId);        

            decimal ipmt = 0;
            ipmt = PayManager.CalculateSimpleInterestRate(opening, interestRate, validPayrollPeriod.TotalDays.Value);
            
            loan.PayrollPeriodId = payrollPeriodId;
            loan.EmployeeId = employeeId;
            loan.DeductionId = deductionId;
            loan.DoNotAdjustLoan = false;
            loan.AdjustmentType = (int)LoanAdjustmentType.AdjustInterest;
            //decimal newPPMT = 0;
            decimal ppmt = opening / decimal.Parse(currentRemInstallment.Text.Trim());     

            loan.IPMTAdjustment = decimal.Parse(txtInterestAdjustmentAmount.Text);
            loan.AdjustmentAmount = loan.IPMTAdjustment;
            
            Status status = PayManager.SaveUpdateIntertestAdjustment(loan,  opening, ppmt, ipmt);
            
            if (status.IsSuccess)
                NewMessage.ShowNormalMessage("Interest rate adjusted.", "realoadTable");
            else
                NewMessage.ShowWarningMessage(status.ErrorMessage);
        }


       
        public void btnLoanSaveAdjustment_Click(object sender, DirectEventArgs e)
        {

            decimal opening = decimal.Parse(currentOpening.Text.Trim());
            float interestRate = float.Parse(txtInterestRate.Text.Trim());
            int noOfPayments = int.Parse(txtNoOfPayment.Text.Trim());

            decimal ipmt = 0;


            int employeeId = int.Parse(currentEmployeeId.Text);
            int payrollPeriodId = int.Parse(currentPayrollPeriod.Text);
            int deductionId = int.Parse(currentDeductionId.Text);

            if (BLL.BaseBiz.PayrollDataContext.LoanAdjustments.Any(x => x.EmployeeId == employeeId && x.PayrollPeriodId == payrollPeriodId
                && x.DeductionId == deductionId))
            {
                NewMessage.ShowWarningMessage("Adjustment already exists for current month, please delete the prevoius adjustment to place new adjustment.");
                return;
            }

            DAL.LoanAdjustment loan = new DAL.LoanAdjustment();

            loan.AdjustmentDateEng = dateAdjustmentPeriod.SelectedDate;
            loan.AdjustmentDate = BLL.BaseBiz.GetAppropriateDate(loan.AdjustmentDateEng.Value);

            //if (payrollPeriodId != 0)
            //{
                PayrollPeriod validPayrollPeriod = CommonManager.GetPayrollPeriod(payrollPeriodId);

                if (loan.AdjustmentDateEng < validPayrollPeriod.StartDateEng || loan.AdjustmentDateEng > validPayrollPeriod.EndDateEng)
                {
                    NewMessage.ShowWarningMessage("Adjustment date should lie in current period.");
                    return;
                }

                PEmployeeDeduction empDeduction = PayManager.GetEmployeeDeduction(employeeId, deductionId);

                CustomDate takenOn = CustomDate.GetCustomDateFromString(loan.AdjustmentDate, IsEnglish);
                //CustomDate startFrom = CustomDate.GetCustomDateFromString(empDeduction.StartingFrom, IsEnglish);

                // For first month, if Taken On and Starting From lies in same month and Taken on is not the first day then calculate proprotinately 
                if (takenOn.Day != 1)
                {
                    //firstItem.IPMT = empDeduction.AdvanceLoanAmount.Value * ((decimal)empDeduction.InterestRate.Value / 100 / 12);

                    //int totayDaysInStartingMonth = DateHelper.GetTotalDaysInTheMonth(takenOn.Year, takenOn.Month, IsEnglish);
                    int daysDiffBetTakenOnStartingFrom = (loan.AdjustmentDateEng.Value - validPayrollPeriod.StartDateEng.Value).Days;
                    ipmt = opening * (decimal)(empDeduction.InterestRate.Value / 100.0) * (daysDiffBetTakenOnStartingFrom / (decimal)365.0);
                }
                else
                {
                    ipmt =  PayManager.CalculateSimpleInterestRate(opening
                       , interestRate, DateHelper.GetTotalDaysInTheMonth(takenOn.Year, takenOn.Month, IsEnglish));
                }

            //}


           


            loan.PayrollPeriodId = payrollPeriodId;
            loan.EmployeeId = employeeId;
            loan.DeductionId = deductionId;
            loan.DoNotAdjustLoan = false;

          
            loan.AdjustmentType = int.Parse(cmbWinAdjustmentType.SelectedItem.Value);
            loan.AdjustmentPaymentOption = int.Parse(cmbWinAdjustmentPayment.SelectedItem.Value);
            if (cmbWinAdjustmentInstallmentOption.SelectedItem != null && cmbWinAdjustmentInstallmentOption.SelectedItem.Value != null)
                loan.AdjustmentInstallmentOption = int.Parse(cmbWinAdjustmentInstallmentOption.SelectedItem.Value);

            decimal newPPMT = 0;
            decimal ppmt = 0;


            // do not update this field as needed to preserve when adjustment deleted
            loan.ToBePaidPrev = noOfPayments;
            loan.SINewPPMTAmountOrNewPPMTPrev = empDeduction.SINewPPMTAmount;
            loan.AdjustmentAmount = decimal.Parse(txtWinLoanAdjustmentAmount.Text);


            if (loan.AdjustmentType == (int)LoanAdjustmentType.PartialSettlement)
            {
                loan.PPMTAdjustment = decimal.Parse(txtWinLoanAdjustmentAmount.Text);

                loan.AdjustedPPMT = opening - loan.PPMTAdjustment;

                if (cmbWinAdjustmentInstallmentOption.SelectedItem == null ||
                    cmbWinAdjustmentInstallmentOption.SelectedItem.Value == null)
                {
                    NewMessage.ShowWarningMessage("Installment option is required.");
                    return;
                }

                if (loan.AdjustmentInstallmentOption == (int)LoanAdjustmentInstallmentOption.ReduceAmount)
                {

                    ppmt = loan.AdjustedPPMT.Value / decimal.Parse(currentRemInstallment.Text.Trim());


                    int beforeDays = (loan.AdjustmentDateEng.Value - validPayrollPeriod.StartDateEng.Value).Days;
                    int totayDaysInStartingMonth = DateHelper.GetTotalDaysInTheMonth(takenOn.Year, takenOn.Month, IsEnglish);
                    int afterDays = totayDaysInStartingMonth - beforeDays;

                    if (afterDays < 0)
                        afterDays = 0;

                    ipmt =
                        (opening * beforeDays * (decimal)(interestRate / 100.0) / (decimal)365.0) +
                        (loan.AdjustedPPMT.Value * afterDays * (decimal)(interestRate / 100.0) / (decimal)365.0);

                }
                else if (loan.AdjustmentInstallmentOption == (int)LoanAdjustmentInstallmentOption.DefineNewInstallment)
                {
                    int data;
                    if (int.TryParse(txtAdjustmentNoOfInstallment.Text, out data) == false)
                    {
                        NewMessage.ShowWarningMessage("No of Installment is required.");
                        return;
                    }

                    noOfPayments = int.Parse(txtAdjustmentNoOfInstallment.Text);
                    loan.AddLoanNewInstallment = noOfPayments;

                    ppmt = (loan.AdjustedPPMT.Value) / noOfPayments;
                    newPPMT = ppmt;
                    // make -1 here as it do not count current
                    noOfPayments += int.Parse(currentPaidInstallment.Text) - 1;
                    loan.ToBePaidNew = noOfPayments;

                    int beforeDays = (loan.AdjustmentDateEng.Value - validPayrollPeriod.StartDateEng.Value).Days;
                    int totayDaysInStartingMonth = DateHelper.GetTotalDaysInTheMonth(takenOn.Year, takenOn.Month, IsEnglish);
                    int afterDays = totayDaysInStartingMonth - beforeDays;

                    ipmt =
                        (opening * beforeDays * (decimal)(interestRate / 100.0) / (decimal)365.0) +
                        (loan.AdjustedPPMT.Value * afterDays * (decimal)(interestRate / 100.0) / (decimal)365.0);                   
                }
                else if (loan.AdjustmentInstallmentOption == (int)LoanAdjustmentInstallmentOption.DefineNewInstallmentWithFixedAmount)
                {
                    decimal data;
                    if (decimal.TryParse(txtAdjustmentNoOfInstallment.Text, out data) == false)
                    {
                        NewMessage.ShowWarningMessage("New installment amount is required.");
                        return;
                    }

                    ppmt = data;
                    newPPMT = data;
                    loan.NewFixedInstallment = data;

                    noOfPayments = (int)Math.Ceiling(((opening - decimal.Parse(txtWinLoanAdjustmentAmount.Text)) / data));
                    
                    
                   
                    // make -1 here as it do not count current
                    noOfPayments += int.Parse(currentPaidInstallment.Text) - 1;
                    loan.ToBePaidNew = noOfPayments;

                    int beforeDays = (loan.AdjustmentDateEng.Value - validPayrollPeriod.StartDateEng.Value).Days;
                    int totayDaysInStartingMonth = DateHelper.GetTotalDaysInTheMonth(takenOn.Year, takenOn.Month, IsEnglish);
                    int afterDays = totayDaysInStartingMonth - beforeDays;

                    ipmt =
                        (opening * beforeDays * (decimal)(interestRate / 100.0) / (decimal)365.0) +
                        (loan.AdjustedPPMT.Value * afterDays * (decimal)(interestRate / 100.0) / (decimal)365.0);
                }
                else
                {

                    ppmt = opening / decimal.Parse(currentRemInstallment.Text.Trim());

                    newPPMT = ppmt;

                    int beforeDays = (loan.AdjustmentDateEng.Value - validPayrollPeriod.StartDateEng.Value).Days;
                    int totayDaysInStartingMonth = DateHelper.GetTotalDaysInTheMonth(takenOn.Year, takenOn.Month, IsEnglish);
                    int afterDays = totayDaysInStartingMonth - beforeDays;

                    if (afterDays < 0)
                        afterDays = 0;

                    ipmt =
                        (opening * beforeDays * (decimal)(interestRate / 100.0) / (decimal)365.0) +
                        (loan.AdjustedPPMT.Value * afterDays * (decimal)(interestRate / 100.0) / (decimal)365.0);

                   

                    // no rem installment or period will be change so recalculate
                    noOfPayments = (int) Math.Ceiling((double)((loan.AdjustedPPMT - ppmt) / ppmt));
                    // add paid installment period
                    noOfPayments += int.Parse(currentPaidInstallment.Text);

                    
                    loan.ToBePaidNew = noOfPayments;
                   
                }
                //loan.PPMTAdjustment
                //ipmt = PayManager.CalculateSimpleInterestRate(opening, interestRate);
                //if (txtAdjustmentIPMT.Text.Trim() == "" || txtAdjustmentIPMT.Text.Trim().Equals("0"))
                //    loan.IPMTAdjustment = null;
                //else
                //    loan.IPMTAdjustment = decimal.Parse(txtAdjustmentIPMT.Text);
            }
            else
            {

                newPPMT = decimal.Parse(txtWinLoanAdjustmentAmount.Text);

                ppmt = newPPMT;// +(loan.PPMTAdjustment == null ? 0 : loan.PPMTAdjustment.Value);  //decimal.Parse(lblPPMT.Text);

                // then value is reset so revert ppmt to old value
                if (newPPMT == 0)
                {
                    ppmt = opening / decimal.Parse(currentRemInstallment.Text.Trim());
                }
            }



            Status status = PayManager.SaveUpdate(loan, double.Parse(txtInterestRate.Text.Trim()), opening, ppmt, ipmt
                , ddlLoans.SelectedItem.Text, newPPMT, noOfPayments);

           


            if (status.IsSuccess)
                NewMessage.ShowNormalMessage("Adjustment saved.", "realoadTable");
            else
                NewMessage.ShowWarningMessage(status.ErrorMessage);
        }

        #endregion

        protected void ddlLoans_SelectedIndexChanged(object sender, EventArgs e)
        {
            System.Web.UI.WebControls.ListItem firstItem =  ddlEmployees.Items[0];
            ddlEmployees.Items.Clear();
           

            ddlEmployees.DataSource = PayManager.GetSimpleLoansEmployees(int.Parse(ddlLoans.SelectedValue));
            ddlEmployees.DataBind();

            ddlEmployees.Items.Insert(0, firstItem);
        }

        public string GetAmount(object value,bool isLabel)
        {
            string currency = GetCurrency(value);
            string str;
            if (currency.ToString() == "0" || currency.ToString() == "0.00")
                str = "";
            else
                str = currency;

            if (isLabel && str == "")
                return "-";


            return str;
        }

        protected void btnLoad_Click(object sender, EventArgs e)
        {
            PayrollPeriod period = CommonManager.GetLastPayrollPeriod();
            if (period != null)
            {
                dispEligiblePeriod.Text = period.Name.Replace("/", " ");
                if (this.IsEnglish == false)
                {
                    dispEligiblePeriod.Text += "(" +
                        period.StartDateEng.Value.ToString("yyyy-MMM-dd") + " - " +
                         period.EndDateEng.Value.ToString("yyyy-MMM-dd") + ")";

                    dispAddLoan.Text = dispEligiblePeriod.Text;
                    dispInterestCurrentPeriod.Text = dispEligiblePeriod.Text;
                    dispInterestAdjustment.Text = dispEligiblePeriod.Text;
                }
                
            }

            if (!IsPostBack && !X.IsAjaxRequest)
            {
                dateAdjustmentPeriod.SelectedDate = period.StartDateEng.Value;
            }


            if (ddlEmployees.SelectedValue == "-1" || ddlLoans.SelectedValue == "-1")
            {
                pnlDetails.Visible = false;
            }
            else
            {
                pnlDetails.Visible = true;

                int employeeId = int.Parse(ddlEmployees.SelectedValue);
                int deductionId = int.Parse(ddlLoans.SelectedValue);
                // int payrollPeriodId = int.Parse(ddlPayrollPeriods.SelectedValue);

                bool hasCurrentPayrollPeriod = false;

               

                // if has current month adjustment show Delete option or hide
                if (BLL.BaseBiz.PayrollDataContext.LoanAdjustments.Any(x => x.EmployeeId == employeeId &&
                    x.DeductionId == deductionId && x.PayrollPeriodId == period.PayrollPeriodId))
                {
                    btnDeleteAdjustment.Show();
                }
                else
                    btnDeleteAdjustment.Hide();

                //set information
                PEmployeeDeduction empDed = PayManager.GetEmployeeDeduction(employeeId, deductionId);
                if (empDed != null)
                {
                     btnEditLoan.Listeners.Click.Handler =
                    string.Format("return popupUpdateDeductionCall({0});",empDed.EmployeeDeductionId);

                    lblLoanAmount.Text = GetCurrency(empDed.AdvanceLoanAmount);
                    lblTakenOn.Text = empDed.TakenOn;
                    txtInterestRate.Text = empDed.InterestRate.ToString();
                    txtNoOfPayment.Text = empDed.ToBePaidOver.ToString();

                    txtNewPPMT.Text = "";
                    if (empDed.SINewPPMTAmount != null && empDed.SINewPPMTAmount.Value != 0)
                        txtNewPPMT.Text = GetCurrency(empDed.SINewPPMTAmount);

                    EEmployee emp = empDed.EEmployee;
                    lblEID.Text = emp.EmployeeId.ToString();
                    lblName.Text = emp.Name;
                    lblDesignation.Text = emp.EDesignation.Name;
                    lblBranch.Text = emp.Branch.Name;
                    lblDepartment.Text = emp.Department.Name;
                }

             

                LoadGrid(employeeId, deductionId, ref hasCurrentPayrollPeriod);

                if (empDed != null)
                {
                    //txtInterestRate.Enabled = hasCurrentPayrollPeriod;
                    //btnSave.Visible = hasCurrentPayrollPeriod;
                }
                //if valid payroll period not exists then show message
                else if (!hasCurrentPayrollPeriod)
                {
                    divMsgCtl.InnerHtml = Resources.ResourceTooltip.LoanAdjustmentNoValidPayrollPeriod;
                    divMsgCtl.Hide = false;
                }

                if (EmployeeManager.IsRetiredOrResignedIncludedInCalculation(employeeId))
                {
                    divWarningMsg.InnerHtml = "Employee already retired.";
                    divWarningMsg.Hide = false;
                    //btnSave.Visible = false;
                    return;
                }
            }
        }

        private void LoadGrid(int employeeId, int deductionId, ref bool hasCurrentPayrollPeriod)
        {

            double interestRate = double.Parse(txtInterestRate.Text.Trim());
            decimal? newPPMT = 0;
            int noOfPayments = int.Parse(txtNoOfPayment.Text.Trim());
            if (!string.IsNullOrEmpty(txtNewPPMT.Text.Trim()))
                newPPMT = decimal.Parse(txtNewPPMT.Text.Trim());


           

            bool isInstallmentComplete = false;

            List<GetLoanAdjustmentInstallmentsResult> loans = new List<GetLoanAdjustmentInstallmentsResult>();

            // if no of payment is too high
           
            //else
            {
                loans = PayManager.GetLoanAdjustments(
                    deductionId, employeeId, ref hasCurrentPayrollPeriod, ref isInstallmentComplete, interestRate, newPPMT, noOfPayments);
            }


            if (noOfPayments > 2000)
            {
                divWarningMsg.InnerHtml = "Current Installment period  " + noOfPayments + " is greater than 2,000, so could not be loaded, please delete the current adjustment.";
                divWarningMsg.Hide = false;
            }

            gvwSimpleLoanInstallments.DataSource = loans;
            gvwSimpleLoanInstallments.DataBind();


            currentEmployeeId.Text = employeeId.ToString();
            currentDeductionId.Text = deductionId.ToString();

            int paidInstallment = 0;
            //Process to set row separate for paid ones
            foreach (GridViewRow row in gvwSimpleLoanInstallments.Rows)
            {
                paidInstallment += 1;

                //GetLoanAdjustmentInstallmentsResult loanItem = row.DataItem as GetLoanAdjustmentInstallmentsResult;
                bool IsPayrollPeriodSaved = (bool)gvwSimpleLoanInstallments.DataKeys[row.RowIndex]["IsPayrollPeriodSaved"];

                if (IsPayrollPeriodSaved == false)
                {
                    int columnIndex = 0;
                    foreach (TableCell cell in row.Cells)
                    {
                        if (columnIndex != 6)
                            cell.CssClass = "hightlightCell";

                        columnIndex += 1;
                    }

                    currentPayrollPeriod.Text = CommonManager.GetLastPayrollPeriod().PayrollPeriodId.ToString();
                    currentOpening.Text = gvwSimpleLoanInstallments.DataKeys[row.RowIndex]["Opening"].ToString();
                    currentRemInstallment.Text = gvwSimpleLoanInstallments.DataKeys[row.RowIndex]["RemainingInstallments"].ToString();
                    currentPaidInstallment.Text = paidInstallment.ToString();

                    break;
                }

               
            }






            //if all installment paid then show paid msg

            if (loans != null && loans.Count > 0)
            {
                bool isAllPaid = true;
                foreach (GetLoanAdjustmentInstallmentsResult item in loans)
                {
                    if (item.IsPayrollPeriodSaved != null && item.IsPayrollPeriodSaved.Value == false)
                    {
                        isAllPaid = false;
                        break;
                    }
                }
                if (isAllPaid)
                {
                    divMsgCtl.InnerHtml = Resources.Messages.InstallmentComplete;
                    divMsgCtl.Hide = false;
                }
            }
        }

        protected void gvwSimpleLoanInstallments_RowCommand(object sender, GridViewCommandEventArgs e)
        {

        }

        protected void gvwSimpleLoanInstallments_RowDataBound(object sender, GridViewRowEventArgs e)
        {
           
        }

       

    }
}
