﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;

namespace Web.CP
{
    public partial class PasswordChangeRule : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        private void Initialise()
        {
            LoadChangingRules();
        }

        private void Clear()
        {
            txtDays.Text = "";
            if (rbForcePswdChange.Checked)
                rbForcePswdChange.Checked = false;
            if (rbRecommendPswdChange.Checked)
                rbRecommendPswdChange.Checked = false;
        }

        private void LoadChangingRules()
        {
            Setting obj = UserManager.GetSettingForPasswordChangeRule();
            if (obj != null)
            {
                if (obj.PasswordChangingDays != null)
                {
                    txtDays.Text = obj.PasswordChangingDays.ToString();

                    if (obj.PasswordChangeType == (int)PasswordChangeTypeEnum.ForcePwdChange)
                        rbForcePswdChange.Checked = true;
                    else
                        rbRecommendPswdChange.Checked = true;

                }
            }
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SavePswdDays");
            if (Page.IsValid)
            {
                if (!rbForcePswdChange.Checked && !rbRecommendPswdChange.Checked)
                {
                    NewMessage.ShowWarningMessage("Please check one of the radion buttons.");
                    return;
                }

                int days = int.Parse(txtDays.Text.Trim());
                int changeType = 0;
                if (rbForcePswdChange.Checked)
                    changeType = (int)PasswordChangeTypeEnum.ForcePwdChange;
                else
                    changeType = (int)PasswordChangeTypeEnum.RecommendPwdChange;

                Status status = UserManager.SaveUpdatePasswordChangeRule(days, changeType);
                if (status.IsSuccess)
                {
                    NewMessage.ShowNormalMessage("Password change rule saved.");
                    Clear();
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }
            }
        }
    }
}