﻿<%@ Page Title="Attendance sync status" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="AttendanceUploadStatus.aspx.cs" Inherits="Web.CP.AttendanceUploadStatus" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
         function searchListEL() {
             <%=gridDevices.ClientID %>.getStore().pageSize = <%=cmbPageSize.ClientID %>.getValue();
            <%=PagingToolbar1.ClientID %>.doRefresh();
        }


    </script>
    <style type="text/css">
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 0px !important;
        }
        #menu
        {
            display: none;
        }
        .centerButtons
        {
            padding-left: 15px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <ext:ResourceManager ID="ResourceManager1" runat="server" />
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Attendance sync status
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel" style="width: 560px;">
        <div>
            <div>
                <!-- panel-btns -->
                <h4 class="panel-title sectionHeading" style="margin-left: 20px; margin-bottom: -20px;">
                    <i></i>Attendance sync status
                </h4>
            </div>
            <div class="panel-body" style="width: 540px">
                <ext:GridPanel ID="gridDevices" runat="server" Cls="itemgrid">
                    <Store>
                        <ext:Store ID="storeDevices" runat="server" AutoLoad="true" OnReadData="Store_ReadData"
                            RemotePaging="true" RemoteSort="true">
                            <Proxy>
                                <ext:PageProxy />
                            </Proxy>
                            <Model>
                                <ext:Model ID="Model4" runat="server" IDProperty="IPAddress">
                                    <Fields>
                                        <ext:ModelField Name="IPAddress" Type="String" />
                                        <ext:ModelField Name="LastDate" Type="Date" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                    </Store>
                    <ColumnModel>
                        <Columns>
                            <ext:Column ID="Column8" Width="150" Sortable="false" MenuDisabled="true" runat="server"
                                Text="IP Address" Align="Left" DataIndex="IPAddress" />
                            <ext:DateColumn ID="Column7" Sortable="false" MenuDisabled="true" runat="server"
                                Text="Date" Align="Center" DataIndex="LastDate" />
                        </Columns>
                    </ColumnModel>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                    </SelectionModel>
                    <BottomBar>
                        <ext:PagingToolbar ID="PagingToolbar1" runat="server" StoreID="storeEmpList" DisplayInfo="true">
                            <Items>
                                <ext:Label ID="lblPageSize" runat="server" Text="Page size:" />
                                <ext:ToolbarSpacer ID="ToolbarSpacer_listItems" runat="server" Width="10" />
                                <ext:ComboBox ID="cmbPageSize" runat="server" Width="80" SelectOnFocus="true" Selectable="true"
                                    ValueField="Value" DisplayField="Text" ForceSelection="true" AllowBlank="false">
                                    <Listeners>
                                        <Select Handler="searchListEL()" />
                                    </Listeners>
                                    <Items>
                                        <ext:ListItem Value="20" Text="20" />
                                        <ext:ListItem Value="50" Text="50" />
                                        <ext:ListItem Value="100" Text="100" />
                                        <ext:ListItem Value="200" Text="200" />
                                    </Items>
                                    <SelectedItems>
                                        <ext:ListItem Index="0">
                                        </ext:ListItem>
                                    </SelectedItems>
                                </ext:ComboBox>
                            </Items>
                        </ext:PagingToolbar>
                    </BottomBar>
                </ext:GridPanel>
            </div>
        </div>
    </div>
    <br />
</asp:Content>
