﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;

namespace Web.CP
{
    public partial class IncomeIncrementHistory : BasePage
    {
       // private PEmployeeIncrement currentIncrement = null;
        protected void Page_Load(object sender, EventArgs e)
        {
           
            //currentIncrement = PayManager.GetCurrentIncrement(id);
            if (!IsPostBack)
            {
                BindData();

                //PayManager mgr = new PayManager();
                //gvw.DataSource =  mgr.GetIncrementHistory(id);
                //gvw.DataBind();


            }

            
        }

        private void BindData()
        {
            PayManager mgr = new PayManager();
            int id = int.Parse(Request.QueryString["id"]);
            PEmployeeIncome empIncome = mgr.GetEmployeeIncome(id);

            gvw.DataSource = PayManager.GetIncomeChangeHistory(empIncome.IncomeId, empIncome.EmployeeId);
            gvw.DataBind();
        }

        public string GetAmountOrRate(object amount, object rate)
        {
            if (amount != null)
            {
                return GetCurrency(amount);
            }
            else if(rate !=null)
            {
                return rate.ToString() + "%";
            }
            return "";
        }

        public string GetDifference(object diff,object amount)
        {
            if (amount != null)
            {
                return GetCurrency(diff);
            }
            else if(diff != null)
            {
                return diff.ToString() + "%";
            }
            return "";
        }

        protected void gvw_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvw.PageIndex = e.NewPageIndex;
            BindData();
        }

      
    }
}
