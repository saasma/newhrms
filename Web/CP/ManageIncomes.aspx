<%@ Page Title="Income List" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="ManageIncomes.aspx.cs" Inherits="Web.CP.Listing" %>

<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function popupAddIncomeCall() {

            var ret = popupIncome();
            if (typeof (ret) != 'undefined') {
                if (ret == 'ReloadIncome') {
                    //refreshIncomeList(0);
                    __doPostBack('<%= updPanel.ClientID %>', '');
                }
            }

            return false;
        }

        function popupUpdateIncomeCall(incomeId) {
            var ret = popupIncome('IId=' + incomeId);
            if (typeof (ret) != 'undefined') {
                if (ret == 'ReloadIncome') {
                    //refreshIncomeList(0);
                    __doPostBack('<%= updPanel.ClientID %>', '');
                }
            }
            return false;
        }

        //callback function in case of chrome to be called from modal popup as it doesn't supports modal window
        function parentIncomeCallbackFunction(state, popupWindow) {
            if (state == 'ReloadIncome') {
                //refreshIncomeList(0);
                popupWindow.close();
                __doPostBack('<%= updPanel.ClientID %>', '');
                // popupWindow.close();
            }
        }

        $(document).ready(function () {

            setMovementToGrid('#<%= gvwIncomes.ClientID %>');

        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Income list
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div runat="server" style="color: #F49D25" id="voucherAlsoChange">
            Note : If voucher is being used, please associate the newly added income to the
            voucher group also.</div>
        <asp:UpdatePanel ID="updPanel" runat="server">
            <ContentTemplate>
                <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
                <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
                <asp:GridView Style="clear: both" PagerStyle-HorizontalAlign="Center" PagerStyle-CssClass="defaultPagingBar"
                    CssClass="table table-primary mb30 table-bordered table-hover" AutoGenerateColumns="false" UseAccessibleHeader="true"
                    ID="gvwIncomes" runat="server" CellPadding="3" DataKeyNames="IncomeId" rateColumns="False"
                    GridLines="None" OnRowCreated="gvwEmployees_RowCreated" OnRowDeleting="gvwIncomes_RowDeleting">
                    <%-- <RowStyle BackColor="#E3EAEB"  />--%>
                    <Columns>
                     <asp:TemplateField HeaderText="SN" HeaderStyle-Width="50px" HeaderStyle-HorizontalAlign="Center"
                            ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <%# Eval("SN")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Title" HeaderStyle-Width="200px" HeaderStyle-HorizontalAlign="Left"
                            ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <%# Eval("Title") %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Short name" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left"
                            ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <%# Eval("Abbreviation") %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Type" HeaderStyle-Width="150px" HeaderStyle-HorizontalAlign="Left"
                            ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <%#  new DAL.IncomeCalculation().Get(Eval("Calculation").ToString()) %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Defined Type" HeaderStyle-Width="80px" HeaderStyle-HorizontalAlign="Left"
                            ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <%#  Eval("IsDefinedType") == null ? "" : (Convert.ToBoolean(Eval("IsDefinedType")) == true ? "Yes": "") %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--<asp:TemplateField HeaderText="Leave Encashment" HeaderStyle-Width="150px" HeaderStyle-HorizontalAlign="Left"
                            ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <%#  (Convert.ToBoolean(Eval("IsCountForLeaveEncasement")) == true ? "Yes" : "")%>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                        <asp:TemplateField HeaderText="Taxable" HeaderStyle-Width="70px" HeaderStyle-HorizontalAlign="Left"
                            ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <%#  (Convert.ToBoolean(Eval("IsTaxable")) == true ? "Yes" : "No")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Deduct Leave" HeaderStyle-Width="90px" HeaderStyle-HorizontalAlign="Left"
                            ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <%#  (Convert.ToBoolean(Eval("IsUpdatedWithLeave")) == true ? "No" : "Yes")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="PF" HeaderStyle-Width="80px" HeaderStyle-HorizontalAlign="Left"
                            ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <%#  (Convert.ToBoolean(Eval("IsPFCalculated")) == true ? "Yes" : "")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Notes" HeaderStyle-Width="150px" HeaderStyle-HorizontalAlign="Left"
                            ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <%#  Eval("Notes")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Active" HeaderStyle-Width="70px" HeaderStyle-HorizontalAlign="Left"
                            ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <%#  (Convert.ToBoolean(Eval("IsEnabled")) == true ? "Yes" : "")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50px"
                            HeaderText="Order" ItemStyle-HorizontalAlign="Right">
                            <ItemTemplate>
                                <asp:TextBox data-col='0' data-row='<%# Container.DataItemIndex %>' runat="server"
                                    Style='text-align: right' Width="50" ID="txtOrder" Text='<%# Eval("Order") %>'></asp:TextBox>
                                <asp:CompareValidator runat="server" ID="valOrder" Type="Integer" Operator="DataTypeCheck"
                                    Display="None" ValidationGroup="saveupdate" ControlToValidate="txtOrder"></asp:CompareValidator>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-Width="30px" HeaderText="Edit" HeaderStyle-HorizontalAlign="Center"
                            ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:ImageButton ID="btnEdit" Visible='<%# Eval("IncomeId").ToString()=="0" ? false : true %>'
                                    CommandName="Delete" ImageUrl="~/images/edit.gif" runat="server" OnClientClick='<%# "return popupUpdateIncomeCall(" +  Eval("IncomeId") + ");" %>'
                                    AlternateText="Edit" />
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-Width="30px" HeaderText="Delete" HeaderStyle-HorizontalAlign="Center"
                            ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:ImageButton ID="btnDelete" Visible='<%# BLL.Manager.PayManager.IsIncomeDeletable ( (int) Eval( "IncomeId"),(bool)Eval( "IsBasicIncome") ) %>'
                                    CommandName="Delete" ImageUrl="~/images/delet.png" runat="server" OnClientClick='return confirm("Do you want to delete the income?")'
                                    AlternateText="Delete" />
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="odd" />
                    <AlternatingRowStyle CssClass="even" />
                    <EmptyDataTemplate>
                        No incomes
                    </EmptyDataTemplate>
                </asp:GridView>
                <div class="buttonsDiv">
                    <asp:LinkButton ID="btnAddNew" Style="margin-left: 10px;height:30px;" OnClientClick="return popupAddIncomeCall();"
                        runat="server" CssClass="createbtns floatleft" Text="Add Income" />
                    <asp:Button ID="Button1" runat="server" CssClass="save" Text="Save Order" OnClick="Unnamed1_Click"
                        OnClientClick="valGroup='saveupdate';return CheckValidation();" />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
