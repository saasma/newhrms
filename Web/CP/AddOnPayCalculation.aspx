﻿<%@ Page Title="Add-On Pay" EnableEventValidation="false" MaintainScrollPositionOnPostBack="true" Language="C#" MasterPageFile="~/Master/HROld.Master" AutoEventWireup="true" CodeBehind="AddOnPayCalculation.aspx.cs" Inherits="Web.CP.AddOnPayCalculation" %>
<%@ Register src="../UserControls/AddOnCalculation.ascx" tagname="Calculation" tagprefix="uc1" %>
<%@ Register src="../UserControls/AddOnCalculationNew.ascx" tagname="CalculationNew" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

  
  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <uc1:Calculation Id="Calculation1" runat="server" />

      <uc1:CalculationNew Id="Calculation1New" Visible="false" runat="server" />
</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="pageScriptsContent" runat="server">
  <script src="../Scripts/currency.js" type="text/javascript"></script>

    <script src="../Scripts/ScrollTableHeader-v1.03.js" type="text/javascript"></script>
</asp:Content>