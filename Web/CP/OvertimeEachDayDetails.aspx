﻿<%@ Page Title="Overtime Details List" Language="C#" AutoEventWireup="true" MasterPageFile="~/Master/HROld.Master"
    CodeBehind="OvertimeEachDayDetails.aspx.cs" Inherits="Web.NewHR.OvertimeEachDayDetails" %>

<%@ Register Src="~/Controls/FooterCtl.ascx" TagName="FooterCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/HeaderCtl.ascx" TagName="HeaderCtl" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .holiday, .holiday a, .holiday td
        {
            color: #469146;
            background-color: #F0FFF0 !important;
        }
        .leave, .leave a, .leave td
        {
            color: #D95CA9;
            background-color: #FFF0F5 !important;
        }
        
        .weeklyholiday, .weeklyholiday a, .weeklyholiday td
        {
            color: #8F8F1A;
            background-color: #FAFAD2 !important;
        }
    </style>
    <script type="text/javascript">


   var getRowClass = function (record) {
    
        var dayValue = record.data.DayType;
        

        if(dayValue==3)
         {
            return "holiday";
         }
         else if(dayValue==1)
         {
            return "leave";
         }
        
         else if(dayValue==2 || dayValue == 4)
         {
            return "weeklyholiday";
         }
         else //if(dayValue=="Working Day")
         {
            return "";
         }
         //else 

      };

  
    var CommandHandler = function (command, record) {
            <%= hdnEmployeeID.ClientID %>.setValue(record.data.EmployeeID); 

            var dateStr = 
                record.data.OTDate.getFullYear() + '/' + (record.data.OTDate.getMonth() + 1) + '/' + record.data.OTDate.getDate();

             <%= hdnOTDate.ClientID %>.setValue(dateStr); 
            
            if(command == 'Saved')
            {   
                 <%= btnRevertToSaved.ClientID %>.fireEvent('click');
            }  
            if(command == 'Reject')
            {   
                 <%= btnReject.ClientID %>.fireEvent('click');
            }           
             if(command == 'Approve')
            { 
            <%= hdnApprovedHour.ClientID %>.setValue(record.data.ApprovedHrsFormatted); 
            <%= hdnApprovedMin.ClientID %>.setValue(record.data.ApprovedMinFormatted); 
            <%= btnApproveByLine.ClientID %>.fireEvent('click');
            }    
        };

        function searchList() {
            <%=PagingToolbar1.ClientID %>.doRefresh();
        }

        var gridRowBeforeSelect = function (record, index,e1,e2,e3) {
//                var gridrecord = <%=gridTimeSheet.ClientID %>.getStore().getAt(index.index);
//                if(gridrecord.data.StatusText=="Rejected" || gridrecord.data.StatusText=="Approved")
//                {
//                    return false;
//                }
        };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>
    <ext:ResourceManager ID="ResourceManager1" runat="server" DisableViewState="false"
        ShowWarningOnAjaxFailure="false" ScriptMode="Release" Namespace="CompanyX" />
    <ext:Hidden ID="hdnEmployeeID" runat="server" />
    <ext:Hidden ID="hdnOTDate" runat="server" />
    <ext:Hidden ID="hdnApprovedHour" runat="server" />
    <ext:Hidden ID="hdnApprovedMin" runat="server" />
    <ext:LinkButton ID="btnReject" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnReject_Click">
                <Confirmation ConfirmRequest="true" Message="Are you sure, you want to Reject?" />
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
     <ext:LinkButton ID="btnRevertToSaved" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnRevertToSaved_Click">
             <Confirmation ConfirmRequest="true" Message="Are you sure, you want revert to saved status?" />
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:LinkButton ID="btnApproveByLine" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnApproveByLine_Click">
                <Confirmation ConfirmRequest="true" Message="Are you sure, you want to Approve?" />
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <div id="bodypart">
        <div class="bodypart">
            <ext:DisplayField runat="server" StyleSpec="margin-top:5px;margin-bottom:5px;" Html="<h3> Overtime Approvals</h3>"
                ID="title">
            </ext:DisplayField>
            <div class="attribute" style="padding: 10px">
                <table>
                    <tr>
                        <td>
                            <ext:DateField MarginSpec="0 5 0 5" LabelWidth="40" Width="170" FieldLabel="Date"
                                ID="calDate" runat="server" LabelAlign="Left" LabelSeparator="">
                                <Plugins>
                                    <ext:GenericPlugin ID="HighlightDates1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                                </Plugins>
                            </ext:DateField>
                        </td>
                        <td style="width: 60px; padding-left: 10px">
                            Employee
                        </td>
                        <td style="width: 200px;">
                            <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                                <Proxy>
                                    <ext:AjaxProxy Url="../Handler/EmpSearch.ashx">
                                        <ActionMethods Read="GET" />
                                        <Reader>
                                            <ext:JsonReader Root="plants" TotalProperty="total" />
                                        </Reader>
                                    </ext:AjaxProxy>
                                </Proxy>
                                <Model>
                                    <ext:Model ID="Model2" IDProperty="Name" Name="ItemLineModel" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Name" Type="String" />
                                            <ext:ModelField Name="EmployeeId" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                            <ext:ComboBox LabelSeparator="" ID="cmbSearch" LabelWidth="70" runat="server" DisplayField="Name"
                                ValueField="EmployeeId" EmptyText="Search" StoreID="storeSearch" TypeAhead="false"
                                Width="180" HideBaseTrigger="true" MinChars="1" TriggerAction="All" ForceSelection="true">
                                <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                                    <ItemTpl ID="ItemTpl1" runat="server">
                                        <Html>
                                            <tpl>
                                            <div class="search-item">
                                                            <span>{Name}</span>  
                                             </div>
					                </tpl>
                                        </Html>
                                    </ItemTpl>
                                </ListConfig>
                                <Triggers>
                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                </Triggers>
                                <Listeners>
                                    <Select Handler="this.getTrigger(0).show();" />
                                    <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                    <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                                </Listeners>
                            </ext:ComboBox>
                        </td>
                        <td>
                            <ext:ComboBox Width="200" ForceSelection="true" LabelWidth="45" ID="cmbStatus" runat="server"
                                FieldLabel="Status" LabelSeparator="">
                                <Items>
                                    <ext:ListItem Text="All" Value="-1" />
                                    <ext:ListItem Text="Saved" Value="0" />
                                    <ext:ListItem Text="Approved" Value="1" />
                                    <ext:ListItem Text="Rejected" Value="2" />
                                </Items>
                                <SelectedItems>
                                    <ext:ListItem Index="0" />
                                </SelectedItems>
                            </ext:ComboBox>
                        </td>
                        <td style="width: 20px">
                        </td>
                        <td>
                            <ext:Button runat="server" Width="100" Height="30" ID="btnLoad" Text="Load">
                                <Listeners>
                                    <Click Handler="searchList();" />
                                </Listeners>
                            </ext:Button>
                        </td>
                    </tr>
                </table>
            </div>
            <br />
            <table>
                <tr>
                    <td>
                        <ext:GridPanel ID="gridTimeSheet" runat="server" Cls="itemgrid" Width="1190" Scroll="None">
                            <Store>
                                <ext:Store ID="Store3" OnReadData="Store_ReadData" runat="server" RemoteSort="true"
                                    AutoLoad="true" PageSize="50">
                                    <Proxy>
                                        <ext:PageProxy />
                                    </Proxy>
                                    <Sorters>
                                        <ext:DataSorter Direction="ASC" Property="OTDate" />
                                    </Sorters>
                                    <AutoLoadParams>
                                        <ext:Parameter Name="start" Value="0" Mode="Raw" />
                                    </AutoLoadParams>
                                    <Parameters>
                                        <ext:StoreParameter Name="EmployeeId" Value="#{cmbSearch}.getValue()" Mode="Raw"
                                            ApplyMode="Always" />
                                        <ext:StoreParameter Name="status" Value="#{cmbStatus}.getValue()" Mode="Raw" />
                                        <ext:StoreParameter Name="OverTimePeriodID" Value="" Mode="Raw" ApplyMode="Always" />
                                        <ext:StoreParameter Name="Date" Value="#{calDate}.getValue()" Mode="Raw" />
                                    </Parameters>
                                    <Model>
                                        <ext:Model ID="Model1" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="EmployeeID" />
                                                <ext:ModelField Name="Name" />
                                                <ext:ModelField Name="Branch" />
                                                <ext:ModelField Name="Designation" />
                                                <ext:ModelField Name="OTDate" Type="Date" />
                                                <ext:ModelField Name="NepDate" />
                                                <ext:ModelField Name="OTHours" />
                                                <ext:ModelField Name="StatusText" />
                                                <ext:ModelField Name="OTMinute" />
                                                <ext:ModelField Name="ApprovedMin" />
                                                <ext:ModelField Name="ApprovedMinFormatted" />
                                                <ext:ModelField Name="ApprovedHrsFormatted" />
                                                <ext:ModelField Name="DataIndexCheckColumn" Type="Boolean" DefaultValue="0" />
                                                <ext:ModelField Name="ClockIn" />
                                                <ext:ModelField Name="ClockOut" />
                                                <ext:ModelField Name="DayValueText" />
                                                <ext:ModelField Name="DayType" />
                                                <ext:ModelField Name="AprovedBy" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <Plugins>
                                <ext:CellEditing ID="CellEditing1" ClicksToEdit="1" runat="server">
                                </ext:CellEditing>
                            </Plugins>
                            <ColumnModel ID="ColumnModel2" runat="server">
                                <Columns>
                                    <ext:Column ID="colEId" runat="server" Text="EIN" Width="50" DataIndex="EmployeeID"
                                        Align="Center" Sortable="true" MenuDisabled="true">
                                    </ext:Column>
                                    <ext:Column ID="Column1" runat="server" Text="Employee Name" Width="150" DataIndex="Name"
                                        Sortable="true" MenuDisabled="true">
                                    </ext:Column>
                                    <ext:DateColumn ID="colApprovedDate" runat="server" Text="OT Date" Width="90" DataIndex="OTDate"
                                        Sortable="true" MenuDisabled="true" Format="dd-MMM-yyyy" />
                                     <ext:Column ID="Column10" runat="server" Text="Nep Date" Width="90" DataIndex="NepDate"
                                        Sortable="true" MenuDisabled="true">
                                    </ext:Column>
                                    <ext:Column ID="Column8" runat="server" Text="Day Type" Width="100" DataIndex="DayValueText"
                                        Sortable="false" MenuDisabled="true">
                                    </ext:Column>
                                    <ext:Column ID="Column3" runat="server" Text="In Time" Width="80" DataIndex="ClockIn"
                                        Sortable="false" MenuDisabled="true">
                                    </ext:Column>
                                    <ext:Column ID="Column7" runat="server" Text="Out Time" Width="80" DataIndex="ClockOut"
                                        Sortable="false" MenuDisabled="true">
                                    </ext:Column>
                                    <ext:Column ID="Column4" runat="server" Align="Center" Text="OT Hours" Width="80"
                                        DataIndex="OTHours" Sortable="false" MenuDisabled="true">
                                    </ext:Column>
                                    <ext:Column ID="Column6" runat="server" Text="Status" Width="100" DataIndex="StatusText"
                                        Sortable="false" MenuDisabled="true">
                                    </ext:Column>
                                    <ext:Column ID="Column9" runat="server" Text="Approved By" Width="100" DataIndex="AprovedBy"
                                        Sortable="false" MenuDisabled="true">
                                    </ext:Column>
                                    <ext:Column ID="Column5" runat="server" Align="Center" StyleSpec="background-color:#FCE4D6"
                                        Text="Approved HR" Width="90" DataIndex="ApprovedHrsFormatted" Sortable="false"
                                        MenuDisabled="true">
                                        <Editor>
                                            <ext:NumberField ID="txtApprovedHours" AllowDecimals="false" MinValue="0" MaxValue="60"
                                                runat="server" MaskRe="[0-9]">
                                            </ext:NumberField>
                                        </Editor>
                                    </ext:Column>
                                    <ext:Column ID="Column2" runat="server" Align="Center" StyleSpec="background-color:#FCE4D6"
                                        Text="Approved Min" Width="90" DataIndex="ApprovedMinFormatted" Sortable="false"
                                        MenuDisabled="true">
                                        <Editor>
                                            <ext:NumberField ID="txtApprovedMins" AllowDecimals="false" MinValue="0" MaxValue="60"
                                                runat="server" MaskRe="[0-9]">
                                            </ext:NumberField>
                                        </Editor>
                                    </ext:Column>
                                    <ext:CommandColumn ID="columnActions" runat="server" Width="80" OverOnly="false"
                                        Text="Actions" Sortable="false">
                                        <Commands>
                                            <ext:GridCommand Text="">
                                                <Menu EnableScrolling="false">
                                                    <Items>
                                                        <ext:MenuCommand Text="Revert to Saved" Icon="BulletBlue" CommandName="Saved" />
                                                    </Items>
                                                </Menu>
                                                <ToolTip Text="Action" />
                                            </ext:GridCommand>
                                        </Commands>
                                        <Listeners>
                                            <Command Handler="CommandHandler(command,record);" />
                                        </Listeners>
                                    </ext:CommandColumn>
                                </Columns>
                            </ColumnModel>
                            <SelectionModel>
                                <ext:CheckboxSelectionModel ID="CheckboxSelectionModel1" runat="server" Mode="Simple">
                                    <Listeners>
                                        <BeforeSelect Fn="gridRowBeforeSelect" />
                                    </Listeners>
                                </ext:CheckboxSelectionModel>
                            </SelectionModel>
                            <View>
                                <ext:GridView ID="GridView1" runat="server">
                                    <Listeners>
                                    </Listeners>
                                    <GetRowClass Fn="getRowClass" />
                                </ext:GridView>
                            </View>
                            <BottomBar>
                                <ext:PagingToolbar ID="PagingToolbar1" runat="server" PageSize="50" DisplayInfo="true"
                                    DisplayMsg="Displaying Overtime {0} - {1} of {2}" EmptyMsg="No Records to display">
                                </ext:PagingToolbar>
                            </BottomBar>
                        </ext:GridPanel>
                    </td>
                </tr>
            </table>
            <div class="buttonBlock" style="width: 68%; margin-top: 20px">
                <table class="fieldTable firsttdskip">
                    <tr>
                        <td style="width: 120px">
                            <ext:Button ID="btnApprove" runat="server" Text="Approve" Width="100" Height="30"
                                Cls="">
                                <DirectEvents>
                                    <Click OnEvent="btnApprove_Click">
                                        <EventMask ShowMask="true" />
                                        <Confirmation ConfirmRequest="true" Message="Are you sure, you want to approve the Overtime?" />
                                        <ExtraParams>
                                            <ext:Parameter Name="gridItems" Value="Ext.encode(#{gridTimeSheet}.getRowsValues({selectedOnly:true}))"
                                                Mode="Raw" />
                                        </ExtraParams>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </td>
                        <td style="width: 120px">
                            <ext:Button ID="btnRejectSelected" runat="server" Text="Reject" Width="100" Height="30">
                                <DirectEvents>
                                    <Click OnEvent="btnRejectSelected_Click">
                                        <EventMask ShowMask="true" />
                                        <Confirmation ConfirmRequest="true" Message="Are you sure, you want to Reject the Overtime?" />
                                        <ExtraParams>
                                            <ext:Parameter Name="gridItems" Value="Ext.encode(#{gridTimeSheet}.getRowsValues({selectedOnly:true}))"
                                                Mode="Raw" />
                                        </ExtraParams>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </td>
                         <td style="width: 120px">
                            <ext:Button ID="btnRevertAll1" runat="server" Text="Revert to Save" Width="100" Height="30">
                                <DirectEvents>
                                    <Click OnEvent="btnRevertAll_Click">
                                        <EventMask ShowMask="true" />
                                        <Confirmation ConfirmRequest="true" Message="Are you sure, you want to Revert the Overtime?" />
                                        <ExtraParams>
                                            <ext:Parameter Name="gridItems" Value="Ext.encode(#{gridTimeSheet}.getRowsValues({selectedOnly:true}))"
                                                Mode="Raw" />
                                        </ExtraParams>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </td>
                        <td>
                            <ext:LinkButton ID="btnExport" runat="server" AutoPostBack="true" Icon="PageExcel"
                                Text="Export" OnClick="btnExcelPrint_Click">
                              
                            </ext:LinkButton>
                        </td>
                    </tr>
                </table>
            </div>
            <br />
        </div>
    </div>
</asp:Content>
