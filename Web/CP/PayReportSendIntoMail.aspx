<%@ Page MaintainScrollPositionOnPostback="true" Title="Pay reports sending" Language="C#"
    MasterPageFile="~/Master/HROld.Master" AutoEventWireup="true" CodeBehind="PayReportSendIntoMail.aspx.cs"
    Inherits="Web.CP.PayReportSendIntoMail" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        function selectDeselectAll(chk) {
            $('input[type=checkbox]').each(
                    function (index) {
                        if ($(this).prop('disabled') == false && this.id.indexOf('chkSelect') > 0)
                            this.checked = chk.checked;
                    }
                );
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Mail sending
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="attribute" style="padding: 10px">
            <table>
                <tr>
                    <td>
                        <strong>Payroll Period</strong>
                        <asp:DropDownList AutoPostBack="true" ID="ddlPayrollPeriods" DataTextField="Name"
                            DataValueField="PayrollPeriodId" AppendDataBoundItems="true" Width="150px" runat="server"
                            OnSelectedIndexChanged="ddlPayrollPeriods_SelectedIndexChanged">
                            <asp:ListItem Value="-1" Text=""></asp:ListItem>
                        </asp:DropDownList>
                        <%--<asp:RequiredFieldValidator InitialValue="-1" Display="None" ControlToValidate="ddlPayrollPeriods"
                        ValidationGroup="Overtime" ID="RequiredFieldValidator2" runat="server" ErrorMessage="Payroll period is required."></asp:RequiredFieldValidator>--%>
                        &nbsp; &nbsp; <strong>Employee Search</strong>
                        <asp:TextBox ID="txtEmployeeName" OnTextChanged="ddlPayrollPeriods_SelectedIndexChanged"
                            runat="server" AutoPostBack="true" />
                    </td>
                    <td style='padding-left:10px;'><strong>Add-On</strong>
                           <asp:DropDownList ID="ddlAddOnName" OnSelectedIndexChanged='ddlAddOnName_Change'
            AutoPostBack="true" runat="server" Width="150px" DataTextField="Name" DataValueField="AddOnId" />
     
                    </td>
                    <td>
                     &nbsp; &nbsp; <strong>Branch</strong>
                        <asp:DropDownList ID="ddlBranch" Width="120px" DataTextField="Name" DataValueField="BranchId"  onselectedindexchanged="ddlBranch_Selected"
                            AppendDataBoundItems="true" runat="server" AutoPostBack="true" >
                            <asp:ListItem Text="" Value="-1" />

                        </asp:DropDownList>
                    </td>
                    <td style='padding-left:10px;'>
                     <asp:Button OnClientClick="" Width="100px" OnClick="btnLoad_Click" Style="display: inline" ID="btnLoad" runat="server"
            CssClass="btn btn-default btn-sect btn-sm" Text="Load" />
                    </td>
                </tr>
            </table>
        </div>
        <cc2:EmptyDisplayGridView Width="100%" Style='clear: both; margin-bottom: 0px;' GridLines="None"
            PagerStyle-CssClass="defaultPagingBar" CssClass="table table-primary mb30 table-bordered table-hover"
            ShowHeaderWhenEmpty="True" ID="gvwEmployees" runat="server" DataKeyNames="CalculationEmployeeId,EmployeeId,Email"
            AutoGenerateColumns="False" ShowFooterWhenEmpty="False" OnPageIndexChanging="gvwEmployees_PageIndexChanging">
            <Columns>
                <asp:TemplateField HeaderText="EIN" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="40px">
                    <ItemTemplate>
                        <%# Eval("EmployeeId")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Employee Name" HeaderStyle-HorizontalAlign="Left"
                    HeaderStyle-Width="300px">
                    <ItemTemplate>
                        <%# Eval("Name")%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Email" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="300px">
                    <ItemTemplate>
                        <%# Eval("Email")%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Mail Sent" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                    HeaderStyle-Width="80px">
                    <ItemTemplate>
                        <%# UIHelper.GetYesNoImageTag(Eval("HasSentInMail"),Page)%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Select" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="80px"
                    HeaderStyle-HorizontalAlign="Center">
                    <HeaderTemplate>
                        <asp:CheckBox ID="chkSelect" onclick="selectDeselectAll(this)" runat="server" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:CheckBox ID="chkSelect" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField></asp:TemplateField>
            </Columns>
            <PagerSettings Mode="NumericFirstLast" NextPageText="Next �" PreviousPageText="� Prev"
                FirstPageText="First" LastPageText="Last" />
            <RowStyle CssClass="odd" />
            <AlternatingRowStyle CssClass="even" />
            <PagerStyle CssClass="defaultPagingBar" />
            <SelectedRowStyle CssClass="selected" />
            <EmptyDataTemplate>
                <strong style='padding-top: 6px; padding-left: 3px'>No employees in the payroll period.
                </strong>
            </EmptyDataTemplate>
        </cc2:EmptyDisplayGridView>
        <uc1:PagingCtl ID="pagingCtl" OnNextRecord="btnNext_Click" OnPrevRecord="btnPrevious_Click"
            OnDropDownShowRecordsChanged="ddlRecords_SelectedIndexChanged" runat="server" />
        <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
        <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
        <div cssclass="buttonsDiv" id="divButtons" runat="server" style='text-align: right;
            margin-top: 15px'>
            <%--<asp:Button ID="btnSendMailForAll" Style="display:none" OnClientClick="if(confirm('{0}')==false) return false;"
                runat="server" CssClass="update" Text="Send to all" OnClick="btnSendMailForAll_Click" />--%>
            &nbsp;&nbsp;
            <asp:Button ID="btnSendMail" OnClientClick="if(confirm('{0}')==false) return false;"
                runat="server" CssClass="btn btn-warning btn-sect btn-sm" Text="Send mail" OnClick="btnSendMail_Click" />
        </div>
    </div>
</asp:Content>
