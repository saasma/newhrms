﻿<%@ Page Title="Holiday" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="Holiday.aspx.cs" Inherits="Web.CP.Holiday" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

    var prepareToolbar = function (grid, toolbar, rowIndex, record) {
    
        if (record.data.IsDeletable > 0) {
            toolbar.items.getAt(2).hide();
        } 
    };


    var CommandHandlerWH = function(command, record){

            <%= hdnWeeklyHolidayId.ClientID %>.setValue(record.data.ID);
            
            if(command =="Edit")
            {                
                <%= btnEditWH.ClientID %>.fireEvent('click');
            }
            else if(command =="Delete")
            {
                <%= btnDeleteWH.ClientID %>.fireEvent('click');
            }           

        };



    function searchList() {
        <%=gridAnnualHoliday.ClientID %>.getStore().pageSize = <%=cmbPageSize.ClientID %>.getValue();
        <%=PagingToolbar1.ClientID %>.doRefresh();
    }

    var HolidayTypeChange = function()
    {
        var holidayType = <%= chkIsNationalHoldiay.ClientID %>.getValue();

        <%= cmbGender.ClientID %>.setValue("-2");
        <%= cmbGender.ClientID %>.enable();
        

        if(holidayType != null && holidayType == true)
        {
            <%= cmbGender.ClientID %>.disable();
            <%= cmbGender.ClientID %>.setValue("-1");
        }

    };

    var UptoChange = function()
    {
        <%= txtEndDate.ClientID %>.clear();
        var value = <%= chkUpto.ClientID %>.getValue();
        if(value == true)
            <%= txtEndDate.ClientID %>.enable();
        else
            <%= txtEndDate.ClientID %>.disable();
    };

    var prepareToolbarAH = function (grid, toolbar, rowIndex, record) {
    
//        if (record.data.IsDeletable > 0) {
//            toolbar.items.getAt(2).hide();
//        } 
    };

    var CommandHandlerAH = function(command, record){

            <%= hdnAnnualHolidayId.ClientID %>.setValue(record.data.ID);
            
            if(command =="Edit")
            {                
                <%= btnEditAH.ClientID %>.fireEvent('click');
            }
            else if(command =="Delete")
            {
                <%= btnDeleteAH.ClientID %>.fireEvent('click');
            }
           
        };

    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
 <script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>
   
    <ext:Hidden ID="hdnWeeklyHolidayId" runat="server" />
    <ext:Hidden ID="hdnAnnualHolidayId" runat="server" />
    <ext:LinkButton ID="btnEditWH" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnEditWH_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:LinkButton ID="btnDeleteWH" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnDeleteWH_Click">
                <EventMask ShowMask="true" />
                <Confirmation ConfirmRequest="true" Message="Do you want to delete the weekly holiday?" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:LinkButton ID="btnEditAH" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnEditAH_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:LinkButton ID="btnDeleteAH" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnDeleteAH_Click">
                <EventMask ShowMask="true" />
                <Confirmation ConfirmRequest="true" Message="Do you want to delete the annual holiday?" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Holidays
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div style="background: #C1DDF1; color: black; border: 1px solid #C1DDF1;
            padding: 5px; margin-bottom: 10px; width: 1060px;">
            Weekly Holidays</div>
        <ext:Button runat="server" ID="btnAddWeeklyHoliday" Cls="btn-sm btn-sect" Icon="Add"
            Text="<i></i>Add New Holiday">
            <DirectEvents>
                <Click OnEvent="btnAddWeeklyHoliday_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:Button>
        <br />
        <ext:GridPanel ID="gridWeeklyHolidays" runat="server" Cls="itemgrid" Width="480">
            <Store>
                <ext:Store ID="storeWeeklyHolidays" runat="server">
                    <Model>
                        <ext:Model ID="Model1" runat="server" IDProperty="ID">
                            <Fields>
                                <ext:ModelField Name="ID" Type="Int" />
                                <ext:ModelField Name="EffectiveFrom" Type="string" />
                                <ext:ModelField Name="DayofWeekName" Type="string" />
                                <ext:ModelField Name="IsFullDayValue" Type="string" />
                                <ext:ModelField Name="IsDeletable" Type="Int" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>
            <ColumnModel>
                <Columns>
                    <ext:Column ID="colDayofWeekName" Width="100" Sortable="false" MenuDisabled="true"
                        runat="server" Text="Title" Align="Left" DataIndex="DayofWeekName" />
                    <ext:Column ID="colIsFullDayValue" Width="100" Sortable="false" MenuDisabled="true"
                        runat="server" Text="Nature" Align="Left" DataIndex="IsFullDayValue" />
                    <ext:Column ID="Column1" Width="200" Sortable="false" MenuDisabled="true" runat="server"
                        Text="Effective From" Align="Left" DataIndex="EffectiveFrom" />
                    <ext:CommandColumn ID="CommandColumn1" runat="server" Width="80" Text="">
                        <Commands>
                            <ext:GridCommand CommandName="Edit" Text="<i class='fa fa-pencil'></i>">
                                <ToolTip Text="Edit" />
                            </ext:GridCommand>
                            <ext:CommandSeparator />
                            <ext:GridCommand CommandName="Delete" Text="<i class='fa fa-trash-o'></i>">
                                <ToolTip Text="Delete" />
                            </ext:GridCommand>
                        </Commands>
                        <PrepareToolbar Fn="prepareToolbar" />
                        <Listeners>
                            <Command Handler="CommandHandlerWH(command,record);" />
                        </Listeners>
                    </ext:CommandColumn>
                </Columns>
            </ColumnModel>
            <SelectionModel>
                <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" />
            </SelectionModel>
        </ext:GridPanel>
        <br />
        <br />
        <div style="background: #C1DDF1; color: black; border: 1px solid #C1DDF1;
            padding: 5px; margin-bottom: 10px; width: 1060px;">
            Annual Holidays</div>
        <table border="0">
            <tr>
                <td style="width: 200px; vertical-align: bottom;">
                    <ext:Button runat="server" ID="btnAddAnnualHoliday" Cls="btn-sm btn-sect" Text="<i></i>Add New Holiday"
                        Icon="Add">
                        <DirectEvents>
                            <Click OnEvent="btnAddAnnualHoliday_Click">
                                <EventMask ShowMask="true" />
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </td>
                <td style="width: 680px; vertical-align: bottom;">
                    <ext:Button runat="server" ID="btnImportExcel" Cls="btn-sm btn-sect" Text="<i></i>Import From Excel"
                        Icon="PageExcel" OnClientClick="HolidayImport();return false;">
                    </ext:Button>
                </td>
                <td>
                    <ext:ComboBox ID="cmbYearFilter" runat="server" ValueField="FinancialDateId" DisplayField="Name"
                        Width="180" FieldLabel="Select Year" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                        QueryMode="Local">
                        <Items>
                            <ext:ListItem Text="All" Value="-1" />
                        </Items>
                        <Store>
                            <ext:Store ID="Store7" runat="server">
                                <Model>
                                    <ext:Model ID="Model9" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Name" Type="String" />
                                            <ext:ModelField Name="FinancialDateId" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <SelectedItems>
                            <ext:ListItem Text="All" Value="-1" />
                        </SelectedItems>
                        <Listeners>
                            <Select Fn="searchList" />
                        </Listeners>
                    </ext:ComboBox>
                </td>
            </tr>
        </table>
        <br />
        

        <ext:GridPanel ID="gridAnnualHoliday" runat="server" Cls="itemgrid" Width="1060">
            <Store>
                <ext:Store ID="storeAnnualHoliday" runat="server" AutoLoad="true" OnReadData="Store_ReadData"
                    RemotePaging="true" RemoteSort="true">
                    <Proxy>
                        <ext:PageProxy />
                    </Proxy>
                    <Model>
                        <ext:Model ID="Model5" runat="server" IDProperty="ID">
                            <Fields>
                                <ext:ModelField Name="ID" Type="Int" />
                                <ext:ModelField Name="NepDate" Type="String" />
                                <ext:ModelField Name="EngDate" Type="Date" />
                                <ext:ModelField Name="Description" Type="String" />
                                <ext:ModelField Name="IsNationalHoliday" Type="Boolean" />
                                <ext:ModelField Name="BranchName" Type="String" />
                                <ext:ModelField Name="IsDeletable" Type="Int" />
                                <ext:ModelField Name="AppliesTo" Type="String" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>
            <ColumnModel>
                <Columns>
                    <ext:Column ID="colNepDate" Width="120" Sortable="false" MenuDisabled="true" runat="server"
                        Text="Nepali Date" Align="Left" DataIndex="NepDate">
                    </ext:Column>
                    <ext:DateColumn ID="DateColumn1" runat="server" Flex="1" Align="Left" Text="English Date"
                        Width="170" MenuDisabled="true" Sortable="false" Format="MMMM dd, yyyy" DataIndex="EngDate">
                    </ext:DateColumn>
                    <ext:Column ID="Column3" Width="200" Sortable="false" MenuDisabled="true"
                        runat="server" Text="Holiday Name" Align="Left" DataIndex="Description" />

                   <ext:CheckColumn ID="colIsNationalHoliday" runat="server" Sortable="false" MenuDisabled="true" Width="100"
                     Text="National" Align="Center" DataIndex="IsNationalHoliday"  ></ext:CheckColumn>

                    <ext:Column ID="Column5" Width="70" Sortable="false" MenuDisabled="true" runat="server"
                        Text="Applies To" Align="Left" DataIndex="AppliesTo" />
                    <ext:Column ID="Column6" Width="170" Sortable="false" MenuDisabled="true" runat="server"
                        Text="Ethnicity" Align="Left" DataIndex="Ethnicity" Hidden="true" />
                    <ext:Column ID="Column12" Width="340" Sortable="false" MenuDisabled="true" runat="server"
                        Text="Applicable Branches" Align="Left" DataIndex="BranchName" Wrap="true" />
                    <ext:CommandColumn ID="CommandColumn2" runat="server" Width="80" Text="">
                        <Commands>
                            <ext:GridCommand CommandName="Edit" Text="<i class='fa fa-pencil'></i>">
                                <ToolTip Text="Edit" />
                            </ext:GridCommand>
                            <ext:CommandSeparator />
                            <ext:GridCommand CommandName="Delete" Text="<i class='fa fa-trash-o'></i>">
                                <ToolTip Text="Delete" />
                            </ext:GridCommand>
                        </Commands>
                        <PrepareToolbar Fn="prepareToolbarAH" />
                        <Listeners>
                            <Command Handler="CommandHandlerAH(command,record);" />
                        </Listeners>
                    </ext:CommandColumn>
                </Columns>
            </ColumnModel>
            <SelectionModel>
                <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" />
            </SelectionModel>
            <View>
                <ext:GridView EnableTextSelection="true" />
            </View>
            <BottomBar>
                <ext:PagingToolbar Height="40" ID="PagingToolbar1" runat="server" StoreID="storeAnnualHoliday"
                    DisplayInfo="true">
                    <Items>
                        <ext:Label ID="Label1" runat="server" Text="Page size:" />
                        <ext:ToolbarSpacer ID="ToolbarSpacer1" runat="server" Width="10" />
                        <ext:ComboBox ID="cmbPageSize" runat="server" Width="80" SelectOnFocus="true" Selectable="true"
                            ValueField="Value" DisplayField="Text" ForceSelection="true" AllowBlank="false">
                            <Listeners>
                                <Select Handler="searchList()" />
                            </Listeners>
                            <Items>
                                <ext:ListItem Value="50" Text="50" />
                                <ext:ListItem Value="100" Text="100" />
                                <ext:ListItem Value="200" Text="200" />
                            </Items>
                            <SelectedItems>
                                <ext:ListItem Index="0">
                                </ext:ListItem>
                            </SelectedItems>
                        </ext:ComboBox>
                    </Items>
                </ext:PagingToolbar>
            </BottomBar>
          
        </ext:GridPanel>
    </div>
    <ext:Window ID="wWH" runat="server" Title="Weekly Holiday" Icon="None" Resizable="false"
        Width="450" Height="250" BodyPadding="5" Hidden="true" Modal="true">
        <Content>
            <table style="margin-left: 20px; margin-top: 20px;">
                <tr>
                    <td style="width: 200px;">
                        <ext:ComboBox ID="cmbDayOfWeek" runat="server" ValueField="Value" DisplayField="Text"
                            Width="180" FieldLabel="Day of the Week" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                            QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store3" runat="server">
                                    <Model>
                                        <ext:Model ID="Model3" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="Text" Type="String" />
                                                <ext:ModelField Name="Value" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator ID="rfvDayOfWeek" runat="server" ErrorMessage="Day of Week is required"
                            Display="None" ControlToValidate="cmbDayOfWeek" ValidationGroup="WeeklyHoliday"></asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <ext:ComboBox ID="cmbFullOrHalf" runat="server" ValueField="Value" DisplayField="Text"
                            Width="180" FieldLabel="Holiday Type" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                            QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store1" runat="server">
                                    <Model>
                                        <ext:Model ID="Model2" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="Text" Type="String" />
                                                <ext:ModelField Name="Value" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator ID="rfvFullOrHalf" runat="server" ErrorMessage="Holiday Type is required"
                            Display="None" ControlToValidate="cmbFullOrHalf" ValidationGroup="WeeklyHoliday"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:ComboBox ID="cmbWHEffectiveFrom" runat="server" ValueField="PayrollPeriodId"
                            DisplayField="Name" Width="180" FieldLabel="Starting from" LabelAlign="top" LabelSeparator=""
                            ForceSelection="true" QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store2" runat="server">
                                    <Model>
                                        <ext:Model ID="Model4" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="Name" Type="String" />
                                                <ext:ModelField Name="PayrollPeriodId" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Starting from is required"
                            Display="None" ControlToValidate="cmbWHEffectiveFrom" ValidationGroup="WeeklyHoliday"></asp:RequiredFieldValidator>
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
        </Content>
        <Buttons>
            <ext:Button runat="server" ID="btnSaveWH" Text="Add">
                <DirectEvents>
                    <Click OnEvent="btnSaveWH_Click">
                        <EventMask ShowMask="true" />
                    </Click>
                </DirectEvents>
                <Listeners>
                    <Click Handler="valGroup = 'WeeklyHoliday'; if(CheckValidation()) return ''; else return false;">
                    </Click>
                </Listeners>
            </ext:Button>
            <ext:Button ID="Button2" runat="server" Text="Close">
                <Listeners>
                    <Click Handler="#{wWH}.hide();">
                    </Click>
                </Listeners>
            </ext:Button>
        </Buttons>
    </ext:Window>
    <ext:Window ID="wAnnualHoliday" runat="server" Title="Annual Holiday" Icon="None"
        Resizable="false" Width="680" Height="680" BodyPadding="5" Hidden="true" Modal="true"
       >
        <Content>
            <table style="margin-left: 20px; margin-top: 0px;">
                <tr>
                    <td colspan="2" style="padding-top: 0px;">
                        <ext:TextField ID="txtHolidayName" Width="380" FieldLabel="Holiday Name" runat="server"
                            LabelAlign="Top" LabelSeparator="" />
                        <asp:RequiredFieldValidator ID="rfvHolidayName" runat="server" ErrorMessage="Holiday Name is required"
                            Display="None" ControlToValidate="txtHolidayName" ValidationGroup="AnnualHoliday"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td style="padding-top: 10px;width:220px;">
                        <ext:DateField ID="txtStartDate" runat="server" FieldLabel="Starting from" LabelAlign="Top"
                            LabelSeparator="" Width="180" >
                             <Plugins>
                                    <ext:GenericPlugin ID="HighlightDates1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                                </Plugins>
                            </ext:DateField>
                        <asp:RequiredFieldValidator ID="rfvStartDate" runat="server" ErrorMessage="Starting from is required"
                            Display="None" ControlToValidate="txtStartDate" ValidationGroup="AnnualHoliday"></asp:RequiredFieldValidator>
                    </td>
                    <td style="padding-top: 10px; float: left; margin-top: 5px;">
                        <div>
                            <ext:Checkbox ID="chkUpto" runat="server" LabelSeparator="" FieldLabel="Upto" Width="50"
                                LabelAlign="Left" LabelWidth="30">
                                <Listeners>
                                    <Change Fn="UptoChange" />
                                </Listeners>
                            </ext:Checkbox>
                        </div>
                        <br />
                        <div style="margin-top: -23px;">
                            <ext:DateField ID="txtEndDate" runat="server" FieldLabel="" LabelSeparator="" Width="180"
                                Disabled="true" >
                                 <Plugins>
                                    <ext:GenericPlugin ID="GenericPlugin1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                                </Plugins>
                                </ext:DateField>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="padding-top: 0px;" >
                        <ext:Checkbox ID="chkIsNationalHoldiay" LabelWidth="140" runat="server" LabelAlign="Left" Width="200" LabelSeparator="" FieldLabel="Is National Holiday?">
                            <Listeners>
                                <Change Fn="HolidayTypeChange" />
                            </Listeners>
                        </ext:Checkbox>
                    </td>
                    
                    <td style="padding-top: 0px;padding-bottom:20px;">
                        <ext:ComboBox ID="cmbGender"  runat="server" ValueField="Value" DisplayField="Text"
                            Width="180" FieldLabel="Applies to" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                            QueryMode="Local">
                            <Items>
                                <ext:ListItem Text="Select Applies to" Value="-2" />
                                <ext:ListItem Text="All" Value="-1" />
                                <ext:ListItem Text="Female" Value="0" />
                            </Items>
                            <SelectedItems>
                                <ext:ListItem Text="Select Applies to" Value="-2" />
                            </SelectedItems>
                        </ext:ComboBox>
                         <asp:RequiredFieldValidator InitialValue="-2" ID="RequiredFieldValidator2" runat="server"
                                ErrorMessage="Applies to is required." ControlToValidate="cmbGender" Display="None"
                                ValidationGroup="AnnualHoliday"></asp:RequiredFieldValidator>
                    </td>
               
                </tr>
                <tr>
                    <td colspan="2" style="padding-top: 0px;">
                        <ext:ItemSelector ID="selectorBranch" MultiSelect="true" ValueField="BranchId" DisplayField="Name" LabelSeparator=""
                            LabelAlign="Top" runat="server" FieldLabel="Branches" AllowBlank="false" FromTitle="Available Branch"
                            Width="600" Height="400" ToTitle="Selected Branch">
                            <Store>
                                <ext:Store ID="store8" runat="server" AutoLoad="true">
                                    <Model>
                                        <ext:Model ID="Model10" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="BranchId" Type="String" />
                                                <ext:ModelField Name="Name" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            
                        </ext:ItemSelector>
                    </td>
                </tr>
               
            </table>
        </Content>
        <Buttons>
            <ext:Button runat="server" ID="btnSaveAnnualHoliday" Text="Add">
                <DirectEvents>
                    <Click OnEvent="btnSaveAnnualHoliday_Click">
                        <EventMask ShowMask="true" />
                    </Click>
                </DirectEvents>
                <Listeners>
                    <Click Handler="valGroup = 'AnnualHoliday'; if(CheckValidation()) return ''; else return false;">
                    </Click>
                </Listeners>
            </ext:Button>
            <ext:Button ID="Button3" runat="server" Text="Close">
                <Listeners>
                    <Click Handler="#{wAnnualHoliday}.hide();">
                    </Click>
                </Listeners>
            </ext:Button>
        </Buttons>
    </ext:Window>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
