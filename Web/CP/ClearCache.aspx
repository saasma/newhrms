﻿<%@ Page Title="Clear Cache" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="ClearCache.aspx.cs" Inherits="Web.CP.ClearCache" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .divStyle
        {
            background-color: #D9E1F2;
        }
    </style>
    
    <script type="text/javascript">

   
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Clear Cache Memory
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="divStyle">
            <br />
            
            <table style="margin-left: 30px;">
                <tr>
                    <td>
                        <ext:Button runat="server" ID="btnSave" Cls="btn btn-primary" Text="<i></i>Clear">
                            <DirectEvents>
                                <Click OnEvent="btnSave_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                         
                        </ext:Button>
                    </td>
                </tr>
            </table>
            <br />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
