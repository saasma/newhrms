﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using System.Text;
using BLL.Manager;
using Utils.Helper;
using DAL;
using System.Drawing;
using BLL.Base;


namespace Web.CP
{
    public partial class ManageVoucherGroup : BasePage
    {
        TaxManager taxMgr = new TaxManager();

        List<CalcGetHeaderListResult> headerList = new List<CalcGetHeaderListResult>();

        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.AdministratorTools;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.NIBL
                || CommonManager.CompanySetting.WhichCompany==WhichCompany.Janata
                || CommonManager.CompanySetting.WhichCompany == WhichCompany.NCC)
                Response.Redirect("ManageVoucherGroupNew.aspx", true);

            if (!IsPostBack)
            {
               
                Initialise();

            }

            if (Request.Form["__EVENTTARGET"] != null && Request.Form["__EVENTTARGET"].Equals("Reload"))
                LoadGroup();
            
            JavascriptHelper.AttachPopUpCode(Page, "medicalTaxCreditPopup", "Popup/AEVoucherGroup.aspx", 500, 400);
            JavascriptHelper.AttachPopUpCode(Page, "voucherHead", "Popup/AESalaryHeadAssociation.aspx", 500, 400);
        }

        void Initialise()
        {
            if (CommonManager.Setting.AccountingVoucherEnum != null && CommonManager.Setting.AccountingVoucherEnum == (int)AccountingVoucherTypeEnum.GLOBUS)
            {
                gvwList.Columns[0].HeaderText = "Associated Account";
                gvwList.Columns[2].Visible = true;
                divCategory.Visible = true;
            }

            LoadGroup();

        }

        public string GetVoucherGroupName(object value)
        {
            if (value == null)
                return "";
            if (int.Parse(value.ToString()) == 0)
                return "";
            return VouccherManager.GetVoucher(int.Parse(value.ToString())).AccountGroupName;
        }

        public string GetVoucherCategory(object val)
        {
            if (val == null)
                return "";
            int categoryId = int.Parse(val.ToString());
            VoucherGroupCategory cat = VouccherManager.GetCategoryById(categoryId);

            if (cat == null)
                return "";

            if (cat.CategoryID == (int)GlobusVoucherCategoryIDEnum.MainVoucher)
                return "";

            return cat.Name;

        }
        public string GetType(int type)
        {
            if (type == (int)CalculationColumnType.Income
                || type == (int)CalculationColumnType.DeemedIncome
            || type == (int)CalculationColumnType.IncomeLeaveEncasement
                || type == (int)CalculationColumnType.IncomePF)
                return "Pay";

            return "Deduction";
        }

        public string GetVoucherType(object value)
        {
            if ((VoucherGroupTypeEnum)int.Parse(value.ToString()) == VoucherGroupTypeEnum.Positive)
                return "Positive(Dr)";
            if ((VoucherGroupTypeEnum)int.Parse(value.ToString()) == VoucherGroupTypeEnum.Positive)
                return "Negative(Cr)";
            return ((VoucherGroupTypeEnum)int.Parse(value.ToString())).ToString();
        }

        public string GetVoucherGroup(object value)
        {
            return ((VoucherGroupEnum)int.Parse(value.ToString())).ToString();
        }

        void LoadGroup()
        {

            gridCategory.DataSource = VouccherManager.GetVoucherGroupCategoryList();
            gridCategory.DataBind();

            gvwList.DataSource = VouccherManager.GetVoucherGroupList();
            gvwList.DataBind();
                



            headerList =   CalculationManager.GetHeaderListForVoucher();

            gridIncomeDeductionList.DataSource = VouccherManager.GetHeadList().ToList();
            gridIncomeDeductionList.DataBind();


        }

        public string GetHeaderName(object type, object value)
        {
            int typev = int.Parse(type.ToString());
            int valuev = int.Parse(value.ToString());
            foreach (CalcGetHeaderListResult item in headerList)
            {

                if (item.Type == typev && item.SourceId == valuev)
                    return item.HeaderName;
            }

            return "";
        }

        protected void btnLoad_Click(object sender, EventArgs e)
        {
            

            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage -= 1;
            LoadGroup();;
        }

       



    }
}
