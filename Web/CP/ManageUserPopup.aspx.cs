﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using DAL;
using BLL;
using Utils.Helper;
using Utils;
using Utils.Security;
using System.Web.Security;
using Utils.Web;

namespace Web.CP
{
    public partial class ManageUserPopup : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialise();
                TextBoxWatermarkExtender1.Enabled = false;
                AutoCompleteExtenderOrganization.Enabled = false;
            }
        }

        private void Initialise()
        {
            string userName = string.Empty;
            string userType = string.Empty;

            if (!string.IsNullOrEmpty(Request.QueryString["UserName"]))
                userName = Request.QueryString["UserName"];

            if (!string.IsNullOrEmpty(Request.QueryString["UserType"]))
                userType = Request.QueryString["UserType"];

            BindEmployee();
            chkIsActive.Checked = true;
            chkPayrollSignOffApproval.Checked = false;

            ListItem firstItem = ddlRoles.Items[0];
            firstItem.Selected = false;
            ddlRoles.Items.Clear();
            ddlRoles.ClearSelection();

            if (userType.ToLower() == "false")//User Type
            {
                //RequiredFieldValidator2.Enabled = false;
                rowSignOff.Visible = true;
                rowName.Visible = true;
                txtActiveDirectoryUserName.Text = "";
                txtActiveDirectoryUserName.Visible = false;
                rowAD.Visible = false;

                txtUserName.Enabled = true;

                ddlRoles.Items.Clear();

                ddlRoles.DataSource = UserManager.GetRoles((int)Role.Employee);
                ddlRoles.DataBind();
                rfvEmp.Enabled = false;
            }
            else//Employee Type
            {
                rfvEmp.Enabled = true;
                RequiredFieldValidator2.Enabled = true;
                rowSignOff.Visible = false;
                txtActiveDirectoryUserName.Visible = true;
                rowAD.Visible = true;
                rowName.Visible = false;

                txtUserName.Enabled = false;

                ddlRoles.Items.Clear();

                ListItem item = new ListItem(Role.Employee.ToString(), ((int)Role.Employee).ToString());
                ddlRoles.Items.Insert(0, item);
                ddlRoles.Items[0].Selected = true;
            }


            ddlRoles.Items.Insert(0, firstItem);

            BindDetailsByUserName(userName);
        }

        private void BindEmployee()
        {
            ddlEmployeeList.ClearSelection();
            ddlEmployeeList.Items[0].Selected = true;
            ddlEmployeeList.Enabled = true;
            EmployeeManager mgr = new EmployeeManager();
            ddlEmployeeList.DataSource = EmployeeManager.GetEmployeeListByCompany(SessionManager.CurrentCompanyId);
            ddlEmployeeList.DataBind();
        }

        private void BindDetailsByUserName(string userName)
        {
            UUser user = UserManager.GetUserByUserName(userName);

            if (user != null)
            {
                txtUserName.Text = user.UserName;
                txtName.Text = user.NameIfNotEmployee;
                txtActiveDirectoryUserName.Text = user.ActiveDirectoryName;
                txtEmail.Text = user.Email;
                chkIsActive.Checked = user.IsActive.Value;
                btnInsertUpdate.Text = Resources.Messages.Update;
                chkPayrollSignOffApproval.Checked = false;
                if (user.DocumentIsViewOnly != null)
                {
                    UIHelper.SetSelectedInDropDown(ddlDocumentPermission, user.DocumentIsViewOnly.ToString().ToLower());
                }
                if (user.AllowableIPList == null)
                    txtIPList.Text = "";
                else
                    txtIPList.Text = user.AllowableIPList;
                ddlRoles.ClearSelection();
                ListItem item = ddlRoles.Items.FindByValue(user.RoleId.ToString());
                if (item != null)
                    item.Selected = true;
                if (user.IsEmployeeType.Value)
                {
                    ddlEmployeeList.ClearSelection();
                    ListItem item1 = ddlEmployeeList.Items.FindByValue(user.EmployeeId.ToString());
                    if (item1 != null)
                        item1.Selected = true;

                    txtUserName.Enabled = true;
                    ddlEmployeeList.Enabled = false;
                }
                else
                {
                    if (user.UserMappedEmployeeId != null)
                    {
                        ddlEmployeeList.ClearSelection();
                        ListItem item1 = ddlEmployeeList.Items.FindByValue(user.UserMappedEmployeeId.ToString());
                        if (item1 != null)
                            item1.Selected = true;
                    }

                    if (user.EnablePayrollSignOff != null && user.EnablePayrollSignOff.Value)
                        chkPayrollSignOffApproval.Checked = user.EnablePayrollSignOff.Value;
                    txtUserName.Enabled = false;
                    ddlEmployeeList.Enabled = true;
                }
            }
        }


        protected void ddlEmployeeList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Request.QueryString["UserType"] == "False")
                return;

            if (ddlEmployeeList.SelectedValue != "-1")
            {
                UUser user = UserManager.GetUserByEmployee(int.Parse(ddlEmployeeList.SelectedValue));
                if (user != null)
                {
                    txtUserName.Text = user.UserName;
                    txtName.Text = user.NameIfNotEmployee;
                    txtEmail.Text = user.Email;
                    chkIsActive.Checked = user.IsActive.Value;
                    ddlRoles.ClearSelection();
                    ListItem item = ddlRoles.Items.FindByValue(user.RoleId.ToString());
                    if (item != null)
                        item.Selected = true;
                    txtUserName.Enabled = false;

                    btnInsertUpdate.Text = Resources.Messages.Update;
                }
                else
                {
                    EEmployee employee = EmployeeManager.GetEmployeeById(int.Parse(ddlEmployeeList.SelectedValue));
                    txtEmail.Text = employee.EAddresses[0].CIEmail;

                    txtUserName.Enabled = true;
                    btnInsertUpdate.Text = Resources.Messages.Save;
                }

            }
        }

        protected void btnInsertUpdate_Click(object sender, EventArgs e)
        {
            if (this.IsValid)
            {
                bool userType = Convert.ToBoolean(Request.QueryString["UserType"].ToString());

                if (Request.QueryString["UserName"] == null)
                {
                    UUser user = new UUser();
                    user.UserName = txtUserName.Text.Trim();
                    user.NameIfNotEmployee = txtName.Text.Trim();
                    user.ActiveDirectoryName = txtActiveDirectoryUserName.Text.Trim();
                    user.Email = txtEmail.Text.Trim();
                    user.RoleId = int.Parse(ddlRoles.SelectedValue);
                    user.CompanyId = SessionManager.CurrentCompanyId;
                    user.AllowableIPList = txtIPList.Text.Trim();
                    user.IsEmployeeType = userType;
                    if (ddlDocumentPermission.SelectedValue != "-1")
                        user.DocumentIsViewOnly = bool.Parse(ddlDocumentPermission.SelectedValue);
                    if (user.IsEmployeeType.Value)
                    {
                        user.EmployeeId = int.Parse(ddlEmployeeList.SelectedValue);
                    }
                    else
                    {
                        user.UserMappedEmployeeId = int.Parse(ddlEmployeeList.SelectedValue);
                        user.EnablePayrollSignOff = chkPayrollSignOffApproval.Checked;
                    }
                    user.IsActive = chkIsActive.Checked;


                    if (UserManager.IsUserNameAlreadyExists(user.UserName))
                    {
                        msgWarning.InnerHtml = Resources.Messages.UserNameAlreadExistsMsg;
                        msgWarning.Hide = false;
                    }
                    else
                    {
                        UserManager.CreateUser(user);
                        JavascriptHelper.DisplayClientMsg(Resources.Messages.UserCreatedMsg, Page, "closePopup();");                       
                    }
                }
                else
                {
                    UUser user = new UUser();
                    user.UserName = Request.QueryString["UserName"].ToString();
                    user.NewUserName = txtUserName.Text.Trim();
                    user.NameIfNotEmployee = txtName.Text.Trim();
                    user.Email = txtEmail.Text.Trim();
                    user.RoleId = int.Parse(ddlRoles.SelectedValue);
                    user.IsActive = chkIsActive.Checked;
                    user.ActiveDirectoryName = txtActiveDirectoryUserName.Text.Trim();
                    user.AllowableIPList = txtIPList.Text.Trim();
                    //user.UserMappedEmployeeId = int.Parse(ddlEmployeeList.SelectedValue);
                    user.EnablePayrollSignOff = chkPayrollSignOffApproval.Checked;
                    if (ddlDocumentPermission.SelectedValue != "-1")
                        user.DocumentIsViewOnly = bool.Parse(ddlDocumentPermission.SelectedValue);

                    if (userType)
                    {
                    }
                    else
                    {
                        user.UserMappedEmployeeId = int.Parse(ddlEmployeeList.SelectedValue);
                        if (user.UserMappedEmployeeId != null && user.UserMappedEmployeeId == -1)
                            user.UserMappedEmployeeId = null;
                        //user.EnablePayrollSignOff = chkPayrollSignOffApproval.Checked;
                    }

                    //if (UserManager.IsRoleNameAlreadyExists(role.Name, role.RoleId))
                    //{
                    //    msgWarning.InnerHtml = Resources.Messages.RoleAlreadyExistsMsg;
                    //    msgWarning.Hide = false;
                    //}
                    //else
                    //{

                    if (UserManager.UpdateUser(user))
                    {
                        JavascriptHelper.DisplayClientMsg(Resources.Messages.UserUpdatedMsg, Page, "closePopup();");
                    }
                    else
                    {
                        msgWarning.InnerHtml = "Username already exists.";
                        msgWarning.Hide = false;
                    }
                    //}
                }
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            txtUserName.Enabled = ddlEmployeeList.Enabled = true;
            ddlEmployeeList.ClearSelection();
            ddlEmployeeList.Items[0].Selected = true;
            Clear();
        }

        void Clear()
        {
            txtUserName.Enabled = true;
            txtActiveDirectoryUserName.Text = "";
            ddlEmployeeList.Enabled = true;
            txtUserName.Text = "";
            txtName.Text = "";
            txtEmail.Text = "";
            chkIsActive.Checked = true;
            chkPayrollSignOffApproval.Checked = false;
            btnInsertUpdate.Text = Resources.Messages.Save;
            //this.CustomId = 0;
        }


    }
}