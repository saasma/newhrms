﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using Utils.Helper;
using System.IO;
using Utils;
using Utils.Security;
using Utils.Calendar;
using Web.Helper;
using System.Collections;
using Utils.Web;
using Utils.Base;

namespace Web
{


    public partial class EmployeeMaster : BasePage
    {


        protected void Page_Init(object sender, EventArgs e)
        {
            Initialize();
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            AddColumns();
            //if (!IsPostBack)

            if (!Page.IsPostBack)
            {
                //Initialize();

            }

            BindEmployees();
            //if (!IsPostBack)
            {


                //gvEmployeeIncome.Columns[9].HeaderText = CommonManager.GetHandicappedName;


            }
        }


        protected void btnPrevious_Click()
        {
            pagingCtl.CurrentPage -= 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage -= 1;
            BindEmployees();
        }

        protected void ChangePageNumber()
        {
            BindEmployees();
        }

        protected void btnNext_Click()
        {
            pagingCtl.CurrentPage += 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage += 1;
            BindEmployees();
        }

        protected void ddlRecords_SelectedIndexChanged()
        {
            //_tempCurrentPage = 1;
            pagingCtl.CurrentPage = 1;
            BindEmployees();
        }


        private void Initialize()
        {
            List<Branch> list = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);

            ddlBranch.DataSource
                = list;
            ddlBranch.DataBind();

            ddlDepartment.DataSource
                = DepartmentManager.GetAllDepartments();

            ddlDepartment.DataBind();

            List<KeyValue> listStatus = new JobStatus().GetMembers();
            listStatus.RemoveAt(0);
            ddlStatus.DataSource = listStatus;
            ddlStatus.DataBind();
        }

        protected Field OnCreateFilterableField(object sender, ColumnBase column, Field defaultField)
        {
            ((TextField)defaultField).Icon = Icon.Magnifier;

            return defaultField;
        }

        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
        }

        private void AddColumns()
        {
            List<GetEmployeeMasterOtherHeadersResult> headers = EmployeeManager.GetEmployeeMasterHeader();

            for (int i = 0; i < headers.Count; i++)
            {
                TemplateField field = new TemplateField();
                field.ItemTemplate = new EmployeeMasterGridViewTemplate(DataControlRowType.DataRow, headers[i]);
                field.HeaderTemplate = new EmployeeMasterGridViewTemplate(DataControlRowType.Header, headers[i]);
                gvEmployeeIncome.Columns.Insert(gvEmployeeIncome.Columns.Count - 2, field);
            }
        }



        void BindEmployees()
        {
            List<GetEmployeeMasterResult> list =
               EmployeeManager.GetEmployeeMaster(pagingCtl.CurrentPage - 1, int.Parse(pagingCtl.DDLRecords.SelectedValue), "", int.Parse(ddlBranch.SelectedValue), int.Parse(ddlDepartment.SelectedValue), int.Parse(ddlStatus.SelectedValue)
               , txtEmpSearchText.Text.Trim());
            gvEmployeeIncome.DataSource = list;
            gvEmployeeIncome.DataBind();


            int total = 0;

            if (list.Count > 0)
                total = list[0].TotalRows.Value;

            pagingCtl.UpdatePagingBar(total);

        }


        protected void btnUpdate_Click(object sender, EventArgs e)
        {

            List<GetEmployeeMasterResult> list =
             EmployeeManager.GetEmployeeMaster(0, int.MaxValue, "", int.Parse(ddlBranch.SelectedValue), int.Parse(ddlDepartment.SelectedValue), int.Parse(ddlStatus.SelectedValue)
             , txtEmpSearchText.Text.Trim());
            gvEmployeeIncome.DataSource = list;
            gvEmployeeIncome.DataBind();


            GridViewExportUtil.Export("EmployeeMaster.xls", gvEmployeeIncome);

        }

        protected void btnLoad_Click(object sender, EventArgs e)
        {
            BindEmployees();
        }

    }


    public class EmployeeMasterGridViewTemplate : ITemplate
    {
        private GetEmployeeMasterOtherHeadersResult header;
        private DataControlRowType rowType;
        private string list = "";

        public EmployeeMasterGridViewTemplate(DataControlRowType rowType, GetEmployeeMasterOtherHeadersResult header)
        {
            this.header = header;
            this.rowType = rowType;
        }



        public void InstantiateIn(Control container)
        {
            switch (rowType)
            {
                case DataControlRowType.Header:

                    Literal lblHeader = new Literal();
                    lblHeader.Text = header.HeaderName;


                    container.Controls.Add(lblHeader);

                    break;

                case DataControlRowType.DataRow:

                    Literal lbl = new Literal();
                    lbl.DataBinding += new EventHandler(lbl_Binding);
                    container.Controls.Add(lbl);

                    break;
            }
        }


        public void lbl_Binding(object sender, EventArgs e)
        {
            GridViewRow row = ((GridViewRow)(((Control)(sender)).NamingContainer));

            GetEmployeeMasterResult dataSource = (GetEmployeeMasterResult)row.DataItem;
            Literal lbl = (Literal)sender;


            string value = dataSource.GetValue(header);

            if (!string.IsNullOrEmpty(value) && (value == "Y" || value == "Ye"))
                lbl.Text = "Yes";
            else if (!string.IsNullOrEmpty(value) && !value.Contains("%") && value != "Yes")
                lbl.Text = BaseHelper.GetCurrency(value, SessionManager.DecimalPlaces);
            else
                lbl.Text = value;

        }


    }

}
