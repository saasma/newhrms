﻿<%@ Page Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" Title="Branch Transfer Details" EnableEventValidation="false"
    CodeBehind="BranchTransferDetails.aspx.cs" Inherits="Web.CP.BranchTransferDetails" %>

<%@ Register Src="~/Controls/ContentHeader.ascx" TagName="ContentHeader" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .GridRowHover
        {
            cursor: pointer;
        }
        .empName
        {
            font-size: 20px;
            color: #439EC6;
            font-weight: bold;
        }
        .titleDesign
        {
            font-size: 15px;
            color: #000000;
            text-decoration: underline;
            padding-top: 10px;
        }
        
        .items span
        {
            display: block;
            padding-top: 3px;
            font-weight: bold;
        }
        .selected td
        {
            background-color: #E0EDF9 !important;
        }
        form strong
        {
            display: block !important;
            padding-bottom: 3px;
        }
        .fieldTable > tbody > tr > td
        {
            padding-left: 15px !important;
            padding-top: 15px;
        }
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
    </style>
    <script type="text/javascript">
        function popupUpdateLeaveCall(LeaveId) {
            var ret = popup('Id=' + LeaveId);


            return false;
        }
    </script>
    <link href="../css/core.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <uc1:ContentHeader Id="ContentHeader1" runat="server" />
    <div class="contentArea">
        <h4>
            Branch Transfer Details</h4>
        <div class="attribute">
            <div class="left">
                <strong>Branch Filter</strong>
                <asp:DropDownList Width="200px" ID="ddlFilterByBranch" runat="server" DataValueField="BranchId"
                    DataTextField="Name" AppendDataBoundItems="true" OnSelectedIndexChanged="LoadEmployees"
                    AutoPostBack="true">
                    <asp:ListItem Value="-1">--Select Branch--</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="left" style="padding-left: 10px;">
                <strong>Employee</strong>
                <asp:DropDownList Width="200px" ID="ddlEmployeeList" runat="server" DataValueField="EmployeeId"
                    DataTextField="Name" AutoPostBack="true" OnSelectedIndexChanged="LoadEmployeeDetails">
                    <asp:ListItem Value="-1">--Select Employee--</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ValidationGroup="AEEmployee" ID="valEmployeeReq" ControlToValidate="ddlEmployeeList"
                    InitialValue="-1" Display="None" runat="server" ErrorMessage="Please select an Employee."></asp:RequiredFieldValidator>
            </div>
        </div>
        <div style="clear: both">
        </div>
        <uc2:MsgCtl ID="divMsgCtl" Width="900px" EnableViewState="false" Hide="true" runat="server" />
        <uc2:WarningCtl ID="divWarningMsg" Width="900px" EnableViewState="false" Hide="true"
            runat="server" />
        <div>
            <asp:GridView ID="gvw" Width="100%" DataKeyNames="BranchDepartmentId" PagerStyle-HorizontalAlign="Center"
                PagerStyle-CssClass="defaultPagingBar" AllowPaging="true" runat="server" AutoGenerateColumns="False"
                CellPadding="4" GridLines="None" PageSize="100" OnSelectedIndexChanged="gvw_SelectedIndexChanged">
                <Columns>
                    <asp:TemplateField HeaderText="Letter No" HeaderStyle-Width="80px" HeaderStyle-HorizontalAlign="Left"
                        ItemStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <%# Eval("LetterNumber")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Letter Date" HeaderStyle-Width="80px" HeaderStyle-HorizontalAlign="Left"
                        ItemStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <%# Eval("LetterDate")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="From Branch" HeaderStyle-Width="150px" HeaderStyle-HorizontalAlign="Left"
                        ItemStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <%# Eval("FromBranch")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="To Branch" HeaderStyle-Width="150px" HeaderStyle-HorizontalAlign="Left"
                        ItemStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <%# Eval("Branch")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Department" HeaderStyle-Width="150px" HeaderStyle-HorizontalAlign="Left"
                        ItemStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <%# Eval("Department")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="FromDate" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left"
                        HeaderText="Applicable Date" />
                    <asp:TemplateField HeaderText="Status" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left"
                        ItemStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <%# GetStatus( Eval("Status") )%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Attendance Date" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left"
                        ItemStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <%# Eval("AttendanceDate")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="" HeaderStyle-Width="90px" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <a href="javascript:void(0)" onclick='<%# "popupUpdateLeaveCall(" +  Eval("BranchDepartmentId") + ");return false;" %>'
                                style="width:80px;display:block;" id="ImageButton12" />Review Team</a>
                        </ItemTemplate>
                        <HeaderStyle Width="30px"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Edit" HeaderStyle-Width="100px" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:ImageButton Visible='<%# IsEditable(Container.DataItemIndex) %>' ID="ImageButton1"
                                runat="server" CommandName="Select" ImageUrl="~/images/edit.gif" />
                        </ItemTemplate>
                        <HeaderStyle Width="30px"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:TemplateField>
                    <asp:TemplateField></asp:TemplateField>
                </Columns>
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                <SelectedRowStyle CssClass="selected" />
                <PagerStyle HorizontalAlign="Center" CssClass="defaultPagingBar"></PagerStyle>
                <EmptyDataTemplate>
                    <b>No Change history. </b>
                </EmptyDataTemplate>
            </asp:GridView>
        </div>
        <div class="widget" style="margin-top: 5px" runat="server" id="block" visible="false">
            <div class="widget-body">
                <table style='clear: both; padding: 10px; margin-top: 10px;'>
                    <tr>
                        <td valign="top" style="padding-left: 10px;">
                            <asp:Image ID="image" runat="server" ImageUrl="~/images/sample.jpg" Width="150px"
                                Height="150px" />
                        </td>
                        <td valign="top" style="padding-left: 50px" class="items">
                            <asp:Label ID="lblName" CssClass="empName" runat="server" />
                            <asp:Label ID="lblWorkingIn" runat="server" Text="Working In" class="titleDesign" />
                            <asp:Label ID="lblBranch" runat="server" />
                            <asp:Label ID="lblDepartment" runat="server" />
                            <asp:Label ID="lblSince" runat="server" />
                            <asp:Label ID="lblTime" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" valign="top" style="padding-left: 10px" class="items">
                            <asp:Label ID="Label1" runat="server" Text="Transfer Details" class="titleDesign" />
                            <table class="fieldTable firsttdskip" style="margin-left: -10px">
                                <tr>
                                    <td>
                                        <strong>Letter Number</strong>
                                        <asp:TextBox ID="txtLetterNumber" Width="200px" runat="server" />
                                    </td>
                                    <td>
                                        <strong style="padding-bottom: 0px; margin-top: -3px;">Letter Date</strong>
                                        <%-- <My:Calendar Id="calLetterDate" runat="server" />--%>
                                        <pr:CalendarExtControl Width="200px" ID="calLetterDate" runat="server" LabelSeparator="" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="calLetterDate"
                                            Display="None" ErrorMessage="Letter date is required." ValidationGroup="AEEmployee"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>Transferred Branch *</strong>
                                        <asp:DropDownList Width="200px" ID="ddlTransferToBranch" runat="server" DataValueField="BranchId"
                                            DataTextField="Name" OnSelectedIndexChanged="LoadDepartments" AppendDataBoundItems="true"
                                            AutoPostBack="true">
                                            <asp:ListItem Value="-1" Text=""></asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlTransferToBranch"
                                            Display="None" ErrorMessage="Branch is required." InitialValue="-1" ValidationGroup="AEEmployee"></asp:RequiredFieldValidator>
                                    </td>
                                    <td>
                                        <strong>Department *</strong>
                                        <asp:DropDownList Width="200px" ID="ddlTransferToDepartment" runat="server" DataValueField="DepartmentId"
                                            DataTextField="Name" OnSelectedIndexChanged="LoadSubDepartments" AutoPostBack="true">
                                            <asp:ListItem Value="-1" Text=""></asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlTransferToDepartment"
                                            Display="None" ErrorMessage="Department is required." InitialValue="-1" ValidationGroup="AEEmployee"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>Sub Department</strong>
                                        <asp:DropDownList Width="200px" ID="ddlSubDepartment" runat="server" DataValueField="SubDepartmentId"
                                            DataTextField="Name">
                                            <asp:ListItem Value="-1" Text=""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <strong>Special Responsibility</strong>
                                        <asp:TextBox ID="txtSpecialResp" Width="200px" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>Departure Date</strong>
                                        <%--  <My:Calendar Id="calDepartureDate" runat="server" />--%>
                                        <pr:CalendarExtControl Width="200px" ID="calDepartureDate" runat="server" LabelSeparator="" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="calDepartureDate"
                                            Display="None" ErrorMessage="Departure date is required." ValidationGroup="AEEmployee"></asp:RequiredFieldValidator>
                                    </td>
                                    <td>
                                        <strong>Applicable Date *</strong>
                                        <%-- <My:Calendar Id="calFromDate" runat="server" />--%>
                                        <pr:CalendarExtControl Width="200px" ID="calFromDate" runat="server" LabelSeparator="" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="calFromDate"
                                            Display="None" ErrorMessage="Applicable date is required." ValidationGroup="AEEmployee"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <strong>Note</strong>
                                        <asp:TextBox ID="txtNote" TextMode="MultiLine" runat="server" Width="420" Height="50" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <div class="buttonsDiv" style="text-align: left; clear: both" runat="server" id="buttonsBar">
                    &nbsp; &nbsp;
                    <asp:Button ID="btnDelete" CssClass="cancel" CausesValidation="false" runat="server"
                        Text="Delete" OnClick="btnDelete_Click" Style='display: none' />
                    <asp:Button ID="btnSave" CssClass="save" runat="server" Text="Save" OnClientClick="valGroup='AEEmployee';return CheckValidation()"
                        OnClick="btnSave_Click" />
                    <asp:Button ID="btnCancel" CssClass="cancel" CausesValidation="false" runat="server"
                        Text="Cancel" OnClick="btnCancel_Click" />
                </div>
                <div style="clear: both" />
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="pageScriptsContent" runat="server">
    <script type="text/javascript">
    </script>
</asp:Content>
