﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Manager;
using Utils.Helper;
using BLL;
using DAL;
using Utils.Web;
using BLL.Base;

namespace Web.CP
{
    public partial class ManageCITIncomes : BasePage
    {

        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.AdministratorTools;
            }
        }     


        CommonManager commonMgr = new CommonManager();
        PayManager payMgr = new PayManager();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialise();
            }
            if (Request.Form["__EVENTTARGET"] != null && Request.Form["__EVENTTARGET"].Equals("Reload"))
            {
                LoadGratuity();               
            }

            
        }

        void Initialise()
        {
            IncomeList();
            LoadGratuity();
        }
        void LoadGratuity()
        {
          
        }
        public string GetApplicableTo(object val)
        {
            return Util.GetConstantResource(val.ToString());
        }

     

        void IncomeList()
        {
            List<PIncome> list = PayManager.GetIncomeListByCompanyForCIT(SessionManager.CurrentCompanyId); ;

            list.Add(new PIncome { IncomeId = 0, Title = "PF" });
            // no pf in cit income for NIBL
            //if (CommonManager.CompanySetting.WhichCompany == WhichCompany.NIBL)
            //{
            //    foreach (PIncome inc in list)
            //    {
            //        if (inc.TitleWithPF.Contains("+ PF"))
            //        {
            //            inc.NoPFIncome = true;
            //        }
            //    }
            //}


            chkListSalaries.DataSource = list;                
            chkListSalaries.DataBind();




            foreach (CITIncome income in commonMgr.GetCITIncomeListForCompany(
                SessionManager.CurrentCompanyId))
            {
                ListItem item = chkListSalaries.Items.FindByValue(income.IncomeId.ToString());
                if (item != null)
                    item.Selected = true;
            }

            bool allChecked = true;
            foreach (ListItem item in chkListSalaries.Items)
            {
                item.Attributes.Add("onclick", string.Format("stateChanged(this,{0})", item.Value));
                if (item.Selected == false)
                    allChecked = false;
            }

            chkAll.Checked = allChecked;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

            string str="";
            List<int> incomeIDS = new List<int>();
            foreach(ListItem item in chkListSalaries.Items)
            {
                if (item.Selected)
                {
                    if (str == "")
                        str += item.Value;
                    else
                        str += "," + item.Value;

                    incomeIDS.Add(int.Parse(item.Value));
                }
            }

            CommonManager.DeleteAndSaveAllCITIncomes(incomeIDS);

            divMsgCtl.InnerHtml = "Information saved.";
            divMsgCtl.Hide = false;

        }

        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            chkAll.Enabled = true;
            chkListSalaries.Enabled = true;
            btnSave.Visible = true;
            section.Visible = true;
            //LoadDepartments();
            //ClearDepartmentFields();
            //details.Visible = true;
            //txtDepartmentName.Focus();
        }
    }
}
