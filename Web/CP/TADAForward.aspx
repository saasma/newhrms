<%@ Page Title="TADA List" Language="C#" MasterPageFile="~/Master/HROld.Master" AutoEventWireup="true"
    CodeBehind="TADAForward.aspx.cs" Inherits="Web.CP.TADAForward" %>

<%@ Import Namespace="BLL.Manager" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../../Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        var loadingButtonWhenSubmitting = false;
        var skipLoadingCheck = true;
        function addSkillSetToEmployeePopup(requestid) {
            var theText = requestid.getAttribute("value");
            positionHistoryPopup("isPopup=true&reqid=" + theText);
        }

        //ctl00_mainContent_gvw_ctl02_btnAction var theText = $(requestid).attributes.value.nodeValue();


        function selectDeselectAll(chk) {
            $('input[type=checkbox]').each(
                    function (index) {
                        if (this.id.indexOf('chkDelete') > 0)
                            this.checked = chk.checked;
                    }
                );
        }
       
    </script>
    <style type="text/css">
        .tableLightColor a:hover
        {
            color: #048FC2;
            text-decoration: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    TADA List
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="attribute" style="padding:10px">
            <table>
                <tr>
                    <td>
                        Month
                        <asp:CheckBox ID="chkHasDate" runat="server" />
                    </td>
                    <td>
                        Status
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td style="width: 150px">
                        <My:Calendar Id="calMonth" IsSkipDay="true" runat="server" />
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlStatus" runat="server">
                            <asp:ListItem Text="All" Value="-2" />
                            <asp:ListItem Value="-1">Saved</asp:ListItem>
                            <asp:ListItem Value="0">Pending</asp:ListItem>
                            <asp:ListItem Value="1">Recommended</asp:ListItem>
                            <asp:ListItem Value="2" Selected="True">Approved</asp:ListItem>
                            <asp:ListItem Value="3">Rejected</asp:ListItem>
                            <asp:ListItem Value="4">Forwarded</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="padding-left: 5px">
                        <asp:TextBox Width="160px" ID="txtEmpSearchText" runat="server"></asp:TextBox>
                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtEmpSearchText"
                            WatermarkText="Employee Name" WatermarkCssClass="searchBoxText" />
                        <cc1:AutoCompleteExtender EnableCaching="true" ID="AutoCompleteExtenderOrganization"
                            runat="server" MinimumPrefixLength="2" ServiceMethod="GetEmployeeNames" ServicePath="~/PayrollService.asmx"
                            TargetControlID="txtEmpSearchText" CompletionSetCount="10" CompletionInterval="250"
                            CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList"
                            CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
                        </cc1:AutoCompleteExtender>
                    </td>
                    <td valign="top">
                        <asp:Button ID="btnLoad" CssClass="btn btn-default btn-sm btn-sect btnLeftMargin" OnClientClick="valGroup='AEEmployee';return CheckValidation();"
                            OnClick="btnLoad_Click" runat="server" Text="Load" />
                    </td>
                </tr>
            </table>
        </div>
        <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" Width="390px" runat="server" />
        <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
        <div class="clear">
            <cc2:EmptyDisplayGridView CssClass="table table-primary mb30 table-bordered table-hover" UseAccessibleHeader="true" ShowHeaderWhenEmpty="True"
                ID="gvw" runat="server" DataKeyNames="" AutoGenerateColumns="False" CellPadding="4"
                GridLines="None" ShowFooterWhenEmpty="False" OnRowCreated="gvwEmployees_RowCreated">
                <Columns>
                    <asp:BoundField DataField="EmployeeName" HeaderStyle-Width="110px" HeaderStyle-HorizontalAlign="Left"
                        HeaderText="Employee"></asp:BoundField>
                    <asp:BoundField DataField="MonthText" HeaderStyle-Width="110px" HeaderStyle-HorizontalAlign="Left"
                        HeaderText="Month"></asp:BoundField>
                    <%--<asp:BoundField DataField="Name" HeaderStyle-Width="180px" HeaderStyle-HorizontalAlign="Left" HeaderText="Name"></asp:BoundField>--%>
                    <asp:BoundField DataField="RequestedTotal" DataFormatString="{0:N2}" HeaderStyle-HorizontalAlign="Right"
                        ItemStyle-HorizontalAlign="Right" HeaderStyle-Width="120px" HeaderText="Claimed Amount">
                    </asp:BoundField>
                    <asp:BoundField DataField="Total" DataFormatString="{0:N2}" HeaderStyle-HorizontalAlign="Right"
                        ItemStyle-HorizontalAlign="Right" HeaderStyle-Width="120px" HeaderText="Approved Amount">
                    </asp:BoundField>
                    <asp:BoundField DataField="StatusText" HeaderStyle-Width="120px" HeaderStyle-HorizontalAlign="Left"
                        HeaderText="Status"></asp:BoundField>
                    <asp:TemplateField HeaderStyle-Width="80px" HeaderStyle-HorizontalAlign="Left" HeaderText="">
                        <ItemTemplate>
                            <asp:HyperLink NavigateUrl='<%# "TADAForwardDetails.aspx?ID=" +  Eval("RequestID")%>'
                                runat="server" Text="View" ID="btnAction">
                            </asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                <EmptyDataTemplate>
                    <b>No list. </b>
                </EmptyDataTemplate>
            </cc2:EmptyDisplayGridView>
        </div>
        <div class="buttonsDiv">
            <asp:Button ID="btnExport" CssClass="btn btn-info btn-sm" OnClick="btnExport_Click" Visible="true"
                CausesValidation="false" runat="server" Text="Export" />
        </div>
    </div>
</asp:Content>
