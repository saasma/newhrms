﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Manager;
using Utils.Helper;
using BLL;
using DAL;
using Utils.Web;
using BLL.Base;
using BLL.BO;
using Bll;
using Web.Helper;

namespace Web.CP
{
    public partial class OvertimeDetails : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.AdministratorTools;
            }
        }

        CommonManager commonMgr = new CommonManager();
        PayManager payMgr = new PayManager();

        #region "Control state related"
        private string _sortBy = "EIN";
        private SortDirection _sortDirection = SortDirection.Ascending;

        protected override void OnInit(EventArgs e)
        {
            Page.RegisterRequiresControlState(this);
            base.OnInit(e);
        }
        protected override void LoadControlState(object savedState)
        {
            object[] rgState = (object[])savedState;
            base.LoadControlState(rgState[0]);
            _sortBy = rgState[1].ToString();
            _sortDirection = (SortDirection)rgState[2];
            //_tempCurrentPage = (int)rgState[2];

        }
        protected override object SaveControlState()
        {
            object[] rgState = new object[3];
            rgState[0] = base.SaveControlState();
            rgState[1] = _sortBy;
            rgState[2] = _sortDirection;
            // rgState[2] = _tempCurrentPage;
            return rgState;
        }
        #endregion

        int overtimeId = 0;

        protected void btnPostToAddOn_Click(object sender, EventArgs e)
        {
            Status status = CommonManager.PostOvertimeToAddOn(overtimeId);
            if (status.IsSuccess)
            {
                divMsgCtl.InnerHtml = "OT amount posted to Add On.";
                divMsgCtl.Hide = false;
            }
            else
            {
                divWarningMsg.InnerHtml = status.ErrorMessage;
                divWarningMsg.Hide = false;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            overtimeId = int.Parse(Request.QueryString["id"]);

           

            if (!IsPostBack)
            {
                Initialise();
            }
            if (Request.Form["__EVENTTARGET"] != null && Request.Form["__EVENTTARGET"].Equals("Reload"))
            {
                LoadOvertime(overtimeId);        
            }
           
           JavascriptHelper.AttachNonDialogPopUpCode(Page, "importPopup", "../ExcelWindow/OTTotalHourExcel.aspx", 450, 500);

           Page.ClientScript.RegisterClientScriptBlock(GetType(), "sdfsdf", "overtimeId = " + overtimeId + ";", true);

           
        }

        public void LoadDesignation()
        {
            List<TextValue> list = new List<TextValue>();
            
            list = CommonManager.GetOvertimeDesignationList(overtimeId);

            list.Insert(0, new TextValue { ID = -1, Text = "--Select Designation--" });

            ddlDesignation.DataSource = list;
            ddlDesignation.DataBind();
        }

        void Initialise()
        {
            details.NavigateUrl += overtimeId;

            OvertimePeriod overtime = commonMgr.GetOvertimePeriod(overtimeId);

            title.InnerHtml += overtime.Name + " for " + FormatDate(overtime.StartDate) + " to " +
                FormatDate(overtime.EndDate);

            ddlBranch.DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            ddlBranch.DataBind();

            LoadDesignation();
            IncomeList();
            LoadGratuity();

            LoadOvertime(overtimeId);
        }
        void LoadGratuity()
        {
            

           
        }

        public string GetYear(object yearid)
        {
            FinancialDate year = CommonManager.GetFiancialDateById(int.Parse(yearid.ToString()));
            year.SetName(IsEnglish);
            return year.Name;
        }

        public string GetApplicableTo(object val)
        {
           
            return "";
        }

        public string GetHour(object value)
        {
            if (value == null)
                return "";

            double hour = TimeSpan.FromMinutes(Convert.ToDouble(value)).TotalHours;

            return hour.ToString("N2");
        
        }

        protected void gvwClass_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int GratuityClassId = (int)-1 ;

            if (GratuityClassId != 0)
            {
                bool deleted = CommonManager.DeleteGratuityClass(GratuityClassId);

                if (deleted)
                {

                    divMsgCtl.InnerHtml = "Class deleted.";
                    divMsgCtl.Hide = false;
                }
                else
                {
                    divMsgCtl.InnerHtml = "Class is being used, can not be deleted.";
                    divMsgCtl.Hide = false;
                }

            }
            LoadGratuity();
        }

        protected void gvwDepartments_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int GratuityRuleId = -1;

            if (GratuityRuleId != 0)
            {
                CommonManager.DeleteGratuityRule(GratuityRuleId);

                divMsgCtl.InnerHtml = Resources.Messages.GratutiyRuleDeletedMsg;
                divMsgCtl.Hide = false;
                
                
            }
            LoadGratuity();
        }

        void IncomeList()
        {
          
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

           
        }

        public string GetEligibilityType(object type)
        {
            int val = int.Parse(type.ToString());

            if (val == 0)
                return "Not Eligible";
            if (val == 1)
                return BonusEligiblity.Full.ToString(); ;

            return BonusEligiblity.Proportionate.ToString();
        }

        protected void btnAddNew_Click(object sender, EventArgs e)
        {
           
        }


        public void btnTab_click(object sender, EventArgs e)
        {
            LoadOvertime(overtimeId);
        }

        void LoadOvertime(int bonusId)
        {
            hdnBonusId.Value = bonusId.ToString();


            int eligiblity = -1;

           

            int totalRecords = 0;

            List<GetGeneratedOvertimeListResult> list = CommonManager.GetOvertimeList(overtimeId
                , pagingCtl.CurrentPage - 1, int.Parse(pagingCtl.DDLRecords.SelectedValue), ref totalRecords,txtEmpSearchText.Text.Trim()
                , eligiblity,int.Parse(ddlDesignation.SelectedValue),int.Parse(ddlBranch.SelectedValue));

            gvwEmployees.DataSource = list;
            gvwEmployees.DataBind();

              if (totalRecords == 0)
                pagingCtl.Visible = false;
            else
                pagingCtl.Visible = true;
            pagingCtl.UpdatePagingBar(totalRecords);

            //int totalRecords = 0;
            //gvwList.DataSource = ProjectManager.GetProjectList(
            //    GetEngDate(calFilterFrom.SelectedDate.ToString()), GetEngDate(calFilterTo.SelectedDate.ToString()),
            //    chkAllProjects.Checked, txtSearch.Text.Trim(), pagintCtl.CurrentPage, int.Parse(pagintCtl.DDLRecords.SelectedValue), ref totalRecords);


            //gvwList.DataBind();



        }

      

        protected void gvwEmployees_Sorting(object sender, GridViewSortEventArgs e)
        {

            if (e.SortExpression != _sortBy)
            {
                e.SortDirection = SortDirection.Ascending;
                _sortDirection = e.SortDirection;
            }
            else if (_sortDirection == SortDirection.Ascending)
            {
                e.SortDirection = SortDirection.Descending;
            }
            else if (_sortDirection == SortDirection.Descending)
            {
                e.SortDirection = SortDirection.Ascending;
            }

            _sortDirection = e.SortDirection;

            _sortBy = e.SortExpression;


            e.Cancel = true;

            if (!string.IsNullOrEmpty(hdnBonusId.Value))
                //pagintCtl.CurrentPage = 1;
                //_tempCurrentPage = 1;
                LoadOvertime(int.Parse(hdnBonusId.Value));
        }


        protected void btnPrevious_Click()
        {
            pagingCtl.CurrentPage -= 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage -= 1;
            LoadOvertime(overtimeId);
        }

        protected void btnNext_Click()
        {
            pagingCtl.CurrentPage += 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage += 1;
            LoadOvertime(overtimeId);
        }

        protected void ddlRecords_SelectedIndexChanged()
        {
            //SessionManager.EmployeeDisplayRecodsPerPage = int.Parse(pagintCtl.DDLRecords.SelectedValue);
            //_tempCurrentPage = 1;
            pagingCtl.CurrentPage = 1;
            LoadOvertime(overtimeId);
        }

        protected void btnLoad_Click(object sender, EventArgs e)
        {
            LoadOvertime(overtimeId);
        }
        protected void btnExport_Click(object sender, EventArgs e)
        {
            int total = 0;

            int eligiblity = -1;


            OvertimePeriod overtime = commonMgr.GetOvertimePeriod(overtimeId);

          

            List<GetGeneratedOvertimeListResult> list = CommonManager.GetOvertimeList(overtimeId
                ,0,999999, ref total, txtEmpSearchText.Text.Trim()
                , eligiblity, int.Parse(ddlDesignation.SelectedValue), int.Parse(ddlBranch.SelectedValue));


            ExcelHelper.ExportToExcel("Overtime List", list,
                new List<String>() { "TotalOTMinute","NewOTHour" },
                new List<String>() { },
                new Dictionary<string, string>() { {"OnDaySalary","On Day Salary" },{"TotalOTHour","Total OT Hrs"},
                                },
                new List<string>() { "SalaryAmount", "OnDaySalary", "TotalOTHour", "OTAmount", "SST", "TDS", "NetPaid" },
                new Dictionary<string, string>() { {"Overtime List from " + FormatDate(overtime.StartDate) + " to " +
                FormatDate(overtime.EndDate),""} },
                new List<string> { "SN","EIN","PANNo", "AccountNo", "Name", "Level", "Designation", "Branch","SalaryAmount","OnDaySalary","TotalOTHour",
                    "OTAmount","SST","TDS","NetPaid","Remarks"});
        }
        protected void btnGenerate_Click(object sender, EventArgs e)
        {
            bool status = CommonManager.GenerateOvertime(overtimeId,false,-1);

            if (status)
            {
                divMsgCtl.InnerHtml = "Employee list generated for Overtime.";
                divMsgCtl.Hide = false;

                LoadOvertime(overtimeId);

                LoadDesignation();
            }
            else
            {
                divWarningMsg.InnerHtml = "Overtime already distributed, can not be changed.";
                divWarningMsg.Hide = false;
            }
        }

        protected void btnProcessApproved_Click(object sender, EventArgs e)
        {
            bool status = CommonManager.GenerateOvertime(overtimeId, true,-1);

            if (status)
            {
                divMsgCtl.InnerHtml = "Approved ot processed.";
                divMsgCtl.Hide = false;

                LoadOvertime(overtimeId);

                LoadDesignation();
            }
            else
            {
                divWarningMsg.InnerHtml = "Overtime already distributed, can not be changed.";
                divWarningMsg.Hide = false;
            }
        }
        protected void btnCalculate_Click(object sender, EventArgs e)
        {
            

          


            bool status = CommonManager.GenerateBonus(overtimeId, false, true);

            if (status)
            {

                divMsgCtl.InnerHtml = "Overtime calculated.";
                divMsgCtl.Hide = false;

                LoadGratuity();

                LoadOvertime(overtimeId);
            }
            else
            {
                divWarningMsg.InnerHtml = "Overtime already distributed, can not be changed.";
                divWarningMsg.Hide = false;
            }
        }
    }
}
