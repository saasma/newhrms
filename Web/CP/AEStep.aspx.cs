﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using Utils.Base;
using BLL;
using DAL;
using Utils.Web;
using Utils.Helper;
using System.Text.RegularExpressions;

namespace Web.CP
{
    public partial class AEStep : BasePage
    {
        /// <summary>
        /// 0-> Data Insert Mode
        /// 1-> Data Update Mode
        /// </summary>
        public string SaveMode
        {
            get { return this.hdSaveMode.Value; }
            set { this.hdSaveMode.Value = value; }
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                
                Initialize();
            }
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            if (SaveMode.Equals("0"))
            {
                string errMsg = string.Empty;
                Step step = new Step();
                step.Name = txtStepName.Text.Trim();
                step.Order = int.Parse(txtOrder.Text.Trim() == "" ? "0" : txtOrder.Text.Trim());
                step.CompanyId = SessionManager.CurrentCompanyId;
                step.CreatedOn = System.DateTime.Now;
                step.CreatedBy = SessionManager.CurrentLoggedInEmployeeId;
                if (PositionGradeStepAmountManager.SaveStep(step, ref errMsg))
                {
                    ClearField();
                    BindGrade();
                }
                else
                {
                    divWarningMsg.InnerHtml = errMsg;
                    divWarningMsg.Hide = false;
                }
            }
            else
            {
                string errMsg = string.Empty;
                Step step = new Step();
                step.StepId = int.Parse(hdStepId.Value);
                step.Name = txtStepName.Text.Trim();
                step.Order = int.Parse(txtOrder.Text.Trim() == "" ? "0" : txtOrder.Text.Trim());
                step.CompanyId = SessionManager.CurrentCompanyId;
                step.ModifiedOn = System.DateTime.Now;
                step.ModifiedBy = SessionManager.CurrentLoggedInEmployeeId;
                if (PositionGradeStepAmountManager.UpdateStep(step, ref errMsg))
                {                    
                    ClearField();
                    BindGrade();
                }
                else
                {
                    divWarningMsg.InnerHtml = errMsg;
                    divWarningMsg.Hide = false;
                }
            }
        }



        protected void gvStep_OnRowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            string errMessage =string.Empty;
            int id = (int)gvStep.DataKeys[e.RowIndex][0];
            if (!PositionGradeStepAmountManager.CanStepBeDeleted(id,ref errMessage))
            {
                divWarningMsg.InnerHtml = errMessage;
                divWarningMsg.Hide = false;
                return;

            }
            if (PositionGradeStepAmountManager.DeleteStep(id))
            {
                ClearField();
                BindGrade();
            }

        }

        

        void Initialize()
        {
            BindGrade();

        }

        void BindGrade()
        {
            List<Step> list = PositionGradeStepAmountManager.GetAllSteps(SessionManager.CurrentCompanyId);
            gvStep.DataSource = list;
            gvStep.DataBind();
            if (list.Count == 0)
                txtOrder.Text = "1";
            else
                txtOrder.Text = (list[list.Count - 1].Order + 1).ToString();
        }

        void ClearField()
        {
            hdStepId.Value = "0";
            SaveMode = "0";
            
            txtStepName.Text = string.Empty;
            divWarningMsg.Hide = true;
        }
    }
}
