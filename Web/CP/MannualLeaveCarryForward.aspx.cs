﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;
using Utils.Calendar;
using Utils.Helper;

namespace Web.NewHR
{
    public partial class MannualLeaveCarryForward : BasePage
    {

        // previously it was said for sick leave, now for annual leave
        int leaveTypeId = 2;


        protected void Page_Load(object sender, EventArgs e)
        {

            if (!X.IsAjaxRequest && !IsPostBack)
            {
                Initialise();

              

            }
        }

        public void Initialise()
        {
            PayrollPeriod period = CommonManager.GetLastPayrollPeriod();

            //calDate.Text = CustomDate.GetTodayDate(IsEnglish).ToString();
            cmbYear.Store[0].DataSource = CommonManager.GetYearList();
            cmbYear.Store[0].DataBind();

            ExtControlHelper.ComboBoxSetSelected(period.Year.ToString(), cmbYear);
            ExtControlHelper.ComboBoxSetSelected(period.Month.ToString(), cmbMonth);

            cmbMonth.Store[0].DataSource = DateManager.GetCurrentMonthList();
            cmbMonth.Store[0].DataBind();

            PagingToolbar1.DoRefresh();
        }


        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
           

            int year = int.Parse(cmbYear.SelectedItem.Value);
            int month = int.Parse(cmbMonth.SelectedItem.Value);

            DateTime startDate = DateTime.Now, endDate = DateTime.Now;

            PayrollPeriod period = CommonManager.GetPayrollPeriod(month, year);


            if (period == null)
            {
                NewMessage.ShowWarningMessage("Period does not exists for this year/month.");
                return;
            }
            else
            {
                startDate = period.StartDateEng.Value;
                endDate = period.EndDateEng.Value;
            }

            PayrollPeriod startPeriod = CommonManager.GetLeaveStartPeriodForTheMonth(
                period.Year.Value, period.Month, period.PayrollPeriodId);




            int totalRecords = 0;






          

            int employeeId = -1;

            int.TryParse(cmbSearch.SelectedItem.Value, out employeeId);

            if (employeeId == 0)
                employeeId = -1;



    
            

            List<GetOxfamCarryForwardResult> list = 
                BLL.BaseBiz.PayrollDataContext.GetOxfamCarryForward
                (startPeriod.PayrollPeriodId,period.PayrollPeriodId,SessionManager.CurrentCompanyId,leaveTypeId
                ,-1,-1,-1,employeeId, e.Page-1,e.Limit).ToList();



            if (list.Count > 0)
                e.Total = list[0].TotalRows.Value;

            //if (list.Any())
            gridList.Store[0].DataSource = list;
            gridList.Store[0].DataBind();

        }


        public void btnExport_Click(object sender, EventArgs e)
        {
            int year = int.Parse(cmbYear.SelectedItem.Value);
            int month = int.Parse(cmbMonth.SelectedItem.Value);

            DateTime startDate = DateTime.Now, endDate = DateTime.Now;

            PayrollPeriod period = CommonManager.GetPayrollPeriod(month, year);


            if (period == null)
            {
                NewMessage.ShowWarningMessage("Period does not exists for this year/month.");
                return;
            }
            else
            {
                startDate = period.StartDateEng.Value;
                endDate = period.EndDateEng.Value;
            }

            PayrollPeriod startPeriod = CommonManager.GetLeaveStartPeriodForTheMonth(
                period.Year.Value, period.Month, period.PayrollPeriodId);




            int totalRecords = 0;








            int employeeId = -1;

            int.TryParse(cmbSearch.SelectedItem.Value, out employeeId);

            if (employeeId == 0)
                employeeId = -1;






            List<GetOxfamCarryForwardResult> list =
                BLL.BaseBiz.PayrollDataContext.GetOxfamCarryForward
                (startPeriod.PayrollPeriodId, period.PayrollPeriodId, SessionManager.CurrentCompanyId, leaveTypeId
                , -1, -1, -1, employeeId, 0, 99999).ToList();




            //List<GetHolidayAdditionalPayListResult> list = EmployeeManager.GetHolidayAdditionalPayDetails
            //    ((period == null ? 0 : period.PayrollPeriodId), startDate, endDate, employeeId, 0, 99999999, "", ref totalRecords
            //    , isApprovedOnly, 0);


            string titleText = "Carry forward for  " + period.Year;

            Bll.ExcelHelper.ExportToExcel("Carry forward List", list,
                new List<string> { "INo",  "Encased", "ManualAdjustment", "LeaveTypeId", "TotalRows" },
            new List<String>() { },
            new Dictionary<string, string>() { { "Title", "Leave" },{ "BF", "Opening" }  ,{"Earned","Accrued for the year"},{"Balance","Closing Balance"}
                ,{"MannualCarryForward","Optional carryforward"}
            },
            new List<string>() { "Amount", "BasicAmount" }
            , new List<string> { "BF","Earned", "Taken","Lapsed","Balance","MannualCarryForward" }
            , new List<string> { }
            , new List<string> { }
            , new Dictionary<string, string>() { { "Carry forward List ", titleText } }
            , new List<string> { "SN", "EIN", "Name", "Title", "BF","Earned", "Taken", "Lapsed", 
                 "Balance","MannualCarryForward"}); 


        }



        protected void btnSave_Click(object sender, DirectEventArgs e)
        {


            string gridItemsJson = e.ExtraParams["gridItems"];
            List<GetOxfamCarryForwardResult> list = JSON.Deserialize<List<GetOxfamCarryForwardResult>>(gridItemsJson);
           
             int year = int.Parse(cmbYear.SelectedItem.Value);
            int month = int.Parse(cmbMonth.SelectedItem.Value);

            DateTime startDate = DateTime.Now, endDate = DateTime.Now;

            PayrollPeriod period = CommonManager.GetPayrollPeriod(month, year);


            if (period == null)
            {
                NewMessage.ShowWarningMessage("Period does not exists for this year/month.");
                return;
            }

            List<LeaveMannualCarryForward> dbList =
                BLL.BaseBiz.PayrollDataContext.LeaveMannualCarryForwards
                .Where(x => x.PayrollPeriodIdId == period.PayrollPeriodId && x.LeaveTypeId == leaveTypeId).ToList();


            foreach (GetOxfamCarryForwardResult item in list)
            {

                LeaveMannualCarryForward dbItem = dbList.FirstOrDefault(x => x.EmployeeId == item.EIN);


                if (item.MannualCarryForward == null)
                {
                    if (dbItem != null)
                        BLL.BaseBiz.PayrollDataContext.LeaveMannualCarryForwards.DeleteOnSubmit(dbItem);
                }
                else if (dbItem == null)
                {
                    dbItem = new LeaveMannualCarryForward();
                    dbItem.EmployeeId = item.EIN.Value;
                    dbItem.LeaveTypeId = leaveTypeId;
                    dbItem.PayrollPeriodIdId = period.PayrollPeriodId;
                    dbItem.Accured = item.Earned == null ? 0 : (double)item.Earned.Value;

                    dbItem.Taken = item.Taken == null ? 0 : (double)item.Taken.Value;
                    dbItem.Balance = item.Balance == null ? 0 : (double)item.Balance.Value;
                    dbItem.MannualCarryForward = item.MannualCarryForward == null ? 0 : (double)item.MannualCarryForward;

                    BLL.BaseBiz.PayrollDataContext.LeaveMannualCarryForwards.InsertOnSubmit(dbItem);
                }
                else
                    dbItem.MannualCarryForward = item.MannualCarryForward;
                    

            }

            BLL.BaseBiz.PayrollDataContext.SubmitChanges();

            NewMessage.ShowNormalMessage("Carry forward has been saved.");

            //Status status = OvertimeManager.SaveUpdateHolidayAdditionalPay(list, int.Parse(hdnPeriodId.Text));
            //if (status.IsSuccess)
            //{
            //    CheckboxSelectionModel1.ClearSelection();
            //    NewMessage.ShowNormalMessage("Pay has been approved successfully.");
            //    PagingToolbar1.DoRefresh();
            //}
            //else
            //{
            //    NewMessage.ShowWarningMessage(status.ErrorMessage);
            //}

        }


        protected void btnApproveByLine_Click(object sender, DirectEventArgs e)
        {
           
        }

        protected void btnReject_Click(object sender, DirectEventArgs e)
        {
            
        }
    }
}
