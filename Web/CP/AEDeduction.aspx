﻿<%@ Page Title="Deduction details" Language="C#" MasterPageFile="~/Master/ForPopupPage.Master"
    AutoEventWireup="true" CodeBehind="AEDeduction.aspx.cs" Inherits="Web.CP.AEDeduction" %>

<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningMsgCtl" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        var txtVariable = '#<%= txtAmount2.ClientID %>';
        var txtAdvance = '#<%= txtAdvance.ClientID %>';
        var txtTakenOn = '#<%= txtTakenOn.ClientID %>'
        var txtInstallmentAmt = '#<%= txtInstallmentAmount.ClientID %>'
        var txtPaid = '#<%= txtPaid.ClientID %>'
        var txtBalance = '#<%= txtBalance.ClientID %>';
        var txtNoOfInstallments = '#<%= txtNoOfInstallments.ClientID %>';
        var txtRemainingInstallments = '#<%= txtRemainingInstallments.ClientID%>';
        var chkVariableAmount = null;
        var rowLoanType = '#<%= rowLoanType.ClientID %>';
        var rowchkIsRefundableAdvance = '#<%= rowchkIsRefundableAdvance.ClientID %>';
        var rowRateForAll = '#<%=rowRateForAll.ClientID %>';

        var chkIsDeductionEnabled = '#<%= chkIsDeductionEnabled.ClientID %>';
        var txtIsFixedInstallment = '#<%= txtIsFixedInstallment.ClientID %>';
        $(document).ready(
            function () {
                chkVariableAmount = document.getElementById('<%= chkHasVariableAmount.ClientID %>'); ;
            }
        );

        function closePopup() {


            // alert(window.opener.parentReloadCallbackFunction)
            //if ($.browser.msie == false && typeof (window.opener.reloadDeduction) != 'undefined' && ($.browser.chrome || $.browser.safari)) {
            window.opener.reloadDeduction(window, document.getElementById('<%= hiddenEmployeeId.ClientID %>').value);
            //            } else {
            //                window.returnValue = "Reload";
            //                window.close();
            //            }

        }

        function popDepositListCall() {

        }

        var hasVariableAmount;
        function displayInstallmentCall(id) {
            //debugger;
            var ddlCalculation = document.getElementById('<%= ddlCalculation.ClientID %>');
            hasVariableAmount = document.getElementById('<%= chkHasVariableAmount.ClientID %>').checked;
            var ret = displayInstallment('type=' + ddlCalculation.value + '&hasVariableAmount=' + hasVariableAmount + '&EmpDedId=' + id);

            processReturnValue(ret);

        }


        function loadCalculation(ret, closingWindow) {
            closingWindow.close();
            processReturnValue(ret);
        }

        function processReturnValue(ret) {

            document.getElementById('<%=hdnStartNewInstallmentAsInfoChanged.ClientID %>').value = 'true';

            if (typeof (ret) == "object") {


                $(txtAdvance).val(ret["Advance"]);
                $(txtTakenOn).val(ret["TakenOn"]);
                //obj["ToBePaidOver"]
                if (hasVariableAmount) {
                    $(txtPaid).val('');
                    $(txtBalance).val('');
                    $(txtNoOfInstallments).val('');
                    $(txtRemainingInstallments).val('');
                    $(txtInstallmentAmt).val('');
                }
                else {
                    $(txtInstallmentAmt).val(ret["MonthlyPayment"]);
                    $(txtBalance).val(ret["Balance"]);
                    $(txtNoOfInstallments).val(ret["ToBePaidOver"]);
                    $(txtRemainingInstallments).val(ret["RemainingInstallment"]);
                    $(txtPaid).val(ret["Paid"]);
                    $(txtIsFixedInstallment).val(ret["IsFixedInstallment"]);
                }


            }
        }


        var pnl1Id = '#<%= pnl1.ClientID %>';
        var pnl2Id = '#<%= pnl2.ClientID %>';
        var pnl3Id = '#<%= pnl3.ClientID %>';
        function changeCalculationDisplay(ddl) {
            $(rowRateForAll).hide();
            var selIndex = ddl.selectedIndex;
            // Variable and Fixed
            if (selIndex == 1 || selIndex == 2) {
                $(pnl1Id).show();
                $(pnl2Id).hide();
                $(pnl3Id).hide();
                $(chkIsDeductionEnabled).show();

            }
            // Percent of Income
            else if (selIndex == 3) {
                $(pnl1Id).hide();
                $(pnl2Id).show();
                $(pnl3Id).hide();
                $(chkIsDeductionEnabled).show();
                $(rowRateForAll).show();
            }
            // Loan and Advance
            else if (selIndex == 4 || selIndex == 5) {
                $(pnl1Id).hide();
                $(pnl2Id).hide();
                $(pnl3Id).show();
                $(chkIsDeductionEnabled).hide();
            }

            if (selIndex == 5)
                $(rowchkIsRefundableAdvance).show();
            else
                $(rowchkIsRefundableAdvance).hide();

            if (selIndex == 4) {
                $(rowLoanType).show();
            }
            else {
                $(rowLoanType).hide();
            }
        }

        //validates the "Advance"
        function ValidateTextBox1(source, args) {
            var ddlCalculation = document.getElementById('<%= ddlCalculation.ClientID %>');
            var selValue = ddlCalculation.value;
            var str;
            args.IsValid = true;

            if (document.getElementById('ctl00_mainContent_btnInstallment') != null) {

                var isInstallmentButtonVisible = document.getElementById('<%= btnInstallment.ClientID %>').disabled;

                if ((selValue == "LoanInstallment" || selValue == "Advance") && isInstallmentButtonVisible == false) {
                    str = new String(args.Value);
                    if (str.trim() == "") {
                        args.IsValid = false;
                        //alert("Advance is required, use EMI/Installment calculation window.");
                        return;
                    }
                }
            }
        }

        function ValidateAdvanceGreaterThanVariable(source, args) {
            var ddlCalculation = document.getElementById('<%= ddlCalculation.ClientID %>');
            var selValue = ddlCalculation.value;
            args.IsValid = true;

            if ((selValue == "LoanInstallment" || selValue == "Advance") && chkVariableAmount.checked) {

                var variable = getNumber($(txtVariable).val());
                var advance = getNumber(args.Value);


                if (variable >= advance) {
                    args.IsValid = false;
                    alert("Advance amount should be greater than variable amount.");
                }

            }
        }



        //validates for install amt, & below controls
        function ValidateTextBox2(source, args) {
            var ddlCalculation = document.getElementById('<%= ddlCalculation.ClientID %>');

            var selValue = ddlCalculation.value;
            var str;
            args.IsValid = true;

            if (selValue == "LoanInstallment" || selValue == "Advance") {

                if (chkVariableAmount.checked == false) {

                    str = new String(args.Value);
                    if (str.trim() == "")
                        args.IsValid = false;
                }
            }
        }

        function deductionHistoryCall(id) {
            var ret = deductionHistory("id=" + id);
            if (typeof (ret) != 'undefined') {

            }
        }

        function calculateRepaymentMonthlyAmount() {
            var txtLoanAmount = document.getElementById('<%= txtLoanAmount.ClientID %>').value;
            var txtTotalInstallment = document.getElementById('<%= txtTotalInstallment.ClientID %>').value;
            var txtMonthlyRepayment = document.getElementById('<%= txtMonthlyRepayment.ClientID %>');

            var loanAmount = getNumber(txtLoanAmount);
            var totalInstallment = parseInt(txtTotalInstallment);

            if (txtMonthlyRepayment.value != "")
                return;

            if (isNaN(loanAmount) || isNaN(totalInstallment))
                return;


            txtMonthlyRepayment.value = (loanAmount / totalInstallment).toFixed("2");

        }

    </script>
    <style type="text/css">
        .paddinAll
        {
            padding: 10px;
        }
        table tr td
        {
            padding: 0 10px 8px 0;
        }
        h3
        {
            font: bold 13px Arial;
        }
        .marginTopss
        {
            margin-top: 10px;
            display: block;
        }
        .loanAdvTable
        {
            background-color: white;
            width: 60px;
            border: 1px solid white !important;
            padding: 0px;
        }
        .tableLightColor th
        {
            text-align: left;
        }
        .tableLightColor .even td{padding-left:0px!important;}
        .tableLightColor th{padding-left:0px!important;}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <asp:HiddenField ID="hdEditSequence" runat="server" />
    <asp:HiddenField ID="hdEEditSequence" runat="server" />
    <asp:HiddenField ID="hdnStartNewInstallmentAsInfoChanged" runat="server" Value="false" />
    <asp:HiddenField ID="hiddenEmployeeId" Value="0" runat="server" />
    <div class="popupHeader">
    </div>
    <div class=" marginal">
        <table cellpadding="0" style='padding-left: 12px' cellspacing="0">
            <tr>
                <td colspan="4">
                    <uc1:WarningMsgCtl ID="WarningMsgCtl1" runat="server" Width="100%" EnableViewState="false"
                        Hide="true" />
                </td>
            </tr>
            <tr>
                <td width="40px">
                    Title *
                </td>
                <td width="185px">
                    <asp:TextBox Width="185px" ID="txtTitle" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtTitle"
                        Display="None" ErrorMessage="Title is required." ValidationGroup="AEDeduction"></asp:RequiredFieldValidator>
                </td>
                <td width="40px">
                    Short&nbsp;name&nbsp;*
                </td>
                <td width="185px">
                    <asp:TextBox ID="txtAbbreviation" Width="185px" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtAbbreviation"
                        Display="None" ErrorMessage="Abbreviation is required." ValidationGroup="AEDeduction"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td colspan="4" runat="server" id="tdNotes">
                    Notes
                    <asp:TextBox Style="margin-left: 15px;" runat="server" ID="txtNotes" Width="463px" />
                </td>
            </tr>
            <tr>
                <td colspan="4" style="padding-left: 50px">
                    <asp:CheckBox runat="server" ID="chkIsDeductionEnabled" Checked="true" Text="Is Deduction Active" />
                </td>
            </tr>
        </table>
        <div class="clear" style="padding-top: 5px; overflow: hidden; padding-left: 5px">
            <fieldset runat="server" id="rowCalculation" class="large-heading clear" style="padding-top: 0px;">
                <h2>
                    Default Setup</h2>
                <div class="bevel paddinAll" style="width: 510px">
                    <table cellpadding="0" cellspacing="0" style='width: 450px'>
                        <tr>
                            <td width="135px">
                                Calculation
                            </td>
                            <td width="350px">
                                <asp:DropDownList AppendDataBoundItems="true" ID="ddlCalculation" Width="150px" runat="server"
                                    onchange="changeCalculationDisplay(this)">
                                    <asp:ListItem Text="--Select Calculation--" Value="-1"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" InitialValue="-1" runat="server"
                                    ControlToValidate="ddlCalculation" Display="None" ErrorMessage="Please select deduction calculation type."
                                    ValidationGroup="AEDeduction"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                <asp:CheckBox ID="chkIsExemptFromIncomeTax" runat="server" Text="This deduction is exempt from Income Tax" />
                                <span style="color: #FF0000; display: block; font-style: italic; padding-left: 20px;
                                    font-size: 10px;">Deductions are normally NOT Exempt from Tax. Consult an expert
                                    before checking this option. </span>
                            </td>
                        </tr>
                        <tr runat="server" id="rowRateForAll" style='display: none'>
                            <td width="80px">
                                Rate
                            </td>
                            <td width="350px">
                                <asp:TextBox ID="txtRateForAll" Width="150px" runat="server"></asp:TextBox>
                                <asp:CompareValidator Operator="GreaterThanEqual" ValueToCompare="0" ValidationGroup="AEDeduction"
                                    Type="Double" ID="CompareValidator2" Display="None" ControlToValidate="txtRateForAll"
                                    runat="server" ErrorMessage="Invalid rate."></asp:CompareValidator>
                            </td>
                        </tr>
                        <tr runat="server" id="rowchkIsRefundableAdvance">
                            <td style='padding-bottom: 0px!important; display: none'>
                                &nbsp;
                            </td>
                            <td style='padding-bottom: 0px!important; display: none'>
                                <asp:CheckBox ID="chkIsRefundableAdvance" runat="server" Text="Refundable Advance" />
                            </td>
                        </tr>
                    </table>
                </div>
            </fieldset>
            <h2 style="font-size: 14px; margin-left: 10px;">
                Employee Details</h2>
            <%--for variable & fixed amount--%>
            <asp:Panel ID="pnl1" runat="server" Style='padding-left: 10px'>
                <div class="bevel paddinAll marginTopss" style="width: 510px">
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="120px">
                                Amount
                            </td>
                            <td width="350px">
                                <asp:TextBox ID="txtAmount1" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="valReqdAmt1" runat="server" ControlToValidate="txtAmount1"
                                    Display="None" ErrorMessage="Amount is required." ValidationGroup="AEDeduction"></asp:RequiredFieldValidator>
                                <asp:CompareValidator Operator="DataTypeCheck" ValueToCompare="0" ValidationGroup="AEDeduction"
                                    Type="Currency" ID="valCompAmt1" Display="None" ControlToValidate="txtAmount1"
                                    runat="server" ErrorMessage="Invalid amount."></asp:CompareValidator>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
            <%--for Percent of income--%>
            <asp:Panel ID="pnl2" runat="server" CssClass="clear" Style='padding-left: 10px'>
                <div class="bevel paddinAll marginTopss" style="width: 510px">
                    <table cellpadding="0" cellspacing="0">
                        <tr runat="server" id="rowRate">
                            <td width="80px">
                                Rate
                            </td>
                            <td width="350px">
                                <asp:TextBox ID="txtRate" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="valReqdRate" runat="server" ControlToValidate="txtRate"
                                    Display="None" ErrorMessage="Rate is required." ValidationGroup="AEDeduction"></asp:RequiredFieldValidator>
                                <asp:CompareValidator Operator="GreaterThanEqual" ValueToCompare="0" ValidationGroup="AEDeduction"
                                    Type="Double" ID="valCompRate" Display="None" ControlToValidate="txtRate" runat="server"
                                    ErrorMessage="Invalid rate."></asp:CompareValidator>
                            </td>
                        </tr>
                        <tr>
                            <td width="120px" style='vertical-align: top'>
                                Income(s)
                            </td>
                            <td width="350pxpx">
                                <div style="overflow-y: scroll; height: 100px; width: 370px; border: 1px solid #C1D6F3;
                                    padding: 0px">
                                    <asp:CheckBoxList ID="chkListIncomes" RepeatColumns="2" RepeatDirection="Horizontal"
                                        runat="server" DataTextField="Title" DataValueField="IncomeId">
                                    </asp:CheckBoxList>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td width="120px" style='vertical-align: top'>
                                <asp:CheckBox ID="chkApplyStatusFilter" runat="server" Text="Status Filter" />
                            </td>
                        </tr>
                        <tr>
                            <td width="120px" style='vertical-align: top'>
                            </td>
                            <td width="350pxpx">
                                <div runat="server" id="divStatus" style="overflow-y: scroll;display:none; height: 70px; width: 370px;
                                    border: 1px solid #C1D6F3; padding: 0px">
                                    <asp:CheckBoxList ID="chkStatusList" RepeatColumns="3" RepeatDirection="Horizontal"
                                        runat="server" DataTextField="Value" DataValueField="Key">
                                    </asp:CheckBoxList>
                                </div>
                            </td>
                        </tr>
                        <tr runat="server" id="rowCutOff">
                            <td width="120px">
                                Cut Off
                            </td>
                            <td width="350px">
                                <asp:TextBox ID="txtCutOff2" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="valReqdCutOff2" runat="server" ControlToValidate="txtCutOff2"
                                    Display="None" ErrorMessage="Cut off is required." ValidationGroup="AEDeduction"></asp:RequiredFieldValidator>
                                <asp:CompareValidator Operator="GreaterThanEqual" ValueToCompare="0" ValidationGroup="AEDeduction"
                                    Type="Currency" ID="valCompCutOff2" Display="None" ControlToValidate="txtCutOff2"
                                    runat="server" ErrorMessage="Invalid cut off."></asp:CompareValidator>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
            <%--Loan Repayment--%>
            <asp:Panel ID="pnlLoanRepayment" runat="server" CssClass="clear" Style='padding-left: 10px;
                display: none'>
                <div class="bevel paddinAll marginTopss" style="width: 510px">
                    <table cellpadding="0" cellspacing="0">
                        <tr runat="server" id="Tr4">
                            <td style="width: 100px">
                                Loan Amount
                            </td>
                            <td>
                                <asp:TextBox ID="txtLoanAmount" onblur="calculateRepaymentMonthlyAmount();" Width="140px"
                                    runat="server"></asp:TextBox>
                            </td>
                            <td colspan="2" style="padding-left: 20px;">
                                Total Installment
                            </td>
                        </tr>
                        <tr runat="server" id="Tr1">
                            <td style="width: 100px">
                                Monthly Repayment
                            </td>
                            <td>
                                <asp:TextBox ID="txtMonthlyRepayment" Width="140px" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtMonthlyRepayment"
                                    Display="None" ErrorMessage="Amount is required." ValidationGroup="AEDeduction"></asp:RequiredFieldValidator>
                                <asp:CompareValidator Operator="GreaterThanEqual" ValueToCompare="0" ValidationGroup="AEDeduction"
                                    Type="Currency" ID="CompareValidator1" Display="None" ControlToValidate="txtMonthlyRepayment"
                                    runat="server" ErrorMessage="Invalid amount."></asp:CompareValidator>
                            </td>
                            <td colspan="2" style="padding-left: 20px;">
                                <asp:TextBox onblur="calculateRepaymentMonthlyAmount();" ID="txtTotalInstallment"
                                    Width="140px" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvTotalInstallment" runat="server" ControlToValidate="txtTotalInstallment"
                                    Display="None" ErrorMessage="Total Installment is required." ValidationGroup="AEDeduction"></asp:RequiredFieldValidator>
                                <asp:CompareValidator Operator="GreaterThanEqual" ValueToCompare="0" ValidationGroup="AEDeduction"
                                    Type="Integer" ID="cvTotalInstallment" Display="None" ControlToValidate="txtTotalInstallment"
                                    runat="server" ErrorMessage="Invalid Total Installment."></asp:CompareValidator>
                            </td>
                        </tr>
                        <tr runat="server" id="Tr5">
                            <td style="width: 100px">
                                Installment No
                            </td>
                            <td>
                                <asp:TextBox ID="txtInstallmentNo" Width="140px" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvInstallmentNo" runat="server" ControlToValidate="txtInstallmentNo"
                                    Display="None" ErrorMessage="Installment No is required." ValidationGroup="AEDeduction"></asp:RequiredFieldValidator>
                                <asp:CompareValidator Operator="GreaterThanEqual" ValueToCompare="0" ValidationGroup="AEDeduction"
                                    Type="Integer" ID="cvInstallmentNo" Display="None" ControlToValidate="txtInstallmentNo"
                                    runat="server" ErrorMessage="Invalid Installment No."></asp:CompareValidator>
                            </td>
                        </tr>
                        <tr>
                            <td width="120px" style='vertical-align: top'>
                                Taken On
                            </td>
                            <td width="">
                                <My:Calendar Id="calRepaymentTakenOn" IsEnglishCalendar="true" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td width="120px" style='vertical-align: top'>
                                Start From
                            </td>
                            <td width="">
                                <My:Calendar Id="calFrom" IsSkipDay="true" runat="server" />
                            </td>
                            <td colspan="2" style="padding-left: 20px;">
                                Installments
                            </td>
                        </tr>
                        <tr runat="server" id="Tr2">
                            <td width="120px" style='vertical-align: top'>
                                Deduct Until
                            </td>
                            <td width="">
                                <My:Calendar Id="calTo" IsSkipDay="true" runat="server" />
                            </td>
                            <td>
                                or
                            </td>
                            <td>
                                <asp:TextBox ID="txtRepaymentLoanNoOfInstallment" Width="140px" AutoPostBack="true"
                                    OnTextChanged="txtRepaymentLoanNoOfInstallment_Change" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 10px">
                            </td>
                        </tr>
                        <tr runat="server" id="Tr3">
                            <td style="width: 100px">
                                Loan Account No
                            </td>
                            <td>
                                <asp:TextBox ID="txtLoanAccountNo" Width="140px" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnl3" runat="server" CssClass="clear" Style='padding-left: 10px'>
                <div class="bevel paddinAll marginTopss" style="width: 510px">
                    <table cellpadding="0" cellspacing="0" runat="server" id="tblLoanAdvance" style='width: 100%;'>
                        <tr style="display: none">
                            <td width="120px">
                                <asp:CheckBox ID="chkHasVariableAmount" Checked="false" runat="server" Text="Variable Amount"
                                    TextAlign="Right" OnCheckedChanged="chkHasVariableAmount_CheckedChanged" />
                            </td>
                            <td runat="server" id="colAmount">
                                <asp:TextBox ID="txtAmount2" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="valReqdAmt2" runat="server" ControlToValidate="txtAmount2"
                                    Display="None" ErrorMessage="Amount is required." ValidationGroup="AEDeduction"></asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="valCompAmt2" runat="server" ControlToValidate="txtAmount2"
                                    Display="None" ErrorMessage="Invalid amount." Operator="GreaterThanEqual" Type="Currency"
                                    ValidationGroup="AEDeduction" ValueToCompare="0"></asp:CompareValidator>
                            </td>
                        </tr>
                        <tr runat="server">
                            <td class="lf">
                            </td>
                            <td>
                                <asp:Button ID="btnInstallment" runat="server" OnClientClick="displayInstallmentCall({0});return false;"
                                    Text="Installment Calculation" />
                                <a href='javascript:deductionHistoryCall(<%= this.CustomId %>)' style='text-decoration: underline'>
                                    Show History</a>
                            </td>
                            <td>
                                <asp:Button Visible="false" OnClientClick='return confirm("Confirm mark the loan as paid?");' OnClick="btnMarkAsPaid_Click" ID="btnMarkAsPaid" style='background-color: lightgray!important; border: 1px solid gray; padding: 5px;
                                width: 170px;' runat="server" Text="Mark the Loan as Paid" />
                            </td>
                        </tr>
                        <tr id="rowLoanType" runat="server">
                            <td class="lf">
                            </td>
                            <td>
                                <asp:RadioButton ID="rdbIsEMI" runat="server" GroupName="LoanType" Text="EMI" />
                                <asp:RadioButton ID="rdbIsSimple" runat="server" Checked="true" GroupName="LoanType"
                                    Text="Simple Interest" />
                            </td>
                        </tr>
                    </table>
                    <table class="tableLightColor">
                        <tr>
                            <th>
                                Advance
                            </th>
                            <th>
                                Taken On
                            </th>
                            <th>
                                Installment
                            </th>
                            <th>
                                Paid
                            </th>
                            <th>
                                Balance
                            </th>
                            <th>
                                Installments
                            </th>
                            <th>
                                Remaining
                            </th>
                        </tr>
                        <tr class="even">
                            <td>
                                <asp:TextBox ID="txtAdvance" Width='65px' CssClass="disabledTxt loanAdvTable" runat="server" ReadOnly="true"></asp:TextBox>
                                <asp:CustomValidator ID="valLAAdvance" runat="server" ErrorMessage="Advance is required, use EMI/Installment calculation window."
                                    ClientValidationFunction="ValidateTextBox1" ControlToValidate="txtAdvance" Display="None"
                                    ValidateEmptyText="true" ValidationGroup="AEDeduction"></asp:CustomValidator>
                                <asp:CustomValidator ID="CustomValidator1" runat="server" ClientValidationFunction="ValidateAdvanceGreaterThanVariable"
                                    ControlToValidate="txtAdvance" Display="None" ValidateEmptyText="true" ValidationGroup="AEDeduction"></asp:CustomValidator>
                            </td>
                            <td>
                                <asp:TextBox ID="txtTakenOn" CssClass="disabledTxt loanAdvTable" runat="server" ReadOnly="true"></asp:TextBox>
                                <asp:CustomValidator ID="valLATakenOn" runat="server" ClientValidationFunction="ValidateTextBox1"
                                    ControlToValidate="txtTakenOn" Display="None" ErrorMessage="Taken on is required, use EMI/Installment calculation window."
                                    ValidateEmptyText="true" ValidationGroup="AEDeduction"></asp:CustomValidator>
                            </td>
                            <td>
                                <asp:TextBox ID="txtInstallmentAmount"  Width='70px'  CssClass="disabledTxt loanAdvTable" runat="server"
                                    ReadOnly="true"></asp:TextBox>
                                <asp:HiddenField ID="txtIsFixedInstallment" runat="server"></asp:HiddenField>
                                <asp:HiddenField ID="txtSINewPPMTAmount" runat="server" />
                                <%-- <asp:CustomValidator ID="valLAInstallmentAmount" runat="server" ClientValidationFunction="ValidateTextBox2"
                                    ControlToValidate="txtInstallmentAmount" Display="None" ErrorMessage="Installment amount is required, use EMI/Installment calculation window."
                                    ValidateEmptyText="true" ValidationGroup="AEDeduction"></asp:CustomValidator>--%>
                            </td>
                            <td>
                                <asp:TextBox ID="txtPaid"    CssClass="disabledTxt loanAdvTable" runat="server" ReadOnly="true"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="txtBalance" Width='65px'  CssClass="disabledTxt loanAdvTable" runat="server" ReadOnly="true"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="txtNoOfInstallments" CssClass="disabledTxt loanAdvTable" runat="server"
                                    ReadOnly="true"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="txtRemainingInstallments" CssClass="disabledTxt loanAdvTable" runat="server"
                                    ReadOnly="true"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
            <div style="padding-bottom: 10px; clear: both; padding-top: 10px;">
                <asp:CheckBox Style="padding-left: 10px;" Visible="false" Text="Exclude In Salary For Retirement"
                    ID="chkExcludeRegularInstallmentInRetirement" runat="server" /></div>
        </div>
        <br />
        <div style='padding-left: 12px; text-align: right; padding-right: 22px'>
            <asp:Button ID="btnSave" runat="server" CssClass="save" OnClientClick="valGroup='AEDeduction';return CheckValidation()"
                Text="Save" ValidationGroup="AEDeduction" OnClick="btnSave_Click" />
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="cancel" OnClientClick="window.close()" />
        </div>
    </div>
</asp:Content>
