<%@ Page Title="Retrospect Increment Adjustment" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="AdjustRetrospectIncrement.aspx.cs" Inherits="Web.CP.AdjustRetrospectIncrement" %>

<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/CalendarCtl.ascx" TagPrefix="uc" TagName="Calendar" %>
<%@ Register Src="../Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .increment
        {
        }
        .tableLightColor a:hover
        {
            color: #048FC2;
            text-decoration: none;
        }
        table td
        {
            padding-top: 4px;
        }
        .newbreadcum
        {
            list-style-type: none;
            margin: 0;
            padding: 0;
            margin-bottom: 10px;
        }
        .newbreadcum li
        {
            float: left;
            padding-right: 15px;
        }
    </style>
    <script type="text/javascript">
        function refreshWindow() {
            __doPostBack('Reload', '');
        }

        function importPopupProcess(btn) {




            var ret = importPopup();


            return false;
        }

        function importPopupProcessARI() {
            var IncomeIdValue = <%= IncomeId %>;
            var TypeValue = <%= Type %>;

            var ret = importPopupAdArrInc('IncomeId=' + IncomeIdValue + '&Type=' + TypeValue);

            return false;
        }

         function ACE_item_selected(source, eventArgs) {
        var value = eventArgs.get_value();
        document.getElementById('<%= hiddenEmployeeID.ClientID %>').value = value;
    }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <asp:HiddenField ID="hdIncreaseType" runat="server" />
    <asp:HiddenField ID="hdIncomeType" runat="server" />
     <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                   Adjust Arrear Increment
                </h4>
            </div>
        </div>
    </div>
    <div class="contentArea" style='padding:10px;'>
 <asp:HiddenField ID="hiddenEmployeeID" runat="server" />
        <asp:Repeater ID="links" runat="server" Visible="false">
            <HeaderTemplate>
                <ul class="newbreadcum">
            </HeaderTemplate>
            <ItemTemplate>
                <li>
                    <asp:HyperLink ID="link" runat="server" Text='<%#Eval("Text")+ " >>" %>' NavigateUrl='<%#Eval("Url") %>' />
                </li>
            </ItemTemplate>
            <FooterTemplate>
                </ul></FooterTemplate>
        </asp:Repeater>
        <div class="attribute" style="clear: both;padding:10px;">
            <table style="margin-left: -3px">
                <tr>
                    <td>
                        Income :
                    </td>
                    <td style='padding-left:10px;'>
                        <asp:Label Style="font-weight: bold; color: #048FC2" ID="lblIncome" runat="server" />
                    </td>
                     <td style='padding-left:10px;'>
                       <asp:TextBox ID="txtEmpSearch" runat="server" OnTextChanged="txtEmpSearch_TextChanged"
                            AutoPostBack="true" Width="160" />
                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtEmpSearch"
                            WatermarkText="Search Employee" WatermarkCssClass="searchBoxText" />
                        <cc1:AutoCompleteExtender EnableCaching="true" ID="AutoCompleteExtenderOrganization"
                            runat="server" MinimumPrefixLength="2" ServiceMethod="GetEmployeeNamesAndEINWithRetiredAlso"
                            ServicePath="~/PayrollService.asmx" TargetControlID="txtEmpSearch" CompletionSetCount="10"
                            CompletionInterval="250" OnClientItemSelected="ACE_item_selected" CompletionListCssClass="AutoExtender"
                            CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
                        </cc1:AutoCompleteExtender>
                    </td>
                    <td style='padding-left:10px;'>
                        <asp:Button ID="btnGenerate" CssClass="update" runat="server" Text="Generate" OnClick="btnGenerate_Click" />
                    </td>
                    <td style="padding-left:400px;">
                            <asp:LinkButton ID="btnExport" runat="server" Text="Excel import" OnClientClick="importPopupProcessARI();return false;"
                                CssClass=" excel marginRight tiptip" Style="float: left;" />
                    </td>
                </tr>
            </table>
            <div style="margin-bottom: 5px; margin-top: 5px">
                <asp:Label Style="font-weight: bold; text-decoration: underline;" Visible="false"
                    ID="lblIncomeDependent" Text="Incomes Dependent upon " runat="server" />
            </div>
            <asp:DataList RepeatColumns="4" ItemStyle-Width="250px" DataKeyField="IncomeId" RepeatDirection="Horizontal"
                runat="server" ID="rptOtherIncomes" OnDeleteCommand="rptOtherIncomes_DeleteCommand"
                OnSelectedIndexChanged="rptOtherIncomes_SelectedIndexChanged">
                <ItemTemplate>
                    <asp:HyperLink runat="server" Text='<%# Eval("Title")%>' NavigateUrl='<%# "AdjustRetrospectIncrement.aspx?Id=" + Eval("IncomeId") + "&Type=" + Eval("Type") %>' />
                    &nbsp;
                    <asp:LinkButton ID="btnDelete" CommandName="Delete" CommandArgument='<%#Eval("IncomeId") %>'
                        Visible='<%# IsDeleteVisible(Eval("IncomeId"),1) %>' runat="server" OnClientClick="return confirm('Confirm delete the adjustment?');">
                    <img src="../images/retdelete.png" alt="delete group" />
                    </asp:LinkButton>
                </ItemTemplate>
            </asp:DataList>
            <asp:Panel runat="server" ID="hplAllowancePanel" Visible="false">
                <div style="margin-bottom: 5px; margin-top: 5px">
                    <asp:Label Style="font-weight: bold; text-decoration: underline;"
                        ID="Label1" Text="Allowance" runat="server" />
                         <asp:Button ID="btnGenerateAllAllowance" Width="100" Height="25" runat="server" Text="Generate" OnClick="btnGenerateAllAllowance_Click" />
                </div>
                <asp:DataList RepeatColumns="4" ItemStyle-Width="250px" DataKeyField="IncomeId" RepeatDirection="Horizontal"
                    runat="server" ID="rptAllowances" OnDeleteCommand="rptOtherIncomes_DeleteCommand"
                    OnSelectedIndexChanged="rptOtherIncomes_SelectedIndexChanged">
                    <ItemTemplate>
                        <asp:HyperLink ID="HyperLink1" runat="server" Text='<%# Eval("Title")%>' NavigateUrl='<%# "AdjustRetrospectIncrement.aspx?Id=" + Eval("IncomeId") + "&Type=" + Eval("Type") %>' />
                        &nbsp;
                        <asp:LinkButton ID="btnDelete" CommandName="Delete" CommandArgument='<%#Eval("IncomeId") %>'
                            Visible='<%# IsDeleteVisible(Eval("IncomeId"),Eval("Type")) %>' runat="server" OnClientClick="return confirm('Confirm delete the adjustment?');">
                    <img src="../images/retdelete.png" alt="delete group" />
                        </asp:LinkButton>
                    </ItemTemplate>
                </asp:DataList>
            </asp:Panel>

        </div>
        <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
        <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
        <div class="clear" style="margin-top: 10px">
            <cc2:EmptyDisplayGridView AllowSorting="true" OnSorting="gvwEmployees_Sorting" CssClass="tableLightColor"
                UseAccessibleHeader="true" DataKeyNames="EmployeeId,IncomeId,PayrollPeriodId"
                GridLines="None" HeaderStyle-CssClass="sortLink" ID="gvMassIncrement" runat="server"
                ShowFooterWhenEmpty="False" ShowHeaderWhenEmpty="True" AutoGenerateColumns="False">
                <Columns>
                    <asp:BoundField DataField="EmployeeId" HeaderText="EIN" SortExpression="EmployeeId"
                        ItemStyle-HorizontalAlign="Right" HeaderStyle-Width="30px" />
                    <asp:BoundField DataField="EmployeeIncomeId" HeaderText="EmployeeIncomeId" Visible="false" />
                    <asp:TemplateField HeaderText="&nbsp;Name" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:HyperLink CssClass="employeeClass" Target="_blank" Width="130px" ID="Label2"
                                runat="server" NavigateUrl='<%# "~/newhr/EmployeePayroll.aspx?Id=" +  Eval("EmployeeId") %>'
                                Text='<%# "&nbsp;" + Eval("Name") %>'></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Designation" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="100px"
                        HeaderText="Designation" />
                    <asp:TemplateField HeaderText="Status" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <%# GetStatus( Eval("Status")) %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="OldAmount" HeaderText="Current Salary" ItemStyle-HorizontalAlign="Right"
                        HeaderStyle-Width="100px" DataFormatString="{0:N2}" />
                    <asp:BoundField DataField="NewAmount" HeaderText="New Salary" ItemStyle-HorizontalAlign="Right"
                        HeaderStyle-Width="100px" DataFormatString="{0:N2}" />
                    <asp:BoundField DataField="EffectiveDate" HeaderText="Effective Date" ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-Width="80px" />
                    <asp:BoundField DataField="Months" HeaderText="Months" ItemStyle-HorizontalAlign="Right"
                        HeaderStyle-Width="70px" />
                    <asp:BoundField DataField="Days" HeaderText="Days" ItemStyle-HorizontalAlign="Right"
                        HeaderStyle-Width="70px" />
                    <asp:TemplateField HeaderText="Total Increase">
                        <ItemTemplate>
                            <asp:TextBox data-col='1' class="increment" increment="true" data-row='<%# Container.DataItemIndex %>'
                                ID="txtTotalIncreased" AutoCompleteType="None" Width="70px" Style='text-align: right'
                                runat="server" Text='<%#GetCurrency(Eval("TotalIncrease"))%>' />
                            <asp:CompareValidator SetFocusOnError="true" ValueToCompare="0" Display="None" Operator="DataTypeCheck"
                                Type="Currency" ID="valOpeningBalance1" ControlToValidate="txtTotalIncreased"
                                ValidationGroup="Balance" runat="server" ErrorMessage="Invalid amount."></asp:CompareValidator>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Total PF">
                        <ItemTemplate>
                            <asp:TextBox data-col='2' class="increment" increment="true" data-row='<%# Container.DataItemIndex %>'
                                ID="txtTotalPFIncreased" Width="70px" AutoCompleteType="None" Style='text-align: right'
                                runat="server" Text='<%#GetCurrency(Eval("TotalPFIncrease"))%>' />
                            <asp:CompareValidator SetFocusOnError="true" ValueToCompare="0" Display="None" Operator="GreaterThanEqual"
                                Type="Currency" ID="valOpeningBalance12" ControlToValidate="txtTotalPFIncreased"
                                ValidationGroup="Balance" runat="server" ErrorMessage="Invalid amount."></asp:CompareValidator>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                <EmptyDataTemplate>
                    <b>No Employees are found</b>
                </EmptyDataTemplate>
            </cc2:EmptyDisplayGridView>
        </div>
        <uc1:PagingCtl ID="pagintCtl" OnNextRecord="btnNext_Click" OnPrevRecord="btnPrevious_Click"
            OnDropDownShowRecordsChanged="ddlRecords_SelectedIndexChanged" runat="server" />
        <div class="buttonsDiv">
            <asp:Button ID="btnSave" OnClientClick="valGroup='Balance';return CheckValidation();"
                CssClass="save" runat="server" Text="save" ValidationGroup="Balance" OnClick="btnSave_Click" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="pageScriptsContent" runat="server">
    <script type="text/javascript">


        $(document).ready(function () {
            setMovementToGrid('#<%= gvMassIncrement.ClientID %>');

        });

        function selectAllText(textbox) {

            if (typeof (textbox.getAttribute) != 'undefined') {
                if (textbox.getAttribute("readonly") != null || textbox.getAttribute("disabled") != null)
                    return;
            }
            else if (textbox.length > 0) {
                if (textbox[0].getAttribute("readonly") != null || textbox[0].getAttribute("disabled") != null)
                    return;
            }

            textbox.select();
        }      
        
    </script>
</asp:Content>
