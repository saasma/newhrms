<%@ Page MaintainScrollPositionOnPostback="true" Title="Change Employee Status" Language="C#"
    MasterPageFile="~/Master/HROld.Master" AutoEventWireup="true" CodeBehind="ChangeStatus.aspx.cs"
    Inherits="Web.CP.ChangeStatus" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Employee Status
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="contentArea">
            <div class="alert alert-info">
                <br />
                Change the employee status only if you require. Rigo Payroll has automatically created
                the default statuses to make it easy for you.
                <br />
                <br />
                If you change the statuses, make sure the <b>lowest status is entered first</b>
                ,other statuses are entered <b>sequentially in the ascending order.</b>
                <br />
                <br />
                <span style="color: Red">If employees are already imported then do not change the status
                    or consult with Rigo before any changes. </span>
                <br />
                <br />
                <span style="color: green" runat="server" id="spanContractLevelGradeCase">For Contract
                    checked status salary will not be auto assigned using Level/Grade matrix, specific
                    amount with income has to be assinged to each employees.<br />
                    <br />
                </span>
            </div>
            <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
            <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
            <cc2:EmptyDisplayGridView Style='clear: both;' GridLines="None" PagerStyle-CssClass="defaultPagingBar"
                CssClass="table table-primary mb30 table-bordered table-hover" ShowHeaderWhenEmpty="True" ID="gvw" runat="server"
                DataKeyNames="StatusId" AutoGenerateColumns="False" ShowFooterWhenEmpty="False"
                AllowPaging="True" PageSize="20">
                <Columns>
                    <asp:TemplateField HeaderText="Order" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="40px">
                        <ItemTemplate>
                            <%# Eval("StatusId")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Default Name" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="100px">
                        <ItemTemplate>
                            <%# Eval("DefaultName")%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Custom Name" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-Width="120px">
                        <ItemTemplate>
                            <asp:TextBox ID="txt" Width="120px" runat="server" Text='<%# Eval("NewName")%>' />
                            <asp:RequiredFieldValidator SetFocusOnError="true" ID="RequiredFieldValidator1" runat="server"
                                ErrorMessage='Name is required.' ControlToValidate="txt" Display="None" ValidationGroup="Rate"></asp:RequiredFieldValidator>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Treat as Contract Status" HeaderStyle-HorizontalAlign="Left"
                        HeaderStyle-Width="100px">
                        <ItemTemplate>
                            <asp:CheckBox ID="chkIsContract1" Checked='<%# Eval("IsContract") %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <PagerSettings Mode="NumericFirstLast" NextPageText="Next �" PreviousPageText="� Prev"
                    FirstPageText="First" LastPageText="Last" />
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                <PagerStyle CssClass="defaultPagingBar" />
                <SelectedRowStyle CssClass="selected" />
            </cc2:EmptyDisplayGridView>
        </div>
        <%-- <asp:Button ID="btnDelete" OnClientClick="return confirm('Are you sure, you want to delete the Status?');" CssClass="delete" CausesValidation="false"
        runat="server" Text="Delete" ValidationGroup="Rate" OnClick="btnDelete_Click" />--%>
        <asp:Button ID="btnSave" CssClass="btn btn-primary btn-sect btn-sm" OnClientClick="valGroup = 'Rate';return CheckValidation();"
            runat="server" Text="Save Changes" ValidationGroup="Rate" OnClick="btnSave_Click" />
    </div>
</asp:Content>
