﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;

namespace Web.CP
{
    public partial class DeductionLoanAdvanceHistory : BasePage
    {
        private PEmployeeIncrement currentIncrement = null;
        PayManager payMgr =new PayManager();
        protected void Page_Load(object sender, EventArgs e)
        {
            this.CustomId = int.Parse(Request.QueryString["id"]);
            //currentIncrement = PayManager.GetCurrentIncrement(id);
           
            if (!IsPostBack)
            {
                
                //PayManager mgr = new PayManager();
                BindData();
               
            }

            
        }

        void BindData()
        {
            PEmployeeDeduction empDeduction = payMgr.GetEmployeeDeduction(this.CustomId);

            if( empDeduction.PDeduction.Calculation==DeductionCalculation.Advance)
            {
                gvw.Columns[3].HeaderText = "Installment";
                gvw.Columns[5].Visible = false;
                gvw.Columns[6].Visible = false;
                gvw.Columns[7].Visible = false;
            }

            gvw.DataSource = PayManager.GetLoanAdvForHistory(empDeduction.EmployeeId, empDeduction.DeductionId);
            gvw.DataBind();
        }
        public  string GetRate(object  rate)
        {
            if (rate == null)
                return "";
            return rate + "%";
        }

        public string GetPaymentTerms(object terms, object toBePaidOver)
        {
            string value = "";
            if (terms != null)
            {
                EMILoadPaymentTerm convert = new EMILoadPaymentTerm();
                value =  convert.GetPaymentTerm(int.Parse(terms.ToString()));
            }
            else //for advance Terms will be null so set monthly
            {
                value = EMILoadPaymentTerm.MONTHLY.ToString();
            }
            if (toBePaidOver != null)
            {
                value += " - " + int.Parse(toBePaidOver.ToString());
            }
            return value;
        }

        public  bool IsCurrent(object  id)
        {
            int value =Convert.ToInt32(id);
            if (currentIncrement != null)
            {
                if (value == currentIncrement.Id)
                    return true;
            }

            return false;
        }

        protected void gvw_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
           
            gvw.PageIndex = e.NewPageIndex;
            BindData();
        }
    }
}
