﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/ForPopupPage.Master" AutoEventWireup="true" CodeBehind="ChangePasswordPopup.aspx.cs" Inherits="Web.CP.ChangePasswordPopup" %>
<%@ Register src="../UserControls/ChangePasswordCtl.ascx" tagname="ChangePasswordCtl" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../js/jquery-1.11.1.min.js" type="text/javascript"></script>
    <link href="../css/core.css" rel="stylesheet" type="text/css" />
    <link href="../css/style.default.css" rel="stylesheet" type="text/css" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <div style='padding:10px'>
    <uc1:ChangePasswordCtl ID="ChangePasswordCtl1" runat="server" />
    </div>
</asp:Content>
