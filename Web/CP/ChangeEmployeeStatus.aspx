﻿<%@ Page Title="Change Employee Status" Language="C#" MasterPageFile="~/Master/ForPopupPage.Master"
    AutoEventWireup="true" CodeBehind="ChangeEmployeeStatus.aspx.cs" Inherits="Web.CP.ChangeEmployeeStatus" %>

<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/CalendarCtl.ascx" TagPrefix="uc" TagName="Calendar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        function closePopup() {
            clearUnload();
            //if ($.browser.msie == false && typeof (window.opener.loadAndHide) != 'undefined' && ($.browser.chrome || $.browser.safari)) {
            window.opener.loadAndHide(window);
            //            } else {
            //                
            //                window.close();
            //            }
        }

        function clearUnload() {
            window.onunload = null;
        }





        ////       function onStatusSave() {
        ////            if (typeof (EmployeeId) != 'undefined') {


        ////                if (confirm("Confirm save status for the employee?") == false)
        ////                    return;

        ////                var status = $('#' + ddlStatusId).val();
        ////                //var dateFrom = $(hdnFromDate).val();
        ////                //dateTo = getCalendarSelectedDate('calTo.ClientID');
        ////                dateFrom = getCalendarSelectedDate('<%= calFrom.ClientID %>');
        ////                
        ////                showLoading();
        ////                Web.PayrollService.SaveEmployeeStatus(editId, status, dateFrom, parseInt(EmployeeId), onStatusSaveCallback);

        ////                $(btn).val("Save");
        ////            }
        ////        }

        ////        function onStatusSaveCallback(result) {
        ////            if (result) {

        ////                var status = $('#' + ddlStatusId).val();

        ////                var ddlStatus = document.getElementById(ddlStatusId);
        ////                if (isDownGradeEnable == false) {
        ////                    for (var i = ddlStatus.options.length - 1; i >= 0; i--) {
        ////                        if (ddlStatus.options[i].value == status) {
        ////                            for (var j = i - 1; j > 0; j--) {
        ////                                ddlStatus.remove(j);
        ////                            }
        ////                            break;
        ////                        }
        ////                    }
        ////                }

        ////                var currentStateId = editId;
        ////                editId = 0;

        ////                document.getElementById(ddlStatusId).selectedIndex = 0;
        ////              



        ////            }
        ////            else
        ////                hideLoading();
        ////        }

        function validateStatusToDate(source, args) {
            var firstDate = getCalendarSelectedDate('<%= calFrom.ClientID %>');

            var isUptoDefined = $('#<%=chkDOJToNotDefined.ClientID %>').attr('checked');

            if (isUptoDefined == false) {
                args.IsValid = true;
                return;
            }

            var secondDate = getCalendarSelectedDate('<%= calTo.ClientID %>');

            //if equal also then valid
            if (isDateSame(firstDate, secondDate)) {
                args.IsValid = true;
                return;
            }

            args.IsValid = isSecondCalendarCtlDateGreater(firstDate, secondDate);

            if (args.IsValid == false) {
                if (confirm("To date is not greater than from date."))
                    args.IsValid = true;
                else
                    args.IsValid = false;
            }

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <div class="popupHeader">
        <h2 class="headlinespop">
            Change Employee Status
        </h2>
    </div>
    <div class="bevel marginal">
        <div class="fields paddpop">
            <uc2:WarningCtl ID="divWarningMsg" Width="400px" EnableViewState="false" Hide="true"
                runat="server" />
            <table class="removePadding" runat="server" id="tbl" width="480px" cellpadding="0"
                cellspacing="0">
                <tr>
                    <td>
                        Event Type<br />
                        <asp:DropDownList Width="138px" AppendDataBoundItems="true" ID="ddlEventType" runat="server"
                            DataValueField="EventID" DataTextField="Name">
                            <asp:ListItem Value="-1" Text=""></asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="reqdEvenType" runat="server" ControlToValidate="ddlEventType"
                            Display="None" ErrorMessage="Event type is required." InitialValue="-1" ValidationGroup="AEStatus"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                  <tr>
                    <td style="height: 10px">
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Label ID="lblStatusName" runat="server" Text="New Status" />
                        <br />
                        <asp:DropDownList Style="margin-top: 10px" CssClass='statusWidth' ID="ddlStatus"
                            runat="server" AppendDataBoundItems="true" Width="150px">
                            <asp:ListItem Selected="true" Value="-1" Text="--Status--"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator InitialValue="-1" ID="valReqdName1" runat="server" ControlToValidate="ddlStatus"
                            Display="None" ErrorMessage="Please select a status." ValidationGroup="AEStatus"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td style="height: 10px">
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblFromDate" runat="server" Text="From" />
                    </td>
                    <td>
                        Up to
                        <asp:CheckBox ID="chkDOJToNotDefined" Style="padding-left: 47px" ToolTip="Define up to date or not"
                            Text="Define Up To" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td style="height: 10px">
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:HiddenField ID="hdnFromDate" runat="server" />
                        <uc:Calendar Id="calFrom" runat="server" />
                        <asp:CustomValidator ValidateEmptyText="true" ID="valCustomeToDate1" runat="server"
                            ControlToValidate="ddlStatus" ValidationGroup="AEStatus" Display="None" ErrorMessage=""
                            ClientValidationFunction="validateStatusToDate" />
                    </td>
                    <td>
                        <uc:Calendar Id="calTo" runat="server" Enabled="false" />
                    </td>
                </tr>
                <tr>
                    <td style="padding-top: 15px" colspan="2">
                        <asp:TextBox TextMode="MultiLine" ID="txtNote" runat="server" Width="415" Height="50" />
                    </td>
                </tr>
            </table>
            <br />
            <div style="color: #FF6300">
                Note : If From Date lies in Past Attendance, then resave the past attendance of
                the employee for leave balance.</div>
            <asp:Button ID="btnSaveStatus" OnClick="btnSaveStatus_Click" runat="server" OnClientClick="valGroup='AEStatus';if( CheckValidation() && confirm('Confirm save status for the employee?') )  {} else return false;"
                Text="Save Status" class="save" />
        </div>
    </div>
</asp:Content>
