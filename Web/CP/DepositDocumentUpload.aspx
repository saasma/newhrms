﻿<%@ Page Title="Deposit Document Upload" Language="C#" MasterPageFile="~/Master/ForPopupPage.Master" AutoEventWireup="true"
    CodeBehind="DepositDocumentUpload.aspx.cs" Inherits="Web.CP.DepositDocumentUpload" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">

        function closeWindow(returnValue) {

            // alert(window.opener.parentReloadCallbackFunction)
            if (isNonModalBrowser() && typeof (window.opener.refreshDocument)) {
                window.opener.refreshDocument(returnValue, window);
            } else {
                window.returnValue = returnValue;
                window.close();
            }



        }

       
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <style type="text/css">
        div.fields p label.aud
        {
            width: 80px;
            padding: 5px 5px 0 0px;
        }
        div.fields p input.file
        {
            width: 150px;
            height: 25px;
        }
        .marginleft
        {
            margin-left: 72px !important;
        }
    </style>
    <div class="popupHeader">
    </div>
    <div style="width: 450px; margin: 20px auto 0px auto;">
        <div class="bevel">
            <div class="fields">
                <table>
                    <tr id="rowDesc" runat="server">
                        <td style='width:60px!important;vertical-align:top'>
                           
                                Description
                           
                        </td>
                        <td>
                            <asp:TextBox Width="200px"  Rows="3" ID="txtDesc" runat="server" TextMode="MultiLine" />
                        </td>
                    </tr>
                    <tr>
                        <td style='width:100px!important'>
                            
                                Select File
                           
                        </td>
                        <td>
                            <asp:FileUpload ID="fup" Width="150px" CssClass="file" runat="server" />
                            <asp:RequiredFieldValidator ValidationGroup="File"  ControlToValidate="fup" Display="None" Text="Please select a file."
                                ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please select a file."></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td style='width:100px!important'>
                            
                        </td>
                        <td style='text-align:right'>
                        <asp:Button ID="btnUpload"   OnClientClick="valGroup='File';return CheckValidation();"   CssClass="update marginleft" runat="server" OnClick="btnUpload_Click"
                                Text="Upload" />
                        </td>
                    </tr>
                </table>
                <%--<asp:ValidationSummary runat="server" ShowSummary="false" ShowMessageBox="true" />--%>
            </div>
        </div>
    </div>
</asp:Content>
