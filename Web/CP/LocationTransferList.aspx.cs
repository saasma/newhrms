﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using System.Text;
using BLL.Manager;
using Utils.Helper;
using DAL;
using System.Drawing;
using BLL.Base;


namespace Web.CP
{
    public partial class LocationTransferList : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialise();
            }

            if (Request.Form["__EVENTTARGET"] != null && Request.Form["__EVENTTARGET"].Equals("Reload"))
                BindLocationTransfer();

            JavascriptHelper.AttachPopUpCode(Page, "popLocationTransferDetls", "LocationTransferPopUp.aspx", 620, 500);
        }

        private void Initialise()
        {
            BindLocationTransfer();
        }

        private void BindLocationTransfer()
        {
            int totalRecords = 0;
            gvwList.DataSource = NewHRManager.GetLocationTransfers(pagingCtl.CurrentPage - 1, int.Parse(pagingCtl.DDLRecords.SelectedValue), txtEmpSearchText.Text.Trim(), -1, ref totalRecords);
            gvwList.DataBind();

            if (totalRecords == 0)
                pagingCtl.Visible = false;
            else
                pagingCtl.Visible = true;
            pagingCtl.UpdatePagingBar(totalRecords);
        }

        protected void btnLoad_Click(object sender, EventArgs e)
        {
            BindLocationTransfer();
        }

        protected void txtEmpSearchText_TextChanged(object sender, EventArgs e)
        {
            BindLocationTransfer();
        }

        protected void btnPrevious_Click()
        {
            pagingCtl.CurrentPage -= 1;
            BindLocationTransfer();
        }

        protected void btnNext_Click()
        {
            pagingCtl.CurrentPage += 1;
            BindLocationTransfer();
        }

        protected void ddlRecords_SelectedIndexChanged()
        {
            pagingCtl.CurrentPage = 1;
            BindLocationTransfer();
        }

        protected void gvwList_OnRowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            string errMessage = string.Empty;
            int locationHistoryId = (int)gvwList.DataKeys[e.RowIndex][0];

            Status status = NewHRManager.DeleteLocationTransfer(locationHistoryId);

            if (status.IsSuccess)
            {
                JavascriptHelper.DisplayClientMsg("Record deleted successfully.", this.Page);
                BindLocationTransfer();
            }
            else
            {
                divWarningMsg.InnerHtml = errMessage;
                divWarningMsg.Hide = false;
            }
            

        }


    }
}