﻿<%@ Page Language="C#"  Title="Retirement Calculation" AutoEventWireup="true" CodeBehind="RetirementCalculation.aspx.cs" Inherits="Web.CP.RetirementCalculation" %>
<%@ Register src="../UserControls/Calculation.ascx" tagname="Calculation" tagprefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
      <link id="Link1" runat="server" rel="stylesheet" href="~/payrollmenu_files/mbcsmbmcp.css" type="text/css" />
  
    <link id="Link2" runat="server" rel="stylesheet" type="text/css" href="~/css/core.css" />
    <link id="Link3" runat="server" rel="Stylesheet" type="text/css" href="~/Styles/calendar/calendar.css" />
  <%--  <link runat="server" rel="stylesheet" href="~/mega_menu/payroll_menu.css" />--%>
     <link id="Link4" runat="server" rel="stylesheet" href="~/Styles/design.css" />
    <script src="../Scripts/jquery-1.3.2.min.js"  type="text/javascript"></script>
    <script src="../Scripts/ScrollTableHeader-v1.03.js" type="text/javascript"></script>
    <script type="text/javascript">

        var loadingButtonWhenSubmitting = true;
        var skipLoadingCheck = false;
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="contentArea">
    <h2 runat="server" style="padding-left:10px" id="title">Save Retirement Calculation</h2>
    <div>
     <input type="button" class='spinner' value="loading" style='width:0px;height:0px;display:none'/>
    <asp:ScriptManager ID="ScriptManager1" runat="server" ScriptMode="Release">
        <Services>
            <asp:ServiceReference Path="~/PayrollService.asmx" />
        </Services>
    </asp:ScriptManager>
   
     <uc1:Calculation Id="Calculation1" runat="server" />
    </div>
    </div>
    </form>
</body>
</html>
