﻿<%@ Page Title="Allowance Forward" Language="C#" AutoEventWireup="true" MasterPageFile="~/Master/NewDetails.Master"
    CodeBehind="AllowanceForward.aspx.cs" Inherits="Web.NewHR.AllowanceForward" %>

<%@ Register Src="~/Controls/FooterCtl.ascx" TagName="FooterCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/HeaderCtl.ascx" TagName="HeaderCtl" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .holiday, .holiday a, .holiday td
        {
            color: #469146;
            background-color: #F0FFF0 !important;
        }
        .leave, .leave a, .leave td
        {
            color: #D95CA9;
            background-color: #FFF0F5 !important;
        }
        
        .weeklyholiday, .weeklyholiday a, .weeklyholiday td
        {
            color: #8F8F1A;
            background-color: #FAFAD2 !important;
        }
    </style>
    <script type="text/javascript">


   var getRowClass = function (record) {
    
        var status = record.data.PaymentStatus;
        

        if(status  == "Paid")
         {
            return "holiday";
         }
        
         else //if(dayValue=="Working Day")
         {
            return "";
         }
         //else 

      };

  
    function refreshEventList(popupWindow) {
            if (typeof (popupWindow) != 'undefined')
                popupWindow.close();

            searchList();

        }

    var CommandHandler = function (command, record) {
            
        };

    function searchList() {
        <%=PagingToolbar1.ClientID %>.doRefresh();
    }

    var renderSelect = function(e1,e2,e3,e4)
    {
        if(e2.record.data.StatusModified=='Approved')
            return '<div class="x-grid-row-checker">&nbsp;</div>';
        else
            return '';
    }
    var gridRowBeforeSelect = function (e1,e2,e3,e4,e5) {
       
        {
            if(e2.data.StatusModified!="Approved")
            {
                return false;
            }
        }

        
    };
      
      var renderEdit = function(e1,e2,e3)
      {
        
            //var theText = requestid.getAttribute("value");
            //
            return '<a href="javascript:void(0);" onclick="processEdit(' + e2.record.data.RequestID + ');return false;">Edit</a>';
      }

      var CommandHandler = function(command,record)
      {
        positionHistoryPopup("isPopup=true&reqid=" + record.data.RequestID);
      }
      function assignOvertime() {

            assignovertimePopup("isPopup=true&assign=true");
        }


        // Fix for Group panel Expand/Collapse case : Uncaught TypeError: Cannot read property 'isCollapsedPlaceholder' of undefined

     Ext.view.Table.override({
        indexInStore: function(node) {
            node = (node && node.isCollapsedPlaceholder) ? this.getNode(node) : this.getNode(node, false);

            if (!node && node !== 0) {
                return -1;
            }

            var recordIndex = node.getAttribute('data-recordIndex');

            if (recordIndex) {
                return parseInt(recordIndex, 10);
            }

            return this.dataSource.indexOf(this.getRecord(node));
        }
    });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>
    <%-- <ext:ResourceManager ID="ResourceManager1" runat="server" DisableViewState="false"
        ShowWarningOnAjaxFailure="false" ScriptMode="Release" Namespace="CompanyX" />--%>
    <ext:Hidden ID="hdnEmployeeID" runat="server" />
    <ext:Hidden ID="hdnOTDate" runat="server" />
    <ext:Hidden ID="hdnIsApprovedOnly" runat="server" />
    <ext:Hidden ID="hdnPeriodId" runat="server" />
    <ext:Hidden ID="hdnSortBy" runat="server" />
    <div class="contentpanel" style='padding-top: 0px;'>
        <div class="innerLR">
            <h3>
                Allowance Requests</h3>
            <div class="alert alert-info" style="margin-bottom: 0px;">
                <table>
                    <tr>
                        <td>
                            <ext:ComboBox Width="120" MarginSpec="0 5 0 5" LabelSeparator="" LabelAlign="Top"
                                QueryMode="Local" ID="cmbType" ForceSelection="true" DisplayField="Name" ValueField="EveningCounterTypeId"
                                runat="server" FieldLabel="Allowance Type">
                                <Store>
                                    <ext:Store ID="Store1" runat="server">
                                        <Model>
                                            <ext:Model ID="Model3" IDProperty="EveningCounterTypeId" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="EveningCounterTypeId" Type="String" />
                                                    <ext:ModelField Name="Name" Type="String" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <SelectedItems>
                                    <ext:ListItem Index="0" />
                                </SelectedItems>
                            </ext:ComboBox>
                        </td>
                        <td>
                            <ext:ComboBox Width="100" MarginSpec="0 5 0 5" LabelSeparator="" LabelAlign="Top"
                                QueryMode="Local" ID="cmbPeriodType" ForceSelection="true" runat="server" FieldLabel="Period">
                                <Items>
                                    <ext:ListItem Text="All" Value="-1">
                                    </ext:ListItem>
                                    <ext:ListItem Text="This Week" Value="1">
                                    </ext:ListItem>
                                    <ext:ListItem Text="This Month" Value="2">
                                    </ext:ListItem>
                                    <ext:ListItem Text="Last Month" Value="3">
                                    </ext:ListItem>
                                </Items>
                                <SelectedItems>
                                    <ext:ListItem Index="0" />
                                </SelectedItems>
                            </ext:ComboBox>
                        </td>
                        <td runat="server" id="t2">
                            <ext:DateField ID="dateFrom" LabelSeparator="" MarginSpec="0 5 0 5" runat="server"
                                Width="100px" FieldLabel="From" LabelAlign="Top">
                                <Plugins>
                                    <ext:GenericPlugin ID="HighlightDates1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                                </Plugins>
                            </ext:DateField>
                        </td>
                        <td runat="server" id="t3">
                            <ext:DateField ID="dateTo" LabelSeparator="" MarginSpec="0 5 0 5" runat="server"
                                Width="100px" FieldLabel="To" LabelAlign="Top">
                                <Plugins>
                                    <ext:GenericPlugin ID="GenericPlugin1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                                </Plugins>
                            </ext:DateField>
                        </td>
                        <td>
                            <ext:ComboBox Width="150" MarginSpec="0 5 0 5" LabelSeparator="" LabelAlign="Top"
                                QueryMode="Local" ID="cmbBranch" ForceSelection="true" DisplayField="Name" ValueField="BranchId"
                                runat="server" FieldLabel="Branch">
                                <Store>
                                    <ext:Store ID="Store2" runat="server">
                                        <Model>
                                            <ext:Model ID="Model4" IDProperty="BranchId" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="BranchId" Type="String" />
                                                    <ext:ModelField Name="Name" Type="String" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <Triggers>
                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                </Triggers>
                                <Listeners>
                                    <Select Handler="this.getTrigger(0).show();" />
                                    <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                    <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                                </Listeners>
                            </ext:ComboBox>
                        </td>
                        <td>
                            <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                                <Proxy>
                                    <ext:AjaxProxy Url="../Handler/EmpSearchID.ashx">
                                        <ActionMethods Read="GET" />
                                        <Reader>
                                            <ext:JsonReader Root="plants" TotalProperty="total" />
                                        </Reader>
                                        <ExtraParams >
                                            <ext:Parameter Name="RetiredAlso" Value="true" />
                                        </ExtraParams>
                                    </ext:AjaxProxy>
                                </Proxy>
                                <Model>
                                    <ext:Model ID="Model2" IDProperty="Name" Name="ItemLineModel" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Name" Type="String" />
                                            <ext:ModelField Name="EmployeeId" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                            <ext:ComboBox LabelSeparator="" MarginSpec="0 5 0 5" FieldLabel="Employee" ID="cmbSearch"
                                LabelAlign="Top" LabelWidth="70" runat="server" DisplayField="Name" ValueField="EmployeeId"
                                EmptyText="Search" StoreID="storeSearch" TypeAhead="false" Width="150" HideBaseTrigger="true"
                                MinChars="1" TriggerAction="All" ForceSelection="true">
                                <ListConfig LoadingText="Searching..." MinWidth="200">
                                    <ItemTpl ID="ItemTpl1" runat="server">
                                        <Html>
                                            <tpl>
                                            <div class="search-item">
                                                            <span>{Name}</span>  
                                             </div>
					                </tpl>
                                        </Html>
                                    </ItemTpl>
                                </ListConfig>
                                <Triggers>
                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                </Triggers>
                                <Listeners>
                                    <Select Handler="this.getTrigger(0).show();" />
                                    <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                    <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                                </Listeners>
                            </ext:ComboBox>
                        </td>
                        <td runat="server" id="Td1">
                            <pr:CalendarExtControl Width="100px" ID="calPostedMonth" runat="server" LabelAlign="Top"
                                LabelSeparator="" FieldLabel="Posted Month" />
                        </td>
                        <td valign="bottom" style="padding-left: 10px;">
                            <ext:Button runat="server" Width="100" Height="30" Cls="btn btn-default" ID="btnLoad"
                                Text="Load">
                                <Listeners>
                                    <Click Handler="searchList();" />
                                </Listeners>
                            </ext:Button>
                        </td>
                        <td valign="bottom" style="padding-left: 10px;">
                            <ext:Button runat="server" Width="130" Height="30" Cls="btn btn-primary" ID="btnAssign"
                                Text="Assign Allowance">
                                <Listeners>
                                    <Click Handler="assignOvertime();" />
                                </Listeners>
                            </ext:Button>
                        </td>
                    </tr>
                </table>
            </div>
            <br />
            <table>
                <tr>
                    <td>
                        <ext:TabPanel ID="tabPanel" runat="server" MinHeight="30" ActiveTabIndex="3" Width="600">
                            <Items>
                                <ext:Panel ID="tabAll" runat="server" Title="All">
                                </ext:Panel>
                                <ext:Panel ID="tabPending" runat="server" Title="Pending">
                                </ext:Panel>
                                <ext:Panel ID="tabRecommended" runat="server" Title="Recommended">
                                </ext:Panel>
                                <ext:Panel ID="tabApproved" runat="server" Title="Approved">
                                </ext:Panel>
                                <ext:Panel ID="tabRejected" runat="server" Title="Rejected">
                                </ext:Panel>
                                <ext:Panel ID="tabForwarded" runat="server" Title="Forwarded / Posted">
                                </ext:Panel>
                            </Items>
                            <Listeners>
                                <TabChange Handler="searchList();" />
                            </Listeners>
                        </ext:TabPanel>
                    </td>
                    <td style="padding-left: 20px;">
                        <span id='spanText' style="color: blue"></span>
                    </td>
                </tr>
            </table>
            <ext:GridPanel ID="gridList" runat="server" Header="true" AutoScroll="true">
                <Store>
                    <ext:Store ID="Store3" runat="server" OnReadData="Store_ReadData" RemoteSort="true"
                        AutoLoad="true" PageSize="20" GroupField="EmployeeName">
                        <Proxy>
                            <ext:PageProxy />
                        </Proxy>
                        <Sorters>
                            <ext:DataSorter Direction="ASC" Property="EmployeeName" />
                        </Sorters>
                        <AutoLoadParams>
                            <ext:Parameter Name="start" Value="0" Mode="Raw" />
                        </AutoLoadParams>
                        <Model>
                            <ext:Model ID="Model1" runat="server" IDProperty="RequestID">
                                <Fields>
                                    <ext:ModelField Name="OvertimeType" />
                                    <ext:ModelField Name="EmployeeID" />
                                    <ext:ModelField Name="EmployeeName" />
                                    <ext:ModelField Name="StartTime" Type="Date" />
                                    <ext:ModelField Name="EndTime" Type="Date" />
                                    <ext:ModelField Name="StartDateNep" />
                                    <ext:ModelField Name="EndDateNep" />
                                    <ext:ModelField Name="Duration" />
                                    <ext:ModelField Name="Reason" />
                                    <ext:ModelField Name="SupervisorName" />
                                    <ext:ModelField Name="StatusModified" />
                                    <ext:ModelField Name="Branch" />
                                    <ext:ModelField Name="Date"  Type="Date"/>
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <Features>
                    <ext:Grouping StartCollapsed="false" IsDynamic="true" ID="Group1" runat="server"
                        HideGroupedHeader="false" EnableGroupingMenu="true" />
                </Features>
                <%-- <Plugins>
                    <ext:CellEditing ID="CellEditing1" ClicksToEdit="1" runat="server">
                    </ext:CellEditing>
                </Plugins>--%>
                <ColumnModel ID="ColumnModel2" runat="server">
                    <Columns>
                        <ext:RowNumbererColumn runat="server" Text="SN" Width="40" />
                        <ext:Column ID="colEId" Locked="true" runat="server" Text="EIN" Width="50" DataIndex="EmployeeID"
                            Align="Center" Sortable="true" MenuDisabled="false">
                        </ext:Column>
                        <ext:Column ID="Column1" Locked="true" runat="server" Text="Name" Width="150" DataIndex="EmployeeName"
                            Sortable="true" MenuDisabled="false">
                        </ext:Column>
                        <ext:Column ID="Column9" Locked="true" runat="server" Text="AllowanceType" Width="120"
                            DataIndex="OvertimeType" Align="Left" Sortable="true" MenuDisabled="false">
                        </ext:Column>
                        <ext:Column ID="Column3" Locked="false" runat="server" Text="Branch" Width="100"
                            DataIndex="Branch" Align="Left" Sortable="true" MenuDisabled="false">
                        </ext:Column>
                        <ext:DateColumn ID="Column10" Format="yyyy-MMM-dd" runat="server" Text="Start Date"
                            Width="100" DataIndex="StartTime" Sortable="true" MenuDisabled="false">
                        </ext:DateColumn>
                        <ext:DateColumn ID="DateColumn1" Format="yyyy-MMM-dd" runat="server" Text="End Date"
                            Width="100" DataIndex="EndTime" Sortable="true" MenuDisabled="false">
                        </ext:DateColumn>
                        <ext:Column ID="Column11" runat="server" Text="Start Date(BS)" Width="100" DataIndex="StartDateNep"
                            Sortable="true" MenuDisabled="false">
                        </ext:Column>
                        <ext:Column ID="Column12" runat="server" Text="End Date(BS)" Width="100" DataIndex="EndDateNep"
                            Sortable="true" MenuDisabled="true">
                        </ext:Column>
                        <ext:Column ID="Column2" runat="server" Align="Center" Text="Units" Width="70" DataIndex="Duration"
                            Sortable="true" MenuDisabled="true">
                        </ext:Column>
                        <ext:Column ID="Column7" runat="server" Align="Left" Text="Reason" Width="120" DataIndex="Reason"
                            Sortable="false" MenuDisabled="true">
                        </ext:Column>
                        <ext:Column ID="Column6" runat="server" Align="Left" Text="Processed By" Width="100"
                            DataIndex="SupervisorName" Sortable="false" MenuDisabled="true">
                        </ext:Column>
                        <ext:Column ID="Column5" runat="server" Text="Status" Width="120" DataIndex="StatusModified"
                            Sortable="true" MenuDisabled="false">
                        </ext:Column>
                       <ext:DateColumn ID="DateColumn21" Format="yyyy-MMM-dd" runat="server" Text="Created On"
                            Width="100" DataIndex="Date" Sortable="true" MenuDisabled="false">
                        </ext:DateColumn>
                        <%--   <ext:Column runat="server" Width="100">
                            <Renderer Fn="renderEdit" />
                        </ext:Column>--%>
                        <ext:CommandColumn ID="CommandColumn1" runat="server" Text="" Width="100">
                            <Commands>
                                <ext:CommandSpacer Width="5" />
                                <ext:GridCommand Cls="editGridButton" Text="Edit" CommandName="Edit" />
                            </Commands>
                            <Listeners>
                                <Command Handler="CommandHandler(command,record);" />
                            </Listeners>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
                <Listeners>
                    <SelectionChange Handler="document.getElementById('spanText').innerHTML = this.getSelectionModel().selected.items.length + ' requests selected';" />
                </Listeners>
                <SelectionModel>
                    <ext:CheckboxSelectionModel ID="CheckboxSelectionModel1" runat="server" Mode="Simple">
                        <Listeners>
                            <BeforeSelect Fn="gridRowBeforeSelect" />
                        </Listeners>
                        <Renderer Fn="renderSelect">
                        </Renderer>
                    </ext:CheckboxSelectionModel>
                </SelectionModel>
                <View>
                    <ext:GridView ID="GridView1" EnableTextSelection="true" runat="server">
                        <Listeners>
                        </Listeners>
                        <%-- <GetRowClass Fn="getRowClass" />--%>
                    </ext:GridView>
                </View>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar1" runat="server" StoreID="storeEmpList" DisplayInfo="true">
                        <Items>
                            <ext:Label ID="lblPageSize" runat="server" Text="Page size:" />
                            <ext:ToolbarSpacer ID="ToolbarSpacer_listItems" runat="server" Width="10" />
                            <ext:ComboBox ID="cmbPageSize" runat="server" Width="80" SelectOnFocus="true" Selectable="true"
                                ValueField="Value" DisplayField="Text" ForceSelection="true" AllowBlank="false">
                                <Listeners>
                                    <Select Handler="#{Store3}.pageSize = this.getValue();searchList()" />
                                    <%--#{cmbPageSize}.pageSize = this.getValue();#{PagingToolbar1}.moveFirst();--%>
                                </Listeners>
                                <Items>
                                    <ext:ListItem Value="20" Text="20" />
                                    <ext:ListItem Value="50" Text="50" />
                                </Items>
                                <SelectedItems>
                                    <ext:ListItem Index="0">
                                    </ext:ListItem>
                                </SelectedItems>
                            </ext:ComboBox>
                        </Items>
                    </ext:PagingToolbar>
                </BottomBar>
            </ext:GridPanel>
            <div class="buttonBlock" style="margin-top: 10px">
                <table class="fieldTable firsttdskip">
                    <tr>
                        <td>
                            <ext:Button ID="btnPost" Cls="btn btn-success" runat="server" Text="Post To Salary"
                                Width="120" Height="30">
                                <DirectEvents>
                                    <Click OnEvent="btnPost_Click">
                                        <EventMask ShowMask="true" />
                                        <Confirmation ConfirmRequest="true" Message="Are you sure, you want to Forward the requests?" />
                                        <ExtraParams>
                                            <ext:Parameter Name="gridItems" Value="Ext.encode(#{gridList}.getRowsValues({selectedOnly:true}))"
                                                Mode="Raw" />
                                        </ExtraParams>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </td>
                        <td style="padding-left: 10px">
                            <ext:Button ID="btnReject" Cls="btn btn-warning" runat="server" Text="Reject" Width="100"
                                Height="30">
                                <DirectEvents>
                                    <Click OnEvent="btnReject_Click">
                                        <EventMask ShowMask="true" />
                                        <Confirmation ConfirmRequest="true" Message="Are you sure, you want to Reject the requests?" />
                                        <ExtraParams>
                                            <ext:Parameter Name="gridItems" Value="Ext.encode(#{gridList}.getRowsValues({selectedOnly:true}))"
                                                Mode="Raw" />
                                        </ExtraParams>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </td>
                        <td style="padding-left: 10px">
                            <ext:Button ID="btnExport" Cls="btn btn-primary" IconAlign="Right" Icon="TableGo"
                                AutoPostBack="true" OnClick="btnExport_Click" runat="server" Text="Export" Width="100"
                                Height="30">
                            </ext:Button>
                        </td>
                    </tr>
                </table>
            </div>
            <br />
        </div>
    </div>
</asp:Content>
