﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using Utils.Helper;
using Utils.Web;
using BLL.Base;
using DAL;

namespace Web
{
    public partial class SettlementAdjustment : BasePage
    {

        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.Retirement;
            }
        }

        private int _tempCurrentPage;
        private int _tempCount;
        private PayManager payMgr = new PayManager();

        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!IsPostBack)
            {
               
                Initialise();

            }
            RegisterScripts();
          
            LoadEmployeesInDDL();
        }

        void RegisterScripts()
        {
            StringBuilder str = new StringBuilder();
            str.AppendFormat("var decimalPlaces = {0};", SessionManager.DecimalPlaces);
          
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "rounding",
                                                        str.ToString()
                                                        , true);
        }
       

        void Initialise()
        {
           

            ddlPayrollPeriods.DataSource = CommonManager.GetCurrentYear(SessionManager.CurrentCompanyId);
            ddlPayrollPeriods.DataBind();

            UIHelper.SetSelectedLastItemInDropDown(ddlPayrollPeriods);

           

            LoadEmployeesInDDL();

            LoadIncomeDeductionList();

        }
        protected void btnLoad_Click(object sender, EventArgs e)
        {

            //if (ddlEmployee.SelectedValue == null || ddlEmployee.SelectedValue == "")
            //    return;

            //int payrollPeriodId = int.Parse(ddlPayrollPeriods.SelectedValue);
            //int employeeId = int.Parse(ddlEmployee.SelectedValue);

            //SettlementDetailType type = (SettlementDetailType)int.Parse(ddlType.SelectedValue);

            //switch (type)
            //{
            //    case SettlementDetailType.LeaveEncash:
            //    case SettlementDetailType.ExcessiveLeave:

            //        SettlementLeaveDetail entity = EmployeeManager.GetSettlementLeaveDetail(
            //            payrollPeriodId, employeeId, type == SettlementDetailType.LeaveEncash ? true : false);

            //        if (entity != null)
            //        {
            //            //if (entity.OldLeaveEncashmentAmount == null)
            //                txtCalculatedAmount.Text = GetCurrency(entity.LeaveEncashmentAmount);
            //            //else
            //            //    txtCalculatedAmount.Text = GetCurrency(entity.OldLeaveEncashmentAmount);

            //            txtNewAmount.Text = GetCurrency(entity.LeaveEncashmentAmount);
            //        }
            //        else
            //        {
            //            txtCalculatedAmount.Text = "0";
            //            txtNewAmount.Text = "0";
            //        }


            //        break;
            //    default:

            //         SettlementDetail sentity = EmployeeManager.GetSettlementDetail(
            //            payrollPeriodId, employeeId, (int)type);

            //         if (sentity != null)
            //        {
            //            if (sentity.OldValue== null)
            //                txtCalculatedAmount.Text = GetCurrency(sentity.Value);
            //            else
            //                txtCalculatedAmount.Text = GetCurrency(sentity.OldValue);

            //            txtNewAmount.Text = GetCurrency(sentity.Value);
            //        }
            //        else
            //        {
            //            txtCalculatedAmount.Text = "0";
            //            txtNewAmount.Text = "0";
            //        }

            //        break;
            //}

           // LoadEmployees();
        }

        protected void LoadEmployees()
        {

            //if (ddlType.SelectedValue.Equals("-1"))
            //{
             

            //    divWarningMsg.InnerHtml = "Type should be selected for adjustment.";
            //    divWarningMsg.Hide = false;

                
            //    return;
            //}


            //if (IsPayrollFinalSaved(int.Parse(ddlPayrollPeriods.SelectedValue)))
            //{
            //    divWarningMsg.InnerHtml = Resources.Messages.CalcAllEmployeeFinalSavedMsg;
            //    divWarningMsg.Hide = false;
            //}
            //else
            //{
                
            //    //foreach (GetIncomeDeductionAdjustmentEmployeesResult item in list)
            //    //{
            //    //    if (item.IsCalculationSaved != null && item.IsCalculationSaved == 1)
            //    //    {
            //    //        divWarningMsg.InnerHtml = Resources.Messages.SomeOfTheEmployeeSalaryAlreadySavedMsg;
            //    //        divWarningMsg.Hide = false;
            //    //        break;
            //    //    }
            //    //}
            //}
            
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
           // Page.Validate("Balance");

           // decimal oldAmount, newAmount;
           // //string notes;
           // int employeeId, payrollPeriodId;
           //// int type;
            
           // if (Page.IsValid)
           // {



           //     payrollPeriodId = int.Parse(ddlPayrollPeriods.SelectedValue);
           //     employeeId = int.Parse(ddlEmployee.SelectedValue);

           //     SettlementDetailType type = (SettlementDetailType)int.Parse(ddlType.SelectedValue);

           //     switch (type)
           //     {
           //         case SettlementDetailType.LeaveEncash:
                    

           //             SettlementLeaveDetail entity = new SettlementLeaveDetail();

           //             entity.EmployeeId = employeeId;
           //             entity.PayrollPeriodId = payrollPeriodId;
           //             entity.LeaveEncashmentDaysOrHours = 0;
           //             //entity.OldLeaveEncashmentAmount = decimal.Parse(txtCalculatedAmount.Text);
           //             entity.LeaveEncashmentAmount = decimal.Parse(txtNewAmount.Text);

           //             EmployeeManager.SaveSettlementLeaveDetail(entity,payrollPeriodId,employeeId,true);

           //             break;
           //         case SettlementDetailType.ExcessiveLeave:

           //               SettlementLeaveDetail entity1 = new SettlementLeaveDetail();

           //               entity1.EmployeeId = employeeId;
           //               entity1.PayrollPeriodId = payrollPeriodId;
           //               entity1.LeaveEncashmentDaysOrHours = 0;
           //               //entity1.OldLeaveEncashmentAmount = decimal.Parse(txtCalculatedAmount.Text);
           //               entity1.LeaveEncashmentAmount = decimal.Parse(txtNewAmount.Text);

           //               EmployeeManager.SaveSettlementLeaveDetail(entity1, payrollPeriodId, employeeId, false);

           //             break;
           //         default:


           //             SettlementDetail entity2 = new SettlementDetail();

           //             entity2.EmployeeId = employeeId;
           //             entity2.PayrollPeriodId = payrollPeriodId;
           //             entity2.Type = (int)type;

           //             entity2.OldValue = decimal.Parse(txtCalculatedAmount.Text);
           //             entity2.Value = decimal.Parse(txtNewAmount.Text);

           //             EmployeeManager.SaveSettlementDetail(entity2, payrollPeriodId, employeeId, (int)type);

           //             break;
           //     }


           //     divMsgCtl.InnerHtml = "Saved";
           //     divMsgCtl.Hide = false;
           // }


        }


        protected void btnCancel_Click(object sender, EventArgs e)
        {
            LoadEmployees();
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            StringBuilder xmlEmployees = new StringBuilder();
            xmlEmployees.Append("<root>");
            bool employeeAdded = false;
          
         
        }

        //protected void btnPostToSalary_Click(object sender, EventArgs e)
        //{

        //    foreach (GridViewRow row in gvw.Rows)
        //    {


        //        int payrollPeriodId = (int)gvw.DataKeys[row.RowIndex]["PayrollPeriodId"];

        //        PayManager.PostOvertimePay(payrollPeriodId);
        //        msgCtl.InnerHtml = Resources.Messages.OvertimePostedMessage;
        //        msgCtl.Hide = false;
        //        break;
        //    }


        //}
        protected void txtEmployeeName_TextChanged(object sender, EventArgs e)
        {
            

        }

        private void LoadIncomeDeductionList()
        {
            //ListItem item = ddlIncomeOrDeduction.Items[0];
            //ddlIncomeOrDeduction.Items.Clear();

            //ddlIncomeOrDeduction.DataSource = PayManager.GetEmployeesIncomeDeductionList(int.Parse(ddlPayrollPeriods.SelectedValue));
            //ddlIncomeOrDeduction.DataBind();

            //ddlIncomeOrDeduction.Items.Insert(0, item);
        }

        protected void ddlPayrollPeriods_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadEmployeesInDDL();

           // LoadIncomeDeductionList();
        }

        protected void ddlDepartment_SelectedIndexChanged1(object sender, EventArgs e)
        {
            LoadEmployeesInDDL();
        }

        private void LoadEmployeesInDDL()
        {
            //ListItem item = ddlEmployee.Items[0];
            //ddlEmployee.Items.Clear();

            ddlEmployee.DataSource = EmployeeManager.GetRetiredResignedEmployeeForPayrollPeriod(
                int.Parse(ddlPayrollPeriods.SelectedValue));
            ddlEmployee.DataBind();

            //if (ddlIncomeOrDeduction.SelectedItem.Text.Trim() == CalculationColumnType.TDS.ToString())
            //{
            //    gvw.Columns[3].Visible = true;
            //}
            //else
            //{
            //    gvw.Columns[3].Visible = false;
            //}

            //ddlEmployee.Items.Insert(0, item);
        }

        protected void btnPrevious_Click()
        {
            //pagingCtl.CurrentPage -= 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage -= 1;
            LoadEmployees();
        }

        protected void btnNext_Click()
        {
         //   pagingCtl.CurrentPage += 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage += 1;
            LoadEmployees();
        }

        protected void ddlRecords_SelectedIndexChanged()
        {
            //SessionManager.EmployeeDisplayRecodsPerPage = int.Parse(pagintCtl.DDLRecords.SelectedValue);
            //_tempCurrentPage = 1;
           // pagingCtl.CurrentPage = 1;
            LoadEmployees();
        }

    }
}
