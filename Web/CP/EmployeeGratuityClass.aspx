<%@ Page Title="Employee Gratuity Class" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="EmployeeGratuityClass.aspx.cs" Inherits="Web.EmployeeGratuityClass" %>

<%@ Import Namespace="BLL.Manager" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   
    <script src="../Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        function refreshWindow() {

            __doPostBack('Refresh', 0);

        }
        jQuery(document).ready(
    function () {

        setMovementToGrid('#<%= gvEmployeeIncome.ClientID %>');
    }
);

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Employee Gratuity Class
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="attribute" style="padding: 10px">
            <table>
                <tr>
                    <td>
                        Search &nbsp;
                        <asp:TextBox ID="txtEmployeeName" runat="server" OnTextChanged="txtEmployeeName_TextChanged"
                            AutoPostBack="true" Width="146px" />
                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtEmployeeName"
                            WatermarkText="Search Employee" WatermarkCssClass="searchBoxText" />
                        <cc1:AutoCompleteExtender EnableCaching="true" ID="AutoCompleteExtenderOrganization"
                            runat="server" MinimumPrefixLength="2" ServiceMethod="GetEmployeeNames" ServicePath="~/PayrollService.asmx"
                            TargetControlID="txtEmployeeName" CompletionSetCount="10" CompletionInterval="250"
                            CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList"
                            CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
                        </cc1:AutoCompleteExtender>
                    </td>
                    <td>
                        <asp:LinkButton Visible="false" ID="btnExport" runat="server" Text="Excel import" OnClientClick="taxImportPopup();return false;"
                            CssClass=" excel marginRight tiptip" Style="float: left;" />
                    </td>
                </tr>
            </table>
        </div>
        <div class="clear">
            <cc1:TextBoxWatermarkExtender ID="TBWE2" runat="server" TargetControlID="txtEmployeeName"
                WatermarkText="Enter employee name" WatermarkCssClass="watermarked" />
            <cc2:EmptyDisplayGridView CssClass="tableLightColor" UseAccessibleHeader="true" ShowHeaderWhenEmpty="True"
                ID="gvEmployeeIncome" runat="server" DataKeyNames="EmployeeId" AutoGenerateColumns="False"
                CellPadding="4" GridLines="None" AllowSorting="True" ShowFooterWhenEmpty="False"
               OnRowDataBound="gv_RowDataBound"
                OnPageIndexChanging="gvwRoles_PageIndexChanging" 
                OnRowEditing="gvwProjects_RowEditing" OnRowUpdating="gvwProjects_RowUpdating">
                <RowStyle BackColor="#E3EAEB" />
                <Columns>
                   
                     <asp:TemplateField HeaderText="EIN" HeaderStyle-Width="50px">
                        <ItemTemplate>
                            <asp:Label ID="lblName11" Text='<%# Eval("EmployeeId") %>' Style="width: 50px!important"
                                runat="server" />
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" Width="150px" />
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Name" HeaderStyle-Width="150px">
                        <ItemTemplate>
                            <asp:Label ID="lblName" Text='<%# Eval("Name") %>' Style="width: 100px!important"
                                runat="server" />
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" Width="150px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Gratuity Class" HeaderStyle-Width="200px">
                        <ItemTemplate>
                            <%# Eval("GratuityClassName")%></ItemTemplate>
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlGratuityClass" Width="200" runat="server" DataTextField="Name"
                                DataValueField="GratuityClassID" AppendDataBoundItems="true">
                                <asp:ListItem Text="--Select Class--" Value="-1" />
                            </asp:DropDownList>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderStyle-Width="50px">
                        <ItemTemplate>
                            <asp:ImageButton ID="ImageButton1" runat="server" CommandName="Edit" ToolTip="Edit"
                                ImageUrl="~/images/edit.gif" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ImageButton1" runat="server" ToolTip="Save" CommandName="Update"
                                ImageUrl="~/css/images/save.png" />
                        </EditItemTemplate>
                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                    </asp:TemplateField>
                </Columns>
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                <EmptyDataTemplate>
                    <b>No employee list. </b>
                </EmptyDataTemplate>
            </cc2:EmptyDisplayGridView>
            <uc1:PagingCtl ID="pagingCtl" OnNextRecord="btnNext_Click" OnPrevRecord="btnPrevious_Click"
                OnDropDownShowRecordsChanged="ddlRecords_SelectedIndexChanged" runat="server" />
        </div>
        <div class="buttonsDiv">
            <%--    <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Balance" DisplayMode="List"
            ShowSummary="false" ShowMessageBox="true" runat="server"></asp:ValidationSummary>--%>
            <asp:Button  ID="btnUpdate" CssClass="update" OnClientClick="valGroup='Balance';return CheckValidation();"
                Visible="false" ValidationGroup="Balance" runat="server" Text="Update"/>
        </div>
    </div>
</asp:Content>
