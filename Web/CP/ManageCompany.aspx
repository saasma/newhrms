<%@ Page Title="Company list" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="ManageCompany.aspx.cs" Inherits="Web.CP.ManageCompany" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
<div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Your Organization Information
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
    <div class="contentArea">        
        <asp:GridView CssClass="table table-primary mb30 table-bordered table-hover" UseAccessibleHeader="true" ID="gvwCompanies"
            runat="server" CellPadding="3" DataKeyNames="CompanyId" AutoGenerateColumns="False"
            GridLines="None" Width="100%" OnRowCreated="gvwCompanies_RowCreated" OnRowDeleting="gvwCompanies_RowDeleting">
            <Columns>
                <asp:BoundField HeaderStyle-HorizontalAlign="Left" DataField="CompanyId" HeaderText="S NO" Visible="false" />
                <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                    DataField="Name" HeaderText="Company Name">
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                    DataField="ResponsiblePerson" HeaderText="Contact Person">
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                    DataField="Address" HeaderText="Address">
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                    DataField="PhoneNo" HeaderText="Phone No">
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundField>
                <asp:TemplateField HeaderText="Default" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <asp:CheckBox runat="server" ID="chk" onclick='<%# "makeCompanyCurrent(this," + Eval("CompanyId") +  ")"  %>'
                            Checked='<%# IsDefault(Eval("IsDefault")) %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Edit" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <asp:HyperLink ImageUrl="~/images/edit.gif" NavigateUrl='<%# "AddEditCompany.aspx?Id=" + Eval("CompanyId") %>'
                            runat="server">                       
                        </asp:HyperLink>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Delete" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <asp:ImageButton ID="btnDelete" CommandName="Delete" ImageUrl="~/images/delet.png"
                            runat="server" OnClientClick='return CanBeDeleted(this)' AlternateText="Delete" />
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:TemplateField>
            </Columns>
            <RowStyle CssClass="odd" />
            <AlternatingRowStyle CssClass="even" />
            <EmptyDataTemplate>
                No Company has been created.
            </EmptyDataTemplate>
        </asp:GridView>
        <%--<div class="buttonsDivSection" style="margin-top:10px;">
            <asp:LinkButton ID="btnAddNew" OnClientClick="window.location ='AddEditCompany.aspx';return false;"
                runat="server" CssClass="btn btn-success btn-sect btn-sm" Text="Add a New Company"/>
        </div>--%>
    </div>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="pageScriptsContent" runat="server">
    <script type="text/javascript">
        function handleEditClick(link) {
            if (!e) var e = window.event
            // handle event
            e.cancelBubble = true;
            if (e.stopPropagation) e.stopPropagation();
        }

        function redirectToOverview(id) {
            Web.PayrollService.SetSelCompany(id, onSuccess);
        }

        function onSuccess(result) {
            if (result) {
                window.location = 'PayrollPeriodPG.aspx';
            }
        }

        function makeCompanyCurrent(chk, cId) {
            $("#<%= gvwCompanies.ClientID %> input[type=checkbox]").
                       removeAttr('checked');

            $(chk).attr('checked', true);

            Web.PayrollService.ChangeIsCurrentStatus(cId, onSuccess);
        }
        function onSuccess(result) {
            //alert(result);
            if (typeof (companyNameId) != 'undefined')
                $('#' + companyNameId).html(result);
        }

        function CanBeDeleted(btn) {

            var chkId = btn.id.replace("btnDelete", "chk");
            if (document.getElementById(chkId).checked) {
                alert("Default Company can't be deleted.");
                return false;
            }
            return true;
        }
        function onSuccessCompanyDelete(result, currentId) {
            alert(result);
        }        
    </script>
</asp:Content>
