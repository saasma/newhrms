<%@ Page Title="Employee Master" Language="C#" EnableViewState="false" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="EmployeeMaster.aspx.cs" Inherits="Web.EmployeeMaster" %>

<%@ Import Namespace="BLL.Manager" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        var skipLoadingCheck = true;
        function searchList() {
            <%=gridEmpBasicInfo.ClientID %>.getStore().pageSize = <%=cmbPageSize.ClientID %>.getValue();
            <%=PagingToolbar1.ClientID %>.doRefresh();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <ext:ResourceManager runat="server" />
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>Employee Master
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <table class="fieldTable">
            <tr>
                <td>
                    <strong>Branch</strong>
                </td>
                <td>
                    <strong>Department</strong>
                </td>
                <td>
                    <strong>Status</strong>
                </td>
                <td>
                    <strong>Employee</strong>
                </td>
                <td rowspan="2" valign="bottom">
                    <asp:Button ID="btnLoads" CssClass="btn btn-default btn-sect btn-sm" Style="width: 80px"
                        OnClick="btnLoad_Click" runat="server" Text="Load"></asp:Button>
                </td>
                <td rowspan="2" valign="bottom">
                    <asp:Button ID="btnUpdate" CssClass="excel" runat="server" Text="Export" OnClick="btnUpdate_Click" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:DropDownList ID="ddlBranch" Width="130px" DataTextField="Name" DataValueField="BranchId"
                        AutoPostBack="false" AppendDataBoundItems="true" runat="server">
                        <asp:ListItem Text="" Value="-1" />
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:DropDownList ID="ddlDepartment" Width="130px" DataTextField="Name" DataValueField="DepartmentId"
                        AutoPostBack="false" runat="server" AppendDataBoundItems="true">
                        <asp:ListItem Text="" Value="-1" />
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:DropDownList ID="ddlStatus" Width="130px" DataTextField="Value" DataValueField="Key"
                        AutoPostBack="false" runat="server" AppendDataBoundItems="true">
                        <asp:ListItem Text="" Value="-1" />
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:TextBox ID="txtEmpSearchText" Width="170px" Style='width: 200px; border-radius: 2px; margin-top: 4px; margin-left: 0px; height: 25px;'
                        runat="server"></asp:TextBox>
                    <cc1:TextBoxWatermarkExtender ID="TBWE2" runat="server" TargetControlID="txtEmpSearchText"
                        WatermarkText="Employee Search" WatermarkCssClass="searchBoxText" />
                    <cc1:AutoCompleteExtender EnableCaching="true" ID="AutoCompleteExtenderOrganization"
                        runat="server" MinimumPrefixLength="2" ServiceMethod="GetEmployeeNames" ServicePath="~/PayrollService.asmx"
                        TargetControlID="txtEmpSearchText" CompletionSetCount="10" CompletionInterval="250"
                        CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList"
                        CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
                    </cc1:AutoCompleteExtender>
                </td>
            </tr>
        </table>
        <br />
        <div class="clear gridBlock">
            <cc2:EmptyDisplayGridView CssClass="table table-primary mb30 table-bordered table-hover"
                UseAccessibleHeader="true" ShowHeaderWhenEmpty="True" ID="gvEmployeeIncome" runat="server"
                DataKeyNames="EmployeeId" AutoGenerateColumns="False" CellPadding="4" GridLines="None"
                AllowSorting="True" ShowFooterWhenEmpty="False">
                <Columns>
                    <asp:BoundField DataField="SN" HeaderText="SN"></asp:BoundField>
                    <asp:BoundField DataField="EmployeeId" HeaderText="EIN"></asp:BoundField>
                    <asp:BoundField DataField="IdCardNo" HeaderText="I No"></asp:BoundField>
                    <asp:BoundField DataField="AccountNo" HeaderText="Account No"></asp:BoundField>
                    <asp:BoundField DataField="Title" HeaderStyle-HorizontalAlign="Left" HeaderText="Title"></asp:BoundField>
                    <asp:TemplateField HeaderText="Name" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:Label runat="server" Width="140" Text='<%# Eval("Name") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Sex" HeaderStyle-HorizontalAlign="Left" HeaderText="Sex"></asp:BoundField>
                    <asp:BoundField DataField="MaritalStatus" HeaderStyle-HorizontalAlign="Left" HeaderText="M. Status"></asp:BoundField>
                    <asp:BoundField DataField="PAddress" HeaderStyle-HorizontalAlign="Left" HeaderText="Address"></asp:BoundField>
                    <asp:BoundField DataField="Course" HeaderStyle-HorizontalAlign="Left" HeaderText="Qualification"></asp:BoundField>
                    <asp:BoundField DataField="Division" HeaderStyle-HorizontalAlign="Left" HeaderText="Division"></asp:BoundField>
                    <asp:BoundField DataField="TaxStatus" HeaderStyle-HorizontalAlign="Left" HeaderText="Tax Status"></asp:BoundField>
                    <asp:BoundField DataField="Handicapped" HeaderStyle-HorizontalAlign="Left" HeaderText="Handicapped"></asp:BoundField>
                    <asp:BoundField DataField="DateOfBirth" HeaderStyle-HorizontalAlign="Left" HeaderText="DOB"></asp:BoundField>
                    <asp:BoundField DataField="DateOfBirthEng" DataFormatString="{0:dd-MMM-yyyy}" HeaderText="DOB Eng"></asp:BoundField>
                    <asp:BoundField DataField="AppointmentLetterDateEng" DataFormatString="{0:dd-MMM-yyyy}"
                        HeaderText="Appoint Letter Date"></asp:BoundField>
                    <asp:BoundField DataField="JoinDate" DataFormatString="{0:dd-MMM-yyyy}" HeaderText="Appoint/Join Date"></asp:BoundField>
                    <%--<asp:BoundField DataField="Position" HeaderStyle-HorizontalAlign="Left"   HeaderText="Position"></asp:BoundField>--%>
                    <asp:TemplateField HeaderText="Level/Position" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Width="100" Text='<%# Eval("LevelPosition") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Designation" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Width="100" Text='<%# Eval("Designation") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Branch" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Width="80" Text='<%# Eval("Branch") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Department" HeaderStyle-HorizontalAlign="Left" HeaderText="Department"></asp:BoundField>
                    <asp:TemplateField HeaderText="Shift" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Width="80" Text='<%# Eval("Shift") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Grade Step" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Width="80" Text='<%# Eval("Grade") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="CostCode" HeaderStyle-HorizontalAlign="Left" HeaderText="CostCode"></asp:BoundField>
                    <asp:BoundField DataField="Status" HeaderStyle-HorizontalAlign="Left" HeaderText="Status"></asp:BoundField>
                    <asp:BoundField DataField="StatusDate" HeaderStyle-HorizontalAlign="Left" HeaderText="Status Date"></asp:BoundField>
                    <asp:BoundField DataField="StatusDateEng" DataFormatString="{0:dd-MMM-yyyy}" HeaderStyle-HorizontalAlign="Left" HeaderText="Eng Status Date"></asp:BoundField>
                    <asp:BoundField DataField="PANNo" HeaderStyle-HorizontalAlign="Left" HeaderText="PANNo"></asp:BoundField>
                    <asp:BoundField DataField="PFNo" HeaderStyle-HorizontalAlign="Left" HeaderText="PFNo"></asp:BoundField>
                    <asp:BoundField DataField="CITNo" HeaderStyle-HorizontalAlign="Left" HeaderText="CITNo"></asp:BoundField>
                    <asp:BoundField DataField="Insurance" HeaderStyle-HorizontalAlign="Left" HeaderText="Insurance"></asp:BoundField>
                    <asp:BoundField DataField="CIT" HeaderStyle-HorizontalAlign="Left" HeaderText="CIT"></asp:BoundField>
                    <asp:BoundField DataField="Email" HeaderStyle-HorizontalAlign="Left" HeaderText="Email"></asp:BoundField>
                </Columns>
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                <EmptyDataTemplate>
                    <b>No employee list. </b>
                </EmptyDataTemplate>
            </cc2:EmptyDisplayGridView>
            <uc1:PagingCtl ID="pagingCtl" ShowDropDown="true" OnNextRecord="btnNext_Click" OnChangePage="ChangePageNumber"
                OnPrevRecord="btnPrevious_Click" OnDropDownShowRecordsChanged="ddlRecords_SelectedIndexChanged"
                runat="server" />
        </div>
        <div class="buttonsDiv">
        </div>
        <table>
            <tr>
                <td>
                    <ext:TabPanel ID="tabPanel" runat="server" MinHeight="30" ActiveTabIndex="0"  MinWidth="600">
                        <Items>
                            <ext:Panel ID="tabBasic" runat="server" Title="Basic">
                                <Items>
                                    <ext:GridPanel StyleSpec="margin-top:10px;" ID="gridEmpBasicInfo" runat="server" Cls="itemgrid" OnReadData="Store_ReadData" Scroll="Horizontal">
                                        <Store>
                                            <ext:Store ID="storeEmpInfo" runat="server" AutoLoad="true" OnReadData="Store_ReadData" RemotePaging="true" RemoteSort="true">
                                                <Proxy>
                                                    <ext:PageProxy />
                                                </Proxy>
                                                <Model>
                                                    <ext:Model ID="Model4" runat="server" IDProperty="">
                                                        <Fields>
                                                            <%-- <ext:ModelField Name="EmployeeId" Type="String" />
                                <ext:ModelField Name="IdCardNo" Type="String" />
                                <ext:ModelField Name="Name" Type="String" />
                                <ext:ModelField Name="BloodGroupName" Type="String" />
                                <ext:ModelField Name="Nationality" Type="String" />
                                <ext:ModelField Name="CitizenshipNo" Type="String" />
                                <ext:ModelField Name="IssueDate" Type="String" />
                                <ext:ModelField Name="Place" Type="String" />
                                <ext:ModelField Name="LiscenceTypeName" Type="String" />
                                <ext:ModelField Name="DrivingLicenceNo" Type="String" />
                                <ext:ModelField Name="DrivingLicenceIssueDate" Type="String" />
                                <ext:ModelField Name="IssuingCountry" Type="String" />
                                <ext:ModelField Name="PassportNo" Type="String" />
                                <ext:ModelField Name="IssuingDate" Type="String" />
                                <ext:ModelField Name="ValidUpto" Type="String" />
                                <ext:ModelField Name="Birthmark" Type="String" />
                                <ext:ModelField Name="MarriageAniversary" Type="String" />
                                <ext:ModelField Name="MotherTongue" Type="String" />
                                <ext:ModelField Name="ReligionName" Type="String" />--%>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel ID="ColumnModel1" runat="server">
                                            <Columns>
                                                <ext:Column ID="colEmployeeId" DataIndex="" runat="server" Text="EIN" MenuDisabled="true" Sortable="false" Align="Left" Width="60" Locked="true" />
                                                <ext:Column ID="colINO" runat="server" DataIndex="" Text="INO" MenuDisabled="true" Sortable="false" Align="Left" Width="80" Locked="true" />
                                                <ext:Column ID="colEmplooyeeName" runat="server" DataIndex="" Text="Name" MenuDisabled="true" Sortable="false" Align="Left" Width="150" Locked="true" />
                                                <ext:Column ID="colBloodGroup" runat="server" DataIndex="" Text="Blood Group" MenuDisabled="true" Sortable="false" Align="Left" Width="80" />
                                                <ext:Column ID="colNationality" runat="server" DataIndex="" Text="Nationality" MenuDisabled="true" Sortable="false" Align="Left" Width="120" />
                                                <ext:Column ID="colCitizenshipNo" runat="server" DataIndex="" Text="Cit. No." MenuDisabled="true" Sortable="false" Align="Left" Width="100" />
                                                <ext:Column ID="colCitIssueDates" runat="server" Text="Cit. Issue Date" MenuDisabled="true" Sortable="false" Align="Left" Width="100" DataIndex="">
                                                </ext:Column>
                                                <ext:Column ID="colCitIssuePlace" runat="server" DataIndex="" Text=" Cit. Issue Place" MenuDisabled="true" Sortable="false" Align="Left" Width="110" />
                                                <ext:Column ID="colLicType" runat="server" DataIndex="" Text="License Type" MenuDisabled="true" Sortable="false" Align="Left" Width="100" />
                                                <ext:Column ID="colLiscenseNo" runat="server" DataIndex="" Text="Liscense No" MenuDisabled="true" Sortable="false" Align="Left" Width="100" />
                                                <ext:Column ID="colLisIssueDate" runat="server" Text="Lis. Issue Date" MenuDisabled="true" Sortable="false" Align="Left" Width="100" DataIndex="">
                                                </ext:Column>
                                                <ext:Column ID="colLisIssueCountry" runat="server" DataIndex="" Text="Lis. Issuing Country" MenuDisabled="true" Sortable="false" Align="Left" Width="130" />
                                                <ext:Column ID="colPassportNo" runat="server" DataIndex="" Text="Passport No." MenuDisabled="true" Sortable="false" Align="Left" Width="100" />
                                                <ext:Column ID="colPIssuingDate" runat="server" Text="Pass. Issuing Date" MenuDisabled="true" Sortable="false" Align="Left" Width="120" DataIndex="">
                                                </ext:Column>
                                                <ext:Column ID="colPValidUptoss" runat="server" Text="Pass. Valid Upto" MenuDisabled="true" Sortable="false" Align="Left" Width="120" DataIndex="">
                                                </ext:Column>
                                                <ext:Column ID="colBirthmark" runat="server" DataIndex="" Text="Birthmark" MenuDisabled="true" Sortable="false" Align="Left" Width="100" />
                                                <ext:Column ID="colMarriageAnneversary" runat="server" DataIndex="" Text="Marriage Anneversary" MenuDisabled="true" Sortable="false" Align="Left" Width="150" />
                                                <ext:Column ID="colReligion" runat="server" DataIndex="" Text="Religion" MenuDisabled="true" Sortable="false" Align="Left" Width="100" />
                                                <ext:Column ID="colEthnicity" runat="server" DataIndex="" Text="Ethnicity" MenuDisabled="true" Sortable="false" Align="Left" Width="100" />
                                                <ext:Column ID="colMotherTongue" runat="server" DataIndex="" Text="Mother Tongue" MenuDisabled="true" Sortable="false" Align="Left" Width="100" />
                                            </Columns>
                                        </ColumnModel>
                                        <Plugins>
                                            <ext:FilterHeader ID="FilterHeader2" runat="server" Remote="true" OnCreateFilterableField="OnCreateFilterableField" />
                                        </Plugins>
                                        <BottomBar>
                                            <ext:PagingToolbar Height="40" ID="PagingToolbar1" runat="server" StoreID="storeEmpInfo"
                                                DisplayInfo="true">
                                                <Items>
                                                    <ext:Label ID="lblPageSize" runat="server" Text="Page size:" />
                                                    <ext:ToolbarSpacer ID="ToolbarSpacer_listItems" runat="server" Width="10" />
                                                    <ext:ComboBox ID="cmbPageSize" runat="server" Width="80" SelectOnFocus="true" Selectable="true"
                                                        ValueField="Value" DisplayField="Text" ForceSelection="true" AllowBlank="false">
                                                        <Listeners>
                                                            <Select Handler="searchList()" />
                                                        </Listeners>
                                                        <Items>
                                                            <ext:ListItem Value="25" Text="25" />
                                                            <ext:ListItem Value="100" Text="100" />
                                                            <ext:ListItem Value="500" Text="500" />
                                                            <ext:ListItem Value="100000" Text="All" />
                                                        </Items>
                                                        <SelectedItems>
                                                            <ext:ListItem Index="0">
                                                            </ext:ListItem>
                                                        </SelectedItems>
                                                    </ext:ComboBox>
                                                </Items>
                                            </ext:PagingToolbar>
                                        </BottomBar>
                                    </ext:GridPanel>
                                </Items>
                            </ext:Panel>
                            <ext:Panel ID="tabPayroll" runat="server" Title="Payroll">
                            </ext:Panel>
                            <ext:Panel ID="tabLeave" runat="server" Title="Leave">
                            </ext:Panel>
                        </Items>
                        <%-- <Listeners>
                            <TabChange Handler="searchList();" />
                        </Listeners>--%>
                    </ext:TabPanel>
                </td>
                <td style="padding-left: 20px;">
                    <span id='spanText' style="color: blue"></span>
                </td>
            </tr>
        </table>
        <br />


    </div>
</asp:Content>
