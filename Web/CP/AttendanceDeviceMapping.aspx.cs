﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using Utils.Helper;
using Utils.Web;
using BLL.Base;

namespace Web
{
    public partial class AttendanceDeviceMapping : BasePage
    {

        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.LeaveAndAttendance;
            }
        }

        private int _tempCurrentPage;
        private int _tempCount;
        private PayManager payMgr = new PayManager();
        protected void Page_Load(object sender, EventArgs e)
        {
            //RegisterEmployeeSavedMsg();
            if (!IsPostBack)
            {
                pagingCtl.CurrentPage = 1;
                BindEmployees();

            }
            if (Request.Form["__EVENTTARGET"] != null && Request.Form["__EVENTTARGET"].ToLower().Contains("refresh"))
                BindEmployees();
            JavascriptHelper.AttachNonDialogPopUpCode(Page, "taxImportPopup", "../ExcelWindow/DeviceExcel.aspx", 450, 500);

            JavascriptHelper.AttachNonDialogPopUpCode(Page, "empTime", "../ExcelWindow/EmployeeAttendanceTimeExcel.aspx", 450, 500);

        }
        //void RegisterEmployeeSavedMsg()
        //{
        //    if (SessionManager.EmployeeSaved)
        //    {
        //        JavascriptHelper.DisplayClientMsgWithTimeout("Employee Information Updated.", Page);
        //        SessionManager.EmployeeSaved = false;
        //    }
        //}

        void BindEmployees()
        {
            int totalRecords = 0;
            gvEmployeeIncome.DataSource
                = TaxManager.GetForDeviceMapping(txtEmployeeName.Text.Trim(), pagingCtl.CurrentPage - 1, int.Parse(pagingCtl.DDLRecords.SelectedValue), ref totalRecords);
            gvEmployeeIncome.DataBind();
            pagingCtl.UpdatePagingBar(totalRecords);

            btnUpdate.Visible = true;
            btnExport.Visible = btnUpdate.Visible;
        }

        protected void btnPrevious_Click()
        {
            pagingCtl.CurrentPage -= 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage -= 1;
            BindEmployees();
        }

        protected void btnNext_Click()
        {
            pagingCtl.CurrentPage += 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage += 1;
            BindEmployees();
        }

        protected void ddlRecords_SelectedIndexChanged()
        {
            //SessionManager.EmployeeDisplayRecodsPerPage = int.Parse(pagintCtl.DDLRecords.SelectedValue);
            //_tempCurrentPage = 1;
            pagingCtl.CurrentPage = 1;
            BindEmployees();
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            Page.Validate("Balance");
            bool isSavedReqd = false;
            string deviceId = "";
            string IsEnabledManual = "false";
            string IsEnabledSkipLateEntry = "false";

            //string previousEmployeeName = string.Empty;
            //string preivousEIN = string.Empty;

            //List<string> deviceIdList = new List<string>();

            var list = new List<KeyValuePair<int, string>>();

            if (Page.IsValid)
            {
                StringBuilder str = new StringBuilder("<root>");
                int companyId = SessionManager.CurrentCompanyId;

                foreach (GridViewRow row in gvEmployeeIncome.Rows)
                {
                    int employeeId = (int)gvEmployeeIncome.DataKeys[row.RowIndex][0];

                    TextBox txt = row.FindControl("txtTaxPaid") as TextBox;
                    CheckBox chk = row.FindControl("chkManualAttendance") as CheckBox;
                    CheckBox chkLateEntry = row.FindControl("chkSkipLateEntry") as CheckBox;
                    CheckBox chkSkipAbsentLateProcessing = row.FindControl("chkSkipAbsentLateProcessing") as CheckBox;
                    Label lblName = row.FindControl("lblName") as Label;

                    isSavedReqd = true;
                    deviceId = txt.Text.Trim();

                    int emp = list.Where(a => a.Value == deviceId).Select(a => a.Key).SingleOrDefault();
                    if (emp != 0)
                    //if (deviceIdList.Contains(deviceId))
                    {
                        //JavascriptHelper.DisplayClientMsg("Device id " + deviceId + " is already set for the employee " + previousEmployeeName + " with EIN " + preivousEIN, Page);
                        JavascriptHelper.DisplayClientMsg("Device id " + deviceId + " is already set for the employee " + EmployeeManager.GetEmployeeById(emp).Name.ToString() + " with EIN " + emp, Page);
                        return;
                    }

                    //deviceIdList.Add(deviceId);

                    list.Add(new KeyValuePair<int, string>(employeeId, deviceId));

                    IsEnabledManual = chk.Checked.ToString();
                    IsEnabledSkipLateEntry = chkLateEntry.Checked.ToString();

                    str.Append(
                        string.Format("<row EmployeeId=\"{0}\" DeviceId=\"{1}\" IsEnabledManual=\"{2}\" IsEnabledSkipLateEntry=\"{3}\" IsExcelImport=\"{4}\"  SkipAbsentAndLateEntryDeduction=\"{5}\"/> ",
                             employeeId, deviceId, IsEnabledManual, IsEnabledSkipLateEntry, "False", chkSkipAbsentLateProcessing.Checked.ToString())
                        );

                    //previousEmployeeName = lblName.Text;
                    //preivousEIN = gvEmployeeIncome.DataKeys[row.RowIndex][0].ToString();

                }
                str.Append("</root>");

                if (isSavedReqd)
                {
                    Status status = TaxManager.SaveForDeviceMapping(str.ToString());
                    if (status.IsSuccess==true)
                        JavascriptHelper.DisplayClientMsg("Information saved.", Page);
                    else
                        JavascriptHelper.DisplayClientMsg(status.ErrorMessage, Page);
                }
            }
        }

        protected void txtEmployeeName_TextChanged(object sender, EventArgs e)
        {
            _tempCurrentPage = 1;
            BindEmployees();

        }
    }
}
