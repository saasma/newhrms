<%@ Page Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/Master/ForPopupPage.Master"
    AutoEventWireup="true" CodeBehind="AEStep.aspx.cs" Title="Step" Inherits="Web.CP.AEStep" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Assembly="Karpach.WebControls" Namespace="Karpach.WebControls" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/UpDownCtl.ascx" TagName="UpDownCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="InfoMsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .colorStyle td
        {
            padding: 2px !important;
        }
        .transparent
        {
            /* this class makes a window partially transparent */
            opacity: 0; /* Standard style for transparency */
            -moz-opacity: 0; /* Transparency for older Mozillas */
            filter: alpha(opacity=00); /* Transparency for IE */
            width: 0;
        }
        .paddinAll
        {
            padding: 10px;
        }
        div.notify
        {
            margin-left: 0;
            width: 41%;
        }
        .lf
        {
            width: 150px;
            padding: 0 0px 0px 0;
        }
        .lfs
        {
            width: 160px;
            padding: 0 10px 0px 0;
            vertical-align: top;
        }
        table tr td
        {
            padding-bottom: 10px;
        }
        .bevel
        {
            margin-bottom: 20px;
        }
        
        div.notify
        {
            width: 510px;
        }
        .displayInline
        {
            display: inline;
        }
    </style>
    <script type="text/javascript">
        function closePopup() {
            // alert(window.opener.parentReloadCallbackFunction)
            if ($.browser.msie == false && typeof (window.opener.reloadLeave) != 'undefined' && ($.browser.chrome || $.browser.safari)) {
                window.opener.reloadLeave(window);
            } else {
                window.returnValue = "Reload";
                window.close();
            }

        }

        

        var RenderRowForEdit = function (id) {

            var lblNameId = id.replace("ImageButton1", "lblName");
            var lblOrderId = id.replace("ImageButton1", "lblOrder");
            var lblStepId = id.replace("ImageButton1", "hdStepId");

            var lblName = document.getElementById(lblNameId);
            var lblOrder = document.getElementById(lblOrderId);
            var lblStepId = document.getElementById(lblStepId);

            var hdSaveMode = document.getElementById('<%=hdSaveMode.ClientID%>');
            var hdStepId = document.getElementById('<%=hdStepId.ClientID%>');

            var txtName = document.getElementById('<%=txtStepName.ClientID%>');
            var txtOrder = document.getElementById('<%=txtOrder.ClientID%>');

            txtName.value = lblName.innerHTML;
            txtOrder.value = lblOrder.innerHTML;
            hdStepId.value = lblStepId.value;
            hdSaveMode.value = "1";

            return false;
        }
     
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <asp:HiddenField ID="hdEditSequence" runat="server" />
    <asp:HiddenField ID="hdEmployeeEditSequence" runat="server" />
    <div class="popupHeader">
        <h3>
            Step</h3>
    </div>
    <div class="marginal" style='margin-top: 0px'>
        <uc2:InfoMsgCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
        <asp:HiddenField ID="hdStepId" runat="server" Value="0" />
        <asp:HiddenField ID="hdSaveMode" runat="server" Value="0" />
        <div class="clear" style="padding:10px 0px 10px 0px">
            <cc2:EmptyDisplayGridView CssClass="tableLightColor" UseAccessibleHeader="true" ShowHeaderWhenEmpty="True"
                ID="gvStep" runat="server" DataKeyNames="StepId,Name" GridLines="None" AutoGenerateColumns="False" width="520px"
                PagerStyle-HorizontalAlign="Center" AllowSorting="True" ShowFooterWhenEmpty="False" OnRowDeleting="gvStep_OnRowDeleting">
                <Columns>
                    <asp:BoundField DataField="StepId" HeaderText="GradeId" Visible="false" />
                    <asp:BoundField DataField="Name" HeaderText="Name" Visible="false" />
                    <asp:TemplateField HeaderText="S.No.">
                        <ItemTemplate>
                            <%#((GridViewRow)Container).RowIndex+1%>
                            <asp:HiddenField ID="hdStepId" runat="server" Value='<%#Eval("StepId")%>' />
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" Width="20px" />
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Name" HeaderStyle-Width="150px">
                        <ItemTemplate>
                            <asp:Label ID="lblName" runat="server" Text='<%#Eval("Name")%>' />
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" Width="150px" />
                    </asp:TemplateField>

                     <asp:TemplateField HeaderText="Order" HeaderStyle-Width="150px">
                        <ItemTemplate>
                            <asp:Label ID="lblOrder" runat="server" Text='<%#Eval("Order")%>' />
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" Width="150px" />
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Edit" HeaderStyle-Width="20px" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>

                            
                            <%--<asp:ImageButton ID="ImageButton1" runat="server" OnClientClick='<%# "return RenderRowOnControl(" + ((GridViewRow)Container).RowIndex + ")"%>'
                                ImageUrl="~/images/edit.gif" />--%>

                                <asp:ImageButton ID="ImageButton1" runat="server" OnClientClick="return RenderRowForEdit(this.id)"
                                ImageUrl="~/images/edit.gif" />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Delete" HeaderStyle-Width="20px" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:ImageButton ID="ImageButton2" OnClientClick="return confirm('Do you want to confirm delete the step?')"
                                runat="server" CommandName="Delete" ImageUrl="~/images/delete.gif" />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:TemplateField>
                </Columns>
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                <EmptyDataTemplate>
                    <b>No step records found. </b>
                </EmptyDataTemplate>
            </cc2:EmptyDisplayGridView>
        </div>
        <div class="bevel paddinAll" style="width: 500px; margin-bottom: 5px!important">
            <table cellpadding="0" cellspacing="0" width="500px">
                <tr>
                    <td class="lf">
                        Step name<label>*</label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtStepName" runat="server" Width="200px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="valGradeName" runat="server" ControlToValidate="txtStepName"
                            Display="None" ErrorMessage="Step is required." ValidationGroup="Step"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="lf">
                        Order<label>*</label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtOrder" Width="200px" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtOrder"
                            Display="None" ErrorMessage="Order is required." ValidationGroup="Step"></asp:RequiredFieldValidator>
                             <asp:CompareValidator ID="valCompOrder" runat="server" ControlToValidate="txtOrder"
                        Display="None" ErrorMessage="Invalid order value." ValidationGroup="Grade" Type="Integer" 
                          Operator="DataTypeCheck" />
                    </td>
                </tr>
            </table>
        </div>
        <div class="clear" style='text-align: right; padding-bottom: 10px; width: 525px'>
            <asp:Button ID="btnSave" CssClass="save" runat="server" OnClientClick="valGroup='Step';return CheckValidation()"
                OnClick="btnOk_Click" Text="Save" ValidationGroup="Leave" />
            <asp:Button ID="btnCancel" runat="server" CssClass="cancel" Text="Cancel" OnClientClick="window.close()" />
        </div>
    </div>
</asp:Content>
