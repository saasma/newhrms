﻿<%@ Page Title="Allowance Type" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="AllowanceType.aspx.cs" Inherits="Web.CP.AllowanceType" %>

<%--<%@ Register Src="~/NewHR/UserControls/EmployeeDetailsCtl.ascx" TagName="EmployeeDetailsCtl"
    TagPrefix="uc1" %>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        var CommandHandler1 = function(command, record){
            <%= hiddenValue.ClientID %>.setValue(record.data.EveningCounterTypeId);
                if(command=="Edit")
                {
                    <%= btnEditLevel.ClientID %>.fireEvent('click');
                }
                else if(command=="Detail")
                {
                     <%= btnDetailLevel.ClientID %>.fireEvent('click');
                }
                else
                {
                
                    <%= btnDeleteLevel.ClientID %>.fireEvent('click');
                }

             }
             
            function typeChange(value)
            {
                //based on
                if(value == "1")
                {
                    document.getElementById("ctl00_ContentPlaceHolder_Main_rowPercent1").style.display = "block";
                    document.getElementById("ctl00_ContentPlaceHolder_Main_rowPercent2").style.visibility = "visible";

                    document.getElementById("ctl00_ContentPlaceHolder_Main_rowFixed").style.display = "none";
                }
                else
                {
                    document.getElementById("ctl00_ContentPlaceHolder_Main_rowPercent1").style.display = "none";
                    document.getElementById("ctl00_ContentPlaceHolder_Main_rowPercent2").style.visibility = "hidden";

                    document.getElementById("ctl00_ContentPlaceHolder_Main_rowFixed").style.display = "block";
                }
            }
             
    </script>
    <style type="text/css">
        .x-tree-icon
        {
            display: none !important;
        }
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
    </style>
     <script>
         /* A header Checkbox of CheckboxSelectionModel deals with the current page only.
         This override demonstrates how to take into account all the pages.
         It works with local paging only. It is not going to work with remote paging.
         */
         Ext.selection.CheckboxModel.override({
             selectAll: function (suppressEvent) {
                 var me = this,
                    selections = me.store.getAllRange(), // instead of the getRange call
                    i = 0,
                    len = selections.length,
                    start = me.getSelection().length;

                 me.suspendChanges();

                 for (; i < len; i++) {
                     me.doSelect(selections[i], true, suppressEvent);
                 }

                 me.resumeChanges();
                 if (!suppressEvent) {
                     me.maybeFireSelectionChange(me.getSelection().length !== start);
                 }
             },

             deselectAll: Ext.Function.createSequence(Ext.selection.CheckboxModel.prototype.deselectAll, function () {
                 this.view.panel.getSelectionMemory().clearMemory();
             }),

             updateHeaderState: function () {
                 var me = this,
                    store = me.store,
                    storeCount = store.getTotalCount(),
                    views = me.views,
                    hdSelectStatus = false,
                    selectedCount = 0,
                    selected, len, i;

                 if (!store.buffered && storeCount > 0) {
                     selected = me.view.panel.getSelectionMemory().selectedIds;
                     hdSelectStatus = true;
                     for (s in selected) {
                         ++selectedCount;
                     }

                     hdSelectStatus = storeCount === selectedCount;
                 }

                 if (views && views.length) {
                     me.toggleUiHeader(hdSelectStatus);
                 }
             }
         });

         Ext.grid.plugin.SelectionMemory.override({
             memoryRestoreState: function (records) {
                 if (this.store !== null && !this.store.buffered && !this.grid.view.bufferedRenderer) {
                     var i = 0,
                        ind,
                        sel = [],
                        len,
                        all = true,
                        cm = this.headerCt;

                     if (!records) {
                         records = this.store.getAllRange(); // instead of getRange
                     }

                     if (!Ext.isArray(records)) {
                         records = [records];
                     }

                     if (this.selModel.isLocked()) {
                         this.wasLocked = true;
                         this.selModel.setLocked(false);
                     }

                     if (this.selModel instanceof Ext.selection.RowModel) {
                         for (ind = 0, len = records.length; ind < len; ind++) {
                             var rec = records[ind],
                                id = rec.getId();

                             if ((id || id === 0) && !Ext.isEmpty(this.selectedIds[id])) {
                                 sel.push(rec);
                             } else {
                                 all = false;
                             }

                             ++i;
                         }

                         if (sel.length > 0) {
                             this.surpressDeselection = true;
                             this.selModel.select(sel, false, !this.grid.selectionMemoryEvents);
                             this.surpressDeselection = false;
                         }
                     } else {
                         for (ind = 0, len = records.length; ind < len; ind++) {
                             var rec = records[ind],
                                id = rec.getId();

                             if ((id || id === 0) && !Ext.isEmpty(this.selectedIds[id])) {
                                 if (this.selectedIds[id].dataIndex) {
                                     var colIndex = cm.getHeaderIndex(cm.down('gridcolumn[dataIndex=' + this.selectedIds[id].dataIndex + ']'))
                                     this.selModel.setCurrentPosition({
                                         row: i,
                                         column: colIndex
                                     });
                                 }
                                 return false;
                             }

                             ++i;
                         }
                     }

                     if (this.selModel instanceof Ext.selection.CheckboxModel) {
                         if (all && (records.length > 0)) {
                             this.selModel.toggleUiHeader(true);
                         } else {
                             this.selModel.toggleUiHeader(false);
                         }
                     }

                     if (this.wasLocked) {
                         this.selModel.setLocked(true);
                     }
                 }
             }
         });
    </script>    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Allowance Types
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <ext:Hidden runat="server" ID="Hidden_CopyValue" />
        <ext:Hidden runat="server" ID="hiddenValue" />
        <ext:Button Cls="btn btn-primary" runat="server" Hidden="true" ID="btnEditLevel"
            runat="server">
            <DirectEvents>
                <Click OnEvent="btnEditLevel_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:Button>
        <ext:Button Cls="btn btn-primary" runat="server" Hidden="true" ID="btnDetailLevel"
            runat="server">
            <DirectEvents>
                <Click OnEvent="btnDetailLevel_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:Button>
        <ext:Button Cls="btn btn-primary" ID="btnDeleteLevel" runat="server" Hidden="true">
            <DirectEvents>
                <Click OnEvent="btnDeleteLevel_Click">
                    <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the Evening Counter Type?" />
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:Button>
        <div class="separator bottom">
        </div>
        <div class="innerLR">
            <%--<uc1:EmployeeDetailsCtl Id="EmployeeDetailsCtl1" runat="server" />--%>
            <div class="widget">
                <div class="widget-body">
                    <ext:GridPanel StyleSpec="margin-top:15px;" ID="GridLevels" runat="server" Cls="itemgrid">
                        <Store>
                            <ext:Store ID="Store3" runat="server">
                                <Model>
                                    <ext:Model ID="Model4" runat="server" IDProperty="OvertimeTypeId">
                                        <Fields>
                                            <ext:ModelField Name="EveningCounterTypeId" Type="string" />
                                            <ext:ModelField Name="Name" Type="string" />
                                            <ext:ModelField Name="Rate" Type="String" />
                                            <ext:ModelField Name="Income" Type="String" />
                                            <ext:ModelField Name="IsProportionate" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:Column ID="Column5" Sortable="false" MenuDisabled="true" runat="server" Text="Allowance Type"
                                    Width="180" Align="Left" DataIndex="Name" />
                                <ext:Column ID="Column3" Sortable="false" MenuDisabled="true" runat="server" Text="Income"
                                    Width="120" Align="Left" DataIndex="Income" />
                                <ext:Column ID="Column4" Sortable="false" MenuDisabled="true" runat="server" Text="Is Proportionate"
                                    Width="120" Align="Left" DataIndex="IsProportionate" />
                                <ext:Column ID="Column2" Sortable="false" MenuDisabled="true" runat="server" Text="Rate"
                                    Width="120" Align="Left" DataIndex="Rate">
                                    <Renderer Fn="getFormattedAmount" />
                                </ext:Column>
                                <ext:CommandColumn runat="server" Text="Actions" Align="Center" Width="125">
                                    <Commands>
                                        <ext:GridCommand Cls="extGridImageIconCls" Text="<i></i>" Icon="pencil" CommandName="Edit" />
                                        <ext:GridCommand Cls="extGridImageIconCls" Text="<i></i>" Icon="Cancel" CommandName="Delete" />
                                        <ext:GridCommand ToolTip-Text="Level wise rate" Text="<i></i>" Icon="MoneyAdd" CommandName="Detail" />
                                    </Commands>
                                    <Listeners>
                                        <Command Handler="CommandHandler1(command,record);" />
                                    </Listeners>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <SelectionModel>
                            <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                        </SelectionModel>
                    </ext:GridPanel>
                    <div class="buttonBlock">
                    </div>
                </div>
            </div>
            <div class="buttonBlock">
                <ext:Button Cls="btn btn-primary" runat="server" ID="btnAddNewLine" Text="<i></i>Add Allowance Type">
                    <Listeners>
                        <Click Handler="#{hiddenValue}.setValue('');#{windowAddEditOvertime}.show();" />
                    </Listeners>
                </ext:Button>
            </div>
            <ext:Window ID="Window1" AutoScroll="true" runat="server" Title="Allowance Rate"
                Icon="Application" Width="650" Height="600" BodyPadding="5" Hidden="true" Modal="true">
                <Content>
                    <span style="padding-left: 10px">Note : Once saved, all level amount will be picked
                        from this list </span>
                    <table class="fieldTable">
                        <tr>
                            <td align="right" colspan="5">
                                <div>
                                    <ext:Button Cls="btn btn-primary" ID="btnCopy" runat="server" Text="Copy" Icon="ArrowIn"
                                        IconAlign="Left">
                                        <DirectEvents>
                                            <Click OnEvent="btnCopy_Click">
                                                <EventMask ShowMask="true" />
                                                <ExtraParams>
                                                    <ext:Parameter Name="CopyValue" Value="Ext.encode(#{GridPanel1}.getRowsValues({selectedOnly : true}))"
                                                        Mode="Raw" />
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <ext:GridPanel StyleSpec="margin-top:15px;" ID="GridPanel1" runat="server" Cls="itemgrid"
                                    Width="525" Height="400" ClicksToEdit="1">
                                    <Store>
                                        <ext:Store ID="storeAllowanceDetail" runat="server" AutoLoad="true">
                                            <Model>
                                                <ext:Model ID="Model1" runat="server" IDProperty="LevelId">
                                                    <Fields>
                                                        <ext:ModelField Name="EveningCounterTypeId" Type="string" />
                                                        <ext:ModelField Name="LevelId" Type="string" />
                                                        <ext:ModelField Name="LevelName" Type="string" />
                                                        <ext:ModelField Name="CalculationType" Type="string" />
                                                        <ext:ModelField Name="Rate" Type="Float" />
                                                    </Fields>
                                                </ext:Model>
                                            </Model>
                                        </ext:Store>
                                    </Store>
                                    <Plugins>
                                        <ext:CellEditing ID="CellEditing1" ClicksToEdit="1" runat="server">
                                            <Listeners>
                                            </Listeners>
                                        </ext:CellEditing>
                                    </Plugins>
                                    <ColumnModel>
                                        <Columns>
                                            <ext:Column ID="Column11" TdCls="wrap" runat="server" Text="Level" Sortable="false"
                                                MenuDisabled="true" Width="175" DataIndex="LevelName" Border="false">
                                            </ext:Column>
                                            <%-- <ext:Column ID="Column_Score" TdCls="wrap" runat="server" Text="Calculation Method"
                                                Sortable="false" MenuDisabled="true" Width="200" DataIndex="CalculationType"
                                                Border="false">
                                                <Renderer Handler="if(value=='1') return 'Based on Salary'; else if(value=='2') return 'Fixed Rate';  else if(value=='3') return 'Based on Monthly Hour'; else '';" />
                                                <Editor>
                                                    <ext:ComboBox ID="cmb" runat="server">
                                                        <Items>
                                                            <ext:ListItem Text="Based on Salary" Value="1" />
                                                            <ext:ListItem Text="Fixed Rate" Value="2" />
                                                            <ext:ListItem Text="Based on Monthly Hour" Value="3" />
                                                        </Items>
                                                    </ext:ComboBox>
                                                </Editor>
                                            </ext:Column>--%>
                                            <ext:Column ID="Column_Label" TdCls="wrap" runat="server" Text="Rate" Sortable="false"
                                                MenuDisabled="true" Width="120" DataIndex="Rate" Border="false">
                                                <Editor>
                                                    <ext:TextField ID="txtRate" SelectOnFocus="true" runat="server" AllowBlank="false" />
                                                </Editor>
                                            </ext:Column>
                                        </Columns>
                                    </ColumnModel>
                                    <SelectionModel>
                                        <ext:CellSelectionModel ID="CellSelectionModel1" runat="server" Mode="Single" />
                                    </SelectionModel>
                                </ext:GridPanel>
                            </td>
                        </tr>
                        <tr>
                            <td valign="bottom">
                                <div class="popupButtonDiv">
                                    <ext:Button Cls="btn btn-primary" runat="server" ID="LinkButton2" Text="<i></i>Save">
                                        <DirectEvents>
                                            <Click OnEvent="btnDetailSaveUpdate_Click">
                                                <EventMask ShowMask="true" />
                                                <ExtraParams>
                                                    <ext:Parameter Name="Values" Value="Ext.encode(#{GridPanel1}.getRowsValues({selectedOnly : false}))"
                                                        Mode="Raw" />
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <div class="btnFlatOr">
                                        or</div>
                                    <ext:LinkButton runat="server" StyleSpec="padding:0px;" Cls="btnFlatLeftGap" ID="LinkButton3"
                                        Text="<i></i>Cancel" runat="server">
                                        <Listeners>
                                            <Click Handler="#{Window1}.hide();">
                                            </Click>
                                        </Listeners>
                                    </ext:LinkButton>
                                </div>
                            </td>
                        </tr>
                    </table>
                </Content>
            </ext:Window>
            <ext:Window ID="WindowCopy" AutoScroll="true" runat="server" Title="Allowance Rate"
                Icon="Application" Width="400" Height="575" BodyPadding="5" Hidden="true" Modal="true">
                <Content>
                    <table class="fieldTable">
                        <tr>
                            <td>
                                <ext:DisplayField ID="txtValueToBeCopied" runat="server" LabelAlign="Left" FieldLabel="Value to be copied"
                                    LabelSeparator=" " LabelWidth="125" Width="150">
                                </ext:DisplayField>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridPanelCopyList" runat="server"
                                    Cls="itemgrid" Width="350" Height="400" ClicksToEdit="1">
                                    <Store>
                                        <ext:Store ID="storeCopyList" runat="server" AutoLoad="true">
                                            <Model>
                                                <ext:Model ID="Model2" runat="server">
                                                    <Fields>
                                                        <ext:ModelField Name="LevelId" Type="string" />
                                                        <ext:ModelField Name="LevelName" Type="string" />
                                                    </Fields>
                                                </ext:Model>
                                            </Model>
                                        </ext:Store>
                                    </Store>
                                    <ColumnModel>
                                        <Columns>
                                            <ext:Column ID="Column1" TdCls="wrap" runat="server" Text="Level" Sortable="false"
                                                MenuDisabled="true" Width="320" DataIndex="LevelName" Border="false">
                                            </ext:Column>
                                        </Columns>
                                    </ColumnModel>
                                    <SelectionModel>
                                        <ext:CheckboxSelectionModel ID="CheckboxSelectionModel1" runat="server" Mode="Multi">
                                        </ext:CheckboxSelectionModel>
                                    </SelectionModel>
                                </ext:GridPanel>
                            </td>
                        </tr>
                        <tr>
                            <td valign="bottom">
                                <div class="popupButtonDiv">
                                    <ext:Button Cls="btn btn-primary" runat="server" ID="LinkButton1" Text="<i></i>Save">
                                        <DirectEvents>
                                            <Click OnEvent="btnCopyTo_Click">
                                                <EventMask ShowMask="true" />
                                                <ExtraParams>
                                                    <ext:Parameter Name="SelectedLevels" Value="Ext.encode(#{gridPanelCopyList}.getRowsValues({selectedOnly : true}))"
                                                        Mode="Raw" />
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <div class="btnFlatOr">
                                        or</div>
                                    <ext:LinkButton runat="server" StyleSpec="padding:0px;" Cls="btnFlatLeftGap" ID="LinkButton4"
                                        Text="<i></i>Cancel" runat="server">
                                        <Listeners>
                                            <Click Handler="#{WindowCopy}.hide();">
                                            </Click>
                                        </Listeners>
                                    </ext:LinkButton>
                                </div>
                            </td>
                        </tr>
                    </table>
                </Content>
            </ext:Window>
            <ext:Window ID="windowAddEditOvertime" AutoScroll="true" runat="server" Title="Allowance Type Details"
                Icon="Application" Width="700" Height="550" BodyPadding="5" Hidden="true" Modal="true">
                <Content>
                    <table>
                        <tr>
                            <td valign="top">
                                <table class="fieldTable">
                                    <tr>
                                        <td colspan="2">
                                            <ext:TextField ID="txtName" runat="server" FieldLabel="Name *" LabelAlign="top" LabelSeparator=""
                                                Width="300" />
                                            <asp:RequiredFieldValidator Display="None" ID="valtxtPublicationName" runat="server"
                                                ValidationGroup="SaveUpdate" ControlToValidate="txtName" ErrorMessage="Evening Counter Name is required." />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <ext:NumberField LabelSeparator="" ID="txtAllowanceRate" Note="For level wise rate, use last option in grid"
                                                runat="server" FieldLabel="Rate *" LabelAlign="Top" Width="300" />
                                            <asp:RequiredFieldValidator Display="None" ID="valAllowanceRate" runat="server" ValidationGroup="SaveUpdate"
                                                ControlToValidate="txtAllowanceRate" ErrorMessage="Evening Counter Rate is required." />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <ext:Checkbox LabelSeparator="" ID="chkIsMonthlyProportionate" runat="server" BoxLabel="Proportionately on Total Month Days"
                                                LabelAlign="Top" Width="300" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <ext:Checkbox LabelSeparator="" ID="chkDoNotShowDaysWarning" runat="server" BoxLabel="Do not validate as Days"
                                             Note="If selected, unit will not be validated for the selected date range" LabelAlign="Top" Width="300" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <ext:Checkbox LabelSeparator="" ID="chkDoNotShowInEmployeePortal" runat="server" BoxLabel="Do not show in Employee Portal"
                                             Note="If selected, this allowance type will not be listed in Employee Portal" LabelAlign="Top" Width="300" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <ext:Store ID="storeIncomes" runat="server">
                                                <Model>
                                                    <ext:Model ID="modelLevel" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="IncomeId" Type="String" />
                                                            <ext:ModelField Name="Title" Type="String" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                            <ext:ComboBox ID="cmbIncomes" LabelSeparator="" ForceSelection="true" LabelAlign="Top"
                                                runat="server" FieldLabel="Income *" Width="200" DisplayField="Title" ValueField="IncomeId"
                                                StoreID="storeIncomes" EmptyText="" QueryMode="Local">
                                            </ext:ComboBox>
                                            <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator1" runat="server"
                                                ValidationGroup="SaveUpdate" ControlToValidate="cmbIncomes" ErrorMessage="Income is required." />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <ext:ComboBox ID="cmbCalculationType" runat="server" Width="200" FieldLabel="Calculation"
                                                LabelAlign="top" LabelSeparator="" ForceSelection="true" QueryMode="Local">
                                                <Items>
                                                    <ext:ListItem Text="Default/Normal" Value="1" />
                                                    <ext:ListItem Text="365 Days" Value="2" />
                                                    <ext:ListItem Text="Monthly Hours" Value="3" />
                                                    <ext:ListItem Text="Based On Salary" Value="4" />
                                                </Items>
                                                <Triggers>
                                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                                </Triggers>
                                                <Listeners>
                                                    <Select Handler="this.getTrigger(0).show();if(this.getValue()=='4') #{gridIncomes}.show(); else #{gridIncomes}.hide();" />
                                                    <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                                    <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                                                </Listeners>
                                            </ext:ComboBox>
                                            <%--   <asp:RequiredFieldValidator Display="None" ID="valcmbPublicationType" runat="server"
                                    ValidationGroup="SaveUpdate" ControlToValidate="cmbCalculationType" ErrorMessage="Calculation is required." />--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="bottom">
                                            <div class="popupButtonDiv">
                                                <ext:Button Cls="btn btn-primary" runat="server" ID="btnOvertimeType" Text="<i></i>Save">
                                                    <DirectEvents>
                                                        <Click OnEvent="btnOvertimeType_Click">
                                                            <EventMask ShowMask="true" />
                                                        </Click>
                                                    </DirectEvents>
                                                    <Listeners>
                                                        <Click Handler="valGroup = 'SaveUpdate'; if(CheckValidation()) return this.disable(); else return false;">
                                                        </Click>
                                                    </Listeners>
                                                </ext:Button>
                                                <div class="btnFlatOr">
                                                    or</div>
                                                <ext:LinkButton runat="server" StyleSpec="padding:0px;" Cls="btnFlatLeftGap" ID="LinkButton5"
                                                    Text="<i></i>Cancel" runat="server">
                                                    <Listeners>
                                                        <Click Handler="#{windowAddEditOvertime}.hide();">
                                                        </Click>
                                                    </Listeners>
                                                </ext:LinkButton>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td valign="top" style='padding-left: 40px; padding-top: 25px;'>
                                <ext:GridPanel  Title="Select Incomes for Based on salary" Height="450" Scroll="Both" Width="310" StyleSpec="margin-top:15px;" ID="gridIncomes"
                                    runat="server" Cls="itemgrid">
                                    <Store>
                                        <ext:Store ID="storeGrid" runat="server" AutoLoad="true">
                                            <Model>
                                                <ext:Model ID="Model3" runat="server" IDProperty="IncomeId">
                                                    <Fields>
                                                        <ext:ModelField Name="IncomeId" Type="String" />
                                                        <ext:ModelField Name="Title" Type="String" />
                                                    </Fields>
                                                </ext:Model>
                                            </Model>
                                        </ext:Store>
                                    </Store>
                                    <SelectionModel>
                                        <ext:CheckboxSelectionModel ShowHeaderCheckbox="false" runat="server" ID="selectionModel" Mode="Simple"/>
                                    </SelectionModel>
                                    <ColumnModel ID="ColumnModel1" runat="server">
                                        <Columns>
                                            <ext:Column ID="DateColumn1" runat="server" Text="Income Head" DataIndex="Title" Align="Left"
                                                Width="250" />
                                        </Columns>
                                    </ColumnModel>
                                </ext:GridPanel>
                            </td>
                        </tr>
                    </table>
                </Content>
            </ext:Window>
        </div>
    </div>
    <br />
</asp:Content>
