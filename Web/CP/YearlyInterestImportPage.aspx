<%@ Page Title="PF/CIT Yearly Interest" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="YearlyInterestImportPage.aspx.cs" Inherits="Web.YearlyInterestImportPage" %>

<%@ Import Namespace="BLL.Manager" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .tableLightColor input
        {
            width: 120px;
        }
    </style>
    <script src="../Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>
    <script type="text/javascript">



        function importPopupProcess(btn) {

            var ID;
            var ddlYears = '<%= ddlYears.ClientID %>';
            ID = parseInt(document.getElementById(ddlYears).value);

            var ret = taxImportPopup("id=" + ID);

            return false;
        }


        function refreshWindow() {

            __doPostBack('Refresh', 0);


        }
        jQuery(document).ready(
    function () {

        setMovementToGrid('#<%= gvEmployeeIncome.ClientID %>');
    }
);

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">

<div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    PF/CIT Yearly Interest
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
    <div class="attribute" style="height:55px; padding-top:15px;">
        <table>
            <tr>
                <td>
                    Search &nbsp;
                    <asp:TextBox ID="txtEmployeeName" AutoPostBack="true" runat="server" Width="146px" Visible="false"
                        OnTextChanged="txtEmployeeName_TextChanged"></asp:TextBox>
                </td>

                <td>
                    <div>
                            Year List &nbsp;
                            <asp:DropDownList AutoPostBack="false" Width="150" ID="ddlYears" runat="server" DataValueField="FinancialDateId"
                                DataTextField="Name"  AppendDataBoundItems="true">
                                <%--<asp:ListItem Value="-1">--Select Year--</asp:ListItem>--%>
                            </asp:DropDownList>
                            <%--OnSelectedIndexChanged="ddlYears_Changed"--%>

                            <%--<asp:RequiredFieldValidator ValidationGroup="Inc" ID="RequiredFieldValidator8" Display="None"
                                runat="server" InitialValue="-1" ControlToValidate="ddlYears" ErrorMessage="Year is required."></asp:RequiredFieldValidator>
                            &nbsp;--%>
                     </div>
                </td>

                <td>
                    <asp:LinkButton ID="btnExport" runat="server" Text="Excel import" OnClientClick="importPopupProcess();return false;"
                        CssClass=" excel marginRight tiptip" Style="float: left;" />
                </td>

                <td>
                    <asp:Button ID="Button1" CssClass="update"  AutoPostBack="false" runat="server" Text="Search" OnClick="btnSearch_Click" />
                        <%--OnClick="btnUpdate_Click"--%>
                </td>

            </tr>
        </table>
    </div>
    <div class="clear">
        <cc1:TextBoxWatermarkExtender ID="TBWE2" runat="server" TargetControlID="txtEmployeeName"
            WatermarkText="Enter employee name" WatermarkCssClass="watermarked" />
        <cc2:EmptyDisplayGridView CssClass="tableLightColor" UseAccessibleHeader="true" ShowHeaderWhenEmpty="True"
            ID="gvEmployeeIncome" runat="server" DataKeyNames="EmployeeId" AutoGenerateColumns="False"
            CellPadding="4" GridLines="None" AllowSorting="True" ShowFooterWhenEmpty="False">
            <RowStyle BackColor="#E3EAEB" />
            <Columns>
                <asp:BoundField HeaderStyle-Width="20px" DataFormatString="{0:000}" DataField="EmployeeId"
                    HeaderText="EIN"></asp:BoundField>
                <asp:TemplateField HeaderText="Name" HeaderStyle-Width="150px">
                    <ItemTemplate>
                        <asp:Label Text='<%# Eval("Name") %>' Style="width: 100px!important" runat="server" />
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left" Width="150px" />
                </asp:TemplateField>


                <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="PF Interest" HeaderStyle-Width="80px">
                    <ItemTemplate>
                        <asp:TextBox CssClass='calculationInput' data-col='1' data-row='<%# Container.DataItemIndex %>' Style='text-align: right; background:#E0E0E0;'
                            ID="txtPF" Text='<%# GetCurrency( Eval("PFInterest")) %>' runat="server" ReadOnly="true"></asp:TextBox>
                        <asp:CompareValidator SetFocusOnError="true" ValueToCompare="0" Display="None" Operator="GreaterThanEqual"
                            Type="Currency" ID="valOpeningBalance1" ControlToValidate="txtPF" ValidationGroup="Balance" 
                            runat="server" ErrorMessage="Invalid amount."></asp:CompareValidator>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="CIT Interest" HeaderStyle-Width="80px">
                    <ItemTemplate>
                        <asp:TextBox CssClass='calculationInput' data-col='3' data-row='<%# Container.DataItemIndex %>' Style='text-align: right; background:#E0E0E0;'
                            ID="txtInsurance" Text='<%# GetCurrency( Eval("CITInterest")) %>' runat="server" ReadOnly="true"></asp:TextBox>
                        <asp:CompareValidator SetFocusOnError="true" ValueToCompare="0" Display="None" Operator="GreaterThanEqual"
                            Type="Currency" ID="valOpeningBalance23" ControlToValidate="txtInsurance" ValidationGroup="Balance"
                            runat="server" ErrorMessage="Invalid amount."></asp:CompareValidator>
                    </ItemTemplate>
                </asp:TemplateField>
                
            </Columns>
            <RowStyle CssClass="odd" />
            <AlternatingRowStyle CssClass="even" />
            <EmptyDataTemplate>
                <b> PF/CIT Yearly Interest </b>
            </EmptyDataTemplate>
        </cc2:EmptyDisplayGridView>
        <uc1:PagingCtl ID="pagingCtl" OnNextRecord="btnNext_Click" OnPrevRecord="btnPrevious_Click"
            OnDropDownShowRecordsChanged="ddlRecords_SelectedIndexChanged" runat="server" />
    </div>
    <div class="buttonsDiv">
        <%--    <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Balance" DisplayMode="List"
            ShowSummary="false" ShowMessageBox="true" runat="server"></asp:ValidationSummary>--%>
        <asp:Button ID="btnUpdate" CssClass="update" OnClientClick="valGroup='Balance';return CheckValidation();"
            Visible="true" ValidationGroup="Balance" runat="server" Text="Update" OnClick="btnUpdate_Click" />
    </div>
</div>
</asp:Content>
