﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Base;
using BLL.Manager;
using Utils.Helper;
using DAL;

namespace Web.CP
{
    public partial class AddOnIncomeSelection : BasePage
    {
        int payrollPeriodId = 0;
        PayManager payMgr = new PayManager();
        //int employeeId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            string pp = Request.QueryString["p"];
            if (pp.Contains(":"))
                payrollPeriodId = int.Parse(pp.Substring(0, pp.IndexOf(":")));
            else

                payrollPeriodId = int.Parse(pp);

            if (payrollPeriodId != 0)
            {
                PayrollPeriod payroll= CommonManager.GetPayrollPeriod(payrollPeriodId);
                header.InnerHtml = string.Format(header.InnerHtml, payroll.Name);
            }

            string eventTarget = Request.Form["__EVENTTARGET"];
            //employeeId = UrlHelper.GetIdFromQueryString("EId");
            if (!IsPostBack)
            {
                Initialise();
            }
            else if (eventTarget != null && eventTarget.Equals("Reload"))
            {
                Initialise();
            }


            JavascriptHelper.AttachPopUpCode(Page, "popupIncome", "AEIncome.aspx", 600, 600);
        }

        void Initialise()
        {


            List<PIncome> availableList = null; List<PIncome> availableFixedList = null; 
            List<CalcGetHeaderListResult> selectedList = null;
            List<CalcGetHeaderListResult> selectedDeductionList = null;

            PartialAddOnManager.SetIncomeList(payrollPeriodId, ref availableList, ref availableFixedList, ref selectedList, ref selectedDeductionList);

            lstIncomeList.DataSource = availableList;
            lstIncomeList.DataBind();

            lstFixedIncomeList.DataSource = availableFixedList;
            lstFixedIncomeList.DataBind();


            //lstSelectedIncomeList.DataSource = selectedList;
            //lstSelectedIncomeList.DataBind();

            lstDeductionList.DataSource = PayManager.GetDeductionListByCompany(SessionManager.CurrentCompanyId)
                .Where(x => x.Calculation == DeductionCalculation.PERCENT || x.Calculation == DeductionCalculation.VARIABLE || x.Calculation == DeductionCalculation.FIXED).ToList();
            lstDeductionList.DataBind();

            gvwSelectedIncomesIncomes.DataSource = selectedList;
            gvwSelectedIncomesIncomes.DataBind();


            gvwDeductions.DataSource = selectedDeductionList;
            gvwDeductions.DataBind();


        }


        protected void gvwEmployees_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Remove")
            {
                // Retrieve the row index stored in the 
                // CommandArgument property.
                string typeSoruce = e.CommandArgument.ToString();
                string[] values = typeSoruce.Split(new char[] { ':' });
                int type = int.Parse(values[0]);
                int sourceId = int.Parse(values[1]);


                List<PIncome> incomes = new List<PIncome>();

                incomes.Add(new PIncome { Type = type, SourceId = sourceId });



                if (incomes.Count > 0)
                {
                    PartialAddOnManager.DeleteIncomeHead(payrollPeriodId, incomes);

                    //Initialise();
                    msgInfo.InnerHtml = "Income removed.";
                    msgInfo.Hide = false;
                }

                List<CalcGetHeaderListResult> selectedList = new List<CalcGetHeaderListResult>();

                //PartialAddOnManager.SetIncomeList(payrollPeriodId, ref availableList, ref availableFixedList, ref selectedList);
                foreach (GridViewRow row in gvwSelectedIncomesIncomes.Rows)
                {
                    CalcGetHeaderListResult item = new CalcGetHeaderListResult();
                    DataKey data = gvwSelectedIncomesIncomes.DataKeys[row.RowIndex];
                    item.Type = (int)data["Type"];
                    item.SourceId = (int)data["SourceId"];
                    item.HeaderName = data["HeaderName"].ToString();
                    item.PartialPaidHeaderId = (int)data["PartialPaidHeaderId"];

                    if (item.TypeSourceId != (type + ":" + sourceId))
                        selectedList.Add(item);

                }

                gvwSelectedIncomesIncomes.DataSource = selectedList;
                gvwSelectedIncomesIncomes.DataBind();
                // Add code here to add the item to the shopping cart.
            }
            else
            {
                string typeSoruce = e.CommandArgument.ToString();
                string[] values = typeSoruce.Split(new char[] { ':' });
                int type = int.Parse(values[0]);
                int sourceId = int.Parse(values[1]);


                PartialAddOnManager.ClearValue(payrollPeriodId, type, sourceId);

                msgInfo.InnerHtml = "Clear for the selected income.";
                msgInfo.Hide = false;
            }
        }


        protected void gvwDeductions_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Remove")
            {
                // Retrieve the row index stored in the 
                // CommandArgument property.
                string typeSoruce = e.CommandArgument.ToString();
                string[] values = typeSoruce.Split(new char[] { ':' });
                int type = int.Parse(values[0]);
                int sourceId = int.Parse(values[1]);


                List<PDeduction> incomes = new List<PDeduction>();

                incomes.Add(new PDeduction {DeductionId = sourceId });



                if (incomes.Count > 0)
                {
                    PartialAddOnManager.DeleteDeductionHead(payrollPeriodId, incomes);

                    //Initialise();
                    msgInfo.InnerHtml = "deduction removed.";
                    msgInfo.Hide = false;
                }


                Initialise();
               
                // Add code here to add the item to the shopping cart.
            }
            

        }

        protected void gvwEmployees_RowCreated(object sender, GridViewRowEventArgs e)
        {

            //if (e.Row.RowType == DataControlRowType.DataRow)
            //{
            //    e.Row.Attributes.Add("onmouseover",
            //    "this.originalClass=this.className;this.className='selected'");

            //    e.Row.Attributes.Add("onmouseout",
            //    "this.className=this.originalClass;");


            //}

        }

        protected void btnSaveIncomes_Click(object sender, EventArgs e)
        {

            List<CalcGetHeaderListResult> selectedList = new List<CalcGetHeaderListResult>();
            List<PIncome> incomes = new List<PIncome>();
            List<PDeduction> deductions = new List<PDeduction>();

            //PartialAddOnManager.SetIncomeList(payrollPeriodId, ref availableList, ref availableFixedList, ref selectedList);
            foreach (GridViewRow row in gvwSelectedIncomesIncomes.Rows)
            {
                PIncome item = new PIncome();
                DataKey data = gvwSelectedIncomesIncomes.DataKeys[row.RowIndex];
                item.Type = (int)data["Type"];
                item.SourceId = (int)data["SourceId"];
                item.Title = data["HeaderName"].ToString();
                item.PartialPaidHeaderId = (int)data["PartialPaidHeaderId"];

                DropDownList ddlSetting = row.FindControl("ddlSetting") as DropDownList;
                item.UseIncomeSettings = bool.Parse(ddlSetting.SelectedValue);

                DropDownList ddlType = row.FindControl("ddlAddOnType") as DropDownList;
                item.AddToRegularSalary = bool.Parse(ddlType.SelectedValue);

                incomes.Add(item);
            }

            foreach (GridViewRow row in gvwDeductions.Rows)
            {
                PDeduction item = new PDeduction();
                DataKey data = gvwDeductions.DataKeys[row.RowIndex];
                
                item.DeductionId = (int)data["SourceId"];
                item.Title = data["HeaderName"].ToString();


                deductions.Add(item);
            }

            PartialAddOnManager.SaveIncome(payrollPeriodId, incomes, deductions);

            msgInfo.InnerHtml = "Income(s) saved.";
            msgInfo.Hide = false;



        }
        protected void btnOk_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                List<CalcGetHeaderListResult> selectedList = new List<CalcGetHeaderListResult>();
                List<CalcGetHeaderListResult> deductions = new List<CalcGetHeaderListResult>();

                //PartialAddOnManager.SetIncomeList(payrollPeriodId, ref availableList, ref availableFixedList, ref selectedList);
                foreach (GridViewRow row in gvwSelectedIncomesIncomes.Rows)
                {
                    CalcGetHeaderListResult item = new CalcGetHeaderListResult();
                    DataKey data =  gvwSelectedIncomesIncomes.DataKeys[row.RowIndex];
                    item.Type = (int)data["Type"];
                    item.SourceId = (int)data["SourceId"];
                    item.HeaderName = data["HeaderName"].ToString();
                    item.PartialPaidHeaderId = (int)data["PartialPaidHeaderId"];
                    selectedList.Add(item);
                }

                foreach (GridViewRow row in gvwDeductions.Rows)
                {
                    CalcGetHeaderListResult item = new CalcGetHeaderListResult();
                    DataKey data = gvwDeductions.DataKeys[row.RowIndex];
                    item.Type = (int)data["Type"];
                    item.SourceId = (int)data["SourceId"];
                    item.HeaderName = data["HeaderName"].ToString();
                    item.PartialPaidHeaderId = (int)data["PartialPaidHeaderId"];
                    deductions.Add(item);
                }

                List<PIncome> incomes = new List<PIncome>();
                foreach (ListItem item in lstIncomeList.Items)
                {
                    if (item.Selected)
                    {

                        string[] values = item.Value.Split(new char[] { ':' });
                        int type = int.Parse(values[0]);
                        int souriceId = int.Parse(values[1]);

                        if (selectedList.Any(x => x.Type == type && x.SourceId == souriceId) == false)
                        {


                            incomes.Add(new PIncome { Title = item.Text, Type = int.Parse(values[0]), SourceId = int.Parse(values[1]) });

                            selectedList.Add(new CalcGetHeaderListResult
                            {
                                PartialPaidHeaderId = 0,
                                Type = int.Parse(values[0]),
                                SourceId = int.Parse(values[1]),
                                HeaderName = item.Text
                            });
                        }
                    }
                }

                
                foreach (ListItem item in lstDeductionList.Items)
                {
                    if (item.Selected)
                    {

                        int deductionId = int.Parse(item.Value);

                        if (deductions.Any(x => x.Type == (int)CalculationColumnType.Deduction
                            && x.SourceId == deductionId) == false)
                        {


                            deductions.Add(new CalcGetHeaderListResult
                            {
                                PartialPaidHeaderId = 0,
                                Type = (int)CalculationColumnType.Deduction,
                                SourceId = deductionId,
                                HeaderName = item.Text
                            });
                        }
                    }
                }

                List<PIncome> fixedIncomes = new List<PIncome>();
                foreach (ListItem item in lstFixedIncomeList.Items)
                {
                    if (item.Selected)
                    {
                        string[] values = item.Value.Split(new char[] { ':' });
                        int type = int.Parse(values[0]);
                        int souriceId = int.Parse(values[1]);

                        if (selectedList.Any(x => x.Type == type && x.SourceId == souriceId) == false)
                        {
                            fixedIncomes.Add(new PIncome { Title = item.Text, Type = int.Parse(values[0]), SourceId = int.Parse(values[1]) });

                            selectedList.Add(new CalcGetHeaderListResult
                            {
                                PartialPaidHeaderId = 0,
                                Type = int.Parse(values[0]),
                                SourceId = int.Parse(values[1]),
                                HeaderName = item.Text
                            });
                        }
                    }
                }


                


                if (incomes.Count > 0 || fixedIncomes.Count > 0)
                {
                    //PartialAddOnManager.SaveIncome(payrollPeriodId, incomes, fixedIncomes);

                    //Initialise();
                    //msgInfo.InnerHtml = "Income(s) added.";
                    //msgInfo.Hide = false;
                    gvwSelectedIncomesIncomes.DataSource = selectedList;
                    gvwSelectedIncomesIncomes.DataBind();

                }

                if (deductions.Count > 0)
                {
                    gvwDeductions.DataSource = deductions;
                    gvwDeductions.DataBind();
                }

            }
        }

        protected void gvwSelectedIncomesIncomes_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // Find the drop-down (say in 3rd column)
                var ddl = e.Row.FindControl("ddlAddOnType") as  DropDownList;
                CalcGetHeaderListResult data = e.Row.DataItem as CalcGetHeaderListResult;
                if (null != ddl)
                {
                    ddl.SelectedValue = data.AddToRegularSalary.ToString().ToLower();
                    // bind it
                }

                /*
                // In case of template fields, use FindControl
                dd = e.Row.Cells[2].FindControl("MyDD") as DropDownList;
                */
            }
        }
    }
}
