﻿<%@ Page Title="Insurance list" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="InsuranceList.aspx.cs" Inherits="Web.CP.InsuranceList" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/core.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function medicalTaxCreditPopupCall(medicalTaxId) {
            var ret = medicalTaxCreditPopup("Id=" + medicalTaxId);
            if (ret == "Yes")
                __doPostBack('Reload', '');
        }

        function parentReloadCallbackFunction(childWindow, ret) {
            childWindow.close();
            if (ret == "Yes")
                __doPostBack('Reload', '');

        }

        function refreshWindow() {
            __doPostBack('Reload', '');
        }

        function isPayrollSelected(btn) {



            var ret = shiftPopup("yearid=" + <%=ddlFinancialYear.ClientID %>.value);
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4 runat="server" id="title">
                    Insurance List
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="contentArea">
            <asp:LinkButton runat="server" Visible="false"></asp:LinkButton>
            <div class="attribute" style="padding:10px">
                <table>
                    <tr>
                        <td>
                            Year
                        </td>
                         <td style="padding-left:5px;">
                            <asp:DropDownList ID="ddlFinancialYear" runat="server" Width="125" DataValueField="FinancialDateId" AutoPostBack="true" AppendDataBoundItems="true"
                                DataTextField="Name" CssClass="gapbelow" 
                               OnSelectedIndexChanged="btnLoad_Click"  >
                               
                            </asp:DropDownList>
                        </td>
                        <td style="padding-left:10px;">
                            <strong>Search </strong>&nbsp;
                            <asp:TextBox Width="160px" ID="txtEmpSearchText" runat="server" OnTextChanged="txtEmpSearchText_TextChanged"></asp:TextBox>
                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtEmpSearchText"
                                WatermarkText="Employee Name" WatermarkCssClass="searchBoxText" />
                            <cc1:AutoCompleteExtender EnableCaching="true" ID="AutoCompleteExtenderOrganization"
                                runat="server" MinimumPrefixLength="2" ServiceMethod="GetEmployeeNames" ServicePath="~/PayrollService.asmx"
                                TargetControlID="txtEmpSearchText" CompletionSetCount="10" CompletionInterval="250"
                                CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList"
                                CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
                            </cc1:AutoCompleteExtender>
                        </td>
                        <td>
                            <asp:Button ID="btnLoad" CssClass="btn btn-default btn-sect btn-sm" Style="margin-left: 10px; width:80px;" runat="server"
                                OnClick="btnLoad_Click" Text="Show" />
                            <cc1:TextBoxWatermarkExtender ID="TBWE2" runat="server" TargetControlID="txtEmpSearchText"
                                WatermarkText="Employee name" WatermarkCssClass="searchBoxText" />
                        </td>
                        <td style='padding-left: 10px'>
                            <asp:Button Style="margin-left: 10px" ID="btnSave" Width="150px" CausesValidation="false"
                                CssClass="btn btn-warning btn-sect btn-sm" runat="server" Text="Add Insurance" OnClientClick="window.location='IndividualInsurance.aspx';return false;" />
                        </td>
                        <td style='padding-left: 10px'>
                            <asp:LinkButton ID="btnExport" runat="server" Text="Excel import" OnClientClick="return isPayrollSelected(this)"
                                CssClass=" excel marginRight tiptip" Style="float: left;" />
                        </td>
                    </tr>
                </table>
            </div>
            <div class="separator clear">
                <cc2:EmptyDisplayGridView CssClass="table table-primary mb30 table-bordered table-hover" UseAccessibleHeader="true" ID="gvwList"
                    runat="server" AutoGenerateColumns="False" DataKeyNames="EmployeeId,FinancialYearId" GridLines="None"
                    ShowFooterWhenEmpty="False" ShowHeaderWhenEmpty="True" OnRowCreated="gvwList_RowCreated"
                    Width="100%">
                    <Columns>
                        <asp:BoundField HeaderStyle-HorizontalAlign="Left" DataField="EmployeeId" HeaderText="EIN"
                            HeaderStyle-Width="40px" />
                        <asp:BoundField HeaderStyle-Width="150px" HeaderStyle-HorizontalAlign="Left" DataField="Name"
                            HeaderText="Name" />
                      <%--  <asp:BoundField HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left" DataField="InsuranceType"
                            HeaderText="Insurance Type" />--%>
                        <asp:BoundField HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left" DataField="InsuranceCompany"
                            HeaderText="Insurance Company" />
                         <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                HeaderStyle-Width="80px" HeaderText="Health Insurance">
                                <ItemTemplate>
                                    <%# Convert.ToBoolean(Eval("IsHealthInsurance")) == true ? "Yes" : ""%>
                                </ItemTemplate>
                            </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-Width="90px" HeaderText="Paid by">
                            <ItemTemplate>
                                <%# Convert.ToDecimal(Eval("EmployerPaysPercent")) == 0 ? "Employee" : "Company"%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-Width="80px" HeaderText="Policy No">
                            <ItemTemplate>
                                <%# (Eval("PolicyNo"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-Width="80px" HeaderText="Start Date">
                            <ItemTemplate>
                                <%# (Eval("StartDateEng","{0:yyyy-MMM-dd}"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-Width="80px" HeaderText="End Date">
                            <ItemTemplate>
                                <%# (Eval("EndDateEng", "{0:yyyy-MMM-dd}"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right"
                            HeaderStyle-Width="80px" HeaderText="Policy Amount">
                            <ItemTemplate>
                                <%# GetCurrency(Eval("PolicyAmount"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right"
                            HeaderStyle-Width="80px" HeaderText="Premium">
                            <ItemTemplate>
                                <%# GetCurrency(Eval("Premium"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-Width="100px"  HeaderText="Note">
                            <ItemTemplate>
                                <%# (Eval("Note"))%>                                
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:HyperLink ID="linkEdit" ImageUrl="~/images/edit.gif" NavigateUrl='<%# "~/CP/IndividualInsurance.aspx?EmpId=" + Eval("EmployeeId") + "&Id=" +  Eval("Id") + "&YearId=" + Eval("FinancialYearId") %>'
                                    runat="server"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="odd" />
                    <AlternatingRowStyle CssClass="even" />
                    <EmptyDataTemplate>
                        No records.
                    </EmptyDataTemplate>
                </cc2:EmptyDisplayGridView>
                <uc1:PagingCtl ID="pagingCtl" OnNextRecord="btnNext_Click" OnPrevRecord="btnPrevious_Click"
                    OnDropDownShowRecordsChanged="ddlRecords_SelectedIndexChanged" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>
