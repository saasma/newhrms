﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Manager;
using Utils.Helper;
using BLL;
using DAL;
using Utils.Web;
using BLL.Base;
using BLL.BO;
using Bll;

namespace Web.CP
{
    public partial class AdvanceBonusDetails : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.AdministratorTools;
            }
        }

        CommonManager commonMgr = new CommonManager();
        PayManager payMgr = new PayManager();

        #region "Control state related"
        private string _sortBy = "EIN";
        private SortDirection _sortDirection = SortDirection.Ascending;

        protected override void OnInit(EventArgs e)
        {
            Page.RegisterRequiresControlState(this);
            base.OnInit(e);
        }
        protected override void LoadControlState(object savedState)
        {
            object[] rgState = (object[])savedState;
            base.LoadControlState(rgState[0]);
            _sortBy = rgState[1].ToString();
            _sortDirection = (SortDirection)rgState[2];
            //_tempCurrentPage = (int)rgState[2];

        }
        protected override object SaveControlState()
        {
            object[] rgState = new object[3];
            rgState[0] = base.SaveControlState();
            rgState[1] = _sortBy;
            rgState[2] = _sortDirection;
            // rgState[2] = _tempCurrentPage;
            return rgState;
        }
        #endregion

        int bonusId = 0;

        DAL.Bonus bonusEntity = null;

        protected void Page_Load(object sender, EventArgs e)
        {

            bonusId = int.Parse(Request.QueryString["id"]);

           

            if (!IsPostBack)
            {
                Initialise();
            }
            if (Request.Form["__EVENTTARGET"] != null && Request.Form["__EVENTTARGET"].Equals("Reload"))
            {
                LoadBonus(bonusId);        
            }
           
            JavascriptHelper.AttachPopUpCode(Page, "insertUpdateGratuityRule", "AEBonus.aspx", 600, 700);
            JavascriptHelper.AttachPopUpCode(Page, "updateBonus", "AEBonusEmp.aspx", 400, 300);
            JavascriptHelper.AttachNonDialogPopUpCode(Page, "importPopup", "../ExcelWindow/BonusExcel.aspx", 450, 500);

            Page.ClientScript.RegisterClientScriptBlock(GetType(), "sdfsdf", "bonusId = " + bonusId + ";", true);

           
        }

        public bool IsFinalBonus
        {
            set { }
            get
            {
                if (bonusEntity == null)
                    bonusEntity = commonMgr.GetBonus(bonusId);

                return bonusEntity.IsFinalBonus != null && bonusEntity.IsFinalBonus.Value;
            }
        }

        void Initialise()
        {
            DAL.Bonus bonus1 = commonMgr.GetBonus(bonusId);
            title.InnerHtml += " : " + bonus1.Name;


            //if (bonus1.IsFinalBonus != null && bonus1.IsFinalBonus.Value)
            //{
            //    gvwEmployees.Columns[18].HeaderText = "SWF Bonus Amount";
            //}
            //else
            //    gvwEmployees.Columns[19].Visible = false;

            IncomeList();
            LoadGratuity();

            LoadBonus(bonusId);
        }
        void LoadGratuity()
        {
            gvwGratuityRules.DataSource = CommonManager.GetBonusList().Where(x => x.BonusId == bonusId);
            gvwGratuityRules.DataBind();

           
        }

        public string GetYear(object yearid)
        {
            FinancialDate year = CommonManager.GetFiancialDateById(int.Parse(yearid.ToString()));
            year.SetName(IsEnglish);
            return year.Name;
        }

        public string GetApplicableTo(object val)
        {
           
            return "";
        }

      
        void IncomeList()
        {
          
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

           
        }

        public string GetEligibilityType(object type)
        {
            int val = int.Parse(type.ToString());

            if (val == 0)
                return "Not Eligible";
            if (val == 1)
                return BonusEligiblity.Full.ToString(); ;

            return BonusEligiblity.Proportionate.ToString();
        }

        protected void btnAddNew_Click(object sender, EventArgs e)
        {
           
        }



        public void btnTab_click(object sender, EventArgs e)
        {
            LoadBonus(bonusId);
        }

        void LoadBonus(int bonusId)
        {
            hdnBonusId.Value = bonusId.ToString();

            LoadGratuity();

            int eligiblity = -1;

            if (tab.ActiveTabIndex == 0)
                eligiblity = 1;
            else
                eligiblity = 0;

            int totalRecords = 0;

            List<BonusEmployeeBO> list = CommonManager.GetBonusList(bonusId
                , pagingCtl.CurrentPage - 1, int.Parse(pagingCtl.DDLRecords.SelectedValue), ref totalRecords,txtEmpSearchText.Text.Trim()
                , eligiblity);

            gvwEmployees.DataSource = list;
            gvwEmployees.DataBind();

              if (totalRecords == 0)
                pagingCtl.Visible = false;
            else
                pagingCtl.Visible = true;
            pagingCtl.UpdatePagingBar(totalRecords);

            //int totalRecords = 0;
            //gvwList.DataSource = ProjectManager.GetProjectList(
            //    GetEngDate(calFilterFrom.SelectedDate.ToString()), GetEngDate(calFilterTo.SelectedDate.ToString()),
            //    chkAllProjects.Checked, txtSearch.Text.Trim(), pagintCtl.CurrentPage, int.Parse(pagintCtl.DDLRecords.SelectedValue), ref totalRecords);


            //gvwList.DataBind();



        }

        protected void btnPostToAddOn_Click(object sender, EventArgs e)
        {
            Status status = CommonManager.PostBonusToAddOn(bonusId);
            if (status.IsSuccess)
            {
                divMsgCtl.InnerHtml = "Bonus amount posted to Add On.";
                divMsgCtl.Hide = false;
            }
            else
            {
                divWarningMsg.InnerHtml = status.ErrorMessage;
                divWarningMsg.Hide = false;
            }
        }

        protected void gvwEmployees_Sorting(object sender, GridViewSortEventArgs e)
        {

            if (e.SortExpression != _sortBy)
            {
                e.SortDirection = SortDirection.Ascending;
                _sortDirection = e.SortDirection;
            }
            else if (_sortDirection == SortDirection.Ascending)
            {
                e.SortDirection = SortDirection.Descending;
            }
            else if (_sortDirection == SortDirection.Descending)
            {
                e.SortDirection = SortDirection.Ascending;
            }

            _sortDirection = e.SortDirection;

            _sortBy = e.SortExpression;


            e.Cancel = true;

            if (!string.IsNullOrEmpty(hdnBonusId.Value))
                //pagintCtl.CurrentPage = 1;
                //_tempCurrentPage = 1;
                LoadBonus(int.Parse(hdnBonusId.Value));
        }


        protected void btnPrevious_Click()
        {
            pagingCtl.CurrentPage -= 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage -= 1;
            LoadBonus(bonusId);
        }

        protected void btnNext_Click()
        {
            pagingCtl.CurrentPage += 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage += 1;
            LoadBonus(bonusId);
        }

        protected void ddlRecords_SelectedIndexChanged()
        {
            //SessionManager.EmployeeDisplayRecodsPerPage = int.Parse(pagintCtl.DDLRecords.SelectedValue);
            //_tempCurrentPage = 1;
            pagingCtl.CurrentPage = 1;
            LoadBonus(bonusId);
        }

        protected void btnLoad_Click(object sender, EventArgs e)
        {
            LoadBonus(bonusId);
        }
        protected void btnExport_Click(object sender, EventArgs e)
        {
            int total = 0;

            int eligiblity = -1;

            if (tab.ActiveTabIndex == 0)
                eligiblity = 1;
            else
                eligiblity = 0;

            DAL.Bonus bonus1 = commonMgr.GetBonus(bonusId);

            List<BonusEmployeeBO> list = CommonManager.GetBonusList(bonusId, 0, 9999999, ref total, "", eligiblity);

            List<string> hiddenList = new List<string> { "Status", "Eligibility", "OtherUneligibleDaysReason", "BonusId", "Bonus", "NIBLFinalBonusGroupType" };

            if (bonus1.IsFinalBonus == null || bonus1.IsFinalBonus == false)
            {
                hiddenList.Add("AdvanceBonus");
                hiddenList.Add("AnnualSalary");
                hiddenList.Add("FinaBonus30Percent");
                hiddenList.Add("FinalBonus70Percent");
            }
            else
                hiddenList.Add("Proportion");

            string maxBonusName = bonus1.IsFinalBonus != null && bonus1.IsFinalBonus.Value ? "SWF Bonus Amount" : "Maximum Bonus";
           // string bonus = bonus1.IsFinalBonus != null && bonus1.IsFinalBonus.Value ? "Bonus" : "Bonus";

            ExcelHelper.ExportToExcel("Bonus List", list,
                hiddenList,
                new List<String>() { },
                new Dictionary<string, string>() {{"SWFMonthlyBased","SWF Bonus Amount Monthly Based"},{"FinaBonus30Percent","30 % Amount"},{"FinalBonus70Percent","70 % Amount"},{"AdvanceBonus","Advance Bonus"}
                    ,{"AdvanceBonusMonthlySalaryBased","Advance Bonus Monthly Salary Based"}, {"EmployeeId","EIN" },{"Name","Employee"},{"StatusName","Status"},{"JoinDate","Joined"},
                    {"StdWorkDays","Standard Days"},{"OtherUneligibleDays","Stop Pay Days"},{"OtherUneligibleDaysReason","Other Deduct Days Reason"}
                ,{"ActualWorkdays","Actual Days"},{"EligibilityName","Eligibility"},{"Cat","Group"},{"Bonus","Calculated Bonus"}
                ,{"MaxBonus",maxBonusName},{"BonusPaid","Remaining Bonus"}},
                new List<string>() { "FinaBonus30Percent","FinalBonus70Percent","AnnualSalary","SWFMonthlyBased","AdvanceBonusMonthlySalaryBased","NetPaid","AdvanceBonus","SST","TDS", "MonthlySalary", "Bonus", "MaxBonus", "BonusPaid", "StdWorkDays", "OtherUneligibleDays","ActualWorkdays","UnpaidLeave" },
                new Dictionary<string, string>() { { "Bonus", bonus1.Name} },
                new List<string> { });
        }
        protected void btnGenerate_Click(object sender, EventArgs e)
        {

            bool status = CommonManager.GenerateBonus(bonusId, true, false);
            if (status)
            {
                divMsgCtl.InnerHtml = "Employee list generated for Bonus.";
                divMsgCtl.Hide = false;

                LoadBonus(bonusId);
            }
            else
            {
                divWarningMsg.InnerHtml = "Bonus already calculated, can not be changed.";
                divWarningMsg.Hide = false;
            }
        }

        protected void btnCalculate_Click(object sender, EventArgs e)
        {
            bool status = CommonManager.GenerateBonus(bonusId, false, true);

            if (status)
            {

                divMsgCtl.InnerHtml = "Bonus calculated.";
                divMsgCtl.Hide = false;

                LoadGratuity();

                LoadBonus(bonusId);
            }
            else
            {
                divWarningMsg.InnerHtml = "Bonus already calculated, can not be changed.";
                divWarningMsg.Hide = false;
            }
        }
    }
}
