<%@ Page Title="Leave Transfer" Language="C#" MasterPageFile="~/Master/Details.Master"
    AutoEventWireup="true" CodeBehind="LeaveTransfer.aspx.cs" Inherits="Web.CP.LeaveTransfer" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="../Scripts/jquery-1.3.2.min.js"></script>
    <script type="text/javascript">
        function LoadBalance(activeControl) {
            var txtDonorBalance = document.getElementById('<%=txtDonorBalance.ClientID%>');
            var txtReveiverBalance = document.getElementById('<%=txtReveiverBalance.ClientID%>');
            var value = activeControl.options[activeControl.selectedIndex].value;

            if (activeControl.id == '<%=ddlDonor.ClientID %>'.toString()) {

                if (activeControl.selectedIndex != "0")
                    txtDonorBalance.value = value.split(":")[1];
                else
                    txtDonorBalance.value = "";

            }

            else if (activeControl.id == '<%=ddlReceiver.ClientID %>') {

                if (activeControl.selectedIndex != "0")
                    txtReveiverBalance.value = value.split(":")[1];
                else
                    txtReveiverBalance.value = "";
            }

            else {

                txtDonorBalance.value = "";
                txtReveiverBalance.value = "";
            }

        }

        function IsReveiverDonorSame() {

            var ddlDonor = document.getElementById('<%=ddlDonor.ClientID%>');
            var ddlReceiver = document.getElementById('<%=ddlReceiver.ClientID%>');
            //            if (ddlDonor.selectedIndex == "-1" || ddlReceiver.selectedIndex == "-1") {

            //                alert("Donor or Receiver is not selected.\nPlease select Donor or Receiver");
            //                return false;
            //            }

            ////            if (ddlDonor.selectedIndex == "0" || ddlReceiver.selectedIndex == "0") {

            ////                alert("Donor or Receiver is not selected.\nPlease select Donor or Receiver");
            ////                return false;
            ////                        }

            valGroup = "Donate";
            if (!CheckValidation())
                return false;

            if (ddlDonor.options[ddlDonor.selectedIndex].value == ddlDonor.options[ddlReceiver.selectedIndex].value) {

                alert("Donor and Receiver are same.\nLeave can not be transfered to same employee.\nPlease select different Donor or Receiver.");
                return false;
            }



            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <div class="contentArea">
        <%--<h2>
            Leave Adjustment</h2>--%>
        <div class="attribute">
            <table>
                <tr>
                    <td>
                        <strong>Payroll Period</strong>
                    </td>
                    <td>
                        <strong>Leave Type</strong>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:DropDownList AppendDataBoundItems="true" ID="ddlPayrollPeriods" runat="server"
                            DataTextField="Name" DataValueField="PayrollPeriodId" AutoPostBack="true" OnSelectedIndexChanged="ddlPayrollPeriodss_SelectedIndexChanged">
                            <asp:ListItem Text="--Select Payroll Period--" Value="-1"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlPayrollPeriods"
                            InitialValue="-1" Display="None" ErrorMessage="Please select payroll period."
                            ValidationGroup="Load"></asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <asp:DropDownList AppendDataBoundItems="true" Width="180px" ID="ddlLeaves" runat="server"
                            DataTextField="Title" DataValueField="LeaveTypeId" AutoPostBack="true" OnSelectedIndexChanged="ddlLeaves_SelectedIndexChanged">
                            <asp:ListItem Text="--Select Leave--" Value="-1"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlLeaves"
                            InitialValue="-1" Display="None" ErrorMessage="Please select leave." ValidationGroup="Load"></asp:RequiredFieldValidator>
                    </td>
                </tr>
            </table>
        </div>

         <uc2:MsgCtl ID="MsgCtl1" EnableViewState="false" Hide="true" runat="server" />
        <uc2:WarningCtl ID="WarningCtl1" EnableViewState="false" Hide="true" runat="server" />

        <div class="clear" style="margin-top: 10px" runat="server" id="divLeaveTransfer">
            <table>
                <tr>
                    <th>
                        Donor
                    </th>
                    <th>
                        Current Balance
                    </th>
                    <th>
                        Receiver
                    </th>
                    <th>
                        Current Balance
                    </th>
                    <th>
                        Donating Leave
                    </th>
                </tr>
                <tr>
                    <td>
                        <asp:DropDownList ID="ddlDonor" Width="180px" runat="server" onchange="Javascript:LoadBalance(this)">
                            <asp:ListItem Text="--Select Donor--" Value="-1"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlDonor"
                            InitialValue="-1" Display="None" ErrorMessage="Please select doner." ValidationGroup="Donate"></asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <asp:TextBox style='text-align:right' BackColor="LightGray" ReadOnly="true" ID="txtDonorBalance" runat="server" />
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlReceiver" Width="180px" runat="server" onchange="Javascript:LoadBalance(this)">
                            <asp:ListItem Text="--Select Receiver--" Value="-1"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="ddlReceiver"
                            InitialValue="-1" Display="None" ErrorMessage="Please select receiver." ValidationGroup="Donate"></asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <asp:TextBox style='text-align:right'  BackColor="LightGray" ReadOnly="true" ID="txtReveiverBalance" runat="server" />
                    </td>
                    <td>
                        <asp:TextBox style='text-align:right'  ID="txtDonatingHours" runat="server" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtDonatingHours"
                            Display="None" ErrorMessage="Donation is required." ValidationGroup="Donate"></asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="cval" Display="None" runat="server" ErrorMessage="Invalid donation."
                            ControlToValidate="txtDonatingHours" Type="Double" ValidationGroup="Donate" ValueToCompare="0"
                            Operator="GreaterThan" />
                        <asp:CompareValidator ID="CompareValidator1" Display="None" runat="server" ErrorMessage="Not enough balance for transfer."
                            ControlToValidate="txtDonatingHours" Type="Double" ValidationGroup="Donate" ControlToCompare="txtDonorBalance"
                            Operator="LessThanEqual" />
                    </td>
                </tr>
                <tr>
                    <td colspan="5" align="right">
                        <asp:Button ID="btnDonate" CssClass="update" ValidationGroup="Donate" runat="server"
                            Text="Donate" OnClientClick="return IsReveiverDonorSame();" OnClick="btnDonate_Click" />
                    </td>
                </tr>
            </table>
        </div>
        <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
        <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
        <div class="clear" style="margin-top: 10px">
            <cc2:EmptyDisplayGridView CssClass="tableLightColor" UseAccessibleHeader="true" GridLines="None"
                ID="gvwLeaveTransfer" runat="server" ShowFooterWhenEmpty="False" ShowHeaderWhenEmpty="True"
                AutoGenerateColumns="False">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <%--<asp:Label ID="lblRowId" runat="server" Text='<%#(((GridViewRow), Container).RowIndex)%>' />--%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderStyle-Width="140px" DataField="Title" HeaderStyle-HorizontalAlign="Left"
                        ItemStyle-HorizontalAlign="Left" HeaderText="Leave Name"></asp:BoundField>
                    <asp:BoundField HeaderStyle-Width="180px" DataField="DonorName" HeaderStyle-HorizontalAlign="Left"
                        ItemStyle-HorizontalAlign="Left" HeaderText="Donor Employee"></asp:BoundField>
                    <asp:BoundField HeaderStyle-Width="180px" DataField="ReceiverName" HeaderStyle-HorizontalAlign="Left"
                        ItemStyle-HorizontalAlign="Left" HeaderText="Receiver Employee"></asp:BoundField>
                    <asp:BoundField HeaderStyle-Width="120px" DataFormatString="{0:0.##}" DataField="LeaveHours"
                        HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" HeaderText="Leave Transfered">
                    </asp:BoundField>
                </Columns>
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                <EmptyDataTemplate>
                    <b>Leave Transfer not found in selected leave type of selected payroll period.</b>
                </EmptyDataTemplate>
            </cc2:EmptyDisplayGridView>
        </div>
    </div>
</asp:Content>
