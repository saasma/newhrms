<%@ Page Title="Deduction List" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="ManageDeductions.aspx.cs" Inherits="Web.CP.ManageDeductions" %>

<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        function popupAddDeductionCall() {

            var ret = popupDeduction();

            if (typeof (ret) != 'undefined') {
                if (ret == 'Reload') {
                    //refreshIncomeList(0);
                    __doPostBack('<%= updPanel.ClientID %>', '')
                }
            }

            return false;
        }

        function reloadDeduction(popupWindow) {
            popupWindow.close();
            __doPostBack('<%= updPanel.ClientID %>', '');
        }

        function popupUpdateDeductionCall(deductionId) {
            var ret = popupDeduction('DId=' + deductionId);

            if (typeof (ret) != 'undefined') {
                if (ret == 'Reload') {
                    //refreshIncomeList(0);
                    __doPostBack('<%= updPanel.ClientID %>', '');
                }
            }
            return false;
        }


        $(document).ready(function () {




            //$(".tiptip").tipTip();
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Deduction list
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div runat="server" style="color: #F49D25" id="voucherAlsoChange">
            Note : If voucher is being used, please associate the newly added deduction to the
            voucher group also.</div>
        <asp:UpdatePanel ID="updPanel" runat="server">
            <ContentTemplate>
                <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
                <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
                <asp:GridView Style="clear: both" PagerStyle-HorizontalAlign="Center" PagerStyle-CssClass="defaultPagingBar"
                    CssClass="table table-primary mb30 table-bordered table-hover" UseAccessibleHeader="true" ID="gvwDeductions" runat="server"
                    CellPadding="3" DataKeyNames="DeductionId" AutoGenerateColumns="false" rateColumns="False"
                    GridLines="None" OnRowDeleting="gvwDeductions_RowDeleting">
                    <%-- <RowStyle BackColor="#E3EAEB"  />--%>
                    <Columns>
                        <asp:TemplateField HeaderText="Title" HeaderStyle-Width="170px" HeaderStyle-HorizontalAlign="Left"
                            ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <%# Eval("Title") %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Short name" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left"
                            ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <%# Eval("Abbreviation") %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Type" HeaderStyle-Width="200px" HeaderStyle-HorizontalAlign="Left"
                            ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <%# new  DAL.DeductionCalculation().Get( Eval("Calculation").ToString()) %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Notes" HeaderStyle-Width="150px" HeaderStyle-HorizontalAlign="Left"
                            ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <%#  Eval("Notes")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Active" HeaderStyle-Width="70px" HeaderStyle-HorizontalAlign="Left"
                            ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <%#  (Convert.ToBoolean(Eval("IsEnabled")) == true ? "Yes" : "")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50px"
                            HeaderText="Order" ItemStyle-HorizontalAlign="Right">
                            <ItemTemplate>
                                <asp:TextBox data-col='0' data-row='<%# Container.DataItemIndex %>' runat="server"
                                    Style='text-align: right' Width="50" ID="txtOrder" Text='<%# Eval("Order") %>'></asp:TextBox>
                                <asp:CompareValidator runat="server" ID="valOrder" Type="Integer" Operator="DataTypeCheck"
                                    Display="None" ValidationGroup="saveupdate" ControlToValidate="txtOrder"></asp:CompareValidator>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-Width="30px" HeaderText="Edit" HeaderStyle-HorizontalAlign="Center"
                            ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:ImageButton ID="btnEdit" CommandName="Delete" ImageUrl="~/images/edit.gif" runat="server"
                                    OnClientClick='<%# "return popupUpdateDeductionCall(" +  Eval("DeductionId") + ");" %>'
                                    AlternateText="Edit" />
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-Width="30px" HeaderText="Delete" HeaderStyle-HorizontalAlign="Center"
                            ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:ImageButton ID="btnDelete" Visible='<%# BLL.Manager.PayManager.IsDeductionDeletable ( (int) Eval( "DeductionId") ) %>'
                                    CommandName="Delete" ImageUrl="~/images/delet.png" runat="server" OnClientClick='return confirm("Do you want to delete the deduction?")'
                                    AlternateText="Delete" />
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:TemplateField>
                        <%--  <asp:HyperLinkField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"  DataNavigateUrlFields="CompanyId"  
                DataNavigateUrlFormatString="AddEditCompany.aspx?Id={0}" >
              
               </asp:HyperLinkField>--%>
                        <%--<asp:HyperLinkField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"  DataNavigateUrlFields="CompanyId" 
                DataNavigateUrlFormatString="ManageBranch.aspx?Id={0}" Text="Manage branch" />--%>
                    </Columns>
                    <RowStyle CssClass="odd" />
                    <AlternatingRowStyle CssClass="even" />
                    <%--<FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />--%>
                    <EmptyDataTemplate>
                        No deductions
                    </EmptyDataTemplate>
                    <%--<SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
        <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
        <EditRowStyle BackColor="#7C6F57" />
        <AlternatingRowStyle BackColor="White" />--%>
                </asp:GridView>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="buttonsDiv">
            <asp:LinkButton ID="btnAddNew" Style="margin-left: 10px;height:30px;width:120px;" OnClientClick="return popupAddDeductionCall();"
                runat="server" CssClass="createbtns floatleft" Text="Add Deduction" />
            <asp:Button ID="Button1" runat="server" CssClass="save" Text="Save Order" OnClick="Unnamed1_Click"
                OnClientClick="valGroup='saveupdate';return CheckValidation();" />
        </div>
    </div>
</asp:Content>
