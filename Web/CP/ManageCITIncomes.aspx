<%@ Page Title="CIT Incomes" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="ManageCITIncomes.aspx.cs" Inherits="Web.CP.ManageCITIncomes" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        function insertUpdateGratuityRuleCall(gratuityRuleId) {
            var ret = insertUpdateGratuityRule('Id=' + gratuityRuleId);

            if (typeof (ret) != 'undefined') {
                if (ret == 'Reload') {
                    __doPostBack('Reload', '');
                }
            }
        }

        var chkAll = '#<%= chkAll.ClientID %>';

        function refresh(popupWindow) {
            popupWindow.close();
            __doPostBack('Reload', '');
        }

        function changeStates(chk) {
            var isChecked = $(chkAll).is(':checked');

            $("INPUT[type='checkbox']").attr('checked', isChecked);

        }


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    CIT Incomes
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="contentArea">
            <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
            <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
            <br />
            <asp:LinkButton ID="btnAddNew" runat="server" Style='margin-bottom: 5px;height:30px;width:100px' CssClass="createbtns"
                Text="Edit Incomes" OnClick="btnAddNew_Click" />
            <div class="attribute" style='background: url()!important' runat="server" id="section">
                <span style="padding: 0pt 0pt 8px 2px; display: block;">
                    <asp:CheckBox Enabled="false" ID="chkAll" onclick="changeStates(this)" runat="server" />
                    <b style="font-size: 16px;"><b>Salary includes:</b></span>
                <%--<div style="overflow-y: scroll; height: 110px; width: 95%; border: 1px solid #333">--%>
                <asp:CheckBoxList Enabled="false" ID="chkListSalaries" RepeatColumns="3" RepeatDirection="Horizontal"
                    runat="server" DataTextField="Title" DataValueField="IncomeId">
                </asp:CheckBoxList> 
                <br />
                <asp:Button ID="btnSave" Visible="false" runat="server" Text="Save" CssClass="save"
                    OnClick="btnSave_Click" />
                <%--</div>--%>
            </div>
        </div>
    </div>
</asp:Content>
