<%@ Page Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/Master/Details.Master"
    AutoEventWireup="true" CodeBehind="GradeStep.aspx.cs" Title="Grade Step" Inherits="Web.CP.GradeStep" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Assembly="Karpach.WebControls" Namespace="Karpach.WebControls" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/UpDownCtl.ascx" TagName="UpDownCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="InfoMsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .colorStyle td
        {
            padding: 2px !important;
        }
        .transparent
        {
            /* this class makes a window partially transparent */
            opacity: 0; /* Standard style for transparency */
            -moz-opacity: 0; /* Transparency for older Mozillas */
            filter: alpha(opacity=00); /* Transparency for IE */
            width: 0;
        }
        .paddinAll
        {
            padding: 10px;
        }
        div.notify
        {
            margin-left: 0;
            width: 41%;
        }
        .lf
        {
            width: 150px;
            padding: 0 0px 0px 0;
        }
        .lfs
        {
            width: 160px;
            padding: 0 10px 0px 0;
            vertical-align: top;
        }
        table tr td
        {
            padding-bottom: 10px;
        }
        .bevel
        {
            margin-bottom: 20px;
        }
        
        div.notify
        {
            width: 510px;
        }
        .displayInline
        {
            display: inline;
        }
    </style>
    <script type="text/javascript">
        function closePopup() {
            // alert(window.opener.parentReloadCallbackFunction)
            if ($.browser.msie == false && typeof (window.opener.reloadLeave) != 'undefined' && ($.browser.chrome || $.browser.safari)) {
                window.opener.reloadLeave(window);
            } else {
                window.returnValue = "Reload";
                window.close();
            }
        }

        function GradePopUpCall() {
            var ret = PopUpGrade("isPopup=true");
            if (typeof (ret) != 'undefined') {
                //ChangeDropDownList('ddlDepartments.ClientID ', ret);
            }
        }

        function StepPopUpCall() {
            var ret = PopUpStep("isPopup=true");
            if (typeof (ret) != 'undefined') {
                //ChangeDropDownList('ddlDepartments.ClientID ', ret);
            }
        }
        function PopUpPositionCall() {
            var ret = PopUpPosition("isPopup=true");
            if (typeof (ret) != 'undefined') {
                //ChangeDropDownList('ddlDepartments.ClientID ', ret);
            }
        }

        function parentReloadCallbackFunction(childWindow) {
            childWindow.close();
            
        }
     
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <asp:HiddenField ID="hdEditSequence" runat="server" />
    <asp:HiddenField ID="hdEmployeeEditSequence" runat="server" />
    <div class="contentArea">
        <div class="attribute">
            <table>
                <tr>
                    <td>
                        <strong>Position </strong>&nbsp;                        
                        <asp:DropDownList ID="ddlPosition" runat="server" DataTextField="Name" DataValueField="PositionId" />
                    </td>
                    <td>
                        <asp:LinkButton ID="LinkButton1" runat="server" OnClientClick="PopUpPositionCall();" Text="Manage Position"></asp:LinkButton>
                    </td>
                    <td>
                        <asp:LinkButton ID="LinkButton2" runat="server" Text="Manage Grade" OnClientClick="GradePopUpCall();"></asp:LinkButton>
                    </td>
                    <td>
                        <asp:LinkButton ID="LinkButton3" runat="server" Text="Manage Step"  OnClientClick="StepPopUpCall();"></asp:LinkButton>
                    </td>
                </tr>
            </table>
        </div>
        <br />
        <div style="clear: both" id="divGrid" runat="server">

        </div>
    </div>
</asp:Content>
