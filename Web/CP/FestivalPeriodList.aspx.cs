﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Manager;
using Utils.Helper;
using BLL;
using DAL;
using Utils.Web;
using BLL.Base;
using BLL.BO;
using Bll;

namespace Web.CP
{
    public partial class FestivalPeriodList : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.AdministratorTools;
            }
        }

        CommonManager commonMgr = new CommonManager();
        PayManager payMgr = new PayManager();

        #region "Control state related"
        private string _sortBy = "EIN";
        private SortDirection _sortDirection = SortDirection.Ascending;

        protected override void OnInit(EventArgs e)
        {
            Page.RegisterRequiresControlState(this);
            base.OnInit(e);
        }
        protected override void LoadControlState(object savedState)
        {
            object[] rgState = (object[])savedState;
            base.LoadControlState(rgState[0]);
            _sortBy = rgState[1].ToString();
            _sortDirection = (SortDirection)rgState[2];
            //_tempCurrentPage = (int)rgState[2];

        }
        protected override object SaveControlState()
        {
            object[] rgState = new object[3];
            rgState[0] = base.SaveControlState();
            rgState[1] = _sortBy;
            rgState[2] = _sortDirection;
            // rgState[2] = _tempCurrentPage;
            return rgState;
        }
        #endregion


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialise();
            }
            if (Request.Form["__EVENTTARGET"] != null && Request.Form["__EVENTTARGET"].Equals("Reload"))
            {
                LoadGratuity();               
            }

            JavascriptHelper.AttachPopUpCode(Page, "insertUpdateGratuityRule", "AEFestivalPeriod.aspx", 400, 400);

        }

        void Initialise()
        {
            IncomeList();
            LoadGratuity();
        }
        void LoadGratuity()
        {
            gvwGratuityRules.DataSource = CommonManager.GetFestivalPeriodList();
            gvwGratuityRules.DataBind();

           
        }

       
        public string GetApplicableTo(object val)
        {
           
            return "";
        }

        protected void gvwDepartments_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
        //    int bonusid = (int)gvwGratuityRules.DataKeys[e.RowIndex][0];

        //    if (bonusid != 0)
        //    {
        //        bool deleted = CommonManager.DeleteBonus(bonusid);

        //        if (deleted)
        //        {

        //            divMsgCtl.InnerHtml = "Bonus deleted.";
        //            divMsgCtl.Hide = false;
        //        }
        //        else
        //        {
        //            divMsgCtl.InnerHtml = "Bonus is already distributed, can not be deleted.";
        //            divMsgCtl.Hide = false;
        //        }

        //        LoadGratuity();
        //    }
            //LoadGratuity();
        }

       

        void IncomeList()
        {
          
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

           
        }
        public string GetPeriod(object value)
        {
            return
                CommonManager.GetPayrollPeriod(int.Parse(value.ToString())).Name;
        }
        public string GetEligibilityType(object type)
        {
            int val = int.Parse(type.ToString());

            if (val == 0)
                return BonusEligiblity.NotEligible.ToString();
            if (val == 1)
                return BonusEligiblity.Full.ToString(); ;

            return BonusEligiblity.Proportionate.ToString();
        }

        protected void btnAddNew_Click(object sender, EventArgs e)
        {
           
        }

        public void LoadBonus(int bonusId)
        {
            hdnBonusId.Value = bonusId.ToString();

         

          
        }

        protected void gvwEmployees_Sorting(object sender, GridViewSortEventArgs e)
        {

            if (e.SortExpression != _sortBy)
            {
                e.SortDirection = SortDirection.Ascending;
                _sortDirection = e.SortDirection;
            }
            else if (_sortDirection == SortDirection.Ascending)
            {
                e.SortDirection = SortDirection.Descending;
            }
            else if (_sortDirection == SortDirection.Descending)
            {
                e.SortDirection = SortDirection.Ascending;
            }

            _sortDirection = e.SortDirection;

            _sortBy = e.SortExpression;


            e.Cancel = true;

            if (!string.IsNullOrEmpty(hdnBonusId.Value))
                //pagintCtl.CurrentPage = 1;
                //_tempCurrentPage = 1;
                LoadBonus(int.Parse(hdnBonusId.Value));
        }

        protected void gvwGratuityRules_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            
        }
    }
}
