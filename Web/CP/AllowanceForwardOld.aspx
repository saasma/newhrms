<%@ Page Title="Allowance List" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" EnableViewState="true" CodeBehind="AllowanceForwardOld.aspx.cs"
    Inherits="Web.CP.AllowanceForwardOld" %>

<%@ Import Namespace="BLL.Manager" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../_assets/themes/yui/tabs.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        var loadingButtonWhenSubmitting = false;
        var skipLoadingCheck = true;
        function addSkillSetToEmployeePopup(requestid) {
            var theText = requestid.getAttribute("value");
            positionHistoryPopup("isPopup=true&reqid=" + theText);
        }

        function refreshEventList(popupWindow) {
            if (typeof (popupWindow) != 'undefined')
                popupWindow.close();

            __doPostBack('<%=btnLoad.UniqueID %>', '');

        }


        function selectDeselectAll(chk) {
            $('input[type=checkbox]').each(
                    function (index) {
                        if (this.id.indexOf('chkDelete') > 0)
                            this.checked = chk.checked;
                    }
                );
        }

        function assignOvertime() {

            assignovertimePopup("isPopup=true&assign=true");
        }
       
    </script>
    <script src="../Employee/override.js" type="text/javascript"></script>
    <style type="text/css">
        .tableLightColor a:hover
        {
            color: #048FC2;
            text-decoration: none;
        }
        #content
        {
            padding: 0px !important;
        }
        .contentLeftBlockRemoval
        {
            padding-left: 0px;
        }
        .mb30 th
        {
            font-weight: normal !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <ext:ResourceManager ID="ResourceManager1" DisableViewState="false" runat="server"
        ScriptMode="Release" />
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Allowance Requests
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="attribute">
            <table class="fieldTable">
                <tr>
                    <td>
                        Period
                    </td>
                    <td>
                        From
                    </td>
                    <td>
                        To
                    </td>
                    <td id="brTd" runat="server">
                        Branch
                    </td>
                    <td style="padding-left: 5px">
                        Search Employee
                    </td>
                    <td style="padding-left: 5px; display: none;">
                        Status
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:DropDownList ID="ddlType" runat="server">
                            <asp:ListItem Value="-1" Text="All" Selected="True" />
                            <asp:ListItem Value="1">This Week</asp:ListItem>
                            <asp:ListItem Value="2">This Month</asp:ListItem>
                            <asp:ListItem Value="3">Last Month</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td runat="server" id="t2">
                        <ext:DateField ID="dateFrom" runat="server" Width="120px">
                            <Plugins>
                                <ext:GenericPlugin ID="HighlightDates1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                            </Plugins>
                        </ext:DateField>
                    </td>
                    <td runat="server" id="t3">
                        <ext:DateField ID="dateTo" runat="server" Width="120px">
                            <Plugins>
                                <ext:GenericPlugin ID="GenericPlugin1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                            </Plugins>
                        </ext:DateField>
                    </td>
                    <td id="brTd2" runat="server">
                        <asp:DropDownList ID="ddlBranch" AutoPostBack="false" AppendDataBoundItems="true"
                            DataTextField="Name" DataValueField="BranchId" runat="server" Width="150px">
                            <asp:ListItem Text="--Select Branch--" Value="-1" Selected="True" />
                        </asp:DropDownList>
                    </td>
                    <td style="padding-left: 5px">
                        <asp:TextBox Width="160px" ID="txtEmpSearchText" runat="server"></asp:TextBox>
                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtEmpSearchText"
                            WatermarkText="Employee Name" WatermarkCssClass="searchBoxText" />
                        <cc1:AutoCompleteExtender EnableCaching="true" ID="AutoCompleteExtenderOrganization"
                            runat="server" MinimumPrefixLength="2" ServiceMethod="GetEmployeeNames" ServicePath="~/PayrollService.asmx"
                            TargetControlID="txtEmpSearchText" CompletionSetCount="10" CompletionInterval="250"
                            CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList"
                            CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
                        </cc1:AutoCompleteExtender>
                    </td>
                    <td style="padding-left: 5px; display: none;">
                        <asp:DropDownList ID="ddlStatus" runat="server">
                            <asp:ListItem Text="All" Selected="True" Value="-1" />
                            <asp:ListItem Value="0">Pending</asp:ListItem>
                            <asp:ListItem Value="1">Recommended</asp:ListItem>
                            <asp:ListItem Value="2">Approved</asp:ListItem>
                            <asp:ListItem Value="3">Rejected</asp:ListItem>
                            <asp:ListItem Value="4">Forwarded/Posted</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                    </td>
                    <td valign="top">
                        <asp:Button ID="btnLoad" CssClass="load" OnClientClick="valGroup='AEEmployee';return CheckValidation();"
                            OnClick="btnLoad_Click" runat="server" Text="Load" />
                    </td>
                    <td>
                        <asp:LinkButton ID="btnAssign" OnClientClick="assignOvertime(); return false;" CssClass="save"
                            Style="height: 16px; width: 140px" runat="server" Text="Assign Allowance" />
                    </td>
                </tr>
            </table>
            <%--</ContentTemplate>
            </asp:UpdatePanel>--%>
        </div>
        <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" Width="390px" runat="server" />
        <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
        <div class="clear">
            <cc1:TabContainer ID="tab" runat="server" AutoPostBack="true" OnActiveTabChanged="btnTab_click"
                CssClass="yui" ActiveTabIndex="0">
                <cc1:TabPanel runat="server" ID="tabAll">
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="lblAll" Text="All" />
                    </HeaderTemplate>
                </cc1:TabPanel>
                <cc1:TabPanel runat="server" ID="tabPending">
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="lblPending" Text="Pending" />
                    </HeaderTemplate>
                </cc1:TabPanel>
                <cc1:TabPanel runat="server" ID="tabRecommended">
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="lblRecommended" Text="Recommended" />
                    </HeaderTemplate>
                </cc1:TabPanel>
                <cc1:TabPanel runat="server" ID="tabApproved">
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="Approved" Text="Approved" />
                    </HeaderTemplate>
                </cc1:TabPanel>
                <cc1:TabPanel runat="server" ID="tabRejected">
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="Rejected" Text="Rejected" />
                    </HeaderTemplate>
                </cc1:TabPanel>
                <cc1:TabPanel runat="server" ID="tabForwardedPosted">
                    <HeaderTemplate>
                        <asp:Label runat="server" ID="lblForwardedPosted" Text="Forwarded/Posted" />
                    </HeaderTemplate>
                </cc1:TabPanel>
            </cc1:TabContainer>
            <cc2:EmptyDisplayGridView CssClass="table table-primary mb30 table-bordered table-hover"
                UseAccessibleHeader="true" ShowHeaderWhenEmpty="True" ID="gvw" runat="server"
                DataKeyNames="RequestID" AutoGenerateColumns="False" CellPadding="4" GridLines="None"
                ShowFooterWhenEmpty="False" OnRowCreated="gvwEmployees_RowCreated">
                <Columns>
                    <asp:TemplateField HeaderStyle-Width="20px" HeaderText="Select" ItemStyle-HorizontalAlign="Center"
                        HeaderStyle-HorizontalAlign="Center">
                        <HeaderTemplate>
                            <asp:CheckBox ID="chk12Delete1" onclick="selectDeselectAll(this)" runat="server" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:CheckBox Visible='<%# Eval("Status").ToString() == "2" ? true : false %>' ID="chkDelete"
                                runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderStyle-Width="40px" HeaderStyle-HorizontalAlign="Left" HeaderText="EIN"
                        DataField="EmployeeID"></asp:BoundField>
                    <%--<asp:BoundField DataField="Date" DataFormatString="{0:dd-M-yyyy}" HeaderStyle-Width="90px"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="Date"></asp:BoundField>--%>
                    <asp:BoundField DataField="EmployeeName" HeaderStyle-Width="160px" HeaderStyle-HorizontalAlign="Left"
                        HeaderText="Employee"></asp:BoundField>
                    <%--<asp:BoundField DataField="Name" HeaderStyle-Width="180px" HeaderStyle-HorizontalAlign="Left" HeaderText="Name"></asp:BoundField>--%>
                    <asp:BoundField DataField="StartTime" DataFormatString="{0:yyyy/MM/dd}" HeaderStyle-Width="80px"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="Start Date"></asp:BoundField>
                    <asp:BoundField DataField="EndTime" DataFormatString="{0:yyyy/MM/dd}" HeaderStyle-Width="80px"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="End Date"></asp:BoundField>
                    <asp:BoundField DataField="StartDateNep" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Right"
                        HeaderText="Start Date(BS)"></asp:BoundField>
                    <asp:BoundField DataField="EndDateNep" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Right"
                        HeaderText="End Date(BS)"></asp:BoundField>
                    <%-- <asp:BoundField DataField="Position" HeaderText="Position"></asp:BoundField>--%>
                    <asp:BoundField DataField="Duration" HeaderStyle-Width="60px" HeaderStyle-HorizontalAlign="Right"
                        HeaderText="Units"></asp:BoundField>
                    <%--<asp:BoundField DataField="Duration" HeaderStyle-Width="130px" HeaderStyle-HorizontalAlign="Left"
                        HeaderText="Duration (days)"></asp:BoundField>--%>
                    <asp:BoundField DataField="Reason" HeaderStyle-Width="150px" HeaderStyle-HorizontalAlign="Left"
                        HeaderText="Reason"></asp:BoundField>
                    <asp:BoundField DataField="OvertimeType" HeaderStyle-Width="130px" HeaderStyle-HorizontalAlign="Left"
                        HeaderText="Counter Type"></asp:BoundField>
                    <asp:BoundField DataField="SupervisorName" HeaderStyle-Width="120px" HeaderStyle-HorizontalAlign="Left"
                        HeaderText="Proccessed By"></asp:BoundField>
                    <%--<asp:BoundField DataField="SupervisorName" HeaderStyle-Width="120px" HeaderStyle-HorizontalAlign="Left"
                        HeaderText="Processed By"></asp:BoundField>--%>
                    <asp:BoundField DataField="StatusModified" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left"
                        HeaderText="Status"></asp:BoundField>
                    <asp:TemplateField HeaderStyle-Width="20px" HeaderStyle-HorizontalAlign="Left" HeaderText="">
                        <ItemTemplate>
                            <asp:HyperLink NavigateUrl="javascript:void(0)" value='<%# Eval("RequestID") %>'
                                onclick='<%# "addSkillSetToEmployeePopup(this);" %>' runat="server" Text="Edit/View"
                                ID="btnAction">
                            </asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <%--<%# "<a  href='#'  onclick='addSkillSetToEmployeePopup(" + Eval("RequestID") + ")'>Action</a>"%>--%>
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                <EmptyDataTemplate>
                    <b>No list. </b>
                </EmptyDataTemplate>
            </cc2:EmptyDisplayGridView>
            <uc1:PagingCtl ID="pagingCtl" ShowDropDown="true" OnNextRecord="btnNext_Click" OnChangePage="ChangePageNumber"
                OnPrevRecord="btnPrevious_Click" OnDropDownShowRecordsChanged="ddlRecords_SelectedIndexChanged"
                runat="server" />
        </div>
        <div class="buttonsDiv">
            <asp:Button ID="btnReject" CssClass="delete" ToolTip="Reject Approved List" OnClick="btnReject_Click"
                Visible="true" Width="120" CausesValidation="false" runat="server" OnClientClick="return confirm('Are you srue you want to Reject the selected requests?');"
                Text="Reject" />
            <asp:Button ID="btnDelete" CssClass="update" OnClick="btnForward_Click" Visible="true"
                Width="175" CausesValidation="false" runat="server" OnClientClick="return confirm('Are you srue you want to Forward the selected requests?');"
                Text="Forward Selected" />
            <asp:Button ID="btnExport" CssClass="save" OnClick="btnExport_Click" Visible="true"
                CausesValidation="false" runat="server" Text="Export" />
        </div>
    </div>
</asp:Content>
