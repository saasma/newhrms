﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Base;
using BLL.Manager;
using Utils.Helper;
using DAL;

namespace Web.CP
{
    public partial class IncomeList : BasePage
    {
        PayManager payMgr = new PayManager();
        int employeeId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            string eventTarget = Request.Form["__EVENTTARGET"];

            if (string.IsNullOrEmpty(Request.QueryString["EId"]))
            {
                Response.Write("Employee should be selected for leave assignment,please select employee first.");
                Response.End();
                return;
            }

            employeeId = UrlHelper.GetIdFromQueryString("EId");
            if (!IsPostBack)
            {
                Initialise(); 
                
                ddlDesig.DataSource = new CommonManager().GetAllDesignations();
                ddlDesig.DataBind();

                List<Branch> branches = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
                ddlBranch.DataSource = branches;
                ddlBranch.DataBind();

                BizHelper.Load(new JobStatus(), ddlStatus);
            }
            else if (eventTarget != null && eventTarget.Equals("Reload"))
            {
                Initialise();
            }

            JavascriptHelper.AttachPopUpCode(Page, "popupCreateNew", "AEIncome.aspx", 600, 650);
        }

        public void rdbListAddTo_Change(object sender, EventArgs e)
        {
            if (rdbListAddTo.Items[0].Selected)
            {
                ddlDesig.Enabled = false;
                ddlBranch.Enabled = false;
                ddlStatus.Enabled = false;
            }
            else
            {
                ddlDesig.Enabled = true;
                ddlBranch.Enabled = true;
                ddlStatus.Enabled = true;
            }
        }

        void Initialise()
        {
            

            int empId = employeeId;
            lstIncomeList.DataSource = payMgr.GetIncomeListForEmp(empId).OrderBy(x => x.Title).ToList();
            lstIncomeList.DataBind();



        }

        protected void btnOk_Click(object sender, EventArgs e)
        {

            if (Page.IsValid)
            {

                bool result = false;
                foreach (ListItem item in lstIncomeList.Items)
                {
                    if (item.Selected)
                    {
                        result = payMgr.AddIncomeToEmployee(int.Parse(item.Value),
                    employeeId, rdbListAddTo.Items[1].Selected, SessionManager.CurrentCompanyId
                    , int.Parse(ddlDesig.SelectedValue), int.Parse(ddlStatus.SelectedValue)
                    , int.Parse(ddlBranch.SelectedValue));
                    }
                }



                if (result)
                {
                    string msg;
                    if (rdbListAddTo.Items[1].Selected)
                        msg = "Income added to all employees.";
                    else
                        msg = "Income added to the employee.";

                    JavascriptHelper.DisplayClientMsg(msg, this, "closePopup();");
                }
            }
        }
    }
}
