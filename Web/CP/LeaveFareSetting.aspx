<%@ Page MaintainScrollPositionOnPostback="true" Title="Leave Settings" Language="C#"
    MasterPageFile="~/Master/HROld.Master" AutoEventWireup="true" CodeBehind="LeaveFareSetting.aspx.cs"
    Inherits="Web.CP.LeaveFareSetting" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        tr
        {
            padding-top: 10px;
        }
        .headerWithUnderLine
        {
            color: #1F4E78;
            padding-bottom: 2px;
            border-bottom: 1px solid #2F75B5;
            width: 300px;
        }
        .fieldHeader
        {
            padding-top: 15px;
        }
        .fieldHeader span
        {
            color: #000000;
            font-weight: bold;
        }
        input[type=submit]
        {
            cursor: pointer;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Leave Fare Settings
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="panel panel-default">
            <!-- panel-heading -->
            <div class="panel-body">
                <div class="contentArea1">
                    <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
                    <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
                    <table style='clear: both; float: left'>
                        <tr>
                            <td valign="top">
                                <table>
                                    <tr>
                                        <td colspan="2" style="padding-top: 0px">
                                            <h3 class="headerWithUnderLine">
                                                Leave Fare Settings</h3>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fieldHeader" style='width: 190;'>
                                            <My:Label ID="Label4" Text="Select Income Type for leave fare calculation" runat="server" ShowAstrick="false" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <%--<My:Label ID="Label5" Text="IRO name:" runat="server" ShowAstrick="true" /> --%>
                                            <asp:DropDownList Enabled="false" AppendDataBoundItems="true" Width="183px" ID="ddlIncome"
                                                runat="server" DataValueField="IncomeId" DataTextField="Title">
                                                <asp:ListItem Text="" Value="-1"></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlIncome"
                                                InitialValue="-1" Display="None" ErrorMessage="Please select income type." ValidationGroup="AECompany"></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnEdit1" Width="60" Height="22" Style="margin-top: 1px; border: 1px solid gray"
                                                Visible="true" runat="server" Text="Edit" OnClick="btnEdit1_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fieldHeader" colspan="2">
                                            <My:Label ID="Label1" Text="Select Leave Type" runat="server" ShowAstrick="false" />
                                            <%--<My:Label ID="Label5" Text="IRO name:" runat="server" ShowAstrick="true" /> --%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:DropDownList Enabled="false" AppendDataBoundItems="true" Width="183px" ID="ddlLeaves"
                                                runat="server" DataValueField="LeaveTypeId" DataTextField="Title">
                                                <asp:ListItem Text="" Value="-1"></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlLeaves"
                                                InitialValue="-1" Display="None" ErrorMessage="Please select leave type." ValidationGroup="AECompany"></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnEdit2" Width="60" Height="22" Style="margin-top: 1px; border: 1px solid gray"
                                                runat="server" Text="Edit" OnClick="btnEdit2_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fieldHeader" colspan="2">
                                            <My:Label ID="Label2" Text="Eligible full days for Leave Fare" runat="server" ShowAstrick="false" />
                                            <%--<My:Label ID="Label5" Text="IRO name:" runat="server" ShowAstrick="true" /> --%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtFullDays" runat="server" Width="183px" Height="18" Enabled="false" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtFullDays"
                                                InitialValue="-1" Display="None" ErrorMessage="Eligible full days is required."
                                                ValidationGroup="AECompany"></asp:RequiredFieldValidator>
                                            <asp:CompareValidator ValidationGroup="AECompany" ID="CompareValidator5" runat="server"
                                                ControlToValidate="txtFullDays" Type="Integer" Operator="GreaterThanEqual" ValueToCompare="0"
                                                Display="None" ErrorMessage="Eligible full days is invalid." />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnEdit3" Width="60" Height="22" Style="margin-top: 1px; border: 1px solid gray"
                                                runat="server" Text="Edit" OnClick="btnEdit3_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fieldHeader" colspan="2">
                                            <My:Label ID="Label3" Text="Allow Proportionate calculation if days are less than Eligible days"
                                                runat="server" ShowAstrick="false" />
                                            <%--<My:Label ID="Label5" Text="IRO name:" runat="server" ShowAstrick="true" /> --%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:DropDownList Enabled="false" AppendDataBoundItems="true" Width="183px" ID="ddlAllowProportionateCalculation"
                                                runat="server">
                                                <asp:ListItem Text="" Value="-1"></asp:ListItem>
                                                <asp:ListItem Text="Yes" Value="true"></asp:ListItem>
                                                <asp:ListItem Text="No" Value="false"></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="ddlAllowProportionateCalculation"
                                                InitialValue="-1" Display="None" ErrorMessage="Value is required." ValidationGroup="AECompany"></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:Button ID="Button1" Width="60" Height="22" Style="margin-top: 1px; border: 1px solid gray"
                                                runat="server" Text="Edit" OnClick="btnEdit4_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fieldHeader" colspan="2">
                                            <My:Label ID="Label5" Text="Proportionate Calculation Based value, 0 for actual month days" runat="server" ShowAstrick="false" />
                                            <%--<My:Label ID="Label5" Text="IRO name:" runat="server" ShowAstrick="true" /> --%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtProportionateValue" runat="server" Width="183px" Height="18"
                                                Enabled="false" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtProportionateValue"
                                                InitialValue="-1" Display="None" ErrorMessage="Proportionate value is required."
                                                ValidationGroup="AECompany"></asp:RequiredFieldValidator>
                                            <asp:CompareValidator ValidationGroup="AECompany" ID="CompareValidator1" runat="server"
                                                ControlToValidate="txtProportionateValue" Type="Integer" Operator="GreaterThanEqual"
                                                ValueToCompare="0" Display="None" ErrorMessage="Proportionate value is invalid." />
                                        </td>
                                        <td>
                                            <asp:Button ID="Button2" Width="60" Height="22" Style="margin-top: 1px; border: 1px solid gray"
                                                runat="server" Text="Edit" OnClick="btnEdit5_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <div style="clear: both">
                    </div>
                    <asp:Button ID="btnSave" style='margin-top:20px;' CssClass="btn btn-primary btn-sm" OnClientClick="valGroup = 'AECompany';if( CheckValidation() ) { return confirm('Are you sure, you want to change the leave setting?');}"
                        runat="server" Text="Save Changes" ValidationGroup="AECompany" OnClick="btnSave_Click" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
