﻿<%@ Page Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" Title="Insurance" EnableEventValidation="false" CodeBehind="IndividualInsurance.aspx.cs"
    Inherits="Web.CP.IndividualInsurance" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .GridRowHover
        {
            cursor: pointer;
        }
        .anchorHover
        {
            color: Blue !important;
        }
        
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <ext:ResourceManager ID="ResourceManager1" DisableViewState="false" runat="server"
        ShowWarningOnAjaxFailure="false" ScriptMode="Release" Namespace="Rigo" />
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Insurance
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="contentArea">
            <div class="attribute" style="padding: 10px">
                <asp:Label ID="lblYear" runat="server" />
                <asp:Button Style="margin-left: 10px" ID="btnAdd" OnClick="btnAdd_Click" Width="100px"
                    CausesValidation="false" CssClass="save" runat="server" Text="Add" />
                <%-- <strong>Branch Filter</strong>
            <asp:DropDownList Width="200px" ID="ddlFilterByBranch" runat="server" DataValueField="BranchId"
                DataTextField="Name" AppendDataBoundItems="true" OnSelectedIndexChanged="LoadEmployees"
                AutoPostBack="true">
                <asp:ListItem Value="-1">--Select Branch--</asp:ListItem>
            </asp:DropDownList>--%>
                &nbsp;&nbsp;<strong> Employee</strong>
                <asp:DropDownList Width="200px" ID="ddlEmployeeList" runat="server" DataValueField="EmployeeId"
                    DataTextField="IDAndName" AutoPostBack="true" OnSelectedIndexChanged="LoadInsuranceListing">
                    <asp:ListItem Value="-1">--Select Employee--</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ValidationGroup="Inc" ID="valEmployeeReq" ControlToValidate="ddlEmployeeList"
                    InitialValue="-1" Display="None" runat="server" ErrorMessage="Please select an Employee."></asp:RequiredFieldValidator>
            </div>
            <uc2:MsgCtl ID="divMsgCtl" Width="900px" EnableViewState="false" Hide="true" runat="server" />
            <uc2:WarningCtl ID="divWarningMsg" Width="900px" EnableViewState="false" Hide="true"
                runat="server" />
            <div>
                <div class="separator clear">
                    <cc2:EmptyDisplayGridView CssClass="tableLightColor" UseAccessibleHeader="true" ID="gvwList"
                        runat="server" AutoGenerateColumns="False" DataKeyNames="Id,EmployeeId" GridLines="None"
                        ShowFooterWhenEmpty="False" ShowHeaderWhenEmpty="True" Width="100%" OnSelectedIndexChanged="gvw_SelectedIndexChanged"
                        OnRowCommand="gvwList_RowCommand">
                        <Columns>
                            <asp:BoundField HeaderStyle-HorizontalAlign="Left" DataField="EmployeeId" HeaderText="EIN"
                                HeaderStyle-Width="40px" />
                            <asp:BoundField HeaderStyle-Width="150px" HeaderStyle-HorizontalAlign="Left" DataField="Name"
                                HeaderText="Name" />
                            <%-- <asp:BoundField HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left" DataField="InsuranceType"
                                HeaderText="Insurance Type" />--%>
                            <asp:BoundField HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left" DataField="InsuranceCompany"
                                HeaderText="Insurance Company" />
                             <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                HeaderStyle-Width="80px" HeaderText="Health Insurance">
                                <ItemTemplate>
                                    <%# Convert.ToBoolean(Eval("IsHealthInsurance")) == true ? "Yes" : ""%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                HeaderStyle-Width="80px" HeaderText="Start Date">
                                <ItemTemplate>
                                    <%# (Eval("StartDateEng","{0:yyyy-MMM-dd}"))%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                HeaderStyle-Width="80px" HeaderText="End Date">
                                <ItemTemplate>
                                    <%# (Eval("EndDateEng", "{0:yyyy-MMM-dd}"))%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left"
                                HeaderStyle-Width="90px" HeaderText="Paid By">
                                <ItemTemplate>
                                    <%# Convert.ToDecimal(Eval("EmployerPaysPercent")) == 0 ? "Employee" : "Company"%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right"
                                HeaderStyle-Width="80px" HeaderText="Policy Amount">
                                <ItemTemplate>
                                    <%# GetCurrency(Eval("PolicyAmount"))%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right"
                                HeaderStyle-Width="70px" HeaderText="Premium">
                                <ItemTemplate>
                                    <%# GetCurrency(Eval("Premium"))%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                HeaderStyle-Width="100px" HeaderText="Note">
                                <ItemTemplate>
                                    <%# (Eval("Note"))%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%-- <asp:TemplateField HeaderStyle-Width="60px" HeaderText="Status" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>  
                                <%# Convert.ToBoolean(Eval("IsExpired")) ? "Expired" : "Un Expired"%>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                            <%-- <asp:TemplateField HeaderStyle-Width="60px" HeaderText="Change Status" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:LinkButton CssClass="anchorHover" CommandArgument='<%# Eval("ID") %>' OnClientClick="return confirm('Are you sure, you want to change the status?');"
                                    CommandName="Expired" Text='<%# Convert.ToBoolean(Eval("IsExpired")) ? "Un Expire" : "Expire" %>'
                                    ID="ImageButton2" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                            <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:ImageButton CommandName="Select" ID="ImageButton1" ImageUrl="~/images/edit.gif"
                                        runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <%--                <PagerSettings Mode="NumericFirstLast" NextPageText="Next »" PreviousPageText="« Prev"
                    FirstPageText="First" LastPageText="Last" />--%>
                        <SelectedRowStyle CssClass="selected" />
                        <RowStyle CssClass="odd" />
                        <AlternatingRowStyle CssClass="odd" />
                        <EmptyDataTemplate>
                            No records.
                        </EmptyDataTemplate>
                    </cc2:EmptyDisplayGridView>
                </div>
            </div>
            <%--   <div class="buttonsDiv" style="text-align: left">
            
        </div>--%>
            <table style='clear: both; display: none' runat="server" id="tblDetails">
                <tr>
                    <td>
                        <div class="content_title_nobg" style='width: 500px; margin-left: 2px;'>
                            Insurance details</div>
                        <fieldset class="mainFieldset" style='margin-top: 0px; padding-left: 15px; width: 500px'>
                            <table class="dataField" style="height: 300px">
                                <%-- <tr>
                                    <td class="fieldHeader">
                                        Insurance Type
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlIsPersonalInsurance" Width="200px" runat="server">
                                            <asp:ListItem Value="true">Personal Insurance</asp:ListItem>
                                            <asp:ListItem Value="false">Family Insurance</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>--%>
                                <tr>
                                    <td class="fieldHeader">
                                        Insurance company
                                    </td>
                                    <td>
                                        <asp:DropDownList EnableViewState="false" Width="250px" ID="ddlInstitution" runat="server"
                                            DataValueField="Id" DataTextField="InstitutionName" AppendDataBoundItems="true">
                                            <asp:ListItem Value="-1">--Select One--</asp:ListItem>
                                        </asp:DropDownList>
                                        <input onclick="InstitutionPopupCall()" id="btnInstitutionPopup" class="browse" type="button" />
                                    </td>
                                </tr>
                                <tr runat="server" id="rowHealthInsurance">
                                    <td class="fieldHeader">
                                        Health Insurance
                                    </td>
                                    <td>
                                        <asp:CheckBox ToolTip="If selected, will be treated as health insurance" runat="server" ID="chkIsHealthInsurance" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="fieldHeader">
                                        Policy No
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtPolicyNo" Width="135px" runat="server" MaxLength="25"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="fieldHeader">
                                        Financial Year *
                                    </td>
                                    <td>
                                        <asp:DropDownList Enabled="false" AutoPostBack="true" OnSelectedIndexChanged="ddlFinancialYears_Changed"
                                            ID="ddlFinancialYears" DataValueField="FinancialDateId" DataTextField="Name"
                                            runat="server">
                                            <asp:ListItem Value="-1">--Select Year--</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ValidationGroup="Inc" ID="RequiredFieldValidator1" Display="None"
                                            runat="server" InitialValue="-1" ControlToValidate="ddlFinancialYears" ErrorMessage="Year is required."></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="fieldHeader">
                                        Policy Start
                                    </td>
                                    <td>
                                        <ext:DateField ID="dateFrom" runat="server">
                                            <%-- <Plugins>
                                                <ext:GenericPlugin ID="GenericPlugin1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                                            </Plugins>--%>
                                        </ext:DateField>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="fieldHeader">
                                        Policy End
                                    </td>
                                    <td>
                                        <ext:DateField ID="dateTo" runat="server">
                                            <%-- <Plugins>
                                                <ext:GenericPlugin ID="GenericPlugin1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                                            </Plugins>--%>
                                        </ext:DateField>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="fieldHeader">
                                        Paid By *
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlPaidBy" Width="137px" runat="server">
                                            <asp:ListItem Value="0">Employee</asp:ListItem>
                                            <asp:ListItem Value="1">Company</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr style="display: none">
                                    <td class="fieldHeader">
                                        Employee pays (%)*
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtEmployeePays" runat="server" MaxLength="3" onkeypress="return AllowOnlyNumbers(event);"></asp:TextBox>
                                        <%--<asp:RequiredFieldValidator ValidationGroup="Inc" ID="valEmployeePays" Display="None"
                                        runat="server" ControlToValidate="txtEmployeePays" ErrorMessage="Employee Pays is required."></asp:RequiredFieldValidator>
                                    <asp:RangeValidator ValidationGroup="Inc" ID="valMaxHundred2" Type="Double" MinimumValue="0"
                                        MaximumValue="100" Display="None" runat="server" ErrorMessage="Employee Pays must be between 0 to 100."
                                        ControlToValidate="txtEmployeePays"></asp:RangeValidator>
                                    <asp:CustomValidator ValidationGroup="Inc" ID="CustomValidator1" runat="server" ErrorMessage="The sum of  employee and employer pays percent must be 100."
                                        Display="None" ClientValidationFunction="IsHundredPercent" ControlToValidate="txtEmployerPays"
                                        ValidateEmptyText="true"></asp:CustomValidator>--%>
                                    </td>
                                </tr>
                                <tr style="visibility: hidden">
                                    <td class="fieldHeader" style="vertical-align: top">
                                        Employer pays (%)*
                                    </td>
                                    <td>
                                        <asp:TextBox onkeyup="ChangeStatus(this.value)" ID="txtEmployerPays" runat="server"
                                            MaxLength="3" onkeypress="return AllowOnlyNumbers(event);"></asp:TextBox>
                                        <%-- <asp:RequiredFieldValidator ValidationGroup="Inc" ID="valEmployerPays" Display="None"
                                        runat="server" ControlToValidate="txtEmployerPays" ErrorMessage="Employer Pays is required."></asp:RequiredFieldValidator>
                                    <asp:RangeValidator ValidationGroup="Inc" ID="valMaxHundred1" Type="Double" MinimumValue="0"
                                        MaximumValue="100" Display="None" runat="server" ErrorMessage="Employer Pays must be between 0 to 100."
                                        ControlToValidate="txtEmployerPays"></asp:RangeValidator>--%>
                                        <div runat="server" id="divDeduct" style='margin-top: 4px'>
                                            <asp:CheckBox ID="chkDeductFromSalary" Text="Deduct insurance premium from salary"
                                                runat="server" /><br />
                                            <asp:RadioButtonList RepeatLayout="Flow" ID="rdbDeductFromSalary" runat="server"
                                                RepeatColumns="1" RepeatDirection="Horizontal" DataTextField="Text" DataValueField="Value">
                                                <asp:ListItem Selected="True" Text="Deduct in the month of payment"></asp:ListItem>
                                                <asp:ListItem Text="Deduct in monthly installments"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                    <td>
                        <div class="content_title_nobg" style='width: 500px; margin-left: 2px;'>
                            Premium details</div>
                        <fieldset class="mainFieldset" style='margin-top: 0px; padding-left: 15px; width: 511px'>
                            <table class="dataField" style='width: 500px; height: 280px'>
                                <tr>
                                    <td class="fieldHeader">
                                        Policy amount
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtPolicyAmount" runat="server"></asp:TextBox>
                                        <asp:CompareValidator ValidationGroup="Inc" ID="valPolicyAmtDataType" Display="None"
                                            ControlToValidate="txtPolicyAmount" Type="Currency" Operator="DataTypeCheck"
                                            runat="server" ErrorMessage="Policy Amount must be number."></asp:CompareValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="fieldHeader">
                                        Periodic premium*
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtPremium" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ValidationGroup="Inc" ID="valPremium" ControlToValidate="txtPremium"
                                            Display="None" runat="server" ErrorMessage="Premium Amount is required."></asp:RequiredFieldValidator>
                                        <asp:CompareValidator ValidationGroup="Inc" ID="CompareValidator1" Display="None"
                                            ControlToValidate="txtPremium" Type="Currency" Operator="DataTypeCheck" runat="server"
                                            ErrorMessage="Premium Amount must be number."></asp:CompareValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="fieldHeader">
                                        Premium payment frequency
                                        <asp:RadioButtonList Enabled="false" Width="305px" Style='border: 1px solid #7F9DB9;
                                            margin-top: 3px' ID="rdbPaymentPeriod" runat="server" RepeatDirection="Horizontal"
                                            DataTextField="Text" DataValueField="Value">
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="fieldHeader">
                                        Note
                                        <asp:TextBox ID="txtNote" runat="server" TextMode="MultiLine" Rows="4" Style='width: 100%'></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
            </table>
            <fieldset visible="false" class="mainFieldset1" runat="server" id="fsApplySettings"
                style='background-color: #E9F0FB; margin-top: 0px; padding-left: 15px; border: 1px solid #10B0EA'>
                <legend>Apply Settings</legend><a id="A3" href="javascript:void(0)" onclick="javascript: CheckBoxListSelect ('<%= chkSettings.ClientID %>',true)">
                    Select All</a> | <a href="javascript:void(0)" id="A4" onclick="javascript: CheckBoxListSelect ('<%= chkSettings.ClientID %>',false)">
                        Remove Selection</a><br />
                <asp:CheckBoxList ID="chkSettings" runat="server" Style='margin-top: 2px; border: 1px solid #7F9DB9;
                    padding: 3px' RepeatDirection="Horizontal">
                    <%-- <asp:ListItem Value="1">Insurance Name</asp:ListItem>--%>
                    <asp:ListItem Value="2">Institution Name</asp:ListItem>
                    <asp:ListItem Value="3">Employer Pays</asp:ListItem>
                    <asp:ListItem Value="4">Employee Pays</asp:ListItem>
                    <asp:ListItem Value="5">Policy Amount</asp:ListItem>
                    <asp:ListItem Value="6">Premium</asp:ListItem>
                    <asp:ListItem Value="7">Premium Payment Freq</asp:ListItem>
                    <asp:ListItem Value="8">Deduct from Salary</asp:ListItem>
                </asp:CheckBoxList>
                <br />
                <asp:DropDownList ID="ddlFilterBy" runat="server" AutoPostBack="True" CausesValidation="false"
                    OnSelectedIndexChanged="ddlFilterBy_SelectedIndexChanged">
                    <asp:ListItem Text="--Select filter by--" Value="-1"></asp:ListItem>
                    <asp:ListItem Text="Branch" Value="1"></asp:ListItem>
                    <asp:ListItem Text="Department" Value="2"></asp:ListItem>
                    <asp:ListItem Text="Cost Code" Value="3"></asp:ListItem>
                </asp:DropDownList>
                <asp:DropDownList ID="ddlFilterByValues" runat="server" AutoPostBack="True" CausesValidation="false"
                    OnSelectedIndexChanged="ddlFilterByValues_SelectedIndexChanged">
                    <asp:ListItem Text="--Select value--" Value="-1"></asp:ListItem>
                </asp:DropDownList>
                <div style='margin-top: 6px; margin-bottom: 3px'>
                    <a id="A1" href="javascript:void(0)" onclick="javascript: CheckBoxListSelect ('<%= chkList.ClientID %>',true)">
                        Select All</a> | <a id="A2" href="javascript:void(0)" onclick="javascript: CheckBoxListSelect ('<%= chkList.ClientID %>',false)">
                            Remove Selection</a>
                </div>
                <asp:CheckBoxList Style='border: 1px solid #7F9DB9' ID="chkList" runat="server" RepeatColumns="6"
                    RepeatDirection="Horizontal" DataTextField="Name" DataValueField="EmployeeId"
                    BorderWidth="1px">
                </asp:CheckBoxList>
            </fieldset>
            <div class="buttonsDiv" id="buttonsDiv" runat="server" style="display: none">
                <asp:Button ID="btnDelete" CssClass="cancel" CausesValidation="false" runat="server"
                    Text="Delete" OnClick="btnDelete_Click" Style='display: none' />
                <asp:Button ID="btnSave" CssClass="save" runat="server" Text="Save" OnClientClick="valGroup='Inc';return CheckValidation()"
                    OnClick="btnSave_Click" />
                <asp:Button ID="btnCancel" CssClass="cancel" CausesValidation="false" runat="server"
                    Text="Cancel" OnClick="btnCancel_Click" />
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="pageScriptsContent" runat="server">
    <script type="text/javascript">


        function InstitutionPopupCall() {
            var ret = InstitutionPopup("isPopup=true");
            if (typeof (ret) != 'undefined') {
                ChangeDropDownList('<%= ddlInstitution.ClientID %>', ret);
            }
        }

        function parentReloadCallbackFunction(childWindow, ret) {
            childWindow.close();
            if (typeof (ret) != 'undefined') {
                ChangeDropDownList('<%= ddlInstitution.ClientID %>', ret);
            }
        }

        function IsHundredPercent(source, args) {
            var p1 = document.getElementById('<%= txtEmployerPays.ClientID %>');
            var p2 = document.getElementById('<%= txtEmployeePays.ClientID %>');

            if (parseFloat(p1.value) + parseFloat(p2.value) == 100) {
                args.IsValid = true;
            }
            else {
                args.IsValid = false;
                p1.focus();
            }
        }

        function AllowOnlyNumbers(evt) {
            var e = event || evt;
            var charCode = e.which || e.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

        function CheckBoxListSelect(chkBoxControl, state) {
            if (chkBoxControl == null)
                return;
            var chkBoxList = document.getElementById(chkBoxControl);
            if (chkBoxList == null)
                return;
            var chkBoxCount = chkBoxList.getElementsByTagName("input");
            for (var i = 0; i < chkBoxCount.length; i++) {
                chkBoxCount[i].checked = state;
            }

            return false;
        }

        function clearUnload() {
            window.onunload = null;
        }

        function handleDelete() {
            if (confirm('Do you want to delete insurance?')) {
                clearUnload();
                return true;
            }
            else
                return false;
        }
        var divDeduct = '<%= divDeduct.ClientID  %>';
        function ChangeStatus(txtValue) {
            return;

            if (txtValue == "" || txtValue == "0") {
                disableElement(divDeduct);
            }
            else {
                enableElement(divDeduct);
            }
        }


        window.onkeydown = function (evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;

            var valid = false;

            if (evt.ctrlKey && charCode == 46) {
                if (confirm('Confirm delete insurance?'))

                    __doPostBack('<%= btnDelete.ClientID.ToString().Replace("_","$") %>', '');
            }
        }
    </script>
</asp:Content>
