using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Base;
using BLL.Manager;
using Utils.Helper;
using Utils.Web;
using DAL;
using Utils;

namespace Web.CP
{
    public partial class AEBranch : BasePage, IDisableAfterCalculation
    {
        CompanyManager compMgr = new CompanyManager();
        BranchManager branchMgr = new BranchManager();
        CommonManager commonMgr = new CommonManager();
        Branch bra = new Branch();
        List<Branch> source = new List<Branch>();

        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.AdministratorTools;
            }
        }

        public int BranchId
        {
            get
            {
                if (ViewState["BranchId"] == null)
                    return 0;
                return int.Parse(ViewState["BranchId"].ToString());
            }
            set
            {
                ViewState["BranchId"] = value;
            }
        }

        public PageModeList PageModeList
        {
            get
            {
                if (ViewState["PageModeList"] == null)
                    return PageModeList.NotSet;
                return (PageModeList)ViewState["PageModeList"];
            }
            set
            {
                ViewState["PageModeList"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            LoadDistrict();
            LoadVDC();

            
            //RestoreValues();
            if (!IsPostBack)
            {
                LoadEmployees();
                Initialise();
            }
            //JavascriptHelper.AttachEnableDisablingJSCode(chkHasSeperateRetirementAC, txtSeperateRetirementAC.ClientID, false);
            //JavascriptHelper.AttachEnableDisablingJSCode(chkHasSeperateEmpInsuraceAC, txtSeperateEmpInsuranceAC.ClientID, false);
            JavascriptHelper.AttachEnableDisablingJSCode(chkIsInRemoteArea, divRemoteArea.ClientID, false);
         
        }

       

        void EnableDisableAllValidators(bool enable)
        {
            //first disable all validation control
            foreach (BaseValidator ctl in Page.GetValidators("AEDeduction"))
            {
                ctl.Enabled = enable;
            }
        }

        void DisableValidationForHiddenPnlControls()
        {
                   }

        void Initialise()
        {
            string fundName;
            if (SessionManager.CurrentCompany.HasPFRFFund)
                fundName = SessionManager.CurrentCompany.PFRFName;
            else
                fundName = Resources.Messages.DefaultFundFullName;

            //chkHasSeperateRetirementAC.Text = string.Format(
            //       chkHasSeperateRetirementAC.Text, fundName);

            //valCustomRFAC.ErrorMessage = string.Format(
            //    valCustomRFAC.ErrorMessage, fundName);


            //valRegPhone.ValidationExpression = Config.PhoneRegExp;
            //valRegFax.ValidationExpression = Config.FaxRegExp;

           
            PrepareForAdd();

            //for update
            this.CustomId = UrlHelper.GetIdFromQueryString("IId");

            int branchId = 0;

            if (!string.IsNullOrEmpty(Request.QueryString["IId"]))
            {
                hdnBranchId.Value = Request.QueryString["IId"];
                branchId = int.Parse(hdnBranchId.Value);
            }

            cmbRegionBranchList.DataSource = BranchManager.GetRegionalBranchList(branchId);
            cmbRegionBranchList.DataBind();
            chkIsActive.Checked = true;

            if (branchId != 0)
            {
                this.PageModeList = PageModeList.Update;

                Branch branch = new BranchManager().GetById(branchId);
                ProcessBranch(branch);
                btnSave.Text = Resources.Messages.Update;
            }
            else
            {
                this.PageModeList = PageModeList.Insert;
            }

           
        }

        void LoadDistrict()
        {
            string id = Request.Form[ddlDistricts.UniqueID];
            CommonManager mgr = new CommonManager();
            ddlDistricts.DataSource = mgr.GetRemoteDistricts();
            ddlDistricts.DataBind();
            if (id != null)
                UIHelper.SetSelectedInDropDown(ddlDistricts, id);
        }

        void LoadEmployees()
        {
            cmbBranchHead.DataSource = EmployeeManager.GetAllActiveEmployees();
            cmbBranchHead.DataBind();

            cmbRegionHead.DataSource = cmbBranchHead.DataSource;
            cmbRegionHead.DataBind();
        }

        Branch ProcessBranch(Branch b)
        {
            if (b != null)
            {
                txtBranchName.Text = b.Name;
                txtCode.Text = b.Code == null ? "" : b.Code;
                txtResponsiblePerson.Text = b.ResponsiblePerson;
                txtAddress.Text = b.Address;
                txtPhone.Text = b.Phone;
                txtFax.Text = b.Fax;
                txtAreaCode.Text = b.AreaCode;
                txtEmail.Text = b.Email;
                chkIsInRemoteArea.Checked = b.IsInRemoteArea.Value;
                txtBankName.Text = b.BankName;
                txtBankAccountNo.Text = b.BankAccountNo;

                if (b.IsRegionalOffice != null)
                    chkIsRegionalOffice.Checked = b.IsRegionalOffice.Value;
                if (chkIsRegionalOffice.Checked)
                {
                    if (b.RegionalHeadId != null)
                        UIHelper.SetSelectedInDropDown(cmbRegionHead, b.RegionalHeadId.ToString());
                }
                else
                {
                    if (b.RegionalBranchId != null)
                        UIHelper.SetSelectedInDropDown(cmbRegionBranchList, b.RegionalBranchId.ToString());
                }

                if (b.IsManualAttendance != null)
                    chkIsManualAttendance.Checked = b.IsManualAttendance.Value;
                else
                    chkIsManualAttendance.Checked = false;

                if (b.IsInRemoteArea.Value)
                {
                    UIHelper.SetSelectedInDropDown(ddlDistricts, b.VDCList.DistrictId.ToString());
                    LoadVDC();
                    //UIHelper.SetSelectedInDropDown(ddlDistricts, b.DistrictId.ToString());
                    UIHelper.SetSelectedInDropDown(ddlVDCs, b.VDCId.ToString());
                    lblRemoteAreaValue.Text = b.VDCList.RemoteArea.Value;
                }

                //chkHasSeperateRetirementAC.Checked = b.HasSeperateRetirementFund.Value;
                //chkHasSeperateEmpInsuraceAC.Checked = b.HasSeperateEmpInsurance.Value;

                //if (b.HasSeperateRetirementFund.Value)
                //{
                //    txtSeperateRetirementAC.Text = b.RetirementFundACNo;
                //}
                //if (b.HasSeperateEmpInsurance.Value)
                //{
                //    txtSeperateEmpInsuranceAC.Text = b.EmpInsuranceACNo;
                //}
                if (b.BranchHeadId != null)
                {
                    //cmbBranchHead.SelectedItem.Value = b.BranchHeadId.ToString();
                    UIHelper.SetSelectedInDropDown(cmbBranchHead, b.BranchHeadId.ToString());
                }

                if (b.BID != null)
                    txtBranchID.Text = b.BID;

                if (b.IsActive == null || b.IsActive == true)
                    chkIsActive.Checked = true;
                else
                {
                    chkIsActive.Checked = false;

                    if (!string.IsNullOrEmpty(b.InactiveDate))
                        calInActiveDate.Text = b.InactiveDate;

                    calInActiveDate.Show();
                }
                
            }
            else
            {
                b = new Branch();
                b.Name = txtBranchName.Text.Trim();
                b.Code = txtCode.Text.Trim();
                b.ResponsiblePerson = txtResponsiblePerson.Text.Trim();
                b.Address = txtAddress.Text.Trim();
                b.AreaCode = txtAreaCode.Text.Trim();
                b.Phone = txtPhone.Text.Trim();
                b.Fax = txtFax.Text.Trim();
                b.Email = txtEmail.Text.Trim();
                b.BankName = txtBankName.Text.Trim();
                b.BankAccountNo = txtBankAccountNo.Text.Trim();
                b.IsInRemoteArea = chkIsInRemoteArea.Checked;
                b.IsManualAttendance = chkIsManualAttendance.Checked;
                b.IsRegionalOffice = chkIsRegionalOffice.Checked;

                if (b.IsRegionalOffice.Value)
                {
                    if (cmbRegionHead.SelectedValue != "-1")
                        b.RegionalHeadId = int.Parse(cmbRegionHead.SelectedValue);
                }
                else
                {
                    if (cmbRegionBranchList.SelectedValue != "-1")
                        b.RegionalBranchId = int.Parse(cmbRegionBranchList.SelectedValue);
                }

                if (b.IsInRemoteArea.Value)
                {
                    //b.DistrictId = int.Parse(ddlDistricts.SelectedValue);
                    b.VDCId = int.Parse(ddlVDCs.SelectedValue);
                }
                b.CompanyId = SessionManager.CurrentCompanyId;
                //b.HasSeperateRetirementFund = chkHasSeperateRetirementAC.Checked;
                //b.HasSeperateEmpInsurance = chkHasSeperateEmpInsuraceAC.Checked;

                //if (b.HasSeperateRetirementFund.Value)
                //{
                //    b.RetirementFundACNo = txtSeperateRetirementAC.Text.Trim();
                //}
                //if (b.HasSeperateEmpInsurance.Value)
                //{
                //    b.EmpInsuranceACNo = txtSeperateEmpInsuranceAC.Text.Trim();
                //}

                if (cmbBranchHead.SelectedValue != "-1")
                {
                    //b.BranchHeadId = int.Parse(cmbBranchHead.SelectedItem.Value);
                    b.BranchHeadId = int.Parse(cmbBranchHead.SelectedValue);
                }

                if (!string.IsNullOrEmpty(txtBranchID.Text.Trim()))
                    b.BID = txtBranchID.Text.Trim();

                if (chkIsActive.Checked)
                    b.IsActive = true;
                else
                {
                    b.IsActive = false;                    
                }

                return b;
            }
            return null;
        }

        void LoadVDC()
        {
            string id = Request.Form[ddlVDCs.UniqueID];
            CommonManager mgr = new CommonManager();
            ddlVDCs.DataSource = mgr.GetAllVDCs(int.Parse(ddlDistricts.SelectedValue));
            ddlVDCs.DataBind();
            if (id != null)
                UIHelper.SetSelectedInDropDown(ddlVDCs, id);
        }
    

        /// <summary>
        /// Prepare for adding by hiding value fields
        /// </summary>
        void PrepareForAdd()
        {
            
        }

  


        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (chkIsInRemoteArea.Checked == false)
            {
                valCustDistrict.Enabled = false;
                valCustVDC.Enabled = false;
            }
            Page.Validate("AEBranch");
            if (Page.IsValid)
            {
                Branch b = ProcessBranch(null);

                if (!string.IsNullOrEmpty(hdnBranchId.Value))
                {
                    if (cmbRegionBranchList.SelectedValue != "-1"
                        && cmbRegionBranchList.SelectedValue == hdnBranchId.Value.ToString())
                    {
                        WarningMsgCtl1.InnerHtml = "Same branch can not be of regional branch.";
                        WarningMsgCtl1.Hide = false;
                        return;
                    }

                    int selBranchId = int.Parse(hdnBranchId.Value);
                    b.BranchId = selBranchId;
                    b.CompanyId = SessionManager.CurrentCompanyId;
                    
                    if (b.IsActive != null && b.IsActive.Value == false && string.IsNullOrEmpty(calInActiveDate.Text.Trim()))
                    {
                        WarningMsgCtl1.InnerHtml = "Inactive date is required.";
                        WarningMsgCtl1.Hide = false;
                        calInActiveDate.Show();
                        calInActiveDate.Focus();
                        return;
                    }

                    if (b.IsActive == false)
                    {
                        b.InactiveDate = calInActiveDate.Text.Trim();
                        b.InactiveDateEng = GetEngDate(b.InactiveDate);
                        string msg = BranchManager.IsActiveEmployeeAssociatedWithBranch(b.BranchId, b.InactiveDateEng);
                        if (!string.IsNullOrEmpty(msg))
                        {
                            WarningMsgCtl1.InnerHtml = msg;
                            WarningMsgCtl1.Hide = false;
                            return;
                        }
                    }
                    
                    if (branchMgr.Update(b))
                    {
                        JavascriptHelper.DisplayClientMsg(Resources.Messages.BranchUpdatedMsg, Page, "closePopup();");

                    }
                    else
                    {
                        WarningMsgCtl1.InnerHtml = "Branch name already exists.";
                        WarningMsgCtl1.Hide = false;
                        return;
                    }
                    //JavascriptHelper.DisplayClientMsg("Branch information updated.", Page);
                }
                else
                {
                    b.CompanyId = SessionManager.CurrentCompanyId;
                    if (branchMgr.Save(b))
                    {
                        JavascriptHelper.DisplayClientMsg(Resources.Messages.BranchSavedMsg, Page, "closePopup();");
                    }
                    else
                    {
                        WarningMsgCtl1.InnerHtml = "Branch name already exists.";
                        WarningMsgCtl1.Hide = false;
                        return;
                    }
                    //JavascriptHelper.DisplayClientMsg("Branch information saved.", Page);
                }
                CommonManager.ResetCache();
               
            }
            else
            {
                JavascriptHelper.DisplayClientMsg("Validation fail.", Page);
            }
            valCustDistrict.Enabled = true;
            valCustVDC.Enabled = true;
        }



        protected void Page_PreRender(object sender, EventArgs e)
        {
            


            //set enabling/disabling
            if (chkIsInRemoteArea.Checked)
            {
                ddlDistricts.Attributes.Remove("disabled");
                ddlVDCs.Attributes.Remove("disabled");
                lblRemoteAreaValue.Text = commonMgr.GetRemoteAreaValue(int.Parse(ddlVDCs.SelectedValue));

            }
            else
            {
                ddlDistricts.Attributes.Add("disabled", "disabled");
                ddlVDCs.Attributes.Add("disabled", "disabled");
            }

            if (lblRemoteAreaValue.Text == string.Empty)
                lblRemoteAreaValue.Style.Add("display", "none");
            else
                lblRemoteAreaValue.Style.Remove("display");

            if (chkIsRegionalOffice.Checked)
            {
                trRegionHead.Style["display"] = "";
                trRegionalBranch.Style["display"] = "none";
            }
            else
            {
                trRegionHead.Style["display"] = "none";
                trRegionalBranch.Style["display"] = "";
            }

            if (chkIsActive.Checked == false)
                calInActiveDate.Show();

        }

        protected void chkHasVariableAmount_CheckedChanged(object sender, EventArgs e)
        {
            //set enabling/disabling
            if (chkIsInRemoteArea.Checked)
            {
                ddlDistricts.Attributes.Remove("disabled");
                ddlVDCs.Attributes.Remove("disabled");
                lblRemoteAreaValue.Text = commonMgr.GetRemoteAreaValue(int.Parse(ddlVDCs.SelectedValue));

            }
            else
            {
                ddlDistricts.Attributes.Add("disabled", "disabled");
                ddlVDCs.Attributes.Add("disabled", "disabled");
            }

            if (lblRemoteAreaValue.Text == string.Empty)
                lblRemoteAreaValue.Style.Add("display", "none");
            else
                lblRemoteAreaValue.Style.Remove("display");

            //if (chkHasSeperateRetirementAC.Checked)
            //    txtSeperateRetirementAC.Attributes.Remove("disabled");
            //else
            //    txtSeperateRetirementAC.Attributes.Add("disabled", "disabled");

            //if (chkHasSeperateEmpInsuraceAC.Checked)
            //    txtSeperateEmpInsuranceAC.Attributes.Remove("disabled");
            //else
            //    txtSeperateEmpInsuranceAC.Attributes.Add("disabled", "disabled");
        }


        #region IDisableAfterCalculation Members

        public void DisableControlsAfterSalary()
        {
            if (this.PageModeList == BLL.PageModeList.Update)
            {
                //if (PayManager.HasDeductionIncludedInCalculation(this.DeductionId))
                //{
                //    this.ddlCalculation.Enabled = false;

                //    this.chkIsRefundableAdvance.Enabled = false;
                //    this.rdbIsEMI.Enabled = false;
                //    this.rdbIsSimple.Enabled = false;
                //    //ddlDeposits.Enabled = false;
                //    //chkIsExemptFromIncomeTax.Enabled = false;
                //    chkListIncomes.Enabled = false;
                //    chkHasVariableAmount.Enabled = false;
                //}
            }
            else if (!string.IsNullOrEmpty(hdnBranchId.Value))
            {
                //empDeduction = payMgr.GetEmployeeDeduction(this.CustomId);

                //if (empDeduction != null && PayManager.HasDeductionIncludedInCalculation(empDeduction.DeductionId))
                //{
                //    this.ddlCalculation.Enabled = false;

                //    this.chkIsRefundableAdvance.Enabled = false;
                //    this.rdbIsEMI.Enabled = false;
                //    this.rdbIsSimple.Enabled = false;
                //    //ddlDeposits.Enabled = false;
                //    //chkIsExemptFromIncomeTax.Enabled = false;
                //    chkListIncomes.Enabled = false;
                //    chkHasVariableAmount.Enabled = false;
                //}
            }
           

            //if add mode then only show emi or simple option in other mode disable it
            if (this.PageModeList != BLL.PageModeList.Insert)
            {
                //rdbIsEMI.Enabled = false;
                //rdbIsSimple.Enabled = false;
            }
        }

        #endregion
    }
}
