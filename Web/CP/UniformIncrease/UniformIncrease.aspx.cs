﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Web.CP.UniformIncrease
{
    public partial class UniformIncrease : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string type = Request.QueryString["Itype"];
            if( !string.IsNullOrEmpty(type) && type=="PercentOfIncome")
            {
                rdoIncomeRs.Checked = false;
                rdoIncomeRs.Enabled = false;
                rdoIncomePercent.Checked = true;

                txtIncomeRs.CssClass = "disabledClass";
                txtPercentIncome.CssClass = "";

                txtIncomeRs.ReadOnly = true;
                txtPercentIncome.ReadOnly = false;

                rdoIncomeRs.Enabled = false;
            }
        }
    }
}