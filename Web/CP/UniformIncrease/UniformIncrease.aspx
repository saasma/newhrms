﻿<%@ Page Language="C#" Title="Uniform Increase" AutoEventWireup="true" CodeBehind="UniformIncrease.aspx.cs"
    MasterPageFile="~/Master/ForPopupPage.Master" Inherits="Web.CP.UniformIncrease.UniformIncrease" %>

<asp:Content ID="contentHead1" ContentPlaceHolderID="head" runat="server">
    <script src="../../Scripts/common.js?v=300" type="text/javascript"></script>
    <script src="../../Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        function closePopup() {

            var retval = "";
            var rdoIncomeRs = document.getElementById('<%=rdoIncomeRs.ClientID%>');
            var rdoIncomePercent = document.getElementById('<%=rdoIncomePercent.ClientID%>');
            var value = "";
            var type = "";

            var txtIncomeRs = document.getElementById('<%=txtIncomeRs.ClientID%>');
            var txtPercentIncome = document.getElementById('<%=txtPercentIncome.ClientID%>');

            var incomeValue = txtIncomeRs.value.replaceAll(",", "");
            var percentValue = txtPercentIncome.value.replaceAll(",", "");

            if (rdoIncomeRs.checked) {


                if (incomeValue == "" ) {

                    alert("Please enter a valid amount.");
                    return false;
                }
                type = "BYAMOUNT:";
                retval = "BYAMOUNT:" + incomeValue;
                value = incomeValue;
            }
            else if (rdoIncomePercent.checked) {


                if (percentValue == "") {

                    alert("Please enter a valid percentage.");
                    return false;
                }
                type = "BYPERCENT:";
                retval = "BYPERCENT:" + percentValue;
                value = percentValue;
            }
            else {

                alert("Please select any one option to apply uniform increase in salary for the employees.");
                return false;
            }

            value = parseFloat(value);
            if (isNaN(value))
                retval = type + "0";

            //if ($.browser.msie == false && typeof (window.opener.setUniformIncrement) != 'undefined' && ($.browser.chrome || $.browser.safari)) {
                window.opener.setUniformIncrement(retval);
               
//            } else {
//                window.returnValue = retval;

//               
//            }

            window.close();
            return false;

        }



        function EnableControl(source) {

            var txtIncomeRs = document.getElementById('<%=txtIncomeRs.ClientID%>');
            var txtPercentIncome = document.getElementById('<%=txtPercentIncome.ClientID%>');
            txtIncomeRs.readOnly = true;
            txtPercentIncome.readOnly = true;
            if (source.id.indexOf("rdoIncomeRs") != -1) {

                txtPercentIncome.value = "";
                txtIncomeRs.readOnly = false;

                txtIncomeRs.className = '';
                txtPercentIncome.className = 'disabledClass';
            }
            else if (source.id.indexOf("rdoIncomePercent") != -1) {

                txtIncomeRs.value = "";
                txtPercentIncome.readOnly = false;

                txtIncomeRs.className = 'disabledClass';
                txtPercentIncome.className = ''
            }
        }


        function process() {
            valGroup = 'save';
            if (CheckValidation()) {
                closePopup(); 
                return false;
            }
            return true;
        }
    </script>
    <style type="text/css">
    
    .disabledClass
    {
    	background-color:lightgrey;
    }
        
    </style>
</asp:Content>
<asp:Content ContentPlaceHolderID="mainContent" ID="mainContent1" runat="server">
    <div class="popupHeader">
         <h3>
                Uniform Increment</h3>
    </div>
    <div style="padding-top: 20px;padding-left:12px">
        <table>
            <tr>
                <td>
                    <asp:RadioButton Checked="true" ID="rdoIncomeRs" runat="server" GroupName="UniformIncrease" onclick="EnableControl(this)"
                        Text="Increase All Selected Employee Income By Rs." />
                </td>
                <td>
                    <asp:TextBox ID="txtIncomeRs" runat="server"  />
                    <asp:CompareValidator runat="server" ID="comp1" ControlToValidate="txtIncomeRs" Type="Currency" Operator="DataTypeCheck" ValidationGroup="save" Display="None"
                    ErrorMessage="Invalid amount." />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:RadioButton GroupName="UniformIncrease" ID="rdoIncomePercent" onclick="EnableControl(this)"
                        runat="server" Text="Increase All Selected Employee Income By %" />
                </td>
                <td>
                    <asp:TextBox ID="txtPercentIncome" CssClass="disabledClass" runat="server" ReadOnly="true" />
                    <asp:CompareValidator runat="server" ID="CompareValidator1" ControlToValidate="txtPercentIncome" Type="Double" Operator="DataTypeCheck" ValidationGroup="save" Display="None"
                    ErrorMessage="Invalid percent." />
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <input type="button" ID="btnOk" value="Ok"  class="save" onclick="return process();"  />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
<%--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript">
        function closePopup() {

            var retval = "";
            var rdoIncomeRs = document.getElementById('<%=rdoIncomeRs.ClientID%>');
            var rdoIncomePercent = document.getElementById('<%=rdoIncomePercent.ClientID%>');
            if (rdoIncomeRs.checked) {

                var txtIncomeRs = document.getElementById('<%=txtIncomeRs.ClientID%>');
                if (txtIncomeRs.value == "" || parseFloat(txtIncomeRs.value) <= 0) {

                    alert("Please enter a valid amount." + '\n' + "Amount should always be greater than zero.");
                    return false;
                }
                retval = "BYRS:" + txtIncomeRs.value;
            }
            else if (rdoIncomePercent.checked) {

                var txtPercentIncome = document.getElementById('<%=txtPercentIncome.ClientID%>');
                if (txtIncomePercent.value == "" || parseFloat(txtIncomePercent.value) <= 0) {

                    alert("Please enter a valid percentage." + '\n' + "Percentage should always be greater than zero.");
                    return false;
                }
                retval = "BYPERCENT" + txtPercentIncome.value;
            }
            else {

                alert("Please select any one option to apply uniform increase in salary for the nominated employees");
                return false;
            }
            window.returnValue = retval;
            window.close();
        }

        function EnableControl(source) {
            alert('check point');
            var txtIncomeRs = document.getElementById('<%=txtIncomeRs.ClientID%>');
            var txtPercentIncome = document.getElementById('<%=txtPercentIncome.ClientID%>');
            txtIncomeRs.enabled = false;
            txtPercentIncome.enabled = false;
            if (source.id.indexOf("rdoIncomeRs") != -1) {

                txtIncomeRs.enabled = true;
            }
            else if (source.id.indexOf("rdoIncomePercent") != -1) {

                txtPercentIncome.enabled = true;
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table>
            <tr>
                <td>
                    <asp:RadioButton ID="rdoIncomeRs" runat="server" GroupName="UniformIncrease" onclick="EnableControl(this)"
                        Text="Increase All Selected Employee Income By Rs." />
                </td>
                <td>
                    <asp:TextBox ID="txtIncomeRs" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:RadioButton GroupName="UniformIncrease" ID="rdoIncomePercent" onclick="EnableControl(this)"
                        runat="server" Text="Increase All Selected Employee Income By %" />
                </td>
                <td>
                    <asp:TextBox ID="txtPercentIncome" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnOk" Text="Ok" runat="server" OnClientClick="return closePopup()" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>--%>
