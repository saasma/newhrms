﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using Utils.Helper;
using Utils.Web;
using BLL.Base;
using DAL;
using Utils.Base;

namespace Web
{


    public partial class TaxableIncomeAndIRDComparisionReport : BasePage
    {

        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.Payroll;
            }
        }

        int payrollPeriodId;
        /// <summary>
        /// For Excel export to work
        /// </summary>
        /// <param name="control"></param>
        public override void VerifyRenderingInServerForm(Control control)
        {

        }

        void SetPeriod()
        {
            int month = 0;
            int year = 0;

            if (!string.IsNullOrEmpty(ddlPayrollFromMonth.SelectedValue))
            {


                month = int.Parse(ddlPayrollFromMonth.SelectedValue);
                year = int.Parse(ddlPayrollFromYear.SelectedValue);

                PayrollPeriod period = CommonManager.GetPayrollPeriod(month, year);
                if (period != null)
                    payrollPeriodId = period.PayrollPeriodId;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!IsPostBack)
            //    gvEmployeeIncome.Columns[14].HeaderText = CommonManager.GetHandicappedName;

           if (!IsPostBack)
            {
                //PayrollPeriod payroll = CommonManager.GetLastPayrollPeriod();
                //if (payroll != null)
                //{
                //    title.InnerHtml += payroll.Name;
                //}


                List<Branch> list = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);

                ddlBranch.DataSource
                    = list;
                ddlBranch.DataBind();

                CommonManager.SetFirstAndLastFinalSavedPayrollPeriod(ddlPayrollFromYear, ddlPayrollFromMonth);

            }
            SetPeriod();
            //AddColumns();
            
            if (IsPostBack)
            {

                BindEmployees();

            }
        }

       


        protected void gvw_DataBound(object sender, EventArgs e)
        {
           

        }

        #region "Group Header"
        public void AddCell(GridViewRow row, bool skipRight)
        {
            TableCell left = new TableHeaderCell();
            //left.ColumnSpan = count;
            //left.HorizontalAlign = HorizontalAlign.Center;
           // left.BackColor = ColorTranslator.FromHtml(color);
           // left.Style.Add("border-bottom", String.Format("1px solid {0}", color));
            //left.Text = Utils.Helper.Util.GetTextResource("CalculationIncomesTitle");
            left.Style["border-top"] = "1px solid white";
            left.Style["border-left"] = "1px solid white";
            if (skipRight == false)
                left.Style["border-right"] = "1px solid white";
            left.Style["background"] = "white";
            row.Cells.Add(left);
        }

        public delegate bool IsRequiredColumn(TaxDetailHeaderEnum type);
        /// <summary>
        /// Provides logic in application if the ColumnType is Income or Not
        /// </summary>
        public static bool IsDefaultIncome(TaxDetailHeaderEnum type)
        {
            return type == TaxDetailHeaderEnum.DefaultIncome;
        }
        public static bool IsPastIncome(TaxDetailHeaderEnum type)
        {
            return type == TaxDetailHeaderEnum.PastIncome;
        }
        public static bool IsCurrentIncome(TaxDetailHeaderEnum type)
        {
            return type == TaxDetailHeaderEnum.CurrentIncome;
        }
        public static bool IsForecastIncome(TaxDetailHeaderEnum type)
        {
            return type == TaxDetailHeaderEnum.ForecastIncome;
        }
        public static bool IsOneTimeIncome(TaxDetailHeaderEnum type)
        {
            return type == TaxDetailHeaderEnum.OneTimeIncome;
        }
        public static bool IsDeemedIncome(TaxDetailHeaderEnum type)
        {
            return type == TaxDetailHeaderEnum.DeemedIncome;
        }

 


        private  void AddSingleEmptyGroupColumn(GridViewRow row)
        {
            TableCell cell = new TableHeaderCell();
            //cell.BackColor = ColorTranslator.FromHtml(incomeColor);
            cell.Style.Add("border-bottom", String.Format("1px solid {0}", "black"));
            row.Cells.Add(cell);

        }

        private  void AddGroupColumn(GridViewRow row, int count, string columnGroupText)
        {
            TableHeaderCell left = new TableHeaderCell();
            left.ColumnSpan = count;
            left.HorizontalAlign = HorizontalAlign.Center;
           // left.BackColor = ColorTranslator.FromHtml(incomeColor);
            left.Text = columnGroupText;
            row.Cells.Add(left);

        }

        public static Label GetLabelInHeader(ControlCollection ctls)
        {
            foreach (var ctl in ctls)
            {
                if ((ctl as Label) != null)
                    return ctl as Label;
            }
            return null;
        }

        private  int GetSimilarColumnCount(GridView grid, GridViewRow headerRow, int columnStartIndex, IsRequiredColumn columnCheckingMethod)
        {
            int count = 0;
            for (; columnStartIndex < grid.Columns.Count; columnStartIndex++)
            {
                Label lbl = GetLabelInHeader(headerRow.Cells[columnStartIndex].Controls);
                if (lbl != null)
                {
                    TaxDetailHeaderEnum columnType = (TaxDetailHeaderEnum)Int32.Parse(lbl.Attributes["GroupType"]);
                    //if (CalculationValue.IsHPLUnitColumn(columnType))
                    if (columnCheckingMethod(columnType))
                        count += 1;
                    else
                    {
                        break;
                    }
                }
            }
            return count;
        }


        protected void btnUpdate_Click(object sender, EventArgs e)
        {

            //PayrollPeriod payroll = CommonManager.GetLastPayrollPeriod();
            //if (payroll != null)
            {
                ExportToExcel();
                //GridViewExportUtil.Export(string.Format("Tax Details List for {0}.xls", payroll.Name), gvEmployeeIncome);
            }
        }

       

        #endregion


        //string value = dataSource.GetValue(header);
        void BindEmployees()
        {
            if (payrollPeriodId == 0)
                return;

            //try
            {
                int? empId = null;

                if (!string.IsNullOrEmpty(hiddenEmployeeID.Value))
                    empId = int.Parse(hiddenEmployeeID.Value);

                List<Report_GetTaxableIncomeIRDDepositedComparisionResult> list = EmployeeManager.GetTaxableAndIRDDepositedComparisionReport
                    (payrollPeriodId,empId,chkHideRetired.Checked,int.Parse(ddlBranch.SelectedValue));

                // calcualte from tax details
                foreach(var item in list)
                {
                    item.GetValue();

                    item.TDSIncome = Convert.ToDecimal(item.TotalTaxableIncome) - Convert.ToDecimal(item.SSTIncome);


                    item.RemainingSSTIncome = Convert.ToDecimal(item.SSTIncome) - Convert.ToDecimal(item.SSTIncomeDeposited);
                    item.RemainingSST = Convert.ToDecimal(item.TotalSST) - Convert.ToDecimal(item.SSTDeposited);

                    item.RemainingTDSIncome = Convert.ToDecimal(item.TDSIncome) - Convert.ToDecimal(item.TDSIncomeDeposited);
                    item.RemainingTDS = Convert.ToDecimal(item.TotalTDS) - Convert.ToDecimal(item.TDSDeposited);
                }



                gvEmployeeIncome.DataSource = list;
                gvEmployeeIncome.DataBind();
            }
            //catch (Exception exp)
            //{
            //    divWarningMsg.InnerHtml = "Please make sure there is no red mark in current salary for income/deduction adjsutment.";
            //    divWarningMsg.Hide = false;
            //}
        }

        //protected void btnUpdate_Click(object sender, EventArgs e)
        //{

        //    PayrollPeriod payroll = CommonManager.GetLastPayrollPeriod();
        //    if (payroll != null)
        //    {
        //        ExportToExcel();
        //        //GridViewExportUtil.Export(string.Format("Tax Details List for {0}.xls", payroll.Name), gvEmployeeIncome);
        //    }
        //}



        public void ExportToExcel()
        {
            bool gridViewLastColumnVisibility = gvEmployeeIncome.Columns[gvEmployeeIncome.Columns.Count - 1].Visible;
            GridLines gridViewGridLines = gvEmployeeIncome.GridLines;




            Response.Clear();

            Response.AddHeader("content-disposition", "attachment; filename=\"Taxable income and IRD Deposited.xls\"");

            //Response.Charset = "";


            Response.ContentType = "application/vnd.xls";
            //Response.ContentType = "application/ms-excel";
            Response.ContentEncoding = System.Text.Encoding.Default;
            //Response.ContentEncoding = Encoding.;
            Response.Charset = "utf-8";

            Response.HeaderEncoding = Encoding.UTF8;
            System.IO.StringWriter stringWrite = new System.IO.StringWriter();

            System.Web.UI.HtmlTextWriter htmlWrite =
            new HtmlTextWriter(stringWrite);
            //  gvw.HeaderStyle.BackColor = System.Drawing.Color.Red;



            //gvw.DataSource = CalculationManager.GetCalculationList(SessionManager.CurrentCompanyId,
            //        this.GetPayrollPeriodId(), 1,
            //        999999, ref _tempCount, chkHasRetiredOrResigned.Checked, null);
            // gvw.DataBind();





            ClearControls(gvEmployeeIncome);


           // gvw.Columns[gvEmployeeIncome.Columns.Count - 1].Visible = false;
            gvEmployeeIncome.GridLines = GridLines.Both;


            foreach (GridViewRow r in gvEmployeeIncome.Rows)
            {
                int columnIndex = 0;
                foreach (TableCell c in r.Cells)
                {
                    //skip two columns
                    if (++columnIndex > 2)
                        c.HorizontalAlign = HorizontalAlign.Right;

                    //c.Attributes.Add("class", "text");
                    // c.Text = r.Cells.ToString();
                    c.Width = 80;

                }

            }


            gvEmployeeIncome.RenderControl(htmlWrite);

            Response.Write(stringWrite.ToString());

            Response.End();

            gvEmployeeIncome.Columns[gvEmployeeIncome.Columns.Count - 1].Visible = gridViewLastColumnVisibility;
            gvEmployeeIncome.GridLines = gridViewGridLines;

            //gvw.DataSource = CalculationManager.GetCalculationList(SessionManager.CurrentCompanyId,
            //        this.GetPayrollPeriodId(), 1,
            //        int.Parse(pagingCtl.DDLRecords.SelectedValue), ref _tempCount, chkHasRetiredOrResigned.Checked, null);

            //gvw.DataBind();


        }

        private void ClearControls(Control control)
        {
            for (int i = control.Controls.Count - 1; i >= 0; i--)
            {
                ClearControls(control.Controls[i]);
            }
            //
            // if(control is Table)

            if (!(control is TableCell))
            {
                if (control.GetType().GetProperty("SelectedItem") != null)
                {
                    LiteralControl literal = new LiteralControl();
                    control.Parent.Controls.Add(literal);
                    try
                    {
                        literal.Text =
                            (string)control.GetType().GetProperty("SelectedItem").
                                GetValue(control, null);
                    }
                    catch
                    { }
                    control.Parent.Controls.Remove(control);
                }
                else if (control.GetType().GetProperty("Text") != null)
                {
                    LiteralControl literal = new LiteralControl();
                    control.Parent.Controls.Add(literal);
                    literal.Text =
                        (string)control.GetType().GetProperty("Text").
                            GetValue(control, null);
                    control.Parent.Controls.Remove(control);
                }
            }
            return;
        }





        /// <summary>
        /// Replace any of the contained controls with literals
        /// </summary>
        /// <param name="control"></param>
        private void PrepareGridViewForExport(Control gv)
        {



            LinkButton lb = new LinkButton();

            Literal l = new Literal();

            string name = String.Empty;

            for (int i = 0; i < gv.Controls.Count; i++)
            {


                if (gv.Controls[i].GetType() == typeof(LinkButton))
                {

                    l.Text = (gv.Controls[i] as LinkButton).Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                else if (gv.Controls[i].GetType() == typeof(TextBox))
                {

                    l.Text = (gv.Controls[i] as TextBox).Text.ToString();

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);


                }

                if (gv.Controls[i].HasControls())
                {

                    PrepareGridViewForExport(gv.Controls[i]);

                }

            }
        }

    }
    
}
