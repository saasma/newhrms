<%@ Page Title="CIT Balance Yearly Report" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="CITBalanceYearwiseReport.aspx.cs" Inherits="Web.CP.CITBalanceYearwiseReport" %>

<%@ Register Src="../UserControls/CITBalanceYearwiseReportUC.ascx" TagName="CITBalanceYearwiseReportUC" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">

<div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    CIT Balance Yearly Report
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
    <div style='margin-top:15px'>
    <uc1:CITBalanceYearwiseReportUC Id="CITBalanceYearwiseReportUC1" runat="server" />
    </div>
</div>
</asp:Content>
