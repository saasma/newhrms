﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using Utils.Helper;
using System.IO;
using Utils;
using Utils.Security;
using Utils.Calendar;

namespace Web.CP
{
    public partial class CustomGratuityStartDate : BasePage
    {


        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.HumanResource;
            }
        }
     
        protected void Page_Load(object sender, EventArgs e)
        {
           


            if (!X.IsAjaxRequest && !IsPostBack)
            {

               
               Initialize();
               btnLoad_Click(null, null);
                              

            }
        }

    
        private void Initialize()
        {

           

        }

        protected void btnAddNew_Click(object sender, DirectEventArgs e)
        {
            ClearControls();
            window.Show();
        }

        protected void ClearControls()
        {
            txtDate.Text = "";
            hiddenValue.Text = "";
            txtComment.Text = "";

        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveUpdProjectAssociation");
            if (Page.IsValid)
            {
                EmployeeCustomGratuityDate obj = new EmployeeCustomGratuityDate();

                if (cmbEmployee.SelectedItem == null || cmbEmployee.SelectedItem.Value == null)
                {
                    NewMessage.ShowWarningMessage("Employee selection is required.");
                    return;
                }

                obj.EIN = int.Parse(cmbEmployee.SelectedItem.Value);
                obj.DateEng = DateTime.Parse(txtDate.Text.Trim());
                obj.Comment = txtComment.Text.Trim();

                if (!string.IsNullOrEmpty(hiddenValue.Text))
                    obj.EIN = int.Parse(hiddenValue.Text);





                Status status = LeaveAttendanceManager.SaveUpdateGratuityStartDate(obj);
                if (status.IsSuccess)
                {
                    if (!string.IsNullOrEmpty(hiddenValue.Text))
                        NewMessage.ShowNormalMessage("Record updated successfully.");
                    else
                        NewMessage.ShowNormalMessage("Record saved successfully.");
                    window.Close();
                    btnLoad_Click(null, null);

                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }

            }
        }

        protected void btnEdit_Click(object sender, DirectEventArgs e)
        {

            int id = int.Parse(hiddenValue.Text);


            EmployeeCustomGratuityDate obj = LeaveAttendanceManager.GetGratuityDate(id);
            if (obj != null)
            {
                
                txtDate.Text = obj.DateEng.ToString();
                txtComment.Text = obj.Comment;

              
                window.Show();
            }
        }

        protected void btnDelete_Click(object sender, DirectEventArgs e)
        {
            int id = int.Parse(hiddenValue.Text);

            LeaveAttendanceManager.DeleteGratuityDate(new EmployeeCustomGratuityDate { EIN = id });

            NewMessage.ShowNormalMessage("Record deleted successfully.");
            storeGrid.Reload();

        }



        protected void btnLoad_Click(object sender, DirectEventArgs e)
        {
            int totalRecords = 0;



            int empId = -1;

            if (cmbEmpSearch.SelectedItem != null && cmbEmpSearch.SelectedItem.Value != null)
                empId = int.Parse(cmbEmpSearch.SelectedItem.Value);

            //if (list.Any())
            storeGrid.DataSource = BLL.BaseBiz.PayrollDataContext.GetCustomGratuityStartDate(empId).ToList();
            storeGrid.DataBind();

        }

        public void btnExport_Click(object sender, EventArgs e)
        {

            int empId = -1;

            if (cmbEmpSearch.SelectedItem != null && cmbEmpSearch.SelectedItem.Value != null)
                empId = int.Parse(cmbEmpSearch.SelectedItem.Value);

            //if (list.Any())
            List<GetCustomGratuityStartDateResult> list = BLL.BaseBiz.PayrollDataContext.GetCustomGratuityStartDate(empId).ToList();




            Bll.ExcelHelper.ExportToExcel("Gratuity Dates", list,
                new List<String> { },
            new List<String>() { },
            new Dictionary<string, string>() { { "DateEng", "New Date" }, { "ActualStartDate", "Current Date" } },
            new List<string>() { }
            , new Dictionary<string, string>() {
              
            }
            , new List<string> { });


        }


    }
}