﻿<%@ Page Title="Employee Catalogue" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="AAEmployeeCatalogue.aspx.cs" Inherits="Web.CP.AAEmployeeCatalogue" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/ContentHeader.ascx" TagName="ContentHeader" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .button
        {
            clear: both;
        }
        
        
        .phones-view
        {
            background-color: #fff;
            text-shadow: #fff 0 1px 0;
            position: relative;
            display: block;
            height: auto;
        }
        
        .phones-view div.phone img
        {
            margin-bottom: 1px;
        }
        
        .phones-view div.phone
        {
            float: left;
            padding: 3px;
            margin: 3px; /*    margin: 10px 0 0 25px;*/
            text-align: left;
            line-height: 11px;
            color: #333;
            font-size: 11px;
            font-family: "Helvetica Neue" ,sans-serif;
            height: 135px;
            width: 385px;
            overflow: hidden;
            border-top: 1px solid transparent;
            cursor: pointer;
            background-color: #dfe8f6;
            display: inline-block;
        }
        
        .x-ie6 .phones-view div.phone, .x-ie7 .phones-view div.phone, .x-ie8 .phones-view div.phone
        {
            border-top: none;
            padding: 3px 2px;
            margin: 2px;
        }
        
        .phones-view div.phone-hover
        {
            background-color: #eee;
        }
        
        .phones-view .x-item-selected
        {
            border: 1px solid blue !important;
        }
        
        .phones-view div.phone strong
        {
            color: #000;
            display: block;
        }
        
        .phones-view div.phone span
        {
            color: #000;
        }
        
        .phones-view div.phone:after
        {
            content: 'Tenant : null';
            visibility: hidden;
            background-color: White;
            color: White;
        }
        .phone p
        {
            margin-bottom: 5px !important;
            width: 282px;
        }
        #content
        {
            padding-left: 5px !important;
        }
        .contentLeftBlockRemoval
        {
            padding-left: 5px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <%--  <ext:ResourceManager runat="server" />--%>
    <ext:Hidden ID="Hidden_EmployeeId" runat="server">
    </ext:Hidden>
    <ext:Hidden ID="Hidden_MemberName" runat="server">
    </ext:Hidden>
    <ext:Hidden ID="Hidden_IsEditMember" runat="server" Text="True">
    </ext:Hidden>
    <ext:Hidden ID="GridData" runat="server" />
    <ext:Hidden ID="Hidden_CurrentPage" runat="server" Text="1">
    </ext:Hidden>
    <ext:Hidden ID="Hidden_searchText" runat="server">
    </ext:Hidden>
    <ext:Hidden ID="Hidden_Sort" Text="1" runat="server">
    </ext:Hidden>
    <ext:Hidden ID="Hidden_IsActive" runat="server" Text="True">
    </ext:Hidden>
    <ext:Hidden ID="Hidden_PageSize" runat="server" Text="20">
    </ext:Hidden>
    <ext:Hidden ID="Hidden_FromDate" runat="server">
    </ext:Hidden>
    <ext:Hidden ID="Hidden_ToDate" runat="server">
    </ext:Hidden>
    <ext:Hidden ID="Hidden_IsShowRunningBalance" runat="server">
    </ext:Hidden>
    <ext:Hidden ID="Hidden_GridOrThumbnail" Text="true" runat="server">
    </ext:Hidden>
    <ext:Store ID="StorePaging" runat="server">
        <Model>
            <ext:Model ID="StorePagingModel" runat="server" IDProperty="Value">
                <Fields>
                    <ext:ModelField Name="Text" Type="Int">
                    </ext:ModelField>
                    <ext:ModelField Name="Value" Type="Int">
                    </ext:ModelField>
                </Fields>
            </ext:Model>
        </Model>
    </ext:Store>
    <ext:Menu ID="Menu1" runat="server" Width="150">
        <Items>
            <ext:MenuItem ID="menuButton1" Width="190" Icon="Pencil" runat="server" Text="View Details"
                OnClientClick="var a = #{Hidden_EmployeeId}.getValue(); window.open('../newhr/EmployeeDetails.aspx?Id='+a)">
            </ext:MenuItem>
        </Items>
        <Listeners>
        </Listeners>
    </ext:Menu>
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Employee List
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <asp:LinkButton runat="server" Visible="false"></asp:LinkButton>
        <div class="alert alert-info">
            <div>
                <table>
                    <tr>
                        <td>
                            <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                                <Proxy>
                                    <ext:AjaxProxy Url="../Handler/EmpSearch.ashx">
                                        <ActionMethods Read="GET" />
                                        <Reader>
                                            <ext:JsonReader Root="plants" TotalProperty="total" />
                                        </Reader>
                                    </ext:AjaxProxy>
                                </Proxy>
                                <Model>
                                    <ext:Model ID="Model2" IDProperty="Name" Name="ItemLineModel" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Name" Type="String" />
                                            <ext:ModelField Name="EmployeeId" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                            <ext:ComboBox ID="txtSearchWord" LabelSeparator="" EmptyText="Search by Name" FieldLabel="Search Employee"
                                LabelWidth="70" LabelAlign="Top" runat="server" DisplayField="Name" ValueField="EmployeeId"
                                StoreID="storeSearch" TypeAhead="false" Width="150" PageSize="9999" HideBaseTrigger="true"
                                MinChars="1" TriggerAction="All">
                                <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                                    <ItemTpl ID="ItemTpl1" runat="server">
                                        <Html>
                                            <tpl>
                                            <div class="search-item">
                                                            <span>{Name}</span>  
                                             </div>
					                </tpl>
                                        </Html>
                                    </ItemTpl>
                                </ListConfig>
                                <Triggers>
                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                </Triggers>
                                <Listeners>
                                    <Select Handler="this.getTrigger(0).show();" />
                                    <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                    <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                                </Listeners>
                            </ext:ComboBox>
                        </td>
                        <td style="padding-left: 10px">
                            <ext:ComboBox FieldLabel="Branch" SelectionMode="All" ID="cmbBranch" Width="180px"
                                runat="server" ValueField="BranchId" DisplayField="Name" LabelAlign="Top" LabelSeparator=""
                                ForceSelection="true" QueryMode="Local">
                                <Store>
                                    <ext:Store ID="Store1" runat="server">
                                        <Model>
                                            <ext:Model ID="Model3" IDProperty="BranchId" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="BranchId" />
                                                    <ext:ModelField Name="Name" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <Triggers>
                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                </Triggers>
                                <Listeners>
                                    <Select Handler="this.getTrigger(0).show();" />
                                    <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                    <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                                </Listeners>
                            </ext:ComboBox>
                        </td>
                        <td style="padding-left: 10px">
                            <ext:ComboBox FieldLabel="Department" SelectionMode="All" ID="cmbDepartment" Width="180px"
                                runat="server" ValueField="DepartmentId" DisplayField="Name" LabelAlign="Top"
                                LabelSeparator="" ForceSelection="true" QueryMode="Local">
                                <Store>
                                    <ext:Store ID="Store2" runat="server">
                                        <Model>
                                            <ext:Model ID="Model4" IDProperty="DepartmentId" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="DepartmentId" />
                                                    <ext:ModelField Name="Name" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <Triggers>
                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                </Triggers>
                                <Listeners>
                                    <Select Handler="this.getTrigger(0).show();" />
                                    <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                    <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                                </Listeners>
                            </ext:ComboBox>
                        </td>
                        <td style="padding-left: 10px">
                            <ext:ComboBox FieldLabel="Designation" SelectionMode="All" ID="cmbDesignation" Width="180px"
                                runat="server" ValueField="DesignationId" DisplayField="Name" LabelAlign="Top"
                                LabelSeparator="" ForceSelection="true" QueryMode="Local">
                                <Store>
                                    <ext:Store ID="Store3" runat="server">
                                        <Model>
                                            <ext:Model ID="Model5" IDProperty="DesignationId" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="DesignationId" />
                                                    <ext:ModelField Name="Name" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <Triggers>
                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                </Triggers>
                                <Listeners>
                                    <Select Handler="this.getTrigger(0).show();" />
                                    <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                    <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                                </Listeners>
                            </ext:ComboBox>
                        </td>
                        <td style="vertical-align: bottom; padding-left: 10px;">
                            <ext:Button ID="btnSearch" runat="server" Cls="btn btn-default btn-metro" Width="100" Height="30" Text="Search">
                                <Listeners>
                                    <Click Handler="searchList();" />
                                </Listeners>
                                <%--<DirectEvents>
                                <Click OnEvent="ButtonSearch_Click">
                                </Click>
                            </DirectEvents>--%>
                            </ext:Button>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div style="clear: both">
        </div>
        <ext:Panel ID="RightView" runat="server" MarginSpec="1 1 1 1" BodyCls="dbPanelBodyStyle"
            FrameHeader="false" Layout="BorderLayout" Border="true" Height="600" Style="width: 1200px;"
            Region="Center">
            <Items>
                <ext:Panel ID="Panel_VendorTransactionList" Hidden="false" runat="server" FrameHeader="false"
                    Border="false" Region="Center" Header="false" Title="Employee Catalogue" Layout="BorderLayout">
                    <TopBar>
                        <ext:Toolbar ID="Toolbar1" runat="server" Height="34">
                            <Items>
                                <%--  <ext:Button ID="Toggle1" runat="server" Icon="BorderAll" EnableToggle="true" ToggleGroup="Group1"
                                Pressed="true">
                                <DirectEvents>
                                    <Click OnEvent="ToggleButton_Click">
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="Toggle2" runat="server" Icon="TextAlignJustify" EnableToggle="true"
                                ToggleGroup="Group1">
                                <DirectEvents>
                                    <Click OnEvent="ToggleButton_Click">
                                    </Click>
                                </DirectEvents>
                            </ext:Button>--%>
                                <%-- <ext:ToolbarSpacer ID="ToolbarSpacer1" runat="server" />--%>
                                <ext:ComboBox ID="cmbSortBy" Height="28" LabelSeparator="" runat="server" FieldLabel="Sort By"
                                    LabelAlign="Right" LabelWidth="50" Width="200" Editable="false">
                                    <Items>
                                        <ext:ListItem Text="EIN" Value="employeeId" />
                                        <ext:ListItem Text="Name" Value="name" />
                                        <ext:ListItem Text="Designation" Value="designation" />
                                        <ext:ListItem Text="Joined Date" Value="joined" />
                                    </Items>
                                    <SelectedItems>
                                        <ext:ListItem Index="0" />
                                    </SelectedItems>
                                    <Listeners>
                                        <Select Handler="#{Hidden_Sort}.setValue(#{cmbSortBy}.getValue() + #{cmbSortByOrder}.getValue());searchList();" />
                                    </Listeners>
                                </ext:ComboBox>
                                <ext:ComboBox ID="cmbSortByOrder" Height="28" LabelSeparator="" runat="server" FieldLabel="Order"
                                    LabelAlign="Right" LabelWidth="50" Width="150" Editable="false">
                                    <Items>
                                        <ext:ListItem Text="ASC" Value=" ascending" />
                                        <ext:ListItem Text="DESC" Value=" descending" />
                                    </Items>
                                    <SelectedItems>
                                        <ext:ListItem Index="0" />
                                    </SelectedItems>
                                    <Listeners>
                                        <Select Handler="#{Hidden_Sort}.setValue(#{cmbSortBy}.getValue() + #{cmbSortByOrder}.getValue());searchList();" />
                                    </Listeners>
                                </ext:ComboBox>
                            </Items>
                        </ext:Toolbar>
                    </TopBar>
                    <Items>
                        <ext:DataView ContextMenuID="Menu1" ID="phones" runat="server" DeferInitialRefresh="false"
                            ItemSelector="div.phone" OverItemCls="phone-hover" MultiSelect="true" AutoScroll="true"
                            Cls="phones-view" TrackOver="true">
                            <Store>
                                <ext:Store AutoLoad="false" ID="StoreRoomList" runat="server" PageSize="100">
                                    <Proxy>
                                        <ext:AjaxProxy Json="true" Url="../Handler/AAEmployeeCatalogueHandler.ashx">
                                            <ActionMethods Read="GET" />
                                            <Reader>
                                                <ext:JsonReader Root="data" TotalProperty="total" />
                                            </Reader>
                                        </ext:AjaxProxy>
                                    </Proxy>
                                    <AutoLoadParams>
                                        <ext:Parameter Name="start" Value="0" Mode="Raw" />
                                    </AutoLoadParams>
                                    <Parameters>
                                        <ext:StoreParameter Name="Search" Value="#{txtSearchWord}.getRawValue()" Mode="Raw"
                                            ApplyMode="Always" />
                                        <ext:StoreParameter Name="Branch" Value="#{cmbBranch}.getValue()" Mode="Raw" ApplyMode="Always" />
                                        <ext:StoreParameter Name="Department" Value="#{cmbDepartment}.getValue()" Mode="Raw"
                                            ApplyMode="Always" />
                                        <ext:StoreParameter Name="Designation" Value="#{cmbDesignation}.getValue()" Mode="Raw"
                                            ApplyMode="Always" />
                                        <ext:StoreParameter Name="GridOrThumbnail" Value="#{Hidden_GridOrThumbnail}.getValue()"
                                            Mode="Raw" ApplyMode="Always" />
                                        <ext:StoreParameter Name="MemberName" Value="#{Hidden_MemberName}.getValue()" Mode="Raw"
                                            ApplyMode="Always" />
                                        <ext:StoreParameter Name="sort" Value="#{Hidden_Sort}.getValue()" Mode="Raw" ApplyMode="Always" />
                                    </Parameters>
                                    <Model>
                                        <ext:Model ID="Model1" runat="server" IDProperty="EmployeeId">
                                            <Fields>
                                                <ext:ModelField Name="EmployeeId" Type="string" />
                                                <ext:ModelField Name="Name" Type="string" />
                                                <ext:ModelField Name="Department" Type="string" />
                                                <ext:ModelField Name="Branch" />
                                                <ext:ModelField Name="Mobile" Type="string" />
                                                <ext:ModelField Name="Designation" Type="string" />
                                                <ext:ModelField Name="Since" Type="string" />
                                                <ext:ModelField Name="IdCardNo" Type="string" />
                                                <ext:ModelField Name="Telephone" Type="string" />
                                                <ext:ModelField Name="Email" Type="string" />
                                                <ext:ModelField Name="Mobile" Type="string" />
                                                <ext:ModelField Name="Phone" Type="string" />
                                                <ext:ModelField Name="UrlPhoto" Type="string" />
                                                <ext:ModelField Name="GridOrThumbnail" Type="Boolean" />
                                                <ext:ModelField Name="RowNumber" Type="string" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                    <%-- <Sorters>
                                    <ext:DataSorter Property="name" Direction="ASC" />
                                </Sorters>--%>
                                </ext:Store>
                            </Store>
                            <Tpl ID="Tpl1" runat="server">
                                <Html>
                                    <tpl for=".">
                                                
                                                
                                                <tpl if="GridOrThumbnail==true">
                                                     
                                                  <div class="phone" style="background-color:#F2F2F2">
                                                               <div style="float:left; ">
                                                                    <img src="../Uploads/{UrlPhoto}" width="90" height="100">
                                                                    </img>
                                                                </div>

                                                                <div style="float:left; margin-left:5px;  ">
                                                                    <p>
                                                            
                                                                        <span style='font-weight:bold'>{Name} - {EmployeeId}</span>
                                                                    </p>
                                                                    <p>
                                                                        <span>{Designation}</span>, Joined <span>{Since}</span>
                                                                    </p>
                                                                    <p>
                                                                        {Branch}<br />
                                                                        {Department}
                                                                    </p>
                                                                    <p>
                                                                        <span>{Telephone}</span>
                                                                    </p>
                                                                    <p>
                                                                        <span>{Mobile}</span>
                                                                    </p>
                                                                    
                                                                </div>
                                                                
                                                                <span style="color:blue; padding-left:2px; text-align:left; ">{Email}</span>
                                                                
                                                        </div>
                                                        </tpl>

                                                               
                                                    
                                              </tpl>
                                </Html>
                            </Tpl>
                            <Plugins>
                                <ext:DataViewAnimated ID="DataViewAnimated1" runat="server" Duration="550" IDProperty="id" />
                            </Plugins>
                            <Listeners>
                                <ItemMouseEnter Fn="itemClick" />
                            </Listeners>
                        </ext:DataView>
                    </Items>
                    <BottomBar>
                        <ext:PagingToolbar ID="PagingToolbar_AdjustmentList" runat="server" StoreID="StoreRoomList">
                        </ext:PagingToolbar>
                    </BottomBar>
                </ext:Panel>
            </Items>
        </ext:Panel>
    </div>
    <br />
    <script type="text/javascript">
        function medicalTaxCreditPopupCall(medicalTaxId) {
            var ret = medicalTaxCreditPopup("Id=" + medicalTaxId);
            if (ret == "Yes")
                __doPostBack('Reload', '');
        }

        function parentReloadCallbackFunction(childWindow, ret) {
            childWindow.close();
            if (ret == "Yes")
                __doPostBack('Reload', '');

        }

        function refreshWindow() {
            __doPostBack('Reload', '');
        }
    </script>
    <script type="text/javascript">

        var Hidden_searchText = null;
        var Hidden_PageSize = null;
        var Hidden_FromDate = null;
        var Hidden_ToDate = null;
        var Hidden_EmployeeId = null;
        var PagingToolbar_AdjustmentList = null;
        var Hidden_CurrentPage = null;

       



        var Hidden_IsShowRunningBalance = null;
   

        //to track if dash board mode or customer details listing
        var isDashboardVisible = true;

        Ext.onReady(function () {
            
            Hidden_searchText = <%=Hidden_searchText.ClientID %>;
            Hidden_PageSize = <%=Hidden_PageSize.ClientID %>;
            Hidden_FromDate = <%=Hidden_FromDate.ClientID %>;
            Hidden_ToDate = <%=Hidden_ToDate.ClientID %>;
            Hidden_EmployeeId = <%=Hidden_EmployeeId.ClientID %>;
            Hidden_CurrentPage = <%=Hidden_CurrentPage.ClientID %>;
            PagingToolbar_AdjustmentList = <%= PagingToolbar_AdjustmentList.ClientID %>;




            Hidden_IsShowRunningBalance = <%=Hidden_IsShowRunningBalance.ClientID %>;
         
        });

      
        function searchList() 
        {
            <%=StoreRoomList.ClientID %>.reload();
        }

        function setValueInControl() 
        {
            Hidden_searchText.setValue(getSearchText());
            Hidden_FromDate.setValue(getStartDate());
            Hidden_ToDate.setValue(getEndDate());
            Hidden_IsShowRunningBalance.setValue(getRunningBalanceState());
        }
      
        function RenderForZeroAmount(value)
        {
            if(value.toString() == "0")
                return "-" ;
            else
                return value;
        }

        

        var getRenewedImage = function(value)
        {
            if( value == true)
                return "<img src='../images/ok.gif' style='width:15px;height:15px' />";
            else
                return "<img src='../images/exclaim.gif'  style='width:15px;height:15px' />";
        }

           function searchByText()
        {
            setValueInControl();
            searchList();
        }


        var GetGridRow = function (value, meta, record) 
        {
            if(record == null || typeof(record) == 'undefined')
                return "";

                 var link = "";
            if(record.data.DocumentType == "")
                link = DocumentTab.MemberPayment;
                
            else if(record.data.DocumentType == 'Journal')
               link = DocumentTab.AdjustmentEntries;
            
            else if(record.data.DocumentType == 'GeneralPayment')
                link = DocumentTab.GeneralPayment;

            var absUrl = getAbsoluteUrl(link , record.data.DocumentID);
            return "<a href='" + absUrl +"'>" + value + "<a/>";
        };         


         var memID = null;
       
        

       

        var itemClick = function (view, record, item, index, e) 
        {

            var item = e.getTarget(".phone");

            var row = e.getTarget("tr.phonesRow"),
            item2 = row && Ext.get(row).child("td.phonesCell");

            if (item) {
                memID = record.data.EmployeeId;
                <%= Hidden_EmployeeId.ClientID %>.setValue(memID);
             
            }
            

        };
       
    </script>
</asp:Content>
