﻿using System;
using System.Collections.Generic;
using System.Linq;
using BLL;
using BLL.Manager;
using DAL;
using Utils.Helper;
using Utils.Web;

namespace Web.CP
{
    public partial class MGDeductionDepositList : System.Web.UI.Page
    {
        private PayManager payMgr = new PayManager();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialise();
            }
        }

        private void Initialise()
        {
            LoadDeposit();
            if (ddlDeposits.SelectedItem != null)
            {
                txtDepositRename.Text = ddlDeposits.SelectedItem.Text;
            }
            else
            {
                txtDepositRename.Text = string.Empty;
            }
        }

        private void LoadDeposit()
        {
            ddlDeposits.DataSource = payMgr.GetDepositList(SessionManager.CurrentCompanyId);
            ddlDeposits.DataBind();
        }

        protected void btnAddDepsoit_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                var entity = new PDeductionDepositList();
                entity.CompanyId = SessionManager.CurrentCompanyId;
                entity.Title = txtNewDeposit.Text.Trim();

                payMgr.Save(entity);

                txtNewDeposit.Text = string.Empty;
                Initialise();
                JavascriptHelper.DisplayClientMsg("Deposit added.", Page);
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            if (ddlDeposits.SelectedItem != null)
            {
                var entity = new PDeductionDepositList();
                entity.DeductionDepositListId = int.Parse(ddlDeposits.SelectedValue);

                if (payMgr.Delete(entity))
                {
                    JavascriptHelper.DisplayClientMsg("Deposit deleted.", Page);
                    Initialise();
                }
                else
                {
                    JavascriptHelper.DisplayClientMsg("Deposit is in use.", Page);
                }
            }
        }

        protected void btnRename_Click(object sender, EventArgs e)
        {
            if (Page.IsValid && ddlDeposits.SelectedItem != null)
            {
                var entity = new PDeductionDepositList();
                entity.Title = txtDepositRename.Text.Trim();
                entity.DeductionDepositListId = int.Parse(ddlDeposits.SelectedValue);

                payMgr.Update(entity);
                LoadDeposit();
                UIHelper.SetSelectedInDropDown(ddlDeposits, entity.DeductionDepositListId.ToString());
                JavascriptHelper.DisplayClientMsg("Deposit updated.", Page);
            }
        }
    }
}
