﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Manager;
using BLL;
using DAL;

using Utils.Helper;
using BLL.Base;

namespace Web.CP
{
    public partial class LeaveAdjustmentPage : BasePage
    {

        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.LeaveAndAttendance;
            }
        }

        public bool isPayrollEditable = true;

        protected void Page_Load(object sender, EventArgs e)
        {

            JavascriptHelper.AttachNonDialogPopUpCode(Page, "shiftPopup", "../ExcelWindow/LeaveAdjustmentImportExcel.aspx", 450, 500);

          
            if( !IsPostBack)
            {
                ddlLeaves.DataSource
                    = LeaveAttendanceManager.GetAllLeaves(SessionManager.CurrentCompanyId);
                ddlLeaves.DataBind();

                //ListItem item = new ListItem(LeaveAttendanceManager.DefaultLeaves[1][1],LeaveAttendanceManager.DefaultLeaves[1][0]);
                //ddlLeaves.Items.Insert(1, item);

                ////ListItem item1 = new ListItem(LeaveAttendanceManager.DefaultLeaves[2][1], LeaveAttendanceManager.DefaultLeaves[2][0]);
                ////ddlLeaves.Items.Insert(2, item1);

                //ListItem item2 = new ListItem(LeaveAttendanceManager.DefaultLeaves[3][1], LeaveAttendanceManager.DefaultLeaves[3][0]);
                //ddlLeaves.Items.Insert(2, item2);

                ddlPayrollPeriods.DataSource =
                    CommonManager.GetAllYear(SessionManager.CurrentCompanyId);
                ddlPayrollPeriods.DataBind();
                ListItem selItem = ddlPayrollPeriods.Items[ddlPayrollPeriods.Items.Count - 1];
                if (selItem != null)
                    selItem.Selected = true;
            }

            string code=string.Format( "var diffMsg = '{0}';", Resources.Messages.LeaveAdjustmentDiffMsg);
            RegisterJSCode(code);

            if (Request.Form["__EVENTTARGET"] != null && Request.Form["__EVENTTARGET"].ToLower().Contains("refresh"))
                btnLoad_Click(null,null);
        }

        /// <summary>
        /// Display appropriate message like Payroll already saved so not editable or some employee salary already saved so not editable.
        /// </summary>
        /// <param name="payrollPeriodId"></param>
        /// <param name="list"></param>
        public void ShowAppropriateMessage(bool isEditable)
        {

            if (!isEditable)
            {
                divWarningMsg.InnerHtml = "Next payroll period attendance is already saved. Editing is disabled.";
                divWarningMsg.Hide = false;
            }
            //else
            //{
            //    foreach (LeaveGetLeaveAdjustmentsResult item in list)
            //    {
            //        if (item.IsUsedInCalculation != null && item.IsUsedInCalculation == true)
            //        {
            //            divWarningMsg.InnerHtml = Resources.Messages.SomeOfTheEmployeeSalaryAlreadySavedMsg;
            //            divWarningMsg.Hide = false;
            //            break;
            //        }
            //    }
            //}
        }

        protected void btnLoad_Click(object sender, EventArgs e)
        {
            int payrollId = int.Parse(ddlPayrollPeriods.SelectedValue);
            string selectedValue = ddlLeaves.SelectedValue;
            int leaveId;
            int totalRecords = 0;
            //btnSave.Visible = true;

            //if normal leave is selected
            if (int.TryParse(selectedValue, out leaveId))
            {
                //if (!LeaveAttendanceManager.IsPayrollPeriodValidForLeaveTransfer(payrollId) &&
                //    CommonManager.CompanySetting.WhichCompany != WhichCompany.PSI)
                //{
                //    isPayrollEditable = false;
                //}
                //else
                //    isPayrollEditable = true;

                List<LeaveGetLeaveAdjustmentsResult> list = LeaveAttendanceManager.GetLeaveAdjustments(payrollId, leaveId, pagingCtl.CurrentPage - 1, int.Parse(pagingCtl.DDLRecords.SelectedValue), ref totalRecords
                    , txtEmpSearchText.Text.Trim());
                if (list.Count > 0)
                {
                    string value = list[0].Notes;
                }

                // If has editable rows
                bool isEditable = false;
                foreach (LeaveGetLeaveAdjustmentsResult row in list)
                {
                    if (row.IsEditable != null && row.IsEditable.Value)
                        isEditable = true;
                }
                btnSave.Visible = true; //isEditable || (CommonManager.CompanySetting.WhichCompany == WhichCompany.PSI);

                gvwLeaveAdjustmentList.DataSource =list;
                    
                gvwLeaveAdjustmentList.DataBind();
                pagingCtl.UpdatePagingBar(totalRecords);

                gvwLeaveAdjustmentList.Visible = true;

                if (leaveId >= 0)
                {

                    LeaveAttendanceManager mgr = new LeaveAttendanceManager();
                    LLeaveType leave = mgr.GetLeaveType(leaveId);
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "regType,",
                        string.Format("var isManualLeave = {0};", (leave.FreqOfAccrual == LeaveAccrue.MANUALLY ? "true" : "false"))
                        , true);


                    ShowAppropriateMessage(isEditable);
                }

            }
           

        }

        protected void btnPrevious_Click()
        {
            pagingCtl.CurrentPage -= 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage -= 1;
            btnLoad_Click(null, null);
        }

        protected void btnNext_Click()
        {
            pagingCtl.CurrentPage += 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage += 1;
            btnLoad_Click(null, null);
        }

        protected void ddlRecords_SelectedIndexChanged()
        {
            //SessionManager.EmployeeDisplayRecodsPerPage = int.Parse(pagintCtl.DDLRecords.SelectedValue);
            //_tempCurrentPage = 1;
            pagingCtl.CurrentPage = 1;
            btnLoad_Click(null, null);
        }

        public bool IsEnabled(object value)
        {
            //if( isPayrollEditable)
              //  return true;

            if (((bool)value) == false)
                return true;
            return false;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            bool updated = false;
            int payrollPeriodId = 0;
            string empList = "";

            int? leaveTypeId = null;

            //bool isNoteExists = false;

            foreach (GridViewRow row in gvwLeaveAdjustmentList.Rows)
            {
                int leaveId = (int) gvwLeaveAdjustmentList.DataKeys[row.RowIndex]["LeaveTypeId"];
                int employeeId = (int)gvwLeaveAdjustmentList.DataKeys[row.RowIndex]["EmployeeId"];
                payrollPeriodId = (int)gvwLeaveAdjustmentList.DataKeys[row.RowIndex]["PayrollPeriodId"];
                TextBox txtAdjustment = (TextBox)row.FindControl("txtAdjustment");
                TextBox txtNote = row.FindControl("txtNote") as TextBox;

                string invalidText = Util.HasInvalidCharacterForXMLParsingAndImport(txtNote.Text.Trim());
                if (!string.IsNullOrEmpty(invalidText))
                {
                    //divWarningMsg.InnerHtml = "\"" + invalidText + "\" character can not be set in the note.";
                    //divWarningMsg.Hide = false;
                    NewMessage.ShowWarningMessage("\"" + invalidText + "\" character can not be set in the note.");
                    return;
                }

                if (gvwLeaveAdjustmentList.Rows.Count == 1 && string.IsNullOrEmpty(txtNote.Text.Trim()))
                {
                    //divWarningMsg.InnerHtml = "Note must be placed to save adjustment.";
                    //divWarningMsg.Hide = false;
                    NewMessage.ShowWarningMessage("Note must be placed to save adjustment.");
                    return;
                }

                if (txtAdjustment.Enabled == false)
                    continue;

                if (empList == "")
                    empList = employeeId.ToString();
                else
                    empList += "," + employeeId.ToString();

                LeaveAdjustment leave =  new LeaveAdjustment();
                leave.PayrollPeriodId = payrollPeriodId;
                leave.EmployeeId = employeeId;
                leave.LeaveTypeId = leaveId;
                leave.Notes = txtNote.Text.Trim();

                // need to make rounding from db or proportionate calculation instead of here
                //string strValue = decimal.Parse(txtBalance.Text).ToString("N2");
                //if (strValue == "")
                //    strValue = "0";

                leave.Adjusted = decimal.Parse(txtAdjustment.Text);
                leaveTypeId = leave.LeaveTypeId;
                LeaveAttendanceManager.UpdateLeaveAdjustment(leave);
                updated = true;
            }

            if (updated)
            {
                if (payrollPeriodId != 0 && empList != "")
                    LeaveAttendanceManager.CallToResaveAtte(payrollPeriodId, empList, leaveTypeId);

                divMsgCtl.InnerHtml = Resources.Messages.LeaveAdjustmentSaved;
                divMsgCtl.Hide = false;
                btnLoad_Click(null, null);
            }
                
        }

        public  string GetFormattedValue(object  encased)
        {
            if (encased == null)
                return "0";
            
            if(double.Parse(encased.ToString())==0)
                return "0";

            return Util.FormatForLeaveWithDecimal(encased);
            
        }
        
        protected void Page_PreRender(object sender, EventArgs e)
        {
          

        }
    }
}
