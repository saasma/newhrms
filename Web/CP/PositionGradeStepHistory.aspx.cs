﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using BLL.Manager;
using BLL.Base;

namespace Web.CP
{
    public partial class PositionGradeStepHistory : BasePage
    {
        private CommonManager comm = new CommonManager();
        private List<BLL.BO.PositionHistoryBO> list;
        protected void Page_Load(object sender, EventArgs e)
        {
            this.CustomId = int.Parse(Request.QueryString["EIN"]);
            if (!IsPostBack)
            {
                this.Initialise();
            }
        }

        private void Initialise()
        {
            this.BindData();
        }

        private void BindData()
        {
            this.list = PositionGradeStepAmountManager.GetEmployeePositionGradeStepHistory(CustomId);
            this.gvw.DataSource = list;
            this.gvw.DataBind();
        }

        protected void gvw_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            this.gvw.PageIndex = e.NewPageIndex;
            this.BindData();
        }
    }
}
