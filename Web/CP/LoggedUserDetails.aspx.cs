﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using Utils.Helper;
using BLL.Base;

namespace Web.CP
{
    public partial class LoggedUserDetails : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            List<Web.Global.LoggedInUser> list = Application["UsersOnline"] as List<Web.Global.LoggedInUser>;
            if (list != null)
            {
                adminUserCount.Text = list.Where(x => x.IsEmployee == false).Count().ToString();

                //employeeUserCount.Text = list.Where(x => x.IsEmployee == true).Count().ToString();
            }
        }

     
    }
}
