<%@ Page Language="C#" MasterPageFile="~/Master/HROld.Master" AutoEventWireup="true"
    CodeBehind="HolidayOld.aspx.cs" Inherits="Web.CP.HolidayOld" Title="Holiday" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/CalendarCtl.ascx" TagPrefix="uc" TagName="Calendar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function EnableDisableApplyTo(obj) {
            toggleElementUsingCheckBoxState(obj, '<%= ddlAppliesTo.ClientID %>', true);
            if (obj.checked) {
                // select the first item i.e. All
                $("#" + '<%= ddlAppliesTo.ClientID %>')[0][0].selected = true;
            }
        }
                

    </script>
    <style type="text/css">
        .leftContents
        {
            width: 470px !important;
        }
        .contentLeftBlockRemoval{padding-left:0px!important;}
        .content_right{margin-left:40px;}
        
        .chkDateToPadding
        {
            padding-left:5px;
        }
        
        
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
     <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Annual Holiday
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel" style="margin-top:0px !important; padding-top:0px !important;">
    
    <div class="contentArea">
        
        <div class="leftContents" style="width:570px!important">
            
            <div class="content_box" style='height: 100%; width: inherit'>
                <table class="hr" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="fieldHeader" width="100px">
                            <label class="emp_label">
                                Date</label>
                        </td>
                        <td>
                            <uc:Calendar Id="Calendar1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="fieldHeader" width="100px">
                            <asp:CheckBox ID="chkToDate" Text="To Date" runat="server" AutoPostBack="true" CssClass="chkDateToPadding"
                                oncheckedchanged="chkToDate_CheckedChanged"/>
                        </td>
                        <td>
                            <uc:Calendar Id="CalendarToDate" runat="server" Enabled="false" />
                        </td>
                    </tr>
                    <tr>
                        <td class="fieldHeader" width="100px">
                            <label class="emp_label">
                                Descriptions</label>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtDescription" CssClass="gapbelow" MaxLength="50" 
                                Width="300">
                            </asp:TextBox>
                            <asp:RequiredFieldValidator ID="valDescription" runat="server" ErrorMessage="Description is required."
                                ControlToValidate="txtDescription" Display="None" ValidationGroup="AnnualHoliday"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="fieldHeader" colspan="2">
                            <label class="emp_label">
                                Is National Holiday?</label>
                            <asp:CheckBox ID="chkIsNationalHoliday" runat="server" onclick="EnableDisableApplyTo(this);"
                                Text="" CssClass="padleft" />
                        </td>
                    </tr>
                    <tr runat="server" id="trBranch">
                        <td class="fieldHeader" width="100px" valign="top">
                            <label class="emp_label">
                                Branch</label>
                        </td>
                        <td>
                            <%--<asp:DropDownList ID="ddlBranch" runat="server" AppendDataBoundItems="true" DataTextField="Name"
                                DataValueField="BranchId" CssClass="gapbelow" Width="160px">
                                <asp:ListItem Value="-1" Text="All"></asp:ListItem>
                            </asp:DropDownList>    --%>
                            (Please don't select any branch when holiday is applied to all branches)
                            <div id='containerDIV' style="overflow-y: scroll; height: 200px; width: 375px; border: 1px solid #C1D6F3;
                                padding: 0px; ">
                                <asp:CheckBoxList ID="chkBranchList" Width="350px" Height="100px" RepeatColumns="2"
                                    RepeatDirection="Horizontal" RepeatLayout="Table" runat="server" DataValueField="BranchId"
                                    DataTextField="Name">
                                </asp:CheckBoxList>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="fieldHeader" width="100px">
                            <label class="emp_label">
                                Applies To</label>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlAppliesTo" runat="server" AppendDataBoundItems="true" DataTextField="CasteName"
                                DataValueField="CasteId" CssClass="gapbelow" Width="160px">
                                <asp:ListItem Value="-2" Text="--Select Applies to--"></asp:ListItem>
                                <asp:ListItem Value="-1" Text="All"></asp:ListItem>
                                <asp:ListItem Value="0" Text="Female Only"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator InitialValue="-2" ID="RequiredFieldValidator2" runat="server"
                                ErrorMessage="Applies to is required." ControlToValidate="ddlAppliesTo" Display="None"
                                ValidationGroup="AnnualHoliday"></asp:RequiredFieldValidator>
                            <asp:Button ID="btnAdd" style="margin-left:10px" runat="server" Text="Add To List" OnClick="AddHoliday" OnClientClick="valGroup='AnnualHoliday';return CheckValidation();"
                                CssClass="save" ValidationGroup="AnnualHoliday" />
                        </td>
                    </tr>
                    <tr>
                        <td class="fieldHeader" width="100px">
                            <label class="emp_label">
                                Year Filter</label>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlFinancialYear" runat="server" Width="125" DataValueField="FinancialDateId" AutoPostBack="true" AppendDataBoundItems="true"
                                DataTextField="Name" CssClass="gapbelow" 
                                onselectedindexchanged="ddlFinancialYear_SelectedIndexChanged">
                                <asp:ListItem Value="-1">All</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
                <cc2:EmptyDisplayGridView ShowHeaderWhenEmpty="True" ID="gvwHoliday" CssClass="tableLightColor"
                    UseAccessibleHeader="true" runat="server" DataKeyNames="ID" AutoGenerateColumns="False"
                    OnRowCreated="gvwHoliday_RowCreated" GridLines="None" PageSize="100" AllowPaging="True"
                    OnPageIndexChanging="gvwHoliday_PageIndexChanging" OnRowDeleting="gvwHoliday_RowDeleting"
                    ShowFooterWhenEmpty="False">
                    <Columns>
                        <asp:BoundField HeaderStyle-Width="65" SortExpression="Nep Date" HeaderStyle-HorizontalAlign="Left"
                            ItemStyle-HorizontalAlign="Left" DataField="Date" HeaderText="Nep Date">
                            <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField HeaderStyle-Width="110" SortExpression="Date" HeaderStyle-HorizontalAlign="Left"
                            ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyyy-MMM-dd}" DataField="EngDate" HeaderText="Eng Date">
                            <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField HeaderStyle-Width="140" SortExpression="Description" HeaderStyle-HorizontalAlign="Left"
                            ItemStyle-HorizontalAlign="Left" DataField="Description" HeaderText="Description">
                            <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField SortExpression="AppliesTo" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            DataField="AppliesTo" HeaderText="Applies To">
                            <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField HeaderStyle-Width="180" SortExpression="Branch" HeaderStyle-HorizontalAlign="Left"
                            ItemStyle-HorizontalAlign="Left" DataField="BranchName" HeaderText="Branch">
                            <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundField>
                        <asp:CheckBoxField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            DataField="IsNationalHoliday" HeaderText="National">
                            <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:CheckBoxField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:ImageButton Visible='<%# ((int)Eval("IsDeletable"))==0 %>' ID="ImageButton2"
                                    OnClientClick="return confirm('Do you want to delete the holiday?')" runat="server"
                                    CommandName="Delete" ImageUrl="~/images/delet.png" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="odd" />
                    <AlternatingRowStyle CssClass="even" />
                    <EmptyDataTemplate>
                        <span class="negativelabel">No Holiday Exists!</span>
                    </EmptyDataTemplate>
                </cc2:EmptyDisplayGridView>
            </div>
        </div>
        <div class="content_right">
            <div class="content_title">
                Weekly Holiday
            </div>
            <div class="content_box" style='height: 100%'>
                <div class="input_box">
                    <label class="emp_label" style="padding-top: 2px">
                        Effective From
                    </label>
                    <asp:DropDownList ID="ddlPayrollPeriod" runat="server" Width="125" DataValueField="PayrollPeriodId"
                        DataTextField="Name" CssClass="gapbelow">
                    </asp:DropDownList>
                </div>
                <div class="input_box">
                    <label class="emp_label" style="padding-top: 2px">
                        Day of the Week
                    </label>
                    <asp:DropDownList ID="ddlDayOfWeek" runat="server" AppendDataBoundItems="true" DataTextField="Text"
                        DataValueField="Value" CssClass="gapbelow" Width="125">
                        <asp:ListItem Value="-1" Text="--Select Day of Week--"></asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Days of Week is required"
                        InitialValue="-1" Display="None" ControlToValidate="ddlDayOfWeek" ValidationGroup="WeeklyHoliday"></asp:RequiredFieldValidator>
                </div>
                <div class="input_box">
                    <asp:RadioButtonList ID="rdbFullOrHalf" CssClass="padleft" runat="server" RepeatDirection="Horizontal"
                        RepeatColumns="2" DataTextField="Text" DataValueField="Value">
                    </asp:RadioButtonList>
                </div>
                <div class="input_box">
                    <asp:Button CssClass="save" ID="btnAddWeeklyHoliday" runat="server" Text="Add To List"
                        OnClientClick="valGroup='WeeklyHoliday';return CheckValidation();" OnClick="AddWeeklyHoliday"
                        ValidationGroup="WeeklyHoliday" />
                </div>
                <div class="input_box">
                    <cc2:EmptyDisplayGridView ShowHeaderWhenEmpty="True" CssClass="tableLightColor" UseAccessibleHeader="true"
                        ID="gvwWeeklyHoliday" runat="server" GridLines="None" DataKeyNames="EffectiveFrom,ID"
                        AutoGenerateColumns="False" Width="450px" OnRowCreated="gvwWeeklyHoliday_RowCreated"
                        OnRowDeleting="gvwWeeklyHoliday_RowDeleting" OnSelectedIndexChanged="gvwWeeklyHoliday_SelectedIndexChanged"
                        ShowFooterWhenEmpty="False">
                        <Columns>
                            <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                DataField="EffectiveFrom" HeaderText="Effective From">
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:BoundField>
                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                HeaderText="Day of Week">
                                <ItemTemplate>
                                    <%# DAL.DaysOfWeek.Get((int)(Eval("DayofWeek")))%>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                HeaderText="Nature">
                                <ItemTemplate>
                                    <%# GetNature(Eval("IsFullDay"))%>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImageButton2" Visible='<%# ((int)Eval("IsDeletable"))==0 %>'
                                        OnClientClick="return confirm('Do you want to delete the weekly holiday?')" runat="server"
                                        CommandName="Delete" ImageUrl="~/images/delet.png" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <RowStyle CssClass="odd" />
                        <AlternatingRowStyle CssClass="even" />
                        <EmptyDataTemplate>
                            <span class="negativelabel">No Weekly Holiday Exists!</span>
                        </EmptyDataTemplate>
                    </cc2:EmptyDisplayGridView>
                </div>
            </div>
        </div>
    </div>
    </div>
</asp:Content>
