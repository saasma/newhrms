﻿<%@ Page Title="Payroll periods" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="PayrollPeriodPG.aspx.cs" Inherits="Web.CP.PayrollPeriodPG" %>

<%@ Register Src="../UserControls/PayrollPeriodCtl.ascx" TagName="PayrollPeriod"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Payroll Periods
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <uc1:PayrollPeriod Id="PayrollPeriod1" runat="server" />
    </div>
</asp:Content>
