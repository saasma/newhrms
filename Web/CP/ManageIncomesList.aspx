﻿<%@ Page Title="Income List" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="ManageIncomesList.aspx.cs" Inherits="Web.CP.ManageIncomesList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">


    //callback function in case of chrome to be called from modal popup as it doesn't supports modal window
        function parentIncomeCallbackFunction(state, popupWindow) {
             popupWindow.close();
             <%= hdnIncomeId.ClientID %>.setValue('');
             <%= btnLoad.ClientID %>.fireEvent('click');
        }


     function processChangeOrder(e1,e2,e3,e4,e5)
        {
              var sourceId = e2.records[0].data.IncomeId;
            <%=hdnSource.ClientID %>.setValue(sourceId);
            var destinationId = e3.data.IncomeId;
            <%=hdnDest.ClientID %>.setValue(destinationId);
            <%=hdnDropMode.ClientID %>.setValue(e4);

            <%=btnChangeOrder.ClientID %>.fireEvent('click');
        }

    var prepareCommands = function (grid, toolbar, rowIndex, record) {
         
         var btn = toolbar.items.get(0);

         if (record.data.IncomeId > 0) {
            btn.menu.items.get(0).show();
        }

        if (record.data.IncomeDeletable == true) {
            btn.menu.items.get(1).show();
        }

        if(record.data.IsEnabled == true)
            btn.menu.items.get(2).setText('Disable');

        if(record.data.IsBasicIncome == false)
        {
            btn.menu.items.get(2).show();
        }

        
    };


     var CommandHandler = function(command, record){

            <%= hdnIncomeId.ClientID %>.setValue(record.data.IncomeId);
            
            if(command =="Edit")
            {
                popupIncome('IId=' + record.data.IncomeId);
            }
            else if(command =="Delete")
            {
                <%= btnDelete.ClientID %>.fireEvent('click');
            }
            else
            {
                if(record.data.IsEnabled == true)
                {
                    <%= btnDisable.ClientID %>.fireEvent('click');  
                 }
                else
                {
                    <%= btnEnable.ClientID %>.fireEvent('click');
                 }
            }

        };

    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <ext:Hidden runat="server" ID="hdnIncomeId">
    </ext:Hidden>
    <ext:Hidden runat="server" ID="hdnDropMode" />
    <ext:Hidden runat="server" ID="hdnSource" />
    <ext:Hidden runat="server" ID="hdnDest" />
    <ext:LinkButton ID="btnChangeOrder" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnChangeOrder_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:LinkButton ID="btnDelete" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnDelete_Click">
                <Confirmation ConfirmRequest="true" Message="Do you want to delete the income?" />
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:LinkButton ID="btnEnable" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnEnableDisable_Click">
                <Confirmation ConfirmRequest="true" Message="Do you really want to enable income for all employees?" />
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:LinkButton ID="btnDisable" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnEnableDisable_Click">
                <Confirmation ConfirmRequest="true" Message="Do you really want to disable income for all employees?" />
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Income List
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="alert alert-info" style="margin-top: 10px; padding: 2px;">
            <table>
                <tr>
                    <td style="width: 250px;">
                        <ext:TextField ID="txtIncomeSearch" runat="server" LabelAlign="Top" FieldLabel="Search Income"
                            Width="200" LabelSeparator="" />
                    </td>
                    <td valign="bottom" style="width: 120px;">
                        <ext:Button runat="server" ID="btnLoad" Cls="btn btn-default" Text="<i></i>Load">
                            <DirectEvents>
                                <Click OnEvent="btnLoad_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                    </td>
                    <td valign="bottom">
                        <ext:Button runat="server" ID="btnAddIncome" Cls="btn-sm btn-sect" Text="<i></i>Add Income" Icon="Add"
                            OnClientClick="return popupIncome();">
                        </ext:Button>
                    </td>
                </tr>
            </table>
        </div>
    
        <div runat="server" style="color: #F49D25" id="voucherAlsoChange">
            Note : If voucher is being used, please associate the newly added income to the
            voucher group also.</div>
        <ext:GridPanel ID="gridIncomes" runat="server" Cls="itemgrid" EnableDragDrop="true"
            Width="1060" DDGroup="ddGroup">
            <Store>
                <ext:Store ID="storeIncomes" runat="server" OnReadData="IncomeData_Refresh">
                    <Model>
                        <ext:Model ID="Model1" runat="server" IDProperty="IncomeId">
                            <Fields>
                                <ext:ModelField Name="IncomeId" Type="String" />
                                <ext:ModelField Name="Title" Type="string" />
                                <ext:ModelField Name="Abbreviation" Type="string" />
                                <ext:ModelField Name="CalculationType" Type="string" />
                                <ext:ModelField Name="IsUpdatedWithLeaveValue" Type="string" />
                                <ext:ModelField Name="IsPFCalculatedValue" Type="string" />
                                <ext:ModelField Name="IsDefinedTypeValue" Type="String" />
                                <ext:ModelField Name="IsTaxableValue" Type="String" />
                                <ext:ModelField Name="IsEnabledValue" Type="String" />
                                <ext:ModelField Name="IsEnabled" Type="Boolean" />
                                <ext:ModelField Name="Order" Type="string" />
                                <ext:ModelField Name="IncomeDeletable" Type="Boolean" />
                                <ext:ModelField Name="IsBasicIncome" Type="Boolean" />
                                <ext:ModelField Name="Notes" Type="String" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>
            <ColumnModel>
                <Columns>
                    <ext:RowNumbererColumn ID="colRN" runat="server" Text="SN" MenuDisabled="true" Align="Center"
                        Sortable="false" Width="50" />
                    <ext:Column ID="colTitle" Width="240" Sortable="true" MenuDisabled="true" runat="server"
                        Text="Title" Align="Left" DataIndex="Title" />
                    <ext:Column ID="colAbbreviation" Width="100" Sortable="true" MenuDisabled="true"
                        runat="server" Text="Short Name" Align="Left" DataIndex="Abbreviation" />
                    <ext:Column ID="colCalculationType" Width="130" Sortable="true" MenuDisabled="true"
                        runat="server" Text="Type" Align="Left" DataIndex="CalculationType" />
                    <ext:Column ID="Column1" Width="100" Sortable="true" MenuDisabled="true" runat="server"
                        Text="Defined Type" Align="Center" DataIndex="IsDefinedTypeValue" />
                    <ext:Column ID="Column2" Width="70" Sortable="true" MenuDisabled="true" runat="server"
                        Text="Taxable" Align="Center" DataIndex="IsTaxableValue" />
                    <ext:Column ID="colIsUpdatedWithLeave" Sortable="true" MenuDisabled="true" runat="server"
                        Text="Deduct Leave" Width="90" Align="Center" DataIndex="IsUpdatedWithLeaveValue">
                    </ext:Column>
                    <ext:Column ID="colIsPFCalculated" Sortable="true" MenuDisabled="true" runat="server"
                        Text="PF" Width="40" Align="Center" DataIndex="IsPFCalculatedValue">
                    </ext:Column>
                    <ext:Column ID="Column3" Sortable="false" MenuDisabled="true" runat="server" Text="Notes"
                        Width="150" Align="Left" DataIndex="Notes">
                    </ext:Column>
                    <ext:Column ID="Column4" Sortable="false" MenuDisabled="true" runat="server" Text="Active"
                        Width="50" Align="Center" DataIndex="IsEnabledValue">
                    </ext:Column>
                    <ext:CommandColumn ID="ContextCommand" runat="server" Width="50" OverOnly="false">
                        <PrepareToolbar Fn="prepareCommands">
                        </PrepareToolbar>
                        <Commands>
                            <ext:GridCommand Icon="ApplicationGo">
                                <Menu EnableScrolling="false">
                                    <Items>
                                        <ext:MenuCommand Text="Edit" CommandName="Edit" Hidden="true" />
                                        <ext:MenuCommand Text="Delete" CommandName="Delete" Hidden="true" />
                                        <ext:MenuCommand Text="Enable" CommandName="Archive" Hidden="true" />
                                    </Items>
                                </Menu>
                                <ToolTip Text="Action" />
                            </ext:GridCommand>
                        </Commands>
                        <Listeners>
                            <Command Handler="CommandHandler(command,record);" />
                        </Listeners>
                    </ext:CommandColumn>
                </Columns>
            </ColumnModel>
            <SelectionModel>
                <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" />
            </SelectionModel>
            <View>
                <ext:GridView ID="GridView1" runat="server">
                    <Plugins>
                        <ext:GridDragDrop ID="GridDragDrop1" runat="server" DragGroup="ddGroup" DropGroup="ddGroup" />
                    </Plugins>
                    <Listeners>
                        <BeforeItemClick Fn="processChangeOrder" />
                        <Drop Fn="processChangeOrder" />
                        <%-- <AfterRender Handler="this.plugins[0].dragZone.getDragText = getDragDropText;" />--%>
                        <%--  <Drop Handler="var dropOn = overModel ? ' ' + dropPosition + ' ' + overModel.get('Name') : ' on empty view'; 
                                               Ext.net.Notification.show({title:'Drag from right to left', html:'Dropped ' + data.records[0].get('Name') + dropOn});" />--%>
                    </Listeners>
                </ext:GridView>
            </View>
            <%--<BottomBar>
                        <ext:PagingToolbar ID="PagingToolbar1" runat="server">
                        </ext:PagingToolbar>
                    </BottomBar>--%>
        </ext:GridPanel>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
