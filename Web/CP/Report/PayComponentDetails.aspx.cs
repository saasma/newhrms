﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//using Web.CP.Report.Templates;

using BLL;
using DevExpress.XtraReports.UI;
using Web.ReportDataSetTableAdapters;
using Utils.Calendar;
using BLL.Base;
using BLL.Manager;
using DAL;

namespace Web.CP.Report
{
    public partial class PayComponentDetails : BasePage
    {


        protected void Page_Load(object sender, EventArgs e)
        {
            report.Filter.Employee = false;
            report.Filter.Income = true;
            //if (!IsPostBack || CommonManager.CompanySetting.RemoveStyleWhileExportingReport)
                LoadReport();
        }

        protected void LoadReport()
        {
           
            Report_Pay_ComponentDetailsTableAdapter
                adap = new Report_Pay_ComponentDetailsTableAdapter();


            Report.Templates.Pay.ReportComponentDetails report1 = new Report.Templates.Pay.ReportComponentDetails();

            BLL.BaseBiz.SetConnectionPwd(adap.Connection);
            report1.DataSource = adap.GetData(
                report.Filter.StartDateEng, report.Filter.EndDateEng, SessionManager.CurrentCompanyId,
               report.Filter.IncomeId, report.Filter.BranchId, report.Filter.DepartmentId, report.Filter.DepartmentName);
            report1.DataMember = "Report";


            //if same payroll then hide group header
            //if (report.Filter.StartDate == report.Filter.EndDate)
            //{
            //    //report1.GroupHeader.Visible = false;
            //    report1.labelTitle.Text += DateHelper.GetMonthShortName(report.Filter.StartDate.Month, IsEnglish)
            //                            + "/" + report.Filter.StartDate.Year;
            //}
            //else
            {
                report1.labelTitle.Text += " " +
                    (DateHelper.GetMonthShortName(report.Filter.StartDate.Month, IsEnglish)
                     + "/" + report.Filter.StartDate.Year + " to " +

                     DateHelper.GetMonthShortName(report.Filter.EndDate.Month, IsEnglish)
                     + "/" + report.Filter.EndDate.Year);
            }


            report.DisplayReport(report1);

            if(ReportHelper.IsReportExportState())
            {
                this.report.clearCache = true;
            }
        }
    }
}
