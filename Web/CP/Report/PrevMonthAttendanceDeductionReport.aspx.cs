﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using BLL;
using DevExpress.XtraReports.UI;
using Web.CP.Report.Templates.HR;
using Web.ReportDataSetTableAdapters;
using Utils.Calendar;
using BLL.Base;
using DAL;
using BLL.Manager;
using System.Data;
using DevExpress.XtraPrinting;
using Web.CP.Report.Templates.Pay;

namespace Web.CP.Report
{
    public partial class PrevMonthAttendanceDeductionReport : BasePage
    {
        private int Name_Column_Index = 1;
        private int top = 65;


        protected void LoadReportHandler()
        { }

        protected void Page_Load(object sender, EventArgs e)
        {
            //report.Filter.PayrollFrom = false;
            report.Filter.PayrollTo = false;

            report.Filter.Employee = false;

            report.Filter.CostCode = false;
            report.Filter.Program = false;
            report.Filter.Branch = false;

            report.Filter.Department = false;
            report.Filter.SubDepartment = false;
            report.Filter.MultiSelectionDropDown = false;

            report.Filter.PaySummaryType = false;

            //if (!IsPostBack || CommonManager.CompanySetting.RemoveStyleWhileExportingReport)
            LoadReport();
        }


        private void AddRange(XRControlStyle[] xRControlStyle)
        {
            throw new NotImplementedException();
        }

        private void Event_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (Web.CP.Report.ReportHelper.IsReportExportState())
            {
                ((XRLabel)sender).BackColor = System.Drawing.Color.Transparent;
                ((XRLabel)sender).BorderColor = System.Drawing.Color.Black;
            }

        }

        protected void ClearBorderControlEvent(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ReportHelper.TableCell_BeforePrintEvent(sender, e);
        }
        protected void ClearBorderLessControlEvent(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ReportHelper.TableCell_BeforePrintEvent(sender, e);
        }

        private void CreateUsingLabels(XtraReport report)       
        {
            DataTable dataTable = (report.DataSource as DataSet).Tables[0];
            int firstColumnExtraWidth = 60;

            Color colorHeader = Color.FromArgb(201, 214, 237);
            Color colorEven = Color.FromArgb(239, 243, 250);

            int colCount = dataTable.Columns.Count;
            int colWidth = 80;

            report.PageWidth = colWidth * colCount;
            (report as PaySalarySummary).labelTitle.WidthF = report.PageWidth + 100;
            int currentXLocation = 0;
            int prevWidth = 0;
            int currentWidth = 0;
            // Create header captions
            for (int i = 0; i < colCount; i++)
            {

                XRLabel label = new XRLabel();
                label.BeforePrint += new System.Drawing.Printing.PrintEventHandler(Event_BeforePrint);
                label.TextAlignment = TextAlignment.MiddleLeft;

                ReportHelper.HeaderLabelStyle(label);





                if (i == 0)
                {
                    currentWidth = colWidth / 2; //for SN
                    label.TextAlignment = TextAlignment.MiddleCenter;
                }
                else if (i == 1 || i == 2)
                {
                    currentWidth = 60;//colWidth / 2; //for title

                }
                else if (i == 3)
                {
                    currentWidth = colWidth + firstColumnExtraWidth; //for name

                }
                else if (i >= 4 && i <= 8)
                {
                    currentWidth = colWidth + 20; //first cost code                   
                }
                //else if (i == 4) // for Program
                //{
                //    currentWidth = colWidth + 20;
                //}
                else
                {
                    currentWidth = colWidth;//for other amount columns

                }
                currentXLocation += (prevWidth);
                prevWidth = currentWidth;

                label.Location = new Point(currentXLocation, top);


                label.Text = dataTable.Columns[i].Caption;
                label.Padding = new PaddingInfo(4, 2, 2, 2);

                label.Font = new Font(label.Font.FontFamily, label.Font.Size, FontStyle.Bold);
                label.CanGrow = false;//dont allow to grow as design will be meshed up
                // if (i != Name_Column_Index)
                if (i == 0)
                {
                    label.Borders = DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom;
                    label.Size = new Size(currentWidth, 50);
                }
                else
                {
                    label.Borders = DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom;



                    label.Size = new Size(currentWidth, 50);
                }

                report.Bands[BandKind.PageHeader].Controls.Add(label);
            }


            currentXLocation = 0;
            prevWidth = 0;
            currentWidth = 0;
            // Create data-bound labels with different odd and even backgrounds
            for (int i = 0; i < colCount; i++)
            {
                XRLabel label = new XRLabel();

                label.Text = "-";
                label.BeforePrint += new System.Drawing.Printing.PrintEventHandler(Event_BeforePrint);


                if (i == 0)
                {
                    currentWidth = colWidth / 2;
                    label.TextAlignment = TextAlignment.MiddleCenter;
                }
                else if (i == 1 || i== 2)
                {
                    currentWidth = 60;
                    label.TextAlignment = TextAlignment.MiddleLeft;

                }
                else if (i == 3)
                {
                    currentWidth = colWidth + firstColumnExtraWidth; //for name
                    label.TextAlignment = TextAlignment.MiddleLeft;
                }
                else if (i >= 4 && i <= 8)
                {
                    currentWidth = colWidth + 20; //first cost code
                    label.TextAlignment = TextAlignment.MiddleLeft;
                }
                //else if (i == 4)
                //{
                //    currentWidth = colWidth + 20; //first for program
                //    label.TextAlignment = TextAlignment.MiddleLeft;
                //}
                else
                {
                    currentWidth = colWidth;
                    label.TextAlignment = TextAlignment.MiddleRight;



                    //label.EvaluateBinding += new BindingEventHandler(BindingEventHandler);
                }
                currentXLocation += (prevWidth);
                prevWidth = currentWidth;

                //if (i == Name_Column_Index)
                label.Location = new Point(currentXLocation, 0);
                //else
                //{
                //    label.Location = new Point((colWidth * i) + firstColumnExtraWidth, 0);                   
                //}
                label.Padding = new PaddingInfo(2, 2, 2, 0);
                if (i == Name_Column_Index)
                    label.DataBindings.Add("Text", null, dataTable.Columns[i].ColumnName);
                else
                    label.DataBindings.Add("Text", null, dataTable.Columns[i].ColumnName, "{0:n2}");





                label.CanGrow = false;



                //if (i != Name_Column_Index)
                //{
                // 
                if (i == 0)
                {
                    label.Size = new Size(currentWidth, 20);
                    label.Borders = DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom;
                }

                else
                {
                    label.Borders = DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom;

                    label.Size = new Size(currentWidth, 20);
                }
                //}
                //else
                //{
                //    label.Borders = DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom;
                //    label.TextAlignment = TextAlignment.MiddleLeft;
                //    label.Size = new Size(colWidth + firstColumnExtraWidth, 20);

                //}

                ReportHelper.LabelStyle(label);
                label.Font = new Font(label.Font.FontFamily, label.Font.Size, FontStyle.Regular);

                //label.BackColor = i == 0 ? colorEven : Color.White;
                //label.BorderColor = colorHeader;
                // Place the labels onto a Detail band
                report.Bands[BandKind.Detail].Controls.Add(label);
            }

            currentXLocation = 0;
            prevWidth = 0;
            currentWidth = 0;

            //create footer captions for Total
            for (int i = 0; i < colCount; i++)
            {
                XRLabel label = new XRLabel();
                //set properties for sum/total
                //label.StyleName = "ReportFooter";
                label.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(190)))), ((int)(((byte)(216)))));

                label.BeforePrint += new System.Drawing.Printing.PrintEventHandler(Event_BeforePrint);

                if (i == 0)
                {
                    currentWidth = colWidth / 2;
                }
                else if (i == 1 || i==2)
                {
                    currentWidth = 60;

                }
                else if (i == 3)
                {
                    currentWidth = colWidth + firstColumnExtraWidth; //for name

                }
                else if (i >= 4 && i <= 8)
                {
                    currentWidth = colWidth + 20; //first cost code

                }
                //else if (i == 4)
                //{
                //    currentWidth = colWidth + 20; //Program

                //}
                else
                {
                    currentWidth = colWidth;

                }
                currentXLocation += (prevWidth);
                prevWidth = currentWidth;

                label.Location = new Point(currentXLocation, 0);

                label.Padding = new PaddingInfo(2, 2, 2, 0);




                DevExpress.XtraReports.UI.XRSummary summary = new DevExpress.XtraReports.UI.XRSummary();


                //skip for first two columns
                if (i == 2)
                {
                    label.Text = "Total";
                }
                else if (i == (colCount - 1))
                { }
                else if (i > 8)
                {
                    label.DataBindings.Add("Text", null, dataTable.Columns[i].ColumnName);



                    summary.FormatString = "{0:n2}";

                    summary.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
                    label.Summary = summary;

                }


                label.CanGrow = false;
                //label.EvenStyleName = "EvenStyle";
                //label.OddStyleName = "OddStyle";
                //label.StylePriority.UseBackColor = true;
                //label.StylePriority.UseBorders = true;
                //label.StylePriority.UseBorderColor = true;
                // label.OddStyleName = "OddStyle";
                //label.EvenStyleName = "EvenStyle";
                //if (i != Name_Column_Index)
                //{
                if (i == 0)

                    label.Borders = DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom;

                else
                    label.Borders = DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom; ;

                label.TextAlignment = TextAlignment.MiddleRight;
                label.Size = new Size(currentWidth, 20);
                //}
                //else
                //{
                //    label.Text = "Total";
                //    label.Borders = DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom;
                //    label.TextAlignment = TextAlignment.MiddleLeft;
                //    label.Size = new Size(colWidth + firstColumnExtraWidth, 20);
                //}
                label.Font = new Font(label.Font.FontFamily, label.Font.Size, FontStyle.Bold);

                //label.BackColor = i == 0 ? colorEven : Color.White;
                //label.BorderColor = colorHeader;
                // Place the labels onto a Detail band
                report.Bands[BandKind.ReportFooter].Controls.Add(label);
            }
        }


        public void BindingEventHandler( object sender, BindingEventArgs e)
        {
            if (e.Value == DBNull.Value)// || e.Value.ToString() == "0")
                e.Value = "0.00";

        }

        protected void LoadReport()
        {
            PayrollPeriod payrollPeriod = CommonManager.GetPayrollPeriod(report.Filter.StartDate.Month,
                                                                         report.Filter.StartDate.Year);
            if (payrollPeriod == null)
            {
                return;
            }



            Report.Templates.Pay.PaySalarySummary mainReport = new Web.CP.Report.Templates.Pay.PaySalarySummary();
            mainReport.Name = "Attendance Deduction " + payrollPeriod.Name;
            mainReport.labelTitle.Text = mainReport.Name;
            //mainReport.labelTitle.Text += string.Format(" {0} ({1} {2})", payrollPeriod.Name,
            //                                            DateHelper.GetMonthsForOtherPayrollPeriod(payrollPeriod.Month,
            //                                                                                      IsEnglish)
            //                                                                                      , DateManager.GetOtherYearForPayrollPeriod(payrollPeriod, IsEnglish));
            //XlsxExportOptions o;
            //mainReport.ExportToXlsx(

            mainReport.Bands[BandKind.ReportFooter].Visible = false;

            //List<PDeduction> loanRepaymentDeductions = PayManager.GetDeductionListByCompany(SessionManager.CurrentCompanyId)
            //    .Where(x => x.Calculation == DeductionCalculation.LOAN_Repayment).ToList();

            List<DAL.Report_Pay_PrevMonthAttendanceDeductionResult> data = ReportManager.GetAttendanceSummary
                (payrollPeriod.Month, payrollPeriod.Year.Value);

            List<CalcGetHeaderListResult> headerList =
                CalculationManager.GetPaySummaryReportHeaderList(SessionManager.CurrentCompanyId,
                                                                 payrollPeriod.PayrollPeriodId)
                .Where(x => x.Type == 1 || x.Type == 4 || x.Type == 10).ToList();

            headerList = CalculationValue.SortHeaders(headerList, PayManager.GetIncomeOrderValues(), PayManager.GetDeductionOrderValues());

            //if (!CommonManager.CalculationConstant.CompanyHasHourlyLeave.Value)
            //{
            //    CalcGetHeaderListResult headerPDays = new CalcGetHeaderListResult();
            //    headerPDays.HeaderName = "Prev Month Deduct Days";
            //    headerPDays.SourceId = (int)CalculationColumnType.ReportPDays;
            //    headerPDays.Type = (int)CalculationColumnType.ReportPDays;
            //    headerList.Insert(0, headerPDays);
            //}

            DataTable dataTable = CreateDataTable(data, headerList);
            DataSet dset = new DataSet();
            dset.Tables.Add(dataTable);
            mainReport.DataSource = dset;
            mainReport.DataMember = "Report";

            CreateUsingLabels(mainReport);
            if (data.Count > 0)
                report.DisplayReport(mainReport);

            CurrencyRate rate = CommonManager.GetCurrencyRateForSalarySavedPayroll(payrollPeriod.PayrollPeriodId);
            // Show D2 Fixed/Current Rate Amount
            if (CommonManager.CompanySetting.IsD2 && rate != null && CommonManager.IsD2MonthInDollar(payrollPeriod.Year.Value,payrollPeriod.Month ))
            {
                DevExpress.XtraReports.UI.XRLabel lblCurrency = new XRLabel();
                lblCurrency.WidthF = 400;
                lblCurrency.LocationF = new PointF(0, 30);
                mainReport.Bands[BandKind.PageHeader].Controls.Add(lblCurrency);

                lblCurrency.Text = string.Format(Resources.Messages.D2FixedCurrentRateText,
                       rate.FixedRateDollar, rate.CurrentRateDollar);

                lblCurrency.LeftF = mainReport.labelTitle.LeftF;
                lblCurrency.WidthF = mainReport.labelTitle.WidthF;
            }


            if (ReportHelper.IsReportExportState())
            {
                this.report.clearCache = true;
            }

        }

        private DataTable CreateDataTable(List<DAL.Report_Pay_PrevMonthAttendanceDeductionResult> data, List<CalcGetHeaderListResult> headerList
            )
        {
            DataTable dataTable = new DataTable();
            dataTable.TableName = "Report";

            dataTable.Columns.Add("SN", typeof(string));            
            dataTable.Columns.Add("I No", typeof(string));
            dataTable.Columns.Add("Title", typeof(string));
            dataTable.Columns.Add("Name", typeof(string));
            dataTable.Columns.Add("Branch", typeof(string));
            dataTable.Columns.Add("Department", typeof(string));
            dataTable.Columns.Add("Designation", typeof(string));
            dataTable.Columns.Add("PM Deduction", typeof(string));

            // Create table cells, fill the header cells with text, bind the cells to data
            for (int i = 0; i < headerList.Count; i++)
            {

                DataColumn column = new DataColumn(headerList[i].Type + ":" + headerList[i].SourceId, typeof(decimal));
                column.Caption = headerList[i].HeaderName;

                if (headerList[i].SourceId.Value == 4)
                    column.Caption += " Income";

                if (headerList[i].SourceId.Value == 10)
                    column.Caption += " Deduction";

                dataTable.Columns.Add(column);

            }

            //dataTable.Columns.Add("Account No", typeof(string));
          

            //add rows
            decimal? value;
            foreach (Report_Pay_PrevMonthAttendanceDeductionResult row in data)
            {
                List<object> list = new List<object>();
                list.Add(row.SN);                
                list.Add(row.IdCardNo);
                list.Add(row.Title);
                list.Add(row.Name);
                list.Add(row.Branch);
                list.Add(row.Department);
                list.Add(row.Designation);
                list.Add(row.PrevMontDeductDays == null ? 0 : row.PrevMontDeductDays);

                for (int i = 0; i < headerList.Count; i++)
                {
                    value = row.GetCellValue(headerList[i].Type.Value, headerList[i].SourceId.Value, 2, null);
                    list.Add(value);
                }

                //list.Add(row.AccountNO);
               
                dataTable.Rows.Add(list.ToArray());
            }

            return dataTable;

        }
    }
}
