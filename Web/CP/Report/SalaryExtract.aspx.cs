﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using BLL;
using DevExpress.XtraReports.UI;
using Web.CP.Report.Templates.HR;
using Web.ReportDataSetTableAdapters;
using Utils.Calendar;
using BLL.Base;
using DAL;
using BLL.Manager;
using System.Data;

namespace Web.CP.Report
{
    public partial class SalaryExtract : BasePage
    {


        protected  void LoadReportHandler()
        {}
      
        protected void Page_Load(object sender, EventArgs e)
        {
            //report.Filter.PayrollFrom = false;
            report.Filter.PayrollTo = false;
            report.Filter.Branch = true;
            report.Filter.Department = true;
            report.Filter.Employee = false;

           // if (!IsPostBack || CommonManager.CompanySetting.RemoveStyleWhileExportingReport)
                LoadReport();
        }

        protected void LoadReport()
        {
            PayrollPeriod payrollPeriod = CommonManager.GetPayrollPeriod(report.Filter.StartDate.Month,
                                                                        report.Filter.StartDate.Year);
            if (payrollPeriod == null)
                return;

            Report_Pay_GetBankDetailsTableAdapter adapBank = new Report_Pay_GetBankDetailsTableAdapter();
            Report_Pay_SalaryExtractTableAdapter
                adap = new Report_Pay_SalaryExtractTableAdapter();

            Report.Templates.HR.ReportPaySalaryExtract mainReport = new ReportPaySalaryExtract();

            mainReport.lblName.Text = SessionManager.CurrentCompany.Name;
          

            ReportPaySalaryExtractSubReport subReportIncome = new ReportPaySalaryExtractSubReport();
            ReportPaySalaryExtractSubReport subReportDeduction = new ReportPaySalaryExtractSubReport();
            ReportPaySalaryExtractBankDetails subReportBank = new ReportPaySalaryExtractBankDetails();
            

            BLL.BaseBiz.SetConnectionPwd(adap.Connection);
            ReportDataSet.Report_Pay_SalaryExtractDataTable incomeTable =
                adap.GetData(payrollPeriod.Month, payrollPeriod.Year,
                                                                      SessionManager.CurrentCompanyId, true,report.Filter.BranchId,report.Filter.DepartmentId);

            BLL.BaseBiz.SetConnectionPwd(adap.Connection);
            ReportDataSet.Report_Pay_SalaryExtractDataTable deductionTable =
                adap.GetData(payrollPeriod.Month, payrollPeriod.Year,
                                                      SessionManager.CurrentCompanyId, false, report.Filter.BranchId, report.Filter.DepartmentId);

            BLL.BaseBiz.SetConnectionPwd(adap.Connection);
            ReportDataSet.Report_Pay_GetBankDetailsDataTable bankTable =
                adapBank.GetData(payrollPeriod.Month, payrollPeriod.Year,
                                                      SessionManager.CurrentCompanyId, report.Filter.BranchId, report.Filter.DepartmentId);

            //set no. of rows in each table equal for the design
            ReportHelper.SetEqualRows(incomeTable, deductionTable);


            subReportIncome.DataSource = incomeTable;
            subReportDeduction.DataSource = deductionTable;
            subReportBank.DataSource = bankTable; 

            subReportIncome.DataMember = "Report1";
            subReportDeduction.DataMember = "Report2";
            subReportBank.DataMember = "Report3";

            subReportIncome.xrLabelTitle1.Text = "Earnings";
            subReportDeduction.xrLabelTitle1.Text = "Deductions";
            subReportIncome.xrLabelSubTitle.Text = "Earning Head";
            subReportDeduction.xrLabelSubTitle.Text = "Deduction Head";
            subReportIncome.lblGross.Text = "Gross Income";
            subReportDeduction.lblGross.Text = "Total Deduction";

            mainReport.xrSubreportIncome.ReportSource = subReportIncome;
            mainReport.xrSubreportDeduction.ReportSource = subReportDeduction;
            mainReport.xrSubreportBankDetails.ReportSource = subReportBank;

            if (bankTable.Rows.Count > 0)
                mainReport.xrSubreportBankDetails.Visible = true;

            //mainReport.labelTitle.Text
              ///  = string.Format(Resources.Messages.ReportPaySalaryExtractTitle, payrollPeriod.Name);

            mainReport.labelTitle.Text += string.Format(" {0} ({1} {2})", payrollPeriod.Name,
                                                        DateHelper.GetMonthsForOtherPayrollPeriod(payrollPeriod.Month,
                                                                                                  IsEnglish)
                                                                                                  , DateManager.GetOtherYearForPayrollPeriod(payrollPeriod, IsEnglish));

            // Cash Paid details
            decimal? cashPaid = CalculationManager.GetPayrollCashPaidSum(payrollPeriod.Month, payrollPeriod.Year.Value, report.Filter.BranchId, report.Filter.DepartmentId);
            if (Convert.ToDecimal(cashPaid) == 0)
            {
                mainReport.cashHeader.Visible = false;
                mainReport.cashLeft.Visible = false;
                mainReport.rightCashDetails.Visible = false;
            }
            else
            {
                mainReport.rightCashDetails.Text = GetCurrency(cashPaid);
            }
           

            report.DisplayReport(mainReport);
            if (incomeTable.Rows.Count > 0 && deductionTable.Rows.Count > 0)
            {
                decimal netTotal = (ReportHelper.GetTotal(incomeTable, "Amount") - ReportHelper.GetTotal(deductionTable, "Amount"));
                //set Net Total in main report
                mainReport.xrLabelNetTotal.Text = netTotal.ToString("N2");
            }

              if (ReportHelper.IsReportExportState())
            {
                this.report.clearCache = true;
               
            }

              
        }
    }
}
