﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using BLL;
using DevExpress.XtraReports.UI;
using Web.CP.Report.Templates.HR;
using Web.ReportDataSetTableAdapters;
using Utils.Calendar;
using BLL.Base;
using DAL;
using BLL.Manager;
using System.Data;
using System.IO;

namespace Web.CP.Report
{
    public partial class PayNIBLPF : BasePage
    {
        protected  void LoadReportHandler()
        {        
        }
      
        protected void Page_Load(object sender, EventArgs e)
        {
            //report.Filter.PayrollFrom = false;
            report.Filter.PayrollTo = false;
          //  report.Filter.Branch = false;
            report.Filter.Department = false;
            report.Filter.Employee = false;
            report.Filter.CostCode = true;
            report.Filter.Program = true;

            report.Filter.Department = false;
            report.Filter.MultiSelectionDropDown = true;
            report.Filter.Bank = true;
            //if (!IsPostBack || CommonManager.CompanySetting.RemoveStyleWhileExportingReport)
                LoadReport();

        }

     
        protected void LoadReport()
        {
            PayrollPeriod payrollPeriod = CommonManager.GetPayrollPeriod(report.Filter.StartDate.Month,
                                                                         report.Filter.StartDate.Year);
            if( payrollPeriod ==null)
                return;





            Report_Pay_NIBLPFTableAdapter mainAdapter = new Report_Pay_NIBLPFTableAdapter();
            BLL.BaseBiz.SetConnectionPwd(mainAdapter.Connection);
            ReportDataSet.Report_Pay_NIBLPFDataTable mainTable =
                mainAdapter.GetData(payrollPeriod.Month, payrollPeriod.Year, SessionManager.CurrentCompanyId,
                report.Filter.BranchId, report.Filter.DepartmentId);


            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.Quest)
            {
                Report.Templates.Pay.ReportOtherPFForQuest mainReport = new Report.Templates.Pay.ReportOtherPFForQuest();

                CustomDate todayDate = CustomDate.GetTodayDate(IsEnglish);
                CustomDate date =CustomDate.GetCustomDateFromString(payrollPeriod.StartDate,IsEnglish);

                // replaec date
                mainReport.lblDate.Text = mainReport.lblDate.Text.Replace("#Date#", todayDate.Year + "." + todayDate.Month + ".");
                // replace subject
                mainReport.lblSubject.Text = mainReport.lblSubject.Text.Replace("#Month#",date.Year + " " + 
                    DateHelper.GetMonthName(date.Month,IsEnglish));


                // replace amount
                double total = mainTable.Sum(x=>x.Amount);

                mainReport.lblDetails.Text = mainReport.lblDetails.Text
                        .Replace("#Date#", todayDate.Year + "." + todayDate.Month + ".")
                        .Replace("#Amount#", GetCurrency(total));

                mainReport.DataSource = mainTable;
                mainReport.DataMember = "Report";

                report.DisplayReport(mainReport);
            }
            else
            {

                Report.Templates.Pay.ReportPFNIBL mainReport = new Report.Templates.Pay.ReportPFNIBL();
                mainReport.labelBranchName.Text = report.Filter.BranchName + " PF Contribution Report";

                mainReport.DataSource = mainTable;
                mainReport.DataMember = "Report";

                report.DisplayReport(mainReport);
            }

            if(ReportHelper.IsReportExportState())
            {
                this.report.clearCache = true;
            }
        }
    }
}
