﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReportPayBranchDepartmentWise.aspx.cs"
    MasterPageFile="~/Master/NewDetails.Master" Inherits="Web.Report.ReportPayBranchDepartmentWise"
    Title="Branch or Departmentwise Cost to the Company" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <ext:ResourcePlaceHolder ID="ResourcePlaceHolder1" runat="server" Mode="Script" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <script>
      var storeDynamic = null;
      Ext.onReady(
        function () {        
            storeDynamic = <%= storeDynamic.ClientID %>;
        }
    ); 


//    if(storeDynamic!=null)
//    storeDynamic.currentPage =1;

//        function searchList()
//        {
//            PagingToolbar_ListItems.doRefresh();
//        }

//        function searchListDynamic()
//        {
//            PagingToolbar1.doRefresh();
//        }
        
              

//    function GetServiceList() 
//    {
//    searchList();
//    }
    
    </script>
    <div id="1">
        <ext:Hidden ID="Hidden1" runat="server" />
        <ext:Hidden ID="HiddenFilterHeader" runat="server" />
        <ext:Hidden ID="HiddenCurrentPage" runat="server" />
        <ext:Hidden ID="HiddenSelectedFields" runat="server" />
        <ext:Hidden ID="Hidden_ClassOrGroupID" runat="server" />
        <div class="pageheader">
            <div class="media">
                <div class="media-body">
                    <h4>
                        Branch Department Wise Cost Allocation
                    </h4>
                </div>
            </div>
        </div>
        <div class="contentpanel">
            <div id="container" style="width: inherit;">
                <div id="fullBlock" class="account_rev">
                    <div class="alert alert-info">
                        <table class="fieldTable">
                            <tr>
                                <td>
                                    <pr:CalendarExtControl Width="150px" FieldLabel="From" ID="txtFromDate" runat="server"
                                        LabelAlign="Top" LabelSeparator="">
                                    </pr:CalendarExtControl>
                                </td>
                                <td>
                                    <pr:CalendarExtControl Width="150px" FieldLabel="To" ID="txtToDate" runat="server"
                                        LabelAlign="Top" LabelSeparator="">
                                    </pr:CalendarExtControl>
                                </td>
                                <td style="">
                                    <ext:ComboBox ID="cmbShow" Width="120" runat="server" ValueField="FieldServerMaping"
                                        LabelWidth="70" DisplayField="FieldName" FieldLabel="Show" LabelAlign="Top" LabelSeparator=""
                                        QueryMode="Local" ForceSelection="true">
                                        <Items>
                                            <ext:ListItem Text="Department" Value="1" />
                                            <ext:ListItem Text="Branch" Value="2" />
                                        </Items>
                                        <SelectedItems>
                                            <ext:ListItem Index="0">
                                            </ext:ListItem>
                                        </SelectedItems>
                                    </ext:ComboBox>
                                </td>
                                <td>
                                    <ext:ComboBox ID="cmbBranch" Width="180px" runat="server" ValueField="BranchId" DisplayField="Name"
                                        FieldLabel="Branch" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                                        QueryMode="Local">
                                        <Store>
                                            <ext:Store ID="Store5" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model5" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="BranchId" />
                                                            <ext:ModelField Name="Name" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <Triggers>
                                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                        </Triggers>
                                        <Listeners>
                                            <Select Handler="this.getTrigger(0).show();" />
                                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                            <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                                        </Listeners>
                                    </ext:ComboBox>
                                </td>
                                <td style="width: 200px">
                                    <ext:ComboBox ID="cmbDepartment" Width="180px" runat="server" ValueField="DepartmentId"
                                        DisplayField="Name" FieldLabel="Department" LabelAlign="top" LabelSeparator=""
                                        ForceSelection="true" QueryMode="Local">
                                        <Store>
                                            <ext:Store ID="Store6" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model6" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="DepartmentId" />
                                                            <ext:ModelField Name="Name" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <Triggers>
                                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                        </Triggers>
                                        <Listeners>
                                            <Select Handler="this.getTrigger(0).show();" />
                                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                            <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                                        </Listeners>
                                    </ext:ComboBox>
                                </td>
                                <td style="padding-top: 25px; width: 100px">
                                    <ext:Button Width="70" ID="btnSearch" runat="server" Text="Search" Height="30">
                                        <%-- <Listeners>
                                                            <Click Handler="GetServiceList()" />
                                                        </Listeners>--%>
                                        <DirectEvents>
                                            <Click OnEvent="btnSearch_Click">
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </td>
                                <td style="padding-top: 25px">
                                    <ext:Button ID="Button1" runat="server" Text="Export" Cls="updatebtn" Height="30">
                                        <Menu>
                                            <ext:Menu ID="Menu2" runat="server" ShowSeparator="false" Cls="dropdownbtn">
                                                <Items>
                                                    <ext:MenuItem AutoPostBack="true" runat="server" OnClick="btnDynamicExcelPrint_Click"
                                                        Text="Excel" ID="btnExcelPrint">
                                                    </ext:MenuItem>
                                                </Items>
                                            </ext:Menu>
                                        </Menu>
                                    </ext:Button>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="padding-top: 20px; border: none;">
                    </div>
                    <div class="">
                        <ext:GridPanel ID="GridPanel1" runat="server" Border="false" MinHeight="500" AutoScroll="true"
                            Hidden="true">
                            <Store>
                                <ext:Store PageSize="20" runat="server" ID="storeDynamic" RemoteSort="false" OnReadData="Store_ReadDataDynamic"
                                    AutoLoad="false">
                                    <Model>
                                        <ext:Model ID="DynamicModel" Name="Test" runat="server" />
                                    </Model>
                                    <Proxy>
                                        <ext:PageProxy>
                                        </ext:PageProxy>
                                    </Proxy>
                                    <Parameters>
                                        <ext:StoreParameter Name="SelectedFieldsLineJson" Value="Ext.encode(#{GridPanel1}.getRowsValues({selectedOnly : false}))"
                                            Mode="Raw" />
                                    </Parameters>
                                </ext:Store>
                            </Store>
                            <ColumnModel>
                                <Columns>
                                    <ext:Column runat="server" DataIndex="Test" Hidden="true" Text='Test' Locked="true"
                                        Border="false" />
                                </Columns>
                            </ColumnModel>
                            <%--  <BottomBar>
                                        <ext:PagingToolbar ID="PagingToolbar1" runat="server" StoreID="storeDynamic" DisplayInfo="true"
                                            Hidden="true">
                                            <Items>
                                                <ext:Label ID="Label1" runat="server" Text="Page size:" />
                                                <ext:ToolbarSpacer ID="ToolbarSpacer1" runat="server" Width="10" />
                                                <ext:ComboBox ID="ComboBox1" runat="server" Width="80" SelectOnFocus="true" Selectable="true"
                                                    ValueField="Value" DisplayField="Text">
                                                    <Listeners>
                                                        <Select Handler="#{storeDynamic}.pageSize = this.getValue();#{PagingToolbar1}.moveFirst(); searchListDynamic()" />
                                                    </Listeners>
                                                    <Items>
                                                        <ext:ListItem Value="20" Text="20" />
                                                        <ext:ListItem Value="50" Text="50" />
                                                        <ext:ListItem Value="100" Text="100" />
                                                    </Items>
                                                    <SelectedItems>
                                                        <ext:ListItem Index="0">
                                                        </ext:ListItem>
                                                    </SelectedItems>
                                                </ext:ComboBox>
                                            </Items>
                                        </ext:PagingToolbar>
                                    </BottomBar>--%>
                        </ext:GridPanel>
                    </div>
                </div>
            </div>
</asp:Content>
