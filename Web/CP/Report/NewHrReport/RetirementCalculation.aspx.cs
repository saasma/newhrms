﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Ext.Net;
using DAL;
using BLL;
using BLL.Base;
using BLL.Manager;
using Utils.Helper;
using Utils.Calendar;
//using Web.Report.Templates;
using System.IO;
using DevExpress.XtraPrinting;
using BLL.BO;
using System.Xml;
using System.Xml.Xsl;
using Newtonsoft.Json.Linq;
using System.Collections;
using System.Reflection;
using System.Data;
using Bll;
using Utils.Web;
using Newtonsoft.Json;

namespace Web.Report
{

    public partial class RetirementCalculation : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!X.IsAjaxRequest && !IsPostBack)
            {
                LoadHeader();
                BindIncomeList();
                
            }
        }

        
        void BindIncomeList()
        {
           
        }

        protected void btnPDFPrint_Click(object sender, EventArgs e)
        {
            
        }


        protected Field OnCreateFilterableField(object sender, ColumnBase column, Field defaultField)
        {
            
                ((TextField)defaultField).Icon = Icon.Magnifier;
                return defaultField;
        }

        private DataTable CreateDataTable(List<object> data, List<CalcGetHeaderListResult> headerList)
        {
            DataTable dataTable = new DataTable();
            dataTable.TableName = "Report";

            dataTable.Columns.Add("SN", typeof(string));
            dataTable.Columns.Add("EIN", typeof(string));
            dataTable.Columns.Add("AccountNo", typeof(string));
            dataTable.Columns.Add("Name", typeof(string));

            dataTable.Columns.Add("Branch", typeof(string));
            dataTable.Columns.Add("Department", typeof(string));
            dataTable.Columns.Add("Position", typeof(string));
            dataTable.Columns.Add("Designation", typeof(string));

            dataTable.Columns.Add("Join Date", typeof(string));
            dataTable.Columns.Add("Join Date Eng", typeof(string));
            dataTable.Columns.Add("Retirement Date", typeof(string));
            dataTable.Columns.Add("Retirement Date Eng", typeof(string));
            dataTable.Columns.Add("Retired Month", typeof(string));
            dataTable.Columns.Add("Retiring Month Days", typeof(string));

            

            // Create table cells, fill the header cells with text, bind the cells to data
            for (int i = 0; i < headerList.Count; i++)
            {

                DataColumn column = new DataColumn(headerList[i].RetirementTypeSourceId, typeof(decimal));
                column.Caption = headerList[i].HeaderName;

                dataTable.Columns.Add(column);

            }

         
            //add rows
            object value;
            foreach (object[] row in data)
            {
                List<object> list = new List<object>();
                list.Add(row[0]);
                list.Add(row[1]);
                list.Add(row[2]);
                list.Add(row[3]);

                list.Add(row[4]);
                list.Add(row[5]);
                list.Add(row[6]);
                list.Add(row[7]);

                list.Add(row[8]);
                list.Add(row[9]);
                list.Add(row[10]);
                list.Add(row[11]);

                list.Add(row[12]);
                list.Add(row[13]);

                //int index = 14;
                for (int i = 0; i < headerList.Count; i++)
                {
                    value = row[i + 14];
                    list.Add(value);
                }

               
                dataTable.Rows.Add(list.ToArray());
            }

            return dataTable;

        }

        

        protected void btnDynamicExcelPrint_Click(object sender, EventArgs e)
        {


            DateTime? start = null;
            DateTime? end = null;

            if (!string.IsNullOrEmpty(txtFromDate.Text.Trim()))
                start = GetEngDate(txtFromDate.Text);

            if (!string.IsNullOrEmpty(txtToDate.Text.Trim()))
                end = GetEngDate(txtToDate.Text);

            int? empId = null;

            if (cmbEmpSearch.SelectedItem != null && cmbEmpSearch.SelectedItem.Value != null)
                empId = int.Parse(cmbEmpSearch.SelectedItem.Value);



            List<Object> objList = new List<object>();



            List<object> _newObjList = new List<object>();

            List<CalcGetHeaderListResult> headerList = ReportManager.GetRetirementHeaderList();

            var empList = BLL.BaseBiz.PayrollDataContext.GetRetiredSettlementList(start, end, empId, 0, 999999).ToList();




            List<GetRetiredSettlementListDetailsResult> dataList = BLL.BaseBiz.PayrollDataContext.GetRetiredSettlementListDetails(
                String.Join(",", empList.Select(x => x.EmployeeId.ToString()).ToArray())
                ).ToList();




            decimal total = 0;
            decimal grossIncome = 0;
            decimal deductionTotal = 0;
            foreach (var emp in empList)
            {
                total = 0;
                grossIncome = 0;
                deductionTotal = 0;

                object[] row = new object[headerList.Count + 14];
                row[0] = emp.SN;
                row[1] = emp.EmployeeId;
                row[2] = emp.AccountNo;
                row[3] = emp.Name;
                row[4] = emp.Branch;
                row[5] = emp.Department;
                row[6] = emp.LevelPosition;
                row[7] = emp.Designation;
                row[8] = " " + emp.JoinDate;
                row[9] = emp.JoinDateEng.Value.ToString("dd-MMM-yyyy");
                row[10] =" " +  emp.RetirementDate;
                row[11] = emp.RetirementDateEng == null ? "" : emp.RetirementDateEng.Value.ToString("dd-MMM-yyyy");
                row[12] = emp.RetiredMonth;
                row[13] = emp.RetiringMonthDays;

                int index = 14;

                foreach (CalcGetHeaderListResult header in headerList)
                {
                    GetRetiredSettlementListDetailsResult dataRow = dataList.FirstOrDefault(x => x.EmployeeId == emp.EmployeeId
                        && x.Type == header.Type && x.SourceId == header.SourceId
                        && x.PayrollPeriodId == emp.PayrollPeriodId
                        && x.IsRetirement == (header.IsRetirement == true ? 1 : 0));

                    row[index] = dataRow == null ? 0 : Convert.ToDecimal(GetCurrency(dataRow.Amount));


                    if (header.Type == (int)CalculationColumnType.IncomeGross)
                    {
                        row[index] = total;

                        grossIncome = total; 
                        total = 0;
                    }
                    else if (header.Type == (int)CalculationColumnType.DeductionTotal)
                    {
                        row[index] = total;

                        deductionTotal = total; total = 0;
                    }
                    else if (header.Type == (int)CalculationColumnType.NetSalary)
                    {
                        row[index] = grossIncome - deductionTotal;
                    }
                    else
                    {

                        total += Convert.ToDecimal(row[index]);
                    }

                    index++;

                    
                }

                _newObjList.Add(row);
            }



            DataTable _Data = CreateDataTable(_newObjList, headerList);
            ExcelHelper.ExportDynamicDtToExcel(_Data,"Retirement Settlement List.xls");

        }

        public void LoadHeader()
        {
            List<CalcGetHeaderListResult> headerList = ReportManager.GetRetirementHeaderList();

            foreach (CalcGetHeaderListResult item in headerList)
            {
                DynamicModel.Fields.Add(new ModelField { Name = item.RetirementTypeSourceId, Type = ModelFieldType.Float });

                ColumnBase newCol = new Column();
                newCol.Sortable = false;
                newCol.MenuDisabled = true;
                newCol.Text = item.HeaderName;
                newCol.Align = Alignment.Right;
                newCol.DataIndex = item.RetirementTypeSourceId;
                newCol.Width = new Unit(120);
                newCol.Renderer = new Renderer("getFormattedAmount1");

                

                gridList.ColumnModel.Columns.Add(newCol);
            }



        }

        protected void Store_ReadDataDynamic(object sender, StoreReadDataEventArgs e)
        {


            
             DateTime? start = null;
             DateTime? end = null;

             if (!string.IsNullOrEmpty(txtFromDate.Text.Trim()))
                 start = GetEngDate(txtFromDate.Text);

             if (!string.IsNullOrEmpty(txtToDate.Text.Trim()))
                 end = GetEngDate(txtToDate.Text);

             int? empId = null;

             if (cmbEmpSearch.SelectedItem != null && cmbEmpSearch.SelectedItem.Value != null)
                 empId = int.Parse(cmbEmpSearch.SelectedItem.Value);


        
            List<Object> objList = new List<object>();
            

            
            List<object> _newObjList = new List<object>();

            List<CalcGetHeaderListResult> headerList = ReportManager.GetRetirementHeaderList();

            var empList = BLL.BaseBiz.PayrollDataContext.GetRetiredSettlementList(start, end, empId, e.Start, e.Limit).ToList();
                 

           

            List<GetRetiredSettlementListDetailsResult> dataList = BLL.BaseBiz.PayrollDataContext.GetRetiredSettlementListDetails(
                String.Join(",", empList.Select(x => x.EmployeeId.ToString()).ToArray())
                ).ToList();



            decimal total = 0;
            decimal grossIncome = 0;
            decimal deductionTotal = 0;
            foreach (var emp in empList)
            {
                total = 0;
                grossIncome = 0;
                deductionTotal = 0;

                object[] row = new object[headerList.Count + 15];
                row[0] = emp.SN;
                row[1] = emp.EmployeeId;
                row[2] = emp.AccountNo;
                row[3] = emp.Name;
                row[4] = emp.Branch;
                row[5] = emp.Department;
                row[6] = emp.LevelPosition;
                row[7] = emp.Designation;
                row[8] = emp.JoinDate;
                row[9] = emp.JoinDateEng;
                row[10] = emp.RetirementDate;
                row[11] = emp.RetirementDateEng;
                row[12] = emp.RetiredMonth;
                row[13] = emp.RetiringMonthDays;
                row[14] = emp.PayrollPeriodId;
                int index = 15;

                foreach (CalcGetHeaderListResult header in headerList)
                {
                    GetRetiredSettlementListDetailsResult dataRow = dataList.FirstOrDefault(x => x.EmployeeId == emp.EmployeeId
                        && x.Type == header.Type && x.SourceId == header.SourceId
                        && x.PayrollPeriodId == emp.PayrollPeriodId
                        && x.IsRetirement == (header.IsRetirement == true ? 1 : 0));

                    row[index] = dataRow == null ? 0 : dataRow.Amount;


                    if (header.Type == (int)CalculationColumnType.IncomeGross)
                    {
                        row[index] = total;
                        grossIncome = total;
                        total = 0;
                    }
                    else if (header.Type == (int)CalculationColumnType.DeductionTotal)
                    {
                        row[index] = total;
                        deductionTotal = total;
                        total = 0;
                    }
                    else if (header.Type == (int)CalculationColumnType.NetSalary)
                    {
                        row[index] = grossIncome - deductionTotal;
                    }
                    else
                    {

                        total += Convert.ToDecimal(row[index]);
                    }
                    index++;


                }

                _newObjList.Add(row);
            }





            gridList.GetStore().DataSource = _newObjList;
            storeDynamic.DataBind();

            if (empList.Count > 0)
                e.Total = empList[0].TotalRows.Value;
            else
                e.Total = 0;
        }


        private void GetDynamicColsvalue(string KeyValue)
        {

           string EmployeeID = KeyValue;

        }

        private void AddField(ModelField field)
        {
            if (X.IsAjaxRequest)
            {
                this.storeDynamic.AddField(field);
            }
            else
            {
                this.storeDynamic.Model[0].Fields.Add(field);
            }
        }

       
        //protected void cmbBranch_Change(object sender, DirectEventArgs e)
        //{
        //    cmbDepartment.Store[0].DataSource = DepartmentManager.GetAllDepartmentsByBranch(int.Parse(cmbBranch.SelectedItem.Value));
        //    cmbDepartment.Store[0].DataBind();

        //}


        protected void btnSearch_Click(object sender, DirectEventArgs e)
        {


            if (string.IsNullOrEmpty(txtFromDate.Text))
            {
                NewMessage.ShowNormalMessage("From is required.");
                return;
            }

            if (string.IsNullOrEmpty(txtToDate.Text))
            {
                NewMessage.ShowNormalMessage("To is required.");
                return;
            }

            gridList.GetStore().Reload();
        }

       

        #region "ABSTRACT-METHOD"


     
        #endregion
    }
}
