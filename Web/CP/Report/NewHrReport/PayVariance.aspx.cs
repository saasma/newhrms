﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Ext.Net;
using DAL;
using BLL;
using BLL.Base;
using BLL.Manager;
using Utils.Helper;
using Utils.Calendar;
//using Web.Report.Templates;
using System.IO;
using DevExpress.XtraPrinting;
using BLL.BO;
using System.Xml;
using System.Xml.Xsl;
using Newtonsoft.Json.Linq;
using System.Collections;
using System.Reflection;
using System.Data;
using Bll;
using Utils.Web;
using Newtonsoft.Json;

namespace Web.Report
{

    public partial class PayVariance : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!X.IsAjaxRequest && !IsPostBack)
            {
                BindIncomeList();
                CustomDate date = CustomDate.GetTodayDate(IsEnglish).GetFirstDateOfThisMonth();
                CustomDate date2 = CustomDate.GetTodayDate(IsEnglish).GetLastDateOfThisMonth();
                txtFromDate.Text = date.ToString();
                txtToDate.Text = date2.ToString();
                //btnSearch_Click(null, null);
            }
        }

        void Print(bool isPDF)
        {
           
        }

        void BindIncomeList()
        {
            List<Variance_GetAllPayVarianceReportHeaderResult> headerList = ReportManager.GetALLVarianceHeaderList();
            cmbShow.GetStore().DataSource = headerList;
            cmbShow.GetStore().DataBind();
        }

        protected void btnPDFPrint_Click(object sender, EventArgs e)
        {
            Print(true);
        }


        protected Field OnCreateFilterableField(object sender, ColumnBase column, Field defaultField)
        {
            
                ((TextField)defaultField).Icon = Icon.Magnifier;
                return defaultField;
        }

        private DataTable GetDataTable(List<object> _data, List<DynamicReportFieldsBO> Columns, int PayrollPeriodId, int PayrollPeriodIdEnd)
        {
            DataTable table = new DataTable();

             string OldGroupHeaderName = string.Empty;
            foreach (DynamicReportFieldsBO _fields in Columns)
            {

                //DataColumn column = new DataColumn(headerList[i].Type + ":" + headerList[i].SourceId, typeof(decimal));
                //column.Caption = headerList[i].HeaderName;

                DataColumn colString = new DataColumn();
                colString.AllowDBNull = true;
                colString.ColumnName = _fields.ServerMappingFields;

                List<Variance_GetAllPayVarianceReportHeaderResult> headerList = ReportManager.GetALLVarianceHeaderList().Where(x=>x.Type == _fields.Type 
                    && x.SourceId == _fields.SourceId).ToList();
                string GroupHeaderName = string.Empty;
               
                if (headerList.Any())
                {
                    GroupHeaderName = headerList[0].HeaderName;
                    OldGroupHeaderName = GroupHeaderName;
                }

                if (!string.IsNullOrEmpty(GroupHeaderName) || !string.IsNullOrEmpty(OldGroupHeaderName))
                colString.Caption = (string.IsNullOrEmpty(GroupHeaderName)?OldGroupHeaderName:GroupHeaderName) + ":" +_fields.Caption;
                else
                    colString.Caption = _fields.Caption;

                if (_fields.DataType.ToLower() == "string")
                    colString.DataType = typeof(string);
                if (_fields.DataType.ToLower() == "float")
                    colString.DataType = typeof(float);
                if (_fields.DataType.ToLower() == "date")
                    colString.DataType = typeof(DateTime);
                table.Columns.Add(colString);
            }

            foreach (object[] obj in _data)
            {
                table.Rows.Add(obj);
            }


            List<Variance_GetAllPayVarianceReportHeaderResult> _IncomeList1 = ReportManager.GetALLVarianceHeaderList();
            List<Variance_GetAllPayVarianceReportHeaderResult> _IncomeList = new List<Variance_GetAllPayVarianceReportHeaderResult>();

            foreach (Variance_GetAllPayVarianceReportHeaderResult item in _IncomeList1)
            {
                Variance_GetAllPayVarianceReportHeaderResult _obj = new Variance_GetAllPayVarianceReportHeaderResult();
                _obj.TypeSource = item.TypeSource;
                _IncomeList.Add(_obj);
            }
            List<Ext.Net.ListItem> _SelectedShowType = cmbShow.SelectedItems.ToList();
            List<Variance_GetAllPayVarianceReportHeaderResult> ListSelectedIncomes = new List<Variance_GetAllPayVarianceReportHeaderResult>();
            for (int index = 0; index <= _SelectedShowType.ToList().Count() - 1; index++)
            {
                Variance_GetAllPayVarianceReportHeaderResult _PIncome = new Variance_GetAllPayVarianceReportHeaderResult();
                _PIncome.TypeSource =  _SelectedShowType[index].Value;
                ListSelectedIncomes.Add(_PIncome);
                // table.Columns.Remove(_SelectedShowType[index].Value);

            }

            if (ListSelectedIncomes.Any())
            {
                var ListToHideColumns =
                    from A in _IncomeList
                    where !(
                        from B in ListSelectedIncomes
                        select B.TypeSource
                        ).Contains(A.TypeSource)
                    select A;

                foreach (Variance_GetAllPayVarianceReportHeaderResult _item in ListToHideColumns)
                {
                    if (table.Columns.Contains(PayrollPeriodId.ToString() + ":" + _item.TypeSource))
                        table.Columns.Remove(PayrollPeriodId.ToString() + ":" + _item.TypeSource);

                    if (table.Columns.Contains(PayrollPeriodIdEnd.ToString() + ":" + _item.TypeSource))
                        table.Columns.Remove(PayrollPeriodIdEnd.ToString() + ":" + _item.TypeSource);

                    if (table.Columns.Contains(_item.TypeSource))
                        table.Columns.Remove(_item.TypeSource);
                }
            }

            return table;
        }


        private DataTable GetDataTableForExcel(List<object> _data,List<DynamicReportFieldsBO> Columns)
        {
            DataTable table = new DataTable();

            foreach (DynamicReportFieldsBO _fields in Columns)
            {
                DataColumn colString = new DataColumn();
         
                colString.AllowDBNull = true;
              //  colString.ColumnName = NewMessage.GetTextValue(_fields.ServerMappingFields);// HttpContext.GetGlobalResourceObject("ResourceEnums", _fields.ServerMappingFields).ToString();
                colString.ColumnName = _fields.ServerMappingFields;
                colString.Caption = _fields.Caption;

                if (_fields.DataType.ToLower() == "string")
                    colString.DataType = typeof(string);
                if (_fields.DataType.ToLower() == "float")
                    colString.DataType = typeof(float);
                if (_fields.DataType.ToLower() == "date")
                    colString.DataType = typeof(DateTime);
                table.Columns.Add(colString);


                
            }

            foreach (object[] obj in _data)
            {
                table.Rows.Add(obj);
            }

            //foreach (object _hiddenCols in ColumnsToHide)
            //{
            //    table.Columns.Remove(NewMessage.GetTextValue(_hiddenCols.ToString()).ToString());
            //}

            //ReorderTable(ref table, _SelectedFields.ToArray());

            return table;
        }


        public static void ReorderTable(ref DataTable table, params String[] columns)
        {
            if (columns.Length != table.Columns.Count)
                throw new ArgumentException("Count of columns must be equal to table.Column.Count", "columns");

            for (int i = 0; i < columns.Length; i++)
            {

                table.Columns[columns[i]].SetOrdinal(i);
            }
        }

        protected void btnDynamicExcelPrint_Click(object sender, EventArgs e)
        {

            CustomDate date1 = CustomDate.GetCustomDateFromString(txtFromDate.Text.Trim(), IsEnglish);
            PayrollPeriod startPeriod = CommonManager.GetPayrollPeriod(date1.Month, date1.Year);


            int PayrollPeriodId = 0, PayrollPeriodIdEnd = 0;//need to remove later

            CustomDate date2 = CustomDate.GetCustomDateFromString(txtToDate.Text.Trim(), IsEnglish);
            PayrollPeriod EndPeriod = CommonManager.GetPayrollPeriod(date2.Month, date2.Year);
            if (startPeriod == null || EndPeriod == null)
            {
                NewMessage.ShowNormalMessage("Date " + txtFromDate.Text + "-" + txtToDate.Text + " is not in payroll period");
                return;
            }
            else
            {
                PayrollPeriodId = startPeriod.PayrollPeriodId;
                PayrollPeriodIdEnd = EndPeriod.PayrollPeriodId;
            }

            //PayrollPeriodId = 92; PayrollPeriodIdEnd = 4000;

            if (PayrollPeriodId == PayrollPeriodIdEnd)
                return;

            period1.Text = PayrollPeriodId.ToString();
            period2.Text = PayrollPeriodIdEnd.ToString();

            List<Object> objList = new List<object>();



            List<object> _newObjList = new List<object>();
            List<CalcGetHeaderListResult> headerList = ReportManager.GetVarianceHeaderList(PayrollPeriodId, PayrollPeriodIdEnd);
            headerList = CalculationValue.SortHeaders(headerList, PayManager.GetIncomeOrderValues(), PayManager.GetDeductionOrderValues());

            string[]  headerListArray = cmbShow.SelectedItems.Select(x => x.Value).ToList().ToArray();
            string headerListID = string.Join(",", headerListArray);

            var empList = BLL.BaseBiz.PayrollDataContext.Variance_GetPayVarianceReportEmployeeList(PayrollPeriodId, PayrollPeriodIdEnd,
              0, int.MaxValue, headerListID).ToList();

          
            List<Variance_GetPayVarianceReportEmployeeListDetailsResult> dataList = BLL.BaseBiz.PayrollDataContext
                .Variance_GetPayVarianceReportEmployeeListDetails(PayrollPeriodId, PayrollPeriodIdEnd,
                String.Join(",", empList.Select(x => x.EmployeeId.ToString()).ToArray()),headerListID
                ).ToList();


            PayManager.SetReasonForPayVarianceReport(PayrollPeriodId, PayrollPeriodIdEnd, dataList, String.Join(",", empList.Select(x => x.EmployeeId.ToString()).ToArray()));


            foreach (var emp in empList)
            {
                object[] row = new object[(headerList.Count * 3) + 4];
                row[0] = emp.EmployeeId;
                row[1] = dataList.FirstOrDefault(x => x.EmployeeId == emp.EmployeeId).Name;
                row[2] = dataList.FirstOrDefault(x => x.EmployeeId == emp.EmployeeId).Reason1;
                row[3] = dataList.FirstOrDefault(x => x.EmployeeId == emp.EmployeeId).Reason2;

                int index = 4;
                foreach (CalcGetHeaderListResult header in headerList)
                {
                    Variance_GetPayVarianceReportEmployeeListDetailsResult dataRow = dataList.FirstOrDefault(x => x.EmployeeId == emp.EmployeeId
                        && x.Type == header.Type && x.SourceId == header.SourceId);

                    row[index++] = dataRow == null ? 0 : dataRow.Amount1;
                    row[index++] = dataRow == null ? 0 : dataRow.Amount2;
                    row[index++] = dataRow == null ? 0 : dataRow.Difference;
                }

                _newObjList.Add(row);
            }

            List<ColumnBase> columns = new List<ColumnBase>();
            GridFilters filterList = new GridFilters();
            ModelFieldCollection _ModelFieldCollection = new ModelFieldCollection();
            List<DynamicReportFieldsBO> _listFieldsColumns = new List<DynamicReportFieldsBO>();
            DynamicReportFieldsBO _DynamicReportFieldsBO = new DynamicReportFieldsBO();

            _DynamicReportFieldsBO.FieldName = "EIN";
            _DynamicReportFieldsBO.ServerMappingFields = "EIN";
            _DynamicReportFieldsBO.Caption = "EIN";
            _DynamicReportFieldsBO.DataType = "string";
            _listFieldsColumns.Add(_DynamicReportFieldsBO);

            _DynamicReportFieldsBO = new DynamicReportFieldsBO();
            _DynamicReportFieldsBO.FieldName = "Name";
            _DynamicReportFieldsBO.ServerMappingFields = "Name";
            _DynamicReportFieldsBO.Caption = "Name";
            _DynamicReportFieldsBO.DataType = "string";
            _listFieldsColumns.Add(_DynamicReportFieldsBO);

            _DynamicReportFieldsBO = new DynamicReportFieldsBO();
            _DynamicReportFieldsBO.FieldName = "Reason1";
            _DynamicReportFieldsBO.ServerMappingFields = "Reason1";
            _DynamicReportFieldsBO.Caption = "Reason1";
            _DynamicReportFieldsBO.DataType = "string";
            _listFieldsColumns.Add(_DynamicReportFieldsBO);

            _DynamicReportFieldsBO = new DynamicReportFieldsBO();
            _DynamicReportFieldsBO.FieldName = "Reason2";
            _DynamicReportFieldsBO.ServerMappingFields = "Reason2";
            _DynamicReportFieldsBO.Caption = "Reason2";
            _DynamicReportFieldsBO.DataType = "string";
            _listFieldsColumns.Add(_DynamicReportFieldsBO);



            List<Variance_GetAllPayVarianceReportHeaderResult> _IncomeList = ReportManager.GetALLVarianceHeaderList();
            List<Ext.Net.ListItem> _SelectedShowType = cmbShow.SelectedItems.ToList();
            List<Variance_GetAllPayVarianceReportHeaderResult> ListSelectedIncomes = new List<Variance_GetAllPayVarianceReportHeaderResult>();
            for (int index = 0; index <= _SelectedShowType.ToList().Count() - 1; index++)
            {
                Variance_GetAllPayVarianceReportHeaderResult _PIncome = new Variance_GetAllPayVarianceReportHeaderResult();
                _PIncome.HeaderName = _SelectedShowType[index].Text;
                _PIncome.TypeSource = _SelectedShowType[index].Value;
                ListSelectedIncomes.Add(_PIncome);

            }

            var ListToHideColumns =
                from A in _IncomeList
                where !(
                    from B in ListSelectedIncomes
                    select B.TypeSource
                    ).Contains(A.TypeSource)
                select A;


            //List<DynamicReportFieldsBO> _listFieldsDynamicColumns = new List<DynamicReportFieldsBO>();
            foreach (CalcGetHeaderListResult _item in headerList)
            {


                //if (ListSelectedIncomes.Where(x => x.HeaderName.ToLower() == _item.HeaderName.ToLower()).ToList().Any() || !ListSelectedIncomes.Any())
                //{
                DynamicReportFieldsBO baseColumn = new DynamicReportFieldsBO();
                baseColumn.FieldName = PayrollPeriodId + ":" + _item.Type + ":" + _item.SourceId; //_item.HeaderName;
                baseColumn.ServerMappingFields = PayrollPeriodId + ":" + _item.Type + ":" + _item.SourceId;//_item.HeaderName.Replace(" ", "");
                baseColumn.Caption = DateHelper.GetMonthShortName(startPeriod.Month, IsEnglish);
                baseColumn.GroupName = _item.HeaderName;
                baseColumn.Type = _item.Type.Value;
                baseColumn.SourceId = _item.SourceId.Value;
                baseColumn.DataType = "string";
                _listFieldsColumns.Add(baseColumn);

                DynamicReportFieldsBO compareColumn = new DynamicReportFieldsBO();
                compareColumn.FieldName = PayrollPeriodIdEnd + ":" + _item.Type + ":" + _item.SourceId; //_item.HeaderName;
                compareColumn.ServerMappingFields = PayrollPeriodIdEnd + ":" + _item.Type + ":" + _item.SourceId;//_item.HeaderName.Replace(" ", "");
                compareColumn.Caption = DateHelper.GetMonthShortName(EndPeriod.Month, IsEnglish);
                baseColumn.GroupName = _item.HeaderName;
                baseColumn.Type = _item.Type.Value;
                baseColumn.SourceId = _item.SourceId.Value;
                compareColumn.DataType = "string";
                _listFieldsColumns.Add(compareColumn);

                DynamicReportFieldsBO diffColumn = new DynamicReportFieldsBO();
                diffColumn.FieldName = _item.Type + ":" + _item.SourceId; //_item.HeaderName;
                diffColumn.ServerMappingFields = _item.Type + ":" + _item.SourceId;//_item.HeaderName.Replace(" ", "");
                diffColumn.Caption = "Difference";
                baseColumn.GroupName = _item.HeaderName;
                baseColumn.Type = _item.Type.Value;
                baseColumn.SourceId = _item.SourceId.Value;
                diffColumn.DataType = "string";
                _listFieldsColumns.Add(diffColumn);
                //}
            }

            DataTable _Data = GetDataTable(_newObjList, _listFieldsColumns,PayrollPeriodId,PayrollPeriodIdEnd);
            ExcelHelper.ExportDynamicDtToExcel(_Data);

        }


        protected void Store_ReadDataDynamic(object sender, StoreReadDataEventArgs e)
        {


             GridPanel1.Show();
             
             CustomDate date1 = CustomDate.GetCustomDateFromString(txtFromDate.Text.Trim(),IsEnglish);
             PayrollPeriod startPeriod = CommonManager.GetPayrollPeriod(date1.Month, date1.Year);

           
             int PayrollPeriodId = 0, PayrollPeriodIdEnd = 0;//need to remove later

             CustomDate date2 = CustomDate.GetCustomDateFromString(txtToDate.Text.Trim(), IsEnglish);
             PayrollPeriod EndPeriod = CommonManager.GetPayrollPeriod(date2.Month, date2.Year);
             if (startPeriod == null || EndPeriod == null)
             {
                 NewMessage.ShowNormalMessage("Date "+ txtFromDate.Text + "-" + txtToDate.Text +" is not in payroll period");
                 return;
             }
             else
             {
                 PayrollPeriodId = startPeriod.PayrollPeriodId;
                 PayrollPeriodIdEnd = EndPeriod.PayrollPeriodId;
             }

            // PayrollPeriodId = 92; PayrollPeriodIdEnd = 4000;
             
             if (PayrollPeriodId == PayrollPeriodIdEnd)
             {
                 NewMessage.ShowWarningMessage("Please select different month, same month can not be selected for the report.");
                 return;
             }


             bool isColumnReDisplayNeeded = true;

             if (period1.Text == "")
                isColumnReDisplayNeeded=true;
             else if(period1.Text != PayrollPeriodId.ToString())
                 isColumnReDisplayNeeded = true;
             if (period2.Text != PayrollPeriodIdEnd.ToString())
                 isColumnReDisplayNeeded = true;

             period1.Text = PayrollPeriodId.ToString();
             period2.Text = PayrollPeriodIdEnd.ToString();

            List<Object> objList = new List<object>();
            

            
            List<object> _newObjList = new List<object>();
            List<CalcGetHeaderListResult> headerList = ReportManager.GetVarianceHeaderList(PayrollPeriodId, PayrollPeriodIdEnd);
            headerList = CalculationValue.SortHeaders(headerList, PayManager.GetIncomeOrderValues(), PayManager.GetDeductionOrderValues());

            string[] headerListArray = cmbShow.SelectedItems.Select(x => x.Value).ToList().ToArray();
            string headerListID = string.Join(",", headerListArray);

            var empList = BLL.BaseBiz.PayrollDataContext.Variance_GetPayVarianceReportEmployeeList(PayrollPeriodId, PayrollPeriodIdEnd,
                e.Start, e.Limit,headerListID).ToList();

            if (empList.Count > 0)
                e.Total = empList[0].TotalRows.Value;
            else
                e.Total = 0;

            List<Variance_GetPayVarianceReportEmployeeListDetailsResult> dataList = BLL.BaseBiz.PayrollDataContext
                .Variance_GetPayVarianceReportEmployeeListDetails(PayrollPeriodId, PayrollPeriodIdEnd,
                String.Join(",", empList.Select(x=>x.EmployeeId.ToString()).ToArray() ),headerListID
                ).ToList();


            PayManager.SetReasonForPayVarianceReport(PayrollPeriodId, PayrollPeriodIdEnd, dataList, String.Join(",", empList.Select(x => x.EmployeeId.ToString()).ToArray()));

            
            int eStartingPaging = 0;
            int PagingStart = 1;
            if (e.Start > 0)
            {
                PagingStart = int.Parse(e.Parameters["page"]);
                eStartingPaging = (storeDynamic.PageSize * (PagingStart - 1));
                //if(eStartingPaging>_newObjList.Count)
                //    eStartingPaging = (storeDynamic.PageSize * PagingStart-1);
            }


            foreach (var emp in empList)
            {
                object[] row = new object[ (headerList.Count * 3) + 4];
                row[0] = emp.EmployeeId;

                row[1] = dataList.FirstOrDefault(x => x.EmployeeId == emp.EmployeeId).Name;
                row[2] = dataList.FirstOrDefault(x => x.EmployeeId == emp.EmployeeId).Reason1;
                row[3] = dataList.FirstOrDefault(x => x.EmployeeId == emp.EmployeeId).Reason2;

                int index = 4;
                foreach (CalcGetHeaderListResult header in headerList)
                {
                    Variance_GetPayVarianceReportEmployeeListDetailsResult dataRow= dataList.FirstOrDefault(x=>x.EmployeeId == emp.EmployeeId
                        && x.Type == header.Type && x.SourceId == header.SourceId);

                    row[index++] = dataRow == null ? 0 :  dataRow.Amount1;
                    row[index++] = dataRow == null ? 0 : dataRow.Amount2;
                    row[index++] = dataRow == null ? 0 : dataRow.Difference;
                }

                _newObjList.Add(row);
            }

            List<ColumnBase> columns = new List<ColumnBase>();
            GridFilters filterList = new GridFilters();
            ModelFieldCollection _ModelFieldCollection = new ModelFieldCollection();
            List<DynamicReportFieldsBO> _listFieldsColumns = new List<DynamicReportFieldsBO>();
            DynamicReportFieldsBO _DynamicReportFieldsBO = new DynamicReportFieldsBO();

            _DynamicReportFieldsBO.FieldName = "EIN";
            _DynamicReportFieldsBO.ServerMappingFields = "EIN";
            _DynamicReportFieldsBO.Caption = "EIN";
            _DynamicReportFieldsBO.DataType = "string";
            _listFieldsColumns.Add(_DynamicReportFieldsBO);

            _DynamicReportFieldsBO = new DynamicReportFieldsBO();
            _DynamicReportFieldsBO.FieldName = "Name";
            _DynamicReportFieldsBO.ServerMappingFields = "Name";
            _DynamicReportFieldsBO.Caption = "Name";
            _DynamicReportFieldsBO.DataType = "string";
            _listFieldsColumns.Add(_DynamicReportFieldsBO);

            _DynamicReportFieldsBO = new DynamicReportFieldsBO();
            _DynamicReportFieldsBO.FieldName = "Reason1";
            _DynamicReportFieldsBO.ServerMappingFields = "Reason1";
            _DynamicReportFieldsBO.Caption = "Reason1";
            _DynamicReportFieldsBO.DataType = "string";
            _listFieldsColumns.Add(_DynamicReportFieldsBO);

            _DynamicReportFieldsBO = new DynamicReportFieldsBO();
            _DynamicReportFieldsBO.FieldName = "Reason2";
            _DynamicReportFieldsBO.ServerMappingFields = "Reason2";
            _DynamicReportFieldsBO.Caption = "Reason2";
            _DynamicReportFieldsBO.DataType = "string";
            _listFieldsColumns.Add(_DynamicReportFieldsBO);



            List<Variance_GetAllPayVarianceReportHeaderResult> _IncomeList = ReportManager.GetALLVarianceHeaderList();
            List<Ext.Net.ListItem> _SelectedShowType = cmbShow.SelectedItems.ToList();
            List<Variance_GetAllPayVarianceReportHeaderResult> ListSelectedIncomes = new List<Variance_GetAllPayVarianceReportHeaderResult>();
            for (int index = 0; index <= _SelectedShowType.ToList().Count() - 1; index++)
            {
                Variance_GetAllPayVarianceReportHeaderResult _PIncome = new Variance_GetAllPayVarianceReportHeaderResult();
                _PIncome.HeaderName = _SelectedShowType[index].Text;
                _PIncome.TypeSource = _SelectedShowType[index].Value;
                ListSelectedIncomes.Add(_PIncome);

            }

            var ListToHideColumns =
                from A in _IncomeList
                where !(
                    from B in ListSelectedIncomes
                    select B.TypeSource
                    ).Contains(A.TypeSource)
                select A;


            //List<DynamicReportFieldsBO> _listFieldsDynamicColumns = new List<DynamicReportFieldsBO>();
            foreach (CalcGetHeaderListResult _item in headerList)
            {


                //if (ListSelectedIncomes.Where(x => x.HeaderName.ToLower() == _item.HeaderName.ToLower()).ToList().Any() || !ListSelectedIncomes.Any())
                //{
                    DynamicReportFieldsBO baseColumn = new DynamicReportFieldsBO();
                    baseColumn.FieldName = PayrollPeriodId + ":" + _item.Type + ":" + _item.SourceId; //_item.HeaderName;
                    baseColumn.ServerMappingFields = PayrollPeriodId + ":" + _item.Type + ":" + _item.SourceId;//_item.HeaderName.Replace(" ", "");
                    baseColumn.Caption = DateHelper.GetMonthShortName(startPeriod.Month, IsEnglish);
                    baseColumn.GroupName = _item.HeaderName;
                    baseColumn.Type = _item.Type.Value;
                    baseColumn.SourceId = _item.SourceId.Value;
                    baseColumn.DataType = "string";
                    _listFieldsColumns.Add(baseColumn);

                    DynamicReportFieldsBO compareColumn = new DynamicReportFieldsBO();
                    compareColumn.FieldName = PayrollPeriodIdEnd + ":" + _item.Type + ":" + _item.SourceId; //_item.HeaderName;
                    compareColumn.ServerMappingFields = PayrollPeriodIdEnd + ":" + _item.Type + ":" + _item.SourceId;//_item.HeaderName.Replace(" ", "");
                    compareColumn.Caption = DateHelper.GetMonthShortName(EndPeriod.Month, IsEnglish);
                    baseColumn.GroupName = _item.HeaderName;
                    baseColumn.Type = _item.Type.Value;
                    baseColumn.SourceId = _item.SourceId.Value;
                    compareColumn.DataType = "string";
                    _listFieldsColumns.Add(compareColumn);

                    DynamicReportFieldsBO diffColumn = new DynamicReportFieldsBO();
                    diffColumn.FieldName = _item.Type + ":" + _item.SourceId; //_item.HeaderName;
                    diffColumn.ServerMappingFields = _item.Type + ":" + _item.SourceId;//_item.HeaderName.Replace(" ", "");
                    diffColumn.Caption = "Difference";
                    baseColumn.GroupName = _item.HeaderName;
                    baseColumn.Type = _item.Type.Value;
                    baseColumn.SourceId = _item.SourceId.Value;
                    diffColumn.DataType = "string";
                    _listFieldsColumns.Add(diffColumn);
                //}
            }
     

            //List<DynamicReportFieldsBO> _listFields = new List<DynamicReportFieldsBO>();
            //_listFields.AddRange(_listFieldsStaticColumns);
            //_listFields.AddRange(_listFieldsDynamicColumns);
            List<string> _SelectedFields = new List<string>();

            if (X.IsAjaxRequest && isColumnReDisplayNeeded)
            {
                this.storeDynamic.RemoveFields();
            }


            int colCount = 1;
            int columnIndex = -4;

            Column columnGroup = new Column();
            if (isColumnReDisplayNeeded)
            {
                foreach (DynamicReportFieldsBO _model in _listFieldsColumns)
                {
                    string FieldName = string.Empty;
                    if(_model.ServerMappingFields.Split(':').Length>2)
                    {
                        string[] arry = _model.ServerMappingFields.Split(':');
                        FieldName = arry[1] + ":" + arry[2];
                    }
                    else
                        FieldName = _model.ServerMappingFields;



                    if (ListSelectedIncomes.Where(x => x.TypeSource == FieldName).ToList().Any() || !ListSelectedIncomes.Any() 
                        || _model.FieldName=="EIN" || _model.FieldName=="Name" || _model.FieldName=="Reason1" || _model.FieldName=="Reason2")
                    {
                        columnIndex += 1;
                        ColumnBase _column = new Column();
                        if (columnIndex <= 0)
                            columns.Add(_column);

                        //if (columnIndex != 0)
                        //{
                        else if ((columnIndex / 3.0).ToString().Contains(".3333"))
                        {
                            columnGroup = new Column();
                            columnGroup.Text = _model.GroupName;
                            //columnGroup.Width = new Unit(200);
                            columns.Add(columnGroup);
                            _column.Renderer.Fn = "getFormattedAmount1";
                            columnGroup.Columns.Add(_column);
                        }
                        else
                        {
                            _column.Renderer.Fn = "getFormattedAmount1";
                            columnGroup.Columns.Add(_column);

                            if ((columnIndex % 3) == 0)
                                _column.Renderer.Fn = "change";
                        }
                        //}

                        string ServerMappingField = _model.ServerMappingFields;
                        string DataType = _model.DataType;


                        _column.DataIndex = ServerMappingField;
                        _column.Width = 100;
                        _column.ToolTip = _model.FieldName;
                        if (colCount == 1)
                        {
                            _column.Lockable = true;
                            _column.Locked = true;
                            _column.Align = Alignment.Left;
                            _column.Width = 50;
                        }
                        if (colCount == 2 || colCount==3 || colCount==4)
                        {
                            _column.Lockable = true;
                            _column.Locked = true;
                            _column.Align = Alignment.Left;
                            _column.Width = 160;

                            if(colCount==3 || colCount==4)
                                _column.Renderer.Fn = "renderForReason";
                        }
                        if (colCount > 4)
                        {
                            _column.Lockable = false;
                            _column.Locked = false;
                            _column.Align = Alignment.Right;
                        }

                        _column.Text = _model.Caption;

                        if (DataType.ToLower() == "string")
                            storeDynamic.Fields.Add(ServerMappingField, "String");
                        if (DataType.ToLower() == "float")
                            storeDynamic.Fields.Add(ServerMappingField, "Float");



                        // gf.Local = true;

                        ModelField _ModelField = new ModelField();
                        _ModelField.Name = ServerMappingField;

                        if (DataType.ToLower() == "string")
                            _ModelField.Type = ModelFieldType.String;
                        if (DataType.ToLower() == "float")
                            _ModelField.Type = ModelFieldType.Float;

                        this.AddField(_ModelField);
                        colCount++;
                    }
                }
            }


            if (_listFieldsColumns.Any())
            {
                //if (isColumnReDisplayNeeded)
                    this.storeDynamic.RebuildMeta();

                    DataTable _Data = GetDataTable(_newObjList, _listFieldsColumns, PayrollPeriodId, PayrollPeriodIdEnd);
                if(e.Sort.Any())
                _Data.DefaultView.Sort = e.Sort[0].Property + " " + e.Sort[0].Direction;
                GridPanel1.GetStore().DataSource = _Data;
                //storeDynamic.DataBind();

                if (isColumnReDisplayNeeded)
                    this.GridPanel1.ColumnModel.Columns.AddRange(columns);
                storeDynamic.DataBind();
                if (X.IsAjaxRequest)
                {
                    // this.GridPanel1.Features.Add(filterList);
                     this.GridPanel1.Reconfigure();
                  X.AddScript(" if(storeDynamic!=null) storeDynamic.currentPage =" + PagingStart + ";");
                }
            }

        
        }


        private void GetDynamicColsvalue(string KeyValue)
        {

           string EmployeeID = KeyValue;

        }

        private void AddField(ModelField field)
        {
            if (X.IsAjaxRequest)
            {
                this.storeDynamic.AddField(field);
            }
            else
            {
                this.storeDynamic.Model[0].Fields.Add(field);
            }
        }

       
        //protected void cmbBranch_Change(object sender, DirectEventArgs e)
        //{
        //    cmbDepartment.Store[0].DataSource = DepartmentManager.GetAllDepartmentsByBranch(int.Parse(cmbBranch.SelectedItem.Value));
        //    cmbDepartment.Store[0].DataBind();

        //}


        protected void btnSearch_Click(object sender, DirectEventArgs e)
        {


            if (string.IsNullOrEmpty(txtFromDate.Text))
            {
                NewMessage.ShowNormalMessage("From is required.");
                return;
            }

            if (string.IsNullOrEmpty(txtToDate.Text))
            {
                NewMessage.ShowNormalMessage("To is required.");
                return;
            }

            GridPanel1.GetStore().Reload();
        }

       

        #region "ABSTRACT-METHOD"


     
        #endregion
    }
}
