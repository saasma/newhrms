﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReportAdditionsTerminations.aspx.cs"
    MasterPageFile="~/Master/NewDetails.Master" Inherits="Web.Report.ReportAdditionsTerminations"
    Title="Additions and Terminations" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <ext:ResourcePlaceHolder ID="ResourcePlaceHolder1" runat="server" Mode="Script" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <script>
      var storeReport = null;
      var storeReport = null;
      var PagingToolbar1=null;
      Ext.onReady(
        function () {        
              storeReport = <%= storeReport.ClientID %>;
              storeReport1 = <%= storeReport1.ClientID %>;
              PagingToolbar1 = <%= PagingToolbar1.ClientID %>;
              PagingToolbar2 = <%= PagingToolbar2.ClientID %>;
        }
    ); 



    var tipRenderer1 = function (data1, data2) {

   
        this.setTitle(data2.value[1].toString());

    };

    function SaveHeadCountChart_MonthWise() {
        <%= Chart1.ClientID %>.save({
            type: 'image/png'
        });
    }

     function SaveHeadCountChart_YearWise() {
        <%= ChartYearWiseCount.ClientID %>.save({
            type: 'image/png'
        });
    }


        function searchList()
        {
            PagingToolbar_ListItems.doRefresh();
        }

        function searchListDynamic()
        {
            PagingToolbar1.doRefresh();
        }

          function searchListDynamic1()
        {
            PagingToolbar2.doRefresh();
        }
        
              

    function GetServiceList() 
    {
    searchList();
    }
    
    </script>
    <style type="text/css">
        .x-panel-header-default
        {
            background-image: none;
            background-color: transparent;
        }
    </style>
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Additions and Terminations
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="alert alert-info">
            <table class="fieldTable">
                <tr>
                    <td>
                        <pr:CalendarExtControl Width="150px" FieldLabel="From" ID="txtFromDate" runat="server"
                            LabelAlign="Top" LabelSeparator="">
                        </pr:CalendarExtControl>
                    </td>
                    <td>
                        <pr:CalendarExtControl Width="150px" FieldLabel="To" ID="txtToDate" runat="server"
                            LabelAlign="Top" LabelSeparator="">
                        </pr:CalendarExtControl>
                    </td>
                    <td>
                        <ext:ComboBox ID="cmbBranch" Width="180px" runat="server" ValueField="BranchId" DisplayField="Name"
                            Hidden="true" FieldLabel="Branch" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                            QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store5" runat="server">
                                    <Model>
                                        <ext:Model ID="Model5" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="BranchId" />
                                                <ext:ModelField Name="Name" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <Triggers>
                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                            </Triggers>
                            <Listeners>
                                <Select Handler="this.getTrigger(0).show();" />
                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                            </Listeners>
                        </ext:ComboBox>
                    </td>
                    <td style="width: 10px">
                        <ext:ComboBox ID="cmbDepartment" Width="180px" runat="server" ValueField="DepartmentId"
                            Hidden="true" DisplayField="Name" FieldLabel="Department" LabelAlign="top" LabelSeparator=""
                            ForceSelection="true" QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store6" runat="server">
                                    <Model>
                                        <ext:Model ID="Model6" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="DepartmentId" />
                                                <ext:ModelField Name="Name" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <Triggers>
                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                            </Triggers>
                            <Listeners>
                                <Select Handler="this.getTrigger(0).show();" />
                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                            </Listeners>
                        </ext:ComboBox>
                    </td>
                    <td style="padding-top: 25px; width: 100px">
                        <ext:Button Width="70" ID="btnSearch" runat="server" Text="Search" Height="30">
                            <%-- <Listeners>
                                                            <Click Handler="GetServiceList()" />
                                                        </Listeners>--%>
                            <DirectEvents>
                                <Click OnEvent="btnSearch_Click">
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                    </td>
                </tr>
            </table>
        </div>
        <div style="padding-top: 10px">
        </div>
        <div style="float: left">
            <ext:Panel ID="Chart1Panel" runat="server" Width="700" Height="200" Hidden="true"
                Layout="FitLayout">
                <Items>
                    <ext:Chart ID="Chart1" runat="server" InsetPadding="30" Region="Center">
                        <Store>
                            <ext:Store ID="Store1" runat="server" AutoDataBind="true">
                                <Model>
                                    <ext:Model ID="Model1" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="MonthName" />
                                            <ext:ModelField Name="HeadCount" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <Axes>
                            <ext:NumericAxis Minimum="0" Fields="HeadCount" Grid="false">
                                <Label Font="10px Arial">
                                    <Renderer Handler="return Ext.util.Format.number(value, '0,0');" />
                                </Label>
                            </ext:NumericAxis>
                            <ext:CategoryAxis Position="Bottom" Fields="MonthName">
                                <Label Font="11px Arial">
                                    <Renderer Handler="return value.substr(0, 3);" />
                                </Label>
                            </ext:CategoryAxis>
                        </Axes>
                        <Series>
                            <ext:AreaSeries Axis="Left" XField="MonthName" YField="HeadCount">
                                <Style Opacity="0.93" />
                            </ext:AreaSeries>
                        </Series>
                    </ext:Chart>
                </Items>
            </ext:Panel>
            <ext:Button ID="btnSaveMonthWiseChart" runat="server" Text="export chart" Handler="SaveHeadCountChart_MonthWise();"
                Hidden="true" />
        </div>
        <div style="float: left; height: 300px">
            <ext:Label runat="server" ID="lblDate" StyleSpec="font-size:12px;color:rgb(69, 150, 14)">
            </ext:Label>
            <br />
            <br />
            <ext:Label Text="" runat="server" ID="lblTotalCount" StyleSpec="font-size:40px">
            </ext:Label>
            <ext:Label Text="YTD" runat="server" ID="lblYTD" Hidden="true">
            </ext:Label>
            <br />
            <ext:Chart ID="ChartYearWiseCount" runat="server" Height="120" Width="250px" Hidden="true"
                Region="Center" Shadow="true" StyleSpec="background:#fff" Animate="true">
                <Store>
                    <ext:Store ID="storeYearWiseChartCount" runat="server" AutoDataBind="true">
                        <Model>
                            <ext:Model ID="Model17" runat="server">
                                <Fields>
                                    <ext:ModelField Name="HeadCount" />
                                    <ext:ModelField Name="MonthName" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <Axes>
                    <ext:NumericAxis Fields="HeadCount" Position="Bottom" Minimum="0" Hidden="true">
                        <Label>
                            <Renderer Handler="return Ext.util.Format.number(value, '0,0');" />
                        </Label>
                    </ext:NumericAxis>
                    <ext:CategoryAxis Fields="MonthName" Position="Left" />
                </Axes>
                <Series>
                    <ext:BarSeries Axis="Bottom" Highlight="true" XField="MonthName" YField="HeadCount">
                        <Tips TrackMouse="true" Width="140" Height="28">
                            <Renderer Handler="this.setTitle(storeItem.get('MonthName') + ': ' + storeItem.get('HeadCount') );" />
                        </Tips>
                        <Label Display="InsideEnd" Field="HeadCount" Orientation="Horizontal" Color="#333"
                            TextAnchor="middle" />
                    </ext:BarSeries>
                </Series>
            </ext:Chart>
            <ext:Button ID="btnSaveYearWiseChart" runat="server" Text="export chart" Handler="SaveHeadCountChart_YearWise();"
                Hidden="true" />
        </div>
        <div style="clear: both">
        </div>
        <div class="" style="margin-top: 20px">
            <div style="text-align: right; padding-bottom: 10px">
                <ext:LinkButton ID="lnkExcelPrint" Icon="PageExcel" runat="server" Text="Export"
                    OnClick="btnExcelPrint_Click" Hidden="true" AutoPostBack="true">
                </ext:LinkButton>
            </div>
            <h4 id="h1" style="border-bottom: 1px solid #C0C0C0; display: none">
                Additions</h4>
            <br />
            <ext:GridPanel ID="GridPanel1" runat="server" Border="false" AutoScroll="true" MinHeight="300"
                ForceFit="true" Hidden="true">
                <Store>
                    <ext:Store PageSize="50" runat="server" ID="storeReport" RemoteSort="true" OnReadData="Store_ReadDataDynamic"
                        AutoLoad="false">
                        <Model>
                            <ext:Model ID="Model_SearchLedger" runat="server">
                                <Fields>
                                    <ext:ModelField Name="EmployeeId" Type="String" />
                                    <ext:ModelField Name="Name" Type="String" />
                                    <ext:ModelField Name="HireDate" Type="String" />
                                    <ext:ModelField Name="Position" Type="String" />
                                    <ext:ModelField Name="Branch" Type="String" />
                                    <ext:ModelField Name="Department" Type="String" />
                                    <ext:ModelField Name="RowNumber" Type="String" />
                                    <ext:ModelField Name="TotalRows" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Proxy>
                            <ext:PageProxy>
                            </ext:PageProxy>
                        </Proxy>
                    </ext:Store>
                </Store>
                <ColumnModel ID="ColumnModel_Ledger" runat="server" Cls="gridheader">
                    <Columns>
                        <ext:Column ID="Column1" runat="server" Text="Name" DataIndex="Name" Width="150" />
                        <ext:DateColumn ID="Column_Date1" runat="server" Text="Hire Date" DataIndex="HireDate"
                            Width="80" />
                        <ext:Column ID="Column_Document" runat="server" Text="Job Title" Align="Left" DataIndex="Position"
                            Width="150">
                        </ext:Column>
                        <ext:Column ID="Column2" runat="server" Text="Branch" Align="Left" DataIndex="Branch"
                            Width="150">
                        </ext:Column>
                        <ext:Column ID="Column3" runat="server" Text="Department" Align="Left" DataIndex="Department"
                            Width="150">
                        </ext:Column>
                    </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar1" runat="server" StoreID="storeReport" DisplayInfo="true">
                        <Items>
                            <ext:Label ID="Label1" runat="server" Text="Page size:" />
                            <ext:ToolbarSpacer ID="ToolbarSpacer1" runat="server" Width="10" />
                            <ext:ComboBox ID="ComboBox1" runat="server" Width="80" SelectOnFocus="true" Selectable="true"
                                ValueField="Value" DisplayField="Text">
                                <Listeners>
                                    <Select Handler="#{storeReport}.pageSize = this.getValue();#{PagingToolbar1}.moveFirst(); searchListDynamic()" />
                                </Listeners>
                                <Items>
                                    <ext:ListItem Value="20" Text="20" />
                                    <ext:ListItem Value="50" Text="50" />
                                    <ext:ListItem Value="100" Text="100" />
                                </Items>
                                <SelectedItems>
                                    <ext:ListItem Index="1">
                                    </ext:ListItem>
                                </SelectedItems>
                            </ext:ComboBox>
                        </Items>
                    </ext:PagingToolbar>
                </BottomBar>
            </ext:GridPanel>
        </div>
        <div class="" style="min-height: 1000px; margin-top: 20px;">
            <div style="text-align: right; padding-bottom: 10px">
                <ext:LinkButton ID="lnkExcelPrint1" Icon="PageExcel" runat="server" Text="Export"
                    OnClick="btnExcelPrint1_Click" Hidden="true" AutoPostBack="true">
                </ext:LinkButton>
            </div>
            <h4 id="h2" style="display: none; border-bottom: 1px solid #C0C0C0;">
                Terminations</h4>
            <br />
            <ext:GridPanel ID="GridPanel2" runat="server" Border="false" AutoScroll="true" MinHeight="500"
                ForceFit="true" Hidden="true">
                <Store>
                    <ext:Store PageSize="50" runat="server" ID="storeReport1" RemoteSort="true" AutoLoad="false"
                        OnReadData="Store_ReadData1">
                        <Model>
                            <ext:Model ID="Model2" runat="server">
                                <Fields>
                                    <ext:ModelField Name="EmployeeId" Type="String" />
                                    <ext:ModelField Name="Name" Type="String" />
                                    <ext:ModelField Name="HireDate" Type="String" />
                                    <ext:ModelField Name="TerminationDate" Type="String" />
                                    <ext:ModelField Name="Position" Type="String" />
                                    <ext:ModelField Name="Branch" Type="String" />
                                    <ext:ModelField Name="Department" Type="String" />
                                    <ext:ModelField Name="RowNumber" Type="String" />
                                    <ext:ModelField Name="TotalRows" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Proxy>
                            <ext:PageProxy>
                            </ext:PageProxy>
                        </Proxy>
                    </ext:Store>
                </Store>
                <ColumnModel ID="ColumnModel1" runat="server" Cls="gridheader">
                    <Columns>
                        <ext:Column ID="Column4" runat="server" Text="Name" DataIndex="Name" Width="150" />
                        <ext:DateColumn ID="DateColumn1" runat="server" Text="Hire Date" DataIndex="HireDate"
                            Width="80" />
                        <ext:Column ID="Column5" runat="server" Text="Job Title" Align="Left" DataIndex="Position"
                            Width="150">
                        </ext:Column>
                        <ext:Column ID="Column6" runat="server" Text="Branch" Align="Left" DataIndex="Branch"
                            Width="150">
                        </ext:Column>
                        <ext:Column ID="Column7" runat="server" Text="Department" Align="Left" DataIndex="Department"
                            Width="150">
                        </ext:Column>
                        <ext:DateColumn ID="DateColumn2" runat="server" Text="Termination Date" DataIndex="TerminationDate"
                            Width="80" />
                    </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar2" runat="server" StoreID="storeReport1" DisplayInfo="true">
                        <Items>
                            <ext:Label ID="Label2" runat="server" Text="Page size:" />
                            <ext:ToolbarSpacer ID="ToolbarSpacer2" runat="server" Width="10" />
                            <ext:ComboBox ID="ComboBox2" runat="server" Width="80" SelectOnFocus="true" Selectable="true"
                                ValueField="Value" DisplayField="Text">
                                <Listeners>
                                    <Select Handler="#{storeReport1}.pageSize = this.getValue();#{PagingToolbar2}.moveFirst(); searchListDynamic1()" />
                                </Listeners>
                                <Items>
                                    <ext:ListItem Value="20" Text="20" />
                                    <ext:ListItem Value="50" Text="50" />
                                    <ext:ListItem Value="100" Text="100" />
                                </Items>
                                <SelectedItems>
                                    <ext:ListItem Index="1">
                                    </ext:ListItem>
                                </SelectedItems>
                            </ext:ComboBox>
                        </Items>
                    </ext:PagingToolbar>
                </BottomBar>
            </ext:GridPanel>
        </div>
        </Content> </ext:Container> </Items> </ext:Panel>
    </div>
</asp:Content>
