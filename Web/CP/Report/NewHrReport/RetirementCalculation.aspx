﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RetirementCalculation.aspx.cs"
    MasterPageFile="~/Master/NewDetails.Master" Inherits="Web.Report.RetirementCalculation"
    Title="Retirement Settlement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        var template = '<span style="color:{0};font-weight:bold">{1}</span>';

        var getFormattedAmount1 = function (value) {
            if (value == "-")
                return "-";
            var val = parseFloat(value);
            if (isNaN(val))
                return "-";

            if (val == 0)
                return "-";

            return getFormattedAmount(value);
        }

        var renderForReason = function (value) {
            return "<span title='" + value + "'>" + value + "</span>";
        }
        var change = function (value) {
            if (value == 0)
                return "-";
            return Ext.String.format(template, (value > 0) ? "green" : "red", getFormattedAmount(value));
        };
    </script>
    <style type="text/css">
        .x-grid-cell-inner
        {
            padding: 5px 5px 5px 5px;
        }
        .x-column-header-inner
        {
            padding: 7px 5px 7px 5px;
        }
    </style>
    <ext:ResourcePlaceHolder ID="ResourcePlaceHolder1" runat="server" Mode="Script" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <script>
      var storeDynamic = null;
      Ext.onReady(
        function () {        
            storeDynamic = <%= storeDynamic.ClientID %>;
        }
    ); 


//    if(storeDynamic!=null)
//    storeDynamic.currentPage =1;

//        function searchList()
//        {
//            PagingToolbar_ListItems.doRefresh();
//        }

        function searchListDynamic()
        {
            <%=PagingToolbar1.ClientID %>.doRefresh();
        }
       
      var nameRenderer = function(v1,v2,v3)
      {
            var periodid = v2.record.data.PayrollPeriodId;
            var empid = v2.record.data.EmployeeId;
            var name = v2.record.data.Name;

            return '<a href="../../../newhr/Retirement.aspx?id=' + empid + '&pid=' + periodid + '">'+name+'<a/>'
      }

//    function GetServiceList() 
//    {
//    searchList();
//    }
    
    </script>
    <div id="a1">
        <ext:Hidden ID="Hidden1" runat="server" />
        <ext:Hidden ID="period1" runat="server" />
        <ext:Hidden ID="period2" runat="server" />
        <ext:Hidden ID="HiddenFilterHeader" runat="server" />
        <ext:Hidden ID="HiddenCurrentPage" runat="server" />
        <ext:Hidden ID="HiddenSelectedFields" runat="server" />
        <ext:Hidden ID="Hidden_ClassOrGroupID" runat="server" />
        <div class="pageheader">
            <div class="media">
                <div class="media-body">
                    <h4>
                        Retirement Calculation Report
                    </h4>
                </div>
            </div>
        </div>
        <div class="contentpanel">
            <div id="container" style="width: inherit;">
                <div id="fullBlock" class="account_rev">
                    <div class="alert alert-info">
                        <table class="fieldTable1">
                            <tr>
                                <td>
                                    <pr:CalendarExtControl Width="150px" FieldLabel="From Month" ID="txtFromDate" runat="server"
                                        LabelAlign="Top" LabelSeparator="">
                                    </pr:CalendarExtControl>
                                </td>
                                <td>
                                    <pr:CalendarExtControl StyleSpec="margin-left:10px;" Width="150px" FieldLabel="To Month"
                                        ID="txtToDate" runat="server" LabelAlign="Top" LabelSeparator="">
                                    </pr:CalendarExtControl>
                                </td>
                                <td>
                                    <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                                        <Proxy>
                                            <ext:AjaxProxy Url="../../../Handler/EmpSearchID.ashx">
                                                <ExtraParams>
                                                    <ext:Parameter Name="RetiredAlso" Value="true" />
                                                </ExtraParams>
                                                <ActionMethods Read="GET" />
                                                <Reader>
                                                    <ext:JsonReader Root="plants" TotalProperty="total" />
                                                </Reader>
                                            </ext:AjaxProxy>
                                        </Proxy>
                                        <Model>
                                            <ext:Model ID="Model5" IDProperty="EmployeeId" Name="ItemLineModel" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="Name" Type="String" />
                                                    <ext:ModelField Name="EmployeeId" Type="String" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                    <ext:ComboBox LabelSeparator="" StyleSpec="margin-left:10px;" ID="cmbEmpSearch" FieldLabel="Search Employee"
                                        EmptyText="Employee Name" LabelWidth="80" LabelAlign="Top" runat="server" DisplayField="Name"
                                        ValueField="EmployeeId" StoreID="storeSearch" TypeAhead="false" Width="180" PageSize="9999"
                                        HideBaseTrigger="true" MinChars="1" TriggerAction="All" ForceSelection="false">
                                        <ListConfig LoadingText="Searching..." MinWidth="180" StyleSpec="border-top:1px solid #98c0f4;">
                                            <ItemTpl ID="ItemTpl2" runat="server">
                                                <Html>
                                                    <tpl>
                                                        <div class="search-item">
                                                                        <span>{Name}</span>  
                                                         </div>
					                            </tpl>
                                                </Html>
                                            </ItemTpl>
                                        </ListConfig>
                                        <Triggers>
                                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                        </Triggers>
                                        <Listeners>
                                            <Select Handler="this.getTrigger(0).show(); SelEmpSearch();" />
                                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                            <TriggerClick Handler="if (index == 0) { 
                                                       this.clearValue(); 
                                                       this.getTrigger(0).hide();
                                                   }" />
                                        </Listeners>
                                    </ext:ComboBox>
                                </td>
                                <td style="padding-top: 25px; width: 100px">
                                    <ext:Button Width="70" StyleSpec="margin-left:10px;" ID="btnSearch" runat="server"
                                        Text="Search" Height="30">
                                        <Listeners>
                                            <Click Handler="searchListDynamic()" />
                                        </Listeners>
                                        <%--  <DirectEvents>
                                            <Click OnEvent="btnSearch_Click">
                                                <EventMask ShowMask="true" />
                                            </Click>
                                        </DirectEvents>--%>
                                    </ext:Button>
                                </td>
                                <td style="padding-top: 25px">
                                    <ext:Button ID="Button1" runat="server" Text="Export" Cls="updatebtn" Height="30">
                                        <Menu>
                                            <ext:Menu ID="Menu2" runat="server" ShowSeparator="false" Cls="dropdownbtn">
                                                <Items>
                                                    <ext:MenuItem AutoPostBack="true" runat="server" OnClick="btnDynamicExcelPrint_Click"
                                                        Text="Excel" ID="btnExcelPrint">
                                                    </ext:MenuItem>
                                                </Items>
                                            </ext:Menu>
                                        </Menu>
                                    </ext:Button>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="">
                        <ext:GridPanel ID="gridList" runat="server" Border="false" AutoScroll="true">
                            <Store>
                                <ext:Store PageSize="25" runat="server" ID="storeDynamic" RemoteSort="true" OnReadData="Store_ReadDataDynamic"
                                    AutoLoad="false">
                                    <%--<Reader>
                                            <ext:ArrayReader>
                                           
                                            </ext:ArrayReader>
                                        </Reader>--%>
                                    <Model>
                                        <ext:Model ID="DynamicModel" Name="Test" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="SN" Type="Int" />
                                                <ext:ModelField Name="EmployeeId" Type="Int" />
                                                <ext:ModelField Name="AccountNo" Type="String" />
                                                <ext:ModelField Name="Name" Type="String" />
                                                <ext:ModelField Name="Branch" Type="String" />
                                                <ext:ModelField Name="Department" Type="String" />
                                                <ext:ModelField Name="LevelPosition" Type="String" />
                                                <ext:ModelField Name="Designation" Type="String" />
                                                <ext:ModelField Name="JoinDate" Type="String" />
                                                <ext:ModelField Name="JoinDateEng" Type="Date" />
                                                <ext:ModelField Name="RetirementDate" Type="String" />
                                                <ext:ModelField Name="RetirementDateEng" Type="Date" />
                                                <ext:ModelField Name="RetiredMonth" Type="String" />
                                                <ext:ModelField Name="RetiringMonthDays" Type="String" />
                                                <ext:ModelField Name="PayrollPeriodId" Type="Int" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                    <Proxy>
                                        <ext:PageProxy>
                                            <Reader>
                                                <ext:ArrayReader>
                                                </ext:ArrayReader>
                                            </Reader>
                                        </ext:PageProxy>
                                    </Proxy>
                                    <%-- <Parameters>
                                        <ext:StoreParameter Name="SelectedFieldsLineJson" Value="Ext.encode(#{GridPanel1}.getRowsValues({selectedOnly : false}))"
                                            Mode="Raw" />
                                    </Parameters>--%>
                                </ext:Store>
                            </Store>
                            <ColumnModel runat="server" ID="ColumnList">
                                <Columns>
                                    <ext:Column ID="Column1" runat="server" StyleSpec="background-color:#FCE4D6" Width="50"
                                        DataIndex="SN" Text='SN' Locked="true" Border="false" />
                                    <ext:Column ID="Column2" runat="server" StyleSpec="background-color:#FCE4D6" Width="50"
                                        DataIndex="EmployeeId" Text='EIN' Locked="true" Border="false" />
                                    <ext:Column ID="Column4" runat="server" StyleSpec="background-color:#FCE4D6" Width="80"
                                        DataIndex="AccountNo" Text='AccountNo' Locked="true" Border="false">
                                    </ext:Column>
                                    <ext:Column ID="Column3" runat="server" StyleSpec="background-color:#FCE4D6" Width="150"
                                        DataIndex="Name" Text='Name' Locked="true" Border="false">
                                        <Renderer Fn="nameRenderer" />
                                    </ext:Column>
                                    <ext:Column ID="Column5" runat="server" StyleSpec="background-color:#FCE4D6" Width="120"
                                        DataIndex="Branch" Text='Branch' Locked="false" Border="false">
                                    </ext:Column>
                                    <ext:Column ID="Column6" runat="server" StyleSpec="background-color:#FCE4D6" Width="120"
                                        DataIndex="Department" Text='Department' Locked="false" Border="false">
                                    </ext:Column>
                                    <ext:Column ID="Column7" runat="server" StyleSpec="background-color:#FCE4D6" Width="120"
                                        DataIndex="LevelPosition" Text='Position' Locked="false" Border="false">
                                    </ext:Column>
                                    <ext:Column ID="Column8" runat="server" StyleSpec="background-color:#FCE4D6" Width="120"
                                        DataIndex="Designation" Text='Designation' Locked="false" Border="false">
                                    </ext:Column>
                                    <ext:Column ID="Column9" runat="server" StyleSpec="background-color:#FCE4D6" Width="100"
                                        DataIndex="JoinDate" Text='Join Date' Locked="false" Border="false">
                                    </ext:Column>
                                    <ext:DateColumn Format="dd-MMM-yyyy" StyleSpec="background-color:#FCE4D6" ID="Column10"
                                        runat="server" Width="100" DataIndex="JoinDateEng" Text='Join Date Eng' Locked="false"
                                        Border="false">
                                    </ext:DateColumn>
                                    <ext:Column ID="Column11" runat="server" StyleSpec="background-color:#FCE4D6" Width="100"
                                        DataIndex="RetirementDate" Text='Ret Date' Locked="false" Border="false">
                                    </ext:Column>
                                    <ext:DateColumn Format="dd-MMM-yyyy" StyleSpec="background-color:#FCE4D6" ID="DateColumn1"
                                        runat="server" Width="100" DataIndex="RetirementDateEng" Text='Ret Date Eng'
                                        Locked="false" Border="false">
                                    </ext:DateColumn>
                                    <ext:Column ID="Column12" runat="server" StyleSpec="background-color:#FCE4D6" Width="100"
                                        DataIndex="RetiredMonth" Text='Ret Month' Locked="false" Border="false">
                                    </ext:Column>
                                    <ext:Column ID="Column13" runat="server" StyleSpec="background-color:#FCE4D6" Width="80"
                                        DataIndex="RetiringMonthDays" Text='Month Days' Locked="false" Border="false">
                                    </ext:Column>
                                </Columns>
                            </ColumnModel>
                            <BottomBar>
                                <ext:PagingToolbar ID="PagingToolbar1" runat="server" StoreID="storeDynamic" DisplayInfo="true"
                                    Hidden="false">
                                    <%-- <Items>
                                        <ext:Label ID="Label1" runat="server" Text="Page size:" />
                                        <ext:ToolbarSpacer ID="ToolbarSpacer1" runat="server" Width="10" />
                                    </Items>--%>
                                </ext:PagingToolbar>
                            </BottomBar>
                        </ext:GridPanel>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
