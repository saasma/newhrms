﻿<%@ Page Title="Variance Summary" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="VarianceSummary.aspx.cs" Inherits="Web.Employee.VarianceSummary" %>

<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .details
        {
            padding-top: 10px;
        }
        .details tr td
        {
            padding-top: 4px;
        }
        .blockDiv
        {
            margin-top: 15px;
        }
        strong
        {
            font-size: 15px !important;
            font-weight: bold !important;
            padding-bottom: 6px;
            display: block;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="contentArea">
        <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
        <div class="pageheader">
            <div class="media">
                <div class="media-body">
                    <h4>
                        Salary Verification Summary
                    </h4>
                </div>
            </div>
        </div>
        <div class="contentpanel">
            <div class="attribute" style="padding: 10px">
                <table>
                    <tr>
                        <td>
                            <strong>Payroll Period</strong>
                        </td>
                        <td>
                            <asp:DropDownList AppendDataBoundItems="true" ID="ddlPayrollPeriods" runat="server"
                                DataTextField="Name" DataValueField="PayrollPeriodId">
                                <asp:ListItem Text="--Select Payroll Period--" Value="-1"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlPayrollPeriods"
                                InitialValue="-1" Display="None" ErrorMessage="Please select payroll period."
                                ValidationGroup="Load"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:Button ID="btnLoad" CssClass="load" runat="server" OnClientClick="valGroup='Load';return CheckValidation()"
                                Text="Load" OnClick="btnLoad_Click" />
                        </td>
                    </tr>
                </table>
            </div>
            <div class="blockdiv">
                <table>
                    <tr>
                        <td>
                            <strong>Month</strong>
                        </td>
                        <td>
                            <strong>
                                <asp:Label ID="lbl1" runat="server" /></strong>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <strong>
                                <asp:Label ID="lbl2" runat="server" /></strong>
                        </td>
                    </tr>
                </table>
            </div>
            <table style='width: 300px; clear: both; margin-top: 10px;' class="tableLightColor">
                <tr class="even">
                    <td>
                        Regular Payment
                    </td>
                    <td style="text-align: right;">
                        <asp:Label runat="server" ID="lblRegularCount" />
                    </td>
                </tr>
                <tr class="odd">
                    <td>
                        Stop Payment
                    </td>
                    <td style="text-align: right;">
                        <asp:Label runat="server" ID="lblStopPaymentCount" />
                    </td>
                </tr>
                <tr class="even">
                    <td>
                        <b>Total Employees in Payroll</b>
                    </td>
                    <td style="text-align: right;">
                        <asp:Label runat="server" ID="lblTotalCount" />
                    </td>
                </tr>
            </table>
            <table style='width: 300px; clear: both; margin-top: 10px;' class="tableLightColor">
                <tr class="even">
                    <td>
                        Zero Adjustment
                    </td>
                    <td style="text-align: right;">
                        <asp:Label runat="server" ID="lblZeroAdjCount" />
                    </td>
                </tr>
                <tr class="odd">
                    <td>
                        Retired Employees
                    </td>
                    <td style="text-align: right;">
                        <asp:Label runat="server" ID="lblRetCount" />
                    </td>
                </tr>
                <tr class="even">
                    <td>
                        Salary Increased
                    </td>
                    <td style="text-align: right;">
                        <asp:Label runat="server" ID="lblSalaryIncCount" />
                    </td>
                </tr>
                <tr class="odd">
                    <td>
                        This month salary started
                    </td>
                    <td style="text-align: right;">
                        <asp:Label runat="server" ID="lblThisMonthSalaryCalc" />
                    </td>
                </tr>
            </table>
            <div class="blockdiv" style="margin-top: 10px;">
                <strong runat="server" id="stopPaymentFull">Stop payment</strong>
                <asp:GridView ID="gvwFullStopPayment" Width="400px" CssClass="tableLightColor" GridLines="None"
                    runat="server" AutoGenerateColumns="false">
                    <Columns>
                        <asp:BoundField HeaderText="EIN" DataField="EmployeeId" />
                        <asp:BoundField HeaderText="I No" DataField="IdCardNo" />
                        <asp:TemplateField HeaderText="Employee" HeaderStyle-HorizontalAlign="Left" ControlStyle-Width="120px">
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# Eval("Name") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="From Date" DataField="FromDate" />
                        <asp:BoundField HeaderText="From Eng" DataFormatString="{0:MM/dd/yyyy}" DataField="EngFromDate" />
                        <asp:BoundField HeaderText="To Date" DataField="ToDate" />
                        <asp:BoundField HeaderText="To Eng" DataFormatString="{0:MM/dd/yyyy}" DataField="EngToDate" />
                        <asp:BoundField HeaderText="Notes" DataField="Notes" />
                    </Columns>
                    <RowStyle CssClass="odd" />
                    <AlternatingRowStyle CssClass="even" />
                </asp:GridView>
            </div>
            <div class="blockDiv">
                <strong runat="server" id="stoppaymentPartial">Partial payment</strong>
                <asp:GridView ID="gvwPartialStopPayment" Width="400px" CssClass="tableLightColor"
                    GridLines="None" runat="server" AutoGenerateColumns="false">
                    <Columns>
                        <asp:BoundField HeaderText="EIN" DataField="EmployeeId" />
                        <asp:BoundField HeaderText="I No" DataField="IdCardNo" />
                        <asp:TemplateField HeaderText="Employee" HeaderStyle-HorizontalAlign="Left" ControlStyle-Width="120px">
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("Name") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="From Date" DataField="FromDate" />
                        <asp:BoundField HeaderText="From Eng" DataFormatString="{0:MM/dd/yyyy}" DataField="EngFromDate" />
                        <asp:BoundField HeaderText="To Date" DataField="ToDate" />
                        <asp:BoundField HeaderText="To Eng" DataFormatString="{0:MM/dd/yyyy}" DataField="EngToDate" />
                        <asp:BoundField HeaderText="Notes" DataField="Notes" />
                    </Columns>
                    <RowStyle CssClass="odd" />
                    <AlternatingRowStyle CssClass="even" />
                </asp:GridView>
            </div>
            <div class="blockDiv">
                <strong runat="server" id="StrongNetZero">Zero Salary Adjustment</strong>
                <asp:GridView ID="gvwNetZero" Width="400px" CssClass="tableLightColor" GridLines="None"
                    runat="server" AutoGenerateColumns="false">
                    <Columns>
                        <asp:BoundField HeaderText="EIN" DataField="EmployeeId" />
                        <asp:BoundField HeaderText="I No" DataField="IdCardNo" />
                        <asp:TemplateField HeaderText="Employee" HeaderStyle-HorizontalAlign="Left" ControlStyle-Width="120px">
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("Name") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="odd" />
                    <AlternatingRowStyle CssClass="even" />
                </asp:GridView>
            </div>
            <div class="blockDiv">
                <strong runat="server" id="StrongThisMonth">New Joining Employees this Month</strong>
                <asp:GridView ID="gvwThisMonth" Width="400px" CssClass="tableLightColor" GridLines="None"
                    runat="server" AutoGenerateColumns="false">
                    <Columns>
                        <asp:BoundField HeaderText="EIN" DataField="EmployeeId" />
                        <asp:BoundField HeaderText="I No" DataField="IdCardNo" />
                        <asp:TemplateField HeaderText="Employee" HeaderStyle-HorizontalAlign="Left" ControlStyle-Width="120px">
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("Name") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="From Date" DataField="FromDate" />
                        <asp:BoundField HeaderText="From Eng" DataFormatString="{0:MM/dd/yyyy}" DataField="EngFromDate" />
                    </Columns>
                    <RowStyle CssClass="odd" />
                    <AlternatingRowStyle CssClass="even" />
                </asp:GridView>
            </div>
            <div class="blockDiv">
                <strong runat="server" id="StrongRetirement">Retired Employees</strong>
                <asp:GridView ID="gvwRetirement" Width="400px" CssClass="tableLightColor" GridLines="None"
                    runat="server" AutoGenerateColumns="false">
                    <Columns>
                        <asp:BoundField HeaderText="EIN" DataField="EmployeeId" />
                        <asp:BoundField HeaderText="I No" DataField="IdCardNo" />
                        <asp:TemplateField HeaderText="Employee" HeaderStyle-HorizontalAlign="Left" ControlStyle-Width="120px">
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("Name") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="From Date" DataField="FromDate" />
                        <asp:BoundField HeaderText="From Eng" DataFormatString="{0:MM/dd/yyyy}" DataField="EngFromDate" />
                    </Columns>
                    <RowStyle CssClass="odd" />
                    <AlternatingRowStyle CssClass="even" />
                </asp:GridView>
            </div>
            <div class="blockDiv">
                <strong runat="server" id="StrongSalaryInc">Salary Increased</strong>
                <asp:GridView ID="gvwSalaryInc" Width="400px" CssClass="tableLightColor" GridLines="None"
                    runat="server" AutoGenerateColumns="false">
                    <Columns>
                        <asp:BoundField HeaderText="EIN" DataField="EmployeeId" />
                        <asp:BoundField HeaderText="I No" DataField="IdCardNo" />
                        <asp:TemplateField HeaderText="Employee" HeaderStyle-HorizontalAlign="Left" ControlStyle-Width="120px">
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("Name") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="From Date" DataField="FromDate" />
                        <asp:BoundField HeaderText="From Eng" DataFormatString="{0:MM/dd/yyyy}" DataField="FromDateEng" />
                        <asp:TemplateField HeaderText="Income" HeaderStyle-HorizontalAlign="Left" ControlStyle-Width="100px">
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("Income") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="Old Amount" DataFormatString="{0:N2}" DataField="PreviousAmountRate" />
                        <asp:BoundField HeaderText="New Amount" DataFormatString="{0:N2}" DataField="NewAmountRate" />
                        <asp:BoundField HeaderText="Is Arrear" DataField="IsReterospect" />
                    </Columns>
                    <RowStyle CssClass="odd" />
                    <AlternatingRowStyle CssClass="even" />
                </asp:GridView>
            </div>
        </div>

        <asp:Button ID="btnApprove" Width="120px" OnClientClick="if(confirm('{0}')==false) return false;"
                    Visible="false" runat="server" CssClass="save" Text="Approve" OnClick="btnApprove_Click" />
             
</asp:Content>
