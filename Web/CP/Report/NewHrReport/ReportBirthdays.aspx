﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReportBirthdays.aspx.cs"
    MasterPageFile="~/Master/NewDetails.Master" Inherits="Web.Report.ReportBirthdays"
    Title="Birthdays" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        var fnRenderValue = function (value, meta, record, rowIndex) {

            var templateTotal = '<div style="color:{0};font-weight:bold;font-size:20px;border-bottom:1px solid grey;padding-bottom:5px;padding-top:10px">{1}</div>';
            var ID = record.data.ID;
            if (ID == 'month')
                return Ext.String.format(templateTotal, " #24170D", value);
            else
                return value;

        };


    </script>
    <style type="text/css">
        .x-grid-td
        {
            border-bottom-width: 0px !important;
        }
    </style>
    <ext:ResourcePlaceHolder ID="ResourcePlaceHolder1" runat="server" Mode="Script" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Birthdays
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div style="text-align: right; width: 400px; padding-bottom: 10px">
            <ext:LinkButton ID="LinkButton1" Icon="PageExcel" runat="server" Text="Export" OnClick="btnExcel_Click"
                AutoPostBack="true">
            </ext:LinkButton>
        </div>
        <ext:GridPanel ID="GridPanel1" runat="server" Width="800" Border="false"  AutoScroll="true"
            Header="false">
            <Store>
                <ext:Store PageSize="50" runat="server" ID="StoreGrid1" RemoteSort="true" OnReadData="Store_ReadData"
                    AutoLoad="true">
                    <Model>
                        <ext:Model ID="Model133" runat="server">
                            <Fields>
                                <ext:ModelField Name="Name" Type="String" />
                                <ext:ModelField Name="ID" Type="String" />
                            </Fields>
                        </ext:Model>
                    </Model>
                    <Proxy>
                        <ext:PageProxy>
                        </ext:PageProxy>
                    </Proxy>
                </ext:Store>
            </Store>
            <ColumnModel ID="ColumnModel1" runat="server" Cls="gridheader">
                <Columns>
                    <ext:Column ID="c1" runat="server" Text="Name" DataIndex="Name" Width="300" HideTitleEl="true">
                        <Renderer Fn="fnRenderValue">
                        </Renderer>
                    </ext:Column>
                </Columns>
            </ColumnModel>
            <View>
                <ext:GridView ID="GridView1" runat="server" StripeRows="false">
                </ext:GridView>
            </View>
        </ext:GridPanel>
    </div>
</asp:Content>
