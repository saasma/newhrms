﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Ext.Net;
using DAL;
using BLL;
using BLL.Base;
using BLL.Manager;
using Utils.Helper;
using Utils.Calendar;
//using Web.Report.Templates;
using System.IO;
using DevExpress.XtraPrinting;
using BLL.BO;
using System.Xml;
using System.Xml.Xsl;
using Newtonsoft.Json.Linq;
using System.Collections;
using System.Reflection;
using System.Data;
using Bll;

namespace Web.Report
{

    public partial class ReportPayBranchDepartmentWise : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!X.IsAjaxRequest && !IsPostBack)
            {
                cmbBranch.Store[0].DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
                cmbBranch.Store[0].DataBind();

                DepartmentManager _DepartmentManager = new DepartmentManager();
                cmbDepartment.Store[0].DataSource = _DepartmentManager.GetDepartmentsByCompany(SessionManager.CurrentCompanyId);
                cmbDepartment.Store[0].DataBind();

                CustomDate date = CustomDate.GetTodayDate(IsEnglish).GetFirstDateOfThisMonth();
                CustomDate date2 = CustomDate.GetTodayDate(IsEnglish).GetLastDateOfThisMonth();
                txtFromDate.Text = date.ToString();
                txtToDate.Text = date2.ToString();

                btnSearch_Click(null, null);


            }
        }

        void Print(bool isPDF)
        {
           
        }

        protected void btnPDFPrint_Click(object sender, EventArgs e)
        {
            Print(true);
        }


        protected Field OnCreateFilterableField(object sender, ColumnBase column, Field defaultField)
        {
            
                ((TextField)defaultField).Icon = Icon.Magnifier;
                return defaultField;
        }

        private DataTable GetDataTable(List<object> _data, List<DynamicReportFieldsBO> Columns)
        {
            DataTable table = new DataTable();

            foreach (DynamicReportFieldsBO _fields in Columns)
            {

                //DataColumn column = new DataColumn(headerList[i].Type + ":" + headerList[i].SourceId, typeof(decimal));
                //column.Caption = headerList[i].HeaderName;

                DataColumn colString = new DataColumn();
                colString.AllowDBNull = true;
                colString.ColumnName = _fields.ServerMappingFields;
                colString.Caption = _fields.Caption;

                if (_fields.DataType.ToLower() == "string")
                    colString.DataType = typeof(string);
                if (_fields.DataType.ToLower() == "float")
                    colString.DataType = typeof(float);
                if (_fields.DataType.ToLower() == "date")
                    colString.DataType = typeof(DateTime);
                table.Columns.Add(colString);

            }

            foreach (object[] obj in _data)
            {

                table.Rows.Add(obj);
            }
            //foreach (object _hiddenCols in ColumnsToHide)
            //{
            //    table.Columns.Remove(NewMessage.GetTextValue(_hiddenCols.ToString()).ToString());
            //}


            return table;
        }




        private DataTable GetDataTableForExcel(List<object> _data,List<DynamicReportFieldsBO> Columns)
        {
            DataTable table = new DataTable();

            foreach (DynamicReportFieldsBO _fields in Columns)
            {
                DataColumn colString = new DataColumn();
         
                colString.AllowDBNull = true;
              //  colString.ColumnName = NewMessage.GetTextValue(_fields.ServerMappingFields);// HttpContext.GetGlobalResourceObject("ResourceEnums", _fields.ServerMappingFields).ToString();
                colString.ColumnName = _fields.ServerMappingFields;
                colString.Caption = _fields.Caption;

                if (_fields.DataType.ToLower() == "string")
                    colString.DataType = typeof(string);
                if (_fields.DataType.ToLower() == "float")
                    colString.DataType = typeof(float);
                if (_fields.DataType.ToLower() == "date")
                    colString.DataType = typeof(DateTime);
                table.Columns.Add(colString);


                
            }

            foreach (object[] obj in _data)
            {
                table.Rows.Add(obj);
            }

            //foreach (object _hiddenCols in ColumnsToHide)
            //{
            //    table.Columns.Remove(NewMessage.GetTextValue(_hiddenCols.ToString()).ToString());
            //}

            //ReorderTable(ref table, _SelectedFields.ToArray());

            return table;
        }


        public static void ReorderTable(ref DataTable table, params String[] columns)
        {
            if (columns.Length != table.Columns.Count)
                throw new ArgumentException("Count of columns must be equal to table.Column.Count", "columns");

            for (int i = 0; i < columns.Length; i++)
            {

                table.Columns[columns[i]].SetOrdinal(i);
            }
        }

        protected void btnDynamicExcelPrint_Click(object sender, EventArgs e)
        {

            if (string.IsNullOrEmpty(txtFromDate.Text))
                return;
            if (string.IsNullOrEmpty(txtToDate.Text))
                return;

            CustomDate date1 = CustomDate.GetCustomDateFromString(txtFromDate.Text.Trim(), IsEnglish);
            PayrollPeriod startPeriod = CommonManager.GetPayrollPeriod(date1.Month, date1.Year);


            int PayrollPeriodId = 0, PayrollPeriodIdEnd = 0;//need to remove later

            CustomDate date2 = CustomDate.GetCustomDateFromString(txtToDate.Text.Trim(), IsEnglish);
            PayrollPeriod EndPeriod = CommonManager.GetPayrollPeriod(date2.Month, date2.Year);
            if (startPeriod == null || EndPeriod == null)
            {
                NewMessage.ShowNormalMessage("Date " + txtFromDate.Text + "-" + txtToDate.Text + " is not in payroll period");
                return;
            }
            else
            {
                PayrollPeriodId = startPeriod.PayrollPeriodId;
                PayrollPeriodIdEnd = EndPeriod.PayrollPeriodId;
            }



            List<Branch> dataBranch = new List<Branch>();
            List<Department> dataDepartment = new List<Department>();
            List<Object> objList = new List<object>();
            bool isDepartmentWise = false;

            if (!string.IsNullOrEmpty(cmbShow.SelectedItem.Value))
            {
                if (cmbShow.SelectedItem.Value == "1")//departmentWise---show department
                {
                    isDepartmentWise = true;
                    DepartmentManager _DepartmentManager = new DepartmentManager();
                    if (!string.IsNullOrEmpty(cmbDepartment.SelectedItem.Value))
                        dataDepartment = _DepartmentManager.GetDepartmentsByCompany(SessionManager.CurrentCompanyId).Where(x => x.DepartmentId == int.Parse(cmbDepartment.SelectedItem.Value)).ToList();
                    else
                        dataDepartment = _DepartmentManager.GetDepartmentsByCompany(SessionManager.CurrentCompanyId);

                    for (int i = 0; i < dataDepartment.Count; i++)
                        objList.Add(dataDepartment[i]);
                }

                else//branchwise show branch
                {

                    isDepartmentWise = false;
                    if (!string.IsNullOrEmpty(cmbBranch.SelectedItem.Value))
                        dataBranch = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId).Where(x => x.BranchId == int.Parse(cmbBranch.SelectedItem.Value)).ToList();
                    else
                        dataBranch = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);

                    for (int i = 0; i < dataBranch.Count; i++)
                        objList.Add(dataBranch[i]);
                }
            }



            //List<GetEmployeeMasterResult> data = EmployeeManager.GetEmployeeMaster(e.Start, e.Limit, "").ToList();
            //  GridPanel1.EnableColumnHide = true;


            //for creating list obj of fixed columns from SP and DynamicColums from tablee
            List<object> _newObjList = new List<object>();
            // List<testdynamicTable> _testdynamicTable = DynamicReportManager.GetDynamicColumns();
            //  List<GetEmployeeMasterOtherHeadersResult> _dynamicTable = EmployeeManager.GetEmployeeMasterHeader();

            List<CalcGetHeaderListResult> _dynamicTable = ReportManager.GetHeaderList(PayrollPeriodId, PayrollPeriodIdEnd);
            _dynamicTable = CalculationValue.SortHeaders(_dynamicTable, PayManager.GetIncomeOrderValues(), PayManager.GetDeductionOrderValues());

            int FixedListCols = 1;
            //if (isDepartmentWise)
            //    FixedListCols = dataDepartment[0].GetType().GetProperties().Length;
            //else
            //    FixedListCols = dataBranch[0].GetType().GetProperties().Length;

            int DynamicCols = _dynamicTable.Count();
            int TotalFields = FixedListCols + DynamicCols;

            PropertyInfo[] properties = null;

            if (isDepartmentWise)
                properties = dataDepartment[0].GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
            else
                properties = dataBranch[0].GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);



            int BranchID, DepartmentID, ShowType;

            if (string.IsNullOrEmpty(cmbBranch.SelectedItem.Value))
                BranchID = -1;
            else
                BranchID = int.Parse(cmbBranch.SelectedItem.Value);

            if (string.IsNullOrEmpty(cmbDepartment.SelectedItem.Value))
                DepartmentID = -1;
            else
                DepartmentID = int.Parse(cmbDepartment.SelectedItem.Value);

            ShowType = int.Parse(cmbShow.SelectedItem.Value);

            if (isDepartmentWise)
            {
                List<Report_Pay_DepartmentBranchWiseResult> _ListReportPayBranchDepartmentWise = ReportManager.ReportPayDepartmentBranchWise(PayrollPeriodId, PayrollPeriodIdEnd, -1, -1, ShowType).ToList();


                foreach (object item in dataDepartment)
                {
                    Array stringArray = Array.CreateInstance(typeof(object), TotalFields);

                    for (int col = 0; col <= TotalFields - 1; col++)
                    {
                        if (col <= FixedListCols - 1)//setting array for fixed columns
                        {
                            object o = properties[col].GetValue(item, null);
                            string cellValue = o == null ? null : o.ToString();
                            // stringArray.SetValue(cellValue, col);
                            stringArray.SetValue(((DAL.Department)(item)).Name, col);
                            //stringArray.SetValue(cellValue, col);
                        }
                        else//getting dynamic cols
                        {
                            foreach (CalcGetHeaderListResult _Column in _dynamicTable)
                            {
                                //getting value based on dynamiccolumn column
                                //getColumnValueBasedOnKeyValue();

                                int ID = int.Parse(dataDepartment[0].GetType().GetProperty("DepartmentId").GetValue(item, null).ToString());
                                string ValueList = _Column.TypeSourceId;//_Column.GetType().GetProperty("TypeSourceID").GetValue(item, null).ToString();
                                //  string[] arrayValueList = ValueList.Split(';');
                                List<EmployeeExtraFieldsValuesBO> _ListEmployeeExtraFieldsValuesBO = new List<EmployeeExtraFieldsValuesBO>();
                                //for (int i = 0; i < arrayValueList.Length; i++)
                                //{
                                string[] arrayInnerValueList = ValueList.Split(':');
                                if (arrayInnerValueList.Length > 0)
                                {
                                    EmployeeExtraFieldsValuesBO _EmployeeExtraFieldsValuesBO = new EmployeeExtraFieldsValuesBO();
                                    _EmployeeExtraFieldsValuesBO.ID = ID;
                                    _EmployeeExtraFieldsValuesBO.TypeID = int.Parse(arrayInnerValueList[0]);
                                    _EmployeeExtraFieldsValuesBO.SourceID = int.Parse(arrayInnerValueList[1]);


                                    
                                    Report_Pay_DepartmentBranchWiseResult _GetValue = _ListReportPayBranchDepartmentWise.SingleOrDefault(x => x.SourceId == _EmployeeExtraFieldsValuesBO.SourceID && x.Type == _EmployeeExtraFieldsValuesBO.TypeID && x.ID == ID);

                                    if (_GetValue != null)
                                    {
                                        //_EmployeeExtraFieldsValuesBO.Value = _GetValue.Amount.ToString();  //need to get value from another table
                                        stringArray.SetValue(_GetValue.Amount.ToString(), col);
                                    }
                                    else
                                        stringArray.SetValue("-", col);
                                    
                                    //  _ListEmployeeExtraFieldsValuesBO.Add(_EmployeeExtraFieldsValuesBO);
                                }
                                //}

                                //List<CalcGetHeaderListResult> _ListExtraHeaderEmployee = ReportManager.GetHeaderList(PayrollPeriodId, PayrollPeriodIdEnd).Where(x => x.HeaderName.ToLower() == _Column.HeaderName.ToLower()).ToList();
                                //int SourceID = _ListExtraHeaderEmployee[0].SourceId.Value;
                                //int Type = _ListExtraHeaderEmployee[0].Type.Value;
                                //String ColumnValue = "-";
                                //EmployeeExtraFieldsValuesBO _ValueList = _ListEmployeeExtraFieldsValuesBO.SingleOrDefault(x => x.ID == ID && x.SourceID == SourceID && x.TypeID == Type && x.ID==ID);
                                //if (_ValueList != null)
                                //    ColumnValue = _ValueList.Value;
                                //stringArray.SetValue(ColumnValue, col);
                                col++;
                            }
                        }
                    }
                    _newObjList.Add(stringArray);
                }
            }
            else
            {
                List<Report_Pay_DepartmentBranchWiseResult> _ListReportPayBranchDepartmentWise = ReportManager.ReportPayDepartmentBranchWise(PayrollPeriodId, PayrollPeriodIdEnd, -1, -1, ShowType).ToList();
                                  

                foreach (object item in dataBranch)
                {
                    Array stringArray = Array.CreateInstance(typeof(object), TotalFields);

                    for (int col = 0; col <= TotalFields - 1; col++)
                    {
                        if (col <= FixedListCols - 1)//setting array for fixed columns
                        {
                            object o = properties[col].GetValue(item, null);
                            string cellValue = o == null ? null : o.ToString();
                            // stringArray.SetValue(cellValue, col);
                            stringArray.SetValue(((DAL.Branch)(item)).Name, col);
                        }

                        else//getting dynamic cols
                        {
                            foreach (CalcGetHeaderListResult _Column in _dynamicTable)
                            {
                                //getting value based on dynamiccolumn column
                                //getColumnValueBasedOnKeyValue();

                                int ID = int.Parse(dataBranch[0].GetType().GetProperty("BranchId").GetValue(item, null).ToString());
                                string ValueList = _Column.TypeSourceId;//_Column.GetType().GetProperty("TypeSourceID").GetValue(item, null).ToString();
                                //  string[] arrayValueList = ValueList.Split(';');
                                List<EmployeeExtraFieldsValuesBO> _ListEmployeeExtraFieldsValuesBO = new List<EmployeeExtraFieldsValuesBO>();
                                //for (int i = 0; i < arrayValueList.Length; i++)
                                //{
                                string[] arrayInnerValueList = ValueList.Split(':');
                                if (arrayInnerValueList.Length > 0)
                                {
                                    //EmployeeExtraFieldsValuesBO _EmployeeExtraFieldsValuesBO = new EmployeeExtraFieldsValuesBO();
                                    //_EmployeeExtraFieldsValuesBO.ID = ID;
                                    //_EmployeeExtraFieldsValuesBO.TypeID = int.Parse(arrayInnerValueList[0]);
                                    //_EmployeeExtraFieldsValuesBO.SourceID = int.Parse(arrayInnerValueList[1]);
                                     Report_Pay_DepartmentBranchWiseResult _GetValue = _ListReportPayBranchDepartmentWise.SingleOrDefault(x => x.SourceId == int.Parse(arrayInnerValueList[1]) && x.Type == int.Parse(arrayInnerValueList[0]) && x.ID == ID);
                                    //if (_GetValue != null)
                                    //    _EmployeeExtraFieldsValuesBO.Value = _GetValue.Amount.ToString();  //need to get value from another table
                                    //_ListEmployeeExtraFieldsValuesBO.Add(_EmployeeExtraFieldsValuesBO);
                                    if (_GetValue != null)
                                    {
                                        //_EmployeeExtraFieldsValuesBO.Value = _GetValue.Amount.ToString();  //need to get value from another table
                                        stringArray.SetValue(_GetValue.Amount.ToString(), col);
                                    }
                                    else
                                        stringArray.SetValue("-", col);

                                }
                                col++;
                            }
                        }
                    }
                    _newObjList.Add(stringArray);
                }

            }


   //---------------------------

            List<ColumnBase> columns = new List<ColumnBase>();
            GridFilters filterList = new GridFilters();
            ModelFieldCollection _ModelFieldCollection = new ModelFieldCollection();

            //GetEmployeeMasterResult _SPParam = new GetEmployeeMasterResult();
            //List<DynamicReportFieldsBO> _listFieldsStaticColumns = DynamicReportManager.GetDynamicReportAvilableFields(_SPParam, new List<string> { "" });
            
            List<DynamicReportFieldsBO> _listFieldsStaticColumns = new List<DynamicReportFieldsBO>();
        


            //if (isDepartmentWise)
            //{
            //    foreach (Department _data in dataDepartment)
            //    {
                    DynamicReportFieldsBO _DynamicReportFieldsBO = new DynamicReportFieldsBO();
                    _DynamicReportFieldsBO.FieldName = "Branch/Department";
                    _DynamicReportFieldsBO.ServerMappingFields = "Branch/Department";
                    _DynamicReportFieldsBO.Caption = "Branch/Department";
                    _DynamicReportFieldsBO.DataType = "string";
                    _listFieldsStaticColumns.Add(_DynamicReportFieldsBO);
            //    }
            //}

            
           // List<DynamicReportFieldsBO> _listFieldsDynamicColumns = DynamicReportManager.GetDynamicFieldsProperty();


            //List<GetEmployeeMasterOtherHeadersResult> _testdynamicTable = EmployeeManager.GetEmployeeMasterHeader();

            List<DynamicReportFieldsBO> _listFieldsDynamicColumns = new List<DynamicReportFieldsBO>();
            foreach (CalcGetHeaderListResult _item in _dynamicTable)
            {
                DynamicReportFieldsBO _DynamicReportFields = new DynamicReportFieldsBO();
                _DynamicReportFields.FieldName = _item.Type + ":" + _item.SourceId; //_item.HeaderName;
                _DynamicReportFields.ServerMappingFields = _item.Type + ":" + _item.SourceId;//_item.HeaderName.Replace(" ", "");
                _DynamicReportFields.Caption = _item.HeaderName;
                _DynamicReportFields.DataType = "string";

                _listFieldsDynamicColumns.Add(_DynamicReportFields);
            }
     

            List<DynamicReportFieldsBO> _listFields = new List<DynamicReportFieldsBO>();
            _listFields.AddRange(_listFieldsStaticColumns);
            _listFields.AddRange(_listFieldsDynamicColumns);

            //string GetType = "FAMS_GetAssetSearchListReportSPResult";
            //DataTable dt = GetDataTableForExcel(_newObjList, _ExcelhiddenColsList,_listFields);

            DataTable dt = GetDataTableForExcel(_newObjList, _listFields);
            ExcelHelper.ExportDynamicDtToExcel(dt);



            //if (data.Any())
            //{
            //    ExcelHelper.ExportToExcelDynamic<Object>("Asset List", _newObjList,
            //       _ExcelhiddenColsList,
            //        new List<String>() { },
            //       columnRenameList,
            //    //    new Dictionary<string, string>() { { "AssetName", "Asset Name" }, { "AssetNumber", "Asset Number" }, { "ClassOrGroupRef_Name", "Class" }, 
            //    //{ "SysNo", "System Number" },{"LocationRef_Name","Location"}, {"GLAssetAccount","G/L Asset Account"}, {"AcquisitionDate","Acquisition Date"},{"CapitalizedDate","Capitalized Date"},{"AcquisitionCost","Acquisition Cost"} },
            //        new List<string>() { }
            //        , new Dictionary<string, string>() { }
            //        , PropertyNamesOrderList);
            //}
            //,new List<string> { "SysNo", "AssetName", "AssetNumber", "ClassOrGroupRef_Name", "LocationRef_Name" });

        }
        protected void Store_ReadDataDynamic(object sender, StoreReadDataEventArgs e)
        {


             //PayrollPeriod newPayrollPeriod = new PayrollPeriod();
             //CustomDate _customDate = new CustomDate();

            GridPanel1.Show();
             CustomDate date1 = CustomDate.GetCustomDateFromString(txtFromDate.Text.Trim(),IsEnglish);
             PayrollPeriod startPeriod = CommonManager.GetPayrollPeriod(date1.Month, date1.Year);

           
             int PayrollPeriodId = 0, PayrollPeriodIdEnd = 0;//need to remove later

             CustomDate date2 = CustomDate.GetCustomDateFromString(txtToDate.Text.Trim(), IsEnglish);
             PayrollPeriod EndPeriod = CommonManager.GetPayrollPeriod(date2.Month, date2.Year);
             if (startPeriod == null || EndPeriod == null)
             {
                 NewMessage.ShowNormalMessage("Date "+ txtFromDate.Text + "-" + txtToDate.Text +" is not in payroll period");
                 return;
             }
             else
             {
                 PayrollPeriodId = startPeriod.PayrollPeriodId;
                 PayrollPeriodIdEnd = EndPeriod.PayrollPeriodId;
             }


            List<Branch> dataBranch = new List<Branch>();
            List<Department> dataDepartment = new List<Department>();
            List<Object> objList = new List<object>();
            bool isDepartmentWise = false;

             if (!string.IsNullOrEmpty(cmbShow.SelectedItem.Value))
             {
                 if (cmbShow.SelectedItem.Value == "1")//departmentWise---show department
                 {
                     isDepartmentWise = true;
                     DepartmentManager _DepartmentManager = new DepartmentManager();
                     if (!string.IsNullOrEmpty(cmbDepartment.SelectedItem.Value))
                         dataDepartment = _DepartmentManager.GetDepartmentsByCompany(SessionManager.CurrentCompanyId).Where(x => x.DepartmentId == int.Parse(cmbDepartment.SelectedItem.Value)).ToList();
                     else
                         dataDepartment = _DepartmentManager.GetDepartmentsByCompany(SessionManager.CurrentCompanyId);

                     for (int i = 0; i < dataDepartment.Count; i++)
                         objList.Add(dataDepartment[i]);
                 }

                 else//branchwise show branch
                 {

                     isDepartmentWise =false;
                     if (!string.IsNullOrEmpty(cmbBranch.SelectedItem.Value))
                         dataBranch = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId).Where(x => x.BranchId == int.Parse(cmbBranch.SelectedItem.Value)).ToList();
                     else
                         dataBranch = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);

                     for (int i = 0; i < dataBranch.Count; i++)
                         objList.Add(dataBranch[i]);
                 }
             }

            List<object> _newObjList = new List<object>();
            List<CalcGetHeaderListResult> _dynamicTable = ReportManager.GetHeaderList(PayrollPeriodId, PayrollPeriodIdEnd);
            _dynamicTable = CalculationValue.SortHeaders(_dynamicTable, PayManager.GetIncomeOrderValues(), PayManager.GetDeductionOrderValues());
             
            int FixedListCols = 1;
            int DynamicCols = _dynamicTable.Count();
            int TotalFields = FixedListCols + DynamicCols;

             PropertyInfo[] properties = null;

             if (isDepartmentWise)
                 properties = dataDepartment[0].GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
             else
                 properties = dataBranch[0].GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);



              int BranchID, DepartmentID, ShowType;

              if (string.IsNullOrEmpty(cmbBranch.SelectedItem.Value))
                  BranchID = -1;
              else
                  BranchID = int.Parse(cmbBranch.SelectedItem.Value);

              if (string.IsNullOrEmpty(cmbDepartment.SelectedItem.Value))
                  DepartmentID = -1;
              else
                  DepartmentID = int.Parse(cmbDepartment.SelectedItem.Value);

              ShowType = int.Parse(cmbShow.SelectedItem.Value);

              if (isDepartmentWise)
              {

                  List<Report_Pay_DepartmentBranchWiseResult> _ListReportPayBranchDepartmentWise = ReportManager.ReportPayDepartmentBranchWise(PayrollPeriodId, PayrollPeriodIdEnd, -1, -1, ShowType).ToList();


                  foreach (object item in dataDepartment)
                  {
                      Array stringArray = Array.CreateInstance(typeof(object), TotalFields);

                      for (int col = 0; col <= TotalFields - 1; col++)
                      {
                          if (col <= FixedListCols - 1)
                          {
                              object o = properties[col].GetValue(item, null);
                              string cellValue = o == null ? null : o.ToString();
                              stringArray.SetValue(((DAL.Department)(item)).Name, col);
                              //stringArray.SetValue(cellValue, col);
                          }
                          else
                          {
                              foreach (CalcGetHeaderListResult _Column in _dynamicTable)
                              {
                                  int ID = int.Parse(dataDepartment[0].GetType().GetProperty("DepartmentId").GetValue(item, null).ToString());
                                  string ValueList = _Column.TypeSourceId;
                                  List<EmployeeExtraFieldsValuesBO> _ListEmployeeExtraFieldsValuesBO = new List<EmployeeExtraFieldsValuesBO>();
                                  string[] arrayInnerValueList = ValueList.Split(':');
                                  if (arrayInnerValueList.Length > 0)
                                      {
                                          EmployeeExtraFieldsValuesBO _EmployeeExtraFieldsValuesBO = new EmployeeExtraFieldsValuesBO();
                                          _EmployeeExtraFieldsValuesBO.ID = ID;
                                          _EmployeeExtraFieldsValuesBO.TypeID = int.Parse(arrayInnerValueList[0]);
                                          _EmployeeExtraFieldsValuesBO.SourceID = int.Parse(arrayInnerValueList[1]);
                                          

                                          
                                          Report_Pay_DepartmentBranchWiseResult _GetValue = _ListReportPayBranchDepartmentWise.SingleOrDefault(x => x.SourceId == _EmployeeExtraFieldsValuesBO.SourceID 
                                              && x.Type == _EmployeeExtraFieldsValuesBO.TypeID && x.ID == ID);

                                          if (_GetValue != null)
                                          {
                                              stringArray.SetValue(string.Format("{0:N2}", _GetValue.Amount), col);
                                              
                                          }
                                          else
                                              stringArray.SetValue("-", col);
                                      }
                                  col++;
                              }
                          }
                      }
                      _newObjList.Add(stringArray);
                  }
              }
              else
              {
                  List<Report_Pay_DepartmentBranchWiseResult> _ListReportPayBranchDepartmentWise = ReportManager.ReportPayDepartmentBranchWise(PayrollPeriodId, PayrollPeriodIdEnd, -1, -1, ShowType).ToList();
                                        

                  foreach (object item in dataBranch)
                  {
                      Array stringArray = Array.CreateInstance(typeof(object), TotalFields);

                      for (int col = 0; col <= TotalFields - 1; col++)
                      {
                          if (col <= FixedListCols - 1)//setting array for fixed columns
                          {
                              object o = properties[col].GetValue(item, null);
                              string cellValue = o == null ? null : o.ToString();
                              stringArray.SetValue(((DAL.Branch)(item)).Name, col);
                          }

                          else
                          {
                              foreach (CalcGetHeaderListResult _Column in _dynamicTable)
                              {

                                  int ID = int.Parse(dataBranch[0].GetType().GetProperty("BranchId").GetValue(item, null).ToString());
                                  string ValueList = _Column.TypeSourceId;//_Column.GetType().GetProperty("TypeSourceID").GetValue(item, null).ToString();
                                  List<EmployeeExtraFieldsValuesBO> _ListEmployeeExtraFieldsValuesBO = new List<EmployeeExtraFieldsValuesBO>();
                               
                                      string[] arrayInnerValueList = ValueList.Split(':');
                                      if (arrayInnerValueList.Length > 0)
                                      {
                                          Report_Pay_DepartmentBranchWiseResult _GetValue = _ListReportPayBranchDepartmentWise.SingleOrDefault(x => x.SourceId ==  int.Parse(arrayInnerValueList[1]) && x.Type ==  int.Parse(arrayInnerValueList[0]) && x.ID==ID);
                                          if (_GetValue != null)
                                          {
                                              stringArray.SetValue(string.Format("{0:N2}", _GetValue.Amount), col);
                                          }
                                          else
                                              stringArray.SetValue("-", col);

                                      }
                                  col++;
                              }
                          }
                      }
                      _newObjList.Add(stringArray);
                  }

              }


            //---------------------------

            List<ColumnBase> columns = new List<ColumnBase>();
            GridFilters filterList = new GridFilters();
            ModelFieldCollection _ModelFieldCollection = new ModelFieldCollection();
            List<DynamicReportFieldsBO> _listFieldsStaticColumns = new List<DynamicReportFieldsBO>();
            DynamicReportFieldsBO _DynamicReportFieldsBO = new DynamicReportFieldsBO();
            _DynamicReportFieldsBO.FieldName = "Branch/Department";
            _DynamicReportFieldsBO.ServerMappingFields = "Branch/Department";
            _DynamicReportFieldsBO.Caption = "Branch/Department";
            _DynamicReportFieldsBO.DataType = "string";
            _listFieldsStaticColumns.Add(_DynamicReportFieldsBO);

            List<DynamicReportFieldsBO> _listFieldsDynamicColumns = new List<DynamicReportFieldsBO>();
            foreach (CalcGetHeaderListResult _item in _dynamicTable)
            {
                DynamicReportFieldsBO _DynamicReportFields = new DynamicReportFieldsBO();
                _DynamicReportFields.FieldName =_item.Type + ":" + _item.SourceId; //_item.HeaderName;
                _DynamicReportFields.ServerMappingFields = _item.Type + ":" + _item.SourceId;//_item.HeaderName.Replace(" ", "");
                _DynamicReportFields.Caption = _item.HeaderName;
                _DynamicReportFields.DataType = "string";

                _listFieldsDynamicColumns.Add(_DynamicReportFields);
            }
     

            List<DynamicReportFieldsBO> _listFields = new List<DynamicReportFieldsBO>();
            _listFields.AddRange(_listFieldsStaticColumns);
            _listFields.AddRange(_listFieldsDynamicColumns);
            List<string> _SelectedFields = new List<string>();

            if (X.IsAjaxRequest)
            {
                this.storeDynamic.RemoveFields();
            }


            int colCount = 1;
            foreach (DynamicReportFieldsBO _model in _listFields)
            {

                string ServerMappingField = _model.ServerMappingFields;
                string DataType = _model.DataType;
                ColumnBase _column = new Column();
                if (DataType.ToLower() == "date")
                {
                    _column = new DateColumn();
                    ((DateColumn)_column).Format = "MM/dd/yyyy";

                }
                _column.DataIndex = ServerMappingField;
               _column.Width = 100;
               _column.ToolTip = _model.FieldName;
                if (colCount == 1)
                {
                    _column.Lockable = true;
                    _column.Locked = true;
                    _column.Align = Alignment.Left;
                    _column.Width = 160;
                  
                   
                }

                if (colCount > 1)
                {
                    _column.Lockable = false;
                    _column.Locked = false;
                    _column.Align = Alignment.Right;
                }

                _column.Text = _model.Caption;
                 columns.Add(_column);

                if (DataType.ToLower() == "string")
                    storeDynamic.Fields.Add(ServerMappingField, "String");
                if (DataType.ToLower() == "float")
                    storeDynamic.Fields.Add(ServerMappingField, "Float");
                if (DataType.ToLower() == "date")
                    storeDynamic.Fields.Add(ServerMappingField, "Date");


               // gf.Local = true;

                ModelField _ModelField = new ModelField();
                _ModelField.Name = ServerMappingField;

                if (DataType.ToLower() == "string")
                    _ModelField.Type = ModelFieldType.String;
                if (DataType.ToLower() == "float")
                    _ModelField.Type = ModelFieldType.Float;
                if (DataType.ToLower() == "date")
                    _ModelField.Type = ModelFieldType.Date;
                this.AddField(_ModelField);
                colCount++;
            }

            if (_listFields.Any())
            {
                this.storeDynamic.RebuildMeta();
                e.Total = _newObjList.Count();
                DataTable _Data = GetDataTable(_newObjList, _listFields);
                if(e.Sort.Any())
                _Data.DefaultView.Sort = e.Sort[0].Property + " " + e.Sort[0].Direction;
                GridPanel1.GetStore().DataSource = _Data;
                storeDynamic.DataBind();
                this.GridPanel1.ColumnModel.Columns.AddRange(columns);
                storeDynamic.DataBind();
                if (X.IsAjaxRequest)
                {
                    // this.GridPanel1.Features.Add(filterList);
                     this.GridPanel1.Reconfigure();
                   //  X.AddScript(" if(storeDynamic!=null) storeDynamic.currentPage =" + PagingStart + ";");
                }
            }

        
        }


        private void GetDynamicColsvalue(string KeyValue)
        {

           string EmployeeID = KeyValue;

        }

        private void AddField(ModelField field)
        {
            if (X.IsAjaxRequest)
            {
                this.storeDynamic.AddField(field);
            }
            else
            {
                this.storeDynamic.Model[0].Fields.Add(field);
            }
        }

       
        //protected void cmbBranch_Change(object sender, DirectEventArgs e)
        //{
        //    cmbDepartment.Store[0].DataSource = DepartmentManager.GetAllDepartmentsByBranch(int.Parse(cmbBranch.SelectedItem.Value));
        //    cmbDepartment.Store[0].DataBind();

        //}


        protected void btnSearch_Click(object sender, DirectEventArgs e)
        {

            if (string.IsNullOrEmpty(txtFromDate.Text))
            {
                NewMessage.ShowNormalMessage("From is required.");
                return;
            }

            if (string.IsNullOrEmpty(txtToDate.Text))
            {
                NewMessage.ShowNormalMessage("To is required.");
                return;
            }

            GridPanel1.GetStore().Reload();
        }

       

        #region "ABSTRACT-METHOD"


     
        #endregion
    }
}
