﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Ext.Net;
using DAL;
using BLL;
using BLL.Base;
using BLL.Manager;
using Utils.Helper;
using Utils.Calendar;
//using Web.Report.Templates;
using System.IO;
using DevExpress.XtraPrinting;
using BLL.BO;
using System.Xml;
using System.Xml.Xsl;
using Newtonsoft.Json.Linq;
using System.Collections;
using System.Reflection;
using System.Data;
using Bll;
using Utils.Web;
using Newtonsoft.Json;

namespace Web.Report
{

    public partial class YearEndSummaryReport : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            LoadHeader();

            if (!X.IsAjaxRequest && !IsPostBack)
            {
                
                BindIncomeList();
                
            }
        }

        
        void BindIncomeList()
        {
           
        }

        protected void btnPDFPrint_Click(object sender, EventArgs e)
        {
            
        }


        protected Field OnCreateFilterableField(object sender, ColumnBase column, Field defaultField)
        {
            
                ((TextField)defaultField).Icon = Icon.Magnifier;
                return defaultField;
        }

        private DataTable CreateDataTable(List<object> data, List<GetYearEndSummaryHeaderResult> headerList)
        {
            DataTable dataTable = new DataTable();
            dataTable.TableName = "Report";

            dataTable.Columns.Add("SN", typeof(string));
            dataTable.Columns.Add("EIN", typeof(string));
            dataTable.Columns.Add("Name", typeof(string));

         

            

            // Create table cells, fill the header cells with text, bind the cells to data
            for (int i = 0; i < headerList.Count; i++)
            {

                DataColumn column = new DataColumn(headerList[i].PeriodTypeSourceId, typeof(decimal));
                column.Caption = headerList[i].Name;

                dataTable.Columns.Add(column);

            }

         
            //add rows
            object value;
            foreach (object[] row in data)
            {
                List<object> list = new List<object>();
                list.Add(row[0]);
                list.Add(row[1]);
                list.Add(row[2]);


                //int index = 14;
                for (int i = 0; i < headerList.Count; i++)
                {
                    value = row[i + 3];
                    list.Add(value);
                }

               
                dataTable.Rows.Add(list.ToArray());
            }

            return dataTable;

        }

        

        protected void btnDynamicExcelPrint_Click(object sender, EventArgs e)
        {


            int yearId = int.Parse(hiddenyear.Text);


            int? empId = null;

            if (cmbEmpSearch.SelectedItem != null && cmbEmpSearch.SelectedItem.Value != null)
                empId = int.Parse(cmbEmpSearch.SelectedItem.Value);

            List<object> _newObjList;
            List<GetYearEndSummaryListResult> empList;
            int totalRows;
            ProcessList(0, 99999, yearId, empId, out _newObjList, out empList, out totalRows);

            List<GetYearEndSummaryHeaderResult> headerList = ReportManager.GetYearEndSummaryHeaders(yearId);

            DataTable _Data = CreateDataTable(_newObjList, headerList);
            ExcelHelper.ExportDynamicDtToExcel(_Data, "Year End Summary.xls");

        }

        public void cmbYear_Change(object sender, EventArgs e)
        {
            string yr = cmbYear.SelectedItem.Value;
            int val = 0;
            if (int.TryParse(yr, out val) == false)
                return;

            int yearId = int.Parse(cmbYear.SelectedItem.Value);


            hiddenyear.Text = yearId.ToString();

            List<GetYearEndSummaryHeaderResult> headerList = ReportManager.GetYearEndSummaryHeaders(yearId);

           // gridList.ColumnModel.Columns.Clear();

            foreach (GetYearEndSummaryHeaderResult item in headerList)
            {
                DynamicModel.Fields.Add(new ModelField { Name = item.PeriodTypeSourceId, Type = ModelFieldType.Float });

                ColumnBase newCol = new Column();
                newCol.Sortable = false;
                newCol.MenuDisabled = true;
                newCol.Text = item.Name;
                newCol.Align = Alignment.Right;
                newCol.DataIndex = item.PeriodTypeSourceId;
                newCol.Width = new Unit(120);
                newCol.Renderer = new Renderer("getFormattedAmount1");

                if (item.Type == (int)TypeEnum.Salary)
                    newCol.Cls = "columnCls";
                else if (item.Type == (int)TypeEnum.TotalCash || item.Type == (int)TypeEnum.GrossIncome
                     || item.Type == (int)TypeEnum.TaxableAmount || item.Type == (int)TypeEnum.TotalTax)
                    newCol.Cls = "columnTotal";

                gridList.ColumnModel.Columns.Add(newCol);
            }
        }
        public void LoadHeader()
        {
            List<FinancialDate> list = new CommonManager().GetAllFinancialDates(); ;

            foreach (FinancialDate item in list)
                item.SetName(IsEnglish);

            // for nibl skip first financial year for tax calculation
            List<FinancialDate> listNew = new List<FinancialDate>();


            bool first = true;
            foreach (var item in list)
            {
              

                listNew.Add(item);

                first = false;
            }

            cmbYear.Store[0].DataSource = listNew;
            cmbYear.Store[0].DataBind();


            

        }

        enum TypeEnum
        {
            Salary = 1,
            AddOn = 2,
            TotalCash=3,
            DeemedIncome = 4,
            InsuranceIncome = 5,

            GrossIncome = 55,
            CIT = 66,


            InsuranceDeduction = 88,
            TaxableAmount = 100,
            SST = 111,
            TDS = 122,
            TotalTax = 133

        }
           
        protected void Store_ReadDataDynamic(object sender, StoreReadDataEventArgs e)
        {



            int yearId = int.Parse(hiddenyear.Text);


            int? empId = null;

            if (cmbEmpSearch.SelectedItem != null && cmbEmpSearch.SelectedItem.Value != null)
                empId = int.Parse(cmbEmpSearch.SelectedItem.Value);

            List<object> _newObjList;
            List<GetYearEndSummaryListResult> empList;
            int totalRows;
            ProcessList(e.Start,e.Limit, yearId, empId, out _newObjList, out empList, out totalRows);


            gridList.GetStore().DataSource = _newObjList;
            storeDynamic.DataBind();

            if (empList.Count > 0)
                e.Total = totalRows;
            else
                e.Total = 0;
        }

        private void ProcessList(int start,int limit, int yearId, int? empId, out List<object> _newObjList, out List<GetYearEndSummaryListResult> empList, out int totalRows)
        {


            List<Object> objList = new List<object>();



            _newObjList = new List<object>();


            List<GetYearEndSummaryHeaderResult> headerList = ReportManager.GetYearEndSummaryHeaders(yearId);

            empList = BLL.BaseBiz.PayrollDataContext.GetYearEndSummaryList(null, null, empId, start, limit).ToList();




            List<GetYearEndSummaryListDetailsResult> dataList = BLL.BaseBiz.PayrollDataContext.GetYearEndSummaryListDetails(
                String.Join(",", empList.Select(x => x.EmployeeId.ToString()).ToArray()), yearId
                ).ToList();



            
            foreach (var emp in empList)
            {
                //total = 0;
                //grossIncome = 0;
                //deductionTotal = 0;

                object[] row = new object[headerList.Count + 3];
                row[0] = emp.SN;
                row[1] = emp.EmployeeId;
                row[2] = emp.Name;

                int index = 3;

                decimal totalCashIncome = 0;
                decimal totalTax = 0;

                foreach (GetYearEndSummaryHeaderResult header in headerList)
                {
                    GetYearEndSummaryListDetailsResult dataRow = dataList.FirstOrDefault(x => x.EmployeeId == emp.EmployeeId
                        && x.Type == header.Type
                        && x.PayrollPeriodId == header.PayrollPeriodId
                        && x.SourceId == header.SourceId);

                    row[index] = dataRow == null ? 0 : dataRow.Amount;

                    if (dataRow != null)
                    {
                        if (dataRow.Type == (int)TypeEnum.Salary || dataRow.Type == (int)TypeEnum.AddOn)
                            totalCashIncome += Convert.ToDecimal(dataRow.Amount);
                        else if (dataRow.Type == (int)TypeEnum.DeemedIncome)
                            totalCashIncome += Convert.ToDecimal(dataRow.Amount);

                        if (dataRow.Type == (int)TypeEnum.SST)
                            totalTax += Convert.ToDecimal(dataRow.Amount);
                        if (dataRow.Type == (int)TypeEnum.TDS)
                            totalTax += Convert.ToDecimal(dataRow.Amount);
                        if (dataRow.Type == (int)TypeEnum.InsuranceIncome)
                        {
                            row[index] = GetInsuranceIncomeCompanyPayAmount(dataRow.Name).ToString("n2");

                            totalCashIncome += Convert.ToDecimal(row[index]);
                        }
                    }

                    if (header.Type == (int)TypeEnum.TotalCash)
                        row[index] = totalCashIncome.ToString("n2");
                    else if (header.Type == (int)TypeEnum.GrossIncome)
                        row[index] = totalCashIncome.ToString("n2");
                    else if (header.Type == (int)TypeEnum.TotalTax)
                        row[index] = totalTax.ToString("n2");

                    index++;


                }

                _newObjList.Add(row);
            }

            totalRows = empList[0].TotalRows.Value;

        }

        public decimal GetInsuranceIncomeCompanyPayAmount(string data)
        {
            if (data == null)
                return 0;

            string[] rows = data.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

            foreach (string strRow in rows)
            {
                AnnualTaxBO item = new AnnualTaxBO();

               
                
                string[] values = strRow.Split(new char[] { ':' });

                if (values.Length <= 1)
                    continue;


                if (values.Length >= 3)
                {
                    // Add On PF Deduction already added in Add-On month so skip here
                    if (values[0].ToLower().Contains("Insurance Income".ToLower())
                        && values[2].ToLower().Contains("None Cash Yearly".ToLower()))
                    {

                        string amount = values[1];
                        decimal amt = 0;

                        decimal.TryParse(amount, out amt);
                        return amt;

                    }

                }
            }

            return 0;
        }
        private void GetDynamicColsvalue(string KeyValue)
        {

           string EmployeeID = KeyValue;

        }

        private void AddField(ModelField field)
        {
            if (X.IsAjaxRequest)
            {
                this.storeDynamic.AddField(field);
            }
            else
            {
                this.storeDynamic.Model[0].Fields.Add(field);
            }
        }

       
        //protected void cmbBranch_Change(object sender, DirectEventArgs e)
        //{
        //    cmbDepartment.Store[0].DataSource = DepartmentManager.GetAllDepartmentsByBranch(int.Parse(cmbBranch.SelectedItem.Value));
        //    cmbDepartment.Store[0].DataBind();

        //}


        protected void btnSearch_Click(object sender, DirectEventArgs e)
        {


            //if (string.IsNullOrEmpty(txtFromDate.Text))
            //{
            //    NewMessage.ShowNormalMessage("From is required.");
            //    return;
            //}

            //if (string.IsNullOrEmpty(txtToDate.Text))
            //{
            //    NewMessage.ShowNormalMessage("To is required.");
            //    return;
            //}

            gridList.GetStore().Reload();
        }

       

        #region "ABSTRACT-METHOD"


     
        #endregion
    }
}
