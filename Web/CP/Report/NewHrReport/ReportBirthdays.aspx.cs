﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Ext.Net;
using DAL;
using BLL;
using BLL.Base;
using BLL.Manager;
using Utils.Helper;
using Utils.Calendar;
//using Web.Report.Templates;
using System.IO;
using DevExpress.XtraPrinting;
using BLL.BO;
using System.Xml;
using System.Xml.Xsl;
using Newtonsoft.Json.Linq;
using System.Collections;
using System.Reflection;
using System.Data;
using Bll;

namespace Web.Report
{



    public partial class ReportBirthdays : BasePage
    {

        public class BirthdayBO
        {
            public string ID { get; set; }
            public string Name { get; set; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {


        }



        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DateTime CurrentDate = GetCurrentDateForSalary();
            List<BirthdayBO> List = new List<BirthdayBO>();
            List<BirthdayBO> ListMonth1 = GetBrithDayListByDate(CurrentDate);
            List.AddRange(ListMonth1);
            DateTime PreviousMonth = CurrentDate.AddMonths(1);
            List<BirthdayBO> ListMonth2 = GetBrithDayListByDate(PreviousMonth);
            List.AddRange(ListMonth2);
            List<BirthdayBO> ListMonth3 = GetBrithDayListByDate(PreviousMonth.AddMonths(1));
            List.AddRange(ListMonth3);
            StoreGrid1.DataSource = List;
            StoreGrid1.DataBind();
        }

        protected List<BirthdayBO> GetBrithDayListByDate(DateTime Date)
        {

            DateTime start = new DateTime(Date.Year, Date.Month, 1);
            DateTime end = new DateTime(Date.Year, Date.Month, DateTime.DaysInMonth(Date.Year, Date.Month));

            //CustomDate startDate = CustomDate.GetTodayDate(true);
            //int month = startDate.Month;
            string MonthName = DateHelper.GetMonthName(start.Month, true); //DateHelper.GetMonthName(month, this.IsEnglish);//+ "-"  + startDate.Year;

            BirthdayBO _BirthDayBO1 = new BirthdayBO();
            List<BirthdayBO> _BirthDayBOList = new List<BirthdayBO>();
            _BirthDayBO1.ID = "month";
            _BirthDayBO1.Name = MonthName;
            _BirthDayBOList.Add(_BirthDayBO1);
            List<EEmployee> _EmployeeList = EmployeeManager.GetAllEmployeesForEmployeeImportExport().OrderBy(x => x.Name)
                .Where(x => x.DateOfBirthEng != null && x.DateOfBirthEng.Value.Month == start.Month)
                .OrderBy(x => x.DateOfBirthEng).ToList();

            _EmployeeList = _EmployeeList.OrderBy(x => x.DateOfBirthEng.Value.Day).ToList();

            foreach (EEmployee _EmployeeData in _EmployeeList)
            {

                BirthdayBO _BirthDayBO = new BirthdayBO();


                if (_EmployeeData.DateOfBirthEng != null)
                {
                    _BirthDayBO.Name = _EmployeeData.DateOfBirthEng.Value.Day + " " + _EmployeeData.Name
                         + " (" + ValidateBirthDate(_EmployeeData.DateOfBirthEng.Value, start).ToString() + ")";
                    _BirthDayBOList.Add(_BirthDayBO);

                    if (IsEnglish == false)
                    {
                        _BirthDayBO.Name += " ," +
                            CustomDate.ConvertEngToNep(new CustomDate(
                                _EmployeeData.DateOfBirthEng.Value.Day, _EmployeeData.DateOfBirthEng.Value.Month,
                            _EmployeeData.DateOfBirthEng.Value.Year, true)).ToStringMonthDaySkipYear();
                    }
                }

            }

            return _BirthDayBOList;

        }

        public int ValidateBirthDate(DateTime birthday,DateTime refer)
        {
           // DateTime now = DateTime.Today;
            int age = refer.Year - birthday.Year;
            if (birthday > refer.AddYears(-age)) age--;
            return age;
        }

        protected void btnExcel_Click(object sender, EventArgs e)
        {


            DateTime CurrentDate = GetCurrentDateForSalary();
            List<BirthdayBO> List = new List<BirthdayBO>();
            List<BirthdayBO> ListMonth1 = GetBrithDayListByDate(CurrentDate);
            List.AddRange(ListMonth1);
            DateTime NextMonth = CurrentDate.AddMonths(1);
            List<BirthdayBO> ListMonth2 = GetBrithDayListByDate(NextMonth);
            List.AddRange(ListMonth2);
            List<BirthdayBO> ListMonth3 = GetBrithDayListByDate(NextMonth.AddMonths(1));
            List.AddRange(ListMonth3);

            ExcelHelper.ExportToExcel("Birthdays", List,
            new List<String>() { "ID" },
            new List<String>() { },
            new Dictionary<string, string>() { },
            new List<string>() { }
            , new Dictionary<string, string>() { }
            , new List<string> { });
        }


        #region "ABSTRACT-METHOD"



        #endregion
    }
}

