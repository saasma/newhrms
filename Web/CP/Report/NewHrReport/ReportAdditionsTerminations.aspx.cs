﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Ext.Net;
using DAL;
using BLL;
using BLL.Base;
using BLL.Manager;
using Utils.Helper;
using Utils.Calendar;
//using Web.Report.Templates;
using System.IO;
using DevExpress.XtraPrinting;
using BLL.BO;
using System.Xml;
using System.Xml.Xsl;
using Newtonsoft.Json.Linq;
using System.Collections;
using System.Reflection;
using System.Data;
using Bll;

namespace Web.Report
{

    public class HeadCountMonthWiseBo1
    {
        public string MonthName { get; set; }
        public int HeadCount { get; set; }
        public int displayOrder { get; set; }
    }

    public partial class ReportAdditionsTerminations : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!X.IsAjaxRequest && !IsPostBack)
            {
                cmbBranch.Store[0].DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
                cmbBranch.Store[0].DataBind();

                DepartmentManager _DepartmentManager = new DepartmentManager();
                cmbDepartment.Store[0].DataSource = _DepartmentManager.GetDepartmentsByCompany(SessionManager.CurrentCompanyId);
                cmbDepartment.Store[0].DataBind();


                CustomDate date = CustomDate.GetTodayDate(IsEnglish).GetFirstDateOfThisMonth();
                CustomDate date2 = CustomDate.GetTodayDate(IsEnglish).GetLastDateOfThisMonth();
                txtFromDate.Text = date.ToString();
                txtToDate.Text = date2.ToString();
                btnSearch_Click(null, null);
            }
        }


        protected void btnExcelPrint1_Click(object sender, EventArgs e)
        {
            int BranchID = -1, DepartmentID = -1;
            if (!string.IsNullOrEmpty(cmbBranch.SelectedItem.Value))
                BranchID = int.Parse(cmbBranch.SelectedItem.Value);

            if (!string.IsNullOrEmpty(cmbDepartment.SelectedItem.Value))
                DepartmentID = int.Parse(cmbDepartment.SelectedItem.Value);

           // List<ReportNewHR_GetHeadCountResult> data = EmployeeManager.GetHeadCount(0, int.MaxValue, "", BranchID, DepartmentID, GetEngDate(GetCurrentDateForSalary().ToString())).ToList();
            List<ReportNewHR_GetTerminationListResult> data = EmployeeManager.GetEmployeeTerminations(0, int.MaxValue, "", BranchID, DepartmentID, GetEngDate(txtFromDate.Text), GetEngDate(txtToDate.Text)).ToList();

            ExcelHelper.ExportToExcel("Employee Terminations", data,
          new List<String>() { "EmployeeId", "SN", "Status" },
          new List<String>() { },
          new Dictionary<string, string>() { { "Position", "Job Title" }, { "HireDate", "Hire Date" }, { "TerminationDate", "Termination Date" } },
          new List<string>() { }
          , new Dictionary<string, string>() { }
          , new List<string> { "Name", "HireDate", "Position", "Branch", "Department","TerminationDate" });
        }

        protected void btnExcelPrint_Click(object sender, EventArgs e)
        {
            int BranchID = -1, DepartmentID = -1;
            if (!string.IsNullOrEmpty(cmbBranch.SelectedItem.Value))
                BranchID = int.Parse(cmbBranch.SelectedItem.Value);

            if (!string.IsNullOrEmpty(cmbDepartment.SelectedItem.Value))
                DepartmentID = int.Parse(cmbDepartment.SelectedItem.Value);

            //List<ReportNewHR_GetHeadCountResult> data = EmployeeManager.GetHeadCount(0, int.MaxValue, "", BranchID, DepartmentID, GetEngDate()).ToList();

            List<ReportNewHR_GetAdditionsListResult> data = EmployeeManager.GetEmployeeAdditions(0, int.MaxValue, "", BranchID, DepartmentID, GetEngDate(txtFromDate.Text), GetEngDate(txtToDate.Text)).ToList();
        
            ExcelHelper.ExportToExcel("Employee Additions", data,
          new List<String>() { "EmployeeId","SN","Status"},
          new List<String>() { },
          new Dictionary<string, string>() { { "Position", "Job Title" },{"HireDate","Hire Date"}},
          new List<string>() { }
          , new Dictionary<string, string>() { }
          , new List<string> { "Name", "HireDate", "Position", "Branch", "Department"});
        }

        protected void Store_ReadData1(object sender, StoreReadDataEventArgs e)
        {
            int BranchID = -1, DepartmentID = -1;
            if (!string.IsNullOrEmpty(cmbBranch.SelectedItem.Value))
                BranchID = int.Parse(cmbBranch.SelectedItem.Value);

            if (!string.IsNullOrEmpty(cmbDepartment.SelectedItem.Value))
                DepartmentID = int.Parse(cmbDepartment.SelectedItem.Value);

            string sortBy = "";
            if (e.Sort.Any())
                sortBy = e.Sort[0].Property + " " + e.Sort[0].Direction;



            List<ReportNewHR_GetTerminationListResult> dataTerminations = EmployeeManager.GetEmployeeTerminations(e.Start, e.Limit, sortBy, BranchID, DepartmentID, GetEngDate(txtFromDate.Text), GetEngDate(txtToDate.Text)).ToList();
            storeReport1.DataSource = dataTerminations;
            storeReport1.DataBind();


        }

        protected void Store_ReadDataDynamic(object sender, StoreReadDataEventArgs e)
        {

            int BranchID=-1,DepartmentID=-1;
            if (!string.IsNullOrEmpty(cmbBranch.SelectedItem.Value))
                BranchID = int.Parse(cmbBranch.SelectedItem.Value);

            if (!string.IsNullOrEmpty(cmbDepartment.SelectedItem.Value))
                DepartmentID = int.Parse(cmbDepartment.SelectedItem.Value);

            string sortBy = "";
            if (e.Sort.Any())
                sortBy = e.Sort[0].Property + " " + e.Sort[0].Direction;


            List<ReportNewHR_GetAdditionsListResult> dataAdditions = EmployeeManager.GetEmployeeAdditions(e.Start, e.Limit, sortBy, BranchID, DepartmentID,GetEngDate(txtFromDate.Text),GetEngDate(txtToDate.Text)).ToList();
            storeReport.DataSource = dataAdditions;
            storeReport.DataBind();


            //List<ReportNewHR_GetTerminationListResult> dataTerminations = EmployeeManager.GetEmployeeTerminations(e.Start, e.Limit, sortBy, BranchID, DepartmentID, GetEngDate(txtFromDate.Text), GetEngDate(txtToDate.Text)).ToList();
            //storeReport1.DataSource = dataTerminations;
            //storeReport1.DataBind();


          


        }

        protected void btnSearch_Click(object sender, DirectEventArgs e)
        {

            if (string.IsNullOrEmpty(txtFromDate.Text))
            {
                NewMessage.ShowNormalMessage("From Date is required.");
                return;
            }

            if (string.IsNullOrEmpty(txtToDate.Text))
            {
                NewMessage.ShowNormalMessage("To Date is required.");
                return;
            }

            lblTotalCount.Text = EmployeeManager.GetHeadCountByMonth(-1, -1, GetCurrentDateForSalary()).ToString();
            lblDate.Text = DateTime.Today.Date.ToLongDateString();
            ShowControls();
            LoadChart();
            GridPanel1.GetStore().Reload();
            GridPanel2.GetStore().Reload();
        }

        protected void LoadChart()
        {
              //-------------------for chart section

            int BranchID = -1, DepartmentID = -1;
            if (!string.IsNullOrEmpty(cmbBranch.SelectedItem.Value))
                BranchID = int.Parse(cmbBranch.SelectedItem.Value);

            if (!string.IsNullOrEmpty(cmbDepartment.SelectedItem.Value))
                DepartmentID = int.Parse(cmbDepartment.SelectedItem.Value);

            List<HeadCountMonthWiseBo1> _ChartList = new List<HeadCountMonthWiseBo1>();
            CustomDate startDate = CustomDate.GetCustomDateFromString(txtToDate.Text, IsEnglish);
                
                //CustomDate.GetCustomDateFromString(BLL.BaseBiz.GetAppropriateDate(GetCurrentDateForSalary()), IsEnglish);
           
               int i = 1;
               for (; i <= 12; i++)
               {
                   HeadCountMonthWiseBo1 _ChartBO = new HeadCountMonthWiseBo1();
                   int month = startDate.Month;


                   string MonthName = DateHelper.GetMonthName(month, this.IsEnglish);//+ "-"  + startDate.Year;

                   _ChartBO.MonthName = MonthName;
                   _ChartBO.HeadCount = EmployeeManager.GetHeadCountByMonth(BranchID, DepartmentID, startDate.GetLastDateOfThisMonth().EnglishDate);
                   _ChartBO.displayOrder = 13 - i;
                   startDate = startDate.GetFirstDateOfThisMonth();
                   startDate = startDate.DecrementByOneDay();

                   if (month > 1)
                       month -= 1;

                   if (month == 1)
                       month = 12;

                   if (month > 12)
                   {
                       month = 1;
                   }

                   _ChartList.Add(_ChartBO);
               }

               Store1.DataSource = _ChartList.OrderBy(x => x.displayOrder);
               Store1.DataBind();

               CustomDate startDate1 = CustomDate.GetCustomDateFromString(txtToDate.Text, IsEnglish);
            //CustomDate.GetCustomDateFromString(BLL.BaseBiz.GetAppropriateDate(GetCurrentDateForSalary()), IsEnglish);

            //---display pervious 3 years Head Count==============================================
            //LastDate of the Year;

            int LoopCnt = 12- startDate1.Month;
            for (int l = 1; l <= LoopCnt; l++)
               {
                   startDate1 = startDate1.GetLastDateOfThisMonth();
                   startDate1 = startDate1.IncrementByOneDay();

               }


               for (int l=1; l <= 12; l++)
               {
                   startDate1 = startDate1.GetFirstDateOfThisMonth();
                   startDate1 = startDate1.DecrementByOneDay();
                 
               }

               HeadCountMonthWiseBo1 _ChartBOYear = new HeadCountMonthWiseBo1();
               _ChartBOYear.MonthName = startDate1.Year.ToString();
               List<HeadCountMonthWiseBo1>_ChartListYear=  new List<HeadCountMonthWiseBo1>();
               
               _ChartBOYear.HeadCount = EmployeeManager.GetHeadCountByMonth(BranchID, DepartmentID, startDate1.GetLastDateOfThisMonth().EnglishDate);

               _ChartBOYear.displayOrder =3;
               _ChartListYear.Add(_ChartBOYear);

               for (int j=1; j <= 12; j++)
               {
                   startDate1 = startDate1.GetFirstDateOfThisMonth();
                   startDate1 = startDate1.DecrementByOneDay();

               }

               HeadCountMonthWiseBo1 _ChartBOYear1 = new HeadCountMonthWiseBo1();
               _ChartBOYear1.MonthName = startDate1.Year.ToString();
               _ChartBOYear1.HeadCount = EmployeeManager.GetHeadCountByMonth(BranchID, DepartmentID, startDate1.GetLastDateOfThisMonth().EnglishDate);
               _ChartBOYear1.displayOrder = 2;
               _ChartListYear.Add(_ChartBOYear1);

               for (int k=1; k <= 12; k++)
               {
                   startDate1 = startDate1.GetFirstDateOfThisMonth();
                   startDate1 = startDate1.DecrementByOneDay();

               }
               HeadCountMonthWiseBo1 _ChartBOYear2 = new HeadCountMonthWiseBo1();
               _ChartBOYear2.MonthName = startDate1.Year.ToString();
               _ChartBOYear2.HeadCount = EmployeeManager.GetHeadCountByMonth(BranchID, DepartmentID, startDate1.GetLastDateOfThisMonth().EnglishDate);
               _ChartBOYear2.displayOrder = 1;
               _ChartListYear.Add(_ChartBOYear2);
              //display yearWise;

              storeYearWiseChartCount.DataSource = _ChartListYear.OrderBy(x => x.displayOrder); ;
              storeYearWiseChartCount.DataBind();
            //================================================================
        }


        protected void ShowControls()
        {
            GridPanel1.Show();
            Chart1Panel.Show();
            GridPanel2.Show();
            string jsScript1 = "document.getElementById('h1').style.display = 'block';";
            string jsScript2 = "document.getElementById('h2').style.display = 'block';";
            X.AddScript(jsScript1);
            X.AddScript(jsScript2);
            ChartYearWiseCount.Show();
            btnSaveMonthWiseChart.Show();
            btnSaveYearWiseChart.Show();
            lnkExcelPrint.Show();
            lnkExcelPrint1.Show();
            lblYTD.Show();
        }

        #region "ABSTRACT-METHOD"


     
        #endregion
    }
}
