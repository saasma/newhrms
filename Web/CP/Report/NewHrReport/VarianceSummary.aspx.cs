﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BLL.Manager;
using Web.ReportDataSetTableAdapters;
using Web.Master;
using BLL;
using Web.CP.Report.Templates.Pay.Detail;
using Utils;
using BLL.Base;
using System.IO;
using Utils.Calendar;

namespace Web.Employee
{
    public partial class VarianceSummary : BasePage
    {
      
        protected void Page_Load(object sender, EventArgs e)
        {


            

            if (!IsPostBack)
            {
                Initialise();
            }



        }

        private void Initialise()
        {

            ddlPayrollPeriods.DataSource =
                   CommonManager.GetAllYear(SessionManager.CurrentCompanyId);
            ddlPayrollPeriods.DataBind();

            string period = Request.QueryString["PeriodId"];
            if (string.IsNullOrEmpty(period))
            {
                ListItem selItem = ddlPayrollPeriods.Items[ddlPayrollPeriods.Items.Count - 1];
                if (selItem != null)
                    selItem.Selected = true;
            }
            else
            {
                ddlPayrollPeriods.SelectedValue = period;
            }

            CommonManager mgr = new CommonManager();


            btnLoad_Click(null, null);

        }

        protected void btnLoad_Click(object sender,EventArgs e)
        {
            int perioid = int.Parse(ddlPayrollPeriods.SelectedValue);

            
            PayrollPeriod payrollPeriod = CommonManager.GetPayrollPeriod(perioid);

            lbl1.Text = payrollPeriod.Name;
            lbl2.Text = DateHelper.GetMonthsForOtherPayrollPeriod(payrollPeriod.Month, IsEnglish)
                        + " " +
                        DateManager.GetOtherYearForPayrollPeriod(payrollPeriod, IsEnglish);


            int? total = 0;
            List<SalaryVariance_SummaryResult> list = ReportManager.GetSalaryVarianceSummary(perioid, ref total);
            List<SalaryVariance_SalaryChangeResult> list1 = ReportManager.GetSalaryChange(perioid);

            lblRegularCount.Text = total.ToString();
            lblStopPaymentCount.Text = list.Where(x => x.Type == 1).Count().ToString();
            lblTotalCount.Text = (total + list.Where(x => x.Type == 1).Count()).ToString();

            lblZeroAdjCount.Text = list.Where(x => x.Type == 3).Count().ToString();
            lblRetCount.Text = list.Where(x => x.Type == 5).Count().ToString();
            lblThisMonthSalaryCalc.Text = list.Where(x => x.Type == 4).Count().ToString();
            lblSalaryIncCount.Text = list1.Count().ToString();


         
            gvwFullStopPayment.DataSource = list.Where(x => x.Type == 1).ToList();
            gvwFullStopPayment.DataBind();


            
            gvwPartialStopPayment.DataSource = list.Where(x => x.Type == 2).ToList();
            gvwPartialStopPayment.DataBind();

           
            gvwNetZero.DataSource = list.Where(x => x.Type == 3).ToList();
            gvwNetZero.DataBind();

           
            gvwThisMonth.DataSource = list.Where(x => x.Type == 4).ToList();
            gvwThisMonth.DataBind();


           
            gvwRetirement.DataSource = list.Where(x => x.Type == 5).ToList();
            gvwRetirement.DataBind();

            
          
            gvwSalaryInc.DataSource = list1.ToList();
            gvwSalaryInc.DataBind();
        }

        protected void btnApprove_Click(object sender, EventArgs e)
        {

            CalculationManager.ApproveSalary(CommonManager.GetLastPayrollPeriod().PayrollPeriodId);

            divWarningMsg.InnerHtml = "Salary has been approved, now saving can be done.";
            divWarningMsg.Hide = false;
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {

           
            DateTime date = DateTime.Now;
            // process to display sign off
            if (CommonManager.CompanySetting.EnablePayrollSignFeature)
            {
                bool isApprovalReqd = PayManager.HasSignOffNotBeingDoneForLatestChanges(ref date);

                if (SessionManager.User.EnablePayrollSignOff != null && SessionManager.User.EnablePayrollSignOff.Value
                    && isApprovalReqd)
                {

                    btnApprove.Visible = true;
                    btnApprove.OnClientClick = "if(confirm('Are you sure, you want to Approve the payroll?')==false) return false;";
                }
                else
                    btnApprove.Visible = false;

            }

        }

    }
}
