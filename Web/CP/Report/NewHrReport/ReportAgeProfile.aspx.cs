﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Ext.Net;
using DAL;
using BLL;
using BLL.Base;
using BLL.Manager;
using Utils.Helper;
using Utils.Calendar;
//using Web.Report.Templates;
using System.IO;
using DevExpress.XtraPrinting;
using BLL.BO;
using System.Xml;
using System.Xml.Xsl;
using Newtonsoft.Json.Linq;
using System.Collections;
using System.Reflection;
using System.Data;
using Bll;

namespace Web.Report
{

    public class AgeProfileBo
    {
        public string Name { get; set; }
        public int Data1 { get; set; }
        public int displayOrder { get; set; }
    }

    public class AgeHeadCountByDepartmentBo
    {
        public string Department { get; set; }
        public int Under18 { get; set; }
        public int EightTo25 { get; set; }
        public int TwentySixTo34 { get; set; }
        public int ThirtyFiveTo44 { get; set; }
        public int FourtyFiveTo54 { get; set; }
        public int FiftyFiveTo64 { get; set; }
        public int SixtyFiveAbove { get; set; }

    }

    public partial class ReportAgeProfile : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest && !IsPostBack)
            {
                gender.InnerHtml = string.Format(gender.InnerHtml, CommonManager.GetGenderName);

                cmbBranch.Store[0].DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
                cmbBranch.Store[0].DataBind();

                DepartmentManager _DepartmentManager = new DepartmentManager();
                cmbDepartment.Store[0].DataSource = _DepartmentManager.GetDepartmentsByCompany(SessionManager.CurrentCompanyId);
                cmbDepartment.Store[0].DataBind();

                txtFromDate.Text = CustomDate.GetTodayDate(IsEnglish).ToString();
                txtToDate.Text = txtFromDate.Text;

                btnView_Click(null, null);
            }  
        }

    


        protected void btnExcelPrint1_Click(object sender, EventArgs e)
        {
            int branchId = -1, departmentId = -1, employeeId = -1;

            if (!string.IsNullOrEmpty(cmbBranch.SelectedItem.Value))
                branchId = int.Parse(cmbBranch.SelectedItem.Value);

            if (!string.IsNullOrEmpty(cmbDepartment.SelectedItem.Value))
                departmentId = int.Parse(cmbDepartment.SelectedItem.Value);

            if (!string.IsNullOrEmpty(cmbEmpSearch.SelectedItem.Value))
                employeeId = int.Parse(cmbEmpSearch.SelectedItem.Value);

            DateTime fromDate = GetEngDate(txtFromDate.Text.Trim());
            DateTime toDate = GetEngDate(txtToDate.Text.Trim());

            List<ReportNewHR_GetGenderProfileResult> data = EmployeeManager.GetGenderProfile(branchId, fromDate, toDate, departmentId, employeeId);


            //bind gender chart;
            StoreChart1.DataSource = data;
            StoreChart1.DataBind();

            ReportNewHR_GetGenderProfileResult _ReportNewHR_GetGenderProfileResult = new ReportNewHR_GetGenderProfileResult();
            _ReportNewHR_GetGenderProfileResult.Name = "Total";
            var TotalCount = (from List in data
                              select List.ValueCount).Sum();

            _ReportNewHR_GetGenderProfileResult.ValueCount = TotalCount.Value;
            _ReportNewHR_GetGenderProfileResult.Percentage = "100%";

            data.Add(_ReportNewHR_GetGenderProfileResult);


            ExcelHelper.ExportToExcel("Head Count", data,
          new List<String>() { },
          new List<String>() { },
          new Dictionary<string, string>() { { "ValueCount", "Count" }},
          new List<string>() { }
          , new Dictionary<string, string>() { }
          , new List<string> { "Name", "ValueCount", "PerCentage"});
        }


        protected void btnExcelPrint2_Click(object sender, EventArgs e)
        {

            List<AgeHeadCountByDepartmentBo> _listAgeHeadCountByDepartmentBo = new List<AgeHeadCountByDepartmentBo>();

            //-------------------------------------

            string CheckedID = "";

            RadioGrpBranchDepartment.CheckedItems.ForEach(delegate(Radio radio)
            {
                CheckedID = radio.ID;
            });

            int branchId = -1, departmentId = -1, employeeId = -1;

            if (!string.IsNullOrEmpty(cmbBranch.SelectedItem.Value))
                branchId = int.Parse(cmbBranch.SelectedItem.Value);

            if (!string.IsNullOrEmpty(cmbDepartment.SelectedItem.Value))
                departmentId = int.Parse(cmbDepartment.SelectedItem.Value);

            if (!string.IsNullOrEmpty(cmbEmpSearch.SelectedItem.Value))
                employeeId = int.Parse(cmbEmpSearch.SelectedItem.Value);

            DateTime fromDate = GetEngDate(txtFromDate.Text.Trim());
            DateTime toDate = GetEngDate(txtToDate.Text.Trim());

            //if departmentWise
            if (CheckedID == "RadioDepartmentWise")
            {
                List<Department> _DepartmentList = DepartmentManager.GetAllDepartments().Where(x => (departmentId == -1 || x.DepartmentId == departmentId)).ToList();

                foreach (Department _Department in _DepartmentList)
                {
                    AgeHeadCountByDepartmentBo _AgeHeadCountByDepartmentBo = new AgeHeadCountByDepartmentBo();
                    int under18 = EmployeeManager.GetAgeGroupHeadByDepartment(branchId, _Department.DepartmentId, toDate, 0, 18, fromDate, employeeId);//0-18
                    int EighteenTo25 = EmployeeManager.GetAgeGroupHeadByDepartment(branchId, _Department.DepartmentId, toDate, 18, 25, fromDate, employeeId);//0-18
                    int TwentySix34 = EmployeeManager.GetAgeGroupHeadByDepartment(branchId, _Department.DepartmentId, toDate, 26, 34, fromDate, employeeId);//0-18
                    int ThirtyFive44 = EmployeeManager.GetAgeGroupHeadByDepartment(branchId, _Department.DepartmentId, toDate, 35, 44, fromDate, employeeId);//0-18
                    int FourtyFive54 = EmployeeManager.GetAgeGroupHeadByDepartment(branchId, _Department.DepartmentId, toDate, 45, 54, fromDate, employeeId);//0-18
                    int FiftyFiveTo64 = EmployeeManager.GetAgeGroupHeadByDepartment(branchId, _Department.DepartmentId, toDate, 55, 64, fromDate, employeeId);//0-18
                    int SixtyFiveAbov = EmployeeManager.GetAgeGroupHeadByDepartment(branchId, _Department.DepartmentId, toDate, 65, 200, fromDate, employeeId);//0-18


                    _AgeHeadCountByDepartmentBo.Department = _Department.Name;
                    _AgeHeadCountByDepartmentBo.Under18 = under18;
                    _AgeHeadCountByDepartmentBo.EightTo25 = EighteenTo25;
                    _AgeHeadCountByDepartmentBo.TwentySixTo34 = TwentySix34;
                    _AgeHeadCountByDepartmentBo.ThirtyFiveTo44 = ThirtyFive44;
                    _AgeHeadCountByDepartmentBo.FourtyFiveTo54 = FourtyFive54;
                    _AgeHeadCountByDepartmentBo.FiftyFiveTo64 = FiftyFiveTo64;
                    _AgeHeadCountByDepartmentBo.SixtyFiveAbove = SixtyFiveAbov;
                    _listAgeHeadCountByDepartmentBo.Add(_AgeHeadCountByDepartmentBo);
                }
            }
            else
            {
                //BranchWise

                List<Branch> _BranchList = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId).Where(x => (branchId == -1 || x.BranchId == branchId)).ToList();

                foreach (Branch _Branch in _BranchList)
                {
                    AgeHeadCountByDepartmentBo _AgeHeadCountByDepartmentBo = new AgeHeadCountByDepartmentBo();
                    int under18 = EmployeeManager.GetAgeGroupHeadByDepartment(_Branch.BranchId, departmentId, toDate, 0, 18, fromDate, employeeId);//0-18
                    int EighteenTo25 = EmployeeManager.GetAgeGroupHeadByDepartment(_Branch.BranchId, departmentId, toDate, 18, 25, fromDate, employeeId);//0-18
                    int TwentySix34 = EmployeeManager.GetAgeGroupHeadByDepartment(_Branch.BranchId, departmentId, toDate, 26, 34, fromDate, employeeId);//0-18
                    int ThirtyFive44 = EmployeeManager.GetAgeGroupHeadByDepartment(_Branch.BranchId, departmentId, toDate, 35, 44, fromDate, employeeId);//0-18
                    int FourtyFive54 = EmployeeManager.GetAgeGroupHeadByDepartment(_Branch.BranchId, departmentId, toDate, 45, 54, fromDate, employeeId);//0-18
                    int FiftyFiveTo64 = EmployeeManager.GetAgeGroupHeadByDepartment(_Branch.BranchId, departmentId, toDate, 55, 64, fromDate, employeeId);//0-18
                    int SixtyFiveAbov = EmployeeManager.GetAgeGroupHeadByDepartment(_Branch.BranchId, departmentId, toDate, 65, 200, fromDate, employeeId);//0-18


                    _AgeHeadCountByDepartmentBo.Department = _Branch.Name;
                    _AgeHeadCountByDepartmentBo.Under18 = under18;
                    _AgeHeadCountByDepartmentBo.EightTo25 = EighteenTo25;
                    _AgeHeadCountByDepartmentBo.TwentySixTo34 = TwentySix34;
                    _AgeHeadCountByDepartmentBo.ThirtyFiveTo44 = ThirtyFive44;
                    _AgeHeadCountByDepartmentBo.FourtyFiveTo54 = FourtyFive54;
                    _AgeHeadCountByDepartmentBo.FiftyFiveTo64 = FiftyFiveTo64;
                    _AgeHeadCountByDepartmentBo.SixtyFiveAbove = SixtyFiveAbov;
                    _listAgeHeadCountByDepartmentBo.Add(_AgeHeadCountByDepartmentBo);
                }

            }


            ExcelHelper.ExportToExcel("Report_Age_Profile", _listAgeHeadCountByDepartmentBo,
          new List<String>() {  },
          new List<String>() { },
          new Dictionary<string, string>() { { "Department", "Branch/Department" }, { "Under18", "Under 18" }, { "EightTo25", "18-25" }, { "TwentySixTo34", "26-34" }
              , { "ThirtyFiveTo44", "35-44" }, { "FourtyFiveTo54", "45-54" }, { "FiftyFiveTo64", "55-64" }, { "SixtyFiveAbove", "65+" } },
          new List<string>() { }
          , new Dictionary<string, string>() { }
          , new List<string> {  });
        }

        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {

             List<GenderHeadCountByDepartmentBo> _listGenderHeadCountByDepartmentBo = new List<GenderHeadCountByDepartmentBo>();
             List<AgeProfileBo> _AgeProfileBoList = new List<AgeProfileBo>();

             int branchId = -1, departmentId = -1, employeeId = -1;

             if (!string.IsNullOrEmpty(cmbBranch.SelectedItem.Value))
                 branchId = int.Parse(cmbBranch.SelectedItem.Value);

             if (!string.IsNullOrEmpty(cmbDepartment.SelectedItem.Value))
                 departmentId = int.Parse(cmbDepartment.SelectedItem.Value);

             if (!string.IsNullOrEmpty(cmbEmpSearch.SelectedItem.Value))
                 employeeId = int.Parse(cmbEmpSearch.SelectedItem.Value);

             DateTime fromDate = GetEngDate(txtFromDate.Text.Trim());
             DateTime toDate = GetEngDate(txtToDate.Text.Trim());

                    AgeProfileBo _AgeProfileBo = new AgeProfileBo();
                    int Below18 = EmployeeManager.GetAgeGroupHeadByDepartment(branchId, departmentId, toDate, 0, 18, fromDate, employeeId);//0-18
                    _AgeProfileBo.Name = "under 18";
                    _AgeProfileBo.Data1 = Below18;
                    _AgeProfileBoList.Add(_AgeProfileBo);

                    AgeProfileBo _AgeProfileBo1 = new AgeProfileBo();
                    int EightTo25 = EmployeeManager.GetAgeGroupHeadByDepartment(branchId, departmentId, toDate, 18, 25, fromDate, employeeId);//18-25
                    _AgeProfileBo1.Name = "18-25";
                    _AgeProfileBo1.Data1 = EightTo25;
                    _AgeProfileBoList.Add(_AgeProfileBo1);

                    AgeProfileBo _AgeProfileBo2 = new AgeProfileBo();

                    int TwentySixTo34 = EmployeeManager.GetAgeGroupHeadByDepartment(branchId, departmentId, toDate, 26, 34, fromDate, employeeId);//26-34
                    _AgeProfileBo2.Name = "26-34";
                    _AgeProfileBo2.Data1 = TwentySixTo34;
                    _AgeProfileBoList.Add(_AgeProfileBo2);


                    AgeProfileBo _AgeProfileBo3 = new AgeProfileBo();
                    int ThirtyFiveTo44 = EmployeeManager.GetAgeGroupHeadByDepartment(branchId, departmentId, toDate, 35, 44, fromDate, employeeId);//35-44
                    _AgeProfileBo3.Name = "35-44";
                    _AgeProfileBo3.Data1 = ThirtyFiveTo44;
                    _AgeProfileBoList.Add(_AgeProfileBo3);

                    AgeProfileBo _AgeProfileBo4 = new AgeProfileBo();
                    int FourtyFiveTO54 = EmployeeManager.GetAgeGroupHeadByDepartment(branchId, departmentId, toDate, 45, 54, fromDate, employeeId);//45-54
                    _AgeProfileBo4.Name = "45-54";
                    _AgeProfileBo4.Data1 = FourtyFiveTO54;
                    _AgeProfileBoList.Add(_AgeProfileBo4);

                    AgeProfileBo _AgeProfileBo5 = new AgeProfileBo();
                    int FiftyFiveTO64 = EmployeeManager.GetAgeGroupHeadByDepartment(branchId, departmentId, toDate, 55, 64, fromDate, employeeId);//55-64
                    _AgeProfileBo5.Name = "55-64";
                    _AgeProfileBo5.Data1 = FiftyFiveTO64;
                    _AgeProfileBoList.Add(_AgeProfileBo5);

                    AgeProfileBo _AgeProfileBo6 = new AgeProfileBo();
                    int SixtyFiveAbove = EmployeeManager.GetAgeGroupHeadByDepartment(branchId, departmentId, toDate, 65, 200, fromDate, employeeId);//65+
                    _AgeProfileBo6.Name = "65+";
                    _AgeProfileBo6.Data1 = SixtyFiveAbove;
                    _AgeProfileBoList.Add(_AgeProfileBo6);


                   //bind gender chart;
                    StoreChart1.DataSource = _AgeProfileBoList;
                    StoreChart1.DataBind();

          

           
         
            List<AgeHeadCountByDepartmentBo> _listAgeHeadCountByDepartmentBo = new List<AgeHeadCountByDepartmentBo>();

            //-------------------------------------

            string CheckedID = "";

            RadioGrpBranchDepartment.CheckedItems.ForEach(delegate(Radio radio)
            {
                CheckedID = radio.ID;
            });

          
            //if departmentWise
            if (CheckedID == "RadioDepartmentWise")
            {
                List<Department> _DepartmentList = DepartmentManager.GetAllDepartments().Where(x => (departmentId == -1 || x.DepartmentId == departmentId)).ToList();

                foreach (Department _Department in _DepartmentList)
                {
                    AgeHeadCountByDepartmentBo _AgeHeadCountByDepartmentBo = new AgeHeadCountByDepartmentBo();
                    int under18 = EmployeeManager.GetAgeGroupHeadByDepartment(branchId, _Department.DepartmentId, toDate, 0, 18, fromDate, employeeId);//0-18
                    int EighteenTo25 = EmployeeManager.GetAgeGroupHeadByDepartment(branchId, _Department.DepartmentId, toDate, 18, 25, fromDate, employeeId);//0-18
                    int TwentySix34 = EmployeeManager.GetAgeGroupHeadByDepartment(branchId, _Department.DepartmentId, toDate, 26, 34, fromDate, employeeId);//0-18
                    int ThirtyFive44 = EmployeeManager.GetAgeGroupHeadByDepartment(branchId, _Department.DepartmentId, toDate, 35, 44, fromDate, employeeId);//0-18
                    int FourtyFive54 = EmployeeManager.GetAgeGroupHeadByDepartment(branchId, _Department.DepartmentId, toDate, 45, 54, fromDate, employeeId);//0-18
                    int FiftyFiveTo64 = EmployeeManager.GetAgeGroupHeadByDepartment(branchId, _Department.DepartmentId, toDate, 55, 64, fromDate, employeeId);//0-18
                    int SixtyFiveAbov = EmployeeManager.GetAgeGroupHeadByDepartment(branchId, _Department.DepartmentId, toDate, 65, 200, fromDate, employeeId);//0-18


                    _AgeHeadCountByDepartmentBo.Department = _Department.Name;
                    _AgeHeadCountByDepartmentBo.Under18 = under18;
                    _AgeHeadCountByDepartmentBo.EightTo25 = EighteenTo25;
                    _AgeHeadCountByDepartmentBo.TwentySixTo34 = TwentySix34;
                    _AgeHeadCountByDepartmentBo.ThirtyFiveTo44 = ThirtyFive44;
                    _AgeHeadCountByDepartmentBo.FourtyFiveTo54 = FourtyFive54;
                    _AgeHeadCountByDepartmentBo.FiftyFiveTo64 = FiftyFiveTo64;
                    _AgeHeadCountByDepartmentBo.SixtyFiveAbove = SixtyFiveAbov;
                    _listAgeHeadCountByDepartmentBo.Add(_AgeHeadCountByDepartmentBo);
                }
            }
            else
            {
                //BranchWise

                List<Branch> _BranchList = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);

                foreach (Branch _Branch in _BranchList)
                {
                    AgeHeadCountByDepartmentBo _AgeHeadCountByDepartmentBo = new AgeHeadCountByDepartmentBo();
                    int under18 = EmployeeManager.GetAgeGroupHeadByDepartment(_Branch.BranchId, departmentId, toDate, 0, 18, fromDate, employeeId);//0-18
                    int EighteenTo25 = EmployeeManager.GetAgeGroupHeadByDepartment(_Branch.BranchId, departmentId, toDate, 18, 25, fromDate, employeeId);//0-18
                    int TwentySix34 = EmployeeManager.GetAgeGroupHeadByDepartment(_Branch.BranchId, departmentId, toDate, 26, 34, fromDate, employeeId);//0-18
                    int ThirtyFive44 = EmployeeManager.GetAgeGroupHeadByDepartment(_Branch.BranchId, departmentId, toDate, 35, 44, fromDate, employeeId);//0-18
                    int FourtyFive54 = EmployeeManager.GetAgeGroupHeadByDepartment(_Branch.BranchId, departmentId, toDate, 45, 54, fromDate, employeeId);//0-18
                    int FiftyFiveTo64 = EmployeeManager.GetAgeGroupHeadByDepartment(_Branch.BranchId, departmentId, toDate, 55, 64, fromDate, employeeId);//0-18
                    int SixtyFiveAbov = EmployeeManager.GetAgeGroupHeadByDepartment(_Branch.BranchId, departmentId, toDate, 65, 200, fromDate, employeeId);//0-18


                    _AgeHeadCountByDepartmentBo.Department = _Branch.Name;
                    _AgeHeadCountByDepartmentBo.Under18 = under18;
                    _AgeHeadCountByDepartmentBo.EightTo25 = EighteenTo25;
                    _AgeHeadCountByDepartmentBo.TwentySixTo34 = TwentySix34;
                    _AgeHeadCountByDepartmentBo.ThirtyFiveTo44 = ThirtyFive44;
                    _AgeHeadCountByDepartmentBo.FourtyFiveTo54 = FourtyFive54;
                    _AgeHeadCountByDepartmentBo.FiftyFiveTo64 = FiftyFiveTo64;
                    _AgeHeadCountByDepartmentBo.SixtyFiveAbove = SixtyFiveAbov;
                    _listAgeHeadCountByDepartmentBo.Add(_AgeHeadCountByDepartmentBo);
                }

            }

            GridPanel2.GetStore().DataSource = _listAgeHeadCountByDepartmentBo;
            GridPanel2.GetStore().DataBind();


            DataTable table = new DataTable();
            table.Columns.Add("Department", typeof(string));
            table.Columns.Add("Under 18", typeof(string));
            table.Columns.Add("18-25", typeof(string));
            table.Columns.Add("26-34", typeof(string));
            table.Columns.Add("35-44", typeof(string));
            table.Columns.Add("45-54", typeof(string));
            table.Columns.Add("55-64", typeof(string));
            table.Columns.Add("65+", typeof(string));

            // Here we add five DataRows.
            foreach (AgeHeadCountByDepartmentBo _list in _listAgeHeadCountByDepartmentBo)
            {
                table.Rows.Add(_list.Department,_list.Under18,_list.EightTo25,_list.TwentySixTo34,_list.ThirtyFiveTo44,_list.FourtyFiveTo54,_list.FiftyFiveTo64,_list.SixtyFiveAbove);
            }

            StoreChartDeparmentWise.DataSource = table;
            StoreChartDeparmentWise.DataBind();

        
            //================================================================


        }

        protected void RadioSelect_Change(object sender, DirectEventArgs e)
        {

            //Radio radio = (Radio)sender;
            //Store store = this.GridPanel1.GetStore();

            //if (radio.ID == "RadioDepartmentWise") //DepartmentWise
            //{

            //    store.DataSource = gridData().Take(3);
            //    store.DataBind();
            //}
        }

        protected void btnView_Click(object sender, DirectEventArgs e)
        {

            if (string.IsNullOrEmpty(txtFromDate.Text))
            {
                NewMessage.ShowNormalMessage("Date is required.");
                return;
            }
         

            ShowControls();
            GridPanel2.GetStore().Reload();
            
          
        }

        protected void ShowControls()
        {
            
            GridPanel2.Show();
            Chart1.Show();
            Chart2.Show();
            lnkSaveChart1.Show();
            lnkSaveChart2.Show();
            string jsScript1 = "document.getElementById('block1').style.display = 'block';";
            string jsScript2 = "document.getElementById('block2').style.display = 'block';";

            X.AddScript(jsScript1);
            X.AddScript(jsScript2);

        }

        #region "ABSTRACT-METHOD"


     
        #endregion
    }
}

