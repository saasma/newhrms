﻿<%@ Page Title="Genderwise Status Report" Language="C#" AutoEventWireup="true" CodeBehind="ReportGenderWiseStatus.aspx.cs"
    MasterPageFile="~/Master/NewDetails.Master" Inherits="Web.Report.ReportGenderWiseStatus" %>

<%@ Register Src="~/Controls/FooterCtl.ascx" TagName="FooterCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/HeaderCtl.ascx" TagName="HeaderCtl" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <ext:ResourcePlaceHolder ID="ResourcePlaceHolder1" runat="server" Mode="Script" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <script type="text/javascript">
        
               

    function searchList() {
            <%=PagingToolbar1.ClientID %>.doRefresh();
        }
       
    </script>
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4 id="header" runat="server">
                    Genderwise Service Status</h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="alert alert-info">
            <table class="fieldTable">
                <tr>
                    <td>
                        <ext:ComboBox ID="cmbStatusName" Width="200" runat="server" ValueField="StatusId"
                            LabelWidth="70" DisplayField="Name" FieldLabel="Status" LabelAlign="Left" LabelSeparator=""
                            ForceSelection="true" QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store6" runat="server">
                                    <Model>
                                        <ext:Model ID="Model6" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="StatusId" />
                                                <ext:ModelField Name="Name" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <Triggers>
                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                            </Triggers>
                            <Listeners>
                                <Select Handler="this.getTrigger(0).show();" />
                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                <TriggerClick Handler="if (index == 0) { 
                                                               this.clearValue(); 
                                                               this.getTrigger(0).hide();
                                                           }" />
                            </Listeners>
                        </ext:ComboBox>
                    </td>
                    <td>
                        <ext:Button runat="server" Width="100" Height="30" ID="btnLoad" Cls="btn btn-default btn-sm btn-sect"
                            Text="Load" OnClientClick="searchList();">
                            <Listeners>
                                <Click Handler="valGroup = 'LoadTimeSheet'; return CheckValidation();">
                                </Click>
                            </Listeners>
                        </ext:Button>
                    </td>
                    <td>
                        <ext:LinkButton ID="btnExport" runat="server" AutoPostBack="true" Icon="PageExcel"
                            Text="Export" OnClick="btnExcelPrint_Click">
                        </ext:LinkButton>
                    </td>
                </tr>
            </table>
        </div>
        <table>
            <tr>
                <td>
                    <ext:GridPanel ID="gridTimeSheet" runat="server" Cls="itemgrid" Width="1150" Scroll="None">
                        <Store>
                            <ext:Store ID="Store3" runat="server" AutoLoad="true" PageSize="100">
                                <Proxy>
                                    <ext:AjaxProxy Json="true" Url="~/Handler/ReportGenderWiseStatus.ashx">
                                        <ActionMethods Read="GET" />
                                        <Reader>
                                            <ext:JsonReader Root="data" TotalProperty="total" />
                                        </Reader>
                                    </ext:AjaxProxy>
                                </Proxy>
                                <AutoLoadParams>
                                    <ext:Parameter Name="start" Value="0" Mode="Raw" />
                                    <ext:Parameter Name="IsEmpView" Value="-1" Mode="Raw" />
                                </AutoLoadParams>
                                <Parameters>
                                    <ext:StoreParameter Name="StatusName" Value="#{cmbStatusName}.getValue()" Mode="Raw"
                                        ApplyMode="Always" />
                                </Parameters>
                                <Model>
                                    <ext:Model ID="Model1" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="StatusName" Type="String" />
                                            <ext:ModelField Name="Male" Type="string" />
                                            <ext:ModelField Name="Female" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel ID="ColumnModel2" runat="server">
                            <Columns>
                                <ext:Column ID="colEId" runat="server" Text="Service Status" Width="200" DataIndex="StatusName"
                                    Align="Left" Sortable="false" MenuDisabled="true">
                                </ext:Column>
                                <ext:Column ID="Column1" runat="server" Text="Male" Width="100" DataIndex="Male"
                                    Sortable="false" MenuDisabled="true">
                                </ext:Column>
                                <ext:Column ID="Column2" runat="server" Text="Female" Width="100" DataIndex="Female"
                                    Align="Left" Sortable="false" MenuDisabled="true">
                                </ext:Column>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar1" runat="server" PageSize="100" DisplayInfo="true"
                                Hidden="true" DisplayMsg="Displaying Status {0} - {1} of {2}" EmptyMsg="No Records to display">
                            </ext:PagingToolbar>
                        </BottomBar>
                    </ext:GridPanel>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
