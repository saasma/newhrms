﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReportGenderProfile.aspx.cs"
    MasterPageFile="~/Master/NewDetails.Master" Inherits="Web.Report.ReportGenderProfile"
    Title="Gender Profile" %>

<%@ Register Src="~/Controls/ContentHeader.ascx" TagName="ContentHeader" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <ext:ResourcePlaceHolder ID="ResourcePlaceHolder1" runat="server" Mode="Script" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <script>
      var storeReport = null;
      Ext.onReady(
        function () {        
            storeReport = <%= storeReport.ClientID %>;
        }
    ); 



   var tipRenderer = function (storeItem, item) {
            //calculate percentage.
            var total = 0;

            <%= ChartPieGender.ClientID %>.getStore().each(function (rec) {
                total += rec.get('ValueCount');
            });
            
            this.setTitle(storeItem.get('Name') + ': ' + Math.round(storeItem.get('ValueCount') / total * 100) + '%');
        };


        function RenderPercentage(storeItem, item,record)
        {
      
           var total = 0;

            <%= ChartPieGender.ClientID %>.getStore().each(function (rec) {
                total += rec.get('ValueCount');
            });
            
    
            var res = (Math.round( record.data.ValueCount / total * 100) + '%');
         
          return res;
        }

    function SavePieChart() {
        <%= ChartPieGender.ClientID %>.save({
            type: 'image/png'
        });
    }

    
    function SaveDepartmentChart() {
        <%= ChartDepartmentWise.ClientID %>.save({
            type: 'image/png'
        });
    }

    </script>
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4 runat="server" id="gender">
                    {0} Profile
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="alert alert-info">
            <table>
                <tr>
                    <td style="width: 160px">
                        <pr:CalendarExtControl FieldLabel="From Date" ID="txtFromDate" runat="server" Width="110"
                            LabelWidth="30" LabelAlign="Top" LabelSeparator="">
                        </pr:CalendarExtControl>
                    </td>
                    <td style="width: 160px">
                        <pr:CalendarExtControl FieldLabel="To" ID="txtToDate" runat="server" Width="110"
                            LabelAlign="Top" LabelSeparator="">
                        </pr:CalendarExtControl>
                    </td>
                    <td style="width: 180px">
                        <ext:ComboBox ID="cmbBranch" Width="170px" runat="server" ValueField="BranchId" DisplayField="Name"
                            FieldLabel="Branch" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                            QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store5" runat="server">
                                    <Model>
                                        <ext:Model ID="Model5" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="BranchId" />
                                                <ext:ModelField Name="Name" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <Triggers>
                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                            </Triggers>
                            <Listeners>
                                <Select Handler="this.getTrigger(0).show();" />
                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                <TriggerClick Handler="if (index == 0) { 
                                                               this.clearValue(); 
                                                               this.getTrigger(0).hide();
                                                           }" />
                            </Listeners>
                        </ext:ComboBox>
                    </td>
                    <td style="width: 180px">
                        <ext:ComboBox ID="cmbDepartment" Width="170px" runat="server" ValueField="DepartmentId"
                            DisplayField="Name" FieldLabel="Department" LabelAlign="top" LabelSeparator=""
                            ForceSelection="true" QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store6" runat="server">
                                    <Model>
                                        <ext:Model ID="Model6" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="DepartmentId" />
                                                <ext:ModelField Name="Name" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <Triggers>
                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                            </Triggers>
                            <Listeners>
                                <Select Handler="this.getTrigger(0).show();" />
                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                <TriggerClick Handler="if (index == 0) { 
                                                               this.clearValue(); 
                                                               this.getTrigger(0).hide();
                                                           }" />
                            </Listeners>
                        </ext:ComboBox>
                    </td>
                    <td>
                        <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                            <Proxy>
                                <ext:AjaxProxy Url="../../../Handler/EmpSearchID.ashx">
                                    <ActionMethods Read="GET" />
                                    <Reader>
                                        <ext:JsonReader Root="plants" TotalProperty="total" />
                                    </Reader>
                                </ext:AjaxProxy>
                            </Proxy>
                            <Model>
                                <ext:Model ID="Model1" IDProperty="EmployeeId" Name="ItemLineModel" runat="server">
                                    <Fields>
                                        <ext:ModelField Name="Name" Type="String" />
                                        <ext:ModelField Name="EmployeeId" Type="String" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                        <ext:ComboBox LabelSeparator="" ID="cmbEmpSearch" FieldLabel="Search Employee" EmptyText="Employee Name"
                            LabelAlign="Top" runat="server" DisplayField="Name" ValueField="EmployeeId" StoreID="storeSearch"
                            TypeAhead="false" Width="200" PageSize="9999" HideBaseTrigger="true" MinChars="1"
                            TriggerAction="All" ForceSelection="false">
                            <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                                <ItemTpl ID="ItemTpl2" runat="server">
                                    <Html>
                                        <tpl>
                                                                            <div class="search-item">
                                                                                            <span>{Name}</span>  
                                                                             </div>
					                                                </tpl>
                                    </Html>
                                </ItemTpl>
                            </ListConfig>
                            <Triggers>
                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                            </Triggers>
                            <Listeners>
                                <Select Handler="this.getTrigger(0).show();" />
                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                <TriggerClick Handler="if (index == 0) { 
                                                                           this.clearValue(); 
                                                                           this.getTrigger(0).hide();
                                                                       }" />
                            </Listeners>
                        </ext:ComboBox>
                    </td>
                    <td style="padding-top: 20px">
                        <ext:RadioGroup ID="RadioGrpBranchDepartment" runat="server" ColumnsNumber="2" Width="300">
                            <Items>
                                <ext:Radio ID="RadioDepartmentWise" runat="server" BoxLabel="Department Wise" Checked="true" />
                                <ext:Radio ID="RadioBranchWise" runat="server" BoxLabel="Branch Wise" />
                            </Items>
                        </ext:RadioGroup>
                    </td>
                    <td style="padding-top: 25px; width: 100px">
                        <ext:Button Width="70" ID="btnSearch" runat="server" Text="Search" Height="30">
                            <DirectEvents>
                                <Click OnEvent="btnSearch_Click">
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                    </td>
                </tr>
            </table>
        </div>
        <div style="padding-top: 10px">
        </div>
        <div style="">
            <div class="widget" id="block1" style="position: relative; overflow: visible; display: none;
                width: 1000px">
                <div class="widget-head">
                    <h4 class="heading glyphicons history">
                        <i></i>Company Male/Female Ratio</h4>
                </div>
                <div class="widget-body list" style="padding: 10px;">
                    <ext:Chart Width="350" Height="350" ID="ChartPieGender" runat="server" Animate="true"
                        Shadow="true" InsetPadding="60">
                        <Store>
                            <ext:Store ID="StorePieGender" runat="server" AutoDataBind="true">
                                <Model>
                                    <ext:Model ID="Model2" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Name" />
                                            <ext:ModelField Name="ValueCount" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <Series>
                            <ext:PieSeries AngleField="ValueCount" ShowInLegend="true" Donut="0" Highlight="true"
                                HighlightSegmentMargin="20">
                                <Label Field="Name" Display="Rotate" Contrast="true" Font="10px Arial" />
                                <Tips TrackMouse="true" Width="140" Height="28">
                                    <Renderer Fn="tipRenderer" />
                                </Tips>
                            </ext:PieSeries>
                        </Series>
                    </ext:Chart>
                    <div style="padding: 10px 0 10px 0">
                        <ext:LinkButton ID="lnkSaveChart1" Handler="SavePieChart()" runat="server" Text="Export Chart"
                            Hidden="true">
                        </ext:LinkButton>
                    </div>
                    <div style="text-align: right; width: 700px; padding-bottom: 10px">
                        <ext:LinkButton Icon="PageExcel" runat="server" Text="Export" OnClick="btnExcelPrint1_Click"
                            AutoPostBack="true">
                        </ext:LinkButton>
                    </div>
                    <ext:GridPanel ID="GridPanel1" runat="server" Border="false" Region="West" AutoScroll="true"
                        Hidden="true" Width="700">
                        <Store>
                            <ext:Store PageSize="50" runat="server" ID="storeReport" RemoteSort="true" OnReadData="Store_ReadDataDynamic"
                                AutoLoad="false">
                                <Model>
                                    <ext:Model ID="Model_SearchLedgerasdfa" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Name" Type="String" />
                                            <ext:ModelField Name="ValueCount" Type="String" />
                                            <ext:ModelField Name="Percentage" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:PageProxy>
                                    </ext:PageProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                        <ColumnModel ID="ColumnModel_Ledger" runat="server" Cls="gridheader">
                            <Columns>
                                <ext:Column ID="Column1" runat="server" Text="Gender" DataIndex="Name" Width="300" />
                                <ext:Column ID="Column_Document" runat="server" Text="Count" Align="Left" DataIndex="ValueCount"
                                    Width="200">
                                </ext:Column>
                                <ext:Column ID="Column2" runat="server" Text="Percentage" Align="Left" DataIndex="Percentage"
                                    Width="200">
                                    <Renderer Fn="RenderPercentage">
                                    </Renderer>
                                </ext:Column>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>
                </div>
                <br />
                <br />
            </div>
        </div>
        <br />
        <div>
            <br />
            <div class="widget" id="block2" style="width: 1000px;">
                <div class="widget-head">
                    <h4 class="heading glyphicons history">
                        <i></i>Male/Female head count by Branch/Department</h4>
                </div>
                <div class="widget-body list" style="padding: 10px">
                    <ext:Chart Height="200" Width="400" ID="ChartDepartmentWise" runat="server" Shadow="true"
                        Hidden="true" Animate="true">
                        <Store>
                            <ext:Store ID="StoreChartDeparmentWise" runat="server" AutoDataBind="true">
                                <Model>
                                    <ext:Model ID="Model3" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Male" />
                                            <ext:ModelField Name="Female" />
                                            <ext:ModelField Name="Other" />
                                            <ext:ModelField Name="Department" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <LegendConfig Position="Right" />
                        <Axes>
                            <ext:NumericAxis Fields="Male,Female,Other" Position="Bottom" Grid="true" Hidden="true">
                                <Label>
                                    <Renderer Handler="return String(value);" />
                                </Label>
                            </ext:NumericAxis>
                            <ext:CategoryAxis Fields="Department" Position="Left" />
                        </Axes>
                        <Series>
                            <ext:BarSeries Axis="Bottom" Gutter="80" XField="Department" YField="Male,Female,Other"
                                Stacked="true">
                                <Tips TrackMouse="true" Width="65" Height="28">
                                    <Renderer Handler="this.setTitle(String(item.value[1]))" />
                                </Tips>
                            </ext:BarSeries>
                        </Series>
                    </ext:Chart>
                    <div style="padding: 10px 0 10px 0">
                        <ext:LinkButton ID="lnkSaveChart2" runat="server" Handler="SaveDepartmentChart()"
                            Text="Export Chart" Hidden="true">
                        </ext:LinkButton>
                    </div>
                    <div style="text-align: right; width: 700px; padding-bottom: 10px">
                        <ext:LinkButton ID="LinkButton1" Icon="PageExcel" runat="server" Text="Export" OnClick="btnExcelPrint2_Click"
                            AutoPostBack="true">
                        </ext:LinkButton>
                    </div>
                    <ext:GridPanel ID="GridPanel2" runat="server" Border="false"  AutoScroll="true"
                       Width="700">
                        <Store>
                            <ext:Store runat="server" ID="StoreBranchDepartmentWiseHeadCount" RemoteSort="true"
                                AutoLoad="false">
                                <Model>
                                    <ext:Model ID="Model133" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Department" Type="String" />
                                            <ext:ModelField Name="Male" Type="Int" />
                                            <ext:ModelField Name="Female" Type="Int" />
                                            <ext:ModelField Name="Other" Type="Int" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel ID="ColumnModel1" runat="server" Cls="gridheader">
                            <Columns>
                                <ext:Column ID="Column3" runat="server" Text="Branch/Department" DataIndex="Department"
                                    Width="300" />
                                <ext:Column ID="Column4" runat="server" Text="Male" Align="Left" DataIndex="Male"
                                    Width="150">
                                </ext:Column>
                                <ext:Column ID="Column5" runat="server" Text="Female" Align="Left" DataIndex="Female"
                                    Width="150">
                                </ext:Column>
                                <ext:Column ID="Column6" runat="server" Text="Other" Align="Left" DataIndex="Other"
                                    Width="150">
                                </ext:Column>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>

                     <div style="text-align: right; width: 700px; padding-bottom: 10px;margin-top:20px">
                        <ext:LinkButton ID="btnExportGenderByLevelPosition" Icon="PageExcel" runat="server" Text="Export" OnClick="btnExportGenderByLevelPosition_Click"
                            AutoPostBack="true">
                        </ext:LinkButton>
                    </div>
                    <ext:GridPanel ID="GridPanelGenderByLevelPosition" runat="server" Border="false"  AutoScroll="true"
                        Width="700">
                        <Store>
                            <ext:Store  runat="server" ID="Store1" RemoteSort="true"
                                AutoLoad="true">
                                <Model>
                                    <ext:Model ID="Model4" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Name" Type="String" />
                                            <ext:ModelField Name="Number" Type="Int" />
                                            <ext:ModelField Name="Male" Type="Int" />
                                            <ext:ModelField Name="Female" Type="Int" />
                                            <ext:ModelField Name="ThirdGender" Type="Int" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel ID="ColumnModel2" runat="server" Cls="gridheader">
                            <Columns>
                                <ext:Column ID="Column7" runat="server" Text="Position" DataIndex="Name"
                                    Width="300" />
                                 <ext:Column ID="Column11" runat="server" Text="Number" Align="Center" DataIndex="Number"
                                    Width="100">
                                 </ext:Column>
                                <ext:Column ID="Column8" runat="server" Text="Male" Align="Center" DataIndex="Male"
                                    Width="100">
                                </ext:Column>
                                <ext:Column ID="Column9" runat="server" Text="Female" Align="Center" DataIndex="Female"
                                    Width="100">
                                </ext:Column>
                                <ext:Column ID="Column10" runat="server" Text="Other" Align="Center" DataIndex="ThirdGender"
                                    Width="100">
                                </ext:Column>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
