﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using Web.CP.Report.Templates.HR;
using BLL.BO;
using System.IO;
using Utils;
using System.Data;
using Utils.Calendar;
using DevExpress.XtraPrinting;
using System.Security.AccessControl;
using Utils.Helper;
using System.Text;
using Bll;


namespace Web.Report
{
    public partial class ReportGenderWiseStatus : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                cmbStatusName.GetStore().DataSource = CommonManager.GetStatusName();
                cmbStatusName.GetStore().DataBind();
            }
           
        }

        protected void btnExcelPrint_Click(object sender, EventArgs e)
        {
              
            string StatusName = "-1";

            if(!string.IsNullOrEmpty(cmbStatusName.SelectedItem.Value))
                StatusName = cmbStatusName.SelectedItem.Text;

            List<ReportNewHR_GenderWiseStatusResult> resultSet = NewHRManager.GetGenderWiseStatusReport(0, int.MaxValue, StatusName);
          ExcelHelper.ExportToExcel("Genderwise Status Report", resultSet,
          new List<String>() {"TotalRows" },
          new List<String>() {},
          new Dictionary<string, string>() { {"StatusName", "Service Status"}},
          new List<string>() { }
          , new Dictionary<string, string>() { }
          , new List<string> { "StatusName", "Male", "Female"});
        }

        







        


    }
}