﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Ext.Net;
using DAL;
using BLL;
using BLL.Base;
using BLL.Manager;
using Utils.Helper;
using Utils.Calendar;
//using Web.Report.Templates;
using System.IO;
using DevExpress.XtraPrinting;
using BLL.BO;
using System.Xml;
using System.Xml.Xsl;
using Newtonsoft.Json.Linq;
using System.Collections;
using System.Reflection;
using System.Data;
using Bll;

namespace Web.Report
{

    public class GenderProfileBo
    {
        public string Name { get; set; }
        public int Data1 { get; set; }
        public int displayOrder { get; set; }
    }

    public class GenderHeadCountByDepartmentBo
    {
        public string Department { get; set; }
        public int Male { get; set; }
        public int Female { get; set; }
        public int Other { get; set; }

    }

    public partial class ReportGenderProfile : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!X.IsAjaxRequest && !IsPostBack)
            {
                gender.InnerHtml = string.Format(gender.InnerHtml, CommonManager.GetGenderName);

                cmbBranch.Store[0].DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
                cmbBranch.Store[0].DataBind();

                DepartmentManager _DepartmentManager = new DepartmentManager();
                cmbDepartment.Store[0].DataSource = _DepartmentManager.GetDepartmentsByCompany(SessionManager.CurrentCompanyId);
                cmbDepartment.Store[0].DataBind();

                txtFromDate.Text = CustomDate.GetTodayDate(IsEnglish).ToString();
                txtToDate.Text = txtFromDate.Text;

                btnSearch_Click(null, null);
            }  
        }

    


        protected void btnExcelPrint1_Click(object sender, EventArgs e)
        {
            int branchId = -1, departmentId = -1, employeeId = -1;

            if (!string.IsNullOrEmpty(cmbBranch.SelectedItem.Value))
                branchId = int.Parse(cmbBranch.SelectedItem.Value);

            if (!string.IsNullOrEmpty(cmbDepartment.SelectedItem.Value))
                departmentId = int.Parse(cmbDepartment.SelectedItem.Value);

            if (!string.IsNullOrEmpty(cmbEmpSearch.SelectedItem.Value))
                employeeId = int.Parse(cmbEmpSearch.SelectedItem.Value);

            List<ReportNewHR_GetGenderProfileResult> data = EmployeeManager.GetGenderProfile(branchId, GetEngDate(txtFromDate.Text.Trim()), GetEngDate(txtToDate.Text.Trim()), departmentId, employeeId).ToList();
            //bind gender chart;
            StorePieGender.DataSource = data;
            StorePieGender.DataBind();

            ReportNewHR_GetGenderProfileResult _ReportNewHR_GetGenderProfileResult = new ReportNewHR_GetGenderProfileResult();
            _ReportNewHR_GetGenderProfileResult.Name = "Total";
            var TotalCount = (from List in data
                              select List.ValueCount).Sum();

            _ReportNewHR_GetGenderProfileResult.ValueCount = TotalCount.Value;
            _ReportNewHR_GetGenderProfileResult.Percentage = "100%";

            data.Add(_ReportNewHR_GetGenderProfileResult);


            ExcelHelper.ExportToExcel("Head Count", data,
          new List<String>() { },
          new List<String>() { },
          new Dictionary<string, string>() { { "ValueCount", "Count" }},
          new List<string>() { }
          , new Dictionary<string, string>() { }
          , new List<string> { "Name", "ValueCount", "PerCentage"});
        }


        protected void btnExcelPrint2_Click(object sender, EventArgs e)
        {

            List<GenderHeadCountByDepartmentBo> _listGenderHeadCountByDepartmentBo = new List<GenderHeadCountByDepartmentBo>();

            //-------------------------------------

            string CheckedID = "";

            RadioGrpBranchDepartment.CheckedItems.ForEach(delegate(Radio radio)
            {
                CheckedID = radio.ID;
            });

            int branchId = -1, departmentId = -1, employeeId = -1;

            if (!string.IsNullOrEmpty(cmbBranch.SelectedItem.Value))
                branchId = int.Parse(cmbBranch.SelectedItem.Value);

            if (!string.IsNullOrEmpty(cmbDepartment.SelectedItem.Value))
                departmentId = int.Parse(cmbDepartment.SelectedItem.Value);

            if (!string.IsNullOrEmpty(cmbEmpSearch.SelectedItem.Value))
                employeeId = int.Parse(cmbEmpSearch.SelectedItem.Value);
            DateTime fromDate = GetEngDate(txtFromDate.Text.Trim());
            DateTime toDate = GetEngDate(txtToDate.Text.Trim());

            //if departmentWise
            if (CheckedID == "RadioDepartmentWise")
            {
                List<Department> _DepartmentList = DepartmentManager.GetAllDepartments().Where(x => (departmentId == -1 || x.DepartmentId == departmentId)).ToList();

                foreach (Department _Department in _DepartmentList)
                {
                    GenderHeadCountByDepartmentBo _GenderHeadCountByDepartmentBo = new GenderHeadCountByDepartmentBo();
                    int MaleCount = EmployeeManager.GetGenderHeadByDepartment(branchId, _Department.DepartmentId, fromDate, 1, toDate, employeeId);//Male
                    int FemaleCount = EmployeeManager.GetGenderHeadByDepartment(branchId, _Department.DepartmentId, fromDate, 0, toDate, employeeId);//Female
                    int OtherCount = EmployeeManager.GetGenderHeadByDepartment(branchId, _Department.DepartmentId, fromDate, 2, toDate, employeeId);//Other
                    _GenderHeadCountByDepartmentBo.Department = _Department.Name;
                    _GenderHeadCountByDepartmentBo.Male = MaleCount;
                    _GenderHeadCountByDepartmentBo.Female = FemaleCount;
                    _GenderHeadCountByDepartmentBo.Other = OtherCount;
                    _listGenderHeadCountByDepartmentBo.Add(_GenderHeadCountByDepartmentBo);
                }
            }
            else
            {
                //BranchWise

                List<Branch> _BranchList = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId).Where(x => (branchId == -1 || x.BranchId == branchId)).ToList();

                foreach (Branch _Branch in _BranchList)
                {
                    GenderHeadCountByDepartmentBo _GenderHeadCountByDepartmentBo = new GenderHeadCountByDepartmentBo();
                    int MaleCount = EmployeeManager.GetGenderHeadByDepartment(_Branch.BranchId, departmentId, fromDate, 1, toDate, employeeId);//Male
                    int FemaleCount = EmployeeManager.GetGenderHeadByDepartment(_Branch.BranchId, departmentId, fromDate, 0, toDate, employeeId);//Female
                    int OtherCount = EmployeeManager.GetGenderHeadByDepartment(_Branch.BranchId, departmentId, fromDate, 2, toDate, employeeId);//Other
                    _GenderHeadCountByDepartmentBo.Department = _Branch.Name;
                    _GenderHeadCountByDepartmentBo.Male = MaleCount;
                    _GenderHeadCountByDepartmentBo.Other = OtherCount;
                    _GenderHeadCountByDepartmentBo.Female = FemaleCount;
                    _listGenderHeadCountByDepartmentBo.Add(_GenderHeadCountByDepartmentBo);
                }

            }

            ExcelHelper.ExportToExcel("Head Count", _listGenderHeadCountByDepartmentBo,
          new List<String>() {  },
          new List<String>() { },
          new Dictionary<string, string>() { { "Department", "Branch/Department" }},
          new List<string>() { }
          , new Dictionary<string, string>() { }
          , new List<string> { "Department", "Male", "Female", "Other" });
        }


        protected void btnExportGenderByLevelPosition_Click(object sender, EventArgs e)
        {

           


            int branchId = -1, departmentId = -1, employeeId = -1;

            if (!string.IsNullOrEmpty(cmbBranch.SelectedItem.Value))
                branchId = int.Parse(cmbBranch.SelectedItem.Value);

            if (!string.IsNullOrEmpty(cmbDepartment.SelectedItem.Value))
                departmentId = int.Parse(cmbDepartment.SelectedItem.Value);

            if (!string.IsNullOrEmpty(cmbEmpSearch.SelectedItem.Value))
                employeeId = int.Parse(cmbEmpSearch.SelectedItem.Value);
            DateTime fromDate = GetEngDate(txtFromDate.Text.Trim());
            DateTime toDate = GetEngDate(txtToDate.Text.Trim());


            List<GetGenderByPositionLevelResult> list = BLL.BaseBiz.PayrollDataContext
                .GetGenderByPositionLevel(toDate, fromDate, branchId, departmentId).ToList();

            GetGenderByPositionLevelResult total = new GetGenderByPositionLevelResult { Name = "Total", Number = 0, Male = 0, Female = 0, ThirdGender = 0 };
            
            foreach (var item in list)
            {
                total.Number += Convert.ToInt32(item.Number);
                total.Male += Convert.ToInt32(item.Male);
                total.Female += Convert.ToInt32(item.Female);
                total.ThirdGender += Convert.ToInt32(item.ThirdGender);
            }
            list.Add(total);

            ExcelHelper.ExportToExcel("Gender Count by Position", list,
          new List<String>() { "Order" },
          new List<String>() {  },
          new Dictionary<string, string>() { { "Name", "Position" }, { "ThirdGender", "Other" } },
          new List<string>() { }
          , new List<string> { "Number", "Male", "Female", "ThirdGender" }
          , new List<string> { }
          , new Dictionary<string, string>() { { "Gender Count by Position", "" } }
          , new List<string> { });
        }

        protected void Store_ReadDataDynamic(object sender, StoreReadDataEventArgs e)
        {
            int branchId = -1, departmentId = -1, employeeId = -1;

            if (!string.IsNullOrEmpty(cmbBranch.SelectedItem.Value))
                branchId = int.Parse(cmbBranch.SelectedItem.Value);

            if (!string.IsNullOrEmpty(cmbDepartment.SelectedItem.Value))
                departmentId = int.Parse(cmbDepartment.SelectedItem.Value);

            if (!string.IsNullOrEmpty(cmbEmpSearch.SelectedItem.Value))
                employeeId = int.Parse(cmbEmpSearch.SelectedItem.Value);
            DateTime fromDate = GetEngDate(txtFromDate.Text.Trim());
            DateTime toDate = GetEngDate(txtToDate.Text.Trim());


            List<ReportNewHR_GetGenderProfileResult> data = EmployeeManager.GetGenderProfile(branchId, fromDate, toDate, departmentId, employeeId);
            
            //bind gender chart;
            StorePieGender.DataSource = data;
            StorePieGender.DataBind();

            ReportNewHR_GetGenderProfileResult _ReportNewHR_GetGenderProfileResult = new ReportNewHR_GetGenderProfileResult();
            _ReportNewHR_GetGenderProfileResult.Name = "Total";
            var TotalCount = (from List in data
                             select List.ValueCount).Sum();

            _ReportNewHR_GetGenderProfileResult.ValueCount = TotalCount.Value;
            _ReportNewHR_GetGenderProfileResult.Percentage = "100%";

            data.Add(_ReportNewHR_GetGenderProfileResult);

            storeReport.DataSource = data;
            storeReport.DataBind();


            //Department Wise Gender head Count-----

           
         
            List<GenderHeadCountByDepartmentBo> _listGenderHeadCountByDepartmentBo = new List<GenderHeadCountByDepartmentBo>();

            //-------------------------------------

            string CheckedID = "";

            RadioGrpBranchDepartment.CheckedItems.ForEach(delegate(Radio radio)
            {
                CheckedID = radio.ID;
            });

          
            //if departmentWise
            if (CheckedID == "RadioDepartmentWise")
            {
                List<Department> _DepartmentList = DepartmentManager.GetAllDepartments().Where(x => (departmentId == -1 || x.DepartmentId == departmentId)).ToList();

                foreach (Department _Department in _DepartmentList)
                {
                    GenderHeadCountByDepartmentBo _GenderHeadCountByDepartmentBo = new GenderHeadCountByDepartmentBo();
                    int MaleCount = EmployeeManager.GetGenderHeadByDepartment(branchId, _Department.DepartmentId, fromDate, 1, toDate, employeeId);//Male
                    int FemaleCount = EmployeeManager.GetGenderHeadByDepartment(branchId, _Department.DepartmentId, fromDate, 0, toDate, employeeId);//Female
                    int OtherCount = EmployeeManager.GetGenderHeadByDepartment(branchId, _Department.DepartmentId, fromDate, 2, toDate, employeeId);//Other
                    _GenderHeadCountByDepartmentBo.Department = _Department.Name;
                    _GenderHeadCountByDepartmentBo.Male = MaleCount;
                    _GenderHeadCountByDepartmentBo.Female = FemaleCount;
                    _GenderHeadCountByDepartmentBo.Other = OtherCount;
                    _listGenderHeadCountByDepartmentBo.Add(_GenderHeadCountByDepartmentBo);
                }
            }
            else
            {
                //BranchWise

                List<Branch> _BranchList = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId).Where(x => (branchId == -1 || x.BranchId == branchId)).ToList();

                foreach (Branch _Branch in _BranchList)
                {
                    GenderHeadCountByDepartmentBo _GenderHeadCountByDepartmentBo = new GenderHeadCountByDepartmentBo();
                    int MaleCount = EmployeeManager.GetGenderHeadByDepartment(_Branch.BranchId, departmentId, fromDate, 1, toDate, employeeId);//Male
                    int FemaleCount = EmployeeManager.GetGenderHeadByDepartment(_Branch.BranchId, departmentId, fromDate, 0, toDate, employeeId);//Female
                    int OtherCount = EmployeeManager.GetGenderHeadByDepartment(_Branch.BranchId, departmentId, fromDate, 2, toDate, employeeId);//Other
                    _GenderHeadCountByDepartmentBo.Department = _Branch.Name;
                    _GenderHeadCountByDepartmentBo.Male = MaleCount;
                    _GenderHeadCountByDepartmentBo.Other = OtherCount;
                    _GenderHeadCountByDepartmentBo.Female = FemaleCount;
                    _listGenderHeadCountByDepartmentBo.Add(_GenderHeadCountByDepartmentBo);
                }

            }

            StoreChartDeparmentWise.DataSource = _listGenderHeadCountByDepartmentBo;
            StoreChartDeparmentWise.DataBind();

            GridPanel2.GetStore().DataSource = _listGenderHeadCountByDepartmentBo;
            GridPanel2.GetStore().DataBind();

            //================================================================


        }

        protected void RadioSelect_Change(object sender, DirectEventArgs e)
        {

            //Radio radio = (Radio)sender;
            //Store store = this.GridPanel1.GetStore();

            //if (radio.ID == "RadioDepartmentWise") //DepartmentWise
            //{

            //    store.DataSource = gridData().Take(3);
            //    store.DataBind();
            //}
        }

        protected void btnSearch_Click(object sender, DirectEventArgs e)
        {

            if (string.IsNullOrEmpty(txtFromDate.Text))
            {
                NewMessage.ShowNormalMessage("Date is required.");
                return;
            }
        

            ShowControls();
            GridPanel1.GetStore().Reload();
            
            int branchId = -1, departmentId = -1, employeeId = -1;

            if (!string.IsNullOrEmpty(cmbBranch.SelectedItem.Value))
                branchId = int.Parse(cmbBranch.SelectedItem.Value);

            if (!string.IsNullOrEmpty(cmbDepartment.SelectedItem.Value))
                departmentId = int.Parse(cmbDepartment.SelectedItem.Value);

            if (!string.IsNullOrEmpty(cmbEmpSearch.SelectedItem.Value))
                employeeId = int.Parse(cmbEmpSearch.SelectedItem.Value);
            DateTime fromDate = GetEngDate(txtFromDate.Text.Trim());
            DateTime toDate = GetEngDate(txtToDate.Text.Trim());


            List<GetGenderByPositionLevelResult> list = BLL.BaseBiz.PayrollDataContext.GetGenderByPositionLevel(
              toDate, fromDate, branchId, departmentId).ToList();

            GetGenderByPositionLevelResult total = new GetGenderByPositionLevelResult { Name = "Total",Number = 0,Male=0,Female = 0,ThirdGender= 0 };
           
            foreach (var item in list)
            {
                total.Number += Convert.ToInt32(item.Number);
                total.Male +=  Convert.ToInt32(item.Male);
                total.Female +=  Convert.ToInt32(item.Female);
                total.ThirdGender += Convert.ToInt32(item.ThirdGender);
            }
            list.Add(total);

            GridPanelGenderByLevelPosition.Store[0].DataSource = list;
            GridPanelGenderByLevelPosition.Store[0].DataBind();

        }

        protected void ShowControls()
        {
            GridPanel1.Show();
            GridPanel2.Show();
            ChartPieGender.Show();
            ChartDepartmentWise.Show();
            lnkSaveChart1.Show();
            lnkSaveChart2.Show();
            
         

            string jsScript1 = "document.getElementById('block1').style.display = 'block';";
            string jsScript2 = "document.getElementById('block2').style.display = 'block';";

            X.AddScript(jsScript1);
            X.AddScript(jsScript2);

        }

        #region "ABSTRACT-METHOD"


     
        #endregion
    }
}

