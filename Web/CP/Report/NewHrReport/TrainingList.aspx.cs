﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using Utils.Helper;
using System.IO;
using Utils;
using Utils.Security;
using Utils.Calendar;
using Web.Helper;
using System.Collections;

namespace Web.Report
{
    public partial class TrainingList : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.HumanResource;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack && !X.IsAjaxRequest)
                Initialise();

            JavascriptHelper.AttachNonDialogPopUpCode(Page, "TrainingImport", "../../../ExcelWindow/ETrainingImport.aspx", 450, 500);
        }

        private void Initialise()
        {
            storeBranch.DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            storeBranch.DataBind();

            storeLevel.DataSource = NewPayrollManager.GetAllParentLevelList().OrderBy(x => x.Name).ToList();
            storeLevel.DataBind();

            cmbTrainingType.Store[0].DataSource = CommonManager.GetTrainingTypeList();
            cmbTrainingType.Store[0].DataBind();
        }

        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            int total = 0, pageSize = 0, employeeId = -1, branchId = -1, levelId = -1, trainingTypeId = -1;
            string employeeName = cmbSearch.Text;
            string startDate = "", endDate = "";

            if (cmbSearch.SelectedItem != null && cmbSearch.SelectedItem.Value != null)
            {
                int.TryParse(cmbSearch.SelectedItem.Value, out employeeId);

                if (employeeId != 0)
                    employeeName = "";
                else
                    employeeId = -1;
            }

            if (cmbBranch.SelectedItem != null && cmbBranch.SelectedItem.Value != null)
                branchId = int.Parse(cmbBranch.SelectedItem.Value);

            if (cmbLevel.SelectedItem != null && cmbLevel.SelectedItem.Value != null)
                levelId = int.Parse(cmbLevel.SelectedItem.Value);

            if (cmbTrainingType.SelectedItem != null && cmbTrainingType.SelectedItem.Value != null)
                trainingTypeId = int.Parse(cmbTrainingType.SelectedItem.Value);

            if (cmbPageSize.SelectedItem != null && cmbPageSize.SelectedItem.Value != null)
                pageSize = int.Parse(cmbPageSize.SelectedItem.Value);

            if (txtStartDate.SelectedValue != null)
                startDate = txtStartDate.Text.Trim();
            if (txtEndDate.SelectedValue != null)
                endDate = txtEndDate.Text.Trim();

            List<ReportNewHR_TrainingReportResult> list = NewHRManager.GetTrainingList(e.Page - 1, e.Limit, employeeId, startDate, endDate, branchId, levelId, trainingTypeId, employeeName);

            //-- start filtering -----------------------------------------------------------
            FilterHeaderConditions fhc = new FilterHeaderConditions(e.Parameters["filterheader"]);

            foreach (FilterHeaderCondition condition in fhc.Conditions)
            {
                string dataIndex = condition.DataIndex;
                FilterType type = condition.Type;
                string op = condition.Operator;
                object value = null;

                switch (condition.Type)
                {
                    case FilterType.Boolean:
                        value = condition.Value<bool>();
                        break;

                    case FilterType.Date:
                        switch (condition.Operator)
                        {
                            case "=":
                                value = condition.Value<DateTime>();
                                break;

                            case "compare":
                                value = FilterHeaderComparator<DateTime>.Parse(condition.JsonValue);
                                break;
                        }
                        break;
                    case FilterType.String:
                        value = condition.Value<string>();
                        break;
                    case FilterType.Numeric:
                        value = condition.Value<int>();
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                list.RemoveAll(item =>
                {
                    object oValue = item.GetType().GetProperty(dataIndex).GetValue(item, null);
                    string matchValue = null;
                    string itemValue = null;

                    if (type == FilterType.String)
                    {
                        matchValue = (string)value;

                        if (dataIndex == "EmployeeId")
                            itemValue = oValue.ToString();
                        else
                            itemValue = oValue as string;

                        matchValue = matchValue.ToLower();
                        itemValue = (itemValue == null ? "" : itemValue.ToLower());
                    }

                    switch (op)
                    {
                        case "=":
                            return oValue == null || !oValue.Equals(value);
                        case "compare":
                            return !((IEquatable<IComparable>)value).Equals((IComparable)oValue);
                        case "+":
                            return itemValue == null || !itemValue.Contains(matchValue); //return itemValue == null || !itemValue.StartsWith(matchValue);
                        case "-":
                            return itemValue == null || !itemValue.EndsWith(matchValue);
                        case "!":
                            return itemValue == null || itemValue.IndexOf(matchValue) >= 0;
                        case "*":
                            return itemValue == null || itemValue.IndexOf(matchValue) < 0;
                        default:
                            throw new Exception("Not supported operator");
                    }
                });
            }
            //-- end filtering ------------------------------------------------------------

            //-- start sorting ------------------------------------------------------------

            if (e.Sort.Length > 0)
            {
                list.Sort(delegate(ReportNewHR_TrainingReportResult obj1, ReportNewHR_TrainingReportResult obj2)
                {
                    object a;
                    object b;

                    int direction = e.Sort[0].Direction == Ext.Net.SortDirection.DESC ? -1 : 1;

                    a = obj1.GetType().GetProperty(e.Sort[0].Property).GetValue(obj1, null);
                    b = obj2.GetType().GetProperty(e.Sort[0].Property).GetValue(obj2, null);
                    return CaseInsensitiveComparer.Default.Compare(a, b) * direction;
                });
            }

           // Session["ExportTrainings"] = list;

            storeTraining.DataSource = list;

            if (list.Count > 0)
                total = list[0].TotalRows.Value;

            e.Total = total;
        }

        protected Field OnCreateFilterableField(object sender, ColumnBase column, Field defaultField)
        {
            ((TextField)defaultField).Icon = Icon.Magnifier;

            return defaultField;
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            //if (Session["ExportTrainings"] == null)
            //{
            //    NewMessage.ShowWarningMessage("Please reload the data for export.");
            //    return;
            //}

            //List<ReportNewHR_TrainingReportResult> list = (List<ReportNewHR_TrainingReportResult>)Session["ExportTrainings"];

            //Session["ExportTrainings"] = null;


            int  pageSize = 0, employeeId = -1, branchId = -1, levelId = -1, trainingTypeId = -1;
            string employeeName = cmbSearch.Text;
            string startDate = "", endDate = "";

            if (cmbSearch.SelectedItem != null && cmbSearch.SelectedItem.Value != null)
            {
                int.TryParse(cmbSearch.SelectedItem.Value, out employeeId);

                if (employeeId != 0)
                    employeeName = "";
                else
                    employeeId = -1;
            }

            if (cmbBranch.SelectedItem != null && cmbBranch.SelectedItem.Value != null)
                branchId = int.Parse(cmbBranch.SelectedItem.Value);

            if (cmbLevel.SelectedItem != null && cmbLevel.SelectedItem.Value != null)
                levelId = int.Parse(cmbLevel.SelectedItem.Value);

            if (cmbTrainingType.SelectedItem != null && cmbTrainingType.SelectedItem.Value != null)
                trainingTypeId = int.Parse(cmbTrainingType.SelectedItem.Value);

            if (cmbPageSize.SelectedItem != null && cmbPageSize.SelectedItem.Value != null)
                pageSize = int.Parse(cmbPageSize.SelectedItem.Value);

            if (txtStartDate.SelectedValue != null)
                startDate = txtStartDate.Text.Trim();
            if (txtEndDate.SelectedValue != null)
                endDate = txtEndDate.Text.Trim();

            List<ReportNewHR_TrainingReportResult> list = NewHRManager.GetTrainingList(0, 999999, employeeId, startDate, endDate, branchId, levelId, trainingTypeId, employeeName);


            List<string> hiddenColumnList = new List<string>();
            hiddenColumnList = new List<string> { "TotalRows", "RowNum", "TrainingId", "INo", "Note", "Position" };

            Bll.ExcelHelper.ExportToExcel("Employee Training", list,
                   hiddenColumnList,
               new List<String>() { },
               new Dictionary<string, string>() { {"EmployeeId","EIN"}, 
                    {"TrainingName","Training Name"}, {"TrainingTypeName","Training Type"}, { "ResourcePerson", "Resource Person" },
                    { "InstitutionName", "Institution" }, { "TrainingFrom", "Training From" }, { "TrainingTo", "Training To" } },
               new List<string>() { }
               , new List<string> { }
               , new List<string> { }
               , new Dictionary<string, string>() { }
               , new List<string> { "Name", "EmployeeId", "TrainingName", "TrainingTypeName", "ResourcePerson", "InstitutionName", "Country", "TrainingFrom", "TrainingTo", "Duration" });



        }


    }
}