﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HeadCount.aspx.cs" MasterPageFile="~/Master/NewDetails.Master"
    Inherits="Web.Report.HeadCount" Title="Head Count" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <ext:ResourcePlaceHolder ID="ResourcePlaceHolder1" runat="server" Mode="Script" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <script>
      var storeReport = null;
      Ext.onReady(
        function () {        
            storeReport = <%= storeReport.ClientID %>;
        }
    ); 


    var tipRenderer1 = function (data1, data2) {

   
        this.setTitle(data2.value[1].toString());

    };

    function SaveHeadCountChart_MonthWise() {
        <%= HeadCountChart.ClientID %>.save({
            type: 'image/png'
        });
    }

     function SaveHeadCountChart_YearWise() {
        <%= ChartYearWiseCount.ClientID %>.save({
            type: 'image/png'
        });
    }


        function searchList()
        {
            PagingToolbar_ListItems.doRefresh();
        }

        function searchListDynamic()
        {
            PagingToolbar1.doRefresh();
        }
        
              

    function GetServiceList() 
    {
    searchList();
    }
    
    </script>
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4 id="header" runat="server">
                    Head Count</h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <ext:Hidden ID="Hidden1" runat="server" />
        <div class="alert alert-info">
            <table>
                <tr>
                    <td style="width: 160px">
                        <pr:CalendarExtControl Width="150px" FieldLabel="From" ID="txtFromDate" runat="server"
                            LabelAlign="Top" LabelSeparator="">
                        </pr:CalendarExtControl>
                    </td>
                    <td style="width: 160px">
                        <pr:CalendarExtControl Width="150px" FieldLabel="To" ID="txtToDate" runat="server"
                            LabelAlign="Top" LabelSeparator="">
                        </pr:CalendarExtControl>
                    </td>
                    <td style="width: 190px">
                        <ext:ComboBox ID="cmbBranch" Width="180px" runat="server" ValueField="BranchId" DisplayField="Name"
                            FieldLabel="Branch" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                            QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store5" runat="server">
                                    <Model>
                                        <ext:Model ID="Model5" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="BranchId" />
                                                <ext:ModelField Name="Name" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <Triggers>
                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                            </Triggers>
                            <Listeners>
                                <Select Handler="this.getTrigger(0).show();" />
                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                            </Listeners>
                        </ext:ComboBox>
                    </td>
                    <td style="width: 200px">
                        <ext:ComboBox ID="cmbDepartment" Width="180px" runat="server" ValueField="DepartmentId"
                            DisplayField="Name" FieldLabel="Department" LabelAlign="top" LabelSeparator=""
                            ForceSelection="true" QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store6" runat="server">
                                    <Model>
                                        <ext:Model ID="Model6" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="DepartmentId" />
                                                <ext:ModelField Name="Name" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <Triggers>
                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                            </Triggers>
                            <Listeners>
                                <Select Handler="this.getTrigger(0).show();" />
                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                            </Listeners>
                        </ext:ComboBox>
                    </td>
                    <td>
                        <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                            <Proxy>
                                <ext:AjaxProxy Url="../Handler/EmpSearchID.ashx">
                                    <ActionMethods Read="GET" />
                                    <Reader>
                                        <ext:JsonReader Root="plants" TotalProperty="total" />
                                    </Reader>
                                </ext:AjaxProxy>
                            </Proxy>
                            <Model>
                                <ext:Model ID="Model2" IDProperty="EmployeeId" Name="ItemLineModel" runat="server">
                                    <Fields>
                                        <ext:ModelField Name="Name" Type="String" />
                                        <ext:ModelField Name="EmployeeId" Type="String" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                        <ext:ComboBox LabelSeparator="" ID="cmbEmpSearch" FieldLabel="Search Employee" EmptyText="Employee Name"
                            LabelAlign="Top" runat="server" DisplayField="Name" ValueField="EmployeeId" StoreID="storeSearch"
                            TypeAhead="false" Width="200" PageSize="9999" HideBaseTrigger="true" MinChars="1"
                            TriggerAction="All" ForceSelection="false">
                            <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                                <ItemTpl ID="ItemTpl2" runat="server">
                                    <Html>
                                        <tpl>
                                                        <div class="search-item">
                                                                        <span>{Name}</span>  
                                                         </div>
					                            </tpl>
                                    </Html>
                                </ItemTpl>
                            </ListConfig>
                            <Triggers>
                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                            </Triggers>
                            <Listeners>
                                <Select Handler="this.getTrigger(0).show();" />
                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                <TriggerClick Handler="if (index == 0) { 
                                                       this.clearValue(); 
                                                       this.getTrigger(0).hide();
                                                   }" />
                            </Listeners>
                        </ext:ComboBox>
                    </td>
                    <td style="padding-left:10px;padding-top: 25px; width: 100px">
                        <ext:Button Width="70" ID="btnSearch" runat="server" Text="Search" Height="30">
                            <%-- <Listeners>
                                                            <Click Handler="GetServiceList()" />
                                                        </Listeners>--%>
                            <DirectEvents>
                                <Click OnEvent="btnSearch_Click">
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                    </td>
                </tr>
            </table>
        </div>
        <div style="padding-top: 20px">
        </div>
        <div style="float: left">
            <ext:Chart ID="HeadCountChart" runat="server" Height="200" Width="750" Hidden="true"
                Region="Center" Shadow="true" StyleSpec="background:#fff" Animate="true">
                <Store>
                    <ext:Store ID="storeChartHeadCount" runat="server" AutoDataBind="true">
                        <Model>
                            <ext:Model ID="Model3" runat="server">
                                <Fields>
                                    <ext:ModelField Name="MonthName" />
                                    <ext:ModelField Name="HeadCount" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <Axes>
                    <ext:NumericAxis Fields="HeadCount" Hidden="true">
                        <Label>
                            <Renderer Handler="return Ext.util.Format.number(value, '0,0');" />
                        </Label>
                    </ext:NumericAxis>
                    <ext:CategoryAxis Position="Bottom" Fields="MonthName">
                    </ext:CategoryAxis>
                </Axes>
                <Series>
                    <ext:ColumnSeries Axis="Left" Highlight="true" XField="MonthName" YField="HeadCount"
                        GroupGutter="10">
                        <Tips TrackMouse="true" Width="70" Height="28" ID="columnTips">
                            <Renderer Fn="tipRenderer1" />
                        </Tips>
                        <%--   <Label Display="InsideEnd" Field="Data1" Orientation="Horizontal" Color="#333" TextAnchor="middle">
                                                <Renderer Handler="return Ext.util.Format.number(value, '0');" />
                                            </Label>--%>
                        <Label Display="Outside" Field="HeadCount" Orientation="Horizontal" Color="#333"
                            TextAnchor="middle" />
                    </ext:ColumnSeries>
                </Series>
            </ext:Chart>
            <ext:Button ID="btnSaveMonthWiseChart" runat="server" Text="export chart" Handler="SaveHeadCountChart_MonthWise();"
                Hidden="true" />
        </div>
        <div style="float: left; margin-left: 50px;">
            <ext:Label runat="server" ID="lblDate" StyleSpec="font-size:12px;color:rgb(69, 150, 14)">
            </ext:Label>
            <br />
            <ext:Label Text="" runat="server" ID="lblTotalCount" StyleSpec="font-size:40px">
            </ext:Label>
            <ext:Label Text="YTD" runat="server" ID="lblYTD" Hidden="true">
            </ext:Label>
            <br />
            <ext:Chart ID="ChartYearWiseCount" runat="server" Height="120" Width="250px" Hidden="true"
                Shadow="true" StyleSpec="background:#fff" Animate="true">
                <Store>
                    <ext:Store ID="storeYearWiseChartCount" runat="server" AutoDataBind="true">
                        <Model>
                            <ext:Model ID="Model1" runat="server">
                                <Fields>
                                    <ext:ModelField Name="HeadCount" />
                                    <ext:ModelField Name="MonthName" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <Axes>
                    <ext:NumericAxis Fields="HeadCount" Position="Bottom" Minimum="0" Hidden="true">
                        <Label>
                            <Renderer Handler="return Ext.util.Format.number(value, '0,0');" />
                        </Label>
                    </ext:NumericAxis>
                    <ext:CategoryAxis Fields="MonthName" Position="Left" />
                </Axes>
                <Series>
                    <ext:BarSeries Axis="Bottom" Highlight="true" XField="MonthName" YField="HeadCount">
                        <Tips TrackMouse="true" Width="140" Height="28">
                            <Renderer Handler="this.setTitle(storeItem.get('MonthName') + ': ' + storeItem.get('HeadCount') );" />
                        </Tips>
                        <Label Display="InsideEnd" Field="HeadCount" Orientation="Horizontal" Color="#333"
                            TextAnchor="middle" />
                    </ext:BarSeries>
                </Series>
            </ext:Chart>
            <ext:Button ID="btnSaveYearWiseChart" runat="server" Text="export chart" Handler="SaveHeadCountChart_YearWise();"
                Hidden="true" />
        </div>
        <div style="clear: both">
        </div>
        <div class="">
            <div style="text-align: left; margin-top: 20px; padding-bottom: 10px">
                <ext:LinkButton ID="lnkExcelPrint" Icon="PageExcel" runat="server" Text="Export"
                    OnClick="btnExcelPrint_Click" Hidden="true" AutoPostBack="true">
                </ext:LinkButton>
            </div>
            <ext:GridPanel ID="GridPanel1" runat="server" Border="false" AutoScroll="true" MinHeight="500">
                <Store>
                    <ext:Store PageSize="50" runat="server" ID="storeReport" RemoteSort="true" OnReadData="Store_ReadDataDynamic"
                        AutoLoad="false">
                        <Model>
                            <ext:Model ID="Model_SearchLedger" runat="server">
                                <Fields>
                                    <ext:ModelField Name="EmployeeId" Type="String" />
                                    <ext:ModelField Name="Name" Type="String" />
                                    <ext:ModelField Name="HireDate" Type="String" />
                                    <ext:ModelField Name="Position" Type="String" />
                                    <ext:ModelField Name="Branch" Type="String" />
                                    <ext:ModelField Name="Department" Type="String" />
                                    <ext:ModelField Name="RowNumber" Type="String" />
                                    <ext:ModelField Name="TotalRows" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Proxy>
                            <ext:PageProxy>
                            </ext:PageProxy>
                        </Proxy>
                    </ext:Store>
                </Store>
                <ColumnModel ID="ColumnModel_Ledger" runat="server" Cls="gridheader">
                    <Columns>
                        <ext:Column ID="Column1" runat="server" Text="Name" DataIndex="Name" Width="150" />
                        <ext:DateColumn ID="Column_Date1" runat="server" Text="Hire Date" DataIndex="HireDate"
                            Width="100" />
                        <ext:Column ID="Column_Document" runat="server" Text="Job Title" Align="Left" DataIndex="Position"
                            Width="150">
                        </ext:Column>
                        <ext:Column ID="Column2" runat="server" Text="Branch" Align="Left" DataIndex="Branch"
                            Width="150">
                        </ext:Column>
                        <ext:Column ID="Column3" runat="server" Text="Department" Align="Left" DataIndex="Department"
                            Width="150">
                        </ext:Column>
                    </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar1" runat="server" StoreID="storeReport" DisplayInfo="true">
                        <Items>
                            <ext:Label ID="Label1" runat="server" Text="Page size:" />
                            <ext:ToolbarSpacer ID="ToolbarSpacer1" runat="server" Width="10" />
                            <ext:ComboBox ID="ComboBox1" runat="server" Width="80" SelectOnFocus="true" Selectable="true"
                                ValueField="Value" DisplayField="Text">
                                <Listeners>
                                    <Select Handler="#{storeReport}.pageSize = this.getValue();#{PagingToolbar1}.moveFirst(); searchListDynamic()" />
                                </Listeners>
                                <Items>
                                    <ext:ListItem Value="20" Text="20" />
                                    <ext:ListItem Value="50" Text="50" />
                                    <ext:ListItem Value="100" Text="100" />
                                </Items>
                                <SelectedItems>
                                    <ext:ListItem Index="1">
                                    </ext:ListItem>
                                </SelectedItems>
                            </ext:ComboBox>
                        </Items>
                    </ext:PagingToolbar>
                </BottomBar>
            </ext:GridPanel>
        </div>
        </Content> </ext:Container> </Items> </ext:Panel>
    </div>
</asp:Content>
