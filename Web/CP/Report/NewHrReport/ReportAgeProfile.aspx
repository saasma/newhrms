﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReportAgeProfile.aspx.cs"
    MasterPageFile="~/Master/NewDetails.Master" Inherits="Web.Report.ReportAgeProfile"
    Title="Age Profile" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <ext:ResourcePlaceHolder ID="ResourcePlaceHolder1" runat="server" Mode="Script" />
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <script>
     
        var tipRenderer1 = function (data1, data2) {
        this.setTitle(data2.value[1].toString());

    };

    function SaveChart1() {
        <%= Chart1.ClientID %>.save({
            type: 'image/png'
        });
    }

    
    function SaveChart2() {
        <%= Chart2.ClientID %>.save({
            type: 'image/png'
        });
    }

    </script>
   <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4 runat="server" id="gender">
                   {0} Profile
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
            <ext:Panel ID="panelCenter" runat="server" Region="Center">
                <Items>
                    <ext:Container ID="Container3" Region="None" runat="server">
                        <Content>
                            <div class="alert alert-info">
                                <table class="fieldTable">
                                    <tr>
                                        <td style="width:120px">
                                            <pr:CalendarExtControl FieldLabel="From Date" ID="txtFromDate" runat="server" Width="110"
                                                LabelWidth="30" LabelAlign="Top" LabelSeparator="">
                                            </pr:CalendarExtControl>
                                        </td>
                                         <td style="width: 120px">
                                            <pr:CalendarExtControl FieldLabel="To" ID="txtToDate" runat="server" Width="110"
                                                LabelAlign="Top" LabelSeparator="">
                                            </pr:CalendarExtControl>
                                        </td>
                                         <td style="width: 180px">
                                            <ext:ComboBox ID="cmbBranch" Width="170px" runat="server" ValueField="BranchId" DisplayField="Name"
                                                FieldLabel="Branch" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                                                QueryMode="Local">
                                                <Store>
                                                    <ext:Store ID="Store5" runat="server">
                                                        <Model>
                                                            <ext:Model ID="Model5" runat="server">
                                                                <Fields>
                                                                    <ext:ModelField Name="BranchId" />
                                                                    <ext:ModelField Name="Name" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                    </ext:Store>
                                                </Store>
                                                <Triggers>
                                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                                </Triggers>
                                                <Listeners>
                                                    <Select Handler="this.getTrigger(0).show();" />
                                                    <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                                    <TriggerClick Handler="if (index == 0) { 
                                                               this.clearValue(); 
                                                               this.getTrigger(0).hide();
                                                           }" />
                                                </Listeners>
                                            </ext:ComboBox>
                                        </td>
                                        <td style="width: 180px">
                                            <ext:ComboBox ID="cmbDepartment" Width="170px" runat="server" ValueField="DepartmentId"
                                                DisplayField="Name" FieldLabel="Department" LabelAlign="top" LabelSeparator=""
                                                ForceSelection="true" QueryMode="Local">
                                                <Store>
                                                    <ext:Store ID="Store6" runat="server">
                                                        <Model>
                                                            <ext:Model ID="Model6" runat="server">
                                                                <Fields>
                                                                    <ext:ModelField Name="DepartmentId" />
                                                                    <ext:ModelField Name="Name" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                    </ext:Store>
                                                </Store>
                                                <Triggers>
                                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                                </Triggers>
                                                <Listeners>
                                                    <Select Handler="this.getTrigger(0).show();" />
                                                    <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                                    <TriggerClick Handler="if (index == 0) { 
                                                               this.clearValue(); 
                                                               this.getTrigger(0).hide();
                                                           }" />
                                                </Listeners>
                                            </ext:ComboBox>
                                        </td>
                                        <td>
                                            <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                                                <Proxy>
                                                    <ext:AjaxProxy Url="../../../Handler/EmpSearchID.ashx">
                                                        <ActionMethods Read="GET" />
                                                        <Reader>
                                                            <ext:JsonReader Root="plants" TotalProperty="total" />
                                                        </Reader>
                                                    </ext:AjaxProxy>
                                                </Proxy>
                                                <Model>
                                                    <ext:Model ID="Model2" IDProperty="EmployeeId" Name="ItemLineModel" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="Name" Type="String" />
                                                            <ext:ModelField Name="EmployeeId" Type="String" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                            <ext:ComboBox LabelSeparator="" ID="cmbEmpSearch" FieldLabel="Search Employee" EmptyText="Employee Name"
                                                LabelAlign="Top" runat="server" DisplayField="Name" ValueField="EmployeeId" StoreID="storeSearch"
                                                TypeAhead="false" Width="200" PageSize="9999" HideBaseTrigger="true" MinChars="1"
                                                TriggerAction="All" ForceSelection="false">
                                                <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                                                    <ItemTpl ID="ItemTpl2" runat="server">
                                                        <Html>
                                                            <tpl>
                                                                            <div class="search-item">
                                                                                            <span>{Name}</span>  
                                                                             </div>
					                                                </tpl>
                                                        </Html>
                                                    </ItemTpl>
                                                </ListConfig>
                                                <Triggers>
                                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                                </Triggers>
                                                <Listeners>
                                                    <Select Handler="this.getTrigger(0).show();" />
                                                    <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                                    <TriggerClick Handler="if (index == 0) { 
                                                                           this.clearValue(); 
                                                                           this.getTrigger(0).hide();
                                                                       }" />
                                                </Listeners>
                                            </ext:ComboBox>
                                        </td>
                                        <td style="padding-top: 20px">
                                            <ext:RadioGroup ID="RadioGrpBranchDepartment" runat="server" ColumnsNumber="2" Width="300">
                                                <Items>
                                                    <ext:Radio ID="RadioDepartmentWise" runat="server" BoxLabel="Department Wise" Checked="true" />
                                                    <ext:Radio ID="RadioBranchWise" runat="server" BoxLabel="Branch Wise" />
                                                </Items>
                                            </ext:RadioGroup>
                                        </td>
                                        <td style="padding-top: 25px; width: 100px">
                                            <ext:Button Width="70" ID="btnSearch" runat="server" Text="Search" Height="30">
                                                <DirectEvents>
                                                    <Click OnEvent="btnView_Click">
                                                    </Click>
                                                </DirectEvents>
                                            </ext:Button>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </Content>
                        <Content>
                            <div style="padding-top: 10px">
                            </div>
                            <div style="float: left">
                                <div class="widget" id="block1" style="position: relative; overflow: visible; display: none;
                                    width: 900px">
                                    <div class="widget-head">
                                        <h4 class="heading glyphicons history">
                                            <i></i>Breakdown By Age</h4>
                                    </div>
                                    <div class="widget-body list" style="padding: 10px">
                                        <ext:Chart ID="Chart1" runat="server" Height="200" Width="750" Hidden="true"
                                            Region="Center" Shadow="true" StyleSpec="background:#fff" Animate="true">
                                            <Store>
                                                <ext:Store ID="StoreChart1" runat="server" AutoDataBind="true">
                                                    <Model>
                                                        <ext:Model ID="Model1" runat="server">
                                                            <Fields>
                                                                <ext:ModelField Name="Name" />
                                                                <ext:ModelField Name="Data1" />
                                                            </Fields>
                                                        </ext:Model>
                                                    </Model>
                                                </ext:Store>
                                            </Store>
                                            <Axes>
                                                <ext:NumericAxis Fields="Data1" Hidden="true">
                                                    <Label>
                                                        <Renderer Handler="return Ext.util.Format.number(value, '0,0');" />
                                                    </Label>
                                                </ext:NumericAxis>
                                                <ext:CategoryAxis Position="Bottom" Fields="Name">
                                                </ext:CategoryAxis>
                                            </Axes>
                                            <Series>
                                                <ext:ColumnSeries Axis="Left" Highlight="true" XField="Name" YField="Data1" GroupGutter="10">
                                                    <Tips TrackMouse="true" Width="70" Height="28" ID="columnTips">
                                                        <Renderer Fn="tipRenderer1" />
                                                    </Tips>
                                                    <%--   <Label Display="InsideEnd" Field="Data1" Orientation="Horizontal" Color="#333" TextAnchor="middle">
                                                <Renderer Handler="return Ext.util.Format.number(value, '0');" />
                                            </Label>--%>
                                                    <Label Display="Outside" Field="Data1" Orientation="Horizontal" Color="#333" TextAnchor="middle" />
                                                </ext:ColumnSeries>
                                            </Series>
                                        </ext:Chart>
                                        <div style="padding: 10px 0 10px 0">
                                            <ext:LinkButton ID="lnkSaveChart1" Handler="SaveChart1()" runat="server" Text="Export Chart"
                                                Hidden="true">
                                            </ext:LinkButton>
                                        </div>
                                    </div>
                                    <br />
                                    <br />
                                </div>
                            </div>
                            <br />
                         
                            <div class="" style="min-height: 2000px; margin-top: 20px;">
                                <br />
                                <div class="widget" id="block2" style="position: relative; display: none; width: 1000px;
                                    >
                                    <div class="widget-head">
                                        <h4 class="heading glyphicons history">
                                            <i></i>Age Breakdown By Department</h4>
                                    </div>
                                    <div class="widget-body list" style="padding: 10px">
                                        <ext:Chart Height="200" Width="400" ID="Chart2" runat="server" Shadow="true" Hidden="true"
                                            Animate="true">
                                            <Store>
                                                <ext:Store ID="StoreChartDeparmentWise" runat="server" AutoDataBind="true">
                                                    <Model>
                                                        <ext:Model ID="Model3" runat="server">
                                                            <Fields>
                                                                <ext:ModelField Name="Department" Type="String" />
                                                                <ext:ModelField Name="Under 18" Type="Int" />
                                                                <ext:ModelField Name="18-25" Type="Int" />
                                                                <ext:ModelField Name="26-34" Type="Int" />
                                                                <ext:ModelField Name="35-44" Type="Int" />
                                                                <ext:ModelField Name="45-54" Type="Int" />
                                                                <ext:ModelField Name="55-64" Type="Int" />
                                                                <ext:ModelField Name="65+" Type="Int" />
                                                            </Fields>
                                                        </ext:Model>
                                                    </Model>
                                                </ext:Store>
                                            </Store>
                                            <LegendConfig Position="Right">
                                            </LegendConfig>
                                            <Axes>
                                                <ext:NumericAxis Fields="Under 18,18-25,26-34,35-44,45-54,55-64,65+" Position="Bottom"
                                                    Grid="true" Hidden="true">
                                                    <Label>
                                                        <Renderer Handler="return String(value);" />
                                                    </Label>
                                                </ext:NumericAxis>
                                                <ext:CategoryAxis Fields="Department" Position="Left" />
                                            </Axes>
                                            <Series>
                                                <ext:BarSeries Axis="Bottom" Gutter="80" XField="Department" YField="Under 18,18-25,26-34,35-44,45-54,55-64,65+"
                                                    Stacked="true">
                                                    <Tips TrackMouse="true" Width="65" Height="28">
                                                        <Renderer Handler="this.setTitle(String(item.value[1]))" />
                                                    </Tips>
                                                </ext:BarSeries>
                                            </Series>
                                        </ext:Chart>
                                        <div style="padding: 10px 0 10px 0">
                                            <ext:LinkButton ID="lnkSaveChart2" runat="server" Handler="SaveChart2()" Text="Export Chart"
                                                Hidden="true">
                                            </ext:LinkButton>
                                        </div>
                                        <div style="text-align: right; width: 950px; padding-bottom: 10px">
                                            <ext:LinkButton ID="LinkButton1" Icon="PageExcel" runat="server" Text="Export" OnClick="btnExcelPrint2_Click"
                                                AutoPostBack="true">
                                            </ext:LinkButton>
                                        </div>
                                        <ext:GridPanel ID="GridPanel2" runat="server" Border="false"  AutoScroll="true"
                                            Hidden="true">
                                            <Store>
                                                <ext:Store runat="server" ID="StoreGrid2" RemoteSort="true" OnReadData="Store_ReadData"
                                                    AutoLoad="false">
                                                    <Model>
                                                        <ext:Model ID="Model133" runat="server">
                                                            <Fields>
                                                                <ext:ModelField Name="Department" Type="String" />
                                                                <ext:ModelField Name="Under18" Type="Int" />
                                                                <ext:ModelField Name="EightTo25" Type="Int" />
                                                                <ext:ModelField Name="TwentySixTo34" Type="Int" />
                                                                <ext:ModelField Name="ThirtyFiveTo44" Type="Int" />
                                                                <ext:ModelField Name="FourtyFiveTo54" Type="Int" />
                                                                <ext:ModelField Name="FiftyFiveTo64" Type="Int" />
                                                                <ext:ModelField Name="SixtyFiveAbove" Type="Int" />
                                                            </Fields>
                                                        </ext:Model>
                                                    </Model>
                                                    <Proxy>
                                                        <ext:PageProxy>
                                                        </ext:PageProxy>
                                                    </Proxy>
                                                </ext:Store>
                                            </Store>
                                            <ColumnModel ID="ColumnModel1" runat="server" Cls="gridheader">
                                                <Columns>
                                                    <ext:Column ID="c1" runat="server" Text="Branch/Department" DataIndex="Department"
                                                        Width="300" />
                                                    <ext:Column ID="c2" runat="server" Text="Under 18" Align="Left" DataIndex="Under18"
                                                        Width="100">
                                                    </ext:Column>
                                                    <ext:Column ID="c3" runat="server" Text="18-25" Align="Left" DataIndex="EightTo25"
                                                        Width="100">
                                                    </ext:Column>
                                                    <ext:Column ID="c4" runat="server" Text="26-34" Align="Left" DataIndex="TwentySixTo34"
                                                        Width="100">
                                                    </ext:Column>
                                                    <ext:Column ID="c5" runat="server" Text="35-44" Align="Left" DataIndex="ThirtyFiveTo44"
                                                        Width="100">
                                                    </ext:Column>
                                                    <ext:Column ID="c6" runat="server" Text="45-54" Align="Left" DataIndex="FourtyFiveTo54"
                                                        Width="100">
                                                    </ext:Column>
                                                    <ext:Column ID="c7" runat="server" Text="55-64" Align="Left" DataIndex="FiftyFiveTo64"
                                                        Width="100">
                                                    </ext:Column>
                                                    <ext:Column ID="c8" runat="server" Text="65+" Align="Left" DataIndex="SixtyFiveAbove"
                                                        Width="100">
                                                    </ext:Column>
                                                </Columns>
                                            </ColumnModel>
                                        </ext:GridPanel>
                                    </div>
                                </div>
                            </div>
                        </Content>
                    </ext:Container>
                </Items>
            </ext:Panel>
        </div>
    </div>
</asp:Content>
