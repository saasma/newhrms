﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PayVariance.aspx.cs" MasterPageFile="~/Master/NewDetails.Master"
    Inherits="Web.Report.PayVariance" Title="Pay Variance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        var template = '<span style="color:{0};font-weight:bold">{1}</span>';

        var getFormattedAmount1 = function (value) {
            if (value == "-")
                return "-";
            var val = parseFloat(value);
            if (isNaN(val))
                return "-";

            if (val == 0)
                return "-";

            return getFormattedAmount(value);
        }

        var renderForReason = function (value) {
            return "<span title='" + value + "'>" + value + "</span>";
        }
        var change = function (value) {
            if (value == 0)
                return "-";
            return Ext.String.format(template, (value > 0) ? "green" : "red", getFormattedAmount(value));
        };
    </script>
    <ext:ResourcePlaceHolder ID="ResourcePlaceHolder1" runat="server" Mode="Script" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <script>
      var storeDynamic = null;
      Ext.onReady(
        function () {        
            storeDynamic = <%= storeDynamic.ClientID %>;
        }
    ); 


//    if(storeDynamic!=null)
//    storeDynamic.currentPage =1;

//        function searchList()
//        {
//            PagingToolbar_ListItems.doRefresh();
//        }

        function searchListDynamic()
        {
            <%=PagingToolbar1.ClientID %>.doRefresh();
        }
       

//    function GetServiceList() 
//    {
//    searchList();
//    }
    
    </script>
    <div id="a1">
        <ext:Hidden ID="Hidden1" runat="server" />
        <ext:Hidden ID="period1" runat="server" />
        <ext:Hidden ID="period2" runat="server" />
        <ext:Hidden ID="HiddenFilterHeader" runat="server" />
        <ext:Hidden ID="HiddenCurrentPage" runat="server" />
        <ext:Hidden ID="HiddenSelectedFields" runat="server" />
        <ext:Hidden ID="Hidden_ClassOrGroupID" runat="server" />
        <div class="pageheader">
            <div class="media">
                <div class="media-body">
                    <h4>
                        Pay Variance Report
                    </h4>
                </div>
            </div>
        </div>
        <div class="contentpanel">
            <div id="container" style="width: inherit;">
                <div id="fullBlock" class="account_rev">
                    <div class="alert alert-info">
                        Shows list for the difference of min 1 only and salary should be saved for both
                        period
                        <table class="fieldTable">
                            <tr>
                                <td>
                                    <pr:CalendarExtControl Width="150px" FieldLabel="From Month" ID="txtFromDate" runat="server"
                                        LabelAlign="Top" LabelSeparator="">
                                    </pr:CalendarExtControl>
                                </td>
                                <td>
                                    <pr:CalendarExtControl Width="150px" FieldLabel="To Month" ID="txtToDate" runat="server"
                                        LabelAlign="Top" LabelSeparator="">
                                    </pr:CalendarExtControl>
                                </td>
                                <td style="">
                                    <ext:MultiCombo ID="cmbShow" Width="250" runat="server" ValueField="TypeSource" LabelWidth="70"
                                        DisplayField="HeaderName" FieldLabel="Filter Header" LabelAlign="Top" LabelSeparator=""
                                        QueryMode="Local" ForceSelection="true">
                                        <Store>
                                            <ext:Store ID="Store1" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model1" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="TypeSource" />
                                                            <ext:ModelField Name="HeaderName" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                    </ext:MultiCombo>
                                </td>
                                <td style="padding-top: 25px; width: 100px">
                                    <ext:Button Width="70" ID="btnSearch" runat="server" Text="Search" Height="30">
                                        <Listeners>
                                            <Click Handler="searchListDynamic()" />
                                        </Listeners>
                                        <%--  <DirectEvents>
                                            <Click OnEvent="btnSearch_Click">
                                                <EventMask ShowMask="true" />
                                            </Click>
                                        </DirectEvents>--%>
                                    </ext:Button>
                                </td>
                                <td style="padding-top: 25px">
                                    <ext:Button ID="Button1" runat="server" Text="Export" Cls="updatebtn" Height="30">
                                        <Menu>
                                            <ext:Menu ID="Menu2" runat="server" ShowSeparator="false" Cls="dropdownbtn">
                                                <Items>
                                                    <ext:MenuItem AutoPostBack="true" runat="server" OnClick="btnDynamicExcelPrint_Click"
                                                        Text="Excel" ID="btnExcelPrint">
                                                    </ext:MenuItem>
                                                </Items>
                                            </ext:Menu>
                                        </Menu>
                                    </ext:Button>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="">
                        <ext:GridPanel ID="GridPanel1" runat="server" Border="false" AutoScroll="true" Hidden="true">
                            <Store>
                                <ext:Store PageSize="25" runat="server" ID="storeDynamic" RemoteSort="true" OnReadData="Store_ReadDataDynamic"
                                    AutoLoad="false">
                                    <Model>
                                        <ext:Model ID="DynamicModel" Name="Test" runat="server" />
                                    </Model>
                                    <Proxy>
                                        <ext:PageProxy>
                                        </ext:PageProxy>
                                    </Proxy>
                                    <%-- <Parameters>
                                        <ext:StoreParameter Name="SelectedFieldsLineJson" Value="Ext.encode(#{GridPanel1}.getRowsValues({selectedOnly : false}))"
                                            Mode="Raw" />
                                    </Parameters>--%>
                                </ext:Store>
                            </Store>
                            <ColumnModel>
                                <Columns>
                                    <ext:Column ID="Column1" runat="server" DataIndex="Test" Hidden="true" Text='Test'
                                        Locked="true" Border="false" />
                                </Columns>
                            </ColumnModel>
                            <BottomBar>
                                <ext:PagingToolbar ID="PagingToolbar1" runat="server" StoreID="storeDynamic" DisplayInfo="true"
                                    Hidden="false">
                                    <%-- <Items>
                                        <ext:Label ID="Label1" runat="server" Text="Page size:" />
                                        <ext:ToolbarSpacer ID="ToolbarSpacer1" runat="server" Width="10" />
                                    </Items>--%>
                                </ext:PagingToolbar>
                            </BottomBar>
                        </ext:GridPanel>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
