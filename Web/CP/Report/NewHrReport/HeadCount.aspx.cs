﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Ext.Net;
using DAL;
using BLL;
using BLL.Base;
using BLL.Manager;
using Utils.Helper;
using Utils.Calendar;
//using Web.Report.Templates;
using System.IO;
using DevExpress.XtraPrinting;
using BLL.BO;
using System.Xml;
using System.Xml.Xsl;
using Newtonsoft.Json.Linq;
using System.Collections;
using System.Reflection;
using System.Data;
using Bll;

namespace Web.Report
{

    public class HeadCountMonthWiseBo
    {
        public string MonthName { get; set; }
        public int HeadCount { get; set; }
        public int displayOrder { get; set; }
    }

    public partial class HeadCount : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!X.IsAjaxRequest && !IsPostBack)
            {
                cmbBranch.Store[0].DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
                cmbBranch.Store[0].DataBind();

                DepartmentManager _DepartmentManager = new DepartmentManager();
                cmbDepartment.Store[0].DataSource = _DepartmentManager.GetDepartmentsByCompany(SessionManager.CurrentCompanyId);
                cmbDepartment.Store[0].DataBind();
                
                txtFromDate.Text = CustomDate.GetTodayDate(IsEnglish).ToString();
                txtToDate.Text = txtFromDate.Text;

                btnSearch_Click(null, null);

            }
        }

      
        protected void btnExcelPrint_Click(object sender, EventArgs e)
        {
            int BranchID = -1, DepartmentID = -1, ein = -1;
            if (!string.IsNullOrEmpty(cmbBranch.SelectedItem.Value))
                BranchID = int.Parse(cmbBranch.SelectedItem.Value);

            if (!string.IsNullOrEmpty(cmbDepartment.SelectedItem.Value))
                DepartmentID = int.Parse(cmbDepartment.SelectedItem.Value);

            if (!string.IsNullOrEmpty(cmbEmpSearch.SelectedItem.Value))
                ein = int.Parse(cmbEmpSearch.SelectedItem.Value);

           List<ReportNewHR_GetHeadCountResult> data = EmployeeManager.GetHeadCount(0, int.MaxValue, "", BranchID, DepartmentID, GetEngDate(txtFromDate.Text)
               , GetEngDate(txtToDate.Text),ein).ToList();

            ExcelHelper.ExportToExcel("Head Count", data,
          new List<String>() { "EmployeeId","SN","Status"},
          new List<String>() { },
          new Dictionary<string, string>() { { "Position", "Job Title" },{"HireDate","Hire Date"}},
          new List<string>() { }
          , new Dictionary<string, string>() { }
          , new List<string> { "Name", "HireDate", "Position", "Branch", "Department"});
        }

        protected void Store_ReadDataDynamic(object sender, StoreReadDataEventArgs e)
        {

            int BranchID=-1,DepartmentID=-1,ein=-1;
            if (!string.IsNullOrEmpty(cmbBranch.SelectedItem.Value))
                BranchID = int.Parse(cmbBranch.SelectedItem.Value);

            if (!string.IsNullOrEmpty(cmbDepartment.SelectedItem.Value))
                DepartmentID = int.Parse(cmbDepartment.SelectedItem.Value);

            if(!string.IsNullOrEmpty(cmbEmpSearch.SelectedItem.Value))
                ein = int.Parse(cmbEmpSearch.SelectedItem.Value);

            string sortBy = "";
            if (e.Sort.Any())
                sortBy = e.Sort[0].Property + " " + e.Sort[0].Direction;


    
            List<ReportNewHR_GetHeadCountResult> data = EmployeeManager.GetHeadCount(e.Start, e.Limit, sortBy, BranchID, DepartmentID, GetEngDate(txtFromDate.Text)
                ,GetEngDate(txtToDate.Text),ein).ToList();
            storeReport.DataSource = data;
            storeReport.DataBind();
            if (data.Count > 0)
                e.Total = data[0].TotalRows.Value;
            List<HeadCountMonthWiseBo> _ChartList = new List<HeadCountMonthWiseBo>();

            CustomDate startDate = CustomDate.GetCustomDateFromString(txtFromDate.Text.Trim(), IsEnglish);

            CustomDate endDate = CustomDate.GetCustomDateFromString(txtToDate.Text.Trim(), IsEnglish);

               int i = 1;
               for (; i <= 12; i++)
               {
                   HeadCountMonthWiseBo _ChartBO = new HeadCountMonthWiseBo();
                   int month = startDate.Month;


                   string MonthName = DateHelper.GetMonthName(month, this.IsEnglish);//+ "-"  + startDate.Year;

                   _ChartBO.MonthName = MonthName;
                   if(i==1)
                       _ChartBO.HeadCount = EmployeeManager.GetHeadCountByMonth(BranchID, DepartmentID, startDate.GetLastDateOfThisMonth().EnglishDate.AddYears(-1));
                   else
                    _ChartBO.HeadCount = EmployeeManager.GetHeadCountByMonth(BranchID, DepartmentID, startDate.GetLastDateOfThisMonth().EnglishDate);
                  
                   if (i == 1)
                       _ChartBO.displayOrder = 1;
                   else
                       _ChartBO.displayOrder = 13 - i;
                   startDate = startDate.GetFirstDateOfThisMonth();
                   startDate = startDate.DecrementByOneDay();

                   //startDate = startDate.GetLastDateOfThisMonth();
                   //startDate = startDate.DecrementByOneDay();

                   if (month > 1)
                       month -= 1;

                   if (month == 1)
                       month = 12;

                   if (month > 12)
                   {
                       month = 1;
                   }

                   _ChartList.Add(_ChartBO);
               }

               storeChartHeadCount.DataSource = _ChartList.OrderBy(x => x.displayOrder);
               storeChartHeadCount.DataBind();

               CustomDate startDate1 = CustomDate.GetCustomDateFromString(txtFromDate.Text.Trim(), IsEnglish);

            //---display pervious 3 years Head Count==============================================
            //LastDate of the Year;

            int LoopCnt = 12- startDate1.Month;
            for (int l = 1; l <= LoopCnt; l++)
               {
                   startDate1 = startDate1.GetLastDateOfThisMonth();
                   startDate1 = startDate1.IncrementByOneDay();

               }


               for (int l=1; l <= 12; l++)
               {
                   startDate1 = startDate1.GetFirstDateOfThisMonth();
                   startDate1 = startDate1.DecrementByOneDay();
                 
               }

               HeadCountMonthWiseBo _ChartBOYear = new HeadCountMonthWiseBo();
               _ChartBOYear.MonthName = startDate1.Year.ToString();
               List<HeadCountMonthWiseBo>_ChartListYear=  new List<HeadCountMonthWiseBo>();
               
               _ChartBOYear.HeadCount = EmployeeManager.GetHeadCountByMonth(BranchID, DepartmentID, startDate1.GetLastDateOfThisMonth().EnglishDate);

               _ChartBOYear.displayOrder =3;
               _ChartListYear.Add(_ChartBOYear);

               for (int j=1; j <= 12; j++)
               {
                   startDate1 = startDate1.GetFirstDateOfThisMonth();
                   startDate1 = startDate1.DecrementByOneDay();

               }

               HeadCountMonthWiseBo _ChartBOYear1 = new HeadCountMonthWiseBo();
               _ChartBOYear1.MonthName = startDate1.Year.ToString();
               _ChartBOYear1.HeadCount = EmployeeManager.GetHeadCountByMonth(BranchID, DepartmentID, startDate1.GetLastDateOfThisMonth().EnglishDate);
               _ChartBOYear1.displayOrder = 2;
               _ChartListYear.Add(_ChartBOYear1);

               for (int k=1; k <= 12; k++)
               {
                   startDate1 = startDate1.GetFirstDateOfThisMonth();
                   startDate1 = startDate1.DecrementByOneDay();

               }
               HeadCountMonthWiseBo _ChartBOYear2 = new HeadCountMonthWiseBo();
               _ChartBOYear2.MonthName = startDate1.Year.ToString();
               _ChartBOYear2.HeadCount = EmployeeManager.GetHeadCountByMonth(BranchID, DepartmentID, startDate1.GetLastDateOfThisMonth().EnglishDate);
               _ChartBOYear2.displayOrder = 1;
               _ChartListYear.Add(_ChartBOYear2);
              //display yearWise;

              storeYearWiseChartCount.DataSource = _ChartListYear.OrderBy(x => x.displayOrder); ;
              storeYearWiseChartCount.DataBind();
            //================================================================


        }

        protected void btnSearch_Click(object sender, DirectEventArgs e)
        {

            if (string.IsNullOrEmpty(txtFromDate.Text))
            {
                NewMessage.ShowNormalMessage("Date is required.");
                return;
            }
            lblTotalCount.Text = EmployeeManager.GetHeadCountByMonth(-1, -1, DateTime.Today.Date).ToString();
            lblDate.Text = DateTime.Today.Date.ToLongDateString();

            ShowControls();
            GridPanel1.GetStore().Reload();
            
          
        }

        protected void ShowControls()
        {
            GridPanel1.Show();
            HeadCountChart.Show();
            ChartYearWiseCount.Show();
            btnSaveMonthWiseChart.Show();
            btnSaveYearWiseChart.Show();
            lnkExcelPrint.Show();
            lblYTD.Show();
        }

        #region "ABSTRACT-METHOD"


     
        #endregion
    }
}
