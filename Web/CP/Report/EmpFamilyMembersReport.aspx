﻿<%@ Page Title="Employee Family Members Report" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="EmpFamilyMembersReport.aspx.cs" Inherits="Web.CP.Report.EmpFamilyMembersReport" %>

<%@ Register Assembly="DevExpress.XtraReports.v16.1.Web, Version=16.1.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraReports.Web" TagPrefix="dxxr" %>
<%@ Register Src="~/Controls/Report/ReportFilterBranchDep.ascx" TagName="ReportFilterBranchDep"
    TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Report/ReportContainer.ascx" TagName="ReportContainer"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Employee Family Members Report
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <uc1:ReportContainer runat="server" ViewerPaddingLeft="20px" OnReloadReport="LoadReport"
            id="report" /></div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
