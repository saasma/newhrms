﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using BLL;
using DevExpress.XtraReports.UI;
using Web.CP.Report.Templates.HR;
using Web.ReportDataSetTableAdapters;
using Utils.Calendar;
using BLL.Base;
using DAL;
using BLL.Manager;
using System.Data;

namespace Web.CP.Report
{
    public partial class PayEmpLoan : BasePage
    {


        protected  void LoadReportHandler()
        {}
      
        protected void Page_Load(object sender, EventArgs e)
        {
            //report.Filter.PayrollFrom = false;
            report.Filter.PayrollTo = false;
         
            report.Filter.Employee = false;
            report.Filter.LoanType = true;

            //if (!IsPostBack || CommonManager.CompanySetting.RemoveStyleWhileExportingReport)
                LoadReport();
        }

        protected void LoadReport()
        {
            PayrollPeriod payrollPeriod = CommonManager.GetPayrollPeriod(report.Filter.StartDate.Month,
                                                                         report.Filter.StartDate.Year);

            if( payrollPeriod ==null)
                return;
            

            Report_Pay_Emp_LoanTableAdapter
                adap = new Report_Pay_Emp_LoanTableAdapter();

            Report.Templates.Pay.ReportEmpLoan mainReport = new Report.Templates.Pay.ReportEmpLoan();


            mainReport.labelHeader.Text += " for the month " + payrollPeriod.Name;
            BLL.BaseBiz.SetConnectionPwd(adap.Connection);
            ReportDataSet.Report_Pay_Emp_LoanDataTable table =
                adap.GetData(payrollPeriod.Month, payrollPeriod.Year,
                                                                      SessionManager.CurrentCompanyId, 
                                                                      report.Filter.BranchId,report.Filter.DepartmentId,
                                                                      report.Filter.DepartmentName
                                                                      ,report.Filter.LoanTypeValue);

            //set payment terms
            EMILoadPaymentTerm convert = new EMILoadPaymentTerm();
            foreach (ReportDataSet.Report_Pay_Emp_LoanRow row in table)
            {
                row.PaymentTerms = convert.GetPaymentTerm(int.Parse(row.PaymentTerms));
            }



            mainReport.DataSource = table;

           

            mainReport.DataMember = "Report1";
          

            report.DisplayReport(mainReport);

           if(ReportHelper.IsReportExportState())
           {
               this.report.clearCache = true;
           }

        }
    }
}
