﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//using Web.CP.Report.Templates;

using BLL;
using DevExpress.XtraReports.UI;
using Web.ReportDataSetTableAdapters;
using Utils.Calendar;
using BLL.Base;
using BLL.Manager;
using DAL;

namespace Web.CP.Report
{
    public partial class HRLeaveBalance : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.LeaveAndAttendance;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            report.Filter.PayrollTo = false;
            report.Filter.Leave = true;
            report.Filter.SubDepartment = true;

            

            //if (!IsPostBack || CommonManager.CompanySetting.RemoveStyleWhileExportingReport )
            if (IsPostBack)
                LoadReport();
          
        }

        protected void LoadReport()
        {
            if (!IsPostBack)
                return;

            PayrollPeriod payrollPeriodFrom = CommonManager.GetPayrollPeriod(report.Filter.StartDate.Month,
                                                                        report.Filter.StartDate.Year);
            //PayrollPeriod payrollPeriodTo = CommonManager.GetPayrollPeriod(report.Filter.EndDate.Month,
            //                                                                         report.Filter.EndDate.Year);
            //if null get first payroll period
            if (payrollPeriodFrom == null)
                payrollPeriodFrom = CommonManager.GetFirstPayrollPeriod(SessionManager.CurrentCompanyId);

            //if (payrollPeriodTo == null)
            //    payrollPeriodTo = CommonManager.GetLastPayrollPeriod();

            //again null then no report
            if (payrollPeriodFrom == null)
                return;

            ////for report to date should be greater or equal to from date
            //if (payrollPeriodTo.PayrollPeriodId < payrollPeriodFrom.PayrollPeriodId)
            //    return;


            ReportDataSetTableAdapters.Report_HR_LeaveBalanceTableAdapter
                adap = new Report_HR_LeaveBalanceTableAdapter();


            Report.Templates.HR.ReportLeaveBalance report1 = new Report.Templates.HR.ReportLeaveBalance();

            BLL.BaseBiz.SetConnectionPwd(adap.Connection);
            report1.DataSource = adap.GetData(
                report.Filter.StartDate.Month, report.Filter.StartDate.Year,
                report.Filter.StartDate.Month, report.Filter.StartDate.Year,
                SessionManager.CurrentCompanyId,
               report.Filter.EmployeeName,report.Filter.LeaveTypeId,report.Filter.BranchId, report.Filter.DepartmentId,report.Filter.SubDepartmentId,
               0,report.Filter.DepartmentName,
               (SessionManager.CurrentLoggedInEmployeeId!= 0));
            report1.DataMember = "Report";


            //if same payroll then hide group header
            //if (report.Filter.StartDate == report.Filter.EndDate)
            //{
            //    report1.GroupHeader.Visible = false;
            //    report1.XRTitle.Text += DateHelper.GetMonthShortName(report.Filter.StartDate.Month, IsEnglish)
            //                            + "/" + report.Filter.StartDate.Year;
            //}
            //else
            //{
            //    report1.XRTitle.Text +=
            //        (DateHelper.GetMonthShortName(report.Filter.StartDate.Month, IsEnglish)
            //         + "/" + report.Filter.StartDate.Year + " to " +

            //         DateHelper.GetMonthShortName(report.Filter.EndDate.Month, IsEnglish)
            //         + "/" + report.Filter.EndDate.Year);
            //}


            report.DisplayReport(report1);

            if (ReportHelper.IsReportExportState())
            {
                this.report.clearCache = true;

            }


        }
    }
}
