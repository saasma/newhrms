﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//using Web.CP.Report.Templates;

using BLL;
using DevExpress.XtraReports.UI;
using Web.ReportDataSetTableAdapters;
using Utils.Calendar;
using BLL.Base;
using BLL.Manager;
using DAL;

namespace Web.CP.Report
{
    public partial class ConsolidatedLeaveReport : BasePage
    {

        

        protected void Page_Load(object sender, EventArgs e)
        {
            report.Filter.PayrollTo = true;
            report.Filter.Leave = true;
            report.Filter.SubDepartment = false;
            report.Filter.Branch = false;
            report.Filter.Department = false;


            if (IsPostBack)
                LoadReport();
          
        }

        protected void LoadReport()
        {
            if (!IsPostBack)
                return;

            PayrollPeriod payrollPeriodFrom = CommonManager.GetPayrollPeriod(report.Filter.StartDate.Month,
                                                                        report.Filter.StartDate.Year);
            PayrollPeriod payrollPeriodTo = CommonManager.GetPayrollPeriod(report.Filter.EndDate.Month,
                                                                                     report.Filter.EndDate.Year);
            //if null get first payroll period
            if (payrollPeriodFrom == null)
                payrollPeriodFrom = CommonManager.GetFirstPayrollPeriod(SessionManager.CurrentCompanyId);

            if (payrollPeriodTo == null)
                payrollPeriodTo = CommonManager.GetLastPayrollPeriod();

            //again null then no report
            if (payrollPeriodFrom == null || payrollPeriodTo == null)
                return;

            if (AttendanceManager.IsAttendanceSavedForEmployee(report.Filter.EmployeeId, payrollPeriodTo.PayrollPeriodId) == false)
            {
                Attendence lastAtteSavePayroll = BLL.BaseBiz.PayrollDataContext.Attendences
                    .Where(x => x.EmployeeId == report.Filter.EmployeeId).OrderByDescending(x => x.PayrollPeriodId).FirstOrDefault();
                if (lastAtteSavePayroll != null)
                {
                    if(lastAtteSavePayroll.PayrollPeriodId != payrollPeriodTo.PayrollPeriodId)
                    {
                        if (lastAtteSavePayroll.PayrollPeriodId < payrollPeriodFrom.PayrollPeriodId)
                        {
                            report.Filter.WarningMessage = payrollPeriodTo.Name + " attendance not saved.";
                        }
                        else
                        {
                            report.Filter.WarningMessage = payrollPeriodTo.Name + " attendance not saved, report has been generated upto " +
                                CommonManager.GetPayrollPeriod(lastAtteSavePayroll.PayrollPeriodId).Name + " only.";
                        }
                    }
                    payrollPeriodTo = CommonManager.GetPayrollPeriod(lastAtteSavePayroll.PayrollPeriodId);
                }
            }

            //for report to date should be greater or equal to from date
            if (payrollPeriodTo.PayrollPeriodId < payrollPeriodFrom.PayrollPeriodId)
                return;


            ReportDataSetTableAdapters.Report_HR_LeaveBalanceMonthByMonthTableAdapter
                adap = new Report_HR_LeaveBalanceMonthByMonthTableAdapter();


            Report.Templates.HR.ReportLeaveBalanceMonthByMonth report1 = new Report.Templates.HR.ReportLeaveBalanceMonthByMonth();

            BLL.BaseBiz.SetConnectionPwd(adap.Connection);

            ReportDataSet.Report_HR_LeaveBalanceMonthByMonthDataTable tbl = adap.GetData(payrollPeriodFrom.PayrollPeriodId,
                payrollPeriodTo.PayrollPeriodId, report.Filter.LeaveTypeId, report.Filter.EmployeeId);

            foreach (ReportDataSet.Report_HR_LeaveBalanceMonthByMonthRow item in tbl)
            {
                if (item.Earned == 0)
                    item.SetEarnedNull();
                if (item.ManualAdjustment == 0)
                    item.SetManualAdjustmentNull();
            }

            report1.DataSource = tbl;
            report1.DataMember = "Report";

            report1.labelTitle.Text = "Leave Summary Report ";
            //if same payroll then hide group header
            //if (report.Filter.StartDate == report.Filter.EndDate)
            //{
            //    report1.GroupHeader.Visible = false;
            //    report1.XRTitle.Text += DateHelper.GetMonthShortName(report.Filter.StartDate.Month, IsEnglish)
            //                            + "/" + report.Filter.StartDate.Year;
            //}
            //else
            //{
            report1.labelTitle.Text +=
                    (DateHelper.GetMonthShortName(payrollPeriodFrom.Month, IsEnglish)
                     + "/" + payrollPeriodFrom.Year + " to " +

                     DateHelper.GetMonthShortName(payrollPeriodTo.Month, IsEnglish)
                     + "/" + payrollPeriodTo.Year);
            


            report.DisplayReport(report1);

            if (ReportHelper.IsReportExportState())
            {
                this.report.clearCache = true;

            }


        }
    }
}
