﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//using Web.CP.Report.Templates;

using BLL;
using DevExpress.XtraReports.UI;
using Web.ReportDataSetTableAdapters;
using Utils.Calendar;
using BLL.Base;
using DAL;
using BLL.Manager;

namespace Web.CP.Report
{
    public partial class PayGratuity : BasePage
    {


        protected void Page_Load(object sender, EventArgs e)
        {
            report.Filter.Employee = true;
            report.Filter.Income = true;
            report.Filter.PayrollTo = false;
            report.Filter.Income = false;
            report.Filter.GratuityType = false;
            report.Filter.ShowGratuityGenerateButton = true;
            report.Filter.RetiredOnly = true;
            //if (!IsPostBack || CommonManager.CompanySetting.RemoveStyleWhileExportingReport)
            if (IsPostBack)
                LoadReport();
        }


        protected void LoadReport()
        {
            if (!IsPostBack)
                return;

            PayrollPeriod payrollPeriod = CommonManager.GetPayrollPeriod(report.Filter.StartDate.Month,
                                                                        report.Filter.StartDate.Year);
            if (payrollPeriod == null)
                return;


            Report_Pay_GratuityTableAdapter
                adap = new Report_Pay_GratuityTableAdapter();

            int employeeId = report.Filter.EmployeeId;

            string employeeSearch = report.Filter.EmployeeName;
            if (employeeId != 0)
                employeeSearch = "";

            Report.Templates.Pay.ReportPayGratuity report1 = new Report.Templates.Pay.ReportPayGratuity();
            BLL.BaseBiz.SetConnectionPwd(adap.Connection);
            report1.DataSource = adap.GetData(
                payrollPeriod.PayrollPeriodId, SessionManager.CurrentCompanyId,
               report.Filter.BranchId, report.Filter.DepartmentId, report.Filter.DepartmentName, report.Filter.GratuityTypeValue, employeeId, employeeSearch, report.Filter.RetiredOnlyValue);
            report1.DataMember = "Report";

            report1.labelTitle.Text += payrollPeriod.Name;       
            //if same payroll then hide group header
            //if (report.Filter.StartDate == report.Filter.EndDate)
            //{
            //    //report1.GroupHeader.Visible = false;
            //    report1.labelTitle.Text += DateHelper.GetMonthShortName(report.Filter.StartDate.Month, IsEnglish)
            //                            + "/" + report.Filter.StartDate.Year;
            //}
            //else
            {
              
            }


            report.DisplayReport(report1);

            if (ReportHelper.IsReportExportState())
            {
                this.report.clearCache = true;
            }
        }
    }
}
