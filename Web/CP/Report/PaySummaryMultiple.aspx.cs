﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using BLL;
using DevExpress.XtraReports.UI;
using Web.CP.Report.Templates.HR;
using Web.ReportDataSetTableAdapters;
using Utils.Calendar;
using BLL.Base;
using DAL;
using BLL.Manager;
using System.Data;
using DevExpress.XtraPrinting;

namespace Web.CP.Report
{
    public partial class PaySummaryMultiple : BasePage
    {
        private int Name_Column_Index = 1;

        protected  void LoadReportHandler()
        {}
      
        protected void Page_Load(object sender, EventArgs e)
        {
            //report.Filter.PayrollFrom = false;

            report.Filter.SubDepartment = true;
            report.Filter.Employee = true;

           // if (!IsPostBack || CommonManager.CompanySetting.RemoveStyleWhileExportingReport)
            if(IsPostBack)
                LoadReport();
        }

        private void Event_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (Web.CP.Report.ReportHelper.IsReportExportState())
            {
                ((XRLabel)sender).BackColor = System.Drawing.Color.Transparent;
                ((XRLabel)sender).BorderColor = System.Drawing.Color.Black;
            }

        }

        protected void ClearBorderControlEvent(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ReportHelper.TableCell_BeforePrintEvent(sender, e);
        }
        protected void ClearBorderLessControlEvent(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ReportHelper.TableCell_BeforePrintEvent(sender, e);
        }

        private void CreateUsingLabels(XtraReport report)
        //Report.Templates.Pay.SalarySummary report, List<CalcGetHeaderListResult> headerList)
        {
            DataTable dataTable = (report.DataSource as DataSet).Tables[0];
            int firstColumnExtraWidth = 60;

            Color colorHeader = Color.FromArgb(201, 214, 237);
            Color colorEven = Color.FromArgb(239, 243, 250);

            int colCount = dataTable.Columns.Count; ;//-1 for name
            int colWidth = 90;// (report.PageWidth - (report.Margins.Left + report.Margins.Right)) / colCount;

            report.PageWidth =  colWidth * colCount;

            (report as Report.Templates.Pay.SalarySummary).labelTitle.WidthF = report.PageWidth;
           
            
            int currentXLocation = 0;
            int prevWidth = 0;
            int currentWidth = 0;
            // Create header captions
            for (int i = 0; i < colCount; i++)
            {

                XRLabel label = new XRLabel();
                label.BeforePrint += new System.Drawing.Printing.PrintEventHandler(Event_BeforePrint);
                label.TextAlignment = TextAlignment.MiddleLeft;

                if (i == 0 || i == 1 || i == 2 || i == 3)
                {
                    currentWidth = colWidth / 2; //for SN
                    label.TextAlignment = TextAlignment.MiddleCenter;
                }
                //else if (i == 1 || i == 2)
                //{
                //    currentWidth = colWidth / 2; //for title

                //}
                //else if (i == 3)
                //{
                //    currentWidth = colWidth + firstColumnExtraWidth; //for name

                //}
                else if (i == 4)
                {
                    currentWidth = colWidth + 20; //first cost code

                }
                else
                {
                    currentWidth = colWidth;//for other amount columns

                }
                currentXLocation += (prevWidth);
                prevWidth = currentWidth;

                label.Location = new Point(currentXLocation, 50);


                label.Text = dataTable.Columns[i].Caption;
                label.Padding = new PaddingInfo(2, 2, 2, 2);

                label.Font = new Font(label.Font.FontFamily, label.Font.Size, FontStyle.Bold);
                label.CanGrow = false;//dont allow to grow as design will be meshed up
                // if (i != Name_Column_Index)
                if (i == 0)
                {
                    label.Borders = DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom;
                    label.Size = new Size(currentWidth, 50);
                }
                else
                    label.Borders = DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom;

                     label.Size = new Size(currentWidth, 50);
                
                ReportHelper.HeaderLabelStyle(label);
                
                // Place the headers onto a PageHeader band
                report.Bands[BandKind.PageHeader].Controls.Add(label);
            }


            currentXLocation = 0;
            prevWidth = 0;
            currentWidth = 0;
            // Create data-bound labels with different odd and even backgrounds
            for (int i = 0; i < colCount; i++)
            {
                XRLabel label = new XRLabel();
                label.BeforePrint += new System.Drawing.Printing.PrintEventHandler(Event_BeforePrint);

                if (i == 0 || i == 1 || i == 2 || i == 3)
                {
                    currentWidth = colWidth / 2;
                    label.TextAlignment = TextAlignment.MiddleCenter;
                }
                //else if (i == 1 || i == 2)
                //{
                //    currentWidth = colWidth / 2;
                //    label.TextAlignment = TextAlignment.MiddleLeft;

                //}
                //else if (i == 3)
                //{
                //    currentWidth = colWidth + firstColumnExtraWidth; //for name
                //    label.TextAlignment = TextAlignment.MiddleLeft;
                //}
                else if (i == 4)
                {
                    currentWidth = colWidth + 20; //first cost code
                    label.TextAlignment = TextAlignment.MiddleLeft;
                }
                else
                {
                    currentWidth = colWidth;
                    label.TextAlignment = TextAlignment.MiddleRight;
                }
                currentXLocation += (prevWidth);
                prevWidth = currentWidth;

                // if (i == Name_Column_Index)
                label.Location = new Point(currentXLocation, 0);
                //else
                //{
                //    label.Location = new Point((colWidth * i) + firstColumnExtraWidth, 0);                   
                //}
                label.Padding = new PaddingInfo(2, 2, 2, 0);
                if (i == Name_Column_Index)
                    label.DataBindings.Add("Text", null, dataTable.Columns[i].ColumnName);
                else
                    label.DataBindings.Add("Text", null, dataTable.Columns[i].ColumnName, "{0:n2}");




                label.CanGrow = false;


               
                //if (i != Name_Column_Index)
                //{
                if (i == 0)

                    label.Borders = DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom;

                else
                    label.Borders = DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom;

                label.Size = new Size(currentWidth, 20);
                //}
                //else
                //{
                //    label.Borders = DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom;
                //    label.TextAlignment = TextAlignment.MiddleLeft;
                //    label.Size = new Size(colWidth + firstColumnExtraWidth, 20);

                //}
                label.Font = new Font(label.Font.FontFamily, label.Font.Size, FontStyle.Regular);

                //label.BackColor = i == 0 ? colorEven : Color.White;
                ReportHelper.LabelStyle(label);
                
                // Place the labels onto a Detail band
                report.Bands[BandKind.Detail].Controls.Add(label);
            }

            currentXLocation = 0;
            prevWidth = 0;
            currentWidth = 0;

            //create footer captions for Total
            for (int i = 0; i < colCount; i++)
            {
                XRLabel label = new XRLabel();
                //set properties for sum/total


                label.BeforePrint += new System.Drawing.Printing.PrintEventHandler(Event_BeforePrint);

                if (i == 0 || i == 1 || i == 2 || i == 3)
                {
                    currentWidth = colWidth / 2;
                }
                //else if (i == 1 || i == 2)
                //{
                //    currentWidth = colWidth / 2;

                //}
                //else if (i == 3)
                //{
                //    currentWidth = colWidth + firstColumnExtraWidth; //for name

                //}
                else if (i == 4)
                {
                    currentWidth = colWidth + 20; //first cost code

                }
                else
                {
                    currentWidth = colWidth;

                }
                currentXLocation += (prevWidth);
                prevWidth = currentWidth;

                label.Location = new Point(currentXLocation, 0);

                label.Padding = new PaddingInfo(2, 2, 2, 0);




                DevExpress.XtraReports.UI.XRSummary summary = new DevExpress.XtraReports.UI.XRSummary();


                //skip for first two columns
                if (i == 3)
                {
                    label.Text = "Total:";
                }
                else if (i >= 4)
                {
                    label.DataBindings.Add("Text", null, dataTable.Columns[i].ColumnName);



                    summary.FormatString = "{0:n2}";

                    summary.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
                    label.Summary = summary;

                }


                label.CanGrow = false;

               
                //if (i != Name_Column_Index)
                //{
                if (i == 0)

                    label.Borders = DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom;

                else
                    label.Borders = DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom;

                label.TextAlignment = TextAlignment.MiddleRight;
                label.Size = new Size(currentWidth, 20);
                //}
                //else
                //{
                //    label.Text = "Total";
                //    label.Borders = DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom;
                //    label.TextAlignment = TextAlignment.MiddleLeft;
                //    label.Size = new Size(colWidth + firstColumnExtraWidth, 20);
                //}
                label.Font = new Font(label.Font.FontFamily, label.Font.Size, FontStyle.Bold);

                //label.BackColor = i == 0 ? colorEven : Color.White;
                ReportHelper.FooterLabel(label);
                
                // Place the labels onto a Detail band
                report.Bands[BandKind.ReportFooter].Controls.Add(label);
            }
        }


        protected void LoadReport()
        {
            if (!IsPostBack)
                return;

            PayrollPeriod payrollPeriodFrom = CommonManager.GetPayrollPeriod(report.Filter.StartDate.Month,
                                                                         report.Filter.StartDate.Year);
            PayrollPeriod payrollPeriodTo = CommonManager.GetPayrollPeriod(report.Filter.EndDate.Month,
                                                                                     report.Filter.EndDate.Year);
            //if null get first payroll period
            if (payrollPeriodFrom == null)
                payrollPeriodFrom = CommonManager.GetFirstPayrollPeriod(SessionManager.CurrentCompanyId);

            if (payrollPeriodTo == null)
                payrollPeriodTo = CommonManager.GetLastPayrollPeriod();

            //again null then no report
            if (payrollPeriodFrom == null || payrollPeriodTo == null)
                return;

            //for report to date should be greater or equal to from date
            if (payrollPeriodTo.PayrollPeriodId < payrollPeriodFrom.PayrollPeriodId)
                return;





            Report.Templates.Pay.SalarySummary mainReport = new Web.CP.Report.Templates.Pay.SalarySummary();
            mainReport.Name = "Consolidated Summary";

            mainReport.labelTitle.Text =
                string.Format("Consolidated YTD Salary Summary : {0}({2}) to {1}({3}) ", payrollPeriodFrom.Name, payrollPeriodTo.Name
                , DateHelper.GetMonthsForOtherPayrollPeriod(payrollPeriodFrom.Month, IsEnglish), DateHelper.GetMonthsForOtherPayrollPeriod(payrollPeriodTo.Month, IsEnglish));


            List<DAL.Report_Pay_SalarySummary_MultipleMonthsResult> data = ReportManager.GetSalarySummaryForMultiple
               (payrollPeriodFrom.PayrollPeriodId, payrollPeriodTo.PayrollPeriodId, report.Filter.EmployeeName, report.Filter.BranchId,
               report.Filter.DepartmentId, report.Filter.SubDepartmentId);

            List<Report_Pay_SalarySummary_MultipleMonthsDetailsResult> dataList = BLL.BaseBiz.PayrollDataContext
                .Report_Pay_SalarySummary_MultipleMonthsDetails(payrollPeriodFrom.PayrollPeriodId, payrollPeriodTo.PayrollPeriodId, null).ToList();

            List<CalcGetHeaderListResult> headerList = ReportManager.GetHeaderList(payrollPeriodFrom.PayrollPeriodId, payrollPeriodTo.PayrollPeriodId);
          
            CalcGetHeaderListResult opening = headerList.FirstOrDefault(x => x.Type == 0);
           
            headerList = CalculationValue.SortHeaders(headerList, PayManager.GetIncomeOrderValues(), PayManager.GetDeductionOrderValues());


            if (opening != null)
                headerList.Insert(0, opening);

            foreach (var item in data)
            {
                item.SetList(dataList.Where(x => x.EmployeeId == item.EmployeeId).ToList(), headerList, 2);

                //, headerList, 2);
            }


            if (!CommonManager.CalculationConstant.CompanyHasHourlyLeave.Value)
            {
                CalcGetHeaderListResult headerPDays = new CalcGetHeaderListResult();
                headerPDays.HeaderName = "P.Days";
                headerPDays.SourceId = (int)CalculationColumnType.ReportPDays;
                headerPDays.Type = (int)CalculationColumnType.ReportPDays;
                headerList.Insert(0, headerPDays);
            }


            DataTable dataTable = CreateDataTable(data, headerList);
            DataSet dset = new DataSet();
            dset.Tables.Add(dataTable);
            mainReport.DataSource = dset;
            mainReport.DataMember = "Report";


            // Create bands
            //DetailBand detail = new DetailBand();
            //PageHeaderBand pageHeader = new PageHeaderBand();
            //ReportFooterBand reportFooter = new ReportFooterBand();
            //detail.Height = 20;
            //reportFooter.Height = 20;
            //pageHeader.Height = 20;
            //// Place the bands onto a report
            //mainReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] { detail, pageHeader, reportFooter });









            CreateUsingLabels(mainReport);

            if (dataTable.Rows.Count > 0)
                report.DisplayReport(mainReport);

            if (ReportHelper.IsReportExportState())
            {
                this.report.clearCache = true;
            }
            // report.ReportToolbar.Width = new Unit(958);
            //report.ReportViewer.Width = new Unit(mainReport.PageWidth);

        }

        private DataTable CreateDataTable(List<DAL.Report_Pay_SalarySummary_MultipleMonthsResult> data, List<CalcGetHeaderListResult> headerList)
        {
            DataTable dataTable = new DataTable();
            dataTable.TableName = "Report";

          
            dataTable.Columns.Add("SN", typeof(string));
            dataTable.Columns.Add("EIN", typeof(string));
            dataTable.Columns.Add("Title", typeof(string));
            dataTable.Columns.Add("I No", typeof(string));
            dataTable.Columns.Add("Name", typeof(string));
            dataTable.Columns.Add("Bank Account Number", typeof(string));

            // Create table cells, fill the header cells with text, bind the cells to data
            for (int i = 0; i < headerList.Count; i++)
            {
                DataColumn column = new DataColumn(headerList[i].Type + ":" + headerList[i].SourceId, typeof(decimal));
                column.Caption = headerList[i].HeaderName;

                dataTable.Columns.Add(column);
            }

            int snNumber = 1;

            //add rows
            foreach (Report_Pay_SalarySummary_MultipleMonthsResult row in data)
            {
                List<object> list = new List<object>();
                row.SN = snNumber++;
                list.Add(row.SN);
                list.Add(row.EmployeeId);
                list.Add(row.Title);
                list.Add(row.IDCardNo == null ? "" : row.IDCardNo);
                list.Add(row.Name);
                list.Add(row.AccountNo);
                

                for (int i = 0; i < headerList.Count; i++)
                {
                    list.Add(row.GetCellValue(headerList[i].Type.Value, headerList[i].SourceId.Value, 2,null));
                }
                

                dataTable.Rows.Add(list.ToArray());
            }



            return dataTable;


        }
    }
}
