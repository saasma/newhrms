﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using BLL;
using DevExpress.XtraReports.UI;
using Web.CP.Report.Templates.HR;
using Web.ReportDataSetTableAdapters;
using Utils.Calendar;
using BLL.Base;
using DAL;
using BLL.Manager;
using System.Data;

namespace Web.CP.Report
{
    public partial class PayCIT : BasePage
    {
        protected  void LoadReportHandler()
        {        
        }
      
        protected void Page_Load(object sender, EventArgs e)
        {
            //report.Filter.PayrollFrom = false;
            report.Filter.SetEditHeaderFooter(ReportHeaderFooterEnum.CITReport);
            report.Filter.PayrollTo = false;
            report.Filter.Branch = false;
            report.Filter.Department = false;
            report.Filter.Employee = false;
            report.Filter.CostCode = true;
            report.Filter.Program = true;

            if (SessionManager.CurrentCompany.PFRFFunds.Count >= 1
         && SessionManager.CurrentCompany.PFRFFunds[0].HasOtherPFFund != null
         && SessionManager.CurrentCompany.PFRFFunds[0].HasOtherPFFund.Value)
            {
                report.Filter.PFType = true;
            }

           // if (!IsPostBack)
                LoadReport();
        }

        protected void LoadReport()
        {
            PayrollPeriod payrollPeriod = CommonManager.GetPayrollPeriod(report.Filter.StartDate.Month,
                                                                         report.Filter.StartDate.Year);
            if( payrollPeriod ==null)
                return;

            Report_Pay_CITTableAdapter mainAdapter = new Report_Pay_CITTableAdapter();

            BLL.BaseBiz.SetConnectionPwd(mainAdapter.Connection);
            ReportDataSet.Report_Pay_CITDataTable mainTable =
                mainAdapter.GetData(payrollPeriod.PayrollPeriodId, SessionManager.CurrentCompanyId
                , report.Filter.CostCodeId, report.Filter.ProgramId,report.Filter.PFTypeValue);

            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.NIBL)
            {
                Report.Templates.Pay.ReportNewCIT mainReport = new Report.Templates.Pay.ReportNewCIT();


                mainReport.lblMonthYear.Text = payrollPeriod.Name;

                SetReportDetails(mainReport);

                mainReport.DataSource = mainTable;
                mainReport.DataMember = "Report";

                report.DisplayReport(mainReport);
            }
            else
            {
                Report.Templates.Pay.ReportCIT mainReport = new Report.Templates.Pay.ReportCIT();

                ReportHeaderFooter headerFooter = VouccherManagerNew.GetReportHeaderFooter((int)ReportHeaderFooterEnum.CITReport);
                if (headerFooter != null)
                {
                    mainReport.header.Html = headerFooter.Header;
                    mainReport.footer.Html = headerFooter.Footer;
                }
           
                mainReport.lblMonthYear.Text = payrollPeriod.Name;

                SetReportDetails(mainReport);

                mainReport.DataSource = mainTable;
                mainReport.DataMember = "Report";

                report.DisplayReport(mainReport);
            }

            

            
           
           
        }

        public void SetReportDetails(Report.Templates.Pay.ReportNewCIT mainReport)
        {
            CompanyManager mgr = new CompanyManager();
            Company c = mgr.GetById(SessionManager.CurrentCompanyId);

            mainReport.lblName.Text = c.Name;
            mainReport.lblAddress.Text = c.Address;
            mainReport.lblCITCode.Text = c.CITCompanyCode;

            mainReport.lblPhone.Text = c.PhoneNo;
            if (string.IsNullOrEmpty(c.CITBankName))
                mainReport.cellBankName.Visible = false;
            else
                mainReport.lblBankName.Text = c.CITBankName;

            if (!string.IsNullOrEmpty(c.PFRFFunds[0].CITReportDescription))
                mainReport.cellDescription.Text =
                    c.PFRFFunds[0].CITReportDescription;

          //  DataView CITDataView = new DataView(mainReport);
            
        }

        public void SetReportDetails(Report.Templates.Pay.ReportCIT mainReport)
        {
            CompanyManager mgr = new CompanyManager();
            Company c = mgr.GetById(SessionManager.CurrentCompanyId);

            mainReport.lblName.Text = c.Name;
            mainReport.lblAddress.Text = c.Address;
            mainReport.lblCITCode.Text = c.CITCompanyCode;

            mainReport.lblPhone.Text = c.PhoneNo;
            //if (string.IsNullOrEmpty(c.CITBankName))
            //    mainReport.cellBankName.Visible = false;
            //else
                mainReport.lblBankName.Text = c.CITBankName;

            //if (!string.IsNullOrEmpty(c.PFRFFunds[0].CITReportDescription))
            //    mainReport.cellDescription.Text =
            //        c.PFRFFunds[0].CITReportDescription;

            //  DataView CITDataView = new DataView(mainReport);

        }
    }
}
