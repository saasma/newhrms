﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
//using Web.CP.Report.Templates;
using BLL;
using Web.ReportDataSetTableAdapters;
using DevExpress.XtraReports.Web;
using System.IO;

using BLL.Base;
using Web.CP.Report.Templates.Pay.Detail;
using System.Web;
using DevExpress.Web;
using Utils.Helper;

namespace Web.CP.Report
{
    public partial class EmployeeListDetails : BasePage
    {

        //protected void Page_Load(object sender, EventArgs e)
        //{
        //    LoadReport();
        //}

        //public ReportToolbar ReportToolbar
        //{
        //    get
        //    {
        //        return ReportToolbar1;
        //    }

        //}

        
        //private bool ClearCache = false;
        public bool clearCache
        {
            get
            {
                if (ViewState["ClearCache"] == null)
                    return false;
                return bool.Parse(ViewState["ClearCache"].ToString());
            }
            set
            {
                ViewState["ClearCache"] = value;
            }
        }

        public Unit ViewerPaddingLeft
        {
            get
            {
                return this.rptViewer.Paddings.PaddingLeft;
            }
            set
            {
                this.rptViewer.Paddings.PaddingLeft = value;
            }

        }

        public void SelectLastPayrollAndDisableDropDown()
        {
            SelectLastPayrollAndDisableDropDown();
        }
     

        //public ReportViewer ReportViewer
        //{
        //    get
        //    {
        //        return rptViewer;
        //    }
        //}

       // public event LoadReport ReloadReport;


       

        //public event CompanySpecificExport CompanyExport;


    
        //public void DisplayReport(DevExpress.XtraReports.UI.XtraReport xtraReport)
        //{




        //    this.rptViewer.Report = xtraReport;
        //    this.ReportToolbar1.ReportViewer = this.rptViewer;

        //}

       
        protected void ReportViewer1_CacheReportDocument(object sender, CacheReportDocumentEventArgs e)
        {
            return;
            ///won't work for export so commented
            //get page name as key
            //e.Key = this.Page.ToString();// Guid.NewGuid().ToString();
            //Page.Session[e.Key] = e.SaveDocumentToMemoryStream();
            //this.clearCache = false;

        }
        protected void ReportViewer1_RestoreReportDocumentFromCache(object sender, RestoreReportDocumentFromCacheEventArgs e)
        {
            return;
            ///won't work for export so commented

            //if (!this.clearCache)
            //{
            //    Stream stream = Page.Session[e.Key] as Stream;
            //    if (stream != null)
            //        e.RestoreDocumentFromStream(stream);
            //}
        }

        protected void Page_Load(object sender, EventArgs e)
        {
           
            //LoadReport();

            if (!IsPostBack)
            {
                rptFilter.PayrollFrom = false;
                rptFilter.PayrollTo = false;
                rptFilter.Branch = true;
                rptFilter.SubDepartment = true;
                rptFilter.FilterBlockMode = true;
              

            }

            if (IsPostBack)
                LoadReport();
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            //if (!IsPostBack)
            //    report.Filter.SelectLastPayroll();
        }

        protected void btnLoad_Click(object sender, EventArgs e)
        {
            LoadReport();
        }
        public void btnEmployee_Command(object sender, CommandEventArgs e)
        {
            int empId = Convert.ToInt32(e.CommandArgument);
            //cmbEmpList.SelectedItem = new DevExpress.Web.ASPxEditors.ListEditItem("", empId);
            //cmbEmpList.SelectedItem.Value = empId;
           // cmbEmpList.Value = empId;
           // cmbEmpList.Text = ((LinkButton)sender).Text;
            LoadReport();
        }
        protected void LoadReport()
        {

            if (rptFilter.EmployeeId == 0 || rptFilter.EmployeeId == -1)
            {
                JavascriptHelper.DisplayClientMsg("Employee selection is required.", this);
                return;
            }

            Report_EmployeeHRDetailsNameTableAdapter
               adap = new Report_EmployeeHRDetailsNameTableAdapter();

           HEducationTableAdapter eductaionAdap = new HEducationTableAdapter();
           HTrainingTableAdapter trainingAdap = new HTrainingTableAdapter();
          HPreviousEmploymentTableAdapter empAdap =  new HPreviousEmploymentTableAdapter();
          HDocumentTableAdapter docAdap=  new HDocumentTableAdapter();
           BLL.BaseBiz.SetConnectionPwd(eductaionAdap.Connection);
           BLL.BaseBiz.SetConnectionPwd(trainingAdap.Connection);
           BLL.BaseBiz.SetConnectionPwd(empAdap.Connection);
           BLL.BaseBiz.SetConnectionPwd(docAdap.Connection);

            // save in session
           HttpContext.Current.Items["EmployeeList"] = ReportManager.GetEmployeeHRDetails("", rptFilter.BranchId, rptFilter.DepartmentId, rptFilter.SubDepartmentId, rptFilter.EmployeeId);
           HttpContext.Current.Items["HRTrainingList"] = (trainingAdap).GetData(rptFilter.EmployeeId);
           HttpContext.Current.Items["HREducationList"] = (eductaionAdap).GetData(rptFilter.EmployeeId);
           HttpContext.Current.Items["HRPrevEmploymentList"] = (empAdap).GetData(rptFilter.EmployeeId);
           HttpContext.Current.Items["HRDocumentList"] = (docAdap).GetData(rptFilter.EmployeeId);

            ReportOtherEmployeeContainer report1 = new ReportOtherEmployeeContainer();
            report1.ShowAddress = true;
            report1.ShowHRInformation = true;
            report1.ShowQualification = true;
            report1.ShowSkillSet = true;
            report1.ShowPayInformation = true;
            ASPxListBox listBox = rptFilter.FiterListBox;
            if (listBox != null && listBox.SelectedValues != null)
            {
                foreach (object value in listBox.SelectedValues)
                {
                    if (value != null)
                    {
                        switch (value.ToString())
                        {
                            case "Address":
                                report1.ShowAddress = false;
                                break;
                            case "HR":
                                report1.ShowHRInformation = false;
                                break;
                            case "Qualification":
                                report1.ShowQualification = false;
                                break;
                            case "Skillset":
                                report1.ShowSkillSet = false;
                                break;
                            case "Pay":
                                report1.ShowPayInformation = false;
                                break;
                        }
                    }
                }
            }
            


            BLL.BaseBiz.SetConnectionPwd(adap.Connection);

            report1.DataSource = adap.GetData("", rptFilter.BranchId, rptFilter.DepartmentId,
                                           rptFilter.SubDepartmentId, SessionManager.CurrentCompanyId, rptFilter.DepartmentName, rptFilter.EmployeeId);

            //report1.DataSource = ReportManager.GetEmployeeHRDetails
              //  (rptFilter.EmployeeName, rptFilter.BranchId, rptFilter.DepartmentId, rptFilter.SubDepartmentId);

            report1.DataMember = "Employee Contact";
           // report.DisplayReport(report1);

            this.rptViewer.Report = report1;
            this.ReportToolbar1.ReportViewer = this.rptViewer;

            //if (ReportHelper.IsReportExportState())
            //{
            //    this.clearCache = true;
            //}

        }

      
    }
}
