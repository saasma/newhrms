﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//using Web.CP.Report.Templates;

using BLL;
using DevExpress.XtraReports.UI;
using Web.CP.Report.Templates.HR;
using Web.ReportDataSetTableAdapters;
using Utils.Calendar;
using BLL.Base;
using System.Data;
using BLL.Manager;

namespace Web.CP.Report
{
    public partial class HRHolidayList : BasePage
    {


        protected void Page_Load(object sender, EventArgs e)
        {
            report.Filter.DateFromControl = true;
            report.Filter.DateToControl = true;

            report.Filter.PayrollFrom = false;
            report.Filter.PayrollTo = false;

            report.Filter.Employee = false;
            report.Filter.Branch = false;
            report.Filter.Department = false;

            //if (!IsPostBack || CommonManager.CompanySetting.RemoveStyleWhileExportingReport)
                LoadReport();
        }

        protected void LoadReport()
        {
            Report_HRReport_holidaysTableAdapter adap = new Report_HRReport_holidaysTableAdapter();
            
            ReportHolidayList report1 = new ReportHolidayList();

            //report1.DataSource = adap.GetData(report.Filter.EmployeeName, report.Filter.BranchId,report.Filter.DepartmentId,
                                              //SessionManager.CurrentCompanyId);
            //report.Filter.DateFrom.Day
            //string fromYear = Convert.ToString(report.Filter.DateFrom.Year);
            //string fromMonth = Convert.ToString(report.Filter.DateFrom.Month);
            //string fromDate = Convert.ToString(report.Filter.DateFrom.Day);

            //string toYear = Convert.ToString(report.Filter.DateTo.Year);
            //string toMonth = Convert.ToString(report.Filter.DateTo.Month);
            //string toDate = Convert.ToString(report.Filter.DateTo.Day);

            //CustomDate from = new CustomDate(report.Filter.DateFrom.Day, report.Filter.DateFrom.Month, report.Filter.DateFrom.Year, IsEnglish);
            //CustomDate to = new CustomDate(report.Filter.DateTo.Day, report.Filter.DateTo.Month, report.Filter.DateTo.Year, IsEnglish);

            //DateTime fromDateEng = Convert.ToDateTime(GetEngDate(from.ToString()));
            //DateTime toDateEng = Convert.ToDateTime(GetEngDate(to.ToString()));

            BLL.BaseBiz.SetConnectionPwd(adap.Connection);
            ReportDataSet.Report_HRReport_holidaysDataTable adapGetData = adap.GetData(report.Filter.DateFrom.EnglishDate, report.Filter.DateTo.EnglishDate
                ,SessionManager.CurrentCompanyId); ;

            bool isHRReverseDate = CommonManager.CompanySetting.IsHRInOppositeDate;

            foreach (DataRow row in adapGetData.Rows)
            {
                int value = int.Parse(row["AppliesTo"].ToString());
                row["AppliesTo"] = HolidayManager.GetRemarkText(value);

                string date = row["Date"].ToString();
                //format the date
                row["Date"] = GetFormattedDate(date, isHRReverseDate);
            }

            report1.DataSource = adapGetData;

            report1.labelTitle.Text += " from " + report.Filter.DateFrom.ToString() + " to " + report.Filter.DateTo.ToString();

                          
            report1.DataMember = "HolidayListReport";            
            report.DisplayReport(report1);

            if (ReportHelper.IsReportExportState())
            {
                this.report.clearCache = true;

            }
        }

        public string GetFormattedDate(string date, bool isHRReverseDate)
        {
            if (isHRReverseDate && CompanyManager.IsEnglish ==false)
            {
                CustomDate customeDate = CustomDate.GetCustomDateFromString(date, false);
                DateTime engDate = GetEngDate(date);

                string formattedDate = engDate.Day + " " +
                    DateHelper.GetMonthName(engDate.Month, true) + ", " + engDate.Year
                    + " : " + engDate.DayOfWeek.ToString();
                return formattedDate;
            }
            else
            {
                CustomDate customeDate = CustomDate.GetCustomDateFromString(date, IsEnglish);
                DateTime engDate = GetEngDate(date);

                string formattedDate = customeDate.Day + " " +
                    DateHelper.GetMonthName(customeDate.Month, IsEnglish) + ", " + customeDate.Year
                    + " : " + engDate.DayOfWeek.ToString();
                return formattedDate;
            }
        }
       
    }
}
