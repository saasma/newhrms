﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using BLL;
using DevExpress.XtraReports.UI;
using Web.CP.Report.Templates.HR;
using Web.ReportDataSetTableAdapters;
using Utils.Calendar;
using BLL.Base;
using DAL;
using BLL.Manager;
using System.Data;
using DevExpress.XtraPrinting;
using Web.CP.Report.Templates.Pay;

namespace Web.CP.Report
{
    public partial class ProjectReport : BasePage
    {
        private int Name_Column_Index = 1;

        protected  void LoadReportHandler()
        {}
      
        protected void Page_Load(object sender, EventArgs e)
        {
            //report.Filter.PayrollFrom = false;
            report.Filter.PayrollTo = false;
            report.Filter.Employee = false;
            report.Filter.Department = false;
            report.Filter.Branch = false;
            report.Filter.Project = true;

           // if (!IsPostBack)
                LoadReport();
        }

        private void Event_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (Web.CP.Report.ReportHelper.IsReportExportState())
            {
                ((XRLabel)sender).BackColor = System.Drawing.Color.Transparent;
                ((XRLabel)sender).BorderColor = System.Drawing.Color.Black;
            }

        }
 


        private void CreateUsingLabels(XtraReport report)
        //Report.Templates.Pay.SalarySummary report, List<CalcGetHeaderListResult> headerList)
        {
            DataTable dataTable = (report.DataSource as DataSet).Tables[0];
            int firstColumnExtraWidth = 60;

            Color colorHeader = Color.FromArgb(201, 214, 237);
            Color colorEven = Color.FromArgb(239, 243, 250);

            int colCount = dataTable.Columns.Count;
            int colWidth = 80;

            report.PageWidth = 100 + colWidth * colCount;
            (report as SalarySummary).labelTitle.WidthF = report.PageWidth - 50;
            

       

         
            int currentXLocation = 0;
            int prevWidth = 0;
            int currentWidth = 0;
            // Create header captions
            for (int i = 0; i < colCount; i++)
            {


               
                if (i == 0)
                {
                    currentWidth = colWidth / 2; //for SN
                    //label.TextAlignment = TextAlignment.MiddleCenter;
                }
                else if( i==1)
                {
                    currentWidth = colWidth / 2; //for title

                }
                else if( i==2)
                {
                    currentWidth = colWidth + firstColumnExtraWidth; //for name
                   
                }
                else if (i == 3)
                {
                    currentWidth = colWidth + 20; //first cost code
                   
                }
                else
                {
                    currentWidth = colWidth;//for other amount columns
                 
                }
                currentXLocation += (prevWidth);
                prevWidth = currentWidth;


                XRLabel label = ReportHelper.CreateLabel(currentXLocation, currentWidth, dataTable.Columns[i].Caption,
                                                null, false);

                label.BeforePrint += new System.Drawing.Printing.PrintEventHandler(Event_BeforePrint);
                label.TextAlignment = TextAlignment.MiddleLeft;
                label.Font = new Font(label.Font.FontFamily,9,FontStyle.Bold);

                if (i == 0)

                    label.Borders = DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom;

                else
                    label.Borders = DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom;


               
                // Place the headers onto a PageHeader band
                ReportHelper.HeaderLabelStyle(label);
                report.Bands[BandKind.PageHeader].Controls.Add(label);
            }

           
            currentXLocation = 0;
            prevWidth = 0;
            currentWidth = 0;
            // Create data-bound labels with different odd and even backgrounds
            for (int i = 0; i < colCount; i++)
            {
                XRLabel label = new XRLabel();
                label.Text = "-";
                label.BeforePrint += new System.Drawing.Printing.PrintEventHandler(Event_BeforePrint);

                if (i == 0)
                {
                    currentWidth = colWidth / 2;
                    label.TextAlignment = TextAlignment.MiddleCenter;
                }
                else if (i == 1)
                {
                    currentWidth = colWidth / 2;
                    label.TextAlignment = TextAlignment.MiddleLeft;

                }
                else if (i == 2)
                {
                    currentWidth = colWidth + firstColumnExtraWidth; //for name
                    label.TextAlignment = TextAlignment.MiddleLeft;
                }
                else if (i == 3)
                {
                    currentWidth = colWidth + 20; //first cost code
                    label.TextAlignment = TextAlignment.MiddleLeft;
                }
                else
                {
                    currentWidth = colWidth;
                    label.TextAlignment = TextAlignment.MiddleRight;

                    //label.EvaluateBinding += new BindingEventHandler(BindingEventHandler);
                }
                currentXLocation += (prevWidth);
                prevWidth = currentWidth;

               // if (i == Name_Column_Index)
                    label.Location = new Point(currentXLocation, 0);
                //else
                //{
                //    label.Location = new Point((colWidth * i) + firstColumnExtraWidth, 0);                   
                //}
                label.Padding = new PaddingInfo(2, 2, 2, 0);
                if (i == Name_Column_Index)
                    label.DataBindings.Add("Text", null, dataTable.Columns[i].Caption);
                else
                    label.DataBindings.Add("Text", null, dataTable.Columns[i].Caption, "{0:n2}");

               
              

                
                label.CanGrow = false;
                
                
               // label.OddStyleName = "OddStyle";
                //label.EvenStyleName = "EvenStyle";
                //if (i != Name_Column_Index)
                //{
                if (i == 0)

                    label.Borders = DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom;

                else
                    label.Borders = DevExpress.XtraPrinting.BorderSide.Right |  DevExpress.XtraPrinting.BorderSide.Bottom;
                   
                    label.Size = new Size(currentWidth, 20);
                //}
                //else
                //{
                //    label.Borders = DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom;
                //    label.TextAlignment = TextAlignment.MiddleLeft;
                //    label.Size = new Size(colWidth + firstColumnExtraWidth, 20);
                    
                //}
                label.Font = new Font(label.Font.FontFamily, label.Font.Size , FontStyle.Regular);

                //label.BackColor = i == 0 ? colorEven : Color.White;
                ReportHelper.LabelStyle(label);
                //label.BorderColor = colorHeader;
                // Place the labels onto a Detail band
                report.Bands[BandKind.Detail].Controls.Add(label);
            }

            currentXLocation = 0;
            prevWidth = 0;
            currentWidth = 0;

            //create footer captions for Total
            for (int i = 0; i < colCount; i++)
            {
                XRLabel label = new XRLabel();
                //set properties for sum/total
                

                label.BeforePrint += new System.Drawing.Printing.PrintEventHandler(Event_BeforePrint);

                if (i == 0)
                {
                    currentWidth = colWidth / 2;
                }
                else if (i == 1)
                {
                    currentWidth = colWidth / 2;

                }
                else if (i == 2)
                {
                    currentWidth = colWidth + firstColumnExtraWidth; //for name

                }
                else if (i == 3)
                {
                    currentWidth = colWidth + 20; //first cost code

                }
                else
                {
                    currentWidth = colWidth;

                }
                currentXLocation += (prevWidth);
                prevWidth = currentWidth;

                    label.Location = new Point(currentXLocation, 0);

                label.Padding = new PaddingInfo(2, 2, 2, 0);


                

                DevExpress.XtraReports.UI.XRSummary summary = new DevExpress.XtraReports.UI.XRSummary();


                //skip for first two columns
                if (i == 3)
                {
                    label.Text = "Total:";
                }
                else if (i >=4)
                {
                    label.DataBindings.Add("Text", null, dataTable.Columns[i].Caption);



                    summary.FormatString = "{0:n2}";
                    
                    summary.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
                    label.Summary = summary;

                }


                label.CanGrow = false;

                // label.OddStyleName = "OddStyle";
                //label.EvenStyleName = "EvenStyle";
                //if (i != Name_Column_Index)
                //{
                if( i==0)

                    label.Borders = DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom;

                else
                    label.Borders = DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom;

                    label.TextAlignment = TextAlignment.MiddleRight;
                    label.Size = new Size(currentWidth, 20);
                //}
                //else
                //{
                //    label.Text = "Total";
                //    label.Borders = DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom;
                //    label.TextAlignment = TextAlignment.MiddleLeft;
                //    label.Size = new Size(colWidth + firstColumnExtraWidth, 20);
                //}
                label.Font = new Font(label.Font.FontFamily, label.Font.Size, FontStyle.Bold);

                //label.BackColor = i == 0 ? colorEven : Color.White;
                ReportHelper.FooterLabel(label);
                //label.BorderColor = colorHeader;
                // Place the labels onto a Detail band
                report.Bands[BandKind.ReportFooter].Controls.Add(label);
            }
        }


        public void BindingEventHandler(
   object sender,
   BindingEventArgs e
)
        {
            if (e.Value == DBNull.Value)// || e.Value.ToString() == "0")
                e.Value = "0.00";
           
        }

        protected void LoadReport()
        {
            PayrollPeriod payrollPeriod = CommonManager.GetPayrollPeriod(report.Filter.StartDate.Month,
                                                                         report.Filter.StartDate.Year);

            if (payrollPeriod == null)
                return;

            Report.Templates.Pay.SalarySummary mainReport = new Web.CP.Report.Templates.Pay.SalarySummary();
            mainReport.Name = "Project Report";

            mainReport.labelTitle.CanGrow = true;
            mainReport.labelTitle.Multiline = true;
            mainReport.labelTitle.Text = report.Filter.ProjectName + "\n\n";

            mainReport.labelTitle.Text += string.Format("{0} ({1})", payrollPeriod.Name,
                                                        DateHelper.GetMonthsForOtherPayrollPeriod(payrollPeriod.Month,
                                                                                                  IsEnglish));
           



            List<DAL.Report_Project_EmployeeListResult> data = ProjectManager
                .GetEmployeePayEmployeeList(payrollPeriod.PayrollPeriodId, report.Filter.ProjectId);

            List<DAL.Report_Project_HeaderListResult> headerList =
                ProjectManager.GetProjectPayHeaderList(payrollPeriod.PayrollPeriodId, SessionManager.CurrentCompanyId,
                                                       report.Filter.ProjectId);





            decimal total = 0;

            DataTable dataTable = CreateDataTable(data, headerList,ref total);

            mainReport.labelTitle.Text += "\n\nAmounts in NRs. " + total.ToString("N2");

            DataSet dset = new DataSet();
            dset.Tables.Add(dataTable);
            mainReport.DataSource = dset;
            mainReport.DataMember = "Report";


            // Create bands
            //DetailBand detail = new DetailBand();
            //PageHeaderBand pageHeader = new PageHeaderBand();
            //ReportFooterBand reportFooter = new ReportFooterBand();
            //detail.Height = 20;
            //reportFooter.Height = 20;
            //pageHeader.Height = 20;
            //// Place the bands onto a report
            //mainReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] { detail, pageHeader, reportFooter });









            CreateUsingLabels(mainReport);

            if (data.Count > 0)

                report.DisplayReport(mainReport);

            // report.ReportToolbar.Width = new Unit(958);
            //report.ReportViewer.Width = new Unit(mainReport.PageWidth);

        }

        private DataTable CreateDataTable(List<DAL.Report_Project_EmployeeListResult> data, List<Report_Project_HeaderListResult> headerList,ref decimal total)
        {
            DataTable dataTable = new DataTable();
            dataTable.TableName = "Report";

            string pf = null;
            string inc = null;

            List<string> columnNameList = new List<string>();

            dataTable.Columns.Add("SN", typeof(string));
            dataTable.Columns.Add("Title", typeof(string));
            dataTable.Columns.Add("Name", typeof(string));
            dataTable.Columns.Add("Position", typeof(string));
            dataTable.Columns.Add("Grade", typeof(string));

            // Create table cells, fill the header cells with text, bind the cells to data
            for (int i = 0; i < headerList.Count; i++)
            {
               
                //if column name already exists then add space
                if (columnNameList.Contains(headerList[i].HeaderName))
                {
                    //add space to distinguish from income type
                    dataTable.Columns.Add(headerList[i].HeaderName + " ", typeof(decimal));
                }
                else
                {
                    //TODO: rem for adjustment    
                    //if (!dataTable.Columns.Contains(headerList[i].HeaderName))
                    dataTable.Columns.Add(headerList[i].HeaderName, typeof(decimal));
                }

                columnNameList.Add(headerList[i].HeaderName);
            }

            //add rows
            decimal? value;
            int index = 1;
            foreach (Report_Project_EmployeeListResult row in data)
            {
                List<object> list = new List<object>();
                list.Add(index++);
                list.Add(row.Title);
                list.Add(row.Name);
                list.Add(row.Position);
                list.Add(row.Grade);
                
                for (int i = 0; i < headerList.Count; i++)
                {
                    value = row.GetCellValue(int.Parse( headerList [i].Type.ToString()), headerList[i].SourceId.Value, 2, null);
                    //if (value == null)
                    //    list.Add("0");
                    //else
                        list.Add(value);
                }

                dataTable.Rows.Add(list.ToArray());
                decimal? sum = row.GetCellValue(5, 5, 2, null);
                total += (sum == null ? 0 : sum.Value);

            }



            return dataTable;


        }
    }
}
