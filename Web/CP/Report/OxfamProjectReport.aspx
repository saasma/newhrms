<%@ Page Title="Project Report" Language="C#" MaintainScrollPositionOnPostback="true"
    EnableEventValidation="false" MasterPageFile="~/Master/HROld.Master" AutoEventWireup="true"
    CodeBehind="OxfamProjectReport.aspx.cs" Inherits="Web.CP.Report.OxfamProjectReport" %>

<%@ Register Assembly="DevExpress.XtraReports.v16.1.Web, Version=16.1.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraReports.Web" TagPrefix="dxxr" %>
<%@ Register Src="~/Controls/Report/ReportFilterBranchDep.ascx" TagName="ReportFilterBranchDep"
    TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Report/ReportContainer.ascx" TagName="ReportContainer"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .w3-note
        {
            background-color: #ffffcc;
            border-left: 6px solid #ffeb3b;
        }
        
        .w3-panel
        {
            padding: 8px 16px;
            margin-bottom: 16px !important;
            width: 460px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                   Project Report</h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
       
       <div style="padding-bottom:10px">
       <span> Report Type &nbsp;</span>
        <asp:DropDownList ID="cmbReportType" Width="120px" DataTextField="Text" DataValueField="Value"
            runat="server">
            <asp:ListItem Text="Project Report" Value="1" Selected="True" />
            <asp:ListItem Text="Tax Voucher" Value="2" />
        </asp:DropDownList>
        </div>
      
        <uc1:ReportContainer runat="server" Message="" OnReloadReport="LoadReport" id="report" />
        <%--   <ext:ComboBox ID="cmbReportType" runat="server" Width="80" SelectOnFocus="true" Selectable="true"
            ValueField="Value" DisplayField="Text">
            <Items>
                <ext:ListItem Value="1" Text="" />
                <ext:ListItem Value="2" Text="" />
            </Items>
            <SelectedItems>
                <ext:ListItem Index="0">
                </ext:ListItem>
            </SelectedItems>
        </ext:ComboBox>--%>
    </div>
</asp:Content>
