﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using BLL;
using DevExpress.XtraReports.UI;
using Web.CP.Report.Templates.HR;
using Web.ReportDataSetTableAdapters;
using Utils.Calendar;
using BLL.Base;
using DAL;
using BLL.Manager;
using System.Data;

namespace Web.CP.Report
{
    public partial class CITDetails : BasePage
    {


        protected  void LoadReportHandler()
        {}
      
        protected void Page_Load(object sender, EventArgs e)
        {
            //report.Filter.PayrollFrom = false;
            report.Filter.PayrollTo = false;

            report.Filter.Branch = false;
            report.Filter.Department = false;
            report.Filter.Employee = true;

            report.Filter.RetiredOnly = true;
            //if (!IsPostBack || CommonManager.CompanySetting.RemoveStyleWhileExportingReport)
                LoadReport();
        }

        protected void LoadReport()
        {
            PayrollPeriod payrollPeriod = CommonManager.GetPayrollPeriod(report.Filter.StartDate.Month,
                                                                         report.Filter.StartDate.Year);

            if( payrollPeriod ==null)
                return;


            bool readingSumForMiddleFiscalYearStartedReqd = false;
            int startingPayrollPeriodId = 0, endingPayrollPeriodId = 0;
            PayrollPeriod startingMonthParollPeriodInFiscalYear =
               CalculationManager.GenerateForPastIncome(
                    payrollPeriod.PayrollPeriodId, ref readingSumForMiddleFiscalYearStartedReqd,
                    ref startingPayrollPeriodId, ref endingPayrollPeriodId);

            Report_GetMonthlyCITTableAdapter
                adap = new Report_GetMonthlyCITTableAdapter();

            Report.Templates.Pay.CITDetails mainReport = new Report.Templates.Pay.CITDetails();


            mainReport.labelHeader.Text += payrollPeriod.Name;
            BLL.BaseBiz.SetConnectionPwd(adap.Connection);
            ReportDataSet.Report_GetMonthlyCITDataTable table =
                adap.GetData(payrollPeriod.PayrollPeriodId,startingPayrollPeriodId,endingPayrollPeriodId,
                                                                report.Filter.RetiredOnlyValue,report.Filter.EmployeeId);

          



            mainReport.DataSource = table;

           

            mainReport.DataMember = "Report1";
          

            report.DisplayReport(mainReport);

           if(ReportHelper.IsReportExportState())
           {
               this.report.clearCache = true;
           }

        }
    }
}
