﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//using Web.CP.Report.Templates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using Web.CP.Report.Templates.HR;
using Web.ReportDataSetTableAdapters;
using DevExpress.XtraReports.Web;
using System.IO;

using BLL;
using DevExpress.XtraReports.UI;
using Web.ReportDataSetTableAdapters;
using Utils.Calendar;
using BLL.Base;
using BLL.Manager;
using DAL;
using Utils;
using Web.UserControls;
using DevExpress.Web;

namespace Web.CP.Report
{
    public partial class SkillsetReport : BasePage
    {

        //protected void Page_Load(object sender, EventArgs e)
        //{
        //    LoadReport();
        //}

        //public ReportToolbar ReportToolbar
        //{
        //    get
        //    {
        //        return ReportToolbar1;
        //    }

        //}


        //private bool ClearCache = false;
        public bool clearCache
        {
            get
            {
                if (ViewState["ClearCache"] == null)
                    return false;
                return bool.Parse(ViewState["ClearCache"].ToString());
            }
            set
            {
                ViewState["ClearCache"] = value;
            }
        }

        public Unit ViewerPaddingLeft
        {
            get
            {
                return this.rptViewer.Paddings.PaddingLeft;
            }
            set
            {
                this.rptViewer.Paddings.PaddingLeft = value;
            }

        }

        public void SelectLastPayrollAndDisableDropDown()
        {
            SelectLastPayrollAndDisableDropDown();
        }


        //public ReportViewer ReportViewer
        //{
        //    get
        //    {
        //        return rptViewer;
        //    }
        //}

        // public event LoadReport ReloadReport;




        //public event CompanySpecificExport CompanyExport;



        //public void DisplayReport(DevExpress.XtraReports.UI.XtraReport xtraReport)
        //{




        //    this.rptViewer.Report = xtraReport;
        //    this.ReportToolbar1.ReportViewer = this.rptViewer;

        //}


        protected void ReportViewer1_CacheReportDocument(object sender, CacheReportDocumentEventArgs e)
        {
            return;
            ///won't work for export so commented
            //get page name as key
            //e.Key = this.Page.ToString();// Guid.NewGuid().ToString();
            //Page.Session[e.Key] = e.SaveDocumentToMemoryStream();
            //this.clearCache = false;

        }
        protected void ReportViewer1_RestoreReportDocumentFromCache(object sender, RestoreReportDocumentFromCacheEventArgs e)
        {
            return;
            ///won't work for export so commented

            //if (!this.clearCache)
            //{
            //    Stream stream = Page.Session[e.Key] as Stream;
            //    if (stream != null)
            //        e.RestoreDocumentFromStream(stream);
            //}
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            //LoadReport();

            //if (!IsPostBack)
            {


                ASPxListBox listBox = (ASPxListBox)this.ddlSkillSetList.FindControl("listBox");
                listBox.ValueType = typeof(int);

                listBox.DataSource = new CommonManager().GetAllSkillSet().OrderBy(x => x.Name).ToList();
                listBox.DataBind();

                listBox.Items.Insert(0, new ListEditItem { Text = "(Select All)", Value = "" });
            }
            LoadReport();
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            //if (!IsPostBack)
            //    report.Filter.SelectLastPayroll();
        }

        protected void btnLoad_Click(object sender, EventArgs e)
        {
            LoadReport();
        }

        public string GetSelectedSkillSet()
        {
            ASPxListBox listBox = (ASPxListBox)this.ddlSkillSetList.FindControl("listBox");
            string str = "";
            if (listBox != null && listBox.SelectedValues != null)
            {
                foreach (object value in listBox.SelectedValues)
                {
                    if (value != null)
                    {
                        if (str == "")
                            str = value.ToString();
                        else
                            str += "," + value.ToString();
                    }
                }
            }
            return str;
        }

        protected void LoadReport()
        {



            Report.Templates.HR.SkillSetReport report1 = new Report.Templates.HR.SkillSetReport();
            Report_SkillSetEmployeeListTableAdapter adap = new Report_SkillSetEmployeeListTableAdapter();

            BLL.BaseBiz.SetConnectionPwd(adap.Connection);
            ReportDataSet.Report_SkillSetEmployeeListDataTable adapGetData =
                adap.GetData(chkNotHavingSkillSet.Checked, GetSelectedSkillSet(), SessionManager.CurrentCompanyId);

            //for (int i = adapGetData.Rows.Count - 1; i >= 0; i--)
            //{
            //    if (i - 1 > 0)
            //    {

            //    }
            //}


           
            report1.DataSource = adapGetData;
            report1.DataMember = "Report";

            this.rptViewer.Report = report1;
            this.ReportToolbar1.ReportViewer = this.rptViewer;

            //report.DisplayReport(report1);

        }

    }
}
