﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using BLL;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using Web.ReportDataSetTableAdapters;
using BLL.Manager;

using Utils;
using Web.CP.Report.Templates.HR;
using Utils.Base;
using System.IO;
using DAL;

namespace Web.CP.Report
{


    public static  class ReportHelper
    {
        private static DevExpress.XtraReports.UI.XRControlStyle EvenStyle;

        public static void SetEqualRows(DataTable income, DataTable deduction)
        {
            

            if (income.Rows.Count > deduction.Rows.Count)
            {
                int diff = income.Rows.Count - deduction.Rows.Count;
                for (int i = 0; i < diff; i++)
                {
                    deduction.Rows.Add(deduction.NewRow());
                }
            }
            else if( deduction.Rows.Count > income.Rows.Count)
            {
                int diff = deduction.Rows.Count - income.Rows.Count;
                for (int i = 0; i < diff; i++)
                {
                    income.Rows.Add(income.NewRow());
                }
            }
        }

        public static void SetEqualRows(DataTable income, DataTable deduction,int employeeId)
        {


            if (income.Rows.Count > deduction.Rows.Count)
            {
                int diff = income.Rows.Count - deduction.Rows.Count;
                for (int i = 0; i < diff; i++)
                {
                    DataRow r = deduction.NewRow();
                    if (deduction.Columns["EIN"] != null)
                        r["EIN"] = employeeId;
                    deduction.Rows.Add(r);
                }
            }
            else if (deduction.Rows.Count > income.Rows.Count)
            {
                int diff = deduction.Rows.Count - income.Rows.Count;
                for (int i = 0; i < diff; i++)
                {
                    DataRow rr = income.NewRow();
                    if (income.Columns["EmployeeId"] != null)
                        rr["EmployeeId"] = employeeId;
                    income.Rows.Add(rr);
                }
            }
        }

        public static decimal GetTotal(DataTable table, string columnName)
        {
            decimal total = 0;

            foreach (DataRow row in table.Rows)
            {
                if( !DBNull.Value.Equals(row[columnName]))
                {
                    decimal value = decimal.Parse(row[columnName].ToString());

                    //if (value >= 0)
                        total += value;
                }
            }

            return total;
        }

        //Label style
        public static void LabelStyle(XRLabel label)
        {
            label.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(190)))), ((int)(((byte)(216)))));
            label.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(243)))), ((int)(((byte)(250)))));
        }

        public static void LabelOddStyle(XRLabel label)
        {

        }
        public static void LabelEvenStyle(XRLabel label)
        {
            EvenStyle = new DevExpress.XtraReports.UI.XRControlStyle();
            // 
            // EvenStyle
            // 
            EvenStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(243)))), ((int)(((byte)(250)))));
            EvenStyle.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(190)))), ((int)(((byte)(216)))));
            EvenStyle.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            EvenStyle.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            EvenStyle.Name = "EvenStyle";
            EvenStyle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;

            label.EvenStyleName = "EvenStyle";
        }
        public static void FooterLabel(XRLabel label)
        {
            label.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(190)))), ((int)(((byte)(216)))));
        }

        //Header Label style
        public static void HeaderLabelStyle(XRLabel label)
        {
            label.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(214)))), ((int)(((byte)(237)))));
            label.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(190)))), ((int)(((byte)(216)))));
        }

        //public static void TableCell_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        //{
        //    if (CommonManager.CompanySetting.RemoveStyleWhileExportingReport && Web.CP.Report.ReportHelper.IsReportExportState())
        //    {
        //        ((DevExpress.XtraReports.UI.XRControl)sender).BackColor = System.Drawing.Color.Transparent;
        //        ((DevExpress.XtraReports.UI.XRControl)sender).BorderColor = System.Drawing.Color.Black;
        //    }
        //}

        

        static Color colorHeader = Color.FromArgb(199, 209, 228);
        private static Color colorBorder = Color.FromArgb(175, 190, 216);
        static Color colorEven = Color.FromArgb(239, 243, 250);

       


        public static void TableCell_BeforePrintEvent(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (CommonManager.CompanySetting.RemoveStyleWhileExportingReport && Web.CP.Report.ReportHelper.IsReportExportState()
                && (DevExpress.XtraReports.UI.XRControl)sender != null)
            {
                ((DevExpress.XtraReports.UI.XRControl)sender).BackColor = System.Drawing.Color.Transparent;
                ((DevExpress.XtraReports.UI.XRControl)sender).BorderColor = System.Drawing.Color.Black;

                ((XRControl)sender).StyleName = "";
                ((XRControl)sender).EvenStyleName = "";
                ((XRControl)sender).OddStyleName = "";
                
               
            }

            

        }


        public static Dictionary<string, Color> attendanceCellColors = GetAttendanceColor();

        public static Dictionary<string, Color> GetAttendanceColor()
        {
            Dictionary<string, Color> colors = new Dictionary<string, Color>();
            List<DAL.LLeaveType> leaves = LeaveAttendanceManager.GetAllLeaves(SessionManager.CurrentCompanyId);
            
            foreach (DAL.LLeaveType type in leaves)
            {
                colors[type.Abbreviation] = System.Drawing.ColorTranslator.FromHtml(type.LegendColor);

                if (type.IsHalfDayAllowed.Value)
                {
                    colors[type.Abbreviation + "/2"] = System.Drawing.ColorTranslator.FromHtml(type.LegendColor);
                }
            
            }


            //colors[DAL.HolidaysConstant.National_Holiday] = Color.FromName(DAL.HolidaysConstant.GetColor(DAL.HolidaysConstant.National_Holiday));
            colors[DAL.HolidaysConstant.Caste_Holiday] = Color.FromName(DAL.HolidaysConstant.GetColor(DAL.HolidaysConstant.Caste_Holiday));
            colors[DAL.HolidaysConstant.Weekly_Holiday] = System.Drawing.ColorTranslator.FromHtml(DAL.HolidaysConstant.GetColor(DAL.HolidaysConstant.Weekly_Holiday));
            //colors[DAL.HolidaysConstant.Female_Holiday] = Color.FromName(DAL.HolidaysConstant.GetColor(DAL.HolidaysConstant.Female_Holiday));
            
            colors["UPL"] = Color.FromName(Config.UPLColor);
            colors["UPL/2"] = Color.FromName(Config.UPLColor);

            colors["FH"] = Color.FromName(Config.atteColor("FH"));
            colors["NH"] = Color.FromName(Config.atteColor("NH"));

            colors["ABS"] = Color.FromName(Config.UPLColor);
            colors["ABS/2"] = Color.FromName(Config.UPLColor);


            return colors;
        }

        public static Color GetAttendanceCellColor(string value)
        {
            if (attendanceCellColors.ContainsKey(value))
                return attendanceCellColors[value];

            return Color.White; 
        }

        public static XRLabel CreateLabel(int x, int width, string headerName, XRLabel fontRef,bool isGroupType)
        {
            XRLabel groupLabel = new XRLabel();
            if (isGroupType)
            groupLabel.Location = new Point(x, 20);
            else
            {
                groupLabel.Location = new Point(x, 50);
            }
            groupLabel.Text = headerName;
            if(isGroupType)
            groupLabel.Size = new Size(width, 30);
            else
            {
                groupLabel.Size = new Size(width, 50);
            }
            groupLabel.BackColor = colorHeader;
            groupLabel.BorderColor = colorBorder;
            if (fontRef != null)
                groupLabel.Font = new Font(fontRef.Font.FontFamily, fontRef.Font.Size, FontStyle.Bold);
            groupLabel.CanGrow = false; //dont allow to grow as design will be meshed up

            //groupLabel.Borders = DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Top;
                               
            groupLabel.TextAlignment = TextAlignment.MiddleCenter;
            groupLabel.BeforePrint += new System.Drawing.Printing.PrintEventHandler(ReportHelper.Label_BeforePrint);
            groupLabel.Padding = new PaddingInfo(4,2,2,0);



            return groupLabel;

        }



        public static void Label_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (CommonManager.CompanySetting.RemoveStyleWhileExportingReport  && Web.CP.Report.ReportHelper.IsReportExportState())
            {
                ((XRLabel)sender).BackColor = System.Drawing.Color.Transparent;
                ((XRLabel)sender).BorderColor = System.Drawing.Color.Black;
            }

        }
        public static bool IsReportExportState()
        {
            string eventArgument = HttpContext.Current.Request.Form["__EVENTARGUMENT"];
            if (eventArgument != null && (eventArgument.Contains("saveToDisk") || eventArgument.Contains("saveToWindow")))
            {
                return true;
            }
            return false;
        }


        /// <summary>
        /// Returns Company Info DataTable
        /// </summary>
        /// <returns></returns>
        public static   ReportDataSet.Report_Company_InfoDataTable GetCompanyDataTable()
        {
            ReportDataSet.Report_Company_InfoDataTable compayInfoTable =
                MyCache.GetFromCache<ReportDataSet.Report_Company_InfoDataTable>("GetCompanyDataTable");

            if (compayInfoTable != null)
                return compayInfoTable;
   

            Report_Company_InfoTableAdapter companyInfoAdapter =
                new Report_Company_InfoTableAdapter();

            DAL.BaseBiz.SetConnectionPwd(companyInfoAdapter.Connection);

            compayInfoTable =
                companyInfoAdapter.GetData(SessionManager.CurrentCompanyId); ;

            MyCache.SaveToCache<ReportDataSet.Report_Company_InfoDataTable>("GetCompanyDataTable", compayInfoTable);

            return compayInfoTable;
          
        }

        public static void BindCompanyInfo(object sender)
        {
            XRSubreport subReport = sender as XRSubreport;
            if (subReport.Name.Equals("subReportCompanyInfo"))
            {
                Web.CP.Report.Templates.HR.ReportCompanyInfo companyInfoReport =
                    (Web.CP.Report.Templates.HR.ReportCompanyInfo)subReport.ReportSource;

                

                ReportDataSet.Report_Company_InfoDataTable compayInfoTable = ReportHelper.GetCompanyDataTable();

                companyInfoReport.DataSource = compayInfoTable;
                companyInfoReport.DataMember = "Company Information";
            }
        }

        public static ReportTravellAllowanceSettlement GetTravellAllowanceSettlementReport(int requestId, string travellingFromDate, string travellingToDate)
        {

            //List<GetPAREmployeeHeaderResult> data = EmployeeManager.GetPAREmployeeHeader(hdnEmployeeID.Value.ToString()).ToList();


            DAL.TARequest request = new DAL.TARequest();
            request = TravelAllowanceManager.getRequestByID(requestId);

            travellingFromDate = request.TravellingFromEng.Value.ToString("dd MMM yyyy");
            travellingToDate = request.TravellingToEng.Value.ToString("dd MMM yyyy");

            ReportDataSet.Report_TravellAllowanceSettlementHeaderInfoDataTable dtHeader = new ReportDataSet.Report_TravellAllowanceSettlementHeaderInfoDataTable();
            ReportDataSet.Report_TravellAllowanceSettlementPhaseIDataTable dtTravellAllowancePhaseI = new ReportDataSet.Report_TravellAllowanceSettlementPhaseIDataTable();
            ReportDataSet.Report_TravellAllowanceSettlementPhaseIIDataTable dtTravellAllowancePhaseII = new ReportDataSet.Report_TravellAllowanceSettlementPhaseIIDataTable();
            ReportDataSet.Report_TravellAllowanceSettlementPhaseIIIDataTable dtTravellAllowancePhaseIII = new ReportDataSet.Report_TravellAllowanceSettlementPhaseIIIDataTable();

            //ReportDataSetTableAdapters.GetPAREmployeeHeaderTableAdapter adap =
            //    new ReportDataSetTableAdapters.GetPAREmployeeHeaderTableAdapter();
            ReportDataSet ds = new ReportDataSet();



            DAL.EEmployee eemployee = EmployeeManager.GetEmployeeById(request.EmployeeId.Value);

            //txtAdvanceAmount.Text = GetCurrency(request.Advance);
            string LocationText = "";

            if (request.LocationId != null)
                LocationText = TravelAllowanceManager.GetLocationList().SingleOrDefault(x => x.LocationId == request.LocationId.Value).LocationName;
            else
                LocationText = request.LocationName;

            PPay pay = BLL.BaseBiz.PayrollDataContext.PPays.FirstOrDefault(x => x.EmployeeId == request.EmployeeId);


            if (request.AdvanceStatus == (int)AdvanceStatusEnum.HRAdvance)
            {
                List<DAL.TARequestLine> lines = new List<DAL.TARequestLine>();
                lines = TravelAllowanceManager.getAllowanceByEmployeeID(SessionManager.CurrentLoggedInEmployeeId, request.RequestID, request.LocationId.Value);

                foreach (DAL.TARequestLine _item in lines)
                {
                    dtTravellAllowancePhaseI.Rows.Add(_item.AllowanceName, _item.Quantity, GetCurrency( _item.Rate), GetCurrency(_item.Rate.Value * decimal.Parse(_item.Quantity.Value.ToString())));

                }

                foreach (var line in lines)
                {
                    line.FromDate = travellingFromDate;
                    line.ToDate = travellingToDate;
                    line.AllowType = 1;
                    if (line.Total != null)
                    {
                        line.ActualAmount = line.Total.Value;
                        line.ApprovedAmout = (decimal)line.Quantity * line.Rate.Value; ;
                    }
                }

                decimal total = lines.Sum(x => x.Total).Value;
                string dispTotalApproved = GetCurrency(lines.Sum(x => (decimal)x.Quantity.Value * x.Rate.Value));
                string dispTotalSettle = GetCurrency(lines.Sum(x => x.FinalTotal == null ? Convert.ToDecimal(x.SettTotal) : Convert.ToDecimal(x.FinalTotal)));

                foreach (DAL.TARequestLine _item in lines)
                {
                    dtTravellAllowancePhaseII.Rows.Add(_item.FromDate, _item.ToDate, _item.Quantity, GetCurrency(_item.Total));
                    dtTravellAllowancePhaseIII.Rows.Add(GetAllowTextById(_item.AllowType), GetCurrency(_item.Total));
                }


                dtHeader.Rows.Add(eemployee.Name, eemployee.Branch.Name + "," + eemployee.Department.Name, eemployee.EDesignation.Name, request.PlaceOfTravel + "," + request.CountryName,
                    request.CountryName, request.PurposeOfTravel, Convert.ToDateTime(request.SettTravellingFromEng).ToShortDateString(), Convert.ToDateTime(request.SettTravellingToEng).ToShortDateString(), request.Days + " Days " + request.Night + " Night", request.Night,
                    request.TravelByText, request.ExpensePaidByText, LocationText, dispTotalApproved, GetCurrency(total), dispTotalSettle,
                    GetCurrency(request.Advance), GetCurrency(total - (request.Advance == null ? 0 : request.Advance)).ToString(),
                    GetCurrency(request.RequestedAdvance), (pay == null || pay.BankACNo == null ? "" : pay.BankACNo));
            }
            else
            {


                List<DAL.TARequestLine> lines = new List<DAL.TARequestLine>();
                if (request.LocationId != null)
                {
                    lines = TravelAllowanceManager.getAllowanceByEmployeeID(SessionManager.CurrentLoggedInEmployeeId, request.RequestID, request.LocationId.Value);
                    //storeAllowances.DataSource = lines;
                    //storeAllowances.DataBind();
                    foreach (DAL.TARequestLine _item in lines)
                    {
                        dtTravellAllowancePhaseI.Rows.Add(_item.AllowanceName, _item.Quantity, GetCurrency(_item.Rate), GetCurrency(_item.Rate.Value * decimal.Parse(_item.Quantity.Value.ToString())));

                    }
                }


                string dispTotalApproved = GetCurrency(lines.Sum(x => (decimal)x.Quantity.Value * x.Rate.Value));
                foreach (var line in lines)
                {
                    line.FromDate = travellingFromDate;
                    line.ToDate = travellingToDate;
                    line.ApprovedAmout = (decimal)line.Quantity * line.Rate.Value; ;
                    line.Quantity = line.SettQuantity;
                    //if (line.Total != null)

                    line.Total = line.SettTotal;
                    if (line.Total != null)
                        line.ActualAmount = line.Total.Value;
                    //if null Actual or else selected
                    line.AllowType = line.FinalAllowType == null ? 1 : line.FinalAllowType.Value;

                }

                decimal settlementTotal = lines.Sum(x => x.FinalTotal == null ? x.Total : x.FinalTotal.Value).Value;
                decimal total = lines.Sum(x => x.Total).Value;

                //dispTotal.Text = GetCurrency(total);
                //dispTotalSettle.Text = GetCurrency(total);

                //storeSettlement.DataSource = lines;
                //storeSettlement.DataBind();

                //storeSettlementFinal.DataSource = lines;
                //storeSettlementFinal.DataBind();
                // txtPayment.Text = GetCurrency((Math.Abs(decimal.Parse(dispTotalSettle.Text) - decimal.Parse(txtAdvanceAmount.Text)).ToString()));

                foreach (DAL.TARequestLine _item in lines)
                {
                    dtTravellAllowancePhaseII.Rows.Add(_item.FromDate, _item.ToDate, _item.Quantity, GetCurrency(_item.Total));

                    dtTravellAllowancePhaseIII.Rows.Add(GetAllowTextById(_item.AllowType), GetCurrency(_item.FinalTotal == null ? _item.Total : _item.FinalTotal.Value));
                }

                dtHeader.Rows.Add(eemployee.Name, eemployee.Branch.Name + "," + eemployee.Department.Name, eemployee.Department.Name, eemployee.EDesignation.Name, request.PlaceOfTravel + "," + request.CountryName,
                    request.CountryName, request.PurposeOfTravel, 
                    Convert.ToDateTime(request.SettTravellingFromEng == null ? request.TravellingFromEng : request.SettTravellingFromEng).ToShortDateString(),
                    Convert.ToDateTime(request.SettTravellingToEng == null ? request.TravellingToEng : request.SettTravellingToEng).ToShortDateString(), 
                    (request.SettDays == null ? request.Days : request.SettDays) + " Days " + 
                    (request.SettNight == null ? request.Night : request.SettNight) + " Night", request.SettNight,
               request.TravelByText, request.ExpensePaidByText, LocationText, dispTotalApproved, GetCurrency(total), GetCurrency(settlementTotal),
               GetCurrency(request.Advance), GetCurrency(settlementTotal - (request.Advance == null ? 0 : request.Advance)).ToString(),
               GetCurrency(request.RequestedAdvance),(pay == null || pay.BankACNo == null ? "" : pay.BankACNo));

            }



            //adap.Connection.ConnectionString = Config.ConnectionString;
            //  ReportDataSet.GetPAREmployeeHeaderDataTable table = dtHeader.GetData(TimeSheetID);


            // Process for report

            HttpContext.Current.Items["dtTravellAllowancePhaseI"] = dtTravellAllowancePhaseI;
            HttpContext.Current.Items["dtTravellAllowancePhaseII"] = dtTravellAllowancePhaseII;
            HttpContext.Current.Items["dtTravellAllowancePhaseIII"] = dtTravellAllowancePhaseIII;
            HttpContext.Current.Items["RequestId"] = requestId;

            ReportTravellAllowanceSettlement report = new ReportTravellAllowanceSettlement();
            report.DataSource = dtHeader;
            report.DataMember = "Report";

            //            report.labelTitle.Text = "";



            return report;
        }

        public static string GetCurrency(object amount)
        {
            return BaseHelper.GetCurrency(amount, SessionManager.DecimalPlaces);
        }

        public static string GetAllowTextById(int value)
        {
            string rtnvalue = "";
            switch (value)
            {
                case 1:
                    rtnvalue = "Actual";
                    break;
                case 2:
                    rtnvalue = "Approved";
                    break;
                case 3:
                    rtnvalue = "Edit";

                    break;
            }
            return rtnvalue;
        }

        public static void TravelRequestPrint(int requestId, string travellingFromDate, string travellingToDate)
        {
            bool isPDF = true;

            if (CommonManager.SettingTA.IsLocationEditable != null && CommonManager.SettingTA.IsLocationEditable.Value)
            {
                SanaKisanTA report = new SanaKisanTA();

                ReportDataSet.Report_HR_EmpListDataTable dtHeader = new ReportDataSet.Report_HR_EmpListDataTable();
                Report_HR_EmpListTableAdapter mainAdapter = new Report_HR_EmpListTableAdapter();

                dtHeader = mainAdapter.GetData("", -1, -1, -1, -1, "");

                report.DataSource = dtHeader;
                report.DataMember = "Report";
                report.SetSubReport(null, requestId);


                using (MemoryStream stream = new MemoryStream())
                {
                    if (isPDF)
                        report.ExportToPdf(stream);
                    else
                        report.ExportToXls(stream);

                    HttpContext.Current.Response.Clear();
                    if (isPDF)
                    {

                        HttpContext.Current.Response.ContentType = "application/pdf";
                        HttpContext.Current.Response.AppendHeader("Content-Type", "application/pdf");

                        HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=\"Statement.pdf\"");

                    }
                    else
                    {

                        report.ExportToXls(stream);
                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        HttpContext.Current.Response.AppendHeader("Content-Type", "application/vnd.ms-excel");

                        HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=\"Statement.xls\"");
                    }

                    HttpContext.Current.Response.AddHeader("Content-Length", stream.ToArray().Length.ToString());

                    HttpContext.Current.Response.OutputStream.Write(stream.ToArray(), 0, stream.ToArray().Length);
                    HttpContext.Current.Response.End();
                }
            }
            else
            {

                ReportTravellAllowanceSettlement report = GetTravellAllowanceSettlementReport(requestId, travellingFromDate, travellingToDate);

                using (MemoryStream stream = new MemoryStream())
                {
                    if (isPDF)
                        report.ExportToPdf(stream);
                    else
                        report.ExportToXls(stream);

                    HttpContext.Current.Response.Clear();
                    if (isPDF)
                    {

                        HttpContext.Current.Response.ContentType = "application/pdf";
                        HttpContext.Current.Response.AppendHeader("Content-Type", "application/pdf");

                        HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=\"" + report.labelTitle.Text + " Statement.pdf\"");

                    }
                    else
                    {

                        report.ExportToXls(stream);
                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        HttpContext.Current.Response.AppendHeader("Content-Type", "application/vnd.ms-excel");

                        HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=\"" + report.labelTitle.Text + " Statement.xls\"");
                    }

                    HttpContext.Current.Response.AddHeader("Content-Length", stream.ToArray().Length.ToString());

                    HttpContext.Current.Response.OutputStream.Write(stream.ToArray(), 0, stream.ToArray().Length);
                    HttpContext.Current.Response.End();
                }

            }
        }



    }
}
