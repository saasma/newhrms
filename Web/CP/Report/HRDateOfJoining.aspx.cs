﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//using Web.CP.Report.Templates;

using BLL;
using DevExpress.XtraReports.UI;
using Web.CP.Report.Templates.HR;
using Web.ReportDataSetTableAdapters;
using Utils.Calendar;
using BLL.Base;
using BLL.Manager;
using DAL;
using BaseBiz = BLL.BaseBiz;

namespace Web.CP.Report
{
    public partial class HRDateOfJoining : BasePage
    {


        protected void Page_Load(object sender, EventArgs e)
        {
            report.Filter.PayrollFrom = false;
            report.Filter.PayrollTo = false;
            report.Filter.DateToControl = true;
            report.Filter.SubDepartment = true;
            report.Filter.RetiredOnly = true;
            if (CommonManager.CompanySetting.IsHRInOppositeDate)
            {
                report.Filter.ToDate.IsEnglishCalendar = !IsEnglish;
                report.Filter.ToDate.SelectTodayDate();
            }
            report.Filter.DateToControlText.Text = "Wordays Upto";

            //if (!IsPostBack || CommonManager.CompanySetting.RemoveStyleWhileExportingReport)
                LoadReport();
        }

        protected void LoadReport()
        {
            Report_HR_DateOfJoiningTableAdapter
                adap = new Report_HR_DateOfJoiningTableAdapter();


           ReportHRDateOfJoining report1 = new ReportHRDateOfJoining();

            

            //PayrollPeriod payroll =  CommonManager.GetPayrollPeriod(report.Filter.EndDate.Month,report.Filter.EndDate.Year);


            DateTime? date = null;

            //if (payroll != null)
            //    date = payroll.EndDateEng;

            date = report.Filter.DateTo.EnglishDate;


            // Set for DBDefence
            BaseBiz.SetConnectionPwd(adap.Connection);

            report1.DataSource = adap.GetData(report.Filter.EmployeeName, report.Filter.BranchId,report.Filter.DepartmentId,
                                           report.Filter.SubDepartmentId,    SessionManager.CurrentCompanyId,date,report.Filter.DepartmentName
                                           ,report.Filter.RetiredOnlyValue);
            report1.DataMember = "AttendanceReport";


            ////if same payroll then hide group header
            //if (filterPayroll.StartDate == filterPayroll.EndDate)
            //{
            //    report1.GroupHeader.Visible = false;
            //    report1.XRTitle.Text += DateHelper.GetMonthShortName(filterPayroll.StartDate.Month, IsEnglish)
            //                            + "/" + filterPayroll.StartDate.Year;
            //}
            //else
            //{
            //    report1.XRTitle.Text +=
            //        (DateHelper.GetMonthShortName(filterPayroll.StartDate.Month, IsEnglish)
            //         + "/" + filterPayroll.StartDate.Year + " to " +

            //         DateHelper.GetMonthShortName(filterPayroll.EndDate.Month, IsEnglish)
            //         + "/" + filterPayroll.EndDate.Year);
            //}



            report.DisplayReport(report1);

            if (ReportHelper.IsReportExportState())
            {
                this.report.clearCache = true;

            }
        }
    }
}
