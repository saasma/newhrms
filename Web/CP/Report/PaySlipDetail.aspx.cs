﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using BLL;
using DevExpress.XtraReports.UI;
using Web.CP.Report.Templates.HR;
using Web.ReportDataSetTableAdapters;
using Utils.Calendar;
using BLL.Base;
using DAL;
using BLL.Manager;
using System.Data;
using Utils;
using System.IO;
using Web.CP.Report.Templates.Pay.Detail;
using Utils.Helper;

namespace Web.CP.Report
{
    public enum PayslipType
    {
        NormalTotal = 1,
        AddOn = 2,
        AdjustmentWithArrear = 3,
        NewAddOn = 4
    }
    public partial class PaySlipDetail : BasePage
    {

        protected  void LoadReportHandler()
        {
        
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //report.Filter.PayrollFrom = false;
            report.Filter.PayrollFrom = false;
            report.Filter.PayrollPeriod = true;
            report.Filter.PayrollTo = false;
            //report.Filter.Branch = false;
            report.Filter.Department = false;
            report.Filter.MultiSelectionDropDown = true;
            report.Filter.PaySummaryType = true;
            //report.Filter.Employee = false;

            if (IsPostBack)// || CommonManager.CompanySetting.RemoveStyleWhileExportingReport)
                LoadReport();
        }

        protected void LoadReport()
        {
            int[] values = report.Filter.PayrollPeriodValue;
            if (values == null || values.Length < 2)
                return;

            PayrollPeriod payrollPeriod = CommonManager.GetPayrollPeriod(values[0]);
                
            if( payrollPeriod ==null)
                return;

            //int payslipType = 1;
            PayslipType paysliptype = PayslipType.NormalTotal;

            if (values[1] == 1 && report.Filter.PaySummaryTypeValue == 3)
            {
                paysliptype = PayslipType.AdjustmentWithArrear;
                //payslipType = 3;
            }
            else
            {
                paysliptype = (PayslipType)values[1];
                //payslipType = values[1];
            }


            // LoanNIBL pay slip
            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.NIBL)
            {
                if (IsPostBack)
                {
                    if (report.Filter.EmployeeId == 0)
                    {
                        JavascriptHelper.DisplayClientMsg("Employee should be selected.", this.Page);
                        return;
                    }
                }
                if (report.Filter.EmployeeId == 0)
                    return;
                LoadNIBLReport(report.Filter.EmployeeId, payrollPeriod, values);
                return;
            }


            Report_Pay_PaySlipDetailTableAdapter mainAdapter = 
                new Report_Pay_PaySlipDetailTableAdapter();
            

            Report.Templates.Pay.Detail.ReportPaySlip mainReport = new Report.Templates.Pay.Detail.ReportPaySlip();
         //   Report.Templates.Pay.Detail.ReportPaySlipD2 mainReportD2 = new Templates.Pay.Detail.ReportPaySlipD2();
            Report.Templates.Pay.Detail.ReportPaySlipWithTaxDetails mainReportWithTax = new Templates.Pay.Detail.ReportPaySlipWithTaxDetails();
            Report.Templates.Pay.Detail.ReportPaySlipAdjustment mainReportAdjustment = new Templates.Pay.Detail.ReportPaySlipAdjustment();


            bool readingSumForMiddleFiscalYearStartedReqd = false;
            int startingPayrollPeriodId = 0, endingPayrollPeriodId = 0;
            PayrollPeriod startingMonthParollPeriodInFiscalYear =
               CalculationManager.GenerateForPastIncome(
                    payrollPeriod.PayrollPeriodId, ref readingSumForMiddleFiscalYearStartedReqd,
                    ref startingPayrollPeriodId, ref endingPayrollPeriodId);
            //FOr D2 this report can be viewd one emp at a time only
            int? employeeId = null;
            employeeId = report.Filter.EmployeeId;
            if (employeeId == 0)
                employeeId = null;

            if (paysliptype == PayslipType.AdjustmentWithArrear)
            {
                mainReportAdjustment.labelMonth.Text = string.Format("{0} ({1} {2})", payrollPeriod.Name,
                                                       DateHelper.GetMonthsForOtherPayrollPeriod(payrollPeriod.Month,
                                                                                                 IsEnglish)
                                                                                                 , DateManager.GetOtherYearForPayrollPeriod(payrollPeriod, IsEnglish));
                mainReportAdjustment.labelCompanyName.Text = new CompanyManager().GetById(SessionManager.CurrentCompanyId).Name;
                //string logoLoc = ReportManager.PaySlipLogoAbsolutePath;
                //if (File.Exists(logoLoc))
                //{
                //    mainReportD2.logo.Image = new System.Drawing.Bitmap(logoLoc);
                //    mainReportD2.logo.Visible = true;
                //}
            }
            else if (CommonManager.CompanySetting.AppendTaxDetailsInPayslip)
            {
                mainReportWithTax.labelMonth.Text = string.Format("{0} ({1} {2})", payrollPeriod.Name,
                                                           DateHelper.GetMonthsForOtherPayrollPeriod(payrollPeriod.Month,
                                                                                                     IsEnglish)
                                                                                                     , DateManager.GetOtherYearForPayrollPeriod(payrollPeriod, IsEnglish));
                mainReportWithTax.labelCompanyName.Text = new CompanyManager().GetById(SessionManager.CurrentCompanyId).Name;
              
            }
            else// if (CommonManager.CompanySetting.IsD2 == false)
            {
                mainReport.labelMonth.Text = string.Format("{0} ({1} {2})", payrollPeriod.Name,
                                                           DateHelper.GetMonthsForOtherPayrollPeriod(payrollPeriod.Month,
                                                                                                     IsEnglish)
                                                                                                     , DateManager.GetOtherYearForPayrollPeriod(payrollPeriod, IsEnglish));
                mainReport.labelCompanyName.Text = new CompanyManager().GetById(SessionManager.CurrentCompanyId).Name;
                string logoLoc = ReportManager.PaySlipLogoAbsolutePath;
                if (File.Exists(logoLoc))
                {
                    mainReport.logo.Image = new System.Drawing.Bitmap(logoLoc);
                    mainReport.logo.Visible = true;
                }
            }
            
           
            

            BLL.BaseBiz.SetConnectionPwd(mainAdapter.Connection);
            ReportDataSet.Report_Pay_PaySlipDetailDataTable mainTable =
                mainAdapter.GetData(payrollPeriod.Month, payrollPeriod.Year, SessionManager.CurrentCompanyId,0,report.Filter.EmployeeName.Trim()
                , report.Filter.MultiSelectionDropDownValues, values[1], values[2], report.Filter.BranchId);



            #region "Income/Deduction/Atte List"

            // Code to retrieve all emp once & save the DataTable in the request Cache so that multiple db request can be prevented
            // due to large no of employees
            Report_Pay_PaySlipDetail_EmpHeaderTableAdapter incomeHeaderAdapter = new Report_Pay_PaySlipDetail_EmpHeaderTableAdapter();
            BLL.BaseBiz.SetConnectionPwd(incomeHeaderAdapter.Connection);

            Report_Pay_PaySlip_AttendanceTableAdapter attendacDetailAdapter = new Report_Pay_PaySlip_AttendanceTableAdapter();
            BLL.BaseBiz.SetConnectionPwd(attendacDetailAdapter.Connection);

           

            ReportDataSet.Report_Pay_PaySlipDetail_EmpHeaderDataTable incomesTable = incomeHeaderAdapter.GetData(payrollPeriod.Month, payrollPeriod.Year,
                SessionManager.CurrentCompanyId, employeeId, true, null, startingPayrollPeriodId, endingPayrollPeriodId, (int)paysliptype, values[2]);
            ReportDataSet.Report_Pay_PaySlipDetail_EmpHeaderDataTable deductionTable = incomeHeaderAdapter.GetData(payrollPeriod.Month, payrollPeriod.Year,
                SessionManager.CurrentCompanyId, employeeId, false, null, startingPayrollPeriodId, endingPayrollPeriodId, (int)paysliptype, values[2]);

            ReportDataSet.Report_Pay_PaySlip_AttendanceDataTable attendanceTable =
                    attendacDetailAdapter.GetData(payrollPeriod.Month, payrollPeriod.Year, SessionManager.CurrentCompanyId, employeeId, null);

            try
            {
                //to prevent db timeout due to 100 connection full
                incomeHeaderAdapter.Connection.Close();
                attendacDetailAdapter.Connection.Close();
            }

            catch (Exception exp)
            {
                Log.log("Error", exp);
            }
            HttpContext.Current.Items["PaySlipIncomeList"] = incomesTable;
            HttpContext.Current.Items["PaySlipDeductionList"] = deductionTable;
            HttpContext.Current.Items["PaySlipAttendanceList"] = attendanceTable;
            #endregion

            if (paysliptype == PayslipType.AdjustmentWithArrear)
            {
                mainReportAdjustment.Month = payrollPeriod.Month;
                mainReportAdjustment.Year = payrollPeriod.Year.Value;
                mainReportAdjustment.IsIncome = true;
                mainReportAdjustment.CompanyId = SessionManager.CurrentCompanyId;
                mainReportAdjustment.DataSource = mainTable;
                mainReportAdjustment.DataMember = "Report";
                report.DisplayReport(mainReportAdjustment);
                if (ReportHelper.IsReportExportState())
                {
                    this.report.clearCache = true;
                }
            }
            else if (CommonManager.CompanySetting.AppendTaxDetailsInPayslip)
            {
                mainReportWithTax.Month = payrollPeriod.Month;
                mainReportWithTax.Year = payrollPeriod.Year.Value;
                mainReportWithTax.IsIncome = true;
                mainReportWithTax.CompanyId = SessionManager.CurrentCompanyId;
                mainReportWithTax.DataSource = mainTable;
                mainReportWithTax.DataMember = "Report";
                report.DisplayReport(mainReportWithTax);
                if (ReportHelper.IsReportExportState())
                {
                    this.report.clearCache = true;
                }
            }
            else// if (CommonManager.CompanySetting.IsD2 == false)
            {
                mainReport.Month = payrollPeriod.Month;
                mainReport.Year = payrollPeriod.Year.Value;
                mainReport.IsIncome = true;
                mainReport.CompanyId = SessionManager.CurrentCompanyId;
                mainReport.DataSource = mainTable;
                mainReport.DataMember = "Report";
                report.DisplayReport(mainReport);
                if (ReportHelper.IsReportExportState())
                {
                    this.report.clearCache = true;
                }
            }
           
            //else
            //{
            //    mainReportD2.Month = payrollPeriod.Month;
            //    mainReportD2.Year = payrollPeriod.Year.Value;
            //    mainReportD2.IsIncome = true;
            //    mainReportD2.CompanyId = SessionManager.CurrentCompanyId;
            //    mainReportD2.DataSource = mainTable;
            //    mainReportD2.DataMember = "Report";
            //    report.DisplayReport(mainReportD2);
            //    if (ReportHelper.IsReportExportState())
            //    {
            //        this.report.clearCache = true;
            //    }
            //}
            
        }


        protected void LoadNIBLReport(int ein, PayrollPeriod payrollPeriod,int[] values)
        {

            {


                Report_Pay_PaySlipNIBLTableAdapter mainAdapter =
                new Report_Pay_PaySlipNIBLTableAdapter();

                //bool readingSumForMiddleFiscalYearStartedReqd = false;
                //int startingPayrollPeriodId = 0, endingPayrollPeriodId = 0;
                //PayrollPeriod startingMonthParollPeriodInFiscalYear =
                //   CalculationManager.GenerateForPastIncome(
                //        payrollPeriod.PayrollPeriodId, ref readingSumForMiddleFiscalYearStartedReqd,
                //        ref startingPayrollPeriodId, ref endingPayrollPeriodId);


                ReportPaySlipNIBL mainReport = new ReportPaySlipNIBL();



                mainReport.labelMonth.Text = string.Format("{0} ({1} {2})", payrollPeriod.Name,
                                                            DateHelper.GetMonthsForOtherPayrollPeriod(payrollPeriod.Month,
                                                                                                      IsEnglish)
                                                                                                      , DateManager.GetOtherYearForPayrollPeriod(payrollPeriod, IsEnglish));

                mainReport.labelCompanyName.Text = new CompanyManager().GetById(SessionManager.CurrentCompanyId).Name;
                string logoLoc = ReportManager.PaySlipLogoAbsolutePath;
                if (File.Exists(logoLoc))
                {
                    mainReport.logo.Image = new System.Drawing.Bitmap(logoLoc);
                    mainReport.logo.Visible = true;
                }


                BLL.BaseBiz.SetConnectionPwd(mainAdapter.Connection);
                ReportDataSet.Report_Pay_PaySlipNIBLDataTable mainTable =
                    mainAdapter.GetData(payrollPeriod.Month, payrollPeriod.Year, ein, values[1], values[2]);



                #region "Income/Deduction/Atte List"



                Report_Pay_PaySlip_AttendanceTableAdapter attendacDetailAdapter = new Report_Pay_PaySlip_AttendanceTableAdapter();
                BLL.BaseBiz.SetConnectionPwd(attendacDetailAdapter.Connection);


                ReportDataSet.Report_Pay_PaySlip_AttendanceDataTable attendanceTable =
                        attendacDetailAdapter.GetData(payrollPeriod.Month, payrollPeriod.Year, SessionManager.CurrentCompanyId, ein, ein.ToString());

                try
                {
                    //to prevent db timeout due to 100 connection full
                    mainAdapter.Connection.Close();
                    attendacDetailAdapter.Connection.Close();
                }
                catch (Exception exp)
                {
                    Log.log("Error", exp);
                }
                HttpContext.Current.Items["PaySlipIncomeList"] = mainTable;
                HttpContext.Current.Items["PaySlipAttendanceList"] = attendanceTable;

                mainReport.Month = payrollPeriod.Month;
                mainReport.Year = payrollPeriod.Year.Value;
                mainReport.IsIncome = true;
                mainReport.CompanyId = SessionManager.CurrentCompanyId;

                mainReport.lblName.Text = EmployeeManager.GetEmployeeName(ein);
                mainReport.lblEIN.Text = ein.ToString();

                DAL.GetEmployeeCurrentBranchDepartmentResult dep = BLL.BaseBiz.PayrollDataContext
                    .GetEmployeeCurrentBranchDepartment(payrollPeriod.EndDateEng.Value.Date, ein)
                    .FirstOrDefault();

                mainReport.lblDep.Text = dep.Department;

                int? desigId = BLL.BaseBiz.PayrollDataContext.GetEmployeeCurrentPositionForDate(payrollPeriod.EndDateEng.Value.Date,
                   ein);
                mainReport.lblDesig.Text = CommonManager.GetDesigId(desigId.Value).Name;

                mainReport.DataSource = attendanceTable;
                mainReport.DataMember = "Report";


                report.DisplayReport(mainReport);
            }

                #endregion



        }
    }
}
