﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using BLL;
using DevExpress.XtraReports.UI;
using Web.CP.Report.Templates.HR;
using Web.ReportDataSetTableAdapters;
using Utils.Calendar;
using BLL.Base;
using DAL;
using BLL.Manager;
using System.Data;
using DevExpress.XtraPrinting;
using Web.CP.Report.Templates.Pay.Detail;

namespace Web.CP.Report
{
    public partial class PayVariance : BasePage
    {
        private int Name_Column_Index = 1;
        private int count = 0;
        protected  void LoadReportHandler()
        {}

        //public bool WithTax
        //{
        //    get
        //    {
        //        if(string.IsNullOrEmpty(Request.QueryString["withtax"]))
        //        {
        //            return false;
        //        }
        //        return true;
        //    }
        //}

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["withtax"]))
            {

                header.InnerHtml  = "Income and Tax Calculation Summary";

            }
            if (!string.IsNullOrEmpty(Request.QueryString["taxonly"]))
            {
                header.InnerHtml = "Tax Payment Details";

               
            }


            //report.Filter.PayrollFrom = false;
            ++count;

            report.Filter.Employee = false;
            report.Filter.RetiredEmployee = true;
            report.Filter.SubDepartment = true;
            report.Filter.Branch = false;
            report.Filter.Department = false;

            if (!string.IsNullOrEmpty(Request.QueryString["withtax"]) || !string.IsNullOrEmpty(Request.QueryString["taxonly"]))
            {
                report.Filter.PayrollTo = false;
            }

            report.Filter.IncludeRetiredEmployeeInSearch = true;
            if (IsPostBack)
            {
                //prevent to  load the report for all employees by checking the selected filte ronly
                if (report.Filter.RetiredEmployeeId != 0 || report.Filter.EmployeeName.Length != 0 || report.Filter.BranchId != -1
                    || report.Filter.DepartmentId != -1 || report.Filter.SubDepartmentId != -1)
                    LoadReport();
            }
        }

        private void Event_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (Web.CP.Report.ReportHelper.IsReportExportState())
            {
                ((XRLabel)sender).BackColor = System.Drawing.Color.Transparent;
                ((XRLabel)sender).BorderColor = System.Drawing.Color.Black;
            }

        }


        public void SetDefault()
        {
            if (!IsPostBack)
            {
                //load first payroll of the financial year into From Date
                List<PayrollPeriod> list = CommonManager.GetCurrentYearPayrollList(SessionManager.CurrentCompanyId);
                if (list.Count > 0)
                {
                    report.Filter.SetFromPayrollPeriod(list[0]);
                }
            }
           
        }
       
        protected void LoadReport()
        {
            if (!IsPostBack)
                return;

            SetDefault();


            PayrollPeriod payrollPeriodFrom = CommonManager.GetPayrollPeriod(report.Filter.StartDate.Month,
                                                                         report.Filter.StartDate.Year);


            PayrollPeriod payrollPeriodTo = CommonManager.GetPayrollPeriod(report.Filter.EndDate.Month,
                                                                                     report.Filter.EndDate.Year);


            if (!string.IsNullOrEmpty(Request.QueryString["withtax"]) || !string.IsNullOrEmpty(Request.QueryString["taxonly"]))
            {
                if (payrollPeriodFrom == null)
                    return;


                payrollPeriodTo = payrollPeriodFrom;



                bool readingSumForMiddleFiscalYearStartedReqd = false;
                //contains the id of starting-ending payroll period to be read for history of regular income
                int startingPayrollPeriodId = 0, endingPayrollPeriodId = 0;
                // int notWorkedMonthInFiscalYear = 0, passedMonthWorked = 0;


                CalculationManager.GenerateForPastIncome(
                         payrollPeriodTo.PayrollPeriodId, ref readingSumForMiddleFiscalYearStartedReqd,
                         ref startingPayrollPeriodId, ref endingPayrollPeriodId);

                payrollPeriodFrom = CommonManager.GetPayrollPeriod(startingPayrollPeriodId);

                if (payrollPeriodFrom == null)
                    return;
                //int empEndingPayrollId = endingPayrollPeriodId;

            }
            else
            {

                //if null get first payroll period
                if (payrollPeriodFrom == null)
                    payrollPeriodFrom = CommonManager.GetFirstPayrollPeriod(SessionManager.CurrentCompanyId);

                if (payrollPeriodTo == null)
                    payrollPeriodTo = CommonManager.GetLastPayrollPeriod();

                //again null then no report
                if (payrollPeriodFrom == null || payrollPeriodTo == null)
                    return;

                //for report to date should be greater or equal to from date
                if (payrollPeriodTo.PayrollPeriodId < payrollPeriodFrom.PayrollPeriodId)
                    return;
            }




            ReportEmpVarianceList mainReport = new ReportEmpVarianceList();
            Report_Pay_EmployeeVarianceTableAdapter mainAdapter = new Report_Pay_EmployeeVarianceTableAdapter();

            mainReport.lblName.Text = "Name : " + EmployeeManager.GetEmployeeName(report.Filter.RetiredEmployeeId);
            mainReport.lblEIN.Text = "EIN : " + report.Filter.RetiredEmployeeId.ToString();

            if (!string.IsNullOrEmpty(Request.QueryString["withtax"]))
            {

                header.InnerHtml = mainReport.labelTitle.Text = "Income and Tax Calculation Summary";
                
            }
            if (!string.IsNullOrEmpty(Request.QueryString["taxonly"]))
            {
                mainReport.lblCompanyName.Text = SessionManager.CurrentCompany.Name;
                header.InnerHtml = mainReport.labelTitle.Text = "Tax Payment Details";

                mainReport.lblName.Text = EmployeeManager.GetEmployeeName(report.Filter.RetiredEmployeeId);
                mainReport.lblEIN.Text = "PAN NO : " + EmployeeManager.GetEmployeeById(report.Filter.RetiredEmployeeId)
                    .EFundDetails[0].PANNo;
            }
            //Report_Pay_PaySlipDetail_EmpHeaderTableAdapter headerAdapter = new Report_Pay_PaySlipDetail_EmpHeaderTableAdapter();


            mainReport.EmployeeId = report.Filter.RetiredEmployeeId;

          

            BLL.BaseBiz.SetConnectionPwd(mainAdapter.Connection);
            ReportDataSet.Report_Pay_EmployeeVarianceDataTable mainTable =
                mainAdapter.GetData(payrollPeriodFrom.PayrollPeriodId, payrollPeriodTo.PayrollPeriodId,
                SessionManager.CurrentCompanyId, report.Filter.BranchId, report.Filter.DepartmentId,report.Filter.SubDepartmentId ,
                report.Filter.RetiredEmployeeId);

           
            // Save Variance List
            List<DAL.Report_Pay_EmployeeVarianceDetailsResult> data =
            ReportManager.GetEmployeeVarianceReport(payrollPeriodFrom.PayrollPeriodId, payrollPeriodTo.PayrollPeriodId, report.Filter.RetiredEmployeeId);
            HttpContext.Current.Items["VarianceList"] = data;

            List<CalcGetHeaderListResult> headerList = ReportManager.GetHeaderList(payrollPeriodFrom.PayrollPeriodId, payrollPeriodTo.PayrollPeriodId);

            // for SST and TDS only
            if (!string.IsNullOrEmpty(Request.QueryString["taxonly"]))
            {
                List<CalcGetHeaderListResult> newSSTAndTDSOnlyList = new List<CalcGetHeaderListResult>();
                newSSTAndTDSOnlyList.Add(new CalcGetHeaderListResult { Type = (int)CalculationColumnType.SST,SourceId = (int)CalculationColumnType.SST,HeaderName="SST" });
                newSSTAndTDSOnlyList.Add(new CalcGetHeaderListResult { Type = (int)CalculationColumnType.TDS, SourceId = (int)CalculationColumnType.TDS, HeaderName = "TDS" });

                headerList = newSSTAndTDSOnlyList;
            }
            
            headerList = CalculationValue.SortHeaders(headerList, PayManager.GetIncomeOrderValues(), PayManager.GetDeductionOrderValues());
            HttpContext.Current.Items["VarianceHeaderList"] = headerList;


            

            mainReport.PayrollStartId = payrollPeriodFrom.PayrollPeriodId;
            mainReport.PayrollEndId = payrollPeriodTo.PayrollPeriodId;
            //mainReport.Month = payrollPeriod.Month;
            //mainReport.Year = payrollPeriod.Year.Value;
            //mainReport.IsIncome = true;
            mainReport.CompanyId = SessionManager.CurrentCompanyId;





            mainReport.DataSource = mainTable;
            mainReport.DataMember = "Report";



            report.DisplayReport(mainReport);

            if(ReportHelper.IsReportExportState())
            {
                this.report.clearCache = true;
            }



        }

       
    }
}
