<%@ Page Title="Pay Component Report" Language="C#" MaintainScrollPositionOnPostback="true"
    MasterPageFile="~/Master/HROld.Master" AutoEventWireup="true" CodeBehind="PayComponentDetails.aspx.cs"
    Inherits="Web.CP.Report.PayComponentDetails" %>

<%@ Register Assembly="DevExpress.XtraReports.v16.1.Web, Version=16.1.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraReports.Web" TagPrefix="dxxr" %>
<%@ Register Src="~/Controls/Report/ReportContainer.ascx" TagName="ReportContainer"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Pay Component Report
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <uc1:ReportContainer runat="server" OnReloadReport="LoadReport" id="report" />
    </div>
    <%--<div class="contentArea">
     <uc1:ReportFilterBranchDep  OnReloadReport="LoadReport"  ID="filterPayroll" runat="server" />
     <div class="separator clear">
  
            <dxxr:ReportToolbar ID="ReportToolbar1"  runat="server"
                ShowDefaultButtons="False" Width="648px">
                <Items>
                    <dxxr:ReportToolbarButton ItemKind="Search" />
                    <dxxr:ReportToolbarSeparator />
                    <dxxr:ReportToolbarButton ItemKind="PrintReport" />
                    <dxxr:ReportToolbarButton ItemKind="PrintPage" />
                    <dxxr:ReportToolbarSeparator />
                    <dxxr:ReportToolbarButton Enabled="False" ItemKind="FirstPage" />
                    <dxxr:ReportToolbarButton Enabled="False" ItemKind="PreviousPage" />
                    <dxxr:ReportToolbarLabel ItemKind="PageLabel" />
                    <dxxr:ReportToolbarComboBox ItemKind="PageNumber" Width="65px">
                    </dxxr:ReportToolbarComboBox>
                    <dxxr:ReportToolbarLabel ItemKind="OfLabel" />
                    <dxxr:ReportToolbarTextBox IsReadOnly="True" ItemKind="PageCount" />
                    <dxxr:ReportToolbarButton ItemKind="NextPage" />
                    <dxxr:ReportToolbarButton ItemKind="LastPage" />
                    <dxxr:ReportToolbarSeparator />
                    <dxxr:ReportToolbarButton ItemKind="SaveToDisk" />
                    <dxxr:ReportToolbarButton ItemKind="SaveToWindow" />
                    <dxxr:ReportToolbarComboBox ItemKind="SaveFormat" Width="70px">
                        <Elements>
                            <dxxr:ListElement Value="pdf" />
                            <dxxr:ListElement Value="xls" />
                            <dxxr:ListElement Value="xlsx" />
                            <dxxr:ListElement Value="rtf" />
                            <dxxr:ListElement Value="mht" />
                            <dxxr:ListElement Value="html" />
                            <dxxr:ListElement Value="txt" />
                            <dxxr:ListElement Value="csv" />
                            <dxxr:ListElement Value="png" />
                        </Elements>
                    </dxxr:ReportToolbarComboBox>
                </Items>
    </dxxr:ReportToolbar>
           
   
 <dxxr:ReportViewer ClientInstanceName="ReportViewer1"  ID="rptViewer" runat="server" AutoSize="False" 
        CssFilePath="~/App_Themes/Aqua/{0}/styles.css" CssPostfix="Aqua" Height="900px" 
        LoadingPanelText="" SpriteCssFilePath="~/App_Themes/Aqua/{0}/sprite.css" Width="808px" 
          
        >
     <LoadingPanelImage Url="~/App_Themes/Aqua/Editors/Loading.gif">
     </LoadingPanelImage>
     <LoadingPanelStyle ForeColor="#303030">
     </LoadingPanelStyle>
    </dxxr:ReportViewer>
</div>
 </div>--%>
</asp:Content>
