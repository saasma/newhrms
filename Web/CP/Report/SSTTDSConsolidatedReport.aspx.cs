﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using BLL;
using DevExpress.XtraReports.UI;
using Web.CP.Report.Templates.HR;
using Web.ReportDataSetTableAdapters;
using Utils.Calendar;
using BLL.Base;
using DAL;
using BLL.Manager;
using System.Data;

namespace Web.CP.Report
{
    public partial class SSTTDSConsolidatedReport : BasePage
    {
        protected  void LoadReportHandler()
        {        
        }
      
        protected void Page_Load(object sender, EventArgs e)
        {
            //report.Filter.PayrollFrom = false;
            
            report.Filter.Department = false;
            report.Filter.Employee = false;
            report.Filter.CostCode = false;
            report.Filter.Program = false;
            report.Filter.AddOn = false;
            report.Filter.SSTTDSType = true;
            report.Filter.Unit = false;
            report.Filter.Employee = true;
            // if (!IsPostBack)
            LoadReport();
        }

        protected void LoadReport()
        {
            PayrollPeriod payrollPeriod = CommonManager.GetPayrollPeriod(report.Filter.StartDate.Month,
                                                                         report.Filter.StartDate.Year);
            if( payrollPeriod ==null)
                return;


            PayrollPeriod payrollPeriod2 = CommonManager.GetPayrollPeriod(report.Filter.EndDate.Month,
                                                                        report.Filter.EndDate.Year);
            if (payrollPeriod2 == null)
                return;


          


            if (report.Filter.SSTTDSTypeValue)
                mainTitle.InnerHtml = "TDS Consolidated Report Details from " + payrollPeriod.Name + " to " + payrollPeriod2.Name;
            else
                mainTitle.InnerHtml = "SST Consolidated Report Details " + payrollPeriod.Name + " to " + payrollPeriod2.Name;

            Report_Pay_TDSTableAdapter mainAdapter = new Report_Pay_TDSTableAdapter();

            Report.Templates.Pay.ReportPayTDSDetails mainReport = new Report.Templates.Pay.ReportPayTDSDetails();
            BLL.BaseBiz.SetConnectionPwd(mainAdapter.Connection);
            ReportDataSet.Report_Pay_TDSDataTable mainTable =
                mainAdapter.GetData(payrollPeriod.Month, payrollPeriod.Year, SessionManager.CurrentCompanyId, report.Filter.BranchId, report.Filter.DepartmentId,
                !report.Filter.SSTTDSTypeValue
                ,report.Filter.CostCodeId,report.Filter.ProgramId,report.Filter.AddOnId, report.Filter.UnitId, payrollPeriod.PayrollPeriodId, payrollPeriod2.PayrollPeriodId, null,report.Filter.EmployeeId);

            //mainReport.labelTitle.Text += payrollPeriod.Name;       
           
            mainReport.DataSource = mainTable;
            mainReport.DataMember = "Report";
           
            report.DisplayReport(mainReport);
        }
    }
}
