﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using BLL;
using DevExpress.XtraReports.UI;
using Web.CP.Report.Templates.HR;
using Web.ReportDataSetTableAdapters;
using Utils.Calendar;
using BLL.Base;
using DAL;
using BLL.Manager;
using System.Data;
using Web.Helper;

namespace Web.CP.Report
{
    public partial class OxfamProjectReport : BasePage
    {
        protected  void LoadReportHandler()
        {        
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //report.Filter.PayrollFrom = false;
            report.Filter.PayrollTo = false;
            report.Filter.Branch = false;
            report.Filter.Department = false;
            report.Filter.Employee = false;
            report.Filter.RetiredEmployee = false;
            report.Filter.CostCode = false;
            report.Filter.Program = false;
            
            report.Filter.IncludeRetiredEmployeeInSearch = false;
            
            // if (!IsPostBack || CommonManager.CompanySetting.RemoveStyleWhileExportingReport)

            report.ReportViewer.Height = new Unit(2200);

            LoadReport();

        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                report.Filter.SelectLastPayrollAndDisableDropDown();

                if (!IsPostBack && !string.IsNullOrEmpty(Request.QueryString["ID"]))
                {
                    int empId = int.Parse(Request.QueryString["ID"]);
                    string name = EmployeeManager.GetEmployeeName(empId);
                    report.Filter.SetSelectedRetEmployee(empId, name);
                    LoadReport();
                }

            }
        }

    

   
        protected void LoadReport()
        {
            PayrollPeriod payrollPeriod = CommonManager.GetPayrollPeriod(report.Filter.StartDate.Month,report.Filter.StartDate.Year);
            if (payrollPeriod == null)
                return;

            if (string.IsNullOrEmpty(cmbReportType.SelectedItem.Value))
                return;
            else
            {
                int ReportType = int.Parse(cmbReportType.SelectedItem.Value);
                if (ReportType == 1)
                    BindProjectSalaryReport(payrollPeriod);
                if (ReportType == 2)
                    BindTaxVoucherReport(payrollPeriod);
            }
            
        
        }

        private void BindTaxVoucherReport(PayrollPeriod payrollPeriod)
        {
            Report.Templates.Pay.OxfamProjectTaxVoucher mainReport = new Report.Templates.Pay.OxfamProjectTaxVoucher();
            Oxfam_GetTaxVoucherTableAdapter mainAdapter = new Oxfam_GetTaxVoucherTableAdapter();
            BLL.BaseBiz.SetConnectionPwd(mainAdapter.Connection);

            ReportDataSet.Oxfam_GetTaxVoucherDataTable mainTable =
            mainAdapter.GetData(payrollPeriod.PayrollPeriodId);
            mainReport.DataSource = mainTable;
            decimal Total = mainTable.ToList().Sum(x => x.Amount);
            mainReport.lblAmountInWords.Text = AmountToWords(Total);

            mainReport.lblDate.Text = payrollPeriod.EndDateEng.Value.ToString("dd-MMM-yy");

            mainReport.DataMember = "Report";
            report.DisplayReport(mainReport);

            if (ReportHelper.IsReportExportState())
            {
                this.report.clearCache = true;
            }
        }


        private void BindProjectSalaryReport(PayrollPeriod payrollPeriod)
        {
            Report.Templates.Pay.OxfamProjectSalaryReport mainReport = new Report.Templates.Pay.OxfamProjectSalaryReport();
            Oxfam_GetProjectReportTableAdapter mainAdapter = new Oxfam_GetProjectReportTableAdapter();
            BLL.BaseBiz.SetConnectionPwd(mainAdapter.Connection);

            ReportDataSet.Oxfam_GetProjectReportDataTable mainTable =
            mainAdapter.GetData(payrollPeriod.PayrollPeriodId);
            mainReport.DataSource = mainTable;
            decimal Total = mainTable.ToList().Sum(x => x.Amount);
            mainReport.lblAmountInWords.Text = AmountToWords(Total);

            DateTime date = new DateTime(payrollPeriod.EndDateEng.Value.Year, payrollPeriod.EndDateEng.Value.Month,
                25);

            mainReport.lblDate.Text = date.ToString("dd-MMM-yy");

            mainReport.DataMember = "Report";
            report.DisplayReport(mainReport);

            if (ReportHelper.IsReportExportState())
            {
                this.report.clearCache = true;
            }
        }

        private string AmountToWords(decimal total)
        {
         
            int dec = 0;
            if (total.ToString().IndexOf(".") > 0)
            {
                dec = int.Parse(total.ToString().Substring(total.ToString().IndexOf(".") + 1));
            }

            int amount = (int)total;

            string str = "( " + WebHelper.IntegerToWritten(amount);

            //if(dec >0)
            //    str += " and " + WebHelper.IntegerToWritten(dec) + " paisa only )";
            //else
            str += " only )";

           return str;
        }
    }
}
