﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using BLL;
using DevExpress.XtraReports.UI;
using Web.CP.Report.Templates.HR;
using Web.ReportDataSetTableAdapters;
using Utils.Calendar;
using BLL.Base;
using DAL;
using BLL.Manager;
using System.Data;

namespace Web.CP.Report
{
    public partial class VoucherReport : BasePage
    {
        protected  void LoadReportHandler()
        {        
        }
      
        protected void Page_Load(object sender, EventArgs e)
        {
            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.PSI)
            {
                //report.Filter.PayrollFrom = false;
                report.Filter.PayrollTo = false;
                report.Filter.Branch = false;
                report.Filter.Department = false;
                report.Filter.Employee = true;
                report.Filter.CostCode = true;
                report.Filter.Program = false;
                report.Filter.RetiredOnly = true;
            }
            else
            {
                report.Filter.PayrollTo = false;
                report.Filter.Branch = false;
                report.Filter.Department = true;
                report.Filter.Employee = true;
                report.Filter.CostCode = false;
                report.Filter.Program = false;
                report.Filter.RetiredOnly = true;
            }


            //if (IsPostBack)
            //{
            //    report.Filter.DDLCostCode.SelectedIndex = 1;
            //}
            
            // if (!IsPostBack || CommonManager.CompanySetting.RemoveStyleWhileExportingReport)

            if (IsPostBack)
                LoadReport();


            DispWarning();
        }

        public void DispWarning()
        {
            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.PSI)
            {
                warningEmpList.InnerHtml = EmployeeManager.GetWarningForPSIVoucher();
            }
        }

        protected void LoadReport()
        {
            if (!IsPostBack)
                return;

            PayrollPeriod payrollPeriod = CommonManager.GetPayrollPeriod(report.Filter.StartDate.Month,
                                                                         report.Filter.StartDate.Year);
            if( payrollPeriod ==null)
                return;

            
            Report.Templates.Pay.ReportVoucher mainReport = new Report.Templates.Pay.ReportVoucher();
            mainReport.labelTitle.Text += " " + payrollPeriod.Name;


            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.PSI)
            {
                GetAllVoucherDetailsTableAdapter mainAdapter = new GetAllVoucherDetailsTableAdapter();


                BLL.BaseBiz.SetConnectionPwd(mainAdapter.Connection);
                ReportDataSet.GetAllVoucherDetailsDataTable mainTable =
                    mainAdapter.GetData(payrollPeriod.PayrollPeriodId, report.Filter.CostCodeId, SessionManager.CurrentCompanyId
                    , report.Filter.RetiredOnlyValue, report.Filter.EmployeeId);

                mainReport.DataSource = mainTable;
            }
            else
            {
                GetAllHeiferVoucherDetailsTableAdapter mainAdapter = new GetAllHeiferVoucherDetailsTableAdapter();


                BLL.BaseBiz.SetConnectionPwd(mainAdapter.Connection);
                ReportDataSet.GetAllHeiferVoucherDetailsDataTable mainTable =
                    mainAdapter.GetData(payrollPeriod.PayrollPeriodId, report.Filter.CostCodeId, SessionManager.CurrentCompanyId
                    , report.Filter.RetiredOnlyValue, report.Filter.EmployeeId,report.Filter.DepartmentId);

                mainReport.DataSource = mainTable;

            }

            mainReport.DataMember = "Report";
           
            report.DisplayReport(mainReport);

            if(ReportHelper.IsReportExportState())
            {
                this.report.clearCache = true;
            }
        }
    }
}
