<%@ Page Title="Settlement Report" Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/Master/HROld.Master" AutoEventWireup="true" CodeBehind="PaySettlement.aspx.cs" Inherits="Web.CP.Report.PaySettlement" %>
<%@ Register assembly="DevExpress.XtraReports.v16.1.Web, Version=16.1.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraReports.Web" tagprefix="dxxr" %>
<%@ Register src="~/Controls/Report/ReportFilterBranchDep.ascx" tagname="ReportFilterBranchDep" tagprefix="uc1" %>
<%@ Register src="~/Controls/Report/ReportContainer.ascx" tagname="ReportContainer" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Pay Settlement Report
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel" style="margin-top:7px !important; padding-top:7px !important;">
    <uc1:ReportContainer runat="server"  OnReloadReport="LoadReport"  id="report" />
    </div>
</asp:Content>
