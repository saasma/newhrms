<%@ Page Title="Add On Report" Language="C#" MaintainScrollPositionOnPostback="false"
    MasterPageFile="~/Master/HROld.Master" AutoEventWireup="true" CodeBehind="AddOnReport.aspx.cs"
    Inherits="Web.CP.Report.AddOnReport" %>
    <%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Assembly="DevExpress.XtraReports.v16.1.Web, Version=16.1.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraReports.Web" TagPrefix="dxxr" %>
<%@ Register Src="~/Controls/Report/ReportFilterBranchDep.ascx" TagName="ReportFilterBranchDep"
    TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Report/ReportContainer.ascx" TagName="ReportContainer"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .DXRResetStyle td
        {
            /*width:120px!important;*/
        }
        .selection
        {
           
            background-color:lightgray!important;
        }
         .dxeCaptionCell_Aqua.dxeCaptionVATSys.dxeTextEditCTypeSys{padding-top:8px;}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Add On Report
                </h4>
            </div>
        </div>
    </div>
    <div class="contentArea">
        <div class="attribute" style="margin-top: 10px;margin-left:20px;margin-right:20px;">
            <asp:HiddenField ID="hiddenEmployeeID" runat="server" />
            <table cellpadding="3" cellspacing="0" class="fieldTable">
                <tr>
                    <td class="filterHeader" runat="server" id="rowEmp1">
                        <strong>Year</strong>
                    </td>
                    <td class="filterHeader" runat="server" id="Td1">
                        <strong>Employee</strong>
                    </td>
                    <td rowspan="2" valign="bottom">
                        <asp:Button ID="btnLoad" Style='float: left' CssClass="load" runat="server" Text="Load"
                            OnClick="btnLoad_Click" />
                    </td>
                </tr>
                <tr>
                    <td runat="server" id="rowPayrollPeroid2">
                         <asp:DropDownList ID="ddlYear" AutoPostBack="true" runat="server" AppendDataBoundItems="true"
                        Width="200px" DataValueField="FinancialDateId" OnSelectedIndexChanged="ddlYear_Change" DataTextField="Name">
                        <asp:ListItem Text="--Select--" Value="-1" />
                    </asp:DropDownList>
                    </td>
                     <td class="filterHeader" runat="server" id="Td2">
                      <asp:TextBox Width="180px" ID="txtEmpSearch" runat="server" />
                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtEmpSearch"
                                WatermarkText="Search Text" WatermarkCssClass="searchBoxText" />
                            <cc1:AutoCompleteExtender EnableCaching="true" ID="AutoCompleteExtenderOrganization"
                                runat="server" MinimumPrefixLength="2" ServiceMethod="GetEmployeeNamesWithRetiredAlso"
                                ServicePath="~/PayrollService.asmx" TargetControlID="txtEmpSearch" 
                                CompletionSetCount="10" CompletionInterval="250" CompletionListCssClass="AutoExtender"
                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
                            </cc1:AutoCompleteExtender>
                    </td>
                </tr>
            </table>
        </div>

        <div style="clear: both;margin-left:20px;margin-right:20px;">
            <cc2:EmptyDisplayGridView CssClass="table table-primary mb30 table-bordered table-hover"
             UseAccessibleHeader="true" ShowHeaderWhenEmpty="True"
            ID="gvEmployeeIncome" runat="server" DataKeyNames="AddOnId,PayrollPeriodId" AutoGenerateColumns="False"
             AllowSorting="True" 
                ShowFooterWhenEmpty="False" 
                onselectedindexchanged="gvEmployeeIncome_SelectedIndexChanged">
           
            <Columns>
                <asp:BoundField DataField="SN" HeaderStyle-Width="50px" HeaderText="SN" >
                </asp:BoundField>
                 <asp:TemplateField HeaderStyle-Width="200px" HeaderText="Pay Month" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                       <%# Eval("Period") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-Width="250px"  HeaderText="Add on Name" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                       <%# Eval("Name") %>
                    </ItemTemplate>
                </asp:TemplateField>
               <asp:TemplateField HeaderStyle-Width="150px" >
                <ItemTemplate >
                    <asp:LinkButton runat="server" ID="btn" Text="View" CommandName="Select" />
                </ItemTemplate>
               </asp:TemplateField>
                <asp:TemplateField></asp:TemplateField>
            </Columns>
            <RowStyle CssClass="odd" />
                    <AlternatingRowStyle CssClass="even" />
                    <SelectedRowStyle CssClass="selection"   />
            <EmptyDataTemplate>
                <b>No employee list. </b>
            </EmptyDataTemplate>
        </cc2:EmptyDisplayGridView>
        </div>
        <table style='margin-left:20px;'>
            <tr>
                <td valign="top" style="width: 1200px">
                    <div class="clear" style="">
                        <dxxr:ReportToolbar EnableViewState="false" CssClass="toolbarContainer" ID="ReportToolbar1"
                            ReportViewerID="rptViewer" runat="server" ShowDefaultButtons="False" Width="100%"
                           >
                            <Images SpriteCssFilePath="~/App_Themes/Aqua/{0}/sprite.css">
                            </Images>
                            <Styles CssFilePath="~/App_Themes/Aqua/{0}/styles.css" CssPostfix="Aqua">
                                <LabelStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                <ButtonStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                </ButtonStyle>
                                <EditorStyle>
                                    <Paddings PaddingBottom="1px" PaddingLeft="3px" PaddingRight="3px" PaddingTop="2px" />
                                </EditorStyle>
                            </Styles>
                            <Items>
                                <dxxr:ReportToolbarButton ItemKind="Search" />
                                <dxxr:ReportToolbarSeparator />
                                <dxxr:ReportToolbarButton ItemKind="PrintReport" />
                                <dxxr:ReportToolbarButton ItemKind="PrintPage" />
                                <dxxr:ReportToolbarSeparator />
                                <dxxr:ReportToolbarButton Enabled="False" ItemKind="FirstPage" />
                                <dxxr:ReportToolbarButton Enabled="False" ItemKind="PreviousPage" />
                                <dxxr:ReportToolbarLabel ItemKind="PageLabel" />
                                <dxxr:ReportToolbarComboBox ItemKind="PageNumber" Width="65px">
                                </dxxr:ReportToolbarComboBox>
                                <dxxr:ReportToolbarLabel ItemKind="OfLabel" />
                                <dxxr:ReportToolbarTextBox ItemKind="PageCount" />
                                <dxxr:ReportToolbarButton ItemKind="NextPage" />
                                <dxxr:ReportToolbarButton ItemKind="LastPage" />
                                <dxxr:ReportToolbarSeparator />
                                <dxxr:ReportToolbarButton ItemKind="SaveToDisk" />
                                <dxxr:ReportToolbarButton ItemKind="SaveToWindow" />
                                <dxxr:ReportToolbarComboBox ItemKind="SaveFormat" Width="70px">
                                    <Elements>
                                        <dxxr:ListElement Value="pdf" />
                                        <dxxr:ListElement Value="xls" />
                                        <dxxr:ListElement Value="xlsx" />
                                        <dxxr:ListElement Value="rtf" />
                                        <dxxr:ListElement Value="mht" />
                                        <dxxr:ListElement Value="html" />
                                        <dxxr:ListElement Value="txt" />
                                        <dxxr:ListElement Value="csv" />
                                        <dxxr:ListElement Value="png" />
                                    </Elements>
                                </dxxr:ReportToolbarComboBox>
                            </Items>
                        </dxxr:ReportToolbar>
                        <dxxr:ReportViewer BackColor="White" ClientInstanceName="ReportViewer1" ID="rptViewer"
                            runat="server" CssFilePath="~/App_Themes/Aqua/{0}/styles.css" CssPostfix="Aqua"
                            AutoSize="false" Height="1170px" Width="100%" LoadingPanelText="" SpriteCssFilePath="~/App_Themes/Aqua/{0}/sprite.css">
                            <LoadingPanelImage Url="~/App_Themes/Aqua/Editors/Loading.gif">
                            </LoadingPanelImage>
                            <Border BorderColor="#C1C1C1" BorderStyle="Solid" BorderWidth="0px" />
                            <LoadingPanelStyle ForeColor="#303030">
                            </LoadingPanelStyle>
                            <Paddings Padding="10px" PaddingLeft="55px" PaddingBottom="5px" PaddingTop="35px" />
                        </dxxr:ReportViewer>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
