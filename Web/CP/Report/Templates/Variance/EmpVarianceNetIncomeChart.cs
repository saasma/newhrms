using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace Web.CP.Report.Templates.Pay.Variance
{
    public partial class EmpVarianceNetIncomeChart : DevExpress.XtraReports.UI.XtraReport
    {
        public int EmployeeId
        { 
            get; set; 
        }

        public EmpVarianceNetIncomeChart()
        {
            InitializeComponent();
        }

    }
}
