using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using BLL;
using DevExpress.XtraReports.UI;
using Web.CP.Report.Templates.HR;
using Web.ReportDataSetTableAdapters;
using Utils.Calendar;
using BLL.Base;
using DAL;
using BLL.Manager;
using System.Data;
using DevExpress.XtraPrinting;

namespace Web.CP.Report.Templates.Pay.Detail
{
    public partial class ReportEmpVarianceList : DevExpress.XtraReports.UI.XtraReport
    {

        public int PayrollStartId { get; set; }
        public int PayrollEndId { get; set; }
        public int CompanyId { get; set; }
        public int EmployeeId { get; set; }
        //public bool IsIncome { get; set; }

        private int Name_Column_Index = 1;

        private void CreateUsingLabels(XtraReport report)
        //Report.Templates.Pay.SalarySummary report, List<CalcGetHeaderListResult> headerList)
        {
            DataTable dataTable = (report.DataSource as DataSet).Tables[0];
            int firstColumnExtraWidth = 60;

            Color colorHeader = Color.FromArgb(201, 214, 237);
            Color colorEven = Color.FromArgb(239, 243, 250);

            int colCount = dataTable.Columns.Count; ;//-1 for name
            int colWidth = 80;// (report.PageWidth - (report.Margins.Left + report.Margins.Right)) / colCount;

            report.PageWidth = colWidth * colCount + 20 + 500;
            this.PageWidth = report.PageWidth + 500;

            //report as Report.Templates.Pay.SalarySummary).labelTitle.WidthF = report.PageWidth;


            int currentXLocation = 0;
            int prevWidth = 0;
            int currentWidth = 0;
            // Create header captions
            for (int i = 0; i < colCount; i++)
            {

                XRLabel label = new XRLabel();
                label.BeforePrint += new System.Drawing.Printing.PrintEventHandler(Event_BeforePrint);
                label.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;


                //else if (i == 1)
                //{
                //    currentWidth = colWidth / 2; //for title

                //}
                //else if (i == 2)
                //{
                //    currentWidth = colWidth + firstColumnExtraWidth; //for name

                //}
                //else if (i == 3)
                //{
                //    currentWidth = colWidth + 20; //first cost code

                //}
                //else
                if (i == 0)
                    currentWidth = colWidth / 2;
                else if (i == 1)
                    currentWidth = colWidth * 2;
                else
                {
                    currentWidth = colWidth;//for other amount columns
                }
                currentXLocation += (prevWidth);
                prevWidth = currentWidth;

                label.Location = new Point(currentXLocation, 50);


                label.Text = dataTable.Columns[i].Caption;
                label.Padding = new PaddingInfo(2, 0, 2, 2);

                label.Font = new Font(label.Font.FontFamily, label.Font.Size, FontStyle.Bold);
                label.CanGrow = false;//dont allow to grow as design will be meshed up
                // if (i != Name_Column_Index)
                //{
                //    label.Borders = DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom;



                //    label.Size = new Size(currentWidth, 50);
                //}

                //label.BackColor = colorHeader;
                //new
                //label.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(150)))), ((int)(((byte)(111)))));
                label.BorderColor = colorHeader;
                ReportHelper.HeaderLabelStyle(label);
                // Place the headers onto a PageHeader band
                if (i == 0)
                {
                    label.Borders = DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom;
                    label.Size = new Size(currentWidth, 50);
                }
                else
                {
                    label.Borders = DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom;



                    label.Size = new Size(currentWidth, 50);
                }
                report.Bands[BandKind.PageHeader].Controls.Add(label);
            }


            currentXLocation = 0;
            prevWidth = 0;
            currentWidth = 0;
            // Create data-bound labels with different odd and even backgrounds
            for (int i = 0; i < colCount; i++)
            {
                XRLabel label = new XRLabel();
                label.BeforePrint += new System.Drawing.Printing.PrintEventHandler(Event_BeforePrint);

                //if (i == 0)
                //{
                //    currentWidth = colWidth / 2;
                //    label.TextAlignment = TextAlignment.MiddleCenter;
                //}
                //else if (i == 1)
                //{
                //    currentWidth = colWidth / 2;
                //    label.TextAlignment = TextAlignment.MiddleLeft;

                //}
                //else if (i == 2)
                //{
                //    currentWidth = colWidth + firstColumnExtraWidth; //for name
                //    label.TextAlignment = TextAlignment.MiddleLeft;
                //}
                //else if (i == 3)
                //{
                //    currentWidth = colWidth + 20; //first cost code
                //    label.TextAlignment = TextAlignment.MiddleLeft;
                //}
                //else
                if (i == 0)
                    currentWidth = colWidth / 2;
                else if (i == 1)
                    currentWidth = colWidth * 2;
                else
                {
                    currentWidth = colWidth;//for other amount columns
                }


                if (i == 0 || i == 1)

                    label.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;

                else


                    label.TextAlignment = TextAlignment.MiddleRight;


                currentXLocation += (prevWidth);
                prevWidth = currentWidth;

                // if (i == Name_Column_Index)
                label.Location = new Point(currentXLocation, 0);
                //else
                //{
                //    label.Location = new Point((colWidth * i) + firstColumnExtraWidth, 0);                   
                //}
                label.Padding = new PaddingInfo(2, 0, 2, 0);
                //if (i == Name_Column_Index)
                //    label.DataBindings.Add("Text", null, dataTable.Columns[i].ColumnName);
                //else
                label.DataBindings.Add("Text", null, dataTable.Columns[i].ColumnName, "{0:n2}");




                label.CanGrow = false;


                // label.OddStyleName = "OddStyle";
                //label.EvenStyleName = "EvenStyle";
                //if (i != Name_Column_Index)
                //{
                if (i == 0)
                {

                    label.Borders = DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom;
                    label.Size = new Size(currentWidth, 20);
                }
                else
                    label.Borders = DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom;

                label.Size = new Size(currentWidth, 20);
                //}
                //else
                //{
                //    label.Borders = DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom;
                //    label.TextAlignment = TextAlignment.MiddleLeft;
                //    label.Size = new Size(colWidth + firstColumnExtraWidth, 20);

                //}
                label.Font = new Font(label.Font.FontFamily, label.Font.Size, FontStyle.Regular);

                //label.BackColor = i == 0 ? colorEven : Color.White;
                //label.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(196)))));
                ReportHelper.LabelStyle(label);
                //label.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(205)))), ((int)(((byte)(162)))));
                label.BorderColor = colorHeader;
                // Place the labels onto a Detail band
                report.Bands[BandKind.Detail].Controls.Add(label);
            }

            currentXLocation = 0;
            prevWidth = 0;
            currentWidth = 0;

            //create footer captions for Total
            for (int i = 0; i < colCount; i++)
            {
                XRLabel label = new XRLabel();
                //set properties for sum/total


                label.BeforePrint += new System.Drawing.Printing.PrintEventHandler(Event_BeforePrint);

                //if (i == 0)
                //{
                //    currentWidth = colWidth / 2;
                //}
                //else if (i == 1)
                //{
                //    currentWidth = colWidth / 2;

                //}
                //else if (i == 2)
                //{
                //    currentWidth = colWidth + firstColumnExtraWidth; //for name

                //}
                //else if (i == 3)
                //{
                //    currentWidth = colWidth + 20; //first cost code

                //}
                //else
                if (i == 0)
                    currentWidth = colWidth / 2;
                else if (i == 1)
                    currentWidth = colWidth * 2;
                else
                {
                    currentWidth = colWidth;//for other amount columns
                }


                currentXLocation += (prevWidth);
                prevWidth = currentWidth;

                label.Location = new Point(currentXLocation, 0);

                label.Padding = new PaddingInfo(2, 0, 2, 0);




                DevExpress.XtraReports.UI.XRSummary summary = new DevExpress.XtraReports.UI.XRSummary();


                //skip for first two columns
                if (i == 0)
                {
                    label.Text = "Total";
                }
                else if (i >= 1)
                {
                    label.DataBindings.Add("Text", null, dataTable.Columns[i].ColumnName);



                    summary.FormatString = "{0:n2}";

                    summary.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
                    label.Summary = summary;

                }


                label.CanGrow = false;

                // label.OddStyleName = "OddStyle";
                //label.EvenStyleName = "EvenStyle";
                //if (i != Name_Column_Index)
                //{

                //}
                //else
                //{
                //    label.Text = "Total";
                if (i == 0)

                    label.Borders = DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom;

                else
                    label.Borders = DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom;

                label.TextAlignment = TextAlignment.MiddleRight;
                label.Size = new Size(currentWidth, 20);
                //label.Borders = DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom;
                //    label.TextAlignment = TextAlignment.MiddleLeft;
                //    label.Size = new Size(colWidth + firstColumnExtraWidth, 20);
                //}
                label.Font = new Font(label.Font.FontFamily, label.Font.Size, FontStyle.Bold);

                //label.BackColor = i == 0 ? colorEven : Color.White;
                //label.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(205)))), ((int)(((byte)(162)))));
                label.BorderColor = colorHeader;

                // Place the labels onto a Detail band
                report.Bands[BandKind.ReportFooter].Controls.Add(label);

            }
        }


        protected void LoadReport(int employeeId, XRSubreport subReport)
        {




            Report.Templates.Pay.SalarySummary mainReport = new Web.CP.Report.Templates.Pay.SalarySummary();
            mainReport.Name = "Variance Report";

            List<DAL.Report_Pay_EmployeeVarianceDetailsResult> allData = HttpContext.Current.Items["VarianceList"]
                 as List<DAL.Report_Pay_EmployeeVarianceDetailsResult>;
            List<DAL.Report_Pay_EmployeeVarianceDetailsResult> data = new List<Report_Pay_EmployeeVarianceDetailsResult>();
            foreach (DAL.Report_Pay_EmployeeVarianceDetailsResult entity in allData)
            {
                if (entity.EmployeeId == employeeId)
                {
                    data.Add(entity);


                }


            }

            //lblName.Text += EmployeeManager.GetEmployeeName(employeeId);
            //lblEIN.Text += employeeId.ToString(); ;

            mainReport.labelTitle.Visible = false;


            List<CalcGetHeaderListResult> headerList = HttpContext.Current.Items["VarianceHeaderList"]
                as List<CalcGetHeaderListResult>;




            DataTable dataTable = CreateDataTable(data, headerList);
            DataSet dset = new DataSet();
            dset.Tables.Add(dataTable);




            mainReport.DataSource = dset;
            mainReport.DataMember = "Report";









            CreateUsingLabels(mainReport);




            if (dataTable.Rows.Count > 0)
            {
                // DisplayReport(mainReport);


                subReport.ReportSource = mainReport;

                if (!string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["withtax"]))
                {

                    LoadTaxDetails(employeeId, this.PayrollEndId);
                }
                else
                {
                    tableDetails.Visible = false;
                    tableFooter.Visible = false;
                }
            }
            else
            {
                subReport.ReportSource = null;
            }
            // report.ReportToolbar.Width = new Unit(958);
            //report.ReportViewer.Width = new Unit(mainReport.PageWidth);

        }

        public void LoadTaxDetails(int empid, int periodId)
        {
            string data = CalculationManager.GetSalaryCalcForTaxDetails(empid,
                SessionManager.CurrentCompanyId, periodId);

            // replace CIT name in tax details with other Retirement fundname
            if (!string.IsNullOrEmpty(SessionManager.CurrentCompany.OtherFundAbbreviation))
            {
                string name = SessionManager.CurrentCompany.OtherFundAbbreviation;
                data = data.Replace("CIT Deduction", name + " Deduction");
                data = data.Replace("CIT Settlement", name + " Settlement");
                data = data.Replace("Provisional CIT", "Provisional " + name);
                data = data.Replace("Sum of CIT And PF", "Sum of " + name + " And PF");
            }

            EEmployee employee = EmployeeManager.GetEmployeeById(empid);

            EmployeeRegularIncomeHistory details = BLL.BaseBiz.PayrollDataContext
                .EmployeeRegularIncomeHistories.FirstOrDefault(x => x.EmployeeId == employee.EmployeeId
                    && x.PayrollPeriodId == periodId);

            string sex = "";

            if (details != null)
            {
                if (details.Gender == 0)
                    sex = "Female";
                else if (details.Gender == 1)
                    sex = "Male";
                else
                    sex = "Third Gender";


                // Append additional information
                data = string.Format(";Sex::{0}:2", sex) +
                        string.Format(";Tax Status (M/S)::{0}:2",
                        GetTaxStatus(employee.MaritalStatus, employee.HasCoupleTaxStatus)) + data;
            }
            else
            {
                if (employee.Gender == 0)
                    sex = "Female";
                else if (employee.Gender == 1)
                    sex = "Male";
                else
                    sex = "Third Gender";


                // Append additional information
                data = string.Format(";Sex::{0}:2", sex) +
                        string.Format(";Tax Status (M/S)::{0}:2",
                        GetTaxStatus(employee.MaritalStatus, employee.HasCoupleTaxStatus)) + data;

            }

            ReportDataSet.TaxCalculationDetailsDataTable mainTable = GetDataTable(data, employee);


            //mainReport.Month = payrollPeriod.Month;
            //mainReport.Year = payrollPeriod.Year.Value;
            //mainReport.IsIncome = true;
            //mainReport.CompanyId = SessionManager.CurrentCompanyId;

            this.DataSource = mainTable;
            this.DataMember = "Report";

            //report.DisplayReport(mainReport);
        }
        public ReportDataSet.TaxCalculationDetailsDataTable GetDataTable(string data, EEmployee emp)
        {

            string[] rows = data.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

            ReportDataSet.TaxCalculationDetailsDataTable table = new ReportDataSet.TaxCalculationDetailsDataTable();
            ReportDataSet.TaxCalculationDetailsRow rowTotalIfIncomeInsuranceExists = table.NewTaxCalculationDetailsRow();

            decimal totalTax = 0;
            decimal totalTaxAmount = 0;
            decimal taxableIncome = 0;
            decimal totalSSTAndTDS = 0;
            decimal totalAddOnTax = 0;
            decimal insuranceIncome = 0;
            decimal totalTaxPaid = 0;

            foreach (string strRow in rows)
            {
                string[] values = strRow.Split(new char[] { ':' });



                ReportDataSet.TaxCalculationDetailsRow row = table.NewTaxCalculationDetailsRow();

                row.Header = values[0];

                if (values.Length <= 1)
                    continue;



                if (!string.IsNullOrEmpty(values[1]))
                    row["Amount"] = decimal.Parse(values[1]);

                if (row.Header.ToLower().Equals("Insurance Income".ToLower()))
                {
                    row[0] = "Add Insurance Paid by Company";

                    // create total row
                    insuranceIncome = decimal.Parse(values[1]);

                    rowTotalIfIncomeInsuranceExists[0] = "Total";
                    rowTotalIfIncomeInsuranceExists["Amount"] = 0;
                    table.AddTaxCalculationDetailsRow(rowTotalIfIncomeInsuranceExists);
                    table.AddTaxCalculationDetailsRow(row);
                    continue;
                }
                else if (row.Header.ToLower().Equals("Annual Taxable Income".ToLower()))
                {
                    if (rowTotalIfIncomeInsuranceExists != null)
                        rowTotalIfIncomeInsuranceExists["Amount"] = decimal.Parse(values[1]) - insuranceIncome;

                    row[0] = "Gross Income";
                }
                else if (row.Header.ToLower().Contains("min of a, b or c".ToLower()))
                {
                    row[0] = "Less: Retirement Fund Deduction";
                }
                else if (row.Header.ToLower().Equals("Remote Area".ToLower()))
                {
                    row[0] = "Less: Remote Area";
                }
                else if (row.Header.ToLower().Equals("Insurance Premium".ToLower()))
                {
                    row[0] = "Less: Insurance Deduction";
                }
                else if (row.Header.ToLower().Equals("Annual Taxable Amount".ToLower()))
                {
                    row[0] = "Taxable Income";
                    taxableIncome = decimal.Parse(values[1]);
                    row.RowType = 3;
                }
                // Calculation for other fixed also
                else if (row.Header.ToLower().Contains("1% tax".ToLower()) || row.Header.ToLower().Contains("15% tax".ToLower())
                    || row.Header.ToLower().Contains("25% tax".ToLower()) || row.Header.ToLower().Contains("surcharge".ToLower()))
                {
                    row[0] = row.Header;


                    //if (row.Header.ToLower().Contains("surcharge".ToLower()))
                    //{
                    //    decimal taxAmount = taxableIncome - totalTaxAmount;
                    //    if (taxAmount < 0)
                    //        taxAmount = 0;
                    //    row["Amount2"] = taxAmount;
                    //    totalTaxAmount += taxAmount;
                    //}
                    if (values.Length >= 3)
                    {
                        //row[0] += " (" + values[2] + ")";

                        if (!string.IsNullOrEmpty(values[2]))
                        {
                            decimal taxAmount = decimal.Parse(values[2].Replace("on", "").Replace(" ", ""));
                            row["Amount2"] = taxAmount;
                            totalTaxAmount += taxAmount;
                        }
                    }

                    totalTax += decimal.Parse(values[1]);
                }
                else if (row.Header.ToLower().Contains("medical tax"))
                {
                    row[0] = "Less: Medical Tax Credit";
                }
                //else if (row.Header.ToLower().Contains("Female Rebate".ToLower()))
                //{
                //    row[0] = "Less: Female Tax Rebate";
                //}
                else if (row.Header.ToLower().Contains("Female Rebate".ToLower()))
                {
                    row[0] = "Less: Female Tax Rebate";
                    row["Amount"] = Math.Abs(decimal.Parse(values[1]));
                }
                else if (row.Header.ToLower().Contains("Total SST for the Year".ToLower()) || row.Header.ToLower().Contains("Total TDS for the Year".ToLower()))
                {
                    //row[0] = "Less: Female Tax Rebate";
                    totalSSTAndTDS += decimal.Parse(values[1]);

                    if (row.Header.ToLower().Contains("Total SST for the Year".ToLower()))
                        continue;

                    row["Amount"] = totalSSTAndTDS;
                    row[0] = "Total Tax Payable";
                }
                else if (row.Header.ToLower().Contains("SST Add-on Paid".ToLower()) || row.Header.ToLower().Contains("TDS Add-on Paid".ToLower()))
                {
                    // add on will not be displayed separately, it will be added in total tax and tax paid
                    totalAddOnTax += decimal.Parse(values[1]);

                    totalSSTAndTDS += (Math.Abs(totalAddOnTax));

                    totalTaxPaid += (Math.Abs(totalAddOnTax));

                    //if (row.Header.ToLower().Contains("SST Add-on Paid".ToLower()))
                    continue;

                    //row["Amount"] = Math.Abs(totalAddOnTax);

                    //row[0] = "Less: Add on Tax";
                }
                else if (row.Header.ToLower().Contains("SST Paid in Past Mont".ToLower())
                    || row.Header.ToLower().Contains("TDS Paid in Past Mont".ToLower())
                     || row.Header.ToLower().Contains("SST this Month".ToLower())
                     || row.Header.ToLower().Contains("TDS this Month".ToLower()))
                {
                    //row[0] = "Less: Female Tax Rebate";
                    totalTaxPaid += decimal.Parse(values[1]);

                    if (row.Header.ToLower().Contains("SST Paid in Past Mont".ToLower())
                    || row.Header.ToLower().Contains("TDS Paid in Past Mont".ToLower())
                     || row.Header.ToLower().Contains("SST this Month".ToLower()))
                        continue;

                    row["Amount"] = totalTaxPaid;
                    row[0] = "Tax Paid";
                }
                else
                    continue;


                //try
                //{


                table.AddTaxCalculationDetailsRow(row);
            }




            return table;

        }




        public string GetTaxStatus(string martialStatus, bool? hasCoupleTax)
        {
            if (martialStatus.ToString().ToLower() == "single")
                return "Unmarried-Single Tax Status";

            if (martialStatus.ToLower() == "married" && hasCoupleTax != null && hasCoupleTax.Value)
                return "Married-Couple Tax Status";

            return "Married-Single Tax Status";
        }


        private DataTable CreateDataTable(List<DAL.Report_Pay_EmployeeVarianceDetailsResult> data, List<CalcGetHeaderListResult> headerList)
        {
            DataTable dataTable = new DataTable();
            dataTable.TableName = "Report";

            dataTable.Columns.Add("SN", typeof(string));
            dataTable.Columns.Add("Month", typeof(string));
            dataTable.Columns.Add("Date", typeof(string));
            //dataTable.Columns.Add("Name", typeof(string));
            //dataTable.Columns.Add("CostCode", typeof(string));

            List<CalcGetHeaderListResult> newheaderList = new List<CalcGetHeaderListResult>();

            // Create table cells, fill the header cells with text, bind the cells to data
            for (int i = 0; i < headerList.Count; i++)
            {
                DataColumn column = new DataColumn(headerList[i].Type + ":" + headerList[i].SourceId, typeof(decimal));
                column.Caption = headerList[i].HeaderName;


                // if data does not exists then skip the header

                decimal total = 0;
                foreach (Report_Pay_EmployeeVarianceDetailsResult row in data)
                {
                    total += Convert.ToDecimal(row.GetCellValue(headerList[i].Type.Value, headerList[i].SourceId.Value, 2, null));
                }

                if (total == 0)
                    continue;

                newheaderList.Add(headerList[i]);
                dataTable.Columns.Add(column);
            }

            int snNumber = 1;
            decimal? value = 0;
            //add rows
            foreach (Report_Pay_EmployeeVarianceDetailsResult row in data)
            {
                List<object> list = new List<object>();

                if (row.Month.IndexOf("/") > 0)
                    row.Month = row.Month.Substring(row.Month.IndexOf("/") + 1) + " " + row.Month.Substring(0, row.Month.IndexOf("/"));

                list.Add(snNumber.ToString());
                list.Add(row.Month);
                list.Add(string.Format("{0:yyyy/MM/dd}", row.CreatedOn));
                //row.SN = snNumber++;
                //list.Add(row.SN);
                //list.Add(row.Title);
                //list.Add(row.Name);
                //list.Add(row.CostCode); 

                decimal rowTotal = 0;

                for (int i = 0; i < newheaderList.Count; i++)
                {
                    value = row.GetCellValue(newheaderList[i].Type.Value, newheaderList[i].SourceId.Value, 2, null);
                    list.Add(value);

                    rowTotal += Convert.ToDecimal(value);
                }


                if (!string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["taxonly"]) && rowTotal == 0)
                    continue;


                dataTable.Rows.Add(list.ToArray());

                snNumber += 1;
            }



            return dataTable;


        }
        public void SetSubReport(int employeeId, object sender)
        {

            XRSubreport subReport = (XRSubreport)sender;

            switch (subReport.Name)
            {

                case "subReportIncomes":
                    LoadReport(employeeId, subReport);
                    break;


            }
        }

        public ReportEmpVarianceList()
        {
            InitializeComponent();
        }

        private void Event_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (Web.CP.Report.ReportHelper.IsReportExportState())
            {
                ((XRLabel)sender).BackColor = System.Drawing.Color.Transparent;
                ((XRLabel)sender).BorderColor = System.Drawing.Color.Black;
            }

        }

        protected void ClearBorderControlEvent(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ReportHelper.TableCell_BeforePrintEvent(sender, e);
        }
        protected void ClearBorderLessControlEvent(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ReportHelper.TableCell_BeforePrintEvent(sender, e);
        }

        private void subReportIncomes_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRSubreport report = (XRSubreport)sender;


            Web.CP.Report.Templates.Pay.Detail.ReportEmpVarianceList mainReport =
            (Web.CP.Report.Templates.Pay.Detail.ReportEmpVarianceList)report.Report;



            mainReport.SetSubReport(this.EmployeeId, sender);

        }

        private void xrChart1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {



            XRChart report = (XRChart)sender;
            int employeeId = Convert.ToInt32(GetCurrentColumnValue("EmployeeId"));


            // report.DataSource

            //mainReport.SetSubReport(Convert.ToInt32(GetCurrentColumnValue("EmployeeId")), sender);

        }





    }
}
