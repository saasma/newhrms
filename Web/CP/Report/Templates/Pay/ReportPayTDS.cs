using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using BLL;
using DevExpress.XtraReports.UI;
using Web.CP.Report.Templates.HR;
using Web.ReportDataSetTableAdapters;
using Utils.Calendar;
using BLL.Base;
using DAL;
using BLL.Manager;
using System.Data;

namespace Web.CP.Report.Templates.Pay
{
    public partial class ReportPayTDS : DevExpress.XtraReports.UI.XtraReport
    {
        public ReportPayTDS()
        {
            InitializeComponent();
        }

        public void SetSubReport(object sender)
        {
            ReportHelper.BindCompanyInfo(sender);
        }
    }
}
