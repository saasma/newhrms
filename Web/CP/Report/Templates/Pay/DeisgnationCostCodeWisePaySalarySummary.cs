using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace Web.CP.Report.Templates.Pay
{
    public partial class DeisgnationCostCodeWisePaySalarySummary : DevExpress.XtraReports.UI.XtraReport
    {
        public DeisgnationCostCodeWisePaySalarySummary()
        {
            InitializeComponent();
        }

        protected void ClearBorderControlEvent(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
           // ReportHelper.TableCell_BeforePrintEvent(sender, e);
        }
        protected void ClearBorderLessControlEvent(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
           // ReportHelper.TableCell_BeforePrintEvent(sender, e);
        }

        private void xrLabel1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRLabel label = sender as XRLabel;

            label.Text = "Total " + label.Text;
        }
    }
}
