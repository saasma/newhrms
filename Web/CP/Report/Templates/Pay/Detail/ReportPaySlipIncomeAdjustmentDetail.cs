using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace Web.CP.Report.Templates.Pay.Detail
{
    public partial class ReportPaySlipIncomeAdjustmentDetail : DevExpress.XtraReports.UI.XtraReport
    {
        public int EmployeeId
        { 
            get; set; 
        }

        public ReportPaySlipIncomeAdjustmentDetail()
        {
            InitializeComponent();
        }

    }
}
