namespace Web.CP.Report.Templates.Pay.Detail
{
    partial class ReportPaySlipWithTaxDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrPageBreak2 = new DevExpress.XtraReports.UI.XRPageBreak();
            this.xrLine7 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine9 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.totalLbl = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.subReportTaxDetails = new DevExpress.XtraReports.UI.XRSubreport();
            this.xrLine4 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine6 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine5 = new DevExpress.XtraReports.UI.XRLine();
            this.subReportAttendace = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportDeduction = new DevExpress.XtraReports.UI.XRSubreport();
            this.xrLine3 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
            this.subReportIncome = new DevExpress.XtraReports.UI.XRSubreport();
            this.labelMonth = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.xrPageBreak1 = new DevExpress.XtraReports.UI.XRPageBreak();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.reportDataSet1 = new Web.ReportDataSet();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.labelTitle = new DevExpress.XtraReports.UI.XRLabel();
            this.labelCompanyName = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine8 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
            this.DateTimeStyle = new DevExpress.XtraReports.UI.XRControlStyle();
            this.SlipHeaderStyle = new DevExpress.XtraReports.UI.XRControlStyle();
            this.InfoStyle = new DevExpress.XtraReports.UI.XRControlStyle();
            this.LineStyle = new DevExpress.XtraReports.UI.XRControlStyle();
            this.ContentLineStyle = new DevExpress.XtraReports.UI.XRControlStyle();
            this.formattingRule1 = new DevExpress.XtraReports.UI.FormattingRule();
            this.report_Pay_PaySlipDetailTableAdapter = new Web.ReportDataSetTableAdapters.Report_Pay_PaySlipDetailTableAdapter();
            this.report_HR_DateOfJoiningTableAdapter = new Web.ReportDataSetTableAdapters.Report_HR_DateOfJoiningTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.reportDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageBreak2,
            this.xrLine7,
            this.xrLine9,
            this.xrLabel12,
            this.totalLbl,
            this.xrLabel19,
            this.xrLabel11,
            this.subReportTaxDetails,
            this.xrLine4,
            this.xrLabel7,
            this.xrLabel6,
            this.xrLabel3,
            this.xrLabel18,
            this.xrLine6,
            this.xrLabel17,
            this.xrLine5,
            this.subReportAttendace,
            this.subReportDeduction,
            this.xrLine3,
            this.xrLine2,
            this.subReportIncome,
            this.labelMonth,
            this.xrLabel9,
            this.xrLabel10,
            this.xrLabel4,
            this.xrLabel5,
            this.xrLabel2,
            this.xrLabel8,
            this.xrLabel1,
            this.xrLine1});
            this.Detail.HeightF = 566.0416F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.StylePriority.UseBorders = false;
            this.Detail.StylePriority.UseBorderWidth = false;
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPageBreak2
            // 
            this.xrPageBreak2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 554.0416F);
            this.xrPageBreak2.Name = "xrPageBreak2";
            // 
            // xrLine7
            // 
            this.xrLine7.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine7.LocationFloat = new DevExpress.Utils.PointFloat(7.541758F, 353.8542F);
            this.xrLine7.Name = "xrLine7";
            this.xrLine7.SizeF = new System.Drawing.SizeF(349.9985F, 6.999969F);
            this.xrLine7.StylePriority.UseBorders = false;
            // 
            // xrLine9
            // 
            this.xrLine9.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine9.LocationFloat = new DevExpress.Utils.PointFloat(7.543386F, 383.8542F);
            this.xrLine9.Name = "xrLine9";
            this.xrLine9.SizeF = new System.Drawing.SizeF(349.9969F, 6.999969F);
            this.xrLine9.StylePriority.UseBorders = false;
            // 
            // xrLabel12
            // 
            this.xrLabel12.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel12.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Report_Pay_PaySlipDetail.DeductionTotal", "{0:N2}")});
            this.xrLabel12.Font = new System.Drawing.Font("Tahoma", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(179.0835F, 324.8401F);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(172.7916F, 23.00003F);
            this.xrLabel12.StylePriority.UseBorders = false;
            this.xrLabel12.StylePriority.UseFont = false;
            this.xrLabel12.StylePriority.UseTextAlignment = false;
            this.xrLabel12.Text = "xrLabel12";
            this.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // totalLbl
            // 
            this.totalLbl.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.totalLbl.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Report_Pay_PaySlipDetail.NetSalary", "{0:N2}")});
            this.totalLbl.Font = new System.Drawing.Font("Tahoma", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalLbl.LocationFloat = new DevExpress.Utils.PointFloat(179.0834F, 360.8541F);
            this.totalLbl.Name = "totalLbl";
            this.totalLbl.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.totalLbl.SizeF = new System.Drawing.SizeF(172.7916F, 23.00003F);
            this.totalLbl.StylePriority.UseBorders = false;
            this.totalLbl.StylePriority.UseFont = false;
            this.totalLbl.StylePriority.UseTextAlignment = false;
            this.totalLbl.Text = "totalLbl";
            this.totalLbl.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel19
            // 
            this.xrLabel19.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel19.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel19.ForeColor = System.Drawing.Color.Black;
            this.xrLabel19.LocationFloat = new DevExpress.Utils.PointFloat(7.081858F, 360.8541F);
            this.xrLabel19.Name = "xrLabel19";
            this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel19.SizeF = new System.Drawing.SizeF(100F, 23.00003F);
            this.xrLabel19.StyleName = "SlipHeaderStyle";
            this.xrLabel19.StylePriority.UseBorders = false;
            this.xrLabel19.StylePriority.UseForeColor = false;
            this.xrLabel19.StylePriority.UseTextAlignment = false;
            this.xrLabel19.Text = "Net Paid:";
            this.xrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel11
            // 
            this.xrLabel11.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel11.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Report_Pay_PaySlipDetail.IncomeGross", "{0:N2}")});
            this.xrLabel11.Font = new System.Drawing.Font("Tahoma", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(179.0835F, 245.4182F);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(172.7916F, 23.00003F);
            this.xrLabel11.StylePriority.UseBorders = false;
            this.xrLabel11.StylePriority.UseFont = false;
            this.xrLabel11.StylePriority.UseTextAlignment = false;
            this.xrLabel11.Text = "xrLabel11";
            this.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // subReportTaxDetails
            // 
            this.subReportTaxDetails.LocationFloat = new DevExpress.Utils.PointFloat(366.6667F, 5.086263E-05F);
            this.subReportTaxDetails.Name = "subReportTaxDetails";
            this.subReportTaxDetails.ReportSource = new Web.CP.Report.Templates.Pay.Detail.ReportTaxCalculationDetailsForPayslip();
            this.subReportTaxDetails.Scripts.OnBeforePrint = "xrSubreport1_BeforePrint";
            this.subReportTaxDetails.SizeF = new System.Drawing.SizeF(711.3333F, 516.5068F);
            this.subReportTaxDetails.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrSubreport1_BeforePrint);
            // 
            // xrLine4
            // 
            this.xrLine4.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.xrLine4.LocationFloat = new DevExpress.Utils.PointFloat(3.540115F, 152.8684F);
            this.xrLine4.Name = "xrLine4";
            this.xrLine4.SizeF = new System.Drawing.SizeF(354F, 6.256622F);
            this.xrLine4.StylePriority.UseBorders = false;
            this.xrLine4.StylePriority.UseForeColor = false;
            this.xrLine4.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClearBorderControlEvent);
            // 
            // xrLabel7
            // 
            this.xrLabel7.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel7.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Report_Pay_PaySlipDetail.IDCardNo")});
            this.xrLabel7.Font = new System.Drawing.Font("Tahoma", 9F);
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(132.0833F, 46.00001F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 2, 0, 0, 100F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(201.2916F, 23.00002F);
            this.xrLabel7.StylePriority.UseBorders = false;
            this.xrLabel7.StylePriority.UseFont = false;
            this.xrLabel7.StylePriority.UsePadding = false;
            this.xrLabel7.StylePriority.UseTextAlignment = false;
            this.xrLabel7.Text = "xrLabel7";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel6
            // 
            this.xrLabel6.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(8.54009F, 45.99996F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(111.8766F, 23.00002F);
            this.xrLabel6.StyleName = "InfoStyle";
            this.xrLabel6.Text = "EMP Code :";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrLabel6.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClearBorderControlEvent);
            // 
            // xrLabel3
            // 
            this.xrLabel3.BackColor = System.Drawing.Color.Transparent;
            this.xrLabel3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(8.54009F, 129.8683F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.xrLabel3.StyleName = "SlipHeaderStyle";
            this.xrLabel3.StylePriority.UseBackColor = false;
            this.xrLabel3.StylePriority.UseBorders = false;
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.Text = "Attendance";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrLabel3.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClearBorderControlEvent);
            // 
            // xrLabel18
            // 
            this.xrLabel18.BackColor = System.Drawing.Color.Transparent;
            this.xrLabel18.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel18.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel18.LocationFloat = new DevExpress.Utils.PointFloat(8.54009F, 188F);
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel18.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.xrLabel18.StyleName = "SlipHeaderStyle";
            this.xrLabel18.StylePriority.UseBackColor = false;
            this.xrLabel18.StylePriority.UseBorders = false;
            this.xrLabel18.StylePriority.UseTextAlignment = false;
            this.xrLabel18.Text = "Earnings";
            this.xrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrLabel18.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClearBorderControlEvent);
            // 
            // xrLine6
            // 
            this.xrLine6.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.xrLine6.LocationFloat = new DevExpress.Utils.PointFloat(3.540115F, 211.9982F);
            this.xrLine6.Name = "xrLine6";
            this.xrLine6.SizeF = new System.Drawing.SizeF(354F, 5.210022F);
            this.xrLine6.StylePriority.UseBorders = false;
            this.xrLine6.StylePriority.UseForeColor = false;
            this.xrLine6.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClearBorderControlEvent);
            // 
            // xrLabel17
            // 
            this.xrLabel17.BackColor = System.Drawing.Color.Transparent;
            this.xrLabel17.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel17.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(8.54009F, 268.4182F);
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel17.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.xrLabel17.StyleName = "SlipHeaderStyle";
            this.xrLabel17.StylePriority.UseBackColor = false;
            this.xrLabel17.StylePriority.UseBorders = false;
            this.xrLabel17.StylePriority.UseTextAlignment = false;
            this.xrLabel17.Text = "Deductions";
            this.xrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrLabel17.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClearBorderControlEvent);
            // 
            // xrLine5
            // 
            this.xrLine5.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.xrLine5.LocationFloat = new DevExpress.Utils.PointFloat(3.540115F, 240.2082F);
            this.xrLine5.Name = "xrLine5";
            this.xrLine5.SizeF = new System.Drawing.SizeF(354.0002F, 5.209991F);
            this.xrLine5.StylePriority.UseBorders = false;
            this.xrLine5.StylePriority.UseForeColor = false;
            this.xrLine5.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClearBorderControlEvent);
            // 
            // subReportAttendace
            // 
            this.subReportAttendace.LocationFloat = new DevExpress.Utils.PointFloat(8.54009F, 159.125F);
            this.subReportAttendace.Name = "subReportAttendace";
            this.subReportAttendace.ReportSource = new Web.CP.Report.Templates.Pay.Detail.ReportPaySlipAttendanceDetail();
            this.subReportAttendace.Scripts.OnBeforePrint = "xrSubreport1_BeforePrint";
            this.subReportAttendace.SizeF = new System.Drawing.SizeF(343.335F, 22.99995F);
            this.subReportAttendace.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrSubreport1_BeforePrint);
            // 
            // subReportDeduction
            // 
            this.subReportDeduction.LocationFloat = new DevExpress.Utils.PointFloat(8.54009F, 296.6283F);
            this.subReportDeduction.Name = "subReportDeduction";
            this.subReportDeduction.ReportSource = new Web.CP.Report.Templates.Pay.Detail.ReportPaySlipDeductionDetail();
            this.subReportDeduction.Scripts.OnBeforePrint = "xrSubreport1_BeforePrint";
            this.subReportDeduction.SizeF = new System.Drawing.SizeF(343.335F, 22.99997F);
            this.subReportDeduction.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrSubreport1_BeforePrint);
            // 
            // xrLine3
            // 
            this.xrLine3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.xrLine3.LocationFloat = new DevExpress.Utils.PointFloat(3.540115F, 319.6301F);
            this.xrLine3.Name = "xrLine3";
            this.xrLine3.SizeF = new System.Drawing.SizeF(354F, 5.209991F);
            this.xrLine3.StylePriority.UseBorders = false;
            this.xrLine3.StylePriority.UseForeColor = false;
            this.xrLine3.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClearBorderControlEvent);
            // 
            // xrLine2
            // 
            this.xrLine2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.xrLine2.LocationFloat = new DevExpress.Utils.PointFloat(3.540115F, 182.1283F);
            this.xrLine2.Name = "xrLine2";
            this.xrLine2.SizeF = new System.Drawing.SizeF(354F, 5.871796F);
            this.xrLine2.StylePriority.UseBorders = false;
            this.xrLine2.StylePriority.UseForeColor = false;
            this.xrLine2.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClearBorderControlEvent);
            // 
            // subReportIncome
            // 
            this.subReportIncome.LocationFloat = new DevExpress.Utils.PointFloat(8.54009F, 217.2083F);
            this.subReportIncome.Name = "subReportIncome";
            this.subReportIncome.ReportSource = new Web.CP.Report.Templates.Pay.Detail.ReportPaySlipIncomeDetail();
            this.subReportIncome.Scripts.OnBeforePrint = "xrSubreport1_BeforePrint";
            this.subReportIncome.SizeF = new System.Drawing.SizeF(343.335F, 22.99998F);
            this.subReportIncome.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrSubreport1_BeforePrint);
            // 
            // labelMonth
            // 
            this.labelMonth.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.labelMonth.Font = new System.Drawing.Font("Tahoma", 9F);
            this.labelMonth.LocationFloat = new DevExpress.Utils.PointFloat(132.0833F, 5.086263E-05F);
            this.labelMonth.Name = "labelMonth";
            this.labelMonth.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 2, 0, 0, 100F);
            this.labelMonth.SizeF = new System.Drawing.SizeF(201.2915F, 23F);
            this.labelMonth.StylePriority.UseBorders = false;
            this.labelMonth.StylePriority.UseFont = false;
            this.labelMonth.StylePriority.UsePadding = false;
            this.labelMonth.StylePriority.UseTextAlignment = false;
            this.labelMonth.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel9
            // 
            this.xrLabel9.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel9.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Report_Pay_PaySlipDetail.Department")});
            this.xrLabel9.Font = new System.Drawing.Font("Tahoma", 9F);
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(132.0833F, 69.00004F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 2, 0, 0, 100F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(201.2916F, 23.00001F);
            this.xrLabel9.StylePriority.UseBorders = false;
            this.xrLabel9.StylePriority.UseFont = false;
            this.xrLabel9.StylePriority.UsePadding = false;
            this.xrLabel9.StylePriority.UseTextAlignment = false;
            this.xrLabel9.Text = "xrLabel9";
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrLabel9.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClearBorderControlEvent);
            // 
            // xrLabel10
            // 
            this.xrLabel10.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(8.54009F, 0F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(111.8766F, 23F);
            this.xrLabel10.StyleName = "InfoStyle";
            this.xrLabel10.Text = "Month :";
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrLabel10.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClearBorderControlEvent);
            // 
            // xrLabel4
            // 
            this.xrLabel4.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(8.54009F, 68.99999F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(111.8766F, 23.00002F);
            this.xrLabel4.StyleName = "InfoStyle";
            this.xrLabel4.Text = "Department :";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrLabel4.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClearBorderControlEvent);
            // 
            // xrLabel5
            // 
            this.xrLabel5.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(8.54009F, 91.99987F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(111.8766F, 23.00003F);
            this.xrLabel5.StyleName = "InfoStyle";
            this.xrLabel5.Text = "Designation :";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrLabel5.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClearBorderControlEvent);
            // 
            // xrLabel2
            // 
            this.xrLabel2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Report_Pay_PaySlipDetail.Name")});
            this.xrLabel2.Font = new System.Drawing.Font("Tahoma", 9F);
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(132.0833F, 22.99998F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(201.2931F, 23F);
            this.xrLabel2.StylePriority.UseBorders = false;
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UsePadding = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "xrLabel2";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel8
            // 
            this.xrLabel8.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel8.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Report_Pay_PaySlipDetail.Designation")});
            this.xrLabel8.Font = new System.Drawing.Font("Tahoma", 9F);
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(132.0833F, 92.00002F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 2, 0, 0, 100F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(201.2915F, 22.99998F);
            this.xrLabel8.StylePriority.UseBorders = false;
            this.xrLabel8.StylePriority.UseFont = false;
            this.xrLabel8.StylePriority.UsePadding = false;
            this.xrLabel8.StylePriority.UseTextAlignment = false;
            this.xrLabel8.Text = "xrLabel8";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrLabel8.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClearBorderControlEvent);
            // 
            // xrLabel1
            // 
            this.xrLabel1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(8.54009F, 22.99993F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(111.8766F, 23F);
            this.xrLabel1.StyleName = "InfoStyle";
            this.xrLabel1.Text = "Name :";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrLabel1.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClearBorderControlEvent);
            // 
            // xrLine1
            // 
            this.xrLine1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(3.540115F, 291.4199F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(354F, 5.209991F);
            this.xrLine1.StylePriority.UseBorders = false;
            this.xrLine1.StylePriority.UseForeColor = false;
            this.xrLine1.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClearBorderControlEvent);
            // 
            // xrPageBreak1
            // 
            this.xrPageBreak1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 57.37488F);
            this.xrPageBreak1.Name = "xrPageBreak1";
            this.xrPageBreak1.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClearBorderControlEvent);
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 5F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageBreak1});
            this.BottomMargin.HeightF = 59.37488F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.Scripts.OnBeforePrint = "BottomMargin_BeforePrint";
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // reportDataSet1
            // 
            this.reportDataSet1.DataSetName = "ReportDataSet";
            this.reportDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.labelTitle,
            this.labelCompanyName,
            this.xrLine8,
            this.xrLabel20});
            this.PageHeader.HeightF = 49.58342F;
            this.PageHeader.Name = "PageHeader";
            // 
            // labelTitle
            // 
            this.labelTitle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.labelTitle.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTitle.LocationFloat = new DevExpress.Utils.PointFloat(366.6667F, 10F);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.labelTitle.Scripts.OnBeforePrint = "xrLabel20_BeforePrint";
            this.labelTitle.SizeF = new System.Drawing.SizeF(711.3332F, 29.58337F);
            this.labelTitle.StyleName = "SlipHeaderStyle";
            this.labelTitle.StylePriority.UseBackColor = false;
            this.labelTitle.StylePriority.UseFont = false;
            this.labelTitle.StylePriority.UseTextAlignment = false;
            this.labelTitle.Text = "Tax Calculation Details";
            this.labelTitle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // labelCompanyName
            // 
            this.labelCompanyName.BackColor = System.Drawing.Color.Transparent;
            this.labelCompanyName.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCompanyName.ForeColor = System.Drawing.Color.Black;
            this.labelCompanyName.LocationFloat = new DevExpress.Utils.PointFloat(4.666748F, 0.4166667F);
            this.labelCompanyName.Name = "labelCompanyName";
            this.labelCompanyName.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.labelCompanyName.Scripts.OnBeforePrint = "xrLabel20_BeforePrint";
            this.labelCompanyName.SizeF = new System.Drawing.SizeF(348.6682F, 19.58336F);
            this.labelCompanyName.StyleName = "SlipHeaderStyle";
            this.labelCompanyName.StylePriority.UseBackColor = false;
            this.labelCompanyName.StylePriority.UseFont = false;
            this.labelCompanyName.StylePriority.UseForeColor = false;
            this.labelCompanyName.StylePriority.UseTextAlignment = false;
            this.labelCompanyName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLine8
            // 
            this.xrLine8.LineWidth = 2;
            this.xrLine8.LocationFloat = new DevExpress.Utils.PointFloat(3.206787F, 39.58339F);
            this.xrLine8.Name = "xrLine8";
            this.xrLine8.SizeF = new System.Drawing.SizeF(350.1281F, 10.00003F);
            this.xrLine8.StyleName = "LineStyle";
            this.xrLine8.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClearBorderControlEvent);
            // 
            // xrLabel20
            // 
            this.xrLabel20.BackColor = System.Drawing.Color.Transparent;
            this.xrLabel20.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel20.LocationFloat = new DevExpress.Utils.PointFloat(88.00003F, 20.00001F);
            this.xrLabel20.Name = "xrLabel20";
            this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel20.Scripts.OnBeforePrint = "xrLabel20_BeforePrint";
            this.xrLabel20.SizeF = new System.Drawing.SizeF(196.1667F, 19.58336F);
            this.xrLabel20.StyleName = "SlipHeaderStyle";
            this.xrLabel20.StylePriority.UseBackColor = false;
            this.xrLabel20.StylePriority.UseFont = false;
            this.xrLabel20.StylePriority.UseTextAlignment = false;
            this.xrLabel20.Text = "Payment Slip";
            this.xrLabel20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrLabel20.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClearBorderControlEvent);
            // 
            // DateTimeStyle
            // 
            this.DateTimeStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.DateTimeStyle.Name = "DateTimeStyle";
            this.DateTimeStyle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // SlipHeaderStyle
            // 
            this.SlipHeaderStyle.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SlipHeaderStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.SlipHeaderStyle.Name = "SlipHeaderStyle";
            this.SlipHeaderStyle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // InfoStyle
            // 
            this.InfoStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.InfoStyle.BorderColor = System.Drawing.Color.White;
            this.InfoStyle.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.InfoStyle.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InfoStyle.ForeColor = System.Drawing.Color.White;
            this.InfoStyle.Name = "InfoStyle";
            this.InfoStyle.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.InfoStyle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // LineStyle
            // 
            this.LineStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.LineStyle.Name = "LineStyle";
            // 
            // ContentLineStyle
            // 
            this.ContentLineStyle.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.ContentLineStyle.Name = "ContentLineStyle";
            // 
            // formattingRule1
            // 
            this.formattingRule1.Condition = "[NetPay] != 0";
            this.formattingRule1.DataMember = "Report_Pay_PaySlip";
            // 
            // 
            // 
            this.formattingRule1.Formatting.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.formattingRule1.Name = "formattingRule1";
            // 
            // report_Pay_PaySlipDetailTableAdapter
            // 
            this.report_Pay_PaySlipDetailTableAdapter.ClearBeforeFill = true;
            // 
            // report_HR_DateOfJoiningTableAdapter
            // 
            this.report_HR_DateOfJoiningTableAdapter.ClearBeforeFill = true;
            // 
            // ReportPaySlipWithTaxDetails
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.PageHeader});
            this.DataAdapter = this.report_Pay_PaySlipDetailTableAdapter;
            this.DataMember = "Report_Pay_PaySlipDetail";
            this.DataSource = this.reportDataSet1;
            this.FormattingRuleSheet.AddRange(new DevExpress.XtraReports.UI.FormattingRule[] {
            this.formattingRule1});
            this.Margins = new System.Drawing.Printing.Margins(10, 2, 5, 59);
            this.PageHeight = 1169;
            this.PageWidth = 1100;
            this.PaperKind = System.Drawing.Printing.PaperKind.Custom;
            this.Scripts.OnBeforePrint = "ReportPaySlip_BeforePrint";
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.DateTimeStyle,
            this.SlipHeaderStyle,
            this.InfoStyle,
            this.LineStyle,
            this.ContentLineStyle});
            this.Version = "12.1";
            ((System.ComponentModel.ISupportInitialize)(this.reportDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private ReportDataSet reportDataSet1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRSubreport subReportIncome;
        private DevExpress.XtraReports.UI.XRLine xrLine2;
        private ReportPaySlipIncomeDetail reportPaySlipIncomeDetail1;
        private DevExpress.XtraReports.UI.XRSubreport subReportAttendace;
        private ReportPaySlipAttendanceDetail reportPaySlipAttendanceDetail1;
        private DevExpress.XtraReports.UI.XRLine xrLine5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel17;
        private DevExpress.XtraReports.UI.XRPageBreak xrPageBreak1;
        private DevExpress.XtraReports.UI.XRSubreport subReportDeduction;
        private ReportPaySlipDeductionDetail reportPaySlipDeductionDetail1;
        private DevExpress.XtraReports.UI.XRLine xrLine3;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.XRLabel xrLabel20;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel18;
        private DevExpress.XtraReports.UI.XRLine xrLine6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRControlStyle DateTimeStyle;
        private DevExpress.XtraReports.UI.XRControlStyle SlipHeaderStyle;
        private DevExpress.XtraReports.UI.XRControlStyle InfoStyle;
        private DevExpress.XtraReports.UI.XRLine xrLine8;
        private DevExpress.XtraReports.UI.XRControlStyle LineStyle;
        private DevExpress.XtraReports.UI.XRControlStyle ContentLineStyle;
        private DevExpress.XtraReports.UI.XRLine xrLine4;
        private DevExpress.XtraReports.UI.FormattingRule formattingRule1;
        public DevExpress.XtraReports.UI.XRLabel labelCompanyName;
        public DevExpress.XtraReports.UI.XRLabel labelMonth;
        private DevExpress.XtraReports.UI.XRSubreport subReportTaxDetails;
        private ReportDataSetTableAdapters.Report_Pay_PaySlipDetailTableAdapter report_Pay_PaySlipDetailTableAdapter;
        private ReportDataSetTableAdapters.Report_HR_DateOfJoiningTableAdapter report_HR_DateOfJoiningTableAdapter;
        public DevExpress.XtraReports.UI.XRLabel labelTitle;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRLine xrLine7;
        private DevExpress.XtraReports.UI.XRLine xrLine9;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLabel totalLbl;
        private DevExpress.XtraReports.UI.XRLabel xrLabel19;
        private DevExpress.XtraReports.UI.XRPageBreak xrPageBreak2;
    }
}
