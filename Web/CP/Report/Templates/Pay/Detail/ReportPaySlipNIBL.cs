using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using BLL;
using DevExpress.XtraReports.UI;
using Web.ReportDataSetTableAdapters;
using Utils;
using System.Web;

namespace Web.CP.Report.Templates.Pay.Detail
{
    public partial class ReportPaySlipNIBL : DevExpress.XtraReports.UI.XtraReport
    {

        protected void ClearBorderControlEvent(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //ReportHelper.TableCell_BeforePrintEvent(sender, e);
            //((DevExpress.XtraReports.UI.XRControl)sender).ForeColor = System.Drawing.Color.Black;

        }
        protected void ClearBorderLessControlEvent(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //ReportHelper.TableCell_BeforePrintEvent(sender, e);
        }

        public int Month { get; set; }
        public int Year { get; set; }
        public int CompanyId { get; set; }
        public bool IsIncome { get; set; }

        ReportDataSet.Report_Pay_PaySlipNIBLDataTable incomesTable = null;
        ReportDataSet.Report_Pay_PaySlipNIBLDataTable deductionTable = null;

        public ReportPaySlipNIBL()
        {
            InitializeComponent();
        }

        private void xrSubreport1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRSubreport report = (XRSubreport)sender;

            Web.CP.Report.Templates.Pay.Detail.ReportPaySlipNIBL mainReport =
            (Web.CP.Report.Templates.Pay.Detail.ReportPaySlipNIBL)report.Report;

            mainReport.SetSubReport(Convert.ToInt32(GetCurrentColumnValue("EIN")), sender);
        }

        public void SetSubReport(int employeeId, object sender)
        {
            XRSubreport subReport = (XRSubreport)sender;
            object sumObject;

            switch (subReport.Name)
            {
                case "subReportAttendace":

                    Web.CP.Report.Templates.Pay.Detail.ReportPaySlipAttendanceDetail attendanceReport =
                       (Web.CP.Report.Templates.Pay.Detail.ReportPaySlipAttendanceDetail)subReport.ReportSource;

                
                    ReportDataSet.Report_Pay_PaySlip_AttendanceDataTable attendanceTable =
                     HttpContext.Current.Items["PaySlipAttendanceList"] as ReportDataSet.Report_Pay_PaySlip_AttendanceDataTable;

                    if (attendanceTable != null)
                    {
                       


                        attendanceReport.DataSource = attendanceTable;
                        attendanceReport.DataMember = "Attendance Report";

                        
                    }
                    break;
                case "subReportIncome":
                    Web.CP.Report.Templates.Pay.Detail.ReportPaySlipIncomeDetail incomeReport =
                        (Web.CP.Report.Templates.Pay.Detail.ReportPaySlipIncomeDetail)subReport.ReportSource;

                    incomesTable = HttpContext.Current.Items["PaySlipIncomeList"] as ReportDataSet.Report_Pay_PaySlipNIBLDataTable;
                    if (incomesTable != null)
                    {
                        incomesTable = incomesTable.Copy() as ReportDataSet.Report_Pay_PaySlipNIBLDataTable;
                        incomesTable.DefaultView.RowFilter = "IncomeDeductionType = 1";

                        incomeReport.DataSource = incomesTable;
                        incomeReport.DataMember = "Income Report";

                         
                         sumObject = incomesTable.Compute("Sum(Amount)","IncomeDeductionType = 1");

                         if (DBNull.Value.Equals(sumObject))
                             lblGrossEarning.Text = "";
                         else 
                            lblGrossEarning.Text = BLL.BaseBiz.GetCurrency(sumObject.ToString());
                    }

                   
                    break;
                case "subReportDeduction":
                    Web.CP.Report.Templates.Pay.Detail.ReportPaySlipDeductionDetail deductionReport =
                        (Web.CP.Report.Templates.Pay.Detail.ReportPaySlipDeductionDetail)subReport.ReportSource;

                    deductionTable = HttpContext.Current.Items["PaySlipIncomeList"] as ReportDataSet.Report_Pay_PaySlipNIBLDataTable;

                    if (deductionTable != null)
                    {
                        deductionTable = deductionTable.Copy() as ReportDataSet.Report_Pay_PaySlipNIBLDataTable;
                        
                        deductionTable.DefaultView.RowFilter = "IncomeDeductionType = 2";



                        deductionReport.DataSource = deductionTable; 
                        deductionReport.DataMember = "Deduction Report";

                        sumObject = incomesTable.Compute("Sum(Amount)", "IncomeDeductionType = 2");

                        if (DBNull.Value.Equals(sumObject))
                            lblTotalDeductions.Text = "";
                        else 
                            lblTotalDeductions.Text = BLL.BaseBiz.GetCurrency(sumObject.ToString());

                        if (lblGrossEarning.Text != "")
                            lblNetPaid.Text = BLL.BaseBiz.GetCurrency(Convert.ToDecimal(lblGrossEarning.Text) -
                                                Convert.ToDecimal(lblTotalDeductions.Text));
                        else
                            lblNetPaid.Text = "";

                    }
                    break;
                case "subOtherDeductions":
                    Web.CP.Report.Templates.Pay.Detail.ReportPaySlipIncomeDetail incomeReport1 =
                        (Web.CP.Report.Templates.Pay.Detail.ReportPaySlipIncomeDetail)subReport.ReportSource;

                    incomesTable = HttpContext.Current.Items["PaySlipIncomeList"] as ReportDataSet.Report_Pay_PaySlipNIBLDataTable;
                    if (incomesTable != null)
                    {
                        incomesTable = incomesTable.Copy() as ReportDataSet.Report_Pay_PaySlipNIBLDataTable;
                        incomesTable.DefaultView.RowFilter = "IncomeDeductionType = 3";

                        incomeReport1.DataSource = incomesTable; ; 
                        incomeReport1.DataMember = "Income Report";

                        sumObject = incomesTable.Compute("Sum(Amount)", "IncomeDeductionType = 3");

                        if (DBNull.Value.Equals(sumObject))
                            lblOtherDeductions.Text = "";
                        else 
                            lblOtherDeductions.Text = BLL.BaseBiz.GetCurrency(sumObject.ToString());
                    }
                    break;      
            }

            //if (incomesTable != null && deductionTable != null)
            //{
            //    decimal netTotal = (ReportHelper.GetTotal(incomesTable, "Amount") - ReportHelper.GetTotal(deductionTable, "Amount"));
            //    totalLbl.Text = netTotal.ToString("##,###.##");
            //}

        }
       
    }
}
