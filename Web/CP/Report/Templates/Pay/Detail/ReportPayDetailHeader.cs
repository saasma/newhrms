﻿using System;

namespace Web.CP.Report.Templates.Pay.Detail
{
    public partial class ReportPayDetailHeader : DevExpress.XtraReports.UI.XtraReport
    {
        public int EmployeeId { get; set; }

        public ReportPayDetailHeader()
        {
            InitializeComponent();
        }
        protected void ClearBorderControlEvent(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ReportHelper.TableCell_BeforePrintEvent(sender, e);
        }

        protected void ClearBorderLessControlEvent(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ReportHelper.TableCell_BeforePrintEvent(sender, e);
            ((DevExpress.XtraReports.UI.XRControl)sender).Borders = DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right |  DevExpress.XtraPrinting.BorderSide.Bottom;
        }
    }
}
