using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using BLL;
using DevExpress.XtraReports.UI;
using Web.ReportDataSetTableAdapters;
using Utils;
using System.Web;

namespace Web.CP.Report.Templates.Pay.Detail
{
    public partial class ReportPaySlipD2 : DevExpress.XtraReports.UI.XtraReport
    {

        protected void ClearBorderControlEvent(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ReportHelper.TableCell_BeforePrintEvent(sender, e);
            ((DevExpress.XtraReports.UI.XRControl)sender).ForeColor = System.Drawing.Color.Black;


            XRSubreport report = sender as XRSubreport;

            if (report != null)
            {
                Web.CP.Report.Templates.Pay.Detail.ReportPaySlipD2 mainReport =
                (Web.CP.Report.Templates.Pay.Detail.ReportPaySlipD2)report.Report;



                mainReport.SetSubReport(Convert.ToInt32(GetCurrentColumnValue("EIN")), sender);
            }
        }
        protected void ClearBorderLessControlEvent(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ReportHelper.TableCell_BeforePrintEvent(sender, e);
        }

        public int Month { get; set; }
        public int Year { get; set; }
        public int CompanyId { get; set; }
        public bool IsIncome { get; set; }

        ReportDataSet.Report_Pay_PaySlipDetail_EmpHeaderDataTable incomesTable = null;
        ReportDataSet.Report_Pay_PaySlipDetail_EmpHeaderDataTable deductionTable = null;

        public ReportPaySlipD2()
        {
            InitializeComponent();
        }

        public void SetSubReport(int employeeId, object sender)
        {
            XRSubreport subReport = (XRSubreport)sender;          

            switch (subReport.Name)
            {
               
                case "subReportIncome":
                    Web.CP.Report.Templates.Pay.Detail.ReportPayDetailD2 incomeReport =
                        (Web.CP.Report.Templates.Pay.Detail.ReportPayDetailD2)subReport.ReportSource;

                    incomesTable = HttpContext.Current.Items["PaySlipIncomeList"] as 
                        ReportDataSet.Report_Pay_PaySlipDetail_EmpHeaderDataTable;
                    if (incomesTable != null)
                    {
                        incomesTable.DefaultView.RowFilter = "EIN = " + employeeId;

                        incomeReport.DataSource = incomesTable; 
                        incomeReport.DataMember = "Income Report";
                    }
                    break;
                           
            }

          

        }
       
    }
}
