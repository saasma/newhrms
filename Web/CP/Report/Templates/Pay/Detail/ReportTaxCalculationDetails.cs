using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using BLL;
using DevExpress.XtraReports.UI;
using Web.ReportDataSetTableAdapters;
using Utils;
using System.Web;

namespace Web.CP.Report.Templates.Pay.Detail
{
    public partial class ReportTaxCalculationDetails : DevExpress.XtraReports.UI.XtraReport
    {

        protected void ClearBorderControlEvent(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ReportHelper.TableCell_BeforePrintEvent(sender, e);
            ((DevExpress.XtraReports.UI.XRControl)sender).ForeColor = System.Drawing.Color.Black;

        }
        protected void ClearBorderLessControlEvent(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ReportHelper.TableCell_BeforePrintEvent(sender, e);
        }

        public int Month { get; set; }
        public int Year { get; set; }
        public int CompanyId { get; set; }
        public bool IsIncome { get; set; }

        ReportDataSet.Report_Pay_PaySlipDetail_EmpHeaderDataTable incomesTable = null;
        ReportDataSet.Report_Pay_PaySlipDetail_EmpHeaderDataTable deductionTable = null;

        public ReportTaxCalculationDetails()
        {
            InitializeComponent();
        }

       
       
    }
}
