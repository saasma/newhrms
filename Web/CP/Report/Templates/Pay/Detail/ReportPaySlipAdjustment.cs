using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using BLL;
using DevExpress.XtraReports.UI;
using Web.ReportDataSetTableAdapters;
using Utils;
using System.Web;

namespace Web.CP.Report.Templates.Pay.Detail
{
    public partial class ReportPaySlipAdjustment : DevExpress.XtraReports.UI.XtraReport
    {

        protected void ClearBorderControlEvent(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ReportHelper.TableCell_BeforePrintEvent(sender, e);
            ((DevExpress.XtraReports.UI.XRControl)sender).ForeColor = System.Drawing.Color.Black;

        }
        protected void ClearBorderLessControlEvent(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ReportHelper.TableCell_BeforePrintEvent(sender, e);
        }

        public int Month { get; set; }
        public int Year { get; set; }
        public int CompanyId { get; set; }
        public bool IsIncome { get; set; }

        ReportDataSet.Report_Pay_PaySlipDetail_EmpHeaderDataTable incomesTable = null;
        ReportDataSet.Report_Pay_PaySlipDetail_EmpHeaderDataTable deductionTable = null;

        public ReportPaySlipAdjustment()
        {
            InitializeComponent();
        }

        private void xrSubreport1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRSubreport report = (XRSubreport)sender;

            Web.CP.Report.Templates.Pay.Detail.ReportPaySlipAdjustment mainReport =
            (Web.CP.Report.Templates.Pay.Detail.ReportPaySlipAdjustment)report.Report;

            mainReport.SetSubReport(Convert.ToInt32(GetCurrentColumnValue("EIN")), sender);
        }

        public void SetSubReport(int employeeId, object sender)
        {
            XRSubreport subReport = (XRSubreport)sender;          

            switch (subReport.Name)
            {
                //case "subReportAttendace":

                //    Web.CP.Report.Templates.Pay.Detail.ReportPaySlipAttendanceDetail attendanceReport =
                //       (Web.CP.Report.Templates.Pay.Detail.ReportPaySlipAttendanceDetail)subReport.ReportSource;

                
                //    ReportDataSet.Report_Pay_PaySlip_AttendanceDataTable attendanceTable =
                //     HttpContext.Current.Items["PaySlipAttendanceList"] as ReportDataSet.Report_Pay_PaySlip_AttendanceDataTable;

                //    if (attendanceTable != null)
                //    {
                //        attendanceTable.DefaultView.RowFilter = "EmployeeId = " + employeeId;



                //        attendanceReport.DataSource = attendanceTable;
                //        attendanceReport.DataMember = "Attendance Report";
                //    }
                //    break;
                case "subReportIncome":
                    Web.CP.Report.Templates.Pay.Detail.ReportPaySlipIncomeAdjustmentDetail incomeReport =
                        (Web.CP.Report.Templates.Pay.Detail.ReportPaySlipIncomeAdjustmentDetail)subReport.ReportSource;

                    incomesTable = HttpContext.Current.Items["PaySlipIncomeList"] as ReportDataSet.Report_Pay_PaySlipDetail_EmpHeaderDataTable;
                    if (incomesTable != null)
                    {
                        incomesTable.DefaultView.RowFilter = "EIN = " + employeeId;

                        incomeReport.DataSource = incomesTable; 
                        incomeReport.DataMember = "Income Report";
                    }
                    break;
                case "subReportDeduction":
                    Web.CP.Report.Templates.Pay.Detail.ReportPaySlipDeductionDetail deductionReport =
                        (Web.CP.Report.Templates.Pay.Detail.ReportPaySlipDeductionDetail)subReport.ReportSource;

                    deductionTable = HttpContext.Current.Items["PaySlipDeductionList"] as ReportDataSet.Report_Pay_PaySlipDetail_EmpHeaderDataTable;

                    if (deductionTable != null)
                    {
                        deductionTable.DefaultView.RowFilter = "EIN = " + employeeId;



                        deductionReport.DataSource = deductionTable;
                        deductionReport.DataMember = "Deduction Report";
                    }
                    break;                   
            }

            //if (incomesTable != null && deductionTable != null)
            //{
            //    decimal netTotal = (ReportHelper.GetTotal(incomesTable, "Amount") - ReportHelper.GetTotal(deductionTable, "Amount"));
            //    totalLbl.Text = netTotal.ToString("##,###.##");
            //}

        }
       
    }
}
