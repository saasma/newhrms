namespace Web.CP.Report.Templates.Pay.Detail
{
    partial class ReportPaySlipNIBL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrLine11 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine10 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine9 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine7 = new DevExpress.XtraReports.UI.XRLine();
            this.lblOtherDeductions = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.subOtherDeductions = new DevExpress.XtraReports.UI.XRSubreport();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNetPaid = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblGrossEarning = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine4 = new DevExpress.XtraReports.UI.XRLine();
            this.lblEIN = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTotalDeductions = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine6 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine5 = new DevExpress.XtraReports.UI.XRLine();
            this.subReportAttendace = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportDeduction = new DevExpress.XtraReports.UI.XRSubreport();
            this.xrLine3 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
            this.subReportIncome = new DevExpress.XtraReports.UI.XRSubreport();
            this.labelMonth = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDep = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblName = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDesig = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.reportDataSet1 = new Web.ReportDataSet();
            this.report_HR_DateOfJoiningTableAdapter = new Web.ReportDataSetTableAdapters.Report_HR_DateOfJoiningTableAdapter();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.logo = new DevExpress.XtraReports.UI.XRPictureBox();
            this.labelCompanyName = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine8 = new DevExpress.XtraReports.UI.XRLine();
            this.labelTItle = new DevExpress.XtraReports.UI.XRLabel();
            this.DateTimeStyle = new DevExpress.XtraReports.UI.XRControlStyle();
            this.SlipHeaderStyle = new DevExpress.XtraReports.UI.XRControlStyle();
            this.InfoStyle = new DevExpress.XtraReports.UI.XRControlStyle();
            this.LineStyle = new DevExpress.XtraReports.UI.XRControlStyle();
            this.ContentLineStyle = new DevExpress.XtraReports.UI.XRControlStyle();
            this.formattingRule1 = new DevExpress.XtraReports.UI.FormattingRule();
            this.report_HR_GetAttendanceDaysTableAdapter = new Web.ReportDataSetTableAdapters.Report_HR_GetAttendanceDaysTableAdapter();
            this.report_Pay_PaySlipDetailTableAdapter = new Web.ReportDataSetTableAdapters.Report_Pay_PaySlipDetailTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.reportDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine11,
            this.xrLine10,
            this.xrLine9,
            this.xrLine7,
            this.lblOtherDeductions,
            this.xrLabel14,
            this.subOtherDeductions,
            this.xrLabel13,
            this.lblNetPaid,
            this.xrLabel12,
            this.lblGrossEarning,
            this.xrLabel11,
            this.xrLine4,
            this.lblEIN,
            this.xrLabel6,
            this.xrLabel3,
            this.xrLabel19,
            this.lblTotalDeductions,
            this.xrLabel18,
            this.xrLine6,
            this.xrLabel17,
            this.xrLine5,
            this.subReportAttendace,
            this.subReportDeduction,
            this.xrLine3,
            this.xrLine2,
            this.subReportIncome,
            this.labelMonth,
            this.lblDep,
            this.xrLabel10,
            this.xrLabel4,
            this.xrLabel5,
            this.lblName,
            this.lblDesig,
            this.xrLabel1,
            this.xrLine1});
            this.Detail.HeightF = 507.7083F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.StylePriority.UseBorders = false;
            this.Detail.StylePriority.UseBorderWidth = false;
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine11
            // 
            this.xrLine11.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.xrLine11.LocationFloat = new DevExpress.Utils.PointFloat(3.539352F, 453.7275F);
            this.xrLine11.Name = "xrLine11";
            this.xrLine11.SizeF = new System.Drawing.SizeF(347F, 5.209991F);
            this.xrLine11.StylePriority.UseBorders = false;
            this.xrLine11.StylePriority.UseForeColor = false;
            // 
            // xrLine10
            // 
            this.xrLine10.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.xrLine10.LocationFloat = new DevExpress.Utils.PointFloat(3.539352F, 393.0483F);
            this.xrLine10.Name = "xrLine10";
            this.xrLine10.SizeF = new System.Drawing.SizeF(347F, 5.209991F);
            this.xrLine10.StylePriority.UseBorders = false;
            this.xrLine10.StylePriority.UseForeColor = false;
            // 
            // xrLine9
            // 
            this.xrLine9.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.xrLine9.LocationFloat = new DevExpress.Utils.PointFloat(3.539556F, 364.8383F);
            this.xrLine9.Name = "xrLine9";
            this.xrLine9.SizeF = new System.Drawing.SizeF(347F, 5.209991F);
            this.xrLine9.StylePriority.UseBorders = false;
            this.xrLine9.StylePriority.UseForeColor = false;
            // 
            // xrLine7
            // 
            this.xrLine7.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.xrLine7.LocationFloat = new DevExpress.Utils.PointFloat(3.541463F, 275.9183F);
            this.xrLine7.Name = "xrLine7";
            this.xrLine7.SizeF = new System.Drawing.SizeF(347F, 5.209991F);
            this.xrLine7.StylePriority.UseBorders = false;
            this.xrLine7.StylePriority.UseForeColor = false;
            // 
            // lblOtherDeductions
            // 
            this.lblOtherDeductions.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblOtherDeductions.Font = new System.Drawing.Font("Tahoma", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOtherDeductions.LocationFloat = new DevExpress.Utils.PointFloat(211.9145F, 458.9375F);
            this.lblOtherDeductions.Name = "lblOtherDeductions";
            this.lblOtherDeductions.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblOtherDeductions.SizeF = new System.Drawing.SizeF(131.7916F, 22.99997F);
            this.lblOtherDeductions.StylePriority.UseBorders = false;
            this.lblOtherDeductions.StylePriority.UseFont = false;
            this.lblOtherDeductions.StylePriority.UseTextAlignment = false;
            this.lblOtherDeductions.Text = "lblTotalDeductions";
            this.lblOtherDeductions.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel14
            // 
            this.xrLabel14.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel14.Font = new System.Drawing.Font("Times New Roman", 10.75F, System.Drawing.FontStyle.Bold);
            this.xrLabel14.ForeColor = System.Drawing.Color.Black;
            this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(9.997685F, 458.9375F);
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrLabel14.SizeF = new System.Drawing.SizeF(201.9168F, 23.00006F);
            this.xrLabel14.StyleName = "SlipHeaderStyle";
            this.xrLabel14.StylePriority.UseBorders = false;
            this.xrLabel14.StylePriority.UseFont = false;
            this.xrLabel14.StylePriority.UseForeColor = false;
            this.xrLabel14.StylePriority.UsePadding = false;
            this.xrLabel14.StylePriority.UseTextAlignment = false;
            this.xrLabel14.Text = "Total Other Deductions:";
            this.xrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // subOtherDeductions
            // 
            this.subOtherDeductions.LocationFloat = new DevExpress.Utils.PointFloat(8.541946F, 425.8334F);
            this.subOtherDeductions.Name = "subOtherDeductions";
            this.subOtherDeductions.ReportSource = new Web.CP.Report.Templates.Pay.Detail.ReportPaySlipIncomeDetail();
            this.subOtherDeductions.Scripts.OnBeforePrint = "xrSubreport1_BeforePrint";
            this.subOtherDeductions.SizeF = new System.Drawing.SizeF(342F, 22.99998F);
            this.subOtherDeductions.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrSubreport1_BeforePrint);
            // 
            // xrLabel13
            // 
            this.xrLabel13.BackColor = System.Drawing.Color.Transparent;
            this.xrLabel13.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel13.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(9.999974F, 402.8333F);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel13.SizeF = new System.Drawing.SizeF(201.9145F, 23.00003F);
            this.xrLabel13.StyleName = "SlipHeaderStyle";
            this.xrLabel13.StylePriority.UseBackColor = false;
            this.xrLabel13.StylePriority.UseBorders = false;
            this.xrLabel13.StylePriority.UseTextAlignment = false;
            this.xrLabel13.Text = "Other Deductions";
            this.xrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblNetPaid
            // 
            this.lblNetPaid.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblNetPaid.Font = new System.Drawing.Font("Tahoma", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNetPaid.LocationFloat = new DevExpress.Utils.PointFloat(211.9145F, 370.0483F);
            this.lblNetPaid.Name = "lblNetPaid";
            this.lblNetPaid.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNetPaid.SizeF = new System.Drawing.SizeF(131.7916F, 23.00003F);
            this.lblNetPaid.StylePriority.UseBorders = false;
            this.lblNetPaid.StylePriority.UseFont = false;
            this.lblNetPaid.StylePriority.UseTextAlignment = false;
            this.lblNetPaid.Text = "lblTotalDeductions";
            this.lblNetPaid.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel12
            // 
            this.xrLabel12.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel12.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel12.ForeColor = System.Drawing.Color.Black;
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(9.99789F, 370.0483F);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(201.9166F, 23.00003F);
            this.xrLabel12.StyleName = "SlipHeaderStyle";
            this.xrLabel12.StylePriority.UseBorders = false;
            this.xrLabel12.StylePriority.UseForeColor = false;
            this.xrLabel12.StylePriority.UseTextAlignment = false;
            this.xrLabel12.Text = "Net Paid:";
            this.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblGrossEarning
            // 
            this.lblGrossEarning.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblGrossEarning.Font = new System.Drawing.Font("Tahoma", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGrossEarning.LocationFloat = new DevExpress.Utils.PointFloat(211.9145F, 252.9183F);
            this.lblGrossEarning.Name = "lblGrossEarning";
            this.lblGrossEarning.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblGrossEarning.SizeF = new System.Drawing.SizeF(131.7916F, 23.00005F);
            this.lblGrossEarning.StylePriority.UseBorders = false;
            this.lblGrossEarning.StylePriority.UseFont = false;
            this.lblGrossEarning.StylePriority.UseTextAlignment = false;
            this.lblGrossEarning.Text = "totalLbl";
            this.lblGrossEarning.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel11
            // 
            this.xrLabel11.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel11.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel11.ForeColor = System.Drawing.Color.Black;
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(8.54009F, 252.9183F);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(203.3744F, 23.00005F);
            this.xrLabel11.StyleName = "SlipHeaderStyle";
            this.xrLabel11.StylePriority.UseBorders = false;
            this.xrLabel11.StylePriority.UseForeColor = false;
            this.xrLabel11.StylePriority.UseTextAlignment = false;
            this.xrLabel11.Text = "Gross Earnings:";
            this.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLine4
            // 
            this.xrLine4.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.xrLine4.LocationFloat = new DevExpress.Utils.PointFloat(3.541743F, 160.3684F);
            this.xrLine4.Name = "xrLine4";
            this.xrLine4.SizeF = new System.Drawing.SizeF(347F, 5.209991F);
            this.xrLine4.StylePriority.UseBorders = false;
            this.xrLine4.StylePriority.UseForeColor = false;
            this.xrLine4.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClearBorderControlEvent);
            // 
            // lblEIN
            // 
            this.lblEIN.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblEIN.Font = new System.Drawing.Font("Tahoma", 9F);
            this.lblEIN.LocationFloat = new DevExpress.Utils.PointFloat(125.7085F, 56.00001F);
            this.lblEIN.Name = "lblEIN";
            this.lblEIN.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblEIN.SizeF = new System.Drawing.SizeF(226.2916F, 23.00002F);
            this.lblEIN.StylePriority.UseBorders = false;
            this.lblEIN.StylePriority.UseFont = false;
            this.lblEIN.StylePriority.UseTextAlignment = false;
            this.lblEIN.Text = "lblEIN";
            this.lblEIN.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel6
            // 
            this.xrLabel6.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(8.54009F, 56.00001F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(100.0016F, 23.00002F);
            this.xrLabel6.StyleName = "InfoStyle";
            this.xrLabel6.Text = "EMP Code :";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrLabel6.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClearBorderControlEvent);
            // 
            // xrLabel3
            // 
            this.xrLabel3.BackColor = System.Drawing.Color.Transparent;
            this.xrLabel3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(8.541718F, 137.3684F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(203.3727F, 23F);
            this.xrLabel3.StyleName = "SlipHeaderStyle";
            this.xrLabel3.StylePriority.UseBackColor = false;
            this.xrLabel3.StylePriority.UseBorders = false;
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.Text = "Attendance";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrLabel3.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClearBorderControlEvent);
            // 
            // xrLabel19
            // 
            this.xrLabel19.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel19.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel19.ForeColor = System.Drawing.Color.Black;
            this.xrLabel19.LocationFloat = new DevExpress.Utils.PointFloat(8.537979F, 341.8383F);
            this.xrLabel19.Name = "xrLabel19";
            this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel19.SizeF = new System.Drawing.SizeF(203.3765F, 23.00003F);
            this.xrLabel19.StyleName = "SlipHeaderStyle";
            this.xrLabel19.StylePriority.UseBorders = false;
            this.xrLabel19.StylePriority.UseForeColor = false;
            this.xrLabel19.StylePriority.UseTextAlignment = false;
            this.xrLabel19.Text = "Total Deductions:";
            this.xrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblTotalDeductions
            // 
            this.lblTotalDeductions.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblTotalDeductions.Font = new System.Drawing.Font("Tahoma", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalDeductions.LocationFloat = new DevExpress.Utils.PointFloat(211.9145F, 341.8383F);
            this.lblTotalDeductions.Name = "lblTotalDeductions";
            this.lblTotalDeductions.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTotalDeductions.SizeF = new System.Drawing.SizeF(131.7916F, 23.00003F);
            this.lblTotalDeductions.StylePriority.UseBorders = false;
            this.lblTotalDeductions.StylePriority.UseFont = false;
            this.lblTotalDeductions.StylePriority.UseTextAlignment = false;
            this.lblTotalDeductions.Text = "lblTotalDeductions";
            this.lblTotalDeductions.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel18
            // 
            this.xrLabel18.BackColor = System.Drawing.Color.Transparent;
            this.xrLabel18.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel18.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel18.LocationFloat = new DevExpress.Utils.PointFloat(8.541718F, 195.5001F);
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel18.SizeF = new System.Drawing.SizeF(203.3727F, 23F);
            this.xrLabel18.StyleName = "SlipHeaderStyle";
            this.xrLabel18.StylePriority.UseBackColor = false;
            this.xrLabel18.StylePriority.UseBorders = false;
            this.xrLabel18.StylePriority.UseTextAlignment = false;
            this.xrLabel18.Text = "Earnings";
            this.xrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrLabel18.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClearBorderControlEvent);
            // 
            // xrLine6
            // 
            this.xrLine6.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.xrLine6.LocationFloat = new DevExpress.Utils.PointFloat(3.541743F, 219.4983F);
            this.xrLine6.Name = "xrLine6";
            this.xrLine6.SizeF = new System.Drawing.SizeF(347F, 5.210022F);
            this.xrLine6.StylePriority.UseBorders = false;
            this.xrLine6.StylePriority.UseForeColor = false;
            this.xrLine6.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClearBorderControlEvent);
            // 
            // xrLabel17
            // 
            this.xrLabel17.BackColor = System.Drawing.Color.Transparent;
            this.xrLabel17.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel17.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(8.54009F, 281.1283F);
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel17.SizeF = new System.Drawing.SizeF(203.3744F, 23F);
            this.xrLabel17.StyleName = "SlipHeaderStyle";
            this.xrLabel17.StylePriority.UseBackColor = false;
            this.xrLabel17.StylePriority.UseBorders = false;
            this.xrLabel17.StylePriority.UseTextAlignment = false;
            this.xrLabel17.Text = "Deductions";
            this.xrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrLabel17.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClearBorderControlEvent);
            // 
            // xrLine5
            // 
            this.xrLine5.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.xrLine5.LocationFloat = new DevExpress.Utils.PointFloat(3.541743F, 247.7083F);
            this.xrLine5.Name = "xrLine5";
            this.xrLine5.SizeF = new System.Drawing.SizeF(347F, 5.209991F);
            this.xrLine5.StylePriority.UseBorders = false;
            this.xrLine5.StylePriority.UseForeColor = false;
            this.xrLine5.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClearBorderControlEvent);
            // 
            // subReportAttendace
            // 
            this.subReportAttendace.LocationFloat = new DevExpress.Utils.PointFloat(8.541718F, 166.625F);
            this.subReportAttendace.Name = "subReportAttendace";
            this.subReportAttendace.ReportSource = new Web.CP.Report.Templates.Pay.Detail.ReportPaySlipAttendanceDetail();
            this.subReportAttendace.Scripts.OnBeforePrint = "xrSubreport1_BeforePrint";
            this.subReportAttendace.SizeF = new System.Drawing.SizeF(342F, 22.99995F);
            this.subReportAttendace.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrSubreport1_BeforePrint);
            // 
            // subReportDeduction
            // 
            this.subReportDeduction.LocationFloat = new DevExpress.Utils.PointFloat(8.53803F, 309.3383F);
            this.subReportDeduction.Name = "subReportDeduction";
            this.subReportDeduction.ReportSource = new Web.CP.Report.Templates.Pay.Detail.ReportPaySlipDeductionDetail();
            this.subReportDeduction.Scripts.OnBeforePrint = "xrSubreport1_BeforePrint";
            this.subReportDeduction.SizeF = new System.Drawing.SizeF(342.0016F, 22.99997F);
            this.subReportDeduction.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrSubreport1_BeforePrint);
            // 
            // xrLine3
            // 
            this.xrLine3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.xrLine3.LocationFloat = new DevExpress.Utils.PointFloat(3.539378F, 334.1283F);
            this.xrLine3.Name = "xrLine3";
            this.xrLine3.SizeF = new System.Drawing.SizeF(347.0002F, 5.209991F);
            this.xrLine3.StylePriority.UseBorders = false;
            this.xrLine3.StylePriority.UseForeColor = false;
            this.xrLine3.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClearBorderControlEvent);
            // 
            // xrLine2
            // 
            this.xrLine2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.xrLine2.LocationFloat = new DevExpress.Utils.PointFloat(3.541743F, 189.6283F);
            this.xrLine2.Name = "xrLine2";
            this.xrLine2.SizeF = new System.Drawing.SizeF(347.0002F, 5.210022F);
            this.xrLine2.StylePriority.UseBorders = false;
            this.xrLine2.StylePriority.UseForeColor = false;
            this.xrLine2.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClearBorderControlEvent);
            // 
            // subReportIncome
            // 
            this.subReportIncome.LocationFloat = new DevExpress.Utils.PointFloat(8.541718F, 224.7083F);
            this.subReportIncome.Name = "subReportIncome";
            this.subReportIncome.ReportSource = new Web.CP.Report.Templates.Pay.Detail.ReportPaySlipIncomeDetail();
            this.subReportIncome.Scripts.OnBeforePrint = "xrSubreport1_BeforePrint";
            this.subReportIncome.SizeF = new System.Drawing.SizeF(342F, 22.99998F);
            this.subReportIncome.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrSubreport1_BeforePrint);
            // 
            // labelMonth
            // 
            this.labelMonth.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.labelMonth.Font = new System.Drawing.Font("Tahoma", 9F);
            this.labelMonth.LocationFloat = new DevExpress.Utils.PointFloat(125.7085F, 10F);
            this.labelMonth.Name = "labelMonth";
            this.labelMonth.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.labelMonth.SizeF = new System.Drawing.SizeF(226.2915F, 23F);
            this.labelMonth.StylePriority.UseBorders = false;
            this.labelMonth.StylePriority.UseFont = false;
            this.labelMonth.StylePriority.UseTextAlignment = false;
            this.labelMonth.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblDep
            // 
            this.lblDep.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblDep.Font = new System.Drawing.Font("Tahoma", 9F);
            this.lblDep.LocationFloat = new DevExpress.Utils.PointFloat(125.7085F, 79.00004F);
            this.lblDep.Name = "lblDep";
            this.lblDep.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDep.SizeF = new System.Drawing.SizeF(226.2916F, 23.00001F);
            this.lblDep.StylePriority.UseBorders = false;
            this.lblDep.StylePriority.UseFont = false;
            this.lblDep.StylePriority.UseTextAlignment = false;
            this.lblDep.Text = "lblDep";
            this.lblDep.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblDep.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClearBorderControlEvent);
            // 
            // xrLabel10
            // 
            this.xrLabel10.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(8.54009F, 10F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(100.0016F, 23F);
            this.xrLabel10.StyleName = "InfoStyle";
            this.xrLabel10.Text = "Month :";
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrLabel10.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClearBorderControlEvent);
            // 
            // xrLabel4
            // 
            this.xrLabel4.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(8.54009F, 79.00004F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(100.0016F, 23.00002F);
            this.xrLabel4.StyleName = "InfoStyle";
            this.xrLabel4.Text = "Department :";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrLabel4.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClearBorderControlEvent);
            // 
            // xrLabel5
            // 
            this.xrLabel5.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(8.54009F, 101.9999F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(100.0016F, 23.00003F);
            this.xrLabel5.StyleName = "InfoStyle";
            this.xrLabel5.Text = "Designation :";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrLabel5.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClearBorderControlEvent);
            // 
            // lblName
            // 
            this.lblName.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblName.Font = new System.Drawing.Font("Tahoma", 9F);
            this.lblName.LocationFloat = new DevExpress.Utils.PointFloat(125.7085F, 32.99998F);
            this.lblName.Name = "lblName";
            this.lblName.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblName.SizeF = new System.Drawing.SizeF(226.2931F, 23F);
            this.lblName.StylePriority.UseBorders = false;
            this.lblName.StylePriority.UseFont = false;
            this.lblName.StylePriority.UseTextAlignment = false;
            this.lblName.Text = "lblName";
            this.lblName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblDesig
            // 
            this.lblDesig.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblDesig.Font = new System.Drawing.Font("Tahoma", 9F);
            this.lblDesig.LocationFloat = new DevExpress.Utils.PointFloat(125.7085F, 102F);
            this.lblDesig.Name = "lblDesig";
            this.lblDesig.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDesig.SizeF = new System.Drawing.SizeF(226.2915F, 22.99998F);
            this.lblDesig.StylePriority.UseBorders = false;
            this.lblDesig.StylePriority.UseFont = false;
            this.lblDesig.StylePriority.UseTextAlignment = false;
            this.lblDesig.Text = "lblDesig";
            this.lblDesig.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblDesig.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClearBorderControlEvent);
            // 
            // xrLabel1
            // 
            this.xrLabel1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(8.54009F, 32.99998F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(100.0016F, 23F);
            this.xrLabel1.StyleName = "InfoStyle";
            this.xrLabel1.Text = "Name :";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrLabel1.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClearBorderControlEvent);
            // 
            // xrLine1
            // 
            this.xrLine1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(3.541743F, 304.1284F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(347.0002F, 5.209991F);
            this.xrLine1.StylePriority.UseBorders = false;
            this.xrLine1.StylePriority.UseForeColor = false;
            this.xrLine1.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClearBorderControlEvent);
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 13.33333F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 15F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.Scripts.OnBeforePrint = "BottomMargin_BeforePrint";
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // reportDataSet1
            // 
            this.reportDataSet1.DataSetName = "ReportDataSet";
            this.reportDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // report_HR_DateOfJoiningTableAdapter
            // 
            this.report_HR_DateOfJoiningTableAdapter.ClearBeforeFill = true;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.logo,
            this.labelCompanyName,
            this.xrLine8,
            this.labelTItle});
            this.PageHeader.HeightF = 118.3334F;
            this.PageHeader.Name = "PageHeader";
            // 
            // logo
            // 
            this.logo.LocationFloat = new DevExpress.Utils.PointFloat(80F, 0F);
            this.logo.Name = "logo";
            this.logo.SizeF = new System.Drawing.SizeF(196.1667F, 55F);
            this.logo.Sizing = DevExpress.XtraPrinting.ImageSizeMode.CenterImage;
            this.logo.Visible = false;
            // 
            // labelCompanyName
            // 
            this.labelCompanyName.BackColor = System.Drawing.Color.Transparent;
            this.labelCompanyName.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCompanyName.ForeColor = System.Drawing.Color.Black;
            this.labelCompanyName.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 61.16666F);
            this.labelCompanyName.Name = "labelCompanyName";
            this.labelCompanyName.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.labelCompanyName.Scripts.OnBeforePrint = "xrLabel20_BeforePrint";
            this.labelCompanyName.SizeF = new System.Drawing.SizeF(342.0016F, 19.58336F);
            this.labelCompanyName.StyleName = "SlipHeaderStyle";
            this.labelCompanyName.StylePriority.UseBackColor = false;
            this.labelCompanyName.StylePriority.UseFont = false;
            this.labelCompanyName.StylePriority.UseForeColor = false;
            this.labelCompanyName.StylePriority.UseTextAlignment = false;
            this.labelCompanyName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLine8
            // 
            this.xrLine8.LineWidth = 2;
            this.xrLine8.LocationFloat = new DevExpress.Utils.PointFloat(0F, 108.3334F);
            this.xrLine8.Name = "xrLine8";
            this.xrLine8.SizeF = new System.Drawing.SizeF(362F, 10.00003F);
            this.xrLine8.StyleName = "LineStyle";
            this.xrLine8.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClearBorderControlEvent);
            // 
            // labelTItle
            // 
            this.labelTItle.BackColor = System.Drawing.Color.Transparent;
            this.labelTItle.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTItle.LocationFloat = new DevExpress.Utils.PointFloat(0F, 88.75002F);
            this.labelTItle.Name = "labelTItle";
            this.labelTItle.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.labelTItle.Scripts.OnBeforePrint = "xrLabel20_BeforePrint";
            this.labelTItle.SizeF = new System.Drawing.SizeF(362F, 19.58336F);
            this.labelTItle.StyleName = "SlipHeaderStyle";
            this.labelTItle.StylePriority.UseBackColor = false;
            this.labelTItle.StylePriority.UseFont = false;
            this.labelTItle.StylePriority.UseTextAlignment = false;
            this.labelTItle.Text = "Payment Slip";
            this.labelTItle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.labelTItle.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClearBorderControlEvent);
            // 
            // DateTimeStyle
            // 
            this.DateTimeStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.DateTimeStyle.Name = "DateTimeStyle";
            this.DateTimeStyle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // SlipHeaderStyle
            // 
            this.SlipHeaderStyle.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SlipHeaderStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.SlipHeaderStyle.Name = "SlipHeaderStyle";
            this.SlipHeaderStyle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // InfoStyle
            // 
            this.InfoStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.InfoStyle.BorderColor = System.Drawing.Color.White;
            this.InfoStyle.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.InfoStyle.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InfoStyle.ForeColor = System.Drawing.Color.White;
            this.InfoStyle.Name = "InfoStyle";
            this.InfoStyle.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.InfoStyle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // LineStyle
            // 
            this.LineStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.LineStyle.Name = "LineStyle";
            // 
            // ContentLineStyle
            // 
            this.ContentLineStyle.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.ContentLineStyle.Name = "ContentLineStyle";
            // 
            // formattingRule1
            // 
            this.formattingRule1.Condition = "[NetPay] != 0";
            this.formattingRule1.DataMember = "Report_Pay_PaySlip";
            // 
            // 
            // 
            this.formattingRule1.Formatting.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.formattingRule1.Name = "formattingRule1";
            // 
            // report_HR_GetAttendanceDaysTableAdapter
            // 
            this.report_HR_GetAttendanceDaysTableAdapter.ClearBeforeFill = true;
            // 
            // report_Pay_PaySlipDetailTableAdapter
            // 
            this.report_Pay_PaySlipDetailTableAdapter.ClearBeforeFill = true;
            // 
            // ReportPaySlipNIBL
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.PageHeader});
            this.DataAdapter = this.report_HR_DateOfJoiningTableAdapter;
            this.DataMember = "Report_HR_DateOfJoining";
            this.DataSource = this.reportDataSet1;
            this.FormattingRuleSheet.AddRange(new DevExpress.XtraReports.UI.FormattingRule[] {
            this.formattingRule1});
            this.Margins = new System.Drawing.Printing.Margins(24, 27, 13, 15);
            this.PageHeight = 1169;
            this.PageWidth = 413;
            this.PaperKind = System.Drawing.Printing.PaperKind.Custom;
            this.Scripts.OnBeforePrint = "ReportPaySlip_BeforePrint";
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.DateTimeStyle,
            this.SlipHeaderStyle,
            this.InfoStyle,
            this.LineStyle,
            this.ContentLineStyle});
            this.Version = "12.1";
            ((System.ComponentModel.ISupportInitialize)(this.reportDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private ReportDataSet reportDataSet1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRSubreport subReportIncome;
        private DevExpress.XtraReports.UI.XRLine xrLine2;
        private ReportPaySlipIncomeDetail reportPaySlipIncomeDetail1;
        private DevExpress.XtraReports.UI.XRSubreport subReportAttendace;
        private ReportPaySlipAttendanceDetail reportPaySlipAttendanceDetail1;
        private DevExpress.XtraReports.UI.XRLine xrLine5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel17;
        private Web.ReportDataSetTableAdapters.Report_HR_DateOfJoiningTableAdapter report_HR_DateOfJoiningTableAdapter;
        private DevExpress.XtraReports.UI.XRLabel xrLabel19;
        private DevExpress.XtraReports.UI.XRSubreport subReportDeduction;
        private ReportPaySlipDeductionDetail reportPaySlipDeductionDetail1;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel18;
        private DevExpress.XtraReports.UI.XRLine xrLine6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRControlStyle DateTimeStyle;
        private DevExpress.XtraReports.UI.XRControlStyle SlipHeaderStyle;
        private DevExpress.XtraReports.UI.XRControlStyle InfoStyle;
        private DevExpress.XtraReports.UI.XRLine xrLine8;
        private DevExpress.XtraReports.UI.XRControlStyle LineStyle;
        private DevExpress.XtraReports.UI.XRControlStyle ContentLineStyle;
        private DevExpress.XtraReports.UI.XRLine xrLine4;
        private DevExpress.XtraReports.UI.FormattingRule formattingRule1;
        private ReportDataSetTableAdapters.Report_HR_GetAttendanceDaysTableAdapter report_HR_GetAttendanceDaysTableAdapter;
        public DevExpress.XtraReports.UI.XRLabel labelCompanyName;
        public DevExpress.XtraReports.UI.XRPictureBox logo;
        public DevExpress.XtraReports.UI.XRLabel labelMonth;
        private ReportDataSetTableAdapters.Report_Pay_PaySlipDetailTableAdapter report_Pay_PaySlipDetailTableAdapter;
        public DevExpress.XtraReports.UI.XRLabel labelTItle;
        public DevExpress.XtraReports.UI.XRLabel lblGrossEarning;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        public DevExpress.XtraReports.UI.XRLabel lblTotalDeductions;
        public DevExpress.XtraReports.UI.XRLabel lblNetPaid;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        public DevExpress.XtraReports.UI.XRLabel lblOtherDeductions;
        private DevExpress.XtraReports.UI.XRLabel xrLabel14;
        private DevExpress.XtraReports.UI.XRSubreport subOtherDeductions;
        private DevExpress.XtraReports.UI.XRLabel xrLabel13;
        private DevExpress.XtraReports.UI.XRLine xrLine7;
        private DevExpress.XtraReports.UI.XRLine xrLine9;
        private DevExpress.XtraReports.UI.XRLine xrLine10;
        private DevExpress.XtraReports.UI.XRLine xrLine11;
        private DevExpress.XtraReports.UI.XRLine xrLine3;
        public DevExpress.XtraReports.UI.XRLabel lblName;
        public DevExpress.XtraReports.UI.XRLabel lblEIN;
        public DevExpress.XtraReports.UI.XRLabel lblDep;
        public DevExpress.XtraReports.UI.XRLabel lblDesig;
    }
}
