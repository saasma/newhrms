using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using BLL;
using DevExpress.XtraReports.UI;
using Web.ReportDataSetTableAdapters;
using Utils;
using System.Web;
using System.Drawing.Text;

namespace Web.CP.Report.Templates.Pay.Detail
{
    public partial class NRBIncomeVerificationReport : DevExpress.XtraReports.UI.XtraReport
    {

        protected void ClearBorderControlEvent(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ReportHelper.TableCell_BeforePrintEvent(sender, e);
            ((DevExpress.XtraReports.UI.XRControl)sender).ForeColor = System.Drawing.Color.Black;

        }
        protected void ClearBorderLessControlEvent(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ReportHelper.TableCell_BeforePrintEvent(sender, e);

        }

        public NRBIncomeVerificationReport()
        {
            InitializeComponent();
        }

        private void NRBIncomeVerificationReport_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

            //PrivateFontCollection fontColl = new PrivateFontCollection();
            //fontColl.AddFontFile(HttpContext.Current.Server.MapPath("~/theme/fonts/MYPREETI.ttf"));

            //NRBIncomeVerificationReport report = (NRBIncomeVerificationReport)sender;
            //foreach (Band b in report.Bands)
            //{
            //    foreach (XRControl c in b.Controls)
            //    {
            //        c.Font = new Font(fontColl.Families[0], c.Font.Size, c.Font.Style);
            //    }
            //}

        }

       
       
    }
}
