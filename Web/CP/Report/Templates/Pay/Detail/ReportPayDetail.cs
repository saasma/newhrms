using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using BLL;
using DevExpress.XtraReports.UI;
using Web.ReportDataSetTableAdapters;
using Utils;
using System.Web;
using System.Data;
using Web.Helper;

namespace Web.CP.Report.Templates.Pay.Detail
{
    public partial class ReportPayDetail : DevExpress.XtraReports.UI.XtraReport
    {

        public int Month { get; set; }
        public int Year { get; set; }
        public int CompanyId { get; set; }
        public bool IsIncome { get; set; }

        //public void SetSubReport(int employeeId, object sender)
        //{
        //    XRSubreport subReport = (XRSubreport) sender;
        //    Web.CP.Report.Templates.Pay.Detail.ReportPayDetailHeader report =
        //        (Web.CP.Report.Templates.Pay.Detail.ReportPayDetailHeader) subReport.ReportSource;


        //    Report_Pay_PaySlipDetail_EmpHeaderTableAdapter headerAdapter = new Report_Pay_PaySlipDetail_EmpHeaderTableAdapter();


        //    ReportDataSet.Report_Pay_PaySlipDetail_EmpHeaderDataTable incomesTable =
        //       headerAdapter.GetData(this.Month, this.Year,
        //                             SessionManager.CurrentCompanyId, employeeId, true);

        //    report.DataSource  = incomesTable;
        //    report.DataMember = "Report";
            


        //}

        public void SetSubReport(int employeeId, object sender)
        {

            XRSubreport subReport = (XRSubreport)sender;

            switch (subReport.Name)
            {
                case "subReportEmployeeIncomes":
                    Web.CP.Report.Templates.Pay.Detail.ReportPayDetailIncomes report5 =
                       // new ReportIncomes();
                        (Web.CP.Report.Templates.Pay.Detail.ReportPayDetailIncomes)subReport.ReportSource;

                
                      
                    ReportDataSet.Report_Pay_PaySlipDetail_EmpHeaderDataTable incomesTable =
                        HttpContext.Current.Items["PaySlipDetailIncomeList"] as ReportDataSet.Report_Pay_PaySlipDetail_EmpHeaderDataTable;

                   if (incomesTable != null)
                    {
                    
                        incomesTable.DefaultView.RowFilter = "EIN = " + employeeId;

                        report5.DataSource = incomesTable;
                        report5.DataMember = "Report1";
                        //subReport.ReportSource = report5;
                        //subReport.ReportSource = report5;
                    }
                    break;
                case "subReportDeductions":

                    
                    Web.CP.Report.Templates.Pay.Detail.ReportDeductions report2 =
                        (Web.CP.Report.Templates.Pay.Detail.ReportDeductions) subReport.ReportSource;

                    ReportDataSet.Report_Pay_PaySlipDetail_DeductionDataTable tableDeduction
                        = HttpContext.Current.Items["PaySlipDetailDeductionList"] as ReportDataSet.Report_Pay_PaySlipDetail_DeductionDataTable;

                    //ReportDataSet.Report_Pay_PaySlipDetail_EmpHeaderDataTable incomesTable1 =
                    //HttpContext.Current.Items["PaySlipDetailIncomeList"] as ReportDataSet.Report_Pay_PaySlipDetail_EmpHeaderDataTable;


                    if (tableDeduction != null)
                    {
                                               

//ReportHelper.SetEqualRows(incomesTable1, tableDeduction,employeeId);
                        tableDeduction.DefaultView.RowFilter = "EmployeeId = " + employeeId;
                       // incomesTable1.DefaultView.RowFilter = "EIN = " + employeeId;

                        report2.DataSource = tableDeduction;
                        report2.DataMember = "Report1";

                        //decimal total = ReportHelper.GetTotal(tableDeduction, "Amount");
                       // report2.cellTotal.Text = total.ToString("N2");

                    }
                    break;

                case "subReportLoan":
                    Web.CP.Report.Templates.Pay.Detail.ReportPayDetailAdvance reportLoan =
                        (Web.CP.Report.Templates.Pay.Detail.ReportPayDetailAdvance) subReport.ReportSource;

                   
                    ReportDataSet.Report_Pay_PaySlipDetail_LoanDataTable loanTable =
                        HttpContext.Current.Items["PaySlipDetailLoanList"] as ReportDataSet.Report_Pay_PaySlipDetail_LoanDataTable;
                    if (loanTable != null)
                    {
                        loanTable.DefaultView.RowFilter = "EmployeeId = " + employeeId;

                        if (loanTable.DefaultView.Count > 0)
                        {
                            reportLoan.labelTitle.Text = "Loan Name";



                            reportLoan.DataSource = loanTable;
                            reportLoan.DataMember = "Report2";
                        }
                        else
                        {
                            reportLoan.Visible = false;
                        }
                    }
                    break;
                case "subReportLoanSimple":
                    Web.CP.Report.Templates.Pay.Detail.ReportPayDetailLoanSimple reportLoanSimple =
                        (Web.CP.Report.Templates.Pay.Detail.ReportPayDetailLoanSimple) subReport.ReportSource;

                    ReportDataSet.Report_Pay_PaySlipDetail_SimpleLoanDataTable loanSimpleTable =
                        HttpContext.Current.Items["PaySlipDetailSimpleLoanList"] as ReportDataSet.Report_Pay_PaySlipDetail_SimpleLoanDataTable;
                    if (loanSimpleTable != null)
                    {
                        loanSimpleTable.DefaultView.RowFilter = "EmployeeId = " + employeeId;


                        if (loanSimpleTable.DefaultView.Count > 0)
                        {
                            reportLoanSimple.labelTitle.Text = "Loan Name";

                            reportLoanSimple.DataSource = loanSimpleTable;
                            reportLoanSimple.DataMember = "Report2";
                        }
                        else
                        {
                            reportLoanSimple.Visible = false;
                        }
                    }
                    break;
                case "subReportLeave":
                    Web.CP.Report.Templates.Pay.Detail.ReportPayDetailLeave reportLeave =
                        (Web.CP.Report.Templates.Pay.Detail.ReportPayDetailLeave) subReport.ReportSource;

                    ReportDataSet.Report_Pay_PaySlipDetail_LeaveDataTable leaveTable =
                        HttpContext.Current.Items["PaySlipDetailLeaveList"] as ReportDataSet.Report_Pay_PaySlipDetail_LeaveDataTable;
                    if (leaveTable != null)
                    {
                        leaveTable.DefaultView.RowFilter = "EmployeeId = " + employeeId;


                        if (leaveTable.DefaultView.Count > 0)
                        {

                            reportLeave.DataSource = leaveTable;
                            reportLeave.DataMember = "Report3";
                        }
                        else
                            reportLeave.Visible = false;
                    }
                    break;
                case "subReportAdvance":
                    Web.CP.Report.Templates.Pay.Detail.ReportPayDetailAdvance advanceReport =
                        (Web.CP.Report.Templates.Pay.Detail.ReportPayDetailAdvance) subReport.ReportSource;

                
                    ReportDataSet.Report_Pay_PaySlipDetail_AdvanceDataTable advanceTable =
                         HttpContext.Current.Items["PaySlipDetailAdvanceList"] as ReportDataSet.Report_Pay_PaySlipDetail_AdvanceDataTable;
                    if (advanceTable != null)
                    {
                        advanceTable.DefaultView.RowFilter = "EmployeeId = " + employeeId;


                        if (advanceTable.DefaultView.Count > 0)
                        {

                            advanceReport.DataSource = advanceTable;
                            advanceReport.DataMember = "Report4";
                        }
                        else
                        {
                            advanceReport.Visible = false;
                        }
                    }

                    break;
            }





        }

        public ReportPayDetail()
        {
            InitializeComponent();
        }

        private void subReportIncomes_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRSubreport report = (XRSubreport)sender;


            Web.CP.Report.Templates.Pay.Detail.ReportPayDetail mainReport =
            (Web.CP.Report.Templates.Pay.Detail.ReportPayDetail)report.Report;



            mainReport.SetSubReport(Convert.ToInt32(GetCurrentColumnValue("EIN")), sender);

        }

        public void SetSubReport(object sender)
        {
            ReportHelper.BindCompanyInfo(sender);
        }

        protected void ClearBorderControlEvent(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ReportHelper.TableCell_BeforePrintEvent(sender, e);
            

        }
        protected void ClearBorderLessControlEvent(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ReportHelper.TableCell_BeforePrintEvent(sender, e);
            ((DevExpress.XtraReports.UI.XRControl)sender).Borders = DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right |  DevExpress.XtraPrinting.BorderSide.Bottom;
        }

        private void B(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ReportHelper.TableCell_BeforePrintEvent(sender, e);
            //((DevExpress.XtraReports.UI.XRControl)sender).Borders = DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom;
        }

       


        
    }
}
