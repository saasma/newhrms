﻿namespace Web.CP.Report.Templates.Pay.Detail
{
    partial class NRBIncomeVerificationReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;


        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.lblEmpNepName = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.tableOther = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow35 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell107 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTotalIncome = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell109 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTotalFinalTax = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lbl1SalaryTalab = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lbl2Allowance = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPFDeductionAndCITSum = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lbl3ProvidentFund = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblInsuranceDeduction = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lbl4VehicleFacility = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblRemoteAllowance = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lbl5TelephoneFacility = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTotalDeduction = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lbl6HousingAllowance = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSingleMarriedDeduction = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTotalTaxableIncome = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblOnePercent = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lbl7Bonus = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblFifteenPercent = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.label11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lbl8HealthExpenses = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTwentyFivePercent = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lbl9LeaveEncashment = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThirtyFivePercent = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lbl10DashainAllowance = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTotalTax = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell53 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lbl11LifeInsuranceIncome = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMedicalTax = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell57 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lbl12Reward = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell59 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblFemaleRebate = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lbl13ConcessessionalInterest = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell64 = new DevExpress.XtraReports.UI.XRTableCell();
            this.taxDetailsHeaderFormatting = new DevExpress.XtraReports.UI.FormattingRule();
            this.taxDetailsTotalFormatting = new DevExpress.XtraReports.UI.FormattingRule();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.reportDataSet1 = new Web.ReportDataSet();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DateTimeStyle = new DevExpress.XtraReports.UI.XRControlStyle();
            this.SlipHeaderStyle = new DevExpress.XtraReports.UI.XRControlStyle();
            this.InfoStyle = new DevExpress.XtraReports.UI.XRControlStyle();
            this.LineStyle = new DevExpress.XtraReports.UI.XRControlStyle();
            this.ContentLineStyle = new DevExpress.XtraReports.UI.XRControlStyle();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.lblTitle = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.labelCompanyName = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this.tableOther)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblEmpNepName,
            this.xrLabel6,
            this.tableOther,
            this.xrTable2});
            this.Detail.Dpi = 100F;
            this.Detail.HeightF = 609.2916F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.StylePriority.UseBorders = false;
            this.Detail.StylePriority.UseBorderWidth = false;
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblEmpNepName
            // 
            this.lblEmpNepName.BackColor = System.Drawing.Color.Transparent;
            this.lblEmpNepName.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblEmpNepName.Dpi = 100F;
            this.lblEmpNepName.Font = new System.Drawing.Font("Nirmala UI", 10F);
            this.lblEmpNepName.ForeColor = System.Drawing.Color.Black;
            this.lblEmpNepName.LocationFloat = new DevExpress.Utils.PointFloat(5.000063F, 562.0834F);
            this.lblEmpNepName.Name = "lblEmpNepName";
            this.lblEmpNepName.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblEmpNepName.Scripts.OnBeforePrint = "xrLabel20_BeforePrint";
            this.lblEmpNepName.SizeF = new System.Drawing.SizeF(295.2087F, 25.41675F);
            this.lblEmpNepName.StylePriority.UseBackColor = false;
            this.lblEmpNepName.StylePriority.UseBorders = false;
            this.lblEmpNepName.StylePriority.UseFont = false;
            this.lblEmpNepName.StylePriority.UseForeColor = false;
            this.lblEmpNepName.StylePriority.UseTextAlignment = false;
            this.lblEmpNepName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel6
            // 
            this.xrLabel6.BackColor = System.Drawing.Color.Transparent;
            this.xrLabel6.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel6.Dpi = 100F;
            this.xrLabel6.Font = new System.Drawing.Font("Nirmala UI", 10F);
            this.xrLabel6.ForeColor = System.Drawing.Color.Black;
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(5.000063F, 536.6667F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.Scripts.OnBeforePrint = "xrLabel20_BeforePrint";
            this.xrLabel6.SizeF = new System.Drawing.SizeF(295.2087F, 25.41675F);
            this.xrLabel6.StylePriority.UseBackColor = false;
            this.xrLabel6.StylePriority.UseBorders = false;
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.StylePriority.UseForeColor = false;
            this.xrLabel6.StylePriority.UseTextAlignment = false;
            this.xrLabel6.Text = "निवेदक, ";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // tableOther
            // 
            this.tableOther.Dpi = 100F;
            this.tableOther.LocationFloat = new DevExpress.Utils.PointFloat(5F, 430.9374F);
            this.tableOther.Name = "tableOther";
            this.tableOther.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow17,
            this.xrTableRow35});
            this.tableOther.SizeF = new System.Drawing.SizeF(790.4186F, 57.45836F);
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell11,
            this.xrTableCell13,
            this.xrTableCell14,
            this.xrTableCell15});
            this.xrTableRow17.Dpi = 100F;
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.Weight = 1D;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Dpi = 100F;
            this.xrTableCell11.Font = new System.Drawing.Font("Nirmala UI", 10F);
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableCell11.StylePriority.UseFont = false;
            this.xrTableCell11.StylePriority.UsePadding = false;
            this.xrTableCell11.StylePriority.UseTextAlignment = false;
            this.xrTableCell11.Text = "   ऐ. अन्य ( खुलाउने )";
            this.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell11.Weight = 1.1204518790871847D;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Dpi = 100F;
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Weight = 0.37954581669351384D;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Dpi = 100F;
            this.xrTableCell14.Font = new System.Drawing.Font("Nirmala UI", 10F);
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.StylePriority.UseFont = false;
            this.xrTableCell14.Weight = 1.1204568391248717D;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Dpi = 100F;
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Weight = 0.37954546509442966D;
            // 
            // xrTableRow35
            // 
            this.xrTableRow35.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell107,
            this.lblTotalIncome,
            this.xrTableCell109,
            this.lblTotalFinalTax});
            this.xrTableRow35.Dpi = 100F;
            this.xrTableRow35.Name = "xrTableRow35";
            this.xrTableRow35.Weight = 1D;
            // 
            // xrTableCell107
            // 
            this.xrTableCell107.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell107.Dpi = 100F;
            this.xrTableCell107.Font = new System.Drawing.Font("Nirmala UI", 10F);
            this.xrTableCell107.Multiline = true;
            this.xrTableCell107.Name = "xrTableCell107";
            this.xrTableCell107.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 100F);
            this.xrTableCell107.StylePriority.UseBorders = false;
            this.xrTableCell107.StylePriority.UseFont = false;
            this.xrTableCell107.StylePriority.UsePadding = false;
            this.xrTableCell107.StylePriority.UseTextAlignment = false;
            this.xrTableCell107.Text = "कुल आय ";
            this.xrTableCell107.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell107.Weight = 1.1204518790871847D;
            // 
            // lblTotalIncome
            // 
            this.lblTotalIncome.Dpi = 100F;
            this.lblTotalIncome.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblTotalIncome.Name = "lblTotalIncome";
            this.lblTotalIncome.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 3, 0, 0, 100F);
            this.lblTotalIncome.StylePriority.UseFont = false;
            this.lblTotalIncome.StylePriority.UsePadding = false;
            this.lblTotalIncome.StylePriority.UseTextAlignment = false;
            this.lblTotalIncome.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTotalIncome.Weight = 0.37954581669351384D;
            // 
            // xrTableCell109
            // 
            this.xrTableCell109.Dpi = 100F;
            this.xrTableCell109.Font = new System.Drawing.Font("Nirmala UI", 10F);
            this.xrTableCell109.Multiline = true;
            this.xrTableCell109.Name = "xrTableCell109";
            this.xrTableCell109.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 100F);
            this.xrTableCell109.StylePriority.UseFont = false;
            this.xrTableCell109.StylePriority.UsePadding = false;
            this.xrTableCell109.StylePriority.UseTextAlignment = false;
            this.xrTableCell109.Text = "खुद लाग्ने कर ";
            this.xrTableCell109.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell109.Weight = 1.1204568391248717D;
            // 
            // lblTotalFinalTax
            // 
            this.lblTotalFinalTax.Dpi = 100F;
            this.lblTotalFinalTax.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblTotalFinalTax.Name = "lblTotalFinalTax";
            this.lblTotalFinalTax.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 3, 0, 0, 100F);
            this.lblTotalFinalTax.StylePriority.UseFont = false;
            this.lblTotalFinalTax.StylePriority.UsePadding = false;
            this.lblTotalFinalTax.StylePriority.UseTextAlignment = false;
            this.lblTotalFinalTax.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTotalFinalTax.Weight = 0.37954546509442966D;
            // 
            // xrTable2
            // 
            this.xrTable2.Dpi = 100F;
            this.xrTable2.Font = new System.Drawing.Font("Tahoma", 9F);
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(4.999987F, 0F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5,
            this.xrTableRow6,
            this.xrTableRow3,
            this.xrTableRow2,
            this.xrTableRow4,
            this.xrTableRow7,
            this.xrTableRow8,
            this.xrTableRow9,
            this.xrTableRow10,
            this.xrTableRow11,
            this.xrTableRow12,
            this.xrTableRow13,
            this.xrTableRow14,
            this.xrTableRow15,
            this.xrTableRow16});
            this.xrTable2.SizeF = new System.Drawing.SizeF(790.4186F, 430.9374F);
            this.xrTable2.StylePriority.UseFont = false;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell10,
            this.lbl1SalaryTalab,
            this.xrTableCell3,
            this.xrTableCell1});
            this.xrTableRow5.Dpi = 100F;
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 1D;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Dpi = 100F;
            this.xrTableCell10.Font = new System.Drawing.Font("Nirmala UI", 10F);
            this.xrTableCell10.Multiline = true;
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.xrTableCell10.StylePriority.UseFont = false;
            this.xrTableCell10.StylePriority.UsePadding = false;
            this.xrTableCell10.StylePriority.UseTextAlignment = false;
            this.xrTableCell10.Text = "क)  तलब रू . ";
            this.xrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell10.Weight = 1.1204518790871847D;
            // 
            // lbl1SalaryTalab
            // 
            this.lbl1SalaryTalab.Dpi = 100F;
            this.lbl1SalaryTalab.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lbl1SalaryTalab.Name = "lbl1SalaryTalab";
            this.lbl1SalaryTalab.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 3, 0, 0, 100F);
            this.lbl1SalaryTalab.StylePriority.UseFont = false;
            this.lbl1SalaryTalab.StylePriority.UsePadding = false;
            this.lbl1SalaryTalab.StylePriority.UseTextAlignment = false;
            this.lbl1SalaryTalab.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lbl1SalaryTalab.Weight = 0.37954581669351384D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Dpi = 100F;
            this.xrTableCell3.Font = new System.Drawing.Font("Nirmala UI", 10F);
            this.xrTableCell3.Multiline = true;
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.xrTableCell3.StylePriority.UseFont = false;
            this.xrTableCell3.StylePriority.UsePadding = false;
            this.xrTableCell3.StylePriority.UseTextAlignment = false;
            this.xrTableCell3.Text = "स्विकृत अवकाश कोष कट्टी रू. ";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell3.Weight = 1.1204568391248717D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Dpi = 100F;
            this.xrTableCell1.Font = new System.Drawing.Font("Tahoma", 10F);
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 3, 0, 0, 100F);
            this.xrTableCell1.StylePriority.UseFont = false;
            this.xrTableCell1.StylePriority.UsePadding = false;
            this.xrTableCell1.StylePriority.UseTextAlignment = false;
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell1.Weight = 0.37954546509442966D;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell12,
            this.lbl2Allowance,
            this.xrTableCell4,
            this.lblPFDeductionAndCITSum});
            this.xrTableRow6.Dpi = 100F;
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Weight = 1D;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Dpi = 100F;
            this.xrTableCell12.Font = new System.Drawing.Font("Nirmala UI", 10F);
            this.xrTableCell12.Multiline = true;
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.xrTableCell12.StylePriority.UseFont = false;
            this.xrTableCell12.StylePriority.UsePadding = false;
            this.xrTableCell12.StylePriority.UseTextAlignment = false;
            this.xrTableCell12.Text = "ख)  भत्ता ( सबै किसिमको )  रू . ";
            this.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell12.Weight = 1.1204518790871847D;
            // 
            // lbl2Allowance
            // 
            this.lbl2Allowance.Dpi = 100F;
            this.lbl2Allowance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lbl2Allowance.Name = "lbl2Allowance";
            this.lbl2Allowance.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 3, 0, 0, 100F);
            this.lbl2Allowance.StylePriority.UseFont = false;
            this.lbl2Allowance.StylePriority.UsePadding = false;
            this.lbl2Allowance.StylePriority.UseTextAlignment = false;
            this.lbl2Allowance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lbl2Allowance.Weight = 0.37954581669351384D;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Dpi = 100F;
            this.xrTableCell4.Font = new System.Drawing.Font("Nirmala UI", 10F);
            this.xrTableCell4.Multiline = true;
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.xrTableCell4.StylePriority.UseFont = false;
            this.xrTableCell4.StylePriority.UsePadding = false;
            this.xrTableCell4.StylePriority.UseTextAlignment = false;
            this.xrTableCell4.Text = "क)  क. सं. कोष कट्टी र ना. ल. कोष कट्टी रू. ";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell4.Weight = 1.1204568391248717D;
            // 
            // lblPFDeductionAndCITSum
            // 
            this.lblPFDeductionAndCITSum.Dpi = 100F;
            this.lblPFDeductionAndCITSum.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblPFDeductionAndCITSum.Name = "lblPFDeductionAndCITSum";
            this.lblPFDeductionAndCITSum.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 3, 0, 0, 100F);
            this.lblPFDeductionAndCITSum.StylePriority.UseFont = false;
            this.lblPFDeductionAndCITSum.StylePriority.UsePadding = false;
            this.lblPFDeductionAndCITSum.StylePriority.UseTextAlignment = false;
            this.lblPFDeductionAndCITSum.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblPFDeductionAndCITSum.Weight = 0.37954546509442966D;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell17,
            this.lbl3ProvidentFund,
            this.xrTableCell19,
            this.lblInsuranceDeduction});
            this.xrTableRow3.Dpi = 100F;
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 1D;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Dpi = 100F;
            this.xrTableCell17.Font = new System.Drawing.Font("Nirmala UI", 10F);
            this.xrTableCell17.Multiline = true;
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.xrTableCell17.StylePriority.UseFont = false;
            this.xrTableCell17.StylePriority.UsePadding = false;
            this.xrTableCell17.StylePriority.UseTextAlignment = false;
            this.xrTableCell17.Text = "ग)  क .  सं .  कोष थप रू .  ";
            this.xrTableCell17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell17.Weight = 1.1204518790871847D;
            // 
            // lbl3ProvidentFund
            // 
            this.lbl3ProvidentFund.Dpi = 100F;
            this.lbl3ProvidentFund.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lbl3ProvidentFund.Name = "lbl3ProvidentFund";
            this.lbl3ProvidentFund.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 3, 0, 0, 100F);
            this.lbl3ProvidentFund.StylePriority.UseFont = false;
            this.lbl3ProvidentFund.StylePriority.UsePadding = false;
            this.lbl3ProvidentFund.StylePriority.UseTextAlignment = false;
            this.lbl3ProvidentFund.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lbl3ProvidentFund.Weight = 0.37954581669351384D;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Dpi = 100F;
            this.xrTableCell19.Font = new System.Drawing.Font("Nirmala UI", 10F);
            this.xrTableCell19.Multiline = true;
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.xrTableCell19.StylePriority.UseFont = false;
            this.xrTableCell19.StylePriority.UsePadding = false;
            this.xrTableCell19.StylePriority.UseTextAlignment = false;
            this.xrTableCell19.Text = "ख)  जीवन बीमा प्रिमियम कट्टी रू. ";
            this.xrTableCell19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell19.Weight = 1.1204568391248717D;
            // 
            // lblInsuranceDeduction
            // 
            this.lblInsuranceDeduction.Dpi = 100F;
            this.lblInsuranceDeduction.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblInsuranceDeduction.Name = "lblInsuranceDeduction";
            this.lblInsuranceDeduction.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 3, 0, 0, 100F);
            this.lblInsuranceDeduction.StylePriority.UseFont = false;
            this.lblInsuranceDeduction.StylePriority.UsePadding = false;
            this.lblInsuranceDeduction.StylePriority.UseTextAlignment = false;
            this.lblInsuranceDeduction.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblInsuranceDeduction.Weight = 0.37954546509442966D;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell9,
            this.lbl4VehicleFacility,
            this.xrTableCell2,
            this.lblRemoteAllowance});
            this.xrTableRow2.Dpi = 100F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1D;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Dpi = 100F;
            this.xrTableCell9.Font = new System.Drawing.Font("Nirmala UI", 10F);
            this.xrTableCell9.Multiline = true;
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.xrTableCell9.StylePriority.UseFont = false;
            this.xrTableCell9.StylePriority.UsePadding = false;
            this.xrTableCell9.StylePriority.UseTextAlignment = false;
            this.xrTableCell9.Text = "घ)  मोटर सुबिधा  ( सिमा ०.५ % )  रू . ";
            this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell9.Weight = 1.1204518790871847D;
            // 
            // lbl4VehicleFacility
            // 
            this.lbl4VehicleFacility.Dpi = 100F;
            this.lbl4VehicleFacility.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lbl4VehicleFacility.Name = "lbl4VehicleFacility";
            this.lbl4VehicleFacility.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 3, 0, 0, 100F);
            this.lbl4VehicleFacility.StylePriority.UseFont = false;
            this.lbl4VehicleFacility.StylePriority.UsePadding = false;
            this.lbl4VehicleFacility.StylePriority.UseTextAlignment = false;
            this.lbl4VehicleFacility.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lbl4VehicleFacility.Weight = 0.37954581669351384D;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Dpi = 100F;
            this.xrTableCell2.Font = new System.Drawing.Font("Nirmala UI", 10F);
            this.xrTableCell2.Multiline = true;
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.StylePriority.UseFont = false;
            this.xrTableCell2.StylePriority.UseTextAlignment = false;
            this.xrTableCell2.Text = " ग)  दुर्गम भत्ता ";
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell2.Weight = 1.1204569235877835D;
            // 
            // lblRemoteAllowance
            // 
            this.lblRemoteAllowance.Dpi = 100F;
            this.lblRemoteAllowance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblRemoteAllowance.Name = "lblRemoteAllowance";
            this.lblRemoteAllowance.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 3, 0, 0, 100F);
            this.lblRemoteAllowance.StylePriority.UseFont = false;
            this.lblRemoteAllowance.StylePriority.UsePadding = false;
            this.lblRemoteAllowance.StylePriority.UseTextAlignment = false;
            this.lblRemoteAllowance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblRemoteAllowance.Weight = 0.379545380631518D;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell21,
            this.lbl5TelephoneFacility,
            this.xrTableCell23,
            this.lblTotalDeduction});
            this.xrTableRow4.Dpi = 100F;
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 1D;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.Dpi = 100F;
            this.xrTableCell21.Font = new System.Drawing.Font("Nirmala UI", 10F);
            this.xrTableCell21.Multiline = true;
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.xrTableCell21.StylePriority.UseFont = false;
            this.xrTableCell21.StylePriority.UsePadding = false;
            this.xrTableCell21.StylePriority.UseTextAlignment = false;
            this.xrTableCell21.Text = "ङ)  टेलिफोन सुविधा ";
            this.xrTableCell21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell21.Weight = 1.1204518790871847D;
            // 
            // lbl5TelephoneFacility
            // 
            this.lbl5TelephoneFacility.Dpi = 100F;
            this.lbl5TelephoneFacility.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lbl5TelephoneFacility.Name = "lbl5TelephoneFacility";
            this.lbl5TelephoneFacility.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 3, 0, 0, 100F);
            this.lbl5TelephoneFacility.StylePriority.UseFont = false;
            this.lbl5TelephoneFacility.StylePriority.UsePadding = false;
            this.lbl5TelephoneFacility.StylePriority.UseTextAlignment = false;
            this.lbl5TelephoneFacility.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lbl5TelephoneFacility.Weight = 0.37954581669351384D;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Dpi = 100F;
            this.xrTableCell23.Font = new System.Drawing.Font("Nirmala UI", 10F);
            this.xrTableCell23.Multiline = true;
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100F);
            this.xrTableCell23.StylePriority.UseFont = false;
            this.xrTableCell23.StylePriority.UsePadding = false;
            this.xrTableCell23.StylePriority.UseTextAlignment = false;
            this.xrTableCell23.Text = "जम्मा कट्टी रू.";
            this.xrTableCell23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell23.Weight = 1.1204568391248717D;
            // 
            // lblTotalDeduction
            // 
            this.lblTotalDeduction.Dpi = 100F;
            this.lblTotalDeduction.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblTotalDeduction.Name = "lblTotalDeduction";
            this.lblTotalDeduction.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 3, 0, 0, 100F);
            this.lblTotalDeduction.StylePriority.UseFont = false;
            this.lblTotalDeduction.StylePriority.UsePadding = false;
            this.lblTotalDeduction.StylePriority.UseTextAlignment = false;
            this.lblTotalDeduction.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTotalDeduction.Weight = 0.37954546509442966D;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell25,
            this.lbl6HousingAllowance,
            this.xrTableCell27,
            this.lblSingleMarriedDeduction});
            this.xrTableRow7.Dpi = 100F;
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Weight = 1D;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Dpi = 100F;
            this.xrTableCell25.Font = new System.Drawing.Font("Nirmala UI", 10F);
            this.xrTableCell25.Multiline = true;
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.xrTableCell25.StylePriority.UseFont = false;
            this.xrTableCell25.StylePriority.UsePadding = false;
            this.xrTableCell25.StylePriority.UseTextAlignment = false;
            this.xrTableCell25.Text = "च)  आवास सुविधा को सिमा  ( २ %  )";
            this.xrTableCell25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell25.Weight = 1.1204518790871847D;
            // 
            // lbl6HousingAllowance
            // 
            this.lbl6HousingAllowance.Dpi = 100F;
            this.lbl6HousingAllowance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lbl6HousingAllowance.Name = "lbl6HousingAllowance";
            this.lbl6HousingAllowance.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 3, 0, 0, 100F);
            this.lbl6HousingAllowance.StylePriority.UseFont = false;
            this.lbl6HousingAllowance.StylePriority.UsePadding = false;
            this.lbl6HousingAllowance.StylePriority.UseTextAlignment = false;
            this.lbl6HousingAllowance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lbl6HousingAllowance.Weight = 0.37954581669351384D;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.Dpi = 100F;
            this.xrTableCell27.Font = new System.Drawing.Font("Nirmala UI", 10F);
            this.xrTableCell27.Multiline = true;
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100F);
            this.xrTableCell27.StylePriority.UseFont = false;
            this.xrTableCell27.StylePriority.UsePadding = false;
            this.xrTableCell27.StylePriority.UseTextAlignment = false;
            this.xrTableCell27.Text = "दम्पति र व्यक्तिगत छुट सिमा रू.  ";
            this.xrTableCell27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell27.Weight = 1.1204568391248717D;
            // 
            // lblSingleMarriedDeduction
            // 
            this.lblSingleMarriedDeduction.Dpi = 100F;
            this.lblSingleMarriedDeduction.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblSingleMarriedDeduction.Name = "lblSingleMarriedDeduction";
            this.lblSingleMarriedDeduction.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 3, 0, 0, 100F);
            this.lblSingleMarriedDeduction.StylePriority.UseFont = false;
            this.lblSingleMarriedDeduction.StylePriority.UsePadding = false;
            this.lblSingleMarriedDeduction.StylePriority.UseTextAlignment = false;
            this.lblSingleMarriedDeduction.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblSingleMarriedDeduction.Weight = 0.37954546509442966D;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell29,
            this.xrTableCell30,
            this.xrTableCell31,
            this.lblTotalTaxableIncome});
            this.xrTableRow8.Dpi = 100F;
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Weight = 1D;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Dpi = 100F;
            this.xrTableCell29.Font = new System.Drawing.Font("Nirmala UI", 10F);
            this.xrTableCell29.Multiline = true;
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.xrTableCell29.StylePriority.UseFont = false;
            this.xrTableCell29.StylePriority.UsePadding = false;
            this.xrTableCell29.StylePriority.UseTextAlignment = false;
            this.xrTableCell29.Text = "छ)  घर भाडा रू .  ";
            this.xrTableCell29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell29.Weight = 1.1204518790871847D;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.Dpi = 100F;
            this.xrTableCell30.Font = new System.Drawing.Font("Tahoma", 10F);
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 3, 0, 0, 100F);
            this.xrTableCell30.StylePriority.UseFont = false;
            this.xrTableCell30.StylePriority.UsePadding = false;
            this.xrTableCell30.StylePriority.UseTextAlignment = false;
            this.xrTableCell30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell30.Weight = 0.37954581669351384D;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Dpi = 100F;
            this.xrTableCell31.Font = new System.Drawing.Font("Nirmala UI", 10F);
            this.xrTableCell31.Multiline = true;
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100F);
            this.xrTableCell31.StylePriority.UseFont = false;
            this.xrTableCell31.StylePriority.UsePadding = false;
            this.xrTableCell31.StylePriority.UseTextAlignment = false;
            this.xrTableCell31.Text = "कर योग्य आय ";
            this.xrTableCell31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell31.Weight = 1.1204568391248717D;
            // 
            // lblTotalTaxableIncome
            // 
            this.lblTotalTaxableIncome.Dpi = 100F;
            this.lblTotalTaxableIncome.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblTotalTaxableIncome.Name = "lblTotalTaxableIncome";
            this.lblTotalTaxableIncome.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 3, 0, 0, 100F);
            this.lblTotalTaxableIncome.StylePriority.UseFont = false;
            this.lblTotalTaxableIncome.StylePriority.UsePadding = false;
            this.lblTotalTaxableIncome.StylePriority.UseTextAlignment = false;
            this.lblTotalTaxableIncome.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTotalTaxableIncome.Weight = 0.37954546509442966D;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell33,
            this.xrTableCell34,
            this.xrTableCell35,
            this.lblOnePercent});
            this.xrTableRow9.Dpi = 100F;
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Weight = 1D;
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.Dpi = 100F;
            this.xrTableCell33.Font = new System.Drawing.Font("Nirmala UI", 10F);
            this.xrTableCell33.Multiline = true;
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.xrTableCell33.StylePriority.UseFont = false;
            this.xrTableCell33.StylePriority.UsePadding = false;
            this.xrTableCell33.StylePriority.UseTextAlignment = false;
            this.xrTableCell33.Text = "ज)  अन्य आय  रू. ";
            this.xrTableCell33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell33.Weight = 1.1204518790871847D;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Dpi = 100F;
            this.xrTableCell34.Font = new System.Drawing.Font("Tahoma", 10F);
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 3, 0, 0, 100F);
            this.xrTableCell34.StylePriority.UseFont = false;
            this.xrTableCell34.StylePriority.UsePadding = false;
            this.xrTableCell34.StylePriority.UseTextAlignment = false;
            this.xrTableCell34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell34.Weight = 0.37954581669351384D;
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.Dpi = 100F;
            this.xrTableCell35.Font = new System.Drawing.Font("Nirmala UI", 10F);
            this.xrTableCell35.Multiline = true;
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.xrTableCell35.StylePriority.UseFont = false;
            this.xrTableCell35.StylePriority.UsePadding = false;
            this.xrTableCell35.StylePriority.UseTextAlignment = false;
            this.xrTableCell35.Text = "पहिलो सिमा १ % ( सामाजिक सुरक्षा कर )";
            this.xrTableCell35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell35.Weight = 1.1204568391248717D;
            // 
            // lblOnePercent
            // 
            this.lblOnePercent.Dpi = 100F;
            this.lblOnePercent.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblOnePercent.Name = "lblOnePercent";
            this.lblOnePercent.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 3, 0, 0, 100F);
            this.lblOnePercent.StylePriority.UseFont = false;
            this.lblOnePercent.StylePriority.UsePadding = false;
            this.lblOnePercent.StylePriority.UseTextAlignment = false;
            this.lblOnePercent.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblOnePercent.Weight = 0.37954546509442966D;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell37,
            this.lbl7Bonus,
            this.xrTableCell39,
            this.lblFifteenPercent});
            this.xrTableRow10.Dpi = 100F;
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Weight = 1D;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.Dpi = 100F;
            this.xrTableCell37.Font = new System.Drawing.Font("Nirmala UI", 10F);
            this.xrTableCell37.Multiline = true;
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableCell37.StylePriority.UseFont = false;
            this.xrTableCell37.StylePriority.UsePadding = false;
            this.xrTableCell37.StylePriority.UseTextAlignment = false;
            this.xrTableCell37.Text = "   अ.  बोनस रू. ";
            this.xrTableCell37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell37.Weight = 1.1204518790871847D;
            // 
            // lbl7Bonus
            // 
            this.lbl7Bonus.Dpi = 100F;
            this.lbl7Bonus.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lbl7Bonus.Name = "lbl7Bonus";
            this.lbl7Bonus.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 3, 0, 0, 100F);
            this.lbl7Bonus.StylePriority.UseFont = false;
            this.lbl7Bonus.StylePriority.UsePadding = false;
            this.lbl7Bonus.StylePriority.UseTextAlignment = false;
            this.lbl7Bonus.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lbl7Bonus.Weight = 0.37954581669351384D;
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.Dpi = 100F;
            this.xrTableCell39.Font = new System.Drawing.Font("Nirmala UI", 10F);
            this.xrTableCell39.Multiline = true;
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.StylePriority.UseFont = false;
            this.xrTableCell39.StylePriority.UseTextAlignment = false;
            this.xrTableCell39.Text = " दोस्रो सिमा १५ %";
            this.xrTableCell39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell39.Weight = 1.1204568391248717D;
            // 
            // lblFifteenPercent
            // 
            this.lblFifteenPercent.Dpi = 100F;
            this.lblFifteenPercent.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblFifteenPercent.Name = "lblFifteenPercent";
            this.lblFifteenPercent.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 3, 0, 0, 100F);
            this.lblFifteenPercent.StylePriority.UseFont = false;
            this.lblFifteenPercent.StylePriority.UsePadding = false;
            this.lblFifteenPercent.StylePriority.UseTextAlignment = false;
            this.lblFifteenPercent.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblFifteenPercent.Weight = 0.37954546509442966D;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.label11,
            this.lbl8HealthExpenses,
            this.xrTableCell43,
            this.lblTwentyFivePercent});
            this.xrTableRow11.Dpi = 100F;
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Weight = 1D;
            // 
            // label11
            // 
            this.label11.Dpi = 100F;
            this.label11.Font = new System.Drawing.Font("Nirmala UI", 10F);
            this.label11.Multiline = true;
            this.label11.Name = "label11";
            this.label11.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.label11.StylePriority.UseFont = false;
            this.label11.StylePriority.UsePadding = false;
            this.label11.StylePriority.UseTextAlignment = false;
            this.label11.Text = "   आ.  औषधी उपचार रू. ";
            this.label11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.label11.Weight = 1.1204518790871847D;
            // 
            // lbl8HealthExpenses
            // 
            this.lbl8HealthExpenses.Dpi = 100F;
            this.lbl8HealthExpenses.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lbl8HealthExpenses.Name = "lbl8HealthExpenses";
            this.lbl8HealthExpenses.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 3, 0, 0, 100F);
            this.lbl8HealthExpenses.StylePriority.UseFont = false;
            this.lbl8HealthExpenses.StylePriority.UsePadding = false;
            this.lbl8HealthExpenses.StylePriority.UseTextAlignment = false;
            this.lbl8HealthExpenses.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lbl8HealthExpenses.Weight = 0.37954581669351384D;
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.Dpi = 100F;
            this.xrTableCell43.Font = new System.Drawing.Font("Nirmala UI", 10F);
            this.xrTableCell43.Multiline = true;
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.xrTableCell43.StylePriority.UseFont = false;
            this.xrTableCell43.StylePriority.UsePadding = false;
            this.xrTableCell43.StylePriority.UseTextAlignment = false;
            this.xrTableCell43.Text = "तेस्रो सिमा  २५ %";
            this.xrTableCell43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell43.Weight = 1.1204568391248717D;
            // 
            // lblTwentyFivePercent
            // 
            this.lblTwentyFivePercent.Dpi = 100F;
            this.lblTwentyFivePercent.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblTwentyFivePercent.Name = "lblTwentyFivePercent";
            this.lblTwentyFivePercent.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 3, 0, 0, 100F);
            this.lblTwentyFivePercent.StylePriority.UseFont = false;
            this.lblTwentyFivePercent.StylePriority.UsePadding = false;
            this.lblTwentyFivePercent.StylePriority.UseTextAlignment = false;
            this.lblTwentyFivePercent.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTwentyFivePercent.Weight = 0.37954546509442966D;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell45,
            this.lbl9LeaveEncashment,
            this.xrTableCell47,
            this.lblThirtyFivePercent});
            this.xrTableRow12.Dpi = 100F;
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.Weight = 1D;
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.Dpi = 100F;
            this.xrTableCell45.Font = new System.Drawing.Font("Nirmala UI", 10F);
            this.xrTableCell45.Multiline = true;
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableCell45.StylePriority.UseFont = false;
            this.xrTableCell45.StylePriority.UsePadding = false;
            this.xrTableCell45.StylePriority.UseTextAlignment = false;
            this.xrTableCell45.Text = "   इ.  बिदा वापत को भुक्तानी रू. ";
            this.xrTableCell45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell45.Weight = 1.1204518790871847D;
            // 
            // lbl9LeaveEncashment
            // 
            this.lbl9LeaveEncashment.Dpi = 100F;
            this.lbl9LeaveEncashment.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lbl9LeaveEncashment.Name = "lbl9LeaveEncashment";
            this.lbl9LeaveEncashment.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 3, 0, 0, 100F);
            this.lbl9LeaveEncashment.StylePriority.UseFont = false;
            this.lbl9LeaveEncashment.StylePriority.UsePadding = false;
            this.lbl9LeaveEncashment.StylePriority.UseTextAlignment = false;
            this.lbl9LeaveEncashment.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lbl9LeaveEncashment.Weight = 0.37954581669351384D;
            // 
            // xrTableCell47
            // 
            this.xrTableCell47.Dpi = 100F;
            this.xrTableCell47.Font = new System.Drawing.Font("Nirmala UI", 10F);
            this.xrTableCell47.Multiline = true;
            this.xrTableCell47.Name = "xrTableCell47";
            this.xrTableCell47.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100F);
            this.xrTableCell47.StylePriority.UseFont = false;
            this.xrTableCell47.StylePriority.UsePadding = false;
            this.xrTableCell47.StylePriority.UseTextAlignment = false;
            this.xrTableCell47.Text = "चौथो सिमा  ३५ %";
            this.xrTableCell47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell47.Weight = 1.1204568391248717D;
            // 
            // lblThirtyFivePercent
            // 
            this.lblThirtyFivePercent.Dpi = 100F;
            this.lblThirtyFivePercent.Font = new System.Drawing.Font("Tahoma", 9F);
            this.lblThirtyFivePercent.Name = "lblThirtyFivePercent";
            this.lblThirtyFivePercent.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 3, 0, 0, 100F);
            this.lblThirtyFivePercent.StylePriority.UseFont = false;
            this.lblThirtyFivePercent.StylePriority.UsePadding = false;
            this.lblThirtyFivePercent.StylePriority.UseTextAlignment = false;
            this.lblThirtyFivePercent.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblThirtyFivePercent.Weight = 0.37954546509442966D;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell49,
            this.lbl10DashainAllowance,
            this.xrTableCell51,
            this.lblTotalTax});
            this.xrTableRow13.Dpi = 100F;
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Weight = 1D;
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.Dpi = 100F;
            this.xrTableCell49.Font = new System.Drawing.Font("Nirmala UI", 10F);
            this.xrTableCell49.Multiline = true;
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableCell49.StylePriority.UseFont = false;
            this.xrTableCell49.StylePriority.UsePadding = false;
            this.xrTableCell49.StylePriority.UseTextAlignment = false;
            this.xrTableCell49.Text = "   ई.  दशैं भत्ता  रू. ";
            this.xrTableCell49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell49.Weight = 1.1204518790871847D;
            // 
            // lbl10DashainAllowance
            // 
            this.lbl10DashainAllowance.Dpi = 100F;
            this.lbl10DashainAllowance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lbl10DashainAllowance.Name = "lbl10DashainAllowance";
            this.lbl10DashainAllowance.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 3, 0, 0, 100F);
            this.lbl10DashainAllowance.StylePriority.UseFont = false;
            this.lbl10DashainAllowance.StylePriority.UsePadding = false;
            this.lbl10DashainAllowance.StylePriority.UseTextAlignment = false;
            this.lbl10DashainAllowance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lbl10DashainAllowance.Weight = 0.37954581669351384D;
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.Dpi = 100F;
            this.xrTableCell51.Font = new System.Drawing.Font("Nirmala UI", 10F);
            this.xrTableCell51.Multiline = true;
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100F);
            this.xrTableCell51.StylePriority.UseFont = false;
            this.xrTableCell51.StylePriority.UsePadding = false;
            this.xrTableCell51.StylePriority.UseTextAlignment = false;
            this.xrTableCell51.Text = "जम्मा लाग्ने कर ";
            this.xrTableCell51.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell51.Weight = 1.1204568391248717D;
            // 
            // lblTotalTax
            // 
            this.lblTotalTax.Dpi = 100F;
            this.lblTotalTax.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblTotalTax.Name = "lblTotalTax";
            this.lblTotalTax.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 3, 0, 0, 100F);
            this.lblTotalTax.StylePriority.UseFont = false;
            this.lblTotalTax.StylePriority.UsePadding = false;
            this.lblTotalTax.StylePriority.UseTextAlignment = false;
            this.lblTotalTax.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTotalTax.Weight = 0.37954546509442966D;
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell53,
            this.lbl11LifeInsuranceIncome,
            this.xrTableCell55,
            this.lblMedicalTax});
            this.xrTableRow14.Dpi = 100F;
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.Weight = 1D;
            // 
            // xrTableCell53
            // 
            this.xrTableCell53.Dpi = 100F;
            this.xrTableCell53.Font = new System.Drawing.Font("Nirmala UI", 10F);
            this.xrTableCell53.Multiline = true;
            this.xrTableCell53.Name = "xrTableCell53";
            this.xrTableCell53.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableCell53.StylePriority.UseFont = false;
            this.xrTableCell53.StylePriority.UsePadding = false;
            this.xrTableCell53.StylePriority.UseTextAlignment = false;
            this.xrTableCell53.Text = "   उ.  जीवन बिमा रू. ";
            this.xrTableCell53.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell53.Weight = 1.1204518790871847D;
            // 
            // lbl11LifeInsuranceIncome
            // 
            this.lbl11LifeInsuranceIncome.Dpi = 100F;
            this.lbl11LifeInsuranceIncome.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lbl11LifeInsuranceIncome.Name = "lbl11LifeInsuranceIncome";
            this.lbl11LifeInsuranceIncome.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 3, 0, 0, 100F);
            this.lbl11LifeInsuranceIncome.StylePriority.UseFont = false;
            this.lbl11LifeInsuranceIncome.StylePriority.UsePadding = false;
            this.lbl11LifeInsuranceIncome.StylePriority.UseTextAlignment = false;
            this.lbl11LifeInsuranceIncome.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lbl11LifeInsuranceIncome.Weight = 0.37954581669351384D;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Dpi = 100F;
            this.xrTableCell55.Font = new System.Drawing.Font("Nirmala UI", 10F);
            this.xrTableCell55.Multiline = true;
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.xrTableCell55.StylePriority.UseFont = false;
            this.xrTableCell55.StylePriority.UsePadding = false;
            this.xrTableCell55.StylePriority.UseTextAlignment = false;
            this.xrTableCell55.Text = "औषधी उपचार खर्च मिलान रू. ";
            this.xrTableCell55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell55.Weight = 1.1204568391248717D;
            // 
            // lblMedicalTax
            // 
            this.lblMedicalTax.Dpi = 100F;
            this.lblMedicalTax.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblMedicalTax.Name = "lblMedicalTax";
            this.lblMedicalTax.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 3, 0, 0, 100F);
            this.lblMedicalTax.StylePriority.UseFont = false;
            this.lblMedicalTax.StylePriority.UsePadding = false;
            this.lblMedicalTax.StylePriority.UseTextAlignment = false;
            this.lblMedicalTax.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblMedicalTax.Weight = 0.37954546509442966D;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell57,
            this.lbl12Reward,
            this.xrTableCell59,
            this.lblFemaleRebate});
            this.xrTableRow15.Dpi = 100F;
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.Weight = 1D;
            // 
            // xrTableCell57
            // 
            this.xrTableCell57.Dpi = 100F;
            this.xrTableCell57.Font = new System.Drawing.Font("Nirmala UI", 10F);
            this.xrTableCell57.Multiline = true;
            this.xrTableCell57.Name = "xrTableCell57";
            this.xrTableCell57.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableCell57.StylePriority.UseFont = false;
            this.xrTableCell57.StylePriority.UsePadding = false;
            this.xrTableCell57.StylePriority.UseTextAlignment = false;
            this.xrTableCell57.Text = "   ऊ.  रिवार्ड रू. ";
            this.xrTableCell57.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell57.Weight = 1.1204518790871847D;
            // 
            // lbl12Reward
            // 
            this.lbl12Reward.Dpi = 100F;
            this.lbl12Reward.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lbl12Reward.Name = "lbl12Reward";
            this.lbl12Reward.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 3, 0, 0, 100F);
            this.lbl12Reward.StylePriority.UseFont = false;
            this.lbl12Reward.StylePriority.UsePadding = false;
            this.lbl12Reward.StylePriority.UseTextAlignment = false;
            this.lbl12Reward.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lbl12Reward.Weight = 0.37954581669351384D;
            // 
            // xrTableCell59
            // 
            this.xrTableCell59.Dpi = 100F;
            this.xrTableCell59.Font = new System.Drawing.Font("Nirmala UI", 10F);
            this.xrTableCell59.Name = "xrTableCell59";
            this.xrTableCell59.StylePriority.UseFont = false;
            this.xrTableCell59.StylePriority.UseTextAlignment = false;
            this.xrTableCell59.Text = " महिला कर छुट ";
            this.xrTableCell59.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell59.Weight = 1.1204568391248717D;
            // 
            // lblFemaleRebate
            // 
            this.lblFemaleRebate.Dpi = 100F;
            this.lblFemaleRebate.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblFemaleRebate.Name = "lblFemaleRebate";
            this.lblFemaleRebate.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 3, 0, 0, 100F);
            this.lblFemaleRebate.StylePriority.UseFont = false;
            this.lblFemaleRebate.StylePriority.UsePadding = false;
            this.lblFemaleRebate.StylePriority.UseTextAlignment = false;
            this.lblFemaleRebate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblFemaleRebate.Weight = 0.37954546509442966D;
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell61,
            this.lbl13ConcessessionalInterest,
            this.xrTableCell63,
            this.xrTableCell64});
            this.xrTableRow16.Dpi = 100F;
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.Weight = 1D;
            // 
            // xrTableCell61
            // 
            this.xrTableCell61.Dpi = 100F;
            this.xrTableCell61.Font = new System.Drawing.Font("Nirmala UI", 10F);
            this.xrTableCell61.Multiline = true;
            this.xrTableCell61.Name = "xrTableCell61";
            this.xrTableCell61.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableCell61.StylePriority.UseFont = false;
            this.xrTableCell61.StylePriority.UsePadding = false;
            this.xrTableCell61.StylePriority.UseTextAlignment = false;
            this.xrTableCell61.Text = "   ए.  ब्याज अन्तर रू. ";
            this.xrTableCell61.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell61.Weight = 1.1204518790871847D;
            // 
            // lbl13ConcessessionalInterest
            // 
            this.lbl13ConcessessionalInterest.Dpi = 100F;
            this.lbl13ConcessessionalInterest.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lbl13ConcessessionalInterest.Name = "lbl13ConcessessionalInterest";
            this.lbl13ConcessessionalInterest.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 3, 0, 0, 100F);
            this.lbl13ConcessessionalInterest.StylePriority.UseFont = false;
            this.lbl13ConcessessionalInterest.StylePriority.UsePadding = false;
            this.lbl13ConcessessionalInterest.StylePriority.UseTextAlignment = false;
            this.lbl13ConcessessionalInterest.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lbl13ConcessessionalInterest.Weight = 0.37954581669351384D;
            // 
            // xrTableCell63
            // 
            this.xrTableCell63.Dpi = 100F;
            this.xrTableCell63.Font = new System.Drawing.Font("Nirmala UI", 10F);
            this.xrTableCell63.Name = "xrTableCell63";
            this.xrTableCell63.StylePriority.UseFont = false;
            this.xrTableCell63.Weight = 1.1204568391248717D;
            // 
            // xrTableCell64
            // 
            this.xrTableCell64.Dpi = 100F;
            this.xrTableCell64.Font = new System.Drawing.Font("Tahoma", 10F);
            this.xrTableCell64.Name = "xrTableCell64";
            this.xrTableCell64.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 3, 0, 0, 100F);
            this.xrTableCell64.StylePriority.UseFont = false;
            this.xrTableCell64.StylePriority.UsePadding = false;
            this.xrTableCell64.StylePriority.UseTextAlignment = false;
            this.xrTableCell64.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell64.Weight = 0.37954546509442966D;
            // 
            // taxDetailsHeaderFormatting
            // 
            this.taxDetailsHeaderFormatting.Condition = "[RowType]=1";
            this.taxDetailsHeaderFormatting.DataMember = "TaxCalculationDetails";
            // 
            // 
            // 
            this.taxDetailsHeaderFormatting.Formatting.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.taxDetailsHeaderFormatting.Formatting.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.taxDetailsHeaderFormatting.Name = "taxDetailsHeaderFormatting";
            // 
            // taxDetailsTotalFormatting
            // 
            this.taxDetailsTotalFormatting.Condition = "[RowType] = 3";
            // 
            // 
            // 
            this.taxDetailsTotalFormatting.Formatting.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.taxDetailsTotalFormatting.Formatting.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.taxDetailsTotalFormatting.Formatting.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.taxDetailsTotalFormatting.Name = "taxDetailsTotalFormatting";
            // 
            // TopMargin
            // 
            this.TopMargin.Dpi = 100F;
            this.TopMargin.HeightF = 10F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 100F;
            this.BottomMargin.HeightF = 10F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.Scripts.OnBeforePrint = "BottomMargin_BeforePrint";
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // reportDataSet1
            // 
            this.reportDataSet1.DataSetName = "ReportDataSet";
            this.reportDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1});
            this.PageHeader.Dpi = 100F;
            this.PageHeader.HeightF = 28.72916F;
            this.PageHeader.Name = "PageHeader";
            // 
            // xrTable1
            // 
            this.xrTable1.Dpi = 100F;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(5F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(790.4186F, 28.72916F);
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell5,
            this.xrTableCell6,
            this.xrTableCell7,
            this.xrTableCell8});
            this.xrTableRow1.Dpi = 100F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.BackColor = System.Drawing.Color.LightGray;
            this.xrTableCell5.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell5.Dpi = 100F;
            this.xrTableCell5.Font = new System.Drawing.Font("Nirmala UI", 10F);
            this.xrTableCell5.Multiline = true;
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100F);
            this.xrTableCell5.StylePriority.UseBackColor = false;
            this.xrTableCell5.StylePriority.UseBorders = false;
            this.xrTableCell5.StylePriority.UseFont = false;
            this.xrTableCell5.StylePriority.UsePadding = false;
            this.xrTableCell5.StylePriority.UseTextAlignment = false;
            this.xrTableCell5.Text = "विवरण ";
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell5.Weight = 1.1204518790871847D;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.BackColor = System.Drawing.Color.LightGray;
            this.xrTableCell6.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell6.Dpi = 100F;
            this.xrTableCell6.Font = new System.Drawing.Font("Nirmala UI", 10F);
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 100F);
            this.xrTableCell6.StylePriority.UseBackColor = false;
            this.xrTableCell6.StylePriority.UseBorders = false;
            this.xrTableCell6.StylePriority.UseFont = false;
            this.xrTableCell6.StylePriority.UsePadding = false;
            this.xrTableCell6.StylePriority.UseTextAlignment = false;
            this.xrTableCell6.Text = "रकम ";
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell6.Weight = 0.37954581669351384D;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.BackColor = System.Drawing.Color.LightGray;
            this.xrTableCell7.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell7.Dpi = 100F;
            this.xrTableCell7.Font = new System.Drawing.Font("Nirmala UI", 10F);
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100F);
            this.xrTableCell7.StylePriority.UseBackColor = false;
            this.xrTableCell7.StylePriority.UseBorders = false;
            this.xrTableCell7.StylePriority.UseFont = false;
            this.xrTableCell7.StylePriority.UsePadding = false;
            this.xrTableCell7.StylePriority.UseTextAlignment = false;
            this.xrTableCell7.Text = "विवरण ";
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell7.Weight = 1.1204568391248717D;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.BackColor = System.Drawing.Color.LightGray;
            this.xrTableCell8.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell8.Dpi = 100F;
            this.xrTableCell8.Font = new System.Drawing.Font("Nirmala UI", 10F);
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 100F);
            this.xrTableCell8.StylePriority.UseBackColor = false;
            this.xrTableCell8.StylePriority.UseBorders = false;
            this.xrTableCell8.StylePriority.UseFont = false;
            this.xrTableCell8.StylePriority.UsePadding = false;
            this.xrTableCell8.StylePriority.UseTextAlignment = false;
            this.xrTableCell8.Text = "रकम ";
            this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell8.Weight = 0.37954546509442966D;
            // 
            // DateTimeStyle
            // 
            this.DateTimeStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.DateTimeStyle.Name = "DateTimeStyle";
            this.DateTimeStyle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // SlipHeaderStyle
            // 
            this.SlipHeaderStyle.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SlipHeaderStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.SlipHeaderStyle.Name = "SlipHeaderStyle";
            this.SlipHeaderStyle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // InfoStyle
            // 
            this.InfoStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.InfoStyle.BorderColor = System.Drawing.Color.White;
            this.InfoStyle.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.InfoStyle.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InfoStyle.ForeColor = System.Drawing.Color.White;
            this.InfoStyle.Name = "InfoStyle";
            this.InfoStyle.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.InfoStyle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // LineStyle
            // 
            this.LineStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.LineStyle.Name = "LineStyle";
            // 
            // ContentLineStyle
            // 
            this.ContentLineStyle.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.ContentLineStyle.Name = "ContentLineStyle";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblTitle,
            this.xrLabel5,
            this.xrLabel4,
            this.xrLabel3,
            this.xrLabel2,
            this.xrLabel1,
            this.labelCompanyName});
            this.ReportHeader.Dpi = 100F;
            this.ReportHeader.HeightF = 263.3333F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // lblTitle
            // 
            this.lblTitle.BackColor = System.Drawing.Color.Transparent;
            this.lblTitle.Dpi = 100F;
            this.lblTitle.Font = new System.Drawing.Font("Nirmala UI", 10F);
            this.lblTitle.ForeColor = System.Drawing.Color.Black;
            this.lblTitle.LocationFloat = new DevExpress.Utils.PointFloat(5F, 207.9166F);
            this.lblTitle.Multiline = true;
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTitle.Scripts.OnBeforePrint = "xrLabel20_BeforePrint";
            this.lblTitle.SizeF = new System.Drawing.SizeF(790.4187F, 45.41675F);
            this.lblTitle.StylePriority.UseBackColor = false;
            this.lblTitle.StylePriority.UseFont = false;
            this.lblTitle.StylePriority.UseForeColor = false;
            this.lblTitle.StylePriority.UseTextAlignment = false;
            this.lblTitle.Text = "मेरो आर्थिक वर्ष ...................... को पारिश्रमिक आय तपसिल बमोजिम को भएको हुद" +
    "ा उक्त आर्थिक वर्ष को कर दाखिला प्रमाण पत्र उपलब्ध गराई पाउँ भनि अनुरोध गर्दछु ।" +
    "";
            this.lblTitle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel5
            // 
            this.xrLabel5.BackColor = System.Drawing.Color.Transparent;
            this.xrLabel5.Dpi = 100F;
            this.xrLabel5.Font = new System.Drawing.Font("Nirmala UI", 10F);
            this.xrLabel5.ForeColor = System.Drawing.Color.Black;
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(5.000051F, 94.99995F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.Scripts.OnBeforePrint = "xrLabel20_BeforePrint";
            this.xrLabel5.SizeF = new System.Drawing.SizeF(295.2087F, 22F);
            this.xrLabel5.StylePriority.UseBackColor = false;
            this.xrLabel5.StylePriority.UseFont = false;
            this.xrLabel5.StylePriority.UseForeColor = false;
            this.xrLabel5.StylePriority.UseTextAlignment = false;
            this.xrLabel5.Text = "मिति:";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel4
            // 
            this.xrLabel4.BackColor = System.Drawing.Color.Transparent;
            this.xrLabel4.Dpi = 100F;
            this.xrLabel4.Font = new System.Drawing.Font("Nirmala UI", 10F);
            this.xrLabel4.ForeColor = System.Drawing.Color.Black;
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(5.000038F, 165.8332F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.Scripts.OnBeforePrint = "xrLabel20_BeforePrint";
            this.xrLabel4.SizeF = new System.Drawing.SizeF(295.2087F, 19.58337F);
            this.xrLabel4.StylePriority.UseBackColor = false;
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.StylePriority.UseForeColor = false;
            this.xrLabel4.StylePriority.UseTextAlignment = false;
            this.xrLabel4.Text = "महोदय ,";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel3
            // 
            this.xrLabel3.BackColor = System.Drawing.Color.Transparent;
            this.xrLabel3.Dpi = 100F;
            this.xrLabel3.Font = new System.Drawing.Font("Nirmala UI", 10F);
            this.xrLabel3.ForeColor = System.Drawing.Color.Black;
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(5.000038F, 131.2499F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.Scripts.OnBeforePrint = "xrLabel20_BeforePrint";
            this.xrLabel3.SizeF = new System.Drawing.SizeF(295.2087F, 19.58336F);
            this.xrLabel3.StylePriority.UseBackColor = false;
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.StylePriority.UseForeColor = false;
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.Text = "बिषय: कर दाखिला प्रमाण पत्र पाउँ ।";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel2
            // 
            this.xrLabel2.BackColor = System.Drawing.Color.Transparent;
            this.xrLabel2.Dpi = 100F;
            this.xrLabel2.Font = new System.Drawing.Font("Nirmala UI", 10F);
            this.xrLabel2.ForeColor = System.Drawing.Color.Black;
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(4.999987F, 53.99999F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.Scripts.OnBeforePrint = "xrLabel20_BeforePrint";
            this.xrLabel2.SizeF = new System.Drawing.SizeF(295.2086F, 22F);
            this.xrLabel2.StylePriority.UseBackColor = false;
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UseForeColor = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "ललितपुर ।";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel1
            // 
            this.xrLabel1.BackColor = System.Drawing.Color.Transparent;
            this.xrLabel1.Dpi = 100F;
            this.xrLabel1.Font = new System.Drawing.Font("Nirmala UI", 10F);
            this.xrLabel1.ForeColor = System.Drawing.Color.Black;
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(4.999987F, 31.99999F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.Scripts.OnBeforePrint = "xrLabel20_BeforePrint";
            this.xrLabel1.SizeF = new System.Drawing.SizeF(295.2086F, 22F);
            this.xrLabel1.StylePriority.UseBackColor = false;
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseForeColor = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "ठुला करदाता कार्यालय, हरिहरभवन,";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // labelCompanyName
            // 
            this.labelCompanyName.BackColor = System.Drawing.Color.Transparent;
            this.labelCompanyName.Dpi = 100F;
            this.labelCompanyName.Font = new System.Drawing.Font("Nirmala UI", 10F);
            this.labelCompanyName.ForeColor = System.Drawing.Color.Black;
            this.labelCompanyName.LocationFloat = new DevExpress.Utils.PointFloat(4.999987F, 10F);
            this.labelCompanyName.Name = "labelCompanyName";
            this.labelCompanyName.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.labelCompanyName.Scripts.OnBeforePrint = "xrLabel20_BeforePrint";
            this.labelCompanyName.SizeF = new System.Drawing.SizeF(295.2086F, 22F);
            this.labelCompanyName.StylePriority.UseBackColor = false;
            this.labelCompanyName.StylePriority.UseFont = false;
            this.labelCompanyName.StylePriority.UseForeColor = false;
            this.labelCompanyName.StylePriority.UseTextAlignment = false;
            this.labelCompanyName.Text = "श्रीमान प्रमुख कर प्रशासकज्यू ,";
            this.labelCompanyName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // NRBIncomeVerificationReport
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.PageHeader,
            this.ReportHeader});
            this.DataMember = "TaxCalculationDetails";
            this.DataSource = this.reportDataSet1;
            this.FormattingRuleSheet.AddRange(new DevExpress.XtraReports.UI.FormattingRule[] {
            this.taxDetailsHeaderFormatting,
            this.taxDetailsTotalFormatting});
            this.Margins = new System.Drawing.Printing.Margins(6, 6, 10, 10);
            this.PageHeight = 1169;
            this.PageWidth = 827;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Scripts.OnBeforePrint = "ReportPaySlip_BeforePrint";
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.DateTimeStyle,
            this.SlipHeaderStyle,
            this.InfoStyle,
            this.LineStyle,
            this.ContentLineStyle});
            this.Version = "16.1";
            this.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.NRBIncomeVerificationReport_BeforePrint);
            ((System.ComponentModel.ISupportInitialize)(this.tableOther)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private ReportDataSet reportDataSet1;
        private ReportPaySlipIncomeDetail reportPaySlipIncomeDetail1;
        private ReportPaySlipAttendanceDetail reportPaySlipAttendanceDetail1;
        private ReportPaySlipDeductionDetail reportPaySlipDeductionDetail1;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.XRControlStyle DateTimeStyle;
        private DevExpress.XtraReports.UI.XRControlStyle SlipHeaderStyle;
        private DevExpress.XtraReports.UI.XRControlStyle InfoStyle;
        private DevExpress.XtraReports.UI.XRControlStyle LineStyle;
        private DevExpress.XtraReports.UI.XRControlStyle ContentLineStyle;
        private DevExpress.XtraReports.UI.FormattingRule taxDetailsHeaderFormatting;
        private DevExpress.XtraReports.UI.FormattingRule taxDetailsTotalFormatting;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell23;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell27;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell31;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell33;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell35;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell37;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell39;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell label11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell43;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell45;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell47;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell49;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell51;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell53;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell55;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell57;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell59;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell61;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell63;
        public DevExpress.XtraReports.UI.XRTableCell lbl1SalaryTalab;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        public DevExpress.XtraReports.UI.XRTableCell lbl2Allowance;
        public DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        public DevExpress.XtraReports.UI.XRTableCell lblPFDeductionAndCITSum;
        public DevExpress.XtraReports.UI.XRTableCell lbl3ProvidentFund;
        public DevExpress.XtraReports.UI.XRTableCell lblInsuranceDeduction;
        public DevExpress.XtraReports.UI.XRTableCell lbl4VehicleFacility;
        public DevExpress.XtraReports.UI.XRTableCell lblRemoteAllowance;
        public DevExpress.XtraReports.UI.XRTableCell lbl5TelephoneFacility;
        public DevExpress.XtraReports.UI.XRTableCell lblTotalDeduction;
        public DevExpress.XtraReports.UI.XRTableCell lbl6HousingAllowance;
        public DevExpress.XtraReports.UI.XRTableCell lblSingleMarriedDeduction;
        public DevExpress.XtraReports.UI.XRTableCell xrTableCell30;
        public DevExpress.XtraReports.UI.XRTableCell lblTotalTaxableIncome;
        public DevExpress.XtraReports.UI.XRTableCell xrTableCell34;
        public DevExpress.XtraReports.UI.XRTableCell lblOnePercent;
        public DevExpress.XtraReports.UI.XRTableCell lbl7Bonus;
        public DevExpress.XtraReports.UI.XRTableCell lblFifteenPercent;
        public DevExpress.XtraReports.UI.XRTableCell lbl8HealthExpenses;
        public DevExpress.XtraReports.UI.XRTableCell lblTwentyFivePercent;
        public DevExpress.XtraReports.UI.XRTableCell lbl9LeaveEncashment;
        public DevExpress.XtraReports.UI.XRTableCell lblThirtyFivePercent;
        public DevExpress.XtraReports.UI.XRTableCell lbl10DashainAllowance;
        public DevExpress.XtraReports.UI.XRTableCell lblTotalTax;
        public DevExpress.XtraReports.UI.XRTableCell lbl11LifeInsuranceIncome;
        public DevExpress.XtraReports.UI.XRTableCell lblMedicalTax;
        public DevExpress.XtraReports.UI.XRTableCell lbl12Reward;
        public DevExpress.XtraReports.UI.XRTableCell lblFemaleRebate;
        public DevExpress.XtraReports.UI.XRTableCell lbl13ConcessessionalInterest;
        public DevExpress.XtraReports.UI.XRTableCell xrTableCell64;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        public DevExpress.XtraReports.UI.XRLabel labelCompanyName;
        public DevExpress.XtraReports.UI.XRLabel xrLabel2;
        public DevExpress.XtraReports.UI.XRLabel xrLabel1;
        public DevExpress.XtraReports.UI.XRLabel xrLabel3;
        public DevExpress.XtraReports.UI.XRLabel xrLabel4;
        public DevExpress.XtraReports.UI.XRLabel xrLabel5;
        public DevExpress.XtraReports.UI.XRLabel lblTitle;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow35;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell107;
        public DevExpress.XtraReports.UI.XRTableCell lblTotalIncome;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell109;
        public DevExpress.XtraReports.UI.XRTableCell lblTotalFinalTax;
        public DevExpress.XtraReports.UI.XRTable tableOther;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        public DevExpress.XtraReports.UI.XRLabel xrLabel6;
        public DevExpress.XtraReports.UI.XRLabel lblEmpNepName;
    }
}
