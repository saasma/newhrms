﻿using System;
using System.Drawing;
using DevExpress.XtraReports.UI;

namespace Web.CP.Report.Templates.Pay.Detail
{
    public partial class ReportPayDetailAdvance : DevExpress.XtraReports.UI.XtraReport
    {
        public ReportPayDetailAdvance()
        {
            InitializeComponent();
        }
        protected void ClearBorderControlEvent(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ReportHelper.TableCell_BeforePrintEvent(sender, e);
        }
        protected void ClearBorderLessControlEvent(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ReportHelper.TableCell_BeforePrintEvent(sender, e);
            ((DevExpress.XtraReports.UI.XRControl)sender).Borders = DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom;
        }

        protected void BeforePrintReport(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ReportHelper.TableCell_BeforePrintEvent(sender, e);
            ((XRLabel)sender).Font = new Font(((XRLabel)sender).Font.FontFamily, ((XRLabel)sender).Font.Size, FontStyle.Bold);
            ((XRLabel)sender).Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0);
        }
    }
}
