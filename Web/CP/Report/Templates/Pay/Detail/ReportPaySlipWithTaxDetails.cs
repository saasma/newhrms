using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using BLL;
using DevExpress.XtraReports.UI;
using Web.ReportDataSetTableAdapters;
using Utils;
using System.Web;
using BLL.Manager;
using DAL;

namespace Web.CP.Report.Templates.Pay.Detail
{
    public partial class ReportPaySlipWithTaxDetails : DevExpress.XtraReports.UI.XtraReport
    {

        protected void ClearBorderControlEvent(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ReportHelper.TableCell_BeforePrintEvent(sender, e);
            ((DevExpress.XtraReports.UI.XRControl)sender).ForeColor = System.Drawing.Color.Black;

        }
        protected void ClearBorderLessControlEvent(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ReportHelper.TableCell_BeforePrintEvent(sender, e);
        }

        public int Month { get; set; }
        public int Year { get; set; }
        public int CompanyId { get; set; }
        public bool IsIncome { get; set; }

        ReportDataSet.Report_Pay_PaySlipDetail_EmpHeaderDataTable incomesTable = null;
        ReportDataSet.Report_Pay_PaySlipDetail_EmpHeaderDataTable deductionTable = null;

        public ReportPaySlipWithTaxDetails()
        {
            InitializeComponent();
        }

        public ReportDataSet.TaxCalculationDetailsDataTable GetDataTable(string data, EEmployee emp)
        {

            string[] rows = data.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

            ReportDataSet.TaxCalculationDetailsDataTable table = new ReportDataSet.TaxCalculationDetailsDataTable();

            foreach (string strRow in rows)
            {
                string[] values = strRow.Split(new char[] { ':' });

                ReportDataSet.TaxCalculationDetailsRow row = table.NewTaxCalculationDetailsRow();

                row.Header = values[0];

                if (values.Length <= 1)
                    continue;


                if (!string.IsNullOrEmpty(values[1]))

                    row["Amount"] = decimal.Parse(values[1]);

                if (values.Length >= 3)
                    row.Note = values[2];

                //try
                //{
                int rowType = 0;
                if (values.Length >= 4 && int.TryParse(values[3], out rowType))
                    row.RowType = int.Parse(values[3]);
                //}
                //catch { }
                if (emp != null)
                {
                    row.EmployeeId = "EIN : " + emp.EmployeeId;
                    row.Name = "Name : " + emp.Name;
                }

                table.AddTaxCalculationDetailsRow(row);
            }




            return table;

        }
        private void xrSubreport1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRSubreport report = (XRSubreport)sender;

            Web.CP.Report.Templates.Pay.Detail.ReportPaySlipWithTaxDetails mainReport =
            (Web.CP.Report.Templates.Pay.Detail.ReportPaySlipWithTaxDetails)report.Report;

            mainReport.SetSubReport(Convert.ToInt32(GetCurrentColumnValue("EIN")), sender);
        }

        public void SetSubReport(int employeeId, object sender)
        {
            XRSubreport subReport = (XRSubreport)sender;

            if (employeeId == 0)
                return;
            switch (subReport.Name)
            {
                case "subReportTaxDetails":

                    Web.CP.Report.Templates.Pay.Detail.ReportTaxCalculationDetailsForPayslip mainReport =
                   (Web.CP.Report.Templates.Pay.Detail.ReportTaxCalculationDetailsForPayslip)subReport.ReportSource;
                        PayrollPeriod payrollPeriod = CommonManager.GetPayrollPeriod(Month, Year);

                        string data = CalculationManager.GetSalaryCalcForTaxDetails(employeeId,
                            SessionManager.CurrentCompanyId, payrollPeriod.PayrollPeriodId);

                        EEmployee employee = EmployeeManager.GetEmployeeById(employeeId);

                        string sex="";
                        if( employee.Gender==0)
                            sex="Female";
                        else if(employee.Gender ==1 )   
                            sex="Male";
                        else
                            sex = "Third Gender";
                       

                        // Append additional information
                        data = string.Format(";Sex::{0}:2", sex) +
                                string.Format(";Status for Tax Calculation::{0}:2",
                                (employee.MaritalStatus =="Married" && employee.HasCoupleTaxStatus.Value ? "Couple" : "Single" )) + data;


                        ReportDataSet.TaxCalculationDetailsDataTable mainTable = GetDataTable(data, employee);


                        //mainReport.Month = payrollPeriod.Month;
                        //mainReport.Year = payrollPeriod.Year.Value;
                        //mainReport.IsIncome = true;
                        //mainReport.CompanyId = SessionManager.CurrentCompanyId;
           
                        mainReport.DataSource = mainTable;
                        mainReport.DataMember = "Report";
                    break;
                case "subReportAttendace":

                    Web.CP.Report.Templates.Pay.Detail.ReportPaySlipAttendanceDetail attendanceReport =
                       (Web.CP.Report.Templates.Pay.Detail.ReportPaySlipAttendanceDetail)subReport.ReportSource;

                
                    ReportDataSet.Report_Pay_PaySlip_AttendanceDataTable attendanceTable =
                     HttpContext.Current.Items["PaySlipAttendanceList"] as ReportDataSet.Report_Pay_PaySlip_AttendanceDataTable;

                    if (attendanceTable != null)
                    {
                        attendanceTable.DefaultView.RowFilter = "EmployeeId = " + employeeId;



                        attendanceReport.DataSource = attendanceTable;
                        attendanceReport.DataMember = "Attendance Report";
                    }
                    break;
                case "subReportIncome":
                    Web.CP.Report.Templates.Pay.Detail.ReportPaySlipIncomeDetail incomeReport =
                        (Web.CP.Report.Templates.Pay.Detail.ReportPaySlipIncomeDetail)subReport.ReportSource;

                    incomesTable = HttpContext.Current.Items["PaySlipIncomeList"] as ReportDataSet.Report_Pay_PaySlipDetail_EmpHeaderDataTable;
                    if (incomesTable != null)
                    {
                        incomesTable.DefaultView.RowFilter = "EIN = " + employeeId;

                        incomeReport.DataSource = incomesTable; 
                        incomeReport.DataMember = "Income Report";
                    }
                    break;
                case "subReportDeduction":
                    Web.CP.Report.Templates.Pay.Detail.ReportPaySlipDeductionDetail deductionReport =
                        (Web.CP.Report.Templates.Pay.Detail.ReportPaySlipDeductionDetail)subReport.ReportSource;

                    deductionTable = HttpContext.Current.Items["PaySlipDeductionList"] as ReportDataSet.Report_Pay_PaySlipDetail_EmpHeaderDataTable;

                    if (deductionTable != null)
                    {
                        deductionTable.DefaultView.RowFilter = "EIN = " + employeeId;



                        deductionReport.DataSource = deductionTable;
                        deductionReport.DataMember = "Deduction Report";
                    }
                    break;                   
            }

            //if (incomesTable != null && deductionTable != null)
            //{
            //    decimal netTotal = (ReportHelper.GetTotal(incomesTable, "Amount") - ReportHelper.GetTotal(deductionTable, "Amount"));
            //    totalLbl.Text = netTotal.ToString("##,###.##");
            //}

        }
       
    }
}
