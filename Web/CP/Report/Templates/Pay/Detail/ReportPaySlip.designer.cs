namespace Web.CP.Report.Templates.Pay.Detail
{
    partial class ReportPaySlip
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrLabel23 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel22 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel21 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.total4 = new DevExpress.XtraReports.UI.XRLabel();
            this.total3 = new DevExpress.XtraReports.UI.XRLabel();
            this.total2 = new DevExpress.XtraReports.UI.XRLabel();
            this.total1 = new DevExpress.XtraReports.UI.XRLabel();
            this.att2 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.att1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
            this.totalLbl = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine6 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine5 = new DevExpress.XtraReports.UI.XRLine();
            this.subReportAttendace = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportDeduction = new DevExpress.XtraReports.UI.XRSubreport();
            this.xrLine3 = new DevExpress.XtraReports.UI.XRLine();
            this.att3 = new DevExpress.XtraReports.UI.XRLine();
            this.subReportIncome = new DevExpress.XtraReports.UI.XRSubreport();
            this.labelMonth = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.xrPageBreak1 = new DevExpress.XtraReports.UI.XRPageBreak();
            this.logo = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrLine8 = new DevExpress.XtraReports.UI.XRLine();
            this.labelCompanyName = new DevExpress.XtraReports.UI.XRLabel();
            this.labelTItle = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.reportDataSet1 = new Web.ReportDataSet();
            this.report_HR_DateOfJoiningTableAdapter = new Web.ReportDataSetTableAdapters.Report_HR_DateOfJoiningTableAdapter();
            this.DateTimeStyle = new DevExpress.XtraReports.UI.XRControlStyle();
            this.SlipHeaderStyle = new DevExpress.XtraReports.UI.XRControlStyle();
            this.InfoStyle = new DevExpress.XtraReports.UI.XRControlStyle();
            this.LineStyle = new DevExpress.XtraReports.UI.XRControlStyle();
            this.ContentLineStyle = new DevExpress.XtraReports.UI.XRControlStyle();
            this.formattingRule1 = new DevExpress.XtraReports.UI.FormattingRule();
            this.report_HR_GetAttendanceDaysTableAdapter = new Web.ReportDataSetTableAdapters.Report_HR_GetAttendanceDaysTableAdapter();
            this.report_Pay_PaySlipDetailTableAdapter = new Web.ReportDataSetTableAdapters.Report_Pay_PaySlipDetailTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.reportDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel23,
            this.xrLabel22,
            this.xrLabel21,
            this.xrLabel20,
            this.xrLabel16,
            this.xrLabel15,
            this.total4,
            this.total3,
            this.total2,
            this.total1,
            this.att2,
            this.xrLabel7,
            this.xrLabel6,
            this.att1,
            this.xrLabel19,
            this.totalLbl,
            this.xrLabel18,
            this.xrLine6,
            this.xrLabel17,
            this.xrLine5,
            this.subReportAttendace,
            this.subReportDeduction,
            this.xrLine3,
            this.att3,
            this.subReportIncome,
            this.labelMonth,
            this.xrLabel9,
            this.xrLabel10,
            this.xrLabel4,
            this.xrLabel5,
            this.xrLabel2,
            this.xrLabel8,
            this.xrLabel1,
            this.xrLine1,
            this.xrPageBreak1,
            this.logo,
            this.xrLine8,
            this.labelCompanyName,
            this.labelTItle});
            this.Detail.HeightF = 671.0417F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.StylePriority.UseBorders = false;
            this.Detail.StylePriority.UseBorderWidth = false;
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel23
            // 
            this.xrLabel23.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel23.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Report_Pay_PaySlipDetail.PfNo")});
            this.xrLabel23.Font = new System.Drawing.Font("Tahoma", 9F);
            this.xrLabel23.LocationFloat = new DevExpress.Utils.PointFloat(125.7084F, 332.6667F);
            this.xrLabel23.Name = "xrLabel23";
            this.xrLabel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel23.SizeF = new System.Drawing.SizeF(226.2915F, 22.99998F);
            this.xrLabel23.StylePriority.UseBorders = false;
            this.xrLabel23.StylePriority.UseFont = false;
            this.xrLabel23.StylePriority.UseTextAlignment = false;
            this.xrLabel23.Text = "xrLabel23";
            this.xrLabel23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel22
            // 
            this.xrLabel22.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel22.LocationFloat = new DevExpress.Utils.PointFloat(8.540141F, 332.6666F);
            this.xrLabel22.Name = "xrLabel22";
            this.xrLabel22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel22.SizeF = new System.Drawing.SizeF(100.0016F, 23.00003F);
            this.xrLabel22.StyleName = "InfoStyle";
            this.xrLabel22.Text = "PF No :";
            this.xrLabel22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel21
            // 
            this.xrLabel21.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel21.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Report_Pay_PaySlipDetail.AccountNo")});
            this.xrLabel21.Font = new System.Drawing.Font("Tahoma", 9F);
            this.xrLabel21.LocationFloat = new DevExpress.Utils.PointFloat(125.7101F, 309.6667F);
            this.xrLabel21.Name = "xrLabel21";
            this.xrLabel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel21.SizeF = new System.Drawing.SizeF(226.2915F, 22.99998F);
            this.xrLabel21.StylePriority.UseBorders = false;
            this.xrLabel21.StylePriority.UseFont = false;
            this.xrLabel21.StylePriority.UseTextAlignment = false;
            this.xrLabel21.Text = "xrLabel21";
            this.xrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel20
            // 
            this.xrLabel20.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel20.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Report_Pay_PaySlipDetail.Bank")});
            this.xrLabel20.Font = new System.Drawing.Font("Tahoma", 9F);
            this.xrLabel20.LocationFloat = new DevExpress.Utils.PointFloat(125.7101F, 286.6665F);
            this.xrLabel20.Name = "xrLabel20";
            this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel20.SizeF = new System.Drawing.SizeF(226.2915F, 22.99998F);
            this.xrLabel20.StylePriority.UseBorders = false;
            this.xrLabel20.StylePriority.UseFont = false;
            this.xrLabel20.StylePriority.UseTextAlignment = false;
            this.xrLabel20.Text = "xrLabel20";
            this.xrLabel20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel16
            // 
            this.xrLabel16.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(8.540141F, 309.6667F);
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel16.SizeF = new System.Drawing.SizeF(100.0016F, 23.00003F);
            this.xrLabel16.StyleName = "InfoStyle";
            this.xrLabel16.Text = "Account No :";
            this.xrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel15
            // 
            this.xrLabel15.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(8.540141F, 286.6665F);
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel15.SizeF = new System.Drawing.SizeF(100.0016F, 23.00003F);
            this.xrLabel15.StyleName = "InfoStyle";
            this.xrLabel15.Text = "Bank :";
            this.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // total4
            // 
            this.total4.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.total4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Report_Pay_PaySlipDetail.DeductionTotal", "{0:N2}")});
            this.total4.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.total4.LocationFloat = new DevExpress.Utils.PointFloat(180.6682F, 564.507F);
            this.total4.Name = "total4";
            this.total4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 4, 0, 0, 100F);
            this.total4.SizeF = new System.Drawing.SizeF(171.3317F, 23.00005F);
            this.total4.StylePriority.UseBorders = false;
            this.total4.StylePriority.UseFont = false;
            this.total4.StylePriority.UsePadding = false;
            this.total4.StylePriority.UseTextAlignment = false;
            this.total4.Text = "total4";
            this.total4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // total3
            // 
            this.total3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.total3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.total3.ForeColor = System.Drawing.Color.Black;
            this.total3.LocationFloat = new DevExpress.Utils.PointFloat(9.998398F, 565.0067F);
            this.total3.Name = "total3";
            this.total3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.total3.SizeF = new System.Drawing.SizeF(124.1683F, 23.00003F);
            this.total3.StyleName = "SlipHeaderStyle";
            this.total3.StylePriority.UseBorders = false;
            this.total3.StylePriority.UseForeColor = false;
            this.total3.StylePriority.UsePadding = false;
            this.total3.StylePriority.UseTextAlignment = false;
            this.total3.Text = "Total Deductions";
            this.total3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // total2
            // 
            this.total2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.total2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Report_Pay_PaySlipDetail.IncomeGross", "{0:N2}")});
            this.total2.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.total2.LocationFloat = new DevExpress.Utils.PointFloat(180.6682F, 484.5849F);
            this.total2.Name = "total2";
            this.total2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 4, 0, 0, 100F);
            this.total2.SizeF = new System.Drawing.SizeF(171.3317F, 23.00005F);
            this.total2.StylePriority.UseBorders = false;
            this.total2.StylePriority.UseFont = false;
            this.total2.StylePriority.UsePadding = false;
            this.total2.StylePriority.UseTextAlignment = false;
            this.total2.Text = "total2";
            this.total2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // total1
            // 
            this.total1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.total1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.total1.ForeColor = System.Drawing.Color.Black;
            this.total1.LocationFloat = new DevExpress.Utils.PointFloat(9.998372F, 485.0849F);
            this.total1.Name = "total1";
            this.total1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.total1.SizeF = new System.Drawing.SizeF(124.1683F, 23.00003F);
            this.total1.StyleName = "SlipHeaderStyle";
            this.total1.StylePriority.UseBorders = false;
            this.total1.StylePriority.UseForeColor = false;
            this.total1.StylePriority.UsePadding = false;
            this.total1.StylePriority.UseTextAlignment = false;
            this.total1.Text = "Total Earnings";
            this.total1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // att2
            // 
            this.att2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.att2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.att2.LocationFloat = new DevExpress.Utils.PointFloat(5.000025F, 392.035F);
            this.att2.Name = "att2";
            this.att2.SizeF = new System.Drawing.SizeF(347F, 5.209991F);
            this.att2.StylePriority.UseBorders = false;
            this.att2.StylePriority.UseForeColor = false;
            this.att2.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClearBorderControlEvent);
            // 
            // xrLabel7
            // 
            this.xrLabel7.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel7.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Report_Pay_PaySlipDetail.IDCardNo")});
            this.xrLabel7.Font = new System.Drawing.Font("Tahoma", 9F);
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(125.7085F, 217.6666F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(226.2916F, 23.00002F);
            this.xrLabel7.StylePriority.UseBorders = false;
            this.xrLabel7.StylePriority.UseFont = false;
            this.xrLabel7.StylePriority.UseTextAlignment = false;
            this.xrLabel7.Text = "xrLabel7";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel6
            // 
            this.xrLabel6.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(8.54009F, 217.6666F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(100.0016F, 23.00002F);
            this.xrLabel6.StyleName = "InfoStyle";
            this.xrLabel6.Text = "EMP Code :";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrLabel6.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClearBorderControlEvent);
            // 
            // att1
            // 
            this.att1.BackColor = System.Drawing.Color.Transparent;
            this.att1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.att1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.att1.LocationFloat = new DevExpress.Utils.PointFloat(10F, 369.035F);
            this.att1.Name = "att1";
            this.att1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.att1.SizeF = new System.Drawing.SizeF(124.1667F, 23F);
            this.att1.StyleName = "SlipHeaderStyle";
            this.att1.StylePriority.UseBackColor = false;
            this.att1.StylePriority.UseBorders = false;
            this.att1.StylePriority.UseTextAlignment = false;
            this.att1.Text = "Attendance";
            this.att1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.att1.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClearBorderControlEvent);
            // 
            // xrLabel19
            // 
            this.xrLabel19.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel19.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel19.ForeColor = System.Drawing.Color.Black;
            this.xrLabel19.LocationFloat = new DevExpress.Utils.PointFloat(10.00005F, 598.6736F);
            this.xrLabel19.Name = "xrLabel19";
            this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel19.SizeF = new System.Drawing.SizeF(124.1666F, 23.00003F);
            this.xrLabel19.StyleName = "SlipHeaderStyle";
            this.xrLabel19.StylePriority.UseBorders = false;
            this.xrLabel19.StylePriority.UseForeColor = false;
            this.xrLabel19.StylePriority.UseTextAlignment = false;
            this.xrLabel19.Text = "Net Paid:";
            this.xrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // totalLbl
            // 
            this.totalLbl.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.totalLbl.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Report_Pay_PaySlipDetail.NetSalary", "{0:N2}")});
            this.totalLbl.Font = new System.Drawing.Font("Tahoma", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalLbl.LocationFloat = new DevExpress.Utils.PointFloat(180.6683F, 598.6736F);
            this.totalLbl.Name = "totalLbl";
            this.totalLbl.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.totalLbl.SizeF = new System.Drawing.SizeF(172.7916F, 23.00003F);
            this.totalLbl.StylePriority.UseBorders = false;
            this.totalLbl.StylePriority.UseFont = false;
            this.totalLbl.StylePriority.UseTextAlignment = false;
            this.totalLbl.Text = "totalLbl";
            this.totalLbl.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel18
            // 
            this.xrLabel18.BackColor = System.Drawing.Color.Transparent;
            this.xrLabel18.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel18.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel18.LocationFloat = new DevExpress.Utils.PointFloat(10F, 427.1667F);
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel18.SizeF = new System.Drawing.SizeF(124.1667F, 23F);
            this.xrLabel18.StyleName = "SlipHeaderStyle";
            this.xrLabel18.StylePriority.UseBackColor = false;
            this.xrLabel18.StylePriority.UseBorders = false;
            this.xrLabel18.StylePriority.UseTextAlignment = false;
            this.xrLabel18.Text = "Earnings";
            this.xrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrLabel18.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClearBorderControlEvent);
            // 
            // xrLine6
            // 
            this.xrLine6.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.xrLine6.LocationFloat = new DevExpress.Utils.PointFloat(5.000025F, 451.1651F);
            this.xrLine6.Name = "xrLine6";
            this.xrLine6.SizeF = new System.Drawing.SizeF(347F, 5.210022F);
            this.xrLine6.StylePriority.UseBorders = false;
            this.xrLine6.StylePriority.UseForeColor = false;
            this.xrLine6.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClearBorderControlEvent);
            // 
            // xrLabel17
            // 
            this.xrLabel17.BackColor = System.Drawing.Color.Transparent;
            this.xrLabel17.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel17.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(11.45828F, 508.085F);
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel17.SizeF = new System.Drawing.SizeF(122.7084F, 23F);
            this.xrLabel17.StyleName = "SlipHeaderStyle";
            this.xrLabel17.StylePriority.UseBackColor = false;
            this.xrLabel17.StylePriority.UseBorders = false;
            this.xrLabel17.StylePriority.UseTextAlignment = false;
            this.xrLabel17.Text = "Deductions";
            this.xrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrLabel17.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClearBorderControlEvent);
            // 
            // xrLine5
            // 
            this.xrLine5.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.xrLine5.LocationFloat = new DevExpress.Utils.PointFloat(5.000025F, 479.3749F);
            this.xrLine5.Name = "xrLine5";
            this.xrLine5.SizeF = new System.Drawing.SizeF(347F, 5.209991F);
            this.xrLine5.StylePriority.UseBorders = false;
            this.xrLine5.StylePriority.UseForeColor = false;
            this.xrLine5.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClearBorderControlEvent);
            // 
            // subReportAttendace
            // 
            this.subReportAttendace.LocationFloat = new DevExpress.Utils.PointFloat(10F, 398.2916F);
            this.subReportAttendace.Name = "subReportAttendace";
            this.subReportAttendace.ReportSource = new Web.CP.Report.Templates.Pay.Detail.ReportPaySlipAttendanceDetail();
            this.subReportAttendace.Scripts.OnBeforePrint = "xrSubreport1_BeforePrint";
            this.subReportAttendace.SizeF = new System.Drawing.SizeF(342F, 22.99995F);
            this.subReportAttendace.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrSubreport1_BeforePrint);
            // 
            // subReportDeduction
            // 
            this.subReportDeduction.LocationFloat = new DevExpress.Utils.PointFloat(11.45828F, 536.295F);
            this.subReportDeduction.Name = "subReportDeduction";
            this.subReportDeduction.ReportSource = new Web.CP.Report.Templates.Pay.Detail.ReportPaySlipDeductionDetail();
            this.subReportDeduction.Scripts.OnBeforePrint = "xrSubreport1_BeforePrint";
            this.subReportDeduction.SizeF = new System.Drawing.SizeF(342.0016F, 22.99997F);
            this.subReportDeduction.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrSubreport1_BeforePrint);
            // 
            // xrLine3
            // 
            this.xrLine3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.xrLine3.LocationFloat = new DevExpress.Utils.PointFloat(6.458308F, 559.2968F);
            this.xrLine3.Name = "xrLine3";
            this.xrLine3.SizeF = new System.Drawing.SizeF(347.0002F, 5.209991F);
            this.xrLine3.StylePriority.UseBorders = false;
            this.xrLine3.StylePriority.UseForeColor = false;
            this.xrLine3.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClearBorderControlEvent);
            // 
            // att3
            // 
            this.att3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.att3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.att3.LocationFloat = new DevExpress.Utils.PointFloat(5.000025F, 421.295F);
            this.att3.Name = "att3";
            this.att3.SizeF = new System.Drawing.SizeF(347.0002F, 5.210022F);
            this.att3.StylePriority.UseBorders = false;
            this.att3.StylePriority.UseForeColor = false;
            this.att3.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClearBorderControlEvent);
            // 
            // subReportIncome
            // 
            this.subReportIncome.LocationFloat = new DevExpress.Utils.PointFloat(10F, 456.3749F);
            this.subReportIncome.Name = "subReportIncome";
            this.subReportIncome.ReportSource = new Web.CP.Report.Templates.Pay.Detail.ReportPaySlipIncomeDetail();
            this.subReportIncome.Scripts.OnBeforePrint = "xrSubreport1_BeforePrint";
            this.subReportIncome.SizeF = new System.Drawing.SizeF(342F, 22.99998F);
            this.subReportIncome.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrSubreport1_BeforePrint);
            // 
            // labelMonth
            // 
            this.labelMonth.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.labelMonth.Font = new System.Drawing.Font("Tahoma", 9F);
            this.labelMonth.LocationFloat = new DevExpress.Utils.PointFloat(125.7085F, 171.6666F);
            this.labelMonth.Name = "labelMonth";
            this.labelMonth.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.labelMonth.SizeF = new System.Drawing.SizeF(226.2915F, 23F);
            this.labelMonth.StylePriority.UseBorders = false;
            this.labelMonth.StylePriority.UseFont = false;
            this.labelMonth.StylePriority.UseTextAlignment = false;
            this.labelMonth.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel9
            // 
            this.xrLabel9.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel9.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Report_Pay_PaySlipDetail.Department")});
            this.xrLabel9.Font = new System.Drawing.Font("Tahoma", 9F);
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(125.7085F, 240.6667F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(226.2916F, 23.00001F);
            this.xrLabel9.StylePriority.UseBorders = false;
            this.xrLabel9.StylePriority.UseFont = false;
            this.xrLabel9.StylePriority.UseTextAlignment = false;
            this.xrLabel9.Text = "xrLabel9";
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrLabel9.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClearBorderControlEvent);
            // 
            // xrLabel10
            // 
            this.xrLabel10.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(8.54009F, 171.6666F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(100.0016F, 23F);
            this.xrLabel10.StyleName = "InfoStyle";
            this.xrLabel10.Text = "Month :";
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrLabel10.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClearBorderControlEvent);
            // 
            // xrLabel4
            // 
            this.xrLabel4.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(8.54009F, 240.6667F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(100.0016F, 23.00002F);
            this.xrLabel4.StyleName = "InfoStyle";
            this.xrLabel4.Text = "Department :";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrLabel4.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClearBorderControlEvent);
            // 
            // xrLabel5
            // 
            this.xrLabel5.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(8.54009F, 263.6665F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(100.0016F, 23.00003F);
            this.xrLabel5.StyleName = "InfoStyle";
            this.xrLabel5.Text = "Designation :";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrLabel5.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClearBorderControlEvent);
            // 
            // xrLabel2
            // 
            this.xrLabel2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Report_Pay_PaySlipDetail.Name")});
            this.xrLabel2.Font = new System.Drawing.Font("Tahoma", 9F);
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(125.7085F, 194.6666F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(226.2931F, 23F);
            this.xrLabel2.StylePriority.UseBorders = false;
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "xrLabel2";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel8
            // 
            this.xrLabel8.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel8.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Report_Pay_PaySlipDetail.Designation")});
            this.xrLabel8.Font = new System.Drawing.Font("Tahoma", 9F);
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(125.7085F, 263.6666F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(226.2915F, 22.99998F);
            this.xrLabel8.StylePriority.UseBorders = false;
            this.xrLabel8.StylePriority.UseFont = false;
            this.xrLabel8.StylePriority.UseTextAlignment = false;
            this.xrLabel8.Text = "xrLabel8";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrLabel8.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClearBorderControlEvent);
            // 
            // xrLabel1
            // 
            this.xrLabel1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(8.54009F, 194.6666F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(100.0016F, 23F);
            this.xrLabel1.StyleName = "InfoStyle";
            this.xrLabel1.Text = "Name :";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrLabel1.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClearBorderControlEvent);
            // 
            // xrLine1
            // 
            this.xrLine1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(6.458308F, 531.0867F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(347.0002F, 5.209991F);
            this.xrLine1.StylePriority.UseBorders = false;
            this.xrLine1.StylePriority.UseForeColor = false;
            this.xrLine1.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClearBorderControlEvent);
            // 
            // xrPageBreak1
            // 
            this.xrPageBreak1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 659.0417F);
            this.xrPageBreak1.Name = "xrPageBreak1";
            this.xrPageBreak1.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClearBorderControlEvent);
            // 
            // logo
            // 
            this.logo.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.logo.LocationFloat = new DevExpress.Utils.PointFloat(6.458333F, 5F);
            this.logo.Name = "logo";
            this.logo.SizeF = new System.Drawing.SizeF(345F, 90F);
            this.logo.Sizing = DevExpress.XtraPrinting.ImageSizeMode.CenterImage;
            this.logo.StylePriority.UseBorders = false;
            this.logo.Visible = false;
            // 
            // xrLine8
            // 
            this.xrLine8.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine8.LineWidth = 2;
            this.xrLine8.LocationFloat = new DevExpress.Utils.PointFloat(0F, 148.3333F);
            this.xrLine8.Name = "xrLine8";
            this.xrLine8.SizeF = new System.Drawing.SizeF(362F, 10.00003F);
            this.xrLine8.StyleName = "LineStyle";
            this.xrLine8.StylePriority.UseBorders = false;
            this.xrLine8.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClearBorderControlEvent);
            // 
            // labelCompanyName
            // 
            this.labelCompanyName.BackColor = System.Drawing.Color.Transparent;
            this.labelCompanyName.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.labelCompanyName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Report_Pay_PaySlipDetail.Branch")});
            this.labelCompanyName.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCompanyName.ForeColor = System.Drawing.Color.Black;
            this.labelCompanyName.LocationFloat = new DevExpress.Utils.PointFloat(6.458333F, 109.1666F);
            this.labelCompanyName.Name = "labelCompanyName";
            this.labelCompanyName.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.labelCompanyName.Scripts.OnBeforePrint = "xrLabel20_BeforePrint";
            this.labelCompanyName.SizeF = new System.Drawing.SizeF(347.0016F, 19.58336F);
            this.labelCompanyName.StyleName = "SlipHeaderStyle";
            this.labelCompanyName.StylePriority.UseBackColor = false;
            this.labelCompanyName.StylePriority.UseBorders = false;
            this.labelCompanyName.StylePriority.UseFont = false;
            this.labelCompanyName.StylePriority.UseForeColor = false;
            this.labelCompanyName.StylePriority.UseTextAlignment = false;
            this.labelCompanyName.Text = "labelCompanyName";
            this.labelCompanyName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // labelTItle
            // 
            this.labelTItle.BackColor = System.Drawing.Color.Transparent;
            this.labelTItle.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.labelTItle.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTItle.LocationFloat = new DevExpress.Utils.PointFloat(0F, 128.75F);
            this.labelTItle.Name = "labelTItle";
            this.labelTItle.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.labelTItle.Scripts.OnBeforePrint = "xrLabel20_BeforePrint";
            this.labelTItle.SizeF = new System.Drawing.SizeF(362F, 19.58336F);
            this.labelTItle.StyleName = "SlipHeaderStyle";
            this.labelTItle.StylePriority.UseBackColor = false;
            this.labelTItle.StylePriority.UseBorders = false;
            this.labelTItle.StylePriority.UseFont = false;
            this.labelTItle.StylePriority.UseTextAlignment = false;
            this.labelTItle.Text = "Payment Slip";
            this.labelTItle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.labelTItle.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClearBorderControlEvent);
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 13.33333F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 67.5416F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.Scripts.OnBeforePrint = "BottomMargin_BeforePrint";
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // reportDataSet1
            // 
            this.reportDataSet1.DataSetName = "ReportDataSet";
            this.reportDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // report_HR_DateOfJoiningTableAdapter
            // 
            this.report_HR_DateOfJoiningTableAdapter.ClearBeforeFill = true;
            // 
            // DateTimeStyle
            // 
            this.DateTimeStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.DateTimeStyle.Name = "DateTimeStyle";
            this.DateTimeStyle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // SlipHeaderStyle
            // 
            this.SlipHeaderStyle.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SlipHeaderStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.SlipHeaderStyle.Name = "SlipHeaderStyle";
            this.SlipHeaderStyle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // InfoStyle
            // 
            this.InfoStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.InfoStyle.BorderColor = System.Drawing.Color.White;
            this.InfoStyle.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.InfoStyle.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InfoStyle.ForeColor = System.Drawing.Color.White;
            this.InfoStyle.Name = "InfoStyle";
            this.InfoStyle.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.InfoStyle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // LineStyle
            // 
            this.LineStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.LineStyle.Name = "LineStyle";
            // 
            // ContentLineStyle
            // 
            this.ContentLineStyle.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.ContentLineStyle.Name = "ContentLineStyle";
            // 
            // formattingRule1
            // 
            this.formattingRule1.Condition = "[NetPay] != 0";
            this.formattingRule1.DataMember = "Report_Pay_PaySlip";
            // 
            // 
            // 
            this.formattingRule1.Formatting.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.formattingRule1.Name = "formattingRule1";
            // 
            // report_HR_GetAttendanceDaysTableAdapter
            // 
            this.report_HR_GetAttendanceDaysTableAdapter.ClearBeforeFill = true;
            // 
            // report_Pay_PaySlipDetailTableAdapter
            // 
            this.report_Pay_PaySlipDetailTableAdapter.ClearBeforeFill = true;
            // 
            // ReportPaySlip
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin});
            this.DataAdapter = this.report_HR_DateOfJoiningTableAdapter;
            this.DataMember = "Report_HR_DateOfJoining";
            this.DataSource = this.reportDataSet1;
            this.FormattingRuleSheet.AddRange(new DevExpress.XtraReports.UI.FormattingRule[] {
            this.formattingRule1});
            this.Margins = new System.Drawing.Printing.Margins(24, 27, 13, 68);
            this.PageHeight = 1169;
            this.PageWidth = 413;
            this.PaperKind = System.Drawing.Printing.PaperKind.Custom;
            this.Scripts.OnBeforePrint = "ReportPaySlip_BeforePrint";
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.DateTimeStyle,
            this.SlipHeaderStyle,
            this.InfoStyle,
            this.LineStyle,
            this.ContentLineStyle});
            this.Version = "12.1";
            ((System.ComponentModel.ISupportInitialize)(this.reportDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private ReportDataSet reportDataSet1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRSubreport subReportIncome;
        private ReportPaySlipIncomeDetail reportPaySlipIncomeDetail1;
        private ReportPaySlipAttendanceDetail reportPaySlipAttendanceDetail1;
        private DevExpress.XtraReports.UI.XRLine xrLine5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel17;
        private Web.ReportDataSetTableAdapters.Report_HR_DateOfJoiningTableAdapter report_HR_DateOfJoiningTableAdapter;
        private DevExpress.XtraReports.UI.XRPageBreak xrPageBreak1;
        private DevExpress.XtraReports.UI.XRLabel totalLbl;
        private DevExpress.XtraReports.UI.XRLabel xrLabel19;
        private DevExpress.XtraReports.UI.XRSubreport subReportDeduction;
        private ReportPaySlipDeductionDetail reportPaySlipDeductionDetail1;
        private DevExpress.XtraReports.UI.XRLine xrLine3;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel18;
        private DevExpress.XtraReports.UI.XRLine xrLine6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRControlStyle DateTimeStyle;
        private DevExpress.XtraReports.UI.XRControlStyle SlipHeaderStyle;
        private DevExpress.XtraReports.UI.XRControlStyle InfoStyle;
        private DevExpress.XtraReports.UI.XRLine xrLine8;
        private DevExpress.XtraReports.UI.XRControlStyle LineStyle;
        private DevExpress.XtraReports.UI.XRControlStyle ContentLineStyle;
        private DevExpress.XtraReports.UI.FormattingRule formattingRule1;
        private ReportDataSetTableAdapters.Report_HR_GetAttendanceDaysTableAdapter report_HR_GetAttendanceDaysTableAdapter;
        public DevExpress.XtraReports.UI.XRLabel labelCompanyName;
        public DevExpress.XtraReports.UI.XRPictureBox logo;
        public DevExpress.XtraReports.UI.XRLabel labelMonth;
        private ReportDataSetTableAdapters.Report_Pay_PaySlipDetailTableAdapter report_Pay_PaySlipDetailTableAdapter;
        public DevExpress.XtraReports.UI.XRLabel labelTItle;
        private DevExpress.XtraReports.UI.XRLabel xrLabel21;
        private DevExpress.XtraReports.UI.XRLabel xrLabel20;
        private DevExpress.XtraReports.UI.XRLabel xrLabel16;
        private DevExpress.XtraReports.UI.XRLabel xrLabel15;
        private DevExpress.XtraReports.UI.XRLabel xrLabel22;
        private DevExpress.XtraReports.UI.XRLabel xrLabel23;
        public DevExpress.XtraReports.UI.XRLabel att1;
        public DevExpress.XtraReports.UI.XRLine att2;
        public DevExpress.XtraReports.UI.XRLine att3;
        public DevExpress.XtraReports.UI.XRLabel total1;
        public DevExpress.XtraReports.UI.XRLabel total2;
        public DevExpress.XtraReports.UI.XRLabel total3;
        public DevExpress.XtraReports.UI.XRLabel total4;
        public DevExpress.XtraReports.UI.XRSubreport subReportAttendace;
    }
}
