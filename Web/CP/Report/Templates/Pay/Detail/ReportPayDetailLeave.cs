using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace Web.CP.Report.Templates.Pay.Detail
{
    public partial class ReportPayDetailLeave : DevExpress.XtraReports.UI.XtraReport
    {
        public ReportPayDetailLeave()
        {
            InitializeComponent();
        }
        protected void ClearBorderControlEvent(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ReportHelper.TableCell_BeforePrintEvent(sender, e);
        }
        protected void ClearBorderLessControlEvent(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ReportHelper.TableCell_BeforePrintEvent(sender, e);
            
        }

    }
}
