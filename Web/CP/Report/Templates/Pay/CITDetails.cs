using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Web.ReportDataSetTableAdapters;
using BLL;

namespace Web.CP.Report.Templates.Pay
{
    public partial class CITDetails : DevExpress.XtraReports.UI.XtraReport
    {
        public CITDetails()
        {
            InitializeComponent();
        }

        public void SetSubReport(object sender)
        {
            ReportHelper.BindCompanyInfo(sender);
        }
        public void TableCell_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (Web.CP.Report.ReportHelper.IsReportExportState())
            {
                ((DevExpress.XtraReports.UI.XRTableCell)sender).BackColor = System.Drawing.Color.Transparent;
                ((DevExpress.XtraReports.UI.XRTableCell)sender).BorderColor = System.Drawing.Color.Black;
            }
        }

        protected void ClearBorderControlEvent(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ReportHelper.TableCell_BeforePrintEvent(sender, e);
        }
        protected void ClearBorderLessControlEvent(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ReportHelper.TableCell_BeforePrintEvent(sender, e);
        }

    }
}
