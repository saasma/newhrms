using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using BLL;
using DevExpress.XtraReports.UI;
using Web.CP.Report.Templates.HR;
using Web.ReportDataSetTableAdapters;
using Utils.Calendar;
using BLL.Base;
using DAL;
using BLL.Manager;
using System.Data;


namespace Web.CP.Report.Templates.Pay
{
    public partial class ReportCITLikeOtherFund : DevExpress.XtraReports.UI.XtraReport
    {
        public ReportCITLikeOtherFund()
        {
            InitializeComponent();
        }

        public void SetSubReport(object sender)
        {
            //XRSubreport subReport = (XRSubreport)sender;

            //if (subReport.Name.Equals("subReportCompanyInfo"))
            //{
            //    Web.CP.Report.Templates.HR.ReportCompanyInfo companyInfoReport =
            //        (Web.CP.Report.Templates.HR.ReportCompanyInfo)subReport.ReportSource;

            //    Report_Company_InfoTableAdapter companyInfoAdapter =
            //        new Report_Company_InfoTableAdapter();

            //    ReportDataSet.Report_Company_InfoDataTable compayInfoTable =
            //        companyInfoAdapter.GetData(SessionManager.CurrentCompanyId); ;

            //    companyInfoReport.DataSource = compayInfoTable;
            //    companyInfoReport.DataMember = "Company Information";
            //}
        }

        public void TableCell_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (Web.CP.Report.ReportHelper.IsReportExportState())
            {
                ((DevExpress.XtraReports.UI.XRTableCell)sender).BackColor = System.Drawing.Color.Transparent;
                ((DevExpress.XtraReports.UI.XRTableCell)sender).BorderColor = System.Drawing.Color.Black;
            }
        }

    }
}
