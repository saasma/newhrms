using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Web.ReportDataSetTableAdapters;
using BLL;

namespace Web.CP.Report.Templates.Pay
{
    public partial class PFStatement : DevExpress.XtraReports.UI.XtraReport
    {
        public PFStatement()
        {
            InitializeComponent();
        }

        public void SetSubReport(object sender)
        {
            ReportHelper.BindCompanyInfo(sender);
        }
    }
}
