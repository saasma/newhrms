using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Web.ReportDataSetTableAdapters;
using BLL;

namespace Web.CP.Report.Templates.Pay
{
    public partial class ReportPayGratuity : DevExpress.XtraReports.UI.XtraReport
    {
        public ReportPayGratuity()
        {
            InitializeComponent();
        }

        public void SetSubReport(object sender)
        {
            XRSubreport subReport = (XRSubreport)sender;

            if (subReport.Name.Equals("subReportCompanyInfo"))
            {
                Web.CP.Report.Templates.HR.ReportCompanyInfo companyInfoReport =
                    (Web.CP.Report.Templates.HR.ReportCompanyInfo)subReport.ReportSource;

                Report_Company_InfoTableAdapter companyInfoAdapter =
                    new Report_Company_InfoTableAdapter();

                BLL.BaseBiz.SetConnectionPwd(companyInfoAdapter.Connection);
                ReportDataSet.Report_Company_InfoDataTable compayInfoTable =
                    companyInfoAdapter.GetData(SessionManager.CurrentCompanyId); ;

                companyInfoReport.DataSource = compayInfoTable;
                companyInfoReport.DataMember = "Company Information";
            }
        }
        //public void TableCell_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        //{
        //    if (Web.CP.Report.ReportHelper.IsReportExportState())
        //    {
        //        ((DevExpress.XtraReports.UI.XRTableCell)sender).BackColor = System.Drawing.Color.Transparent;
        //        ((DevExpress.XtraReports.UI.XRTableCell)sender).BorderColor = System.Drawing.Color.Black;
        //    }
        //}

        protected void ClearBorderControlEvent(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ReportHelper.TableCell_BeforePrintEvent(sender, e);
        }
        protected void ClearBorderLessControlEvent(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ReportHelper.TableCell_BeforePrintEvent(sender, e);
        }

    }
}
