using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using BLL;
using DevExpress.XtraReports.UI;
using Web.ReportDataSetTableAdapters;
using Utils;
using System.Web;
using System.Data;
using Web.CP.Report.Templates.HR;

namespace Web.CP.Report.Templates.Pay.Detail
{
    public partial class ReportOtherEmployeeContainer : DevExpress.XtraReports.UI.XtraReport
    {

        public bool ShowAddress { get; set; }
        public bool ShowHRInformation { get; set; }
        public bool ShowQualification { get; set; }
        public bool ShowSkillSet { get; set; }
        public bool ShowPayInformation { get; set; }

        //public int Month { get; set; }
        //public int Year { get; set; }
        //public int CompanyId { get; set; }
        //public bool IsIncome { get; set; }

        //public void SetSubReport(int employeeId, object sender)
        //{
        //    XRSubreport subReport = (XRSubreport) sender;
        //    Web.CP.Report.Templates.Pay.Detail.ReportPayDetailHeader report =
        //        (Web.CP.Report.Templates.Pay.Detail.ReportPayDetailHeader) subReport.ReportSource;


        //    Report_Pay_PaySlipDetail_EmpHeaderTableAdapter headerAdapter = new Report_Pay_PaySlipDetail_EmpHeaderTableAdapter();


        //    ReportDataSet.Report_Pay_PaySlipDetail_EmpHeaderDataTable incomesTable =
        //       headerAdapter.GetData(this.Month, this.Year,
        //                             SessionManager.CurrentCompanyId, employeeId, true);

        //    report.DataSource  = incomesTable;
        //    report.DataMember = "Report";
            


        //}

  

        public ReportOtherEmployeeContainer()
        {
            InitializeComponent();
        }

        private void subReportIncomes_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRSubreport report = (XRSubreport)sender;


            ReportAllEmployeeDetails mainReport =
            (ReportAllEmployeeDetails)report.ReportSource;

            mainReport.panelAddress.Visible = this.ShowAddress;
            mainReport.panelHR.Visible = this.ShowHRInformation;
            mainReport.panelQualification.Visible = this.ShowQualification;
            mainReport.subReportDocuments.Visible = this.ShowQualification;
            mainReport.subreportTraining.Visible = this.ShowQualification;
            mainReport.subreportEducation.Visible = this.ShowQualification;
            mainReport.subreportPrevEmployee.Visible = this.ShowQualification;

            mainReport.panelSkillSet.Visible = this.ShowSkillSet;
            
            mainReport.panelPayInformation.Visible = this.ShowPayInformation;
            mainReport.subReportSalaryIncrement.Visible = this.ShowPayInformation;

            mainReport.SetSubReport(Convert.ToInt32(GetCurrentColumnValue("EmployeeId")), sender);

        }

        public void SetSubReport(object sender)
        {
            ReportHelper.BindCompanyInfo(sender);
        }

        protected void ClearBorderControlEvent(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ReportHelper.TableCell_BeforePrintEvent(sender, e);
            

        }
        protected void ClearBorderLessControlEvent(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ReportHelper.TableCell_BeforePrintEvent(sender, e);
            ((DevExpress.XtraReports.UI.XRControl)sender).Borders = DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right |  DevExpress.XtraPrinting.BorderSide.Bottom;
        }

        private void B(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ReportHelper.TableCell_BeforePrintEvent(sender, e);
            //((DevExpress.XtraReports.UI.XRControl)sender).Borders = DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom;
        }


        
    }
}
