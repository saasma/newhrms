using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Web;
using System.Data;
using BLL;
using DAL;
using BLL.Manager;
using System.Data.Linq;

namespace Web.CP.Report.Templates.HR
{
    public partial class SanaKisanTA : DevExpress.XtraReports.UI.XtraReport
    {

       

        public SanaKisanTA()
        {
            InitializeComponent();
           

        }

        //public void SetSubReport(object sender)
        //{
        //    ReportHelper.BindCompanyInfo(sender);
        //}

        protected void ClearBorderControlEvent(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ReportHelper.TableCell_BeforePrintEvent(sender, e);
        }
        protected void ClearBorderLessControlEvent(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ReportHelper.TableCell_BeforePrintEvent(sender, e);
        }



        public void SetSubReport(object sender,int reqId)
        {
          

            // first one

            //lblCompanyName.Text = SessionManager.CurrentCompany.Name;
            lblBranch1.Text = SessionManager.CurrentCompany.Address;

            TARequest request = TravelAllowanceManager.getRequestByID(reqId);


            lblNumber1.Text = request.Number;
            lblDate1.Text = BLL.BaseBiz.GetAppropriateDate(request.TravellingFromEng.Value).ToString();
            lblName1.Text = EmployeeManager.GetEmployeeName(request.EmployeeId.Value) +",";
            EEmployee emp = EmployeeManager.GetEmployeeById(request.EmployeeId.Value);
            lblDesignation1.Text = CommonManager.GetDesigId(emp.DesignationId.Value).Name;
            lblEmpBranch1.Text = BranchManager.GetBranchById(emp.BranchId.Value).Name;

            lblPurpose1.Text = request.PurposeOfTravel;
            lblPlace1.Text = request.LocationName;
            lblDepartureDate1.Text = BLL.BaseBiz.GetAppropriateDate(request.TravellingFromEng.Value).ToString();
            lblArrivalDate1.Text = BLL.BaseBiz.GetAppropriateDate(request.TravellingToEng.Value).ToString(); 
            //request.TravellingToEng.Value.ToString("yyyy/MM/dd");
            lblMode1.Text = request.TravelByText.Equals("Other") ? request.OtherTravelByName : request.TravelByText;


            int index = 1;
            foreach (TARequestLine item in request.TARequestLines)
            {
                switch (index)
                {
                    case 1:
                        lblAllowanceFirst1.Text = "(a) " + item.AllowanceName;
                        lblAllowanceFirst1Value.Text = BLL.BaseBiz.GetCurrency(item.Rate);
                        break;
                    case 2:
                        
                        lblAllowanceSecond1.Text = "(b) " + item.AllowanceName;
                        lblAllowanceSecond1Value.Text = BLL.BaseBiz.GetCurrency(item.Rate);
                        break;
                    case 3:
                        lblAllowanceThird1.Text = string.Format(lblAllowanceThird1.Text, item.AllowanceName);
                        lblAllowanceThird1Value.Text = BLL.BaseBiz.GetCurrency(item.Rate);
                        break;
                }

                index += 1;
            }

            lblTotalValue1.Text = BLL.BaseBiz.GetCurrency(request.Total);
            lblCollectAmount1.Text = string.Format(lblCollectAmount1.Text, BLL.BaseBiz.GetCurrency(request.Total));

            int? approvalEmpId = TravelAllowanceManager.GetTravelRequestApprovalEmployeeId(reqId);
            if(approvalEmpId != null)
            {
                EEmployee empApproval = EmployeeManager.GetEmployeeById(approvalEmpId.Value);
                if (empApproval != null)
                {
                    lblApprovalName1.Text = empApproval.Name;
                    lblApprovalDesig1.Text = CommonManager.GetDesigId(empApproval.DesignationId.Value).Name;

                }
            }

            // second one
           
            lblBranch2.Text = SessionManager.CurrentCompany.Address;

            
            lblNumber2.Text = request.Number;
            lblDate2.Text = BLL.BaseBiz.GetAppropriateDate(request.TravellingFromEng.Value).ToString();
            lblName2.Text = EmployeeManager.GetEmployeeName(request.EmployeeId.Value) + ",";
           
            lblDesignation2.Text = CommonManager.GetDesigId(emp.DesignationId.Value).Name;
            lblEmpBranch2.Text = BranchManager.GetBranchById(emp.BranchId.Value).Name;

            lblPurpose2.Text = request.PurposeOfTravel;
            lblPlace2.Text = request.LocationName;
            lblDepartureDate2.Text = BLL.BaseBiz.GetAppropriateDate(request.TravellingFromEng.Value).ToString();
            lblArrivalDate2.Text = BLL.BaseBiz.GetAppropriateDate(request.TravellingToEng.Value).ToString();
            lblMode2.Text = request.TravelByText.Equals("Other") ? request.OtherTravelByName : request.TravelByText;


            index = 1;
            foreach (TARequestLine item in request.TARequestLines)
            {
                switch (index)
                {
                    case 1:
                        lblAllowanceFirst2.Text = "(a) " + item.AllowanceName;
                        lblAllowanceFirst2Value.Text = BLL.BaseBiz.GetCurrency(item.Rate);
                        break;
                    case 2:

                        lblAllowanceSecond2.Text = "(b) " + item.AllowanceName;
                        lblAllowanceSecond2Value.Text = BLL.BaseBiz.GetCurrency(item.Rate);
                        break;
                    case 3:
                        lblAllowanceThird2.Text = string.Format(lblAllowanceThird1.Text, item.AllowanceName);
                        lblAllowanceThird2Value.Text = BLL.BaseBiz.GetCurrency(item.Rate);
                        break;
                }

                index += 1;
            }

            lblTotalValue2.Text = BLL.BaseBiz.GetCurrency(request.Total);
            lblCollectAmount2.Text = string.Format(lblCollectAmount1.Text, BLL.BaseBiz.GetCurrency(request.Total));

            approvalEmpId = TravelAllowanceManager.GetTravelRequestApprovalEmployeeId(reqId);
            if (approvalEmpId != null)
            {
                EEmployee empApproval = EmployeeManager.GetEmployeeById(approvalEmpId.Value);
                if (empApproval != null)
                {
                    lblApprovalName2.Text = empApproval.Name;
                    lblApprovalDesig2.Text = CommonManager.GetDesigId(empApproval.DesignationId.Value).Name;
                }
            }
        }










    }
}
