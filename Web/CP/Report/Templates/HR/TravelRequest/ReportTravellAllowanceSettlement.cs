using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Web;
using System.Data;
using BLL;

namespace Web.CP.Report.Templates.HR
{
    public partial class ReportTravellAllowanceSettlement : DevExpress.XtraReports.UI.XtraReport
      
      {
        public ReportTravellAllowanceSettlement()
        {
            InitializeComponent();
            lblCompanyName.Text = SessionManager.CurrentCompany.Name;
            lblAddress.Text = SessionManager.CurrentCompany.Address;
          
        }

        //public void SetSubReport(object sender)
        //{
        //    ReportHelper.BindCompanyInfo(sender);
        //}

        protected void ClearBorderControlEvent(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ReportHelper.TableCell_BeforePrintEvent(sender, e);
        }
        protected void ClearBorderLessControlEvent(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ReportHelper.TableCell_BeforePrintEvent(sender, e);
        }

 

        public void SetSubReport(object sender)
        {
            XRSubreport subReport = (XRSubreport)sender;

            //ReportDataSet.GetTimeAllocationInfoByWeekDataTable _TimeAllocationInfoTable = new ReportDataSet.GetTimeAllocationInfoByWeekDataTable();
            //foreach (DataRow dr in ((System.Data.DataTable)(HttpContext.Current.Items["PARTimeAllocationInfo"])).DefaultView.Table.Rows)
            //{
            //    _TimeAllocationInfoTable.ImportRow(dr);
            //}

           // ReportDataSet.Report_TravellAllowanceSettlementPhaseIDataTable _TravelRequestAdvanceSub1 = new ReportDataSet.Report_TravellAllowanceSettlementPhaseIDataTable();


            switch (subReport.Name)
            {
                case "subReportTravellAllowance1":

                    Web.CP.Report.Templates.HR.ReportTravellAllowanceSub1 sub1 =
                       (Web.CP.Report.Templates.HR.ReportTravellAllowanceSub1)subReport.ReportSource;

                   // if (_TimeAllocationInfoTable != null)
                    //{
                        sub1.DataSource = HttpContext.Current.Items["dtTravellAllowancePhaseI"];
                        sub1.DataMember = "Travel Report";

                     
                    //}
                    break;


                case "SubreportTravellAllowance2":

                    Web.CP.Report.Templates.HR.ReportTravellAllowanceSub2 _ReportTravellAllowanceSub2 =
                       (Web.CP.Report.Templates.HR.ReportTravellAllowanceSub2)subReport.ReportSource;

                    _ReportTravellAllowanceSub2.DataSource = HttpContext.Current.Items["dtTravellAllowancePhaseII"];
                    _ReportTravellAllowanceSub2.DataMember = "Travel Report";

                    break;

                case "SubreportTravellAllowance3":

                    Web.CP.Report.Templates.HR.ReportTravellAllowanceSub3 _ReportTravellAllowanceSub3 =
                       (Web.CP.Report.Templates.HR.ReportTravellAllowanceSub3)subReport.ReportSource;

                    _ReportTravellAllowanceSub3.DataSource = HttpContext.Current.Items["dtTravellAllowancePhaseIII"];
                    _ReportTravellAllowanceSub3.DataMember = "Travel Report";


                    break;


            }

            int requestId = int.Parse(HttpContext.Current.Items["RequestId"].ToString());

            DAL.TARequest dbObj = BLL.Manager.TravelAllowanceManager.getRequestByID(requestId);
            if (dbObj != null)
            {
                int status = dbObj.Status.Value;
                // if no advance set then hide second and third grids
                if (dbObj.AdvanceStatus == null)
                {
                    SubreportTravellAllowance2.Visible = false;
                    SubreportTravellAllowance3.Visible = false;
                    xrTable2.Visible = false;
                }
                else if (status == 0 || status == 1)
                {
                    SubreportTravellAllowance2.Visible = false;
                    SubreportTravellAllowance3.Visible = false;
                    xrTable2.Visible = false;
                }
                else if (status < 15)
                {
                    SubreportTravellAllowance3.Visible = false;
                    xrTable2.Visible = false;
                }
            }

        }

        private void subReportTravellAllowance1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRSubreport report = (XRSubreport)sender;

            Web.CP.Report.Templates.HR.ReportTravellAllowanceSettlement mainReport =
            (Web.CP.Report.Templates.HR.ReportTravellAllowanceSettlement)report.Report;

            mainReport.SetSubReport(sender);
        }

        private void SubreportTravellAllowance2_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRSubreport report = (XRSubreport)sender;

            Web.CP.Report.Templates.HR.ReportTravellAllowanceSettlement mainReport =
            (Web.CP.Report.Templates.HR.ReportTravellAllowanceSettlement)report.Report;
            mainReport.SetSubReport(sender);
        }

        private void SubreportTravellAllowance3_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRSubreport report = (XRSubreport)sender;

            Web.CP.Report.Templates.HR.ReportTravellAllowanceSettlement mainReport =
            (Web.CP.Report.Templates.HR.ReportTravellAllowanceSettlement)report.Report;
            mainReport.SetSubReport(sender);

            
        }


      


     


       


      
    }
}
