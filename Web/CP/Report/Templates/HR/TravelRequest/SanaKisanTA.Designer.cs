namespace Web.CP.Report.Templates.HR
{
    partial class SanaKisanTA
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SanaKisanTA));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.reportDataSet1 = new Web.ReportDataSet();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.lblPlace2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPurpose2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDepartureDate2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMode2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblArrivalDate2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblAllowanceFirst2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel35 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblAllowanceSecond2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel39 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblAllowanceThird2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel51 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel50 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblApprovalName2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel54 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblApprovalDesig2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblAllowanceSecond2Value = new DevExpress.XtraReports.UI.XRLabel();
            this.lblAllowanceFirst2Value = new DevExpress.XtraReports.UI.XRLabel();
            this.lblAllowanceThird2Value = new DevExpress.XtraReports.UI.XRLabel();
            this.lblCollectAmount2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTotalValue2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDate2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel22 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel24 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel23 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.lblBranch2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNumber2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel31 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel30 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel32 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel34 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel33 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDesignation2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblName2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel27 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel29 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblEmpBranch2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblApprovalDesig1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblApprovalName1 = new DevExpress.XtraReports.UI.XRLabel();
            this.test = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblCollectAmount1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTotalValue1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblAllowanceThird1Value = new DevExpress.XtraReports.UI.XRLabel();
            this.lblAllowanceSecond1Value = new DevExpress.XtraReports.UI.XRLabel();
            this.lblAllowanceFirst1Value = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMode1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblArrivalDate1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDepartureDate1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPlace1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPurpose1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTotal = new DevExpress.XtraReports.UI.XRLabel();
            this.lblAllowanceThird1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblAllowanceSecond1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblAllowanceFirst1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblEmpBranch1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDesignation1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblName1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDate1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNumber1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblBranch1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblCompanyName = new DevExpress.XtraReports.UI.XRLabel();
            this.logo = new DevExpress.XtraReports.UI.XRPictureBox();
            this.report_HR_DateOfJoiningTableAdapter = new Web.ReportDataSetTableAdapters.Report_HR_DateOfJoiningTableAdapter();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.DateStyle = new DevExpress.XtraReports.UI.XRControlStyle();
            this.report_HR_GetAttendanceDaysTableAdapter = new Web.ReportDataSetTableAdapters.Report_HR_GetAttendanceDaysTableAdapter();
            this.PayBankEven = new DevExpress.XtraReports.UI.XRControlStyle();
            this.PayBankOdd = new DevExpress.XtraReports.UI.XRControlStyle();
            this.PayCashEven = new DevExpress.XtraReports.UI.XRControlStyle();
            this.PayCashOdd = new DevExpress.XtraReports.UI.XRControlStyle();
            this.ReportHeaderStyle = new DevExpress.XtraReports.UI.XRControlStyle();
            this.GrossTotalStyle = new DevExpress.XtraReports.UI.XRControlStyle();
            this.advanceEven = new DevExpress.XtraReports.UI.XRControlStyle();
            this.advanceOdd = new DevExpress.XtraReports.UI.XRControlStyle();
            this.xrControlStyle1 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.TestStyle = new DevExpress.XtraReports.UI.XRControlStyle();
            this.DataStyle = new DevExpress.XtraReports.UI.XRControlStyle();
            this.LineStyle = new DevExpress.XtraReports.UI.XRControlStyle();
            this.ReportStyle = new DevExpress.XtraReports.UI.XRControlStyle();
            this.StyleDate = new DevExpress.XtraReports.UI.XRControlStyle();
            this.AttendanceHeaderStyle = new DevExpress.XtraReports.UI.XRControlStyle();
            this.xrControlStyle2 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.EvenStyle = new DevExpress.XtraReports.UI.XRControlStyle();
            this.OddStyle = new DevExpress.XtraReports.UI.XRControlStyle();
            this.AttributeHeaderStyle = new DevExpress.XtraReports.UI.XRControlStyle();
            this.reportTravellAllowanceSub31 = new Web.CP.Report.Templates.HR.ReportTravellAllowanceSub3();
            ((System.ComponentModel.ISupportInitialize)(this.reportDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportTravellAllowanceSub31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.HeightF = 0F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 0F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 0F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // reportDataSet1
            // 
            this.reportDataSet1.DataSetName = "ReportDataSet";
            this.reportDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblPlace2,
            this.lblPurpose2,
            this.lblDepartureDate2,
            this.lblMode2,
            this.lblArrivalDate2,
            this.lblAllowanceFirst2,
            this.xrLabel35,
            this.lblAllowanceSecond2,
            this.xrLabel39,
            this.lblAllowanceThird2,
            this.xrLabel51,
            this.xrLabel50,
            this.lblApprovalName2,
            this.xrLabel54,
            this.lblApprovalDesig2,
            this.lblAllowanceSecond2Value,
            this.lblAllowanceFirst2Value,
            this.lblAllowanceThird2Value,
            this.lblCollectAmount2,
            this.lblTotalValue2,
            this.lblDate2,
            this.xrLabel20,
            this.xrLabel22,
            this.xrLabel24,
            this.xrLabel23,
            this.xrLabel15,
            this.xrPictureBox1,
            this.lblBranch2,
            this.lblNumber2,
            this.xrLabel18,
            this.xrLabel31,
            this.xrLabel30,
            this.xrLabel32,
            this.xrLabel34,
            this.xrLabel33,
            this.lblDesignation2,
            this.lblName2,
            this.xrLabel27,
            this.xrLabel29,
            this.lblEmpBranch2,
            this.xrLine1,
            this.xrLabel16,
            this.lblApprovalDesig1,
            this.lblApprovalName1,
            this.test,
            this.xrLabel14,
            this.lblCollectAmount1,
            this.lblTotalValue1,
            this.lblAllowanceThird1Value,
            this.lblAllowanceSecond1Value,
            this.lblAllowanceFirst1Value,
            this.lblMode1,
            this.lblArrivalDate1,
            this.lblDepartureDate1,
            this.lblPlace1,
            this.lblPurpose1,
            this.lblTotal,
            this.lblAllowanceThird1,
            this.lblAllowanceSecond1,
            this.lblAllowanceFirst1,
            this.xrLabel13,
            this.xrLabel12,
            this.xrLabel11,
            this.xrLabel10,
            this.xrLabel9,
            this.xrLabel8,
            this.xrLabel7,
            this.lblEmpBranch1,
            this.xrLabel6,
            this.lblDesignation1,
            this.lblName1,
            this.xrLabel5,
            this.xrLabel4,
            this.xrLabel2,
            this.lblDate1,
            this.xrLabel3,
            this.lblNumber1,
            this.xrLabel1,
            this.lblBranch1,
            this.lblCompanyName,
            this.logo});
            this.ReportHeader.HeightF = 1077.877F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // lblPlace2
            // 
            this.lblPlace2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlace2.LocationFloat = new DevExpress.Utils.PointFloat(317.6722F, 801.1788F);
            this.lblPlace2.Name = "lblPlace2";
            this.lblPlace2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPlace2.SizeF = new System.Drawing.SizeF(476.4949F, 18.16666F);
            this.lblPlace2.StylePriority.UseFont = false;
            this.lblPlace2.StylePriority.UseTextAlignment = false;
            this.lblPlace2.Text = "lblPlace1";
            this.lblPlace2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblPurpose2
            // 
            this.lblPurpose2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPurpose2.LocationFloat = new DevExpress.Utils.PointFloat(317.6723F, 783.0122F);
            this.lblPurpose2.Name = "lblPurpose2";
            this.lblPurpose2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPurpose2.SizeF = new System.Drawing.SizeF(476.4948F, 18.16666F);
            this.lblPurpose2.StylePriority.UseFont = false;
            this.lblPurpose2.StylePriority.UseTextAlignment = false;
            this.lblPurpose2.Text = "lblPurpose1";
            this.lblPurpose2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblDepartureDate2
            // 
            this.lblDepartureDate2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDepartureDate2.LocationFloat = new DevExpress.Utils.PointFloat(317.6722F, 819.3455F);
            this.lblDepartureDate2.Name = "lblDepartureDate2";
            this.lblDepartureDate2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDepartureDate2.SizeF = new System.Drawing.SizeF(182.9703F, 18.16666F);
            this.lblDepartureDate2.StylePriority.UseFont = false;
            this.lblDepartureDate2.StylePriority.UseTextAlignment = false;
            this.lblDepartureDate2.Text = "lblDepartureDate1";
            this.lblDepartureDate2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblMode2
            // 
            this.lblMode2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMode2.LocationFloat = new DevExpress.Utils.PointFloat(317.6723F, 837.5123F);
            this.lblMode2.Name = "lblMode2";
            this.lblMode2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMode2.SizeF = new System.Drawing.SizeF(182.9701F, 18.16666F);
            this.lblMode2.StylePriority.UseFont = false;
            this.lblMode2.StylePriority.UseTextAlignment = false;
            this.lblMode2.Text = "lblMode1";
            this.lblMode2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblArrivalDate2
            // 
            this.lblArrivalDate2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblArrivalDate2.LocationFloat = new DevExpress.Utils.PointFloat(623.3146F, 819.3455F);
            this.lblArrivalDate2.Name = "lblArrivalDate2";
            this.lblArrivalDate2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblArrivalDate2.SizeF = new System.Drawing.SizeF(170.8524F, 18.16666F);
            this.lblArrivalDate2.StylePriority.UseFont = false;
            this.lblArrivalDate2.StylePriority.UseTextAlignment = false;
            this.lblArrivalDate2.Text = "lblArrivalDate1";
            this.lblArrivalDate2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblAllowanceFirst2
            // 
            this.lblAllowanceFirst2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAllowanceFirst2.LocationFloat = new DevExpress.Utils.PointFloat(317.6723F, 855.679F);
            this.lblAllowanceFirst2.Name = "lblAllowanceFirst2";
            this.lblAllowanceFirst2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblAllowanceFirst2.SizeF = new System.Drawing.SizeF(182.9702F, 18.16666F);
            this.lblAllowanceFirst2.StylePriority.UseFont = false;
            this.lblAllowanceFirst2.StylePriority.UseTextAlignment = false;
            this.lblAllowanceFirst2.Text = "(a) Travel allowance (NPR) ";
            this.lblAllowanceFirst2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel35
            // 
            this.xrLabel35.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel35.LocationFloat = new DevExpress.Utils.PointFloat(153.3335F, 855.679F);
            this.xrLabel35.Name = "xrLabel35";
            this.xrLabel35.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel35.SizeF = new System.Drawing.SizeF(164.3388F, 18.16667F);
            this.xrLabel35.StylePriority.UseFont = false;
            this.xrLabel35.StylePriority.UseTextAlignment = false;
            this.xrLabel35.Text = "(6) Travel cost estimate:";
            this.xrLabel35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblAllowanceSecond2
            // 
            this.lblAllowanceSecond2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAllowanceSecond2.LocationFloat = new DevExpress.Utils.PointFloat(317.6723F, 873.8456F);
            this.lblAllowanceSecond2.Name = "lblAllowanceSecond2";
            this.lblAllowanceSecond2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblAllowanceSecond2.SizeF = new System.Drawing.SizeF(182.9702F, 18.16666F);
            this.lblAllowanceSecond2.StylePriority.UseFont = false;
            this.lblAllowanceSecond2.StylePriority.UseTextAlignment = false;
            this.lblAllowanceSecond2.Text = "(b) Travel allowance (NPR) ";
            this.lblAllowanceSecond2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel39
            // 
            this.xrLabel39.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel39.LocationFloat = new DevExpress.Utils.PointFloat(317.6723F, 910.179F);
            this.xrLabel39.Name = "xrLabel39";
            this.xrLabel39.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel39.SizeF = new System.Drawing.SizeF(182.9702F, 18.16666F);
            this.xrLabel39.StylePriority.UseFont = false;
            this.xrLabel39.StylePriority.UseTextAlignment = false;
            this.xrLabel39.Text = "(d) Total (NPR)";
            this.xrLabel39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblAllowanceThird2
            // 
            this.lblAllowanceThird2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAllowanceThird2.LocationFloat = new DevExpress.Utils.PointFloat(317.6723F, 892.0123F);
            this.lblAllowanceThird2.Name = "lblAllowanceThird2";
            this.lblAllowanceThird2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblAllowanceThird2.SizeF = new System.Drawing.SizeF(182.9702F, 18.16666F);
            this.lblAllowanceThird2.StylePriority.UseFont = false;
            this.lblAllowanceThird2.StylePriority.UseTextAlignment = false;
            this.lblAllowanceThird2.Text = "(c) Others ({0}) ";
            this.lblAllowanceThird2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel51
            // 
            this.xrLabel51.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel51.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel51.LocationFloat = new DevExpress.Utils.PointFloat(50.83065F, 990.3456F);
            this.xrLabel51.Name = "xrLabel51";
            this.xrLabel51.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel51.SizeF = new System.Drawing.SizeF(250.1722F, 18.16667F);
            this.xrLabel51.StylePriority.UseBorders = false;
            this.xrLabel51.StylePriority.UseFont = false;
            this.xrLabel51.StylePriority.UseTextAlignment = false;
            this.xrLabel51.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel50
            // 
            this.xrLabel50.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel50.LocationFloat = new DevExpress.Utils.PointFloat(51.00016F, 960.3458F);
            this.xrLabel50.Name = "xrLabel50";
            this.xrLabel50.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel50.SizeF = new System.Drawing.SizeF(250.1722F, 18.16667F);
            this.xrLabel50.StylePriority.UseFont = false;
            this.xrLabel50.StylePriority.UseTextAlignment = false;
            this.xrLabel50.Text = "Sincerely ,";
            this.xrLabel50.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblApprovalName2
            // 
            this.lblApprovalName2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblApprovalName2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblApprovalName2.LocationFloat = new DevExpress.Utils.PointFloat(50.83065F, 1008.512F);
            this.lblApprovalName2.Name = "lblApprovalName2";
            this.lblApprovalName2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblApprovalName2.SizeF = new System.Drawing.SizeF(250.1722F, 18.16667F);
            this.lblApprovalName2.StylePriority.UseBorders = false;
            this.lblApprovalName2.StylePriority.UseFont = false;
            this.lblApprovalName2.StylePriority.UseTextAlignment = false;
            this.lblApprovalName2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel54
            // 
            this.xrLabel54.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel54.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel54.LocationFloat = new DevExpress.Utils.PointFloat(51.00016F, 1044.846F);
            this.xrLabel54.Name = "xrLabel54";
            this.xrLabel54.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel54.SizeF = new System.Drawing.SizeF(250.1722F, 18.16667F);
            this.xrLabel54.StylePriority.UseBorders = false;
            this.xrLabel54.StylePriority.UseFont = false;
            this.xrLabel54.StylePriority.UseTextAlignment = false;
            this.xrLabel54.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblApprovalDesig2
            // 
            this.lblApprovalDesig2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblApprovalDesig2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblApprovalDesig2.LocationFloat = new DevExpress.Utils.PointFloat(50.83065F, 1026.679F);
            this.lblApprovalDesig2.Name = "lblApprovalDesig2";
            this.lblApprovalDesig2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblApprovalDesig2.SizeF = new System.Drawing.SizeF(250.1722F, 18.16667F);
            this.lblApprovalDesig2.StylePriority.UseBorders = false;
            this.lblApprovalDesig2.StylePriority.UseFont = false;
            this.lblApprovalDesig2.StylePriority.UseTextAlignment = false;
            this.lblApprovalDesig2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblAllowanceSecond2Value
            // 
            this.lblAllowanceSecond2Value.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAllowanceSecond2Value.LocationFloat = new DevExpress.Utils.PointFloat(500.6425F, 873.8456F);
            this.lblAllowanceSecond2Value.Name = "lblAllowanceSecond2Value";
            this.lblAllowanceSecond2Value.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblAllowanceSecond2Value.SizeF = new System.Drawing.SizeF(182.9702F, 18.16666F);
            this.lblAllowanceSecond2Value.StylePriority.UseFont = false;
            this.lblAllowanceSecond2Value.StylePriority.UseTextAlignment = false;
            this.lblAllowanceSecond2Value.Text = "lblAllowanceSecond1Value";
            this.lblAllowanceSecond2Value.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblAllowanceFirst2Value
            // 
            this.lblAllowanceFirst2Value.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAllowanceFirst2Value.LocationFloat = new DevExpress.Utils.PointFloat(500.6425F, 855.679F);
            this.lblAllowanceFirst2Value.Name = "lblAllowanceFirst2Value";
            this.lblAllowanceFirst2Value.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblAllowanceFirst2Value.SizeF = new System.Drawing.SizeF(182.9702F, 18.16666F);
            this.lblAllowanceFirst2Value.StylePriority.UseFont = false;
            this.lblAllowanceFirst2Value.StylePriority.UseTextAlignment = false;
            this.lblAllowanceFirst2Value.Text = "lblAllowanceFirst1Value";
            this.lblAllowanceFirst2Value.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblAllowanceThird2Value
            // 
            this.lblAllowanceThird2Value.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAllowanceThird2Value.LocationFloat = new DevExpress.Utils.PointFloat(500.6425F, 892.0121F);
            this.lblAllowanceThird2Value.Name = "lblAllowanceThird2Value";
            this.lblAllowanceThird2Value.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblAllowanceThird2Value.SizeF = new System.Drawing.SizeF(182.9702F, 18.16666F);
            this.lblAllowanceThird2Value.StylePriority.UseFont = false;
            this.lblAllowanceThird2Value.StylePriority.UseTextAlignment = false;
            this.lblAllowanceThird2Value.Text = "lblAllowanceThird1Value";
            this.lblAllowanceThird2Value.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblCollectAmount2
            // 
            this.lblCollectAmount2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCollectAmount2.LocationFloat = new DevExpress.Utils.PointFloat(153.503F, 942.1791F);
            this.lblCollectAmount2.Name = "lblCollectAmount2";
            this.lblCollectAmount2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblCollectAmount2.SizeF = new System.Drawing.SizeF(640.6641F, 18.16666F);
            this.lblCollectAmount2.StylePriority.UseFont = false;
            this.lblCollectAmount2.StylePriority.UseTextAlignment = false;
            this.lblCollectAmount2.Text = "(7) Please collect NPR {0}, as travel advance from account section.";
            this.lblCollectAmount2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblTotalValue2
            // 
            this.lblTotalValue2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalValue2.LocationFloat = new DevExpress.Utils.PointFloat(500.6425F, 910.179F);
            this.lblTotalValue2.Name = "lblTotalValue2";
            this.lblTotalValue2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTotalValue2.SizeF = new System.Drawing.SizeF(182.9702F, 18.16666F);
            this.lblTotalValue2.StylePriority.UseFont = false;
            this.lblTotalValue2.StylePriority.UseTextAlignment = false;
            this.lblTotalValue2.Text = "lblTotalValue1";
            this.lblTotalValue2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblDate2
            // 
            this.lblDate2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDate2.LocationFloat = new DevExpress.Utils.PointFloat(644.9973F, 634.3923F);
            this.lblDate2.Name = "lblDate2";
            this.lblDate2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDate2.SizeF = new System.Drawing.SizeF(149.1696F, 14F);
            this.lblDate2.StylePriority.UseFont = false;
            this.lblDate2.StylePriority.UseTextAlignment = false;
            this.lblDate2.Text = "Date";
            this.lblDate2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel20
            // 
            this.xrLabel20.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel20.LocationFloat = new DevExpress.Utils.PointFloat(603.3307F, 634.3923F);
            this.xrLabel20.Name = "xrLabel20";
            this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel20.SizeF = new System.Drawing.SizeF(41.66663F, 14F);
            this.xrLabel20.StylePriority.UseFont = false;
            this.xrLabel20.StylePriority.UseTextAlignment = false;
            this.xrLabel20.Text = "Date:";
            this.xrLabel20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel22
            // 
            this.xrLabel22.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel22.LocationFloat = new DevExpress.Utils.PointFloat(50.66127F, 652.3925F);
            this.xrLabel22.Name = "xrLabel22";
            this.xrLabel22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel22.SizeF = new System.Drawing.SizeF(250.1721F, 18.16666F);
            this.xrLabel22.StylePriority.UseFont = false;
            this.xrLabel22.StylePriority.UseTextAlignment = false;
            this.xrLabel22.Text = "Dispatch No.:";
            this.xrLabel22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel24
            // 
            this.xrLabel24.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel24.LocationFloat = new DevExpress.Utils.PointFloat(50.66117F, 681.3456F);
            this.xrLabel24.Name = "xrLabel24";
            this.xrLabel24.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel24.SizeF = new System.Drawing.SizeF(38.33328F, 18.16666F);
            this.xrLabel24.StylePriority.UseFont = false;
            this.xrLabel24.StylePriority.UseTextAlignment = false;
            this.xrLabel24.Text = "Dear";
            this.xrLabel24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel23
            // 
            this.xrLabel23.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.2F, System.Drawing.FontStyle.Bold);
            this.xrLabel23.LocationFloat = new DevExpress.Utils.PointFloat(355.6425F, 606.4118F);
            this.xrLabel23.Name = "xrLabel23";
            this.xrLabel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel23.SizeF = new System.Drawing.SizeF(158.3332F, 18.16667F);
            this.xrLabel23.StylePriority.UseFont = false;
            this.xrLabel23.StylePriority.UseTextAlignment = false;
            this.xrLabel23.Text = "Travel Order";
            this.xrLabel23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel15
            // 
            this.xrLabel15.BackColor = System.Drawing.Color.Transparent;
            this.xrLabel15.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold);
            this.xrLabel15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(88)))), ((int)(((byte)(88)))));
            this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(281.003F, 569.5433F);
            this.xrLabel15.Multiline = true;
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel15.Scripts.OnBeforePrint = "xrLabel3_BeforePrint";
            this.xrLabel15.SizeF = new System.Drawing.SizeF(300.6612F, 18.74664F);
            this.xrLabel15.StyleName = "ReportHeaderStyle";
            this.xrLabel15.StylePriority.UseBackColor = false;
            this.xrLabel15.StylePriority.UseFont = false;
            this.xrLabel15.StylePriority.UseForeColor = false;
            this.xrLabel15.StylePriority.UseTextAlignment = false;
            this.xrLabel15.Text = "SANA KISAN BIKAS BANK LIMITED";
            this.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrPictureBox1
            // 
            this.xrPictureBox1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrPictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox1.Image")));
            this.xrPictureBox1.LocationFloat = new DevExpress.Utils.PointFloat(205.8306F, 569.5433F);
            this.xrPictureBox1.Name = "xrPictureBox1";
            this.xrPictureBox1.SizeF = new System.Drawing.SizeF(61.66661F, 50F);
            this.xrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.CenterImage;
            this.xrPictureBox1.StylePriority.UseBorders = false;
            // 
            // lblBranch2
            // 
            this.lblBranch2.BackColor = System.Drawing.Color.Transparent;
            this.lblBranch2.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblBranch2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(88)))), ((int)(((byte)(88)))));
            this.lblBranch2.LocationFloat = new DevExpress.Utils.PointFloat(281.003F, 588.2901F);
            this.lblBranch2.Multiline = true;
            this.lblBranch2.Name = "lblBranch2";
            this.lblBranch2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblBranch2.Scripts.OnBeforePrint = "xrLabel3_BeforePrint";
            this.lblBranch2.SizeF = new System.Drawing.SizeF(300.6611F, 18.1217F);
            this.lblBranch2.StyleName = "ReportHeaderStyle";
            this.lblBranch2.StylePriority.UseBackColor = false;
            this.lblBranch2.StylePriority.UseFont = false;
            this.lblBranch2.StylePriority.UseForeColor = false;
            this.lblBranch2.StylePriority.UseTextAlignment = false;
            this.lblBranch2.Text = "Branch / Address";
            this.lblBranch2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblNumber2
            // 
            this.lblNumber2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumber2.LocationFloat = new DevExpress.Utils.PointFloat(112.4973F, 634.3923F);
            this.lblNumber2.Name = "lblNumber2";
            this.lblNumber2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNumber2.SizeF = new System.Drawing.SizeF(188.3361F, 18F);
            this.lblNumber2.StylePriority.UseFont = false;
            this.lblNumber2.StylePriority.UseTextAlignment = false;
            this.lblNumber2.Text = "Ref. No.:";
            this.lblNumber2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel18
            // 
            this.xrLabel18.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel18.LocationFloat = new DevExpress.Utils.PointFloat(50.66127F, 634.3923F);
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel18.SizeF = new System.Drawing.SizeF(61.83602F, 18F);
            this.xrLabel18.StylePriority.UseFont = false;
            this.xrLabel18.StylePriority.UseTextAlignment = false;
            this.xrLabel18.Text = "Ref. No.:";
            this.xrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel31
            // 
            this.xrLabel31.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel31.LocationFloat = new DevExpress.Utils.PointFloat(153.3333F, 801.1788F);
            this.xrLabel31.Name = "xrLabel31";
            this.xrLabel31.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel31.SizeF = new System.Drawing.SizeF(164.3388F, 18.16667F);
            this.xrLabel31.StylePriority.UseFont = false;
            this.xrLabel31.StylePriority.UseTextAlignment = false;
            this.xrLabel31.Text = "(2) Place to visit:";
            this.xrLabel31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel30
            // 
            this.xrLabel30.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel30.LocationFloat = new DevExpress.Utils.PointFloat(153.3333F, 783.0122F);
            this.xrLabel30.Name = "xrLabel30";
            this.xrLabel30.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel30.SizeF = new System.Drawing.SizeF(164.3388F, 18.16667F);
            this.xrLabel30.StylePriority.UseFont = false;
            this.xrLabel30.StylePriority.UseTextAlignment = false;
            this.xrLabel30.Text = "(1) Purpose of visit:";
            this.xrLabel30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel32
            // 
            this.xrLabel32.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel32.LocationFloat = new DevExpress.Utils.PointFloat(153.3333F, 819.3455F);
            this.xrLabel32.Name = "xrLabel32";
            this.xrLabel32.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel32.SizeF = new System.Drawing.SizeF(164.3388F, 18.16667F);
            this.xrLabel32.StylePriority.UseFont = false;
            this.xrLabel32.StylePriority.UseTextAlignment = false;
            this.xrLabel32.Text = "(3) Date of departure:";
            this.xrLabel32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel34
            // 
            this.xrLabel34.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel34.LocationFloat = new DevExpress.Utils.PointFloat(153.3333F, 837.5123F);
            this.xrLabel34.Name = "xrLabel34";
            this.xrLabel34.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel34.SizeF = new System.Drawing.SizeF(164.3388F, 18.16667F);
            this.xrLabel34.StylePriority.UseFont = false;
            this.xrLabel34.StylePriority.UseTextAlignment = false;
            this.xrLabel34.Text = "(5) Mode of Travel:";
            this.xrLabel34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel33
            // 
            this.xrLabel33.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel33.LocationFloat = new DevExpress.Utils.PointFloat(500.6424F, 819.3455F);
            this.xrLabel33.Name = "xrLabel33";
            this.xrLabel33.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel33.SizeF = new System.Drawing.SizeF(122.6722F, 18.16666F);
            this.xrLabel33.StylePriority.UseFont = false;
            this.xrLabel33.StylePriority.UseTextAlignment = false;
            this.xrLabel33.Text = "(4) Date of arrival:";
            this.xrLabel33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblDesignation2
            // 
            this.lblDesignation2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDesignation2.LocationFloat = new DevExpress.Utils.PointFloat(50.66117F, 699.5125F);
            this.lblDesignation2.Name = "lblDesignation2";
            this.lblDesignation2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDesignation2.SizeF = new System.Drawing.SizeF(250.1722F, 18.16667F);
            this.lblDesignation2.StylePriority.UseFont = false;
            this.lblDesignation2.StylePriority.UseTextAlignment = false;
            this.lblDesignation2.Text = "Employee Designation";
            this.lblDesignation2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblName2
            // 
            this.lblName2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName2.LocationFloat = new DevExpress.Utils.PointFloat(88.99444F, 681.3456F);
            this.lblName2.Name = "lblName2";
            this.lblName2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblName2.SizeF = new System.Drawing.SizeF(211.8389F, 18.16666F);
            this.lblName2.StylePriority.UseFont = false;
            this.lblName2.StylePriority.UseTextAlignment = false;
            this.lblName2.Text = "Employee Name";
            this.lblName2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel27
            // 
            this.xrLabel27.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel27.LocationFloat = new DevExpress.Utils.PointFloat(50.66117F, 717.6791F);
            this.xrLabel27.Name = "xrLabel27";
            this.xrLabel27.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel27.SizeF = new System.Drawing.SizeF(250.1722F, 18.16667F);
            this.xrLabel27.StylePriority.UseFont = false;
            this.xrLabel27.StylePriority.UseTextAlignment = false;
            this.xrLabel27.Text = "Sana Kisan Bikas Bank Ltd";
            this.xrLabel27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel29
            // 
            this.xrLabel29.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel29.LocationFloat = new DevExpress.Utils.PointFloat(153.5029F, 754.0123F);
            this.xrLabel29.Name = "xrLabel29";
            this.xrLabel29.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel29.SizeF = new System.Drawing.SizeF(640.6641F, 18.16666F);
            this.xrLabel29.StylePriority.UseFont = false;
            this.xrLabel29.StylePriority.UseTextAlignment = false;
            this.xrLabel29.Text = "You are requested to travel as per the following:";
            this.xrLabel29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblEmpBranch2
            // 
            this.lblEmpBranch2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmpBranch2.LocationFloat = new DevExpress.Utils.PointFloat(50.66117F, 735.8458F);
            this.lblEmpBranch2.Name = "lblEmpBranch2";
            this.lblEmpBranch2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblEmpBranch2.SizeF = new System.Drawing.SizeF(250.1722F, 18.16667F);
            this.lblEmpBranch2.StylePriority.UseFont = false;
            this.lblEmpBranch2.StylePriority.UseTextAlignment = false;
            this.lblEmpBranch2.Text = "Branch / Address";
            this.lblEmpBranch2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLine1
            // 
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(0.0001271566F, 545.0001F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(825.9999F, 3.333313F);
            // 
            // xrLabel16
            // 
            this.xrLabel16.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel16.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(51.00031F, 506.3986F);
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel16.SizeF = new System.Drawing.SizeF(250.1722F, 18.16667F);
            this.xrLabel16.StylePriority.UseBorders = false;
            this.xrLabel16.StylePriority.UseFont = false;
            this.xrLabel16.StylePriority.UseTextAlignment = false;
            this.xrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblApprovalDesig1
            // 
            this.lblApprovalDesig1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblApprovalDesig1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblApprovalDesig1.LocationFloat = new DevExpress.Utils.PointFloat(50.83081F, 488.2318F);
            this.lblApprovalDesig1.Name = "lblApprovalDesig1";
            this.lblApprovalDesig1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblApprovalDesig1.SizeF = new System.Drawing.SizeF(250.1722F, 18.16667F);
            this.lblApprovalDesig1.StylePriority.UseBorders = false;
            this.lblApprovalDesig1.StylePriority.UseFont = false;
            this.lblApprovalDesig1.StylePriority.UseTextAlignment = false;
            this.lblApprovalDesig1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblApprovalName1
            // 
            this.lblApprovalName1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblApprovalName1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblApprovalName1.LocationFloat = new DevExpress.Utils.PointFloat(50.83081F, 470.0652F);
            this.lblApprovalName1.Name = "lblApprovalName1";
            this.lblApprovalName1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblApprovalName1.SizeF = new System.Drawing.SizeF(250.1722F, 18.16667F);
            this.lblApprovalName1.StylePriority.UseBorders = false;
            this.lblApprovalName1.StylePriority.UseFont = false;
            this.lblApprovalName1.StylePriority.UseTextAlignment = false;
            this.lblApprovalName1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // test
            // 
            this.test.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.test.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.test.LocationFloat = new DevExpress.Utils.PointFloat(50.83081F, 451.8985F);
            this.test.Name = "test";
            this.test.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.test.SizeF = new System.Drawing.SizeF(250.1722F, 18.16667F);
            this.test.StylePriority.UseBorders = false;
            this.test.StylePriority.UseFont = false;
            this.test.StylePriority.UseTextAlignment = false;
            this.test.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel14
            // 
            this.xrLabel14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(51.00035F, 421.8986F);
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel14.SizeF = new System.Drawing.SizeF(250.1722F, 18.16667F);
            this.xrLabel14.StylePriority.UseFont = false;
            this.xrLabel14.StylePriority.UseTextAlignment = false;
            this.xrLabel14.Text = "Sincerely ,";
            this.xrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblCollectAmount1
            // 
            this.lblCollectAmount1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCollectAmount1.LocationFloat = new DevExpress.Utils.PointFloat(153.5031F, 403.732F);
            this.lblCollectAmount1.Name = "lblCollectAmount1";
            this.lblCollectAmount1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblCollectAmount1.SizeF = new System.Drawing.SizeF(640.6641F, 18.16666F);
            this.lblCollectAmount1.StylePriority.UseFont = false;
            this.lblCollectAmount1.StylePriority.UseTextAlignment = false;
            this.lblCollectAmount1.Text = "(7) Please collect NPR {0}, as travel advance from account section.";
            this.lblCollectAmount1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblTotalValue1
            // 
            this.lblTotalValue1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalValue1.LocationFloat = new DevExpress.Utils.PointFloat(500.6426F, 371.7319F);
            this.lblTotalValue1.Name = "lblTotalValue1";
            this.lblTotalValue1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTotalValue1.SizeF = new System.Drawing.SizeF(182.9702F, 18.16666F);
            this.lblTotalValue1.StylePriority.UseFont = false;
            this.lblTotalValue1.StylePriority.UseTextAlignment = false;
            this.lblTotalValue1.Text = "lblTotalValue1";
            this.lblTotalValue1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblAllowanceThird1Value
            // 
            this.lblAllowanceThird1Value.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAllowanceThird1Value.LocationFloat = new DevExpress.Utils.PointFloat(500.6426F, 353.5652F);
            this.lblAllowanceThird1Value.Name = "lblAllowanceThird1Value";
            this.lblAllowanceThird1Value.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblAllowanceThird1Value.SizeF = new System.Drawing.SizeF(182.9702F, 18.16666F);
            this.lblAllowanceThird1Value.StylePriority.UseFont = false;
            this.lblAllowanceThird1Value.StylePriority.UseTextAlignment = false;
            this.lblAllowanceThird1Value.Text = "lblAllowanceThird1Value";
            this.lblAllowanceThird1Value.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblAllowanceSecond1Value
            // 
            this.lblAllowanceSecond1Value.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAllowanceSecond1Value.LocationFloat = new DevExpress.Utils.PointFloat(500.6426F, 335.3986F);
            this.lblAllowanceSecond1Value.Name = "lblAllowanceSecond1Value";
            this.lblAllowanceSecond1Value.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblAllowanceSecond1Value.SizeF = new System.Drawing.SizeF(182.9702F, 18.16666F);
            this.lblAllowanceSecond1Value.StylePriority.UseFont = false;
            this.lblAllowanceSecond1Value.StylePriority.UseTextAlignment = false;
            this.lblAllowanceSecond1Value.Text = "lblAllowanceSecond1Value";
            this.lblAllowanceSecond1Value.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblAllowanceFirst1Value
            // 
            this.lblAllowanceFirst1Value.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAllowanceFirst1Value.LocationFloat = new DevExpress.Utils.PointFloat(500.6426F, 317.232F);
            this.lblAllowanceFirst1Value.Name = "lblAllowanceFirst1Value";
            this.lblAllowanceFirst1Value.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblAllowanceFirst1Value.SizeF = new System.Drawing.SizeF(182.9702F, 18.16666F);
            this.lblAllowanceFirst1Value.StylePriority.UseFont = false;
            this.lblAllowanceFirst1Value.StylePriority.UseTextAlignment = false;
            this.lblAllowanceFirst1Value.Text = "lblAllowanceFirst1Value";
            this.lblAllowanceFirst1Value.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblMode1
            // 
            this.lblMode1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMode1.LocationFloat = new DevExpress.Utils.PointFloat(317.6724F, 299.0652F);
            this.lblMode1.Name = "lblMode1";
            this.lblMode1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMode1.SizeF = new System.Drawing.SizeF(182.9701F, 18.16666F);
            this.lblMode1.StylePriority.UseFont = false;
            this.lblMode1.StylePriority.UseTextAlignment = false;
            this.lblMode1.Text = "lblMode1";
            this.lblMode1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblArrivalDate1
            // 
            this.lblArrivalDate1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblArrivalDate1.LocationFloat = new DevExpress.Utils.PointFloat(623.3148F, 280.8985F);
            this.lblArrivalDate1.Name = "lblArrivalDate1";
            this.lblArrivalDate1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblArrivalDate1.SizeF = new System.Drawing.SizeF(170.8524F, 18.16666F);
            this.lblArrivalDate1.StylePriority.UseFont = false;
            this.lblArrivalDate1.StylePriority.UseTextAlignment = false;
            this.lblArrivalDate1.Text = "lblArrivalDate1";
            this.lblArrivalDate1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblDepartureDate1
            // 
            this.lblDepartureDate1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDepartureDate1.LocationFloat = new DevExpress.Utils.PointFloat(317.6723F, 280.8985F);
            this.lblDepartureDate1.Name = "lblDepartureDate1";
            this.lblDepartureDate1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDepartureDate1.SizeF = new System.Drawing.SizeF(182.9703F, 18.16666F);
            this.lblDepartureDate1.StylePriority.UseFont = false;
            this.lblDepartureDate1.StylePriority.UseTextAlignment = false;
            this.lblDepartureDate1.Text = "lblDepartureDate1";
            this.lblDepartureDate1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblPlace1
            // 
            this.lblPlace1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlace1.LocationFloat = new DevExpress.Utils.PointFloat(317.6723F, 262.7318F);
            this.lblPlace1.Name = "lblPlace1";
            this.lblPlace1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPlace1.SizeF = new System.Drawing.SizeF(476.4949F, 18.16666F);
            this.lblPlace1.StylePriority.UseFont = false;
            this.lblPlace1.StylePriority.UseTextAlignment = false;
            this.lblPlace1.Text = "lblPlace1";
            this.lblPlace1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblPurpose1
            // 
            this.lblPurpose1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPurpose1.LocationFloat = new DevExpress.Utils.PointFloat(317.6724F, 244.5652F);
            this.lblPurpose1.Name = "lblPurpose1";
            this.lblPurpose1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPurpose1.SizeF = new System.Drawing.SizeF(476.4948F, 18.16666F);
            this.lblPurpose1.StylePriority.UseFont = false;
            this.lblPurpose1.StylePriority.UseTextAlignment = false;
            this.lblPurpose1.Text = "lblPurpose1";
            this.lblPurpose1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblTotal
            // 
            this.lblTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.LocationFloat = new DevExpress.Utils.PointFloat(317.6725F, 371.7319F);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTotal.SizeF = new System.Drawing.SizeF(182.9702F, 18.16666F);
            this.lblTotal.StylePriority.UseFont = false;
            this.lblTotal.StylePriority.UseTextAlignment = false;
            this.lblTotal.Text = "(d) Total (NPR)";
            this.lblTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblAllowanceThird1
            // 
            this.lblAllowanceThird1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAllowanceThird1.LocationFloat = new DevExpress.Utils.PointFloat(317.6724F, 353.5652F);
            this.lblAllowanceThird1.Name = "lblAllowanceThird1";
            this.lblAllowanceThird1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblAllowanceThird1.SizeF = new System.Drawing.SizeF(182.9702F, 18.16666F);
            this.lblAllowanceThird1.StylePriority.UseFont = false;
            this.lblAllowanceThird1.StylePriority.UseTextAlignment = false;
            this.lblAllowanceThird1.Text = "(c) Others ({0}) ";
            this.lblAllowanceThird1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblAllowanceSecond1
            // 
            this.lblAllowanceSecond1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAllowanceSecond1.LocationFloat = new DevExpress.Utils.PointFloat(317.6724F, 335.3986F);
            this.lblAllowanceSecond1.Name = "lblAllowanceSecond1";
            this.lblAllowanceSecond1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblAllowanceSecond1.SizeF = new System.Drawing.SizeF(182.9702F, 18.16666F);
            this.lblAllowanceSecond1.StylePriority.UseFont = false;
            this.lblAllowanceSecond1.StylePriority.UseTextAlignment = false;
            this.lblAllowanceSecond1.Text = "(b) Travel allowance (NPR) ";
            this.lblAllowanceSecond1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblAllowanceFirst1
            // 
            this.lblAllowanceFirst1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAllowanceFirst1.LocationFloat = new DevExpress.Utils.PointFloat(317.6724F, 317.2319F);
            this.lblAllowanceFirst1.Name = "lblAllowanceFirst1";
            this.lblAllowanceFirst1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblAllowanceFirst1.SizeF = new System.Drawing.SizeF(182.9702F, 18.16666F);
            this.lblAllowanceFirst1.StylePriority.UseFont = false;
            this.lblAllowanceFirst1.StylePriority.UseTextAlignment = false;
            this.lblAllowanceFirst1.Text = "(a) Travel allowance (NPR) ";
            this.lblAllowanceFirst1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel13
            // 
            this.xrLabel13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(153.3336F, 317.2319F);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel13.SizeF = new System.Drawing.SizeF(164.3388F, 18.16667F);
            this.xrLabel13.StylePriority.UseFont = false;
            this.xrLabel13.StylePriority.UseTextAlignment = false;
            this.xrLabel13.Text = "(6) Travel cost estimate:";
            this.xrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel12
            // 
            this.xrLabel12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(153.3335F, 299.0652F);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(164.3388F, 18.16667F);
            this.xrLabel12.StylePriority.UseFont = false;
            this.xrLabel12.StylePriority.UseTextAlignment = false;
            this.xrLabel12.Text = "(5) Mode of Travel:";
            this.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel11
            // 
            this.xrLabel11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(500.6426F, 280.8985F);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(122.6722F, 18.16666F);
            this.xrLabel11.StylePriority.UseFont = false;
            this.xrLabel11.StylePriority.UseTextAlignment = false;
            this.xrLabel11.Text = "(4) Date of arrival:";
            this.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel10
            // 
            this.xrLabel10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(153.3335F, 280.8985F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(164.3388F, 18.16667F);
            this.xrLabel10.StylePriority.UseFont = false;
            this.xrLabel10.StylePriority.UseTextAlignment = false;
            this.xrLabel10.Text = "(3) Date of departure:";
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel9
            // 
            this.xrLabel9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(153.3335F, 262.7318F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(164.3388F, 18.16667F);
            this.xrLabel9.StylePriority.UseFont = false;
            this.xrLabel9.StylePriority.UseTextAlignment = false;
            this.xrLabel9.Text = "(2) Place to visit:";
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel8
            // 
            this.xrLabel8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(153.3335F, 244.5652F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(164.3388F, 18.16667F);
            this.xrLabel8.StylePriority.UseFont = false;
            this.xrLabel8.StylePriority.UseTextAlignment = false;
            this.xrLabel8.Text = "(1) Purpose of visit:";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel7
            // 
            this.xrLabel7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(153.503F, 215.5652F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(640.6641F, 18.16666F);
            this.xrLabel7.StylePriority.UseFont = false;
            this.xrLabel7.StylePriority.UseTextAlignment = false;
            this.xrLabel7.Text = "You are requested to travel as per the following:";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblEmpBranch1
            // 
            this.lblEmpBranch1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmpBranch1.LocationFloat = new DevExpress.Utils.PointFloat(50.66133F, 197.3987F);
            this.lblEmpBranch1.Name = "lblEmpBranch1";
            this.lblEmpBranch1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblEmpBranch1.SizeF = new System.Drawing.SizeF(250.1722F, 18.16667F);
            this.lblEmpBranch1.StylePriority.UseFont = false;
            this.lblEmpBranch1.StylePriority.UseTextAlignment = false;
            this.lblEmpBranch1.Text = "Branch / Address";
            this.lblEmpBranch1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel6
            // 
            this.xrLabel6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(50.66133F, 179.2319F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(250.1722F, 18.16667F);
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.StylePriority.UseTextAlignment = false;
            this.xrLabel6.Text = "Sana Kisan Bikas Bank Ltd";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblDesignation1
            // 
            this.lblDesignation1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDesignation1.LocationFloat = new DevExpress.Utils.PointFloat(50.66133F, 161.0652F);
            this.lblDesignation1.Name = "lblDesignation1";
            this.lblDesignation1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDesignation1.SizeF = new System.Drawing.SizeF(250.1722F, 18.16667F);
            this.lblDesignation1.StylePriority.UseFont = false;
            this.lblDesignation1.StylePriority.UseTextAlignment = false;
            this.lblDesignation1.Text = "Employee Designation";
            this.lblDesignation1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblName1
            // 
            this.lblName1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName1.LocationFloat = new DevExpress.Utils.PointFloat(88.99463F, 142.8985F);
            this.lblName1.Name = "lblName1";
            this.lblName1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblName1.SizeF = new System.Drawing.SizeF(211.8389F, 18.16666F);
            this.lblName1.StylePriority.UseFont = false;
            this.lblName1.StylePriority.UseTextAlignment = false;
            this.lblName1.Text = "Employee Name";
            this.lblName1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel5
            // 
            this.xrLabel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(50.66133F, 142.8985F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(38.33328F, 18.16666F);
            this.xrLabel5.StylePriority.UseFont = false;
            this.xrLabel5.StylePriority.UseTextAlignment = false;
            this.xrLabel5.Text = "Dear";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel4
            // 
            this.xrLabel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.2F, System.Drawing.FontStyle.Bold);
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(342.3095F, 64.13528F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(158.3332F, 18.16667F);
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.StylePriority.UseTextAlignment = false;
            this.xrLabel4.Text = "Travel Order";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(50.6614F, 113.9453F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(250.1721F, 18.16666F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "Dispatch No.:";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblDate1
            // 
            this.lblDate1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDate1.LocationFloat = new DevExpress.Utils.PointFloat(644.9976F, 95.9453F);
            this.lblDate1.Name = "lblDate1";
            this.lblDate1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDate1.SizeF = new System.Drawing.SizeF(149.1696F, 14F);
            this.lblDate1.StylePriority.UseFont = false;
            this.lblDate1.StylePriority.UseTextAlignment = false;
            this.lblDate1.Text = "Date";
            this.lblDate1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(603.3309F, 95.9453F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(41.66663F, 14F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.Text = "Date:";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblNumber1
            // 
            this.lblNumber1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumber1.LocationFloat = new DevExpress.Utils.PointFloat(112.4974F, 95.9453F);
            this.lblNumber1.Name = "lblNumber1";
            this.lblNumber1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNumber1.SizeF = new System.Drawing.SizeF(188.3361F, 18F);
            this.lblNumber1.StylePriority.UseFont = false;
            this.lblNumber1.StylePriority.UseTextAlignment = false;
            this.lblNumber1.Text = "Ref. No.:";
            this.lblNumber1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(50.6614F, 95.9453F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(61.83602F, 18F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "Ref. No.:";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblBranch1
            // 
            this.lblBranch1.BackColor = System.Drawing.Color.Transparent;
            this.lblBranch1.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblBranch1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(88)))), ((int)(((byte)(88)))));
            this.lblBranch1.LocationFloat = new DevExpress.Utils.PointFloat(281.003F, 46.0136F);
            this.lblBranch1.Multiline = true;
            this.lblBranch1.Name = "lblBranch1";
            this.lblBranch1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblBranch1.Scripts.OnBeforePrint = "xrLabel3_BeforePrint";
            this.lblBranch1.SizeF = new System.Drawing.SizeF(300.6612F, 18.12168F);
            this.lblBranch1.StyleName = "ReportHeaderStyle";
            this.lblBranch1.StylePriority.UseBackColor = false;
            this.lblBranch1.StylePriority.UseFont = false;
            this.lblBranch1.StylePriority.UseForeColor = false;
            this.lblBranch1.StylePriority.UseTextAlignment = false;
            this.lblBranch1.Text = "Branch / Address";
            this.lblBranch1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblCompanyName
            // 
            this.lblCompanyName.BackColor = System.Drawing.Color.Transparent;
            this.lblCompanyName.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold);
            this.lblCompanyName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(88)))), ((int)(((byte)(88)))));
            this.lblCompanyName.LocationFloat = new DevExpress.Utils.PointFloat(281.003F, 27.26693F);
            this.lblCompanyName.Multiline = true;
            this.lblCompanyName.Name = "lblCompanyName";
            this.lblCompanyName.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblCompanyName.Scripts.OnBeforePrint = "xrLabel3_BeforePrint";
            this.lblCompanyName.SizeF = new System.Drawing.SizeF(300.66F, 18.74667F);
            this.lblCompanyName.StyleName = "ReportHeaderStyle";
            this.lblCompanyName.StylePriority.UseBackColor = false;
            this.lblCompanyName.StylePriority.UseFont = false;
            this.lblCompanyName.StylePriority.UseForeColor = false;
            this.lblCompanyName.StylePriority.UseTextAlignment = false;
            this.lblCompanyName.Text = "SANA KISAN BIKAS BANK LIMITED";
            this.lblCompanyName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // logo
            // 
            this.logo.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.logo.Image = ((System.Drawing.Image)(resources.GetObject("logo.Image")));
            this.logo.LocationFloat = new DevExpress.Utils.PointFloat(205.8306F, 27.26691F);
            this.logo.Name = "logo";
            this.logo.SizeF = new System.Drawing.SizeF(61.66661F, 50F);
            this.logo.Sizing = DevExpress.XtraPrinting.ImageSizeMode.CenterImage;
            this.logo.StylePriority.UseBorders = false;
            // 
            // report_HR_DateOfJoiningTableAdapter
            // 
            this.report_HR_DateOfJoiningTableAdapter.ClearBeforeFill = true;
            // 
            // PageFooter
            // 
            this.PageFooter.HeightF = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // DateStyle
            // 
            this.DateStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.DateStyle.Name = "DateStyle";
            // 
            // report_HR_GetAttendanceDaysTableAdapter
            // 
            this.report_HR_GetAttendanceDaysTableAdapter.ClearBeforeFill = true;
            // 
            // PayBankEven
            // 
            this.PayBankEven.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(243)))), ((int)(((byte)(250)))));
            this.PayBankEven.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(199)))), ((int)(((byte)(209)))), ((int)(((byte)(228)))));
            this.PayBankEven.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.PayBankEven.Font = new System.Drawing.Font("Tahoma", 9F);
            this.PayBankEven.Name = "PayBankEven";
            this.PayBankEven.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            // 
            // PayBankOdd
            // 
            this.PayBankOdd.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(199)))), ((int)(((byte)(209)))), ((int)(((byte)(228)))));
            this.PayBankOdd.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.PayBankOdd.Font = new System.Drawing.Font("Tahoma", 9F);
            this.PayBankOdd.Name = "PayBankOdd";
            this.PayBankOdd.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            // 
            // PayCashEven
            // 
            this.PayCashEven.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(243)))), ((int)(((byte)(250)))));
            this.PayCashEven.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(199)))), ((int)(((byte)(209)))), ((int)(((byte)(228)))));
            this.PayCashEven.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.PayCashEven.Font = new System.Drawing.Font("Tahoma", 9F);
            this.PayCashEven.Name = "PayCashEven";
            this.PayCashEven.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            // 
            // PayCashOdd
            // 
            this.PayCashOdd.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(199)))), ((int)(((byte)(209)))), ((int)(((byte)(228)))));
            this.PayCashOdd.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.PayCashOdd.Font = new System.Drawing.Font("Tahoma", 9F);
            this.PayCashOdd.Name = "PayCashOdd";
            this.PayCashOdd.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            // 
            // ReportHeaderStyle
            // 
            this.ReportHeaderStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(227)))), ((int)(((byte)(214)))));
            this.ReportHeaderStyle.BorderColor = System.Drawing.Color.Transparent;
            this.ReportHeaderStyle.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReportHeaderStyle.ForeColor = System.Drawing.Color.Black;
            this.ReportHeaderStyle.Name = "ReportHeaderStyle";
            // 
            // GrossTotalStyle
            // 
            this.GrossTotalStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.GrossTotalStyle.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(199)))), ((int)(((byte)(209)))), ((int)(((byte)(228)))));
            this.GrossTotalStyle.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.GrossTotalStyle.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrossTotalStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(32)))), ((int)(((byte)(32)))));
            this.GrossTotalStyle.Name = "GrossTotalStyle";
            this.GrossTotalStyle.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.GrossTotalStyle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // advanceEven
            // 
            this.advanceEven.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.advanceEven.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(199)))), ((int)(((byte)(209)))), ((int)(((byte)(228)))));
            this.advanceEven.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.advanceEven.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.advanceEven.ForeColor = System.Drawing.SystemColors.ControlText;
            this.advanceEven.Name = "advanceEven";
            this.advanceEven.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.advanceEven.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // advanceOdd
            // 
            this.advanceOdd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(196)))));
            this.advanceOdd.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(199)))), ((int)(((byte)(209)))), ((int)(((byte)(228)))));
            this.advanceOdd.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.advanceOdd.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.advanceOdd.ForeColor = System.Drawing.SystemColors.ControlText;
            this.advanceOdd.Name = "advanceOdd";
            this.advanceOdd.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.advanceOdd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrControlStyle1
            // 
            this.xrControlStyle1.Name = "xrControlStyle1";
            this.xrControlStyle1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            // 
            // TestStyle
            // 
            this.TestStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.TestStyle.BorderColor = System.Drawing.Color.White;
            this.TestStyle.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TestStyle.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TestStyle.ForeColor = System.Drawing.Color.White;
            this.TestStyle.Name = "TestStyle";
            this.TestStyle.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TestStyle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // DataStyle
            // 
            this.DataStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(220)))), ((int)(((byte)(225)))));
            this.DataStyle.BorderColor = System.Drawing.Color.White;
            this.DataStyle.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.DataStyle.ForeColor = System.Drawing.Color.White;
            this.DataStyle.Name = "DataStyle";
            this.DataStyle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // LineStyle
            // 
            this.LineStyle.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.LineStyle.Name = "LineStyle";
            // 
            // ReportStyle
            // 
            this.ReportStyle.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReportStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(155)))), ((int)(((byte)(24)))));
            this.ReportStyle.Name = "ReportStyle";
            this.ReportStyle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // StyleDate
            // 
            this.StyleDate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.StyleDate.Name = "StyleDate";
            this.StyleDate.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.StyleDate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // AttendanceHeaderStyle
            // 
            this.AttendanceHeaderStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.AttendanceHeaderStyle.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.AttendanceHeaderStyle.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.AttendanceHeaderStyle.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AttendanceHeaderStyle.ForeColor = System.Drawing.Color.White;
            this.AttendanceHeaderStyle.Name = "AttendanceHeaderStyle";
            this.AttendanceHeaderStyle.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.AttendanceHeaderStyle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrControlStyle2
            // 
            this.xrControlStyle2.Name = "xrControlStyle2";
            this.xrControlStyle2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            // 
            // EvenStyle
            // 
            this.EvenStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(243)))), ((int)(((byte)(250)))));
            this.EvenStyle.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(190)))), ((int)(((byte)(216)))));
            this.EvenStyle.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.EvenStyle.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EvenStyle.Name = "EvenStyle";
            this.EvenStyle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // OddStyle
            // 
            this.OddStyle.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(190)))), ((int)(((byte)(216)))));
            this.OddStyle.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.OddStyle.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OddStyle.Name = "OddStyle";
            this.OddStyle.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.OddStyle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // AttributeHeaderStyle
            // 
            this.AttributeHeaderStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(214)))), ((int)(((byte)(237)))));
            this.AttributeHeaderStyle.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(190)))), ((int)(((byte)(216)))));
            this.AttributeHeaderStyle.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.AttributeHeaderStyle.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AttributeHeaderStyle.Name = "AttributeHeaderStyle";
            this.AttributeHeaderStyle.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.AttributeHeaderStyle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // SanaKisanTA
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.PageFooter});
            this.DataMember = "Report_HR_DateOfJoining";
            this.DataSource = this.reportDataSet1;
            this.Margins = new System.Drawing.Printing.Margins(0, 1, 0, 0);
            this.PageHeight = 1169;
            this.PageWidth = 827;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.DateStyle,
            this.PayBankEven,
            this.PayBankOdd,
            this.PayCashEven,
            this.PayCashOdd,
            this.ReportHeaderStyle,
            this.GrossTotalStyle,
            this.advanceEven,
            this.advanceOdd,
            this.xrControlStyle1,
            this.TestStyle,
            this.DataStyle,
            this.LineStyle,
            this.ReportStyle,
            this.StyleDate,
            this.AttendanceHeaderStyle,
            this.xrControlStyle2,
            this.EvenStyle,
            this.OddStyle,
            this.AttributeHeaderStyle});
            this.Version = "12.1";
            ((System.ComponentModel.ISupportInitialize)(this.reportDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportTravellAllowanceSub31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private ReportDataSet reportDataSet1;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private Web.ReportDataSetTableAdapters.Report_HR_DateOfJoiningTableAdapter report_HR_DateOfJoiningTableAdapter;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRControlStyle DateStyle;
        private ReportDataSetTableAdapters.Report_HR_GetAttendanceDaysTableAdapter report_HR_GetAttendanceDaysTableAdapter;
        private DevExpress.XtraReports.UI.XRControlStyle PayBankEven;
        private DevExpress.XtraReports.UI.XRControlStyle PayBankOdd;
        private DevExpress.XtraReports.UI.XRControlStyle PayCashEven;
        private DevExpress.XtraReports.UI.XRControlStyle PayCashOdd;
        private DevExpress.XtraReports.UI.XRControlStyle ReportHeaderStyle;
        private DevExpress.XtraReports.UI.XRControlStyle GrossTotalStyle;
        private DevExpress.XtraReports.UI.XRControlStyle advanceEven;
        private DevExpress.XtraReports.UI.XRControlStyle advanceOdd;
        private DevExpress.XtraReports.UI.XRControlStyle xrControlStyle1;
        private DevExpress.XtraReports.UI.XRControlStyle TestStyle;
        private DevExpress.XtraReports.UI.XRControlStyle DataStyle;
        private DevExpress.XtraReports.UI.XRControlStyle LineStyle;
        private DevExpress.XtraReports.UI.XRControlStyle ReportStyle;
        private DevExpress.XtraReports.UI.XRControlStyle StyleDate;
        private DevExpress.XtraReports.UI.XRControlStyle AttendanceHeaderStyle;
        private DevExpress.XtraReports.UI.XRControlStyle xrControlStyle2;
        private DevExpress.XtraReports.UI.XRControlStyle EvenStyle;
        private DevExpress.XtraReports.UI.XRControlStyle OddStyle;
        private DevExpress.XtraReports.UI.XRControlStyle AttributeHeaderStyle;
        private ReportTravellAllowanceSub3 reportTravellAllowanceSub31;
        public DevExpress.XtraReports.UI.XRLabel lblBranch1;
        public DevExpress.XtraReports.UI.XRLabel lblCompanyName;
        public DevExpress.XtraReports.UI.XRPictureBox logo;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        public DevExpress.XtraReports.UI.XRLabel lblNumber1;
        public DevExpress.XtraReports.UI.XRLabel lblDate1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        public DevExpress.XtraReports.UI.XRLabel lblName1;
        public DevExpress.XtraReports.UI.XRLabel lblEmpBranch1;
        public DevExpress.XtraReports.UI.XRLabel xrLabel6;
        public DevExpress.XtraReports.UI.XRLabel lblDesignation1;
        public DevExpress.XtraReports.UI.XRLabel xrLabel7;
        public DevExpress.XtraReports.UI.XRLabel xrLabel8;
        public DevExpress.XtraReports.UI.XRLabel xrLabel10;
        public DevExpress.XtraReports.UI.XRLabel xrLabel9;
        public DevExpress.XtraReports.UI.XRLabel xrLabel12;
        public DevExpress.XtraReports.UI.XRLabel xrLabel11;
        public DevExpress.XtraReports.UI.XRLabel lblAllowanceThird1;
        public DevExpress.XtraReports.UI.XRLabel lblAllowanceSecond1;
        public DevExpress.XtraReports.UI.XRLabel lblAllowanceFirst1;
        public DevExpress.XtraReports.UI.XRLabel xrLabel13;
        public DevExpress.XtraReports.UI.XRLabel lblTotal;
        public DevExpress.XtraReports.UI.XRLabel lblPurpose1;
        public DevExpress.XtraReports.UI.XRLabel lblPlace1;
        public DevExpress.XtraReports.UI.XRLabel lblDepartureDate1;
        public DevExpress.XtraReports.UI.XRLabel lblArrivalDate1;
        public DevExpress.XtraReports.UI.XRLabel lblMode1;
        public DevExpress.XtraReports.UI.XRLabel lblAllowanceSecond1Value;
        public DevExpress.XtraReports.UI.XRLabel lblAllowanceFirst1Value;
        public DevExpress.XtraReports.UI.XRLabel lblTotalValue1;
        public DevExpress.XtraReports.UI.XRLabel lblAllowanceThird1Value;
        public DevExpress.XtraReports.UI.XRLabel lblCollectAmount1;
        public DevExpress.XtraReports.UI.XRLabel lblApprovalName1;
        public DevExpress.XtraReports.UI.XRLabel test;
        public DevExpress.XtraReports.UI.XRLabel xrLabel14;
        public DevExpress.XtraReports.UI.XRLabel xrLabel16;
        public DevExpress.XtraReports.UI.XRLabel lblApprovalDesig1;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        public DevExpress.XtraReports.UI.XRLabel lblPlace2;
        public DevExpress.XtraReports.UI.XRLabel lblPurpose2;
        public DevExpress.XtraReports.UI.XRLabel lblDepartureDate2;
        public DevExpress.XtraReports.UI.XRLabel lblMode2;
        public DevExpress.XtraReports.UI.XRLabel lblArrivalDate2;
        public DevExpress.XtraReports.UI.XRLabel lblAllowanceFirst2;
        public DevExpress.XtraReports.UI.XRLabel xrLabel35;
        public DevExpress.XtraReports.UI.XRLabel lblAllowanceSecond2;
        public DevExpress.XtraReports.UI.XRLabel xrLabel39;
        public DevExpress.XtraReports.UI.XRLabel lblAllowanceThird2;
        public DevExpress.XtraReports.UI.XRLabel xrLabel51;
        public DevExpress.XtraReports.UI.XRLabel xrLabel50;
        public DevExpress.XtraReports.UI.XRLabel lblApprovalName2;
        public DevExpress.XtraReports.UI.XRLabel xrLabel54;
        public DevExpress.XtraReports.UI.XRLabel lblApprovalDesig2;
        public DevExpress.XtraReports.UI.XRLabel lblAllowanceSecond2Value;
        public DevExpress.XtraReports.UI.XRLabel lblAllowanceFirst2Value;
        public DevExpress.XtraReports.UI.XRLabel lblAllowanceThird2Value;
        public DevExpress.XtraReports.UI.XRLabel lblCollectAmount2;
        public DevExpress.XtraReports.UI.XRLabel lblTotalValue2;
        public DevExpress.XtraReports.UI.XRLabel lblDate2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel20;
        private DevExpress.XtraReports.UI.XRLabel xrLabel22;
        private DevExpress.XtraReports.UI.XRLabel xrLabel24;
        private DevExpress.XtraReports.UI.XRLabel xrLabel23;
        public DevExpress.XtraReports.UI.XRLabel xrLabel15;
        public DevExpress.XtraReports.UI.XRPictureBox xrPictureBox1;
        public DevExpress.XtraReports.UI.XRLabel lblBranch2;
        public DevExpress.XtraReports.UI.XRLabel lblNumber2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel18;
        public DevExpress.XtraReports.UI.XRLabel xrLabel31;
        public DevExpress.XtraReports.UI.XRLabel xrLabel30;
        public DevExpress.XtraReports.UI.XRLabel xrLabel32;
        public DevExpress.XtraReports.UI.XRLabel xrLabel34;
        public DevExpress.XtraReports.UI.XRLabel xrLabel33;
        public DevExpress.XtraReports.UI.XRLabel lblDesignation2;
        public DevExpress.XtraReports.UI.XRLabel lblName2;
        public DevExpress.XtraReports.UI.XRLabel xrLabel27;
        public DevExpress.XtraReports.UI.XRLabel xrLabel29;
        public DevExpress.XtraReports.UI.XRLabel lblEmpBranch2;
    }
}
