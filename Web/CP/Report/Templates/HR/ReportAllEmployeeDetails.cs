using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Web.ReportDataSetTableAdapters;
using BLL;
using Utils;
using DAL;
using BLL.Manager;
using Utils.Calendar;
using System.Collections.Generic;
using Web.UserControls;
using System.Web;

namespace Web.CP.Report.Templates.HR
{
    public partial class ReportAllEmployeeDetails : DevExpress.XtraReports.UI.XtraReport
    {
        public ReportAllEmployeeDetails()
        {
            InitializeComponent();
        }

        protected void ClearBorderControlEvent(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //   ReportHelper.TableCell_BeforePrintEvent(sender, e);
        }
        protected void ClearBorderLessControlEvent(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //ReportHelper.TableCell_BeforePrintEvent(sender, e);
        }

        public void SetSubReport(int employeeId, object sender)
        {

            XRSubreport report1 = (XRSubreport)sender;
            Report.Templates.HR.ReportAllEmployeeDetails reportSource = report1.ReportSource as
                Report.Templates.HR.ReportAllEmployeeDetails;
            // Report.Templates.HR.ReportOtherEmployeeDetails report1 = new Report.Templates.HR.ReportOtherEmployeeDetails();

            try
            {

                List<Report_EmployeeHRDetailsListResult> employeeList =
                      HttpContext.Current.Items["EmployeeList"] as List<Report_EmployeeHRDetailsListResult>;

                if (employeeList != null)
                {

                    Report_EmployeeHRDetailsListResult emp
                         = employeeList.Find(x => x.EmployeeId == employeeId);



                    //EEmployee emp = EmployeeManager.GetEmployeeById(employeeId);

                    SetGeneralInfo(emp, reportSource);
                    SetAddress(emp, reportSource);
                    SetHRInformation(emp, reportSource);
                    SetQualification(emp, reportSource);
                    SetIncomes(emp, reportSource);
                }
            }
            catch (Exception exp1)
            {
                Log.log("Other emp details error for employee id : " + employeeId, exp1);
            }

            report1.ReportSource.DataMember = "Report";

            // this.rptViewer.Report = report1;
            // this.ReportToolbar1.ReportViewer = this.rptViewer;

        }
        void SetIncomes(Report_EmployeeHRDetailsListResult emp, Report.Templates.HR.ReportAllEmployeeDetails rpt)
        {
            decimal? incomePf = 0, deductionPf = 0;
            List<EmployeeIncomeListResult> incomeList =
                 PayManager.GetEmployeeIncomeListWithoutAnyDeduction(emp.EmployeeId.Value, ref incomePf, ref deductionPf);

            string values = "";

            foreach (EmployeeIncomeListResult item in incomeList)
            {
                if (values == "")
                    values = item.Title + " : " + item.Value;
                else
                    values += "\n" + item.Title + " : " + item.Value;
            }
            rpt.lblIncomes.Text = values;

            GetSalaryIncrementsTableAdapter adap = new GetSalaryIncrementsTableAdapter();
            BLL.BaseBiz.SetConnectionPwd(adap.Connection);
            rpt.subReportSalaryIncrement.ReportSource.DataSource = adap.GetData(emp.EmployeeId);
            rpt.subReportSalaryIncrement.ReportSource.DataMember = "Report";

            // Education
            ReportDataSet.HEducationDataTable educationTable = HttpContext.Current.Items["HREducationList"] as ReportDataSet.HEducationDataTable;
            if (educationTable != null)
            {
                educationTable.DefaultView.RowFilter = "EmployeeId = " + emp.EmployeeId;
                rpt.subreportEducation.ReportSource.DataSource = educationTable;
                rpt.subreportEducation.ReportSource.DataMember = "Report";
            }
            // Education
            //rpt.subreportEducation.ReportSource.DataSource = (new HEducationTableAdapter()).GetData(emp.EmployeeId);
            //rpt.subreportEducation.ReportSource.DataMember = "Report";

            // Trainings
            ReportDataSet.HTrainingDataTable trainingTable = HttpContext.Current.Items["HRTrainingList"] as ReportDataSet.HTrainingDataTable;
            if (trainingTable != null)
            {
                trainingTable.DefaultView.RowFilter = "EmployeeId = " + emp.EmployeeId;
                rpt.subreportTraining.ReportSource.DataSource = trainingTable;
                rpt.subreportTraining.ReportSource.DataMember = "Report";
            }


            // Prev Employee
            ReportDataSet.HPreviousEmploymentDataTable prevEmployeeTable = HttpContext.Current.Items["HRPrevEmploymentList"] as ReportDataSet.HPreviousEmploymentDataTable;
            if (prevEmployeeTable != null)
            {
                prevEmployeeTable.DefaultView.RowFilter = "EmployeeId = " + emp.EmployeeId;
                rpt.subreportPrevEmployee.ReportSource.DataSource = prevEmployeeTable;
                rpt.subreportPrevEmployee.ReportSource.DataMember = "Report";
            }


            ReportDataSet.HDocumentDataTable docTable = HttpContext.Current.Items["HRDocumentList"] as ReportDataSet.HDocumentDataTable;
            if (docTable != null)
            {
                docTable.DefaultView.RowFilter = "EmployeeId = " + emp.EmployeeId;
                //HDocumentTableAdapter adapDocs = new HDocumentTableAdapter();
                //ReportDataSet.HDocumentDataTable docTable = adapDocs.GetData(emp.EmployeeId);
                foreach (ReportDataSet.HDocumentRow row in docTable.Rows)
                {
                    row.Url = "~/DocumentHandler.ashx?id=" + System.IO.Path.GetFileNameWithoutExtension(row.Url).ToString();
                }
                rpt.subReportDocuments.ReportSource.DataSource = docTable;
                rpt.subReportDocuments.ReportSource.DataMember = "Report";
            }
        }

        void SetQualification(Report_EmployeeHRDetailsListResult emp, Report.Templates.HR.ReportAllEmployeeDetails rpt)
        {
            //if (emp.HHumanResources.Count > 0)
            rpt.lblQualification.Text = string.Format("Qualification: {0}", emp.Qualification);
            //else
            //  rpt.lblQualification.Text = "";
            rpt.lblSkillSetList.Text = CommonManager.GetEmployeeSkillSetHTMLForReport(emp.EmployeeId.Value);
        }

        // Return the number of years, months, days, hours, minutes, seconds,
        // and milliseconds you need to add to from_date to get to_date.
        private void GetElapsedTime(DateTime from_date, DateTime to_date,
            out int years, out int months, out int days, out int hours,
            out int minutes, out int seconds, out int milliseconds)
        {
            // If from_date > to_date, switch them around.
            if (from_date > to_date)
            {
                GetElapsedTime(to_date, from_date,
                    out years, out months, out days, out hours,
                    out minutes, out seconds, out milliseconds);
                years = -years;
                months = -months;
                days = -days;
                hours = -hours;
                minutes = -minutes;
                seconds = -seconds;
                milliseconds = -milliseconds;
            }
            else
            {
                // Handle the years.
                years = to_date.Year - from_date.Year;

                // See if we went too far.
                DateTime test_date = from_date.AddMonths(12 * years);
                if (test_date > to_date)
                {
                    years--;
                    test_date = from_date.AddMonths(12 * years);
                }

                // Add months until we go too far.
                months = 0;
                while (test_date <= to_date)
                {
                    months++;
                    test_date = from_date.AddMonths(12 * years + months);
                }
                months--;

                // Subtract to see how many more days,
                // hours, minutes, etc. we need.
                from_date = from_date.AddMonths(12 * years + months);
                TimeSpan remainder = to_date - from_date;
                days = remainder.Days;
                hours = remainder.Hours;
                minutes = remainder.Minutes;
                seconds = remainder.Seconds;
                milliseconds = remainder.Milliseconds;
            }
        }

        void SetHRInformation(Report_EmployeeHRDetailsListResult emp, Report.Templates.HR.ReportAllEmployeeDetails rpt)
        {

            // ECurrentStatus firstStatus = EmployeeManager.GetFirstStatus(emp.EmployeeId);
            DateTime from_date, to_date;
            int years, months, days, hours;
            int minutes, seconds, milliseconds;



            GetElapsedTime(emp.JoinEngDate.Value, DateTime.Now, out years, out months, out days, out hours, out minutes, out seconds, out milliseconds);

            rpt.lblEmployeeFor.Text = string.Format("Employee for {0} years, {1} months, {2} days ", years, months, days);
            rpt.lblJoinedOn.Text = string.Format("Joined On {0}", emp.JoinEngDate.Value.ToShortDateString());

            int? currentStatusId = emp.CurrentStatusID;
            //rpt.lblCurrentStatus.Text = "";
            if (currentStatusId != null)
                rpt.lblCurrentStatus.Text = string.Format("Current Status: {0}", JobStatus.GetStatusForDisplay(currentStatusId));


            string values = "";
            List<ECurrentStatus> statusList = new EmployeeManager().GetCurrentStatuses(emp.EmployeeId.Value);
            foreach (ECurrentStatus status in statusList)
            {
                currentStatusId = status.CurrentStatus;
                if (values == "")
                    values = JobStatus.GetStatusForDisplay(currentStatusId) + " on " + status.FromDateEng.ToShortDateString();
                else
                    values += "\n" + JobStatus.GetStatusForDisplay(currentStatusId) + " on " + status.FromDateEng.ToShortDateString();
            }
            rpt.lblStatuses.Text = values;


            //if (emp.HHumanResources.Count > 0)
            {
                rpt.lblBloodGroup.Text = string.Format("Blood Group: {0}", CommonManager.GetBloodGroupNameById( Convert.ToInt32(emp.BloodGroup)));
                rpt.lblPassportNo.Text = string.Format("Passport No: {0}", emp.PassportNo);
                rpt.lblDriverLicense.Text = string.Format("Driving Licence No: {0}", emp.DrivingLicenseNo);
                if (!string.IsNullOrEmpty(emp.PassportValidUpto))
                {
                    CustomDate date = CustomDate.GetCustomDateFromString(emp.PassportValidUpto, true);

                    if (string.IsNullOrEmpty(emp.PassportNo) == false)
                        rpt.lblPassportValidUpto.Text = string.Format("Valid upto: {0}", date.EnglishDate.ToShortDateString());
                    else
                        rpt.lblPassportValidUpto.Text = string.Format("Valid upto: {0}", " ");
                }
                else
                    rpt.lblPassportValidUpto.Text = "";
            }
            //else
            //{
            //    rpt.lblBloodGroup.Text = "";
            //    rpt.lblPassportNo.Text = "";
            //    rpt.lblDriverLicense.Text = "";
            //    rpt.lblPassportValidUpto.Text = "";
            //}
        }
        void SetAddress(Report_EmployeeHRDetailsListResult emp, Report.Templates.HR.ReportAllEmployeeDetails rpt)
        {
            // Present Address
            rpt.lblPresentLocality.Text = emp.PSLocality;
            rpt.lblPresentZone.Text = "";
            //if (emp.EAddresses[0].PSZoneId != null && emp.EAddresses[0].PSZoneId != -1)
            rpt.lblPresentZone.Text = emp.PSZone;
            rpt.lblPresentDistrict.Text = "";
            rpt.lblPresentDistrict.Text = emp.PSDistrict;

            // Permanent Address
            rpt.lblPermanentLocality.Text = emp.PELocality;
            rpt.lblPermanentZone.Text = "";

            rpt.lblPermanentZone.Text = emp.PEZone;
            rpt.lblPermanentDistrict.Text = "";

            rpt.lblPermanentDistrict.Text = emp.PEDistrict;
            rpt.lblPermanentCountry.Text = "";

            rpt.lblPermanentCountry.Text = emp.PECountry;

            // Official Email
            rpt.lblOfficialEmail.Text = emp.CIEmail;
            rpt.lblOfficalPhone.Text = emp.CIPhoneNo;
            rpt.lblOfficialExt.Text = emp.Extension;

            // Personal Contact
            rpt.lblPersonalEmail.Text = emp.PersonalEmail;
            rpt.lblPersonalPhone.Text = emp.PersonalPhone;
            rpt.lblPersonalMobile.Text = emp.CIMobileNo;

            // Emergency Contact
            rpt.lblEmergencyName.Text = emp.EmergencyName;
            rpt.lblEmergencyRelation.Text = emp.EmergencyRelation;
            rpt.lblEmergencyPhone.Text = emp.CIEmergencyNo;
            rpt.lblEmergencyMobile.Text = emp.EmergencyMobile;

        }
        void SetGeneralInfo(Report_EmployeeHRDetailsListResult emp, Report.Templates.HR.ReportAllEmployeeDetails rpt)
        {
            rpt.lblName.Text = emp.Title + " " + emp.Name;
            CustomDate date = null;
            try
            {
                // DOB
                //if (emp.IsEnglishDOB != null && emp.IsEnglishDOB.Value && emp.DateOfBirth != null)
                //{
                //    date = CustomDate.GetCustomDateFromString(emp.DateOfBirth, true);
                //    if (date.Year == CalendarCtl.StartEngYear)
                //        date = null;
                //}
                //else 
                if (emp.DateOfBirth != null)
                {
                    if (emp.IsEnglishDOB != null && emp.IsEnglishDOB.Value)
                    {
                        date = CustomDate.GetCustomDateFromString(emp.DateOfBirth, true);
                        if (date.Year == CalendarCtl.StartEngYear)
                            date = null;
                    }
                    else
                    {
                        date = CustomDate.GetCustomDateFromString(emp.DateOfBirth, false);
                        if (date.Year == CalendarCtl.StartNepYear)
                            date = null;
                    }
                }

                if (date != null)
                    rpt.lblAge.Text = string.Format("Age: {0} years", GetAge(date.EnglishDate));
                else
                    rpt.lblAge.Text = "";
            }
            catch (Exception exp1)
            {
                Log.log("Other emp details error for employee id : " + emp.EmployeeId, exp1);
            }

            rpt.lblStatus.Text = "Marital Status: " + emp.MaritalStatus;
            rpt.lblINo.Text = string.Format("I-No: {0}", GetValue(emp.IdCardNo));
            rpt.lblDepartment.Text = string.Format("{0}", GetValue(emp.Department));


            rpt.lblTeam.Text = string.Format("Team: {0}", emp.LeaveProject);



            rpt.lblSubDepartment.Text = string.Format("Sub-Department: {0}", emp.SubDepartment);
            rpt.lblDesignation.Text = string.Format("Designation: {0}", emp.Designation);

            //rpt.lblTier.Text = string.Format("Tier: {0}", emp.Tier);

            if (!string.IsNullOrEmpty(emp.URLPhoto))
                rpt.pic.ImageUrl = Config.UploadLocation + "/" + emp.URLPhoto;
            else
                rpt.pic.ImageUrl = "";


        }


        public string GetValue(string value)
        {
            if (value == null)
                return "";
            return value.ToString();
        }

        public int GetAge(DateTime bday)
        {
            DateTime today = DateTime.Today;
            int age = today.Year - bday.Year;
            if (bday > today.AddYears(-age)) age--;

            return age;
        }

        private void pic_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //var pb = sender as XRPictureBox;
            //if (pb == null) return;

            //var anchor = pb.Tag as string;
            //if (string.IsNullOrEmpty(anchor) == true) return;

            //var img = pb.Image;
            //if (img == null) return;

            //var parentSize = pb.SizeF;
            //var parentLocation = pb.LocationF;

            //// set XRPictureBox size to the image size
            //if (img.Width > parentSize.Width || img.Height > parentSize.Height)
            //{
            //    // estimate image size
            //    var wr = img.Width / (float)parentSize.Width;
            //    var hr = img.Height / (float)parentSize.Height;
            //    var r = Math.Max(wr, hr);

            //    var size = new SizeF(img.Width / r, img.Height / r);
            //    pb.SizeF = size;
            //}
            //else
            //{
            //    var w = Math.Min(img.Width, parentSize.Width);
            //    var h = Math.Min(img.Height, parentSize.Height);
            //    pb.SizeF = new SizeF(w, h);
            //}

            //if (anchor.Contains("Left"))
            //    pb.LeftF = parentLocation.X;
            //else if (anchor.Contains("Right"))
            //    pb.LeftF = parentLocation.X + parentSize.Width - pb.SizeF.Width;
            //else if (anchor.Contains("Center"))
            //    pb.LeftF = parentLocation.X + ((parentSize.Width - pb.SizeF.Width) / 2);

            //if (anchor.Contains("Top"))
            //    pb.TopF = parentLocation.Y;
            //else if (anchor.Contains("Bottom"))
            //    pb.TopF = parentLocation.Y + parentSize.Height - pb.SizeF.Height;
            //else if (anchor.Contains("Middle"))
            //    pb.TopF = parentLocation.Y + ((parentSize.Height - pb.SizeF.Height) / 2);
        }
    }
}
