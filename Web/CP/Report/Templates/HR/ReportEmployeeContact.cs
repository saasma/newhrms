using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Web.ReportDataSetTableAdapters;
using BLL;

namespace Web.CP.Report.Templates.HR
{
    public partial class ReportEmployeeContact : DevExpress.XtraReports.UI.XtraReport
    {
        public ReportEmployeeContact()
        {
            InitializeComponent();
        }

        protected void ClearBorderControlEvent(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ReportHelper.TableCell_BeforePrintEvent(sender, e);
        }
        protected void ClearBorderLessControlEvent(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ReportHelper.TableCell_BeforePrintEvent(sender, e);
        }

        public void SetSubReport(object sender)
        {
            
            XRSubreport subReport = (XRSubreport)sender;

            if (subReport.Name.Equals("subReportCompanyInfo"))
            {
                Web.CP.Report.Templates.HR.ReportCompanyInfo companyInfoReport =
                    (Web.CP.Report.Templates.HR.ReportCompanyInfo)subReport.ReportSource;

                ReportDataSet.Report_Company_InfoDataTable compayInfoTable = ReportHelper.GetCompanyDataTable();

                companyInfoReport.DataSource = compayInfoTable;
                companyInfoReport.DataMember = "Company Information";
            }

            ReportHelper.BindCompanyInfo(sender);
        }

        private void xrLabel13_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRLabel label = (XRLabel)sender;

            label.Text = label.Text.Replace("<br>", Environment.NewLine);
        }

       
    }
}
