using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using BLL;
using DevExpress.XtraReports.UI;
using Web.ReportDataSetTableAdapters;

namespace Web.CP.Report.Templates.HR
{
    public partial class EmpFamilyMembers : DevExpress.XtraReports.UI.XtraReport
    {
        public EmpFamilyMembers()
        {
            InitializeComponent();
        }

        public void SetSubReport(object sender)
        {
            XRSubreport subReport = (XRSubreport)sender;
            
            if (subReport.Name.Equals("subReportCompanyInfo"))
            {
                Web.CP.Report.Templates.HR.ReportCompanyInfo companyInfoReport =
                    (Web.CP.Report.Templates.HR.ReportCompanyInfo)subReport.ReportSource;

            

                ReportDataSet.Report_Company_InfoDataTable compayInfoTable = ReportHelper.GetCompanyDataTable();

                companyInfoReport.DataSource = compayInfoTable;
                companyInfoReport.DataMember = "Company Information";
            }
        }
        protected void tableCell_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //if (Web.CP.Report.ReportHelper.IsReportExportState())
            //{
            //    ((DevExpress.XtraReports.UI.XRTableCell)sender).BackColor = System.Drawing.Color.Transparent;
            //    ((DevExpress.XtraReports.UI.XRTableCell)sender).BorderColor = System.Drawing.Color.Black;
            //}

           
        }

        protected void ClearBorderControlEvent(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ReportHelper.TableCell_BeforePrintEvent(sender, e);
        }
        protected void ClearBorderLessControlEvent(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ReportHelper.TableCell_BeforePrintEvent(sender, e);
        }

        private void ReportHeader_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            lblCompany.Text = SessionManager.CurrentCompany.Name;
            lblAddress.Text = SessionManager.CurrentCompany.Address;
            lblDate.Text = "Report Date: " + DateTime.Now.ToString("MMMM") + " " + DateTime.Now.Day + "," + DateTime.Now.Year.ToString();
        }
    }
}
