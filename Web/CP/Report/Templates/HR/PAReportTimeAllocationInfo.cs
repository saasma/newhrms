using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace Web.CP.Report.Templates.HR
{
    public partial class PAReportTimeAllocationInfo : DevExpress.XtraReports.UI.XtraReport
      
      {
        public PAReportTimeAllocationInfo()
        {
            InitializeComponent();
        }

        //public void SetSubReport(object sender)
        //{
        //    ReportHelper.BindCompanyInfo(sender);
        //}

        protected void ClearBorderControlEvent(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ReportHelper.TableCell_BeforePrintEvent(sender, e);
        }
        protected void ClearBorderLessControlEvent(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ReportHelper.TableCell_BeforePrintEvent(sender, e);
        }


      
    }
}
