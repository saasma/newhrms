namespace Web.CP.Report.Templates.HR
{
    partial class ReportAllEmployeeDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.panelPayInformation = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblIncomes = new DevExpress.XtraReports.UI.XRLabel();
            this.subReportSalaryIncrement = new DevExpress.XtraReports.UI.XRSubreport();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.panelQualification = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblQualification = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.panelSkillSet = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSkillSetList = new DevExpress.XtraReports.UI.XRLabel();
            this.panelHR = new DevExpress.XtraReports.UI.XRPanel();
            this.lblEmployeeFor = new DevExpress.XtraReports.UI.XRLabel();
            this.lblStatuses = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPassportNo = new DevExpress.XtraReports.UI.XRLabel();
            this.lblCurrentStatus = new DevExpress.XtraReports.UI.XRLabel();
            this.lblBloodGroup = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPassportValidUpto = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDriverLicense = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblJoinedOn = new DevExpress.XtraReports.UI.XRLabel();
            this.panelAddress = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPresentLocality = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPresentZone = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPresentDistrict = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPermanentLocality = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPermanentDistrict = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPermanentZone = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPermanentCountry = new DevExpress.XtraReports.UI.XRLabel();
            this.lblOfficialEmail = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblOfficialExt = new DevExpress.XtraReports.UI.XRLabel();
            this.lblOfficalPhone = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPersonalEmail = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPersonalPhone = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPersonalMobile = new DevExpress.XtraReports.UI.XRLabel();
            this.lblEmergencyName = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblEmergencyPhone = new DevExpress.XtraReports.UI.XRLabel();
            this.lblEmergencyRelation = new DevExpress.XtraReports.UI.XRLabel();
            this.lblEmergencyMobile = new DevExpress.XtraReports.UI.XRLabel();
            this.subreportPrevEmployee = new DevExpress.XtraReports.UI.XRSubreport();
            this.subreportTraining = new DevExpress.XtraReports.UI.XRSubreport();
            this.subreportEducation = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportDocuments = new DevExpress.XtraReports.UI.XRSubreport();
            this.lblTeam = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDesignation = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSubDepartment = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDepartment = new DevExpress.XtraReports.UI.XRLabel();
            this.lblINo = new DevExpress.XtraReports.UI.XRLabel();
            this.pic = new DevExpress.XtraReports.UI.XRPictureBox();
            this.lblStatus = new DevExpress.XtraReports.UI.XRLabel();
            this.lblAge = new DevExpress.XtraReports.UI.XRLabel();
            this.lblName = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.reportDataSet1 = new Web.ReportDataSet();
            this.report_HR_DateOfJoiningTableAdapter = new Web.ReportDataSetTableAdapters.Report_HR_DateOfJoiningTableAdapter();
            this.report_HR_GetAttendanceDaysTableAdapter = new Web.ReportDataSetTableAdapters.Report_HR_GetAttendanceDaysTableAdapter();
            this.PayBankEven = new DevExpress.XtraReports.UI.XRControlStyle();
            this.PayBankOdd = new DevExpress.XtraReports.UI.XRControlStyle();
            this.PayCashEven = new DevExpress.XtraReports.UI.XRControlStyle();
            this.PayCashOdd = new DevExpress.XtraReports.UI.XRControlStyle();
            this.ReportHeaderStyle = new DevExpress.XtraReports.UI.XRControlStyle();
            this.GrossTotalStyle = new DevExpress.XtraReports.UI.XRControlStyle();
            this.advanceEven = new DevExpress.XtraReports.UI.XRControlStyle();
            this.advanceOdd = new DevExpress.XtraReports.UI.XRControlStyle();
            this.xrControlStyle1 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.TestStyle = new DevExpress.XtraReports.UI.XRControlStyle();
            this.DataStyle = new DevExpress.XtraReports.UI.XRControlStyle();
            this.LineStyle = new DevExpress.XtraReports.UI.XRControlStyle();
            this.ReportStyle = new DevExpress.XtraReports.UI.XRControlStyle();
            this.StyleDate = new DevExpress.XtraReports.UI.XRControlStyle();
            this.AttendanceHeaderStyle = new DevExpress.XtraReports.UI.XRControlStyle();
            this.xrControlStyle2 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.EvenStyle = new DevExpress.XtraReports.UI.XRControlStyle();
            this.OddStyle = new DevExpress.XtraReports.UI.XRControlStyle();
            this.AttributeHeaderStyle = new DevExpress.XtraReports.UI.XRControlStyle();
            ((System.ComponentModel.ISupportInitialize)(this.reportDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.panelPayInformation,
            this.lblIncomes,
            this.subReportSalaryIncrement,
            this.xrLabel12});
            this.Detail.Dpi = 100F;
            this.Detail.HeightF = 183.7502F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.StylePriority.UseBackColor = false;
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // panelPayInformation
            // 
            this.panelPayInformation.BackColor = System.Drawing.Color.Transparent;
            this.panelPayInformation.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel11,
            this.xrLabel10});
            this.panelPayInformation.Dpi = 100F;
            this.panelPayInformation.LocationFloat = new DevExpress.Utils.PointFloat(3.002561F, 0F);
            this.panelPayInformation.Name = "panelPayInformation";
            this.panelPayInformation.SizeF = new System.Drawing.SizeF(767.001F, 54.18762F);
            this.panelPayInformation.StylePriority.UseBackColor = false;
            // 
            // xrLabel11
            // 
            this.xrLabel11.Dpi = 100F;
            this.xrLabel11.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(90)))), ((int)(((byte)(17)))));
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(0.0006612142F, 31.18759F);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(255F, 23.00002F);
            this.xrLabel11.StylePriority.UseFont = false;
            this.xrLabel11.StylePriority.UseForeColor = false;
            this.xrLabel11.Text = "Incomes";
            // 
            // xrLabel10
            // 
            this.xrLabel10.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(114)))), ((int)(((byte)(196)))));
            this.xrLabel10.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel10.Dpi = 100F;
            this.xrLabel10.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(78)))), ((int)(((byte)(121)))));
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(0.0006613731F, 0F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(766.1641F, 23.00002F);
            this.xrLabel10.StylePriority.UseBorderColor = false;
            this.xrLabel10.StylePriority.UseBorders = false;
            this.xrLabel10.StylePriority.UseFont = false;
            this.xrLabel10.StylePriority.UseForeColor = false;
            this.xrLabel10.Text = "Pay Information";
            // 
            // lblIncomes
            // 
            this.lblIncomes.Dpi = 100F;
            this.lblIncomes.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIncomes.LocationFloat = new DevExpress.Utils.PointFloat(2.169215F, 54.18762F);
            this.lblIncomes.Multiline = true;
            this.lblIncomes.Name = "lblIncomes";
            this.lblIncomes.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblIncomes.SizeF = new System.Drawing.SizeF(255F, 23F);
            this.lblIncomes.StylePriority.UseFont = false;
            this.lblIncomes.StylePriority.UseTextAlignment = false;
            this.lblIncomes.Text = "Incomes";
            this.lblIncomes.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // subReportSalaryIncrement
            // 
            this.subReportSalaryIncrement.Dpi = 100F;
            this.subReportSalaryIncrement.LocationFloat = new DevExpress.Utils.PointFloat(3.001823F, 112.6667F);
            this.subReportSalaryIncrement.Name = "subReportSalaryIncrement";
            this.subReportSalaryIncrement.ReportSource = new Web.CP.Report.Templates.Pay.Detail.ReportOtherEmpSalaryIncrease();
            this.subReportSalaryIncrement.SizeF = new System.Drawing.SizeF(766.1655F, 46.22918F);
            // 
            // xrLabel12
            // 
            this.xrLabel12.Dpi = 100F;
            this.xrLabel12.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(90)))), ((int)(((byte)(17)))));
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(3.005314F, 89.66669F);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(766.9982F, 23.00001F);
            this.xrLabel12.StylePriority.UseFont = false;
            this.xrLabel12.StylePriority.UseForeColor = false;
            this.xrLabel12.Text = "Salary Increase";
            // 
            // TopMargin
            // 
            this.TopMargin.Dpi = 100F;
            this.TopMargin.HeightF = 10F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 100F;
            this.BottomMargin.HeightF = 50.41646F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.BackColor = System.Drawing.Color.Transparent;
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.panelQualification,
            this.panelSkillSet,
            this.panelHR,
            this.panelAddress,
            this.subreportPrevEmployee,
            this.subreportTraining,
            this.subreportEducation,
            this.subReportDocuments,
            this.lblTeam,
            this.lblDesignation,
            this.lblSubDepartment,
            this.lblDepartment,
            this.lblINo,
            this.pic,
            this.lblStatus,
            this.lblAge,
            this.lblName,
            this.xrLabel15});
            this.ReportHeader.Dpi = 100F;
            this.ReportHeader.HeightF = 922.2292F;
            this.ReportHeader.Name = "ReportHeader";
            this.ReportHeader.StylePriority.UseBackColor = false;
            this.ReportHeader.StylePriority.UseBorders = false;
            // 
            // panelQualification
            // 
            this.panelQualification.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel8,
            this.lblQualification,
            this.xrLabel14});
            this.panelQualification.Dpi = 100F;
            this.panelQualification.LocationFloat = new DevExpress.Utils.PointFloat(3.001188F, 647.8331F);
            this.panelQualification.Name = "panelQualification";
            this.panelQualification.SizeF = new System.Drawing.SizeF(766.1663F, 51.83313F);
            // 
            // xrLabel8
            // 
            this.xrLabel8.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(114)))), ((int)(((byte)(196)))));
            this.xrLabel8.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel8.Dpi = 100F;
            this.xrLabel8.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(78)))), ((int)(((byte)(121)))));
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(0.0006370544F, 6.103516E-05F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(766.1656F, 23F);
            this.xrLabel8.StylePriority.UseBorderColor = false;
            this.xrLabel8.StylePriority.UseBorders = false;
            this.xrLabel8.StylePriority.UseFont = false;
            this.xrLabel8.StylePriority.UseForeColor = false;
            this.xrLabel8.Text = "Qualification";
            // 
            // lblQualification
            // 
            this.lblQualification.Dpi = 100F;
            this.lblQualification.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQualification.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(90)))), ((int)(((byte)(17)))));
            this.lblQualification.LocationFloat = new DevExpress.Utils.PointFloat(0.0001017253F, 27.16662F);
            this.lblQualification.Name = "lblQualification";
            this.lblQualification.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblQualification.SizeF = new System.Drawing.SizeF(255F, 23.00002F);
            this.lblQualification.StylePriority.UseFont = false;
            this.lblQualification.StylePriority.UseForeColor = false;
            this.lblQualification.Text = "Qualification: {0}";
            // 
            // xrLabel14
            // 
            this.xrLabel14.Dpi = 100F;
            this.xrLabel14.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(90)))), ((int)(((byte)(17)))));
            this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(484.1665F, 27.16663F);
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel14.SizeF = new System.Drawing.SizeF(281.9996F, 23F);
            this.xrLabel14.StylePriority.UseFont = false;
            this.xrLabel14.StylePriority.UseForeColor = false;
            this.xrLabel14.Text = "Documents";
            // 
            // panelSkillSet
            // 
            this.panelSkillSet.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel9,
            this.lblSkillSetList});
            this.panelSkillSet.Dpi = 100F;
            this.panelSkillSet.LocationFloat = new DevExpress.Utils.PointFloat(3.001963F, 599.6666F);
            this.panelSkillSet.Name = "panelSkillSet";
            this.panelSkillSet.SizeF = new System.Drawing.SizeF(767.0015F, 48.1665F);
            // 
            // xrLabel9
            // 
            this.xrLabel9.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(114)))), ((int)(((byte)(196)))));
            this.xrLabel9.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel9.Dpi = 100F;
            this.xrLabel9.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(78)))), ((int)(((byte)(121)))));
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(766.9999F, 23F);
            this.xrLabel9.StylePriority.UseBorderColor = false;
            this.xrLabel9.StylePriority.UseBorders = false;
            this.xrLabel9.StylePriority.UseFont = false;
            this.xrLabel9.StylePriority.UseForeColor = false;
            this.xrLabel9.Text = "Skill Sets";
            // 
            // lblSkillSetList
            // 
            this.lblSkillSetList.Dpi = 100F;
            this.lblSkillSetList.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSkillSetList.LocationFloat = new DevExpress.Utils.PointFloat(0F, 23.00001F);
            this.lblSkillSetList.Multiline = true;
            this.lblSkillSetList.Name = "lblSkillSetList";
            this.lblSkillSetList.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSkillSetList.SizeF = new System.Drawing.SizeF(484.1667F, 23F);
            this.lblSkillSetList.StylePriority.UseFont = false;
            this.lblSkillSetList.StylePriority.UsePadding = false;
            this.lblSkillSetList.StylePriority.UseTextAlignment = false;
            this.lblSkillSetList.Text = "Skills1";
            this.lblSkillSetList.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // panelHR
            // 
            this.panelHR.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblEmployeeFor,
            this.lblStatuses,
            this.xrLabel13,
            this.lblPassportNo,
            this.lblCurrentStatus,
            this.lblBloodGroup,
            this.lblPassportValidUpto,
            this.lblDriverLicense,
            this.xrLabel7,
            this.lblJoinedOn});
            this.panelHR.Dpi = 100F;
            this.panelHR.LocationFloat = new DevExpress.Utils.PointFloat(3.001302F, 449.5F);
            this.panelHR.Name = "panelHR";
            this.panelHR.SizeF = new System.Drawing.SizeF(767.0022F, 150.1666F);
            // 
            // lblEmployeeFor
            // 
            this.lblEmployeeFor.Dpi = 100F;
            this.lblEmployeeFor.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmployeeFor.LocationFloat = new DevExpress.Utils.PointFloat(0F, 23.00006F);
            this.lblEmployeeFor.Name = "lblEmployeeFor";
            this.lblEmployeeFor.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblEmployeeFor.SizeF = new System.Drawing.SizeF(484.1667F, 23F);
            this.lblEmployeeFor.StylePriority.UseFont = false;
            this.lblEmployeeFor.StylePriority.UseTextAlignment = false;
            this.lblEmployeeFor.Text = "Employee for {0} years, {1} months, {2} days ";
            this.lblEmployeeFor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblStatuses
            // 
            this.lblStatuses.Dpi = 100F;
            this.lblStatuses.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatuses.LocationFloat = new DevExpress.Utils.PointFloat(0.0006103516F, 115.0002F);
            this.lblStatuses.Multiline = true;
            this.lblStatuses.Name = "lblStatuses";
            this.lblStatuses.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblStatuses.SizeF = new System.Drawing.SizeF(484.1667F, 23F);
            this.lblStatuses.StylePriority.UseFont = false;
            this.lblStatuses.StylePriority.UseTextAlignment = false;
            this.lblStatuses.Text = "{New Status on {Change Date}}";
            this.lblStatuses.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel13
            // 
            this.xrLabel13.Dpi = 100F;
            this.xrLabel13.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(90)))), ((int)(((byte)(17)))));
            this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(0.0006103516F, 92.0002F);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel13.SizeF = new System.Drawing.SizeF(255F, 23.00003F);
            this.xrLabel13.StylePriority.UseFont = false;
            this.xrLabel13.StylePriority.UseForeColor = false;
            this.xrLabel13.Text = "Status Change History";
            // 
            // lblPassportNo
            // 
            this.lblPassportNo.Dpi = 100F;
            this.lblPassportNo.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPassportNo.LocationFloat = new DevExpress.Utils.PointFloat(484.167F, 46.00015F);
            this.lblPassportNo.Name = "lblPassportNo";
            this.lblPassportNo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPassportNo.SizeF = new System.Drawing.SizeF(281.9991F, 23F);
            this.lblPassportNo.StylePriority.UseFont = false;
            this.lblPassportNo.Text = "Passport No: {0}";
            // 
            // lblCurrentStatus
            // 
            this.lblCurrentStatus.Dpi = 100F;
            this.lblCurrentStatus.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCurrentStatus.LocationFloat = new DevExpress.Utils.PointFloat(0F, 69.00024F);
            this.lblCurrentStatus.Name = "lblCurrentStatus";
            this.lblCurrentStatus.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblCurrentStatus.SizeF = new System.Drawing.SizeF(484.1667F, 23F);
            this.lblCurrentStatus.StylePriority.UseFont = false;
            this.lblCurrentStatus.StylePriority.UseTextAlignment = false;
            this.lblCurrentStatus.Text = "Current Status: {0}";
            this.lblCurrentStatus.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblBloodGroup
            // 
            this.lblBloodGroup.Dpi = 100F;
            this.lblBloodGroup.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBloodGroup.LocationFloat = new DevExpress.Utils.PointFloat(484.167F, 23.00006F);
            this.lblBloodGroup.Name = "lblBloodGroup";
            this.lblBloodGroup.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblBloodGroup.SizeF = new System.Drawing.SizeF(281.9991F, 23.00003F);
            this.lblBloodGroup.StylePriority.UseFont = false;
            this.lblBloodGroup.StylePriority.UseTextAlignment = false;
            this.lblBloodGroup.Text = "Blood Group: {0}";
            this.lblBloodGroup.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblPassportValidUpto
            // 
            this.lblPassportValidUpto.Dpi = 100F;
            this.lblPassportValidUpto.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPassportValidUpto.LocationFloat = new DevExpress.Utils.PointFloat(484.167F, 69.00021F);
            this.lblPassportValidUpto.Name = "lblPassportValidUpto";
            this.lblPassportValidUpto.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPassportValidUpto.SizeF = new System.Drawing.SizeF(281.9991F, 23F);
            this.lblPassportValidUpto.StylePriority.UseFont = false;
            this.lblPassportValidUpto.Text = "Valid upto: {0}";
            // 
            // lblDriverLicense
            // 
            this.lblDriverLicense.Dpi = 100F;
            this.lblDriverLicense.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDriverLicense.LocationFloat = new DevExpress.Utils.PointFloat(484.167F, 92.00021F);
            this.lblDriverLicense.Name = "lblDriverLicense";
            this.lblDriverLicense.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDriverLicense.SizeF = new System.Drawing.SizeF(281.9991F, 23F);
            this.lblDriverLicense.StylePriority.UseFont = false;
            this.lblDriverLicense.Text = "Driving Licence No: {0}";
            // 
            // xrLabel7
            // 
            this.xrLabel7.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(114)))), ((int)(((byte)(196)))));
            this.xrLabel7.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel7.Dpi = 100F;
            this.xrLabel7.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(78)))), ((int)(((byte)(121)))));
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(0F, 3.051758E-05F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(766.1663F, 23.00003F);
            this.xrLabel7.StylePriority.UseBorderColor = false;
            this.xrLabel7.StylePriority.UseBorders = false;
            this.xrLabel7.StylePriority.UseFont = false;
            this.xrLabel7.StylePriority.UseForeColor = false;
            this.xrLabel7.Text = "HR Information";
            // 
            // lblJoinedOn
            // 
            this.lblJoinedOn.Dpi = 100F;
            this.lblJoinedOn.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblJoinedOn.LocationFloat = new DevExpress.Utils.PointFloat(0F, 46.00016F);
            this.lblJoinedOn.Name = "lblJoinedOn";
            this.lblJoinedOn.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblJoinedOn.SizeF = new System.Drawing.SizeF(484.1667F, 23F);
            this.lblJoinedOn.StylePriority.UseFont = false;
            this.lblJoinedOn.StylePriority.UseTextAlignment = false;
            this.lblJoinedOn.Text = "Joined On {0}";
            this.lblJoinedOn.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // panelAddress
            // 
            this.panelAddress.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel1,
            this.xrLabel2,
            this.lblPresentLocality,
            this.lblPresentZone,
            this.lblPresentDistrict,
            this.lblPermanentLocality,
            this.xrLabel3,
            this.lblPermanentDistrict,
            this.lblPermanentZone,
            this.lblPermanentCountry,
            this.lblOfficialEmail,
            this.xrLabel4,
            this.lblOfficialExt,
            this.lblOfficalPhone,
            this.xrLabel6,
            this.lblPersonalEmail,
            this.lblPersonalPhone,
            this.lblPersonalMobile,
            this.lblEmergencyName,
            this.xrLabel5,
            this.lblEmergencyPhone,
            this.lblEmergencyRelation,
            this.lblEmergencyMobile});
            this.panelAddress.Dpi = 100F;
            this.panelAddress.LocationFloat = new DevExpress.Utils.PointFloat(3.001912F, 176F);
            this.panelAddress.Name = "panelAddress";
            this.panelAddress.SizeF = new System.Drawing.SizeF(767.0013F, 273.5F);
            // 
            // xrLabel1
            // 
            this.xrLabel1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(114)))), ((int)(((byte)(196)))));
            this.xrLabel1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel1.Dpi = 100F;
            this.xrLabel1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(78)))), ((int)(((byte)(121)))));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(766.1657F, 23.00002F);
            this.xrLabel1.StylePriority.UseBorderColor = false;
            this.xrLabel1.StylePriority.UseBorders = false;
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseForeColor = false;
            this.xrLabel1.Text = "Address";
            // 
            // xrLabel2
            // 
            this.xrLabel2.Dpi = 100F;
            this.xrLabel2.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(90)))), ((int)(((byte)(17)))));
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 22.99998F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(299.9994F, 23.00002F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UseForeColor = false;
            this.xrLabel2.Text = "Present";
            // 
            // lblPresentLocality
            // 
            this.lblPresentLocality.Dpi = 100F;
            this.lblPresentLocality.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPresentLocality.LocationFloat = new DevExpress.Utils.PointFloat(0F, 46.00009F);
            this.lblPresentLocality.Name = "lblPresentLocality";
            this.lblPresentLocality.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPresentLocality.SizeF = new System.Drawing.SizeF(299.9994F, 23.00002F);
            this.lblPresentLocality.StylePriority.UseFont = false;
            this.lblPresentLocality.Text = "lblPresentLocality";
            // 
            // lblPresentZone
            // 
            this.lblPresentZone.Dpi = 100F;
            this.lblPresentZone.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPresentZone.LocationFloat = new DevExpress.Utils.PointFloat(0F, 69.00011F);
            this.lblPresentZone.Name = "lblPresentZone";
            this.lblPresentZone.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPresentZone.SizeF = new System.Drawing.SizeF(299.9994F, 23.00002F);
            this.lblPresentZone.StylePriority.UseFont = false;
            this.lblPresentZone.Text = "lblPresentZone";
            // 
            // lblPresentDistrict
            // 
            this.lblPresentDistrict.Dpi = 100F;
            this.lblPresentDistrict.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPresentDistrict.LocationFloat = new DevExpress.Utils.PointFloat(0F, 92.00021F);
            this.lblPresentDistrict.Name = "lblPresentDistrict";
            this.lblPresentDistrict.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPresentDistrict.SizeF = new System.Drawing.SizeF(299.9994F, 23F);
            this.lblPresentDistrict.StylePriority.UseFont = false;
            this.lblPresentDistrict.Text = "lblPresentDistrict";
            // 
            // lblPermanentLocality
            // 
            this.lblPermanentLocality.Dpi = 100F;
            this.lblPermanentLocality.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPermanentLocality.LocationFloat = new DevExpress.Utils.PointFloat(299.9995F, 46.00011F);
            this.lblPermanentLocality.Name = "lblPermanentLocality";
            this.lblPermanentLocality.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPermanentLocality.SizeF = new System.Drawing.SizeF(229.1667F, 23F);
            this.lblPermanentLocality.StylePriority.UseFont = false;
            this.lblPermanentLocality.Text = "lblPermanentLocality";
            // 
            // xrLabel3
            // 
            this.xrLabel3.Dpi = 100F;
            this.xrLabel3.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(90)))), ((int)(((byte)(17)))));
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(299.9995F, 23.00001F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(229.1667F, 23.00002F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.StylePriority.UseForeColor = false;
            this.xrLabel3.Text = "Permanent";
            // 
            // lblPermanentDistrict
            // 
            this.lblPermanentDistrict.Dpi = 100F;
            this.lblPermanentDistrict.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPermanentDistrict.LocationFloat = new DevExpress.Utils.PointFloat(299.9995F, 92.00022F);
            this.lblPermanentDistrict.Name = "lblPermanentDistrict";
            this.lblPermanentDistrict.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPermanentDistrict.SizeF = new System.Drawing.SizeF(229.1667F, 23F);
            this.lblPermanentDistrict.StylePriority.UseFont = false;
            this.lblPermanentDistrict.Text = "lblPermanentDistrict";
            // 
            // lblPermanentZone
            // 
            this.lblPermanentZone.Dpi = 100F;
            this.lblPermanentZone.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPermanentZone.LocationFloat = new DevExpress.Utils.PointFloat(299.9995F, 69.00019F);
            this.lblPermanentZone.Name = "lblPermanentZone";
            this.lblPermanentZone.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPermanentZone.SizeF = new System.Drawing.SizeF(229.1667F, 23F);
            this.lblPermanentZone.StylePriority.UseFont = false;
            this.lblPermanentZone.Text = "lblPermanentZone";
            // 
            // lblPermanentCountry
            // 
            this.lblPermanentCountry.Dpi = 100F;
            this.lblPermanentCountry.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPermanentCountry.LocationFloat = new DevExpress.Utils.PointFloat(299.9995F, 115.0002F);
            this.lblPermanentCountry.Name = "lblPermanentCountry";
            this.lblPermanentCountry.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPermanentCountry.SizeF = new System.Drawing.SizeF(229.1667F, 23F);
            this.lblPermanentCountry.StylePriority.UseFont = false;
            this.lblPermanentCountry.Text = "lblPermanentCountry";
            // 
            // lblOfficialEmail
            // 
            this.lblOfficialEmail.Dpi = 100F;
            this.lblOfficialEmail.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOfficialEmail.LocationFloat = new DevExpress.Utils.PointFloat(5.086255E-05F, 174.2708F);
            this.lblOfficialEmail.Name = "lblOfficialEmail";
            this.lblOfficialEmail.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblOfficialEmail.SizeF = new System.Drawing.SizeF(299.9992F, 23F);
            this.lblOfficialEmail.StylePriority.UseFont = false;
            this.lblOfficialEmail.StylePriority.UseTextAlignment = false;
            this.lblOfficialEmail.Text = "lblOfficialEmail";
            this.lblOfficialEmail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel4
            // 
            this.xrLabel4.Dpi = 100F;
            this.xrLabel4.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(90)))), ((int)(((byte)(17)))));
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 151.2709F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(299.9993F, 23.00003F);
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.StylePriority.UseForeColor = false;
            this.xrLabel4.Text = "Official Contact";
            // 
            // lblOfficialExt
            // 
            this.lblOfficialExt.Dpi = 100F;
            this.lblOfficialExt.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOfficialExt.LocationFloat = new DevExpress.Utils.PointFloat(5.086255E-05F, 220.2709F);
            this.lblOfficialExt.Name = "lblOfficialExt";
            this.lblOfficialExt.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblOfficialExt.SizeF = new System.Drawing.SizeF(299.9992F, 23.00003F);
            this.lblOfficialExt.StylePriority.UseFont = false;
            this.lblOfficialExt.Text = "lblOfficialExt";
            // 
            // lblOfficalPhone
            // 
            this.lblOfficalPhone.Dpi = 100F;
            this.lblOfficalPhone.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOfficalPhone.LocationFloat = new DevExpress.Utils.PointFloat(5.086255E-05F, 197.2709F);
            this.lblOfficalPhone.Name = "lblOfficalPhone";
            this.lblOfficalPhone.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblOfficalPhone.SizeF = new System.Drawing.SizeF(299.9992F, 23F);
            this.lblOfficalPhone.StylePriority.UseFont = false;
            this.lblOfficalPhone.Text = "lblOfficalPhone";
            // 
            // xrLabel6
            // 
            this.xrLabel6.Dpi = 100F;
            this.xrLabel6.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(90)))), ((int)(((byte)(17)))));
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(299.9993F, 151.2709F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(229.1667F, 23.00002F);
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.StylePriority.UseForeColor = false;
            this.xrLabel6.Text = "Personal Contact";
            // 
            // lblPersonalEmail
            // 
            this.lblPersonalEmail.Dpi = 100F;
            this.lblPersonalEmail.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPersonalEmail.LocationFloat = new DevExpress.Utils.PointFloat(299.9993F, 174.2708F);
            this.lblPersonalEmail.Name = "lblPersonalEmail";
            this.lblPersonalEmail.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPersonalEmail.SizeF = new System.Drawing.SizeF(229.1667F, 23F);
            this.lblPersonalEmail.StylePriority.UseFont = false;
            this.lblPersonalEmail.StylePriority.UseTextAlignment = false;
            this.lblPersonalEmail.Text = "lblPersonalEmail";
            this.lblPersonalEmail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblPersonalPhone
            // 
            this.lblPersonalPhone.Dpi = 100F;
            this.lblPersonalPhone.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPersonalPhone.LocationFloat = new DevExpress.Utils.PointFloat(299.9993F, 197.2708F);
            this.lblPersonalPhone.Name = "lblPersonalPhone";
            this.lblPersonalPhone.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPersonalPhone.SizeF = new System.Drawing.SizeF(229.1667F, 23F);
            this.lblPersonalPhone.StylePriority.UseFont = false;
            this.lblPersonalPhone.Text = "lblPersonalPhone";
            // 
            // lblPersonalMobile
            // 
            this.lblPersonalMobile.Dpi = 100F;
            this.lblPersonalMobile.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPersonalMobile.LocationFloat = new DevExpress.Utils.PointFloat(299.9993F, 220.2711F);
            this.lblPersonalMobile.Name = "lblPersonalMobile";
            this.lblPersonalMobile.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPersonalMobile.SizeF = new System.Drawing.SizeF(229.1667F, 23F);
            this.lblPersonalMobile.StylePriority.UseFont = false;
            this.lblPersonalMobile.Text = "lblPersonalMobile";
            // 
            // lblEmergencyName
            // 
            this.lblEmergencyName.Dpi = 100F;
            this.lblEmergencyName.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmergencyName.LocationFloat = new DevExpress.Utils.PointFloat(530.8326F, 174.2709F);
            this.lblEmergencyName.Name = "lblEmergencyName";
            this.lblEmergencyName.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblEmergencyName.SizeF = new System.Drawing.SizeF(236.1689F, 23F);
            this.lblEmergencyName.StylePriority.UseFont = false;
            this.lblEmergencyName.StylePriority.UseTextAlignment = false;
            this.lblEmergencyName.Text = "lblEmergencyName";
            this.lblEmergencyName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel5
            // 
            this.xrLabel5.Dpi = 100F;
            this.xrLabel5.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(90)))), ((int)(((byte)(17)))));
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(530.8326F, 151.2709F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(236.1689F, 23.00003F);
            this.xrLabel5.StylePriority.UseFont = false;
            this.xrLabel5.StylePriority.UseForeColor = false;
            this.xrLabel5.Text = "Emergency Contact";
            // 
            // lblEmergencyPhone
            // 
            this.lblEmergencyPhone.Dpi = 100F;
            this.lblEmergencyPhone.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmergencyPhone.LocationFloat = new DevExpress.Utils.PointFloat(530.8326F, 220.271F);
            this.lblEmergencyPhone.Name = "lblEmergencyPhone";
            this.lblEmergencyPhone.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblEmergencyPhone.SizeF = new System.Drawing.SizeF(236.1689F, 22.99997F);
            this.lblEmergencyPhone.StylePriority.UseFont = false;
            this.lblEmergencyPhone.Text = "lblEmergencyPhone";
            // 
            // lblEmergencyRelation
            // 
            this.lblEmergencyRelation.Dpi = 100F;
            this.lblEmergencyRelation.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmergencyRelation.LocationFloat = new DevExpress.Utils.PointFloat(530.8326F, 197.271F);
            this.lblEmergencyRelation.Name = "lblEmergencyRelation";
            this.lblEmergencyRelation.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblEmergencyRelation.SizeF = new System.Drawing.SizeF(236.1689F, 23.00003F);
            this.lblEmergencyRelation.StylePriority.UseFont = false;
            this.lblEmergencyRelation.Text = "lblEmergencyRelation";
            // 
            // lblEmergencyMobile
            // 
            this.lblEmergencyMobile.Dpi = 100F;
            this.lblEmergencyMobile.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmergencyMobile.LocationFloat = new DevExpress.Utils.PointFloat(530.8326F, 243.2709F);
            this.lblEmergencyMobile.Name = "lblEmergencyMobile";
            this.lblEmergencyMobile.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblEmergencyMobile.SizeF = new System.Drawing.SizeF(236.169F, 22.99997F);
            this.lblEmergencyMobile.StylePriority.UseFont = false;
            this.lblEmergencyMobile.Text = "lblEmergencyMobile";
            // 
            // subreportPrevEmployee
            // 
            this.subreportPrevEmployee.Dpi = 100F;
            this.subreportPrevEmployee.LocationFloat = new DevExpress.Utils.PointFloat(3.000577F, 865.1666F);
            this.subreportPrevEmployee.Name = "subreportPrevEmployee";
            this.subreportPrevEmployee.ReportSource = new Web.CP.Report.Templates.Pay.Detail.PrevEmployeeReport();
            this.subreportPrevEmployee.SizeF = new System.Drawing.SizeF(766.1669F, 46.22913F);
            // 
            // subreportTraining
            // 
            this.subreportTraining.Dpi = 100F;
            this.subreportTraining.LocationFloat = new DevExpress.Utils.PointFloat(3.001188F, 806.1666F);
            this.subreportTraining.Name = "subreportTraining";
            this.subreportTraining.ReportSource = new Web.CP.Report.Templates.Pay.Detail.TrainingReport();
            this.subreportTraining.SizeF = new System.Drawing.SizeF(767.0024F, 46.22913F);
            // 
            // subreportEducation
            // 
            this.subreportEducation.Dpi = 100F;
            this.subreportEducation.LocationFloat = new DevExpress.Utils.PointFloat(3.002561F, 747.6666F);
            this.subreportEducation.Name = "subreportEducation";
            this.subreportEducation.ReportSource = new Web.CP.Report.Templates.Pay.Detail.EducationReport();
            this.subreportEducation.SizeF = new System.Drawing.SizeF(767.0009F, 46.22919F);
            // 
            // subReportDocuments
            // 
            this.subReportDocuments.Dpi = 100F;
            this.subReportDocuments.LocationFloat = new DevExpress.Utils.PointFloat(487.1676F, 699.6663F);
            this.subReportDocuments.Name = "subReportDocuments";
            this.subReportDocuments.ReportSource = new Web.CP.Report.Templates.Pay.Detail.OtherEmpDocuments();
            this.subReportDocuments.SizeF = new System.Drawing.SizeF(282.8359F, 36.8335F);
            // 
            // lblTeam
            // 
            this.lblTeam.Dpi = 100F;
            this.lblTeam.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTeam.LocationFloat = new DevExpress.Utils.PointFloat(303.0013F, 125F);
            this.lblTeam.Multiline = true;
            this.lblTeam.Name = "lblTeam";
            this.lblTeam.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTeam.SizeF = new System.Drawing.SizeF(229.1668F, 23F);
            this.lblTeam.StylePriority.UseFont = false;
            this.lblTeam.Text = "Team: {0}";
            // 
            // lblDesignation
            // 
            this.lblDesignation.Dpi = 100F;
            this.lblDesignation.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDesignation.LocationFloat = new DevExpress.Utils.PointFloat(303.0014F, 102F);
            this.lblDesignation.Name = "lblDesignation";
            this.lblDesignation.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDesignation.SizeF = new System.Drawing.SizeF(229.1667F, 23.00001F);
            this.lblDesignation.StylePriority.UseFont = false;
            this.lblDesignation.Text = "Designation: {0}";
            // 
            // lblSubDepartment
            // 
            this.lblSubDepartment.Dpi = 100F;
            this.lblSubDepartment.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSubDepartment.LocationFloat = new DevExpress.Utils.PointFloat(3.001302F, 148F);
            this.lblSubDepartment.Name = "lblSubDepartment";
            this.lblSubDepartment.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSubDepartment.SizeF = new System.Drawing.SizeF(299.9999F, 23F);
            this.lblSubDepartment.StylePriority.UseFont = false;
            this.lblSubDepartment.Text = "Sub-Department: {0}";
            // 
            // lblDepartment
            // 
            this.lblDepartment.Dpi = 100F;
            this.lblDepartment.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDepartment.LocationFloat = new DevExpress.Utils.PointFloat(3.001302F, 125F);
            this.lblDepartment.Name = "lblDepartment";
            this.lblDepartment.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDepartment.SizeF = new System.Drawing.SizeF(299.9999F, 22.99998F);
            this.lblDepartment.StylePriority.UseFont = false;
            this.lblDepartment.Text = "{0}";
            // 
            // lblINo
            // 
            this.lblINo.Dpi = 100F;
            this.lblINo.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblINo.LocationFloat = new DevExpress.Utils.PointFloat(3.001302F, 79.00002F);
            this.lblINo.Name = "lblINo";
            this.lblINo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblINo.SizeF = new System.Drawing.SizeF(300F, 23.00001F);
            this.lblINo.StylePriority.UseFont = false;
            this.lblINo.Text = "I-No: {0}";
            // 
            // pic
            // 
            this.pic.Dpi = 100F;
            this.pic.LocationFloat = new DevExpress.Utils.PointFloat(533.8347F, 10F);
            this.pic.Name = "pic";
            this.pic.SizeF = new System.Drawing.SizeF(236.1685F, 166F);
            this.pic.Sizing = DevExpress.XtraPrinting.ImageSizeMode.Squeeze;
            this.pic.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.pic_BeforePrint);
            // 
            // lblStatus
            // 
            this.lblStatus.Dpi = 100F;
            this.lblStatus.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus.LocationFloat = new DevExpress.Utils.PointFloat(3.001302F, 56.00001F);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblStatus.SizeF = new System.Drawing.SizeF(300F, 23F);
            this.lblStatus.StylePriority.UseFont = false;
            this.lblStatus.Text = "Marital Status: lblStatus";
            // 
            // lblAge
            // 
            this.lblAge.Dpi = 100F;
            this.lblAge.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAge.LocationFloat = new DevExpress.Utils.PointFloat(3.001302F, 33F);
            this.lblAge.Name = "lblAge";
            this.lblAge.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblAge.SizeF = new System.Drawing.SizeF(300F, 23F);
            this.lblAge.StylePriority.UseFont = false;
            this.lblAge.Text = "Age: {0} years";
            // 
            // lblName
            // 
            this.lblName.Dpi = 100F;
            this.lblName.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(78)))), ((int)(((byte)(121)))));
            this.lblName.LocationFloat = new DevExpress.Utils.PointFloat(3.001302F, 10F);
            this.lblName.Name = "lblName";
            this.lblName.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblName.SizeF = new System.Drawing.SizeF(299.9999F, 23F);
            this.lblName.StylePriority.UseFont = false;
            this.lblName.StylePriority.UseForeColor = false;
            this.lblName.Text = "lblName";
            // 
            // xrLabel15
            // 
            this.xrLabel15.Dpi = 100F;
            this.xrLabel15.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(3.001963F, 102F);
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel15.SizeF = new System.Drawing.SizeF(299.9992F, 23F);
            this.xrLabel15.StylePriority.UseFont = false;
            this.xrLabel15.StylePriority.UseTextAlignment = false;
            this.xrLabel15.Text = "Department: ";
            this.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // reportDataSet1
            // 
            this.reportDataSet1.DataSetName = "ReportDataSet";
            this.reportDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // report_HR_DateOfJoiningTableAdapter
            // 
            this.report_HR_DateOfJoiningTableAdapter.ClearBeforeFill = true;
            // 
            // report_HR_GetAttendanceDaysTableAdapter
            // 
            this.report_HR_GetAttendanceDaysTableAdapter.ClearBeforeFill = true;
            // 
            // PayBankEven
            // 
            this.PayBankEven.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(243)))), ((int)(((byte)(250)))));
            this.PayBankEven.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(199)))), ((int)(((byte)(209)))), ((int)(((byte)(228)))));
            this.PayBankEven.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.PayBankEven.Font = new System.Drawing.Font("Tahoma", 9F);
            this.PayBankEven.Name = "PayBankEven";
            this.PayBankEven.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            // 
            // PayBankOdd
            // 
            this.PayBankOdd.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(199)))), ((int)(((byte)(209)))), ((int)(((byte)(228)))));
            this.PayBankOdd.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.PayBankOdd.Font = new System.Drawing.Font("Tahoma", 9F);
            this.PayBankOdd.Name = "PayBankOdd";
            this.PayBankOdd.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            // 
            // PayCashEven
            // 
            this.PayCashEven.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(243)))), ((int)(((byte)(250)))));
            this.PayCashEven.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(199)))), ((int)(((byte)(209)))), ((int)(((byte)(228)))));
            this.PayCashEven.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.PayCashEven.Font = new System.Drawing.Font("Tahoma", 9F);
            this.PayCashEven.Name = "PayCashEven";
            this.PayCashEven.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            // 
            // PayCashOdd
            // 
            this.PayCashOdd.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(199)))), ((int)(((byte)(209)))), ((int)(((byte)(228)))));
            this.PayCashOdd.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.PayCashOdd.Font = new System.Drawing.Font("Tahoma", 9F);
            this.PayCashOdd.Name = "PayCashOdd";
            this.PayCashOdd.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            // 
            // ReportHeaderStyle
            // 
            this.ReportHeaderStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(227)))), ((int)(((byte)(214)))));
            this.ReportHeaderStyle.BorderColor = System.Drawing.Color.Transparent;
            this.ReportHeaderStyle.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReportHeaderStyle.ForeColor = System.Drawing.Color.Black;
            this.ReportHeaderStyle.Name = "ReportHeaderStyle";
            // 
            // GrossTotalStyle
            // 
            this.GrossTotalStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.GrossTotalStyle.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(199)))), ((int)(((byte)(209)))), ((int)(((byte)(228)))));
            this.GrossTotalStyle.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.GrossTotalStyle.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrossTotalStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(32)))), ((int)(((byte)(32)))));
            this.GrossTotalStyle.Name = "GrossTotalStyle";
            this.GrossTotalStyle.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.GrossTotalStyle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // advanceEven
            // 
            this.advanceEven.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.advanceEven.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(199)))), ((int)(((byte)(209)))), ((int)(((byte)(228)))));
            this.advanceEven.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.advanceEven.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.advanceEven.ForeColor = System.Drawing.SystemColors.ControlText;
            this.advanceEven.Name = "advanceEven";
            this.advanceEven.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.advanceEven.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // advanceOdd
            // 
            this.advanceOdd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(196)))));
            this.advanceOdd.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(199)))), ((int)(((byte)(209)))), ((int)(((byte)(228)))));
            this.advanceOdd.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.advanceOdd.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.advanceOdd.ForeColor = System.Drawing.SystemColors.ControlText;
            this.advanceOdd.Name = "advanceOdd";
            this.advanceOdd.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.advanceOdd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrControlStyle1
            // 
            this.xrControlStyle1.Name = "xrControlStyle1";
            this.xrControlStyle1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            // 
            // TestStyle
            // 
            this.TestStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.TestStyle.BorderColor = System.Drawing.Color.White;
            this.TestStyle.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TestStyle.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TestStyle.ForeColor = System.Drawing.Color.White;
            this.TestStyle.Name = "TestStyle";
            this.TestStyle.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TestStyle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // DataStyle
            // 
            this.DataStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(220)))), ((int)(((byte)(225)))));
            this.DataStyle.BorderColor = System.Drawing.Color.White;
            this.DataStyle.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.DataStyle.ForeColor = System.Drawing.Color.White;
            this.DataStyle.Name = "DataStyle";
            this.DataStyle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // LineStyle
            // 
            this.LineStyle.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.LineStyle.Name = "LineStyle";
            // 
            // ReportStyle
            // 
            this.ReportStyle.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReportStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(155)))), ((int)(((byte)(24)))));
            this.ReportStyle.Name = "ReportStyle";
            this.ReportStyle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // StyleDate
            // 
            this.StyleDate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.StyleDate.Name = "StyleDate";
            this.StyleDate.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.StyleDate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // AttendanceHeaderStyle
            // 
            this.AttendanceHeaderStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.AttendanceHeaderStyle.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.AttendanceHeaderStyle.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.AttendanceHeaderStyle.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AttendanceHeaderStyle.ForeColor = System.Drawing.Color.White;
            this.AttendanceHeaderStyle.Name = "AttendanceHeaderStyle";
            this.AttendanceHeaderStyle.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.AttendanceHeaderStyle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrControlStyle2
            // 
            this.xrControlStyle2.Name = "xrControlStyle2";
            this.xrControlStyle2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            // 
            // EvenStyle
            // 
            this.EvenStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(243)))), ((int)(((byte)(250)))));
            this.EvenStyle.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(190)))), ((int)(((byte)(216)))));
            this.EvenStyle.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.EvenStyle.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EvenStyle.Name = "EvenStyle";
            this.EvenStyle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // OddStyle
            // 
            this.OddStyle.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(190)))), ((int)(((byte)(216)))));
            this.OddStyle.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.OddStyle.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OddStyle.Name = "OddStyle";
            this.OddStyle.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.OddStyle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // AttributeHeaderStyle
            // 
            this.AttributeHeaderStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(214)))), ((int)(((byte)(237)))));
            this.AttributeHeaderStyle.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(190)))), ((int)(((byte)(216)))));
            this.AttributeHeaderStyle.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.AttributeHeaderStyle.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AttributeHeaderStyle.Name = "AttributeHeaderStyle";
            this.AttributeHeaderStyle.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.AttributeHeaderStyle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // ReportAllEmployeeDetails
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader});
            this.DataMember = "Report_HR_DateOfJoining";
            this.DataSource = this.reportDataSet1;
            this.Margins = new System.Drawing.Printing.Margins(0, 0, 10, 50);
            this.PageHeight = 1169;
            this.PageWidth = 827;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Scripts.OnBeforePrint = "ReportEmployeeContact_BeforePrint";
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.PayBankEven,
            this.PayBankOdd,
            this.PayCashEven,
            this.PayCashOdd,
            this.ReportHeaderStyle,
            this.GrossTotalStyle,
            this.advanceEven,
            this.advanceOdd,
            this.xrControlStyle1,
            this.TestStyle,
            this.DataStyle,
            this.LineStyle,
            this.ReportStyle,
            this.StyleDate,
            this.AttendanceHeaderStyle,
            this.xrControlStyle2,
            this.EvenStyle,
            this.OddStyle,
            this.AttributeHeaderStyle});
            this.Version = "16.1";
            ((System.ComponentModel.ISupportInitialize)(this.reportDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private ReportDataSet reportDataSet1;
        private Web.ReportDataSetTableAdapters.Report_HR_DateOfJoiningTableAdapter report_HR_DateOfJoiningTableAdapter;
        private ReportCompanyInfo reportCompanyInfo1;
        private Web.ReportDataSetTableAdapters.Report_HR_GetAttendanceDaysTableAdapter report_HR_GetAttendanceDaysTableAdapter;
        private DevExpress.XtraReports.UI.XRControlStyle PayBankEven;
        private DevExpress.XtraReports.UI.XRControlStyle PayBankOdd;
        private DevExpress.XtraReports.UI.XRControlStyle PayCashEven;
        private DevExpress.XtraReports.UI.XRControlStyle PayCashOdd;
        private DevExpress.XtraReports.UI.XRControlStyle ReportHeaderStyle;
        private DevExpress.XtraReports.UI.XRControlStyle GrossTotalStyle;
        private DevExpress.XtraReports.UI.XRControlStyle advanceEven;
        private DevExpress.XtraReports.UI.XRControlStyle advanceOdd;
        private DevExpress.XtraReports.UI.XRControlStyle xrControlStyle1;
        private DevExpress.XtraReports.UI.XRControlStyle TestStyle;
        private DevExpress.XtraReports.UI.XRControlStyle DataStyle;
        private DevExpress.XtraReports.UI.XRControlStyle LineStyle;
        private DevExpress.XtraReports.UI.XRControlStyle ReportStyle;
        private DevExpress.XtraReports.UI.XRControlStyle StyleDate;
        private DevExpress.XtraReports.UI.XRControlStyle AttendanceHeaderStyle;
        private DevExpress.XtraReports.UI.XRControlStyle xrControlStyle2;
        private DevExpress.XtraReports.UI.XRControlStyle EvenStyle;
        private DevExpress.XtraReports.UI.XRControlStyle OddStyle;
        private DevExpress.XtraReports.UI.XRControlStyle AttributeHeaderStyle;
        public DevExpress.XtraReports.UI.XRLabel lblName;
        public DevExpress.XtraReports.UI.XRLabel lblStatus;
        public DevExpress.XtraReports.UI.XRLabel lblAge;
        public DevExpress.XtraReports.UI.XRPictureBox pic;
        public DevExpress.XtraReports.UI.XRLabel lblDesignation;
        public DevExpress.XtraReports.UI.XRLabel lblSubDepartment;
        public DevExpress.XtraReports.UI.XRLabel lblDepartment;
        public DevExpress.XtraReports.UI.XRLabel lblINo;
        public DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        public DevExpress.XtraReports.UI.XRLabel lblPresentLocality;
        public DevExpress.XtraReports.UI.XRLabel lblPermanentCountry;
        public DevExpress.XtraReports.UI.XRLabel lblPermanentZone;
        public DevExpress.XtraReports.UI.XRLabel lblPermanentDistrict;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        public DevExpress.XtraReports.UI.XRLabel lblPermanentLocality;
        public DevExpress.XtraReports.UI.XRLabel lblPresentDistrict;
        public DevExpress.XtraReports.UI.XRLabel lblPresentZone;
        public DevExpress.XtraReports.UI.XRLabel lblPersonalMobile;
        public DevExpress.XtraReports.UI.XRLabel lblPersonalPhone;
        public DevExpress.XtraReports.UI.XRLabel lblPersonalEmail;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        public DevExpress.XtraReports.UI.XRLabel lblOfficalPhone;
        public DevExpress.XtraReports.UI.XRLabel lblOfficialExt;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        public DevExpress.XtraReports.UI.XRLabel lblOfficialEmail;
        public DevExpress.XtraReports.UI.XRLabel lblEmergencyRelation;
        public DevExpress.XtraReports.UI.XRLabel lblEmergencyPhone;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        public DevExpress.XtraReports.UI.XRLabel lblEmergencyName;
        public DevExpress.XtraReports.UI.XRLabel lblEmergencyMobile;
        public DevExpress.XtraReports.UI.XRLabel xrLabel7;
        public DevExpress.XtraReports.UI.XRLabel lblEmployeeFor;
        public DevExpress.XtraReports.UI.XRLabel lblJoinedOn;
        public DevExpress.XtraReports.UI.XRLabel lblPassportNo;
        public DevExpress.XtraReports.UI.XRLabel lblDriverLicense;
        public DevExpress.XtraReports.UI.XRLabel lblBloodGroup;
        public DevExpress.XtraReports.UI.XRLabel lblPassportValidUpto;
        public DevExpress.XtraReports.UI.XRLabel lblCurrentStatus;
        public DevExpress.XtraReports.UI.XRLabel lblStatuses;
        public DevExpress.XtraReports.UI.XRLabel xrLabel8;
        public DevExpress.XtraReports.UI.XRLabel lblQualification;
        public DevExpress.XtraReports.UI.XRLabel xrLabel9;
        public DevExpress.XtraReports.UI.XRLabel lblSkillSetList;
        public DevExpress.XtraReports.UI.XRLabel xrLabel10;
        public DevExpress.XtraReports.UI.XRLabel xrLabel11;
        public DevExpress.XtraReports.UI.XRLabel lblIncomes;
        public DevExpress.XtraReports.UI.XRLabel xrLabel12;
        public DevExpress.XtraReports.UI.XRLabel lblTeam;
        private DevExpress.XtraReports.UI.XRLabel xrLabel13;
        public DevExpress.XtraReports.UI.XRSubreport subReportSalaryIncrement;
        public DevExpress.XtraReports.UI.XRLabel xrLabel14;
        public DevExpress.XtraReports.UI.XRSubreport subReportDocuments;
        public DevExpress.XtraReports.UI.XRSubreport subreportEducation;
        public DevExpress.XtraReports.UI.XRSubreport subreportTraining;
        public DevExpress.XtraReports.UI.XRSubreport subreportPrevEmployee;
        public DevExpress.XtraReports.UI.XRPanel panelAddress;
        public DevExpress.XtraReports.UI.XRPanel panelHR;
        public DevExpress.XtraReports.UI.XRPanel panelSkillSet;
        public DevExpress.XtraReports.UI.XRPanel panelQualification;
        public DevExpress.XtraReports.UI.XRPanel panelPayInformation;
        public DevExpress.XtraReports.UI.XRLabel xrLabel15;
    }
}
