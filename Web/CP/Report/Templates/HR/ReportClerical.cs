using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Web.ReportDataSetTableAdapters;
using BLL;
using Utils;
using DAL;
using BLL.Manager;
using Utils.Calendar;
using System.Collections.Generic;
using Web.UserControls;
using System.Web;
using System.Linq;

namespace Web.CP.Report.Templates.HR
{
    public partial class ReportClerical : DevExpress.XtraReports.UI.XtraReport
    {
        public ReportClerical()
        {
            InitializeComponent();

            BindClericalReport();

            
        }

        public void BindClericalReport()
        {
            ClericalDataClass obj = (ClericalDataClass)HttpContext.Current.Session["clericalClassData"];

            if (obj == null)
                return;

            List<Report_ClericalNonClericalResult> list = EmployeeManager.Get_ReportClericalNonClerical(obj.BranchId, obj.DepartmentId, obj.PayrollPeriodId, obj.EmployeeId);
            List<Report_ClericalNonClericalResult> listClerical = list.Where(x => x.ClericalType == "Clerical").ToList();

            int i = 0;
            foreach (var item in listClerical)
            {
                if( i == 0)
                    tblClerical.Rows.Add(GetTableRow(item.ClericalType, item.Name, item.TotalEmployees.Value, true));
                else
                    tblClerical.Rows.Add(GetTableRow(item.ClericalType, item.Name, item.TotalEmployees.Value, false));
                i++;
            }

            if (listClerical.Count > 0)
            {
                tblClerical.Rows.Add(GetTotalRow("Total no. of clerical staff:", listClerical.Sum(x => x.TotalEmployees.Value)));
                tblClerical.Rows.Add(GetEmptyRow());
            }

            i = 0;
            List<Report_ClericalNonClericalResult> listNonClerical = list.Where(x => x.ClericalType == "Non Clerical").ToList();
            foreach (var item in listNonClerical)
            {
                if (i == 0)
                    tblClerical.Rows.Add(GetTableRow(item.ClericalType, item.Name, item.TotalEmployees.Value, true));
                else
                    tblClerical.Rows.Add(GetTableRow(item.ClericalType, item.Name, item.TotalEmployees.Value, false));
                i++;
            }

            if (listNonClerical.Count > 0)
            {
                tblClerical.Rows.Add(GetTotalRow("Total no. of non-clerical staff:", listNonClerical.Sum(x => x.TotalEmployees.Value)));
                tblClerical.Rows.Add(GetEmptyRow());
            }

            string companyName = SessionManager.CurrentCompany.Name;
            tblClerical.Rows.Add(GetTotalRow(string.Format("Total no. of staff at {0}: ", companyName), list.Sum(x => x.TotalEmployees.Value)));

        }

        public XRTableRow GetTableRow(string type, string name, int NoOfEmp, bool IsFirst)
        {
            XRTableRow row = new XRTableRow();

            XRTableCell cell0 = new XRTableCell();
            XRTableCell cell1 = new XRTableCell();
            XRTableCell cell2 = new XRTableCell();

            cell0.WidthF = 150;
            cell1.WidthF = 250;
            cell2.WidthF = 150;

            if (IsFirst)
                cell0.Text = type;
            else
                cell0.Text = "";

            row.Font = new Font("Calibri", 10, FontStyle.Regular);

            cell0.Font = new Font("Calibri", 10, FontStyle.Bold);

            cell1.Text = name;
            cell2.Text = NoOfEmp.ToString();

            row.Cells.Add((XRTableCell)cell0);
            row.Cells.Add((XRTableCell)cell1);
            row.Cells.Add((XRTableCell)cell2);

            row.BorderColor = Color.FromArgb(242, 242, 242);
            //row.BackColor = Color.FromArgb(242, 242, 242);            

            return row;
        }

        public XRTableRow GetTotalRow(string name, int total)
        {
            XRTableRow row = new XRTableRow();

            XRTableCell cell0 = new XRTableCell();
            XRTableCell cell1 = new XRTableCell();
            XRTableCell cell2 = new XRTableCell();

            cell0.WidthF = 150;
            cell1.WidthF = 250;
            cell2.WidthF = 150;

            cell0.Text = "";
            cell1.Text = name;
            cell2.Text = total.ToString();

            row.Cells.Add((XRTableCell)cell0);
            row.Cells.Add((XRTableCell)cell1);
            row.Cells.Add((XRTableCell)cell2);

            row.BorderColor = Color.FromArgb(242, 242, 242);

            row.BackColor = Color.FromArgb(242, 242, 242);
            row.Font = new Font("Calibri", 10, FontStyle.Bold);

            return row;
        }

        public XRTableRow GetEmptyRow()
        {
            XRTableRow row = new XRTableRow();

            XRTableCell cell0 = new XRTableCell();
            XRTableCell cell1 = new XRTableCell();
            XRTableCell cell2 = new XRTableCell();

            cell0.WidthF = 150;
            cell1.WidthF = 250;
            cell2.WidthF = 150;

            cell0.Text = " ";
            cell1.Text = "";
            cell2.Text = "";

            row.Cells.Add((XRTableCell)cell0);
            row.Cells.Add((XRTableCell)cell1);
            row.Cells.Add((XRTableCell)cell2);
            row.BorderColor = Color.FromArgb(242, 242, 242);
            return row;
        }

        public void SetSubReport(object sender)
        {
            XRSubreport subReport = (XRSubreport)sender;

            if (subReport.Name.Equals("subReportCompanyInfo"))
            {
                Web.CP.Report.Templates.HR.ReportCompanyInfo companyInfoReport =
                    (Web.CP.Report.Templates.HR.ReportCompanyInfo)subReport.ReportSource;

            

                ReportDataSet.Report_Company_InfoDataTable compayInfoTable = ReportHelper.GetCompanyDataTable();

                companyInfoReport.DataSource = compayInfoTable;
                companyInfoReport.DataMember = "Company Information";
            }
        }
        //protected void tableCell_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        //{
        //    if (Web.CP.Report.ReportHelper.IsReportExportState())
        //    {
        //        ((DevExpress.XtraReports.UI.XRTableCell)sender).BackColor = System.Drawing.Color.Transparent;
        //        ((DevExpress.XtraReports.UI.XRTableCell)sender).BorderColor = System.Drawing.Color.Black;
        //    }
        //}

        protected void ClearBorderControlEvent(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ReportHelper.TableCell_BeforePrintEvent(sender, e);
        }
        protected void ClearBorderLessControlEvent(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ReportHelper.TableCell_BeforePrintEvent(sender, e);
        }
    }
}
