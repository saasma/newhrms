using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Web.ReportDataSetTableAdapters;
using BLL;
using Utils;
using DAL;
using BLL.Manager;
using Utils.Calendar;
using System.Collections.Generic;
using Web.UserControls;

namespace Web.CP.Report.Templates.HR
{
    public partial class ReportOtherEmployeeDetails : DevExpress.XtraReports.UI.XtraReport
    {
        public ReportOtherEmployeeDetails()
        {
            InitializeComponent();
        }

        protected void ClearBorderControlEvent(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
         //   ReportHelper.TableCell_BeforePrintEvent(sender, e);
        }
        protected void ClearBorderLessControlEvent(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //ReportHelper.TableCell_BeforePrintEvent(sender, e);
        }

        public void SetSubReport(int employeeId, object sender)
        {

            XRSubreport report1 = (XRSubreport)sender;
            Report.Templates.HR.ReportOtherEmployeeDetails reportSource = report1.ReportSource as
                Report.Templates.HR.ReportOtherEmployeeDetails;
           // Report.Templates.HR.ReportOtherEmployeeDetails report1 = new Report.Templates.HR.ReportOtherEmployeeDetails();

            try
            {
                EEmployee emp = EmployeeManager.GetEmployeeById(employeeId);

                SetGeneralInfo(emp, reportSource);
                SetAddress(emp, reportSource);
                SetHRInformation(emp, reportSource);
                SetQualification(emp, reportSource);
                SetIncomes(emp, reportSource);
            }
            catch (Exception exp1)
            {
                Log.log("Other emp details error for employee id : " + employeeId, exp1);
            }

            report1.ReportSource.DataMember = "Report";

           // this.rptViewer.Report = report1;
           // this.ReportToolbar1.ReportViewer = this.rptViewer;

        }
        void SetIncomes(EEmployee emp, Report.Templates.HR.ReportOtherEmployeeDetails rpt)
        {
            decimal? incomePf = 0, deductionPf = 0;
            List<EmployeeIncomeListResult> incomeList =
                 PayManager.GetEmployeeIncomeListWithoutAnyDeduction(emp.EmployeeId, ref incomePf, ref deductionPf);

            string values = "";

            foreach (EmployeeIncomeListResult item in incomeList)
            {
                if (values == "")
                    values = item.Title + " : " + item.Value;
                else
                    values += "\n" + item.Title + " : " + item.Value;
            }
            rpt.lblIncomes.Text = values;

            GetSalaryIncrementsTableAdapter adap = new GetSalaryIncrementsTableAdapter();
            rpt.subReportSalaryIncrement.ReportSource.DataSource = adap.GetData(emp.EmployeeId);
            rpt.subReportSalaryIncrement.ReportSource.DataMember = "Report";

            // Education
            rpt.subreportEducation.ReportSource.DataSource = (new HEducationTableAdapter()).GetData(emp.EmployeeId);
            rpt.subreportEducation.ReportSource.DataMember = "Report";

            // Trainings
            rpt.subreportTraining.ReportSource.DataSource = (new HTrainingTableAdapter()).GetData(emp.EmployeeId);
            rpt.subreportTraining.ReportSource.DataMember = "Report";

            // Prev Employee
            rpt.subreportPrevEmployee.ReportSource.DataSource = (new HPreviousEmploymentTableAdapter()).GetData(emp.EmployeeId);
            rpt.subreportPrevEmployee.ReportSource.DataMember = "Report";


            HDocumentTableAdapter adapDocs = new HDocumentTableAdapter();
            ReportDataSet.HDocumentDataTable docTable = adapDocs.GetData(emp.EmployeeId);
            foreach (ReportDataSet.HDocumentRow row in docTable.Rows)
            {

                row.Url = "~/DocumentHandler.ashx?id=" + System.IO.Path.GetFileNameWithoutExtension(row.Url).ToString();
            }
            rpt.subReportDocuments.ReportSource.DataSource = docTable;
            rpt.subReportDocuments.ReportSource.DataMember = "Report";
        }

        void SetQualification(EEmployee emp, Report.Templates.HR.ReportOtherEmployeeDetails rpt)
        {
            if (emp.HHumanResources.Count > 0)
                rpt.lblQualification.Text = string.Format("Qualification: {0}", emp.HHumanResources[0].Qualification);
            else
                rpt.lblQualification.Text = "";
            rpt.lblSkillSetList.Text = CommonManager.GetEmployeeSkillSetHTMLForReport(emp.EmployeeId);
        }

        // Return the number of years, months, days, hours, minutes, seconds,
        // and milliseconds you need to add to from_date to get to_date.
        private void GetElapsedTime(DateTime from_date, DateTime to_date,
            out int years, out int months, out int days, out int hours,
            out int minutes, out int seconds, out int milliseconds)
        {
            // If from_date > to_date, switch them around.
            if (from_date > to_date)
            {
                GetElapsedTime(to_date, from_date,
                    out years, out months, out days, out hours,
                    out minutes, out seconds, out milliseconds);
                years = -years;
                months = -months;
                days = -days;
                hours = -hours;
                minutes = -minutes;
                seconds = -seconds;
                milliseconds = -milliseconds;
            }
            else
            {
                // Handle the years.
                years = to_date.Year - from_date.Year;

                // See if we went too far.
                DateTime test_date = from_date.AddMonths(12 * years);
                if (test_date > to_date)
                {
                    years--;
                    test_date = from_date.AddMonths(12 * years);
                }

                // Add months until we go too far.
                months = 0;
                while (test_date <= to_date)
                {
                    months++;
                    test_date = from_date.AddMonths(12 * years + months);
                }
                months--;

                // Subtract to see how many more days,
                // hours, minutes, etc. we need.
                from_date = from_date.AddMonths(12 * years + months);
                TimeSpan remainder = to_date - from_date;
                days = remainder.Days;
                hours = remainder.Hours;
                minutes = remainder.Minutes;
                seconds = remainder.Seconds;
                milliseconds = remainder.Milliseconds;
            }
        }

        void SetHRInformation(EEmployee emp, Report.Templates.HR.ReportOtherEmployeeDetails rpt)
        {

            ServicePeroid firstStatus = EmployeeManager.GetServicePeroidDate(emp.EmployeeId);
            DateTime from_date, to_date;
            int years, months, days, hours;
            int minutes, seconds, milliseconds;



            GetElapsedTime(firstStatus.FromDateEng, DateTime.Now, out years, out months, out days, out hours, out minutes, out seconds, out milliseconds);

            rpt.lblEmployeeFor.Text = string.Format("Employee for {0} years, {1} months, {2} days ", years, months, days);
            rpt.lblJoinedOn.Text = string.Format("Joined On {0}", firstStatus.FromDateEng.ToShortDateString());

            int? currentStatusId = EmployeeManager.GetEmployeeCurrentStatus(emp.EmployeeId);
            //rpt.lblCurrentStatus.Text = "";
            if (currentStatusId != null)
                rpt.lblCurrentStatus.Text = string.Format("Current Status: {0}", JobStatus.GetStatusForDisplay(currentStatusId));


            string values = "";
            List<ECurrentStatus> statusList = new EmployeeManager().GetCurrentStatuses(emp.EmployeeId);
            foreach (ECurrentStatus status in statusList)
            {
                currentStatusId = status.CurrentStatus;
                if (values == "")
                    values = JobStatus.GetStatusForDisplay(currentStatusId) + " on " + status.FromDateEng.ToShortDateString();
                else
                    values += "\n" + JobStatus.GetStatusForDisplay(currentStatusId) + " on " + status.FromDateEng.ToShortDateString();
            }
            rpt.lblStatuses.Text = values;


            if (emp.HHumanResources.Count > 0)
            {
                rpt.lblBloodGroup.Text = string.Format("Blood Group: {0}", emp.HHumanResources[0].BloodGroup);
                rpt.lblPassportNo.Text = string.Format("Passport No: {0}", emp.HHumanResources[0].PassportNo);
                rpt.lblDriverLicense.Text = string.Format("Driving Licence No: {0}", emp.HHumanResources[0].DrivingLicenseNo);
                CustomDate date = CustomDate.GetCustomDateFromString(emp.HHumanResources[0].PassportValidUpto, true);

                if (string.IsNullOrEmpty(emp.HHumanResources[0].PassportNo) == false)
                    rpt.lblPassportValidUpto.Text = string.Format("Valid upto: {0}", date.EnglishDate.ToShortDateString());
                else
                    rpt.lblPassportValidUpto.Text = string.Format("Valid upto: {0}", " ");
            }
            else
            {
                rpt.lblBloodGroup.Text = "";
                rpt.lblPassportNo.Text = "";
                rpt.lblDriverLicense.Text = "";
                rpt.lblPassportValidUpto.Text = "";
            }
        }
        void SetAddress(EEmployee emp, Report.Templates.HR.ReportOtherEmployeeDetails rpt)
        {
            // Present Address
            rpt.lblPresentLocality.Text = emp.EAddresses[0].PSLocality;
            rpt.lblPresentZone.Text = "";
            if (emp.EAddresses[0].PSZoneId != null && emp.EAddresses[0].PSZoneId != -1)
                rpt.lblPresentZone.Text = CommonManager.GetZoneById(emp.EAddresses[0].PSZoneId.Value).Zone;
            rpt.lblPresentDistrict.Text = "";
            if (emp.EAddresses[0].PSDistrictId != null && emp.EAddresses[0].PSDistrictId != -1)
                rpt.lblPresentDistrict.Text = CommonManager.GetDistrictById(emp.EAddresses[0].PSDistrictId.Value).District;

            // Permanent Address
            rpt.lblPermanentLocality.Text = emp.EAddresses[0].PELocality;
            rpt.lblPermanentZone.Text = "";
            if (emp.EAddresses[0].PEZoneId != null && emp.EAddresses[0].PEZoneId != -1)
                rpt.lblPermanentZone.Text = CommonManager.GetZoneById(emp.EAddresses[0].PEZoneId.Value).Zone;
            rpt.lblPermanentDistrict.Text = "";
            if (emp.EAddresses[0].PEDistrictId != null && emp.EAddresses[0].PEDistrictId != -1)
                rpt.lblPermanentDistrict.Text = CommonManager.GetDistrictById(emp.EAddresses[0].PEDistrictId.Value).District;
            rpt.lblPermanentCountry.Text = "";
            if (emp.EAddresses[0].PECountryId != null && emp.EAddresses[0].PECountryId != -1)
                rpt.lblPermanentCountry.Text = CommonManager.GetCountryById(emp.EAddresses[0].PECountryId.Value).CountryName;

            // Official Email
            rpt.lblOfficialEmail.Text = emp.EAddresses[0].CIEmail;
            rpt.lblOfficalPhone.Text = emp.EAddresses[0].CIPhoneNo;
            rpt.lblOfficialExt.Text = emp.EAddresses[0].Extension;

            // Personal Contact
            rpt.lblPersonalEmail.Text = emp.EAddresses[0].PersonalEmail;
            rpt.lblPersonalPhone.Text = emp.EAddresses[0].PersonalPhone;
            rpt.lblPersonalMobile.Text = emp.EAddresses[0].CIMobileNo;

            // Emergency Contact
            rpt.lblEmergencyName.Text = emp.EAddresses[0].EmergencyName;
            rpt.lblEmergencyRelation.Text = emp.EAddresses[0].EmergencyRelation;
            rpt.lblEmergencyPhone.Text = emp.EAddresses[0].CIEmergencyNo;
            rpt.lblEmergencyMobile.Text = emp.EAddresses[0].EmergencyMobile;

        }
        void SetGeneralInfo(EEmployee emp, Report.Templates.HR.ReportOtherEmployeeDetails rpt)
        {
            rpt.lblName.Text = emp.Title + " " + emp.Name;
            CustomDate date = null; string value = "";
            try
            {
                // DOB
                if (SessionManager.CurrentCompany.IsEnglishDate && emp.DateOfBirth != null)
                {
                    date = CustomDate.GetCustomDateFromString(emp.DateOfBirth, true);
                    if (date.Year == CalendarCtl.StartEngYear)
                        date = null;
                }
                else if (emp.DateOfBirth != null)
                {
                    if (emp.IsEnglishDOB != null && emp.IsEnglishDOB.Value)
                    {
                        date = CustomDate.GetCustomDateFromString(emp.DateOfBirth, true);
                        if (date.Year == CalendarCtl.StartEngYear)
                            date = null;
                    }
                    else
                    {
                        date = CustomDate.GetCustomDateFromString(emp.DateOfBirth, false);
                        if (date.Year == CalendarCtl.StartNepYear)
                            date = null;
                    }
                }

                if (date != null)
                    rpt.lblAge.Text = string.Format("{0} years", GetAge(date.EnglishDate));
                else
                    rpt.lblAge.Text = "";
            }
            catch (Exception exp1)
            {
                Log.log("Other emp details error for employee id : " + emp.EmployeeId, exp1);
            }

            rpt.lblStatus.Text = emp.MaritalStatus;
            rpt.lblINo.Text = string.Format("I-No: {0}", GetValue(emp.EHumanResources[0].IdCardNo));
            rpt.lblDepartment.Text = string.Format("{0}", GetValue(emp.Department.Name));

            LeaveProjectEmployee leave = LeaveRequestManager.GetLeaveProjectForEmployee(emp.EmployeeId);
            if (leave != null)
            {
                rpt.lblTeam.Text = string.Format("Team: {0}", leave.LeaveProject.Name);
                //ddlLeaveProject.SelectedValue = leave.LeaveProjectId.ToString();
            }
            else
                rpt.lblTeam.Text = string.Format("Team: {0}", "");


            // if(emp.EHumanResources[0].lea

            value = "";
            if (emp.SubDepartmentId != null && emp.SubDepartmentId != -1)
            {
                value = new DepartmentManager().GetSubDepartmentById(emp.SubDepartmentId.Value).Name;
            }
            rpt.lblSubDepartment.Text = string.Format("Sub-Department: {0}", GetValue(value));
            rpt.lblDesignation.Text = string.Format("Designation: {0}", GetValue(emp.EDesignation.Name));
            value = "";
            //if (emp.TierId != null && emp.TierId != -1)
            //{
            //    value = new CommonManager().GetTierById(emp.TierId.Value).Name;
            //}
            //rpt.lblTier.Text = string.Format("Tier: {0}", GetValue(value));

            if (emp.HHumanResources.Count > 0 && !string.IsNullOrEmpty(emp.HHumanResources[0].UrlPhotoThumbnail))
                rpt.pic.ImageUrl = Config.UploadLocation + "/" + emp.HHumanResources[0].UrlPhotoThumbnail;


        }


        public string GetValue(string value)
        {
            if (value == null)
                return "";
            return value.ToString();
        }

        public int GetAge(DateTime bday)
        {
            DateTime today = DateTime.Today;
            int age = today.Year - bday.Year;
            if (bday > today.AddYears(-age)) age--;

            return age;
        }

        private void pic_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            var pb = sender as XRPictureBox;
            if (pb == null) return;

            var anchor = pb.Tag as string;
            if (string.IsNullOrEmpty(anchor) == true) return;

            var img = pb.Image;
            if (img == null) return;

            var parentSize = pb.SizeF;
            var parentLocation = pb.LocationF;

            // set XRPictureBox size to the image size
            if (img.Width > parentSize.Width || img.Height > parentSize.Height)
            {
                // estimate image size
                var wr = img.Width / (float)parentSize.Width;
                var hr = img.Height / (float)parentSize.Height;
                var r = Math.Max(wr, hr);

                var size = new SizeF(img.Width / r, img.Height / r);
                pb.SizeF = size;
            }
            else
            {
                var w = Math.Min(img.Width, parentSize.Width);
                var h = Math.Min(img.Height, parentSize.Height);
                pb.SizeF = new SizeF(w, h);
            }

            if (anchor.Contains("Left"))
                pb.LeftF = parentLocation.X;
            else if (anchor.Contains("Right"))
                pb.LeftF = parentLocation.X + parentSize.Width - pb.SizeF.Width;
            else if (anchor.Contains("Center"))
                pb.LeftF = parentLocation.X + ((parentSize.Width - pb.SizeF.Width) / 2);

            if (anchor.Contains("Top"))
                pb.TopF = parentLocation.Y;
            else if (anchor.Contains("Bottom"))
                pb.TopF = parentLocation.Y + parentSize.Height - pb.SizeF.Height;
            else if (anchor.Contains("Middle"))
                pb.TopF = parentLocation.Y + ((parentSize.Height - pb.SizeF.Height) / 2);
        }
    }
}
