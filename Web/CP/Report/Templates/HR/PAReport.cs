using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Web;
using System.Data;

namespace Web.CP.Report.Templates.HR
{
    public partial class PAReport : DevExpress.XtraReports.UI.XtraReport
      
      {
        public PAReport()
        {
            InitializeComponent();
       
          
        }

        //public void SetSubReport(object sender)
        //{
        //    ReportHelper.BindCompanyInfo(sender);
        //}

        protected void ClearBorderControlEvent(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ReportHelper.TableCell_BeforePrintEvent(sender, e);
        }
        protected void ClearBorderLessControlEvent(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ReportHelper.TableCell_BeforePrintEvent(sender, e);
        }

        private void subReportPAR_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRSubreport report = (XRSubreport)sender;

            Web.CP.Report.Templates.HR.PAReport mainReport =
            (Web.CP.Report.Templates.HR.PAReport)report.Report;
         
            mainReport.SetSubReport(sender);
        }

        public void SetSubReport(object sender)
        {
            XRSubreport subReport = (XRSubreport)sender;

            string attendanceTotalWeek1 = "0", attendanceTotalWeek2 = "0",
            attendanceTotalWeek3 = "0", attendanceTotalWeek4 = "0", attendanceTotalWeek5 = "0", attendanceTotalHours = "0";


            string leaveTotalWeek1 = "0", leaveTotalWeek2 = "0",
            leaveTotalWeek3 = "0", leaveTotalWeek4 = "0", leaveTotalWeek5 = "0", leaveTotalHours = "0";

           

            //ReportDataSet.GetTimeAllocationInfoByWeekDataTable _TimeAllocationInfoTable =
            //    ((System.Data.DataTable)(HttpContext.Current.Items["PARTimeAllocationInfo"])).DefaultView.Table
            //    //HttpContext.Current.Items["PARTimeAllocationInfo"]
            //   as ReportDataSet.GetTimeAllocationInfoByWeekDataTable;

            ReportDataSet.GetTimeAllocationInfoByWeekDataTable _TimeAllocationInfoTable = new ReportDataSet.GetTimeAllocationInfoByWeekDataTable();
            foreach (DataRow dr in ((System.Data.DataTable)(HttpContext.Current.Items["PARTimeAllocationInfo"])).DefaultView.Table.Rows)
            {

                _TimeAllocationInfoTable.ImportRow(dr);
            }



            switch (subReport.Name)
            {
                case "subReportPAR":

                    Web.CP.Report.Templates.HR.PAReportTimeAllocationInfo attendanceReport =
                       (Web.CP.Report.Templates.HR.PAReportTimeAllocationInfo)subReport.ReportSource;

               

                    if (_TimeAllocationInfoTable != null)
                    {
                        attendanceReport.DataSource = _TimeAllocationInfoTable;
                        attendanceReport.DataMember = "PAR Report";

                        attendanceTotalWeek1 = _TimeAllocationInfoTable.Compute("Sum(Week1)", "").ToString();
                        attendanceTotalWeek2 = _TimeAllocationInfoTable.Compute("Sum(Week2)", "").ToString();
                        attendanceTotalWeek3 = _TimeAllocationInfoTable.Compute("Sum(Week3)", "").ToString();
                        attendanceTotalWeek4 = _TimeAllocationInfoTable.Compute("Sum(Week4)", "").ToString();
                        attendanceTotalWeek5 = _TimeAllocationInfoTable.Compute("Sum(Week5)", "").ToString();
                        attendanceTotalHours = _TimeAllocationInfoTable.Compute("Sum(TotalHours)", "").ToString();
                    }
                    break;


                case "SubreportPAReportLeave":

                    Web.CP.Report.Templates.HR.PAReportLeaveInfo _LeaveReport =
                       (Web.CP.Report.Templates.HR.PAReportLeaveInfo)subReport.ReportSource;

                   // ReportDataSet.ReportGetEmployeeLeaveListDataTable _LeaveInfoTable = ((System.Data.DataTable)(HttpContext.Current.Items["PAREmployeeLeaveInfo"])).DefaultView.Table
                   // as ReportDataSet.ReportGetEmployeeLeaveListDataTable;

                    ReportDataSet.ReportGetEmployeeLeaveListDataTable _LeaveInfoTable = new ReportDataSet.ReportGetEmployeeLeaveListDataTable();
                    foreach (DataRow dr in ((System.Data.DataTable)(HttpContext.Current.Items["PAREmployeeLeaveInfo"])).DefaultView.Table.Rows)
                    {
                      
                        _LeaveInfoTable.ImportRow(dr);
                    }


                   // ReportDataSet.ReportGetEmployeeLeaveListDataTable _LeaveInfoTable = ((System.Data.DataTable)(HttpContext.Current.Items["PAREmployeeLeaveInfo"])).DefaultView.Table.Copy();
                    


                    if (_TimeAllocationInfoTable != null)
                    {
                      
                        attendanceTotalWeek1 = _TimeAllocationInfoTable.Compute("Sum(Week1)", "").ToString();
                        attendanceTotalWeek2 = _TimeAllocationInfoTable.Compute("Sum(Week2)", "").ToString();
                        attendanceTotalWeek3 = _TimeAllocationInfoTable.Compute("Sum(Week3)", "").ToString();
                        attendanceTotalWeek4 = _TimeAllocationInfoTable.Compute("Sum(Week4)", "").ToString();
                        attendanceTotalWeek5 = _TimeAllocationInfoTable.Compute("Sum(Week5)", "").ToString();
                        attendanceTotalHours = _TimeAllocationInfoTable.Compute("Sum(TotalHours)", "").ToString();
                    }


                    if (_LeaveInfoTable != null)
                    {
                        _LeaveReport.DataSource = _LeaveInfoTable;
                        _LeaveReport.DataMember = "PAR Report";



                        leaveTotalWeek1 = _LeaveInfoTable.Compute("Sum(Week1)", "").ToString();
                        leaveTotalWeek2 = _LeaveInfoTable.Compute("Sum(Week2)", "").ToString();
                        leaveTotalWeek3 = _LeaveInfoTable.Compute("Sum(Week3)", "").ToString();
                        leaveTotalWeek4 = _LeaveInfoTable.Compute("Sum(Week4)", "").ToString();
                        leaveTotalWeek5 = _LeaveInfoTable.Compute("Sum(Week5)", "").ToString();
                        leaveTotalHours = _LeaveInfoTable.Compute("Sum(TotalHours)", "").ToString();
                    }

                    _LeaveReport.xrWeekTotal1.Text = (decimal.Parse((leaveTotalWeek1==""?"0":leaveTotalWeek1)) + decimal.Parse(attendanceTotalWeek1==""?"0":attendanceTotalWeek1)).ToString();
                    _LeaveReport.xrWeekTotal2.Text = (decimal.Parse(leaveTotalWeek2==""?"0":leaveTotalWeek2) + decimal.Parse(attendanceTotalWeek2==""?"0":attendanceTotalWeek2)).ToString();
                    _LeaveReport.xrWeekTotal3.Text = (decimal.Parse(leaveTotalWeek3==""?"0":leaveTotalWeek3) + decimal.Parse(attendanceTotalWeek3==""?"0":attendanceTotalWeek3)).ToString();
                    _LeaveReport.xrWeekTotal4.Text = (decimal.Parse(leaveTotalWeek4==""?"0":leaveTotalWeek4) + decimal.Parse(attendanceTotalWeek4==""?"0":attendanceTotalWeek4)).ToString();
                    _LeaveReport.xrWeekTotal5.Text = (decimal.Parse(leaveTotalWeek5==""?"0":leaveTotalWeek5) + decimal.Parse(attendanceTotalWeek5==""?"0":attendanceTotalWeek5)).ToString();
                    _LeaveReport.xrWeekTotal.Text = (decimal.Parse(leaveTotalHours == "" ? "0" : leaveTotalHours) + decimal.Parse(attendanceTotalHours == "" ? "0" : attendanceTotalHours)).ToString();


                    break;




            }





        }

        private void SubreportPAReportLeave_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRSubreport report = (XRSubreport)sender;

            Web.CP.Report.Templates.HR.PAReport mainReport =
            (Web.CP.Report.Templates.HR.PAReport)report.Report;
            mainReport.SetSubReport(sender);
        }


     


       


      
    }
}
