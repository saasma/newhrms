﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using BLL;
using DevExpress.XtraReports.UI;
using Web.CP.Report.Templates.HR;
using Web.ReportDataSetTableAdapters;
using Utils.Calendar;
using BLL.Base;
using DAL;
using BLL.Manager;
using System.Data;

namespace Web.CP.Report
{
    public partial class PayPFStatement : BasePage
    {


        protected  void LoadReportHandler()
        {}
      
        protected void Page_Load(object sender, EventArgs e)
        {
            //report.Filter.PayrollFrom = false;
            report.Filter.PayrollTo = false;
            report.Filter.Employee = false;
            report.Filter.CostCode = true;
            report.Filter.Unit = true;
            report.Filter.Program = true;
            report.Filter.SubDepartment = true;

            if (SessionManager.CurrentCompany.PFRFFunds.Count >= 1
           && SessionManager.CurrentCompany.PFRFFunds[0].HasOtherPFFund != null
           && SessionManager.CurrentCompany.PFRFFunds[0].HasOtherPFFund.Value)
            {
                report.Filter.PFType = true;
            }
           // if (!IsPostBack)
                LoadReport();
        }
        protected void LoadReport()
        {

            PayrollPeriod payrollPeriod = CommonManager.GetPayrollPeriod(report.Filter.StartDate.Month,
                                                                         report.Filter.StartDate.Year);
            if (payrollPeriod == null)
                return;

            ReportDataSetTableAdapters.Report_Pay_PFStatementTableAdapter
                adap = new Report_Pay_PFStatementTableAdapter();

            CCalculation calc = CalculationManager.GetCalculation(payrollPeriod.PayrollPeriodId);

            Report.Templates.Pay.ReportPF report1 = new Report.Templates.Pay.ReportPF();
            BLL.BaseBiz.SetConnectionPwd(adap.Connection);
            report1.DataSource = adap.GetData( 
                 report.Filter.StartDate.Month, report.Filter.StartDate.Year,SessionManager.CurrentCompanyId,
                 report.Filter.BranchId,report.Filter.DepartmentId,report.Filter.SubDepartmentId, report.Filter.DepartmentName
                 ,report.Filter.CostCodeId,report.Filter.ProgramId,report.Filter.PFTypeValue, report.Filter.UnitId);
            report1.DataMember = "Report";

            string month = string.Empty;
            if (report.Filter.StartDate.Month.ToString().Trim().Length.Equals(1))
                month = "0" + report.Filter.StartDate.Month.ToString();
            else
                month = report.Filter.StartDate.Month.ToString();

            string payrollMonth = report.Filter.StartDate.Year.ToString().Substring(2, 2) + month;
            report1.fromMonthCell.Text = payrollMonth;
            report1.toMonthCell.Text = payrollMonth;

            //Commented by Santosh 30 august 2011
            ////set report texts
            //report1.labelTitle.Text += payrollPeriod.Name;
            //if (report.Filter.BranchId != -1)
            //    report1.labelBranch.Text = report.Filter.BranchName;
            //if (report.Filter.DepartmentId != -1)
            //    report1.labelDepartment.Text = report.Filter.DepartmentName;

            //if (calc != null)
            //{
            //    report1.cellEmployeeCont.Text = string.Format(report1.cellEmployeeCont.Text,
            //        calc.PFEmployeeContribution);
            //    report1.cellEmployerCont.Text = string.Format(report1.cellEmployerCont.Text,
            //                    calc.PFCompanyContribution);
            //}
            //else
            //{
            //    report1.cellEmployeeCont.Text = string.Format(report1.cellEmployeeCont.Text,
            //       "-");
            //    report1.cellEmployerCont.Text = string.Format(report1.cellEmployerCont.Text,
            //                   "-");
            //}

            report.DisplayReport(report1);

        }
    }
}
