﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//using Web.CP.Report.Templates;

using BLL;
using DevExpress.XtraReports.UI;
using Web.ReportDataSetTableAdapters;
using Utils.Calendar;
using BLL.Base;
using DAL;
using BLL.Manager;
using Ext.Net;
namespace Web.CP.Report
{
    public partial class ClericalNonClericalReport : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.HumanResource;
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            report.Filter.Visible = false;

            cmbBranch.Store[0].DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            cmbBranch.Store[0].DataBind();

            DepartmentManager _DepartmentManager = new DepartmentManager();
            cmbDepartment.Store[0].DataSource = _DepartmentManager.GetDepartmentsByCompany(SessionManager.CurrentCompanyId);
            cmbDepartment.Store[0].DataBind();

            if (!IsPostBack)
            {
                txtFromDate.Text = CustomDate.GetTodayDate(IsEnglish).ToString();
                //txtToDate.Text = txtFromDate.Text;

                //btnSearch_Click(null, null);
            }
            LoadReport();
            
        }

        protected void LoadReport()
        {
            Report_ClericalNonClericalTableAdapter
                adap = new Report_ClericalNonClericalTableAdapter();

            Report.Templates.HR.ReportClerical report1 = new Report.Templates.HR.ReportClerical();

            int branchId = -1, departmentId = -1, employeeId = -1;

            if (cmbBranch.SelectedItem != null && cmbBranch.SelectedItem.Value != null)
                branchId = int.Parse(cmbBranch.SelectedItem.Value);

            if (cmbDepartment.SelectedItem != null && cmbDepartment.SelectedItem.Value != null)
                departmentId = int.Parse(cmbDepartment.SelectedItem.Value);

            if (cmbEmpSearch.SelectedItem != null && cmbEmpSearch.SelectedItem.Value != null)
                employeeId = int.Parse(cmbEmpSearch.SelectedItem.Value);

            //DateTime fromDate = GetEngDate(txtFromDate.Text);
            //DateTime toDate = GetEngDate(txtToDate.Text);
            CustomDate date = CustomDate.GetCustomDateFromString(txtFromDate.Text.Trim(), IsEnglish);
            PayrollPeriod period = CommonManager.GetPayrollPeriod(date.Month, date.Year);

            int periodId = 0;

            if (period != null)
                periodId = period.PayrollPeriodId; 


            ClericalDataClass obj = new ClericalDataClass() { BranchId = branchId, DepartmentId = departmentId, EmployeeId = employeeId, PayrollPeriodId = periodId };
            Session["clericalClassData"] = obj;

            BLL.BaseBiz.SetConnectionPwd(adap.Connection);
            report1.DataSource = adap.GetData(branchId, departmentId, periodId, employeeId);
            report1.DataMember = "ReportClericalNonClerical";


         
            string monthName = DateHelper.GetMonthName(date.Month, false);

            report1.lblName.Text = string.Format("Office wise Employee Count for the month of {0} {1}", monthName, date.Year.ToString());

            report.DisplayReport(report1);

            if (ReportHelper.IsReportExportState())
            {
                this.report.clearCache = true;
            }

            if (employeeId != -1)
            {
                EEmployee emp = EmployeeManager.GetEmployeeById(employeeId);
                cmbEmpSearch.SetRawValue(emp.Name + " - " + emp.EmployeeId.ToString());
            }

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            

            //LoadReport();

        }
    }

    public class ClericalDataClass
    {
        public int BranchId { get; set; }
        public int DepartmentId { get; set; }
        public int EmployeeId { get; set; }
        //public DateTime FromDate { get; set; }
        //public DateTime ToDate { get; set; }
        public int PayrollPeriodId { get; set; }
    }
}