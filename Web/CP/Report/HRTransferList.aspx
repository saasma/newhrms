<%@ Page Title="HR Change List Report" Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/Master/Report.Master" AutoEventWireup="true" CodeBehind="HRTransferList.aspx.cs" Inherits="Web.CP.Report.HRTransferList" %>
<%@ Register assembly="DevExpress.XtraReports.v16.1.Web, Version=16.1.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraReports.Web" tagprefix="dxxr" %>
<%@ Register src="~/Controls/Report/ReportFilterBranchDep.ascx" tagname="ReportFilterBranchDep" tagprefix="uc1" %>
<%@ Register src="~/Controls/Report/ReportContainer.ascx" tagname="ReportContainer" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <uc1:ReportContainer runat="server" ViewerPaddingLeft="20px"   OnReloadReport="LoadReport"  id="report" />
</asp:Content>
