﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//using Web.CP.Report.Templates;

using BLL;
using DevExpress.XtraReports.UI;
using Web.ReportDataSetTableAdapters;
using Utils.Calendar;
using BLL.Base;
using BLL.Manager;
using DAL;

namespace Web.CP.Report
{
    public partial class HRLeaveBalanceSummary : BasePage
    {

        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.LeaveAndAttendance;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            report.Filter.PayrollTo = true;
            report.Filter.SubDepartment = false;
            report.Filter.Leave = true;


            //if (!IsPostBack || CommonManager.CompanySetting.RemoveStyleWhileExportingReport )
            if (IsPostBack)
                LoadReport();

        }

        protected void LoadReport() 
        {

            if (!IsPostBack)
                return;

            PayrollPeriod payrollPeriodFrom = CommonManager.GetPayrollPeriod(report.Filter.StartDate.Month,
                                                                        report.Filter.StartDate.Year);
            PayrollPeriod payrollPeriodTo = CommonManager.GetPayrollPeriod(report.Filter.EndDate.Month,
                                                                                     report.Filter.EndDate.Year);
            //if null get first payroll period
            if (payrollPeriodFrom == null)
                payrollPeriodFrom = CommonManager.GetFirstPayrollPeriod(SessionManager.CurrentCompanyId);

            if (payrollPeriodTo == null)
                payrollPeriodTo = CommonManager.GetLastPayrollPeriod();

            //again null then no report
            if (payrollPeriodFrom == null || payrollPeriodTo == null)
                return;

            //for report to date should be greater or equal to from date
            if (payrollPeriodTo.PayrollPeriodId < payrollPeriodFrom.PayrollPeriodId)
                return;


            ReportDataSetTableAdapters.Report_HR_LeaveBalanceMonthsTableAdapter
                adap = new Report_HR_LeaveBalanceMonthsTableAdapter();


            Report.Templates.HR.ReportLeaveBalance report1 = new Report.Templates.HR.ReportLeaveBalance();

            BLL.BaseBiz.SetConnectionPwd(adap.Connection);
            report1.DataSource = adap.GetData(
                payrollPeriodFrom.Month, payrollPeriodFrom.Year,
                payrollPeriodTo.Month, payrollPeriodTo.Year,
                SessionManager.CurrentCompanyId,
               report.Filter.EmployeeName,report.Filter.LeaveTypeId,report.Filter.BranchId, report.Filter.DepartmentId,report.Filter.SubDepartmentId,
               report.Filter.EmployeeId,report.Filter.DepartmentName,
               (SessionManager.CurrentLoggedInEmployeeId!= 0));

            report1.DataMember = "Report";

            report1.labelTitle.Text = "Leave Summary from ";
            //if same payroll then hide group header
            //if (report.Filter.StartDate == report.Filter.EndDate)
            //{
            //    report1.GroupHeader.Visible = false;
            //    report1.XRTitle.Text += DateHelper.GetMonthShortName(report.Filter.StartDate.Month, IsEnglish)
            //                            + "/" + report.Filter.StartDate.Year;
            //}
            //else
            //{
            report1.labelTitle.Text +=
                string.Format("{0}/{1} ({4}) to {2}/{3} ({5})",
                DateHelper.GetMonthShortName(payrollPeriodFrom.Month, IsEnglish),
                payrollPeriodFrom.Year,
                DateHelper.GetMonthShortName(payrollPeriodTo.Month, IsEnglish),
                 payrollPeriodTo.Year,
                payrollPeriodFrom.StartDateEng.Value.ToString("yyyy-MMM-dd"),
                payrollPeriodTo.EndDateEng.Value.ToString("yyyy-MMM-dd")
                );


            report.DisplayReport(report1);

            if (ReportHelper.IsReportExportState())
            {
                this.report.clearCache = true;

            }


        }
    }
}
