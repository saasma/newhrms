﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using BLL;
using DevExpress.XtraReports.UI;
using Web.CP.Report.Templates.HR;
using Web.ReportDataSetTableAdapters;
using Utils.Calendar;
using BLL.Base;
using DAL;
using BLL.Manager;
using System.Data;
using Utils;
using System.IO;

namespace Web.CP.Report
{
    public partial class PayDetail : BasePage
    {


        protected  void LoadReportHandler()
        {}
      
        protected void Page_Load(object sender, EventArgs e)
        {
            //report.Filter.PayrollFrom = false;
            report.Filter.PayrollTo = false;
            report.Filter.Branch = true;
            report.Filter.Department = false;
            //report.Filter.Employee = false;

            //if (!IsPostBack || CommonManager.CompanySetting.RemoveStyleWhileExportingReport)
                LoadReport();
        }

        protected void LoadReport()
        {
            PayrollPeriod payrollPeriod = CommonManager.GetPayrollPeriod(report.Filter.StartDate.Month,
                                                                         report.Filter.StartDate.Year);

            if( payrollPeriod ==null)
                return;


            Report_Pay_PaySlipDetailTableAdapter mainAdapter = new Report_Pay_PaySlipDetailTableAdapter();

            bool readingSumForMiddleFiscalYearStartedReqd = false;
            int startingPayrollPeriodId = 0, endingPayrollPeriodId = 0;
            PayrollPeriod startingMonthParollPeriodInFiscalYear =
               CalculationManager.GenerateForPastIncome(
                    payrollPeriod.PayrollPeriodId, ref readingSumForMiddleFiscalYearStartedReqd,
                    ref startingPayrollPeriodId, ref endingPayrollPeriodId);


            Report.Templates.Pay.Detail.ReportPayDetail mainReport = new Report.Templates.Pay.Detail.ReportPayDetail();

            if (CommonManager.CalculationConstant.CompanyHasHourlyLeave.Value)
            {
                mainReport.labelPayDays.Visible = false;
                mainReport.labelPayDaysValue.Visible = false;
                mainReport.labelPresentDays.Visible = false;
                mainReport.labelPresentDaysValue.Visible = false;
            }

            mainReport.labelCompanyName.Text = SessionManager.CurrentCompany.Name;
            mainReport.labelTitle.Text += payrollPeriod.Name;
            string logoLoc = ReportManager.PaySlipLogoAbsolutePath;
            if (File.Exists(logoLoc))
            {
                mainReport.logo.Image = new System.Drawing.Bitmap(logoLoc);
                mainReport.logo.Visible = true;
            }


            BLL.BaseBiz.SetConnectionPwd(mainAdapter.Connection);
            ReportDataSet.Report_Pay_PaySlipDetailDataTable mainTable =
                mainAdapter.GetData(payrollPeriod.Month, payrollPeriod.Year,
                                                                      SessionManager.CurrentCompanyId,report.Filter.EmployeeId,report.Filter.EmployeeName.Trim(),"",1,null,report.Filter.BranchId);




            #region "Income/Deduction/Atte List"

            // Code to retrieve all emp once & save the DataTable in the request Cache so that multiple db request can be prevented
            // due to large no of employees
            Report_Pay_PaySlipDetail_EmpHeaderTableAdapter incomeAdapter = new Report_Pay_PaySlipDetail_EmpHeaderTableAdapter();
            BLL.BaseBiz.SetConnectionPwd(incomeAdapter.Connection);
            Report_Pay_PaySlipDetail_DeductionTableAdapter deductionAdap = new Report_Pay_PaySlipDetail_DeductionTableAdapter();        
            BLL.BaseBiz.SetConnectionPwd(deductionAdap.Connection);
            //Report_Pay_PaySlipDetail_LoanTableAdapter loanAdapter = new Report_Pay_PaySlipDetail_LoanTableAdapter();
            //BLL.BaseBiz.SetConnectionPwd(loanAdapter.Connection);
            //Report_Pay_PaySlipDetail_SimpleLoanTableAdapter loanSimpleAdapter = new Report_Pay_PaySlipDetail_SimpleLoanTableAdapter();
            //BLL.BaseBiz.SetConnectionPwd(loanSimpleAdapter.Connection);
            //Report_Pay_PaySlipDetail_LeaveTableAdapter leaveAdapter = new Report_Pay_PaySlipDetail_LeaveTableAdapter();
            //BLL.BaseBiz.SetConnectionPwd(leaveAdapter.Connection);
            //Report_Pay_PaySlipDetail_AdvanceTableAdapter advanceAdapter = new Report_Pay_PaySlipDetail_AdvanceTableAdapter();
            //BLL.BaseBiz.SetConnectionPwd(advanceAdapter.Connection);

            //incomes
            ReportDataSet.Report_Pay_PaySlipDetail_EmpHeaderDataTable incomesTable = incomeAdapter.GetData(payrollPeriod.Month, payrollPeriod.Year,
                                              SessionManager.CurrentCompanyId, report.Filter.EmployeeId, true, null, startingPayrollPeriodId, endingPayrollPeriodId, 1, null);
            //deductions
            ReportDataSet.Report_Pay_PaySlipDetail_DeductionDataTable deductionTable
                         = deductionAdap.GetData(payrollPeriod.Month, payrollPeriod.Year, SessionManager.CurrentCompanyId, report.Filter.EmployeeId, null);

            //EMI
            //ReportDataSet.Report_Pay_PaySlipDetail_LoanDataTable loanTable = loanAdapter.GetData(payrollPeriod.Month, payrollPeriod.Year,
            //                                SessionManager.CurrentCompanyId, report.Filter.EmployeeId, null);
            ////Simple Loan
            //ReportDataSet.Report_Pay_PaySlipDetail_SimpleLoanDataTable loanSimpleTable = loanSimpleAdapter.GetData(payrollPeriod.Month, payrollPeriod.Year,
            //                                      SessionManager.CurrentCompanyId, report.Filter.EmployeeId, null);

            //ReportDataSet.Report_Pay_PaySlipDetail_LeaveDataTable leaveTable = leaveAdapter.GetData(payrollPeriod.Month, payrollPeriod.Year,
            //                                 SessionManager.CurrentCompanyId, report.Filter.EmployeeId, null);

            //ReportDataSet.Report_Pay_PaySlipDetail_AdvanceDataTable advanceTable = advanceAdapter.GetData(payrollPeriod.Month, payrollPeriod.Year,
            //                                  SessionManager.CurrentCompanyId, report.Filter.EmployeeId, null);


            try
            {
                //to prevent db timeout due to 100 connection full
                incomeAdapter.Connection.Close();
                deductionAdap.Connection.Close();
                //loanAdapter.Connection.Close();
                //loanSimpleAdapter.Connection.Close();
                //leaveAdapter.Connection.Close();
                //advanceAdapter.Connection.Close();
            }
            catch (Exception exp)
            {
                Log.log("Error", exp);
            }
            HttpContext.Current.Items["PaySlipDetailIncomeList"] = incomesTable;
            HttpContext.Current.Items["PaySlipDetailDeductionList"] = deductionTable;
            //HttpContext.Current.Items["PaySlipDetailLoanList"] = loanTable;
            //HttpContext.Current.Items["PaySlipDetailSimpleLoanList"] = loanSimpleTable;
            //HttpContext.Current.Items["PaySlipDetailLeaveList"] = leaveTable;
            //HttpContext.Current.Items["PaySlipDetailAdvanceList"] = advanceTable;
            #endregion


            mainReport.Month = payrollPeriod.Month;
            mainReport.Year = payrollPeriod.Year.Value;
            mainReport.IsIncome = true;
            mainReport.CompanyId = SessionManager.CurrentCompanyId;


            

            mainReport.DataSource = mainTable;
            mainReport.DataMember = "Report";

            

            report.DisplayReport(mainReport);

            if(ReportHelper.IsReportExportState())
            {
                this.report.clearCache = true;
            }
            ///If no loan then hide loan section
            

            ///If no advance then hide advance section
            

        }
    }
}
