﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using BLL;
using DevExpress.XtraReports.UI;
using Web.CP.Report.Templates.HR;
using Web.ReportDataSetTableAdapters;
using Utils.Calendar;
using BLL.Base;
using DAL;
using BLL.Manager;
using System.Data;

namespace Web.CP.Report
{
    public partial class PayCash : BasePage
    {
        protected  void LoadReportHandler()
        {        
        }
      
        protected void Page_Load(object sender, EventArgs e)
        {
            //report.Filter.PayrollFrom = false;
            report.Filter.PayrollTo = false;
            report.Filter.Branch = true;
            report.Filter.Department = true;
            report.Filter.Employee = false;
            report.Filter.CostCode = true;
            report.Filter.Unit = true;
            report.Filter.Program = true;
           // if (!IsPostBack || CommonManager.CompanySetting.RemoveStyleWhileExportingReport)
                LoadReport();
        }

        protected void LoadReport()
        {
            PayrollPeriod payrollPeriod = CommonManager.GetPayrollPeriod(report.Filter.StartDate.Month,
                                                                         report.Filter.StartDate.Year);
            if( payrollPeriod ==null)
                return;

            Report_Pay_CashTableAdapter mainAdapter = new Report_Pay_CashTableAdapter();
            
            Report.Templates.Pay.ReportPayCash mainReport = new Report.Templates.Pay.ReportPayCash();

            BLL.BaseBiz.SetConnectionPwd(mainAdapter.Connection);
            ReportDataSet.Report_Pay_CashDataTable mainTable =
                mainAdapter.GetData(payrollPeriod.Month, payrollPeriod.Year, SessionManager.CurrentCompanyId,
                report.Filter.BranchId, report.Filter.DepartmentId,report.Filter.CostCodeId
                ,report.Filter.ProgramId, report.Filter.UnitId);

            mainReport.labelTitle.Text += payrollPeriod.Name;            

            //mainReport.Month = payrollPeriod.Month;
            //mainReport.Year = payrollPeriod.Year.Value;
            //mainReport.IsIncome = true;
            //mainReport.CompanyId = SessionManager.CurrentCompanyId;
           
            mainReport.DataSource = mainTable;
            mainReport.DataMember = "Report";
           
            report.DisplayReport(mainReport);

            if(ReportHelper.IsReportExportState())
            {
                this.report.clearCache = true;
            }
        }
    }
}
