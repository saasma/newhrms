﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//using Web.CP.Report.Templates;

using BLL;
using DevExpress.XtraReports.UI;
using Web.CP.Report.Templates.HR;
using Web.ReportDataSetTableAdapters;
using Utils.Calendar;
using BLL.Base;
using BLL.Manager;

namespace Web.CP.Report
{
    public partial class HREmployeeList : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            report.Filter.PayrollFrom = false;
            report.Filter.PayrollTo = false;
            report.Filter.SubDepartment = true;
       //     if (!IsPostBack || CommonManager.CompanySetting.RemoveStyleWhileExportingReport)
                LoadReport();
        }

        protected void LoadReport()
        {
            Report_HR_EmpListTableAdapter
                adap = new Report_HR_EmpListTableAdapter();


            ReportEmployeeList report1 = new ReportEmployeeList();

            BLL.BaseBiz.SetConnectionPwd(adap.Connection);
            report1.DataSource = adap.GetData(report.Filter.EmployeeName, report.Filter.BranchId,report.Filter.DepartmentId,
                                           report.Filter.SubDepartmentId, SessionManager.CurrentCompanyId,report.Filter.DepartmentName);
            report1.DataMember = "EmployeeListReport";
            
            report.DisplayReport(report1);

            if (ReportHelper.IsReportExportState())
            {
                this.report.clearCache = true;
            }
        }
       
      
    }
}
