﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using BLL;
using DevExpress.XtraReports.UI;
using Web.CP.Report.Templates.HR;
using Web.ReportDataSetTableAdapters;
using Utils.Calendar;
using BLL.Base;
using DAL;
using BLL.Manager;
using System.Data;

namespace Web.CP.Report
{
    public partial class PayTDS : BasePage
    {
        protected  void LoadReportHandler()
        {        
        }
      
        protected void Page_Load(object sender, EventArgs e)
        {
            //report.Filter.PayrollFrom = false;
            report.Filter.PayrollTo = false;
            report.Filter.Branch = true;
            report.Filter.Department = false;
            report.Filter.Employee = false;
            report.Filter.CostCode = true;
            report.Filter.Program = true;
            report.Filter.Unit = true;
            report.Filter.AddOn = true;
            report.Filter.Employee = true;
           // if (!IsPostBack)
                LoadReport();
        }

        protected void LoadReport()
        {
            PayrollPeriod payrollPeriod = CommonManager.GetPayrollPeriod(report.Filter.StartDate.Month,
                                                                         report.Filter.StartDate.Year);
            if( payrollPeriod ==null)
                return;

            report.Filter.LoadAddOn(payrollPeriod.PayrollPeriodId);

            Report_Pay_TDSTableAdapter mainAdapter = new Report_Pay_TDSTableAdapter();

            Report.Templates.Pay.ReportPayTDS mainReport = new Report.Templates.Pay.ReportPayTDS();
            BLL.BaseBiz.SetConnectionPwd(mainAdapter.Connection);
            ReportDataSet.Report_Pay_TDSDataTable mainTable =
                mainAdapter.GetData(payrollPeriod.Month, payrollPeriod.Year, SessionManager.CurrentCompanyId, report.Filter.BranchId, report.Filter.DepartmentId,false
                ,report.Filter.CostCodeId,report.Filter.ProgramId,report.Filter.AddOnId, report.Filter.UnitId, null, null, null,report.Filter.EmployeeId);

            //mainReport.labelTitle.Text += payrollPeriod.Name;       
           
            mainReport.DataSource = mainTable;
            mainReport.DataMember = "Report";
           
            report.DisplayReport(mainReport);
        }
    }
}
