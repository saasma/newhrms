﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//using Web.CP.Report.Templates;

using BLL;
using DevExpress.XtraReports.UI;
using Web.ReportDataSetTableAdapters;
using Utils.Calendar;
using BLL.Base;
using DAL;
using BLL.Manager;

namespace Web.CP.Report
{
    public partial class PayLeaveEncashmet : BasePage
    {


        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
                report.BindLeaves();

            report.Filter.Employee = false;
            report.Filter.Income = true;
            report.Filter.PayrollTo = false;
            report.Filter.Income = false;
            report.Filter.Leave = true;
            //if (!IsPostBack || CommonManager.CompanySetting.RemoveStyleWhileExportingReport)
                LoadReport();
        }

        protected void LoadReport()
        {
            PayrollPeriod payrollPeriod = CommonManager.GetPayrollPeriod(report.Filter.StartDate.Month,
                                                                         report.Filter.StartDate.Year);
            if (payrollPeriod == null)
                return;


            Report_Pay_PossibleEncashmentTableAdapter
                adap = new Report_Pay_PossibleEncashmentTableAdapter();


            Report.Templates.Pay.ReportLeaveProvision report1 = new Report.Templates.Pay.ReportLeaveProvision();
            BLL.BaseBiz.SetConnectionPwd(adap.Connection);
            report1.DataSource = adap.GetData(
                payrollPeriod.PayrollPeriodId, SessionManager.CurrentCompanyId,
               report.Filter.BranchId, report.Filter.DepartmentId, report.Filter.DepartmentName
               , report.Filter.LeaveTypeId, LeaveAttendanceManager.GetLeaveYearStartDate(payrollPeriod.PayrollPeriodId));
            report1.DataMember = "Report";


            //if same payroll then hide group header
            //if (report.Filter.StartDate == report.Filter.EndDate)
            //{
            //    //report1.GroupHeader.Visible = false;
            //    report1.labelTitle.Text += DateHelper.GetMonthShortName(report.Filter.StartDate.Month, IsEnglish)
            //                            + "/" + report.Filter.StartDate.Year;
            //}
            //else
            {
                report1.labelTitle.Text = "Leave Encashment for " + payrollPeriod.Name;
            }


            report.DisplayReport(report1);
            
            if(ReportHelper.IsReportExportState() )
            {
                this.report.clearCache = true;
            }
        }
    }
}
