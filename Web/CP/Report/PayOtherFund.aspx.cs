﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using BLL;
using DevExpress.XtraReports.UI;
using Web.CP.Report.Templates.HR;
using Web.ReportDataSetTableAdapters;
using Utils.Calendar;
using BLL.Base;
using DAL;
using BLL.Manager;
using System.Data;

namespace Web.CP.Report
{
    public partial class PayOtherFund : BasePage
    {
        protected  void LoadReportHandler()
        {        
        }
      
        protected void Page_Load(object sender, EventArgs e)
        {
            //report.Filter.PayrollFrom = false;
            report.Filter.PayrollTo = false;
            report.Filter.Branch = false;
            report.Filter.Department = false;
            report.Filter.Employee = false;
            report.Filter.CostCode = true;
            report.Filter.Program = true;

           // if (!IsPostBack)
                LoadReport();
        }

        protected void LoadReport()
        {
            PayrollPeriod payrollPeriod = CommonManager.GetPayrollPeriod(report.Filter.StartDate.Month,
                                                                         report.Filter.StartDate.Year);
            if( payrollPeriod ==null)
                return;

            Report_Pay_CITLikeOtherFundTableAdapter mainAdapter = new Report_Pay_CITLikeOtherFundTableAdapter();

            Report.Templates.Pay.ReportCITLikeOtherFund mainReport = new Report.Templates.Pay.ReportCITLikeOtherFund();

            BLL.BaseBiz.SetConnectionPwd(mainAdapter.Connection);
            ReportDataSet.Report_Pay_CITLikeOtherFundDataTable mainTable =
                mainAdapter.GetData(payrollPeriod.PayrollPeriodId, SessionManager.CurrentCompanyId
                ,report.Filter.CostCodeId,report.Filter.ProgramId);


            if (SessionManager.CurrentCompany.HasOtherFundLikeCIT != null && SessionManager.CurrentCompany.HasOtherFundLikeCIT.Value)
            {
                mainReport.labelTitle.Text = SessionManager.CurrentCompany.OtherFundFullName;
                mainReport.headerNo.Text = SessionManager.CurrentCompany.OtherFundAbbreviation + " No";
                mainReport.headerContribution.Text = SessionManager.CurrentCompany.OtherFundAbbreviation + " Contribution";
            }
           

            //mainReport.Month = payrollPeriod.Month;
            //mainReport.Year = payrollPeriod.Year.Value;
            //mainReport.IsIncome = true;
            //mainReport.CompanyId = SessionManager.CurrentCompanyId;
           
            mainReport.DataSource = mainTable;
            mainReport.DataMember = "Report";

            
           
            report.DisplayReport(mainReport);
        }

       
        
    }
}
