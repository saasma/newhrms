﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using BLL;
using DevExpress.XtraReports.UI;
using Web.CP.Report.Templates.HR;
using Web.ReportDataSetTableAdapters;
using Utils.Calendar;
using BLL.Base;
using DAL;
using BLL.Manager;
using System.Data;

namespace Web.CP.Report
{
    public partial class TaxCertificate : BasePage
    {

        //public string[] otherLabels  = {"ऐ.","ओ.","औ.","अं.","अ:."};

        protected  void LoadReportHandler()
        {        
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            report.Filter.PayrollFrom = false;
            report.Filter.PayrollTo = false;
            report.Filter.Branch = false;
            report.Filter.Department = false;
            report.Filter.Employee = false;
            report.Filter.RetiredEmployee = true;
            report.Filter.CostCode = false;
            report.Filter.Program = false;
            report.Filter.IncludeRetiredEmployeeInSearch = true;
            // if (!IsPostBack || CommonManager.CompanySetting.RemoveStyleWhileExportingReport)
            report.Filter.Year = true;

            report.ReportViewer.Height = new Unit(2200);

            LoadReport();

            if(!IsPostBack)
                report.RemovePDFExport();
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                report.Filter.SelectLastPayrollAndDisableDropDown();

                if (!IsPostBack && !string.IsNullOrEmpty(Request.QueryString["ID"]))
                {
                    int empId = int.Parse(Request.QueryString["ID"]);
                    string name = EmployeeManager.GetEmployeeName(empId);
                    report.Filter.SetSelectedRetEmployee(empId, name);
                    LoadReport();
                }

                
            }
        }

        public ReportDataSet.TaxCalculationDetailsDataTable GetDataTable(string data, EEmployee emp)
        {

            string[] rows = data.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

            ReportDataSet.TaxCalculationDetailsDataTable table = new ReportDataSet.TaxCalculationDetailsDataTable();

            foreach (string strRow in rows)
            {
                string[] values = strRow.Split(new char[] { ':' });

                ReportDataSet.TaxCalculationDetailsRow row = table.NewTaxCalculationDetailsRow();

                row.Header = values[0];

                if (values.Length <= 1)
                    continue;

                if (!string.IsNullOrEmpty(values[1]))
                    row["Amount"] = decimal.Parse(values[1]);

                


                //try
                //{

                int rowType = 0;
                if (values.Length >= 4 && int.TryParse(values[3], out rowType))
                    row.RowType = int.Parse(values[3]);
                //}
                //catch { }
                if (emp != null)
                {
                    row.EmployeeId = "I No : " + (emp.EHumanResources.Count > 0 ? emp.EHumanResources[0].IdCardNo : "");
                    row.Name = "Name : " + emp.Name;
                }

                table.AddTaxCalculationDetailsRow(row);
            }




            return table;

        }

        public string GetTaxStatus(string martialStatus, bool? hasCoupleTax)
        {
            if (martialStatus.ToString().ToLower() == "single")
                return "Unmarried-Single Tax Status";

            if (martialStatus.ToLower() == "married" && hasCoupleTax != null && hasCoupleTax.Value)
                return "Married-Couple Tax Status";

            return "Married-Single Tax Status";
        }

        protected void LoadReport()
        {
            int yearId = report.Filter.YearId;

            if (yearId == 0)
            {
                return;
            }

            PayrollPeriod payrollPeriod = CommonManager.GetLastPeriodForYear(yearId);

            if (payrollPeriod == null || report.Filter.RetiredEmployeeId == 0)
                return;

            if (report.Filter.RetiredEmployeeId == 0)
            {
                Utils.Helper.JavascriptHelper.DisplayClientMsg("Employee selection is required.", this);
                return;
            }

            string data = CalculationManager.GetSalaryCalcForTaxDetails(report.Filter.RetiredEmployeeId,
                SessionManager.CurrentCompanyId, payrollPeriod.PayrollPeriodId);


            Report.Templates.Pay.Detail.NRBIncomeVerificationReport mainReport = new Report.Templates.Pay.Detail.NRBIncomeVerificationReport();


            TaxReportManager rptData = new TaxReportManager();
            rptData.SetValue(data);


            mainReport.lbl1SalaryTalab.Text = rptData.Salary == 0 ? "" :  GetCurrency(rptData.Salary);
            mainReport.lbl2Allowance.Text = rptData.Allowances == 0 ? "" : GetCurrency(rptData.Allowances);
            mainReport.lbl3ProvidentFund.Text = rptData.ProvidentFund == 0 ? "" : GetCurrency(rptData.ProvidentFund);
            mainReport.lbl4VehicleFacility.Text = rptData.VehicleFacility == 0 ? "" : GetCurrency(rptData.VehicleFacility);
            mainReport.lbl5TelephoneFacility.Text = rptData.TelephoneFacility == 0 ? "" : GetCurrency(rptData.TelephoneFacility);
            mainReport.lbl6HousingAllowance.Text = rptData.HousingAllowance == 0 ? "" : GetCurrency(rptData.HousingAllowance);
            mainReport.lbl7Bonus.Text = rptData.Bonus == 0 ? "" : GetCurrency(rptData.Bonus);
            mainReport.lbl8HealthExpenses.Text = rptData.HealthExpenses == 0 ? "" : GetCurrency(rptData.HealthExpenses);
            mainReport.lbl9LeaveEncashment.Text = rptData.LeaveEncashment == 0 ? "" : GetCurrency(rptData.LeaveEncashment);
            mainReport.lbl10DashainAllowance.Text = rptData.DashainAllowance == 0 ? "" : GetCurrency(rptData.DashainAllowance);
            mainReport.lbl11LifeInsuranceIncome.Text = rptData.LifeInsuranceIncome == 0 ? "" : GetCurrency(rptData.LifeInsuranceIncome);
            mainReport.lbl12Reward.Text = rptData.Reward == 0 ? "" : GetCurrency(rptData.Reward);
            mainReport.lbl13ConcessessionalInterest.Text = rptData.ConcessessionalInterest == 0 ? "" : GetCurrency(rptData.ConcessessionalInterest);
            mainReport.lblTotalIncome.Text = rptData.TotalIncomes == 0 ? "" : GetCurrency(rptData.TotalIncomes);

            // add dynamic rows for other incomes
            int index = 0;
            foreach (var item in rptData.otherIncomeList)
            {
                if(item.Amount != 0)
                {
                    XRTableRow row = new XRTableRow();
                    XRTableCell c1 = new XRTableCell();
                    c1.Font = new Font("Tahoma", 10f);                   
                    XRTableCell c2 = new XRTableCell();
                    c2.Font = new Font("Tahoma", 10f);
                    XRTableCell c3 = new XRTableCell();
                    XRTableCell c4 = new XRTableCell();

                   
                    c1.WidthF = (float)295.21;
                    c2.WidthF = 100;
                    c3.WidthF = (float)295.21;
                    c4.WidthF = 100;

                    row.Cells.Add(c1);
                    row.Cells.Add(c2);
                    row.Cells.Add(c3);
                    row.Cells.Add(c4);


                    c1.Text = "     - " +item.Name;
                    c1.Padding = new DevExpress.XtraPrinting.PaddingInfo(20, 0, 0, 0);

                    c2.Text = GetCurrency(item.Amount);
                    c2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
                    c2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 3, 0, 0);

                    mainReport.tableOther.Rows.Insert(1, row);

                    index += 1;
                }
            }

            // Deduction starts
            mainReport.lblPFDeductionAndCITSum.Text = rptData.PFDeductionAndCITSum == 0 ? "" : GetCurrency(rptData.PFDeductionAndCITSum);
            mainReport.lblInsuranceDeduction.Text = rptData.InsuranceDeduction == 0 ? "" : GetCurrency(rptData.InsuranceDeduction);
            mainReport.lblRemoteAllowance.Text = rptData.RemoteAllowance == 0 ? "" : GetCurrency(rptData.RemoteAllowance);
            rptData.TotalDeduction = rptData.PFDeductionAndCITSum + rptData.InsuranceDeduction
                 + rptData.RemoteAllowance;
            mainReport.lblTotalDeduction.Text = rptData.TotalDeduction == 0 ? "" : GetCurrency(rptData.TotalDeduction);
            mainReport.lblSingleMarriedDeduction.Text = rptData.SingleMarriedDeductionOROnePercentAmount == 0 ? "" : GetCurrency(rptData.SingleMarriedDeductionOROnePercentAmount);
            rptData.TotalTaxableIncome = rptData.TotalIncomes - rptData.TotalDeduction - rptData.SingleMarriedDeductionOROnePercentAmount;
            mainReport.lblTotalTaxableIncome.Text = GetCurrency(rptData.TotalTaxableIncome);

            // 1%
            mainReport.lblOnePercent.Text = GetCurrency(rptData.OnePercent);
            mainReport.lblFifteenPercent.Text = rptData.FifteenPercent == 0 ? "" : GetCurrency(rptData.FifteenPercent);
            mainReport.lblTwentyFivePercent.Text = rptData.TwentyFivePercent == 0 ? "" : GetCurrency(rptData.TwentyFivePercent);
            mainReport.lblThirtyFivePercent.Text = rptData.ThirtyFivePercent == 0 ? "" : GetCurrency(rptData.ThirtyFivePercent);
            rptData.TotalTax = rptData.OnePercent + rptData.FifteenPercent + rptData.TwentyFivePercent + rptData.ThirtyFivePercent;
            mainReport.lblTotalTax.Text = GetCurrency(rptData.TotalTax);

            mainReport.lblMedicalTax.Text = rptData.MedicalTax == 0 ? "" : GetCurrency(rptData.MedicalTax);
            mainReport.lblFemaleRebate.Text = rptData.FemaleRebate == 0 ? "" : GetCurrency(rptData.FemaleRebate);

            rptData.TotalFinalTax = rptData.TotalTax - rptData.MedicalTax - rptData.FemaleRebate;
            mainReport.lblTotalFinalTax.Text = GetCurrency(rptData.TotalFinalTax);


            mainReport.lblEmpNepName.Text = EmployeeManager.GetEmployeeById(report.Filter.RetiredEmployeeId
                ).NameNepali;

            string year = CommonManager.GetFiscalYearText(payrollPeriod.PayrollPeriodId);
            mainReport.lblTitle.Text =
                "मेरो आर्थिक वर्ष " + year + " को पारिश्रमिक आय तपसिल बमोजिम को भएको हुदा उक्त आर्थिक वर्ष को कर दाखिला प्रमाण पत्र उपलब्ध गराई पाउँ भनि अनुरोध गर्दछु ।";

            mainReport.Name = "Tax Certificate of " + EmployeeManager.GetEmployeeName(report.Filter.RetiredEmployeeId)
                + " for " + year + " using " + payrollPeriod.Name;

            report.DisplayReport(mainReport);

            if (ReportHelper.IsReportExportState())
            {
                this.report.clearCache = true;
            }
        }
    }
}
