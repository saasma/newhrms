﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using BLL;
using DevExpress.XtraReports.UI;
using Web.CP.Report.Templates.HR;
using Web.ReportDataSetTableAdapters;
using Utils.Calendar;
using BLL.Base;
using DAL;
using BLL.Manager;
using System.Data;
using DevExpress.Web;
using System.Xml.Serialization;
using System.IO;

namespace Web.CP.Report
{
  
    public partial class ReportViewer : BasePage
    {

     

       

        
        protected  void LoadReportHandler()
        {}
      
        protected void Page_Init(object sender, EventArgs e)
        {


            if (!IsPostBack && !IsCallback)
            {
                string queryid = "";
                if (!string.IsNullOrEmpty(Request.QueryString["id"]))
                {
                    hidden1["value"] = Request.QueryString["id"];

                    queryid = Request.QueryString["id"];
                }
                // ASPxQueryBuilder1.OpenConnection("PayrollMgmtConnectionString");
                //ASPxQueryBuilder1.OpenConnection(

                List<QueryBuilderList> listData = CommonManager.GetAllQueries();

                int sn = 0;
                foreach (var item in listData)
                {
                    item.SN = ++sn;
                }

                //Reports rpt = QueryBuilderHelper.GetReports();

                list.DataSource = listData;
                list.DataBind();

                int index = 0;
                foreach (var item in listData)
                {
                    if (item.QueryID.ToString().ToLower().Equals(queryid.ToLower().Trim()))
                        list.SelectedIndex = index;

                    index += 1;
                }
            }

            LoadReport1();

        }

        protected void LoadReport()
        {
          
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("ReportDesigner.aspx", true);
            //if (Session["myquery"] == null)
            //    return;
            //SqlDataSource1.SelectCommand = Session["myquery"].ToString(); ;
            //ASPxGridView1.DataBind();
            //ASPxGridViewExporter1.WriteXlsxToResponse("data.xlsx");
        }

        protected void ASPxQueryBuilder1_SaveQuery(object sender, DevExpress.XtraReports.Web.SaveQueryEventArgs e)
        {
            //TableQuery query = e.ResultQuery;
            Session["myquery"] = e.SelectStatement;
        }

        protected void grid_DataBound(object sender, EventArgs e)
        {
            SetColumnsWidth(sender as ASPxGridView);
        }
        private void SetColumnsWidth(ASPxGridView grid)
        {
            if (grid.Columns.Count == 0)
                return;
            var demoAreaWidth = 918;
            var columnWidth = Math.Max(115, demoAreaWidth / grid.Columns.Count);
            for (var i = 1; i < grid.Columns.Count; i++)
            {
                grid.Columns[i].MinWidth = columnWidth;
            }
            grid.Columns[0].MinWidth = demoAreaWidth - (grid.Columns.Count - 1) * columnWidth;
        }

        protected void ASPxQueryBuilder1_Init(object sender, EventArgs e)
        {

        }

        protected void ASPxQueryBuilder1_Load(object sender, EventArgs e)
        {

        }

        protected void ASPxQueryBuilder1_DataBinding(object sender, EventArgs e)
        {

        }

        protected void ASPxQueryBuilder1_PreRender(object sender, EventArgs e)
        {

        }

        protected void ASPxQueryBuilder1_CustomJSProperties(object sender, DevExpress.Web.CustomJSPropertiesEventArgs e)
        {
            //e.Properties.
        }

        protected void grid_SelectionChanged(object sender, EventArgs e)
        {
            if (list.SelectedIndex == -1)
                return;

            hidden1["value"]  = list.DataKeys[list.SelectedIndex][0].ToString();

            LoadReport1();
        }

        private void LoadReport1()
        {
            if (hidden1.Contains("value") == false)
                return;

            string name = hidden1["value"].ToString();

            QueryBuilderList query = CommonManager.GetQueryBuilder(new Guid(name));

           // ReportItem report = QueryBuilderHelper.GetReport(name);

            if (query != null)
            {

                NorthwindDataSource.SelectCommand = query.Sql;
               
                grid.DataBind();
            }
            else
            {

            }
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            
            if (hidden1.Contains("value")==false)
                return;
            string name = hidden1["value"].ToString();

            QueryBuilderList query = CommonManager.GetQueryBuilder(new Guid(name));

            ASPxGridViewExporter1.WriteXlsxToResponse(query.Name + ".xlsx");
        }
    }
}
