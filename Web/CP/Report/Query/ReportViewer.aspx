<%@ Page Title="Report Designer" Language="C#" MaintainScrollPositionOnPostback="true"
    MasterPageFile="~/Master/HROld.Master" AutoEventWireup="true" CodeBehind="ReportViewer.aspx.cs"
    Inherits="Web.CP.Report.ReportViewer" %>

<%@ Register Assembly="DevExpress.Web.v16.1, Version=16.1.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v16.1, Version=16.1.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.XtraReports.v16.1.Web, Version=16.1.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraReports.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Xpo.v16.1, Version=16.1.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Xpo" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        function callback(s, e) {
            alert('The Click event h   ASPxWebControl.GlobalEmbedRequiredClientLibraries = true;as been invoked');
        }
        function queryBuilder_Init() {
            $(".dxrd-toolbar-item .dxqb-image-save").attr('title', "Save and Close");
        }
        var skipLoadingCheck = true;
    </script>
    <style type="text/css">
        .tbl td,th{padding:5px;}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Report Viewer
                </h4>
                <div style='height: 100px; display: none'>
                    <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1"  GridViewID="grid" runat="server">
                    </dx:ASPxGridViewExporter>
                </div>
            </div>
        </div>
    
        <div class="contentpanel" style='padding-left:0px;'>
            <table>
                <tr>
                    <td valign="top">
                       
                        <asp:GridView style='padding:2px;'  CssClass="tbl" ID="list" DataKeyNames="QueryID" 
                            OnSelectedIndexChanged="grid_SelectionChanged" runat="server" 
                            AutoGenerateColumns="False" CellPadding="10" ForeColor="#333333" 
                            GridLines="None">
                            <AlternatingRowStyle BackColor="White" />
                            <Columns>
                                <asp:TemplateField HeaderText="SN">
                                    <ItemTemplate>
                                        <asp:HyperLink Width="20" runat="server" Text='<%# Eval("SN") %>' ID="link1" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Report">
                                    <ItemTemplate>
                                        <asp:HyperLink Width="150" runat="server" CommandName="Select" NavigateUrl='<%# "ReportViewer.aspx?id=" + Eval("QueryID") %>' Text='<%# Eval("Name") %>' ID="link1" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="">
                                    <ItemTemplate>
                                        <asp:HyperLink runat="server" CommandName="Select" NavigateUrl='<%# "ReportDesigner.aspx?id=" + Eval("QueryID") %>' Text='Edit' ID="link1" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <EditRowStyle BackColor="#2461BF" />
                            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                            <RowStyle BackColor="#EFF3FB" />
                            <SelectedRowStyle BackColor="#FF8800" Font-Bold="True" ForeColor="#FF8800" />
                        </asp:GridView>
                       
                    </td>
                    <td valign="top" style='padding-left:20px;'>
                        <dx:ASPxHiddenField  runat="server" ID="hidden1" />
                        
                        

                         <asp:Button ID="btnSave" CssClass="btn btn-primary btn-sm"
                        runat="server" Text="New Report"  OnClick="Button1_Click" />
                     <asp:Button ID="Button1" CssClass="btn btn-primary btn-sm"
                        runat="server" Text="Export"  OnClick="btnExport_Click" />

                        <dx:ASPxGridView  style='margin-top:20px;'   ID="grid" Settings-HorizontalScrollBarMode="Auto" SettingsBehavior-AllowSort="true"
                            Styles-Cell-CssClass="textAlignLeft" ClientInstanceName="grid"  
                            Width="900px" runat="server"
                            OnDataBound="grid_DataBound" DataSourceID="NorthwindDataSource" 
                             onselectionchanged="grid_SelectionChanged" Theme="Metropolis">
<Settings HorizontalScrollBarMode="Auto" ShowGroupPanel="True" ShowFilterRow="true"></Settings>

                            <SettingsBehavior ColumnResizeMode="Control" AllowEllipsisInText="true" />

<SettingsCommandButton>
<ShowAdaptiveDetailButton ButtonType="Image"></ShowAdaptiveDetailButton>

<HideAdaptiveDetailButton ButtonType="Image"></HideAdaptiveDetailButton>
</SettingsCommandButton>

<Styles>
<Cell CssClass="textAlignLeft"></Cell>
</Styles>
                        </dx:ASPxGridView>
                        <asp:SqlDataSource ID="NorthwindDataSource" ConnectionString="<%$ ConnectionStrings:PayrollMgmtConnectionString %>"
                            runat="server" />
                    </td>
                </tr>
            </table>
        </div>
        </div>
</asp:Content>
