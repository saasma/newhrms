<%@ Page Title="Report Designer" Language="C#" MaintainScrollPositionOnPostback="true"
    MasterPageFile="~/Master/HROld.Master" AutoEventWireup="true" CodeBehind="ReportDesigner.aspx.cs"
    Inherits="Web.CP.Report.ReportDesigner" %>

<%@ Register Assembly="DevExpress.Web.v16.1, Version=16.1.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v16.1, Version=16.1.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.XtraReports.v16.1.Web, Version=16.1.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraReports.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Xpo.v16.1, Version=16.1.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Xpo" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        function callback(s, e) {
            alert('The Click event h   ASPxWebControl.GlobalEmbedRequiredClientLibraries = true;as been invoked');
        }
        function queryBuilder_Init() {
            $(".dxrd-toolbar-item .dxqb-image-save").attr('title', "Save and Close");
        }
    </script>
    <style type="text/css">
        .dx-toast-error
        {
            transform: translate(189px, 60px) !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Report Designer
                </h4>
                <div style='height: 100px; display: none'>
                    <%--<dx:ASPxGridView Visible="false" ID="ASPxGridView1" runat="server" AutoGenerateColumns="true"
                        DataSourceID="SqlDataSource1" KeyFieldName="SettingID">
                        <SettingsCommandButton>
                            <ShowAdaptiveDetailButton ButtonType="Image">
                            </ShowAdaptiveDetailButton>
                            <HideAdaptiveDetailButton ButtonType="Image">
                            </HideAdaptiveDetailButton>
                        </SettingsCommandButton>
                        <Columns>
                        </Columns>
                    </dx:ASPxGridView>
                    <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" GridViewID="ASPxGridView1" runat="server">
                    </dx:ASPxGridViewExporter>
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:PayrollMgmtConnectionString %>"
                        SelectCommand=""></asp:SqlDataSource>--%>
                </div>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <asp:LinkButton ID="ASPxButton11" Style='position: absolute; left: 300px; top: 52px;
            display: none; z-index: 2;' OnClick="Button1_Click" runat="server" Text="Export Data">
        </asp:LinkButton>
        <dx:ASPxQueryBuilder ID="ASPxQueryBuilder1" ClientInstanceName="queryBuilder" Style='margin-top: 0px;'
            runat="server" OnSaveQuery="ASPxQueryBuilder1_SaveQuery" ClientSideEvents-Init="queryBuilder_Init"
            EnableViewState="false">
        </dx:ASPxQueryBuilder>
    </div>
</asp:Content>
