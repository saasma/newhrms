﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using BLL;
using DevExpress.XtraReports.UI;
using Web.CP.Report.Templates.HR;
using Web.ReportDataSetTableAdapters;
using Utils.Calendar;
using BLL.Base;
using DAL;
using BLL.Manager;
using System.Data;
using DevExpress.Web;
using DevExpress.DataAccess.Sql;
using DevExpress.DataAccess.Native.Sql;
using System.Xml.Linq;
using System.IO;
using Utils;

namespace Web.CP.Report
{
   
    public partial class ReportDesigner : BasePage
    {

        string queryID = "";

        protected void Page_Init(object sender, EventArgs e)
        {
            ASPxWebControl.GlobalEmbedRequiredClientLibraries = true;
            
           // QueryBuilderHelper.HideTooltip(Session);
        }
        protected  void LoadReportHandler()
        {}

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack && !IsCallback)
            {


                //ASPxQueryBuilder1.OpenConnection(

                if (!string.IsNullOrEmpty(Request.QueryString["name"]))
                {
                    //hidden1["value"] = Request.QueryString["name"];
                    SelectQuery q = new SelectQuery(Request.QueryString["name"]);



                    ASPxQueryBuilder1.OpenConnection("PayrollMgmtConnectionString", q);
                }
                else if (!string.IsNullOrEmpty(Request.QueryString["id"]))
                {
                    queryID = Request.QueryString["id"];

                    QueryBuilderList queryDB = CommonManager.GetQueryBuilder(new Guid(queryID));

                    //https://www.devexpress.com/Support/Center/Question/Details/T447797
                    string serializedQuery = queryDB.SerializedSql;
                    using (DevExpress.DataAccess.Sql.SqlDataSource ds = new DevExpress.DataAccess.Sql.SqlDataSource(Config.ConnectionString))
                    {
                        ds.LoadFromXml(XElement.Parse(serializedQuery));
                        SelectQuery query = ds.Queries[0] as SelectQuery;
                        ASPxQueryBuilder1.OpenConnection("PayrollMgmtConnectionString", query);

                    }
                    //ASPxQueryBuilder1.OpenConnection("PayrollMgmtConnectionString",query1.Q
                }
                else
                    ASPxQueryBuilder1.OpenConnection("PayrollMgmtConnectionString");

            }
        }

        protected void LoadReport()
        {
          
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (Session["myquery"] == null)
                return;
            //SqlDataSource1.SelectCommand = Session["myquery"].ToString(); ;
            //ASPxGridView1.DataBind();
            //ASPxGridViewExporter1.WriteXlsxToResponse("data.xlsx");
        }

        protected void ASPxQueryBuilder1_SaveQuery(object sender, DevExpress.XtraReports.Web.SaveQueryEventArgs e)
        {
            //TableQuery query = e.ResultQuery;
            //Session["myquery"] = e.SelectStatement;
          
            //e.ResultQuery.ToString

            if (!string.IsNullOrEmpty(Request.QueryString["id"]))
                queryID = Request.QueryString["id"];

            QueryBuilderHelper.SaveQuery(e.SelectStatement, e.ResultQuery, Session,queryID);
            ASPxWebControl.RedirectOnCallback("ReportViewer.aspx?name=" + e.ResultQuery.ToString());
        }

        protected void ASPxQueryBuilder1_Init(object sender, EventArgs e)
        {

        }

        protected void ASPxQueryBuilder1_Load(object sender, EventArgs e)
        {

        }

        protected void ASPxQueryBuilder1_DataBinding(object sender, EventArgs e)
        {

        }

        protected void ASPxQueryBuilder1_PreRender(object sender, EventArgs e)
        {

        }

        protected void ASPxQueryBuilder1_CustomJSProperties(object sender, DevExpress.Web.CustomJSPropertiesEventArgs e)
        {
            //e.Properties.
        }
    }
}
