﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//using Web.CP.Report.Templates;

using BLL;
using DevExpress.XtraReports.UI;
using Web.CP.Report.Templates.HR;
using Web.ReportDataSetTableAdapters;
using Utils.Calendar;
using BLL.Base;
using BLL.Manager;
using DAL;

namespace Web.CP.Report
{
    public partial class HRNewJoinList : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            report.Filter.Employee = false;
            report.Filter.Branch = false;
            report.Filter.Department = false;
            report.Filter.SubDepartment = false;
            report.Filter.PayrollFrom = false;
            report.Filter.PayrollTo = false;
            report.Filter.FromCalendar = true;
            report.Filter.ToCalendar = true;
            report.Filter.RetiredOnly = true;

       //     if (!IsPostBack || CommonManager.CompanySetting.RemoveStyleWhileExportingReport)
                LoadReport();
        }

        protected void LoadReport()
        {
            Report_NewJoinTableAdapter
                adap = new Report_NewJoinTableAdapter();




            ReportNewJoin report1 = new ReportNewJoin();

            BLL.BaseBiz.SetConnectionPwd(adap.Connection);
            ReportDataSet.Report_NewJoinDataTable table = adap.GetData(
                report.Filter.DateFrom.EnglishDate, report.Filter.DateTo.EnglishDate,report.Filter.RetiredOnlyValue);
            foreach (ReportDataSet.Report_NewJoinRow row in table.Rows)
            {
                int status = row.JoinStatus ;
                row.JoinStatusName = JobStatus.GetValueForDisplay(status);
            }
            report1.DataSource = table;
            report1.DataMember = "EmployeeListReport";
            
            report.DisplayReport(report1);

            if (ReportHelper.IsReportExportState())
            {
                this.report.clearCache = true;
            }
        }
       
      
    }
}
