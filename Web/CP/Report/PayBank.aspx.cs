﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using BLL;
using DevExpress.XtraReports.UI;
using Web.CP.Report.Templates.HR;
using Web.ReportDataSetTableAdapters;
using Utils.Calendar;
using BLL.Base;
using DAL;
using BLL.Manager;
using System.Data;
using System.IO;

namespace Web.CP.Report
{
    public partial class PayBank : BasePage
    {
        protected  void LoadReportHandler()
        {        
        }
      
        protected void Page_Load(object sender, EventArgs e)
        {
            //report.Filter.PayrollFrom = false;
            report.Filter.PayrollTo = false;
          //  report.Filter.Branch = false;
            report.Filter.Department = false;
            report.Filter.Employee = false;
            report.Filter.CostCode = true;
            report.Filter.Program = true;
            report.Filter.RetiredOnly = true;
            report.Filter.Unit = true;
            report.Filter.Department = false;
            report.Filter.MultiSelectionDropDown = true;
            report.Filter.Bank = true;
            //if (!IsPostBack || CommonManager.CompanySetting.RemoveStyleWhileExportingReport)
                LoadReport();

        }

        protected void companySepcificExport()
        {
            //if (CommonManager.CompanySetting.WhichCompany == WhichCompany.GMC)
            //{
            //    PayrollPeriod payrollPeriod = CommonManager.GetPayrollPeriod(report.Filter.StartDate.Month,
            //                                                             report.Filter.StartDate.Year);
            //    if (payrollPeriod == null)
            //        return;

            //    Report_Pay_GMCBankTableAdapter mainAdapter = new Report_Pay_GMCBankTableAdapter();

            //    Report.Templates.Pay.ReportPayGMCBank mainReport = new Report.Templates.Pay.ReportPayGMCBank();

            //    BLL.BaseBiz.SetConnectionPwd(mainAdapter.Connection);
            //    ReportDataSet.Report_Pay_GMCBankDataTable mainTable =
            //        mainAdapter.GetData(payrollPeriod.Month, payrollPeriod.Year, SessionManager.CurrentCompanyId,
            //        report.Filter.BranchId, report.Filter.MultiSelectionDropDownValues, report.Filter.CostCodeId
            //        , report.Filter.ProgramId);


            //    decimal total = 0;
            //    foreach (ReportDataSet.Report_Pay_GMCBankRow item in mainTable.Rows)
            //    {
            //        total += (item.Amount);
            //    }

            //    // add GMC header 
            //    ReportDataSet.Report_Pay_GMCBankRow topRow = mainTable.NewReport_Pay_GMCBankRow();
            //    topRow.BB = "003";
            //    topRow.PersonalBankAccountNo = "003000021703";
            //    topRow.Name = "GANDAKI MEDICAL COLLEGE";
            //    topRow.Amount = total * -1;
            //    topRow.TranCode = "055";
            //    mainTable.Rows.InsertAt(topRow, 0);

            //    mainReport.labelTitle.Text += payrollPeriod.Name;

            //    mainReport.DataSource = mainTable;
            //    mainReport.DataMember = "Report";

            //    using (MemoryStream stream = new MemoryStream())
            //    {

            //        mainReport.ExportToXls(stream);

            //        HttpContext.Current.Response.Clear();

            //        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
            //        HttpContext.Current.Response.AppendHeader("Content-Type", "application/vnd.ms-excel");

            //        string ext = ".xls";

            //        HttpContext.Current.Response.AddHeader("Content-Length", stream.ToArray().Length.ToString());

            //        HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=Bank" + "Report" + ext);

            //        HttpContext.Current.Response.OutputStream.Write(stream.ToArray(), 0, stream.ToArray().Length);
            //        HttpContext.Current.Response.End();
            //    }

            //}
            //else if (CommonManager.CompanySetting.IsD2)
            //{
            //    PayrollPeriod payrollPeriod = CommonManager.GetPayrollPeriod(report.Filter.StartDate.Month,
            //                                                             report.Filter.StartDate.Year);
            //    if (payrollPeriod == null)
            //        return;

            //    Report_Pay_GMCBankTableAdapter mainAdapter = new Report_Pay_GMCBankTableAdapter();

            //    Report.Templates.Pay.ReportPayD2Bank mainReport = new Report.Templates.Pay.ReportPayD2Bank();

            //    BLL.BaseBiz.SetConnectionPwd(mainAdapter.Connection);
            //    ReportDataSet.Report_Pay_GMCBankDataTable mainTable =
            //        mainAdapter.GetData(payrollPeriod.Month, payrollPeriod.Year, SessionManager.CurrentCompanyId,
            //        report.Filter.BranchId, report.Filter.MultiSelectionDropDownValues, report.Filter.CostCodeId
            //        , report.Filter.ProgramId);

            //    //mainReport.labelBankNumber.Text = "00101010035138";

               

            //    mainReport.labelTitle.Text += payrollPeriod.Name.Replace("/"," ");
            //    mainReport.labelSubTitle.Text += payrollPeriod.Name.Replace("/", " ");

            //    mainReport.DataSource = mainTable;
            //    mainReport.DataMember = "Report";

            //    using (MemoryStream stream = new MemoryStream())
            //    {

            //        mainReport.ExportToXls(stream);

            //        HttpContext.Current.Response.Clear();

            //        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
            //        HttpContext.Current.Response.AppendHeader("Content-Type", "application/vnd.ms-excel");

            //        string ext = ".xls";

            //        HttpContext.Current.Response.AddHeader("Content-Length", stream.ToArray().Length.ToString());

            //        HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=Bank" + "Report" + ext);

            //        HttpContext.Current.Response.OutputStream.Write(stream.ToArray(), 0, stream.ToArray().Length);
            //        HttpContext.Current.Response.End();
            //    }

            //}
        }

        protected void LoadReport()
        {
            PayrollPeriod payrollPeriod = CommonManager.GetPayrollPeriod(report.Filter.StartDate.Month,
                                                                         report.Filter.StartDate.Year);
            if( payrollPeriod ==null)
                return;

            
              

            Report_Pay_BankTableAdapter mainAdapter = new Report_Pay_BankTableAdapter();

            Report.Templates.Pay.ReportPayBank mainReport = new Report.Templates.Pay.ReportPayBank();

            BLL.BaseBiz.SetConnectionPwd(mainAdapter.Connection);
            ReportDataSet.Report_Pay_BankDataTable mainTable =
                mainAdapter.GetData(payrollPeriod.Month, payrollPeriod.Year, SessionManager.CurrentCompanyId,
                report.Filter.BranchId,report.Filter.DepartmentId, report.Filter.MultiSelectionDropDownValues, report.Filter.CostCodeId
                , report.Filter.ProgramId,report.Filter.BankID,report.Filter.RetiredOnlyValue, report.Filter.UnitId);

            if (report.Filter.BankID != -1)
            {
                Bank bank = CommonManager.GetBank(report.Filter.BankID);
                if (bank != null)
                {
                    mainReport.cellBankName.Text = bank.Name;
                    mainReport.cellBankNo.Text = bank.ACNumber;
                }
            }

            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.PSI)
            {
                mainReport.cellBankName.Text = "";
                mainReport.cellBankNo.Text = CommonManager.GetFirstBank().ACNumber;
            }

            mainReport.labelBranchName.Text = report.Filter.BranchName;

            mainReport.labelTitle.Text += payrollPeriod.Name;           
           
            mainReport.DataSource = mainTable;
            mainReport.DataMember = "Report";
           
            report.DisplayReport(mainReport);

            if(ReportHelper.IsReportExportState())
            {
                this.report.clearCache = true;
            }
        }
    }
}
