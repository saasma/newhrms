<%@ Page Title="Skill Set Report" Language="C#" MaintainScrollPositionOnPostback="true"
    MasterPageFile="~/Master/HROld.Master" AutoEventWireup="true" CodeBehind="SkillsetReport.aspx.cs"
    Inherits="Web.CP.Report.SkillsetReport" %>

<%@ Register Assembly="DevExpress.Web.v16.1, Version=16.1.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.XtraReports.v16.1.Web, Version=16.1.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraReports.Web" TagPrefix="dxxr" %>
<%@ Register Src="~/Controls/Report/ReportFilterBranchDep.ascx" TagName="ReportFilterBranchDep"
    TagPrefix="uc1" %>
<%@ Register Assembly="DevExpress.XtraReports.v16.1.Web, Version=16.1.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraReports.Web" TagPrefix="dxxr" %>
<%@ Register Src="~/Controls/Report/ReportContainer.ascx" TagName="ReportContainer"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .details a:hover
        {
            background-color: lightgray;
        }
    </style>
    <script type="text/javascript">

        var textSeparator = ";";
        function OnListBoxSelectionChanged(listBox, args) {
            if (args.index == 0)
                args.isSelected ? listBox.SelectAll() : listBox.UnselectAll();
            UpdateSelectAllItemState();
            UpdateText();
        }
        function UpdateSelectAllItemState() {
            IsAllSelected() ? checkListBox.SelectIndices([0]) : checkListBox.UnselectIndices([0]);
        }
        function IsAllSelected() {
            var selectedDataItemCount = checkListBox.GetItemCount() - (checkListBox.GetItem(0).selected ? 0 : 1);
            return checkListBox.GetSelectedItems().length == selectedDataItemCount;
        }
        function UpdateText() {
            var selectedItems = checkListBox.GetSelectedItems();
            checkComboBox.SetText(GetSelectedItemsText(selectedItems));
        }

        function SynchronizeListBoxValues(dropDown, args) {
            checkListBox.UnselectAll();
            var texts = dropDown.GetText().split(textSeparator);
            var values = GetValuesByTexts(texts);
            checkListBox.SelectValues(values);
            UpdateSelectAllItemState();
            UpdateText(); // for remove non-existing texts
        }



        function GetSelectedItemsText(items) {
            var texts = [];
            for (var i = 0; i < items.length; i++)
                if (items[i].index != 0)
                    texts.push(items[i].text);
            return texts.join(textSeparator);
        }
        function GetValuesByTexts(texts) {
            var actualValues = [];
            var item;
            for (var i = 0; i < texts.length; i++) {
                item = checkListBox.FindItemByText(texts[i]);
                if (item != null)
                    actualValues.push(item.value);
            }
            return actualValues;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Skill Set Report
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="attribute" style="padding:10px">
            <%--  <asp:UpdatePanel ID="upd" runat="server" UpdateMode="Conditional">
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnLoad" />
                </Triggers>
                <ContentTemplate>--%>
            <asp:HiddenField ID="hiddenEmployeeID" runat="server" />
            <table cellpadding="3" cellspacing="0">
                <tr>
                    <td>
                        <strong>Skill Set</strong>
                    </td>
                    <td class="filterHeader" runat="server" id="rowEmp1">
                    </td>
                    <td rowspan="2" valign="bottom">
                        <asp:Button ID="btnLoad" Style='float: left' CssClass="load" runat="server" Text="Load"
                            OnClick="btnLoad_Click" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <dx:ASPxDropDownEdit ClientInstanceName="checkComboBox" ID="ddlSkillSetList" Width="300px"
                            runat="server" EnableAnimation="False">
                            <DropDownWindowStyle BackColor="#EDEDED" />
                            <DropDownWindowTemplate>
                                <dx:ASPxListBox Height="300" Width="100%" TextField="Name" ValueField="SkillSetId"
                                    ID="listBox" ClientInstanceName="checkListBox" SelectionMode="CheckColumn" runat="server">
                                    <Border BorderStyle="None" />
                                    <BorderBottom BorderStyle="Solid" BorderWidth="1px" BorderColor="#DCDCDC" />
                                    <%--   <Items>
                                        <dx:ListEditItem Text="(Select all)" />
                                    
                                    </Items>--%>
                                    <ClientSideEvents SelectedIndexChanged="OnListBoxSelectionChanged" />
                                </dx:ASPxListBox>
                            </DropDownWindowTemplate>
                            <ClientSideEvents TextChanged="SynchronizeListBoxValues" DropDown="SynchronizeListBoxValues" />
                        </dx:ASPxDropDownEdit>
                    </td>
                    <td>
                        <asp:CheckBox ID="chkNotHavingSkillSet" runat="server" Text="Employees Not having Any SkillSet" />
                    </td>
                </tr>
            </table>
            <%-- </ContentTemplate>
            </asp:UpdatePanel>--%>
        </div>
        <div style="clear: both">
        </div>
        <table>
            <tr>
                <td valign="top" style="width: 900px">
                    <div class="clear" style="">
                        <dxxr:ReportToolbar CssClass="toolbarContainer" ID="ReportToolbar1" ReportViewerID="rptViewer"
                            runat="server" ShowDefaultButtons="False" Width="100%" >
                            <Images SpriteCssFilePath="~/App_Themes/Aqua/{0}/sprite.css">
                            </Images>
                            <Styles CssFilePath="~/App_Themes/Aqua/{0}/styles.css" CssPostfix="Aqua">
                                <LabelStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                <ButtonStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                </ButtonStyle>
                                <EditorStyle>
                                    <Paddings PaddingBottom="1px" PaddingLeft="3px" PaddingRight="3px" PaddingTop="2px" />
                                </EditorStyle>
                            </Styles>
                            <Items>
                                <dxxr:ReportToolbarButton ItemKind="Search" />
                                <dxxr:ReportToolbarSeparator />
                                <dxxr:ReportToolbarButton ItemKind="PrintReport" />
                                <dxxr:ReportToolbarButton ItemKind="PrintPage" />
                                <dxxr:ReportToolbarSeparator />
                                <dxxr:ReportToolbarButton Enabled="False" ItemKind="FirstPage" />
                                <dxxr:ReportToolbarButton Enabled="False" ItemKind="PreviousPage" />
                                <dxxr:ReportToolbarLabel ItemKind="PageLabel" />
                                <dxxr:ReportToolbarComboBox ItemKind="PageNumber" Width="65px">
                                </dxxr:ReportToolbarComboBox>
                                <dxxr:ReportToolbarLabel ItemKind="OfLabel" />
                                <dxxr:ReportToolbarTextBox ItemKind="PageCount" />
                                <dxxr:ReportToolbarButton ItemKind="NextPage" />
                                <dxxr:ReportToolbarButton ItemKind="LastPage" />
                                <dxxr:ReportToolbarSeparator />
                                <dxxr:ReportToolbarButton ItemKind="SaveToDisk" />
                                <dxxr:ReportToolbarButton ItemKind="SaveToWindow" />
                                <dxxr:ReportToolbarComboBox ItemKind="SaveFormat" Width="70px">
                                    <Elements>
                                        <dxxr:ListElement Value="pdf" />
                                        <dxxr:ListElement Value="xls" />
                                        <dxxr:ListElement Value="xlsx" />
                                        <dxxr:ListElement Value="rtf" />
                                        <dxxr:ListElement Value="mht" />
                                        <dxxr:ListElement Value="html" />
                                        <dxxr:ListElement Value="txt" />
                                        <dxxr:ListElement Value="csv" />
                                        <dxxr:ListElement Value="png" />
                                    </Elements>
                                </dxxr:ReportToolbarComboBox>
                            </Items>
                        </dxxr:ReportToolbar>
                        <dxxr:ReportViewer BackColor="White" OnRestoreReportDocumentFromCache="ReportViewer1_RestoreReportDocumentFromCache"
                            OnCacheReportDocument="ReportViewer1_CacheReportDocument" ClientInstanceName="ReportViewer1"
                            ID="rptViewer" runat="server" CssFilePath="~/App_Themes/Aqua/{0}/styles.css"
                            CssPostfix="Aqua" AutoSize="false" Height="1170px" Width="100%" LoadingPanelText=""
                            SpriteCssFilePath="~/App_Themes/Aqua/{0}/sprite.css">
                           
                           
                            <Border BorderColor="#C1C1C1" BorderStyle="Solid" BorderWidth="0px" />
                            <LoadingPanelStyle ForeColor="#303030">
                            </LoadingPanelStyle>
                            <Paddings Padding="10px" PaddingLeft="55px" PaddingBottom="5px" PaddingTop="35px" />
                        </dxxr:ReportViewer>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
