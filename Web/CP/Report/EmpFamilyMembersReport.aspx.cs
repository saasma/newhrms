﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using BLL;
using DevExpress.XtraReports.UI;
using Web.CP.Report.Templates.HR;
using Web.ReportDataSetTableAdapters;
using Utils.Calendar;
using BLL.Base;
using DAL;
using BLL.Manager;
using System.Data;
using DevExpress.XtraPrinting;
using BLL.BO;
using Web.CP.Report;


namespace Web.CP.Report
{
    public partial class EmpFamilyMembersReport : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.HumanResource;
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            report.Filter.PayrollFrom = false;
            report.Filter.PayrollTo = false;
            report.Filter.SubDepartment = false;
            report.Filter.FamiltyType = true;
            LoadReport();
        }

        protected void LoadReport()
        {
            Report_EmployeeFamilyListTableAdapter
                adap = new Report_EmployeeFamilyListTableAdapter();

            Report.Templates.HR.EmpFamilyMembers report1 = new Report.Templates.HR.EmpFamilyMembers();

            BLL.BaseBiz.SetConnectionPwd(adap.Connection);
            report1.DataSource = adap.GetData(report.Filter.EmployeeId, report.Filter.BranchId, report.Filter.DepartmentId, SessionManager.CurrentCompanyId,
                report.Filter.FamiltyTypeValue);
            report1.DataMember = "EmpFamilyMembers";

            report.DisplayReport(report1);

            if (ReportHelper.IsReportExportState())
            {
                this.report.clearCache = true;
            }


        }
    }
}