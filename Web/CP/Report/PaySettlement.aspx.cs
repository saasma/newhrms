﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using BLL;
using DevExpress.XtraReports.UI;
using Web.CP.Report.Templates.HR;
using Web.ReportDataSetTableAdapters;
using Utils.Calendar;
using BLL.Base;
using DAL;
using BLL.Manager;
using System.Data;

namespace Web.CP.Report
{
    public partial class PaySettlement : BasePage
    {

        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.Retirement;
            }
        }

        protected  void LoadReportHandler()
        {
        
        }
      
        protected void Page_Load(object sender, EventArgs e)
        {
            //report.Filter.PayrollFrom = false;
            report.Filter.PayrollTo = false;
            report.Filter.Branch = false;
            report.Filter.Department = false;
            report.Filter.Employee = false;

            //if (!IsPostBack)
                LoadReport();
        }

        protected void LoadReport()
        {
            PayrollPeriod payrollPeriod = CommonManager.GetPayrollPeriod(report.Filter.StartDate.Month,
                                                                         report.Filter.StartDate.Year);

            if( payrollPeriod ==null)
                return;

            Report_Pay_SettlementListTableAdapter mainAdapter =
                new Report_Pay_SettlementListTableAdapter();
            
            Report.Templates.Pay.Detail.ReportPaySettlement mainReport =
                new Report.Templates.Pay.Detail.ReportPaySettlement();
            BLL.BaseBiz.SetConnectionPwd(mainAdapter.Connection);
            ReportDataSet.Report_Pay_SettlementListDataTable mainTable =
                mainAdapter.GetData(payrollPeriod.Month, payrollPeriod.Year, SessionManager.CurrentCompanyId);

            mainReport.Month = payrollPeriod.Month;
            mainReport.Year = payrollPeriod.Year.Value;
          
            mainReport.CompanyId = SessionManager.CurrentCompanyId;
           
            mainReport.DataSource = mainTable;
            mainReport.DataMember = "Report";
           
            report.DisplayReport(mainReport);
        }
    }
}
