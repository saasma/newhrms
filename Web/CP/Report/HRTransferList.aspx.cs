﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//using Web.CP.Report.Templates;

using BLL;
using DevExpress.XtraReports.UI;
using Web.CP.Report.Templates.HR;
using Web.ReportDataSetTableAdapters;
using Utils.Calendar;
using BLL.Base;
using BLL.Manager;
using DAL;

namespace Web.CP.Report
{
    public partial class HRTransferList : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            report.Filter.Employee = false;
            report.Filter.Branch = false;
            report.Filter.Department = false;
            report.Filter.SubDepartment = false;
            report.Filter.PayrollFrom = false;
            report.Filter.PayrollTo = false;
            report.Filter.FromCalendar = true;
            report.Filter.ToCalendar = true;
       //     if (!IsPostBack || CommonManager.CompanySetting.RemoveStyleWhileExportingReport)
                LoadReport();
        }

        protected void LoadReport()
        {
            Report_GetTransferReportTableAdapter
                adap = new Report_GetTransferReportTableAdapter();

            TransferRpt report1 = new TransferRpt();
            string type = Request.QueryString["Type"];

            if (type == null)
                type = "";
            else
            {
                report1.lblTitle.Text = type + " change list";
                this.Page.Title = type + " change list";
            }

            

            BLL.BaseBiz.SetConnectionPwd(adap.Connection);
            ReportDataSet.Report_GetTransferReportDataTable table = adap.GetData(type,
                report.Filter.DateFrom.EnglishDate, report.Filter.DateTo.EnglishDate);
         
            report1.DataSource = table;
            report1.DataMember = "EmployeeListReport";
            
            report.DisplayReport(report1);

            if (ReportHelper.IsReportExportState())
            {
                this.report.clearCache = true;
            }
        }
       
      
    }
}
