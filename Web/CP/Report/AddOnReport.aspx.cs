﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using BLL;
using DevExpress.XtraReports.UI;
using Web.CP.Report.Templates.HR;
using Web.ReportDataSetTableAdapters;
using Utils.Calendar;
using BLL.Base;
using DAL;
using BLL.Manager;
using System.Data;
using DevExpress.XtraPrinting;
using Web.CP.Report.Templates.Pay;

namespace Web.CP.Report
{
    public partial class AddOnReport : BasePage
    {
        private int Name_Column_Index = 1;
        private int top = 65;

        public int addOnId = 0;
        public int payrollPeriodId = 0;

        protected void LoadReportHandler()
        { }

        public void btnLoad_Click(object sender, EventArgs e)
        {
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                List<FinancialDate> list = new CommonManager().GetAllFinancialDates(); ;

                foreach (FinancialDate item in list)
                    item.SetName(IsEnglish);

                ddlYear.DataSource = list;
                ddlYear.DataBind();

            }

            //if (!IsPostBack || CommonManager.CompanySetting.RemoveStyleWhileExportingReport)
            if (IsPostBack)
                LoadReport();
        }


        private void AddRange(XRControlStyle[] xRControlStyle)
        {
            throw new NotImplementedException();
        }

        private void Event_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (Web.CP.Report.ReportHelper.IsReportExportState())
            {
                ((XRLabel)sender).BackColor = System.Drawing.Color.Transparent;
                ((XRLabel)sender).BorderColor = System.Drawing.Color.Black;
            }

        }

        protected void ClearBorderControlEvent(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ReportHelper.TableCell_BeforePrintEvent(sender, e);
        }
        protected void ClearBorderLessControlEvent(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ReportHelper.TableCell_BeforePrintEvent(sender, e);
        }

        private void CreateUsingLabels(XtraReport report)       
        {
            DataTable dataTable = (report.DataSource as DataSet).Tables[0];
            int firstColumnExtraWidth = 60;

            Color colorHeader = Color.FromArgb(201, 214, 237);
            Color colorEven = Color.FromArgb(239, 243, 250);

            int colCount = dataTable.Columns.Count;
            int colWidth = 80;

            report.PageWidth = colWidth * colCount;
            (report as SalarySummary).labelTitle.WidthF = report.PageWidth + 100;
            int currentXLocation = 0;
            int prevWidth = 0;
            int currentWidth = 0;
            // Create header captions
            for (int i = 0; i < colCount; i++)
            {

                XRLabel label = new XRLabel();
                label.BeforePrint += new System.Drawing.Printing.PrintEventHandler(Event_BeforePrint);
                label.TextAlignment = TextAlignment.MiddleLeft;

                ReportHelper.HeaderLabelStyle(label);





                if (i == 0)
                {
                    currentWidth = colWidth / 2; //for SN
                    label.TextAlignment = TextAlignment.MiddleCenter;
                }
                else if (i == 1)
                {
                    currentWidth = 60;//colWidth / 2; //for title

                }
                else if (i == 2)
                {
                    currentWidth = colWidth + firstColumnExtraWidth; //for name

                }
                
                else
                {
                    currentWidth = colWidth;//for other amount columns

                }
                currentXLocation += (prevWidth);
                prevWidth = currentWidth;

                label.Location = new Point(currentXLocation, top);


                label.Text = dataTable.Columns[i].Caption;
                label.Padding = new PaddingInfo(4, 2, 2, 2);

                label.Font = new Font(label.Font.FontFamily, label.Font.Size, FontStyle.Bold);
                label.CanGrow = false;//dont allow to grow as design will be meshed up
                // if (i != Name_Column_Index)
                if (i == 0)
                {
                    label.Borders = DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom;
                    label.Size = new Size(currentWidth, 50);
                }
                else
                {
                    label.Borders = DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom;



                    label.Size = new Size(currentWidth, 50);
                }

                report.Bands[BandKind.PageHeader].Controls.Add(label);
            }


            currentXLocation = 0;
            prevWidth = 0;
            currentWidth = 0;
            // Create data-bound labels with different odd and even backgrounds
            for (int i = 0; i < colCount; i++)
            {
                XRLabel label = new XRLabel();

                label.Text = "-";
                label.BeforePrint += new System.Drawing.Printing.PrintEventHandler(Event_BeforePrint);


                if (i == 0)
                {
                    currentWidth = colWidth / 2;
                    label.TextAlignment = TextAlignment.MiddleCenter;
                }
                else if (i == 1)
                {
                    currentWidth = 60;
                    label.TextAlignment = TextAlignment.MiddleLeft;

                }
                else if (i == 2)
                {
                    currentWidth = colWidth + firstColumnExtraWidth; //for name
                    label.TextAlignment = TextAlignment.MiddleLeft;
                }
               
                else
                {
                    currentWidth = colWidth;
                    label.TextAlignment = TextAlignment.MiddleRight;



                    //label.EvaluateBinding += new BindingEventHandler(BindingEventHandler);
                }
                currentXLocation += (prevWidth);
                prevWidth = currentWidth;

                //if (i == Name_Column_Index)
                label.Location = new Point(currentXLocation, 0);
                //else
                //{
                //    label.Location = new Point((colWidth * i) + firstColumnExtraWidth, 0);                   
                //}
                label.Padding = new PaddingInfo(2, 2, 2, 0);
                if (i == Name_Column_Index)
                    label.DataBindings.Add("Text", null, dataTable.Columns[i].ColumnName);
                else
                    label.DataBindings.Add("Text", null, dataTable.Columns[i].ColumnName, "{0:n2}");





                label.CanGrow = false;



                //if (i != Name_Column_Index)
                //{
                // 
                if (i == 0)
                {
                    label.Size = new Size(currentWidth, 20);
                    label.Borders = DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom;
                }

                else
                {
                    label.Borders = DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom;

                    label.Size = new Size(currentWidth, 20);
                }
                //}
                //else
                //{
                //    label.Borders = DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom;
                //    label.TextAlignment = TextAlignment.MiddleLeft;
                //    label.Size = new Size(colWidth + firstColumnExtraWidth, 20);

                //}

                ReportHelper.LabelStyle(label);
                label.Font = new Font(label.Font.FontFamily, label.Font.Size, FontStyle.Regular);

                //label.BackColor = i == 0 ? colorEven : Color.White;
                //label.BorderColor = colorHeader;
                // Place the labels onto a Detail band
                report.Bands[BandKind.Detail].Controls.Add(label);
            }

            currentXLocation = 0;
            prevWidth = 0;
            currentWidth = 0;

            //create footer captions for Total
            for (int i = 0; i < colCount; i++)
            {
                XRLabel label = new XRLabel();
                //set properties for sum/total
                //label.StyleName = "ReportFooter";
                label.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(190)))), ((int)(((byte)(216)))));

                label.BeforePrint += new System.Drawing.Printing.PrintEventHandler(Event_BeforePrint);

                if (i == 0)
                {
                    currentWidth = colWidth / 2;
                }
                else if (i == 1)  
                {
                    currentWidth = 60;

                }
                else if (i == 2)
                {
                    currentWidth = colWidth + firstColumnExtraWidth; //for name

                }
                
                else
                {
                    currentWidth = colWidth;

                }
                currentXLocation += (prevWidth);
                prevWidth = currentWidth;

                label.Location = new Point(currentXLocation, 0);

                label.Padding = new PaddingInfo(2, 2, 2, 0);




                DevExpress.XtraReports.UI.XRSummary summary = new DevExpress.XtraReports.UI.XRSummary();


                //skip for first two columns
                if (i == 2)
                {
                    label.Text = "Total";
                }
                else if (i == (colCount - 1))
                { }
                else if (i > 5)
                {
                    label.DataBindings.Add("Text", null, dataTable.Columns[i].ColumnName);



                    summary.FormatString = "{0:n2}";

                    summary.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
                    label.Summary = summary;

                }


                label.CanGrow = false;
                //label.EvenStyleName = "EvenStyle";
                //label.OddStyleName = "OddStyle";
                //label.StylePriority.UseBackColor = true;
                //label.StylePriority.UseBorders = true;
                //label.StylePriority.UseBorderColor = true;
                // label.OddStyleName = "OddStyle";
                //label.EvenStyleName = "EvenStyle";
                //if (i != Name_Column_Index)
                //{
                if (i == 0)

                    label.Borders = DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom;

                else
                    label.Borders = DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom; ;

                label.TextAlignment = TextAlignment.MiddleRight;
                label.Size = new Size(currentWidth, 20);
                //}
                //else
                //{
                //    label.Text = "Total";
                //    label.Borders = DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom;
                //    label.TextAlignment = TextAlignment.MiddleLeft;
                //    label.Size = new Size(colWidth + firstColumnExtraWidth, 20);
                //}
                label.Font = new Font(label.Font.FontFamily, label.Font.Size, FontStyle.Bold);

                //label.BackColor = i == 0 ? colorEven : Color.White;
                //label.BorderColor = colorHeader;
                // Place the labels onto a Detail band
                report.Bands[BandKind.ReportFooter].Controls.Add(label);
            }
        }


        public void BindingEventHandler( object sender, BindingEventArgs e)
        {
            if (e.Value == DBNull.Value)// || e.Value.ToString() == "0")
                e.Value = "0.00";

        }

        public void ddlYear_Change(object sender, EventArgs e)
        {
            int yearid = int.Parse(ddlYear.SelectedItem.Value);
            List<BLL.Manager.CalculationManager.AddOnName> list = CalculationManager.GetAddOnList(yearid); ;

            string name = "";
            //if (list.Count > 0)
               

            int sn = 0;
            for (int i = 0; i < list.Count; i++)
            {
                list[i].SN = i + 1;
            }


            for (int i = 0; i < list.Count; i++)
            {
                if (i == 0)
                    name = list[i].Period;
                else if (name == list[i].Period)
                    list[i].Period = "";
                else
                    name = list[i].Period;

                //list[i].Period = name;
            }

            gvEmployeeIncome.DataSource = list;
            gvEmployeeIncome.DataBind();
        }

        protected void LoadReport()
        {
            if (!IsPostBack)
                return;

            //PayrollPeriod payrollPeriod = CommonManager.GetPayrollPeriod(report.Filter.StartDate.Month,
            //                                                             report.Filter.StartDate.Year);
            //if (payrollPeriod == null)
            //{
            //    return;
            //}

            if (gvEmployeeIncome.SelectedIndex != -1 && gvEmployeeIncome.DataKeys.Count > gvEmployeeIncome.SelectedIndex)
            {
                addOnId = (int) gvEmployeeIncome.DataKeys[gvEmployeeIncome.SelectedIndex][0];
                payrollPeriodId = (int)gvEmployeeIncome.DataKeys[gvEmployeeIncome.SelectedIndex][1];
            }


            if (addOnId == 0 && payrollPeriodId == 0)
            {
                return;

            }

            PayrollPeriod payrollPeriod = CommonManager.GetPayrollPeriod(payrollPeriodId);

            AddOn add = CalculationManager.GetAddOnId(addOnId);
            


            Report.Templates.Pay.SalarySummary mainReport = new Web.CP.Report.Templates.Pay.SalarySummary();
            mainReport.Name = (add == null ? "" : add.Name + " of ") + payrollPeriod.Name;
            //mainReport.labelTitle.Text = report.Filter.PaySummaryTypeText + " " + mainReport.labelTitle.Text;
            mainReport.labelTitle.Text = mainReport.Name;
            //XlsxExportOptions o;
            //mainReport.ExportToXlsx(


            List<PDeduction> loanRepaymentDeductions = PayManager.GetDeductionListByCompany(SessionManager.CurrentCompanyId)
                .Where(x => x.Calculation == DeductionCalculation.LOAN_Repayment).ToList();

            string search = txtEmpSearch.Text.Trim();
            if (txtEmpSearch.Text.Trim().Equals("Search Text"))
                search = "";

            List<DAL.Report_Pay_AddonResult> data = ReportManager.GetAddOn(addOnId, -1, -1, search, payrollPeriod.PayrollPeriodId);

            List<CalcGetHeaderListResult> headerList =
                CalculationManager.GetAddOnHeaderList(addOnId,
                                                                 payrollPeriod.PayrollPeriodId);
            headerList = CalculationValue.SortHeaders(headerList, PayManager.GetIncomeOrderValues(), PayManager.GetDeductionOrderValues());

           


            DataTable dataTable = CreateDataTable(data, headerList, loanRepaymentDeductions);
            DataSet dset = new DataSet();
            dset.Tables.Add(dataTable);
            mainReport.DataSource = dset;
            mainReport.DataMember = "Report";

                CreateUsingLabels(mainReport);
            if (data.Count > 0)
            {
               // report.DisplayReport(mainReport);

                //mainReport.DataSource = mainReport;
                //mainReport.DataMember = "Report";



                this.rptViewer.Report = mainReport;
                this.ReportToolbar1.ReportViewer = this.rptViewer;
            }
            CurrencyRate rate = CommonManager.GetCurrencyRateForSalarySavedPayroll(payrollPeriod.PayrollPeriodId);
            // Show D2 Fixed/Current Rate Amount
            if (CommonManager.CompanySetting.IsD2 && rate != null && CommonManager.IsD2MonthInDollar(payrollPeriod.Year.Value, payrollPeriod.Month))
            {
                DevExpress.XtraReports.UI.XRLabel lblCurrency = new XRLabel();
                lblCurrency.WidthF = 400;
                lblCurrency.LocationF = new PointF(0, 30);
                mainReport.Bands[BandKind.PageHeader].Controls.Add(lblCurrency);

                lblCurrency.Text = string.Format(Resources.Messages.D2FixedCurrentRateText,
                       rate.FixedRateDollar, rate.CurrentRateDollar);

                lblCurrency.LeftF = mainReport.labelTitle.LeftF;
                lblCurrency.WidthF = mainReport.labelTitle.WidthF;
            }


           

        }

        private DataTable CreateDataTable(List<DAL.Report_Pay_AddonResult> data, List<CalcGetHeaderListResult> headerList
            , List<PDeduction> loanRepaymentDeductions)
        {
            DataTable dataTable = new DataTable();
            dataTable.TableName = "Report";

            dataTable.Columns.Add("SN", typeof(string));
            dataTable.Columns.Add("EIN", typeof(string));
            dataTable.Columns.Add("Name", typeof(string));
            dataTable.Columns.Add("Branch", typeof(string));
            dataTable.Columns.Add("Level/Position", typeof(string));
            dataTable.Columns.Add("Designation", typeof(string));

            // Create table cells, fill the header cells with text, bind the cells to data
            for (int i = 0; i < headerList.Count; i++)
            {

                DataColumn column = new DataColumn(headerList[i].Type + ":" + headerList[i].SourceId, typeof(decimal));
                column.Caption = headerList[i].HeaderName;

                dataTable.Columns.Add(column);

            }

            dataTable.Columns.Add("Account No", typeof(string));
            dataTable.Columns.Add("Bank Name", typeof(string));


            //add rows
            decimal? value;
            foreach (Report_Pay_AddonResult row in data)
            {
                List<object> list = new List<object>();
                list.Add(row.SN);
                list.Add(row.EmployeeId);
                list.Add(row.Name);
                list.Add(row.Branch);
                list.Add(row.Level);
                list.Add(row.Designation);

                for (int i = 0; i < headerList.Count; i++)
                {
                    value = row.GetCellValue(headerList[i].Type.Value, headerList[i].SourceId.Value, 2, null);
                    list.Add(value);
                }

                list.Add(row.AccountNO);
                list.Add(row.BankName);

                dataTable.Rows.Add(list.ToArray());
                
            }

            return dataTable;

        }

        protected void gvEmployeeIncome_SelectedIndexChanged(object sender, EventArgs e)
        {
            addOnId = (int)gvEmployeeIncome.DataKeys[gvEmployeeIncome.SelectedIndex][0];
            payrollPeriodId = (int)gvEmployeeIncome.DataKeys[gvEmployeeIncome.SelectedIndex][1];
            LoadReport();

        }
    }
}
