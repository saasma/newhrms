﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//using Web.CP.Report.Templates;

using BLL;
using DevExpress.XtraReports.UI;
using Web.ReportDataSetTableAdapters;
using Utils.Calendar;
using BLL.Base;
using BLL.Manager;
using DevExpress.XtraReports.Web;
using System.IO;

namespace Web.CP.Report
{
    public partial class HRAttendanceDays : BasePage
    {


        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!IsPostBack || CommonManager.CompanySetting.RemoveStyleWhileExportingReport)
                LoadReport();
        }

        protected void LoadReport()
        {
            ReportDataSetTableAdapters.Report_HR_GetAttendanceDaysTableAdapter
                adap = new Report_HR_GetAttendanceDaysTableAdapter();


            Report.Templates.HR.ReportAttendnaceDays report1 = new Report.Templates.HR.ReportAttendnaceDays();


            if (CommonManager.CalculationConstant.CompanyHasHourlyLeave.Value)
                report1.tableHeaderPresentDays.Text = " " + Resources.Messages.ReportAttendancePresentDaysReplacement;

            // Set for DBDefence
            BaseBiz.SetConnectionPwd(adap.Connection);

            report1.DataSource = adap.GetData(
                report.Filter.StartDateEng, report.Filter.EndDateEng, SessionManager.CurrentCompanyId,
               report.Filter.EmployeeName, report.Filter.BranchId, report.Filter.DepartmentId, report.Filter.DepartmentName);
            report1.DataMember = "AttendanceReport";


           

            //if same payroll then hide group header
            if (report.Filter.StartDate == report.Filter.EndDate)
            {
                report1.GroupHeader.Visible = false;
                report1.XRTitle.Text += DateHelper.GetMonthShortName(report.Filter.StartDate.Month, IsEnglish)
                                        + "/" + report.Filter.StartDate.Year;
            }
            else
            {
                report1.XRTitle.Text +=
                    (DateHelper.GetMonthShortName(report.Filter.StartDate.Month, IsEnglish)
                     + "/" + report.Filter.StartDate.Year + " to " +

                     DateHelper.GetMonthShortName(report.Filter.EndDate.Month, IsEnglish)
                     + "/" + report.Filter.EndDate.Year);
            }


            report.DisplayReport(report1);

            if (ReportHelper.IsReportExportState())
            {
                this.report.clearCache = true;

            }

        }

      
    }
}
