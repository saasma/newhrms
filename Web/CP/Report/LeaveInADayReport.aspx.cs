﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//using Web.CP.Report.Templates;

using BLL;
using DevExpress.XtraReports.UI;
using Web.CP.Report.Templates.HR;
using Web.ReportDataSetTableAdapters;
using Utils.Calendar;
using BLL.Base;
using System.Data;
using BLL.Manager;

namespace Web.CP.Report
{
    public partial class LeaveInADayReport : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.LeaveAndAttendance;
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            report.Filter.DateFromControlIsEnglish = true;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            report.Filter.DateFromControlIsEnglish = true;

            report.Filter.DateFromControl = true;
            report.Filter.DateToControl = true;

            report.Filter.PayrollFrom = false;
            report.Filter.PayrollTo = false;
            report.Filter.LeaveRequestType = false;

            report.Filter.Employee = true;
            report.Filter.Branch = false;
            report.Filter.Department = false;

            //if (!IsPostBack || CommonManager.CompanySetting.RemoveStyleWhileExportingReport)
                LoadReport();
        }

        protected void LoadReport()
        {
            Report_HRLeaveTakenOnADayTableAdapter adap = new Report_HRLeaveTakenOnADayTableAdapter();

            ReportLeavesTakenInADay report1 = new ReportLeavesTakenInADay();

            //report1.DataSource = adap.GetData(report.Filter.EmployeeName, report.Filter.BranchId,report.Filter.DepartmentId,
                                              //SessionManager.CurrentCompanyId);
            //report.Filter.DateFrom.Day
            //string fromYear = Convert.ToString(report.Filter.DateFrom.Year);
            //string fromMonth = Convert.ToString(report.Filter.DateFrom.Month);
            //string fromDate = Convert.ToString(report.Filter.DateFrom.Day);

            //string toYear = Convert.ToString(report.Filter.DateTo.Year);
            //string toMonth = Convert.ToString(report.Filter.DateTo.Month);
            //string toDate = Convert.ToString(report.Filter.DateTo.Day);

            //CustomDate from = new CustomDate(report.Filter.DateFrom.Day, report.Filter.DateFrom.Month, report.Filter.DateFrom.Year, IsEnglish);
            //CustomDate to = new CustomDate(report.Filter.DateTo.Day, report.Filter.DateTo.Month, report.Filter.DateTo.Year, IsEnglish);

            //DateTime fromDateEng = Convert.ToDateTime(GetEngDate(from.ToString()));
            //DateTime toDateEng = Convert.ToDateTime(GetEngDate(to.ToString()));

            BLL.BaseBiz.SetConnectionPwd(adap.Connection);
            ReportDataSet.Report_HRLeaveTakenOnADayDataTable adapGetData = 
                adap.GetData(report.Filter.DateFrom.EnglishDate,
                report.Filter.DateTo.EnglishDate,report.Filter.EmployeeId); ;

          
          

            report1.DataSource = adapGetData;

            report1.labelTitle.Text +=report.Filter.DateFrom.EnglishDate.ToString("yyyy-MMM-dd")
                + " to " + report.Filter.DateTo.EnglishDate.ToString("yyyy-MMM-dd");

                          
            report1.DataMember = "HolidayListReport";            
            report.DisplayReport(report1);

            if (ReportHelper.IsReportExportState())
            {
                this.report.clearCache = true;

            }
        }

       
    }
}
