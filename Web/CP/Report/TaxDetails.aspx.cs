﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using BLL;
using DevExpress.XtraReports.UI;
using Web.CP.Report.Templates.HR;
using Web.ReportDataSetTableAdapters;
using Utils.Calendar;
using BLL.Base;
using DAL;
using BLL.Manager;
using System.Data;

namespace Web.CP.Report
{
    public partial class TaxDetails : BasePage
    {
        protected  void LoadReportHandler()
        {        
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //report.Filter.PayrollFrom = false;
            report.Filter.PayrollTo = false;
            report.Filter.Branch = false;
            report.Filter.Department = false;
            report.Filter.Employee = false;
            report.Filter.RetiredEmployee = true;
            report.Filter.CostCode = false;
            report.Filter.Program = false;
            report.Filter.IncludeRetiredEmployeeInSearch = true;
            // if (!IsPostBack || CommonManager.CompanySetting.RemoveStyleWhileExportingReport)

            report.ReportViewer.Height = new Unit(2200);

            LoadReport();

        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                report.Filter.SelectLastPayrollAndDisableDropDown();

                if (!IsPostBack && !string.IsNullOrEmpty(Request.QueryString["ID"]))
                {
                    int empId = int.Parse(Request.QueryString["ID"]);
                    string name = EmployeeManager.GetEmployeeName(empId);
                    report.Filter.SetSelectedRetEmployee(empId, name);
                    LoadReport();
                }

            }
        }

        public ReportDataSet.TaxCalculationDetailsDataTable GetDataTable(string data, EEmployee emp)
        {

            string[] rows = data.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

            ReportDataSet.TaxCalculationDetailsDataTable table = new ReportDataSet.TaxCalculationDetailsDataTable();

            foreach (string strRow in rows)
            {
                string[] values = strRow.Split(new char[] { ':' });

                ReportDataSet.TaxCalculationDetailsRow row = table.NewTaxCalculationDetailsRow();

                row.Header = values[0];

                if (values.Length <= 1)
                    continue;

                if (!string.IsNullOrEmpty(values[1]))

                    row["Amount"] = decimal.Parse(values[1]);

                if (values.Length >= 3)
                    row.Note = values[2];

                //try
                //{

                int rowType = 0;
                if (values.Length >= 4 && int.TryParse(values[3], out rowType))
                    row.RowType = int.Parse(values[3]);
                //}
                //catch { }
                if (emp != null)
                {
                    row.EmployeeId = "I No : " + (emp.EHumanResources.Count > 0 ? emp.EHumanResources[0].IdCardNo : "");
                    row.Name = "Name : " + emp.Name;
                }

                table.AddTaxCalculationDetailsRow(row);
            }




            return table;

        }

        public string GetTaxStatus(string martialStatus, bool? hasCoupleTax)
        {
            if (martialStatus.ToString().ToLower() == "single")
                return "Unmarried-Single Tax Status";

            if (martialStatus.ToLower() == "married" && hasCoupleTax != null && hasCoupleTax.Value)
                return "Married-Couple Tax Status";

            return "Married-Single Tax Status";
        }

        protected void LoadReport()
        {
            PayrollPeriod payrollPeriod = CommonManager.GetPayrollPeriod(report.Filter.StartDate.Month,
                                                                         report.Filter.StartDate.Year);
            if( payrollPeriod ==null || report.Filter.RetiredEmployeeId == 0)
                return;

            if (report.Filter.RetiredEmployeeId == 0)
            {
                Utils.Helper.JavascriptHelper.DisplayClientMsg("Employee selection is required.", this);
                return;
            }


            Report.Templates.Pay.Detail.ReportTaxCalculationDetails mainReport = new Report.Templates.Pay.Detail.ReportTaxCalculationDetails();
            
            mainReport.labelTitle.Text += " (" + payrollPeriod.Name + ")";
            string data = CalculationManager.GetSalaryCalcForTaxDetails(report.Filter.RetiredEmployeeId,
                SessionManager.CurrentCompanyId, payrollPeriod.PayrollPeriodId);

            // replace CIT name in tax details with other Retirement fundname
            if (!string.IsNullOrEmpty(SessionManager.CurrentCompany.OtherFundAbbreviation))
            {
                string name = SessionManager.CurrentCompany.OtherFundAbbreviation;
                data = data.Replace("CIT Deduction", name + " Deduction");
                data = data.Replace("CIT Settlement", name + " Settlement");
                data = data.Replace("Provisional CIT", "Provisional " + name);
                data = data.Replace("Sum of CIT And PF", "Sum of " + name + " And PF");
            }

            EEmployee employee = EmployeeManager.GetEmployeeById(report.Filter.RetiredEmployeeId);
            mainReport.Name = "Tax Details of " + employee.Name + " for " + payrollPeriod.Name;

            EmployeeRegularIncomeHistory details = BLL.BaseBiz.PayrollDataContext
                .EmployeeRegularIncomeHistories.FirstOrDefault(x => x.EmployeeId == employee.EmployeeId
                    && x.PayrollPeriodId == payrollPeriod.PayrollPeriodId);

            string sex="";

            if (details != null)
            {
                if (details.Gender == 0)
                    sex = "Female";
                else if (details.Gender == 1)
                    sex = "Male";
                else
                    sex = "Third Gender";


                // Append additional information
                data = string.Format(";Sex::{0}:2", sex) +
                        string.Format(";Tax Status (M/S)::{0}:2",
                        GetTaxStatus(employee.MaritalStatus, employee.HasCoupleTaxStatus)) + data;
            }
            else
            {
                if (employee.Gender == 0)
                    sex = "Female";
                else if (employee.Gender == 1)
                    sex = "Male";
                else
                    sex = "Third Gender";


                // Append additional information
                data = string.Format(";Sex::{0}:2", sex) +
                        string.Format(";Tax Status (M/S)::{0}:2",
                        GetTaxStatus(employee.MaritalStatus, employee.HasCoupleTaxStatus)) + data;

            }

            ReportDataSet.TaxCalculationDetailsDataTable mainTable = GetDataTable(data, employee);


            //mainReport.Month = payrollPeriod.Month;
            //mainReport.Year = payrollPeriod.Year.Value;
            //mainReport.IsIncome = true;
            //mainReport.CompanyId = SessionManager.CurrentCompanyId;
           
            mainReport.DataSource = mainTable;
            mainReport.DataMember = "Report";
           
            report.DisplayReport(mainReport);

            if(ReportHelper.IsReportExportState())
            {
                this.report.clearCache = true;
            }
        }
    }
}
