﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using BLL;
using DevExpress.XtraReports.UI;
using Web.CP.Report.Templates.HR;
using Web.ReportDataSetTableAdapters;
using Utils.Calendar;
using BLL.Base;
using DAL;
using BLL.Manager;
using System.Data;
using Utils;
using System.IO;

namespace Web.CP.Report
{
    
    public partial class RetiredPaySlipDetail : BasePage
    {

        protected  void LoadReportHandler()
        {
        
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //report.Filter.PayrollFrom = false;
            report.Filter.PayrollFrom = false;
            report.Filter.PayrollPeriod = false;
            report.Filter.PayrollTo = false;
            //report.Filter.Branch = false;
            report.Filter.Employee = false;
            report.Filter.RetiredEmployee = true;
            report.Filter.Branch = false;
            report.Filter.Department = false;
            report.Filter.Department = false;
            report.Filter.MultiSelectionDropDown = false;
            report.Filter.PaySummaryType = false;
            //report.Filter.Employee = false;

            if (IsPostBack)// || CommonManager.CompanySetting.RemoveStyleWhileExportingReport)
                LoadReport();
        }

        protected void LoadReport()
        {
            int[] values = report.Filter.PayrollPeriodValue;
            if (values == null || values.Length < 2)
                return;

            PayrollPeriod payrollPeriod = EmployeeManager.GetPastRetiredPayrollPeriod(report.Filter.RetiredEmployeeId);

            if (payrollPeriod == null)
                return;

            //int payslipType = 1;
            PayslipType paysliptype = PayslipType.NormalTotal;

            if (values[1] == 1 && report.Filter.PaySummaryTypeValue == 3)
            {
                paysliptype = PayslipType.AdjustmentWithArrear;
                //payslipType = 3;
            }
            else
            {
                paysliptype = (PayslipType)values[1];
                //payslipType = values[1];
            }

            Report_Pay_PaySlipDetailTableAdapter mainAdapter = 
                new Report_Pay_PaySlipDetailTableAdapter();


            Report.Templates.Pay.Detail.ReportPaySlipRetired mainReport = new Report.Templates.Pay.Detail.ReportPaySlipRetired();
        

         
            int? employeeId = report.Filter.RetiredEmployeeId;

           
            {
                mainReport.labelMonth.Text = string.Format("{0} ({1} {2})", payrollPeriod.Name,
                                                           DateHelper.GetMonthsForOtherPayrollPeriod(payrollPeriod.Month,
                                                                                                     IsEnglish)
                                                                                                     , DateManager.GetOtherYearForPayrollPeriod(payrollPeriod, IsEnglish));
                mainReport.labelCompanyName.Text = new CompanyManager().GetById(SessionManager.CurrentCompanyId).Name;
             

            }
         

            BLL.BaseBiz.SetConnectionPwd(mainAdapter.Connection);
            ReportDataSet.Report_Pay_PaySlipDetailDataTable mainTable =
                mainAdapter.GetData(payrollPeriod.Month, payrollPeriod.Year, SessionManager.CurrentCompanyId,report.Filter.RetiredEmployeeId,""
                , report.Filter.MultiSelectionDropDownValues, values[1], values[2], report.Filter.BranchId);



            #region "Income/Deduction/Atte List"

            // Code to retrieve all emp once & save the DataTable in the request Cache so that multiple db request can be prevented
            // due to large no of employees
            Report_Pay_PaySlipDetail_EmpHeaderTableAdapter incomeHeaderAdapter = new Report_Pay_PaySlipDetail_EmpHeaderTableAdapter();
            BLL.BaseBiz.SetConnectionPwd(incomeHeaderAdapter.Connection);

            Report_Pay_PaySlip_AttendanceTableAdapter attendacDetailAdapter = new Report_Pay_PaySlip_AttendanceTableAdapter();
            BLL.BaseBiz.SetConnectionPwd(attendacDetailAdapter.Connection);

           

            ReportDataSet.Report_Pay_PaySlipDetail_EmpHeaderDataTable incomesTable = incomeHeaderAdapter.GetData(payrollPeriod.Month, payrollPeriod.Year,
                SessionManager.CurrentCompanyId, employeeId, true, null, 0, 0, (int)paysliptype, values[2]);
            ReportDataSet.Report_Pay_PaySlipDetail_EmpHeaderDataTable deductionTable = incomeHeaderAdapter.GetData(payrollPeriod.Month, payrollPeriod.Year,
                SessionManager.CurrentCompanyId, employeeId, false, null, 0, 0, (int)paysliptype, values[2]);

            ReportDataSet.Report_Pay_PaySlip_AttendanceDataTable attendanceTable =
                    attendacDetailAdapter.GetData(payrollPeriod.Month, payrollPeriod.Year, SessionManager.CurrentCompanyId, null,null);

            Report_Pay_RetirementDetailsTableAdapter retirementAdapter = new Report_Pay_RetirementDetailsTableAdapter();
            ReportDataSet.Report_Pay_RetirementDetailsDataTable retirementIncomeTable
                 = retirementAdapter.GetData(payrollPeriod.Month, payrollPeriod.Year, report.Filter.RetiredEmployeeId, true, SessionManager.CurrentCompanyId);
            ReportDataSet.Report_Pay_RetirementDetailsDataTable retirementDeductionTable
                = retirementAdapter.GetData(payrollPeriod.Month, payrollPeriod.Year, report.Filter.RetiredEmployeeId, false, SessionManager.CurrentCompanyId);


            try
            {
                //to prevent db timeout due to 100 connection full
                incomeHeaderAdapter.Connection.Close();
                attendacDetailAdapter.Connection.Close();
            }

            catch (Exception exp)
            {
                Log.log("Error", exp);
            }
            HttpContext.Current.Items["PaySlipIncomeList"] = incomesTable;
            HttpContext.Current.Items["PaySlipDeductionList"] = deductionTable;
            HttpContext.Current.Items["PaySlipRetIncomeList"] = retirementIncomeTable;
            HttpContext.Current.Items["PaySlipRetDeductionList"] = retirementDeductionTable;
            HttpContext.Current.Items["PaySlipAttendanceList"] = attendanceTable;
            #endregion

            
            //else// if (CommonManager.CompanySetting.IsD2 == false)
            {
                mainReport.Month = payrollPeriod.Month;
                mainReport.Year = payrollPeriod.Year.Value;
                mainReport.IsIncome = true;
                mainReport.CompanyId = SessionManager.CurrentCompanyId;
                mainReport.DataSource = mainTable;
                mainReport.DataMember = "Report";
                
                // set net pay
                mainReport.netPay.Text = GetCurrency(
                    incomesTable.Where(x=>x.EIN==employeeId).Select(x => x.Amount).Sum() + retirementIncomeTable.Select(x => x.Amount).Sum()
                    - deductionTable.Where(x => x.EIN == employeeId).Select(x => x.Amount).Sum() - retirementDeductionTable.Select(x => x.Amount).Sum());
                
                report.DisplayReport(mainReport);

                if (ReportHelper.IsReportExportState())
                {
                    this.report.clearCache = true;
                }
            }
           
            //else
            //{
            //    mainReportD2.Month = payrollPeriod.Month;
            //    mainReportD2.Year = payrollPeriod.Year.Value;
            //    mainReportD2.IsIncome = true;
            //    mainReportD2.CompanyId = SessionManager.CurrentCompanyId;
            //    mainReportD2.DataSource = mainTable;
            //    mainReportD2.DataMember = "Report";
            //    report.DisplayReport(mainReportD2);
            //    if (ReportHelper.IsReportExportState())
            //    {
            //        this.report.clearCache = true;
            //    }
            //}
            
        }
    }
}
