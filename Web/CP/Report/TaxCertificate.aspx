<%@ Page Title="Tax Certificate" Language="C#" MaintainScrollPositionOnPostback="true"
    MasterPageFile="~/Master/HROld.Master" AutoEventWireup="true" CodeBehind="TaxCertificate.aspx.cs"
    Inherits="Web.CP.Report.TaxCertificate" %>

<%@ Register Assembly="DevExpress.XtraReports.v16.1.Web, Version=16.1.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraReports.Web" TagPrefix="dxxr" %>
<%@ Register Src="~/Controls/Report/ReportFilterBranchDep.ascx" TagName="ReportFilterBranchDep"
    TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Report/ReportContainer.ascx" TagName="ReportContainer"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .w3-note
        {
            background-color: #ffffcc;
            border-left: 6px solid #ffeb3b;
        }
        
        .w3-panel
        {
            padding: 8px 16px;
            margin-bottom: 16px !important;
            width: 460px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Tax Certificate</h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">

        <table>
            <tr>
               
                 <td style='    padding-left: 0px;
    padding-bottom: 20px;'><a href='../../newhr/yearlyincomesettings.aspx'>Configure Income Settings</a></td>
            </tr>
        </table>

        
        
        
        <uc1:ReportContainer runat="server" Message="" OnReloadReport="LoadReport" id="report" />
    </div>
</asp:Content>
