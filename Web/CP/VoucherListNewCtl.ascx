<%@ Control Language="C#" EnableViewState="true" AutoEventWireup="true" CodeBehind="VoucherListNewCtl.ascx.cs"
    Inherits="Web.VoucherListNewCtl" %>
<%@ Import Namespace="BLL.Manager" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<%@ Register Src="../Controls/MultiCheckCombo.ascx" TagName="MultiCheckCombo" TagPrefix="uc2" %>
<script type="text/javascript">
    var skipLoadingCheck = true;
</script>
<div class="pageheader">
    <div class="media">
        <div class="media-body">
            <h4 id="title" runat="server">
                Accounting Voucher
            </h4>
        </div>
    </div>
</div>
<div class="alert alert-warning" style="margin: 20px 20px 0px 20px;">
    <span runat="server" visible="false" id="salaryNotSaved" style="color: #FF3400">Note
        : All employees salary not saved for this period, so voucher report is not complete.</span>
    <span runat="server" visible="false" id="spanWarningForIncomeDeductionHead"></span>
</div>
<div class="contentpanel">
    <asp:HiddenField runat="server" ID="hiddenAddOnId" />
    <asp:HiddenField runat="server" ID="hiddenSelectedEmp" />
    <div class="attribute" style="padding-bottom: 10px;padding-top:10px;">
        <table class="fieldTable">
            <tr>
                <td class="filterHeader" runat="server" id="rowPayrollFrom1">
                    <strong>Payroll</strong>
                </td>
                <td>
                    Voucher Type
                </td>
                <td runat="server" id="tdExternalCIT1" visible="false">
                    External CIT
                </td>
                <td>
                    Branch
                </td>
                <td>
                    Retire Filter
                </td>
                <td runat="server" id="statusRow1" visible="false">
                    Status
                </td>
                <td>
                    <asp:CheckBox ID="isAddOn" AutoPostBack="true" runat="server" Text="Add-On" OnCheckedChanged="isAddOn_CheckedChanged" />
                </td>
                <td runat="server" id="rowDateFilter1" visible="false">
                    Add on Date filter
                </td>
                <td runat="server" id="tdCardDepartmentFilter1">
                    Card Deparment
                </td>
            </tr>
            <tr>
                <td runat="server" id="rowPayrollFrom2">
                    <asp:DropDownList ID="ddlPayrollFromMonth" runat="server">
                    </asp:DropDownList>
                    <asp:DropDownList ID="ddlPayrollFromYear" runat="server">
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:DropDownList ID="ddlVoucherType" AppendDataBoundItems="true" DataValueField="VoucherTypeID"
                        DataTextField="Name" runat="server" Width="152px">
                        <asp:ListItem Text="--Select Voucher Type--" Value="-1"></asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" Display="None" ValidationGroup="HFamily"
                        runat="server" ControlToValidate="ddlVoucherType" InitialValue="-1" ErrorMessage="Voucher type is required."></asp:RequiredFieldValidator>
                </td>
                <td runat="server" id="tdExternalCIT2" visible="false">
                    <asp:DropDownList ID="ddlExternalCIT" runat="server" Width="100px">
                        <asp:ListItem Text="Include CIT" Value="true"></asp:ListItem>
                        <asp:ListItem Text="Exclude CIT" Value="false"></asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:DropDownList ID="ddlBranch" AppendDataBoundItems="true" DataValueField="BranchId"
                        DataTextField="Name" runat="server" Width="152px">
                        <asp:ListItem Text="--Select Branch--" Value="-1"></asp:ListItem>
                    </asp:DropDownList>
                    <%--   <asp:requiredfieldvalidator id="RequiredFieldValidator1" display="None" validationgroup="HFamily"
                        runat="server" controltovalidate="ddlVoucherType" initialvalue="-1" errormessage="Voucher type is required."></asp:requiredfieldvalidator>--%>
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlRetirementType" Width="100px">
                        <asp:ListItem Text="Active only" Value="false" />
                        <asp:ListItem Text="All" Value="All" />
                        <asp:ListItem Text="Retired only" Value="true" />
                    </asp:DropDownList>
                </td>
                <td runat="server" id="statusRow2" visible="false">
                    <uc2:MultiCheckCombo ID="MultiCheckCombo1" runat="server" />
                </td>
                <td>
                    <asp:DropDownList ID="ddlAddOnName" AutoPostBack="true" OnSelectedIndexChanged="ddlAddOnName_OnSelectedIndexChanged"
                        Enabled="false" runat="server" Width="120px" DataTextField="Name" DataValueField="AddOnId" />
                </td>
                <td runat="server" id="rowDateFilter2" visible="false">
                    <asp:DropDownList ID="ddlAddOnDateFilter" runat="server" Width="160px" DataTextField="Text"
                        DataValueField="Text">
                    </asp:DropDownList>
                </td>
                <td runat="server" id="tdCardDepartmentFilter2">
                    <asp:DropDownList ID="ddlCardDepFilter" runat="server" Width="130px">
                        <asp:ListItem Text="All" Value="-1" />
                        <asp:ListItem Text="Card Dep Only" Value="true" />
                        <asp:ListItem Text="Exclude Card Dep" Value="false" />
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td valign="bottom" style='padding-top:10px'>
                    <asp:Button ID="btnLoad" OnClick="btnLoad_Click" Style='float: left; width: 100px;'
                        CssClass="btn btn-default btn-sm btn-sect" runat="server" Text="View" />
                </td>
                <td valign="bottom" style='padding-top:10px'>
                    <ext:Button ID="btnLoadEmployeeFilter" ToolTip="Employee Filter works only for Retired list and Add-On list" OnClick="btnLoadEmployeeFilter_Click" AutoPostBack="true"
                        runat="server" Cls="btn btn-success" Text="Employee Filter" Width="120">
                        <%--<DirectEvents>
                            <Click OnEvent="btnLoadEmployeeFilter_Click">
                                <EventMask ShowMask="true" />
                            </Click>
                        </DirectEvents>--%>
                    </ext:Button>
                </td>
                <td  style='padding-top:10px' valign="bottom">
                    <asp:Button ID="btnExport" CssClass="excel" runat="server" Text="Export" OnClick="btnExport_Click" />
                </td>
            </tr>
        </table>
    </div>
    <div class="alert alert-warning" style="" runat="server" id="msg">
    </div>
    <div class="clear gridBlock">
        <table>
            <tr>
                <td valign="top">
                    <cc2:EmptyDisplayGridView PageSize="100" AllowPaging="true" EnableViewState="false"
                        CssClass="table table-primary mb30 table-bordered table-hover" UseAccessibleHeader="true"
                        ShowHeaderWhenEmpty="True" ID="gvEmployeeIncome" runat="server" AutoGenerateColumns="False"
                        CellPadding="4" GridLines="None" ShowFooterWhenEmpty="False" OnPageIndexChanging="gvEmployeeIncome_PageIndexChanging">
                        <RowStyle BackColor="#E3EAEB" />
                        <Columns>
                            <asp:BoundField DataField="MainCode" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                                HeaderStyle-HorizontalAlign="Left" HeaderText="MAINCODE"></asp:BoundField>
                            <asp:TemplateField HeaderText="CYCODE">
                                <ItemTemplate>
                                    NPR
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="BranchCode" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                                HeaderStyle-HorizontalAlign="Left" HeaderText="BRCODE"></asp:BoundField>
                            <asp:BoundField DataField="TranCode" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                                HeaderStyle-HorizontalAlign="Left" HeaderText="TRANTYPE"></asp:BoundField>
                            <asp:BoundField DataField="Amount" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                                HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" HeaderText="Amount"
                                DataFormatString="{0:N2}" />
                            <asp:BoundField DataField="TranType" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                                HeaderStyle-HorizontalAlign="Left" HeaderText="Desc1"></asp:BoundField>
                            <asp:BoundField DataField="" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" HeaderStyle-HorizontalAlign="Left"
                                HeaderText="Desc2"></asp:BoundField>
                            <%--<asp:BoundField DataField="Memo" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                                    HeaderStyle-HorizontalAlign="Left" HeaderText="Desc1"></asp:BoundField>--%>
                        </Columns>
                        <RowStyle CssClass="odd" />
                        <AlternatingRowStyle CssClass="even" />
                        <EmptyDataTemplate>
                            <b>No employee list. </b>
                        </EmptyDataTemplate>
                    </cc2:EmptyDisplayGridView>
                    <cc2:EmptyDisplayGridView PageSize="100" Visible="false" AllowPaging="true" EnableViewState="false"
                        CssClass="table table-primary mb30 table-bordered table-hover" UseAccessibleHeader="true"
                        ShowHeaderWhenEmpty="True" ID="gvEmployeeIncomeNCCVoucher1" runat="server" AutoGenerateColumns="False"
                        CellPadding="4" GridLines="None" ShowFooterWhenEmpty="False" >
                        <RowStyle BackColor="#E3EAEB" />
                        <Columns>
                            <asp:BoundField DataField="MainCode" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                                HeaderStyle-HorizontalAlign="Left" HeaderText="MainCode"></asp:BoundField>
                           
                            <asp:BoundField DataField="TranCode" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                                HeaderStyle-HorizontalAlign="Left" HeaderText="TranCode"></asp:BoundField>
                            <asp:BoundField DataField="Desc1" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                                HeaderStyle-HorizontalAlign="Left" HeaderText="Desc1"></asp:BoundField>
                            <asp:BoundField DataField="Desc2" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                                HeaderStyle-HorizontalAlign="Left" HeaderText="Desc2"></asp:BoundField>
                            <asp:BoundField DataField="Desc3" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                                HeaderStyle-HorizontalAlign="Left" HeaderText="Desc3"></asp:BoundField>
                            <asp:BoundField DataField="AMOUNT" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                                HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" HeaderText="AMOUNT"
                                DataFormatString="{0:N2}" />
                            <asp:BoundField DataField="LCYamount" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                                HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" HeaderText="LCYamount"
                                DataFormatString="{0:N2}" />
                            <asp:BoundField DataField="BranchCode" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                                HeaderStyle-HorizontalAlign="Left" HeaderText="BranchCode"></asp:BoundField>
                            <asp:BoundField DataField="BranchEmployeeName" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" HeaderStyle-HorizontalAlign="Left"
                                HeaderText="Branch/EmployeeName"></asp:BoundField>
                            <%--<asp:BoundField DataField="Memo" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                                    HeaderStyle-HorizontalAlign="Left" HeaderText="Desc1"></asp:BoundField>--%>
                        </Columns>
                        <RowStyle CssClass="odd" />
                        <AlternatingRowStyle CssClass="even" />
                        <EmptyDataTemplate>
                            <b>No employee list. </b>
                        </EmptyDataTemplate>
                    </cc2:EmptyDisplayGridView>
                </td>
            </tr>
        </table>
    </div>
    <div class="buttonsDiv">
    </div>
    <ext:Window ButtonAlign="Left" runat="server" Layout="BorderLayout" ID="windowNew"
        Title="Select employees for filter : " Width="500" Height="600" Hidden="true">
        <Items>
            <ext:GridPanel Region="Center" ID="grid" runat="server" Cls="itemgrid" Scroll="Both">
                <Store>
                    <ext:Store ID="Store1" runat="server" AutoLoad="true">
                        <%-- <Proxy>
                            <ext:PageProxy />
                        </Proxy>--%>
                        <Model>
                            <ext:Model ID="Model4" runat="server" IDProperty="ID">
                                <Fields>
                                    <ext:ModelField Name="ID" Type="Int" />
                                    <ext:ModelField Name="Text" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:Column ID="Column4" Sortable="true" MenuDisabled="true" runat="server" Text="EIN"
                            Width="60" Align="Center" DataIndex="ID">
                        </ext:Column>
                        <ext:Column ID="colEmployeeName" Sortable="true" MenuDisabled="true" runat="server"
                            Text="Name" Width="160" Align="Left" DataIndex="Text">
                        </ext:Column>
                    </Columns>
                </ColumnModel>
                <Listeners>
                    <SelectionChange Handler="#{windowNew}.setTitle('Select employees for Add-On filter : ' + this.getSelectionModel().selected.items.length + ' employees selected');" />
                </Listeners>
                <SelectionModel>
                    <ext:CheckboxSelectionModel ID="CheckboxSelectionModel1" runat="server" Mode="Simple">
                        <%--<CustomConfig>
                            <ext:ConfigItem Name="renderer" Value="checkboxRenderer" Mode="Raw" />
                        </CustomConfig>
                        <Listeners>
                            <BeforeSelect Handler="return (record.data.Status == 0 || record.data.Status == 3);" />
                        </Listeners>--%>
                    </ext:CheckboxSelectionModel>
                </SelectionModel>
                <View>
                    <ext:GridView ID="GridView1" runat="server" StripeRows="true" />
                </View>
            </ext:GridPanel>
        </Items>
        <Buttons>
            <ext:Button ID="btnLoadSelectedEmpVoucher" AutoPostBack="true" OnClick="btnLoadSelectedEmpVoucher_Click"
                runat="server" Cls="btn btn-primary" Text="Select Employees" Width="120">
            </ext:Button>
            <ext:Button ID="btnClearSelection" StyleSpec="margin-left:10px" AutoPostBack="true"
                OnClick="btnClearSelection_Click" runat="server" Cls="btn btn-primary" Text="Clear Selection"
                Width="120">
            </ext:Button>
        </Buttons>
    </ext:Window>
</div>
