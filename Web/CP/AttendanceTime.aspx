<%@ Page Title="Attendance Time" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="AttendanceTime.aspx.cs" Inherits="Web.AttendanceTime" %>

<%@ Import Namespace="BLL.Manager" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .tableLightColor input
        {
            width: 120px;
        }
        .tableLightColor .odd td, .tableLightColor .even td
        {
            border-right: 1px solid #DFECFD;
        }
    </style>
    <script src="../Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        function refreshWindow() {

            __doPostBack('Refresh', 0);

        }
        jQuery(document).ready(
    function () {

        setMovementToGrid('#<%= gvEmployeeIncome.ClientID %>');
    }
);

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">

         <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Attendance Time History
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
    
    

    
    <div class="attribute">
        <table>
            <tr>
                <td>
                    <asp:LinkButton ID="btnEmpTime" runat="server" Text="Emp Time Import" OnClientClick="empTime();return false;"
                        CssClass=" excel marginRight tiptip" Style="float: left;" />
                </td>
            </tr>
        </table>
    </div>
    <div class="clear">
        <cc2:EmptyDisplayGridView CssClass="tableLightColor" UseAccessibleHeader="true" ShowHeaderWhenEmpty="True"
            ID="gvEmployeeIncome" runat="server" AutoGenerateColumns="False" CellPadding="4"
            GridLines="None" AllowSorting="True" ShowFooterWhenEmpty="False">
            <RowStyle BackColor="#E3EAEB" />
            <Columns>
                <asp:BoundField ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyyy/MMM/dd}"
                    DataField="DateEng" HeaderText="Date"></asp:BoundField>
                <asp:BoundField DataFormatString="{0:MM/dd/yyyy}" DataField="ShiftName" HeaderText="Shift">
                </asp:BoundField>
                <asp:BoundField ItemStyle-HorizontalAlign="Right" DataField="OfficeInTime" HeaderText="In Time">
                </asp:BoundField>
                <asp:BoundField ItemStyle-HorizontalAlign="Right" DataField="OfficeOutTime" HeaderText="Out Time">
                </asp:BoundField>
                <asp:BoundField ItemStyle-HorizontalAlign="Right" DataField="OfficeFirstHalfInTime"
                    HeaderText="Break In"></asp:BoundField>
                <asp:BoundField ItemStyle-HorizontalAlign="Right" DataField="OfficeSecondHalfOutTime"
                    HeaderText="Break Out"></asp:BoundField>
                <asp:BoundField ItemStyle-HorizontalAlign="Right" DataField="HalfDayHolidayOutTime"
                    HeaderText="Half Holiday Out Time"></asp:BoundField>
                <asp:BoundField ItemStyle-HorizontalAlign="Right" DataField="HalfDayHolidayInTime"
                    HeaderText="HalfDay Leave In Out Time"></asp:BoundField>
                <asp:BoundField ItemStyle-HorizontalAlign="Right" DataField="PublicHolidayInTime"
                    HeaderText="Public Holiday In Time"></asp:BoundField>
                <asp:BoundField ItemStyle-HorizontalAlign="Right" DataField="PublicHolidayOutTime"
                    HeaderText="Public Holiday Out Time"></asp:BoundField>
            </Columns>
            <RowStyle CssClass="odd" />
            <EmptyDataTemplate>
                <b>No time list. </b>
            </EmptyDataTemplate>
        </cc2:EmptyDisplayGridView>
    </div>

    </div>
</asp:Content>
