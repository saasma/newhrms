<%@ Page Title="Employee Latest Event" Language="C#" EnableViewState="false" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="EmployeeLatestEvent.aspx.cs" Inherits="Web.EmployeeLatestEvent" %>

<%@ Import Namespace="BLL.Manager" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Scripts/ScrollTableHeader103.min.js?v=200" type="text/javascript"></script>
    <script type="text/javascript">

        var skipLoadingCheck = true;

        $(document).ready(
            function () {

                $('.tableLightColor tbody').attr('id', 'scrollMe');
                var sth = new ScrollTableHeader();

                sth.addTbody("scrollMe");
                sth.delayAfterScroll = 150;
                sth.minTableRows = 10;
            }
        );
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Employee Latest Event Details
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <table class="fieldTable">
            <tr>
                <td>
                    <strong>Branch</strong>
                </td>
                <td>
                    <strong>Department</strong>
                </td>
                <td>
                    <strong>Status</strong>
                </td>
                <td rowspan="2" valign="bottom">
                    <asp:Button ID="btnLoads" CssClass="btn btn-default btn-sect btn-sm" Style="width: 80px"
                        OnClick="btnLoad_Click" runat="server" Text="Load"></asp:Button>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:DropDownList ID="ddlBranch" Width="130px" DataTextField="Name" DataValueField="BranchId"
                        AutoPostBack="false" AppendDataBoundItems="true" runat="server">
                        <asp:ListItem Text="" Value="-1" />
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:DropDownList ID="ddlDepartment" Width="130px" DataTextField="Name" DataValueField="DepartmentId"
                        AutoPostBack="false" runat="server" AppendDataBoundItems="true">
                        <asp:ListItem Text="" Value="-1" />
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:DropDownList ID="ddlStatus" Width="130px" DataTextField="Value" DataValueField="Key"
                        AutoPostBack="false" runat="server" AppendDataBoundItems="true">
                        <asp:ListItem Text="" Value="-1" />
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
        <br />
        <div class="clear gridBlock">
            <cc2:EmptyDisplayGridView CssClass="table table-primary mb30 table-bordered table-hover"
                UseAccessibleHeader="true" ShowHeaderWhenEmpty="True" ID="gvEmployeeIncome" runat="server"
                DataKeyNames="EmployeeId" AutoGenerateColumns="False" CellPadding="4" GridLines="None"
                AllowSorting="True" ShowFooterWhenEmpty="False">
                <Columns>
                    <asp:BoundField DataField="RowNum" HeaderText="SN"></asp:BoundField>
                    <asp:BoundField DataField="EmployeeId" HeaderText="EIN"></asp:BoundField>
                    <asp:BoundField DataField="INo" HeaderText="I No"></asp:BoundField>
                    <asp:TemplateField HeaderText="Name" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:Label runat="server" Width="140" Text='<%# Eval("Name") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="DateOfBirthEng" DataFormatString="{0:dd-MMM-yyyy}" HeaderStyle-HorizontalAlign="Left" HeaderText="Birth Date">
                    </asp:BoundField>
                    <asp:BoundField DataField="LatestEventName" HeaderStyle-HorizontalAlign="Left" HeaderText="Latest Event">
                    </asp:BoundField>
                    <asp:BoundField DataField="LatestEventDate" DataFormatString="{0:dd-MMM-yyyy}"  HeaderStyle-HorizontalAlign="Left" HeaderText="Latest Event Date">
                    </asp:BoundField>
                    <asp:BoundField DataField="AppointmentDate" DataFormatString="{0:dd-MMM-yyyy}"  HeaderStyle-HorizontalAlign="Left" HeaderText="Appointment Date">
                    </asp:BoundField>
                 

                    <asp:BoundField DataField="POSITION" HeaderStyle-HorizontalAlign="Left" HeaderText="Position">
                    </asp:BoundField>
                    <asp:BoundField DataField="StatusName" HeaderStyle-HorizontalAlign="Left" HeaderText="Service Status">
                    </asp:BoundField>
                    <asp:BoundField DataField="Branch" HeaderStyle-HorizontalAlign="Left" HeaderText="Branch">
                    </asp:BoundField>
                    <asp:BoundField DataField="Department" HeaderStyle-HorizontalAlign="Left" HeaderText="Department">
                    </asp:BoundField>
                </Columns>
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                <EmptyDataTemplate>
                    <b>No employee list. </b>
                </EmptyDataTemplate>
            </cc2:EmptyDisplayGridView>
        </div>
        <div class="buttonsDiv">
            <asp:Button ID="btnUpdate" CssClass="excel" runat="server" Text="Export" OnClick="btnUpdate_Click" />
        </div>
    </div>
</asp:Content>
