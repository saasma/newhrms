﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Base;
using BLL.Manager;
using Utils.Helper;
using DAL;

namespace Web.CP
{
    public partial class AEMedicalTax : BasePage
    {
        TaxManager taxMgr = new TaxManager();

        public string Changed
        {
            get
            {

                if (ViewState["Changed"] != null)
                    return (ViewState["Changed"].ToString());
                return "No";
            }
            set
            {
                ViewState["Changed"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialise();
            }
        }

        void Initialise()
        {


            int id = UrlHelper.GetIdFromQueryString("Id");
            if (id != 0)
            {
                this.CustomId = id;
                MedicalTaxCredit entity = taxMgr.GetMedicalTaxCredit(this.CustomId);
                txtEmployee.Text = entity.EEmployee.Name;
                txtPYUnAdjAmount.Text = Util.FormatForInput(entity.PYAmount);
                LoadList();
            }

            calDate.IsEnglishCalendar = SessionManager.CurrentCompany.IsEnglishDate;
            calDate.SelectTodayDate();

        }

        void LoadList()
        {
            gvw.DataSource = taxMgr.GetMedicalTaxDetailsList(this.CustomId);
            gvw.DataBind();

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid && this.CustomId != 0)
            {
                this.Changed = "Yes";
                int medicalTaxCreditDetailId = 0;

                if (gvw.SelectedIndex != -1)
                {
                    medicalTaxCreditDetailId = (int)gvw.DataKeys[gvw.SelectedIndex][0];
                }

                TaxManager mgr = new TaxManager();
                MedicalTaxCreditDetail detail = mgr.GetMedicalTaxCreditDetail(medicalTaxCreditDetailId);
                MedicalTaxCredit credit = mgr.GetMedicalTaxCredit(this.CustomId);
                decimal prevAmount = detail != null ? detail.Amount.Value : 0;

                if (taxMgr.SaveUpdateMedicalTaxDetail(medicalTaxCreditDetailId, this.CustomId,
                    calDate.SelectedDate.ToString(), txtRefNo.Text.Trim(), txtInstitution.Text.Trim(),
                    decimal.Parse(txtAmount.Text.Trim()), txtNotes.Text.Trim()))
                {

                    
                      
                    if (medicalTaxCreditDetailId == 0)
                    {
                        BLL.BaseBiz.PayrollDataContext.ChangeMonetories.InsertOnSubmit(BLL.BaseBiz.GetMonetoryChangeLog(credit.EmployeeId.Value,
                          (byte)MonetoryChangeLogTypeEnum.MedicalTaxCredit, "",
                           "-", txtAmount.Text , LogActionEnum.Add));   


                        JavascriptHelper.DisplayClientMsg("Medical tax credit detail saved.", Page);
                    }
                    else
                    {


                        if (prevAmount != decimal.Parse(txtAmount.Text))
                        {
                            BLL.BaseBiz.PayrollDataContext.ChangeMonetories.InsertOnSubmit(BLL.BaseBiz.GetMonetoryChangeLog(credit.EmployeeId.Value,
                                                      (byte)MonetoryChangeLogTypeEnum.MedicalTaxCredit, "",
                                                      prevAmount.ToString(), txtAmount.Text, LogActionEnum.Update));   
                        }

                       
                        JavascriptHelper.DisplayClientMsg("Medical tax credit detail updated.", Page);
                    }
                    BLL.BaseBiz.PayrollDataContext.SubmitChanges();

                    Clear();
                }
            }
        }

        void Clear()
        {
            gvw.SelectedIndex = -1;
            calDate.SelectTodayDate();
            txtRefNo.Text = "";
            txtInstitution.Text = "";
            txtAmount.Text = "";
            txtNotes.Text = "";
            btnSave.Text = Resources.Messages.Save;
            LoadList();
        }

        protected void gvw_Editing(object sender, GridViewEditEventArgs e)
        {
         

            e.Cancel = true;
            
        }
        protected void gvw_Deleting(object sender, GridViewDeleteEventArgs e)
        {
            int id = (int)gvw.DataKeys[e.RowIndex][0];

            bool result = taxMgr.DeleteMedicalTaxCreditDetail(id);

            Clear();
            Changed = "Yes";
            e.Cancel = true;
        }
        protected void gvw_SelectedIndexChanged(object sender, EventArgs e)
        {
            if( gvw.SelectedIndex ==-1)
                return;
            int id = (int)gvw.DataKeys[gvw.SelectedIndex][0];

            
            MedicalTaxCreditDetail entity = taxMgr.GetMedicalTaxCreditDetail(id);
            if (entity != null)
            {
                btnSave.Text = Resources.Messages.Update;

                calDate.SetSelectedDate(entity.Date, SessionManager.CurrentCompany.IsEnglishDate);
                txtRefNo.Text = entity.RefNo;
                txtInstitution.Text = entity.Institution;
                txtAmount.Text = entity.Amount.ToString();
                txtNotes.Text = entity.Notes;

            }

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            gvw.SelectedIndex = -1;
            Clear();
        }

        protected void btnUpdatePYAmt_Click(object sender, EventArgs e)
        {
            Page.Validate("AETaxCredit1");
            if (Page.IsValid)
            {
                this.Changed = "Yes";
                if (taxMgr.UpdateMedicalTaxCreditPYAmount(this.CustomId, decimal.Parse(txtPYUnAdjAmount.Text.Trim())))
                {
                    JavascriptHelper.DisplayClientMsg("Last year amount updated.", Page);
                }
            }
        }


        protected void Page_PreRender(object sender, EventArgs e)
        {
            string code = string.Format("var Changed = '{0}';", Changed);
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdfdfs343", code, true);
        }
    }
}
