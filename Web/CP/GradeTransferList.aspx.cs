﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using System.Text;
using BLL.Manager;
using Utils.Helper;
using DAL;
using System.Drawing;
using BLL.Base;

namespace Web.CP
{
    public partial class GradeTransferList : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialise();
            }

            if (Request.Form["__EVENTTARGET"] != null && Request.Form["__EVENTTARGET"].Equals("Reload"))
                BindGradeTransfers();

            JavascriptHelper.AttachPopUpCode(Page, "popGradeTransferDetls", "GradeTransferPopup.aspx", 620, 500);
        }

        private void Initialise()
        {
            BindGradeTransfers();
        }

        private void BindGradeTransfers()
        {
            int totalRecords = 0;
            gvwList.DataSource = NewHRManager.GetGradeTransfers(pagingCtl.CurrentPage - 1, int.Parse(pagingCtl.DDLRecords.SelectedValue), txtEmpSearchText.Text.Trim(), ref totalRecords);
            gvwList.DataBind();

            if (totalRecords == 0)
                pagingCtl.Visible = false;
            else
                pagingCtl.Visible = true;
            pagingCtl.UpdatePagingBar(totalRecords);
        }

        protected void btnLoad_Click(object sender, EventArgs e)
        {
            BindGradeTransfers();
        }

        protected void txtEmpSearchText_TextChanged(object sender, EventArgs e)
        {
            BindGradeTransfers();
        }

        protected void btnPrevious_Click()
        {
            pagingCtl.CurrentPage -= 1;
            BindGradeTransfers();
        }

        protected void btnNext_Click()
        {
            pagingCtl.CurrentPage += 1;
            BindGradeTransfers();
        }

        protected void ddlRecords_SelectedIndexChanged()
        {
            pagingCtl.CurrentPage = 1;
            BindGradeTransfers();
        }


    }
}