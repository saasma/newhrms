﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;
using Utils.Calendar;
using Utils.Helper;

namespace Web.NewHR
{
    public partial class InsurenceClaim : BasePage
    {

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!X.IsAjaxRequest && !IsPostBack)
            {
                Initialise();

            }
            JavascriptHelper.AttachNonDialogPopUpCode(Page, "InuranceImportPopUp", "../ExcelWindow/EmpInsuranceClaimImport.aspx", 450, 500);
        }

        public void Initialise()
        {
            storecmbStatus.DataSource = HRManager.GetTextValuesForInsuranceStatus(typeof(ClaimStatus));// ExtControlHelper.GetTextValues(typeof(ClaimStatus));
            storecmbStatus.DataBind();

        }

        public void btnAddClaim_Click(object sender, DirectEventArgs e)
        {
            InsuranceClaim.ShowPopUp(hiddenClaimId.Text.Trim());
        }



        public void btnEdit_Click(object sender, DirectEventArgs e)
        {
            if (!string.IsNullOrEmpty(hiddenClaimId.Text.Trim()))
            {

                SCClaim obj = HRManager.getClaim(Convert.ToInt32(hiddenClaimId.Text.Trim()));
                if (obj != null)
                {
                    txtCurrentStatus.Text = ((ClaimStatus)obj.Status).ToString();
                }
                WindowStatus.Show();
            }
        }
        public void btnSave_Click(object sender, DirectEventArgs e)
        {
            if (cmbStatus.SelectedItem != null && cmbStatus.SelectedItem.Index != -1)
            {
                if (txtCurrentStatus.Text.ToLower() == cmbStatus.SelectedItem.Text.ToLower())
                {
                    NewMessage.ShowWarningMessage("Similar status type!");
                    return;
                }

                Status status = HRManager.UpdateInsuranceClaimStatus(Convert.ToInt32(hiddenClaimId.Text.Trim()), Convert.ToInt32(cmbStatus.SelectedItem.Value), dfStatusChangeDate.SelectedDate);
                if (status.IsSuccess)
                {
                    hiddenClaimId.Text = "";
                    WindowStatus.Hide();
                    PagingToolbar1.DoRefresh();
                    PagingToolbar2.DoRefresh();
                    PagingToolbar3.DoRefresh();
                    NewMessage.ShowNormalMessage("Status updated successfully.");
                }
                else
                {
                    NewMessage.ShowWarningMessage("Unable to update!");
                }
            }
        }

        public void btnView_Click(object sender, DirectEventArgs e)
        {
            if (!string.IsNullOrEmpty(hiddenClaimId.Text.Trim()))
            {
                LoadEditData();
            }
        }

        protected void LoadEditData()
        {
            DAL.SCClaim obj = HRManager.getClaim(Convert.ToInt32(hiddenClaimId.Text.Trim()));
            if (obj != null)
            {
                InsuranceClaim.ShowPopUp(hiddenClaimId.Text.Trim());
            }
        }

        //public void btnExport_Click(object sender, DirectEventArgs e)
        //{
        //    if (!string.IsNullOrEmpty(hiddenLotNo.Text.Trim()))
        //    {
        //        int? totalRecords = 0;
        //        List<GetEmployeeInsuranceClaimListByLotResult> list = HRManager.GetEmployeeInsuranceClaimListByLot(-1, -1, -1, Convert.ToInt32(hiddenLotNo.Text.Trim()), 1, 0, 99999999, ref totalRecords);
        //        string template = ("~/App_Data/ExcelTemplate/EmpInsuranceClaimImport.xlsx");

        //        // List < FixedValueEdGetEmployeeInsuranceClaimListByLotucationLevel > listEducationLevel = CommonManager.GetEducationLevelList().ToList();
        //        //List<FixedValueEducationFaculty> listEducationFaculty = CommonManager.GetEducationFacultyList().ToList();

        //        ExcelGenerator.ExportEmployeeInsuranceClaim(template, list);

        //    }
        //}

        public void TabPanel1_TabChange(object sender, DirectEventArgs e)
        {
            int tabindex = 0;
            tabindex = TabPanel1.ActiveTabIndex;
            if (tabindex == 0)
            {
                GridClaimList.Show();
                GridLotHistory.Hide();
                GridClaimHistoryList.Hide();
                PagingToolbar1.DoRefresh();
            }
            else if (tabindex == 1)
            {
                GridClaimList.Hide();
                GridLotHistory.Show();
                GridClaimHistoryList.Hide();
                PagingToolbar3.DoRefresh();
            }
            else if (tabindex == 2)
            {
                GridClaimList.Hide();
                GridLotHistory.Hide();
                GridClaimHistoryList.Show();
                PagingToolbar2.DoRefresh();
            }
        }

        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            int ein = -1;
            int? total = 0;

            if (cmbSearch.SelectedItem != null && cmbSearch.SelectedItem.Value != null)
                ein = int.Parse(cmbSearch.SelectedItem.Value);

            DateTime? fromdate = null;
            DateTime? todate = null;


            if (!string.IsNullOrEmpty(dfSearchClaimDateFrom.RawText))
                fromdate = DateTime.Parse(dfSearchClaimDateFrom.Text);

            if (!string.IsNullOrEmpty(dfSearchClaimDateTo.RawText))
                todate = DateTime.Parse(dfSearchClaimDateTo.Text);

            StoreGridClaimList.DataSource = HRManager.GetEmployeeInsuranceClaimListForAdmin((int)ClaimStatus.Completed, ein, fromdate, todate, e.Page - 1, e.Limit, ref total);
            StoreGridClaimList.DataBind();
            e.Total = total.Value;

        }

        protected void Store_ReadData1(object sender, StoreReadDataEventArgs e)
        {
            int type = -1;
            int ein = -1;
            int? totalRecord = 0;

            if (cmbSearch.SelectedItem != null && cmbSearch.SelectedItem.Value != null)
                ein = int.Parse(cmbSearch.SelectedItem.Value);

            DateTime? fromdate = null;
            DateTime? todate = null;
            if (!string.IsNullOrEmpty(dfSearchClaimDateFrom.RawText))
                fromdate = DateTime.Parse(dfSearchClaimDateFrom.Text);

            if (!string.IsNullOrEmpty(dfSearchClaimDateTo.RawText))
                todate = DateTime.Parse(dfSearchClaimDateTo.Text);

            StoreGridClaimHistoryList.DataSource = HRManager.GetEmployeeInsuranceClaimList(type, (int)ClaimStatus.Completed, ein, fromdate, todate, e.Page - 1, e.Limit, ref totalRecord);
            StoreGridClaimHistoryList.DataBind();
            e.Total = totalRecord.Value;

        }

        protected void Store_ReadData2(object sender, StoreReadDataEventArgs e)
        {
            int? totalRecords = 0;

            StoreGridLotHistory.DataSource = HRManager.GetLotDetailsForInsurance(e.Page - 1, e.Limit, ref totalRecords);
            StoreGridLotHistory.DataBind();
            e.Total = totalRecords.Value;

        }


    }
}
