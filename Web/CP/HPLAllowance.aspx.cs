﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using BLL;
using DAL;
using Utils.Web;
using Utils.Helper;
using System.Text;

namespace Web.CP
{
    public partial class HPLAllowance : BasePage
    {
        CommonManager commonMgr = new CommonManager();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                Initialise();


            if (Request.Form["__EVENTTARGET"] != null && Request.Form["__EVENTTARGET"].Contains(ddlPayrollPeriods.ClientID))
            {
                BindOvertimes();
            }


            JavascriptHelper.AttachNonDialogPopUpCode(Page, "shiftPopup", "../ExcelWindow/ShiftAllowanceExcel.aspx", 450, 500);

          

        }






        public void Initialise()
        {



            ddlPayrollPeriods.DataSource = CommonManager.GetCurrentYear(SessionManager.CurrentCompanyId);
            ddlPayrollPeriods.DataBind();
            UIHelper.SelectListItem(ddlPayrollPeriods);

            BindOvertimes();


            if (HPLAllowanceManager.GetAllowanceRate().HideOtherAllowance != null &&
                HPLAllowanceManager.GetAllowanceRate().HideOtherAllowance.Value)
            {
                gvwOvertimes.Columns[13].Visible = false;
                gvwOvertimes.Columns[14].Visible = false;
                gvwOvertimes.Columns[15].Visible = false;
                gvwOvertimes.Columns[16].Visible = false;
                gvwOvertimes.Columns[17].Visible = false;

                gvwOvertimes.Width = new Unit(900);
            }


        }


        public bool IsEnable(object ein)
        {
            if (IsPayrollSaved)
                return false;

            return !IsAttendanceSavedForEmployee(payrollPeriodId, Convert.ToInt32(ein));
        }


        public bool IsPayrollSaved = false;
        public int payrollPeriodId;
        public void BindOvertimes()
        {
            this.IsPayrollSaved = false;
            payrollPeriodId = int.Parse(ddlPayrollPeriods.SelectedValue);
            IsPayrollSaved = IsPayrollFinalSaved(payrollPeriodId);

            gvwOvertimes.SelectedIndex = -1;
            gvwOvertimes.DataSource = HPLAllowanceManager.HPL_GetEmployeeForAllowance(payrollPeriodId);
            gvwOvertimes.DataBind();

       

            if (IsPayrollSaved)
            {
                pnlButtons.Visible = false;
                //btnExport.Visible = false;

                divWarningMsg.InnerHtml = Resources.ResourceTooltip.HPLAllowanceSalaryAlreadyGenerated;
                divWarningMsg.Hide = false;
            }
            else
            {
                pnlButtons.Visible = true;
              //  btnExport.Visible = true;

                if (LeaveAttendanceManager.IsAttendanceSaved(int.Parse(ddlPayrollPeriods.SelectedValue)))
                {
                    divWarningMsg.InnerHtml = Resources.ResourceTooltip.HPLAllowanceAttendanceSaved;
                    divWarningMsg.Hide = false;
                }
            }

            //if (LeaveAttendanceManager.IsAttendanceSaved(int.Parse(ddlPayrollPeriods.SelectedValue)))
            //{
            //    btnExport.Visible = false;
            //    //btnExport.ToolTip = Resources.ResourceTooltip.HPLAllowanceAttendanceSaved;
            //}
            //else
            //    btnExport.Visible = true;
        }

      

        protected void ddlPayrollPeriods_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindOvertimes();
        }


        protected void gvwOvertimes_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvwOvertimes.PageIndex = e.NewPageIndex;
            BindOvertimes();
        }

        

       
        protected void btnSave_Click1(object sender, EventArgs e)
        {
            StringBuilder xml = new StringBuilder();
            xml.Append("<root>");

            int payrollPeriodId = int.Parse(ddlPayrollPeriods.SelectedValue);

            int employeeId;

            float? Days_At_Palati, Days_At_Kirne, On_Call, Shift_Afternoon, Shift_Night, Split_Shift_Morning, Split_Shift_Evening, OT_150, OT_200, OT_300, Public_Holiday,
                Other_Leave, Snacks_Allowance, Special_Compensation_Allowance, Meal_Allowance, TADA, Vehicle_Allowance;



            bool isSaved = false;


            foreach (GridViewRow row in gvwOvertimes.Rows)
            {

                employeeId = (int)gvwOvertimes.DataKeys[row.RowIndex][0];

                TextBox firstTextbox = row.FindControl("Days_At_Palati") as TextBox;

                if (!firstTextbox.Enabled)
                    continue;

                isSaved = true;

                Days_At_Palati = ExcelGenerator.GetValueFromCell("Days_At_Palati", row, employeeId);
                //Days_At_Kirne = ExcelGenerator.GetValueFromCell("Days_At_Kirne", row, employeeId);

                On_Call = ExcelGenerator.GetValueFromCell("On_Call", row, employeeId);

                Shift_Afternoon = ExcelGenerator.GetValueFromCell("Shift_Afternoon", row, employeeId);
                Shift_Night = ExcelGenerator.GetValueFromCell("Shift_Night", row, employeeId);     
                Split_Shift_Morning = ExcelGenerator.GetValueFromCell("Split_Shift_Morning", row, employeeId);
                Split_Shift_Evening = ExcelGenerator.GetValueFromCell("Split_Shift_Evening", row, employeeId);

                OT_150 = ExcelGenerator.GetValueFromCell("OT_150", row, employeeId);
                OT_200 = ExcelGenerator.GetValueFromCell("OT_200", row, employeeId);
                OT_300 = ExcelGenerator.GetValueFromCell("OT_300", row, employeeId);
                Public_Holiday = ExcelGenerator.GetValueFromCell("Public_Holiday", row, employeeId);

                Other_Leave = ExcelGenerator.GetValueFromCell("Other_Leave", row, employeeId);

                Snacks_Allowance = ExcelGenerator.GetValueFromCell("Snacks_Allowance", row, employeeId);
                Special_Compensation_Allowance = ExcelGenerator.GetValueFromCell("Special_Compensation_Allowance", row, employeeId);
                Meal_Allowance = ExcelGenerator.GetValueFromCell("Meal_Allowance", row, employeeId);
                TADA = ExcelGenerator.GetValueFromCell("TADA", row, employeeId);
                Vehicle_Allowance = ExcelGenerator.GetValueFromCell("Vehicle_Allowance", row, employeeId);

                xml.AppendFormat(
                   @"<row PayrollPeriodId='{0}' EmployeeId='{1}' 
                                        {2} {3} 
                                        {4}

                                        {5} {6} {7} {8}

                                        {9} {10} {11} {12} 
                                        {13}
                                        {14} {15} {16} {17} {18} />",
                               payrollPeriodId, employeeId,

                                ExcelGenerator.GetColumnValue("Days_At_Palati", Days_At_Palati),
                                ExcelGenerator.GetColumnValue("Days_At_Kirne",(decimal) 0),

                                ExcelGenerator.GetColumnValue("On_Call", On_Call),

                                ExcelGenerator.GetColumnValue("Shift_Afternoon", Shift_Afternoon),
                                ExcelGenerator.GetColumnValue("Shift_Night", Shift_Night),
                                ExcelGenerator.GetColumnValue("Split_Shift_Morning", Split_Shift_Morning),
                                ExcelGenerator.GetColumnValue("Split_Shift_Evening", Split_Shift_Evening),

                                ExcelGenerator.GetColumnValue("OT_150", OT_150),
                                ExcelGenerator.GetColumnValue("OT_200", OT_200),
                                ExcelGenerator.GetColumnValue("OT_300", OT_300),
                                ExcelGenerator.GetColumnValue("Public_Holiday", Public_Holiday),

                                ExcelGenerator.GetColumnValue("Other_Leave", Other_Leave),

                                ExcelGenerator.GetColumnValue("Snacks_Allowance", Snacks_Allowance),
                                ExcelGenerator.GetColumnValue("Special_Compensation_Allowance", Special_Compensation_Allowance),
                                ExcelGenerator.GetColumnValue("Meal_Allowance", Meal_Allowance),
                                ExcelGenerator.GetColumnValue("TADA", TADA),
                                ExcelGenerator.GetColumnValue("Vehicle_Allowance", Vehicle_Allowance));

            }



            xml.Append("</root>");

            if (isSaved)
            {
                HPLAllowanceManager.HPL_SaveAllowance(xml.ToString()
                                                          , payrollPeriodId);

                divMsgCtl.InnerHtml = Resources.Messages.HPL_Allowance_Saved;
                divMsgCtl.Hide = false;
            }
        }

      
    }

}

