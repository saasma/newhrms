﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using BLL;
using DAL;
using Utils.Web;
using Utils.Helper;
using System.Text;

namespace Web.CP
{


    public class EmployeeStatus
    {
        public int Order { get; set; }
        public int StatusId { get; set; }
        public string DefaultName { get; set; }
        public string NewName { get; set; }
        public bool IsContract { get; set; }
   
    }

    public partial class ChangeStatus : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.AdministratorTools;
            }
        }

        CommonManager commonMgr = new CommonManager();



        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                Initialise();


          

        }






        public void Initialise()
        {
            if (CommonManager.CompanySetting.HasLevelGradeSalary == false)
                spanContractLevelGradeCase.Visible = false;

            List<StatusName> dbStatus = CommonManager.GetStatusName();
            List<EmployeeStatus> statusList = new List<EmployeeStatus>();

            if (dbStatus.Count > 0)
            {
                int order = 0;
                //// if Irregular not saved then add from external
                //if (dbStatus.Count == 5)
                //{
                //    order = 1;
                //    statusList.Add(
                //        new EmployeeStatus
                //        {
                //            Order = order,
                //            StatusId = (int)JobStatusEnum.Irregular,

                //            DefaultName = JobStatusEnum.Irregular.ToString(),
                //            NewName = JobStatusEnum.Irregular.ToString()
                //        }
                //        );
                //}

                for (int i = 0; i < dbStatus.Count; i++)
                {
                    statusList.Add(
                        new EmployeeStatus
                        {
                            //Order = ++order,
                            StatusId = dbStatus[i].StatusId,//dbStatus[i].StatusId,

                            DefaultName = ((JobStatusEnum)dbStatus[i].StatusId).ToString(),
                            NewName = dbStatus[i].Name,

                            IsContract = dbStatus[i].IsContractStatus == null ? false : dbStatus[i].IsContractStatus.Value

                        }
                        );
                }
            }
            else
            {
                statusList.Add(new EmployeeStatus
                {
                    Order = 1,
                    StatusId = (int)JobStatusEnum.Irregular,
                    DefaultName = JobStatusEnum.Irregular.ToString()
                }
                       );
                statusList.Add(new EmployeeStatus
                       {
                           Order = 2,
                           StatusId =(int) JobStatusEnum.Trainee,
                           DefaultName = JobStatusEnum.Trainee.ToString()
                       }
                       );
                statusList.Add(new EmployeeStatus
                {
                    Order = 3,
                    StatusId = (int)JobStatusEnum.Probation,
                    DefaultName = JobStatusEnum.Probation.ToString()
                }
                       );
                statusList.Add(new EmployeeStatus
                {
                    Order = 4,
                    StatusId = (int)JobStatusEnum.Contract,
                    DefaultName = JobStatusEnum.Contract.ToString()
                }
                       );
                statusList.Add(new EmployeeStatus
                {
                    Order = 5,
                    StatusId = (int)JobStatusEnum.Temporary,
                    DefaultName = JobStatusEnum.Temporary.ToString()
                }
                       );
                statusList.Add(new EmployeeStatus
                {
                    Order = 6,
                    StatusId = (int)JobStatusEnum.Permanent,
                    DefaultName = JobStatusEnum.Permanent.ToString()
                }
                       );
            }

        
           
            gvw.DataSource = statusList;
            gvw.DataBind();

            //HPLAllowanceRate rate = HPLAllowanceManager.GetAllowanceRate();

            //if (rate != null)
            //{


            //}


           

        }



        protected void btnSave_Click(object sender, EventArgs e)
        {
            bool contratSelected = false;

            if (Page.IsValid)
            {
                List<StatusName> list = new List<StatusName>();
                foreach (GridViewRow row in gvw.Rows)
                {
                    StatusName status = new StatusName();
                    TextBox txt= row.FindControl("txt") as TextBox;
                    status.StatusId = (int) gvw.DataKeys[row.RowIndex]["StatusId"];
                    status.Name = txt.Text.Trim();

                    CheckBox chkIsContract1 = row.FindControl("chkIsContract1") as CheckBox;

                    status.IsContractStatus = chkIsContract1.Checked;

                    //if (contratSelected == true && chkIsContract1.Checked)
                    //{
                    //    divWarningMsg.InnerHtml = "Multiple status can not be selected as Contract.";
                    //    divWarningMsg.Hide = false;
                    //    return;
                    //}

                    if (chkIsContract1.Checked)
                    {
                        contratSelected = true;
                    }


                    list.Add(status);
                }

                if (contratSelected == false)
                {
                    divWarningMsg.InnerHtml = "Any single status must be selected as contract.";
                    divWarningMsg.Hide = false;
                    return;
                }

                CommonManager.SaveStatusName(list);

                divMsgCtl.InnerHtml = "Saved";
                divMsgCtl.Hide = false;
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            CommonManager.DeleteStatus();
            Initialise();
            divMsgCtl.InnerHtml = "Deleted";
            divMsgCtl.Hide = false;
        }
    }

}

