<%@ Page Title="Income/Deduction Adjustment" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="SettlementAdjustment.aspx.cs" Inherits="Web.SettlementAdjustment" %>

<%@ Import Namespace="BLL.Manager" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>
    <script type="text/javascript">

        function calculatePayFromAdjustedAmount(txt1, txtExistingAmount, txtAdjustmentAmount, txtPay) {

            var txtAdjustedAmountElement = document.getElementById(txt1.id.replace(txtPay, txtPay)); ;
            var txtExistingAmountElement = document.getElementById(txt1.id.replace(txtPay, txtExistingAmount));
            var txtAdjustmentAmountElement = document.getElementById(txt1.id.replace(txtPay, txtAdjustmentAmount));

            //var txtPaidAmountElement = document.getElementById(txt1.id.replace(txtPay, "txtPaidAmount"));

            if (parseFloat(txtAdjustedAmountElement.value) == 0)
                return;

            //            var paidAmount = 0;
            //            if (txtPaidAmountElement != null) {
            //                paidAmount = getNumber(txtPaidAmountElement.value);
            //            }

            //            if (parseFloat(txtAdjustedAmountElement.value) == 0)
            //                txtAdjustmentAmountElement.value = "0";
            //            else 
            txtAdjustmentAmountElement.value = getMoney(getNumber(txtAdjustedAmountElement.value) - getNumber(txtExistingAmountElement.value));

        }

        function calculatePay(txt1, txtFirstId, txtSecondId, txtPay) {
            var txt2 = document.getElementById(txt1.id.replace(txtFirstId, txtSecondId));
            var result = document.getElementById(txt1.id.replace(txtFirstId, txtPay));

            //            var txtPaidAmountElement = document.getElementById(txt1.id.replace(txtFirstId, "txtPaidAmount"));
            //            var paidAmount = 0;
            //            if (txtPaidAmountElement != null) {
            //                paidAmount = getNumber(txtPaidAmountElement.value);
            //            }

            if (parseFloat(txt1.value) == 0)
                result.value = "0.00";
            else
                result.value = getMoney(getNumber(txt1.value) + getNumber(txt2.value));

        }

        function validateAdjustment(source, args) {


            var txtAdjustmentAmount = document.getElementById(source.controltovalidate);
            var txtExistingAmount = document.getElementById(source.controltovalidate.replace('txtAdjustmentAmount', 'txtExistingAmount'));

            var existingAmount = getNumber(txtExistingAmount.value);

            var value = getNumber(txtAdjustmentAmount.value);

            if (typeof (isNegativeValueAllowed) == 'undefined')
                isNegativeValueAllowed = false;

            if (isNegativeValueAllowed == false) {
                if (value < 0) {
                    if ((value + existingAmount) < 0)
                        args.IsValid = false;
                    else
                        args.IsValid = true;
                }
            }
            else
                args.IsValid = true;
        }






        function refreshWindow() {

            //window.location.reload();
        }
        
    </script>
    <script src="../Scripts/currency.js" type="text/javascript"></script>
    <style type="text/css">
        
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Settle Adjustment
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel" style="margin-top:7px !important; padding-top:7px !important;">
    <ext:ResourceManager ID="ResourceManager1" DisableViewState="false" runat="server"
        ShowWarningOnAjaxFailure="false" ScriptMode="Release" Namespace="Rigo" />
    <div class="contentArea">
        <asp:Panel runat="server" class="attribute" style="padding:10px" DefaultButton="btnLoad">
            <%--  <asp:UpdatePanel ID="upd" runat="server" UpdateMode="Conditional">
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnLoad" />
                </Triggers>
                <ContentTemplate>--%>
            <table class="fieldTable">
                <tr>
                    <td>
                        <strong>Payroll Period</strong>
                    </td>
                    <td>
                        <strong>Employee</strong>
                    </td>
                    <%-- <td>
                                <strong>Type</strong>
                            </td>--%>
                    <td>
                        <strong>Type</strong>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:DropDownList AutoPostBack="true" ID="ddlPayrollPeriods" DataTextField="Name"
                            DataValueField="PayrollPeriodId" AppendDataBoundItems="true" Width="150px" runat="server"
                            OnSelectedIndexChanged="ddlPayrollPeriods_SelectedIndexChanged">
                            <asp:ListItem Value="-1" Text=""></asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlPayrollPeriods"
                            Display="None" ErrorMessage="Payroll Period is required." InitialValue="-1" ValidationGroup="Balance"></asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlEmployee" DataTextField="Name" DataValueField="EmployeeId"
                            runat="server" AppendDataBoundItems="false" Width="200px">
                            <asp:ListItem Value="-1" Text=""></asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlEmployee"
                            Display="None" ErrorMessage="Employee is required." InitialValue="-1" ValidationGroup="Balance"></asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlType" DataTextField="Key" DataValueField="Value" runat="server"
                            AppendDataBoundItems="true" Width="200px">
                            <asp:ListItem Value="-1" Text=""></asp:ListItem>
                            <asp:ListItem Value="1" Text="Advance"></asp:ListItem>
                            <asp:ListItem Value="8" Text="Dashain Bonus"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Excessive Leave"></asp:ListItem>
                            <%-- <asp:ListItem Value="3" Text="Leave Encashment"></asp:ListItem>--%>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlType"
                            Display="None" ErrorMessage="Type is required." InitialValue="-1" ValidationGroup="Balance"></asp:RequiredFieldValidator>
                    </td>
                    <td valign="top">
                        <asp:Button ID="btnLoad" CssClass="load" OnClick="btnLoad_Click" runat="server" Text="Load" />
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
            <%--</ContentTemplate>
            </asp:UpdatePanel>--%>
        </asp:Panel>
        <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
        <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
        <div>
            <table cellspacing="5">
                <tr>
                    <td>
                        Calculated Amount :
                    </td>
                    <td>
                        <asp:TextBox ID="txtCalculatedAmount" Width="200" Enabled="false" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        New Amount :
                    </td>
                    <td>
                        <asp:TextBox ID="txtNewAmount" Width="200" runat="server" />
                    </td>
                </tr>
            </table>
        </div>
        <asp:Panel runat="server" DefaultButton="btnUpdate" Style='padding-top: 20px'>
            <div class="clear">
            </div>
            <div>
                <asp:Button OnClientClick="valGroup='Balance';return CheckValidation();" ID="btnUpdate"
                    CssClass="update" Visible="true" ValidationGroup="Balance" runat="server" Text="Apply"
                    OnClick="btnUpdate_Click" />
            </div>
        </asp:Panel>
    </div>
    </div>
</asp:Content>
