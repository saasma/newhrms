﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using Utils.Helper;
using Utils.Web;

namespace Web.CP
{
    public partial class AEIncomeOnly : System.Web.UI.Page
    {
        PayManager payMgr = new PayManager();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        void Process(ref PIncome entity)
        {
            if (entity == null)
            {
                entity = new PIncome();
                entity.Title = txtTitle.Text.Trim();
                entity.Abbreviation = txtAbbreviation.Text.Trim();
                entity.CompanyId = SessionManager.CurrentCompanyId;
            }
            else
            {
                txtTitle.Text = entity.Title;
                txtAbbreviation.Text = entity.Abbreviation;
            }
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                PIncome income = null;
                Process(ref income);

                payMgr.Save(income);

                JavascriptHelper.DisplayClientMsg("Income saved.", this, "closePopup();");
            }
        }
    }
}
