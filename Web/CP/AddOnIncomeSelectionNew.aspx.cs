﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Base;
using BLL.Manager;
using Utils.Helper;
using DAL;
using Utils.Web;

namespace Web.CP
{
    public partial class AddOnIncomeSelectionNew : BasePage
    {
        int payrollPeriodId = 0;
        PayManager payMgr = new PayManager();
        int? employeeId = null;

        bool IsSettingEditable = true;

        protected void Page_Load(object sender, EventArgs e)
        {
            string pp = Request.QueryString["p"];
            if (pp.Contains(":"))
                payrollPeriodId = int.Parse(pp.Substring(0, pp.IndexOf(":")));
            else
                payrollPeriodId = int.Parse(pp);

            if (CalculationManager.IsAllEmployeeSavedFinally(payrollPeriodId, false))
            {
                IsSettingEditable = false;

                gvwSelectedIncomesIncomes.Columns[4].Visible = false;
                gvwSelectedIncomesIncomes.Columns[5].Visible = false;
                gvwSelectedIncomesIncomes.Columns[6].Visible = false;

                gvwDeductions.Columns[2].Visible = false;

                btnSaveIncomes.Visible = false;
                btnGenerateAmount.Visible = false;
                btnOk.Visible = false;
            }
            else
                IsSettingEditable = true;

            if (!string.IsNullOrEmpty(Request.QueryString["ein"]))
            {
                employeeId = int.Parse(Request.QueryString["ein"]);
                if (employeeId != null)
                    btnGenerateAmount.OnClientClick = string.Format("return confirm('Confirm generate income amounts for \"{0}\" only?');",
                        EmployeeManager.GetEmployeeName(employeeId.Value));
            }
            else
            {
                btnGenerateAmount.OnClientClick = string.Format("return confirm('Confirm generate income amounts for all employees, old amount and date will be removed?');");
            }

            if (payrollPeriodId != 0)
            {
                PayrollPeriod payroll= CommonManager.GetPayrollPeriod(payrollPeriodId);
                header.InnerHtml = string.Format(header.InnerHtml, payroll.Name);
            }

            string eventTarget = Request.Form["__EVENTTARGET"];
            //employeeId = UrlHelper.GetIdFromQueryString("EId");
            if (!IsPostBack)
            {
                Initialise();
            }
            else if (eventTarget != null && eventTarget.Equals("Reload"))
            {
                Initialise();
            }


            JavascriptHelper.AttachPopUpCode(Page, "popupIncome", "AEIncome.aspx", 600, 600);
        }

        public void ddlAddOnName_Changed(object sender, EventArgs e)
        {
            Initialise();
        }

        void Initialise()
        {

            if (!IsPostBack)
            {
                ddlAddOnName.DataSource = PayManager.GetAddOnForPayrollPeriod(payrollPeriodId);
                ddlAddOnName.DataBind();

                int queryAddOndId = int.Parse(Request.QueryString["id"]);
                UIHelper.SetSelectedInDropDown(ddlAddOnName, queryAddOndId);
            }


            int addOnId = 0;

            if (!string.IsNullOrEmpty(ddlAddOnName.SelectedValue))
                addOnId = int.Parse(ddlAddOnName.SelectedValue);

            List<PIncome> availableList = null; List<PIncome> availableFixedList = null; 
            List<CalcGetHeaderListResult> selectedList = null;
            List<CalcGetHeaderListResult> selectedDeductionList = null;

            PartialAddOnManager.SetIncomeList(payrollPeriodId,addOnId, ref availableList, ref availableFixedList, ref selectedList, ref selectedDeductionList);

            lstIncomeList.DataSource = availableList;
            lstIncomeList.DataBind();

            lstFixedIncomeList.DataSource = availableFixedList;
            lstFixedIncomeList.DataBind();


            //lstSelectedIncomeList.DataSource = selectedList;
            //lstSelectedIncomeList.DataBind();

            lstDeductionList.DataSource = PayManager.GetDeductionListByCompany(SessionManager.CurrentCompanyId)
                .Where(x => x.Calculation == DeductionCalculation.PERCENT || x.Calculation==DeductionCalculation.VARIABLE || x.Calculation==DeductionCalculation.FIXED).ToList();
            lstDeductionList.DataBind();

            // skip fixed deduction like sst,tds,cit,pf income and deduction
            gvwSelectedIncomesIncomes.DataSource = selectedList.Where(
                x => x.Type != (int)CalculationColumnType.SST && x.Type != (int)CalculationColumnType.TDS &&
                    x.Type != (int)CalculationColumnType.DeductionCIT && x.Type != (int)CalculationColumnType.IncomePF
                    && x.Type != (int)CalculationColumnType.DeductionPF);

            gvwSelectedIncomesIncomes.DataBind();


            gvwDeductions.DataSource = selectedDeductionList;
            gvwDeductions.DataBind();


        }


        protected void gvwEmployees_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Remove")
            {
                // Retrieve the row index stored in the 
                // CommandArgument property.
                string typeSoruce = e.CommandArgument.ToString();
                string[] values = typeSoruce.Split(new char[] { ':' });
                int type = int.Parse(values[0]);
                int sourceId = int.Parse(values[1]);


                List<PIncome> incomes = new List<PIncome>();

                incomes.Add(new PIncome { Type = type, SourceId = sourceId });



                if (incomes.Count > 0)
                {
                    int addOnId = 0;
                    if (!string.IsNullOrEmpty(ddlAddOnName.SelectedValue))
                        addOnId = int.Parse(ddlAddOnName.SelectedValue);

                    Status status =  PartialAddOnManager.DeleteAddOnIncomeHead(addOnId,payrollPeriodId, incomes);


                    if (status.IsSuccess == false)
                    {
                        warMsgCtl.InnerHtml = status.ErrorMessage;
                        warMsgCtl.Hide = false;
                        return;
                    }

                    //Initialise();
                    msgInfo.InnerHtml = "Income removed.";
                    msgInfo.Hide = false;
                }

                List<CalcGetHeaderListResult> selectedList = new List<CalcGetHeaderListResult>();

                //PartialAddOnManager.SetIncomeList(payrollPeriodId, ref availableList, ref availableFixedList, ref selectedList);
                foreach (GridViewRow row in gvwSelectedIncomesIncomes.Rows)
                {
                    CalcGetHeaderListResult item = new CalcGetHeaderListResult();
                    DataKey data = gvwSelectedIncomesIncomes.DataKeys[row.RowIndex];
                    item.Type = (int)data["Type"];
                    item.SourceId = (int)data["SourceId"];
                    item.HeaderName = data["HeaderName"].ToString();
                    item.AddOnHeaderId = (int)data["AddOnHeaderId"];

                    var ddl = row.FindControl("ddlAddOnType") as DropDownList;
                    if (null != ddl)
                    {
                        if (ddl.SelectedIndex == 2)
                            item.AddToRegularSalary = null;
                        else
                            item.AddToRegularSalary = bool.Parse(ddl.SelectedValue);
                    }

                    ddl = row.FindControl("ddlSetting") as DropDownList;
                    if (null != ddl)
                    {
                        item.UseIncomeSetting = bool.Parse(ddl.SelectedValue);
                    }

                    if (item.TypeSourceId != (type + ":" + sourceId))
                        selectedList.Add(item);

                }

                gvwSelectedIncomesIncomes.DataSource = selectedList;
                gvwSelectedIncomesIncomes.DataBind();
                // Add code here to add the item to the shopping cart.
            }
            else
            {
                string typeSoruce = e.CommandArgument.ToString();
                string[] values = typeSoruce.Split(new char[] { ':' });
                int type = int.Parse(values[0]);
                int sourceId = int.Parse(values[1]);
                //int AddOnHeaderId = int.Parse(typeSoruce);

                int addOnId = 0;
                if (!string.IsNullOrEmpty(ddlAddOnName.SelectedValue))
                    addOnId = int.Parse(ddlAddOnName.SelectedValue);

                PartialAddOnManager.ClearAddOnValue(payrollPeriodId, addOnId, type,sourceId);

                msgInfo.InnerHtml = "Clear for the selected income.";
                msgInfo.Hide = false;
            }
        }


        protected void gvwDeductions_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Remove")
            {
                // Retrieve the row index stored in the 
                // CommandArgument property.
                string typeSoruce = e.CommandArgument.ToString();
                string[] values = typeSoruce.Split(new char[] { ':' });
                int type = int.Parse(values[0]);
                int sourceId = int.Parse(values[1]);


                List<PDeduction> incomes = new List<PDeduction>();

                incomes.Add(new PDeduction {DeductionId = sourceId });

                int addOnId = 0;
                if (!string.IsNullOrEmpty(ddlAddOnName.SelectedValue))
                    addOnId = int.Parse(ddlAddOnName.SelectedValue);

                if (incomes.Count > 0)
                {
                    PartialAddOnManager.DeleteAddOnDeductionHead(payrollPeriodId,addOnId, incomes);

                    //Initialise();
                    msgInfo.InnerHtml = "deduction removed.";
                    msgInfo.Hide = false;
                }


                Initialise();
               
                // Add code here to add the item to the shopping cart.
            }
            

        }

        protected void gvwEmployees_RowCreated(object sender, GridViewRowEventArgs e)
        {

            //if (e.Row.RowType == DataControlRowType.DataRow)
            //{
            //    e.Row.Attributes.Add("onmouseover",
            //    "this.originalClass=this.className;this.className='selected'");

            //    e.Row.Attributes.Add("onmouseout",
            //    "this.className=this.originalClass;");


            //}

        }

        protected void btnSaveIncomes_Click(object sender, EventArgs e)
        {

            List<CalcGetHeaderListResult> selectedList = new List<CalcGetHeaderListResult>();
            List<PIncome> incomes = new List<PIncome>();
            List<PDeduction> deductions = new List<PDeduction>();

            //PartialAddOnManager.SetIncomeList(payrollPeriodId, ref availableList, ref availableFixedList, ref selectedList);
            foreach (GridViewRow row in gvwSelectedIncomesIncomes.Rows)
            {
                PIncome item = new PIncome();
                DataKey data = gvwSelectedIncomesIncomes.DataKeys[row.RowIndex];
                item.Type = (int)data["Type"];
                item.SourceId = (int)data["SourceId"];
                item.Title = data["HeaderName"].ToString();
                item.AddOnHeaderId = (int)data["AddOnHeaderId"];

                DropDownList ddlSetting = row.FindControl("ddlSetting") as DropDownList;
                item.UseIncomeSettings = bool.Parse(ddlSetting.SelectedValue);

                DropDownList ddlType = row.FindControl("ddlAddOnType") as DropDownList;
                if (ddlType.SelectedIndex == 2)
                    item.AddToRegularSalary = null;
                else
                    item.AddToRegularSalary = bool.Parse(ddlType.SelectedValue);

                incomes.Add(item);
            }

            foreach (GridViewRow row in gvwDeductions.Rows)
            {
                PDeduction item = new PDeduction();
                DataKey data = gvwDeductions.DataKeys[row.RowIndex];
                
                item.DeductionId = (int)data["SourceId"];
                item.Title = data["HeaderName"].ToString();


                deductions.Add(item);
            }

            int addOnId = 0;
            if (!string.IsNullOrEmpty(ddlAddOnName.SelectedValue))
                addOnId = int.Parse(ddlAddOnName.SelectedValue);

            PartialAddOnManager.SaveAddOnIncome(payrollPeriodId, addOnId, incomes, deductions,false,employeeId);
           
            //if (employeeId != null)
            //    msgInfo.InnerHtml = string.Format("Incomes has been generated for {0} only.", EmployeeManager
            //        .GetEmployeeName(employeeId.Value));
            //else
                msgInfo.InnerHtml = "Incomes setting has been saved, for amount generation click \"Generate Amount\".";
            msgInfo.Hide = false;



        }

        protected void btnGenerateAmount_Click(object sender, EventArgs e)
        {
            int addOnId = 0;
            if (!string.IsNullOrEmpty(ddlAddOnName.SelectedValue))
                addOnId = int.Parse(ddlAddOnName.SelectedValue);

            AddOn addon = PayManager.GetAddOn(addOnId);

            if (addon.AddOnType != null && addon.AddOnType == (int)AddOnTypeEnum.LeaveFare)
            {
                warMsgCtl.InnerHtml = "Amount generation can not be done for Leave fare add on type.";
                warMsgCtl.Hide = false;
                return;
            }



            List<PIncome> incomes = new List<PIncome>();
            List<PDeduction> deductions = new List<PDeduction>();

            //PartialAddOnManager.SetIncomeList(payrollPeriodId, ref availableList, ref availableFixedList, ref selectedList);
            foreach (GridViewRow row in gvwSelectedIncomesIncomes.Rows)
            {
                PIncome item = new PIncome();
                DataKey data = gvwSelectedIncomesIncomes.DataKeys[row.RowIndex];
                item.Type = (int)data["Type"];
                item.SourceId = (int)data["SourceId"];
                item.Title = data["HeaderName"].ToString();
                item.AddOnHeaderId = (int)data["AddOnHeaderId"];

                DropDownList ddlSetting = row.FindControl("ddlSetting") as DropDownList;
                item.UseIncomeSettings = bool.Parse(ddlSetting.SelectedValue);

                DropDownList ddlType = row.FindControl("ddlAddOnType") as DropDownList;
                //item.AddToRegularSalary = bool.Parse(ddlType.SelectedValue);

                if (ddlType.SelectedIndex == 2)
                    item.AddToRegularSalary = null;
                else
                    item.AddToRegularSalary = bool.Parse(ddlType.SelectedValue);

                incomes.Add(item);
            }

            foreach (GridViewRow row in gvwDeductions.Rows)
            {
                PDeduction item = new PDeduction();
                DataKey data = gvwDeductions.DataKeys[row.RowIndex];

                item.DeductionId = (int)data["SourceId"];
                item.Title = data["HeaderName"].ToString();


                deductions.Add(item);
            }

            

            PartialAddOnManager.SaveAddOnIncome(payrollPeriodId, addOnId, incomes, deductions, false, employeeId);
           

            PartialAddOnManager.GenerateIncomeAmountForMultiAddOn(payrollPeriodId, addOnId, employeeId,incomes);

            if (employeeId != null)
                msgInfo.InnerHtml = string.Format("Incomes has been generated for {0} only.", EmployeeManager
                    .GetEmployeeName(employeeId.Value));
            else
                msgInfo.InnerHtml = "Incomes has been generated for all employees.";
            msgInfo.Hide = false;



        }
        protected void btnOk_Click(object sender, EventArgs e)
        {
            if (IsSettingEditable == false)
                return;

            if (Page.IsValid)
            {
                //btnSaveIncomes.Visible = true;

                List<CalcGetHeaderListResult> selectedList = new List<CalcGetHeaderListResult>();
                List<CalcGetHeaderListResult> deductions = new List<CalcGetHeaderListResult>();

                //PartialAddOnManager.SetIncomeList(payrollPeriodId, ref availableList, ref availableFixedList, ref selectedList);
                foreach (GridViewRow row in gvwSelectedIncomesIncomes.Rows)
                {
                    CalcGetHeaderListResult item = new CalcGetHeaderListResult();
                    DataKey data =  gvwSelectedIncomesIncomes.DataKeys[row.RowIndex];
                    item.Type = (int)data["Type"];
                    item.SourceId = (int)data["SourceId"];
                    item.HeaderName = data["HeaderName"].ToString();
                    item.AddOnHeaderId = (int)data["AddOnHeaderId"];

                    var ddl = row.FindControl("ddlAddOnType") as DropDownList;
                    if (null != ddl)
                    {
                        if (ddl.SelectedIndex == 2)
                            item.AddToRegularSalary = null;
                        else
                            item.AddToRegularSalary = bool.Parse(ddl.SelectedValue);

                        //item.AddToRegularSalary = bool.Parse(ddl.SelectedValue);
                    }

                    ddl = row.FindControl("ddlSetting") as DropDownList;
                    if (null != ddl)
                    {
                        item.UseIncomeSetting = bool.Parse(ddl.SelectedValue);
                    }

                    selectedList.Add(item);
                }

                foreach (GridViewRow row in gvwDeductions.Rows)
                {
                    CalcGetHeaderListResult item = new CalcGetHeaderListResult();
                    DataKey data = gvwDeductions.DataKeys[row.RowIndex];
                    item.Type = (int)data["Type"];
                    item.SourceId = (int)data["SourceId"];
                    item.HeaderName = data["HeaderName"].ToString();
                    item.AddOnHeaderId = (int)data["AddOnHeaderId"];
                    deductions.Add(item);
                }

                List<PIncome> incomes = new List<PIncome>();
                foreach (ListItem item in lstIncomeList.Items)
                {
                    if (item.Selected)
                    {

                        string[] values = item.Value.Split(new char[] { ':' });
                        int type = int.Parse(values[0]);
                        int souriceId = int.Parse(values[1]);

                        if (selectedList.Any(x => x.Type == type && x.SourceId == souriceId) == false)
                        {


                            incomes.Add(new PIncome { Title = item.Text, Type = int.Parse(values[0]), SourceId = int.Parse(values[1]) });

                            bool? zeroizeAllEmpSalary = null;

                            if (type == 1)
                            {
                                PIncome income = PayManager.GetIncome(souriceId);
                                if (income.Calculation == IncomeCalculation.FIXED_AMOUNT || income.Calculation== IncomeCalculation.DEEMED_INCOME
                                    || income.Calculation == IncomeCalculation.PERCENT_OF_INCOME)
                                    zeroizeAllEmpSalary = true;
                            }
                            selectedList.Add(new CalcGetHeaderListResult
                            {
                                AddOnHeaderId = 0,
                                Type = int.Parse(values[0]),
                                SourceId = int.Parse(values[1]),
                                HeaderName = item.Text,
                                AddToRegularSalary = zeroizeAllEmpSalary
                            });
                        }
                    }
                }

                
                foreach (ListItem item in lstDeductionList.Items)
                {
                    if (item.Selected)
                    {

                        int deductionId = int.Parse(item.Value);

                        if (deductions.Any(x => x.Type == (int)CalculationColumnType.Deduction
                            && x.SourceId == deductionId) == false)
                        {


                            deductions.Add(new CalcGetHeaderListResult
                            {
                                AddOnHeaderId = 0,
                                Type = (int)CalculationColumnType.Deduction,
                                SourceId = deductionId,
                                HeaderName = item.Text
                            });
                        }
                    }
                }

                List<PIncome> fixedIncomes = new List<PIncome>();
                foreach (ListItem item in lstFixedIncomeList.Items)
                {
                    if (item.Selected)
                    {
                        string[] values = item.Value.Split(new char[] { ':' });
                        int type = int.Parse(values[0]);
                        int souriceId = int.Parse(values[1]);

                        if (selectedList.Any(x => x.Type == type && x.SourceId == souriceId) == false)
                        {
                            fixedIncomes.Add(new PIncome { Title = item.Text, Type = int.Parse(values[0]), SourceId = int.Parse(values[1]) });

                            selectedList.Add(new CalcGetHeaderListResult
                            {
                                AddOnHeaderId = 0,
                                Type = int.Parse(values[0]),
                                SourceId = int.Parse(values[1]),
                                HeaderName = item.Text
                            });
                        }
                    }
                }


                


                if (incomes.Count > 0 || fixedIncomes.Count > 0)
                {
                    //PartialAddOnManager.SaveIncome(payrollPeriodId, incomes, fixedIncomes);

                    //Initialise();
                    //msgInfo.InnerHtml = "Income(s) added.";
                    //msgInfo.Hide = false;
                    gvwSelectedIncomesIncomes.DataSource = selectedList;
                    gvwSelectedIncomesIncomes.DataBind();

                }

                if (deductions.Count > 0)
                {
                    gvwDeductions.DataSource = deductions;
                    gvwDeductions.DataBind();
                }

            }
        }

        protected void gvwSelectedIncomesIncomes_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // Find the drop-down (say in 3rd column)
                var ddl = e.Row.FindControl("ddlAddOnType") as DropDownList;
                CalcGetHeaderListResult data = e.Row.DataItem as CalcGetHeaderListResult;
                if (null != ddl)
                {
                    if (data.AddToRegularSalary == null)
                        ddl.SelectedIndex = 2;
                    else
                        ddl.SelectedValue = data.AddToRegularSalary.ToString().ToLower();
                    // bind it
                }

                ddl = e.Row.FindControl("ddlSetting") as DropDownList;
                if (null != ddl)
                {
                    ddl.SelectedValue = data.UseIncomeSetting.ToString().ToLower();
                    // bind it
                }
                /*
                // In case of template fields, use FindControl
                dd = e.Row.Cells[2].FindControl("MyDD") as DropDownList;
                */
            }
        }
    }
}
