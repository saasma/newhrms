﻿<%@ Page Title="Service Period List" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="ServicePeriodList.aspx.cs" Inherits="Web.CP.ServicePeriodList" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function medicalTaxCreditPopupCall(medicalTaxId) {
            var ret = medicalTaxCreditPopup("Id=" + medicalTaxId);
            if (ret == "Yes")
                __doPostBack('Reload', '');
        }

        function parentReloadCallbackFunction(childWindow, ret) {
            childWindow.close();
            if (ret == "Yes")
                __doPostBack('Reload', '');

        }

        function refreshWindow() {
            __doPostBack('Reload', '');
        }
    </script>


    <style type="text/css">

        .tableLightColor .odd td {
            background: none repeat scroll 0 0 #fafcff;
            border-bottom: 1px solid #DFECFD;
            border-right: 0px solid #DFECFD;
            border-top: 1px solid #DFECFD;
            border-left: 0px solid #DFECFD;
            padding: 0px 0px 0px 0px;
            font: plain;
            height: 23px;
        }
        
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>Service Period Report
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <%--to generate __PostBack function--%>
        <asp:LinkButton runat="server" Visible="false"></asp:LinkButton>
        <div class="attribute" style="padding: 10px">
            <table>
                <tr>
                    <td>Status
                        <%--  <strong>Search </strong>&nbsp;
                        <asp:TextBox Width="160px" ID="txtEmpSearchText" runat="server" OnTextChanged="txtEmpSearchText_TextChanged"></asp:TextBox>--%>
                        <asp:DropDownList ID="ddlJobStatus" runat="server" Width="165px" CssClass="rightMargin"
                            DataValueField="Key" DataTextField="Value">
                        </asp:DropDownList>
                    </td>
                    <td>&nbsp;Date
                       
                       <My:Calendar ID="calDate" runat="server" />
                        <%-- <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>--%>
                        <%-- <cc1:CalendarExtender ID="Calendar1" PopupButtonID="imgPopup" runat="server" TargetControlID="txtDate">
                        </cc1:CalendarExtender>--%>
                        <%--<asp:RequiredFieldValidator ValidationGroup="Name1" ID="RequiredFieldValidator3"
                            Display="None" runat="server" ErrorMessage="Date is Required." ControlToValidate="calDate">*</asp:RequiredFieldValidator>--%>
                        <%--<asp:CompareValidator ID="dateValidator" runat="server" Type="Date" Operator="DataTypeCheck"
                            ValidationGroup="Name1" ControlToValidate="txtDate" ErrorMessage="Please enter a valid date.">
                        </asp:CompareValidator>--%>
                    </td>
                    <td>
                        <asp:Button ID="btnLoad" CssClass="btn btn-default btn-sm" Style="margin-left: 10px; width: 100px;" runat="server"
                            OnClick="btnLoad_Click" Text="Load" OnClientClick="valGroup='Name1';return CheckValidation()" />
                        <%--  <cc1:TextBoxWatermarkExtender ID="TBWE2" runat="server" TargetControlID="txtEmpSearchText"
                            WatermarkText="Employee name" WatermarkCssClass="searchBoxText" />--%>
                    </td>
                    <td>
                        <asp:LinkButton ID="btnExport" runat="server" Text="Export" OnClick="btnExport_Click"
                            CssClass=" excel marginRight tiptip" Style="float: left; width: 90px!important" />
                    </td>
                </tr>
            </table>
        </div>
        <br />
        <div class="separator clear">
            <cc2:EmptyDisplayGridView CssClass="tableLightColor" UseAccessibleHeader="true" ID="gvwList"
                runat="server" AutoGenerateColumns="False" DataKeyNames="EmployeeId" GridLines="None"
                ShowFooterWhenEmpty="False" ShowHeaderWhenEmpty="True" Width="100%">
                <Columns>
                    <asp:BoundField HeaderStyle-HorizontalAlign="Left" DataField="EmployeeId" HeaderText="EIN" ItemStyle-HorizontalAlign="Center"
                        HeaderStyle-Width="40px" />
                    <asp:BoundField HeaderStyle-Width="200px" HeaderStyle-HorizontalAlign="Left" DataField="Name" ItemStyle-HorizontalAlign="Left"
                        HeaderText="Name" />
                    <asp:BoundField HeaderStyle-Width="200px" HeaderStyle-HorizontalAlign="Left" DataField="Designation" ItemStyle-HorizontalAlign="Left"
                        HeaderText="Post" />
                    <asp:BoundField HeaderStyle-Width="150px" HeaderStyle-HorizontalAlign="Left" DataField="Level" ItemStyle-HorizontalAlign="Left"
                        HeaderText="Level" />
                    <asp:BoundField HeaderStyle-Width="150px" HeaderStyle-HorizontalAlign="Left" DataField="Branch" ItemStyle-HorizontalAlign="Left"
                        HeaderText="Branch" />
                    <asp:BoundField HeaderStyle-Width="150px" HeaderStyle-HorizontalAlign="Left" DataField="Department" ItemStyle-HorizontalAlign="Left"
                        HeaderText="Department" />
                    <asp:BoundField HeaderStyle-Width="150px" HeaderStyle-HorizontalAlign="Left" DataField="JoinDateEng" ItemStyle-HorizontalAlign="Left"
                        DataFormatString="{0:dd-MMM-yyyy}" HeaderText="Join Date(A.D)" />
                    <asp:BoundField HeaderStyle-Width="150px" HeaderStyle-HorizontalAlign="Left" DataField="JoinDateNepali" ItemStyle-HorizontalAlign="Left"
                        HeaderText="Join Date(B.S)" />
                    <asp:BoundField HeaderStyle-Width="200px" HeaderStyle-HorizontalAlign="Left" DataField="ServicePeriod" ItemStyle-HorizontalAlign="Center"
                        HeaderText="Service Period" />
                    <asp:BoundField HeaderStyle-Width="150px" HeaderStyle-HorizontalAlign="Left" DataField="Status" ItemStyle-HorizontalAlign="Left"
                        HeaderText="Status" />
                </Columns>
                <%--                <PagerSettings Mode="NumericFirstLast" NextPageText="Next »" PreviousPageText="« Prev"
                    FirstPageText="First" LastPageText="Last" />--%>
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="odd" />
                <EmptyDataTemplate>
                    No records.
                </EmptyDataTemplate>
            </cc2:EmptyDisplayGridView>
        </div>
    </div>
</asp:Content>
