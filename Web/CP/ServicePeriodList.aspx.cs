﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using System.Text;
using BLL.Manager;
using Utils.Helper;
using DAL;
using System.Drawing;
using BLL.Base;
using BLL.BO;
using Bll;
using Web.Helper;


namespace Web.CP
{
    public partial class ServicePeriodList : BasePage
    {
       
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                Initialise();
            }

            if (Request.Form["__EVENTTARGET"] != null && Request.Form["__EVENTTARGET"].Equals("Reload"))
                LoadData();
       
        }

        void Initialise()
        {
            List<KeyValue> statusList = new JobStatus().GetMembers();
            ddlJobStatus.DataSource = statusList;
            ddlJobStatus.DataBind();

            calDate.SelectTodayDate();
        }

        void LoadData()
        {
            DateTime date = new DateTime(calDate.SelectedDate.Year, calDate.SelectedDateEng.Month, calDate.SelectedDateEng.Day, 0, 0, 0);
            List<EmployeeServiceBo> _list = EmployeeManager.GetEmployeeServicePeriodByJobStatus(int.Parse(ddlJobStatus.SelectedItem.Value), date);
            gvwList.DataSource = _list;
            gvwList.DataBind();

            if (IsEnglish)
            {
                gvwList.Columns[7].Visible = false;
            }
        }

        protected void btnLoad_Click(object sender, EventArgs e)
        {
            LoadData();
        }

        public void btnExport_Click(object sender, EventArgs e)
        {
            DateTime date = new DateTime(calDate.SelectedDate.Year, calDate.SelectedDateEng.Month, calDate.SelectedDateEng.Day, 0, 0, 0);
            List<EmployeeServiceBo> _ListResult = EmployeeManager.GetEmployeeServicePeriodByJobStatus(int.Parse(ddlJobStatus.SelectedItem.Value), date);

            foreach (EmployeeServiceBo item in _ListResult)
                item.JoinDateEngText = WebHelper.FormatDateForExcel(item.JoinDateEng);

            List<string> _hideColumnList = new List<string>();
            string[] _hideColumn = { "DepartmentID", "BranchID", "DesignationID","JoinDateEng","IdCardNo","RetirementType" };
            _hideColumnList.AddRange(_hideColumn.ToList());
            if (IsEnglish)
                _hideColumnList.Add("JoinDateNepali");

            ExcelHelper.ExportToExcel("Employee Service Period Report", _ListResult,
            _hideColumnList,
            new List<String>() { },
            new Dictionary<string, string>() { { "EmployeeId", "EIN" }, { "JoinDateEngText", "Join Date(A.D)" }, { "JoinDateEng", "Join Date(A.D)" }, { "JoinDateNepali", "Join Date(B.S)" }, { "ServicePeriod", "Service Period" } },
            new List<string>() { }
            , new List<string> { }
            , new List<string> {"JoinDateEngText" }
            , new Dictionary<string, string>() { {"Service Period List",""} }
            , new List<string> { "EmployeeId", "Name", "Designation", "Level", "Branch", "Department", "JoinDateEngText", "JoinDateNepali", "ServicePeriod" });
        }
            

    }
}
