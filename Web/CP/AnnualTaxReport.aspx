<%@ Page Title="Year End Tax Report" Language="C#" MasterPageFile="~/Master/Details.Master"
    AutoEventWireup="true" CodeBehind="AnnualTaxReport.aspx.cs" Inherits="Web.AnnualTaxReport" %>

<%@ Import Namespace="BLL.Manager" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Scripts/ScrollTableHeader103.min.js?v=200" type="text/javascript"></script>
    <script type="text/javascript">

        var skipLoadingCheck = true;

        $(document).ready(
            function () {

                $('.tableLightColor tbody').attr('id', 'scrollMe');
                var sth = new ScrollTableHeader();

                sth.addTbody("scrollMe");
                sth.delayAfterScroll = 150;
                sth.minTableRows = 10;
            }
        );

        function ACE_item_selected(source, eventArgs) {
            var value = eventArgs.get_value();
            document.getElementById('<%= hiddenEmployeeID.ClientID %>').value = value;
        }

    </script>
    <style type="text/css">
        .tableLightColor a:hover
        {
            color: #048FC2;
            text-decoration: non;
        }
        .container-fluid {
    padding-right: 0px;
    padding-left: 0px;
    
}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <div class="contentArea">
        
    <h3>
        Annual Tax Report</h3>
    <div class="attribute" style='padding:10px;'>
        <table>
            <tr>
                <td>
                    <asp:DropDownList ID="ddlYear" AutoPostBack="true" runat="server" AppendDataBoundItems="true"
                        Width="200px" DataValueField="FinancialDateId" DataTextField="Name">
                        <asp:ListItem Text="--Select--" Value="-1" />
                    </asp:DropDownList>
                </td>
                <td style='padding-left:20px;'>
                    <asp:TextBox ID="txtEmpSearch" runat="server"  Width="160" />
                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtEmpSearch"
                        WatermarkText="Search Employee" WatermarkCssClass="searchBoxText" />
                    <cc1:AutoCompleteExtender EnableCaching="true" ID="AutoCompleteExtenderOrganization"
                        runat="server" MinimumPrefixLength="2" ServiceMethod="GetEmployeeNamesAndEINWithRetiredAlso"
                        ServicePath="~/PayrollService.asmx" TargetControlID="txtEmpSearch" CompletionSetCount="10"
                        CompletionInterval="250" OnClientItemSelected="ACE_item_selected" CompletionListCssClass="AutoExtender"
                        CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
                    </cc1:AutoCompleteExtender>
                </td>
                 <td style='padding-left:20px;'>
                    <asp:Button ID="btnLoad" CssClass="save" runat="server" Text="Load" OnClick="btnLoad_Click" />
                </td>
                <td style="padding-left: 500px;">
                    <asp:Button ID="btnUpdate" CssClass="excel" runat="server" Text="Export" OnClick="btnUpdate_Click" />
                </td>
            </tr>
        </table>
        <asp:HiddenField ID="hiddenEmployeeID" runat="server" />
    </div>
    <div class="clear">
        <cc2:EmptyDisplayGridView  CssClass="table table-primary mb30 table-bordered table-hover" UseAccessibleHeader="true" ShowHeaderWhenEmpty="True"
            ID="gvEmployeeIncome" runat="server" AutoGenerateColumns="False"
            GridLines="None" AllowSorting="True" ShowFooterWhenEmpty="False">
            
            <Columns>
               
                <asp:TemplateField HeaderText="Name"  HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <asp:Label runat="server" Width="150px" Text='<%# Eval("Name") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Annual Amount" ItemStyle-HorizontalAlign="Right">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server"  style='width:90px;text-align:right' Text='<%# GetCurrency(Eval("Total")) %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="None Cash" ItemStyle-HorizontalAlign="Right">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server"  style='width:90px;text-align:right' Text='<%# GetCurrency(Eval("OneTime")) %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Sharwan" ItemStyle-HorizontalAlign="Right">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server"  style='width:90px;text-align:right' Text='<%# GetCurrency(Eval("M4Amount")) %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Addon" ItemStyle-HorizontalAlign="Right">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server"  style='width:90px;text-align:right'   Text='<%# GetCurrency(Eval("M4AddOn")) %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Bhadra" ItemStyle-HorizontalAlign="Right">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server"   style='width:90px;text-align:right'  Text='<%# GetCurrency(Eval("M5Amount")) %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Addon" ItemStyle-HorizontalAlign="Right">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server"   style='width:90px;text-align:right'  Text='<%# GetCurrency(Eval("M5AddOn")) %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Ashwin" ItemStyle-HorizontalAlign="Right">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server"  style='width:90px;text-align:right'  Text='<%# GetCurrency(Eval("M6Amount")) %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Addon" ItemStyle-HorizontalAlign="Right">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# GetCurrency(Eval("M6AddOn")) %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Kartik" ItemStyle-HorizontalAlign="Right">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# GetCurrency(Eval("M7Amount")) %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Addon" ItemStyle-HorizontalAlign="Right">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# GetCurrency(Eval("M7AddOn")) %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Mangsir" ItemStyle-HorizontalAlign="Right">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# GetCurrency(Eval("M8Amount")) %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Addon" ItemStyle-HorizontalAlign="Right">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# GetCurrency(Eval("M8AddOn")) %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Poush" ItemStyle-HorizontalAlign="Right">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# GetCurrency(Eval("M9Amount")) %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Addon" ItemStyle-HorizontalAlign="Right">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%#GetCurrency( Eval("M9AddOn")) %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Magh" ItemStyle-HorizontalAlign="Right">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%#GetCurrency( Eval("M10Amount")) %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Addon" ItemStyle-HorizontalAlign="Right">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# GetCurrency(Eval("M10AddOn")) %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Falgun" ItemStyle-HorizontalAlign="Right">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# GetCurrency(Eval("M11Amount")) %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Addon" ItemStyle-HorizontalAlign="Right">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# GetCurrency(Eval("M11AddOn")) %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Chaitra" ItemStyle-HorizontalAlign="Right">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# GetCurrency(Eval("M12Amount")) %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Addon" ItemStyle-HorizontalAlign="Right">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# GetCurrency(Eval("M12AddOn")) %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Baisakh" ItemStyle-HorizontalAlign="Right">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# GetCurrency(Eval("M1Amount")) %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Addon" ItemStyle-HorizontalAlign="Right">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# GetCurrency(Eval("M1AddOn")) %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Jestha" ItemStyle-HorizontalAlign="Right">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# GetCurrency(Eval("M2Amount")) %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Addon" ItemStyle-HorizontalAlign="Right">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# GetCurrency(Eval("M2AddOn")) %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Ashadh" ItemStyle-HorizontalAlign="Right">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# GetCurrency(Eval("M3Amount")) %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Addon" ItemStyle-HorizontalAlign="Right">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%#GetCurrency( Eval("M3AddOn")) %>' />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <RowStyle CssClass="odd" />
            <HeaderStyle Font-Bold="true" />
            <AlternatingRowStyle CssClass="even" />
            <EmptyDataTemplate>
                <b>No list. </b>
            </EmptyDataTemplate>
        </cc2:EmptyDisplayGridView>
    </div>
    </div>
</asp:Content>
