﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/ForPopupPage.Master" AutoEventWireup="true" CodeBehind="ChangeEmployeeProbation.aspx.cs" Inherits="Web.CP.ChangeEmployeeProbation" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        function closePopup() {
            clearUnload();
            //if ($.browser.msie == false && typeof (window.opener.loadAndHide) != 'undefined' && ($.browser.chrome || $.browser.safari)) {
            window.opener.loadAndHideProbation(window);
            //            } else {
            //                
            //                window.close();
            //            }
        }

        function clearUnload() {
            window.onunload = null;
        }







        function validateStatusToDate(source, args) {

            return true;



        }
    </script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">
    <div class="popupHeader">
        <h2 class="headlinespop">
            Change Employee Probation
        </h2>
    </div>
    <div class="bevel marginal">
        <div class="fields paddpop">
            <table class="removePadding" runat="server" id="tbl" width="480px" cellpadding="0"
                cellspacing="0">
                <tr>
                    <td style="height: 10px">
                        Letter No
                    </td>
                </tr>
                <tr>
                    <td style="padding-top: 10px; padding-bottom:10px;" colspan="2">
                        <asp:TextBox  ID="txtLetterNo" runat="server" />
                        <asp:RequiredFieldValidator ID="rfvLetterNo" ErrorMessage="Please type letter no."
                                Display="None" ControlToValidate="txtLetterNo" ValidationGroup="AEStatus" runat="server">
                            </asp:RequiredFieldValidator>
                    </td>
                </tr>

                <tr>
                    <td style="height: 10px">
                        Probation End Date
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:HiddenField ID="hdnFromDate" runat="server" />
                   
                        <My:Calendar Id="calTo" runat="server"  />
                    </td>
                </tr>
                <tr>
                    <td style="height: 10px">
                        Notes
                    </td>
                </tr>
                <tr>
                    <td style="padding-top: 15px" colspan="2">
                        <asp:TextBox TextMode="MultiLine" ID="txtNote" runat="server" Width="415" Height="50" />
                    </td>
                </tr>
            </table>
            <br />
            <asp:Button ID="btnSaveStatus" OnClick="btnSaveStatus_Click" runat="server" OnClientClick="valGroup='AEStatus';if( CheckValidation() && confirm('Confirm save status for the employee?') )  {} else return false;"
                Text="Save" class="update" />
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
