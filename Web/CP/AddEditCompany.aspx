<%@ Page Title="Company details" Language="C#" MaintainScrollPositionOnPostback="true"
    ValidateRequest="false" EnableEventValidation="false" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="AddEditCompany.aspx.cs" Inherits="Web.CP.AddEditCompany" %>

<%@ Register Src="~/UserControls/MGFunds.ascx" TagName="MGFunds" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .companytabl tr td {
            padding-bottom: 12px !important;
        }

        table tr.seperatoring td {
            padding-bottom: 0px;
            background: url("images/seperators.gif") repeat-x bottom;
        }

        table tr.up td {
            padding-top: 10px !important;
        }
    </style>



</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">

    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>Company Information
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="contentArea">
            <%-- <h2>
            Company Information</h2>--%>
            <asp:HiddenField ID="hdEditSequence" runat="server" />
            <fieldset class="mainFieldset1" style='background: #E7F1FD; padding-left: 30px'>

                <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Width="880px!important" Hide="true"
                    runat="server" />
                <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Width="880px!important"
                    Hide="true" runat="server" />
                <div class="col" style="width: 350px; margin: 15px">
                    <table cellpadding="0" cellspacing="0" class="companytabl" width="380px">
                        <tr>
                            <td class="fieldHeader" width="140px">
                                <My:Label ID="Label1" Text="Company Name" runat="server" ShowAstrick="true" />
                            </td>
                            <td>
                                <asp:TextBox ID="txtCompanyName" runat="server" Width="180px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="valReqdName" runat="server" ControlToValidate="txtCompanyName"
                                    Display="None" ErrorMessage="Company name is required." ValidationGroup="AECompany"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="fieldHeader">
                                <My:Label ID="Label2" Text="Contact Person" runat="server" ShowAstrick="false" />
                            </td>
                            <td>
                                <asp:TextBox ID="txtResponsiblePerson" runat="server" Width="180px"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="valReqdResPerson" runat="server" ControlToValidate="txtResponsiblePerson"
                        Display="None" ErrorMessage="Responsible persion. is required" ValidationGroup="AECompany"></asp:RequiredFieldValidator>--%>
                            </td>
                        </tr>
                        <tr>
                            <td class="fieldHeader">
                                <My:Label Text="Address" runat="server" ShowAstrick="false" />
                            </td>
                            <td>
                                <asp:TextBox ID="txtAddress" TextMode="MultiLine" Style='width: 180px; height: 50px'
                                    runat="server"></asp:TextBox>
                                <%-- <asp:RequiredFieldValidator ID="valReqdAddress" runat="server" ControlToValidate="txtAddress"
                            Display="None" ErrorMessage="Address is required." ValidationGroup="AECompany"></asp:RequiredFieldValidator>--%>
                            </td>
                        </tr>
                        <tr>
                            <td class="fieldHeader">Phone
                            </td>
                            <td>
                                <asp:TextBox ID="txtPhone" runat="server" Width="180px"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="valRegPhone" runat="server" ControlToValidate="txtPhone"
                                    ErrorMessage="Phone number is invalid." ValidationGroup="AECompany" Display="None"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr runat="server" visible="false">
                            <td class="fieldHeader">Fax
                            </td>
                            <td>
                                <asp:TextBox ID="txtFax" runat="server" Width="180px"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="valRegFax" runat="server" ControlToValidate="txtFax"
                                    ErrorMessage="Fax number is invalid." ValidationGroup="AECompany" Display="None"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="fieldHeader">Email
                            </td>
                            <td>
                                <asp:TextBox ID="txtEmail" runat="server" Width="180px"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="valRegEmail" runat="server" ControlToValidate="txtEmail"
                                    ErrorMessage="Email is invalid." ValidationGroup="AECompany" Display="None" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="fieldHeader">Website
                            </td>
                            <td>
                                <asp:TextBox ID="txtWebsite" runat="server" Width="180px"></asp:TextBox>
                                <%--<asp:RegularExpressionValidator ID="valRegWebsite" runat="server" ControlToValidate="txtWebsite"
                            ErrorMessage="Website is invalid." ValidationGroup="AECompany" Display="None"
                            ValidationExpression="//([\w-]+\.)+[\w-]+(/[\w- ./?%&amp;=]*)?"></asp:RegularExpressionValidator>--%>
                            </td>
                        </tr>
                        <tr class="seperatoring">
                            <td class="fieldHeader">Registration No
                            </td>
                            <td>
                                <asp:TextBox ID="txtRegNo" runat="server" Width="180px"></asp:TextBox>
                                <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtRegNo"
                Display="None" ErrorMessage="Company registration no required." ValidationGroup="AECompany"></asp:RequiredFieldValidator>--%>
                            </td>
                        </tr>
                        <tr class="up seperatoring">
                            <td class="fieldHeader">Net Salary Round Off
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlNetSalaryRoundOff" Width="183px" AppendDataBoundItems="true"
                                    runat="server">
                                    <asp:ListItem Text="" Value="-1"></asp:ListItem>
                                </asp:DropDownList>
                                <%-- <asp:RequiredFieldValidator InitialValue="-1" ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlNetSalaryRoundOff"
                Display="None" ErrorMessage="Net salary round off is required." ValidationGroup="AECompany"></asp:RequiredFieldValidator>--%>
                            </td>
                        </tr>
                        <tr class="up">
                            <td class="fieldHeader">Date system
                            </td>
                            <td>
                                <asp:RadioButtonList ID="rdbListDateSystem" AutoPostBack="true" runat="server" RepeatLayout="Flow"
                                    RepeatDirection="Horizontal" OnSelectedIndexChanged="rdbListDateSystem_SelectedIndexChanged">
                                    <asp:ListItem Value="1" Selected="True">English</asp:ListItem>
                                    <asp:ListItem Value="0">Nepali</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                    </table>
                    <%-- <div class="sectionsub clear">--%>
                    <h3>Financial Year</h3>
                    <table cellpadding="0" cellspacing="0" class="companytabl clear" width="374px">
                        <tr>
                            <td class="fieldHeader" width="140px">
                                <My:Label ID="Label3" Text="Starting Date" runat="server" ShowAstrick="true" />
                            </td>
                            <td>
                                <My:Calendar ID="calStartingDate" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="fieldHeader">
                                <My:Label ID="Label5" Text="Ending Date" runat="server" ShowAstrick="true" />
                            </td>
                            <td>
                                <My:Calendar ID="calEndingDate" runat="server" />
                            </td>
                        </tr>
                        <tr class="seperatoring">
                            <td class="fieldHeader">
                                <My:Label ID="Label4" Text="Leave start month" runat="server" ShowAstrick="true" />
                                <%--<My:Label ID="Label5" Text="IRO name:" runat="server" ShowAstrick="true" /> --%>
                            </td>
                            <td>
                                <asp:DropDownList AppendDataBoundItems="true" Width="183px" ID="ddlLeaveStartMonth"
                                    runat="server" DataValueField="Key" DataTextField="Value">
                                    <asp:ListItem Text="" Value="-1"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlLeaveStartMonth"
                                    InitialValue="-1" Display="None" ErrorMessage="Please select leave start month."
                                    ValidationGroup="AECompany"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr class="up" runat="server" visible="false">
                            <td class="fieldHeader">
                                <My:Label ID="Label6" Text="Company Type" runat="server" ShowAstrick="true" />
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlCompanyType" Width="183px" runat="server" AppendDataBoundItems="true">
                                    <asp:ListItem Text="" Value="-1"></asp:ListItem>
                                </asp:DropDownList>
                                <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator0" runat="server" ControlToValidate="ddlCompanyType"
                            InitialValue="-1" Display="None" ErrorMessage="Please select a company type."
                            ValidationGroup="AECompany"></asp:RequiredFieldValidator>--%>
                            </td>
                        </tr>
                        <tr class="up">
                            <td class="fieldHeader">
                                <My:Label ID="Label7" Text="PAN No" runat="server" ShowAstrick="false" />
                            </td>
                            <td>
                                <asp:TextBox ID="txtPanNo" runat="server" Width="180px" />
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtPanNo"
                            Display="None" ErrorMessage="PAN no is required." ValidationGroup="AECompany"></asp:RequiredFieldValidator>--%>
                                <asp:RegularExpressionValidator ID="valRegExPanNo" runat="server" ControlToValidate="txtPanNo"
                                    Display="None" ValidationExpression="" ErrorMessage="Invalid Pan No, must be 9 digit value."
                                    ValidationGroup="AECompany"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr class="up">
                            <td class="fieldHeader">
                                <My:Label ID="lbl234324" Text="IRD Name" runat="server" ShowAstrick="false" />
                                <%--<My:Label ID="Label5" Text="IRO name:" runat="server" ShowAstrick="true" /> --%>
                            </td>
                            <td>
                                <asp:DropDownList onchange="onChangeIRO(this)" AppendDataBoundItems="true" ID="ddlIRO"
                                    Width="183px" runat="server" DataValueField="IROId" DataTextField="Name">
                                    <asp:ListItem Text="" Value="-1"></asp:ListItem>
                                </asp:DropDownList>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="ddlIRO"
                            InitialValue="-1" Display="None" ErrorMessage="Please select IRO." ValidationGroup="AECompany"></asp:RequiredFieldValidator>--%>
                            </td>
                        </tr>
                        <tr class="up">
                            <td class="fieldHeader">
                                <My:Label ID="Label10" Text="TSO" runat="server" ShowAstrick="false" />
                                <%--<My:Label ID="Label5" Text="IRO name:" runat="server" ShowAstrick="true" /> --%>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlTSO" Width="183px" runat="server" DataValueField="TSOId"
                                    DataTextField="Name">
                                </asp:DropDownList>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="ddlIRO"
                            InitialValue="-1" Display="None" ErrorMessage="Please select IRO." ValidationGroup="AECompany"></asp:RequiredFieldValidator>--%>
                            </td>
                        </tr>
                        <tr class="seperatoring">
                            <td class="fieldHeader"></td>
                            <td>
                                <asp:CheckBox ID="chkHasPFRFFund" onclick="onCheckChange(this,fieldSetPFRF)" Checked="true"
                                    runat="server" Text="Retirement Fund" />
                            </td>
                        </tr>
                        <tr class="up" runat="server" id="rowPF1">
                            <td class="fieldHeader">
                                <My:Label ID="Label8" Text="Fund Name" runat="server" ShowAstrick="true" />
                            </td>
                            <td>
                                <asp:TextBox ID="txtPFRFName" runat="server" Width="180px"></asp:TextBox>
                                <asp:CustomValidator Display="None" ValidateEmptyText="true" ClientValidationFunction="ValidateTextBox1"
                                    ControlToValidate="txtPFRFName" ValidationGroup="AECompany" ID="valCustomRFAC"
                                    runat="server" ErrorMessage="Fund name is required." OnServerValidate="valCustomRFAC_ServerValidate"></asp:CustomValidator>
                            </td>
                        </tr>
                        <tr runat="server" id="rowPF2">
                            <td class="fieldHeader">
                                <My:Label ID="Label9" Text="Short Name" runat="server" ShowAstrick="true" />
                            </td>
                            <td>
                                <asp:TextBox ID="txtPFRFAbbr" runat="server" Width="180px"></asp:TextBox>
                                <asp:CustomValidator Display="None" ValidateEmptyText="true" ClientValidationFunction="ValidateTextBox1"
                                    ControlToValidate="txtPFRFAbbr" ValidationGroup="AECompany" ID="CustomValidator1"
                                    runat="server" ErrorMessage="Fund short name is required." OnServerValidate="CustomValidator1_ServerValidate"></asp:CustomValidator>
                            </td>
                        </tr>
                        <tr runat="server" id="Tr1">
                            <td class="fieldHeader">
                                <asp:CheckBox ID="chkOtherRFFund"
                                    runat="server" />
                                <My:Label ID="Label17" Text="Other Fund Name" runat="server" ShowAstrick="false" />
                            </td>
                            <td>
                                <asp:TextBox ID="txtOtherPFRFAbbr" runat="server" Width="180px"></asp:TextBox>
                                <%--  <asp:RequiredFieldValidator Display="None" ValidateEmptyText="true" 
                                ControlToValidate="txtOtherPFRFAbbr" ValidationGroup="AECompany" ID="CustomValidator2"
                                runat="server" ErrorMessage="Other Fund name is required." ></asp:RequiredFieldValidator>--%>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <asp:Button ID="btnSave" CssClass="save" OnClientClick="return validateCP()" runat="server"
                                    Text="Save" ValidationGroup="AECompany" OnClick="btnSave_Click" />
                                <asp:Button ID="btnCancel" CssClass="cancel" runat="server" OnClientClick="window.location= 'ManageCompany.aspx';return false;"
                                    Text="Cancel" />
                            </td>
                        </tr>
                    </table>
                    <%-- </div>--%>
                </div>
                <div class="col" style="width: 350px">
                    <uc1:MGFunds ID="ucFunds" runat="server" />

                    <div runat="server" id="divSalarySetting">
                        <h3 style='padding-left: 0px; border-bottom: 1px solid #10b0ea;'>Salary/Tax Settings</h3>
                        <table style="margin-top:10px"  class="companytabl">
                            <tr>
                                <td class="fieldHeader">Remote area using proportionate days
                                </td>
                                <td class="fieldHeader">
                                    <asp:DropDownList ID="ddlRemoteArea" Width="183px" runat="server" >
                                        <asp:ListItem Text="-Select option-" Value="-1" />
                                         <asp:ListItem Text="Yes" Value="true" />
                                        <asp:ListItem Text="No" Value="false" />
                                    </asp:DropDownList>
                                      <asp:RequiredFieldValidator ID="req100" runat="server" InitialValue="-1" ControlToValidate="ddlRemoteArea"
                                    Display="None" ErrorMessage="Remote area setting is required." ValidationGroup="AECompany"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="fieldHeader">Salary calculaton workdays method
                                </td>
                                <td class="fieldHeader">
                                    <asp:DropDownList ID="ddlSalaryWorkdaySetting" onchange="if(this.value=='3') document.getElementById('<%= rowWorkday.ClientID %>').style.display=''; else document.getElementById('<%= rowWorkday.ClientID %>').style.display='none';" Width="183px" runat="server" >
                                        <asp:ListItem Text="-Select option-" Value="-1" />
                                         <asp:ListItem Text="Default/Acutal month workday" Value="1" />
                                        <asp:ListItem Text="Based on weekly holiday deduction" Value="2" />
                                        <asp:ListItem Text="Fixed days" Value="3" />
                                    </asp:DropDownList>
                                      <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" InitialValue="-1" ControlToValidate="ddlSalaryWorkdaySetting"
                                    Display="None" ErrorMessage="Salary workday method." ValidationGroup="AECompany"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr runat="server" style="display:none" id="rowWorkday">
                                <td class="fieldHeader">
                                    
                                </td>
                                <td class="fieldHeader">
                                    (Fixed workday like 30 or 30.416666 (365/12))

                                    <asp:TextBox ID="txtFixedWorkdayValue" runat="server" Width="180px"></asp:TextBox>
                                </td>
                            </tr>
                             <tr>
                                <td class="fieldHeader">Do not use SST Recovery first in tax
                                </td>
                                <td class="fieldHeader">
                                    <asp:DropDownList ID="ddlSSTRecovery" Width="183px" runat="server" >
                                        <asp:ListItem Text="-Select option-" Value="-1" />
                                         <asp:ListItem Text="Yes" Value="true" />
                                        <asp:ListItem Text="No" Value="false" />
                                    </asp:DropDownList>
                                      <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" InitialValue="-1" ControlToValidate="ddlSSTRecovery"
                                    Display="None" ErrorMessage="SST recovery first in tax is required." ValidationGroup="AECompany"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                             <tr>
                                <td class="fieldHeader">Enable level wise salary
                                </td>
                                <td class="fieldHeader">
                                    <asp:DropDownList ID="ddlEnableLevelwiseSalary" Width="183px" runat="server" >
                                        <asp:ListItem Text="-Select option-" Value="-1" />
                                         <asp:ListItem Text="Yes" Value="true" />
                                        <asp:ListItem Text="No" Value="false" />
                                    </asp:DropDownList>
                                      <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" InitialValue="-1" ControlToValidate="ddlEnableLevelwiseSalary"
                                    Display="None" ErrorMessage="Levelwise salary option is required." ValidationGroup="AECompany"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="fieldHeader">Deduct previous month upl/abs change from attendance in current month salary
                                </td>
                                <td class="fieldHeader">
                                    <asp:DropDownList ID="ddlPreviousMonthUPLInCurrentSalary" Width="183px" runat="server" >
                                        <asp:ListItem Text="-Select option-" Value="-1" />
                                         <asp:ListItem Text="Yes" Value="true" />
                                        <asp:ListItem Text="No" Value="false" />
                                    </asp:DropDownList>
                                      <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" InitialValue="-1" ControlToValidate="ddlPreviousMonthUPLInCurrentSalary"
                                    Display="None" ErrorMessage="Deduction previous month upl option is required." ValidationGroup="AECompany"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="col" style="width: 350px">
                    <fieldset runat="server" class="sectionsub" id="fieldSetPFRF" style="display: inline; float: left; box-shadow: 0 0 0!important">
                        <table cellpadding="0" cellspacing="0" class="companytabl">
                            <tr>
                                <td colspan="2">
                                    <h3 style='padding-left: 0px; border-bottom: 1px solid #10b0ea;'>Citizen Investment Trust</h3>
                                </td>
                            </tr>
                            <tr>
                                <td class="fieldHeader" width="150px">
                                    <My:Label ID="lblPFText" runat="server" Text="Company Code" ShowAstrick="false" />
                                </td>
                                <td>
                                    <asp:TextBox ID="txtCITCompanyCode" runat="server" Width="180px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="fieldHeader">
                                    <My:Label ID="Label11" runat="server" Text="Bank Name" ShowAstrick="false" />
                                </td>
                                <td>
                                    <asp:TextBox ID="txtCITBankName" runat="server" Width="180px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="fieldHeader">
                                    <My:Label ID="Label16" runat="server" Text="CIT Report Description" ShowAstrick="false" />
                                </td>
                                <td>
                                    <asp:TextBox TextMode="MultiLine" Height="80px" ID="txtCITReportDesc" runat="server" Width="250px"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                        <asp:Button ID="btnAddOtherFund" CssClass="update" runat="server"
                            Text="Add Fund" CausesValidation="false" OnClick="btnAddOtherFund_Click" />
                        <table cellpadding="0" cellspacing="0" style="margin-top: 10px" class="companytabl" runat="server" id="tblOtherFund" visible="false">
                            <tr>
                                <td class="fieldHeader" width="150px">
                                    <My:Label ID="Label14" runat="server" Text="Other Retirement Fund Name" ShowAstrick="true" />
                                </td>
                                <td>
                                    <asp:TextBox ID="txtOtherFundName" runat="server" Width="180px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="fieldHeader" width="150px">
                                    <My:Label ID="Label12" runat="server" Text="Other Retirement Fund Abbr" ShowAstrick="true" />
                                </td>
                                <td>
                                    <asp:TextBox ID="txtOtherFundAbbr" runat="server" Width="180px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="fieldHeader">
                                    <My:Label ID="Label13" runat="server" Text="Other Retirement Fund Code" ShowAstrick="false" />
                                </td>
                                <td>
                                    <asp:TextBox ID="txtOtherFundCode" runat="server" Width="180px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="fieldHeader">
                                    <My:Label ID="Label15" runat="server" Text="Deposited by Employee" ShowAstrick="false" />
                                </td>
                                <td>
                                    <asp:CheckBox ID="chkDepositedByEmployee" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </div>
                <div style="clear: left; padding: 10px  0 0 150px; display: block;" />
                <p>
                </p>
            </fieldset>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="pageScriptsContent" runat="server">

    <script type="text/javascript">


        function onChangeIRO(ddl) {
            showLoading();
            Web.PayrollService.GetTSOByIRO(ddl.value, onChangeIROCallback);
        }

        function onChangeIROCallback(result) {
            onDropDownChangeCallback('<%= ddlTSO.ClientID %>', result, false);
     }


     function ValidateTextBox1(source, args) {
         var chk = document.getElementById('<%= chkHasPFRFFund.ClientID %>');
         args.IsValid = true;
         if (chk.checked) {
             var str = new String(args.Value);
             if (str.trim() == "")
                 args.IsValid = false;
         }
     }

     function onCheckChange(chk, fieldSetId) {

         var rowPF1 = '<%= rowPF1.ClientID %>';
         var rowPF2 = '<%= rowPF2.ClientID %>';
         //as PFRF checkbox has multiple to handle
         if (fieldSetId.indexOf('fieldSetPFRF') > 0) {
             toggleElementUsingCheckBoxState(chk, rowPF1, false);
             toggleElementUsingCheckBoxState(chk, rowPF2, false);
         }

         if (chk.checked) {
             enableElement(fieldSetId);
             $('#' + fieldSetId).show();
         }
         else {

             $('#' + fieldSetId).hide();
             disableElement(fieldSetId);
         }
     }

     function valDate() {
         var calStartingDate = '<%= calStartingDate.ClientID %>';
         var calEndingDate = '<%= calEndingDate.ClientID %>';

         if (document.getElementById(calStartingDate + "_datey").disabled || document.getElementById(calStartingDate + "_datey").disabled == 'disabled')
             return true;


         var d1, d2, m1, m2, y1, y2;

         y1 = parseInt(document.getElementById(calStartingDate + "_datey").value);
         y2 = parseInt(document.getElementById(calEndingDate + "_datey").value);

         d1 = parseInt(document.getElementById(calStartingDate + "_dated").value);
         d2 = parseInt(document.getElementById(calEndingDate + "_dated").value);

         m1 = parseInt(document.getElementById(calStartingDate + "_datem").value);
         m2 = parseInt(document.getElementById(calEndingDate + "_datem").value);



         if (isSecondCalendarCtlDateGreater(y1 + "/" + m1 + "/" + d1, y2 + "/" + m2 + "/" + d2) == false) {
             alert(endingDateGreaterThanStarting);
             return false;
         }


         //check if greater than one year
         y1 += 1;

         if (isSecondCalendarCtlDateGreater(y1 + "/" + m1 + "/" + d1, y2 + "/" + m2 + "/" + d2) == true) {
             return confirm(yearCannotBelongerThan12);

         }

         return true;
     }

     function validateCP() {
         valGroup = 'AECompany';
         if (CheckValidation()) {
             return valDate();
         }
         else
             return false;
     }
    </script>
</asp:Content>
