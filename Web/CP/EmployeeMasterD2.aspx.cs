﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using Utils.Helper;
using Utils.Web;
using BLL.Base;
using DAL;
using Utils.Base;

namespace Web
{


    public partial class EmployeeMasterD2 : BasePage
    {

        protected void Page_Init(object sender, EventArgs e)
        {
            Initialise();
        }
    
        protected void Page_Load(object sender, EventArgs e)
        {
           
            {
       

                BindEmployees();

            }
        }

        public void Initialise()
        {
            ddlDepartment.DataSource
                = new DepartmentManager().GetDepartmentsByCompany(SessionManager.CurrentCompanyId);

            ddlDepartment.DataBind();

            List<EDesignation> designations = new CommonManager().GetAllDesignations();

            ddlJobCode.DataSource = designations.Where(x => x.Code != null && x.Code != "").ToList();
            ddlJobCode.DataBind();

            ddlJobTitle.DataSource = designations;
            ddlJobTitle.DataBind();


        }

        public void btnLoad_Click(object sender, EventArgs e)
        {

        }


        void BindEmployees()
        {




            gvD2EmployeeGrid.DataSource = EmployeeManager.GetEmployeeMasterD2(int.Parse(ddlDepartment.SelectedValue),
                ddlJobCode.SelectedValue, int.Parse(ddlJobTitle.SelectedValue), int.Parse(ddlGender.SelectedValue),
                txtEmpSearch.Text.Trim());

                gvD2EmployeeGrid.DataBind();
          

        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
           
                GridViewExportUtil.Export("EmployeeMaster.xls", gvD2EmployeeGrid);
        }
     
    }

    }

