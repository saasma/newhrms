﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Base;
using BLL.Manager;
using DAL;
using Utils.Web;
using Utils.Helper;
using Utils.Calendar;
using BLL.BO;

namespace Web.CP
{
    public partial class HoldPaymentPG : BasePage
    {
        PayManager payMgr = new PayManager();
        HoldPaymentManager holdPayMgr = new HoldPaymentManager();
        PayrollPeriod lastPeriod = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            lastPeriod = CommonManager.GetLastPayrollPeriod();
            if (!IsPostBack)
            {
                /*to initialized filter parameter*/
                LoadPayrollPeriods();

                Initialise();
            }

            if (Request.Form["__EVENTTARGET"] != null && Request.Form["__EVENTTARGET"].Equals("Reload"))
            {
                gvw.SelectedIndex = -1;
                ClearFields();
                LoadStopPayments();
            }
            JavascriptHelper.AttachPopUpCode(Page, "resumePaymentPopup", "ResumePayment.aspx", 355, 160);
            JavascriptHelper.AttachPopUpCode(Page, "importPopup", "../ExcelWindow/HoldPaymentImp.aspx", 450, 500);
        }

        /*for filter parameter*/
        protected void LoadPayrollPeriods()
        {
            List<PayrollPeriodBO> list = CommonManager.GetAllYear(SessionManager.CurrentCompanyId);

           

            ddlPayrollPeriodTo.DataSource = ddlPayrollPeriodsFrom.DataSource = list;
            ddlPayrollPeriodsFrom.DataBind();
            ddlPayrollPeriodTo.DataBind();

           

        }
        void Initialise()
        {
            LoadEmployees();
           
            
            if (lastPeriod != null)
                lblPayrollPeriod.Text = lastPeriod.Name;
            else
                lblPayrollPeriod.Text = "";
           
            LoadStopPayments();
        }

        void LoadEmployees()
        {
            EmployeeManager emgMgr = new EmployeeManager();
            ddlEmployees.DataSource = emgMgr.GetEmployees(SessionManager.CurrentCompanyId);
            ddlEmployees.DataBind();
        }

        void LoadStopPayments()
        {


            int totalRecords = 0;

            gvw.DataSource = holdPayMgr.GetHoldPayments(true, txtFilterEmpName.Text, Convert.ToInt32(ddlPayrollPeriodsFrom.SelectedItem.Value),
                Convert.ToInt32(ddlPayrollPeriodTo.SelectedItem.Value), pagingCtl1.CurrentPage - 1, int.Parse(pagingCtl1.DDLRecords.SelectedValue), ref totalRecords);
            gvw.DataBind();
            pagingCtl1.UpdatePagingBar(totalRecords);

            totalRecords = 0;

            gvwHoldPaymentPastMonths.DataSource = holdPayMgr.GetHoldPayments(false, txtFilterEmpName.Text, Convert.ToInt32(ddlPayrollPeriodsFrom.SelectedItem.Value),
                Convert.ToInt32(ddlPayrollPeriodTo.SelectedItem.Value), pagingCtl2.CurrentPage - 1, int.Parse(pagingCtl2.DDLRecords.SelectedValue), ref totalRecords);
            gvwHoldPaymentPastMonths.DataBind();
            pagingCtl2.UpdatePagingBar(totalRecords);
        }



        #region pagination on grid
        protected void btnNext1_Click()
        {
            pagingCtl1.CurrentPage += 1;
            LoadStopPayments();
        }

        protected void btnPrevious1_Click()
        {
            pagingCtl1.CurrentPage -= 1;
            LoadStopPayments();
        }

        protected void ddlRecords1_SelectedIndexChanged()
        {
            pagingCtl1.CurrentPage = 1;
            LoadStopPayments();
        }


        protected void btnNext2_Click()
        {
            pagingCtl2.CurrentPage += 1;
            LoadStopPayments();
        }

        protected void btnPrevious2_Click()
        {
            pagingCtl2.CurrentPage -= 1;
            LoadStopPayments();
        }

        protected void ddlRecords2_SelectedIndexChanged()
        {
            pagingCtl2.CurrentPage = 1;
            LoadStopPayments();
        }

        #endregion

        protected void ddlEmployees_SelectedIndexChanged(object sender, EventArgs e)
        {
           

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            LoadStopPayments();

        }
        void Process(ref HoldPayment entity)
        {
            if (entity == null)
            {
                entity = new HoldPayment();
                entity.EmployeeId = int.Parse(ddlEmployees.SelectedValue);
                entity.PayrollPeriodId = lastPeriod.PayrollPeriodId;
                //set eng date

                

                entity.Notes = txtNotes.Text.Trim();

               

            }
            else
            {

                ddlEmployees.SelectedValue = entity.EmployeeId.Value.ToString();
                txtNotes.Text = entity.Notes;
                lblPayrollPeriod.Text = CommonManager.GetPayrollPeriod(entity.PayrollPeriodId.Value).Name;



                //check if stop payment start date is passed i.e. stop payment already used in the
                // calculation then disable fields
                //bool isAlreadyUsed = holdPayMgr.IsStopPaymentAlreadyUsed(entity.StopPaymentId);

                //if (isAlreadyUsed)
                //{
                //    divWarningMsg.InnerHtml = "Stop payment already used, so some fields are disabled.";
                //    divWarningMsg.Hide = false;

                //    calFromDate.Enabled = false;
                //    chkExcludeWorkdays.Enabled = false;
                //    chkTreatLikeRetForTax.Enabled = false;

                //}
                //else
                //{
                //    calFromDate.Enabled = true;
                //    chkExcludeWorkdays.Enabled = true;
                //    chkTreatLikeRetForTax.Enabled = true;
                //}

                UIHelper.SetSelectedInDropDown(ddlEmployees, entity.EmployeeId);

                //ddlEmployees_SelectedIndexChanged(null, null);


                //calFromDate.SetSelectedDate(entity.FromDate, SessionManager.CurrentCompany.IsEnglishDate);
                
                
                //calToDate.SetSelectedDate(entity.ToDate, IsEnglish);
                //txtNotes.Text = entity.Notes;


                //chkExcludeWorkdays.Checked = entity.ExcludePeriodInWorkDaysCount.Value;
                //chkTreatLikeRetForTax.Checked = entity.TreatLikeRetirementForTax.Value;
               
            }

        }

        public bool IsDeletable(object stopPaymentId)
        {
            if (SessionManager.IsReadOnlyUser)
                return false;

            return holdPayMgr.IsHoldPaymentDeletable((int)stopPaymentId);
        }

        public bool IsStopPaymentEditable(object id)
        {
            if (SessionManager.IsReadOnlyUser)
                return false;

            return !holdPayMgr.IsHoldPaymentDeletable((int)id);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                int holdPaymentId = 0;

                if (gvw.SelectedIndex != -1)
                {
                    holdPaymentId = (int)gvw.DataKeys[gvw.SelectedIndex]["HoldPaymentId"];
                }
                HoldPayment stopPayment= null;
                Process(ref stopPayment);

                InsertUpdateStatus status = new InsertUpdateStatus();
                if (holdPaymentId == 0)
                {
                    

                     status =  holdPayMgr.Save(stopPayment);
                    if (status.IsSuccess)
                    {
                        divMsgCtl.InnerHtml = "Hold payment saved for the employee.";
                        divMsgCtl.Hide = false;
                        
                        ClearFields();
                    }
                    else
                    {
                        divWarningMsg.InnerHtml = status.ErrorMessage;
                        divWarningMsg.Hide = false;
                    }
                }
                else
                {
                    stopPayment.HoldPaymentId = holdPaymentId;
                    status = holdPayMgr.Update(stopPayment);
                    if (status.IsSuccess)
                    {
                        divMsgCtl.InnerHtml = "Stop payment updated for the employee.";
                        divMsgCtl.Hide = false;
                       
                        ClearFields();
                    }
                    else
                    {
                        divWarningMsg.InnerHtml = status.ErrorMessage;
                        divWarningMsg.Hide = false;
                    }
                }

                if (status.IsSuccess)
                {
                    gvw.SelectedIndex = -1;
                    LoadStopPayments();
                }
            }
        }

        void ClearFields()
        {
            btnSave.Text = Resources.Messages.Save;
            ddlEmployees.ClearSelection();
            ddlEmployees.SelectedIndex = 0;
            txtNotes.Text = "";
           // calFromDate.SelectTodayDate();
        

            ddlEmployees.Enabled = true;

            //calFromDate.Enabled = true;
            //chkExcludeWorkdays.Enabled = true;
            //chkTreatLikeRetForTax.Enabled = true;
        }

        protected void gvw_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvw.SelectedIndex = -1;
            gvw.PageIndex = e.NewPageIndex;
            LoadStopPayments();
        }

       

        protected void gvw_SelectedIndexChanged1(object sender, EventArgs e)
        {
            if (gvw.SelectedIndex >= 0)
            {
                int stopPaymentId = (int)gvw.DataKeys[gvw.SelectedIndex]["HoldPaymentId"];

                HoldPayment entity = holdPayMgr.GetHoldPaymentById(stopPaymentId);
                Process(ref entity);
                btnSave.Text = Resources.Messages.Update;
                ddlEmployees.Enabled = false;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ClearFields();
            gvw.SelectedIndex = -1;
            LoadStopPayments();
        }

        protected void chkShowArchieve_Changed(object sender, EventArgs e)
        {
            LoadStopPayments();
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (ddlEmployees.SelectedValue != "-1")
            {
                lblEmpId.Text = ddlEmployees.SelectedValue;
            }
            else
                lblEmpId.Text = "";

            btnSave.Visible = !SessionManager.IsReadOnlyUser;
        }

        protected void gvw_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            e.Cancel = true;
            int stopPaymentId = (int)gvw.DataKeys[e.RowIndex]["HoldPaymentId"];

            if (IsDeletable(stopPaymentId))
            {
                if (holdPayMgr.Delete(stopPaymentId))
                {
                    divMsgCtl.InnerHtml = "Hold payment deleted.";
                    divMsgCtl.Hide = false;
                }
               
            }
            gvw.SelectedIndex = -1;
            LoadStopPayments();
            ClearFields();
        }



        protected void gvwHoldPaymentPastMonths_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvwHoldPaymentPastMonths.SelectedIndex = -1;
            gvwHoldPaymentPastMonths.PageIndex = e.NewPageIndex;
            LoadStopPayments();
        }



        protected void gvwHoldPaymentPastMonths_SelectedIndexChanged1(object sender, EventArgs e)
        {
            if (gvwHoldPaymentPastMonths.SelectedIndex >= 0)
            {
                int stopPaymentId = (int)gvwHoldPaymentPastMonths.DataKeys[gvwHoldPaymentPastMonths.SelectedIndex]["HoldPaymentId"];

                InsertUpdateStatus status =  new HoldPaymentManager().ReleaseHoldPaymentInCurrentMonth(stopPaymentId);

                if (status.IsSuccess == false)
                {
                    divWarningMsg.InnerHtml = status.ErrorMessage;
                    divWarningMsg.Hide = false;
                    return;
                }


                HoldPayment entity = holdPayMgr.GetHoldPaymentById(stopPaymentId);


                divMsgCtl.InnerHtml = 
                    "Hold payment of " + EmployeeManager.GetEmployeeById(entity.EmployeeId.Value).Name + " has been set to release in " 
                    + CommonManager.GetPayrollPeriod( entity.ReleasedPayrollPeriodId.Value).Name +" period.";
                divMsgCtl.Hide = false;

                LoadStopPayments();
              
            }
        }

        public bool IsReleased(object value)
        {
            if (value == null)
                return true;
            if (Convert.ToBoolean(value) == false)
                return true;
            return false;
        }

        public bool IsCancelReleased(object value)
        {
            if (value == null)
                return true;

            if (Convert.ToInt32(value) == 0)
                return true;

            return false;
        }

        protected void gvwHoldPaymentPastMonths_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            e.Cancel = true;
            int stopPaymentId = (int)gvwHoldPaymentPastMonths.DataKeys[e.RowIndex]["HoldPaymentId"];

            InsertUpdateStatus status = new HoldPaymentManager().DeleteTheReleaseOfHoldPaymentInCurrentMonth(stopPaymentId);

            if (status.IsSuccess == false)
            {
                divWarningMsg.InnerHtml = status.ErrorMessage;
                divWarningMsg.Hide = false;
                return;
            }


            HoldPayment entity = holdPayMgr.GetHoldPaymentById(stopPaymentId);


            divMsgCtl.InnerHtml =
                "Hold payment of " + EmployeeManager.GetEmployeeById(entity.EmployeeId.Value).Name + " has been deleted to release.";
            divMsgCtl.Hide = false;

            LoadStopPayments();
        }

    }
}
