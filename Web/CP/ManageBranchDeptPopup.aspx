﻿<%@ Page Title="Branch Department" Language="C#" MasterPageFile="~/Master/ForPopupPage.Master" AutoEventWireup="true" CodeBehind="ManageBranchDeptPopup.aspx.cs" Inherits="Web.CP.ManageBranchDeptPopup" %>

<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

<style type="text/css">
    .clsColor
    {
        background-color: #e8f1ff;
        height: 500px;
    }
    h2.popstitle
    {
        color: #333333;
        font: bold 13px Arial;
        padding-bottom: 0px !important;
    }
    
    .marginal td{vertical-align:top;}
</style>


<script type="text/javascript">

    function closePopup() {
        window.opener.reloadBrDept(window);
    }

</script>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">

<asp:HiddenField ID="hdnBranchId" runat="server" />

 <div class="clsColor">
        <div class="popupHeader">
            <h3>
               Branch  Department Details</h3>
        </div>

         <uc2:MsgCtl ID="divMsgCtl" Width="920px" EnableViewState="false" Hide="true" runat="server" />
        <uc2:WarningCtl ID="divWarningMsg" Width="920px" EnableViewState="false" Hide="true"
            runat="server" />

         <div class="marginal" style="margin: 5px 20px 0 10px;">         
                <table cellpadding="4px" style="margin-left: 10px;">
                      <tr>
                        <td colspan="3">
                            <h2 id="branchTitle" class="popstitle" runat="server">
                                Add Departments to
                            </h2>
                        </td>
                    </tr>
                    <tr>
                        <td class="fieldHeader">
                            <strong>Available Departments</strong>
                            <asp:ListBox ID="lstSource" runat="server" SelectionMode="Multiple" Width="300" Height="300"
                                DataValueField="DepartmentId" DataTextField="Name"></asp:ListBox>
                            <asp:RequiredFieldValidator ID="test" runat="server" ValidationGroup="add" ControlToValidate="lstSource"
                                Display="None" ErrorMessage="Department should be selected for addition." />
                        </td>
                        <td style="text-align: center">
                            <asp:Button Text=">" runat="server" Width="50" ToolTip="Add" ID="btnAdd" OnClientClick="valGroup='add';return CheckValidation()"
                                OnClick="btnAdd_Click" />
                            <br />
                            <br />
                            <asp:Button Text="<" runat="server" OnClientClick="valGroup='remove';return CheckValidation()"
                                Width="50" ToolTip="Remove" ID="btnRemove" OnClick="btnRemove_Click" />
                            <br />
                            <br />
                            <asp:Button Text=">>" runat="server" Width="50" CausesValidation="false" ToolTip="Add All"
                                ID="btnAddAll" OnClick="btnAddAll_Click" />
                            <br />
                            <br />
                            <asp:Button Text="<<" runat="server" Width="50" CausesValidation="false" ToolTip="Remove All"
                                ID="btnRemoveAll" OnClick="btnRemoveAll_Click" />
                            <br />
                            <br />
                            <asp:CheckBox ID="chkToAll" runat="server" Text="To All Branch" />
                        </td>
                        <td>
                            <strong>Selected Departments</strong>
                            <br />
                            <asp:ListBox ID="lstDest" SelectionMode="Multiple" runat="server" Width="300" Height="300"
                                DataValueField="DepartmentId" DataTextField="Name"></asp:ListBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="remove"
                                ControlToValidate="lstDest" Display="None" ErrorMessage="Department should be selected for removal." />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="btnSave" CssClass="update" runat="server" Text="Save" ValidationGroup="AEBranch"
                                OnClick="btnSave_Click" />
                            <asp:Button ID="btnCancel" OnClientClick="window.close();" CssClass="cancel" runat="server"
                                Text="Cancel" />
                        </td>
                    </tr>
                </table>
        </div>


</div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
