﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Ext.Net;
using DAL;
using BLL;
using BLL.Base;
using BLL.Manager;
using Utils.Helper;
using Utils.Calendar;
//using Web.Report.Templates;
using System.IO;
using DevExpress.XtraPrinting;
using BLL.BO;
using System.Xml;
using System.Xml.Xsl;
using Newtonsoft.Json.Linq;
using System.Collections;
using System.Reflection;
using System.Data;
using Bll;
using Web.Helper;
using System.Web.UI.HtmlControls;

namespace Web.Report
{

    public partial class EmployeeMasterDynamicReport : BasePage
    {


        List<string> _ListColsToHide = new List<string> { "List" };
        protected void Page_Load(object sender, EventArgs e)
        {
          
            if (!X.IsAjaxRequest && !IsPostBack)
            {
                
                GetEmployeeMasterResult _SPParam = new GetEmployeeMasterResult();
                List<DynamicReportFieldsBO> _listFieldsStaticColumns = DynamicReportManager.GetDynamicReportAvilableFields(_SPParam, _ListColsToHide);
                List<DynamicReportFieldsBO> _listFieldsDynamicColumns = DynamicReportManager.GetDynamicFieldsProperty();

                List<DynamicReportFieldsBO> _listFields = new List<DynamicReportFieldsBO>();
                _listFields.AddRange(_listFieldsStaticColumns);
                _listFields.AddRange(_listFieldsDynamicColumns);

                if (_listFields.Any())
                {
                    StoreFields.DataSource = _listFields;
                    StoreFields.DataBind();
                }
             
            }

        }


        void Print(bool isPDF)
        {
           
          
        }

        protected void btnPDFPrint_Click(object sender, EventArgs e)
        {
            Print(true);
        }

        //protected void lnkShowHideReportFilter_Click(object sender, DirectEventArgs e)
        //{
        //    X.AddScript("document.getElementById('divFilter').style.display='none';");
        //    grid.Width = 1200;
        //    divShowhide.InnerText = ">>";
        //}

   

        protected Field OnCreateFilterableField(object sender, ColumnBase column, Field defaultField)
        {
            
                ((TextField)defaultField).Icon = Icon.Magnifier;
                return defaultField;
        }



        private DataTable GetDataTable(List<object> _data, List<DynamicReportFieldsBO> Columns,List<string> ColumnsToHide, List<string> _SelectedFields)
        {
            DataTable table = new DataTable();

            foreach (DynamicReportFieldsBO _fields in Columns)
            {
                DataColumn colString = new DataColumn();
                colString.AllowDBNull = true;
                colString.ColumnName = _fields.ServerMappingFields;
              
                if (_fields.DataType.ToLower() == "string")
                    colString.DataType = typeof(string);
                if (_fields.DataType.ToLower() == "float")
                    colString.DataType = typeof(float);
                if (_fields.DataType.ToLower() == "date")
                    colString.DataType = typeof(DateTime);
                table.Columns.Add(colString);

            }

            foreach (object[] obj in _data)
            {

                table.Rows.Add(obj);
            }
            foreach (object _hiddenCols in ColumnsToHide)
            {
                table.Columns.Remove(NewMessage.GetTextValue(_hiddenCols.ToString()).ToString());
            }


            return table;
        }




        private DataTable GetDataTableForExcel(List<object> _data, List<string> ColumnsToHide, List<DynamicReportFieldsBO> Columns, List<string> _SelectedFields)
        {
            DataTable table = new DataTable();

            foreach (DynamicReportFieldsBO _fields in Columns)
            {
                DataColumn colString = new DataColumn();
         
                colString.AllowDBNull = true;
                colString.ColumnName = NewMessage.GetTextValue(_fields.ServerMappingFields);// HttpContext.GetGlobalResourceObject("ResourceEnums", _fields.ServerMappingFields).ToString();

                if (_fields.DataType.ToLower() == "string")
                    colString.DataType = typeof(string);
                if (_fields.DataType.ToLower() == "float")
                    colString.DataType = typeof(float);
                if (_fields.DataType.ToLower() == "date")
                    colString.DataType = typeof(DateTime);
                table.Columns.Add(colString);

            }

            foreach (object[] obj in _data)
            {
                table.Rows.Add(obj);
            }

            foreach (object _hiddenCols in ColumnsToHide)
            {
                table.Columns.Remove(NewMessage.GetTextValue(_hiddenCols.ToString()).ToString());
            }

            ReorderTable(ref table, _SelectedFields.ToArray());

            return table;
        }


        public static void ReorderTable(ref DataTable table, params String[] columns)
        {
            if (columns.Length != table.Columns.Count)
                throw new ArgumentException("Count of columns must be equal to table.Column.Count", "columns");

            for (int i = 0; i < columns.Length; i++)
            {

                table.Columns[columns[i]].SetOrdinal(i);
            }
        }

        protected void btnDynamicExcelPrint_Click(object sender, EventArgs e)
        {

            List<GetEmployeeMasterResult> data = EmployeeManager.GetEmployeeMaster(0, int.MaxValue, "", -1, -1, -1,"").ToList();

            //  GridPanel1.EnableColumnHide = true;
            List<object> _newObjList = new List<object>();
            List<Object> objList = new List<object>();
            for (int i = 0; i < data.Count; i++)
                objList.Add(data[i]); List<GetEmployeeMasterOtherHeadersResult> _dynamicTable = EmployeeManager.GetEmployeeMasterHeader();

            int FixedListCols = data[0].GetType().GetProperties().Length;
            int DynamicCols = _dynamicTable.Count();

            int TotalFields = FixedListCols + DynamicCols;

            PropertyInfo[] properties = data[0].GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);

            foreach (object item in data)
            {
                Array stringArray = Array.CreateInstance(typeof(object), TotalFields);

                for (int col = 0; col <= TotalFields - 1; col++)
                {
                    if (col <= FixedListCols - 1)//setting array for fixed columns
                    {
                        object o = properties[col].GetValue(item, null);

                        string cellValue = o == null ? null : o.ToString();
                        stringArray.SetValue(cellValue, col);
                    }
                    else//getting dynamic cols
                    {
                        foreach (GetEmployeeMasterOtherHeadersResult _Column in _dynamicTable)
                        {
                            //getting value based on dynamiccolumn column
                            //getColumnValueBasedOnKeyValue();

                            int ID = int.Parse(data[0].GetType().GetProperty("EmployeeId").GetValue(item, null).ToString());
                            string ValueList = data[0].GetType().GetProperty("List").GetValue(item, null).ToString();
                            string[] arrayValueList = ValueList.Split(';');
                            List<EmployeeExtraFieldsValuesBO> _ListEmployeeExtraFieldsValuesBO = new List<EmployeeExtraFieldsValuesBO>();
                            for (int i = 0; i < arrayValueList.Length; i++)
                            {
                                string[] arrayInnerValueList = arrayValueList[i].Split(':');
                                if (arrayInnerValueList.Length > 1)
                                {
                                    EmployeeExtraFieldsValuesBO _EmployeeExtraFieldsValuesBO = new EmployeeExtraFieldsValuesBO();
                                    _EmployeeExtraFieldsValuesBO.ID = ID;
                                    _EmployeeExtraFieldsValuesBO.TypeID = int.Parse(arrayInnerValueList[0]);
                                    _EmployeeExtraFieldsValuesBO.SourceID = int.Parse(arrayInnerValueList[1]);
                                    _EmployeeExtraFieldsValuesBO.Value = arrayInnerValueList[2];
                                    _ListEmployeeExtraFieldsValuesBO.Add(_EmployeeExtraFieldsValuesBO);
                                }
                            }

                            List<GetEmployeeMasterOtherHeadersResult> _ListExtraHeaderEmployee = EmployeeManager.GetEmployeeMasterHeader().Where(x => x.HeaderName.ToLower() == _Column.HeaderName.ToLower()).ToList();
                            int SourceID = _ListExtraHeaderEmployee[0].SourceId;
                            int Type = _ListExtraHeaderEmployee[0].Type;


                            String ColumnValue = "";
                            EmployeeExtraFieldsValuesBO _ValueList = _ListEmployeeExtraFieldsValuesBO.SingleOrDefault(x => x.ID == ID && x.SourceID == SourceID && x.TypeID == Type);
                            if (_ValueList != null)
                                ColumnValue = _ValueList.Value;

                            //get ExtraFields value;

                            GetDynamicColsvalue(ID.ToString());
                            stringArray.SetValue(ColumnValue, col);
                            col++;
                        }
                    }
                }
                _newObjList.Add(stringArray);
            }

            //---------------------------

            List<ColumnBase> columns = new List<ColumnBase>();
            GridFilters filterList = new GridFilters();
            ModelFieldCollection _ModelFieldCollection = new ModelFieldCollection();

            GetEmployeeMasterResult _SPParam = new GetEmployeeMasterResult();


            List<DynamicReportFieldsBO> _listFieldsStaticColumns = DynamicReportManager.GetDynamicReportAvilableFields(_SPParam, new List<string> { "" });
            List<DynamicReportFieldsBO> _listFieldsDynamicColumns = DynamicReportManager.GetDynamicFieldsProperty();


            List<DynamicReportFieldsBO> _listFields = new List<DynamicReportFieldsBO>();
            _listFields.AddRange(_listFieldsStaticColumns);
            _listFields.AddRange(_listFieldsDynamicColumns);

          
            List<string> _ExcelhiddenColsList = new List<string>();
            //_ExcelhiddenColsList.Add("AssetID");
            //_ExcelhiddenColsList.Add("TotalRows");

            List<string> PropertyNamesOrderList = new List<string>();
            Dictionary<string, string> columnRenameList = new Dictionary<string, string>();
            List<string> _SelectedFields = new List<string>();

            if (_listFields.Any())
            {
                List<DynamicReportFieldsBO> _FieldNametoHideList = new List<DynamicReportFieldsBO>();
                string entryLineJson = HiddenSelectedFields.Text;


                if (entryLineJson != "[]")
                {
                    List<DynamicReportFieldsBO> _LineList = JSON.Deserialize<List<DynamicReportFieldsBO>>(entryLineJson);

                    _SelectedFields = (
                       (from _fields in _LineList
                        select _fields.FieldName).ToList()
                          );

                    _FieldNametoHideList = _listFields.Where(i => !_SelectedFields.Contains(i.FieldName)).ToList();


                    foreach (DynamicReportFieldsBO _Hiddenlist in _FieldNametoHideList)
                    {
                        _ExcelhiddenColsList.Add(_Hiddenlist.ServerMappingFields);
                    }

                    foreach (DynamicReportFieldsBO _selFields in _LineList)
                    {

                        DynamicReportFieldsBO _FAMS_DynamicReportField = _listFields.SingleOrDefault(x => x.FieldName.ToLower() == _selFields.FieldName.ToLower());
                        string ServerMappingField = _FAMS_DynamicReportField.ServerMappingFields;
                        PropertyNamesOrderList.Add(ServerMappingField);
                        columnRenameList.Add(ServerMappingField,  NewMessage.GetTextValue(ServerMappingField));
                    }
                }
            }


            //string GetType = "FAMS_GetAssetSearchListReportSPResult";
            //DataTable dt = GetDataTableForExcel(_newObjList, _ExcelhiddenColsList,_listFields);
            DataTable dt = GetDataTableForExcel(_newObjList, _ExcelhiddenColsList, _listFields, _SelectedFields);
            ExcelHelper.ExportDtToExcel(dt);



            //if (data.Any())
            //{
            //    ExcelHelper.ExportToExcelDynamic<Object>("Asset List", _newObjList,
            //       _ExcelhiddenColsList,
            //        new List<String>() { },
            //       columnRenameList,
            //    //    new Dictionary<string, string>() { { "AssetName", "Asset Name" }, { "AssetNumber", "Asset Number" }, { "ClassOrGroupRef_Name", "Class" }, 
            //    //{ "SysNo", "System Number" },{"LocationRef_Name","Location"}, {"GLAssetAccount","G/L Asset Account"}, {"AcquisitionDate","Acquisition Date"},{"CapitalizedDate","Capitalized Date"},{"AcquisitionCost","Acquisition Cost"} },
            //        new List<string>() { }
            //        , new Dictionary<string, string>() { }
            //        , PropertyNamesOrderList);
            //}
            //,new List<string> { "SysNo", "AssetName", "AssetNumber", "ClassOrGroupRef_Name", "LocationRef_Name" });

        }
        protected void Store_ReadDataDynamic(object sender, StoreReadDataEventArgs e)
        {

            List<GetEmployeeMasterResult> data = EmployeeManager.GetEmployeeMaster(e.Start, e.Limit, "", -1, -1, -1, "").ToList();
            //  GridPanel1.EnableColumnHide = true;
            List<Object> objList = new List<object>();
            for (int i = 0; i < data.Count; i++)
                objList.Add(data[i]);

            //for creating list obj of fixed columns from SP and DynamicColums from tablee
            List<object> _newObjList = new List<object>();
            // List<testdynamicTable> _testdynamicTable = DynamicReportManager.GetDynamicColumns();
            List<GetEmployeeMasterOtherHeadersResult> _dynamicTable = EmployeeManager.GetEmployeeMasterHeader();

            int FixedListCols = data[0].GetType().GetProperties().Length;
            int DynamicCols = _dynamicTable.Count();
            int TotalFields = FixedListCols + DynamicCols;
            PropertyInfo[] properties = data[0].GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);

            foreach (object item in data)
            {
                Array stringArray = Array.CreateInstance(typeof(object), TotalFields);

                for (int col = 0; col <= TotalFields - 1; col++)
                {
                    if (col <= FixedListCols - 1)//setting array for fixed columns
                    {
                        object o = properties[col].GetValue(item, null);

                        string cellValue = o == null ? null : o.ToString();
                        stringArray.SetValue(cellValue, col);
                    }
                    else//getting dynamic cols
                    {
                        foreach (GetEmployeeMasterOtherHeadersResult _Column in _dynamicTable)
                        {
                            //getting value based on dynamiccolumn column
                            //getColumnValueBasedOnKeyValue();

                            int ID = int.Parse(data[0].GetType().GetProperty("EmployeeId").GetValue(item, null).ToString());
                            string ValueList = data[0].GetType().GetProperty("List").GetValue(item, null).ToString();
                            string[] arrayValueList = ValueList.Split(';');
                            List<EmployeeExtraFieldsValuesBO> _ListEmployeeExtraFieldsValuesBO = new List<EmployeeExtraFieldsValuesBO>();
                            for (int i = 0; i < arrayValueList.Length; i++)
                            {
                                string[] arrayInnerValueList = arrayValueList[i].Split(':');
                                if (arrayInnerValueList.Length > 1)
                                {
                                    EmployeeExtraFieldsValuesBO _EmployeeExtraFieldsValuesBO = new EmployeeExtraFieldsValuesBO();
                                    _EmployeeExtraFieldsValuesBO.ID = ID;
                                    _EmployeeExtraFieldsValuesBO.TypeID = int.Parse(arrayInnerValueList[0]);
                                    _EmployeeExtraFieldsValuesBO.SourceID = int.Parse(arrayInnerValueList[1]);
                                    _EmployeeExtraFieldsValuesBO.Value = arrayInnerValueList[2];
                                    _ListEmployeeExtraFieldsValuesBO.Add(_EmployeeExtraFieldsValuesBO);
                                }
                            }

                            List<GetEmployeeMasterOtherHeadersResult> _ListExtraHeaderEmployee = EmployeeManager.GetEmployeeMasterHeader().Where(x => x.HeaderName.ToLower() == _Column.HeaderName.ToLower()).ToList();
                            int SourceID = _ListExtraHeaderEmployee[0].SourceId;
                            int Type = _ListExtraHeaderEmployee[0].Type;
                            String ColumnValue = "";
                            EmployeeExtraFieldsValuesBO _ValueList = _ListEmployeeExtraFieldsValuesBO.SingleOrDefault(x => x.ID == ID && x.SourceID == SourceID && x.TypeID == Type);
                            if (_ValueList != null)
                                ColumnValue = _ValueList.Value;
                            //get ExtraFields value;
                            GetDynamicColsvalue(ID.ToString());
                            stringArray.SetValue(ColumnValue, col);
                            col++;
                        }
                    }
                }
                _newObjList.Add(stringArray);
            }

            //---------------------------

            List<ColumnBase> columns = new List<ColumnBase>();
            GridFilters filterList = new GridFilters();
            ModelFieldCollection _ModelFieldCollection = new ModelFieldCollection();

            GetEmployeeMasterResult _SPParam = new GetEmployeeMasterResult();
            List<DynamicReportFieldsBO> _listFieldsStaticColumns = DynamicReportManager.GetDynamicReportAvilableFields(_SPParam, new List<string> { "" });
            List<DynamicReportFieldsBO> _listFieldsDynamicColumns = DynamicReportManager.GetDynamicFieldsProperty();

            List<DynamicReportFieldsBO> _listFields = new List<DynamicReportFieldsBO>();
            _listFields.AddRange(_listFieldsStaticColumns);
            _listFields.AddRange(_listFieldsDynamicColumns);

            List<string> _SelectedFields = new List<string>();

            List<DynamicReportFieldsBO> _LineList = new List<DynamicReportFieldsBO>();
            if (_listFields.Any())
            {
                List<DynamicReportFieldsBO> _SelectedFieldName = new List<DynamicReportFieldsBO>();
                string entryLineJson = e.Parameters["SelectedFieldsLineJson"];
                if (entryLineJson != "[]")
                {
                    _LineList = JSON.Deserialize<List<DynamicReportFieldsBO>>(entryLineJson);
                    _SelectedFields = (
                       (from _fields in _LineList
                        select _fields.FieldName).ToList()
                          );
                    _SelectedFieldName = _listFields.Where(i => !_SelectedFields.Contains(i.FieldName)).ToList();
                }
            }

            // -- start sorting ------------------------------------------------------------
            //if (e.Sort.Length > 0)
            //{
            //    _newObjList.Sort(delegate(object x, object y)
            //    {
            //        object a;
            //        object b;

            //        int direction = e.Sort[0].Direction == Ext.Net.SortDirection.DESC ? -1 : 1;
            //        a = x.GetType().GetProperty(e.Sort[0].Property).GetValue(x, null);
            //        b = y.GetType().GetProperty(e.Sort[0].Property).GetValue(y, null);
            //        return CaseInsensitiveComparer.Default.Compare(a, b) * direction;
            //    });
            //}

            //-- end sorting ------------------------------------------------------------
            //-- start paging ------------------------------------------------------------

            int eStartingPaging = 0;
            int PagingStart = 1;
            if (e.Start > 0)
            {
                PagingStart = int.Parse(e.Parameters["page"]);
                eStartingPaging = (storeDynamic.PageSize * (PagingStart - 1));
                //if(eStartingPaging>_newObjList.Count)
                //    eStartingPaging = (storeDynamic.PageSize * PagingStart-1);
            }

            //int limit = e.Limit;
            //if ((eStartingPaging + e.Limit) > _newObjList.Count)
            //{
            //    limit = data.Count - eStartingPaging;
            //}

            //List<Object> rangeData = (eStartingPaging < 0 || limit < 0) ? _newObjList : _newObjList.GetRange(eStartingPaging, limit);


            //-- end paging ------------------------------------------------------------

            if (X.IsAjaxRequest)
            {
                this.storeDynamic.RemoveFields();
                this.GridFilters2.ClearFilters();
            }


            foreach (DynamicReportFieldsBO _model in _LineList)
            {

                DynamicReportFieldsBO _FAMS_DynamicReportField = _listFields.SingleOrDefault(x => x.FieldName.ToLower() == _model.FieldName.ToLower());
                string ServerMappingField = _FAMS_DynamicReportField.ServerMappingFields;
                string DataType = _FAMS_DynamicReportField.DataType;

                ColumnBase _column = new Column();


                if (DataType.ToLower() == "date")
                {
                    _column = new DateColumn();
                    ((DateColumn)_column).Format = "MM/dd/yyyy";

                }
                _column.DataIndex = ServerMappingField;
                _column.Text = _model.FieldName;


                columns.Add(_column);

                if (DataType.ToLower() == "string")
                    storeDynamic.Fields.Add(ServerMappingField, "String");
                if (DataType.ToLower() == "float")
                    storeDynamic.Fields.Add(ServerMappingField, "Float");
                if (DataType.ToLower() == "date")
                    storeDynamic.Fields.Add(ServerMappingField, "Date");


                // gf.Local = true;

                ModelField _ModelField = new ModelField();
                _ModelField.Name = ServerMappingField;


                if (DataType.ToLower() == "string")
                    _ModelField.Type = ModelFieldType.String;
                if (DataType.ToLower() == "float")
                    _ModelField.Type = ModelFieldType.Float;
                if (DataType.ToLower() == "date")
                    _ModelField.Type = ModelFieldType.Date;

                this.AddField(_ModelField);



            }

            //  this.GridPanel1.Features.Add(filterList);

            if (_LineList.Any())
            {
                this.storeDynamic.RebuildMeta();
                // this.BindSet1();
                e.Total = _newObjList.Count();
                DataTable _Data = GetDataTable(_newObjList, _listFields, _ListColsToHide, _SelectedFields);
                if (e.Sort.Any())
                    _Data.DefaultView.Sort = e.Sort[0].Property + " " + e.Sort[0].Direction;
                //GridPanel1.GetStore().DataSource = GetDataTable(rangeData, _listFields, _ListColsToHide, _SelectedFields).DefaultView.Sort=sortColumn.ToString();
                GridPanel1.GetStore().DataSource = _Data;
                storeDynamic.DataBind();
                this.GridPanel1.ColumnModel.Columns.AddRange(columns);
                storeDynamic.DataBind();
                if (X.IsAjaxRequest)
                {
                    // this.GridPanel1.Features.Add(filterList);
                    this.GridPanel1.Reconfigure();
                    // X.AddScript(" if(storeDynamic!=null) storeDynamic.currentPage =" + PagingStart + ";");
                }
            }
        }


        private void GetDynamicColsvalue(string KeyValue)
        {

           string EmployeeID = KeyValue;

        }

        private void AddField(ModelField field)
        {
            if (X.IsAjaxRequest)
            {
                this.storeDynamic.AddField(field);
            }
            else
            {
                this.storeDynamic.Model[0].Fields.Add(field);
            }
        }

        private void AddFieldFilter(GridFilter _featuresItems)
        {
            if (X.IsAjaxRequest)
            {
                GridFilters2.Filters.Add(_featuresItems);

            }
            else
            {
                
                //this.storeDynamic.Model[0].Fields.Add(_featuresItems);
              //  this.GridPanel1.Features[0]
                 
            }
        }



        protected void btnViewShowReportTemplateWise_Click(object sender, DirectEventArgs e)
        {
            List<DynamicReportFieldsBO> _result = new List<DynamicReportFieldsBO>();
       
            string UserFileName = this.FileDocumentUpload.FileName;
            string ServerFileName = Guid.NewGuid().ToString() + Path.GetExtension(UserFileName);
            string relativePath =   HttpContext.Current.Server.MapPath( @"~/App_Data/DynamicReportTemplate/") + ServerFileName;
            if (this.UploadFile(relativePath))
            {
                double fileSize = FileDocumentUpload.PostedFile.ContentLength;


                using (StreamReader r = new StreamReader(relativePath))
                {
                    string line;
                    while ((line = r.ReadLine()) != null)
                    {
                        DynamicReportFieldsBO _list = new DynamicReportFieldsBO();
                        _list.FieldName = line;
                        _result.Add(_list);
                    }
                }
                gridSelectedFields.GetStore().DataSource = _result;
                gridSelectedFields.GetStore().DataBind();

                storeDynamic.LoadProxy();
                
               // X.AddScript("GetServiceList()");
                

            }

        }

        protected bool UploadFile(string relativePath)
        {

            if (this.FileDocumentUpload.HasFile)
            {

                int fileSize = FileDocumentUpload.PostedFile.ContentLength;
                this.FileDocumentUpload.PostedFile.SaveAs(relativePath);
                return true;
            }
            else
                return false;

        }


        protected void btnSearch_Click(object sender, DirectEventArgs e)
        {
            if (!string.IsNullOrEmpty(cmbGroupBy.SelectedItem.Value))
            {
                   // GroupingSummary1.Enable();
                    GroupingSummary2.Enable();
                    GridPanel1.GetStore().GroupField = cmbGroupBy.SelectedItem.Value;
                   
                
                   // grid.Reconfigure();
                   // grid.Refresh();
                    GridPanel1.Render();
            }

            GridPanel1.GetStore().Reload();
        }

        protected void btnSaveReportTemplate_Click(object sender, DirectEventArgs e)
        {

            string entryLineJson = e.ExtraParams["SelectedFieldsLineJson"];

            if (entryLineJson != "[]")
            {
                List<DynamicReportFieldsBO> _LineList = JSON.Deserialize<List<DynamicReportFieldsBO>>(entryLineJson);
                List<string> _ListToWrite = new List<string>();
                string Template = ("~/App_Data/DynamicReportTemplate.txt");

                string path = HttpContext.Current.Server.MapPath(Template);
                foreach (DynamicReportFieldsBO _list in _LineList)
                {
                    _ListToWrite.Add(_list.FieldName);
                }
                File.WriteAllLines(path, _ListToWrite.ToArray());

                Response.Redirect("~/DownLoadHandler.ashx?filename=DynamicReportTemplate.txt");
            }
        }

        #region "ABSTRACT-METHOD"


     
        #endregion
    }
}
