<%@ Page Title="Tax Details Summary" Language="C#" EnableViewState="true" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="TaxDetailsSummary.aspx.cs" Inherits="Web.TaxDetailsSummary" %>

<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<%@ Import Namespace="BLL.Manager" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        var skipLoadingCheck = true;

        var selEmpId = null;
        function ACE_item_selected(source, eventArgs) {
            var value = eventArgs.get_value();
            selEmpId = value;
            document.getElementById('<%= hiddenEmployeeID.ClientID %>').value = selEmpId;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Tax Calculation Summary
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="attribute">
            <table cellpadding="3" cellspacing="0" class="fieldTable">
                <tr>
                    <td class="filterHeader" runat="server" id="rowPayrollFrom1">
                        <strong>Payroll Period</strong>
                    </td>
                    <td class="filterHeader" runat="server" id="Td2">
                        <strong>Branch</strong>
                    </td>
                    <td class="filterHeader" runat="server" id="Td1">
                        <strong>Employee</strong>
                    </td>
                    <td rowspan="2" valign="bottom">
                        <asp:CheckBox runat="server" ID="chkHideRetired" Text="Hide Retired Employees" />
                    </td>
                    <td rowspan="2" valign="bottom">
                        <asp:Button ID="btnLoad" Style='float: left; width: 80px;' CssClass="btn btn-default btn-sm btn-sect"
                            runat="server" Text="Load" />
                    </td>
                </tr>
                <tr>
                    <td runat="server" id="rowPayrollFrom2">
                        <asp:DropDownList ID="ddlPayrollFromMonth" runat="server">
                        </asp:DropDownList>
                        <asp:DropDownList ID="ddlPayrollFromYear" runat="server">
                        </asp:DropDownList>
                    </td>
                    <td>

                        <asp:DropDownList ID="ddlBranch" Width="120px" DataTextField="Name" DataValueField="BranchId"
                                AppendDataBoundItems="true" runat="server">
                                <asp:ListItem Text="" Value="-1" />
                            </asp:DropDownList>
                    </td>
                    <td valign="bottom">
                        <asp:HiddenField ID="hiddenEmployeeID" runat="server" />
                        <asp:TextBox ID="txtEmpSearch" runat="server" AutoPostBack="true" Width="160" />
                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtEmpSearch"
                            WatermarkText="Search Employee" WatermarkCssClass="searchBoxText" />
                        <cc1:AutoCompleteExtender EnableCaching="true" ID="AutoCompleteExtenderOrganization"
                            runat="server" MinimumPrefixLength="2" ServiceMethod="GetEmployeeNamesAndEINWithRetiredAlso"
                            ServicePath="~/PayrollService.asmx" TargetControlID="txtEmpSearch" OnClientItemSelected="ACE_item_selected"
                            CompletionSetCount="10" CompletionInterval="250" CompletionListCssClass="AutoExtender"
                            CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
                        </cc1:AutoCompleteExtender>
                    </td>
                </tr>
            </table>
        </div>
        Salary should be saved for this report
        <div class="clear gridBlock">
            <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
            <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
            <cc2:EmptyDisplayGridView EnableViewState="false" CssClass="table table-primary mb30 table-bordered table-hover"
                UseAccessibleHeader="true" ShowHeaderWhenEmpty="True" ID="gvEmployeeIncome" runat="server"
                DataKeyNames="EIN" AutoGenerateColumns="False" CellPadding="4" GridLines="None"
                ShowFooterWhenEmpty="False" OnDataBound="gvw_DataBound">
                <Columns>
                    <asp:BoundField DataField="EIN" HeaderText="EIN"></asp:BoundField>
                    <asp:BoundField DataField="INo" HeaderText="INo"></asp:BoundField>
                    <asp:TemplateField HeaderText="Name" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                        HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:Label runat="server" Width="140" Text='<%# Eval("Name") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="AccountNo" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="Account No"></asp:BoundField>
                        <asp:BoundField DataField="PANNo" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="PAN No"></asp:BoundField>
                    <asp:BoundField DataField="Branch" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="Branch"></asp:BoundField>
                    <asp:BoundField DataField="Department" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="Department"></asp:BoundField>
                    <asp:BoundField DataField="LevelPosition" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="Position"></asp:BoundField>
                    <asp:BoundField DataField="Designation" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="Designation"></asp:BoundField>
                    <asp:BoundField DataField="Sex" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" HeaderStyle-HorizontalAlign="Left"
                        HeaderText="Sex"></asp:BoundField>
                    <asp:BoundField DataField="MaritalStatus" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="Marital Status"></asp:BoundField>
                    <asp:BoundField DataField="TaxStatus" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="Tax Status"></asp:BoundField>
                    <asp:BoundField DataField="InsuranceAmount" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="Insurance Amount" DataFormatString="{0:N2}" />
                    <asp:BoundField DataField="PastRegularIncomeAmount" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="Past Regular Income Amount" DataFormatString="{0:N2}" />
                    <asp:BoundField DataField="YearlyGrossIncome" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="Yearly Gross Income" DataFormatString="{0:N2}" />
                    <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="PFDeduction"
                        DataFormatString="{0:N2}" HeaderStyle-HorizontalAlign="Left" HeaderText="PF Deduction" />
                    <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="CITDeduction"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="CIT Deduction" DataFormatString="{0:N2}" />
                    <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="ProvisionalCITDeduction"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="Provisional CIT" DataFormatString="{0:N2}" />
                     <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="ExternalCIT"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="External CIT" DataFormatString="{0:N2}" />
                    <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="DeductForTax"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="Eligible Retirement Fund" DataFormatString="{0:N2}" />
                    <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="RemoteArea"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="Remote Area" DataFormatString="{0:N2}" />
                    <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="InsuranceDeduction"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="Insurance Deduction" DataFormatString="{0:N2}" />
                    <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="HealthInsurance"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="Health Insurance" DataFormatString="{0:N2}" />
                    <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="Handicapped"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="Handicapped Deduction" DataFormatString="{0:N2}" />
                    <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="TaxableAmount"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="Taxable Income" DataFormatString="{0:N2}" />
                    <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="OnePercentAmount"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="1% Amount" DataFormatString="{0:N2}" />
                    <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="FifteenPercentAmount"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="15% Amount" DataFormatString="{0:N2}" />
                    <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="TwentyFivePercentAmount"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="25% Amount" DataFormatString="{0:N2}" />
                    <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="FourtyPercentAmount"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="40% Amount" DataFormatString="{0:N2}" />
                    <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="MedicalTax"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="Medical Tax" DataFormatString="{0:N2}" />
                    <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="OnePercentTax"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="1% Tax" DataFormatString="{0:N2}" />
                    <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="FifteenPercentTax"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="15% Tax" DataFormatString="{0:N2}" />
                    <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="TwentyFivePercentTax"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="25% Tax" DataFormatString="{0:N2}" />
                    <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="FourtyPercentTax"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="40% Tax" DataFormatString="{0:N2}" />
                    <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="TotalTaxInTheYear"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="Total Tax" DataFormatString="{0:N2}" />
                    <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="FemaleRebate"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="Female Rebate" DataFormatString="{0:N2}" />
                    <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="AddOnSSTPaid"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="Add-On SST Paid" DataFormatString="{0:N2}" />
                    <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="AddOnTDSPaid"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="Add-On TDS Paid" DataFormatString="{0:N2}" />
                    <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="Adjustment"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="Adjustment" DataFormatString="{0:N2}" />
                    <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="TaxPayable"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="Tax Payable" DataFormatString="{0:N2}" />
                    <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="SSTPaid"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="SST Paid" DataFormatString="{0:N2}" />
                    <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="TDSPaid"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="TDS Paid" DataFormatString="{0:N2}" />
                    <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="ExcessTaxPaid"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="Excess Tax Paid" DataFormatString="{0:N2}" />
                </Columns>
                <EmptyDataTemplate>
                    <b>No employee list. </b>
                </EmptyDataTemplate>
            </cc2:EmptyDisplayGridView>
        </div>
        <div class="buttonsDiv">
            <asp:Button ID="btnUpdate" CssClass="excel" runat="server" Text="Export" OnClick="btnUpdate_Click" />
        </div>
    </div>
</asp:Content>
