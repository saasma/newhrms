﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using Utils.Base;
using BLL;
using DAL;
using Utils.Web;
using Utils.Helper;
using System.Text.RegularExpressions;
using Utils.Calendar;

namespace Web.CP
{
    public partial class AELeaveMannualEntered : BasePage
    {
        LeaveAttendanceManager leaveMgr = new LeaveAttendanceManager();
        int eId = 0;
       

        public int LeaveId
        {
            get
            {
                if (ViewState["LeaveId"] == null)
                    return 0;
                return int.Parse(ViewState["LeaveId"].ToString());
            }
            set
            {
                ViewState["LeaveId"] = value;
            }
        }

        public PageModeList PageModeList
        {
            get
            {
                if (ViewState["PageModeList"] == null)
                    return PageModeList.NotSet;
                return (PageModeList)ViewState["PageModeList"];
            }
            set
            {
                ViewState["PageModeList"] = value;
            }

        }
        void BindGrid()
        {
            gvw.DataSource = LeaveAttendanceManager.GetMannualLeaveList(eId, this.CustomId);
            gvw.DataBind();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            divWarningMsg.InnerHtml = Resources.Messages.LeaveFormNotEditableDuringIntialSaveMsg;

            eId = UrlHelper.GetIdFromQueryString("EId");
            
            if (!IsPostBack)
            {
                gender.InnerHtml = string.Format(gender.InnerHtml, CommonManager.GetGenderName);
                Initialise();
            }
          
            JavascriptCode();
           // JavascriptHelper.AttachEnableDisablingJSCode(chkMaxEncashOnRetirement, upMaxEnashBalance.ClientID, false);
            //chkMaxEncashOnRetirement.Attributes.Add("onclick", "maxEncashChange(this)");
        }
        public void gvw_RowCommand(object sender, GridViewCommandEventArgs e)
        {

        }

        
        void PreserveLapseCheckboxListSelection()
        {
            int leaveTypeId = UrlHelper.GetIdFromQueryString("Id");
            
            int LeaveId = UrlHelper.GetIdFromQueryString("LId");
            LLeaveType leave=null;
            LeaveAttendanceManager mgr = new LeaveAttendanceManager();
            if (leaveTypeId != 0)
                leave = mgr.GetLeaveType(leaveTypeId);
            else if (LeaveId != 0)
                leave = mgr.GetLeaveType(LeaveId);

           
        }


        void JavascriptCode()
        {
            //add javascript to enable/disable textbox related to CheckBox
            //JavascriptHelper.AttachCodeToDisableOther(chkIsBalanceUnlimited, txtMaxBalance);

            //JavascriptHelper.AttachEnableDisablingJSCode(rdbIsEncashable, divEncashment.ClientID, false);
            //JavascriptHelper.AttachPopUpCode(Page, "openingBalancePopup", "LeaveOpeningBalAsPopup.aspx", 650, 500);
            //JavascriptHelper.AttachEnableDisablingJSCode(rdbIsResetLeaveOn, chkResetMonths.ClientID, false);
            //reg javascript code setting url for leave opening balace popup
            //if (this.Id != 0)
            //{
            //    string code = "var queryString = 'EId={0}&LId={1}'";
            //    code = string.Format(code, eId, this.Id);
            //    ClientScript.RegisterClientScriptBlock(this.GetType(), "sdfdsfsdf1342", code, true);
            //}
        }
        public void btnCancel_Click(object sender, EventArgs e)
        {
            divAddLeave.Style["display"] = "none";
        }
        public void btnAdd_Click(object sender, EventArgs e)
        {
            divAddLeave.Style["display"] = "";
            Clear();
        }

        void Clear()
        {
            upDownPeriodValue.Value = 0;
            PayrollPeriod lastPeriod = CommonManager.GetLastPayrollPeriod();
            if (lastPeriod != null)
            {
                calPeriod.SetSelectedDate(lastPeriod.StartDate, IsEnglish);
            }
        }
        void Initialise()
        {

           // upMaxEnashBalance.DefaultValue = "";
            calPeriod.IsEnglishCalendar = this.IsEnglish;
            calPeriod.SelectTodayDate();
        
            BizHelper.Load(new HalfDayLeave(), ddlAllowHalfDay);
            ddlAllowHalfDay.ClearSelection();
            ddlAllowHalfDay.Items[2].Selected = true;
            BizHelper.Load(new LeaveType(), ddlLeaveType);
            //BizHelper.Load(new LeaveAccrueFrom(), ddlAccrueFrom);         
            //BizHelper.Load(new LeaveEncashment(), ddlEncashment);
           // ddlEncashmentMonth.Items.Clear();
            //ddlEncashmentMonth.DataSource = (DateManager.GetCurrentMonthList());
            //ddlEncashmentMonth.DataTextField = "Value";
            //ddlEncashmentMonth.DataValueField = "Key";
            //ddlEncashmentMonth.DataBind();
            ////ddlEncashmentMonth.Items.Insert(0, "-- Select Month --");
            //ListItem item = new ListItem("Retirement", "13".ToString());
            //ddlEncashmentMonth.Items.Add(item);

            JobStatus obJs = new JobStatus();
            //cblAppliesTo.Items.Clear();
            //cblAppliesTo.DataSource = obJs.GetMembers();
            //cblAppliesTo.DataTextField = "Value";
            //cblAppliesTo.DataValueField = "Key";
            //cblAppliesTo.DataBind();
            ////Remove first All Employees option
            //cblAppliesTo.Items.RemoveAt(0);

            ddlAppliesTo.DataSource = obJs.GetMembersForHirarchyView();
            ddlAppliesTo.DataBind();
            ////Remove first All Employees option
            //ddlAppliesTo.Items.RemoveAt(1);


       

           


            int leaveTypeId = UrlHelper.GetIdFromQueryString("Id");

           


            this.LeaveId = UrlHelper.GetIdFromQueryString("LId");

            if (this.LeaveId != 0)
            {
                leaveTypeId = this.LeaveId;             
            }

                 

            if (this.LeaveId != 0)
            {
                this.PageModeList = PageModeList.Update;
            }
            else if (eId != 0)
            {
                this.PageModeList = PageModeList.UpdateEmployee;

             
                leaveDetails.InnerHtml = EmployeeManager.GetEmployeeById(eId).Name + " : Leave History";

                

                LEmployeeLeave empLeave = leaveMgr.GetEmployeeLeave(leaveTypeId, eId);
                if (empLeave != null)
                    ProcessEmpLeave(empLeave);

                sectionEmployee.Style.Remove("display");
            }
            else
            {
                this.PageModeList = PageModeList.Insert;
                return;
            }


          
            if (leaveTypeId != 0)
            {
                LLeaveType entity = leaveMgr.GetLeaveType(leaveTypeId);
                if (entity != null)
                {
                    this.CustomId = leaveTypeId;

                    btnSave.Text = Resources.Messages.Update;

              


                    Process(entity);

                    
                }
            }

            BindGrid();
        }

        protected void ddlAccrue_SelectedIndexChanged(object sender, EventArgs e)
        {
            //rowLeavePerPeriod.Style.Add("display","none");
            //rowWorkdayIncludesLeaves.Style.Add("display", "none");
            //rowWorkDaysPerPeriod.Style.Add("display", "none");

            //if( ddlAccrue.SelectedValue==LeaveAccrue.MONTHLY ||
            //    ddlAccrue.SelectedValue == LeaveAccrue.YEARLY)
            //{
            //    rowLeavePerPeriod.Style.Remove("display");
            //}
            //if (ddlAccrue.SelectedValue==LeaveAccrue.Based_On_WorkDays )
            //{
            //    rowLeavePerPeriod.Style.Remove("display");
            //    rowWorkdayIncludesLeaves.Style.Remove("display");
            //    rowWorkDaysPerPeriod.Style.Remove("display");
            //}
            //else
            //{
            //    cblWorkDayInclude.Visible = false;
            //    lblWI.Visible = false;
            //    lblreqWI.Visible = false;
            //}
        }

        LLeaveType Process(LLeaveType entity)
        {
            if (entity != null)
            {
                hdEditSequence.Value = entity.EditSequence.ToString();
                txtTitle.Text = entity.Title;
             
                if (entity.AppliesToGender != null)
                    UIHelper.SetSelectedInDropDown(ddlAppliesToGender, entity.AppliesToGender);
              
                UIHelper.SetSelectedInDropDown(ddlLeaveType, entity.Type);

              



                foreach (LLeaveAppliesTo obLAT in entity.LLeaveAppliesTos)
                {
                    ddlAppliesTo.ClearSelection();
                    ListItem item = ddlAppliesTo.Items.FindByValue(obLAT.AppliesTo.ToString());
                    //ListItem item = cblAppliesTo.Items.FindByValue(obLAT.AppliesTo.ToString());
                    if (item != null)
                        item.Selected = true;
                }

                

                if (entity.IsHalfDayAllowed.Value)
                    ddlAllowHalfDay.SelectedIndex = 1;
                else
                    ddlAllowHalfDay.SelectedIndex = 2;

                if (entity.CountHolidayAsLeave != null)
                {
                    ddlCountHolidayAsLeave.SelectedValue = entity.CountHolidayAsLeave.Value.ToString().ToLower();
                }
                if (entity.PastDaysForLeaveRequest != null)
                {
                    txtAllowablePastDays.Text = entity.PastDaysForLeaveRequest.ToString();
                }

                
                
                {
                    if (entity.MannualMaximumPeriod != null)
                        upDownMannualMaxPeriod.Value = entity.MannualMaximumPeriod.Value;
                }

               

            }
          
            return null;
        }

        public  void EnableDisableValidators(bool isEnable,string unusedLeave)
        {
            

            if (isEnable)
            {
                EnabledChildValidationControls(this);
                //EnabledChildValidationControls(rowHalfDay);
            }
          
        }




        protected void btnOk_Click(object sender, EventArgs e)
        {

            //leave.LeaveTypeId = this.CustomId;
            ////for LEmployeeLeave
            //LEmployeeLeave empLeave = ProcessEmpLeave(null);

            //empLeave.EmployeeId = eId;

            MannualLeaveAccure leave = new MannualLeaveAccure();
            leave.EmployeeId = eId;
            leave.LeaveTypeId = this.CustomId;
            leave.Period = upDownPeriodValue.Value;

            CustomDate date = calPeriod.SelectedDate;
            leave.Month = date.Month;
            leave.Year = date.Year;
            leave.Notes = txtNotes.Text.Trim();

            CustomDate firstDay = new CustomDate(1, date.Month, date.Year, IsEnglish);
            leave.Date = firstDay.EnglishDate;

            //double? total = 0;
            // remove this validation as mannual leave like Mourning if defined 15 then multiple 15 days could be taken
            //if (LeaveAttendanceManager.IsMannualLeaveAssignedValidUsingTotalMaxBal(leave.EmployeeId, leave.Month, leave.Year, leave.LeaveTypeId, leave.Period, ref total) == false)
            //{
            //    divWarningMsg.InnerHtml = total + " total balance is greater than the maximum period.";
            //    divWarningMsg.Hide = false;
            //    return;
            //}

            Status status = LeaveAttendanceManager.SaveUpdateMannualAccural(leave);
            if (status.IsSuccess)
            {
                divMsg.InnerHtml = "Balance saved.";
                divMsg.Hide = false;
                divAddLeave.Style["display"] = "none";
            }
            

            BindGrid();
        }



        LEmployeeLeave ProcessEmpLeave(LEmployeeLeave leave)
        {
            if (leave == null)
            {
                leave = new LEmployeeLeave();
                
                    leave.Accured = 0;
                leave.EditSequence = int.Parse(hdEmployeeEditSequence.Value == "" ? "0" : hdEmployeeEditSequence.Value) + 1;
                return leave;
            }
            else
            {
                //txtBeginningBalance.Text = leave.BeginningBalance.ToString();
              
                hdEmployeeEditSequence.Value = leave.EditSequence.ToString();
            }
            return null;
        }


       
        private void EnableDisableControls()
        {
            //check if leave used in the attendance or appear in leave adjustment then disable some fields
            if (this.CustomId != 0 && LeaveAttendanceManager.IsLeaveUsedInAttendance(this.CustomId))
            {
                this.ddlLeaveType.Enabled = false;
               
            }
        }

        protected void gvw_SelectedIndexChanged(object sender, EventArgs e)
        {
            int month =(int) gvw.DataKeys[gvw.SelectedIndex]["Month"];
            int year = (int)gvw.DataKeys[gvw.SelectedIndex]["Year"];

            MannualLeaveAccure leave = LeaveAttendanceManager.GetMannualLeave(
                eId, this.CustomId, month, year);

            divAddLeave.Style["display"] = "";

            if (leave != null)
            {
                CustomDate date = new CustomDate(1, month, year, IsEnglish);
                calPeriod.SetSelectedDate(date.ToString(), IsEnglish);

                upDownPeriodValue.Value = leave.Period;
                txtNotes.Text = leave.Notes;
            }
        }

    

        //protected void ddlUnusedLeave_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    DisplayUnusedLeavePanel();
        //}

      

    }
}
