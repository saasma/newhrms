<%@ Page Title="" Language="C#" MasterPageFile="~/Master/ForPopupPage.Master" AutoEventWireup="true"
    CodeBehind="LocationHistory.aspx.cs" Inherits="Web.CP.LocationHistory" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">
        function closePopup() {
            
            window.returnValue = value;
            window.close();            
            
        }
<%
        Page.Response.Write("window.onunload = closePopup;");
        %>
        
         function clearUnload() {
            window.onunload = null;
        }
        

        //validate Salary calculation date
        function validateSalaryCalFrom(source, args) {
            args.IsValid = true;
            var currentFromDate = '<%= calFromDate.ClientID %>';



            var currentFromDateValue = getCalendarSelectedDate(currentFromDate);

            if (typeof (fromDate) == 'undefined') {
                return true;
            }
           
            if (isSecondCalendarCtlDateGreater(fromDate, currentFromDateValue))
                args.IsValid = true;
            else {
                args.IsValid = false;
                alert(String.format('Current from date must be greater that last from date "{0}".',fromDate));
            }


        }

        
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <div class="popupHeader">
       
          <h3>  Location history</h3>
    </div>
    <div class=" marginal">
        <asp:GridView ID="gvw" Width="100%" DataKeyNames="LocationHistoryId" PagerStyle-HorizontalAlign="Center"
            PagerStyle-CssClass="defaultPagingBar" AllowPaging="true" runat="server" AutoGenerateColumns="False"
            CellPadding="4" GridLines="None" PageSize="10" 
            OnPageIndexChanging="gvw_PageIndexChanging" 
            onselectedindexchanged="gvw_SelectedIndexChanged">
            <Columns>
                <asp:TemplateField HeaderText="Location" HeaderStyle-Width="120px" HeaderStyle-HorizontalAlign="Left"
                    ItemStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <%# Eval("ELocation.Name")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="FromDate" HeaderStyle-Width="120px" HeaderStyle-HorizontalAlign="Left"
                    HeaderText="From" />
                <asp:TemplateField HeaderText="Edit" HeaderStyle-Width="100px" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <asp:ImageButton  Visible='<%# IsEditable(Container.DataItemIndex) %>' ID="ImageButton1" runat="server" CommandName="Select" ImageUrl="~/images/edit.gif" />
                    </ItemTemplate>
                    <HeaderStyle Width="30px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:TemplateField>
                <asp:TemplateField></asp:TemplateField>
            </Columns>
            <RowStyle CssClass="odd" />
            <AlternatingRowStyle CssClass="even" />
            <SelectedRowStyle CssClass="selected" />
            <PagerStyle HorizontalAlign="Center" CssClass="defaultPagingBar"></PagerStyle>
            <EmptyDataTemplate>
                <b>No Location history. </b>
            </EmptyDataTemplate>
        </asp:GridView>
          <uc2:MsgCtl Width='' ID="divMsgCtl"
            EnableViewState="false" Hide="true" runat="server" />
        <uc2:WarningCtl Width='' ID="divWarningMsg"
            EnableViewState="false" Hide="true" runat="server" />
        <fieldset style='margin-top: 10px'>
            <legend style='font-size:14px'>Location information</legend>
            <table>
                <tr>
                    <td class="fieldHeader" style='width: 80px!important'>
                        Location
                    </td>
                    <td>
                        <asp:DropDownList Width="175px" Style='vertical-align: top' CssClass="clasificaionElement" 
                            ID="ddlGrade" runat="server" DataValueField="LocationId" DataTextField="Name" AppendDataBoundItems="true">
                            <asp:ListItem Value="-1" Text=""></asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlGrade"
                            Display="None" ErrorMessage="Location is required." InitialValue="-1" ValidationGroup="AEEmployee"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="fieldHeaderLabel">
                        From date
                    </td>
                    <td>
                        <My:Calendar Id="calFromDate" runat="server" />
                        <asp:TextBox Text="sdf" ID="txtForEmptyValidate" style='position:absolute;top:-1000px;left:-1000px' runat="server" />
                        <asp:CustomValidator ValidateEmptyText="true" ID="valCustomeSalCalFrom1" runat="server"
                            ControlToValidate="txtForEmptyValidate" ValidationGroup="AEEmployee" Display="None"
                            ErrorMessage=""
                            ClientValidationFunction="validateSalaryCalFrom" />
                    </td>
                </tr>
                
                <tr>
                <td colspan="2">
                
                <asp:Button ID="btnSave" OnClientClick="valGroup='AEEmployee';return CheckValidation();" runat="server" CssClass="save" Text="Save" 
                        onclick="btnSave_Click" />
                        
                  <asp:Button ID="btnCancel"   runat="server" CssClass="cancel" Text="Cancel" 
                        onclick="btnCancel_Click" />      
                </td>
                </tr>
            </table>
        </fieldset>
    </div>
</asp:Content>
