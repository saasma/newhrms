<%@ Page Title="Employee Master" Language="C#" EnableViewState="false" MasterPageFile="~/Master/Details.Master"
    AutoEventWireup="true" CodeBehind="EmployeeMasterD2.aspx.cs" Inherits="Web.EmployeeMasterD2" %>

<%@ Import Namespace="BLL.Manager" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        var skipLoadingCheck = true;
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <h3>
        Detailed Employee Report</h3>
    <div class="clear gridBlock">
        <asp:Panel ID="Panel1" runat="server" class="attribute" DefaultButton="btnLoad">
            <%--  <asp:UpdatePanel ID="upd" runat="server" UpdateMode="Conditional">
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnLoad" />
                </Triggers>
                <ContentTemplate>--%>
            <table>
                <tr>
                    <td>
                        <strong>Division</strong>
                    </td>
                    <td>
                        <strong>Job Code</strong>
                    </td>
                    <td>
                        <strong>Job Tile</strong>
                    </td>
                    <td>
                        <strong>Gender</strong>
                    </td>
                    <td>
                        <strong>Employee Name</strong>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    
                  
                    <td>
                        <asp:DropDownList ID="ddlDepartment" DataTextField="Name" DataValueField="DepartmentId"
                            runat="server" AppendDataBoundItems="true" Width="120px" >
                            <asp:ListItem Value="-1" Text=""></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlJobCode"  DataValueField="Code" DataTextField="Code"
                            runat="server" AppendDataBoundItems="true" Width="120px">
                            <asp:ListItem Value="" Text=""></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                   
                   <td>
                        <asp:DropDownList ID="ddlJobTitle" DataValueField="DesignationId" DataTextField="Name"
                            runat="server" AppendDataBoundItems="true" Width="120px">
                            <asp:ListItem Value="-1" Text=""></asp:ListItem>
                        </asp:DropDownList>
                    </td>

                    <td>
                        <asp:DropDownList ID="ddlGender" 
                            runat="server" AppendDataBoundItems="true" Width="120px">
                            <asp:ListItem Value="-1" Text=""></asp:ListItem>
                            <asp:ListItem Value="1" Text="Male" />
                            <asp:ListItem Value="0" Text="Female" />
                        </asp:DropDownList>
                    </td>
                    <td>
                       <asp:TextBox ID="txtEmpSearch" runat="server" />
                         <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtEmpSearch"
                                WatermarkText="Search Text" WatermarkCssClass="searchBoxText" />
                            <cc1:AutoCompleteExtender EnableCaching="true" ID="AutoCompleteExtenderOrganization"
                                runat="server" MinimumPrefixLength="2" ServiceMethod="GetEmployeeNames" ServicePath="~/PayrollService.asmx"
                                TargetControlID="txtEmpSearch"   CompletionSetCount="10"
                                CompletionInterval="250" CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList"
                                CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
                            </cc1:AutoCompleteExtender>
                         </td>
                    <td valign="top">
                        <asp:Button ID="btnLoad" CssClass="load" OnClick="btnLoad_Click" runat="server" Text="Load" />
                    </td>
                   
                </tr>
            </table>
            <%--</ContentTemplate>
            </asp:UpdatePanel>--%>
        </asp:Panel>
     <div style='clear:both'/>
        <cc2:EmptyDisplayGridView CssClass="tableLightColor" UseAccessibleHeader="true" ShowHeaderWhenEmpty="True"
            ID="gvD2EmployeeGrid" runat="server" DataKeyNames="" AutoGenerateColumns="False"
            CellPadding="4" GridLines="None" AllowSorting="True" ShowFooterWhenEmpty="False">
            <RowStyle BackColor="#E3EAEB" />
            <Columns>
                <asp:BoundField DataField="Country" HeaderText="Country"></asp:BoundField>
                <asp:BoundField DataField="Company" HeaderText="Company"></asp:BoundField>
                <asp:BoundField DataField="ICard" HeaderStyle-HorizontalAlign="Left" HeaderText="I#">
                </asp:BoundField>
                <asp:BoundField DataField="FirstName" HeaderStyle-HorizontalAlign="Left" HeaderText="First Name">
                </asp:BoundField>
                <asp:BoundField DataField="LastName" HeaderStyle-HorizontalAlign="Left" HeaderText="Last Name">
                </asp:BoundField>
                <asp:BoundField DataField="HireDate" DataFormatString="{0:MM/dd/yyyy}" HeaderStyle-HorizontalAlign="Right"
                    HeaderText="Hire Date"></asp:BoundField>
                <asp:BoundField DataField="AnnualRate" HeaderStyle-HorizontalAlign="Left" HeaderText="Annual Rate">
                </asp:BoundField>
                <asp:BoundField DataField="Currency" HeaderStyle-HorizontalAlign="Left" HeaderText="Currency">
                </asp:BoundField>
                <asp:BoundField DataField="DepartmentCode" HeaderStyle-HorizontalAlign="Left" HeaderText="Divcd">
                </asp:BoundField>
                <asp:BoundField DataField="Department" HeaderStyle-HorizontalAlign="Left" HeaderText="Division Name">
                </asp:BoundField>
                <asp:BoundField DataField="OfficeLocation" HeaderStyle-HorizontalAlign="Left" HeaderText="Office Location">
                </asp:BoundField>
                <asp:BoundField DataField="DesignationCode" HeaderStyle-HorizontalAlign="Left" HeaderText="Job Code">
                </asp:BoundField>
                <asp:BoundField DataField="Designation" HeaderStyle-HorizontalAlign="Left" HeaderText="Job Title in Worksheet">
                </asp:BoundField>
                <asp:BoundField DataField="FullPartTime" HeaderStyle-HorizontalAlign="Left" HeaderText="Full/Part Time">
                </asp:BoundField>
                <asp:BoundField DataField="CompPhone" HeaderStyle-HorizontalAlign="Left" HeaderText="Comp Phone">
                </asp:BoundField>
                <asp:BoundField DataField="CompPhoneExt" HeaderStyle-HorizontalAlign="Left" HeaderText="Comp Phone Ext">
                </asp:BoundField>
                <asp:BoundField DataField="CompEmail" HeaderStyle-HorizontalAlign="Left" HeaderText="Comp E-Mail">
                </asp:BoundField>
                <asp:BoundField DataField="MgrID" HeaderStyle-HorizontalAlign="Left" HeaderText="Mgr ID">
                </asp:BoundField>
                <asp:BoundField DataField="MgrName" HeaderStyle-HorizontalAlign="Left" HeaderText="MgrName">
                </asp:BoundField>
                <asp:BoundField DataField="HomeAddress" HeaderStyle-HorizontalAlign="Left" HeaderText="Home Address">
                </asp:BoundField>
                <asp:BoundField DataField="City" HeaderStyle-HorizontalAlign="Left" HeaderText="City">
                </asp:BoundField>
                <asp:BoundField DataField="HomePhone" HeaderStyle-HorizontalAlign="Left" HeaderText="Home Phone">
                </asp:BoundField>
                <asp:BoundField DataField="PanNationalID" HeaderStyle-HorizontalAlign="Left" HeaderText="National ID #">
                </asp:BoundField>
                <asp:BoundField DataField="NationalIDDesc" HeaderStyle-HorizontalAlign="Left" HeaderText="National ID Description">
                </asp:BoundField>
                <asp:BoundField DataField="DOB" DataFormatString="{0:MM/dd/yyyy}" HeaderStyle-HorizontalAlign="Left"
                    HeaderText="DOB"></asp:BoundField>
                <asp:BoundField DataField="BirthCountry" HeaderStyle-HorizontalAlign="Left" HeaderText="Birth Country (optional)">
                </asp:BoundField>
                <asp:BoundField DataField="Gender" HeaderStyle-HorizontalAlign="Left" HeaderText="Gender">
                </asp:BoundField>
                <asp:BoundField DataField="MaritalStatus" HeaderStyle-HorizontalAlign="Left" HeaderText="Marital Status">
                </asp:BoundField>
            </Columns>
            <RowStyle CssClass="odd" />
            <AlternatingRowStyle CssClass="even" />
            <EmptyDataTemplate>
                <b>No employee list. </b>
            </EmptyDataTemplate>
        </cc2:EmptyDisplayGridView>
    </div>
    <div class="buttonsDiv">
        <asp:Button ID="btnUpdate" CssClass="update" runat="server" Text="Export" OnClick="btnUpdate_Click" />
    </div>
</asp:Content>
