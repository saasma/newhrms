<%@ Page Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/Master/ForPopupPage.Master"
    AutoEventWireup="true" CodeBehind="CurrentPayIncomeSelection.aspx.cs" Title="Current Pay Incomes" Inherits="Web.CP.CurrentPayIncomeSelection" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Assembly="Karpach.WebControls" Namespace="Karpach.WebControls" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/UpDownCtl.ascx" TagName="UpDownCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="InfoMsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .colorStyle td
        {
            padding: 2px !important;
        }
        .transparent
        {
            /* this class makes a window partially transparent */
            opacity: 0; /* Standard style for transparency */
            -moz-opacity: 0; /* Transparency for older Mozillas */
            filter: alpha(opacity=00); /* Transparency for IE */
            width: 0;
        }
        .paddinAll
        {
            padding: 10px;
        }
        div.notify
        {
            margin-left: 0;
            width: 41%;
        }
        .lf
        {
            width: 150px;
            padding: 0 0px 0px 0;
        }
        .lfs
        {
            width: 160px;
            padding: 0 10px 0px 0;
            vertical-align: top;
        }
        table tr td
        {
            padding-bottom: 10px;
        }
        .bevel
        {
            margin-bottom: 20px;
        }
        
        div.notify
        {
            width: 510px;
        }
        .displayInline
        {
            display: inline;
        }
    </style>
    <script type="text/javascript">
        function closePopup() {
            // alert(window.opener.parentReloadCallbackFunction)
            if ($.browser.msie == false && typeof (window.opener.reloadLeave) != 'undefined' && ($.browser.chrome || $.browser.safari)) {
                window.opener.reloadLeave(window);
            } else {
                window.returnValue = "Reload";
                window.close();
            }

        }


        
     
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <asp:HiddenField ID="hdEditSequence" runat="server" />
    <asp:HiddenField ID="hdEmployeeEditSequence" runat="server" />
    <div class="popupHeader">
        <h3>
            Current Pay Incomes</h3>
    </div>
    <div class="marginal" style='margin-top: 10px'>
        <uc2:InfoMsgCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
        <asp:HiddenField ID="hdGradeId" runat="server" Value="0" />
        <asp:HiddenField ID="hdSaveMode" runat="server" Value="0" />
      
        <div class="bevel paddinAll" style="width: 600px; margin-bottom: 5px!important">
            <table cellpadding="0" cellspacing="0" width="600px">
                <tr>
                    
                    <td>
                        <strong>Select Incomes</strong>
                        <div style="padding-left: 0px">
                            <asp:CheckBoxList ID="chkListSalaries" RepeatColumns="3" RepeatDirection="Horizontal"
                                runat="server" DataTextField="Title" DataValueField="IncomeId">
                            </asp:CheckBoxList>
                        </div>
                    </td>
                </tr>
               
            </table>
        </div>
        <div class="clear" style='text-align: right; padding-right: 30px;'>
            <asp:Button ID="btnSave" CssClass="save" runat="server" OnClientClick="valGroup='Grade';return CheckValidation()"
                OnClick="btnOk_Click" Text="Save" ValidationGroup="Leave" />
            <asp:Button ID="btnCancel" runat="server" CssClass="cancel" Text="Cancel" OnClientClick="window.close()" />
        </div>
    </div>
</asp:Content>
