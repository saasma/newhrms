﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using DAL;
using BLL;
using Utils.Web;
using Utils.Helper;
using Utils.Calendar;
namespace Web.CP
{
    public partial class EMICalculation : BasePage
    {
        CalculationConstant calculationConstants;
        PayManager payMgr = new PayManager();
        public bool HasVariableAmount
        {
            get
            {
                return Boolean.Parse(ViewState["hasVariableAmount"].ToString());
            }
            set
            {
                ViewState["hasVariableAmount"] = value;
            }
        }   

        public string Type
        {
            get
            {
                return ViewState["Type"].ToString();
            }
            set
            {
                ViewState["Type"] = value;
            }
        }

        public bool IsEMI
        {
            get
            {
                int empDedId = UrlHelper.GetIdFromQueryString("EmpDedId");
                PEmployeeDeduction ded = new PayManager().GetEmployeeDeduction(empDedId);
                if (ded != null)
                {
                    if (ded.PDeduction.IsEMI == null || ded.PDeduction.IsEMI == false)
                        return false;
                    return true;
                }

                if (ViewState["IsEMI"] == null)
                    return true;
                return Convert.ToBoolean(ViewState["IsEMI"].ToString());
            }
            set
            {
                ViewState["IsEMI"] = value;
            }
        }

       
        protected void Page_Load(object sender, EventArgs e)
        {

            
            //if deduction not in session then don't load the page
            if (SessionManager.EmployeeDeduction == null)
            {
                warningMsg.InnerHtml = Resources.Messages.EMIReloadMsg;
                warningMsg.Hide = false;
                btnCalculate.Enabled = false;
                return;
            }
            else
                btnCalculate.Enabled = true;


            infoMsg.InnerHtml = Resources.Messages.ValidPayrollPeriodNotDefined;
            //warningMsg.InnerHtml = Resources.Messages.EMIInstallmentRemMsg;

            if (!IsPostBack)
                Initialise();

            string code = "var hasVariableAmount = ";
            if( HasVariableAmount)
                code += "true;";
            else
                code += "false;";

            if (HasVariableAmount)
                txtAmount.Attributes.Remove("onchange");

            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdfdsf23", code, true);

           // JavascriptHelper.AttachEnableDisablingJSCode(chkIsFixedInstallmentAmount, txtFixedInstallmentAmount.ClientID, false, "setFixedInstallment");


            Register();
        }

        /// <summary>
        /// Register javascript like current payroll period starting date to validate for reterospect increment
        /// </summary>
        protected void Register()
        {
            PayrollPeriod payrollPeriod = CommonManager.GetValidLastPayrollPeriod();

            if (payrollPeriod != null)
            {
                string code = string.Format("var payrollStartDate = '{0}';", payrollPeriod.StartDate);
                              
               

                Page.ClientScript.RegisterClientScriptBlock(
                    this.GetType(), "DateReg123", code, true);

            }
            else
            {
                btnCalculate.Enabled = false;
                //btnCalculate.ToolTip = Resources.Messages.ValidPayrollPeriodNotDefined;
                infoMsg.Hide = false;
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (chkIsFixedInstallmentAmount.Checked)
            {
                txtToBePaidOver.CssClass = "disabledTxt";
                txtFixedInstallmentAmount.CssClass = "";
            }
            else
            {
                txtToBePaidOver.CssClass = "";
                txtFixedInstallmentAmount.CssClass = "disabledTxt";
            }

            if (this.Type == DeductionCalculation.LOAN && !this.IsEMI)
            {
                cellInterest1.Visible = false;
                cellInterest2.Visible = false;

                ddlPaymentTerms.ClearSelection();
                ListItem item = ddlPaymentTerms.Items[1];
                item.Selected = true;
                
                ddlPaymentTerms.Enabled = false;
            }
            if (SessionManager.EmployeeDeduction == null)
            {
            }
            else
            {
                int empDedId = UrlHelper.GetIdFromQueryString("EmpDedId");
                if (empDedId != 0)
                {

                    PEmployeeDeduction empDeduction = payMgr.GetEmployeeDeduction(empDedId);
                    if (empDeduction != null &&
                        (empDeduction.RemainingInstallments != 0 && empDeduction.RemainingInstallments != empDeduction.NoOfInstallments)
                        )
                    {
                        if (empDeduction.PDeduction.Calculation == DeductionCalculation.LOAN)
                        {
                            warningMsg.InnerHtml = Resources.Messages.EMIInstallmentRemLoanMsg;


                        }
                        else
                            warningMsg.InnerHtml = Resources.Messages.EMIInstallmentRemAdvanceMsg;

                        warningMsg.Hide = false;
                    }

                    divFixedInstallment.Visible = false;
                    if(empDeduction.PDeduction.Calculation==DeductionCalculation.LOAN
                        && (empDeduction.PDeduction.IsEMI == null || empDeduction.PDeduction.IsEMI == false))
                    {
                        divFixedInstallment.Visible = true;
                    }
                }
            }
        }

        void Initialise()
        {        
            string type = Request.QueryString["type"];
            string value = Request.QueryString["hasVariableAmount"];
            bool hasVariableAmount;
            if (Boolean.TryParse(value, out hasVariableAmount))
            {
                this.HasVariableAmount = hasVariableAmount;
            }
            else
            {
                return;
            }
            if (string.IsNullOrEmpty(type))
            {
                return;
            }

            BizHelper.Load(new EMILoadPaymentTerm(), ddlPaymentTerms);
            ddlMonths.DataSource = DateManager.GetCurrentMonthList();
            ddlMonths.DataBind();
            ddlYears.DataSource = DateManager.GetYearListForPayrollPeriod();
            ddlYears.DataBind();
            calTakenOn.IsEnglishCalendar = SessionManager.CurrentCompany.IsEnglishDate;
            calTakenOn.SelectTodayDate();

            //set in start month also
            ddlMonths.ClearSelection(); ;
            ListItem item = ddlMonths.Items.FindByValue(calTakenOn.SelectedDate.Month.ToString());
            if (item != null)
                item.Selected = true;
            ddlYears.ClearSelection();
            item = ddlYears.Items.FindByValue(calTakenOn.SelectedDate.Year.ToString());
            if (item != null)
                item.Selected = true;

            if (type == DeductionCalculation.Advance)
            {
                Type = type;
                PrepareForAdvance();
            }
            else if (type == DeductionCalculation.LOAN)
            {
                Type = type;
                PrepareForLoan();
            }
            //If has variable amount then hide,disable controls
            if (this.HasVariableAmount)
            {
                rowInterestRate.Visible = false;
                DisableChildControls(rowInterestRate);
                rowToBePaidOver.Visible = false;
                DisableChildControls(rowToBePaidOver);
                rowStartingFrom.Visible = false;
                DisableChildControls(rowStartingFrom);
                btnCalculate.Visible = false;
                tbl.Visible = false;
                rowPaymentTerms.Visible = false;
                rowMarketRate.Visible = false;
                btnOk.Enabled = true;
            }

            int empDedId =  UrlHelper.GetIdFromQueryString("EmpDedId");
            if (empDedId != 0)
            {

                 PEmployeeDeduction ded = payMgr.GetEmployeeDeduction(empDedId);
                 if (ded.IsValid != null && ded.IsValid.Value)
                 {
                     if (ded.PDeduction.IsEMI == null || ded.PDeduction.IsEMI.Value)
                     {
                         this.IsEMI = true;
                     }
                     else
                     {
                         this.IsEMI = false;
                         ddlPaymentTerms.ClearSelection();
                         item = ddlPaymentTerms.Items[1];
                         item.Selected = true;

                         ddlPaymentTerms.Enabled = false;
                     }


                     PEmployeeDeduction empDeduction = null;
                     if (SessionManager.EmployeeDeduction != null)
                         empDeduction = SessionManager.EmployeeDeduction;
                     else
                         empDeduction = payMgr.GetEmployeeDeduction(empDedId);

                     txtAmount.Text = GetCurrency(empDeduction.AdvanceLoanAmount);
                     calTakenOn.SetSelectedDate(empDeduction.TakenOn, SessionManager.CurrentCompany.IsEnglishDate);
                     txtInterestRate.Text = empDeduction.InterestRate.ToString();
                     txtMarketRate.Text = empDeduction.MarketRate.ToString();

                     if (hasVariableAmount == false)
                     {
                         txtToBePaidOver.Text = empDeduction.ToBePaidOver.ToString();
                         if (empDeduction.LoanFixedInstallmentAmount != null && empDeduction.LoanFixedInstallmentAmount.Value)
                         {
                             chkIsFixedInstallmentAmount.Checked = true;
                             if (empDeduction.SINewPPMTAmount != null)
                             {
                                 txtFixedInstallmentAmount.Text = GetCurrency(empDeduction.SINewPPMTAmount);

                                 // if SINewPPMTAmount is 0 shomehow then set installment amount
                                 if (Convert.ToDecimal( empDeduction.SINewPPMTAmount) == 0)
                                     txtFixedInstallmentAmount.Text = GetCurrency(empDeduction.InstallmentAmount);
                             }
                         }
                             // then also fixed installemnt amount
                         else if (Convert.ToDecimal(empDeduction.SINewPPMTAmount) != 0)
                         {
                             chkIsFixedInstallmentAmount.Checked = true;
                             txtFixedInstallmentAmount.Text = GetCurrency(empDeduction.SINewPPMTAmount);
                         }

                         CustomDate date = CustomDate.GetCustomDateFromString(empDeduction.StartingFrom, SessionManager.CurrentCompany.IsEnglishDate);

                         if (date != null)
                         {
                             UIHelper.SetSelectedInDropDown(ddlMonths, date.Month);
                             UIHelper.SetSelectedInDropDown(ddlYears, date.Year);
                         }
                         if (type == DeductionCalculation.LOAN)
                         {
                             txtInterestRate.Text = empDeduction.InterestRate.ToString();
                             txtMarketRate.Text = empDeduction.MarketRate.ToString();
                             UIHelper.SetSelectedInDropDown(ddlPaymentTerms, empDeduction.PaymentTerms);
                             CalculateLoan();
                         }
                         else
                         {
                             CalculateAdvance();
                         }

                     }
                 }
            }
            else
                LoadFromSession();
        }

        void LoadFromSession()
        {
            PEmployeeDeduction ded = SessionManager.EmployeeDeduction;
            if (ded != null)
            {
                txtAmount.Text = GetCurrency(ded.AdvanceLoanAmount);
                calTakenOn.SetSelectedDate(ded.TakenOn,SessionManager.CurrentCompany.IsEnglishDate);
                UIHelper.SetText(txtInterestRate, ded.InterestRate);
            }
        }

        void PrepareForLoan()
        {

            lblAmountText.Text = Resources.Messages.LoanAmount;
            lblMonthsOrInstallments.Text = Resources.Messages.Installments;
            valReqdPaidOver.ErrorMessage = string.Format(valReqdPaidOver.ErrorMessage, lblMonthsOrInstallments.Text);
            valCompPaidOver.ErrorMessage = string.Format(valCompPaidOver.ErrorMessage, lblMonthsOrInstallments.Text);
        }

        void PrepareForAdvance()
        {
            lblAmountText.Text = Resources.Messages.AdvanceAmount;
            lblMonthsOrInstallments.Text = Resources.Messages.Months;
            valReqdPaidOver.ErrorMessage = string.Format(valReqdPaidOver.ErrorMessage, lblMonthsOrInstallments.Text);
            valCompPaidOver.ErrorMessage = string.Format(valCompPaidOver.ErrorMessage, lblMonthsOrInstallments.Text);
            rowInterestRate.Visible = false;
            DisableChildControls(rowInterestRate);
            cellInterest1.Visible = false;
            cellInterest2.Visible = false;
            rowMarketRate.Visible = false;
            rowPaymentTerms.Visible = false;
        }

        protected void btnCalculate_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                if (this.Type == DeductionCalculation.LOAN)
                {
                    bool status = CalculateLoan();
                    if(status)
                    btnOk.Enabled = true;
                    
                }
                else
                {

                    if (CalculateAdvance() == false)
                        return;
                    btnOk.Enabled = true;
                }
            }
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msdgd123", "calculated=true", true);
        }
             


        //If Previous parameters & new parameters are same then remaining installment is same as could have been 
        //modified from salary generation else new parameters set so remaining will be same as ToBePaidOver as just started
        void CheckAndSetForRemainingInstallment(PEmployeeDeduction p, PEmployeeDeduction n)
        {
            PEmployeeDeduction db = payMgr.GetEmployeeDeduction(UrlHelper.GetIdFromQueryString("EmpDedId"));

            //set saved paramters
            p.PDeduction = db.PDeduction;
            n.PDeduction = db.PDeduction;
            p.IsValid = db.IsValid;
            n.IsValid = db.IsValid;


            if (p.IsValid != null && p.IsValid.Value != false)
            {
                if (PEmployeeDeduction.GetUniqueId(p)
                    == PEmployeeDeduction.GetUniqueId(n))
                {


                    int remInstallment = 0;
                    decimal paid = 0;

                    //PEmployeeDeduction.GetPaidAndRemInstallmentForDeduction(p, ref remInstallment, ref paid);
                    hfRemainingInstallment.Value = db.RemainingInstallments.ToString();
                    hfPaid.Value = GetCurrency(db.Paid);
                    hfBalance.Value = GetCurrency(db.Balance);
                    return;

                }
            }
            hfRemainingInstallment.Value = db.RemainingInstallments.ToString();
            hfRemainingInstallment.Value = n.ToBePaidOver.ToString();
            hfPaid.Value = "0";

        }


        bool CalculateAdvance()
        {
            if (txtToBePaidOver.Text.Trim() == string.Empty)
                return false;

            double loanAmount = double.Parse(txtAmount.Text.Trim());
            double toBePaidOver = double.Parse(txtToBePaidOver.Text.Trim());

            //if (toBePaidOver > loanAmount)
            //{
            //    warningMsg.InnerHtml = "To be Paid Over can not be greater than Advance amount.";
            //    warningMsg.Hide = false;
            //    return false;
            //}
            //else
            //    warningMsg.InnerHtml = "";
            double monthlyPayment = loanAmount / toBePaidOver;

            int startingMonth = int.Parse(ddlMonths.SelectedValue);
            int startingYear = int.Parse(ddlYears.SelectedValue);
            DateTime startingDate = new DateTime(startingYear, startingMonth, 1);
            int lastPaymentMonth = (int)((toBePaidOver - 1) * 1);
            DateTime lastPaymentDate = startingDate.AddMonths(lastPaymentMonth);

            lblLastPayment.Text = DateHelper.GetMonthName(lastPaymentDate.Month, IsEnglish) + "-" +
               lastPaymentDate.Year;
            lblMonthlyPayment.Text = GetCurrency(monthlyPayment);

            hfBalance.Value = GetCurrency(loanAmount);

            PEmployeeDeduction deduction = new PEmployeeDeduction();
            deduction.AdvanceLoanAmount = decimal.Parse(txtAmount.Text);
            deduction.TakenOn = calTakenOn.SelectedDate.ToString();
            deduction.ToBePaidOver = int.Parse(txtToBePaidOver.Text);

            deduction.StartingFrom = new CustomDate(1, int.Parse( ddlMonths.SelectedValue), int.Parse( ddlYears.SelectedValue), IsEnglish).ToString();
            deduction.LastPayment = new CustomDate(1, lastPaymentDate.Month, lastPaymentDate.Year, IsEnglish).ToString(); 
            //"1/" + lastPaymentDate.Month + "/" + lastPaymentDate.Year;

            CheckAndSetForRemainingInstallment(SessionManager.EmployeeDeduction, deduction);


            SessionManager.EmployeeDeduction = deduction;

            return true;
        }

        bool CalculateLoan()
        {
            if (txtInterestRate.Text.Trim() == string.Empty)
                return false;
            CommonManager mgr = new CommonManager();
            calculationConstants = CommonManager.CalculationConstant;

            double Days_In_A_Year;
            bool isFixedInstallment = chkIsFixedInstallmentAmount.Checked;
            if (this.IsEMI)
                Days_In_A_Year = 365;
            else
                Days_In_A_Year = 365;

            double loanAmount = double.Parse(txtAmount.Text.Trim());
            double interestRate = double.Parse(txtInterestRate.Text.Trim());
            double fixedInstallmentAmount = 0;
            double toBePaidOver = 0;

            if (isFixedInstallment)
            {
                if (double.TryParse(txtFixedInstallmentAmount.Text, out fixedInstallmentAmount) == false)
                {
                    JavascriptHelper.DisplayClientMsg("Valid fixed amount is required.",this.Page);
                    return false; 
                }
                fixedInstallmentAmount = Convert.ToDouble(txtFixedInstallmentAmount.Text.Trim());
                toBePaidOver = Math.Ceiling((loanAmount / fixedInstallmentAmount));
                txtToBePaidOver.Text = toBePaidOver.ToString();
            }
            else
                toBePaidOver = double.Parse(txtToBePaidOver.Text.Trim());

            double paymentTerms = double.Parse(ddlPaymentTerms.SelectedValue);
            double lengthOfPeriod =paymentTerms / (double)calculationConstants.EMILengthOfPeriod;
            double interestRatePerPeriod = (interestRate * lengthOfPeriod) / 100;
           

            int startingMonth = int.Parse(ddlMonths.SelectedValue);
            int startingYear = int.Parse(ddlYears.SelectedValue);
            //string startingDateStr = "1/" + startingMonth + "/" + startingYear;
            CustomDate date = new CustomDate(1, startingMonth, startingYear, IsEnglish);
            DateTime startingDate = new DateTime(startingYear, startingMonth, 1);
            DateTime startingDateEng = GetEngDate(date.ToString());
            string takenOnDate = calTakenOn.SelectedDate.ToString();
            DateTime takenOnDateEng = GetEngDate(takenOnDate);
            //calc interest on days diff
            int daysDiffBetTakenOnStartingFrom;// = (startingDateEng - takenOnDateEng).Days;

            double interest;

            //was coming -ve so done like this for simple interest case
            if (!this.IsEMI)
            {
                int totayDaysInStartingMonth =
                    DateHelper.GetTotalDaysInTheMonth(startingYear, startingMonth, IsEnglish);
                //daysDiffBetTakenOnStartingFrom = (int)Math.Abs(daysDiffBetTakenOnStartingFrom);
                ///For simple intrest case, if taken on date is before then no change in ppmt,
                ///buf taken on is after starting date then first month will be different
                if (takenOnDateEng > startingDateEng)
                {
                    daysDiffBetTakenOnStartingFrom = totayDaysInStartingMonth - (takenOnDateEng - startingDateEng).Days;
                    interest = loanAmount * (interestRate / 100.0) * (daysDiffBetTakenOnStartingFrom / Days_In_A_Year);
                }
                else
                {
                    //daysDiffBetTakenOnStartingFrom = (int)Days_In_A_Year;
                    interest = loanAmount * (interestRate / 100.0 / CommonManager.CalculationConstant.EMILengthOfPeriod );
                }
            }
            else
            {
                daysDiffBetTakenOnStartingFrom = (startingDateEng - takenOnDateEng).Days;
                interest = loanAmount * (interestRate / 100.0) * daysDiffBetTakenOnStartingFrom / Days_In_A_Year;
            }

            
           
            //add interest also
            //loanAmount += interest;

            double monthlyPaymentEMI;
            double totalInterest;
            if (this.IsEMI)
            {
                if (interestRatePerPeriod != 0)
                {
                    monthlyPaymentEMI =
                        //add interset while calc only
                        (
                          (loanAmount + interest) * interestRatePerPeriod / (1 - Math.Pow(1 / (1 + interestRatePerPeriod), toBePaidOver))
                           );
                }
                else
                {
                    monthlyPaymentEMI =
                        //add interset while calc only
                        (
                          (loanAmount + interest) / toBePaidOver//* interestRatePerPeriod / (1 - Math.Pow(1 / (1 + interestRatePerPeriod), toBePaidOver))
                           );
                }
                totalInterest = monthlyPaymentEMI * toBePaidOver - loanAmount;
            }
            else
            {
                if (isFixedInstallment)
                    monthlyPaymentEMI = fixedInstallmentAmount;
                else
                    monthlyPaymentEMI = (loanAmount )/ toBePaidOver;
                totalInterest = interest;
                interest = 0;
            }
            

            //=IF(B8=1,EDATE(B9,(B7-1)*1),IF(B8=2,EDATE(B9,(B7-1)*2),IF(B8=3,EDATE(B9,(B7-1)*3),IF(B8=6,EDATE(B9,(B7-1)*6),IF(B8=12,EDATE(B9,(B7-1)*12),NA)))))

            int lastPaymentMonth =(int) ((toBePaidOver - 1) * paymentTerms);
            DateTime lastPaymentDate = startingDate.AddMonths(lastPaymentMonth);

            
            PEmployeeDeduction deduction = new PEmployeeDeduction();
            deduction.AdvanceLoanAmount = (decimal)loanAmount;
            // why Interest was added in AmountWithInitialInterest case, this is making PPMT to be changed
            PEmployeeDeduction empDeduction = payMgr.GetEmployeeDeduction(UrlHelper.GetIdFromQueryString("EmpDedId"));
            if (empDeduction.PDeduction.IsEMI != null && empDeduction.PDeduction.IsEMI.Value)
                deduction.AmountWithInitialInterest = (decimal)(loanAmount + interest);
            else
                deduction.AmountWithInitialInterest = (decimal)loanAmount;

            deduction.LoanFixedInstallmentAmount = isFixedInstallment;
            if (isFixedInstallment)
            {
                deduction.SINewPPMTAmount = (decimal)fixedInstallmentAmount;
            }

            //deduction.AmountWithInitialInterest = (decimal)(loanAmount);
            deduction.TakenOn = takenOnDate;
            deduction.TakenOnEng = takenOnDateEng;
            deduction.InterestRate = double.Parse(txtInterestRate.Text);
            deduction.MarketRate = double.Parse(txtMarketRate.Text);
            deduction.ToBePaidOver = (int)toBePaidOver;
            deduction.PaymentTerms = ddlPaymentTerms.SelectedValue;
            deduction.StartingFrom = date.ToString();
            deduction.StartingFromEng = startingDateEng;

            CustomDate lastPayment = new CustomDate(1, lastPaymentDate.Month, lastPaymentDate.Year,IsEnglish);
            deduction.LastPayment = lastPayment.ToString();//"1/" + lastPaymentDate.Month + "/" + lastPaymentDate.Year;
            deduction.LastPaymentEng = GetEngDate(deduction.LastPayment);
            deduction.InterestAmount = (decimal)totalInterest;

            lblMonthlyPayment.Text = GetCurrency(monthlyPaymentEMI);
            lblTotalInterest.Text = GetCurrency(totalInterest);
            lblLastPayment.Text = DateHelper.GetMonthName(lastPaymentDate.Month, SessionManager.CurrentCompany.IsEnglishDate) + "-" +
                lastPaymentDate.Year;

            hfBalance.Value = GetCurrency(loanAmount + totalInterest);


            CheckAndSetForRemainingInstallment(SessionManager.EmployeeDeduction, deduction);

            SessionManager.EmployeeDeduction = deduction;
            return true;
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {

            //if (this.HasVariableAmount)
            //{

            //    PEmployeeDeduction deduction = null;
            //    if (SessionManager.EmployeeDeduction == null)
            //        deduction = new PEmployeeDeduction();
            //    else
            //        deduction = SessionManager.EmployeeDeduction;
                                    
            //    deduction.AdvanceLoanAmount = decimal.Parse(txtAmount.Text);
            //    deduction.TakenOn = calTakenOn.SelectedDate.ToString();

            //    SessionManager.EmployeeDeduction = deduction;
            //}

            //reguired as it saves the value in session
            //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdfsdf324", "window.onload = function() { okClick(); }", true);
        }
        
    }
}
