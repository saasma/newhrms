﻿<%@ Page Title="Periodic Pay Details" Language="C#" AutoEventWireup="true" MasterPageFile="~/Master/NewDetails.Master"
    CodeBehind="PeriodicPayDetails.aspx.cs" Inherits="Web.NewHR.PeriodicPayDetails" %>

<%@ Register Src="~/Controls/FooterCtl.ascx" TagName="FooterCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/HeaderCtl.ascx" TagName="HeaderCtl" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .holiday, .holiday a, .holiday td
        {
            color: #469146;
            background-color: #F0FFF0 !important;
        }
        .leave, .leave a, .leave td
        {
            color: #D95CA9;
            background-color: #FFF0F5 !important;
        }
        
        .weeklyholiday, .weeklyholiday a, .weeklyholiday td
        {
            color: #8F8F1A;
            background-color: #FAFAD2 !important;
        }
    </style>
    <script type="text/javascript">


   var getRowClass = function (record) {
    
        var status = record.data.PaymentStatus;
        

        if(status  == "Paid")
         {
            return "holiday";
         }
        
         else //if(dayValue=="Working Day")
         {
            return "";
         }
         //else 

      };

  
    var CommandHandler = function (command, record) {
            
        };

    function searchList() {
        <%=PagingToolbar1.ClientID %>.doRefresh();
    }

    var gridRowBeforeSelect = function (record, index,e1,e2,e3) {
         var gridrecord = <%=gridList.ClientID %>.getStore().getAt(index.index);
        
        if(typeof(gridrecord) != 'undefined')
        {
        if(gridrecord.data.PaymentStatus=="Paid")
        {
            return false;
        }
        }
    };

    var renderName = function(e1,e2)
    {
        return '<a target="_blank" href="../newhr/EmployeePayroll.aspx?ID=' + e2.record.data.EIN  + '">'+ e2.record.data.Name +'</a>';
    }
     
     function showAtte()
     {
        window.location='../attendance/attendancesummary.aspx?ShowWeeklyPay=' + <%=cmbGroup.ClientID %>.getValue() 
            + "&start=" +  <%=startDate1.ClientID %>.getRawValue() 
            + "&end=" +  <%=endDate1.ClientID %>.getRawValue();
     }

      var CommandCustomerDetails = function(command,record)
        {
             <%=hdnEmployeeID.ClientID %>.setValue(record.data.EIN);
           <%=btnEditDays.ClientID %>.fireEvent('click');
        }

    </script>
    <style type="text/css">
    
    .x-btn-default-toolbar-small-over,.x-btn-default-toolbar-small{background-image:none!important;
                                      background-color:transparent!important;}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>
    <%-- <ext:ResourceManager ID="ResourceManager1" runat="server" DisableViewState="false"
        ShowWarningOnAjaxFailure="false" ScriptMode="Release" Namespace="CompanyX" />--%>
    <ext:Hidden ID="hdnEmployeeID" runat="server" />
    <ext:Hidden ID="hdnCalculationID" runat="server" Text="0" />
    <ext:Hidden ID="hdnIsApprovedOnly" runat="server" />
    <ext:Hidden ID="hdnPeriodId" runat="server" />
    <ext:Hidden ID="hdnSortBy" runat="server" />
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4> Periodic Pay</h4>
            </div>
        </div>
    </div>
    <ext:Button runat="server" Hidden="true" ID="btnEditDays">
        <DirectEvents>
            <Click OnEvent="btnEditDays_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>
    <div class="contentpanel" style="margin-top: 0px !important; padding-top: 0px !important;">
        <div class="innerLR">
           

            <div class="alert alert-info" style="margin-top: 10px;margin-bottom: 0px;">
                <table>
                    <tr>
                         <td>
                            <ext:ComboBox Width="120" Disabled="true" MarginSpec="0 5 0 5" LabelSeparator="" LabelAlign="Top"
                                QueryMode="Local" ID="cmbGroup" ForceSelection="true" DisplayField="Name" ValueField="PayGroupID"
                                runat="server" FieldLabel="Pay Group">
                                <Store>
                                    <ext:Store ID="Store4" runat="server">
                                        <Model>
                                            <ext:Model ID="Model5" IDProperty="Year" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="PayGroupID" Type="String" />
                                                    <ext:ModelField Name="Name" Type="String" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>
                                    <Select OnEvent="cmbGroup_Select">
                                        <EventMask ShowMask="true" />
                                    </Select>
                                </DirectEvents>
                            </ext:ComboBox>
                             <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Pay Group is required."
                                                Display="None" ControlToValidate="cmbGroup" ValidationGroup="InsertUpdate" runat="server">
                                            </asp:RequiredFieldValidator>
                        </td>
                        <td >
                            <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                                <Proxy>
                                    <ext:AjaxProxy Url="../Handler/EmpSearchID.ashx">
                                        <ActionMethods Read="GET" />
                                        <Reader>
                                            <ext:JsonReader Root="plants" TotalProperty="total" />
                                        </Reader>
                                    </ext:AjaxProxy>
                                </Proxy>
                                <Model>
                                    <ext:Model ID="Model2" IDProperty="Name" Name="ItemLineModel" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Name" Type="String" />
                                            <ext:ModelField Name="EmployeeId" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                            <ext:ComboBox LabelSeparator="" FieldLabel="Employee" ID="cmbSearch" LabelAlign="Top"
                                LabelWidth="70" runat="server" DisplayField="Name" ValueField="EmployeeId" EmptyText="Search"
                                StoreID="storeSearch" TypeAhead="false" Width="150" HideBaseTrigger="true" MinChars="1"
                                TriggerAction="All" ForceSelection="true">
                                <ListConfig LoadingText="Searching..." MinWidth="200">
                                    <ItemTpl ID="ItemTpl1" runat="server">
                                        <Html>
                                            <tpl>
                                            <div class="search-item">
                                                            <span>{Name}</span>  
                                             </div>
					                </tpl>
                                        </Html>
                                    </ItemTpl>
                                </ListConfig>
                                <Triggers>
                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                </Triggers>
                                <Listeners>
                                    <Select Handler="this.getTrigger(0).show();" />
                                    <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                    <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                                </Listeners>
                            </ext:ComboBox>
                        </td>
                        <td>
                            <ext:DateField Width="120" Disabled="true"  MarginSpec="0 5 0 5" LabelSeparator="" LabelAlign="Top"
                                 ID="startDate1" 
                                runat="server" FieldLabel="Pay From">
                               
                            </ext:DateField>
                             <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Pay From is required."
                                                Display="None" ControlToValidate="startDate1" ValidationGroup="InsertUpdate" runat="server">
                                            </asp:RequiredFieldValidator>
                        </td>
                         <td>
                            <ext:DateField  Disabled="true"  Width="120" MarginSpec="0 5 0 5" LabelSeparator="" LabelAlign="Top"
                                 ID="endDate1" 
                                runat="server" FieldLabel="Pay Upto">
                               
                            </ext:DateField>
                             <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Pay Upto is required."
                                                Display="None" ControlToValidate="endDate1" ValidationGroup="InsertUpdate" runat="server">
                                            </asp:RequiredFieldValidator>
                        </td>
                        <td valign="bottom" style='padding-left:20px;'>
                            <ext:DisplayField Width="50" MarginSpec="0 5 0 5" LabelSeparator="" LabelAlign="Top"
                                 ID="dispDays" 
                                runat="server" Text="">
                               
                            </ext:DisplayField>
                        </td>
                        
                        <td style="width: 20px">
                        </td>
                        <td valign="bottom">
                            <ext:Button runat="server" Width="100" Height="30" Cls="btn btn-primary" ID="btnLoad"
                                Text="Load">
                                <Listeners>
                                    <Click Handler="valGroup = 'InsertUpdate';  if(CheckValidation()) searchList(); else return false;" />
                                </Listeners>
                            </ext:Button>
                        </td>
                        <td valign="bottom" style="width: 120px; padding-left: 20px;">
                            <ext:SplitButton ID="btnGenerate" Cls="btn btn-primary" runat="server" Text="Generate Pay"
                                Width="140" Height="30">
                                <DirectEvents>
                                    <Click OnEvent="btnGenerate_Click">
                                        <EventMask ShowMask="true" />
                                        <Confirmation ConfirmRequest="true" Message="Are you sure, you want to Generate the Pay?" />
                                    </Click>
                                </DirectEvents>
                                <Listeners>
                                    <Click Handler="valGroup = 'InsertUpdate';  return CheckValidation();">
                                    </Click>
                                </Listeners>
                                <Menu>
                                    <ext:Menu runat="server">
                                        <Items>
                                            <ext:MenuItem runat="server" ID="btnMenuLockPay" Text="Lock Pay">
                                                <DirectEvents>
                                                    <Click OnEvent="btnMenuLockPay_Click">
                                                        <EventMask ShowMask="true" />
                                                        <Confirmation ConfirmRequest="true" Message="Are you sure, you want to Lock the current Pay?" />
                                                    </Click>
                                                </DirectEvents>
                                                <Listeners>
                                                    <Click Handler="valGroup = 'InsertUpdate';  return CheckValidation();">
                                                    </Click>
                                                </Listeners>
                                            </ext:MenuItem>
                                        </Items>
                                    </ext:Menu>
                                </Menu>
                            </ext:SplitButton>
                        </td>
                        <td valign="bottom" style="width: 120px; padding-left: 20px;">
                            <ext:Button ID="SplitButton1" Cls="btn btn-primary" runat="server" Text="Attendance"
                                Width="140" Height="30">
                                <Listeners>
                                    <Click Handler="showAtte();">
                                    </Click>
                                </Listeners>
                              
                               
                            </ext:Button>
                        </td>
                    </tr>
                </table>
            </div>
            <br />
            
            <ext:GridPanel ID="gridList" runat="server" Header="true" AutoScroll="true">
                <Store>
                    <ext:Store ID="Store3" runat="server" OnReadData="Store_ReadData" RemoteSort="true"
                        AutoLoad="true" PageSize="100">
                        <Proxy>
                            <ext:PageProxy />
                        </Proxy>
                        <Sorters>
                            <ext:DataSorter Direction="ASC" Property="Name" />
                        </Sorters>
                        <AutoLoadParams>
                            <ext:Parameter Name="start" Value="0" Mode="Raw" />
                        </AutoLoadParams>
                        <Model>
                            <ext:Model ID="Model1" runat="server" >
                                <Fields>
                                    <ext:ModelField Name="SN" />
                                    <ext:ModelField Name="EIN" />
                                    <ext:ModelField Name="Name" />
                                    <ext:ModelField Name="Designation" />
                                    <ext:ModelField Name="Branch" />
                                    <ext:ModelField Name="Department" />

                                    <ext:ModelField Name="PayDays" />
                                    <ext:ModelField Name="Rate" />
                                    <ext:ModelField Name="GrossAmount" />
                                    <ext:ModelField Name="TDS" />
                                    <ext:ModelField Name="NetPay" />

                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <%--<Plugins>
                    <ext:CellEditing ID="CellEditing1" ClicksToEdit="1" runat="server">
                    </ext:CellEditing>
                </Plugins>--%>
                <ColumnModel ID="ColumnModel2" runat="server">
                    <Columns>
                        <ext:Column ID="Column9" Locked="true" runat="server" Text="SN" Width="50" DataIndex="SN"
                            Align="Center" Sortable="true" MenuDisabled="true">
                        </ext:Column>
                        <ext:Column ID="colEId" Locked="true" runat="server" Text="EIN" Width="50" DataIndex="EIN"
                            Align="Center" Sortable="true" MenuDisabled="true">
                        </ext:Column>
                        <ext:Column ID="Column1" Locked="true" runat="server" Text="Name" Width="150" DataIndex="Name"
                            Sortable="true" MenuDisabled="true">
                                <Renderer Fn="renderName" />
                        </ext:Column>
                        <ext:Column ID="Column10" Locked="true" runat="server" Text="Designation" Width="100"
                            DataIndex="Designation" Sortable="true" MenuDisabled="true">
                        </ext:Column>
                        <ext:Column ID="Column11" Locked="true" runat="server" Text="Branch" Width="100"
                            DataIndex="Branch" Sortable="true" MenuDisabled="true">
                        </ext:Column>
                        <ext:Column ID="Column12" Locked="true" runat="server" Text="Department" Width="100"
                            DataIndex="Department" Sortable="true" MenuDisabled="true">
                        </ext:Column>
                        
                        <ext:Column ID="Column8" runat="server" Align="Center" Text="Pay Days" Width="80"
                            DataIndex="PayDays" Sortable="true" MenuDisabled="true">
                        </ext:Column>
                        <ext:Column ID="Column3" runat="server" Align="Right"  Text="Rate" Width="80" DataIndex="Rate"
                            Sortable="true" MenuDisabled="true">
                            <Renderer Fn="getFormattedAmount" />
                        </ext:Column>
                        <ext:Column ID="Column2" runat="server" Align="Right"  Text="Gross Amount" Width="100" DataIndex="GrossAmount"
                            Sortable="true" MenuDisabled="true">
                            <Renderer Fn="getFormattedAmount" />
                        </ext:Column>
                         <ext:Column ID="Column4" runat="server" Align="Right"  Text="TDS" Width="80" DataIndex="TDS"
                            Sortable="true" MenuDisabled="true">
                            <Renderer Fn="getFormattedAmount" />
                        </ext:Column>
                         <ext:Column ID="Column5" runat="server" Align="Right"  Text="Net Pay" Width="80" DataIndex="NetPay"
                            Sortable="true" MenuDisabled="true">
                            <Renderer Fn="getFormattedAmount" />
                        </ext:Column>
                         <ext:CommandColumn ID="CommandColumn1" Align="Center" Cls="gridc" runat="server" Width="40" >
                                        <Commands>
                                            <ext:GridCommand >
                                                <Menu EnableScrolling="false">
                                                    <Items>
                                                        <ext:MenuCommand Text="Edit Pays Days" CommandName="ChangeAtte" />
                                                    </Items>
                                                </Menu>
                                            </ext:GridCommand>
                                        </Commands>
                                        <Listeners>
                                            <Command Handler="CommandCustomerDetails(command,record);" />
                                        </Listeners>
                                    </ext:CommandColumn>
                        <ext:Column ID="Column6"  MenuDisabled="false" Sortable="false" runat="server" Flex="1" >
                        </ext:Column>
                    </Columns>
                </ColumnModel>
                <%--<Listeners>
                    <SelectionChange Handler="document.getElementById('spanText').innerHTML = this.getSelectionModel().selected.items.length + ' employees selected';" />
                </Listeners>--%>
              <%--  <SelectionModel>
                    <ext:CheckboxSelectionModel ID="CheckboxSelectionModel1" runat="server" Mode="Simple">
                        <Listeners>
                            <BeforeSelect Fn="gridRowBeforeSelect" />
                        </Listeners>
                    </ext:CheckboxSelectionModel>
                </SelectionModel>--%>
                <View>
                    <ext:GridView ID="GridView1" runat="server">
                        <Listeners>
                        </Listeners>
                        <%--<GetRowClass Fn="getRowClass" />--%>
                    </ext:GridView>
                </View>
                <BottomBar>
                     <ext:PagingToolbar ID="PagingToolbar1" runat="server" StoreID="storeEmpList" DisplayInfo="true">
                    </ext:PagingToolbar>
                    
                    <%--<ext:PagingToolbar ID="PagingToolbar1" runat="server" StoreID="storeEmpList" PageSize="50"
                        DisplayInfo="true" DisplayMsg="Displaying employees {0} - {1} of {2}" EmptyMsg="No Employee to display">
                        <Items>
                            <ext:ComboBox runat="server" SelectedIndex="0" Width="80" ID="cmbPageSize">
                                <Items>
                                    <ext:ListItem Text="50" Value="50" />
                                    <ext:ListItem Text="100" Value="100" />
                                </Items>
                                <Listeners>
                                    <Select Handler="#{PagingToolbar1}.pageSize = parseInt( this.getValue()); searchListEL();" />
                                </Listeners>
                            </ext:ComboBox>
                        </Items>
                    </ext:PagingToolbar>--%>
                </BottomBar>
            </ext:GridPanel>
            <div class="buttonBlock" style="width: 68%; margin-top: 20px">
                <table class="fieldTable firsttdskip">
                    <tr>
                        <td style="width: 120px">
                            <ext:Button ID="btnExport" IconAlign="Right" Icon="TableGo" AutoPostBack="true" OnClick="btnExport_Click"
                                runat="server" Text="Export" Width="100" Height="30">
                            </ext:Button>
                        </td>
                        
                    </tr>
                </table>
            </div>
            <br />
        </div>
    </div>
    <ext:Window ID="window" runat="server" ButtonAlign="Left" Title="Adjust Pay Days" Icon="Application"
    Width="400" Height="240" BodyPadding="15" Hidden="true" Modal="false">
    <Content>
        <table class="fieldTable">
            <tr>
                <td>
                    <ext:DisplayField ID="lblEmployee" LabelSeparator="" FieldStyle='font-size:16px;color:#428BCA;' FieldLabel="" LabelAlign="Top"
                        runat="server" Width="200px" />
                </td>
            </tr>
            <tr>
                <td>
                    <ext:DisplayField ID="lblPresentDays" LabelSeparator="" FieldStyle='font-size:16px;color:#428BCA;' FieldLabel="Present days" LabelAlign="Left"
                        runat="server" Width="200px" />
                </td>
            </tr>
            <tr>
               <td style='padding-top:10px'>
                    <ext:TextField ID="txtPayDays" LabelSeparator="" FieldLabel="Pay Days" LabelAlign="Left"
                        runat="server" Width="200px">
                    </ext:TextField>
                    <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator5" runat="server"
                        ValidationGroup="SaveAmount" ControlToValidate="txtPayDays" ErrorMessage="Pay days is required." />
                    <asp:CompareValidator Display="None" ID="RequiredFieldValidator22" runat="server" ValueToCompare="0"
                       Operator="GreaterThan"
                        ValidationGroup="SaveAmount" ControlToValidate="txtPayDays" ErrorMessage="Invalid Pay days."
                        Type="Double" />
                </td>
            </tr>
        </table>
    </Content>
    <Buttons>
        <ext:Button runat="server" ID="btnChangePayDays" Cls="btn btn-primary" Text="<i></i>Change"
            runat="server">
            <DirectEvents>
                <Click OnEvent="btnChangePayDays_Click">
                    <Confirmation ConfirmRequest="true" Message="Confirm change the pay days?" />
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
            <Listeners>
                <Click Handler="valGroup = 'SaveAmount'; return CheckValidation();">
                </Click>
            </Listeners>
        </ext:Button>
        <ext:LinkButton runat="server" Cls="btnFlatLeftGap" ID="LinkButton4" Text="<i></i>Cancel"
            runat="server">
            <Listeners>
                <Click Handler="#{window}.hide();">
                </Click>
            </Listeners>
        </ext:LinkButton>
    </Buttons>
</ext:Window>
</asp:Content>
