﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using System.Text;
using BLL.Manager;
using Utils.Helper;
using DAL;
using System.Drawing;
using BLL.Base;


namespace Web.CP
{
    public partial class ManageMedicalTaxCredit : BasePage
    {
        TaxManager taxMgr = new TaxManager();
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                pagingCtl.Visible = false;
                Initialise();

            }

            if (Request.Form["__EVENTTARGET"] != null && Request.Form["__EVENTTARGET"].Equals("Reload"))
                LoadTaxCreditList();
            
            JavascriptHelper.AttachPopUpCode(Page, "medicalTaxCreditPopup", "AEMedicalTax.aspx", 600, 590);
            JavascriptHelper.AttachNonDialogPopUpCode(Page, "medicalTaxImportPopup", "../ExcelWindow/MedicalTaxDetailExcel.aspx", 450, 500);

        }

        void Initialise()
        {
            int empId = UrlHelper.GetIdFromQueryString("EmpId");
            if (empId != 0)
            {
                EEmployee emp = EmployeeManager.GetEmployeeById(empId);
                if (emp != null)
                {
                    txtEmpSearchText.Text = emp.Name;
                    btnLoad_Click(null, null);
                }
            }
            else
            {
                btnLoad_Click(null, null);
            }

            
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            int totalRecords = 0;
            List<GetMedicalTaxCreditListResult> list = taxMgr.GetMedicalTaxCreditList(
             SessionManager.CurrentCompanyId, txtEmpSearchText.Text.Trim(), 0, 88888, ref totalRecords);

            List<string> hiddenList = new List<string>();
            hiddenList.Add("MedicalTaxCreditId");
            hiddenList.Add("CostCode");

            hiddenList.Add("TotalRows");
            hiddenList.Add("RowNumber");

            Dictionary<string, string> renameList = new Dictionary<string, string>();

            renameList.Add("PYAmount", "Last year amount");
            renameList.Add("CYExpense", "Current year expenses");
            renameList.Add("AdjustableAmount", "Adjustable Amount");
            renameList.Add("CarriedFWDToNextYear", "Transferred to next year");
            renameList.Add("EmployeeId", "EIN");


            Bll.ExcelHelper.ExportToExcel("Employee Medical Tax", list,
                hiddenList,
            new List<String>() { },
            renameList,
            new List<string>() { "PYAmount", "CYExpense", "AdjustableAmount", "CarriedFWDToNextYear" }, new List<string> { "EIN" }
            , new List<string>() {  }
            , new Dictionary<string, string>() { { "Employee Medical Tax", "" } }

            , new List<string> { "EmployeeId", "Name", "Branch", "Department", "PYAmount", "CYExpense", "AdjustableAmount", "CarriedFWDToNextYear"});


        }


        void LoadTaxCreditList()
        {
            int totalRecords = 0;
            List <GetMedicalTaxCreditListResult> list = taxMgr.GetMedicalTaxCreditList(
                SessionManager.CurrentCompanyId, txtEmpSearchText.Text.Trim(), pagingCtl.CurrentPage-1, int.Parse(pagingCtl.DDLRecords.SelectedValue), ref totalRecords);
            gvwList.DataSource = list;
            gvwList.DataBind();
            if (totalRecords == 0)
                pagingCtl.Visible = false;
            else
                pagingCtl.Visible = true;
            pagingCtl.UpdatePagingBar(totalRecords);

            //int totalRecords = 0;
            //gvwList.DataSource = ProjectManager.GetProjectList(
            //    GetEngDate(calFilterFrom.SelectedDate.ToString()), GetEngDate(calFilterTo.SelectedDate.ToString()),
            //    chkAllProjects.Checked, txtSearch.Text.Trim(), pagintCtl.CurrentPage, int.Parse(pagintCtl.DDLRecords.SelectedValue), ref totalRecords);


            //gvwList.DataBind();


            if (list.Count > 0)
            {
                
                bool newFound = false;
                foreach (GetMedicalTaxCreditListResult item in list)
                {
                    if (item.MedicalTaxCreditId == 0)
                    {
                        newFound = true;
                        break;
                    }
                }
                if (newFound)
                    btnSave.Visible = true;
                else
                    btnSave.Visible = false;
            }
            else
                btnSave.Visible = false;


        }

        protected void btnLoad_Click(object sender, EventArgs e)
        {
            pagingCtl.CurrentPage = 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage -= 1;
            LoadTaxCreditList();;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            StringBuilder str = new StringBuilder("");
            foreach (GridViewRow row in gvwList.Rows)
            {
                int empId = (int)gvwList.DataKeys[row.RowIndex]["EmployeeId"];
                int mId = (int)gvwList.DataKeys[row.RowIndex]["MedicalTaxCreditId"];
                if (mId == 0)
                {
                    str.Append(empId + ",");
                }
            }

            taxMgr.SaveMedicalTaxList(SessionManager.CurrentCompanyId, str.ToString());

            JavascriptHelper.DisplayClientMsg("Medical tax credit for the employee(s) is saved.", Page);

            LoadTaxCreditList();
        }

        public bool IsVisible(object val)
        {
            if (val == null)
                return false;
            int mId = int.Parse(val.ToString());
            if (mId == 0)
                return false;
            return true;
        }

        protected void gvwList_RowCreated(object sender, GridViewRowEventArgs e)        
        {
            if (e.Row.RowIndex < 0)
                return;
            int medicalTaxCreditId =(int) gvwList.DataKeys[e.Row.RowIndex]["MedicalTaxCreditId"];
            if (medicalTaxCreditId == 0)
            {
                e.Row.Cells[0].BackColor = Color.Yellow;
                e.Row.Cells[0].ToolTip = Resources.Messages.MedicalTaxNotSavedMsg;
                //e.Row.BackColor = Color.Yellow;
            }
        }

        protected void txtEmpSearchText_TextChanged(object sender, EventArgs e)
        {
            LoadTaxCreditList();
        }

        protected void btnPrevious_Click()
        {
            pagingCtl.CurrentPage -= 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage -= 1;
            LoadTaxCreditList();
        }

        protected void btnNext_Click()
        {
            pagingCtl.CurrentPage += 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage += 1;
            LoadTaxCreditList();
        }

        protected void ddlRecords_SelectedIndexChanged()
        {
            //SessionManager.EmployeeDisplayRecodsPerPage = int.Parse(pagintCtl.DDLRecords.SelectedValue);
            //_tempCurrentPage = 1;
            pagingCtl.CurrentPage = 1;
            LoadTaxCreditList();
        }
    }
}
