﻿<%@ Page Title="Leave list" Language="C#" MasterPageFile="~/Master/ForPopupPage.Master"
    AutoEventWireup="true" CodeBehind="LeaveList.aspx.cs" Inherits="Web.CP.LeaveList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        function closePopup() {


            // alert(window.opener.parentReloadCallbackFunction)
            //if ($.browser.msie == false && typeof (window.opener.reloadLeave) != 'undefined' && ($.browser.chrome || $.browser.safari)) {
            window.opener.reloadLeave(window);
            //            } else {
            //                window.returnValue = "ReloadLeave";
            //                window.close();
            //            }
        }



        function reloadLeave(closingWindow) {
            closingWindow.close();
            __doPostBack('Reload', '');
        }

        function popupCreateNewCall() {
            var ret = popupCreateNew();
            if (typeof (ret) != 'undefined') {
                if (ret == "Reload") {
                    __doPostBack('Reload', '');
                }
            }
        } function doubleClickHander(lst) {
            $('#<%= btnOk.ClientID %>').click();
        }
        
    </script>
    <style type="text/css">
        div.bevel div.fields
        {
            width: 340px !important;
            padding-bottom: 10px !important;
            overflow: hidden;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <div class="popupHeader">
        <h2 class="headlinespop">
            Leave List</h2>
    </div>
    <div class="bevel marginal">
        <div class="fields paddpop">
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td width="230px">
                        <asp:ListBox ondblclick="doubleClickHander(this)" SelectionMode="Multiple" ID="lstLeaveList"
                            DataTextField="Title" DataValueField="LeaveTypeId" runat="server" Height="238px"
                            Width="217px"></asp:ListBox>
                        <asp:RequiredFieldValidator ID="valReqdName" runat="server" ControlToValidate="lstLeaveList"
                            Display="None" ErrorMessage="Leave must be selected for adding." ValidationGroup="Add"></asp:RequiredFieldValidator>
                    </td>
                    <td valign="top">
                        <asp:Button ID="btnOk" runat="server" Text="Ok" CssClass="update" ValidationGroup="Add"
                            OnClientClick="valGroup='Add';return CheckValidation()" OnClick="btnOk_Click" />
                        <br />
                        <asp:Button ID="btnCreateNew" runat="server" Text="Create" CssClass="save" OnClientClick="popupCreateNewCall();return false;" />
                        <br />
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="cancel" OnClientClick="window.close()" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="clear" style="padding-left: 15px">
        <div style="color: #960000; padding-top: 10px; width: 450px; padding-left: 5px; }">
            (Please re-save the attendance after applying leave if the employee joined or leave
            status date lies in the past.To confirm balance can be checked in employee leave
            tab.)
        </div>
        <br />
        <asp:RadioButtonList ID="rdbListAddTo" runat="server" AutoPostBack="true" OnSelectedIndexChanged="rdbListAddTo_Change">
            <asp:ListItem Selected="True" Value="1">Add to this employee</asp:ListItem>
            <asp:ListItem Value="2">Add to all employees</asp:ListItem>
        </asp:RadioButtonList>
        <div style="padding-left: 10px; padding-top: 10px;">
            <table>
                <tr>
                    <td>
                        Status
                    </td>
                    <td>
                        <asp:DropDownList Enabled="false" Style="margin-top: 10px" ID="ddlStatus" runat="server"
                            AppendDataBoundItems="true" Width="150px">
                            <asp:ListItem Selected="true" Value="-1" Text="--All Status--"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
        </div>
        <asp:LinkButton ID="btnRefresh" runat="server" Text="Refresh" Style="display: none" />
    </div>
</asp:Content>
