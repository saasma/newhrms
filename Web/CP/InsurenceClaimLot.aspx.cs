﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;
using Utils.Calendar;
using Utils.Helper;

namespace Web.NewHR
{
    public partial class InsurenceClaimLot : BasePage
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["LotID"] != null && Request.QueryString["LotID"] != "")
            {
                btnSave.Visible = false;
                txtLotNo.Text = Request.QueryString["LotID"].ToString();
            }
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            string ltNO = "";
            List<SCClaimLotDetail> lst = new List<SCClaimLotDetail>();
            RowSelectionModel sm = this.GridClaimList.GetSelectionModel() as RowSelectionModel;
            foreach (SelectedRow row in sm.SelectedRows)
            {
                SCClaimLotDetail obj = new SCClaimLotDetail();
                obj.ClaimRef_ID = Convert.ToInt32(row.RecordID);
                lst.Add(obj);
            }
            if (lst.Count() > 0)
            {
                Status status = HRManager.SaveLot(lst, out ltNO);
                if (status.IsSuccess)
                {
                    txtLotNo.Text = ltNO.ToString();
                    btnSave.Hide();
                    NewMessage.ShowNormalMessage("Successfully assign lot number " + ltNO.ToString() + ".");
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }
            }
        }

        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            int type = -1;
            int status = (int)ClaimStatus.Processing;
            int ein = -1;
            int lotid = -1;
            int havingLotNo = 0;
            int? totalRecords = 0;

            if (Request.QueryString["LotID"] != null && Request.QueryString["LotID"] != "")
            {
                havingLotNo = 1;
                lotid = Convert.ToInt32(Request.QueryString["LotID"]);
                StoreGridClaimList.DataSource = HRManager.GetEmployeeInsuranceClaimListByLot(type, -1, ein, lotid, havingLotNo, e.Page - 1, e.Limit, ref totalRecords);
                StoreGridClaimList.DataBind();
                e.Total = totalRecords.Value;
                return;

            }
            StoreGridClaimList.DataSource = HRManager.GetEmployeeInsuranceClaimListByLot(type, status, ein, lotid, havingLotNo, e.Page - 1, e.Limit, ref totalRecords);
            StoreGridClaimList.DataBind();
            e.Total = totalRecords.Value;
        }
    }
}
