﻿<%@ Page Language="C#" AutoEventWireup="true" Title="Edit Allowance" MasterPageFile="~/Master/ForPopupPage.Master"
    CodeBehind="AllowanceForwardAssignPopup.aspx.cs" Inherits="Web.CP.AllowanceForwardAssignPopup" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<%@ Register Assembly="DevExpress.Web.v16.1, Version=16.1.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function ConfirmInstitutionDeletion() {
            if (ddllist.selectedIndex == -1) {
                alert('No Insurance company to delete.');
                clearUnload();
                return false;
            }
            if (confirm('Do you want to delete the insurance company?')) {
                clearUnload();
                return true;
            }
            else {
                return false;
            }
        }

        function DisplayInstitutionInTextBox(dropdown_id) {

        }



        //capture window closing event


        function closePopup() {

            window.opener.refreshEventList(window);

        }

      
        function clearEmp()
        {
            <%= hdnEmpId.ClientID %>.setValue('');
        }
        function ACE_item_selected(source, eventArgs) {
            var value = eventArgs.get_value();
            <%= hdnEmpId.ClientID %>.setValue(value);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
       <script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>
    <ext:ResourceManager DisableViewState="false" ScriptMode="Release" Namespace="" runat="server" />
    <asp:HiddenField ID="Hidden_CounterTypePrev" runat="server" />
    <asp:HiddenField ID="Hidden_FromPrev" runat="server" />
    <asp:HiddenField ID="Hidden_ToPrev" runat="server" />
    <asp:HiddenField ID="Hidden_DaysPrev" runat="server" />
    <ext:Hidden ID="hdnEmpId" runat="server" />
    <div class="popupHeader">
        <span style='margin-left: 20px; padding-top: 8px; display: block' runat="server"
            id="title">Allowance : </span>
    </div>
    <div class="marginal">
        <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" Width="490px" runat="server" />
        <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Width="490px" Hide="true"
            runat="server" />
        <asp:HiddenField ID="Hidden_OldMinute" runat="server" />
        <asp:HiddenField ID="Hidden_RequestID" runat="server" />
        <table style="margin-bottom: 25px;" class="tbl">
            <tbody>
                <tr>
                    <td colspan="2">
                        <p style="margin-bottom: -3px; margin-top: 10px;">
                            Employee
                        </p>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:TextBox ID="txtEmpSearch" onchange="if(this.value=='') clearEmp();" runat="server"
                            Width="200px" placeholder="Search Employee" Style="margin-top: 2px;" />
                        <cc1:AutoCompleteExtender EnableCaching="true" ID="AutoCompleteExtenderOrganization"
                            runat="server" MinimumPrefixLength="2" ServiceMethod="GetEmployeeNamesWithID"
                            ServicePath="~/PayrollService.asmx" TargetControlID="txtEmpSearch" OnClientItemSelected="ACE_item_selected"
                            CompletionSetCount="10" CompletionInterval="250" CompletionListCssClass="AutoExtender"
                            CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
                        </cc1:AutoCompleteExtender>
                        <asp:RequiredFieldValidator ID="rfvEmployee"  SetFocusOnError="true"
                            ValidationGroup="SaveUpdate" ControlToValidate="txtEmpSearch" Display="None"
                            ErrorMessage="Employee is required." runat="server" />
                        <%--<asp:DropDownList DataTextField="Name" DataValueField="EveningCounterTypeId" ID="DropDownList1"
                            runat="server" Style="width: 200px; margin-right: 25px;">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ValidationGroup="SaveUpdate"
                            Display="None" ErrorMessage="Overtime type is required." ControlToValidate="ddlOvertimeType"></asp:RequiredFieldValidator>--%>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <p style="margin-bottom: -3px; margin-top: 10px;">
                            Allowance type
                        </p>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:DropDownList DataTextField="Name" DataValueField="EveningCounterTypeId" ID="ddlOvertimeType"
                            AppendDataBoundItems="true" runat="server" Style="width: 200px; margin-right: 25px;">
                            <asp:ListItem Text="" Value="-1" Selected="True" />
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator5" ValidationGroup="SaveUpdate"
                            Display="None" InitialValue="-1" ErrorMessage="Allowance Type is required." ControlToValidate="ddlOvertimeType"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:DateField ID="txtFromDate" runat="server" FieldLabel="From Date *" LabelAlign="top"
                            LabelSeparator="" Width="175" AllowBlank="false">
                            <DirectEvents>
                                <Select OnEvent="Date_Change">
                                </Select>
                            </DirectEvents>
                            <Plugins>
                                            <ext:GenericPlugin ID="HighlightDates1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                                        </Plugins>
                        </ext:DateField>
                        <asp:RequiredFieldValidator Enabled="true" Display="None" ID="valtxtPublicationName"
                            runat="server" ValidationGroup="SaveUpdate" ControlToValidate="txtFromDate" ErrorMessage="From date is required." />
                    </td>
                    <td style="padding-left: 10px">
                        <ext:DisplayField ID="txtFromDateNep" StyleSpec="margin-top:15px" runat="server"
                            FieldLabel=" " LabelAlign="top" LabelSeparator="" Width="175" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:DateField ID="txtToDate" runat="server" FieldLabel="To Date *" LabelAlign="top"
                            LabelSeparator="" Width="175" Hidden="false" AllowBlank="false">
                            <DirectEvents>
                                <Select OnEvent="Date_Change">
                                </Select>
                            </DirectEvents>
                            <Plugins>
                                            <ext:GenericPlugin ID="GenericPlugin1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                                        </Plugins>
                        </ext:DateField>
                        <asp:RequiredFieldValidator Enabled="true" Display="None" ID="RequiredFieldValidator111"
                            runat="server" ValidationGroup="SaveUpdate" ControlToValidate="txtToDate" ErrorMessage="To Date is required." />
                    </td>
                    <td style="padding-left: 10px">
                        <ext:DisplayField ID="txtToDateNep" StyleSpec="margin-top:15px" runat="server" FieldLabel=" "
                            LabelAlign="top" LabelSeparator="" Width="175" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:NumberField ID="txtDays" AllowDecimals="false" runat="server" FieldLabel="Days *"
                            LabelAlign="top" LabelSeparator=" " Width="175" Hidden="false" AllowBlank="false"
                            MinValue="0">
                        </ext:NumberField>
                        <asp:RequiredFieldValidator Enabled="true" Display="None" ID="RequiredFieldValidator11"
                            runat="server" ValidationGroup="SaveUpdate" ControlToValidate="txtDays" ErrorMessage="Day(s) is required." />
                    </td>
                </tr>
                <tr>
                    <td style="padding-top: 10px">
                        <strong>Reason : </strong>
                    </td>
                    <td style="padding-top: 10px">
                        <%--<asp:Label runat="server" ID="lblReason"></asp:Label>--%>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox ID="txtReason" runat="server" Height="55" Width="225" TextMode="MultiLine"></asp:TextBox>
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="tbl">
            <tr>
                <td colspan="2">
                    <asp:Button ID="btnUpdate" ValidationGroup="SaveUpdate" runat="server" OnClientClick="valGroup='SaveUpdate';  return CheckValidation(); "
                        Text="Assign" Width="150" Height="30" Style="margin-top: 25px;" OnClick="btnUpdate_Click"
                        CssClass="save" />
                </td>
                <td colspan="2">
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
