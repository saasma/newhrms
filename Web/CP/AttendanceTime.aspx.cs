﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using Utils.Helper;
using Utils.Web;
using BLL.Base;
using DAL;

namespace Web
{
    public partial class AttendanceTime : BasePage
    {

        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.LeaveAndAttendance;
            }
        }

        private int _tempCurrentPage;
        private int _tempCount;
        private PayManager payMgr = new PayManager();
        protected void Page_Load(object sender, EventArgs e)
        {
            //RegisterEmployeeSavedMsg();
            if (!IsPostBack)
            {
               
                BindEmployees();

            }
            if (Request.Form["__EVENTTARGET"] != null && Request.Form["__EVENTTARGET"].ToLower().Contains("refresh"))
                BindEmployees();
           
            JavascriptHelper.AttachNonDialogPopUpCode(Page, "empTime", "../ExcelWindow/EmployeeAttendanceTimeExcel.aspx", 450, 500);
            
        }
        //void RegisterEmployeeSavedMsg()
        //{
        //    if (SessionManager.EmployeeSaved)
        //    {
        //        JavascriptHelper.DisplayClientMsgWithTimeout("Employee Information Updated.", Page);
        //        SessionManager.EmployeeSaved = false;
        //    }
        //}

        void BindEmployees()
        {
            int totalRecords = 0;

            List<AttendanceInOutTime> list= TaxManager.GetTimes();

            List<AttendanceInOutTime> list1 = new List<AttendanceInOutTime>();

            DateTime? currentDate = null;
            if (list.Count > 0)
                currentDate = list[0].DateEng;

            foreach(var item in list)
            {
                if(item.DateEng != currentDate)
                {
                    currentDate = item.DateEng;
                    list1.Add(new AttendanceInOutTime { });
                }

                list1.Add(item);
            }


            gvEmployeeIncome.DataSource
                = list1;
            gvEmployeeIncome.DataBind();
          
        }

     

        protected void txtEmployeeName_TextChanged(object sender, EventArgs e)
        {
            _tempCurrentPage = 1;
            BindEmployees();

        }
    }
}
