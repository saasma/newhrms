<%@ Page Title="" Language="C#" MasterPageFile="~/Master/ForPopupPage.Master" AutoEventWireup="true" CodeBehind="DeductionLoanAdvanceHistory.aspx.cs" Inherits="Web.CP.DeductionLoanAdvanceHistory" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">
        function closePopup() {
            window.returnValue = "Reload";
            window.close();
        }

        function closePopupFromPay() {
            window.returnValue = "ReloadIncome";
            window.close();
        }         
        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">

 <div class="popupHeader">
 <h3>Loan/Advance history</h3>
 </div>
 
<div style='clear:both;padding-left:20px;padding-top:10px' >

    <cc2:EmptyDisplayGridView     PagerStyle-HorizontalAlign="Center"  
        PagerStyle-CssClass="defaultPagingBar"  AllowPaging="true" PageSize="15" 
        GridLines="None" CssClass="tableLightColor"  CellPadding="0" CellSpacing="0" 
        ID="gvw" runat="server" AutoGenerateColumns="False" onpageindexchanging="gvw_PageIndexChanging" 
        >
        
        <Columns>
           
            <asp:BoundField HeaderText="Month" HeaderStyle-HorizontalAlign="Left" DataField="PayrollPeriod" />
            
           
            
            <asp:TemplateField HeaderText="Loan amount" ItemStyle-HorizontalAlign="Right">
                <ItemTemplate>
                    <%# GetCurrency(Eval("Amount"))%>
                    
                </ItemTemplate>
            </asp:TemplateField>
            
             <asp:TemplateField HeaderText="Taken On">
                <ItemTemplate>
                    <%# (Eval("TakenOn"))%>
                    
                </ItemTemplate>
            </asp:TemplateField>
            
             <asp:TemplateField HeaderText="EMI" ItemStyle-HorizontalAlign="Right">
                <ItemTemplate>
                    <%# GetCurrency(Eval("InstallmentAmount"))%>
                    
                </ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="Terms" ItemStyle-HorizontalAlign="Left">
                <ItemTemplate>
                    <%# GetPaymentTerms(Eval("PaymentTerms"),Eval("ToBePaidOver"))%>
                    
                </ItemTemplate>
            </asp:TemplateField>
            
           <asp:TemplateField HeaderText="Interest rate" ItemStyle-HorizontalAlign="Right">
                <ItemTemplate>
                    <%# GetRate(Eval("InterestRate"))%>
                    
                </ItemTemplate>
            </asp:TemplateField>
            
           <asp:TemplateField HeaderText="PPMT" ItemStyle-HorizontalAlign="Right">
                <ItemTemplate>
                    <%# GetCurrency(Eval("PPMT"))%>
                    
                </ItemTemplate>
            </asp:TemplateField>
           
            <asp:TemplateField HeaderText="IPMT" ItemStyle-HorizontalAlign="Right">
                <ItemTemplate>
                    <%# GetCurrency(Eval("IPMT"))%>
                    
                </ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="Unpaid balance" ItemStyle-HorizontalAlign="Right">
                <ItemTemplate>
                    <%# GetCurrency(Eval("UnpaidBalance"))%>
                    
                </ItemTemplate>
            </asp:TemplateField>
            
        </Columns>
        <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
        <EmptyDataTemplate>
            <b>No history.</b>
        </EmptyDataTemplate>
                
    </cc2:EmptyDisplayGridView>
    
    </div> 
</asp:Content>
