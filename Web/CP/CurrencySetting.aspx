<%@ Page MaintainScrollPositionOnPostback="true" Title="Currency Settings" Language="C#"
    MasterPageFile="~/Master/ForPopupPage.Master" AutoEventWireup="true" CodeBehind="CurrencySetting.aspx.cs"
    Inherits="Web.CP.CurrencySetting" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.numeric.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.bgiframe.min.js" type="text/javascript"></script>
    <link href="../css-combo/sexy-combo.css" rel="stylesheet" type="text/css" />
    <link href="../css-combo/custom/custom.css" rel="stylesheet" type="text/css" />
    <link href="../css-combo/sexy/sexy.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        tr
        {
            padding-top: 10px;
        }
    </style>
    <script type="text/javascript">

        function closePopup() {



            window.opener.setCurrency(document.getElementById('ctl00_mainContent_hdn').value);
        }


        window.onunload = closePopup;   
        
    </script>
    <%--  <script src="../Scripts/jquery.sexy-combo.pack.js" type="text/javascript"></script>

   <script src="../Scripts/jquery.sexy-combo.min.js" type="tet/javascript"></script>

    <script src="../Scripts/jquery.sexy-combo.js" type="text/javascript"></script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <div class="popupHeader">
        <h3>
            Currency Settings</h3>
    </div>
    <asp:HiddenField ID="hdn" runat="server" />
    <div class=" marginal" style='margin-top: 15px'>
        <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Width="370px" Hide="true" runat="server" />
        <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Width="370px"  Hide="true" runat="server" />
        <table style='clear: both;'>
            <tr>
                <td class="fieldHeader" style='width: 80px;'>
                    <My:Label ID="Label4" Text="Fixed Rate" runat="server" ShowAstrick="true" />
                    <%--<My:Label ID="Label5" Text="IRO name:" runat="server" ShowAstrick="true" /> --%>
                </td>
                <td>
                    <asp:TextBox Enabled="false" ID="txtFixedRate" runat="server" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtFixedRate"
                        InitialValue="-1" Display="None" ErrorMessage="Fixed rate is required." ValidationGroup="AECompany"></asp:RequiredFieldValidator>
                    <asp:CompareValidator SetFocusOnError="true" ValueToCompare='0' Display="None" Operator="GreaterThan"
                        Type="Double" ControlToValidate="txtFixedRate" ID="CompareValidator1" ValidationGroup="AECompany"
                        runat="server" ErrorMessage='Invalid fixed rate.'></asp:CompareValidator>
                    <asp:Button ID="btnEdit1" Visible="true" runat="server" Width="60" Text="Edit" OnClick="btnEdit1_Click" />
                    <asp:Button ID="btnCancel1" runat="server" Text="Cancel" Width="60"  Visible="false" OnClick="btnCancel1_Click" />
                </td>
            </tr>
            <tr runat="server" id="trCurrentRate">
                <td class="fieldHeader">
                    <My:Label ID="Label1" Text="Current Rate" runat="server" ShowAstrick="true" />
                    <%--<My:Label ID="Label5" Text="IRO name:" runat="server" ShowAstrick="true" /> --%>
                </td>
                <td>
                    <asp:TextBox Enabled="false" ID="txtCurrentRate" runat="server" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtCurrentRate"
                        InitialValue="-1" Display="None" ErrorMessage="Current rate is required." ValidationGroup="AECompany"></asp:RequiredFieldValidator>
                    <asp:CompareValidator SetFocusOnError="true" ValueToCompare='0' Display="None" Operator="GreaterThan"
                        Type="Double" ControlToValidate="txtCurrentRate" ID="CompareValidator2" ValidationGroup="AECompany"
                        runat="server" ErrorMessage='Invalid current rate.'></asp:CompareValidator>
                    <asp:Button ID="btnEdit2" runat="server" Text="Edit"  Width="60"   OnClick="btnEdit2_Click" />
                    <asp:Button ID="btnCancel2" runat="server" Text="Cancel" Width="60"  Visible="false" OnClick="btnCancel2_Click" />
                </td>
            </tr>
            <tr>
                <td colspan="2" style='padding-top: 10px'>
                    <asp:Button ID="btnSave" CssClass="update" OnClientClick="valGroup = 'AECompany';if( CheckValidation() ) { return confirm('Are you sure, you want to change the currency setting?');}"
                        runat="server" Text="Save" ValidationGroup="AECompany" OnClick="btnSave_Click" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
