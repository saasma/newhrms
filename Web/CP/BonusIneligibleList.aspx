<%@ Page Title="Bonus Ineligible List" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="BonusIneligibleList.aspx.cs" Inherits="Web.CP.BonusIneligibleList" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        function insertUpdateGratuityClassCall(classId) {
            var ret = insertUpdateGratuityClass('Id=' + classId);

            if (typeof (ret) != 'undefined') {
                if (ret == 'Reload') {
                    __doPostBack('Reload', '');
                }
            }
        }
        function insertUpdateGratuityRuleCall(gratuityRuleId) {
            var ret = insertUpdateGratuityRule('bonusid=' + <%=ddlBonusList.ClientID %>.value);

            if (typeof (ret) != 'undefined') {
                if (ret == 'Reload') {
                    __doPostBack('Reload', '');
                }
            }

            return false;
        }


        function refresh(popupWindow) {
            popupWindow.close();
            __doPostBack('Reload', '');
        }

        function changeStates(chk) {
            var isChecked = $(chkAll).is(':checked');

            $("INPUT[type='checkbox']").attr('checked', isChecked);

        }

        function importPopupProcess(bonusId) {


            var ret = importPopup("bonusId=" + bonusId);


            return false;
        }

        function viewCalc(bonusId) {
            window.location = 'BonusDetails.aspx?id=' + bonusId;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Bonus InEligible List
                </h4>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hdnBonusId" runat="server" />
    <div class="contentpanel">
        <div class="contentArea">
            Bonus
            <asp:DropDownList ID="ddlBonusList" OnSelectedIndexChanged="bonusChange" AutoPostBack="true" runat="server" Width="125" DataValueField="BonusId"
                AppendDataBoundItems="true" DataTextField="Name" CssClass="gapbelow">
                <asp:ListItem Value="-1">--Select Bonus--</asp:ListItem>
            </asp:DropDownList>
            <asp:LinkButton ID="Button1" class="btn btn-success btn-sm btn-sect" runat="server"
                Text="New InEligible Employee" Style="height: 30px; width: 150px; margin-bottom: 5px;"
                OnClientClick="insertUpdateGratuityRuleCall();return false;" />
            <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
            <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
            <br />
            <div>
                <asp:GridView CssClass="table table-primary mb30 table-bordered table-hover" UseAccessibleHeader="true"
                    ID="gvwGratuityRules" showheaderwhenempty="True" runat="server" AutoGenerateColumns="False"
                    GridLines="None" DataKeyNames="BonusId,EIN" OnRowDeleting="gvwDepartments_RowDeleting"
                    showfooterwhenempty="True" >
                    <Columns>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                            HeaderText="Name">
                            <ItemTemplate>
                                <asp:Label ID="Label1" Text='<%#   Eval("Name")%>' runat="server" Width="200px" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                            HeaderStyle-Width="200px" HeaderText="Reason">
                            <ItemTemplate>
                                <%#     (Eval("Reason"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                      
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="60px">
                            <ItemTemplate>
                              <%--  <input type='image' style='border-width: 0px; vertical-align: sub;' onclick='return insertUpdateGratuityRuleCall(<%# Eval("BonusId") %>);'
                                    src='../images/edit.gif' />
                                &nbsp;--%>
                                <asp:ImageButton ID="ImageButton2" Style='vertical-align: sub' OnClientClick="return confirm('Confirm delete the Bonus?')"
                                    runat="server" CommandName="Delete" ImageUrl="~/images/delet.png" />
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="odd" />
                    <AlternatingRowStyle CssClass="even" />
                </asp:GridView>
                <br />
                <br />
            </div>
        </div>
    </div>
</asp:Content>
