﻿using System;
using System.Collections.Generic;
using System.Linq;
using BLL;
using BLL.Manager;
using System.Xml;
using Utils.Helper;
using BLL.Base;

namespace Web.Master
{
    public partial class HRDepartmentalAction : BaseMasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            LoadMenus();
            Page.ClientScript.RegisterClientScriptBlock(GetType(), "sdfsdf", "url = '" + UrlHelper.GetRootUrl() + "';", true);
        }

        public bool isCustomRole = false;
        public int[] pagesPerimissible = null;

        public XmlDocument ProcessAsPerRole()
        {
            return MyCache.GetMenuXml(SessionManager.User.UserID.ToString());
        }

        public bool IsAccessible(XmlAttribute value, XmlAttribute urlValue)
        {
            var pageId = 0;
            var url = string.Empty;

            if (value == null || string.IsNullOrEmpty(value.Value))
            {
                ;
            }
            else
            {
                pageId = int.Parse(value.Value);
            }
            if (urlValue == null || string.IsNullOrEmpty(urlValue.Value))
            {
                ;
            }
            else
            {
                url = urlValue.Value;
            }
            if (url.ToLower() == "~/cp/allreports.aspx" || url.ToLower() == "~/cp/allsettings.aspx")
            {
                return true;
            }

            if (pageId != 0)
            {
                switch (pageId)
                {
                    case 92:
                    case 93:
                        if (CommonManager.CompanySetting.IsNset == false)
                        {
                            return false;
                        }
                        break;
                    case 43:
                        if (CommonManager.CalculationConstant.CompanyIsHPL.Value == false)
                        {
                            return false;
                        }
                        break;
                }
            }
            if (!string.IsNullOrEmpty(url))
            {
                switch (url)
                {
                    case "~/CP/Report/VoucherReport.aspx":
                        if (CommonManager.CompanySetting.WhichCompany != WhichCompany.PSI)
                        {
                            return false;
                        }
                        break;
                    case "~/CP/EmployeeMasterD2.aspx":
                        if (CommonManager.CompanySetting.IsD2 == false)
                        {
                            return false;
                        }
                        break;
                    case "~/CP/Extension/DeemedSalaryImport.aspx":
                        if (PayManager.HasDeemedIncome())
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                        break;
                }
            }

            if (!isCustomRole)
            {
                return true;
            }
            if (pageId == 0)
            {
                return false;
            }
            return pagesPerimissible.Contains(pageId);
        }

        private void LoadMenus()
        {
            var doc = ProcessAsPerRole();



            var rootNodes =
            (((System.Xml.XmlDocument)((System.Xml.XmlNode)(doc))).ChildNodes);
            rootNodes = rootNodes[1].ChildNodes;
        }


        public string GetToolTip(XmlAttribute value)
        {
            if (value == null)
            {
                return string.Empty;
            }
            return value.Value.ToString();
        }
    }
}
