﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.Security;
using BLL;
using BLL.Manager;
using System.Xml;
using Utils;
using System.IO;
using Utils.Helper;
using DAL;
using Ext.Net;
using System.Xml.Linq;
using BLL.Base;
namespace Web.Master
{
    public partial class EmployeeMS : BaseMasterPage
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            SelectMenu();





        }



        void SelectMenu()
        {
            for (int i = 1; i <= 13; i++)
            {
                System.Web.UI.WebControls.HyperLink menu = this.FindControl("M" + i) as System.Web.UI.WebControls.HyperLink;
                if (menu != null)
                {
                    string page = menu.NavigateUrl.Substring(menu.NavigateUrl.LastIndexOf("/") + 1).ToLower();
                    if (Request.Url.ToString().ToLower().IndexOf(page) >= 0)
                    {
                        menu.CssClass = "active";
                    }
                }
            }
        }
    }
}
