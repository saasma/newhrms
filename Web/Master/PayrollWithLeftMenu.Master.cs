﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.Security;
using BLL;
using BLL.Manager;
using System.Xml;
using Utils;
using System.IO;
using Utils.Helper;
using DAL;
using Ext.Net;
using System.Xml.Linq;
using BLL.Base;
namespace Web.Master
{
    public partial class PayrollWithLeftMenu : BaseMasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            LoadMenus();
        }

        //to differtiate custom role user
        public bool isCustomRole = false;
        public int[] pagesPerimissible = null;

     

        private void LoadMenus()
        {


            

            XmlDocument doc = new XmlDocument();
            doc.Load(HttpContext.Current.Server.MapPath("~/menus/Payroll.xml"));

            //node.AppendChild(newNode);


            XmlNodeList rootNodes =
             (((System.Xml.XmlDocument)((System.Xml.XmlNode)(doc))).ChildNodes);
            rootNodes = rootNodes[1].ChildNodes;

            //load root menus
            rptMainMenu.DataSource = rootNodes;
            rptMainMenu.DataBind();






        }

        public bool IsAccessible(XmlAttribute value, XmlAttribute urlValue)
        {
             return true;
            int pageId = 0;
            string url = "";

            if (value == null || string.IsNullOrEmpty(value.Value))
                ;//return false;
            else
                pageId = int.Parse(value.Value);


            if (urlValue == null || string.IsNullOrEmpty(urlValue.Value))
                ;
            else
                url = urlValue.Value;

            if (url.ToLower() == "~/cp/allreports.aspx" || url.ToLower() == "~/cp/allsettings.aspx")
                return true;

            // Extra logic to hide company specific menus to other company for the menus like "Import CIT only for Nset","Allowance Setting" only for HPL

            if (pageId != 0)
            {
                switch (pageId)
                {
                    case 92: // Import CIT
                    case 93: // Leave Transffer
                        if (CommonManager.CompanySetting.IsNset == false)
                            return false;
                        break;
                    case 43: // HPL Allowance setting
                        if (CommonManager.CalculationConstant.CompanyIsHPL.Value == false)
                            return false;
                        break;
                }
            }
            if (!string.IsNullOrEmpty(url))
            {
                switch (url)
                {
                    case "~/CP/Report/VoucherReport.aspx":
                        if (CommonManager.CompanySetting.WhichCompany != WhichCompany.PSI)
                            return false;
                        break;
                    case "~/CP/EmployeeMasterD2.aspx":
                        if (CommonManager.CompanySetting.IsD2 == false)
                            return false;
                        break;
                    case "~/CP/Extension/DeemedSalaryImport.aspx":
                        if (PayManager.HasDeemedIncome())
                            return true;
                        else
                            return false;
                        break;
                }
            }

            // all pages accessible
            if (!isCustomRole)
                return true;

            if (pageId == 0)
                return false;

            return pagesPerimissible.Contains(pageId);
        }
        //public string GetUrl(XmlAttribute value)
        //{


        //    if (value == null || string.IsNullOrEmpty(value.Value))
        //        return "#javascript:void(0)";

        //    return "#" + value.Value;
        //}

        public string GetToolTip(XmlAttribute value)
        {
            if (value == null)
                return "";

            return value.Value.ToString();
        }

    }
}
