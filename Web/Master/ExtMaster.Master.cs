﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BLL.Base;

namespace Web.Master
{
    public partial class ExtMaster : BaseMasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SelectMenu();



           

        }

      

        void SelectMenu()
        {
            for (int i = 1; i <= 13; i++)
            {
                HyperLink menu = this.FindControl("M" + i) as HyperLink;
                if (menu != null)
                {
                    string page = menu.NavigateUrl.Substring(menu.NavigateUrl.LastIndexOf("/") + 1).ToLower();
                    if (Request.Url.ToString().ToLower().IndexOf(page) >= 0)
                    {
                        menu.CssClass = "active";
                    }
                }
            }
        }
    }
}
