﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BLL.Base;
using BLL;

namespace Web.Master
{
    public partial class ForPopupPage : BaseMasterPage
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {

            // validate if Employee login then don't give the access any popup page

            if (Request.IsAuthenticated == false
                || (Request.IsAuthenticated == true && SessionManager.CurrentLoggedInEmployeeId != 0))
            {
                Response.Redirect("~/Default.aspx");
            }



        }

      


    }
}
