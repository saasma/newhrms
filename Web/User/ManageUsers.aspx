<%@ Page Title="Manage User" Language="C#" ValidateRequest="false" EnableEventValidation="false" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" MaintainScrollPositionOnPostback="true" CodeBehind="ManageUsers.aspx.cs"
    Inherits="Web.User.ManageUsers" %>

<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="Paging" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningMsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="InfoMsgCtl" TagPrefix="uc2" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function changePasswordCall(userName) {
            var ret = changePassword("isPopup=true&u=" + userName);
            if (typeof (ret) != 'undefined') {
            }
        }
        function refreshWindow() {
            window.location.reload();
        }

        function reloadManageUsers(childWindow) {

            childWindow.close();
            __doPostBack('Reload', '');
        }

        function popupAddUser() {
            var ddl = document.getElementById('<%= ddlUserType.ClientID%>');
            var UserType = ddl.value;

            var ret = popupManageUser('UserType=' + UserType);


            if (typeof (ret) != 'undefined') {
                if (ret == 'Reload') {
                    __doPostBack('Reload', '');
                }
            }
            return false;
        }

        function popupEditUser(UserName) {
            var ddl = document.getElementById('<%= ddlUserType.ClientID%>');
            var UserType = ddl.value;
            var ret = popupManageUser('UserName=' + UserName + '&UserType=' + UserType);

            if (typeof (ret) != 'undefined') {
                if (ret == 'Reload') {
                    __doPostBack('Reload', '');
                }
            }
            return false;
        }


        function Check_Click(objRef) {
            //Get the Row based on checkbox
            var row = objRef.parentNode.parentNode;
            if (objRef.checked) {
                //If checked change color to Aqua
                row.style.backgroundColor = "aqua";
            }
            else {
                //If not checked change back to original color
                if (row.rowIndex % 2 == 0) {
                    //Alternating Row Color
                    row.style.backgroundColor = "#C2D69B";
                }
                else {
                    row.style.backgroundColor = "white";
                }
            }

            //Get the reference of GridView
            var GridView = row.parentNode;

            //Get all input elements in Gridview
            var inputList = GridView.getElementsByTagName("input");

            for (var i = 0; i < inputList.length; i++) {
                //The First element is the Header Checkbox
                var headerCheckBox = inputList[0];

                //Based on all or none checkboxes
                //are checked check/uncheck Header Checkbox
                var checked = true;
                if (inputList[i].type == "checkbox" && inputList[i] != headerCheckBox) {
                    if (!inputList[i].checked) {
                        checked = false;
                        break;
                    }
                }
            }
            headerCheckBox.checked = checked;

        }

        function checkAll(objRef) {
            var GridView = objRef.parentNode.parentNode.parentNode;
            var inputList = GridView.getElementsByTagName("input");
            for (var i = 0; i < inputList.length; i++) {
                //Get the Cell To find out ColumnIndex
                var row = inputList[i].parentNode.parentNode;
                if (inputList[i].type == "checkbox" && objRef != inputList[i]) {
                    if (objRef.checked) {
                        //If the header checkbox is checked
                        //check all checkboxes
                        //and highlight all rows
                        row.style.backgroundColor = "aqua";
                        inputList[i].checked = true;
                    }
                    else {
                        //If the header checkbox is checked
                        //uncheck all checkboxes
                        //and change rowcolor back to original
                        if (row.rowIndex % 2 == 0) {
                            //Alternating Row Color
                            row.style.backgroundColor = "#C2D69B";
                        }
                        else {
                            row.style.backgroundColor = "white";
                        }
                        inputList[i].checked = false;
                    }
                }
            }
        }

    </script>
    <style type="text/css">
        .txtClass
        {
            color: White;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
<div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Users
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
    <div class="contentArea1">
        <uc2:InfoMsgCtl ID="msgInfo" EnableViewState="false" Hide="true" runat="server" />
        <uc2:WarningMsgCtl ID="msgWarning" EnableViewState="false" Hide="true" runat="server" />
        
        <div class="attribute" style="padding:10px">
            <table>
                <tr>
                    <td>
                        User Type
                        <asp:DropDownList ID="ddlUserType" Width="120px" runat="server" OnSelectedIndexChanged="ddlUserType_SelectedIndex"
                            AutoPostBack="true">
                            <asp:ListItem Text="User" Value="False" />
                            <asp:ListItem Text="Employee" Value="True" Selected="True" />
                        </asp:DropDownList>
                        User name
                        <asp:TextBox ID="txtUserNameSearch" Width="180px" runat="server" OnTextChanged="txtSearch_Click"
                            AutoPostBack="true" />
                        <cc1:TextBoxWatermarkExtender ID="TBWE2" runat="server" TargetControlID="txtUserNameSearch"
                            WatermarkText="Employee Name" WatermarkCssClass="searchBoxText" />
                        <cc1:AutoCompleteExtender EnableCaching="true" ID="AutoCompleteExtenderOrganization"
                            runat="server" MinimumPrefixLength="2" ServiceMethod="GetEmployeeNames" ServicePath="~/PayrollService.asmx"
                            TargetControlID="txtUserNameSearch" OnClientItemSelected="ACE_item_selected"
                            CompletionSetCount="10" CompletionInterval="250" CompletionListCssClass="AutoExtender"
                            CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
                        </cc1:AutoCompleteExtender>
                    </td>

                    <td><asp:Button UseSubmitBehavior="false" ID="btnAddUser" style="margin-left:20px;" runat="server" CssClass="btn btn-primary btn-sect btn-sm" Text="Add New User" 
                OnClientClick="return popupAddUser();" /></td>
                    <td>
                        <asp:LinkButton ID="LinkButton1" runat="server" Text="Import Users" OnClientClick="userPopup();return false;"
                            CssClass=" btn btn-info btn-sm btn-sect" Style="float: left; margin-left:20px; margin-right:20px;" />
                    </td>

                    <td>
                        <asp:Button OnClientClick="return confirm('Confirm send the passwords to the selected user?');"
                            CssClass="btn btn-success btn-sect btn-sm" ID="btnSendPassword" Width="120px" runat="server" Text="Send Passwords"
                            OnClick="btnSendPassword_Click" />
                    </td>
                </tr>
            </table>
        </div>
        <cc2:EmptyDisplayGridView Width="100%" Style='clear: both; margin-bottom:0px' PagerStyle-HorizontalAlign="Center"
            PagerStyle-CssClass="defaultPagingBar" CssClass="table table-primary mb30 table-bordered table-hover" UseAccessibleHeader="true"
            ShowHeaderWhenEmpty="True" ID="gvwRoles" runat="server" DataKeyNames="UserName"
            AutoGenerateColumns="False" CellPadding="4" GridLines="None" ShowFooterWhenEmpty="False"
            OnPageIndexChanged="gvwRoles_PageIndexChanged" OnPageIndexChanging="gvwRoles_PageIndexChanging"
            OnRowDeleting="gvwRoles_RowDeleting" OnRowCommand="gvwRoles_RowCommand">
            <Columns>
                <asp:TemplateField HeaderStyle-Width="20px" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                    <HeaderTemplate>
                        <asp:CheckBox ID="checkAll" runat="server" onclick="checkAll(this);" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:CheckBox ID="CheckBox1" runat="server" onclick="Check_Click(this)" />
                        <asp:Label ID="lblUserName" Visible="false" Text='<%# Eval("UserName")%>' runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="UserName" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="50px">
                    <ItemTemplate>
                        <%# Eval("UserName")%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Email" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="100px">
                    <ItemTemplate>
                        <%# Eval("Email")%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Role" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="50px">
                    <ItemTemplate>
                        <%# Eval("RoleName")%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="EIN" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="20px">
                    <ItemTemplate>
                        <%# (Eval("EmployeeId"))%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Employee Name"
                    HeaderStyle-Width="120px">
                    <ItemTemplate>
                        <%# (Eval("EmployeeName"))%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Name" HeaderStyle-Width="120px">
                    <ItemTemplate>
                        <%# Eval("NameIfNotEmployee")%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Active" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="80px">
                    <ItemTemplate>
                        <asp:Image ID="image" Style='cursor: pointer; display: block' Width="20" Height="20"
                            runat="server" ToolTip='<%# Convert.ToBoolean( Eval("IsActive")) ? "Active" : "Not Active  " %>'
                            ImageUrl='<%# Convert.ToBoolean( Eval("IsActive")) ? "~/images/yes.png" : "~/images/no.png" %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Change Password" ItemStyle-HorizontalAlign="Center"
                    HeaderStyle-Width="100px">
                    <ItemTemplate>
                        <asp:HyperLink runat="server" Text="Change Password" NavigateUrl='<%# "javascript:changePasswordCall(\"" +  Eval("UserName") + "\")" %> '
                            Style="color: blue" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Switch User" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="100px">
                    <ItemTemplate>
                        <asp:LinkButton ID="btnSwitch" OnClientClick="return confirm('Confirm switch to employee login?');"
                            runat="server" Text="Switch" CommandName="Switch" Style="color: blue" CommandArgument='<%# Eval("EmployeeId") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Send Password" ItemStyle-HorizontalAlign="Center"
                    HeaderStyle-Width="100px">
                    <ItemTemplate>
                        <asp:LinkButton Visible='<%# ! IsMainAdmin(Eval("UserName")) %>' Enabled='<%# IsPasswordSendingEnable(Eval("Email")) %>'
                            ID="btnSendPasswordToMail" runat="server" Text="Send" CommandName="SendPassword"
                            Style="color: blue" CommandArgument='<%# Eval("UserName") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderStyle-Width="50px">
                    <ItemTemplate>
                        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/images/edit.gif" OnClientClick='<%# "javascript:popupEditUser(\"" + Eval("UserName") + "\")"%> ' />
                        &nbsp;
                        <asp:ImageButton Visible='<%# !IsMainAdmin(Eval("UserName")) %>' ID="ImageButton2"
                            OnClientClick="return confirm('Do you really want to delete User?')" runat="server"
                            CommandName="Delete" ImageUrl="~/images/delet.png" />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                </asp:TemplateField>
            </Columns>
            <PagerSettings Mode="NumericFirstLast" NextPageText="Next �" PreviousPageText="� Prev"
                FirstPageText="First" LastPageText="Last" />
            <RowStyle CssClass="odd" />
            <AlternatingRowStyle CssClass="even" />
            <PagerStyle CssClass="defaultPagingBar" />
            <SelectedRowStyle CssClass="selected" />
            <EmptyDataTemplate>
                <b>No Users. </b>
            </EmptyDataTemplate>
        </cc2:EmptyDisplayGridView>
        <uc1:Paging ID="pagingCtl" OnNextRecord="btnNext_Click" OnPrevRecord="btnPrevious_Click"
            OnDropDownShowRecordsChanged="ddlRecords_SelectedIndexChanged" runat="server" />
        <div style='margin-top: 10px'>
        </div>

        <%--<fieldset>
            <legend>User Information </legend>
            <table class="fieldTable">
                <tr id="rowName" runat="server">
                    <td class="fieldHeader">
                        Name *
                    </td>
                    <td style='width: 300px'>
                        <asp:TextBox ID="txtName" runat="server" Width="180px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3333" SetFocusOnError="true"
                            ValidationGroup="user" ControlToValidate="txtName" Display="None" ErrorMessage="Name is required."
                            runat="server" />
                    </td>
                </tr>
                <tr runat="server" id="rowEmployee">
                    <td class="fieldHeader">
                        Employee
                    </td>
                    <td>
                        <asp:DropDownList DataValueField="EmployeeId" Width="250px" DataTextField="NameEIN"
                            ID="ddlEmployeeList" runat="server" AppendDataBoundItems="true" AutoPostBack="True"
                            OnSelectedIndexChanged="ddlEmployeeList_SelectedIndexChanged">
                            <asp:ListItem Text="" Value="-1" />
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" InitialValue="-1" SetFocusOnError="true"
                            ValidationGroup="user" ControlToValidate="ddlEmployeeList" Display="None" ErrorMessage="Employee is required."
                            runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="fieldHeader">
                        User Name *
                    </td>
                    <td style='width: 300px'>
                        <asp:TextBox ID="txtUserName" runat="server" Width="180px"></asp:TextBox>
                        <asp:RequiredFieldValidator SetFocusOnError="true" ValidationGroup="user" ControlToValidate="txtUserName"
                            Display="None" ErrorMessage="User Name is required." runat="server" />
                    </td>
                </tr>
                <tr id="rowAD" runat="server">
                    <td class="fieldHeader">
                        Active Directory User Name
                    </td>
                    <td style='width: 300px'>
                        <asp:TextBox ID="txtActiveDirectoryUserName" runat="server" Width="180px"></asp:TextBox>
                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtActiveDirectoryUserName"
                            WatermarkText="Employee Name" WatermarkCssClass="searchBoxText" />
                        <cc1:AutoCompleteExtender EnableCaching="true" ID="AutoCompleteExtenderOrganization"
                            runat="server" MinimumPrefixLength="1" ServiceMethod="GetActiveDirectoryUser"
                            ServicePath="~/PayrollService.asmx" TargetControlID="txtActiveDirectoryUserName"
                            CompletionSetCount="10" CompletionInterval="250" CompletionListCssClass="AutoExtender"
                            CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
                        </cc1:AutoCompleteExtender>
                    </td>
                </tr>
                <tr>
                    <td class="fieldHeader">
                        Email
                    </td>
                    <td>
                        <asp:TextBox ID="txtEmail" runat="server" Width="180px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="fieldHeader">
                        Allowable IP address like(192.168.1,200.1.20)
                    </td>
                    <td>
                        <asp:TextBox ID="txtIPList" runat="server" Width="300px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="fieldHeader">
                        Role
                    </td>
                    <td>
                        <asp:DropDownList DataTextField="Name" Width="180px" DataValueField="RoleId" ID="ddlRoles"
                            runat="server">
                            <asp:ListItem Text="" Value="-1" />
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" InitialValue="-1" SetFocusOnError="true"
                            ValidationGroup="user" ControlToValidate="ddlRoles" Display="None" ErrorMessage="Role is required."
                            runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="fieldHeader">
                        Is Active
                    </td>
                    <td>
                        <asp:CheckBox Checked="true" ID="chkIsActive" runat="server" />
                    </td>
                </tr>
                <tr id="rowSignOff" runat="server" visible="false">
                    <td class="fieldHeader">
                        Payroll Signoff/Approval
                    </td>
                    <td>
                        <asp:CheckBox Checked="true" ID="chkPayrollSignOffApproval" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                    
                        <asp:Button ValidationGroup="save" OnClientClick="valGroup='user';return CheckValidation()"
                            CssClass="save" ID="btnInsertUpdate" runat="server" Text="Save" OnClick="btnInsertUpdate_Click" />
                        <asp:Button ID="btnCancel" CssClass="cancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                    </td>
                </tr>
            </table>
        </fieldset>--%>
    </div>
</div>
</asp:Content>
