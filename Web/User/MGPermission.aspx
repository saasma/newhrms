<%@ Page Title="Manage Permission" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="MGPermission.aspx.cs" Inherits="Web.User.MGPermission" %>

<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningMsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="InfoMsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script language="javascript" type="text/javascript">
        //************************** Treeview Parent-Child check behaviour ****************************//


        var onCheckChange = function (node, checked) {
            if (!this.locked) {
                this.locked = true;

                if (node.hasChildNodes()) {
                    node.cascade(function (childNode) {
                        childNode.set("checked", checked);
                    });
                }

                var nodoPadre = node.parentNode;
                if (nodoPadre != null) {
                    if (checked == false) {
                        nodoPadre.set("checked", false);
                        var nodoPadre2 = nodoPadre.parentNode;
                        if (nodoPadre2 != null) {
                            nodoPadre2.set("checked", false);
                        }
                    } else {
                        var todosCheck = true;
                        nodoPadre.cascade(function (childNode2) {
                            if ((childNode2.data.checked == false) && (nodoPadre.data.text != childNode2.data.text)) { todosCheck = false; }
                        });
                        if (todosCheck == true) { nodoPadre.set("checked", true); }

                        var nodoPadre2 = nodoPadre.parentNode;
                        if (nodoPadre2 != null) {
                            var todosCheck2 = true;
                            nodoPadre2.cascade(function (childNode3) {
                                if ((childNode3.data.checked == false) && (nodoPadre2.data.text != childNode3.data.text)) { todosCheck2 = false; }
                            });
                            if (todosCheck2 == true) { nodoPadre2.set("checked", true); }
                        }
                    }
                }
                this.locked = false;
            }
        }

        function getSelectedNodes(tree) {
            return "";
        }

        function expandAll(btn) {
          

                <%=treePanel.ClientID %>.expandAll();
           

             

            

        }
        function collapseAll(btn) {
          

            


               <%=treePanel.ClientID %>.collapseAll();
            

        }
    </script>
    <style type="text/css">
        .moduleClass
        {
            background-image: url(../images/next.jpg) !important;
            width:20px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="contentArea">
        <%--<ext:ResourceManager ID="ResourceManager1" Namespace="" ShowWarningOnAjaxFailure="false"
            Theme="Default" runat="server" />--%>
        <div class="pageheader">
            <div class="media">
                <div class="media-body">
                    <h4>
                        Manage Role Permission
                    </h4>
                </div>
            </div>
        </div>
        <div class="contentpanel">
            <div class="alert alert-info">
                <table>
                    <tr>
                        <td>
                            <ext:ComboBox ID="cmbRoles" runat="server" FieldLabel="Role" LabelAlign="Top" Width="200"
                                QueryMode="Local" DisplayField="Name" ValueField="RoleId" ForceSelection="true">
                                <Store>
                                    <ext:Store ID="storeFlowType" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Name" />
                                            <ext:ModelField Name="RoleId" Type="String" />
                                        </Fields>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>
                                    <Select OnEvent="cmbRoles_Change">
                                        <EventMask ShowMask="true" />
                                    </Select>
                                </DirectEvents>
                            </ext:ComboBox>
                            <asp:RequiredFieldValidator ID="valReqdName" runat="server" ControlToValidate="cmbRoles"
                                Display="None" ErrorMessage="Please select a role." ValidationGroup="save"></asp:RequiredFieldValidator>
                        </td>
                        <td style="padding-left: 15px; padding-top: 20px">
                            <ext:LinkButton runat="server" ID="expandAllCollapseAll" Text="Expand">
                                <Listeners>
                                    <Click Handler="expandAll(this);" />
                                </Listeners>
                            </ext:LinkButton>
                        </td>
                        <td style="padding-left: 15px; padding-top: 20px">
                            <ext:LinkButton runat="server" ID="LinkButton1" Text="Collapse">
                                <Listeners>
                                    <Click Handler="collapseAll(this);" />
                                </Listeners>
                            </ext:LinkButton>
                        </td>
                    </tr>
                </table>
            </div>
            <div style='clear: both'>
            </div>
            <uc2:InfoMsgCtl ID="msgInfo" EnableViewState="false" Hide="true" runat="server" />
            <uc2:WarningMsgCtl ID="msgWarning" EnableViewState="false" Hide="true" runat="server" />
            <div>
                <div class="panel-body">
                    <ext:TreePanel ID="treePanel" CheckedNodes="true" StyleSpec="padding-bottom:10px"
                        runat="server" Width="600" MinHeight="600" UseArrows="true" RootVisible="false"
                        MultiSelect="true" FolderSort="true">
                        <SelectionModel>
                            <ext:TreeSelectionModel Mode="Simple" runat="server" />
                        </SelectionModel>
                        <Listeners>
                            <CheckChange Fn="onCheckChange" />
                        </Listeners>
                        <Fields>
                            <ext:ModelField Name="title" />
                            <ext:ModelField Name="url" />
                            <ext:ModelField Name="isPage" />
                            <ext:ModelField Name="moduleId" />
                        </Fields>
                        <ColumnModel>
                            <Columns>
                                <ext:TreeColumn ID="TreeColumn1" runat="server" Text="Pages" Width="400" Sortable="true"
                                    DataIndex="title" />
                            </Columns>
                        </ColumnModel>
                    </ext:TreePanel>
                    <div style='clear: both; text-align: left' class="buttonsDiv">
                        <ext:LinkButton ID="btnSave" Cls="btn btn-primary btn-sect" runat="server" StyleSpec="padding:0px;margin-top:20px;width:100px"
                            Text="Save Changes">
                            <Listeners>
                                <Click Handler="valGroup = 'save'; return CheckValidation();" />
                            </Listeners>
                            <DirectEvents>
                                <Click OnEvent="btnSave_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                        </ext:LinkButton>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
