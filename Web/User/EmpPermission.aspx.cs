﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BLL.Manager;
using BLL;
using System.Text;
using BLL.BO;
using System.Xml;
using Ext.Net;
using BLL.Base;

namespace Web.User
{
    public partial class EmpPermission : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.AdministratorTools;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadTree();
            }
        }

       
        private void LoadTree()
        {
            XmlDocument doc = UserManager.GetEMpMenuXml();

            List<siteMapNode> list = new List<siteMapNode>();

            List<EPermissionPage> pages = UserManager.GetEPermissionPagesByCompanyId();

            XmlNodeList moduleList = doc.ChildNodes[1].ChildNodes;

            Node rootNode = new Node();

            
            foreach (XmlNode module in moduleList)
            {
                Node moduleChild = new Node();
                moduleChild.Checked = true;
                moduleChild.Expanded = true;
                moduleChild.IconCls = "moduleClass";

                moduleChild.CustomAttributes.Add(new ConfigItem { Name = "title", Value = module.Attributes["title"].Value });
                moduleChild.CustomAttributes.Add(new ConfigItem { Name = "moduleId", Value = module.Attributes["moduleId"].Value });
                moduleChild.CustomAttributes.Add(new ConfigItem { Name = "isPage", Value = "false" });
                moduleChild.CustomAttributes.Add(new ConfigItem { Name = "url", Value = module.Attributes["url"].Value });

                rootNode.Children.Add(moduleChild);

                XmlNodeList pageList = module.ChildNodes;
                bool allPageChecked = true;

                foreach (XmlNode page in pageList)
                {

                    Node pagechild = new Node();
                    pagechild.Expanded = false;


                    if (pages != null && IsContainsPage(page.Attributes["url"].Value.ToLower(), pages))
                    {
                        pagechild.Checked = true;
                    }
                    else
                    {
                        allPageChecked = false;
                        pagechild.Checked = false;
                    }
                    pagechild.Leaf = true;
                    pagechild.CustomAttributes.Add(new ConfigItem { Name = "title", Value = page.Attributes["title"].Value });
                    pagechild.CustomAttributes.Add(new ConfigItem { Name = "url", Value = page.Attributes["url"].Value.ToLower() });
                    pagechild.CustomAttributes.Add(new ConfigItem { Name = "moduleId", Value = module.Attributes["moduleId"].Value });
                    pagechild.CustomAttributes.Add(new ConfigItem { Name = "isPage", Value = "true" });

                    moduleChild.Children.Add(pagechild);

                }
                if (allPageChecked)
                    moduleChild.Checked = true;
                else
                    moduleChild.Checked = false;
                
            }

            treePanel.SetRootNode(rootNode);
        }

        public bool IsContainsPage(string url, List<EPermissionPage> pages)
        {
            foreach (EPermissionPage page in pages)
            {
                if (url.Contains(page.Url))
                    return true;
            }
            return false;
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {

            List<EPermissionPage> list = new List<EPermissionPage>();
           
            int count = 0;
            if (treePanel.CheckedNodes != null)
            {
                foreach (SubmittedNode item in treePanel.CheckedNodes)
                {
                    if (item.Attributes["isPage"] != null && item.Attributes["isPage"].ToString() != "")
                    {
                        if ((bool.Parse(item.Attributes["isPage"].ToString()) )|| (!string.IsNullOrEmpty(item.Attributes["url"].ToString())))
                        {
                            string url = item.Attributes["url"].ToString().ToLower().Trim().Replace("~/", "");
                            if (!string.IsNullOrEmpty(url))
                            {
                                EPermissionPage obj = new EPermissionPage();
                                obj.ModuleId = int.Parse(item.Attributes["moduleId"].ToString());
                                obj.CompanyId = SessionManager.CurrentCompanyId;
                                obj.Title = item.Attributes["title"].ToString();
                                obj.Url = url;
                                obj.IsPage = bool.Parse(item.Attributes["isPage"].ToString());
                                list.Add(obj);                                
                                count += 1;
                            }
                        }
                    }
                }
            }


            if (count == 0)
            {
                NewMessage.ShowWarningMessage("Permission must be set for some page or permission not changed.");
                return;
            }

            UserManager.SaveEmpPermission(list);

            NewMessage.ShowNormalMessage("Permission saved for " + count + " pages.");
        }


    }
}