﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using DAL;
using BLL;
using Utils.Helper;

namespace Web.User
{
    public partial class ManageRoleDetails : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialise();
            }
        }

        private void Initialise()
        {
            chkModuleList.DataSource = UserManager.GetModuleList();
            chkModuleList.DataBind();

            chkBranch.DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId)
                .OrderBy(x => x.Name).ToList();
            chkBranch.DataBind();

            rdbNoRestriction_CheckedChanged(null, null);

            if (Request.QueryString["Id"] != null)
            {
                hdnRoleId.Value = Request.QueryString["Id"];
                FillRollDetailsById(int.Parse(hdnRoleId.Value));
            }
        }

        private void FillRollDetailsById(int id)
        {
            URole role = UserManager.GetRoleById(id);
            if (role != null)
            {
                txtName.Text = role.Name;
                txtDescription.Text = role.Description;
                chkIsActive.Checked = role.IsActive.Value;
                btnInsertUpdate.Text = Resources.Messages.Update;
                if (role.IsReadOnly != null)
                {
                    if (role.IsReadOnly.Value)
                    {
                        rdbList.Items[1].Selected = true;
                        rdbList.Items[0].Selected = false;
                    }
                    else
                    {
                        rdbList.Items[0].Selected = true;
                        rdbList.Items[1].Selected = false;
                    }
                }
                else
                {
                    rdbList.Items[0].Selected = true;
                    rdbList.Items[1].Selected = false;
                }

                chkBranch.ClearSelection();

                if (role.HasDepartmentAssigned == null || role.HasDepartmentAssigned == false)
                {
                    rdbNoRestriction.Checked = true;
                    rdbAllowOnly.Checked = false;

                    chkBranch.Enabled = false;
                }
                else
                {
                    rdbNoRestriction.Checked = false;
                    rdbAllowOnly.Checked = true;

                    chkBranch.Enabled = true;

                    foreach (URoleBranch roleDep in role.URoleBranches)
                    {
                        ListItem item = chkBranch.Items.FindByValue(roleDep.BranchId.ToString());
                        if (item != null)
                        {
                            item.Selected = true;
                        }
                    }
                }

                foreach (URoleModule roleDep in role.URoleModules)
                {
                    ListItem item = chkModuleList.Items.FindByValue(roleDep.ModuleRef_ID.ToString());
                    if (item != null)
                    {
                        item.Selected = true;
                    }
                }
            }
        }

        protected void btnInsertUpdate_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(hdnRoleId.Value))
            {
                string rolename = txtName.Text.Trim();

                if (UserManager.IsRoleNameAlreadyExists(rolename, 0))
                {
                    msgWarning.InnerHtml = Resources.Messages.RoleAlreadyExistsMsg;
                    msgWarning.Hide = false;
                }
                else
                {

                    URole role = new URole();
                    role.Name = rolename;

                    if (!string.IsNullOrEmpty(txtDescription.Text.Trim()))
                        role.Description = txtDescription.Text.Trim();

                    role.IsReadOnly = bool.Parse(rdbList.SelectedValue.ToString());
                    role.HasDepartmentAssigned = (rdbAllowOnly.Checked ? true : false);

                    if (role.HasDepartmentAssigned.Value)
                    {
                        foreach (ListItem item in chkBranch.Items)
                        {
                            if (item.Selected)
                            {
                                role.URoleBranches.Add(
                                    new URoleBranch { BranchId = int.Parse(item.Value) });
                                role.HasDepartmentAssigned = true;
                            }
                        }
                    }

                    foreach (ListItem item in chkModuleList.Items)
                    {
                        if (item.Selected)
                        {
                            role.URoleModules.Add(
                                new URoleModule { ModuleRef_ID = int.Parse(item.Value) });
                        }
                    }

                    UserManager.CreateRole(role);
                    JavascriptHelper.DisplayClientMsg(Resources.Messages.RoleCreatedMsg, Page, "closePopup();");
                    Clear();
                }
            }
            else
            {
                URole role = new URole();
                role.Name = txtName.Text.Trim();

                if (!string.IsNullOrEmpty(txtDescription.Text.Trim()))
                    role.Description = txtDescription.Text.Trim();

                role.IsActive = true;// chkIsActive.Checked;
                role.RoleId = int.Parse(hdnRoleId.Value);
                role.IsReadOnly = bool.Parse(rdbList.SelectedValue.ToString());
                role.HasDepartmentAssigned = (rdbAllowOnly.Checked ? true : false);

                foreach (ListItem item in chkModuleList.Items)
                {
                    if (item.Selected)
                    {
                        role.URoleModules.Add(
                            new URoleModule { ModuleRef_ID = int.Parse(item.Value) });
                    }
                }

                if (role.HasDepartmentAssigned.Value)
                {
                    foreach (ListItem item in chkBranch.Items)
                    {
                        if (item.Selected)
                        {
                            role.URoleBranches.Add(
                                new URoleBranch { BranchId = int.Parse(item.Value) });
                            role.HasDepartmentAssigned = true;
                        }
                    }
                }

                if (UserManager.IsRoleNameAlreadyExists(role.Name, role.RoleId))
                {
                    msgWarning.InnerHtml = Resources.Messages.RoleAlreadyExistsMsg;
                    msgWarning.Hide = false;
                }
                else
                {
                    UserManager.UpdateRole(role);
                    JavascriptHelper.DisplayClientMsg(Resources.Messages.RoleUpdatedMsg, Page, "closePopup();");
                    Clear();
                }
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Clear();
        }

        void Clear()
        {
            txtName.Text = "";
            chkIsActive.Checked = true;
            btnInsertUpdate.Text = Resources.Messages.Save;
            rdbList.Items[0].Selected = true;
            chkBranch.ClearSelection();
        }

        protected void rdbNoRestriction_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbAllowOnly.Checked)
            {
                chkBranch.Enabled = true;
            }
            else
                chkBranch.Enabled = false;
            this.chkBranch.ClearSelection();
        }

    }
}