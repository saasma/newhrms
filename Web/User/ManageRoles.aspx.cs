﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using DAL;
using BLL;
using Utils.Helper;

namespace Web.User
{
    public partial class ManageRoles : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.AdministratorTools;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialise();
            }

            if (Request.Form["__EVENTTARGET"] != null && Request.Form["__EVENTTARGET"].Equals("Reload"))
                BindRoles();

            JavascriptHelper.AttachPopUpCode(Page, "rolePopUp", "ManageRoleDetails.aspx", 900, 650);
        }

        private void Initialise()
        {
            BindRoles();
        }

        void BindRoles()
        {
            gvwRoles.DataSource = UserManager.GetRoles(0);
            gvwRoles.DataBind();
        }

        protected void gvwRoles_PageIndexChanged(object sender, EventArgs e)
        {

        }

        public string GetPermission(object value)
        {
            if (value == null)
                return "View and Edit";

            if (bool.Parse(value.ToString()))
                return "View Only";
            return "View and Edit"; 
        }

        public bool CanChange(object value)
        {
            return Convert.ToBoolean(value);
        }

        protected void gvwRoles_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvwRoles.PageIndex = e.NewPageIndex;
            gvwRoles.SelectedIndex = -1;
            BindRoles();
        }

         protected void Button1_Click(object sender, EventArgs e)
        {
            gvwRoles.SelectedIndex = -1;
            BindRoles();
        }    

        protected void gvwRoles_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int id = (int)gvwRoles.DataKeys[e.RowIndex]["RoleId"];

            if(UserManager.IsRoleInUse(id))
            {
                msgWarning.InnerHtml = Resources.Messages.RoleIsInUseMsg;
                msgWarning.Hide = false;
            }
            else
            {
                URole role = new URole();
                role.RoleId =id;

                UserManager.DeleteRole(role);
                msgInfo.InnerHtml = Resources.Messages.RoleDeletedMsg; ;
                msgInfo.Hide = false;

                gvwRoles.SelectedIndex = -1;
                BindRoles();
            }
        }

    }
}
