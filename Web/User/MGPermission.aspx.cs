﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using System.Collections.Generic;
using BLL.Manager;
using BLL;
using System.Text;
using BLL.BO;
using System.Xml;
using Ext.Net;
using BLL.Base;

namespace Web.User
{
    public partial class MGPermission : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.AdministratorTools;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack && !X.IsAjaxRequest)
            {
                LoadPages();
                LoadNodes(null);
            }

        }

        public void cmbRoles_Change(object sender, DirectEventArgs e)
        {
            int roleId = int.Parse(cmbRoles.SelectedItem.Value);

            List<UPermission> pages = UserManager.GetPermissionByRole(roleId);




            LoadNodes(pages);


        }

        protected void ddlRoles_SelectedIndexChanged(object sender, EventArgs e)
        {
            //List<URole> roles = UserManager.GetRoles(0);

            //for (int i = roles.Count - 1; i >= 0; i--)
            //{
            //    URole role = roles[i];
            //    if ((Role)role.RoleId == Role.Administrator
            //        || (Role)role.RoleId == Role.Employee)
            //    {
            //        roles.RemoveAt(i);
            //    }
            //}

            //ddlRoles.DataSource = roles;
            //ddlRoles.DataBind();

            
            //int roleId = int.Parse(Request.Form[ddlRoles.UniqueID]);

            //ddlRoles.SelectedValue = roleId.ToString();

            //if (roleId != -1)
            //{
            //    List<UPermission> pages = UserManager.GetPermissionByRole(roleId);
            //    LoadNodes(pages);
            //}
        }

        public bool IsContainsPage(string url, List<UPermission> pages)
        {
            foreach (UPermission page in pages)
            {
                if (url.Contains(page.Url))
                    return true;
            }
            return false;
        }

        public void LoadNodes(List<UPermission> pages)
        {
            XmlDocument doc = MyCache.GetMenuXml(SessionManager.User.UserID.ToString(), CommonManager.CompanySetting.HasLevelGradeSalary);

            List<string> addedUrls = new System.Collections.Generic.List<string>();

            List<siteMapNode> list = new List<siteMapNode>();

            XmlNodeList moduleList = doc.ChildNodes[1].ChildNodes;

            Node rootNode = new Node();

            foreach (XmlNode module in moduleList)
            {
                Node moduleChild = new Node();
                moduleChild.Checked = false;
                moduleChild.Expanded = true;
                moduleChild.IconCls = "moduleClass";
                
                moduleChild.CustomAttributes.Add(new ConfigItem { Name = "title", Value = module.Attributes["title"].Value });
                moduleChild.CustomAttributes.Add(new ConfigItem { Name = "isPage", Value = "false" });

                rootNode.Children.Add(moduleChild);

                XmlNodeList groupList = module.ChildNodes;
                
                foreach (XmlNode group in groupList)
                {
                    Node groupchild = new Node();
                    
                    groupchild.Expanded = false;
                    groupchild.Checked = false;
                    groupchild.CustomAttributes.Add(new ConfigItem { Name = "title", Value = group.Attributes["title"].Value });
                    groupchild.CustomAttributes.Add(new ConfigItem { Name = "isPage", Value = "false" });
                    moduleChild.Children.Add(groupchild);

                    XmlNodeList pageList = group.ChildNodes;
                    bool allPageChecked = true;
                    foreach (XmlNode page in pageList)
                    {

                        Node pagechild = new Node();
                        pagechild.Expanded = false;
                       

                        if (pages != null && IsContainsPage(page.Attributes["url"].Value.ToLower(), pages))
                        {
                            pagechild.Checked = true;
                        }
                        else
                        {
                            allPageChecked = false;
                            pagechild.Checked = false;
                        }
                        pagechild.Leaf = true;
                        pagechild.CustomAttributes.Add(new ConfigItem { Name = "title", Value = page.Attributes["title"].Value });
                        pagechild.CustomAttributes.Add(new ConfigItem { Name = "url", Value = page.Attributes["url"].Value.ToLower() });
                        pagechild.CustomAttributes.Add(new ConfigItem { Name = "isPage", Value = "true" });
                        pagechild.CustomAttributes.Add(new ConfigItem { Name = "moduleId", Value = page.Attributes["moduleId"].Value });

                        groupchild.Children.Add(pagechild);

                        if(addedUrls.Contains(page.Attributes["url"].Value.ToLower()))
                        {
                            NewMessage.ShowWarningMessage(page.Attributes["url"].Value.ToLower() + " url has been repeated.");
                            return;
                        }

                        addedUrls.Add(page.Attributes["url"].Value.ToLower());
                    }
                    if (allPageChecked)
                        groupchild.Checked = true;
                }
            }

            treePanel.SetRootNode(rootNode);
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            if (cmbRoles.SelectedItem == null || cmbRoles.SelectedItem.Value == null)
            {
                NewMessage.ShowWarningMessage("Role must be selected.");
                return;
            }

            List<UPermission> list = new System.Collections.Generic.List<UPermission>();

            int roleId = int.Parse(cmbRoles.SelectedItem.Value);
            //List<string> pages = new System.Collections.Generic.List<string>();
            int count = 0;
            if (treePanel.CheckedNodes != null)
            {
                foreach (SubmittedNode item in treePanel.CheckedNodes)
                {
                    if (item.Attributes["isPage"] != null && item.Attributes["isPage"].ToString() != "")
                    {
                        if (bool.Parse(item.Attributes["isPage"].ToString()))
                        {
                            string url = item.Attributes["url"].ToString().ToLower().Trim().Replace("~/", "");
                            if (!string.IsNullOrEmpty(url))
                            {
                                UPermission pers = new UPermission();
                                pers.ModuleId = int.Parse(item.Attributes["moduleId"].ToString());
                                pers.Url = url;
                                pers.RoleId = roleId;
                                list.Add(pers);
                                //pages.Add(url);
                                count += 1;
                            }
                        }
                    }
                }
            }

            

            //if (bool.Parse(isPage))
            //{
            //    url = url.Replace("~/", "").ToLower().Trim();
            //    pages.Add(url);
            //}

            if (count == 0)
            {
                NewMessage.ShowWarningMessage("Permission must be set for some page or permission not changed.");
                return;
            }

            UserManager.SavePermission(roleId, list);

            NewMessage.ShowNormalMessage("Permission saved for " + count + " pages.");
        }

        private void LoadPages()
        {

            



            List<URole> roles =  UserManager.GetRoles(0);

            for (int i = roles.Count - 1; i >= 0; i--)
            {
                URole role = roles[i];
                if ((Role)role.RoleId == Role.Administrator
                    || (Role)role.RoleId == Role.Employee)
                {
                    roles.RemoveAt(i);
                }
            }

            cmbRoles.Store[0].DataSource = roles;
            cmbRoles.Store[0].DataBind();

            List<UPage> parentPages = UserManager.GetParentPages();
            foreach (UPage page in parentPages)
            {

                TreeNode parentNode = GetNode(page);               
                //tvPages.Nodes.Add(parentNode);


                //load child pages
                List<UPage> childPages = UserManager.GetChildPages(page.PageId);
                foreach (UPage childPage in childPages)
                {
                    TreeNode pageNode = GetNode(childPage);
                    parentNode.ChildNodes.Add(pageNode);

                    //try to load sub section
                    List<UPageSection> sections = UserManager.GetSections(childPage.PageId);
                    foreach (UPageSection section in sections)
                    {
                        pageNode.ChildNodes.Add(GetNode(section));
                    }
                }
            }

        }

        public TreeNode GetNode(UPage page)
        {
            TreeNode node = new TreeNode();
            node.NavigateUrl = "";
            node.SelectAction = TreeNodeSelectAction.None; ;

            node.Value = page.PageId.ToString();
            node.Text = page.PageName;
            return node;
        }

        public TreeNode GetNode(UPageSection page)
        {
            TreeNode node = new TreeNode();
            node.NavigateUrl = "";
            node.SelectAction = TreeNodeSelectAction.None; ;

            node.Value = page.PageSectionId.ToString();
            node.Text = page.SectionName;
            return node;
        }
        private void UnCheckAllNodes(TreeNode node)
        {
            if (node.ChildNodes == null || node.ChildNodes.Count == 0)
                return;

            foreach (TreeNode childNode in node.ChildNodes)
            {
                childNode.Checked = false;
                childNode.Selected = false;
                UnCheckAllNodes(childNode);
            }
            return;
        }

        private void ClearCheckbox()
        {
            //foreach (TreeNode node in tvPages.Nodes)
            //{
            //    UnCheckAllNodes(node);
            //}
        }

        //private string[] GetSelectedPagesRecursively(TreeNode node)
        //{
        //    List<string> selectedPageID = new List<string>();
        //    if (node.ChildNodes == null || node.ChildNodes.Count == 0)
        //        return selectedPageID.ToArray();

        //    foreach (TreeNode childNode in node.ChildNodes)
        //    {
        //        selectedPageID.Add(childNode.
        //    }
        //    return;
        //}

        //public string[] GetSelectedPages()
        //{
        //    List<string> selectedPageID = new List<string>();

        //    foreach (TreeNode node in tvPages.Nodes)
        //    {
        //        selectedPageID.Add(node.Value);
                   
        //    }
        //    return selectedPageID.ToArray();
        //}

        


        protected void btnInsertUpdate_Click(object sender, EventArgs e)
        {
            //int roleId = int.Parse(ddlRoles.SelectedValue);

            //if (roleId == -1)
            //    return;

            //List<string> selectedPages = new List<string>();

            ////foreach (TreeNode node in tvPages.CheckedNodes)
            ////{
            ////    if (node.Depth != 2)
            ////    {
            ////        selectedPages.Add(node.Value);
            ////    }
            ////}
            //string pages = string.Join(",", selectedPages.ToArray());
            ////UserManager.SavePermission(roleId, pages);

            ////save section
            //StringBuilder xml = new StringBuilder("<root>");
            ////foreach (TreeNode node in tvPages.CheckedNodes)
            ////{
            ////    if (node.Depth == 2)
            ////    {
            ////        xml.Append(
            ////           string.Format("<row RoleId=\"{0}\" PageId=\"{1}\" PageSectionId=\"{2}\"   /> ",
            ////                roleId, node.Parent.Value , node.Value)
            ////           );
            ////    }
            ////}
            //xml.Append("</root>");
            //UserManager.SavePermissionSection(xml.ToString(),roleId);

            //msgInfo.InnerHtml = Resources.Messages.UserPagePermissionSaved;
            //msgInfo.Hide = false;
        }
    }
}
