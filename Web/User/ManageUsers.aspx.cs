﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using DAL;
using BLL;
using Utils.Helper;
using Utils;
using Utils.Security;
using System.Web.Security;

namespace Web.User
{
    public partial class ManageUsers : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.AdministratorTools;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialise();
                ddlUserType_SelectedIndex(null, null);
                //disable AutoComplete if LDPA is not enabled
                //if (!LdapHelper.IsLDAPAccessible())
                {
                    //TextBoxWatermarkExtender1.Enabled = false;
                    //AutoCompleteExtenderOrganization.Enabled = false;
                }
            }

            if (Request.Form["__EVENTTARGET"] != null && Request.Form["__EVENTTARGET"].Equals("Reload"))
            {
                gvwRoles.SelectedIndex = -1;
                BindUsers();
            }

            JavascriptHelper.AttachNonDialogPopUpCode(Page, "userPopup", "../ExcelWindow/EmployeeUserExcel.aspx", 450, 500);
            JavascriptHelper.AttachPopUpCode(Page, "changePassword", "../CP/ChangePasswordPopup.aspx", 600, 470);
            JavascriptHelper.AttachPopUpCode(Page, "popupManageUser", "../CP/ManageUserPopup.aspx", 600, 450);
        }

        private void Initialise()
        {
            BindUsers();
            
            //EmployeeManager mgr = new EmployeeManager();
            //ddlEmployeeList.DataSource = EmployeeManager.GetEmployeeListByCompany(SessionManager.CurrentCompanyId);
            //ddlEmployeeList.DataBind();
        }

        public string GetEmployeeName(object value)
        {
            if (value == null)
                return "";
            return (value as EEmployee).Name;

        }
        public string GetEmployeeId(object value)
        {
            if (value == null)
                return "";
            return (value as EEmployee).EmployeeId.ToString();
        }

        public void txtSearch_Click(object sender, EventArgs e)
        {
            BindUsers();
        }

        void BindUsers()
        {
            int totalRecords = 0;

            gvwRoles.SelectedIndex = -1;
            gvwRoles.DataSource = UserManager.GetUserList(txtUserNameSearch.Text, User.Identity.Name,
                Convert.ToBoolean(ddlUserType.SelectedValue), pagingCtl.CurrentPage - 1, int.Parse(pagingCtl.DDLRecords.SelectedValue),
                 ref totalRecords,false);

            pagingCtl.UpdatePagingBar(totalRecords);
                //UserManager.GetUsers(txtUserNameSearch.Text, User.Identity.Name,
                //Convert.ToBoolean(ddlUserType.SelectedValue));
            gvwRoles.DataBind();
        }

        public bool IsMainAdmin(object UserName)
        {
            if (UserName.ToString().ToLower() == "admin")
                return true;
            return false;
        }

        protected void btnPrevious_Click()
        {
            pagingCtl.CurrentPage -= 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage -= 1;
            BindUsers();
        }

        protected void btnNext_Click()
        {
            pagingCtl.CurrentPage += 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage += 1;
            BindUsers();
        }

        protected void ddlRecords_SelectedIndexChanged()
        {
            //SessionManager.EmployeeDisplayRecodsPerPage = int.Parse(pagintCtl.DDLRecords.SelectedValue);
            //_tempCurrentPage = 1;
            pagingCtl.CurrentPage = 1;
            BindUsers();
        }


        protected void gvwRoles_PageIndexChanged(object sender, EventArgs e)
        {

        }

        public bool CanChange(object value)
        {
            return Convert.ToBoolean(value);
        }


        protected void gvwRoles_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvwRoles.PageIndex = e.NewPageIndex;
            gvwRoles.SelectedIndex = -1;
            BindUsers();
        }
        
        protected void ddlUserType_SelectedIndex(object sender, EventArgs e)
        {
            //clear & load roles for User type
            //ddlEmployeeList.SelectedValue = "-1";

            //for User type
            if (ddlUserType.SelectedIndex == 0)
            {
                //RequiredFieldValidator2.Enabled = false;
                //gvwRoles.Columns[5].Visible = true;
                //gvwRoles.Columns[6].Visible = false;
                gvwRoles.Columns[9].Visible = false;
                //gvwRoles.Columns[4].Visible = false;
                gvwRoles.Columns[3].Visible = true;

                //rowSignOff.Visible = true;
                //rowName.Visible = true;
                //txtActiveDirectoryUserName.Text = "";
                //txtActiveDirectoryUserName.Visible = false;
                //rowAD.Visible = false;

                //txtUserName.Enabled = true;
            }
                //for Employee type
            else
            {
                //RequiredFieldValidator2.Enabled = true;
               // gvwRoles.Columns[5].Visible = false;
                //gvwRoles.Columns[6].Visible = true;
                gvwRoles.Columns[9].Visible = true;

                //gvwRoles.Columns[4].Visible = true;
                gvwRoles.Columns[3].Visible = false;

                //rowSignOff.Visible = false;
                //txtActiveDirectoryUserName.Visible = true;
                //rowAD.Visible = true;
                //rowName.Visible = false;

                //txtUserName.Enabled = false;
            }


            //ListItem firstItem = ddlRoles.Items[0];
            //firstItem.Selected = false;
            //ddlRoles.Items.Clear();
            //ddlRoles.ClearSelection();
            ////user type
            //if (!Convert.ToBoolean(ddlUserType.SelectedValue))
            //{
            //    ddlRoles.DataSource = UserManager.GetRoles((int)Role.Employee);
            //    ddlRoles.DataBind();
            //    //rowEmployee.Visible = false;
            //}
            //else
            //{
            //    ListItem item = new ListItem(Role.Employee.ToString(), ((int)Role.Employee).ToString());
            //    ddlRoles.Items.Insert(0, item);                
            //    ddlRoles.Items[0].Selected = true;
            //    //rowEmployee.Visible = true;
            //}
            //ddlRoles.Items.Insert(0, firstItem);
            BindUsers();
        }

        //protected void btnInsertUpdate_Click(object sender, EventArgs e)
        //{
        //    if (this.IsValid)
        //    {

        //        if (gvwRoles.SelectedIndex == -1)
        //        {
        //            UUser user = new UUser();
        //            user.UserName = txtUserName.Text.Trim();
        //            user.NameIfNotEmployee = txtName.Text.Trim();
        //            user.ActiveDirectoryName = txtActiveDirectoryUserName.Text.Trim();
        //            user.Email = txtEmail.Text.Trim();
        //            user.RoleId = int.Parse(ddlRoles.SelectedValue);
        //            user.CompanyId = SessionManager.CurrentCompanyId;
        //            user.AllowableIPList = txtIPList.Text.Trim();
        //            user.IsEmployeeType = Convert.ToBoolean(ddlUserType.SelectedValue);
        //            if (user.IsEmployeeType.Value)
        //            {
        //                user.EmployeeId = int.Parse(ddlEmployeeList.SelectedValue);
        //            }
        //            else
        //            {
        //                user.UserMappedEmployeeId = int.Parse(ddlEmployeeList.SelectedValue);
        //                user.EnablePayrollSignOff = chkPayrollSignOffApproval.Checked;
        //            }
        //            user.IsActive = chkIsActive.Checked;


        //            if (UserManager.IsUserNameAlreadyExists(user.UserName))
        //            {
        //                msgWarning.InnerHtml = Resources.Messages.UserNameAlreadExistsMsg;
        //                msgWarning.Hide = false;
        //            }
        //            else
        //            {
        //                UserManager.CreateUser(user);
        //                msgInfo.InnerHtml = Resources.Messages.UserCreatedMsg;
        //                msgInfo.Hide = false;
        //                btnCancel_Click(null, null);
        //                BindUsers();
        //            }
        //        }
        //        else
        //        {
        //            UUser user = new UUser();
        //            user.UserName = gvwRoles.DataKeys[gvwRoles.SelectedIndex]["UserName"].ToString();
        //            user.NewUserName = txtUserName.Text.Trim();
        //            user.NameIfNotEmployee = txtName.Text.Trim();
        //            user.Email = txtEmail.Text.Trim();
        //            user.RoleId = int.Parse(ddlRoles.SelectedValue);
        //            user.IsActive = chkIsActive.Checked;
        //            user.ActiveDirectoryName = txtActiveDirectoryUserName.Text.Trim();
        //            user.AllowableIPList = txtIPList.Text.Trim();
        //            //user.UserMappedEmployeeId = int.Parse(ddlEmployeeList.SelectedValue);
        //            user.EnablePayrollSignOff = chkPayrollSignOffApproval.Checked;

        //            if (Convert.ToBoolean(ddlUserType.SelectedValue))
        //            {
        //            }
        //            else
        //            {
        //                user.UserMappedEmployeeId = int.Parse(ddlEmployeeList.SelectedValue);
        //                if (user.UserMappedEmployeeId != null && user.UserMappedEmployeeId == -1)
        //                    user.UserMappedEmployeeId = null;
        //                //user.EnablePayrollSignOff = chkPayrollSignOffApproval.Checked;
        //            }

        //            //if (UserManager.IsRoleNameAlreadyExists(role.Name, role.RoleId))
        //            //{
        //            //    msgWarning.InnerHtml = Resources.Messages.RoleAlreadyExistsMsg;
        //            //    msgWarning.Hide = false;
        //            //}
        //            //else
        //            //{

        //            if (UserManager.UpdateUser(user))
        //            {
        //                msgInfo.InnerHtml = Resources.Messages.UserUpdatedMsg;
        //                msgInfo.Hide = false;
        //                BindUsers();
        //                btnCancel_Click(null, null);
        //            }
        //            else
        //            {
        //                msgWarning.InnerHtml = "Username already exists.";
        //                msgWarning.Hide = false;
        //            }
        //            //}
        //        }
        //    }
        //}

        //protected void btnCancel_Click(object sender, EventArgs e)
        //{
        //    txtUserName.Enabled = ddlEmployeeList.Enabled = true;
        //    ddlEmployeeList.ClearSelection();
        //    ddlEmployeeList.Items[0].Selected = true;
        //    gvwRoles.SelectedIndex = -1;
        //    BindUsers();
        //    Clear();
        //}

      
        //protected void gvwRoles_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    string userName = gvwRoles.DataKeys[gvwRoles.SelectedIndex]["UserName"].ToString();
        //    UUser user = UserManager.GetUserByUserName(userName);

        //    if (user != null)
        //    {
        //        txtUserName.Text = user.UserName;
        //        txtName.Text = user.NameIfNotEmployee;
        //        txtActiveDirectoryUserName.Text = user.ActiveDirectoryName;
        //        txtEmail.Text = user.Email;
        //        chkIsActive.Checked = user.IsActive.Value;
        //        btnInsertUpdate.Text = Resources.Messages.Update;
        //        chkPayrollSignOffApproval.Checked=false;
        //        if (user.AllowableIPList == null)
        //            txtIPList.Text = "";
        //        else
        //            txtIPList.Text = user.AllowableIPList;
        //        ddlRoles.ClearSelection();
        //        ListItem item = ddlRoles.Items.FindByValue(user.RoleId.ToString());
        //        if (item != null)
        //            item.Selected = true;
        //        if (user.IsEmployeeType.Value)
        //        {
        //            ddlEmployeeList.ClearSelection();
        //            ListItem item1 = ddlEmployeeList.Items.FindByValue(user.EmployeeId.ToString());
        //            if (item1 != null)
        //                item1.Selected = true;

        //            txtUserName.Enabled = true;
        //            ddlEmployeeList.Enabled = false;
        //        }
        //        else
        //        {
        //            if (user.UserMappedEmployeeId != null)
        //            {
        //                ddlEmployeeList.ClearSelection();
        //                ListItem item1 = ddlEmployeeList.Items.FindByValue(user.UserMappedEmployeeId.ToString());
        //                if (item1 != null)
        //                    item1.Selected = true;
        //            }
                   
        //            if (user.EnablePayrollSignOff != null && user.EnablePayrollSignOff.Value)
        //                chkPayrollSignOffApproval.Checked = user.EnablePayrollSignOff.Value;
        //            txtUserName.Enabled = false;
        //            ddlEmployeeList.Enabled = true;
        //        }
               
                
        //    }
            
        //}

        protected void gvwRoles_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            string userName = gvwRoles.DataKeys[e.RowIndex]["UserName"].ToString();



            if (UserManager.DeleteUser(userName))
            {
                msgInfo.InnerHtml = Resources.Messages.UserDeletedMsg; ;
                msgInfo.Hide = false;
            }
            else
            {
                msgInfo.InnerHtml = "User can not be deleted, its in use." ;
                msgInfo.Hide = false;
            }

            gvwRoles.SelectedIndex = -1;
            BindUsers();
        }

        //protected void ddlEmployeeList_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    if (ddlUserType.SelectedValue.ToLower() == "false")
        //        return;

        //    if (ddlEmployeeList.SelectedValue != "-1")
        //    {
        //        UUser user = UserManager.GetUserByEmployee(int.Parse(ddlEmployeeList.SelectedValue));
        //        if (user != null)
        //        {
        //            txtUserName.Text = user.UserName;
        //            txtName.Text = user.NameIfNotEmployee;
        //            txtEmail.Text = user.Email;
        //            chkIsActive.Checked = user.IsActive.Value;
        //            ddlRoles.ClearSelection();
        //            ListItem item = ddlRoles.Items.FindByValue(user.RoleId.ToString());
        //            if (item != null)
        //                item.Selected = true;
        //            txtUserName.Enabled = false;

        //            btnInsertUpdate.Text = Resources.Messages.Update;
        //        }
        //        else
        //        {
        //            EEmployee employee = EmployeeManager.GetEmployeeById(int.Parse(ddlEmployeeList.SelectedValue));
        //            txtEmail.Text = employee.EAddresses[0].CIEmail;

        //            txtUserName.Enabled = true;
        //            btnInsertUpdate.Text = Resources.Messages.Save;
        //        }

        //    }
        //}


        public bool IsPasswordSendingEnable(object value)
        {
            if (value == null)
                return false;

            if (string.IsNullOrEmpty(value.ToString()))
                return false;

            return true;
        }

        protected void gvwRoles_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "SendPassword")
            {
                string username = e.CommandArgument.ToString();

                string errorMsg =  UserManager.SendNewPasswordInMail(username);

                if (errorMsg == "")
                {
                    msgInfo.InnerHtml = string.Format(Resources.Messages.UserNewPasswordSentToMail, username);
                    msgInfo.Hide = false;
                }
                else
                {
                    msgWarning.InnerHtml = errorMsg;
                    msgWarning.Hide = false;
                }
            }
            else if (e.CommandName == "Switch")
            {
                int employeeId = int.Parse(e.CommandArgument.ToString());
                UUser user = UserManager.GetUserByEmployee(employeeId);
                if (user != null)
                {

                    BLL.BaseBiz.PayrollDataContext.ChangeUserActivities.InsertOnSubmit(
                  BLL.BaseBiz.GetChangeUserActivityLog("User Switched from " + SessionManager.User.UserName + " to " + user.UserName + "."));
                    BLL.BaseBiz.PayrollDataContext.SubmitChanges();

                    Session.Abandon();
                    FormsAuthentication.SignOut();





                    SecurityHelper.CreateCookie(user.UserName,
                        true, user.URole.Name, false, false);

                    Response.Redirect("~/Employee/Dashboard.aspx", true);

                }
            }
        }

        protected void btnSendPassword_Click(object sender, EventArgs e)
        {
            List<string> userNameList = new List<string>();

            foreach (GridViewRow row in gvwRoles.Rows)
            {
                CheckBox CheckBox1 = (CheckBox)row.FindControl("CheckBox1");
                Label lblUserName = (Label)row.FindControl("lblUserName");

                if (CheckBox1.Checked)
                {
                    userNameList.Add(lblUserName.Text);
                }
            }

            if (userNameList.Count == 0)
            {
                msgWarning.InnerHtml = "Please select UserName";
                msgWarning.Hide = false;
                return;
            }

            int mailSentCount = 0;
            string errorMsg = "";
            int i = 0;

            foreach (var userName in userNameList)
            {
                //if (UserManager.SendNewPasswordInMailToEmployee(userName))
                //    mailSentCount++;

                string msg = UserManager.SendNewPasswordInMail(userName);
                if (msg == "")
                    mailSentCount++;
                else
                {
                    if (i == 0)
                    {
                        errorMsg = msg;
                        i++;
                    }
                }
                
               
            }

            if (gvwRoles.HeaderRow != null)
            {
                CheckBox checkAll = (CheckBox)gvwRoles.HeaderRow.FindControl("checkAll");

                if (checkAll.Checked)
                    checkAll.Checked = false;
            }         

            foreach (GridViewRow row in gvwRoles.Rows)
            {               
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox CheckBox1 = (CheckBox)row.FindControl("CheckBox1");
                    if (CheckBox1.Checked)
                        CheckBox1.Checked = false;
                }
            }

            if (errorMsg != "")
            {
                if (mailSentCount == 0)
                {
                    msgWarning.InnerHtml = errorMsg;
                    msgWarning.Hide = false;
                }
                else
                {
                    msgWarning.InnerHtml = "New password send to " + mailSentCount + " users " + " and failed to send to " + (userNameList.Count - mailSentCount) + " users. " +  errorMsg;
                    msgWarning.Hide = false;
                }
                return;
            }
            

            msgInfo.InnerHtml = string.Format("New password send to {0} users", mailSentCount);
            msgInfo.Hide = false;            
        }


    }
}
