﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using System.Collections.Generic;
using BLL.Manager;
using BLL;
using System.Text;

namespace Web.User
{
    public partial class ManagePermission : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                LoadPages();
            }

        }

        private void LoadPages()
        {
            tvPages.Attributes.Add("onclick", "OnTreeClick(event)");


            List<URole> roles =  UserManager.GetRoles(0);

            for (int i = roles.Count - 1; i >= 0; i--)
            {
                URole role = roles[i];
                if ((Role)role.RoleId == Role.Administrator
                    || (Role)role.RoleId == Role.Employee)
                {
                    roles.RemoveAt(i);
                }
            }

            ddlRoles.DataSource = roles;
            ddlRoles.DataBind();

            List<UPage> parentPages = UserManager.GetParentPages();
            foreach (UPage page in parentPages)
            {

                TreeNode parentNode = GetNode(page);               
                tvPages.Nodes.Add(parentNode);


                //load child pages
                List<UPage> childPages = UserManager.GetChildPages(page.PageId);
                foreach (UPage childPage in childPages)
                {
                    TreeNode pageNode = GetNode(childPage);
                    parentNode.ChildNodes.Add(pageNode);

                    //try to load sub section
                    List<UPageSection> sections = UserManager.GetSections(childPage.PageId);
                    foreach (UPageSection section in sections)
                    {
                        pageNode.ChildNodes.Add(GetNode(section));
                    }
                }
            }

        }

        public TreeNode GetNode(UPage page)
        {
            TreeNode node = new TreeNode();
            node.NavigateUrl = "";
            node.SelectAction = TreeNodeSelectAction.None; ;

            node.Value = page.PageId.ToString();
            node.Text = page.PageName;
            return node;
        }

        public TreeNode GetNode(UPageSection page)
        {
            TreeNode node = new TreeNode();
            node.NavigateUrl = "";
            node.SelectAction = TreeNodeSelectAction.None; ;

            node.Value = page.PageSectionId.ToString();
            node.Text = page.SectionName;
            return node;
        }
        private void UnCheckAllNodes(TreeNode node)
        {
            if (node.ChildNodes == null || node.ChildNodes.Count == 0)
                return;

            foreach (TreeNode childNode in node.ChildNodes)
            {
                childNode.Checked = false;
                childNode.Selected = false;
                UnCheckAllNodes(childNode);
            }
            return;
        }

        private void ClearCheckbox()
        {
            foreach (TreeNode node in tvPages.Nodes)
            {
                UnCheckAllNodes(node);
            }
        }

        //private string[] GetSelectedPagesRecursively(TreeNode node)
        //{
        //    List<string> selectedPageID = new List<string>();
        //    if (node.ChildNodes == null || node.ChildNodes.Count == 0)
        //        return selectedPageID.ToArray();

        //    foreach (TreeNode childNode in node.ChildNodes)
        //    {
        //        selectedPageID.Add(childNode.
        //    }
        //    return;
        //}

        //public string[] GetSelectedPages()
        //{
        //    List<string> selectedPageID = new List<string>();

        //    foreach (TreeNode node in tvPages.Nodes)
        //    {
        //        selectedPageID.Add(node.Value);
                   
        //    }
        //    return selectedPageID.ToArray();
        //}

        protected void ddlRoles_SelectedIndexChanged(object sender, EventArgs e)
        {
            int roleId = int.Parse(ddlRoles.SelectedValue);
            List<UPermission> permissions = UserManager.GetPermissionByRole(roleId);

            ClearCheckbox();
            
            //foreach (UPermission permission in permissions)
            //{
            //    UPage page = UserManager.GetPageById(permission.PageId);

            //    string level;

            //    if (page.IsParent.Value)
            //        level = page.PageId.ToString();
            //    else
            //        level = page.UPage1.PageId + "/" + page.PageId;

            //    TreeNode node = tvPages.FindNode(level);
            //    if (node != null)
            //        node.Checked = true;
            //}

            // section permission

            List<UPermissionSection> sections = UserManager.GetPermissionSectionByRole(roleId);
            foreach (UPermissionSection sectionPermission in sections)
            {
                UPageSection section = UserManager.GetPageSectionById(sectionPermission.PageSectionId);
                string level = section.UPage.UPage1.PageId + "/" + section.UPage.PageId + "/" + section.PageSectionId;
                TreeNode node = tvPages.FindNode(level);
                if (node != null)
                    node.Checked = true;

            }
        }

        protected void btnInsertUpdate_Click(object sender, EventArgs e)
        {
            int roleId = int.Parse(ddlRoles.SelectedValue);

            if (roleId == -1)
                return;

            List<string> selectedPages = new List<string>();

            foreach (TreeNode node in tvPages.CheckedNodes)
            {
                if (node.Depth != 2)
                {
                    selectedPages.Add(node.Value);
                }
            }
            string pages = string.Join(",", selectedPages.ToArray());
            //UserManager.SavePermission(roleId, pages);

            //save section
            StringBuilder xml = new StringBuilder("<root>");
            foreach (TreeNode node in tvPages.CheckedNodes)
            {
                if (node.Depth == 2)
                {
                    xml.Append(
                       string.Format("<row RoleId=\"{0}\" PageId=\"{1}\" PageSectionId=\"{2}\"   /> ",
                            roleId, node.Parent.Value , node.Value)
                       );
                }
            }
            xml.Append("</root>");
            UserManager.SavePermissionSection(xml.ToString(),roleId);

            msgInfo.InnerHtml = Resources.Messages.UserPagePermissionSaved;
            msgInfo.Hide = false;
        }
    }
}
