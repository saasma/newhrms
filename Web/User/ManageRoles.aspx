<%@ Page Title="Manage Role" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="ManageRoles.aspx.cs" Inherits="Web.User.ManageRoles" %>

<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningMsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="InfoMsgCtl" TagPrefix="uc2" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .fieldHeader
        {
            width: 300px !important;
        }
    </style>
    <script type="text/javascript">

        function addRolePopup() {
            var ret = rolePopUp();

            if (typeof (ret) != 'undefined') {
                if (ret == 'Reload') {
                    __doPostBack('Reload', '');
                }
            }
            return false;
        }

        function editRolePopup(roleId) {
            var ret = rolePopUp('Id=' + roleId);

            if (typeof (ret) != 'undefined') {
                if (ret == 'Reload') {
                    __doPostBack('Reload', '');
                }
            }
            return false;
        }

        function reloadRole(childWindow) {
            childWindow.close();
            __doPostBack('Reload', '');
        }
    
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Roles
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="contentArea">
            <div style="text-align: left">
                <asp:Button ID="btnAdd" Width="130px" Style="margin-left: 0px; margin-bottom: 5px;"
                    CausesValidation="false" CssClass="btn btn-primary btn-sm btn-sect" runat="server"
                    Text="Add New Role" OnClientClick="return addRolePopup();" />
            </div>
            <uc2:InfoMsgCtl ID="msgInfo" EnableViewState="false" Hide="true" runat="server" />
            <uc2:WarningMsgCtl ID="msgWarning" EnableViewState="false" Hide="true" runat="server" />
            <cc2:EmptyDisplayGridView Width="100%" Style='clear: both; margin-bottom: 0px' PagerStyle-HorizontalAlign="Center"
                PagerStyle-CssClass="defaultPagingBar" CssClass="table table-primary mb30 table-bordered table-hover"
                UseAccessibleHeader="true" ShowHeaderWhenEmpty="True" ID="gvwRoles" runat="server"
                DataKeyNames="RoleId" AutoGenerateColumns="False" CellPadding="4" GridLines="None"
                ShowFooterWhenEmpty="False" AllowPaging="True" PageSize="10" OnPageIndexChanged="gvwRoles_PageIndexChanged"
                OnPageIndexChanging="gvwRoles_PageIndexChanging" OnRowDeleting="gvwRoles_RowDeleting">
                <Columns>
                    <asp:TemplateField HeaderText="Name" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="150px">
                        <ItemTemplate>
                            <%# Eval("Name") %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Permission" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                        HeaderStyle-Width="120px">
                        <ItemTemplate>
                            <%# GetPermission(Eval("IsReadOnly"))%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Is Fixed" Visible="false" ItemStyle-HorizontalAlign="Center"
                        HeaderStyle-Width="80px">
                        <ItemTemplate>
                            <asp:CheckBox runat="server" Enabled="false" Checked='<%# Eval("IsFixed") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Is Active" Visible="false" ItemStyle-HorizontalAlign="Center"
                        HeaderStyle-Width="80px">
                        <ItemTemplate>
                            <asp:CheckBox ID="CheckBox1" Enabled='false' runat="server" Checked='<%# Eval("IsActive") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="500px"></asp:TemplateField>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderStyle-Width="50px">
                        <ItemTemplate>
                            <%--<asp:ImageButton ID="ImageButton1" Visible='<%# !CanChange(Eval("IsFixed")) %>' runat="server"
                            CommandName="Select" ImageUrl="~/images/edit.gif" />--%>
                            <asp:ImageButton ID="ImageButton1" ImageUrl="~/images/edit.gif" runat="server" ToolTip="Edit"
                                Visible='<%# !CanChange(Eval("IsFixed")) %>' OnClientClick='<%# "return editRolePopup(" +  Eval("RoleId") + ");" %>'
                                AlternateText="Edit" />
                            &nbsp;
                            <asp:ImageButton ID="ImageButton2" OnClientClick="return confirm('Do you really want to delete Role?')"
                                Visible='<%# !CanChange(Eval("IsFixed")) %>' runat="server" CommandName="Delete"
                                ImageUrl="~/images/delet.png" />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                    </asp:TemplateField>
                </Columns>
                <PagerSettings Mode="NumericFirstLast" NextPageText="Next �" PreviousPageText="� Prev"
                    FirstPageText="First" LastPageText="Last" />
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                <PagerStyle CssClass="defaultPagingBar" />
                <SelectedRowStyle CssClass="selected" />
                <EmptyDataTemplate>
                    <b>No Roles defined. </b>
                </EmptyDataTemplate>
            </cc2:EmptyDisplayGridView>
            <div style='margin-top: 10px'>
            </div>
            <%-- <div class="buttonsDiv">
        
     <asp:Button ID="btnRole"  CssClass="addbtns" runat="server" Text="Add" />
        
        </div>--%>
        </div>
    </div>
</asp:Content>
