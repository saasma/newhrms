﻿<%@ Page Title="Role Information" Language="C#" MasterPageFile="~/Master/ForPopupPage.Master"
    AutoEventWireup="true" CodeBehind="ManageRoleDetails.aspx.cs" Inherits="Web.User.ManageRoleDetails" %>

<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningMsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="InfoMsgCtl" TagPrefix="uc2" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .titleDesign
        {
            font-size: 15px;
            color: #000000;
            text-decoration: underline;
            padding-top: 10px;
        }
        
        .items span
        {
            display: block;
            padding-top: 3px;
            font-weight: bold;
        }
        .paddinAll
        {
            padding: 10px;
        }
        table tr td
        {
            padding: 0 10px 8px 0;
        }
        h3
        {
            font: bold 13px Arial;
        }
        .marginTopss
        {
            margin-top: 10px;
            display: block;
        }
        .loanAdvTable
        {
            background-color: white;
            width: 60px;
            border: 1px solid white !important;
            padding: 0px;
        }
        .tableLightColor th
        {
            text-align: left;
        }
        strong
        {
            display: block;
            padding-bottom: 3px;
        }
        .tableLightColor input[type=text]
        {
            height: 20px;
        }
        .clsColor
        {
            background-color: #e8f1ff;
            height: 500px;
        }
    </style>
    <script type="text/javascript">

        function closePopup() {
            window.opener.reloadRole(window);
        }
    
    </script>
    <link href="../css/core.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">
    <div class="clsColor">
        <div class="popupHeader">
            <h3>
                Role Information</h3>
        </div>
        <uc2:InfoMsgCtl ID="msgInfo" EnableViewState="false" Hide="true" runat="server" />
        <uc2:WarningMsgCtl ID="msgWarning" EnableViewState="false" Hide="true" runat="server" />
        <asp:HiddenField ID="hdnRoleId" runat="server" />
        <div class=" marginal" style="margin: 5px 20px 0 10px;">
            <table style="width: 374px" class="fieldTable">
                <tr>
                    <td class="fieldHeader">
                        Role *
                    </td>
                    <td style='width: 400px'>
                        <asp:TextBox ID="txtName" runat="server" Width="200px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" SetFocusOnError="true" ValidationGroup="save"
                            ControlToValidate="txtName" Display="None" ErrorMessage="Name is required." runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="fieldHeader">
                        Desscription
                    </td>
                    <td>
                        <asp:TextBox ID="txtDescription" runat="server" Width="400px" Height="50px" TextMode="MultiLine"></asp:TextBox>
                    </td>
                </tr>
                <tr style='display: none'>
                    <td class="fieldHeader">
                        Is Active
                    </td>
                    <td>
                        <asp:CheckBox Checked="true" ID="chkIsActive" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="fieldHeader">
                        Permission
                    </td>
                    <td>
                        <%--<asp:CheckBox Checked="true" ID="chkIsReadOnly" runat="server" />--%>
                        <asp:RadioButtonList ID="rdbList" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem Text="View and Edit" Value="false" Selected="True" />
                            <asp:ListItem Text="View Only" Value="true" />
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td class="fieldHeader" valign="top">
                        Module Permission 
                        <div style="color:#FF983C;padding-top: 4px;">
               Note : for individual page permission do not select any module, use Role Permission
            </div>
                        
                    </td>
                    <td>
                        <div id='Div1' style="overflow-y: scroll; height: 150px; width: 450px; border: 1px solid #C1D6F3;
                            padding: 0px; margin-left: 0px; margin-top: 0px">
                            <asp:CheckBoxList ID="chkModuleList" Height="150px" RepeatColumns="2" RepeatDirection="Horizontal"
                                RepeatLayout="Table" runat="server" DataValueField="ModuleId" DataTextField="Name">
                            </asp:CheckBoxList>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="fieldHeader" valign="top" colspan="2">
                        <b>Branch Restriction</b>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:RadioButton AutoPostBack="true" ID="rdbNoRestriction" Checked="true" runat="server"
                            Text="No Restriction" GroupName="Restriction" OnCheckedChanged="rdbNoRestriction_CheckedChanged" />
                        <br />
                        <asp:RadioButton AutoPostBack="true" ID="rdbAllowOnly" runat="server" Text="Allow Only"
                            GroupName="Restriction" OnCheckedChanged="rdbNoRestriction_CheckedChanged" />
                        <div id='containerDIV' style="overflow-y: scroll; height: 320px; width: 800px; border: 1px solid #C1D6F3;
                            padding: 0px; margin-left: 20px; margin-top: 10px">
                            <asp:CheckBoxList ID="chkBranch" Width="750px" Height="100px" RepeatColumns="3" RepeatDirection="Horizontal"
                                RepeatLayout="Table" runat="server" DataValueField="BranchId" DataTextField="Name">
                            </asp:CheckBoxList>
                        </div>
                    </td>
                </tr>
                <tr style='padding-top: 15px'>
                    <td colspan="2">
                        <asp:Button ValidationGroup="save" OnClientClick="valGroup='save';return CheckValidation()"
                            CssClass="save" ID="btnInsertUpdate" runat="server" Text="Save" OnClick="btnInsertUpdate_Click" />
                        <asp:Button ID="Button1" CssClass="cancel" runat="server" Text="Cancel" OnClientClick="window.close();" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
