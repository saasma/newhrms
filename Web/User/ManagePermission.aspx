<%@ Page Title="Manage Permission" Language="C#" MasterPageFile="~/Master/Details.Master" AutoEventWireup="true"
    CodeBehind="ManagePermission.aspx.cs" Inherits="Web.User.ManagePermission" %>

<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningMsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="InfoMsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script language="javascript" type="text/javascript">
        //************************** Treeview Parent-Child check behaviour ****************************//  

        function OnTreeClick(evt) {
            var src = window.event != window.undefined ? window.event.srcElement : evt.target;
            var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");
            if (isChkBoxClick) {
                var parentTable = GetParentByTagName("table", src);
                var nxtSibling = parentTable.nextSibling;
                if (nxtSibling && nxtSibling.nodeType == 1)//check if nxt sibling is not null & is an element node
                {
                    if (nxtSibling.tagName.toLowerCase() == "div") //if node has children
                    {
                        //check or uncheck children at all levels
                        CheckUncheckChildren(parentTable.nextSibling, src.checked);
                    }
                }
                //check or uncheck parents at all levels
                CheckUncheckParents(src, true);
            }
        }

        function CheckUncheckChildren(childContainer, check) {
            var childChkBoxes = childContainer.getElementsByTagName("input");
            var childChkBoxCount = childChkBoxes.length;
            for (var i = 0; i < childChkBoxCount; i++) {
                childChkBoxes[i].checked = check;
            }
        }

        function CheckUncheckParents(srcChild, check) {
            var parentDiv = GetParentByTagName("div", srcChild);
            var parentNodeTable = parentDiv.previousSibling;

            if (parentNodeTable) {
                var checkUncheckSwitch;

                if (check) //checkbox checked
                {
                    var isAllSiblingsChecked = true; //AreAllSiblingsChecked(srcChild);
                    if (isAllSiblingsChecked)
                        checkUncheckSwitch = true;
                    else
                        return; //do not need to check parent if any child is not checked
                }
                else //checkbox unchecked
                {
                    checkUncheckSwitch = false;
                }

                var inpElemsInParentTable = parentNodeTable.getElementsByTagName("input");
                if (inpElemsInParentTable.length > 0) {
                    var parentNodeChkBox = inpElemsInParentTable[0];
                    parentNodeChkBox.checked = checkUncheckSwitch;
                    //do the same recursively
                    CheckUncheckParents(parentNodeChkBox, checkUncheckSwitch);
                }
            }
        }

        //   function AreAllSiblingsChecked(chkBox)
        //   {
        //     var parentDiv = GetParentByTagName("div", chkBox);
        //     var childCount = parentDiv.childNodes.length;
        //     for(var i=0; i<childCount; i++)
        //     {
        //        if(parentDiv.childNodes[i].nodeType == 1) //check if the child node is an element node
        //        {
        //            if(parentDiv.childNodes[i].tagName.toLowerCase() =="table")
        //            {
        //               var prevChkBox = parentDiv.childNodes[i].getElementsByTagName("input")[0];
        //              //if any of sibling nodes are not checked, return false
        //              if(!prevChkBox.checked) 
        //              {
        //                return false;
        //              } 
        //            }
        //        }
        //     }
        //     return true;
        //   }

        //utility function to get the container of an element by tagname
        function GetParentByTagName(parentTagName, childElementObj) {
            var parent = childElementObj.parentNode;
            while (parent.tagName.toLowerCase() != parentTagName.toLowerCase()) {
                parent = parent.parentNode;
            }
            return parent;
        }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <div class="contentArea">
        <h3>
            Manage Role Permission</h3>
        <div class="attribute">
            Role
            <asp:DropDownList Width="140px" ID="ddlRoles" runat="server" AppendDataBoundItems="true" AutoPostBack="true"
                DataTextField="Name" DataValueField="RoleId" OnSelectedIndexChanged="ddlRoles_SelectedIndexChanged">
                <asp:ListItem Text="--Select Role--" Value="-1"></asp:ListItem>
            </asp:DropDownList>
            <asp:RequiredFieldValidator ID="valReqdName" runat="server" ControlToValidate="ddlRoles"
                InitialValue="-1" Display="None" ErrorMessage="Please select a role." ValidationGroup="save"></asp:RequiredFieldValidator>
        </div>
        <div style='clear: both'>
        </div>
        <uc2:InfoMsgCtl ID="msgInfo" EnableViewState="false" Hide="true" runat="server" />
        <uc2:WarningMsgCtl ID="msgWarning" EnableViewState="false" Hide="true" runat="server" />
        <asp:TreeView ID="tvPages" runat="server" ShowCheckBoxes="All" ShowLines="True">
        </asp:TreeView>
        <div style='clear: both;' class="buttonsDiv">
            <asp:Button ValidationGroup="save" OnClientClick="valGroup='save';return CheckValidation()"
                CssClass="save" ID="btnInsertUpdate" runat="server" Text="Save" OnClick="btnInsertUpdate_Click" />
        </div>
    </div>
</asp:Content>
