﻿
String.prototype.replaceAll = function (stringToFind, stringToReplace) {
    var temp = this;
    var index = temp.indexOf(stringToFind);
    while (index != -1) {
        temp = temp.replace(stringToFind, stringToReplace);
        index = temp.indexOf(stringToFind);
    }
    return temp;
}


var hasTabControl = false;
var valGroup;
var valControl;
var isValidationSuccess = true;
// validate all controls of the current validation group &
//show message
function CheckValidation() {
  
    if (typeof (Page_Validators) == 'undefined')
        return true;
    
    for (var ii = 0; ii < Page_Validators.length; ii++) {
        var val = Page_Validators[ii];
        valControl = null;
        if (val.validationGroup == valGroup) {
            
            ValidatorValidate(val);

            isValidationSuccess = true;
            if (hasTabControl == true) {
                if (val.isvalid == false && $('#' + val.controltovalidate).prop('disabled') == false) {
                   
                    valControl = val.controltovalidate;
                    messageCallback(val.id, val.errormessage);
                    isValidationSuccess = false;
                    return false;
                }
            }
            else// if (hasTabControl == false)
            {
                //if no tab control then don't validate for the hidden controls/elements                
                if (val.isvalid == false)                      
                {
                    //alert(val.getAttribute('IsCalendar'));
                    if( ($('#' + val.controltovalidate).is("table") && $('#' + val.controltovalidate).is(":visible")  ) // for CheckboxList
                        ||
                        val.getAttribute('IsCalendar') != null //if calendar type
                        ||
                        (val.getAttribute("callback") != null && window[val.getAttribute("callback")]() == true && $('#' + val.controltovalidate).prop('disabled') == false)// validate for display:none case also, like for Festival income case
                        ||    
                     
                        // for other
                        ($('#' + val.controltovalidate).prop('disabled') == false && $('#' + val.controltovalidate).is(":visible"))

//                        ||
//                        // for Appraisal rating control
//                        $('#' + val.controltovalidate).prop('id').toString().indexOf('ratingValue')>=0
                          
                      ) 

                    
                    {

                        valControl = val.controltovalidate;

                        // for Appraisal rating control then focus in comment box if exists
//                        if ($('#' + val.controltovalidate).prop('id').toString().indexOf('ratingValue') >= 0) {
//                            var txtCommentID = val.id.replace("ratingValidation", "commentInput");
//                            var txtCommentCtl = document.getElementById(txtCommentID);
//                            if (txtCommentCtl != 'undefined' && txtCommentCtl != null) {
//                                valControl = txtCommentID;
//                            }
//                        }

                        messageCallback(val.id, val.errormessage);
                        isValidationSuccess = false;

                       

                        return false;
                    }
                }
            }            
            
        }
    }

    //for ok it is good, but for edit/delete event manullay calling is required
    if (typeof (clearUnload) != 'undefined') {
        //call to unload so that close is not called
        clearUnload();
    }
    return true;
}
//call back for the custom validation function
function messageCallback(valId, errormsg) {

    var isExtMsg = false;
    if (valControl != undefined) {
        try {            
            //For RateEditor.aspx tab having page
            if ( typeof(setTabFocus) != 'undefined') {
                setTabFocus(valId, valControl, errormsg);
            }
            else {
                if (errormsg != undefined) {
                    // if ext enabled in the page then show Warning in Ext Control
                    if (typeof (Ext) != 'undefined') {
                        isExtMsg = true;
                        Ext.Msg.show({
                            title: 'Validation failure!', //<- the dialog's title  
                            msg: errormsg, //<- the message  
                            buttons: Ext.Msg.OK, //<- YES and NO buttons  
                            icon: Ext.Msg.WARNING, // <- error icon  
                            minWidth: 300,
                            fn: callback //<- the function that is executed when the user clicks on any button  
                        });
                    }
                    else
                        alert(errormsg);
                }


                if (isExtMsg == false) {
                    //for ext control
                    if (document.getElementById(valControl).tagName == "TABLE") {
                        eval(valControl).focus();
                    }
                    else
                        document.getElementById(valControl).focus();
                }
            }
        }
        catch (ee) {           
            var i = 1;
        }
    }
}

function callback(parameters) {
    if (typeof (valControl) == 'undefined')
        return;

    Ext.defer(function () {
        //Ext.get(valControl).focus();
        if (typeof (valControl) == "string") {
            var element = Ext.getCmp(valControl);
            if (element != undefined)
                element.focus();
        }
        else {
            var element = eval(valControl);
            if (element != undefined)
                element.focus();
        }
    }, 100);
}

//To confirm the passed checkbox clicked with message
function checkboxUncheckedConfirm(chk,message) {
    if (!chk.checked) {
        if (!confirm(message)) {
            chk.checked = true;
        }
    }
}
function checkboxCheckedConfirm(chk, message) {
    if (chk.checked) {
        if (!confirm(message)) {
            chk.checked = false;
        }
    }
}

//
// getPageScroll()
// Returns array with x,y page scroll values.
// Core code from - quirksmode.org
//
function getPageScroll() {
    var yScroll;
    if (self.pageYOffset) {
        yScroll = self.pageYOffset;
    } else if (document.documentElement && document.documentElement.scrollTop) {	 // Explorer 6 Strict
        yScroll = document.documentElement.scrollTop;
    } else if (document.body) {// all other Explorers
        yScroll = document.body.scrollTop;
    }
    arrayPageScroll = new Array('', yScroll)
    return arrayPageScroll;
}

//
// getPageSize()
// Returns array with page width, height and window width, height
// Core code from - quirksmode.org
// Edit for Firefox by pHaez
//
function getPageSize() {

    var xScroll, yScroll;

    if (window.innerHeight && window.scrollMaxY) {
        xScroll = document.body.scrollWidth;
        yScroll = window.innerHeight + window.scrollMaxY;
    } else if (document.body.scrollHeight > document.body.offsetHeight) { // all but Explorer Mac
        xScroll = document.body.scrollWidth;
        yScroll = document.body.scrollHeight;
    } else { // Explorer Mac...would also work in Explorer 6 Strict, Mozilla and Safari
        xScroll = document.body.offsetWidth;
        yScroll = document.body.offsetHeight;
    }

    var windowWidth, windowHeight;
    if (self.innerHeight) {	// all except Explorer
        windowWidth = self.innerWidth;
        windowHeight = self.innerHeight;
    } else if (document.documentElement && document.documentElement.clientHeight) { // Explorer 6 Strict Mode
        windowWidth = document.documentElement.clientWidth;
        windowHeight = document.documentElement.clientHeight;
    } else if (document.body) { // other Explorers
        windowWidth = document.body.clientWidth;
        windowHeight = document.body.clientHeight;
    }

    // for small pages with total height less then height of the viewport
    if (yScroll < windowHeight) {
        pageHeight = windowHeight;
    } else {
        pageHeight = yScroll;
    }
    // for small pages with total width less then width of the viewport
    if (xScroll < windowWidth) {
        pageWidth = windowWidth;
    } else {
        pageWidth = xScroll;
    }

    arrayPageSize = new Array(pageWidth, pageHeight, windowWidth, windowHeight)
    return arrayPageSize;
}



//enable/disable the elements using CheckBox state, handles for div in firefox also
//chk = CheckBox reference, elementId = element or parent element which will be disabled, isReverse = 
// if CheckBox enabled then enabled others or reverse one
 function toggleElementUsingCheckBoxState(chk, elementId, isReverse, callBackFunc) {

  
    if (isReverse)
        isReverse = !chk.checked;
    else
        isReverse = chk.checked;


    if (elementId[0] != '#')
        elementId = '#' + elementId;

    if (isReverse) {

        enableElement(elementId);
    }

    else {
        disableElement(elementId);
    }

    if (typeof (callBackFunc) != 'undefined') {
        callBackFunc(chk);
    }
}

function enableElement(elementId) {
    if (elementId[0] != '#')
        elementId = '#' + elementId;
    //if ($.browser.msie)
    $(elementId).removeAttr('disabled');
    //else
    {
        $(elementId + " select").removeAttr('disabled');
        $(elementId + " input").removeAttr('disabled');
    }
}
function disableElement(elementId) {
    if (elementId[0] != '#')
        elementId = '#' + elementId;
    //if ($.browser.msie)
    $(elementId).attr('disabled', 'disabled');
    //else 
    {
        $(elementId + " select").attr('disabled', 'disabled');
        $(elementId + " input").attr('disabled', 'disabled');
    }
}

//toggle the element depending state
function toggleReverseElement(ctl, elementId) {
    if (ctl.checked == false)
        $("#" + elementId).removeAttr('disabled');
    else
        $("#" + elementId).attr('disabled', 'disabled');
}

function toggleElement(ctl, elementId) {
    if (ctl.checked == true)
        $("#" + elementId).removeAttr('disabled');
    else
        $("#" + elementId).attr('disabled', 'disabled');
}


//function enableElement(id)
//{
//    $("#" + id).removeAttr('disabled');
//}
//function disableElement(id)
//{
//    $("#" + id).attr('disabled', 'disabled');
//}

function makeReadonly(elementId) {
    $(elementId).attr('readonly', true);
    
    $(elementId).addClass('disabledTxt');
}

function removeReadonly(elementId) {
    $(elementId).removeAttr('readonly');
    $(elementId).removeClass('disabledTxt');
}
function changeForApproval(item, ctlToDisable) {
    if (item.value == "1") {
        enableElement(ctlToDisable + "_date");
    }
    else {
        disableElement(ctlToDisable + "_date");
    }
}


function pageLoad(event, args) {
    Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginRequest);
    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(endRequest);

    //in case of pages like RateEditor.aspx
//    if (typeof (pageLoadEvent) != 'undefined') {
//        pageLoadEvent(event, args);
//    }
}

function beginRequest(sender, args) {
    showLoading();
}

function endRequest(sender, args) {
    hideLoading();
}

function showLoading() {
    var objLoadingImage = document.getElementById('loadingPublicImage');

    if (objLoadingImage == null)
        return;

    var arrayPageSize = getPageSize();
    var arrayPageScroll = getPageScroll();

    var h = objLoadingImage.height;
    var w = objLoadingImage.width;

    var imgTop = arrayPageScroll[1] + ((arrayPageSize[3] - h) / 2);
    var imgLeft = ((arrayPageSize[0] - 20 - w) / 2);

   // imgLeft = imgLeft - 200;
   // imgTop = imgTop - 30;

    // read the page size, window size and the scrolled position

    // center loadingImage if the loading image is set
    if (objLoadingImage) {
        objLoadingImage.style.top = imgTop + 'px'; //(arrayPageScroll[1] + (Math.round((screen.height + savingImage.height) / 2)-222) + 'px');

        objLoadingImage.style.left = imgLeft + 'px';
        objLoadingImage.style.display = 'block';
    }
}

function hideLoading() {
    var objLoadingImage = document.getElementById('loadingPublicImage');
    if (objLoadingImage == null)
        return;
    objLoadingImage.style.display = 'none';
}


function getCalendarSelectedDate(calId) {
    return document.getElementById(calId + "_datey").value + "/" +
        document.getElementById(calId + "_datem").value + "/" +
        document.getElementById(calId + "_dated").value
}

function getCalendarSelectedDateAsArray(calId) {
    var array = new Array();
    
    array['Year'] = document.getElementById(calId + "_datey").value;
    array['Month'] = document.getElementById(calId + "_datem").value;
    array['Day'] = document.getElementById(calId + "_dated").value;
    return array;     
}

function setCalendarDate(calId, date) {
    var dates = date.split("/");
    $('#' + calId + "_datey").val(dates[0]);
    $('#' + calId + "_datem").val(dates[1]);
    $('#' + calId + "_dated").val(dates[2]);
}

//Create new list for the drop down from objArray preserving the selection if exists
function ChangeDropDownList(ddlId, objArray) {

    if ($.isArray(objArray) == false)
        return;

    var ddl = document.getElementById(ddlId);
    var selectedValue = ddl.value;
        
    ddl.options.length = 1;

    var length = objArray.length;
    var values;
    var htmlContent;
    for (var i = 0; i < length; i++) {
        values = objArray[i].split("$$");        
        if( values[0] == selectedValue)
            htmlContent = String.format("<option value='{0}' selected='true'>{1}</option>", values[0], values[1]);
        else
            htmlContent = String.format("<option value='{0}'>{1}</option>", values[0], values[1]);
        $('#' + ddlId).append(htmlContent);      
    }

}

function SelectDropDownList(ddlId, value) {
    
}

function leftPad(number, targetLength) {
    var output = number + '';
    while (output.length < targetLength) {
        output = '0' + output;
    }
    return output;
}

function isDateSame(date1, date2) {
    var d1, d2, m1, m2, y1, y2;
    var start = date1.split("/");
    var end = date2.split("/");



    d1 = parseInt(start[2]);
    d2 = parseInt(end[2]);

    m1 = parseInt(start[1]);
    m2 = parseInt(end[1]);

    y1 = parseInt(start[0]);
    y2 = parseInt(end[0]);

    if (d1 == d2 && m1 == m2 && y1 == y2)
        return true;

    return false;

}

function isSecondCalendarCtlDateGreaterSkipDay(calStartingDate, calEndingDate) {
    //var calStartingDate = '<%= calStartingDate.ClientID %>';
    //var calEndingDate = '<%= calEndingDate.ClientID %>';
    //alert(document.getElementById(calStartingDate + "_datey").disabled);



    var d1, d2, m1, m2, y1, y2;
    var start = calStartingDate.split("/");
    var end = calEndingDate.split("/");



    d1 = parseInt(start[2]);
    d2 = parseInt(end[2]);

    m1 = parseInt(start[1]);
    m2 = parseInt(end[1]);

    y1 = parseInt(start[0]);
    y2 = parseInt(end[0]);

    //check year
    if (y2 > y1) {
        return true;
    }
    else if (y2 < y1)
        return false;

    if (m2 > m1)
        return true;
    else if (m2 < m1)
        return false;



    return false;


}


function isSecondCalendarCtlDateGreater(calStartingDate,calEndingDate) {
    //var calStartingDate = '<%= calStartingDate.ClientID %>';
    //var calEndingDate = '<%= calEndingDate.ClientID %>';
    //alert(document.getElementById(calStartingDate + "_datey").disabled);


   
    var d1, d2, m1, m2, y1, y2;
    var start = calStartingDate.split("/");
    var end = calEndingDate.split("/");
   
    

    d1 = parseInt(start[2]);
    d2 = parseInt(end[2]);

    m1 = parseInt(start[1]);
    m2 = parseInt(end[1]);

    y1 = parseInt(start[0]);
    y2 = parseInt(end[0]);
    
    //check year
    if (y2 > y1) {
        return true;
    }
    else if(y2<y1)
        return false;

    if (m2 > m1)
        return true;
    else if (m2 < m1)
        return false;

    //if greater than
    if (d2 > d1) {

        //alert("Financial ending date must be greater than the starting date.");

        return true;
    }
    else
        return false;




}

//check if the second calendar date is greater than the first one, if not focus the second
//date dropdown
function isSecondCalendarGreater(calStarting, calEnding) {

    var date1 = getCalendarSelectedDate(calStarting);
    var date2 = getCalendarSelectedDate(calEnding);

    var d1, d2, m1, m2, y1, y2;
    var start = date1.split("/");
    var end = date2.split("/");

    d1 = parseInt(start[2]);
    d2 = parseInt(end[2]);
    m1 = parseInt(start[1]);
    m2 = parseInt(end[1]);
    y1 = parseInt(start[0]);
    y2 = parseInt(end[0]);

    //check year
    if (y2 > y1) {
        return true;
    }
    else if (y2 < y1) {
        try {
            if ($('#' + calEnding + "_datey").is(':visible'))
                document.getElementById(calEnding + "_datey").focus();
        }
        catch (e1) { }
        return false;
    }

    if (m2 > m1)
        return true;
    else if (m2 < m1) {
        try {
            if ($('#' + calEnding + "_datem").is(':visible'))
                document.getElementById(calEnding + "_datem").focus();
        }
        catch (e2) { }
        return false;
    }
    if (d2 > d1) {
        return true;
    }
    else {
        try {
            if ($('#' + calEnding + "_dated").is(':visible'))
                document.getElementById(calEnding + "_dated").focus();
        }
        catch (e1) { }
        return false;
    }

}

function stopPropagation(evt) {
    evt = (evt) ? evt : window.event;

    evt.cancelBubble = true;
    if (evt.stopPropagation) evt.stopPropagation();


    //if (typeof (e) == 'undefined') e = window.event;
    //    if (!e)  e = window.event;
    //    e.stopPropagation ? e.stopPropagation() : e.cancelBubble = true;
}

eedecreg = new RegExp("\\.", "g");
eethreg = new RegExp(",", "g");
function getNumber(str) {
    str = String(str).replace(eethreg, "");
    str = String(str).replace(eedecreg, ".");
    var res = parseFloat(str);
    if (isNaN(res))
        return 0;
    else
        return res;
}

//function to fill the drop down list after changing any other like drop down, must return KeyValue type
function onDropDownChangeCallback(targetDDLId, result,firstEmptyRequired) {
    hideLoading();

    var list = result;
    if (firstEmptyRequired)
        document.getElementById(targetDDLId).options.length = 1;
    else
        document.getElementById(targetDDLId).options.length = 0;
    for (i = 0; i < list.length; i++) {
        var menu = list[i];
        var htmlContent = String.format("<option value='{0}'>{1}</option>", menu.Key, menu.Value);
        $('#' + targetDDLId).append(htmlContent);
    }
}


///*****************grid arrow movement javascript**********************


function addArrowMovementToGrid(gridId,backGroundChangeOnlyForInput) {

    // here, we create a varialbe to hold a reference to our table.  this
    // will be used as a scope limiter in later jQuery selectors.
    var grid = $('#' + gridId);

    var changeForInputOnly = backGroundChangeOnlyForInput;

    if (grid.length <= 0)
        return;

    // now we calculate the largest row and column numbers. I used 0-based
    // row and column numbering, which is why I have to subtract 1.

    // to count the rows, I select the <tr> elements from within the grid.
    // jQuery selectors return an object that has numeric properties that make
    // it act like an array, so we can grab the length to get the count we need.
    var maxRow = $('tr', grid).length - 1;


    // here we take the first <tr> and count the cells to get the max column.
    var maxCol = $('tr', grid)[0].cells.length - 1;

    function clearSelectionClass(input) {
        if (!changeForInputOnly)
            input.parentNode.className = ''; //td
        input.className = ''; //input
    }

    //focus event
    $('input', grid).focus(function(event) {

        if (!changeForInputOnly)
            this.parentNode.className = 'arrowMovementGridInputFocus'; //td
        this.className = 'arrowMovementGridInputFocus'; //input

       
    });

    $('input', grid).blur(function(event) {
      
        if (!changeForInputOnly)
            this.parentNode.className = ''; //td
        this.className = ''; //input

    });

    // here we use jQuery to select all of the <input> elements within the grid
    // and attach an anonymous function to the keydown event.
    $('input', grid).keydown(function(event) {
        // get a reference to the element that fired the event
        var item = $(this);

        // read the data-row and data-col attributes of the current
        // element, so that we know where we are in the grid.
        var col = parseInt(item.attr('data-col'));
        var row = parseInt(item.attr('data-row'));

        // if a key other than one of the arros keys was pressed, we don't want
        // to do anything, so this is used below to determine whether or not
        // to take action.
        var doFocus = false;

        // the keyCodes for the arrow keys are 37, 38, 39, and 40
        if (event.ctrlKey && event.keyCode == "37") {
            // left - we want to move one column to the left, unless we are already at 0.
            if (col > 0) { col = col - 1; }
            doFocus = true;
        }
        else if (event.keyCode == "38") {
            // up - we want to move one row up, unless we are already at 0.
            if (row > 0) { row = row - 1; }
            doFocus = true;
        }
        else if (event.ctrlKey && event.keyCode == "39") {
            // right - we want to move one column to the right, unless we are already at the max.
            if (col < maxCol) { col = col + 1; }
            doFocus = true;
        }
        else if (event.keyCode == "40") {
            // down - we want to move one row down, unless we are already at the max.
            if (row < maxRow) { row = row + 1; }
            doFocus = true;
        }
        else if (event.ctrlKey && event.keyCode == "36") {
            // ctrl + home (to move to the first item)
            col = 0;
            doFocus = true;
        }
        else if (event.ctrlKey && event.keyCode == "35") {
            // ctrl + end (to move to the first item)

            col = maxCol;
            doFocus = true;
        }

        // if the key that was pressed was an arrow key, this will now be true.
        if (doFocus) {

            // here we are selecting the <input> elements from within the grid that have
            // data-row and data-col attributes that match our destination (there should only
            // be one such element), and setting focus there.
            clearSelectionClass(this);
            var ref = $('input[data-row="' + row + '"][data-col="' + col + '"]', grid);
            ref.focus();

            selectAllText(ref[0]);



        }
    });
}



function setMovementToGrid(grid,backgroundChangeRequired) {

    if (typeof (backgroundChangeRequired) == 'undefined') {
        backgroundChangeRequired = true;
    }

    // here, we create a varialbe to hold a reference to our table.  this
    // will be used as a scope limiter in later jQuery selectors.
    //var grid = $();

    // now we calculate the largest row and column numbers. I used 0-based
    // row and column numbering, which is why I have to subtract 1.


    if ($('tr', grid).length <= 0)
        return;

    // to count the rows, I select the <tr> elements from within the grid.
    // jQuery selectors return an object that has numeric properties that make
    // it act like an array, so we can grab the length to get the count we need.
    var maxRow = $('tr', grid).length - 1;


    // here we take the first <tr> and count the cells to get the max column.
    var maxCol = $('tr', grid)[0].cells.length - 1;

    //focus event
    $('input', grid).focus(function (event) {
        if (this.readOnly) return;
        if (backgroundChangeRequired) {
            this.parentNode.className = 'inputFocus';
            this.className = 'inputFocus';
        }



    });

    $('input', grid).blur(function (event) {
        if (this.readOnly) return;
        if (backgroundChangeRequired) {

            this.parentNode.className = '';
            this.className = '';
        }
    });
    
    // here we use jQuery to select all of the <input> elements within the grid
    // and attach an anonymous function to the keydown event.
    $('input', grid).keydown(function (event) {

        // get a reference to the element that fired the event
        var item = $(this);

        if (this.readOnly) return;


        // read the data-row and data-col attributes of the current
        // element, so that we know where we are in the grid.
        var col = parseInt(item.attr('data-col'));
        var row = parseInt(item.attr('data-row'));

        // if a key other than one of the arros keys was pressed, we don't want
        // to do anything, so this is used below to determine whether or not
        // to take action.
        var doFocus = false;

        // the keyCodes for the arrow keys are 37, 38, 39, and 40
        if (event.ctrlKey && event.keyCode == "37") {
            // left - we want to move one column to the left, unless we are already at 0.
            if (col > 0) { col = col - 1; }
            doFocus = true;
        }
        else if (event.keyCode == "38") {
            // up - we want to move one row up, unless we are already at 0.
            if (row > 0) { row = row - 1; }
            doFocus = true;
        }
        else if (event.ctrlKey && event.keyCode == "39") {
            // right - we want to move one column to the right, unless we are already at the max.
            if (col < maxCol) { col = col + 1; }
            doFocus = true;
        }
        else if (event.keyCode == "40") {
            // down - we want to move one row down, unless we are already at the max.
            if (row < maxRow) { row = row + 1; }
            doFocus = true;
        }
        else if (event.ctrlKey && event.keyCode == "36") {
            // ctrl + home (to move to the first item)
            col = 0;
            doFocus = true;
        }
        else if (event.ctrlKey && event.keyCode == "35") {
            // ctrl + end (to move to the first item)

            col = maxCol;
            doFocus = true;
        }

        // if the key that was pressed was an arrow key, this will now be true.
        if (doFocus) {

            // here we are selecting the <input> elements from within the grid that have
            // data-row and data-col attributes that match our destination (there should only
            // be one such element), and setting focus there.

            if (backgroundChangeRequired) {

                this.parentNode.className = '';
                this.className = '';

            }

            var ref = $('input[data-row="' + row + '"][data-col="' + col + '"]', grid);
            if (!ref.readOnly) {
                ref.focus();
                // document.getElementById($('input[data-row="' + row + '"][data-col="' + col + '"]', grid)[0].id).select();
                //this.select();
                //  $('input[data-row="' + row + '"][data-col="' + col + '"]', grid).select();

                selectAllText(ref[0]);
            }
        }
    });
}

//for calculation as giving problem in ff in adjustment column
function selectAllTextCalculation(textbox) {
    if (typeof (textbox) == 'undefined')
        return;

    if (typeof (textbox.getAttribute) != 'undefined') {
        if (textbox.getAttribute("readonly") != null || textbox.getAttribute("disabled") != null)
            return;
    }
    else if( textbox.length > 0 )
    {
        if (textbox[0].getAttribute("readonly") != null || textbox[0].getAttribute("disabled") != null)
            return;
    }
    //textbox.focus();
    textbox.select();


    //delay some sec so that the browser can finish its default events
//    setTimeout(function () {
//        textbox.select();
//    }, 2);

}

//for calculation as giving problem in ff in adjustment column
function selectAllText(textbox) {

    if (typeof (textbox.getAttribute) != 'undefined') {
        if (textbox.getAttribute("readonly") != null || textbox.getAttribute("disabled") != null)
            return;
    }
    else if (textbox.length > 0) {
        if (textbox[0].getAttribute("readonly") != null || textbox[0].getAttribute("disabled") != null)
            return;
    }
    //textbox.focus();
   // textbox.select();


    //delay some sec so that the browser can finish its default events
        setTimeout(function () {
            textbox.select();
        }, 1);

}

function isNonModalBrowser() {
    return $.browser.msie == false && ($.browser.chrome || $.browser.safari);
}
function onlyIntNumbers(evt) {
    var e = evt || event; // for trans-browser compatibility
    var charCode = e.which || e.keyCode;

    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true;

}
//disable for browser backspace
window.onkeydown = function (e) {
    var doPrevent;
    if (e.keyCode == 8) {
        var d = e.srcElement || e.target;
        if (d.tagName.toUpperCase() == 'INPUT' || d.tagName.toUpperCase() == 'TEXTAREA') {
            doPrevent = d.readOnly || d.disabled;
        }
        // for fckeditor like case
        else if (d.tagName.toUpperCase() == 'DIV')
            doPrevent = false;
        else
            doPrevent = true;
    }
    else
        doPrevent = false;
    if (doPrevent)
        e.preventDefault();
};


// Close the Msg div Block
function hideBlock(iconSpan) {
    iconSpan.parentNode.parentNode.innerHTML = "";
}

function getFormattedAmount(amount) {
    if (typeof (amount) == 'undefined' || amount == null)
        return 0;
    if (amount.toString().indexOf(",") >= 0)
        return amount;

    if (isNaN(amount))
        amount = 0;
    return Ext.util.Format.number(amount, '0,000.00');
}

if (typeof (Ext) != 'undefined') {
    Ext.onReady(function () {

        // Extend timeout for all Ext.Ajax.requests to 90 seconds.  
        // Ext.Ajax is a singleton, this statement will extend the timeout  
        // for all subsequent Ext.Ajax calls.  
        Ext.Ajax.timeout = 90000;
    });
}