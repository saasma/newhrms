﻿// currency(1,000) to number & number to currency converter functions

//eedecreg = new RegExp("\\.", "g");
//eethreg = new RegExp(",", "g");
//function getNumber(str) {
//    str = String(str).replace(eethreg, "");
//    str = String(str).replace(eedecreg, ".");
//    var res = parseFloat(str);
//    if (isNaN(res))
//        return 0;
//    else
//        return res;
//}

function showAdjTextbox(lbl) {
    //access hidden textbox
    if (lbl.nextSibling.style.display == 'none') {
        lbl.style.display = 'none';
        lbl.nextSibling.style.display = 'block';
    }
}

//function AllowOnlyNumbers(evt) {
//    var e = event || evt;
//    var charCode = e.which || e.keyCode;
//    if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 190)
//        return false;
//    return true;
//}

function showAdjLabel(txt) {
    //access hidden textbox
    if (txt.previousSibling.style.display == 'none') {
        txt.style.display = 'none';
        txt.previousSibling.style.display = 'block';

        if (txt.toString().trim() != "") {
            txt.previousSibling.innerHTML = txt.value;          
        
        }
    }
}

function selectDeselectAll(chk) {
    $('input[type=checkbox]').each(
        function(index) {
            if ($(this).prop('disabled') == false && this.id.indexOf('chkDelete') > 0)
                this.checked = chk.checked;
        }
    );
}

// handling adjustment textbox, Esc key
function adjustmentText(evt,txt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode == 13 || charCode == 27) {
        showAdjLabel(txt);

        // handle event to prevent saving
        stopPropagation(evt);
        return false;
        //evt.cancelBubble = true;
        //if (evt.stopPropagation) evt.stopPropagation();

    }

    return true;
}

function IsNumeric(input) {
    return (input - 0) == input && input.length > 0;
}

// checks if new amount is valid, if not sets old amount
//called for variable amount or adjustment changed
function isValidAmount(txt, newAmount, oldAmount, event) {


    if (newAmount == oldAmount)
        return;



    showLoading();    
    
    disableElement(buttonsDiv);


    if (newAmount.toString().charAt(0) != "-" && IsNumeric(getNumber(newAmount).toString())) {

        txt.value = getMoney(getNumber(newAmount));
        txt.setAttribute("amount", txt.value);

        //set in hidden field new changed value with type & make postback for task calculation,
        //if changed for variable income/deduction amount or income adjustment        
        if (txt.getAttribute("taxable") != null) {
            //set as EmployeeId:TypeId:SourceId:Amount
            var str = txt.getAttribute("empId") + ":" + txt.getAttribute("columnType") + ":" +
                txt.getAttribute("sourceId") + ":" + txt.value;

            
            
            Web.PayrollService.GetUpdateAmountForTax(str, payrollId, isRetResignedOnly,GetUpdateAmountForTax_Callback, null, txt.getAttribute("empId") + ":" + txt.id);
        }
        else {
            updateRowsAmounts(txt);
        }


        //
    }
    else {
        //invalid amount so restore old amount
        txt.value = oldAmount;
        hideLoading();
        enableElement(buttonsDiv);
    }

    

    //stopPropagation();
}

//update textbox value after server ajax request due to amount changed, mainly changed needed for tax recalc
function GetUpdateAmountForTax_Callback(result,empid) {

    var employeeid = empid.split(":")[0];
    var txtId =      empid.split(":")[1];

    var keyValues = result.split(";");

    for (var i = 0; i < keyValues.length; i++) {
        var values = keyValues[i].split(":");

        var columnType = values[0];
        var sourceId = values[1];
        var amount = values[2];

        var finding = String.format('input[type=text][empid={0}][columntype={1}][sourceid={2}]',
            employeeid, columnType, sourceId);
        //alert($(finding).length);
        $(finding).val(getMoney(amount));
        
                
    }
    
    updateRowsAmounts(document.getElementById(txtId));
    
}




eeisus=1,eetrue="TRUE",eefalse="FALSE",eedec=".",eeth=",",
eecurrencyreg=new RegExp("[$]","g"),
eepercentreg=new RegExp("%","g")



function myIsNaN(x) {
    return isNaN(x) || typeof x == "number" && !isFinite(x)
    }
function round(n, nd) {
    if (isFinite(n) && isFinite(nd)) {
        var sign_n = n < 0 ? -1: 1,
        abs_n = Math.abs(n),
        factor = Math.pow(10, nd);
        return sign_n * Math.round(abs_n * factor) / factor
    } else
        return NaN
}
function eeinsertThousand(whole) {
    if (whole == "" || whole.indexOf("e") >= 0)
        return whole;
    else {
        var minus_sign = "";
        if (whole.charAt(0) == "-") {
            minus_sign = "-";
            whole = whole.substring(1)
            }
        for (var res = "", str_length = whole.length - 1, ii = 0; ii <= str_length; ii++) {
            if (ii > 0 && ii % 3 == 0)
                res = eeth + res;
            res = whole.charAt(str_length - ii) + res
        }
        return minus_sign + res
    }
}
function getMoney(x) {
    // decimalPlaces is set in Calculation.ascx.cs
    var nd = decimalPlaces;
    if (myIsNaN(x))
        return Number.NaN;
    else {
        var res = round(x, nd);
        if (nd > 0) {
            var str = String(res);
            if (str.indexOf("e") != -1)
                return str;
            if (str.indexOf("E") != -1)
                return str;
            var parts = str.split("."),
            res2 = eeinsertThousand(parts[0].toString());
            if (parts.length < 2) {
                var decimals = "00000000000000".substring(0, nd);
                return res2 + eedec + decimals
            } else {
                var decimals = (parts[1].toString() + "00000000000000").substring(0, nd);
                return res2 + eedec + decimals
            }
        } else
            return eeinsertThousand(res.toString())
        }
}

// update total income, deduction & net salary when column value changes
function updateRowsAmounts(txt) {
    var totalIncome = 0;
    var totalDeduction = 0;

    //ctl00_mainContent_ctl00_gvw_ctl02_txt - 1 - 1
    var pos = txt.id.indexOf("txt");
    var id = txt.id.substring(0,pos);

    //array of income elements & deduction elements
    var incomeList = document.getElementById(id + "incomeListId").value.split(",");
    var deductionList = document.getElementById(id + "deductionListId").value.split(",");

    for (var i = 0; i < incomeList.length; i++) {
        var txtElement = document.getElementById(incomeList[i]);
        if (txtElement.value.trim() != "-") {
            totalIncome += getNumber(txtElement.value);
        }
    }

    for (var i = 0; i < deductionList.length; i++) {
        var txtElement = document.getElementById(deductionList[i]);
        if (txtElement.value.trim() != "-") {
            totalDeduction += getNumber(txtElement.value);
        }
    }


    document.getElementById(id + txtIncomeSumTxt).value = getMoney(totalIncome);
    document.getElementById(id + txtDeductionSumTxt).value = getMoney(totalDeduction);
    var netSalary = (totalIncome - totalDeduction);
    // for -ve value
    if (netSalary < 1) {
        disableElement('ctl00_mainContent_ctl00_pnl');
        $('#ctl00_mainContent_ctl00_divErrorMsg').show();
        document.getElementById(id + txtNetTxt).style.borderColor = 'red';
    }
    else {
        enableElement('ctl00_mainContent_ctl00_pnl');
        $('#ctl00_mainContent_ctl00_divErrorMsg').hide();
        document.getElementById(id + txtNetTxt).style.borderColor = ''; 
    }
    document.getElementById(id + txtNetTxt).value = getMoney(netSalary);

    hideLoading();
    enableElement(buttonsDiv);
}


// chk - Checkbox, className - name of the class .adjustmentIncome or .adjustmentDeduction
function adjustmentCheckbox(chk, className,event) {

    //for incomeonly
//    if (className == ".adjustmentIncome")
//        isIncomeAdjustmentChecked = chk.checked;

    if (chk.checked) {
        $(className).removeAttr("readonly");
        $(className).removeClass("calculationInputDisabled");
    }
    else {

        var adjustmentTextboxes = $(className);

        //prevent un-checking if not zero
        for (var i = 0; i < adjustmentTextboxes.length; i++) {          
            
            if (getNumber(adjustmentTextboxes[i].value) != 0) {
                chk.checked = true;
                alert(msgCannotUnchecked);
                return;
            }
            //updateRowsAmounts(adjustmentTextboxes[i]);
        }
        
        
        // ask if value are also removed msg
//        if (!confirm("All values for this column will be reset(0)?")) {
//            chk.checked = true;
//            return;
//        }

        

        adjustmentTextboxes.attr('readOnly', 'readOnly')        
        // clear amount from this column
        adjustmentTextboxes.attr('value', getMoney(0));
        adjustmentTextboxes.attr('amount', '0');
        adjustmentTextboxes.addClass("calculationInputDisabled");

        for (var i = 0; i < adjustmentTextboxes.length; i++) {

            //isValidAmount(adjustmentTextboxes[i], 0, 1, event);
            //updateRowsAmounts(adjustmentTextboxes[i]);
        }
        // call to recalculate for each rows
    }
}



// for calculation amount textboxes movement using array keys...
function processKey(evt, txt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;

    var valid = false;

    //suppress for enter key
    if (charCode == 13) {
        stopPropagation(evt);
        return false;
    }

    if (charCode == 190 //.
        || charCode == 38  //up
        || charCode == 40  //down
        || charCode == 37
        || charCode == 39
        || charCode == 18)
        valid = true;

    

    if (!valid) {
        if ((charCode > 31 && (charCode < 48 || charCode > 57)))
            return false;
    }
    
    if (charCode == 38) {
        var upTextBox = txt.getAttribute("upText");
        //alert('Up key press');
        if (upTextBox != null) {

            document.getElementById(upTextBox).focus();
        }
    }
    else if (charCode == 40) {
        var downTextBox = txt.getAttribute("downText");
        //alert('Down key press');
        if (downTextBox != null) {
            document.getElementById(downTextBox).focus();
        }
    }
    //left arrow
    else if (evt.ctrlKey && charCode == 37) {
        var leftTextBox = txt.getAttribute("leftText");
        //alert('Down key press');
        if (leftTextBox != null) {
            document.getElementById(leftTextBox).focus();
        }
    }
    //right arrow
    else if (evt.ctrlKey && charCode == 39) {
        var rightTextBox = txt.getAttribute("rightText");

        if (rightTextBox != null) {
            document.getElementById(rightTextBox).focus();
        }
    }

    return true;
}



function addComment(textbox) {

    //var textbox = imgId.previousSibling.previousSibling;

    if (textbox.readOnly)
        return;

    
    var ele = textbox.nextSibling;

    var value = "";

    if (ele.value != null)
        value = ele.value;

    value = commentPopup("value=" + value);


    //value = prompt("Enter comments", value);

    if ( value != null)
        ele.value = value;

    textbox.title = ele.value;
}


