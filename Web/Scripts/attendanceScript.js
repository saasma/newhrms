﻿
// event handler on cell onfocus
function displayOption(txt, event) {
    //If readonly then simply return
    if (txt.readOnly)
        return;

    if (ddlOptionReference == null) {
        ddlOptionReference = $(ddlOption);
        ddlOptionDivReference = $(ddlOptionDiv);

    }
    if (empDetailsDiv == null) {
        empDetailsDiv = $('#empDetailsDiv');
        empDetailsDivOnHover = $('#empDetailsDivOnHover');
    }


    //for first time option creation,
    if (isOptionCreated == false) {

        ddlOptionReference.append("<option value=' '></option>");
        ddlOptionReference.append(String.format("<option value='{0}' disabled='disabled'>{1}</option>", 'Present', 'Present'));
        ddlOptionReference.append(String.format("<option value='{0}'>{1}</option>", defaultLeaves[0][0], defaultLeaves[0][0]));

        ddlOptionReference.append(String.format("<option value='{0}' disabled='disabled'>{1}</option>", 'Absent', 'Absent'));

        if (hasHourlyLeave) {
            addHourlyLeaveOptions("0:0", defaultLeaves[3][0], hoursInADay, hoursInADay, ddlOptionReference, "")
            addHourlyLeaveOptions("0:0", defaultLeaves[1][0], 1, hoursInADay, ddlOptionReference, "")
        }
        else {
            ddlOptionReference.append(String.format("<option value='{0}'>{1}</option>", defaultLeaves[3][0], defaultLeaves[3][0]));
            ddlOptionReference.append(String.format("<option value='{0}'>{1}</option>", defaultLeaves[1][0], defaultLeaves[1][0]));
            ddlOptionReference.append(String.format("<option value='{0}'>{1}</option>", defaultLeaves[2][0], defaultLeaves[2][0]));
        }

    }
    else {
        //make empty as default selection      

        $(ddlOptionReference).val(' ');
    }
    isOptionCreated = true;

    currentTxt = txt;
    currentType = document.getElementById(currentTxt.id.replace("txt", "hfType"));
    //currentTypeValue = document.getElementById(currentTxt.id.replace("txt", "hfTypeValue"));
    //currentDeductDays = document.getElementById(currentTxt.id.replace("txt", "hfDeductDays"));

    var txtId = '#' + txt.id;
    var txtIdReference = $(txtId);
    //get the position of the placeholder element
    var pos = txtIdReference.offset();
    var fleft = (pos.left-60) + 'px';
    var ftop = pos.top - 145 + txtIdReference.outerHeight() + 'px';
    var leftForDiv = (pos.left - 240) + 'px';
    var rowId = getRowId(txtId);
    var employeeDetails = $('#' + rowId + "lblId").html() + " : " + $('#' + rowId + "Label2").html();
    empDetailsDiv.html(employeeDetails);

    filterLeaveOptions(txtId);

    //show the menu directly over the placeholder
    ddlOptionReference.css({
        position: 'absolute',
        zIndex: 5000,
        left: fleft,
        top: ftop
    });
    ddlOptionReference.show();

    empDetailsDiv.css({
        position: 'absolute',
        zIndex: 5000,
        left: leftForDiv,
        top: ftop
    });
    empDetailsDiv.show();

    dropDownInitialise = true;
}

//return row id removing the id of txt & #
function getRowId(id) {
    var rowId = id.substring(0, id.indexOf("txt"));
    return rowId.replace("#", "");
}

function checkIfSavingValid() {
    return true;

    // as now Workday based leave will not be used, if used also balance will be generated from server side

    //ask & implement the logic to
//    var invalidState = false;

//    //check each workday leave of each emp., first validates then set in hfWorkDayLeaves hidden field to be saved
//    $(".lblEmployeeId").each(
//        function (index) {
//            var rowId = this.id.substring(0, this.id.indexOf("lblId")).replace("#", "");

//            var employeeId = document.getElementById(rowId + "hfEmployeeId").value;

//            var list = document.getElementById(rowId + "hfLeaveList").value.split(";");

//            var eachWorkdayLeave = "";
//            for (var i = 0; i < list.length; i++) {
//                if (list[i] != "") {
//                    var leaveDetails = list[i].split(":"); ///1=AttendenceCellType.Leave : LeaveTypeId, Abbreviation
//                    var leaveTypeId = leaveDetails[0];
//                    var leaveAbbr = leaveDetails[1];
//                    var empKey = employeeId + ":" + leaveTypeId;
//                    //balance
//                    var balance = parseFloat(leaveBalance[empKey]);
//                    var workDayDaysCount = parseFloat(leaveWorkDayDaysCount[empKey]);
//                    if (isNaN(workDayDaysCount))
//                        workDayDaysCount = 0;
//                    var isLeaveActive = new Boolean(leaveEmployeeIsActive[empKey]);

//                    //if work days type of leave then calculate accured using cell values
//                    if (leaves[leaveAbbr]['IsWorkDaysType'] == true) {
//                        //balance
//                        var currentAccured = 0;

//                        if (isLeaveActive) {
//                            currentAccured = GetWorkDaysAccured(leaves[leaveAbbr]['WorkDaysPeriod'],
//                                    leaves[leaveAbbr]['LeavePeriod'], leaves[leaveAbbr]['WorkDaysIncludeLeaves'], rowId, workDayDaysCount);
//                        }

//                        var totalBalance = balance + currentAccured;
//                        workDayDaysCount = workDayLeaveDaysCount;
//                        var leaveAssigned = getTotalOfLeaveSetToEmployee(employeeId, leaveTypeId, leaveAbbr, rowId);

//                        if (leaveAssigned > totalBalance) {
//                            alert(String.format(WorkDayResourceMsg, employeeId, leaveAbbr, totalBalance, (leaveAssigned - totalBalance)));
//                            invalidState = true;
//                            return false;
//                        }
//                        //valid
//                        else {
//                            //append each workday leave type for the employee, workDayDaysCount : (balance-taken) : accured
//                            eachWorkdayLeave += ";" + leaveAbbr + ":" + workDayDaysCount + ":" + (totalBalance - leaveAssigned) + ":" + currentAccured;
//                        }
//                    }
//                }
//            }
//            document.getElementById(rowId + "hfWorkDayLeaves").value = eachWorkdayLeave;
//        }
//    );
//    return !(invalidState);
}

function removeComboItems(startIndex) {
    addDefaultItems();
    return;
}

//method to filter leave options for the Employee
function filterLeaveOptions(txtId) {

    var rowId = getRowId(txtId);
    var id = rowId + "hfLeaveList";
    var employeeId = document.getElementById(rowId + "hfEmployeeId").value;

    //split each leaves
    var list = document.getElementById(id).value.split(";");
    //clear cell options placing only space & "P"

    var deleteItems = (hasHourlyLeave) ? (hasHalfHourlyLeave ? (hoursInADay * 2 + 1) : (hoursInADay + 1)) : 3;  //9 : 3; //if has hourly leave or else dont have then 3 only count up UPL-8(for hourly ) or UPL/2(for half day leave)
    document.getElementById(ddlOption.replace("#", "")).options.length = defaultLeaves.length + deleteItems;

    //add holiday options for the current day & employee
    var day = getDayFromTxtId(txtId);
    var holidayList = getHolidayListForCell(txtId, day, employeeId);

    //add leave options
    ddlOptionReference.append("<option value='Leave' disabled='disabled'>Leave</option>");
    var listLength = list.length;
    for (var i = 0; i < listLength; i++) {
        if (list[i] != "") {
            var leaveDetails = list[i].split(":"); ///1=Attende nceCellType.Leave : LeaveTypeId, Abbreviation
            var leaveTypeId = leaveDetails[0];
            var leaveAbbrevation = leaveDetails[1];
            var isHourlyLeave = (hasHourlyLeave && leaves[leaveAbbrevation]['IsHalfDayAllowed'] == true);
            var isCompensatoryLeave = leaves[leaveAbbrevation]['IsCompensatory'];
            var isHolidayOnThisDay = holidayList.length > 0;
            var allowedLeave = isLeaveValidForEmployee(employeeId, leaveTypeId, leaveAbbrevation, rowId);
            var titleOrToolTip = leaves[leaveAbbrevation].FullName + " : " + globalLeaveBalance;

            //if Compensatory leave then add half hour/day & full day leave options
            if (isCompensatoryLeave) {
                var postFix = isHolidayOnThisDay == true ? '++' : '--';
                if (isHourlyLeave) {

                    ddlOptionReference.append(String.format("<option value='{0}'>{1}</option>", "1:" + leaveTypeId, leaveAbbrevation + '-' + (hoursInADay / 2) + postFix));
                    ddlOptionReference.append(String.format("<option value='{0}'>{1}</option>", "1:" + leaveTypeId, leaveAbbrevation + '-' + (hoursInADay) + postFix));
                }
                else {
                    ddlOptionReference.append(String.format("<option value='{0}'>{1}</option>", "1:" + leaveTypeId, leaveAbbrevation + postFix));
                    if (leaves[leaveAbbrevation]['IsHalfDayAllowed'] == true)
                        ddlOptionReference.append(String.format("<option value='{0}'>{1}</option>", "1:" + leaveTypeId, leaveAbbrevation + '/2' + postFix));
                }
            }
            else {
                if (!isHourlyLeave && allowedLeave == "All") {
                    //add LeaveAbbr-HoursInADay, if the company has hourly leave
                    if (hasHourlyLeave) {
                        addHourlyLeaveOptions("1:" + leaveTypeId, leaveAbbrevation, hoursInADay, hoursInADay, ddlOptionReference, titleOrToolTip)
                    }
                    else {
                        var htmlContent = String.format("<option title='{2}' value='{0}'>{1}</option>", "1:" + leaveTypeId, leaveAbbrevation, titleOrToolTip);
                        ddlOptionReference.append(htmlContent);
                    }
                }
                //if allowed only half leave & leave is half allowing type
                if ((allowedLeave == "HalfOnly" || allowedLeave == "All") && leaveDetails[2] == "1") {
                    if (hasHourlyLeave)
                        addHourlyLeaveOptions("1:" + leaveTypeId, leaveAbbrevation, 1, hoursInADay, ddlOptionReference, titleOrToolTip);

                    else {
                        var htmlContent = String.format("<option title='{2}'  value='{0}'>{1}</option>", "1:" + leaveTypeId, leaveAbbrevation + "/2", titleOrToolTip);
                        ddlOptionReference.append(htmlContent);
                    }
                }
                //only restrictive hourly leave available, check if number retured from the function
                else if (isHourlyLeave && allowedLeave != "" && !isNaN(allowedLeave)) {
                    allowedLeave = parseFloat(allowedLeave);
                    addHourlyLeaveOptions("1:" + leaveTypeId, leaveAbbrevation, 1, allowedLeave, ddlOptionReference, titleOrToolTip);
                }
            }
        }
    }

    var valueStr = "";
    ddlOptionReference.append("<option value='Holiday' disabled='disabled'>Holiday</option>");
    var abbr = "";
    for (var i = 0; i < holidayList.length; i++) {
        var holiday = holidayList[i];
        if (holiday.IsWeekly) {
            if (holiday.Type == 0) // half day weekly holiday
            {
                // if abbreviation has already /2 then do nothing
                if (holiday.Abbreviation.indexOf("/2") >= 0)
                    abbr = holiday.Abbreviation;
                else
                    abbr = holiday.Abbreviation + "/2";
            }
            else
                abbr = holiday.Abbreviation;
            valueStr = "2:";
        }
        else {
            abbr = holiday.Abbreviation;
            valueStr = "3:";
        }
        valueStr += holiday.Id;

        var htmlContent = String.format("<option value='{0}'>{1}</option>", valueStr, abbr);
        ddlOptionReference.append(htmlContent);
    }
}

function addHourlyLeaveOptions(value, abbreviation, startingHour, validHours, ddlOptionReference, titleOrToolTip) {

    if (hasHourlyLeave) {
        var htmlContent = [];

        if (hasHalfHourlyLeave) {
            if (startingHour == 1)
                var i = startingHour - 0.5;
            else
                var i = startingHour;
            for (; i <= hoursInADay && i <= validHours; i += 0.5) {
                htmlContent[htmlContent.length] = String.format("<option title='{2}'  value='{0}'>{1}</option>", value, abbreviation + "-" + i, titleOrToolTip);
            }
        }
        else {
            for (var i = startingHour; i <= hoursInADay && i <= validHours; i++) {
                htmlContent[htmlContent.length] = String.format("<option title='{2}'  value='{0}'>{1}</option>", value, abbreviation + "-" + i, titleOrToolTip);
            }
        }
        ddlOptionReference.append(htmlContent.join(''));
    }
}

//get accured for work days leave type
var workDayLeaveDaysCount = 0.0;
//counts the accured for work day leave type using current cell values
//function GetWorkDaysAccured(workDaysPeriod, leavePeriod, workDaysIncludeOtherLeaves, rowId, workDayDaysCount) {
//    var leavesToInclude = workDaysIncludeOtherLeaves.split(",");
//    var accured = 0.0;

//    workDayLeaveDaysCount = workDayDaysCount;
//    for (var i = 1; i <= totalDays; i++) {
//        var cellValue = $('#' + rowId + "txt" + i).val();
//        var cellValueType = parseInt($('#' + rowId + "hfType" + i).val());

//        var hasHalfLeave = false;
//        //has half leave in cell then remove half portion & then compare
//        if (cellValue.indexOf("/2") >= 0) {
//            hasHalfLeave = true;
//            cellValue = cellValue.replace("/2", "");
//        }

//        //if weekly or holiday then count as 1
//        if (cellValueType == 2 || cellValueType == 3) {
//            workDayLeaveDaysCount += 1;
//        }
//        else if (cellValue == "P")
//            workDayLeaveDaysCount += 1;

//        //check for "Work day includes" leave type
//        for (var j = 0; j < leavesToInclude.length; j++) {
//            if (leavesToInclude[j] != "" && cellValue == leavesToInclude[j]) {
//                if (hasHalfLeave)
//                    workDayLeaveDaysCount += 0.5;
//                else
//                    workDayLeaveDaysCount += 1;
//            }
//        }

//        // if count equals to work days period then leave per period is activated
//        if (workDayLeaveDaysCount >= workDaysPeriod) {

//            workDayLeaveDaysCount = 0.0;
//            accured += leavePeriod;
//        }
//    }
//    return accured;
//}

//check if leave valid for employee by the formula (Balance - Current selected)
//return parameters, All - all leave possible, HalfOnly - half only possible, "" -  no leave possible & number - this much hourly possible
var globalLeaveBalance;
function isLeaveValidForEmployee(employeeId, leaveTypeId, leaveAbbr, rowId) {
    var empKey = employeeId + ":" + leaveTypeId;
    var balance = parseFloat(leaveBalance[empKey]);
    globalLeaveBalance = balance;
    var workDayDaysCount = parseFloat(leaveWorkDayDaysCount[empKey]);
    var isActive = new Boolean(leaveEmployeeIsActive[empKey]);

    var isHalfDayAllowed = leaves[leaveAbbr]['IsHalfDayAllowed'];
    var unusedLeave = leaves[leaveAbbr]['UnusedLeave'];
    var isHourlyLeave = isHalfDayAllowed && hasHourlyLeave;

    //if compensotory leave then valid in all cases
    if (leaves[leaveAbbr]['IsCompensatory'] == true) {
        return 'All';
    }

    // Not used
    //if work days type of leave then calculate accured using cell values
//    if (leaves[leaveAbbr]['IsWorkDaysType'] == true) {
//        var dynamicAccuredDays = 0;
//        //if active then only accured 
//        if (isActive) {
//            dynamicAccuredDays = GetWorkDaysAccured(leaves[leaveAbbr]['WorkDaysPeriod'],
//                leaves[leaveAbbr]['LeavePeriod'], leaves[leaveAbbr]['WorkDaysIncludeLeaves'], rowId, workDayDaysCount);
//        }
//        var leaveAssigned = getTotalOfLeaveSetToEmployee(employeeId, leaveTypeId, leaveAbbr, rowId);
//        balance = balance + dynamicAccuredDays;
//    }


    var count = getTotalOfLeaveSetToEmployee(employeeId, leaveTypeId, leaveAbbr, rowId);
    var difference = balance - count;
    if (isHourlyLeave) {

        //if encash & negative leave type & applicable due to status
        if (unusedLeave == 'Encase' && isEncashLeaveTypeBeNegativeBalance == true && isActive == true) {
            return hoursInADay;
        }
        else if (unusedLeave == 'LapseEncase' && isEncashLapseLeaveTypeBeNegativeBalance == true && isActive == true) {
            return hoursInADay;
        }
        else if (unusedLeave == 'Lapse' && isLapseLeaveBeNegativeBalance == true && isActive == true) {
            return hoursInADay;
        }

        if (difference >= oneHourLeave) {
            return difference;
        }
    }
    else {
        // if negative allow setting place then allow for any balance
        if (leaves[leaveAbbr]['AllowNegativeBalance'] == 'true') {
            return 'All';
        }

        //if encash & negative leave type
        if (unusedLeave == 'Encase' && isEncashLeaveTypeBeNegativeBalance == true && isActive == true) {
            return "All";
        }
        else if (unusedLeave == 'LapseEncase' && isEncashLapseLeaveTypeBeNegativeBalance == true && isActive == true) {
            return "All";
        }
        else if (unusedLeave == 'Lapse' && isLapseLeaveBeNegativeBalance == true && isActive == true) {
            return "All";
        }

        if (difference >= 1.0)
            return "All";
        else {
            // for half day leave
            if (!isHourlyLeave) {
                if (difference >= 0.5)
                    return "HalfOnly"; //only half day
            }
            else { //for hourly leave enabled case as for this no half day

            }
        }
    }
    //none
    return "";
}

//get the total days of leave assigned to the employee
function getTotalOfLeaveSetToEmployee(employeeId, leaveTypeId, leaveAbbr, rowId) {
    var hasHalfLeave = false;
    var hasHourlyLeave = false;
    var hourValue;
    var cellValue;
    var cellId = getInitialId(rowId);
    //count current occurence of the leave
    var count = 0.0;

    for (var i = 1; i <= totalDays; i++) {
        cellValue = $('#' + rowId + "txt" + i).val();
        hasHalfLeave = false;
        hasHourlyLeave = false;
        hourValue = 0.0;

        //has half leave in cell then remove half portion & then compare
        if (cellValue.indexOf("/2") >= 0) {
            hasHalfLeave = true;
            cellValue = cellValue.replace("/2", "");
        }
        //has hourly leave
        else if (cellValue.indexOf("-") >= 0) {
            hasHourlyLeave = true;
            var strs = cellValue.split("-");
            //retrieve abbr value only

            cellValue = strs[0];
            hourValue = parseFloat(strs[1]);
        }

        if (cellValue == leaveAbbr) {
            if (hasHalfLeave)
                count += 0.5;
            else if (hasHourlyLeave)
                count += (hourValue); //(hourValue * oneHourLeave);
            else
                count += 1;
        }
    }

    return count;
}


//check if FH valid for this employee
function isValidForFH(txtId, day) {

    var hfIsFemale = getInitialId(txtId).replace("txt", "hfIsFemale");
    var isFemale = $(hfIsFemale).val() == "1";
    if (isFemale)
        return true;
    else
        return false;
}

//Checck if the Caste holiday can be applied to the Employee
function isValidForCH(txtId, day, holidayAppliesToCasteId) {
    var id = getInitialId(txtId).replace("txt", "hfCasteId");
    var casteId = $(id).val();

    if (casteId.toString() == "")
        return false;
    else if (casteId.toString() == holidayAppliesToCasteId.toString())
        return true;
    else
        return false;
}

//ctl00$mainContent$gvw$ctl02$txt1 - last "1" one as day
function getDayFromTxtId(txtId) {
    return parseInt(txtId.replace(getInitialId(txtId), ""));
}

//return 1 or 0.5 as present value for weekly holiday
function GetWeeklyHolidayPresentValue(day) {
    for (var i = 0; i < holidays.length; i++) {
        var holiday = holidays[i];

        if (holiday.Day == day && holiday.Abbreviation == "WH") {
            if (holiday.Type == 1)
                return 0;
            else
            //return 0.5;
                return 0;
        }
    }
    //means doesn't have weekly holiday as the holiday might have been deleted
    return null;
}

function setOption(ddl) {
    if (ddl.value != " ") {
        /// set option for txt,hfType,hfTypeValue controls       
        currentTxt.value = ddl.options[ddl.selectedIndex].text;
        var value = ddl.value;
        if (value == "P") {
            currentType.value = "0:0:0";
            //currentTypeValue.value = "0";
            //currentDeductDays.value = "0";
        }
        else {
            var values = value.split(":");
            currentType.value = values[0] + ':' + values[1] + ':' + getDeductValue(currentTxt.value);
            //currentTypeValue.value = values[1];
            //currentDeductDays.value = getDeductValue(currentTxt.value);
        }

        if (currentTxt.value == currentTxt.OriginalText) {
            $('#' + currentTxt.id).css('background-color', currentTxt.OrginalColor);
        }
        else {
            $('#' + currentTxt.id).css('background-color', 'yellow');
        }
        ifFirstSelectSetForAllCells(currentTxt.id);
        calculateDays(getInitialId(currentTxt.id));
    }
}

///check & fill all empty cell with P as Pay or default Holiday
function ifFirstSelectSetForAllCells(fullId) {
    var id = getInitialId(fullId);
    var isFirstCell = false;
    var hfValue = null;

    //if empty or disabled then process as empty
    if ($('#' + id + "1").val() == "" || $('#' + id + "1").is(':disabled') == true) {
        isFirstCell = true;
    }
    if ($('#' + id + "2").val() == "" || $('#' + id + "2").is(':disabled') == true) {
        isFirstCell = true;
    }
    if (isFirstCell) {
        for (var i = 1; i <= totalDays; i++) {
            if ($('#' + id + i).val() == "") {
                var employeeId = document.getElementById(id.replace('txt', 'hfEmployeeId')).value;
                hfValue = getDefaultEmptyCellValue(id + i, i, employeeId);
                if (hfValue == null)
                    $('#' + id + i).val("P");
                else {
                    $('#' + id + i).val(cellDefaultValue);
                    var values = hfValue.split(":");
                    $('#' + id.replace("txt", "hfType") + i).val(values[0] + ':' + values[1]);
                    //$('#' + id.replace("txt", "hfTypeValue") + i).val(values[1]);
                }

                $('#' + id + i).addClass('attendanceChangedCell');
            }
        }
    }
}

//get the holidays list to display in the cell,handle the case for multiple holiday in a day
function getHolidayListForCell(txtId, day, employeeId) {
    var holidayList = new Array();



    //check for national holiday
    for (var i = 0; i < holidays.length; i++) {
        var holiday = holidays[i];
        if (holiday.Day == day && holiday.IsNationalHoliday) {
            //if Branch holiday
            if (holiday.BranchIDList != "") {
                var value = employeeBranchHoliday[employeeId + ":" + holiday.BranchIDList + ":" + day];
                if (value != 'undefined' && value != null) {
                    holidayList.push(holiday);
                    return holidayList;
                }
            }
            else {
                holidayList.push(holiday);
                return holidayList;
            }
        }
    }



    //check for -1/to all type holiday
    for (var i = 0; i < holidays.length; i++) {
        var holiday = holidays[i];
        if (holiday.Day == day && !holiday.IsWeekly && holiday.Type.toString() == "-1") {
            //if Branch holiday
            if (holiday.BranchIDList != "") {
                var value = employeeBranchHoliday[employeeId + ":" + holiday.BranchIDList + ":" + day];
                if (value != 'undefined' && value != null) {
                    holidayList.push(holiday);
                    return holidayList;
                }
            }
            else {
                holidayList.push(holiday);
                return holidayList;
            }
        }
    }

    // no Caste holiday in HR now

    for (var i = 0; i < holidays.length; i++) {
        var holiday = holidays[i];
        if (holiday.Day == day) {
            if (holiday.Abbreviation == "CH" && isValidForCH(txtId, day, holiday.Type) == true) {
                holidayList.push(holiday);
                return holidayList;
            }
        }
    }

    for (var i = 0; i < holidays.length; i++) {
        var holiday = holidays[i];
        if (holiday.Day == day) {
            if (holiday.Abbreviation == "FH" && isValidForFH(txtId, day) == true) {
                holidayList.push(holiday);
                return holidayList;
            }
        }
    }

    // check for weekly holiday,place last as weekly can be half day, in that case other should go
    for (var i = 0; i < holidays.length; i++) {
        var holiday = holidays[i];
        if (holiday.Day == day & holiday.IsWeekly) {
            holidayList.push(holiday);
            return holidayList;
        }
    }

    return holidayList;
}

var cellDefaultValue;
function getDefaultEmptyCellValue(fullId, day, employeeId) {
    cellDefaultValue = "P";
    var fhValue = null;



    //national holiday
    for (var i = 0; i < holidays.length; i++) {
        var holiday = holidays[i];
        if (holiday.Day == day && !holiday.IsWeekly && holiday.IsNational) {
            //if Branch holiday
            if (holiday.BranchIDList != "") {
                var value = employeeBranchHoliday[employeeId + ":" + holiday.BranchIDList + ":" + day];
                if (value != 'undefined' && value != null) {
                    cellDefaultValue = "NH";
                    return "3:" + holiday.Id;
                }
            }
            else {
                cellDefaultValue = "NH";
                return "3:" + holiday.Id;
            }
        }
    }
    //to all holiday
    for (var i = 0; i < holidays.length; i++) {
        var holiday = holidays[i];
        if (holiday.Day == day && !holiday.IsWeekly && holiday.Type.toString() == "-1") {
            //if Branch holiday
            if (holiday.BranchIDList != "") {
                var value = employeeBranchHoliday[employeeId + ":" + holiday.BranchIDList + ":" + day];
                if (value != 'undefined' && value != null) {
                    cellDefaultValue = "NH";
                    return "3:" + holiday.Id;
                }
            }
            else {
                cellDefaultValue = "NH";
                return fhValue = "3:" + holiday.Id;
            }
        }
    }

    // no caste holiday in HR now
    //for caste holiday
    for (var i = 0; i < holidays.length; i++) {
        var holiday = holidays[i];
        if (holiday.Day == day) {

            if (holiday.Type != -1 && holiday.Type != 0 && !holiday.IsWeekly && isValidForCH('#' + fullId, day, holiday.Type) == true) {
                cellDefaultValue = "CH";
                return fhValue = "3:" + holiday.Id;
            }
        }
    }

    //for female holiday
    for (var i = 0; i < holidays.length; i++) {
        var holiday = holidays[i];
        if (holiday.Day == day) {
            if (holiday.Type == 0 && !holiday.IsWeekly && isValidForFH('#' + fullId, day) == true) {
                cellDefaultValue = "FH";
                return fhValue = "3:" + holiday.Id;
            }
        }
    }

    //for weekly holiday,place at last as it can be half day with Other leave
    for (var i = 0; i < holidays.length; i++) {
        var holiday = holidays[i];
        if (holiday.Day == day && holiday.IsWeekly) {
            if (holiday.Type == 0)
                cellDefaultValue = "WH/2";
            else
                cellDefaultValue = "WH";
            return fhValue = "2:" + holiday.Id;
        }
    }

    return fhValue;
}

window.onload = function () {
    //hide the drop down when click on other page section
    $(document).click(function () {
        if (ddlOptionReference == null) {
            ddlOptionReference = $(ddlOption);
            ddlOptionDivReference = $(ddlOptionDiv);
        }
        ddlOptionReference.hide();
        if (empDetailsDiv != null)
            empDetailsDiv.hide();
    });
}

//get the initial element id leaving the number
function getInitialId(idValue) {
    for (var i = idValue.length - 1; i >= 0; i--) {
        var val = idValue.charAt(i);
        val = parseInt(val);
        if (isNaN(val))
            return idValue;
        idValue = idValue.substring(0, i);
    }
    return idValue;
}


//check if abbr has "-" then hourly leave type
function isHourlyLeaveValue(abbr) {
    return abbr.indexOf("-") > 0;
}
function getHourlyLeaveHour(abbr) {
    var values = abbr.split("-");
    return parseFloat(values[1]);
}
function getAbbrOnlyForHourlyLeave(abbr) {
    var values = abbr.split("-");
    return (values[0]);
}

//get pay(Deduct) value for the abbreviation passed    as later named changed to Deduct as holds deduct values
function getDeductValue(abbr) {

    if (abbr == "P" || abbr == "")
        return 0;

    abbr = abbr.replace("++", "").replace("--", "");

    if (isHourlyLeaveValue(abbr)) {
        var abbrOnly = getAbbrOnlyForHourlyLeave(abbr);
        //abbr = abbrOnly;
        var value = 0;
        //var forFullDay = parseFloat(leaves[abbrOnly]["Pay"]);
        var type = leaves[abbrOnly]["Type"];
        var hour = getHourlyLeaveHour(abbr);

        switch (type) {
            case "FullyPaid":
                value = 0;
                break;
            case "HalfPaid":
                value = hour / 2;  //hour / (2 * hoursInADay);
                break;
            case "Unpaid":
                value = hour; // / hoursInADay;
                break;
            case "NonLeaveAbsence":
                value = 0;
                break;
        }
        return parseFloat(value);
    }
    else {
        if (typeof (leaves[abbr]) == 'undefined')
            return 0;
        return parseFloat(leaves[abbr]["Pay"]);

    }
}

//get present value for the abbreviation passed
function getAbsentValue(abbr) {

    if (abbr == "")
        return 0;

    var isCompensatoryOnHoliday = false;

    //if compensatory on holiday
    if (abbr.indexOf('++') >= 0) {
        isCompensatoryOnHoliday = true;
    }

    // don't count for compensatory leave
    if (abbr.indexOf('++') >= 0 || abbr.indexOf('--') >= 0)
        return 0;

    abbr = abbr.replace("++", "").replace("--", "");

    if (isHourlyLeaveValue(abbr)) {
        var abbrOnly = getAbbrOnlyForHourlyLeave(abbr);

        var value = 0;
        //var forFullDay = parseFloat(leaves[abbrOnly]["Pay"]);
        var type = leaves[abbrOnly]["Type"];
        var hour = getHourlyLeaveHour(abbr);
        switch (type) {
            case "FullyPaid":
                value = hour; //; / hoursInADay;
                break;
            case "HalfPaid":
                value = hour; // / 2;// / hoursInADay;
                break;
            case "Unpaid":
                value = hour; // / hoursInADay;
                break;
            case "NonLeaveAbsence":
                value = 0;
                break;
        }
        if (isCompensatoryOnHoliday)
            return parseFloat(value) * -1;
        else
            return parseFloat(value);
    }
    else {

        if (typeof (leaves[abbr]) == 'undefined')
            return 0;

        if (isCompensatoryOnHoliday)
            return parseFloat(leaves[abbr]["Present"]) * -1;
        else
            return parseFloat(leaves[abbr]["Present"])

    }
}

//function to calculate the pay,present days for the employee when the cell option changes
function calculateDays(id) {
    var pay = 0, present = 0;
    var abbr; var value;
    var hasMsgDisplayed = false;
    for (var i = 1; i <= totalDays; i++) {

        pay += 1;
        present += 1;
        abbr = $('#' + id + i).val();

        if (abbr != "P") {
            pay -= parseFloat(getDeductValue(abbr));  //parseFloat(leaves[abbr]["Pay"]);
            //if weekly holiday handle full/half day
            if (abbr == "WH") {
                value = GetWeeklyHolidayPresentValue(i);
                if (value == null) {
                    //holiday has been deleted, now it will not occure as checked while deleting
                    if (!hasMsgDisplayed) {
                        alert("Weekly holiday has been deleted,\n so remove all weekly holiday for this employee.");
                        hasMsgDisplayed = true;
                        isSavingValid = false;
                        savingInvalidMsg = "Employee having Id 123 has invalid weekly holiday,\n so can't save the records.";
                    }
                }
                else
                    present -= parseFloat(value);
            }
            else {
                present -= parseFloat(getAbsentValue(abbr));  //parseFloat(leaves[abbr]["Present"]);
            }
        }
    }

    pay = (totalDays - pay); //.toFixed(3);
    present = (totalDays - present); //.toFixed(3);


    $('#' + id.replace("txt", "lblPayDays")).html(pay.toString());
    $('#' + id.replace("txt", "lblPresentDays")).html(present.toString());
    $('#' + id.replace("txt", "hfPayDays")).val(pay.toString());
    $('#' + id.replace("txt", "hfPresentDays")).val(present.toString());
}

function selectDeselectAll(chk) {
    $('input[type=checkbox]').each(
            function (index) {
                if ($(this).prop('disabled') == false && this.id.indexOf('chkDelete') > 0)
                    this.checked = chk.checked;
            }
        );
}

function fillAttendance() {

    //check each workday leave of each emp., first validates then set in hfWorkDayLeaves hidden field to be saved
    $(".lblEmployeeId").each(
        function (index) {
            var rowId = this.id.substring(0, this.id.indexOf("lblId")).replace("#", "");
            var employeeId = document.getElementById(rowId + "hfEmployeeId").value;

            var currentTxtId = rowId + "txt1";
            ifFirstSelectSetForAllCells(currentTxtId);
            calculateDays(getInitialId(currentTxtId));

        }
    );
}


function recalcualteAbsentDaysOnLoad() {

    //check each workday leave of each emp., first validates then set in hfWorkDayLeaves hidden field to be saved
    $(".lblEmployeeId").each(
        function (index) {
            var rowId = this.id.substring(0, this.id.indexOf("lblId")).replace("#", "");
            var employeeId = document.getElementById(rowId + "hfEmployeeId").value;

            var currentTxtId = rowId + "txt1";
            //ifFirstSelectSetForAllCells(currentTxtId);
            calculateDays(getInitialId(currentTxtId));

        }
        );
}