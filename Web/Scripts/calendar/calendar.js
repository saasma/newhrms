﻿



//  SET ARRAYS
//init in Content.ascs
if (typeof (isEnglish) == 'undefined')
    isEnglish = false;
if (typeof (todayDate) == 'undefined')
    todayDate = '2068/5/5';

var dateServerControlRef = null;

var showCalendarMonthYear = true;

var todayDateValues = todayDate.split('/');
var year = todayDateValues[0];
var day = todayDateValues[2]; // = 2011;//  Calendar.getYear();	    // Returns year
var month = todayDateValues[1]; // = 10;//  Calendar.getMonth();    // Returns month (0-11)


var month_of_year = new Array('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec ');

var month_of_year_nep = new Array("बैसाख", "ज्येष्ठ", "आषाढ", "श्रावण", "भाद्र", "आश्विन", "कार्तिक", "मंसिर", "पौष", "माघ", "फाल्गुण", "चैत्र");
var month_of_year_nep_eng = new Array("Apr/May", "May/Jun", "Jun/Jul", "Jul/Aug", "Aug/Sep", "Sep/Oct", "Oct/Nov", "Nov/Dec", "Dec/Jan", "Jan/Feb", "Feb/Mar", "Mar/Apr");


//append container div in the body
var _body = document.getElementsByTagName('body')[0];
var _div = document.createElement('div');
_div.setAttribute("id", "dateDiv");
_div.className = 'dateContainerDiv';


if (typeof jQuery !== 'undefined') {
    $(document).ready(function () {
        //add trim for IE8
        if (typeof String.prototype.trim !== 'function') {
            String.prototype.trim = function () {
                return this.replace(/^\s+|\s+$/g, '');
            }
        }

        _body.appendChild(_div);
    });
}
else {
    //add trim for IE8
    if (typeof String.prototype.trim !== 'function') {
        String.prototype.trim = function () {
            return this.replace(/^\s+|\s+$/g, '');
        }
    }

    _body.appendChild(_div);
}




var DAYS_OF_WEEK = 7;
var currentlySelectedDate = '';
var isDateColumnChanged = false;

/* VARIABLES FOR FORMATTING
NOTE: You can format the 'BORDER', 'BGCOLOR', 'CELLPADDING', 'BORDERCOLOR'
      tags to customize your caledanr's look. */

var TR_start = '<TR>';
var TR_end = '</TR>';
var highlight_start = '<TD onclick="dateSelected(event,this)"  customDate class="todayCell" ';
var highlight_end = '</TD>';
var selected_start = '<TD onclick="dateSelected(event,this)"  customDate class="selectedCell" ';
var selected_end = '</TD>';
var TD_start = '<TD onclick="dateSelected(event,this)" customDate class="dayCell" ';
var TD_end = '</TD>';
var TD_WeekDay_Start = '<td customDate>';
var TD_WeekDay_End = '</td>';

/* BEGIN CODE FOR CALENDAR
NOTE: You can format the 'BORDER', 'BGCOLOR', 'CELLPADDING', 'BORDERCOLOR'
tags to customize your calendar's look.*/

function isInputDateValid(dateStr) {

    

    if (isEnglish)
        return 32 - new Date(year, month, 32).getDate();
    else {

        return daysinmonthNE(month + 1, year);
    }
}

var selectedNepEngYear, selectedNepEngMonth, selectedNepEngDay;
function getCurrentMonthYearText() {
    var returnText = "";
    eyear = 0, emonth = 0, eday = 0;
    if (isEnglish)
        returnText =  year + '&nbsp;' + month_of_year[month];
    else {
        returnText =  year + '&nbsp;' + month_of_year_nep[month];
        // add english date for Nepali calendar system
        try {
            var monthStartEngDate = nepToEng[year.toString() + (month+1).toString()];
            if (typeof (monthStartEngDate) != 'undefined') {
                monthStartEngDate = monthStartEngDate.split('/');
                selectedNepEngYear = monthStartEngDate[0]; // = 2011;//  Calendar.getYear();	    // Returns year
                selectedNepEngMonth = monthStartEngDate[1]; // = 10;//  Calendar.getMonth();    // Returns month (0-11)
                selectedNepEngDay = monthStartEngDate[2];

                returnText += '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + selectedNepEngYear + '&nbsp;' + month_of_year_nep_eng[month];
            }
            else {
                selectedNepEngYear = 'undefined';
                selectedNepEngMonth = 'undefined';
                selectedNepEngDay = 'undefined'; 
            }
        }
        catch (ee) { }
    }
    return returnText;
}

function getDayInCurrentMonth() {
    return (new Date()).getDay();
}

function getTotalDaysInCurrentMonth() {
    if (isEnglish)
        return 32 - new Date(year, month, 32).getDate();
    else {
        
        return daysinmonthNE(month+1, year);
    }
}
function getTotalDaysInTheMonth(theYear,theMonth) {
    if (isEnglish)
        return 32 - new Date(theYear, theMonth, 32).getDate();
    else {

        return daysinmonthNE(theMonth + 1, theYear);
    }
}


var day_of_week = new Array('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat');
var day_of_weekNepali = new Array('आ', 'सो', 'मं', 'बु', 'बि', 'शु', 'श');

var cal;    // Used for printing

var calendarMonthYear = null;
var isCalendarInitialised = false;
var isCalendarDDLChanged = false;
var dateBody = null;
var dateYear = null;
var dateMonth = null;
function initCalendar(){

    if (!isCalendarInitialised) 
    {


        todayDateValues = todayDate.split('/');
        year = todayDateValues[0]; // = 2011;//  Calendar.getYear();	    // Returns year
        month = todayDateValues[1]; // = 10;//  Calendar.getMonth();    // Returns month (0-11)

        var monthArray = null;
        if (isEnglish)
            monthArray = month_of_year;
        else
            monthArray = month_of_year_nep;


        cal = '<div class="dateContainer" id="dateContainer" customDate="true">'; //container

        //Start append all months in the table 
//        cal += "<table class='calendarMonthList' customDate><tr>";
//        for (var i = 0; i < 6; i++)
//            cal += '<td customDate><a id="rigocalm' + (i + 1) + '" customDate="true" onclick="monthClicked(' + i + ')" > ' + monthArray[i] + '</a></td>';
//        cal += "</tr><tr>";
//        for (var i = 6; i < 12; i++)
//            cal += '<td customDate><a id="rigocalm' + (i + 1) + '" customDate="true" onclick="monthClicked(' + i + ')" > ' + monthArray[i] + '</a></td>';
//        cal += "</tr></table>";
        //End append all months in the table 


        cal += '<div    class="dateUpperRegion"  customDate="true"><table  customDate="true"><tr style="height:32px;" id="rigoCalendarMonthYearTR"><td  customDate>Month <select style="height:22px" customDate="true" id="dateMonth" onchange="monthYearChange(event,this);">';

        if (isEnglish) {
            //add month in drop down
            for (var i = 1; i <= 12; i++)
                cal += '<option value="' + i + '">' + month_of_year[i - 1] + '</option>';
        }
        else {
            for (var i = 1; i <= 12; i++)
                cal += '<option value="' + i + '">' + month_of_year_nep[i - 1] + '</option>';
        }
        cal += ' </select> </td><td>';
        cal += 'Year <select style="height:22px"  id="dateYear" onclick="stopPropagation(event);"  customDate="true" onchange="monthYearChange(event,this)">';
        //add month in drop down
        if (isEnglish) {
            for (var i = 1944; i <= 2033; i++)
            //for (var i = 2004; i <= 2024; i++)
                cal += '<option value="' + i + '">' + i + '</option>';
        }
        else {
            for (var i = 2000; i <= 2110; i++)
            //for (var i = 2060; i <= 2089; i++)
                cal += '<option value="' + i + '">' + i + '</option>';
        }
        cal += '</select> </td><td customDate class="closeBtn"> <a href="javascript:hideMonthYear()">x<a/> </td> </tr></table> </div>  '; // drop down region

        cal +=
        '<div class="dateHeader" customDate> <table width="100%"> <tr style="height:25px;">  <td customDate> <a customDate class="left_call" title="Prev Month" onclick="monthArrowClicked(true)"> &nbsp; &nbsp; </a>   </td> <td id="calendarMonthYear" onclick="showHideMonthYear()" style="width:175px;cursor:pointer" customDate> '
        + "" +
        ' </td> <td customDate>  <a customDate class="right_call"  title="Next Month" onclick="monthArrowClicked(false)"> &nbsp; &nbsp;  </a> </td>  </tr> </table> </div>'; // Month-Year label
    

        cal += '<div id="dateBody"  customDate="true">';

        cal += ' </div><div class="todayBtn" customdate> <a href="javascript:void(0)" onclick="setTodayDate()" class="btntoday">Today</a></div> </div>';

        _dateBody = document.getElementById('dateBody');


        isCalendarInitialised = true;
    }
}


function showHideMonthYear() {
    if (showCalendarMonthYear)
        showCalendarMonthYear = false;
    else
        showCalendarMonthYear = true;
    showCalendar(dateServerControlRef,true);
}
function monthClicked(monthIndex) {

    dateMonth.options.selectedIndex = monthIndex;
    monthYearChange();
}


function monthArrowClicked(isPreviousMonth) {
    if (isPreviousMonth) {
        if (dateMonth.options.selectedIndex > 0) {
            dateMonth.options.selectedIndex = dateMonth.options.selectedIndex - 1;
            monthYearChange();
        }
        //if last month then goto previous year
        else {

            if (dateYear.options.selectedIndex > 0)
                dateMonth.options.selectedIndex = dateMonth.options.length - 1;
            yearArrowClicked(true);
        }

    }
    else {
        if (dateMonth.options.selectedIndex < dateMonth.options.length - 1) {
            dateMonth.options.selectedIndex = dateMonth.options.selectedIndex + 1;
            monthYearChange();
        }
        else {
            if (dateYear.options.selectedIndex < dateYear.options.length - 1)
                dateMonth.options.selectedIndex = 0;
            yearArrowClicked(false);
        }
    }
}

function yearArrowClicked(isPreviousYear) {


    if (isPreviousYear) {
        if (dateYear.options.selectedIndex > 0) {
            dateYear.options.selectedIndex = dateYear.options.selectedIndex - 1;
            monthYearChange();
        }
      
    }
    else if (dateYear.options.selectedIndex < dateYear.options.length - 1 ) {
        dateYear.options.selectedIndex = dateYear.options.selectedIndex + 1;
        monthYearChange();
    }
  
}

function setTodayDate() {
    setDateValue(todayDate);
    hideCalendar(); 
}

function getHomManyEmptyCellsForCurrentMonth() {

    if (isEnglish) {

        var myDate = new Date(year, month, 1);

        return myDate.getDay();
    }
    else {
        return getpInNEMonth(month, year);
    }
}

function monthYearChange(e,element) {
    //e = (e) ? e : window.event;

    isCalendarDDLChanged = true;
    regenerateCalendar();
    stopPropagation();
}

function stopPropagation(evt) {
    evt = (evt) ? evt : window.event;

    evt.cancelBubble = true;
    if (evt.stopPropagation) evt.stopPropagation();
}

function regenerateCalendar(currentSelelectedDay) {
    if (typeof (currentSelelectedDay) == 'undefined')
        currentSelelectedDay = -1;

    
    var selMonth = document.getElementById('dateMonth').value;
    var selYear = document.getElementById('dateYear').value;


    var html = new Array();

    month = selMonth - 1;
    year = selYear;


    //if( calendarMonthYear == null)
    calendarMonthYear = document.getElementById('calendarMonthYear');
    calendarMonthYear.innerHTML = getCurrentMonthYearText();
    
    html[html.length] = '<table customDate class="dateTable" BORDER="0" CELLSPACING="0" CELLPADDING="2">' + TR_start; // calendar table starting


    //   DO NOT EDIT BELOW THIS POINT  //

    // LOOPS FOR EACH DAY OF WEEK
    for (var index = 0; index < DAYS_OF_WEEK; index++) {
        if(isEnglish)
            html[html.length] = TD_WeekDay_Start + day_of_week[index] + TD_WeekDay_End;
        else
            html[html.length] = TD_WeekDay_Start + day_of_weekNepali[index] + TD_WeekDay_End;
    }

    html[html.length] = TD_end + TR_end;
    html[html.length] = TR_start;


    //get how many to fill the gap
    var gapIndex = getHomManyEmptyCellsForCurrentMonth();
   
    // FILL IN BLANK GAPS UNTIL TODAY'S DAY
    for (index = 0; index < gapIndex; index++)
        html[html.length] = TD_start +'>' + '  ' + TD_end;


    // LOOPS FOR EACH DAY IN CALENDAR
    var startDay = 1;
    var endDay = getTotalDaysInCurrentMonth();

   // html[html.length] = TD_start + startDay + TD_end;
    var engDateForNepaliCalendar = null;
    if (selectedNepEngDay != 0)
        engDateForNepaliCalendar = new Date(selectedNepEngYear, selectedNepEngMonth-1, selectedNepEngDay);

    for (var startDay = 1; startDay <= endDay; startDay++) {

        //if first day in the week
        if ((gapIndex + startDay) % 7 == 1)
            html[html.length] = TR_end + TR_start;


        // if current selected date then highlight
        if (currentSelelectedDay == startDay) 
        {
            if(isEnglish)
                html[html.length] = selected_start + "day=" + startDay + ">" + startDay + selected_end;
            else
            {
                var otherDay = engDateForNepaliCalendar.getDate();
                html[html.length] = selected_start + "day=" + startDay + ">" + '<span class="mainDay">' + startDay + '</span><span class="otherDay">' + ( isNaN(otherDay)  ? "" : otherDay) + "</span>" + selected_end;
            }
        }
        //check if today date then highlight the cell
        else if (year == todayDateValues[0] && (month + 1) == parseInt(todayDateValues[1], '10') && startDay == parseInt(todayDateValues[2], '10')) {
            if(isEnglish)
                html[html.length] = highlight_start + "day=" + startDay + ">" + startDay + highlight_end;
            else{
                var otherDay = engDateForNepaliCalendar.getDate();
                html[html.length] = highlight_start + "day=" + startDay + ">" + '<span class="mainDay">' + startDay + '</span><span class="otherDay">' + (isNaN(otherDay) ? "" : otherDay) + "</span>" + highlight_end;
            }
        } else {
            if(isEnglish)
                html[html.length] = TD_start + "day=" + startDay + ">" + startDay + TD_end;
            else
            {
                var otherDay = engDateForNepaliCalendar.getDate();
                html[html.length] = TD_start + "day=" + startDay + ">" + '<span class="mainDay">' + startDay + '</span><span class="otherDay">' + (isNaN(otherDay) ? "" : otherDay) + "</span>" + TD_end;
            }
         }


         if (engDateForNepaliCalendar != null) {
             engDateForNepaliCalendar.setDate(engDateForNepaliCalendar.getDate() + 1);
         }

    } // end for loop

    html[html.length] = '</TD></TR></TABLE></div>';

   // if (dateBody == null)
    dateBody = document.getElementById('dateBody');
    dateBody.innerHTML = html.join("");

    // hight current month selection
//    for (i = 1; i <= 12; i++) {
//        if (month+1 == i)
//            document.getElementById('rigocalm' + i).className = 'selectedmonth';
//        else
//            document.getElementById('rigocalm' + i).className = '';
//    }


    //_dateBody.getElementById('dateBody').innerHTML = html.join("");
}


//</center><p>

//<!-- Script Size:  4.15 KB -->

//keypress handler to hide calendar when Tab is pressed
var calendarKeyPress = function (el, e) {
    if (e.keyCode == e.TAB)
        hideCalendar(this);
    else if (e.keyCode == e.ENTER) {
        if (el.getValue() == "") {
            el.setValue(todayDate);
            resetCalendarDisplayAfterDateChangedInTextBox(todayDate);
        }
        else {
            var changedDate = addOrSubtractToDate(todayDate, el.getValue());
            if (changedDate != "") {
                el.setValue(changedDate);
                resetCalendarDisplayAfterDateChangedInTextBox(changedDate);
            }
        }

    }
    else if (e.keyCode == e.DOWN) {
       
        var value = el.getValue() == "" ? todayDate : el.getValue();
        var changedDate = addOrSubtractToDate(value, "-1");
        if (changedDate != "") {
            el.setValue(changedDate);
            resetCalendarDisplayAfterDateChangedInTextBox(changedDate);
        }
    }
    else if (e.keyCode == e.UP) {
       
        var value = el.getValue() == "" ? todayDate : el.getValue();
        var changedDate = addOrSubtractToDate(value, "+1");
        if (changedDate != "") {
            el.setValue(changedDate);
            resetCalendarDisplayAfterDateChangedInTextBox(changedDate);
        }
    }
}

function hideCalendar(element) {
    if (dateServerControlRef == element)
        hideCalendar();
}
function hideMonthYear() {
        hideCalendar();
}
function parseDate(value) {
    var values = value.split("/");

    values[0] = parseInt(values[0], '10');
    values[1] = parseInt(values[1], '10');
    values[2] = parseInt(values[2], '10');

    return values;
}

function resetCalendarDisplayAfterDateChangedInTextBox(selectedDate) {
    var values = parseDate(selectedDate);
    if (values[1].toString() != dateMonth.value || values[0].toString() != dateYear.value) {
        dateMonth.value = values[1];
        dateYear.value = values[0];
        monthYearChange();
        //regenerateCalendar();
    }
}

function addOrSubtractToDate(currentDate, inputValue) {
    //do later for english

    inputValue = inputValue.toLocaleString();
    
    // day addition
    if (inputValue.match(new RegExp(/^\+[0-9]{1,3}$/))) { //apply RegEx to check
        var dayToBeAdded = parseInt(inputValue.replace("+", ""), '10'); //get the value to be added
        var dateParts = parseDate(currentDate); //parse current ate
        for (var i = 1; i <= dayToBeAdded; i++) {
            //if current day is the last day
            if (dateParts[2] >= getTotalDaysInTheMonth(dateParts[0], dateParts[1] - 1)) { //if date is the last day of the month
                
                dateParts[2] = 1;
                
                if (dateParts[1] == 12) {
                    dateParts[1] = 1;
                    dateParts[2] += 1;
                }
                else
                    dateParts[1] += 1;
            }
            else {
                dateParts[2] += 1;
            }
        }
        return dateParts[0] + "/" + dateParts[1] + "/" + dateParts[2];
    }
    // day subtraction
    else if (inputValue.match(new RegExp(/^\-[0-9]{1,3}$/))) {
        var dayToBeAdded = parseInt(inputValue.replace("-", ""), '10');
        var dateParts = parseDate(currentDate);
        for (var i = 1; i <= dayToBeAdded; i++) {
            //if current day is the last day
            if (dateParts[2] <= 1) {
                var monthIndex = dateParts[1] - 2; // get prev month index, 2 coz month index starts from 0
                if (monthIndex < 0)
                    monthIndex = 11;
                var yearIndex = monthIndex == 11 ? dateParts[0] - 1 : dateParts[0];

                dateParts[2] = getTotalDaysInTheMonth(yearIndex, monthIndex);
                dateParts[1] = monthIndex + 1;
                dateParts[0] = yearIndex;
            }
            else {
                dateParts[2] -= 1;
            }
        }
        return dateParts[0] + "/" + dateParts[1] + "/" + dateParts[2];
    }
    // month addition
    else if (inputValue.match(new RegExp(/^\+m[0-9]{1,2}$/))) { //apply RegEx to check
        var monthToBeAdded = parseInt(inputValue.replace("+m", ""), '10'); //get the value to be added
        var dateParts = parseDate(currentDate); //parse current ate
        for (var i = 1; i <= monthToBeAdded; i++) {
            //if current day is the last day
            if (dateParts[1] == 12) {
                dateParts[1] = 1;
                dateParts[0] += 1;
            }
            else {
                dateParts[1] += 1;
            }
        }
        var maxDay = getTotalDaysInTheMonth(dateParts[0], dateParts[1] - 1);
        if (dateParts[2] > maxDay)
            dateParts[2] = maxDay;
        return dateParts[0] + "/" + dateParts[1] + "/" + dateParts[2];
    }
    // month subtraction
    else if (inputValue.match(new RegExp(/^\-m[0-9]{1,2}$/))) { //apply RegEx to check
        var monthToBeAdded = parseInt(inputValue.replace("-m", ""), '10'); //get the value to be added
        var dateParts = parseDate(currentDate); //parse current ate
        for (var i = 1; i <= monthToBeAdded; i++) {
            //if current day is the last day
            if (dateParts[1] == 1) {
                dateParts[1] = 12;
                dateParts[0] -= 1;
            }
            else {
                dateParts[1] -= 1;
            }
        }
        var maxDay = getTotalDaysInTheMonth(dateParts[0], dateParts[1] - 1);
        if (dateParts[2] > maxDay)
            dateParts[2] = maxDay;
        return dateParts[0] + "/" + dateParts[1] + "/" + dateParts[2];
    }

    return "";
}

function isValidDate(value) {
    var myRegex = new RegExp("^[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}$");

    if (value.match(myRegex)) {
        var values = value.split("/");
        var theDay = parseInt(values[2], '10');
        var theMonth = parseInt(values[1], '10');
        var theYear = parseInt(values[0], '10');

        //check for year
        if (!isEnglish) {
            if (theYear < 1970 || theYear > 2110) {
                return false;
            }
        }

        //check for month
        if (theMonth < 1 || theMonth > 12) {
            return false;
        }

        //check for day

        var maxDays = getTotalDaysInTheMonth(theYear, theMonth - 1);

        if (theDay < 1 || theDay > maxDays) {
            return false;
        }
    }
    else {
        return false;
    }
            
    return true;
}

//check if the typed date is invalid then clear the value
///http://www.madirish.net/node/118
function checkDateIfValid(element) {

    //hideCalendar(); //then use this othersize next/prev won't work
    
    var value = element.getValue();


    //check for today date when emtpy
    if (value == "" && element.showWhenEmpty != undefined && element.showWhenEmpty =='true') {
        element.setValue(todayDate); return false;
    }

    //check for date increment or decrement
    var changedDate = addOrSubtractToDate(todayDate, value);
    if (changedDate != "") {
        element.setValue(changedDate); return false;
    }



    if (isValidDate(value)) {
    }   
    else {
        element.setValue(''); return false;
    }
    return true;
}

function showCalendar(element,forceRegenerate) {

    if (typeof (forceRegenerate) != 'undefined' && forceRegenerate == true) {
        forceRegenerate = true;
    }
    else
        forceRegenerate = false;
    
    if (typeof (doNotShowCalendar) != 'undefined' && doNotShowCalendar == true) {
        doNotShowCalendar = false;
        return;
    }

    initCalendar();

    if (forceRegenerate == false) {
        //if calender is being displayed & the id matches then don't regenerate calendar
        if (_div.style.display != 'none' && dateServerControlRef != null && dateServerControlRef.id == element.id)
            return;
    }

    var overrideReadOnly = false;
    if (element.showWhenReadOnlyAlso != undefined && element.showWhenReadOnlyAlso == 'true') {
        overrideReadOnly = true;
    }

    //check if field is not readonly and disabled
    if (overrideReadOnly == false) {
        if (element.readOnly == true || element.disabled == true)
            return;
    }
    dateServerControlRef = element;

    //var elementReference = $('#' + element.id);
   // var dateDiv = $('#' + element.id.replace(element.getAttribute('mainId'), 'dateDiv'));

    _div.innerHTML = cal;


    //if (dateYear == null) 
    {
        dateYear = document.getElementById('dateYear');
        dateMonth = document.getElementById('dateMonth');
    }


    //get current date
    var currentDate = dateServerControlRef.getValue();
    
    if ( currentDate != null && currentDate != "" ) {

        //alert(currentDate);

        var dateParts = currentDate.split("/");
        month = parseInt(dateParts[1], '10');
        year = parseInt(dateParts[0], '10');
        day = dateParts[2];
        //document.getElementById('dateYear').value = year;

        dateMonth.value = (month);
        dateYear.value = (year);

    } else {

       //var todayDateValues = todayDate.split('/');
        year = parseInt(todayDateValues[0], '10'); // = 2011;//  Calendar.getYear();	    // Returns year
        month = parseInt(todayDateValues[1], '10'); // = 10;//  Calendar.getMonth();    // Returns month (0-11)
        day = parseInt(todayDateValues[2], '10');
        dateMonth.value = (month);
        dateYear.value = (year);
        
    }



    regenerateCalendar(day);


    //get the position of the placeholder element
    var left = dateServerControlRef.el.getX();
    var top = dateServerControlRef.el.getY();
    width = 0;

    top += (dateServerControlRef.el.getHeight() + 1);

    if (showCalendarMonthYear == false)
        top += 32; //increase top height if display none

    _div.style.left = (left + width ) + "px";
    _div.style.top = top + "px";





    _div.style.display = 'block';

    //Check if the bottom is cut off then display the calendar in the upper side
    if (_div.clientHeight + top > window.innerHeight) {
        _div.style.top = top - _div.clientHeight - dateServerControlRef.el.getHeight() - 2 + "px";
    }

    //Check if the right is cut off then display the calendar in the left side
    if (_div.clientWidth + (left + width) > window.innerWidth) {
        _div.style.left = left - _div.clientWidth - 2 + dateServerControlRef.el.getWidth() + "px";
    }

    if (showCalendarMonthYear)
        document.getElementById('rigoCalendarMonthYearTR').style.display = 'block';
    else
        document.getElementById('rigoCalendarMonthYearTR').style.display = 'none';
}

window.onclick = function (e) {

    if (_div.style.display != "block")
        return;

    //if calendar month/year ddl changed then don't hide
    if (isCalendarDDLChanged) {
        isCalendarDDLChanged = false;
        return; // do not comment else will be hidden in firefox
    }

    e = (e) ? e : window.event;


    var srcElement = (typeof e.target != 'undefined') ? e.target : e.srcElement;

    if (_div.style.display == 'block'
        && dateServerControlRef != null 
        && dateServerControlRef.id != srcElement.id
        && srcElement.getAttribute('customDate') == null && isCalendar(srcElement) == false) {
        hideCalendar();
    }
}
    
function isCalendar(srcElement) {

    //for chrome
    if (typeof (srcElement) != 'undefined' && typeof (srcElement.labels) != 'undefined') {
        for (var i = 0; i < srcElement.labels.length; i++) {
            if (srcElement.labels[i].id.indexOf(dateServerControlRef.id) >= 0 ) {
                return true;
            }
        }
    }

    //for FF

    if (typeof (srcElement) != 'undefined' && typeof (srcElement.parentNode) != 'undefined') {

        if (srcElement.parentNode.id.toString().indexOf(dateServerControlRef.id) >= 0) {
            return true;
        }

    }


    return false;
}

function dateSelected(event,tdElement)
{

    if( tdElement.getAttribute("day") != "") {

        var month = dateMonth.value.toString().length == 1 ? "0" + dateMonth.value : dateMonth.value;
        var day = tdElement.getAttribute("day").trim().toString().length == 1 ? "0" + tdElement.getAttribute("day").trim() : tdElement.getAttribute("day").trim();

        var date = dateYear.value + "/" + month + '/' + day;
        setDateValue(date);
        hideCalendar();
    }
    
}


function setDateValue(date) {

   
    currentlySelectedDate = date;
    dateServerControlRef.setValue(date);
    if (typeof (refreshGridViewForDateInGrid) != 'undefined' && isDateColumnChanged) {
        refreshGridViewForDateInGrid();
    }

    isDateColumnChanged = false;
}

function hideCalendar() {
    //$('#CalendarControl1_dateDiv').hide();
    _div.style.display = 'none';
}

function clearHiddenField(element) {

}

function calendarOnClick(event, sourceElement, e3) {
    if( _div.style.display == 'none')
    showCalendar(sourceElement);
}

//attach calendar display in click
var afterRender = function (field) {
    field.getEl().on('click', function (event, el) {
        showCalendar(eval(this.id));
        
    });
}