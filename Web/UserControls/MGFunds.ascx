﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MGFunds.ascx.cs" Inherits="Web.UserControls.MGFunds" %>
<script type="text/javascript">

    var fieldSetPFRF = '<%= fieldSetPFRF.ClientID %>';

    function ValidateTextBoxSum(source, args) {
        var n1 = parseFloat($('#<%= txtPFCCPercent.ClientID %>').val());
        var n2 = parseFloat($('#<%= txtPFECPercent.ClientID %>').val());
        var sum = (n1 + n2);

        if (sum == 0 || sum > 100)
            args.IsValid = false;
        else
            args.IsValid = true;

    }
</script>
<fieldset runat="server" class="sectionsub" id="fieldSetPFRF" style="display: inline;
    float: left; box-shadow: 0 0 0!important">
    <table cellpadding="0" cellspacing="0" class="companytabl">
        <tr>
            <td colspan="2">
                <h3 style='padding-left: 0px; border-bottom: 1px solid #10b0ea;'>
                    Retirement Fund</h3>
            </td>
        </tr>
        <tr>
            <td class="fieldHeader" width="150px">
                <My:Label ID="lblPFText" runat="server" Text="Company RF No" ShowAstrick="false" />
            </td>
            <td>
                <asp:TextBox ID="txtPFNo" runat="server" Width="180px"></asp:TextBox>
                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator0" runat="server" ControlToValidate="txtPFNo"
                    Display="None" ErrorMessage="RF No is required." ValidationGroup="AECompany"></asp:RequiredFieldValidator>--%>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                <asp:RadioButtonList ID="rdbListPFApproval" runat="server" RepeatDirection="Horizontal"
                    RepeatLayout="Flow">
                    <asp:ListItem Selected="True" Value="1">Approved</asp:ListItem>
                    <asp:ListItem Value="0">Unapproved</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td class="fieldHeader">
                <asp:Label ID="Label2" runat="server" Text="Approval Date"></asp:Label>
            </td>
            <td>
                <My:Calendar Id="calPFApprovalDate" IsSkipDay="true" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="fieldHeader">
                <My:Label ID="Label5" Text="Company Pays (%)" runat="server" ShowAstrick="true" />
            </td>
            <td>
                <asp:TextBox ID="txtPFCCPercent" runat="server" Width="80px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator100" runat="server" ControlToValidate="txtPFCCPercent"
                    Display="None" ErrorMessage="Company pays percent is required." ValidationGroup="AECompany"></asp:RequiredFieldValidator>
                <%--<asp:CompareValidator ID="valPFCC0" runat="server" ControlToValidate="txtPFCCPercent"
                    Display="None" ErrorMessage="Invalid Company pays percent." Operator="GreaterThanEqual"
                    Type="Double" ValidationGroup="AECompany" ValueToCompare="0"></asp:CompareValidator>--%>
                <asp:RangeValidator runat="server" ErrorMessage="Invalid Company pays percent." ValidationGroup="AECompany"
                    Type="Double" ID="CompareValidator141" Display="None" MinimumValue="0" MaximumValue="100"
                    ControlToValidate="txtPFCCPercent"></asp:RangeValidator>
            </td>
        </tr>
        <tr>
            <td class="fieldHeader">
                <My:Label ID="Label6" Text="Employee Pays (%)" runat="server" ShowAstrick="true" />
            </td>
            <td>
                <asp:TextBox ID="txtPFECPercent" runat="server" Width="80px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator00" runat="server" ControlToValidate="txtPFECPercent"
                    Display="None" ErrorMessage="Employee pays percent is required." ValidationGroup="AECompany"></asp:RequiredFieldValidator>
                <%--  <asp:CompareValidator ID="CompareValidator0" runat="server" ControlToValidate="txtPFECPercent"
                    Display="None" ErrorMessage="Invalid Employee pays percent." Operator="GreaterThanEqual"
                    Type="Double" ValidationGroup="AECompany" ValueToCompare="0"></asp:CompareValidator>--%>
                <asp:RangeValidator runat="server" ErrorMessage="Invalid Employee pays percent."
                    ValidationGroup="AECompany" Type="Double" ID="RangeValidator1" Display="None"
                    MinimumValue="0" MaximumValue="100" ControlToValidate="txtPFECPercent"></asp:RangeValidator>
                <asp:CustomValidator Display="None" ValidateEmptyText="true" ClientValidationFunction="ValidateTextBoxSum"
                    ControlToValidate="txtPFECPercent" ValidationGroup="AECompany" ID="valCustomRFAC12"
                    runat="server" ErrorMessage="Total pays cannot be zero or greater than 100."
                    OnServerValidate="valCustomRFAC_ServerValidate"></asp:CustomValidator>
            </td>
        </tr>
        <tr>
            <td class="fieldHeader">
                <My:Label ID="lbl2343241" Text="Effective from" runat="server" ShowAstrick="true" />
                <%--<My:Label ID="Label5" Text="IRO name:" runat="server" ShowAstrick="true" /> --%>
            </td>
            <td>
                <asp:DropDownList AppendDataBoundItems="true" Width="183px" ID="ddlEffectiveFrom"
                    runat="server" DataValueField="Key" DataTextField="Value">
                    <asp:ListItem Text="" Value="-1"></asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator122" runat="server" ControlToValidate="ddlEffectiveFrom"
                    InitialValue="-1" Display="None" ErrorMessage="Please select effective from."
                    ValidationGroup="AECompany"></asp:RequiredFieldValidator>
            </td>
        </tr>
    </table>
    <br />
</fieldset>
<div style="clear: both">
</div>
