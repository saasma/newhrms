﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using Utils.Helper;
using Utils.Web;
using System.Text;

namespace Web.UserControls
{
    public partial class MgCostCode : System.Web.UI.UserControl
    {
        CommonManager commonMgr = new CommonManager();
        List<ECostCode> source = null;

        public bool IsDisplayedAsPopup
        {
            get
            {
                if (Request.QueryString["isPopup"] != null)
                    return true;
                return false;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                Initialise();
        }

        void Initialise()
        {
            LoadCodes();
            if (ddlCostCodes.SelectedItem != null)
                txtCostCodeRename.Text = ddlCostCodes.SelectedItem.Text;
            else
                txtCostCodeRename.Text = "";
        }

        void LoadCodes()
        {
            source = CommonManager.GetAllCostCodes();
            ddlCostCodes.DataSource = source;  //payMgr.GetDepositList(SessionManager.CurrentCompanyId);
            ddlCostCodes.DataBind();
        }

        protected void btnAddDepsoit_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                ECostCode entity = new ECostCode();
                entity.CompanyId = SessionManager.CurrentCompanyId;
                entity.Name = txtNewCostCode.Text.Trim();

                commonMgr.Save(entity);
                CommonManager.ResetCache();
                txtNewCostCode.Text = "";
                Initialise();
                JavascriptHelper.DisplayClientMsg("Cost code added.", Page);
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            if (ddlCostCodes.SelectedItem != null)
            {
                ECostCode entity = new ECostCode();
                entity.CostCodeId = int.Parse(ddlCostCodes.SelectedValue);

                if (commonMgr.Delete(entity))
                {
                    JavascriptHelper.DisplayClientMsg("Cost code deleted.", Page);
                    CommonManager.ResetCache();
                }
                else
                {
                    JavascriptHelper.DisplayClientMsg("Cost code is in use.", Page);
                }
                Initialise();
            }
        }

        protected void btnRename_Click(object sender, EventArgs e)
        {
            if (Page.IsValid && ddlCostCodes.SelectedItem != null)
            {
                ECostCode entity = new ECostCode();
                entity.Name = txtCostCodeRename.Text.Trim();
                entity.CostCodeId = int.Parse(ddlCostCodes.SelectedValue);

                commonMgr.Update(entity);
                CommonManager.ResetCache();
                LoadCodes();
                UIHelper.SetSelectedInDropDown(ddlCostCodes, entity.CostCodeId.ToString());
                JavascriptHelper.DisplayClientMsg("Cost code updated.", Page);
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //output all as js array to be updatable in parent window
            if (IsDisplayedAsPopup && source != null)
            {
                //Page.ClientScript.
                StringBuilder str = new StringBuilder("");
                bool first = true;
                foreach (ECostCode obj in source)
                {
                    if (first == false)
                        str.Append(",");
                    str.Append("'" + obj.CostCodeId + "$$" + obj.Name + "'");
                    first = false;
                }
                Page.ClientScript.RegisterArrayDeclaration("texts", str.ToString());
            }
        }
    }
}