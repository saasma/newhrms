﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MGDepartment.ascx.cs"
    Inherits="Web.UserControls.MGDepartment" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<script type="text/javascript">

    //capture window closing event, must be placed as close to return values
    <%
    if( IsDisplayedFromPopup)
    {
    Response.Write("window.onunload = closePopup;");
    }
     %>
    
    

    function closePopup() {
        clearUnload();
         // alert(window.opener.parentReloadCallbackFunction)
        if ($.browser.msie == false && typeof (window.opener.parentReloadCallbackFunction) != 'undefined' && ($.browser.chrome ||  $.browser.safari)) {
            window.opener.parentReloadCallbackFunction("ReloadDepartment", window, texts);
        } else {
            if (typeof (texts) != 'undefined')
                window.returnValue = texts;
            window.close();
        }
        
    }

    function clearUnload() {
        window.onunload = null;
    }
    
      function handleDelete()
       {            
            if(confirm('Do you want to delete department?'))
            {
                clearUnload(); 
                return true;
            }
            else
                return false;
       }
          
        function popupDep(){
            var ret = popupDepDetails();
            if(typeof(ret) != 'undefined'){
                if(ret == 'Reload'){
                    __doPostBack('Reload','');
                }
            }
             return false;
        }

        function reloadDepartment(childWindow){
            childWindow.close();
            __doPostBack('Reload','');
        }              

        function popupEditDept(departmentId){
            var ret = popupDepDetails('Id=' + departmentId);

            if(typeof(ret) != 'undefined'){
                if(ret == 'Reload'){
                    __doPostBack('Reload','');
                }
            }

            return false;
        }


</script>
<div class="popupHeader" runat="server" id="div">
    <h2 class="headlinespop">
        Manage Department</h2>
</div>
<%--<asp:HiddenField ID="hiddenEmployee" runat="server" />--%>
<div class="marginal1" style='margin-top: 0px'>
    <div class="contentArea">

         <uc2:MsgCtl Width='<%# IsDisplayedFromPopup ? "600px" : "920px" %>' ID="divMsgCtl"
            EnableViewState="false" Hide="true" runat="server" />
        <uc2:WarningCtl Width='<%# IsDisplayedFromPopup ? "600px" : "920px" %>' ID="divWarningMsg"
            EnableViewState="false" Hide="true" runat="server" />

        <%--<h2>Department Information</h2>--%>
        <label class="" style='margin-right: 10px; display: none'>
            Branch</label>

        <asp:DropDownList Style='margin-bottom: 15px; display: none' onchange="clearUnload()"
            AppendDataBoundItems="true" ID="ddlBranch" runat="server" DataTextField="Name"
            DataValueField="BranchId" AutoPostBack="true" OnSelectedIndexChanged="ddlBranch_SelectedIndexChanged"
            Width="180px">
            <asp:ListItem Text="--Select Branch--" Value="-1"></asp:ListItem>
        </asp:DropDownList>
        <asp:GridView CssClass="table table-primary mb30 table-bordered table-hover" PagerStyle-HorizontalAlign="Center" PagerStyle-CssClass="defaultPagingBar"
            ID="gvwDepartments" runat="server" DataKeyNames="DepartmentId" AutoGenerateColumns="False"
            CellPadding="0" CellSpacing="0" GridLines="None" Width="100%" PageSize="100"
            AllowPaging="True" OnRowCreated="gvwEmployees_RowCreated" OnRowDeleting="gvwDepartments_RowDeleting"
            OnPageIndexChanging="gvwDepartments_PageIndexChanging">
            <Columns>
                <asp:TemplateField HeaderStyle-Width="50" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                    HeaderText="SN">
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    <ItemTemplate>
                        <%# Container.DisplayIndex + 1 %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField HeaderStyle-Width="300px" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                    DataField="Name" HeaderText="Department Name">
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                    DataField="Code" HeaderText="Code">
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundField>
                    <asp:BoundField HeaderStyle-Width="200px" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                    DataField="LocationBranch" HeaderText="Location Branch">
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundField>
                 
                <asp:BoundField HeaderStyle-Width="300px" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                    DataField="Description" HeaderText="Description">
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundField>
                <asp:TemplateField HeaderText="Edit" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="30px">
                    <ItemTemplate>
                        <asp:ImageButton ID="ImageButton1" runat="server" OnClientClick='<%# "return popupEditDept(" +  Eval("DepartmentId") + ");" %>'
                            ImageUrl="~/images/edit.gif" />
                    </ItemTemplate>
                    <HeaderStyle Width="30px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Delete" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="30px">
                    <ItemTemplate>
                        <asp:ImageButton ID="ImageButton2" OnClientClick="return handleDelete()" runat="server"
                            CommandName="Delete" ImageUrl="~/images/delet.png" />
                    </ItemTemplate>
                    <HeaderStyle Width="30px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EmptyDataTemplate>
                No Department has been created under this branch.
            </EmptyDataTemplate>
            <RowStyle CssClass="odd" />
            <AlternatingRowStyle CssClass="even" />
            <SelectedRowStyle CssClass="selected" />
        </asp:GridView>
        <div class="buttonsDivSection" style='clear: both; margin-top:10px; margin-left:0px; width: 100%!important;'>
            <%--<asp:LinkButton ID="btnAddNew" runat="server" Style='margin-left: 10px; width: 150px; height:30px;'
                CssClass="createbtns" Text="Create Department" OnClick="btnAddNew_Click" />--%>
            <asp:LinkButton ID="btnAddNew" runat="server" style="width: 150px; height:30px;"
                CssClass="btn btn-primary btn-sect btn-sm" Text="Add New Department" OnClientClick="return popupDep();" />
        </div>
       
     
    </div>
</div>
