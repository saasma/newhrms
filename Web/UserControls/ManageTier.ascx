﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ManageTier.ascx.cs" Inherits="Web.UserControls.ManageTier" %>

<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>


<style type="text/css">
table.alignttable tr td{ vertical-align:top; padding-bottom:10px;}
table.alignttable tr td input,table.alignttable tr td select{ float:left;}


</style>
<script type="text/javascript">
   


    //capture window closing event
    window.onunload = closePopup;
    function closePopup() {
        clearUnload();
        


        // alert(window.opener.parentReloadCallbackFunction)
        if ($.browser.msie == false && typeof (window.opener.parentReloadCallbackFunction) != 'undefined' && ($.browser.chrome ||  $.browser.safari)) {
            window.opener.parentReloadCallbackFunction("ReloadTier", window, texts);
        } else {
            if (typeof (texts) != 'undefined')
                window.returnValue = texts;
            window.close();
        }
        
    }

    function clearUnload() {
        window.onunload = null;
    }

    function handleDelete() {
        if (confirm('Do you want to delete the Tier?')) {
            clearUnload();
            return true;
        }
        else
            return false;
    }

</script>

<div align="left">
    <div class="marginal">
        <asp:GridView CssClass="tableLightColor" PagerStyle-HorizontalAlign="Center" PagerStyle-CssClass="defaultPagingBar"
            UseAccessibleHeader="true" ID="gvwBranches" CellPadding="3" Width="100%" runat="server"
            DataKeyNames="TierId" AutoGenerateColumns="False" GridLines="None" OnSelectedIndexChanged="gvwBranches_SelectedIndexChanged"
            AllowPaging="True" PageSize="10" OnPageIndexChanging="gvwBranches_PageIndexChanging" OnRowDeleting="gvwBranches_RowDeleting">
            <Columns>
                <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                    DataField="Name" HeaderText="Name">
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundField>
              
               
                
                <asp:TemplateField HeaderText="Edit" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="30px">
                    <ItemTemplate>
                        <asp:ImageButton ID="ImageButton1" runat="server" OnClientClick="clearUnload();"
                            CommandName="Select" ImageUrl="~/images/edit.gif" />
                    </ItemTemplate>
                    <HeaderStyle Width="30px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Delete" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="30px">
                    <ItemTemplate>
                        <asp:ImageButton ID="ImageButton2" OnClientClick="return handleDelete()" runat="server"
                            CommandName="Delete" ImageUrl="~/images/delet.png" />
                    </ItemTemplate>
                    <HeaderStyle Width="30px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:TemplateField>
            </Columns>
            <RowStyle CssClass="odd" />
            <AlternatingRowStyle CssClass="even" />
            <SelectedRowStyle CssClass="selected" />
            <PagerStyle HorizontalAlign="Center" CssClass="defaultPagingBar"></PagerStyle>
            <EmptyDataTemplate>
                No Tiers has been created.
            </EmptyDataTemplate>
        </asp:GridView>
        <div class="buttonsDiv" style='clear: both; width: 100%!important;'>
            <asp:LinkButton ID="btnAddNew" runat="server" Style='margin-left: 10px' CssClass="createbtns"
                Text="Create Tier" OnClick="btnAddNew_Click" />
        </div>
        <uc2:MsgCtl ID="divMsgCtl" Width='600px'
            EnableViewState="false" Hide="true" runat="server" />
        <uc2:WarningCtl ID="divWarningMsg" Width='600px'
            EnableViewState="false" Hide="true" runat="server" />
    
        <div id="details" runat="server" class="bevel" style="margin-top: 20px;" visible="false">
            <div class="fields paddpop" style="padding: 10px 0pt 20px 10px ! important;">
                <h2 class="popstitle">
                    Tier Information</h2>
                <table cellpadding="4px">
                    <tr>
                        <td class="fieldHeader">
                            <My:Label ID="Label2" Text="Tier Name" runat="server" ShowAstrick="true" />
                        </td>
                        <td>
                            <asp:TextBox ID="txtName" runat="server" Width="180px" />
                            <asp:RequiredFieldValidator ID="valReqdName" runat="server" ControlToValidate="txtName"
                                Display="None" ErrorMessage="Tier name is required." ValidationGroup="AEBranch"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                 
                    <tr>
                        <td>
                        </td>
                        <td>
                            <asp:Button ID="btnSave" CssClass="update" OnClientClick="valGroup='AEBranch';return CheckValidation()"
                                runat="server" Text="Save" ValidationGroup="AEBranch" OnClick="btnSave_Click" />
                            <asp:Button ID="btnCancel" OnClientClick="clearUnload()" CssClass="cancel" runat="server"
                                Text="Cancel" OnClick="btnCancel_Click" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <%--</fieldset>--%>
    </div>
</div>