﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Manager;
using Utils.Helper;
using BLL;
using DAL;
using Utils.Web;
using System.Text;

namespace Web.UserControls
{
    public partial class MGSkillSet : System.Web.UI.UserControl
    {
        CommonManager commonMgr = new CommonManager();
        List<SkillSet> source = null;
        public bool IsDisplayedAsPopup
        {
            get
            {
                if (Request.QueryString["isPopup"] != null)
                    return true;
                return false;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                Initialise();
            Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "clearUnload();");
        }

        void Initialise()
        {
            LoadDesignations();
           
        }

        void LoadDesignations()
        {
            source = commonMgr.GetAllSkillSet().OrderBy(x => x.Name).ToList();
            gvwBranches.DataSource = source;
            gvwBranches.DataBind();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                SkillSet entity = new SkillSet();

                if (gvwBranches.SelectedIndex == -1)
                {
                    entity.Name = txtName.Text.Trim();
                    

                    commonMgr.Save(entity);

                    divMsgCtl.InnerHtml = "Saved";

                }
                else
                {
                    entity.Name = txtName.Text.Trim();
                    entity.SkillSetId = (int)gvwBranches.DataKeys[gvwBranches.SelectedIndex][0];

                    commonMgr.Update(entity);

                    divMsgCtl.InnerHtml = "Updated";
                }

                divMsgCtl.Hide = false;

                
                gvwBranches.SelectedIndex = -1;
                LoadDesignations();

                ClearFields();
                
            }
        }

        protected void gvwBranches_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvwBranches.PageIndex = e.NewPageIndex;
            gvwBranches.SelectedIndex = -1;
            LoadDesignations();
            ClearFields();

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            gvwBranches.SelectedIndex = -1;
            LoadDesignations();
            ClearFields();
            details.Visible = false;
        }

        protected void gvwBranches_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            CommonManager.ResetCache();
            int id = (int)gvwBranches.DataKeys[e.RowIndex][0];
            if (id != 0)
            {
                SkillSet entity = new SkillSet();
                entity.SkillSetId = id;

                if (commonMgr.Delete(entity))
                {
                    divMsgCtl.InnerHtml = "Skill Set deleted.";
                    LoadDesignations();
                    ClearFields();
                }
                else
                {
                    divMsgCtl.InnerHtml = "Skill Set is in use.";

                }
                divMsgCtl.Hide = false;

            }
           
        }

        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            LoadDesignations();
            ClearFields();
            details.Visible = true;
            txtName.Focus();
        }

        protected void gvwBranches_SelectedIndexChanged(object sender, EventArgs e)
        {
            int selBranchId = (int)gvwBranches.DataKeys[gvwBranches.SelectedIndex][0];
            SkillSet branch = commonMgr.GetSkillSetId(selBranchId);
            if (branch != null)
            {
                txtName.Text = branch.Name;
                
                
                btnSave.Text = Resources.Messages.Update;
                details.Visible = true;
                txtName.Focus();
            }
        }

        void ClearFields()
        {
            txtName.Text = "";

            btnSave.Text = "Save";
        }

        
        protected void Page_PreRender(object sender, EventArgs e)
        {
            //output all as js array to be updatable in parent window
            if (IsDisplayedAsPopup && source != null)
            {
                //Page.ClientScript.
                StringBuilder str = new StringBuilder("");
                bool first = true;
                foreach (SkillSet obj in source)
                {
                    if (first == false)
                        str.Append(",");
                    str.Append("'" +  obj.SkillSetId + "$$" + obj.Name + "'");
                    first = false;
                }
                Page.ClientScript.RegisterArrayDeclaration("texts", str.ToString());
            }
        }
        
    }
}