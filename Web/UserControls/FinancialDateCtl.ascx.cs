﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using Utils.Calendar;
using Utils.Web;
using DAL;
using Utils.Helper;
using BLL.Base;
namespace Web.UserControls
{
    public partial class FinancialDateCtl :BaseUserControl
    {
        CommonManager mgr = new CommonManager();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialise();
            }

            if (gvwFinancialDates.HeaderRow != null)
                gvwFinancialDates.HeaderRow.TableSection = TableRowSection.TableHeader;
        }


        void LoadPayrollPeriods()
        {
            gvwFinancialDates.DataSource = mgr.GetAllFinancialDates();
            gvwFinancialDates.DataBind();

            LoadLastDates();
        }

        private void LoadLastDates()
        {
            for (int i = gvwFinancialDates.Rows.Count - 1; i >= 0; i--)
            {
                GridViewRow row = gvwFinancialDates.Rows[i];
                ImageButton btn = row.FindControl("btnEdit") as ImageButton;


                
                //starting date
                string ending = gvwFinancialDates.DataKeys[row.RowIndex][1].ToString();
                CustomDate date = CustomDate.GetCustomDateFromString(ending,
                                                                     SessionManager.CurrentCompany.IsEnglishDate);

                string nextStarting = date.IncrementByOneDay().ToString();

                calStartingDate.SetSelectedDate(nextStarting,date.IsEnglishDate.Value);

                calStartingDate.Enabled = false;


                CustomDate start = CustomDate.GetCustomDateFromString(nextStarting,IsEnglish);
                int month = start.Month-1;

                if (month == 0)
                    month = 12;
                int year = start.Year+1;
                CustomDate end = new CustomDate(
                    DateHelper.GetTotalDaysInTheMonth(year, month, IsEnglish), month, year, IsEnglish);

                calEndingDate.SetSelectedDate(end.ToString(), IsEnglish);

                btn.Visible = true;
                break;
            }
        }

        void Initialise()
        {
            if (IsEnglish)
                helpNepali.Visible = false;
            else
                helpEnglish.Visible = false;

            calStartingDate.IsEnglishCalendar = SessionManager.CurrentCompany.IsEnglishDate;
            calEndingDate.IsEnglishCalendar = SessionManager.CurrentCompany.IsEnglishDate;

            calStartingDate.IsSkipDay = true;
            calStartingDate.SelectTodayDate();
            calEndingDate.SelectTodayDate();
            calEndingDate.IsSkipDay = true;

            SetPeriod();

            legendTitle.InnerHtml = Resources.Messages.FinancialYearCreateTitle;

            //calStartDate.IsEnglishCalendar = SessionManager.CurrentCompany.IsEnglishDate;
            //calEndDate.IsEnglishCalendar = calStartDate.IsEnglishCalendar;

            //calStartDate.SelectTodayDate();
            //calEndDate.SelectTodayDate();



            CustomDate todayDate = DateManager.GetTodayDate();
            //UIHelper.SetSelectedInDropDown(ddlMonths, todayDate.Month.ToString());
            //UIHelper.SetSelectedInDropDown(ddlYears, todayDate.Year.ToString());

           


            LoadPayrollPeriods();
        }

        private void SetPeriod()
        {
            //if( ddlMonths.SelectedItem != null)
            //txtPeriodName.Text = ddlMonths.SelectedItem.Text + "-" + ddlYears.SelectedItem.Text;
        }


        protected void Page_PreRender(object sender, EventArgs e)
        {
            SetPeriod();
            PayrollPeriod last = CommonManager.GetLastPayrollPeriod();
            //if (last != null)
            //{
            //    lblLastPayrollPeriod.Text = string.Format(lblLastPayrollPeriod.Text, last.Name);
            //}
            //else
            //{
            //    lblLastPayrollPeriod.Text = string.Format(lblLastPayrollPeriod.Text, "");
            //}
        }

        bool IsMonthYearGreaterThan(CustomDate start, CustomDate end)
        {
            if (end.Year > start.Year)
                return true;
            else if (end.Year < start.Year)            
                return false;

            if (end.Month >= start.Month)
                return true;
            else if (end.Month < start.Month)
                return false;

            return true;
        }

        bool Validate()
        {
            //CustomDate startDate = CustomDate.GetCustomDateFromString(SessionManager.CurrentCompanyFinancialDate.StartingDate,
            //    SessionManager.CurrentCompany.IsEnglishDate);
            //CustomDate endDate = CustomDate.GetCustomDateFromString(SessionManager.CurrentCompanyFinancialDate.EndingDate,
            //                SessionManager.CurrentCompany.IsEnglishDate);

            //int month = int.Parse(ddlMonths.SelectedValue);
            //int year = int.Parse(ddlYears.SelectedValue);
            //CustomDate date = new CustomDate(1, month, year, SessionManager.CurrentCompany.IsEnglishDate);
            
            //// Greater than startDate
            //if (!IsMonthYearGreaterThan(startDate, date))
            //{
            //    JavascriptHelper.DisplayClientMsg("Payroll period date must lie between financial start & end date.", Page);
            //    return false;
            //}


            //if (!IsMonthYearGreaterThan(date, endDate))
            //{
            //    JavascriptHelper.DisplayClientMsg("Payroll period date must lie between financial start & end date.", Page);
            //    return false;
            //}

            //if( CommonManager.IsPayrollPeriodAlreadyExists(GetIdFromGrid(),name))
            //{
            //    Utils.Helper.JavascriptHelper.DisplayClientMsg("Payroll period already exists for the current month/year.", Page);
            //    return false;
            //}


            //// Check if new payroll period is just after the last one
            //PayrollPeriod last = mgr.GetLastPayrollPeriod();
            //if( last!= null)
            //{
            //    CustomDate lastDate = new CustomDate(1, last.Month, last.Year.Value, SessionManager.CurrentCompany.IsEnglishDate);

            //    if (!IsMonthYearGreaterThan(lastDate, date))
            //    {
            //        JavascriptHelper.DisplayClientMsg("Payroll period must be the last.", Page);
            //        return false;
            //    }
            //}

            ////Check if the last Payroll period calculation has been final saved or not
            //if (last != null && GetIdFromGrid()==0)
            //{
            //   if(! CalculationManager.IsAllEmployeeSavedFinally(last.PayrollPeriodId,false))
            //   {
            //       JavascriptHelper.DisplayClientMsg("New Payroll period can not be created as last payroll period are not final saved.", Page);
            //       return false;
            //   }
            //}
            return true;
          

        }

        public  string GetDate(object  value)
        {
            CustomDate date = CustomDate.GetCustomDateFromString(value.ToString(),
                                                                 SessionManager.CurrentCompany.IsEnglishDate);

            return date.ToStringSkipDay();
        }

        protected void btnCreate_Click(object sender, EventArgs e)
        {
           if (Validate() == false)
                return;


            FinancialDate entity = new FinancialDate();
            entity.FinancialDateId = GetIdFromGrid();
            entity.CompanyId = SessionManager.CurrentCompanyId;
          

            if( entity.FinancialDateId == 0)
            {
                entity.IsCurrent = true;
            }

            entity.StartingDate = calStartingDate.SelectedDateAsFirstDay.ToString();
            entity.EndingDate = calEndingDate.SelectedDateAsLastDay.ToString();

            if (entity.FinancialDateId != 0)
            {
                //check if financial ending date is greater than last payroll period ending date
                //else show invalid message
                PayrollPeriod payrollPeriod = CommonManager.GetLastPayrollPeriod();
                if( payrollPeriod != null)
                {
                    CustomDate date1 = CustomDate.GetCustomDateFromString(payrollPeriod.EndDate, IsEnglish);
                    CustomDate date2 = CustomDate.GetCustomDateFromString(entity.EndingDate, IsEnglish);

                    if ( DateManager.IsSecondDateGreaterThan(date1,date2)) { }
                    else if( date1.Equals(date2)) { }
                    else
                    {
                        Utils.Helper.JavascriptHelper.DisplayClientMsg(
                            Resources.Messages.FinancialUpdateFailDueToPayrollMsg, Page);
                        return;
                    }
                }

                CommonManager.UpdateFinancialDate(entity);
                divMsgCtl.InnerHtml = Resources.Messages.FinancialYearUpdatedMsg;
                divMsgCtl.Hide = false;
               // Utils.Helper.JavascriptHelper.DisplayClientMsg("Financial year updated successfully.", Page);
            }
            else
            {
                //check if all payroll period are final saved
                PayrollPeriod last = CommonManager.GetLastPayrollPeriod();
                if( last==null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.FinancialYearNoPayrollPeriodDefinedInPrevyear;
                    divWarningMsg.Hide = false;                   
                    return;
                }
                FinancialDate currentFinancialYear = CommonManager.GetCurrentFinancialYear();
                CustomDate d1 = CustomDate.GetCustomDateFromString(last.EndDate,true);
                CustomDate d2 = CustomDate.GetCustomDateFromString(currentFinancialYear.EndingDate,true);

                if( d1.ToString() != d2.ToString() )
                {
                    divWarningMsg.InnerHtml = Resources.Messages.FinancialYearAllPayrollPeriodMustBeDefinedMsg; ;
                    divWarningMsg.Hide = false;        
                    //JavascriptHelper.DisplayClientMsg("All payroll period must be defined in the financial year.", Page);
                    return;
                }

                if (!CalculationManager.IsAllEmployeeSavedFinally(last.PayrollPeriodId, false))
                {
                    divWarningMsg.InnerHtml = Resources.Messages.FinancialYearCannotBeCreatedAsLastPayrollPeriodNotFinalSavedMsg; ;
                    divWarningMsg.Hide = false;    
                    //JavascriptHelper.DisplayClientMsg("New Financial year can not be created as last payroll period are not final saved.", Page);
                    return;
                }

                if (CommonManager.CreateFinancialDate(entity))
                {
                    SendPayrollMessageAfterNewFinancialYearCreation();
                    divMsgCtl.InnerHtml = Resources.Messages.FinancialYearCreatedMsg ;
                    divMsgCtl.Hide = false;
                }
            }

            gvwFinancialDates.SelectedIndex = -1;
            LoadPayrollPeriods();
            btnCreate.Text = "Create";
            legendTitle.InnerHtml = Resources.Messages.FinancialYearCreateTitle;
            
        }

        protected void SendPayrollMessageAfterNewFinancialYearCreation()
        {
            List<string> distinctEmployee = new List<string>();

            DAL.PayrollMessage msg = new DAL.PayrollMessage();
            msg.Subject = "New Financial Year";
            msg.Body = Resources.PayrollMessages.FestivalIncomeInNewYearWarning;
            msg.SendBy = Resources.Messages.DefaultAdministratorUserName;
            msg.ReceivedDate = BLL.BaseBiz.GetCurrentDateAndTime().ToShortDateString();
            msg.ReceivedDateEng = BLL.BaseBiz.GetCurrentDateAndTime();
            msg.IsRead = false;
            distinctEmployee.Add(Page.User.Identity.Name);
            if (Resources.Messages.DefaultAdministratorUserName != Page.User.Identity.Name)
                distinctEmployee.Add(Resources.Messages.DefaultAdministratorUserName);

            PayrollMessageManager.SendMessages(msg, distinctEmployee);
        }

        int GetIdFromGrid()
        {
            if (gvwFinancialDates.SelectedIndex != -1)
            {
                return (int)gvwFinancialDates.DataKeys[gvwFinancialDates.SelectedIndex][0];
            }
            return 0;
        }

        protected void gvwPayrollPeriods_SelectedIndexChanged(object sender, EventArgs e)
        {

            int count = CommonManager.GetTotalPeriodCount();

            int financialId  = (int)gvwFinancialDates.DataKeys[gvwFinancialDates.SelectedIndex][0];
            FinancialDate date = CommonManager.GetFiancialDateById(financialId);
            if (date != null)
            {

                btnCreate.Text = Resources.Messages.Update;
                legendTitle.InnerHtml = Resources.Messages.FinancialYearUpdateTitle;
                calStartingDate.SetSelectedDate(date.StartingDate, IsEnglish);
                calEndingDate.SetSelectedDate(date.EndingDate,IsEnglish);

                this.ControlID = financialId;
                // if no of single period exists then allow to edit start date also
                if (count <= 1)
                    calStartingDate.Enabled = true;
                else
                    calStartingDate.Enabled = false;

                //this.ControlID = financialId;
                //calStartDate.SetSelectedDate(payrollPeriod.StartDate, SessionManager.CurrentCompany.IsEnglishDate);
                //calEndDate.SetSelectedDate(payrollPeriod.EndDate, SessionManager.CurrentCompany.IsEnglishDate);
            }
            //Branch branch = branchMgr.GetById(selBranchId);
            //if (branch != null)
            //{
            //    ProcessBranch(branch);
            //    btnSave.Text = Resources.Messages.Update;
            //}
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            gvwFinancialDates.SelectedIndex = -1;
            LoadPayrollPeriods();
            btnCreate.Text = "Create";
            legendTitle.InnerHtml = Resources.Messages.FinancialYearCreateTitle;
            LoadLastDates();
        }
    }
}