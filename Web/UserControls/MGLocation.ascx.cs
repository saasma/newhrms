﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Manager;
using Utils.Helper;
using BLL;
using DAL;
using Utils.Web;
using System.Text;

namespace Web.UserControls
{
    public partial class MGLocation : System.Web.UI.UserControl
    {
        CommonManager commonMgr = new CommonManager();
        List<ELocation> source = null;

        public bool IsDisplayedAsPopup
        {
            get
            {
                if (Request.QueryString["isPopup"] != null)
                    return true;
                return false;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                Initialise();

        }

        void Initialise()
        {
            LoadDesignations();
            if (ddlDeposits.SelectedItem != null)
                txtDepositRename.Text = ddlDeposits.SelectedItem.Text;
            else
                txtDepositRename.Text = "";
        }

        void LoadDesignations()
        {
            source = commonMgr.GetAllLocations();
            ddlDeposits.DataSource = source;
            ddlDeposits.DataBind();
        }

        protected void btnAddDepsoit_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                ELocation entity = new ELocation();
                entity.CompanyId = SessionManager.CurrentCompanyId;
                entity.Name = txtNewDeposit.Text.Trim();

                commonMgr.Save(entity);
                CommonManager.ResetCache();
                txtNewDeposit.Text = "";
                Initialise();
                JavascriptHelper.DisplayClientMsg("Location added.", Page);
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            if (ddlDeposits.SelectedItem != null)
            {
                ELocation entity = new ELocation();
                entity.LocationId = int.Parse(ddlDeposits.SelectedValue);

                if (commonMgr.Delete(entity))
                {
                    JavascriptHelper.DisplayClientMsg("Location deleted.", Page);
                    CommonManager.ResetCache();
                }
                else
                {
                    JavascriptHelper.DisplayClientMsg("Location is in use.", Page);
                }
                Initialise();
            }
        }

        protected void btnRename_Click(object sender, EventArgs e)
        {
            if (Page.IsValid && ddlDeposits.SelectedItem != null)
            {
                ELocation entity = new ELocation();
                entity.Name = txtDepositRename.Text.Trim();
                entity.LocationId = int.Parse(ddlDeposits.SelectedValue);

                commonMgr.Update(entity);
                CommonManager.ResetCache();
                LoadDesignations();
                UIHelper.SetSelectedInDropDown(ddlDeposits, entity.LocationId.ToString());
                JavascriptHelper.DisplayClientMsg("Location updated.", Page);
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //output all as js array to be updatable in parent window
            if (IsDisplayedAsPopup && source != null)
            {
                //Page.ClientScript.
                StringBuilder str = new StringBuilder("");
                bool first = true;
                foreach (ELocation obj in source)
                {
                    if (first == false)
                        str.Append(",");
                    str.Append("'" + obj.LocationId + "$$" + obj.Name + "'");
                    first = false;
                }
                Page.ClientScript.RegisterArrayDeclaration("texts", str.ToString());
            }
        }
        
    }
}