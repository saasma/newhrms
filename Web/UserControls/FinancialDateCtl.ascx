﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FinancialDateCtl.ascx.cs"
    Inherits="Web.UserControls.FinancialDateCtl" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<script type="text/javascript">




    function valDate() {

        return true;
    }

    function validateCP() {
        valGroup = 'AECompany';
        if (valDate()) {
            return CheckValidation();
        }
        else
            return false;
    }

    function validateDOJToDate(source, args) {
        //debugger;

        args.IsValid = isSecondCalendarCtlDateGreaterSkipDay(getCalendarSelectedDate('<%= calStartingDate.ClientID %>'),
         getCalendarSelectedDate('<%= calEndingDate.ClientID %>'));

    }

    
</script>
<style>
    .innertable td
    {
        padding-left: 0px !important;
    }
</style>
<%--<fieldset>
    <legend><b>Financial dates</b></legend>--%>
<%--   <table>
        <tr>
            <td colspan="2">--%>
<div class="contentArea">
    <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
    <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
    <div>
        <span runat="server" id="helpNepali">Please create the year starting from current year Sharwan to next year Ashadh</span><span runat="server" id="helpEnglish">
        Please create the year starting from current year July to next year June
        </span>
    </div>
    <div>
        <!-- panel-heading -->
        <div class="panel-body" style="padding-left: 0px;">
            <asp:GridView CssClass="table table-primary mb30 table-bordered table-hover" ID="gvwFinancialDates"
                runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="FinancialDateId,EndingDate"
                GridLines="None" OnSelectedIndexChanged="gvwPayrollPeriods_SelectedIndexChanged">
                <%-- <RowStyle BackColor="#EFF3FB" />--%>
                <Columns>
                    <asp:TemplateField HeaderText="Start date" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left"
                        ItemStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <%# GetDate(Eval("StartingDate")) %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="End date" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left"
                        ItemStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <%# GetDate(Eval("EndingDate")) %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Edit" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="100px">
                        <ItemTemplate>
                            <asp:ImageButton Visible="false" ID="btnEdit" runat="server" CommandName="Select"
                                ImageUrl="~/images/edit.gif" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="648px">
                    </asp:TemplateField>
                </Columns>
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                <%-- <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <EditRowStyle BackColor="#2461BF" />
                    <AlternatingRowStyle BackColor="White" />--%>
            </asp:GridView>
        </div>
    </div>
    <%--</td>
        </tr>
        <tr>
            <td colspan="2">
                <br />--%>
    <div class="clear marginTop">
        <fieldset style="width: 500px">
            <legend runat="server" id="legendTitle"></legend>
            <table style='padding: 10px' class="innertable" cellpadding="0" cellspacing="0" width="400px">
                <tr>
                    <td class="fieldHeader">
                        Start date
                    </td>
                    <td>
                        <My:Calendar runat="server" Id="calStartingDate" />
                    </td>
                </tr>
                <tr>
                    <td class="fieldHeader">
                        End date
                    </td>
                    <td>
                        <My:Calendar runat="server" Id="calEndingDate" />
                        <asp:TextBox Style='display: none' ID="txt" runat="server" />
                        <asp:CustomValidator IsCalendar="true" ID="valCustomeToDate2" runat="server" ValidateEmptyText="true"
                            ValidationGroup="G" ControlToValidate="txt" Display="None" ErrorMessage="Ending date must be greater than starting date."
                            ClientValidationFunction="validateDOJToDate" />
                    </td>
                </tr>
                <tr>
                    <td class="fieldHeader">
                        &nbsp;
                    </td>
                    <td colspan="2">
                        <asp:Button ID="btnCreate" CssClass="btn btn-primary btn-sect btn-sm" OnClientClick="valGroup = 'G';return CheckValidation();"
                            ValidationGroup="G" runat="server" Text="Create Financial Year" OnClick="btnCreate_Click" />
                        &nbsp;
                        <asp:Button ID="btnCancel" CssClass="btn btn-default btn-sect btn-sm" runat="server"
                            Text="Cancel" OnClick="btnCancel_Click" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </div>
</div>
<%--  </td>
        </tr>
    </table>--%>
<%--</fieldset>--%>
