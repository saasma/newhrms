﻿<%@ Control Language="C#" AutoEventWireup="true" EnableViewState="false" CodeBehind="EmployeeStatusUpdate.ascx.cs"
    Inherits="Web.UserControls.EmployeeStatusUpdate" %>
<script type="text/javascript">
   
  
    //var lblFromDate = '#lblFrom.ClientID';
   
    var btnPromote = '#<%=btnPromote.ClientID %>';
    var dateFrom;

    function promoteCall(statusId) {
        if (typeof (EmployeeId) != 'undefined') {
            var ret = promote("EId=" + parseInt(EmployeeId) + "&statusId=" + statusId);
            if (typeof (ret) != 'undefined') {
                if (ret == 'ReloadIncome') {
                    //refreshIncomeList(0);
                }
            }
        }
        return false;
    }




    function onStatusDeleteCallback(result) {
        if (result) {

           

            editId = 0;

            //document.getElementById(ddlStatusId).selectedIndex = 0;
            $('#<%= empStatusList.ClientID %>').html(result);

         


        }
        hideLoading();
    }

    function loadAndHide(win) {
        win.close();
        showLoading();
        Web.PayrollService.GetStatusList(parseInt(EmployeeId), onStatusDeleteCallback);
        
    }



   

    function enableChangeCallback(chk) {

        var enableAll = true;
        //if first salary already generated then only enable to date
        if (typeof (hasSalaryGenerated) != 'undefined' && hasSalaryGenerated == true) {
            enableAll = false;
        }

        if (chk.checked) {
            disableElement(ddlJoiningStatusId);
            disableElement(calDOJFrom + "_date");
            disableElement(calDOJTo + "_date");
            disableElement(chkDOJToDefined);
            $(btnPromote).show();
            //disableElement(calDOJTo + "_date");
            //ValidatorEnable(document.getElementById(valDOJ), false);
        }
        else {
            if (enableAll) {
                enableElement(ddlJoiningStatusId);
                enableElement(calDOJFrom + "_date");
                enableElement(calDOJTo + "_date");
                enableElement(chkDOJToDefined);
                enableElement(btnPromote);
                
                $(btnPromote).hide();
            }
            //enableElement(calDOJTo + "_date");
            //ValidatorEnable(document.getElementById(valDOJ), true);
        }


    }

  


    var editId = 0;


    function deleteStatus(statusId) {
        if (confirm("Confirm delete status?")) {

            showLoading();
            Web.PayrollService.DeleteEmployeeStatus(statusId, onStatusDeleteCallback);


        }
        return false;
    }
    function elementsEnabled() {
       
        return true;
    }
</script>
<style>
    .statusWidth
    {
        width: 143px;
    }
    .removePadding td, .removePadding table td
    {
        padding: 2px !important;
    }
</style>
<asp:Panel runat="server"  Style='padding-left: 20px'>
    <h2>
        
        Status Change</h2>
    <div class="removePadding" id="empStatusList" runat="server">
    </div>
   <%-- <table class="removePadding" runat="server" id="tbl" width="480px" cellpadding="0"
        cellspacing="0">
        <tr>
            <td colspan="2">
                <asp:DropDownList CssClass='statusWidth' ID="ddlStatus" runat="server" AppendDataBoundItems="true"
                    Width="120px">
                    <asp:ListItem Selected="true" Value="-1" Text="--Status--"></asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator InitialValue="-1" ID="valReqdName1" runat="server" ControlToValidate="ddlStatus"
                    Display="None" ErrorMessage="Please select a status." ValidationGroup="AEStatus"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                <b style="padding-right: 6px;">From</b>
            </td>
            <td>
                <b style="padding-right: 50px;">Up to</b>
                <asp:CheckBox ID="chkDOJToNotDefined" ToolTip="Define up to date or not" Text="Define Up To"
                    runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hdnFromDate" runat="server" />
                <My:Calendar Id="calFrom" runat="server" />
                <asp:CustomValidator ValidateEmptyText="true" ID="valCustomeToDate1" runat="server"
                    ControlToValidate="ddlStatus" ValidationGroup="AEStatus" Display="None" ErrorMessage=""
                    ClientValidationFunction="validateStatusToDate" />
            </td>
            <td>
                <My:Calendar Id="calTo" runat="server" Enabled="false" />
            </td>
        </tr>
    </table>
    <br />
    <asp:Button ID="btnSaveStatus" runat="server" OnClientClick="if(!elementsEnabled()) return false; valGroup='AEStatus';if( CheckValidation() ) onStatusSave();return false;"
        Text="Save Status" class="update" />--%>
     <asp:Button ID="btnPromote" Width="100px" CssClass="save" style="margin-top:10px" runat="server" OnClientClick="return promoteCall(0);"
        Text="Change Status" />
</asp:Panel>
