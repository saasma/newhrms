﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MGDeduction.ascx.cs"
    Inherits="Web.UserControls.MGDeduction" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>

<script type="text/javascript">

    function popupDeductionCall() {

        if (typeof (EmployeeId) != 'undefined') {
            var ret = popupDeduction("EId=" + parseInt(EmployeeId));
            if (typeof (ret) != 'undefined') {
                if (ret == 'ReloadDeduction') {
                    refreshDeductionList(0);
                }
            }
        }
        return false;
    }

    function reloadDeduction(closingWindow) {
        closingWindow.close();
        refreshDeductionList(0);
    }

        function popupUpdateDeductionCall(deductionId) {
        
            var ret = popupUpdateDeduction('Id=' + deductionId);
            if (typeof (ret) != 'undefined') {
                if (ret == 'Reload') {
                    refreshDeductionList(0);
                }
            }
            return false;
        }
        //call to delete income
        function deleteDeductionCall(deductionId) {
            if (confirm("Do you want to delete the deduction for the employee?")) {
                refreshDeductionList(deductionId);
            }
            return false;
        }

        function refreshDeductionList(deductionId) {
            if (typeof (EmployeeId) != 'undefined') {
                showLoading();
                Web.PayrollService.GetDeductionList(deductionId,parseInt(EmployeeId), refreshDeductionListCallback);
            }
        }

        function refreshDeductionListCallback(result) {
            if (result) {
                document.getElementById('<%=deductions.ClientID %>').innerHTML
                = result;
            }
            hideLoading();
        }
</script>

<div class="employeSectors" style='margin-right: 0; width: 500px;padding-bottom:15px'>
    <h2>
        Deductions</h2>
    <div id="deductions" runat="server">
    </div>
    <asp:Button ID="btnAddDeduction" runat="server" CssClass="addbtns" Enabled="false"
        OnClientClick="return popupDeductionCall();" Text="Add" />
</div>
