﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PastCalculationAdjCtl.ascx.cs"
    Inherits="Web.UserControls.PastCalculationAdjCtl" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/ErrorMsgCtl.ascx" TagName="ErrorMsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="InfoMsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<%@ Register TagPrefix="uc3" Namespace="Web.Controls" %>
<style type="text/css">
    th
    {
        text-align: center !important; /*border: 1px solid #DDDDDD !important;
        border-bottom: 0px !important;*/
        background-color: #B6DBFF !important;
        overflow: hidden;
    }
    .tableLightColor th
    {
        padding: 2px 2px !important;
        vertical-align: middle;
        padding-left: 1px !important;
        border-top: 1px solid #FFF !important;
        border-left: 1px solid #FFF !important;
        border-right: 1px solid #FFF !important;
        border-bottom: 1px solid #FFF !important;
    }
    .tableLightColor td span
    {
        padding: 2px 2px !important;
    }
    .tableLightColor
    {
        font-size: 11px;
    }
    td
    {
        padding: 0px !important;
        border: 0px !important;
        margin: 0px !important;
    }
    .odd
    {
        border-top: 1px solid #DDDDDD !important;
        border-bottom: 1px solid #DDDDDD !important;
    }
    .calculationInput
    {
        background-color: #CCFF9A;
        border-top: 0px !important;
        border-left: 0px !important;
        border-bottom: 0px solid #DDDDDD !important;
        border-right: 1px solid #DDDDDD !important;
    }
    
    .nameHeaderClass
    {
        text-align: left !important;
        padding-left: 6px !important;
    }
    .employeeClass
    {
    	color:Black;
    }
    .employeeClass:hover{color:Black!important;text-decoration:none!important;;}
</style>
<script type="text/javascript">

    function showLoadingContent(element) {

        //alert($('#' + element.id).prop('disabled'));
        if ($('#' + element.id).prop('disabled') == 'disabled')
            return;


        $('#loadingStartingImage').show();
        $('#wrapperDiv').hide();
        $('#' + buttonsDiv + '').hide();
    }


    var buttonsDiv = '<%= pnlButtons.ClientID %>';

    $(document).ready(
            function () {
                //			 
                $('.tableLightColor tbody').attr('id', 'scrollMe');
                var sth = new ScrollTableHeader();
                sth.dynamicRowIndex = 1;
                //			 
                sth.addTbody("scrollMe");
                //                sth.delayAfterScroll = 150;
                //                sth.minTableRows  = 10;

                $('#loadingStartingImage').hide();
                $('#wrapperDiv').show();

                //  $(':text').('.adjustmentIncome').focus(function () { selectAllText($(this)) });

            }
			);

             function ACE_item_selected(source, eventArgs) {
                 var value = eventArgs.get_value();
                
                 //window.location = "Employee.aspx?Id=" + value; 
                 //alert(value);
                 document.getElementById('<%= hiddenEmployeeId.ClientID%>').value = value;

             }
    function setCurrency(value) {
        if (value != "")
            document.getElementById('<%= currencyValue.ClientID %>').innerHTML = value;
    }

    function importPopupProcess(btn) {


        //            if ($('#' + btn.id).attr('disabled'))
        //                return false;

        var payrollPeriodId = document.getElementById('<%= ddlPayrollPeriods.ClientID %>').value;
       
        // if (IsProjectInputType)
        if (payrollPeriodId == 0)
            return false;

        var ret = importPopup("payrollPeriod=" + payrollPeriodId );


        return false;
    }

    
</script>
<img src='<%= Page.ResolveUrl("~/images/saving.gif") %>' id="loadingStartingImage"
    alt="" class="loadingStartingImage" />
<div class="contentArea">
    <div style='margin-bottom: 5px'>
        <h2>
            Adjust Past Salary</h2>
    </div>
    <div class="attribute">
        <strong>Payroll period </strong>
        <asp:HiddenField ID="hiddenEmployeeId" runat="server" />
        <asp:DropDownList Width="140px" onchange="showLoadingContent(this)" AppendDataBoundItems="true"
            DataTextField="Name" DataValueField="PayrollPeriodId" ID="ddlPayrollPeriods"
            runat="server" AutoPostBack="True">
        </asp:DropDownList>
        <asp:TextBox ID="txtEmpSearch" runat="server" OnTextChanged="txtEmpSearch_TextChanged"
            AutoPostBack="true" Width="160" />
        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtEmpSearch"
            WatermarkText="Search Employee" WatermarkCssClass="searchBoxText" />
        <cc1:AutoCompleteExtender EnableCaching="true" ID="AutoCompleteExtenderOrganization"
            runat="server" MinimumPrefixLength="2" ServiceMethod="GetEmployeeNames" ServicePath="~/PayrollService.asmx"
            TargetControlID="txtEmpSearch" CompletionSetCount="10" CompletionInterval="250"
            CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList"
            CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
        </cc1:AutoCompleteExtender>
         <span style="margin-left: 10px"/> 
          <asp:LinkButton ID="LinkButton1" Style='text-decoration: underline' runat="server"
                            Text="Import" OnClientClick="importPopupProcess();return false;" />
         <asp:Button ID="btnLoad" Visible="false" CssClass="update" OnClick="btnSave_Click" runat="server"
            Text="Save" />
            <div style='display: inline'>
            <asp:LinkButton ID="btnExport" runat="server" Text="Export to Excel" OnClick="btnExport_Click"
                CssClass=" excel marginRight" Style='float: right; margin-right: 0px' />
        </div>
    </div>
    <uc2:MsgCtl ID="msgCtl" EnableViewState="false" Hide="true" runat="server" />
    <uc2:InfoMsgCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
    <uc2:ErrorMsgCtl Hide="true" ID="divErrorMsg" ErrorMessage="Values are not properly set so please resolve to proceed forward."
        style='display: none' runat="server" />
</div>
<span runat="server" id="currencyValue" style='float: right; padding: 4px; margin-bottom: 3px;
    background-color: #9cdaf9'></span><a href="javascript:void(0)" onclick="currencyPopup();return false;"
        runat="server" id="linkCurrencyRate" style='float: right; padding: 4px; padding-right: 10px'>
        Exchange Rate</a> <a href="javascript:void(0)" onclick="currencyHistoryPopup();return false;"
            runat="server" id="linkCurrencyHistory" style='float: right; padding: 4px; padding-right: 10px'>
            Exchange History</a>
<div style='display: block; clear: both; margin-left: 10px;' runat="server" id="checkListSection">
    <strong>
        <asp:HyperLink runat="server" ID="checkListLink" Target="_blank" Style='color: red'
            NavigateUrl='~/CP/Extension/ManageCheckList.aspx' />
    </strong>
</div>
<div style='clear: both'>
</div>
<div id="wrapperDiv" style="overflow: auto; overflow-y: hidden; display: none; margin-left: 10px">
    <cc2:EmptyDisplayGridView ID="gvw" CssClass="tableLightColor" UseAccessibleHeader="true"
        runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="CalculationId,EmployeeId,IsRetiredOrResigned,IsFinalSaved"
        GridLines="None" ShowFooterWhenEmpty="False" ShowHeaderWhenEmpty="True" EnableViewState="false"
        OnRowCreated="gvw_RowCreated" OnDataBound="gvw_DataBound">
        <Columns>
            <asp:TemplateField HeaderText="EIN" HeaderStyle-BackColor="#B6DBFF" HeaderStyle-Width="42px"
                ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <asp:Label ID="lblId" Width="40px" runat="server" Text='<%# Eval("EmployeeId") %>'></asp:Label>
                    <asp:HiddenField ID="incomeListId" runat="server" Value="" />
                    <asp:HiddenField ID="deductionListId" runat="server" Value="" />
                    <%-- Don't include any value,instead use DataKeyNames--%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderStyle-BackColor="#1B93D0" HeaderStyle-CssClass="nameHeaderClass"
                HeaderText="&nbsp;Name">
                <ItemTemplate>
                    <%-- <asp:Label Width="150px" ID="Label2" Style='border-right: 1px solid #DDDDDD' runat="server"
                        Text='<%# "&nbsp;" + Eval("Name") %>'></asp:Label>--%>
                    <asp:HyperLink CssClass="employeeClass" Target="_blank" Width="150px" ID="Label2"
                        Style='border-right: 1px solid #DDDDDD' runat="server" NavigateUrl='<%# "~/CP/Employee.aspx?Id=" +  Eval("EmployeeId") %>'
                        Text='<%# "&nbsp;" + Eval("Name") %>'></asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Delete" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                <HeaderTemplate>
                    <img alt="Delete" title="Delete" src="../images/delet.png" />
                    <br />
                    <asp:CheckBox ID="chkDelete" onclick="selectDeselectAll(this)" runat="server" />
                </HeaderTemplate>
                <ItemTemplate>
                    <%-- <asp:Button ID="btnEdit" runat="server" Text="Edit" CommandName="Edit" Visible='<%# Convert.ToInt32(Eval("CalculationId")) != 0 %>' />
                
                <asp:Button ID="btnUpdate" runat="server" Text="Update" CommandName="Update" Visible='False' />--%>
                    <asp:CheckBox ID="chkDelete" ToolTip='<%# Convert.ToBoolean( Eval("IsFinalSaved")) == true ? "Salary already saved." : "" %>'
                        Enabled='<%# !Convert.ToBoolean( Eval("IsFinalSaved")) %>' Visible='<%# Convert.ToInt32(Eval("CalculationId"))!=0 %>'
                        runat="server" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <RowStyle CssClass="odd" />
        <AlternatingRowStyle CssClass="even" />
        <EmptyDataTemplate>
            <b>No salary records.</b>
        </EmptyDataTemplate>
    </cc2:EmptyDisplayGridView>
</div>
<div style='padding-left: 10px'>
    <uc1:PagingCtl ID="pagingCtl" ShowDropDown="true" OnChangePage="ChangePageNumber"
        PageSizeChangedJS="showLoadingContent(this);" OnNextRecord="btnNext_Click" OnPrevRecord="btnPrevious_Click"
        OnDropDownShowRecordsChanged="ddlRecords_SelectedIndexChanged" runat="server" />
</div>
<asp:Panel ID="pnlButtons" CssClass="clear buttonsDiv" Style='text-align: left; width: 1188px'
    runat="server">
    <div style='text-align: right; float: right'>
        <asp:Button ID="btnSave" CssClass="save" OnClientClick="if(confirm('Are you sure, you want to change the salary?')==false) return false; else {showLoadingContent(this);}"
            runat="server" Text="Save" OnClick="btnSave_Click" />
    </div>
</asp:Panel>
