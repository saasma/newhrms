﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Manager;
using BLL.Base;
using BLL;
using Utils;
using Utils.Security;
using Utils.Helper;
using DAL;
using Utils.Calendar;
using Ext.Net;

namespace Web.UserControls
{
    public partial class ManualAttendanceCtl : BaseUserControl
    {
       
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack && !X.IsAjaxRequest)
            {
                SetDate();

                if (IsEnglish)
                    lblDate.Text = CustomDate.GetTodayDate(IsEnglish).EnglishDate.ToShortDateString();
                else
                    lblDate.Text = CustomDate.GetTodayDate(IsEnglish).ToString();

                lblTime.Text = DateTime.Now.ToShortTimeString();


                //if (IsEnglish)
                //    lblCurrentDateTIme1.Text = CustomDate.GetTodayDate(IsEnglish).EnglishDate.ToShortDateString() + " ";
                //else
                //    lblCurrentDateTIme1.Text = CustomDate.GetTodayDate(IsEnglish).ToString() + " ";

                Hidden_lblCurrentDateTIme1.Text = DateTime.Now.ToShortTimeString();


                GetEmployeeTimeResult time = new GetEmployeeTimeResult(); //AttendanceManager.GetEmployeeTime(SessionManager.CurrentLoggedInEmployeeId);
                if (time != null)
                {
                    if (DateTime.Now.ToString("tt") == "PM")
                        ddlCheckInOut.SelectedIndex = 1;
                }

                
                //InNoteGridPanel.Hidden = true;
                btnCheckIn.Hidden = true;
                btn1.Hidden = true;

                //OutNoteGridPanel.Hidden = true;
                //btnCheckOut.Hidden = true;
                //btn2.Hidden = true;

                DAL.AttendanceMapping map = BLL.BaseBiz.PayrollDataContext.AttendanceMappings
                        .SingleOrDefault(x => x.EmployeeId == SessionManager.CurrentLoggedInEmployeeId);

                AttendanceGetInAndOutTimeResult tempInst = new AttendanceGetInAndOutTimeResult();
                AttendanceCheckInCheckOut CheckIn = new AttendanceCheckInCheckOut();
                AttendanceCheckInCheckOut CheckOut = new AttendanceCheckInCheckOut();

                if (map != null)
                {

                    //passed employee id after asking
                    tempInst = AttendanceManager.getAttendanceofGivenDate(SessionManager.CurrentLoggedInEmployeeId, CommonManager.GetCurrentDateAndTime());

                    if (tempInst != null)
                    {
                        if (tempInst.InID != null)
                        {
                            CheckIn = AttendanceManager.getCheckINOutByID(tempInst.InID.ToString());
                        }

                        if (tempInst.OutID != null)
                        {
                            CheckOut = AttendanceManager.getCheckINOutByID(tempInst.OutID.ToString());
                        }



                        if (CheckIn == null || CheckIn.ChkInOutID == null || CheckIn.ChkInOutID == Guid.Empty)
                        {
                            //InNoteGridPanel.Hidden = true;
                            btnCheckIn.Hidden = false;
                            btn1.Hidden = false;

                        }
                        else
                        {
                            //InNoteGridPanel.Hidden = false;
                            btnCheckIn.Hidden = true;
                            btn1.Hidden = true;
                            ColumnIN.Text = "Clocked In " + CheckIn.DateTime.ToShortTimeString();
                            Hidden_CIN.Text = CheckIn.ChkInOutID.ToString();

                            RefreshInComment(CheckIn.DateTime, CheckIn.InOutMode, SessionManager.CurrentLoggedInEmployeeId);


                        }



                        if (CheckOut == null || CheckOut.ChkInOutID == null || CheckOut.ChkInOutID == Guid.Empty)
                        {
                            //OutNoteGridPanel.Hidden = true;
                            if (CheckIn != null && CheckIn.ChkInOutID != null && CheckIn.ChkInOutID != Guid.Empty)
                            {
                                //btnCheckOut.Hidden = false;
                                //btn2.Hidden = false;
                            }
                        }
                        else
                        {

                            //btnCheckOut.Hidden = true;
                            //btn2.Hidden = true;

                            //OutNoteGridPanel.Hidden = false;

                            ColumnOut.Text = "Clocked Out " + CheckOut.DateTime.ToShortTimeString();
                            Hidden_COUT.Text = CheckOut.ChkInOutID.ToString();

                            RefreshInComment(CheckOut.DateTime, CheckOut.InOutMode, SessionManager.CurrentLoggedInEmployeeId);

                            
                        }
                    }


                }
                else
                {
                    NewMessage.ShowWarningMessage("Attendance mapping has not be set, so please ask HR to configure for Attendance.");
                }

                

                // if no Check/Clock in then hide Clock out
                if (CheckIn == null || CheckIn.ChkInOutID == null || CheckIn.ChkInOutID == Guid.Empty)
                {
                    btn2.Hidden = true;
                    btnCheckOut.Hidden = true;
                }


                if (SessionManager.CurrentLoggedInEmployeeId != 0)// && AttendanceManager.IsAttendanceDeviceIntegrated())
                {
                    bool? isManualAtte = attendanceManager.IsManualAttendancePossible(SessionManager.CurrentLoggedInEmployeeId);
                        //EmployeeManager.GetEmployeeById(SessionManager.CurrentLoggedInEmployeeId).Branch.IsManualAttendance;
                    if (isManualAtte != null && isManualAtte.Value == false)
                    {
                        btn1.Hidden = true;
                        btn2.Hidden = true;
                        btnCheckIn.Hidden = true;
                        btnCheckOut.Hidden = true;

                    }
                }

                cmbLastXDays.SelectedItem.Index = 0;
                LoadReport(7);
            }
            

        }


        void SetDate()
        {
            try
            {
                DateTime date = BLL.BaseBiz.GetCurrentDateAndTime();
                //set dates
                lblCurrentDateTIme1.Text = date.DayOfWeek.ToString();

                lblCurrentDateTIme1.Text += " " + date.ToString("dd MMMM, yyyy");


                
                lblCurrentTIme1.Text = DateTime.Now.ToString("hh:mm tt");
            }
            catch (Exception exp1) { }
        }

        protected void RefreshInComment(DateTime date, int InOutMode, int EmpID)
        {
            List<AttendanceCheckInCheckOutComment> comms = new List<AttendanceCheckInCheckOutComment>();
            comms = AttendanceManager.getAllCommentsNew(date, InOutMode, EmpID);
           
            if (InOutMode == (int)ChkINOUTEnum.ChkIn)
            {
                storeInNote.DataSource = comms;
                storeInNote.DataBind();
            }
            else
            {

                
                storeOutNote.DataSource = comms;
                storeOutNote.DataBind();
            }
        }
      
        protected void btnChange_Click(object sender, EventArgs e)
        {
            DAL.AttendanceMapping map = BLL.BaseBiz.PayrollDataContext.AttendanceMappings
                    .SingleOrDefault(x => x.EmployeeId == SessionManager.CurrentLoggedInEmployeeId);

            if (map == null)
            {
                WarningCtl.InnerHtml = "Mapping does not exists.";
                WarningCtl.Hide = false;
                return;
            }

            AttendanceCheckInCheckOut chkInOut = new AttendanceCheckInCheckOut();
            chkInOut.ChkInOutID = Guid.NewGuid();
            chkInOut.DeviceID = map.DeviceId;
            chkInOut.AuthenticationType = (byte)TimeAttendanceAuthenticationType.SelfManualEntryFromSoftware;
            chkInOut.InOutMode = byte.Parse(ddlCheckInOut.SelectedValue);
            chkInOut.IPAddress = HttpContext.Current.Request.UserHostAddress;
            chkInOut.ManualVisibleTime = lblTime.Text.Trim();

            DateTime currentDate = DateTime.Now; //Utils.Helper.Util.GetCurrentDateAndTime();


            //string actualDate = TempEngDate.Text;
            //string actualTime = txtTime.Text;

            DateTime chkinoutDate = currentDate;
            DateTime chkinoutTime = currentDate;

            chkInOut.DateTime = new DateTime(chkinoutDate.Year, chkinoutDate.Month, chkinoutDate.Day, chkinoutTime.Hour, chkinoutTime.Minute, chkinoutTime.Second);
            chkInOut.Date = chkInOut.DateTime.Date;
            //chkInOut.ModifiedBy = SessionManager.User.UserID;
            //chkInOut.ModifiedOn = currentDate;
            chkInOut.ModificationRemarks = "Manual Attendance";

            AttendanceManager.SaveOrUpdateAttendanceException(chkInOut);


            msgCtl.InnerHtml = "Attendance saved.";
            msgCtl.Hide = false;
               
        }

        protected void btnCheckIn_Click(object sender, DirectEventArgs e)
        {

            DAL.AttendanceMapping map = BLL.BaseBiz.PayrollDataContext.AttendanceMappings
                   .SingleOrDefault(x => x.EmployeeId == SessionManager.CurrentLoggedInEmployeeId);

            if (map == null)
            {
                //WarningCtl.InnerHtml = "Mapping does not exists.";
                //WarningCtl.Hide = false;
                //X.Msg.Alert("", "Mapping does not exists.").Show();
                Notification.Show(new NotificationConfig
                {
                    Title = "",
                    Icon = Icon.Information,
                    Html = "Mapping does not exists"
                });
                return;
            }

            AttendanceCheckInCheckOut chkInOut = new AttendanceCheckInCheckOut();
            chkInOut.ChkInOutID = Guid.NewGuid();
            chkInOut.DeviceID = map.DeviceId;
            chkInOut.AuthenticationType = (byte)TimeAttendanceAuthenticationType.SelfManualEntryFromSoftware; ;
            chkInOut.InOutMode = (int)ChkINOUTEnum.ChkIn;
            chkInOut.IPAddress = HttpContext.Current.Request.UserHostAddress;
            chkInOut.ManualVisibleTime = DateTime.Parse(Hidden_lblCurrentDateTIme1.Text).ToShortTimeString(); //lblTime.Text.Trim();

            DateTime currentDate = DateTime.Now; //Utils.Helper.Util.GetCurrentDateAndTime();


            //string actualDate = TempEngDate.Text;
            //string actualTime = txtTime.Text;

            DateTime chkinoutDate = currentDate;
            DateTime chkinoutTime = currentDate;

            chkInOut.DateTime = new DateTime(chkinoutDate.Year, chkinoutDate.Month, chkinoutDate.Day, chkinoutTime.Hour, chkinoutTime.Minute, chkinoutTime.Second);
            chkInOut.Date = chkInOut.DateTime.Date;
            //chkInOut.ModifiedBy = SessionManager.User.UserID;
            //chkInOut.ModifiedOn = currentDate;
            chkInOut.ModificationRemarks = "Manual Attendance";
            int xDays = 7;
            Status status = AttendanceManager.InsertUpdateCheckIN(chkInOut);
            if (status.IsSuccess)
            {

                string selectedValue = cmbLastXDays.SelectedItem.Value;
                
                if (!string.IsNullOrEmpty(selectedValue))
                {
                    xDays = int.Parse(selectedValue);
                }
                LoadReport(xDays);

                //X.Msg.Alert("", "Clock In Saved").Show();
                Notification.Show(new NotificationConfig
                {
                    Title = "",
                    Icon = Icon.Information,
                    Html = "Clock In Saved"
                });
                InNoteGridPanel.Hidden = false;
                btnCheckIn.Hidden = true;
                btn1.Hidden = true;
                ColumnIN.Text = "Clocked In " + chkInOut.DateTime.ToShortTimeString();
                Hidden_CIN.Text = chkInOut.ChkInOutID.ToString();

                RefreshInComment(chkInOut.DateTime, chkInOut.InOutMode, SessionManager.CurrentLoggedInEmployeeId);
               

                AttendanceCheckInCheckOut CheckOut = new AttendanceCheckInCheckOut();

                CheckOut = AttendanceManager.GetCheckInOutTimeForComment(map.DeviceId.ToString(), CommonManager.GetCurrentDateAndTime(), 1);

                
                    if (CheckOut == null)
                    {
                        OutNoteGridPanel.Hidden = true;
                        //btnCheckOut.Hidden = false;
                        //btn2.Hidden = false;
                    }
                    else
                    {

                        //btnCheckOut.Hidden = true;
                        //btn2.Hidden = true;

                        OutNoteGridPanel.Hidden = false;
                        
                        ColumnOut.Text = "Clocked Out " + CheckOut.DateTime.ToShortTimeString();
                        Hidden_COUT.Text = CheckOut.ChkInOutID.ToString();

                        RefreshInComment(CheckOut.DateTime, CheckOut.InOutMode, SessionManager.CurrentLoggedInEmployeeId);
                       

                        
                    }
                


            }
            else
            {
 
            }
            storeMYTimesheet.DataBind();
            LoadReport(xDays);

            btn2.Hidden = false;
            btnCheckOut.Hidden = false;

            if(NewHRManager.RedirectToEmployeePlan())
                Response.Redirect("~/Employee/EmpPlan.aspx");

        }

        protected void btnCheckOut_Click(object sender, DirectEventArgs e)
        {
            DAL.AttendanceMapping map = BLL.BaseBiz.PayrollDataContext.AttendanceMappings
                   .SingleOrDefault(x => x.EmployeeId == SessionManager.CurrentLoggedInEmployeeId);

            if (map == null)
            {
                //WarningCtl.InnerHtml = "Mapping does not exists.";
                //WarningCtl.Hide = false;
                //X.Msg.Alert("", "Mapping does not exists.").Show();
                Notification.Show(new NotificationConfig
                {
                    Title = "",
                    Icon = Icon.Information,
                    Html = "Mapping does not exists."
                });

                return;
            }

            AttendanceCheckInCheckOut chkInOut = new AttendanceCheckInCheckOut();
            chkInOut.ChkInOutID = Guid.NewGuid();
            chkInOut.DeviceID = map.DeviceId;
            chkInOut.AuthenticationType = (byte)TimeAttendanceAuthenticationType.SelfManualEntryFromSoftware; ;
            chkInOut.InOutMode = (int)ChkINOUTEnum.ChkOut;
            chkInOut.IPAddress = HttpContext.Current.Request.UserHostAddress;
            chkInOut.ManualVisibleTime = DateTime.Parse(Hidden_lblCurrentDateTIme1.Text).ToShortTimeString();//lblTime.Text.Trim();

            DateTime currentDate = DateTime.Now; //Utils.Helper.Util.GetCurrentDateAndTime();


            //string actualDate = TempEngDate.Text;
            //string actualTime = txtTime.Text;

            DateTime chkinoutDate = currentDate;
            DateTime chkinoutTime = currentDate;

            chkInOut.DateTime = new DateTime(chkinoutDate.Year, chkinoutDate.Month, chkinoutDate.Day, chkinoutTime.Hour, chkinoutTime.Minute, chkinoutTime.Second);
            chkInOut.Date = chkInOut.DateTime.Date;
            //chkInOut.ModifiedBy = SessionManager.User.UserID;
            //chkInOut.ModifiedOn = currentDate;
            chkInOut.ModificationRemarks = "Manual Attendance";
            int xDays = 7;
            Status status = AttendanceManager.InsertUpdateCheckOut(chkInOut);
            if (status.IsSuccess)
            {
                string selectedValue = cmbLastXDays.SelectedItem.Value;
                
                if (!string.IsNullOrEmpty(selectedValue))
                {
                    xDays = int.Parse(selectedValue);
                }
                LoadReport(xDays);

                //X.Msg.Alert("", "Clock Out Saved").Show();
                Notification.Show(new NotificationConfig
                {
                    Title = "",
                    Icon = Icon.Information,
                    Html = "Clock Out Saved"
                });
                OutNoteGridPanel.Hidden = false;
                
                ColumnOut.Text = "Clocked Out " + chkInOut.DateTime.ToShortTimeString();
                Hidden_COUT.Text = chkInOut.ChkInOutID.ToString();


                RefreshInComment(chkInOut.DateTime, chkInOut.InOutMode, SessionManager.CurrentLoggedInEmployeeId);
                

                
            }
            storeMYTimesheet.DataBind();
            LoadReport(xDays);
            //msgCtl.InnerHtml = "Attendance saved.";
            //msgCtl.Hide = false;
        }

        protected void CheckInCommentNew_Click(object sender, DirectEventArgs e)
        {
            Ext.Net.Button btn = (Ext.Net.Button)sender;


            AttendanceCheckInCheckOutComment comment = new AttendanceCheckInCheckOutComment();
            bool isEdit = false;

            comment.AttendanceDate = DateTime.Parse(Hidden_SelectedDate.Text);

            if (btn.ID == "btnComment1")
            {
                comment.InOutMode = (int)ChkINOUTEnum.ChkIn;
                comment.Note = txtNote1.Text;

            }
            else
            {
                comment.InOutMode = (int)ChkINOUTEnum.ChkOut;
                comment.Note = txtNote2.Text;
            }

            comment.EmpID = SessionManager.CurrentLoggedInEmployeeId;

            Status status = AttendanceManager.InsertUpdateAttendanceComment(comment);
            if (status.IsSuccess)
            {
                X.Msg.Alert("", "Note Saved").Show();
                RefreshInComment(DateTime.Parse(Hidden_SelectedDate.Text), comment.InOutMode.Value, comment.EmpID.Value);
            }
            else
            {
                X.Msg.Alert("", status.ErrorMessage).Show();
            }

        }

        // from page
        protected void CheckInComment_Click(object sender, DirectEventArgs e)
        {


            AttendanceCheckInCheckOutComment comment = new AttendanceCheckInCheckOutComment();

            comment.AttendanceDate = DateTime.Now.Date;


            comment.InOutMode = (int)ChkINOUTEnum.ChkIn;
            comment.Note = txtInNote.Text;


            comment.EmpID = SessionManager.CurrentLoggedInEmployeeId;

            Status status = AttendanceManager.InsertUpdateAttendanceComment(comment);
            if (status.IsSuccess)
            {
                X.Msg.Alert("", "Note Saved").Show();
                RefreshInComment(comment.AttendanceDate.Value, comment.InOutMode.Value, comment.EmpID.Value);
            }
            else
            {
                X.Msg.Alert("", status.ErrorMessage).Show();
            }
        }

        protected void CheckOutComment_Click(object sender, DirectEventArgs e)
        {
            AttendanceCheckInCheckOutComment comment = new AttendanceCheckInCheckOutComment();

            comment.AttendanceDate = DateTime.Now.Date;


            comment.InOutMode = (int)ChkINOUTEnum.ChkOut;
            comment.Note = txtOutNote.Text;


            comment.EmpID = SessionManager.CurrentLoggedInEmployeeId;

            Status status = AttendanceManager.InsertUpdateAttendanceComment(comment);
            if (status.IsSuccess)
            {
                X.Msg.Alert("", "Note Saved").Show();
                RefreshInComment(comment.AttendanceDate.Value, comment.InOutMode.Value, comment.EmpID.Value);
            }
            else
            {
                X.Msg.Alert("", status.ErrorMessage).Show();
            }
        }





        AttendanceManager attendanceManager = new AttendanceManager();

        protected void LoadReport(int xDays)
        {
            DateTime? startDate = null;// = CommonManager.GetCurrentDateAndTime().AddDays(-xDays);
            DateTime? endDate = null;// = CommonManager.GetCurrentDateAndTime();

            DateTime todayDate = new DateTime();
            todayDate = CommonManager.GetCurrentDateAndTime();

            startDate = CommonManager.GetCurrentDateAndTime().AddDays(-7);
            endDate = CommonManager.GetCurrentDateAndTime();
            
             if (xDays == 30)
            {

                startDate = new DateTime(todayDate.Year, todayDate.Month, 1);
                endDate = todayDate;
            }
            else if (xDays == 60)
            {
                endDate = new DateTime(todayDate.Year, todayDate.Month, 1).AddDays(-1);
                startDate = new DateTime(endDate.Value.Year, endDate.Value.Month, 1);
            }


            List<AttendanceReportResult> bindList = new List<AttendanceReportResult>();
            bindList = AttendanceManager.GetAttendanceOfDateRange(SessionManager.CurrentLoggedInEmployeeId, 0, startDate, endDate, 0,0,99999);

            foreach (var item in bindList)
            {
                //if(item.WorkHour!=null)
                //    item.RefinedWorkHour = attendanceManager.MinuteToHourMinuteConverter(Math.Abs((item.WorkHour.Value)));

                DateTime date = item.DateEng.Value;
                if (date.Date == CommonManager.GetCurrentDateAndTime().Date)
                {
                    item.ActualDate = "Today";
                }
                else
                {
                    if (date != null)
                        item.ActualDate = date.ToString("yyyy-MMM-dd");//BLL.BaseBiz.GetAppropriateDate(date);
                }
                
                //if(item.InTime!=null)
                //    item.RefinedInRemarks = item.InTime.Value.ToShortTimeString();
                //if (item.OutTime != null)
                //    item.RefinedOutRemarks = item.OutTime.Value.ToShortTimeString();
            }
            storeMYTimesheet.DataSource = bindList;
            storeMYTimesheet.DataBind();
        }


        protected void xDays_Change(object sender, DirectEventArgs e)
        {
            string selectedValue = cmbLastXDays.SelectedItem.Value;
            int xDays = 7;
            if (!string.IsNullOrEmpty(selectedValue))
            {
                xDays = int.Parse(selectedValue);
            }
            LoadReport(xDays);
        }


        protected void btnViewActivity_Click(object s, DirectEventArgs e)
        {

            CheckOutTime.Text = "";
            CheckInTime.Text = "";

            txtNote1.Text = "";
            txtNote2.Text = "";

            //SelectedDate.Text = DateTime.Parse(Hidden_SelectedDate.Text).ToShortDateString();
            

            AttendanceGetInAndOutTimeResult tempInst = new AttendanceGetInAndOutTimeResult();
            AttendanceCheckInCheckOut CheckIn = new AttendanceCheckInCheckOut();
            AttendanceCheckInCheckOut CheckOut = new AttendanceCheckInCheckOut();

            tempInst = AttendanceManager.getAttendanceofGivenDate(SessionManager.CurrentLoggedInEmployeeId, DateTime.Parse(Hidden_SelectedDate.Text));

            if (tempInst != null)
            {
                if (tempInst.InID != null)
                {
                    CheckIn = AttendanceManager.getCheckINOutByID(tempInst.InID.ToString());
                }

                if (tempInst.OutID != null)
                {
                    CheckOut = AttendanceManager.getCheckINOutByID(tempInst.OutID.ToString());

                    
                }

                if (CheckIn != null && CheckIn.DateTime!=null)
                {
                    
                    CheckInTime.Text = CheckIn.DateTime.ToShortTimeString();
                }
                else
                {
                    CheckInTime.Text = "Clock In Missing";
                }
                if (CheckOut != null && CheckOut.DateTime!=null)
                {
                    CheckOutTime.Text = CheckOut.DateTime.ToShortTimeString();
                }
                else
                {
                    CheckOutTime.Text = "Clock Out Missing";
                }
                //

                List<AttendanceCheckInCheckOutComment> comms1 = new List<AttendanceCheckInCheckOutComment>();
                comms1 = AttendanceManager.getAllCommentsNew(DateTime.Parse(Hidden_SelectedDate.Text), (int)ChkINOUTEnum.ChkIn, SessionManager.CurrentLoggedInEmployeeId);
                if (comms1 != null && comms1.Any())
                {
                    if(comms1.First() != null)
                        txtNote1.Text = comms1.First().Note;
                }

                List<AttendanceCheckInCheckOutComment> comms2 = new List<AttendanceCheckInCheckOutComment>();
                comms2 = AttendanceManager.getAllCommentsNew(DateTime.Parse(Hidden_SelectedDate.Text), (int)ChkINOUTEnum.ChkOut, SessionManager.CurrentLoggedInEmployeeId);
                if (comms2 != null && comms2.Any())
                {
                    if(comms2.First()!=null)
                        txtNote2.Text = comms2.First().Note;
                }

            }

            windAddComment.Center();
            windAddComment.Show();
        }


      



    }
}

//