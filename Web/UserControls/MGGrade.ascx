﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MGGrade.ascx.cs" Inherits="Web.UserControls.MGGrade" %>

<script type="text/javascript">
    function ConfirmLibraryTopicDeletion() {
        var ddllist = document.getElementById('<%= ddlDeposits.ClientID %>');
        if (ddllist.selectedIndex == -1) {
            alert('No grade to delete.');
            return false;
        }
        if (confirm('Do you want to delete the grade?')) {
            clearUnload();
            return true;
        }
        else {
            return false;
        }
    }

    function DisplayLibraryTopicInTextBox(dropdown_id) {
        var ddllist = document.getElementById(dropdown_id);
        document.getElementById('<%= txtDepositRename.ClientID %>').value = ddllist.options[ddllist.selectedIndex].text;
    }

    //capture window closing event
    window.onunload = closePopup;
    function closePopup() {
        clearUnload();
       

        //if not ie & has callback & is chrome then call parent reload as modal doesn't support in chrome
        if ($.browser.msie == false && typeof (window.opener.parentReloadCallbackFunction) != 'undefined' && $.browser.chrome) {
            window.opener.parentReloadCallbackFunction("ReloadGrade", window, texts);
        } else {
            if (typeof (texts) != 'undefined')
                window.returnValue = texts;
            window.close();
        }
    }

    function clearUnload() {
        window.onunload = null;
    }
    
//    //capture window closing event
//    //window.onunload = closePopup;
//    function closePopup() {
//        //debugger;
//        //clear window closing event
//        window.onunload = null;

//        window.returnValue = texts;
//        window.close();
//    }   
//       
</script>




<div class="popupHeader">
<h2 class="headlinespop">Manage Grade</h2>

</div>

<div class="marginal">
<div class="bevel ">
<div class="fields paddpop" style="  padding: 20px 0 20px 10px !important;">


    
    <table cellpadding="0" cellspacing="0" width="600px"   class="alignttable">
    <tr>
        <td class="lf" width="120px">
        New Grade
        </td>
        <td>
        <asp:TextBox ID="txtNewDeposit" runat="server" Width="161px" MaxLength="100" EnableViewState="false"></asp:TextBox>
        <asp:RequiredFieldValidator ValidationGroup="Deposit1" ID="RequiredFieldValidator3"
            Display="None" runat="server" ErrorMessage="Grade name is required." ControlToValidate="txtNewDeposit">*</asp:RequiredFieldValidator>
        <asp:Button ID="btnAddDepsoit"  CssClass="smalladd" style="margin:0px 0 0 5px" OnClientClick="valGroup='Deposit1';return CheckValidation()"
            runat="server" Text="Add" OnClick="btnAddDepsoit_Click" />
        </td>      
    </tr> 
    
    <tr>
        <td class="lf" width="120px">
       Rename Grade
        </td>
        <td>
       <asp:TextBox ID="txtDepositRename" runat="server" Width="161px" MaxLength="100"></asp:TextBox>
            <asp:RequiredFieldValidator ValidationGroup="Deposit2" ID="RequiredFieldValidator4"
                runat="server" ErrorMessage="Grade name is required." ControlToValidate="txtDepositRename"
                Display="None">*</asp:RequiredFieldValidator>
       <asp:Button ID="btnRename" OnClientClick="valGroup='Deposit2';return CheckValidation()"
                runat="server" Text="Update"  CssClass="smallupdate" style="margin:0px 0 0 5px" ValidationGroup="RenameLibraryTopic" OnClick="btnRename_Click" />
        </td>      
    </tr> 
    
    <tr>
        <td class="lf" width="120px">
             Grade List
        </td>
        <td>
         <asp:DropDownList ID="ddlDeposits" runat="server" Width="165px" CssClass="rightMargin"
            DataValueField="GradeId" DataTextField="Name" onchange="DisplayLibraryTopicInTextBox(this.id);">
        </asp:DropDownList>
            
              <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="delete" style="margin:0px 0 0 5px" OnClientClick="javascript:return ConfirmLibraryTopicDeletion();"
            OnClick="btnDelete_Click" />   
            
        </td>      
    </tr> 
       
    </table>
    </div>
    </div>



   
    <div class="clear">
<% if (IsDisplayedAsPopup)
   { %>
<%--<input id="btnClose" type="button"  class="save" value="Close" onclick="closePopup()" />--%>
<%} %>
</div>
    </div>