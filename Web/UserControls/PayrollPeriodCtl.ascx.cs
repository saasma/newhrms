﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using Utils.Calendar;
using Utils.Web;
using DAL;
using Utils.Helper;
using BLL.Base;
using Utils;
namespace Web.UserControls
{
    public partial class PayrollPeriodCtl : BaseUserControl
    {
        CommonManager mgr = new CommonManager();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialise();
            }

         
        }

        public bool IsVisible(object value)
        {
            bool enableDelete = false;

            if (!string.IsNullOrEmpty(Request.QueryString["enable"]))
                enableDelete = true;

            //if (CommonManager.CalculationConstant.CompanyIsHPL != null && CommonManager.CalculationConstant.CompanyIsHPL.Value)
            if(enableDelete || 
                (CommonManager.CalculationConstant.CompanyIsHPL != null && CommonManager.CalculationConstant.CompanyIsHPL.Value))
            {

                int payrollPeriodId = Convert.ToInt32(value);

                return CommonManager.IsLastPeriod(payrollPeriodId);
            }
            else
                return false;
        }


        void LoadPayrollPeriods()
        {
            List<PayrollPeriod> periods = CommonManager.GetCurrentYearPayrollList(SessionManager.CurrentCompanyId);
            gvwPayrollPeriods.DataSource = periods;
            gvwPayrollPeriods.DataBind();

            PayrollPeriod period = CommonManager.GetLastPayrollPeriod();
            string nextStarting;
            CustomDate date;
            if (period != null)
            {
                date = CustomDate.GetCustomDateFromString(period.EndDate, IsEnglish);
                nextStarting = date.IncrementByOneDay().ToString();
                
               
            }
                //is the first payroll period
            else
            {
                FinancialDate currentYear = CommonManager.GetCurrentFinancialYear();
                nextStarting = currentYear.StartingDate;
            }

            date = CustomDate.GetCustomDateFromString(nextStarting, IsEnglish);
            ListItem item = ddlMonths.Items.FindByValue(date.Month.ToString());
            ddlMonths.ClearSelection(); ddlYears.ClearSelection();
            item.Selected = true;
            item = ddlYears.Items.FindByValue(date.Year.ToString());
            item.Selected = true;
            txtPeriodName.Text = ddlMonths.SelectedItem.Text + "-" + ddlYears.SelectedItem.Text;               
                
            //for (int i = gvwPayrollPeriods.Rows.Count - 1; i >= 0; i--)
            //{
            //    GridViewRow row = gvwPayrollPeriods.Rows[i];
            //    ImageButton btn = row.FindControl("btnEdit") as ImageButton;

            //    int payrollPeriodId = (int) gvwPayrollPeriods.DataKeys[row.RowIndex][0];

            //    if (CalculationManager.IsPayrollEditable(payrollPeriodId))
            //    {
            //        btn.Visible = true;


                    
            //    }
            //    break;
            //}
        }

        void Initialise()
        {
            if (!string.IsNullOrEmpty(Request.QueryString["enable"]))
            {
                divWarningMsg.InnerHtml = "Please take database backup before period deletion and unlocking.";
                divWarningMsg.Hide = false;
            }

                ddlMonths.DataSource = DateManager.GetCurrentMonthList();
            ddlMonths.DataBind();

            ddlYears.DataSource = DateManager.GetYearListForPayrollPeriod();
            ddlYears.DataBind();

            SetPeriod();


           
            // show message for new period creation
            string msg = "<ul><li>Import insurance for new year</li>";

            if (BLL.BaseBiz.PayrollDataContext.PIncomes.Any(x => x.AddTaxFromTheBeginning != null && x.AddTaxFromTheBeginning.Value))
            {
                msg += "<li>Set Eligibility date for Add Tax from Beginning incomes like festival types as date may have been changed for new year</li>";
                msg += "<li>Associate all Festival type incomes like (Dashain,Medical,Leave fare) to new employees, goto to not added employee payroll, select all incomes to be added, then select \"Add to these employees\" and click ok</li>";
            }

            Setting setting = CommonManager.Setting;
            if (setting.LapseLeavesAfterNextPeriodCreationOnly != null
                && setting.LapseLeavesAfterNextPeriodCreationOnly.Value
                && SessionManager.CurrentCompany.LeaveStartMonth == 4)
            {
                msg += "<li>After new period creation, leaves will be accured, for lapse of previous month leaves, previous month attendance need to be resaved. </li>";
            }

            liMessage.InnerHtml = msg + "</ul>";

            CustomDate todayDate = DateManager.GetTodayDate();
            UIHelper.SetSelectedInDropDown(ddlMonths, todayDate.Month.ToString());
            UIHelper.SetSelectedInDropDown(ddlYears, todayDate.Year.ToString());

            lblCurrentFinancialYear.Text = string.Format(
                lblCurrentFinancialYear.Text, SessionManager.CurrentCompanyFinancialDate.StartingDate + " - " +
                SessionManager.CurrentCompanyFinancialDate.EndingDate);


            LoadPayrollPeriods();
        }

        private void SetPeriod()
        {
            if( ddlMonths.SelectedItem != null)
            txtPeriodName.Text = ddlMonths.SelectedItem.Text + "/" + ddlYears.SelectedItem.Text;
        }


        protected void Page_PreRender(object sender, EventArgs e)
        {
            SetPeriod();
            PayrollPeriod last = CommonManager.GetLastPayrollPeriod();
            //if (last != null)
            //{
            //    lblLastPayrollPeriod.Text = string.Format(lblLastPayrollPeriod.Text, last.Name);
            //}
            //else
            //{
            //    lblLastPayrollPeriod.Text = string.Format(lblLastPayrollPeriod.Text, "");
            //}
        }

        bool IsMonthYearGreaterThan(CustomDate start, CustomDate end)
        {
            if (end.Year > start.Year)
                return true;
            else if (end.Year < start.Year)            
                return false;

            if (end.Month >= start.Month)
                return true;
            else if (end.Month < start.Month)
                return false;

            return true;
        }

        bool Validate(string name)
        {
            CustomDate startDate = CustomDate.GetCustomDateFromString(SessionManager.CurrentCompanyFinancialDate.StartingDate,
                SessionManager.CurrentCompany.IsEnglishDate);
            CustomDate endDate = CustomDate.GetCustomDateFromString(SessionManager.CurrentCompanyFinancialDate.EndingDate,
                            SessionManager.CurrentCompany.IsEnglishDate);

            int month = int.Parse(ddlMonths.SelectedValue);
            int year = int.Parse(ddlYears.SelectedValue);
            CustomDate date = new CustomDate(1, month, year, SessionManager.CurrentCompany.IsEnglishDate);
            
            // Greater than startDate
            if (!IsMonthYearGreaterThan(startDate, date))
            {
                JavascriptHelper.DisplayClientMsg("Payroll period date must lie between financial start & end date.", Page);
                return false;
            }


            if (!IsMonthYearGreaterThan(date, endDate))
            {
                JavascriptHelper.DisplayClientMsg("Payroll period date must lie between financial start & end date.", Page);
                return false;
            }

            if( CommonManager.IsPayrollPeriodAlreadyExists(GetIdFromGrid(),name))
            {
                Utils.Helper.JavascriptHelper.DisplayClientMsg("Payroll period already exists for the current month/year.", Page);
                return false;
            }


            // Check if new payroll period is just after the last one
            PayrollPeriod last = CommonManager.GetLastPayrollPeriod();
            if( last!= null)
            {
                CustomDate lastDate = new CustomDate(1, last.Month, last.Year.Value, SessionManager.CurrentCompany.IsEnglishDate);

                if (!IsMonthYearGreaterThan(lastDate, date))
                {
                    divWarningMsg.InnerHtml = Resources.Messages.PayrollPeriodMustBeLastMsg;
                    divWarningMsg.Hide = false;
                    //JavascriptHelper.DisplayClientMsg("Payroll period must be the last.", Page);
                    return false;
                }
            }

            //Check if the last Payroll period calculation has been final saved or not
            if (Config.AllowPayrollPeriodCreationBeforeFinalSave == false)
            {
                if (last != null && GetIdFromGrid() == 0)
                {
                    if (!CalculationManager.IsAllEmployeeSavedFinally(last.PayrollPeriodId, false))
                    {
                        //JavascriptHelper.DisplayClientMsg("New Payroll period can not be created as last payroll period are not final saved or all employees are not saved.", Page);
                        divWarningMsg.InnerHtml = Resources.Messages.PayrollPeriodNewCannotBeCreatedAsPreviousNotFinalSavedMsg;
                        divWarningMsg.Hide = false;
                        return false;
                    }
                }
            }


            return true;
          

        }

        protected void btnCreate_Click(object sender, EventArgs e)
        {
            string name = ddlMonths.SelectedItem.Text + "/" + ddlYears.SelectedValue;


            if (Validate(name) == false)
                return;


            PayrollPeriod entity = new PayrollPeriod();
            entity.PayrollPeriodId = GetIdFromGrid();
            entity.CompanyId = SessionManager.CurrentCompanyId;
            entity.Name = name;
            entity.Month = int.Parse(ddlMonths.SelectedValue);
            entity.Year = int.Parse(ddlYears.SelectedValue);
            //entity.StartDate = calStartDate.SelectedDate.ToString();
            //entity.EndDate = calEndDate.SelectedDate.ToString();
            entity.FinancialDateId = SessionManager.CurrentCompanyFinancialDate.FinancialDateId;
            if (mgr.SavePayrollPeriod(entity))
            {


                divMsgCtl.InnerHtml = Resources.Messages.PayrollPeriodInsertSuccessfullyMsg;
                divMsgCtl.Hide = false;

                gvwPayrollPeriods.SelectedIndex = -1;
                LoadPayrollPeriods();
                btnCreate.Text = "Create";
            }
           
        }

        int GetIdFromGrid()
        {
            if (gvwPayrollPeriods.SelectedIndex != -1)
            {
                return (int)gvwPayrollPeriods.DataKeys[gvwPayrollPeriods.SelectedIndex][0];
            }
            return 0;
        }

        protected void gvwPayrollPeriods_SelectedIndexChanged(object sender, EventArgs e)
        {
            int payrollPeriodId = (int)gvwPayrollPeriods.DataKeys[gvwPayrollPeriods.SelectedIndex][0];
            PayrollPeriod payrollPeriod = CommonManager.GetPayrollPeriod(payrollPeriodId);
            if (payrollPeriod != null)
            {

                btnCreate.Text = Resources.Messages.Update;
                txtPeriodName.Text = payrollPeriod.Name;

                UIHelper.SetSelectedInDropDown(ddlMonths, payrollPeriod.Month);
                UIHelper.SetSelectedInDropDown(ddlYears, payrollPeriod.Year);
                //calStartDate.SetSelectedDate(payrollPeriod.StartDate, SessionManager.CurrentCompany.IsEnglishDate);
                //calEndDate.SetSelectedDate(payrollPeriod.EndDate, SessionManager.CurrentCompany.IsEnglishDate);
            }
            //Branch branch = branchMgr.GetById(selBranchId);
            //if (branch != null)
            //{
            //    ProcessBranch(branch);
            //    btnSave.Text = Resources.Messages.Update;
            //}
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            gvwPayrollPeriods.SelectedIndex = -1;
            LoadPayrollPeriods();
            btnCreate.Text = "Create";
        }

        protected void gvwPayrollPeriods_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int payrollPeriodId = int.Parse(e.CommandArgument.ToString());
            string msg = "";
            if (e.CommandName == "Delete1")
            {
                msg = CommonManager.DeleteLastPeriod(payrollPeriodId);
            }
            else if (e.CommandName == "UnLock")
            {
                msg = CommonManager.UnlockLastPeriod(payrollPeriodId);
            }

            MyCache.Reset();

            divWarningMsg.InnerHtml = msg;
            divWarningMsg.Hide=false;

            LoadPayrollPeriods();

           
        }
    }
}