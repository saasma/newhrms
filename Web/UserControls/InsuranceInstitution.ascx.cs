﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Manager;
using Utils.Helper;
using BLL;
using DAL;
using Utils.Web;
using System.Text;

namespace Web.UserControls
{
    public partial class InsuranceInstitution : System.Web.UI.UserControl
    {
        InsuranceManager insMgr = new InsuranceManager();
        List<IInsuranceInstitution> source = new List<IInsuranceInstitution>();
        public bool IsDisplayedAsPopup
        {
            get
            {
                if (Request.QueryString["isPopup"] != null)
                    return true;
                return false;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                Initialise();

        }

        void Initialise()
        {
            Load();
            if (ddlInstitution.SelectedItem != null)
                txtInstitutionRename.Text = ddlInstitution.SelectedItem.Text;
            else
                txtInstitutionRename.Text = "";
        }

        new void Load()
        {
            source = insMgr.GetInsuranceInstitution();
            ddlInstitution.DataSource = source;
            ddlInstitution.DataBind();
        }

        protected void btnAddInstitution_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                IInsuranceInstitution entity = new IInsuranceInstitution();
                entity.InstitutionName = txtNewInstitution.Text.Trim();

                insMgr.Save(entity);

                txtNewInstitution.Text = "";
                Initialise();
                JavascriptHelper.DisplayClientMsg("Insurance company added.", Page);
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            if (ddlInstitution.SelectedItem != null)
            {
                IInsuranceInstitution entity = new IInsuranceInstitution();
                entity.Id = int.Parse(ddlInstitution.SelectedValue);

                if (insMgr.Delete(entity))
                {
                    JavascriptHelper.DisplayClientMsg("Insurance company deleted.", Page);
                    
                }
                else
                {
                    JavascriptHelper.DisplayClientMsg("Insurance company is in use.", Page);
                }
                Initialise();
            }
        }

        protected void btnRename_Click(object sender, EventArgs e)
        {
            if (Page.IsValid && ddlInstitution.SelectedItem != null)
            {
                IInsuranceInstitution entity = new IInsuranceInstitution();
                entity.InstitutionName = txtInstitutionRename.Text.Trim();
                entity.Id = int.Parse(ddlInstitution.SelectedValue);

                insMgr.Update(entity);
                Load();
                UIHelper.SetSelectedInDropDown(ddlInstitution, entity.Id.ToString());
                JavascriptHelper.DisplayClientMsg("Insurance company updated.", Page);
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //output all as js array to be updatable in parent window
            if (IsDisplayedAsPopup && source != null)
            {
                //Page.ClientScript.
                StringBuilder str = new StringBuilder("");
                bool first = true;
                foreach (IInsuranceInstitution obj in source)
                {
                    if (first == false)
                        str.Append(",");
                    str.Append("'" + obj.Id + "$$" + obj.InstitutionName + "'");
                    first = false;
                }
                Page.ClientScript.RegisterArrayDeclaration("texts", str.ToString());
            }
        }
        
    }
}