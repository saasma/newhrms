using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL;
using BLL.Manager;
using DAL;
using Utils.Web;
using Utils.Helper;
using System.Text;
using Utils;

namespace Web.UserControls
{
    public partial class MGDepartment : BaseUserControl
    {
        DepartmentManager depMgr = new DepartmentManager();
        BranchManager branchMgr = new BranchManager();
        EmployeeManager empMgr = new EmployeeManager();
        CompanyManager cmpmgr = new CompanyManager();
        Department dep = new Department();


        public bool IsDisplayedFromPopup
        {
            get
            {
                if (Request.QueryString["isPopup"] != null)
                    return true;
                return false;
            }
        }
        public int BranchId
        {
            get
            {
                if (ViewState["BranchId"] != null)
                    return int.Parse(ViewState["BranchId"].ToString());
                return 0;
            }
            set
            {
                ViewState["BranchId"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            divMsgCtl.DataBind();
            divWarningMsg.DataBind();

            if (!IsPostBack)
            {
                Initialise();
                if (!IsDisplayedFromPopup)
                    div.Visible = false;

                LoadDepartments();
            }

            if (Request.Form["__EVENTTARGET"] != null && Request.Form["__EVENTTARGET"].Equals("Reload"))
                LoadDepartments();

            Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "clearUnload();");

            JavascriptHelper.AttachPopUpCode(Page, "popupDepDetails", "ManageDepAsPopup.aspx", 500, 500);

        }

        public TreeNode GetNode(UPage page)
        {
            TreeNode node = new TreeNode();
            node.NavigateUrl = "";
            node.SelectAction = TreeNodeSelectAction.None; ;

            node.Value = page.PageId.ToString();
            node.Text = page.PageName;
            return node;
        }

        public TreeNode GetNode(Branch page)
        {
            TreeNode node = new TreeNode();
            node.NavigateUrl = "";
            node.SelectAction = TreeNodeSelectAction.None; ;

            node.Value = page.BranchId.ToString();
            node.Text = page.Name;
            return node;
        }

        protected void gvwEmployees_RowCreated(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover",
                "this.originalClass=this.className;this.className='selected'");

                e.Row.Attributes.Add("onmouseout",
                "this.className=this.originalClass;");


            }

        }

        private void UnCheckAllNodes(TreeNode node)
        {
            if (node.ChildNodes == null || node.ChildNodes.Count == 0)
                return;

            foreach (TreeNode childNode in node.ChildNodes)
            {
                childNode.Checked = false;
                childNode.Selected = false;
                UnCheckAllNodes(childNode);
            }
            return;
        }

        


        void Initialise()
        {
           

            List<Branch> parentPages = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);

            ddlBranch.DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            ddlBranch.DataBind();

            if (IsDisplayedFromPopup)
            {
                //maintain branch selection from employee
                this.BranchId = UrlHelper.GetIdFromQueryString("BranchId");
                if (this.BranchId != 0)
                {
                    UIHelper.SetSelectedInDropDown(ddlBranch, this.BranchId.ToString());
                    LoadDepartments();
                }
            }

            
        }

        void LoadDepartments()
        {
            //if (ddlBranch.SelectedValue.Equals("-1") == false)
            {
                List<Department> list = DepartmentManager.GetAllDepartments();
                foreach (Department item in list)
                {
                    if (item.LocationBranchId != null)
                    {
                        Branch branch = BranchManager.GetBranchById(item.LocationBranchId.Value);
                        if (branch != null)
                            item.LocationBranch = branch.Name;
                    }
                }
                gvwDepartments.DataSource = list;
                gvwDepartments.DataBind();
            }
        }
        
        protected void ddlBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            //BranchId = int.Parse(ddlBranch.SelectedValue.ToString());
            LoadDepartments();
        }    

        protected void gvwDepartments_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            CommonManager.ResetCache();
            int id = (int)gvwDepartments.DataKeys[e.RowIndex][0];

            if (id != 0)
            {
                dep.DepartmentId = id;
                if (depMgr.Delete(dep) == 1)
                {
                   
                    divMsgCtl.InnerHtml = Resources.Messages.DepartmentDeletedMsg;
                    divMsgCtl.Hide = false;
                }
                else
                {
                    //JavascriptHelper.DisplayClientMsg("Department is in use.", Page);
                    divWarningMsg.InnerHtml = Resources.Messages.DepartmentIsInUseMsg;
                    divWarningMsg.Hide = false;
                }
            }
            LoadDepartments();
        }
              
        protected void gvwDepartments_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvwDepartments.PageIndex = e.NewPageIndex;
            gvwDepartments.SelectedIndex = -1;
            LoadDepartments();
        }


        protected void Page_PreRender(object sender, EventArgs e)
        {
            //output for the querystring branchId as js array to be updatable in parent/employee window
            if (IsDisplayedFromPopup && this.BranchId != 0)
            {
                //Page.ClientScript.
                StringBuilder str = new StringBuilder("");
                bool first = true;
                List<GetDepartmentListResult> deps= DepartmentManager.GetAllDepartmentsByBranch(this.BranchId);
                foreach (GetDepartmentListResult obj in deps)
                {
                    if (first == false)
                        str.Append(",");
                    str.Append("'" + obj.DepartmentId + "$$" + obj.Name + "'");
                    first = false;
                }
                Page.ClientScript.RegisterArrayDeclaration("texts", str.ToString());
            }

            //if (IsDisplayedFromPopup)
            //{
            //    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdfdsf34", "window.onunload = closePopup;", true);
            //}
        }
       
    }
}