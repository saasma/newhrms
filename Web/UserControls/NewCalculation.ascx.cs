﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using BLL;
using BLL.Base;
using BLL.Manager;
using DAL;
using System.Drawing;
using System.Text;
using Utils.Helper;

namespace Web.UserControls
{
    public partial class NewCalculation : BaseUserControl
    {
        public const int ColumnWidth = 69;

        private CalculationManager calcMgr = new CalculationManager();
        public CCalculation calculation = null;
        public bool isValidAllValid = true;
        public bool editMode = false;

        private List<CalcGetHeaderListResult> headers = new List<CalcGetHeaderListResult>();

        public Dictionary<string, string> headerList = new Dictionary<string, string>();
        private int payrollPeriodId = 0;

        public bool isIncomeAdjChecked = false;
        public bool isDeductionAdjChecked = false;

        private void RestoreAdjustmentCheckboxState()
        {
            foreach (string key in Request.Form.AllKeys)
            {
                if (key.Contains(CalculationManager.idIncomeAdjCheckbox) && Request.Form[key].ToLower() == "on")
                {
                    isIncomeAdjChecked = true;
                }
                if (key.Contains(CalculationManager.idDeductionAdjCheckbox) && Request.Form[key].ToLower() == "on")
                {
                    isDeductionAdjChecked = true;
                }
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialise();

                _tempCurrentPage = 1;

                LoadCalculation();


                RegisterScripts();
            }


            JavascriptHelper.AttachPopUpCode(Page, "commentPopup", "PopupHtml.htm", 300, 300);
        }


        /// <summary>
        /// Contains page logic to display which error msg & what or display saved list with "Generate tax" button
        /// </summary>
        private void Process()
        {
        }

        private void RegisterScripts()
        {
            var str = new StringBuilder();
            str.AppendFormat("var decimalPlaces = {0};", SessionManager.DecimalPlaces);
            str.AppendFormat("var txtIncomeSumTxt = 'txtd{0}';",
                             (((int)CalculationColumnType.IncomeGross)) +
                             ((int)CalculationColumnType.IncomeGross).ToString());
            str.AppendFormat("var txtDeductionSumTxt = 'txtd{0}';",
                            (((int)CalculationColumnType.DeductionTotal)) +
                            ((int)CalculationColumnType.DeductionTotal).ToString());

            str.AppendFormat("var txtNetTxt = 'txtd{0}';",
                            (((int)CalculationColumnType.NetSalary)) +
                            ((int)CalculationColumnType.NetSalary).ToString());

            str.AppendFormat("var msgCannotUnchecked = '{0}';", Resources.Messages.CalcCannotUncheckedForAdjustment);

            Page.ClientScript.RegisterClientScriptBlock(GetType(), "rounding",
                                                        str.ToString()
            , true);
        }

        private bool Initialise()
        {
            ddlPayrollPeriods.Items.Clear();

            ddlPayrollPeriods.DataSource = CommonManager.GetCurrentYear(SessionManager.CurrentCompanyId);
            ddlPayrollPeriods.DataBind();

            ddlPayrollPeriods.ClearSelection();

            if (ddlPayrollPeriods.Items.Count <= 0)
            {
                divErrorMsg.InnerHtml = Resources.Messages.CalcNoPayrollPeriodDefined;
                HideAll(false);

                return false;
            }


            if (payrollPeriodId == 0)
            {
                var item = ddlPayrollPeriods.Items[ddlPayrollPeriods.Items.Count - 1];
                if (item != null)
                {
                    payrollPeriodId = int.Parse(item.Value);
                    item.Selected = true;
                }
            }
            else
            {
                var item = ddlPayrollPeriods.Items.FindByValue(payrollPeriodId.ToString());
                if (item != null)
                {
                    payrollPeriodId = int.Parse(item.Value);
                    item.Selected = true;
                }
            }

            calculation = CalculationManager.IsCalculationSaved(GetPayrollPeriodId());


            chkHasRetiredOrResigned.Visible =
                CalculationManager.HasPayrollPeriodRetirementOrResignedEmp(GetPayrollPeriodId());

            if (!IsPostBack ||
                Request.Form["__EventTarget"] != null && Request.Form["__EventTarget"].Equals(ddlPayrollPeriods.ClientID.Replace("_", "$")))
            {
                if (chkHasRetiredOrResigned.Visible)
                {
                    chkHasRetiredOrResigned.Checked = true;
                }
            }

            if (chkHasRetiredOrResigned.Visible == false)
            {
                chkHasRetiredOrResigned.Checked = false;
            }
            return true;
        }

        private void HideAll(bool show)
        {
            if (!show)
            {
                pagingCtl.Visible = false;
                gvw.Visible = false;
                pnlButtons.Visible = false;

                divErrorMsg.Hide = false;
            }
            else
            {
                pagingCtl.Visible = true;
                gvw.Visible = true;
                pnlButtons.Visible = true;
                divErrorMsg.Hide = true;
            }
        }

        private int GetPayrollPeriodId()
        {
            return payrollPeriodId;
        }



        private void LoadCalculation()
        {
            headers = CalculationManager.GetIncomeHeaderList(SessionManager.CurrentCompanyId, GetPayrollPeriodId());
            foreach (CalcGetHeaderListResult header in headers)
            {
                var key = header.Type + ":" + header.SourceId;
                if (!headerList.ContainsKey(key) )
                {
                    headerList.Add(key, string.Empty);
                }
            }

            AddColumns(headers, gvw, this);

            HideAll(true);

            ChangeDisplayState();

            if (GetPayrollPeriodId() != 0)
            {
                var pageIndex = _tempCurrentPage - 1;
                var pageSize = int.Parse(pagingCtl.DDLRecords.SelectedValue);

                gvw.DataSource = CalculationManager.GetNewCalculationList(SessionManager.CurrentCompanyId,
                    GetPayrollPeriodId(), pageIndex,
                    pageSize, ref _tempCount, chkHasRetiredOrResigned.Checked, null);
                gvw.DataBind();

                if ( gvw.Rows.Count <= 0)
                {
                    divErrorMsg.InnerHtml = Resources.Messages.CalcNoEmployees;
                    HideAll(false);
                    return;
                }
            }
            else
            {
                gvw.DataSource = null;
                gvw.DataBind();

                return;
            }

            SetPagingSetting();
        }


        private void ChangeDisplayState()
        {
            if (CalculationManager.IsAllEmployeeSavedFinally(GetPayrollPeriodId(), chkHasRetiredOrResigned.Checked))
            {
                pnlButtons.Visible = false;
                gvw.Columns[gvw.Columns.Count - 1].Visible = false;
                divWarningMsg.InnerHtml = Resources.Messages.CalcAllEmployeeFinalSavedMsg;
                divWarningMsg.Hide = false;
                return;
            }

            var totalEmployees = CalculationManager.GetTotalApplicableForSalary(GetPayrollPeriodId(), chkHasRetiredOrResigned.Checked);
            var initialSave = calcMgr.GetTotalEmployeeInitialSave(GetPayrollPeriodId(), chkHasRetiredOrResigned.Checked);
            if ( totalEmployees == initialSave)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                btnSaveFinal.Visible = true;

                var msg = string.Format(Resources.Messages.CalcTaxSavingWarningMsg, initialSave);
                btnSaveFinal.OnClientClick
                = string.Format(btnSaveFinal.OnClientClick, msg);
            }
            else
            {
                if (initialSave > 0)
                {
                    gvw.Columns[gvw.Columns.Count - 1].Visible = true;
                    btnDelete.Visible = true;
                    btnSaveFinal.Visible = false;
                    btnSave.Visible = true;
                }
                else
                {
                    btnSaveFinal.Visible = false;
                    btnSave.Visible = true;
                    btnDelete.Visible = false;
                }
            }

            if (editMode)
            {
                btnSaveFinal.Visible = false;
                btnSave.Visible = true;
            }
        }



        protected void btnSaveFinal_Click(object sender, EventArgs e)
        {
            CalculationManager.SaveFinally(GetPayrollPeriodId(), chkHasRetiredOrResigned.Checked);
            LoadCalculation();
            divWarningMsg.InnerHtml = Resources.Messages.CalculationFinalSavedMsg;
            divWarningMsg.Hide = false;
        }

        protected void btnGenerateSalary_Click(object sender, EventArgs e)
        {
            CalculationManager.GenerateSalary(
                GetPayrollPeriodId(), -1, -1, chkHasRetiredOrResigned.Checked);

            LoadCalculation();
        }



        protected void btnDelete_Click(object sender, EventArgs e)
        {
            var xmlEmployees = new StringBuilder();
            xmlEmployees.Append("<root>");
            var employeeAdded = false;
            for (var i = 0; i < gvw.Rows.Count; i++)
            {
                var row = gvw.Rows[i];
                if (row.FindControl("lblId") == null)
                {
                    continue;
                }
                if (row.FindControl("chkDelete") == null)
                {
                    continue;
                }
                var chkDelete = (CheckBox)row.FindControl("chkDelete");

                if (chkDelete.Checked)
                {
                    var empId = int.Parse(((Label)row.FindControl("lblId")).Text);

                    xmlEmployees.AppendFormat("<row EmployeeId='{0}' />", empId);
                    employeeAdded = true;
                }
            }
            xmlEmployees.Append("</root>");

            if (calculation != null && employeeAdded)
            {
                var result = calcMgr.DeleteCalculation(calculation.CalculationId, xmlEmployees.ToString(), ddlPayrollPeriods.SelectedItem.Text);

                if (result)
                {
                    msgCtl.InnerHtml = Resources.Messages.CalculationCompletelyDeletedMsg;
                    msgCtl.Hide = false;
                    calculation = null;
                }
                else
                {
                    msgCtl.InnerHtml = Resources.Messages.CalculationDeletedMsg;
                    msgCtl.Hide = false;
                }


                LoadCalculation();
            }
        }


        protected void btnCancel_Click(object sender, EventArgs e)
        {
            editMode = false;
            LoadCalculation();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            var employeeSaved = SaveCalculation();
            editMode = false;


            if (employeeSaved)
            {
                calculation = CalculationManager.IsCalculationSaved(GetPayrollPeriodId());
                LoadCalculation();

                msgCtl.InnerHtml = Resources.Messages.CalculationSavedMsg;
                msgCtl.Hide = false;
            }
        }


        private bool SaveCalculation()
        {
            var payrollPeriodId = GetPayrollPeriodId();
            var hasAdjIncome = false;
            var hasAdjDeduction = false;
            var adjIncome = string.Empty;
            var adjDeduction = string.Empty;
            var hasPF = SessionManager.CurrentCompany.HasPFRFFund;
            double empContribution = 0;
            double companyContribution = 0;
            var pfRFNo = string.Empty;

            if (hasPF)
            {
                empContribution = SessionManager.CurrentCompany.PFRFFunds[0].EmployeeContribution.Value;
                companyContribution = SessionManager.CurrentCompany.PFRFFunds[0].CompanyContribution.Value;
                pfRFNo = SessionManager.CurrentCompany.PFRFFunds[0].PFRFNo;
            }

            var xmlHeaders = new StringBuilder();
            var xmlEmployees = new StringBuilder();
            var xmlCells = new StringBuilder();
            var xmlIncludeds = new StringBuilder();

            xmlHeaders.Append("<root>");
            var rowHeader = (System.Web.UI.WebControls.TableRow)gvw.HeaderRow;
            for (var j = 2; j < rowHeader.Cells.Count - 1; j++)
            {
                if (rowHeader.Cells[j].Controls.Count > 0)
                {
                    var lblHeader = (Label)rowHeader.Cells[j].Controls[0];
                    TextBox txtAdjustment = null;

                    var columnType =
                    (CalculationColumnType) int.Parse(lblHeader.Attributes["Type"].ToString());



                    
                    var isIncome = string.Empty;
                    if (CalculationValue.IsColumTypeIncome(columnType))
                    {
                        isIncome = "1";
                    }
                    else
                    {
                        if (CalculationValue.IsColumTypeDeduction(columnType))
                        {
                            isIncome = "0";
                        }
                    }
                    xmlHeaders.AppendFormat("<row SourceId='{0}' Type='{1}' HeaderName='{2}' IsIncome='{3}' />",
                                            lblHeader.Attributes["SourceId"], lblHeader.Attributes["Type"],
                                            (txtAdjustment == null)
                                                ? lblHeader.Attributes["HeaderName"]
                                                : (Request.Form[txtAdjustment.UniqueID]),
                                                isIncome);
                }
            }
            xmlHeaders.Append("</root>");

            var employeeSaved = false;

            xmlEmployees.Append("<root>");
            xmlCells.Append("<root>");
            xmlIncludeds.Append("<root>");
            for (var i = 0; i < gvw.Rows.Count; i++)
            {
                var gridViewRow = gvw.Rows[i];
                if (gridViewRow.FindControl("lblId") == null)
                {
                    continue;
                }
                var empId = (int)gvw.DataKeys[gridViewRow.RowIndex]["EmployeeId"];
                var calculationId = (int)gvw.DataKeys[gridViewRow.RowIndex]["CalculationId"];
                var adjustmentIncome = gridViewRow.FindControl("adjIncome") as HiddenField;
                var adjustmentDeduction = gridViewRow.FindControl("adjDeduction") as HiddenField;

                var isRetiredOrResigned = gvw.DataKeys[gridViewRow.RowIndex]["IsRetiredOrResigned"].ToString();
                var isFinalSaved = gvw.DataKeys[gridViewRow.RowIndex]["IsFinalSaved"].ToString();

                if (isFinalSaved == "1")
                {
                    continue;
                }
                var adjIncomeComment = string.Empty;
                var adjDeductionComment = string.Empty;

                if (hasAdjIncome && adjustmentIncome != null && !string.IsNullOrEmpty(adjustmentIncome.Value))
                {
                    adjIncomeComment = adjustmentIncome.Value;
                }
                if (hasAdjDeduction && adjustmentDeduction != null && !string.IsNullOrEmpty(adjustmentDeduction.Value))
                {
                    adjDeductionComment = adjustmentDeduction.Value;
                }

                if (calculationId == 0 || editMode)
                {
                    employeeSaved = true;

                    xmlEmployees.AppendFormat(
                        "<row EmployeeId='{0}' AdjustmentIncome='{1}' AdjustmentDeduction='{2}' IsRetiredOrResigned='{3}' />", empId,
                        adjIncomeComment, adjDeductionComment, isRetiredOrResigned
                    );

                    //var isRetrospectNegative = false;

                    for (var j = 2; j < gridViewRow.Cells.Count - 1; j++)
                    {
                        var txt = (TextBox)gridViewRow.Cells[j].Controls[0];
                        var lblHeader = (Label)rowHeader.Cells[j].Controls[0];

                        if (!txt.Text.Equals(Resources.Messages.EmptySalaryAmountText))
                        {
                            if (Request.Form[txt.UniqueID] == null)
                            {
                                continue;
                            }

                            var amount = decimal.Parse(Request.Form[txt.UniqueID].ToString());
                            var columnType =
                            (CalculationColumnType)int.Parse(lblHeader.Attributes["Type"].ToString());

                            

                            //if (amount < 0 && isRetiredOrResigned == "0" && columnType != CalculationColumnType.TDS && columnType
                            //    != CalculationColumnType.IncomeResterospect && !isRetrospectNegative)
                            //{
                            //    isValidAllValid = false;
                            //    txt.BorderColor = Color.Red;
                            //    txt.BorderWidth = new Unit(1);
                            //    txt.ToolTip = "Invalid value";
                            //    return false;
                            //}

                            if (txt.Attributes["CalculationIncluded"] != null)
                            {
                                var inclduedList = txt.Attributes["CalculationIncluded"].Split(new char[] { ',' });
                                if (inclduedList.Length > 0)
                                {
                                    foreach (var s in inclduedList)
                                    {
                                        if (!string.IsNullOrEmpty(s))
                                        {
                                            var typeSource = s.Split(new char[] { '=' });
                                            xmlIncludeds.AppendFormat(
                                                "<row EmployeeId='{0}' Type='{1}' SourceId='{2}' />",
                                                empId, typeSource[0], typeSource[1]
                                            );
                                        }
                                    }
                                }
                            }

                            
                            xmlCells.AppendFormat("<row SourceId='{0}' Type='{1}' EmployeeId='{2}' Amount='{3}' />",
                                                  lblHeader.Attributes["SourceId"], lblHeader.Attributes["Type"], empId,
                                                  amount
                            );
                        }
                    }
                }
            }
            xmlCells.Append("</root>");
            xmlEmployees.Append("</root>");
            xmlIncludeds.Append("</root>");



            if (employeeSaved)
            {
                calcMgr.SaveUpdateCalculation(payrollPeriodId, hasAdjIncome, adjIncome, hasAdjDeduction, adjDeduction,
                                              hasPF, pfRFNo, empContribution, companyContribution,
                                              xmlHeaders.ToString(), xmlEmployees.ToString(), xmlCells.ToString(),
                                              xmlIncludeds.ToString());
            }
            return employeeSaved;
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (!isValidAllValid)
            {
                btnSaveFinal.Visible = false;
                btnSave.Visible = false;

                if (gvw.Columns[gvw.Columns.Count - 1].Visible)
                {
                    btnDelete.Visible = true;
                }
                else
                {
                    btnDelete.Visible = false;
                }

                divErrorMsg.InnerHtml = Resources.Messages.CaluclationInvalidNegativeValueMsg;
                divErrorMsg.Hide = false;
                CalculationManager.DisableAllCells(gvw);
            }
            var jsCodes = string.Format("var payrollId = {0}; var isRetResignedOnly = {1}"
                , GetPayrollPeriodId(), chkHasRetiredOrResigned.Checked ? "true" : "false");
            Page.ClientScript.RegisterClientScriptBlock(GetType(), "regpayretreg", jsCodes, true);


            if (chkHasRetiredOrResigned.Visible == true && chkHasRetiredOrResigned.Checked == true)
            {
                btnSaveFinal.Text = Resources.Messages.RetRegFinalSaveText;
            }
            else
            {
                btnSaveFinal.Text = Resources.Messages.GenerateTaxText;
            }
        }


        public static void AddColumns(List<CalcGetHeaderListResult> headers, GridView gvw, NewCalculation calculationControl)
        {
            if (gvw.Columns.Count != 3 )
            {
                for (var i = gvw.Columns.Count - 2; i >= 2; i--)
                {
                    gvw.Columns.RemoveAt(i);
                }
            }

            if (gvw.Columns.Count == 3)
            {
                headers = CalculationValue.SortHeaders(headers, PayManager.GetIncomeOrderValues(), PayManager.GetDeductionOrderValues());

                for (var i = 0; i < headers.Count; i++)
                {
                    var field = new TemplateField();
                    field.ItemTemplate = new NewCalcGridViewTemplate(DataControlRowType.DataRow, headers[i], calculationControl);
                    field.HeaderTemplate = new NewCalcGridViewTemplate(DataControlRowType.Header, headers[i], calculationControl);
                    gvw.Columns.Insert(gvw.Columns.Count - 1, field);
                }
            }
        }

        private int _tempCurrentPage;
        private int? _tempCount = 0;

        protected override void OnInit(EventArgs e)
        {
            Page.RegisterRequiresControlState(this);
            base.OnInit(e);
        }
        protected override void LoadControlState(object savedState)
        {
            var rgState = (object[])savedState;
            base.LoadControlState(rgState[0]);

            _tempCurrentPage = (int)rgState[1];
            payrollPeriodId = (int)rgState[2];
            editMode = (bool)rgState[3];
        }
        protected override object SaveControlState()
        {
            var rgState = new object[4];
            rgState[0] = base.SaveControlState();

            rgState[1] = _tempCurrentPage;
            rgState[2] = payrollPeriodId;
            rgState[3] = editMode;

            return rgState;
        }

        protected void btnPrevious_Click()
        {
            _tempCurrentPage = int.Parse(pagingCtl.LabelCurrentPage.Text);
            _tempCurrentPage -= 1;
            LoadCalculation();
        }

        protected void btnNext_Click()
        {
            _tempCurrentPage = int.Parse(pagingCtl.LabelCurrentPage.Text);
            _tempCurrentPage += 1;
            LoadCalculation();
        }
        protected void ddlRecords_SelectedIndexChanged()
        {
            _tempCurrentPage = 1;
            LoadCalculation();
        }

        private void SetPagingSetting()
        {
            if (_tempCount <= 0)
            {
                pagingCtl.Visible = false;
            }
            else
            {
                pagingCtl.Visible = true;
            }
            var totalPages = (int)Utils.Helper.Util.CalculateTotalPages(_tempCount.Value,
                                                                        int.Parse(pagingCtl.DDLRecords.SelectedValue));
            pagingCtl.LabelTotalPage.Text = (totalPages).ToString();
            pagingCtl.LabelCurrentPage.Text = _tempCurrentPage.ToString();

            if (_tempCurrentPage <= 1)
            {
                pagingCtl.ButtonPrev.Enabled = false;
            }
            else
            {
                pagingCtl.ButtonPrev.Enabled = true;
            }
            if (_tempCurrentPage < totalPages)
            {
                pagingCtl.ButtonNext.Enabled = true;
            }
            else
            {
                pagingCtl.ButtonNext.Enabled = false;
            }
        }

        protected void gvw_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if ( e.Row.RowType == DataControlRowType.DataRow)
            {
                var data = e.Row.DataItem as CalcGetNewCalculationListResult;

                if (data.IsAttendanceComplete.Value != 1)
                {
                    var ctl = e.Row.FindControl("lblId") as WebControl;
                    ctl.BorderWidth = new Unit(1);
                    ctl.BorderColor = Color.Red;


                    ctl.ToolTip = Resources.Messages.AttendanceNotCompleteForCalculationMsg;


                    isValidAllValid = false;
                }

                if (data.IsSalaryGenerationUnsuccessfull)
                {
                    var ctl = e.Row.FindControl("lblId") as WebControl;
                    ctl.BorderWidth = new Unit(1);
                    ctl.BorderColor = Color.Red;


                    ctl.ToolTip = Resources.Messages.CalcSalaryGenerationFail;


                    isValidAllValid = false;
                }
            }
        }

        protected void gvw_DataBound(object sender, EventArgs e)
        {
            CalculationManager.CreateGroupInHeader(sender
            , gvw.Columns[gvw.Columns.Count - 1 ].Visible );
        }
    }
}
