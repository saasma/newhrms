﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AnnualTaxCtl.ascx.cs"
    Inherits="Web.UserControls.AnnualTaxCtl" %>
<script type="text/javascript">

    var skipLoadingCheck = true;

 

    var getFormattedAmount1 = function (value, e1, e2) {

        if (e2.data.SourceId == 300)
            return "";

        var vall = parseFloat(value);
        if (isNaN(vall) || vall == 0)
            return "-";

        return getFormattedAmount(value);
    }

    var getRowClass = function (record) {

        var status = record.data.SourceId;


        if (status == 300) {
            return "header12";
        }
        if (status == 301) {
            return "header12";
        }
        else //if(dayValue=="Working Day")
        {
            return "";
        }
        //else 

    };

    function load() {
        <%=btnLoad.ClientID %>.fireEvent('click');
    }
</script>
<style type="text/css">
   
    .header12, .header12 a, .header12 td, .header12 div
    {
        color: #000000;
        background-color: #D9D9D9 !important;
        font-weight: bold;
    }
    
   <%-- .bodypart
    {
        width: inherit;
        padding-right: 20px;
    }--%>
</style>
<div class="contentArea">
    <ext:ResourceManager ID="ResourceManager1" Namespace="" runat="server" />
    <h4>
        Annual Tax Report</h4>
    <div class="attribute" style='padding: 10px;'>
        <table>
            <tr>
                <td>
                    <ext:ComboBox ID="cmbYear" Width="200px" runat="server" ValueField="FinancialDateId"
                        DisplayField="Name" FieldLabel="Year" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                        QueryMode="Local">
                        <Store>
                            <ext:Store ID="Store1" runat="server">
                                <Model>
                                    <ext:Model ID="Model2" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="FinancialDateId" Type="String" />
                                            <ext:ModelField Name="Name" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                </td>
                <td style='padding-left: 20px;'>
                    <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                        <Proxy>
                            <ext:AjaxProxy Url="../Handler/EmpSearchID.ashx">
                                <ActionMethods Read="GET" />
                                <Reader>
                                    <ext:JsonReader Root="plants" TotalProperty="total" />
                                </Reader>
                            </ext:AjaxProxy>
                        </Proxy>
                        <Model>
                            <ext:Model ID="Model5" IDProperty="EmployeeId" Name="ItemLineModel" runat="server">
                                <Fields>
                                    <ext:ModelField Name="Name" Type="String" />
                                    <ext:ModelField Name="EmployeeId" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                    <ext:ComboBox LabelSeparator="" ID="cmbEmpSearch" FieldLabel="Search Employee" EmptyText="Employee Name"
                        LabelAlign="Top" runat="server" DisplayField="Name" ValueField="EmployeeId" StoreID="storeSearch"
                        TypeAhead="false" Width="200" PageSize="9999" HideBaseTrigger="true" MinChars="1"
                        TriggerAction="All" ForceSelection="false">
                        <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                            <ItemTpl ID="ItemTpl2" runat="server">
                                <Html>
                                    <tpl>
                                                        <div class="search-item">
                                                                        <span>{Name}</span>  
                                                         </div>
					                            </tpl>
                                </Html>
                            </ItemTpl>
                        </ListConfig>
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                        </Triggers>
                        <Listeners>
                            <Select Handler="this.getTrigger(0).show();" />
                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                            <TriggerClick Handler="if (index == 0) { 
                                                       this.clearValue(); 
                                                       this.getTrigger(0).hide();
                                                   }" />
                        </Listeners>
                    </ext:ComboBox>
                </td>
                <td style='padding-left: 20px;' valign="bottom">
                    <ext:Button ID="btnLoad" Hidden="true" runat="server" Height="30" Text="Load" Width="100" 
                       >
                        <DirectEvents>
                            <Click OnEvent="btnLoad_Click">
                                <EventMask ShowMask="true" />
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                    <asp:Button runat="server" ID="btn" CssClass="save" Text="Load" OnClientClick="load();return false;" />
                </td>
                <td style="padding-left: 500px;">
                    <asp:Button ID="btnUpdate" CssClass="excel" runat="server" Text="Export" OnClick="btnUpdate_Click" />
                </td>
            </tr>
        </table>
        <asp:HiddenField ID="hiddenEmployeeID" runat="server" />
    </div>
    <div class="clear">
        <ext:GridPanel StyleSpec="margin-top:15px;" ID="grid" runat="server" Cls="itemgrid">
            <Store>
                <ext:Store ID="storeEmpList" runat="server" AutoLoad="true">
                    <Model>
                        <ext:Model ID="Model1" runat="server">
                            <Fields>
                                <ext:ModelField Name="SourceId" Type="Int" />
                                <ext:ModelField Name="RowNumber" Type="String" />
                                <ext:ModelField Name="Name" Type="String" />
                                <ext:ModelField Name="Description" Type="String" />
                                <ext:ModelField Name="Total" Type="String" />
                                <ext:ModelField Name="OneTime" Type="String" />
                                <ext:ModelField Name="M4Amount" Type="String" />
                                <ext:ModelField Name="M4AddOn" Type="String" />
                                <ext:ModelField Name="M5Amount" Type="String" />
                                <ext:ModelField Name="M5AddOn" Type="String" />
                                <ext:ModelField Name="M6Amount" Type="String" />
                                <ext:ModelField Name="M6AddOn" Type="String" />
                                <ext:ModelField Name="M7Amount" Type="String" />
                                <ext:ModelField Name="M7AddOn" Type="String" />
                                <ext:ModelField Name="M8Amount" Type="String" />
                                <ext:ModelField Name="M8AddOn" Type="String" />
                                <ext:ModelField Name="M9Amount" Type="String" />
                                <ext:ModelField Name="M9AddOn" Type="String" />
                                <ext:ModelField Name="M10Amount" Type="String" />
                                <ext:ModelField Name="M10AddOn" Type="String" />
                                <ext:ModelField Name="M11Amount" Type="String" />
                                <ext:ModelField Name="M11AddOn" Type="String" />
                                <ext:ModelField Name="M12Amount" Type="String" />
                                <ext:ModelField Name="M12AddOn" Type="String" />
                                <ext:ModelField Name="M1Amount" Type="String" />
                                <ext:ModelField Name="M1AddOn" Type="String" />
                                <ext:ModelField Name="M2Amount" Type="String" />
                                <ext:ModelField Name="M2AddOn" Type="String" />
                                <ext:ModelField Name="M3Amount" Type="String" />
                                <ext:ModelField Name="M3AddOn" Type="String" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>
            <ColumnModel ID="ColumnModel1" runat="server">
                <Columns>
                    <ext:Column ID="colFromDate" runat="server" Text="SN" DataIndex="RowNumber" MenuDisabled="false"
                        Locked="true" Sortable="false" Align="Center" Width="50" />
                    <ext:Column ID="Column_Name" runat="server" Text="Description" DataIndex="Name" MenuDisabled="false"
                        Locked="true" Sortable="false" Align="Left" Width="180" />
                    <ext:Column ID="Column27" runat="server" Text="Comment" DataIndex="Description" MenuDisabled="false"
                        Locked="true" Sortable="false" Align="Left" Width="120" />
                    <ext:Column ID="Column10" runat="server" Text="Annual Amount" DataIndex="Total" MenuDisabled="false"
                        Locked="true" Sortable="false" Align="Right" Width="100">
                        <Renderer Fn="getFormattedAmount1" />
                    </ext:Column>
                    <ext:Column Hidden="true" ID="Column11" runat="server" Text="None Cash" DataIndex="OneTime"
                        MenuDisabled="false" Sortable="false" Align="Right" Width="100">
                        <Renderer Fn="getFormattedAmount1" />
                    </ext:Column>
                    <ext:Column ID="ColumnM1" runat="server" Text="Sharwan" StyleSpec="background-color:#FCE4D6"
                        DataIndex="M4Amount" MenuDisabled="false" Sortable="false" Align="Right" Width="90">
                        <Renderer Fn="getFormattedAmount1" />
                    </ext:Column>
                    <ext:Column ID="Column2" runat="server" Text="Addon" DataIndex="M4AddOn" MenuDisabled="false"
                        Sortable="false" Align="Right" Width="90">
                        <Renderer Fn="getFormattedAmount1" />
                    </ext:Column>
                    <ext:Column ID="ColumnM2" runat="server" Text="Bhadra" StyleSpec="background-color:#FCE4D6"
                        DataIndex="M5Amount" MenuDisabled="false" Sortable="false" Align="Right" Width="90">
                        <Renderer Fn="getFormattedAmount1" />
                    </ext:Column>
                    <ext:Column ID="Column4" runat="server" Text="Addon" DataIndex="M5AddOn" MenuDisabled="false"
                        Sortable="false" Align="Right" Width="90">
                        <Renderer Fn="getFormattedAmount1" />
                    </ext:Column>
                    <ext:Column ID="ColumnM3" runat="server" Text="Ashwin" StyleSpec="background-color:#FCE4D6"
                        DataIndex="M6Amount" MenuDisabled="false" Sortable="false" Align="Right" Width="90">
                        <Renderer Fn="getFormattedAmount1" />
                    </ext:Column>
                    <ext:Column ID="Column6" runat="server" Text="Addon" DataIndex="M6AddOn" MenuDisabled="false"
                        Sortable="false" Align="Right" Width="90">
                        <Renderer Fn="getFormattedAmount1" />
                    </ext:Column>
                    <ext:Column ID="ColumnM4" runat="server" Text="Kartik" StyleSpec="background-color:#FCE4D6"
                        DataIndex="M7Amount" MenuDisabled="false" Sortable="false" Align="Right" Width="90">
                        <Renderer Fn="getFormattedAmount1" />
                    </ext:Column>
                    <ext:Column ID="Column8" runat="server" Text="Addon" DataIndex="M7AddOn" MenuDisabled="false"
                        Sortable="false" Align="Right" Width="90">
                        <Renderer Fn="getFormattedAmount1" />
                    </ext:Column>
                    <ext:Column ID="ColumnM5" runat="server" Text="Mangsir" StyleSpec="background-color:#FCE4D6"
                        DataIndex="M8Amount" MenuDisabled="false" Sortable="false" Align="Right" Width="90">
                        <Renderer Fn="getFormattedAmount1" />
                    </ext:Column>
                    <ext:Column ID="Column12" runat="server" Text="Addon" DataIndex="M8AddOn" MenuDisabled="false"
                        Sortable="false" Align="Right" Width="90">
                        <Renderer Fn="getFormattedAmount1" />
                    </ext:Column>
                    <ext:Column ID="ColumnM6" runat="server" Text="Poush" StyleSpec="background-color:#FCE4D6"
                        DataIndex="M9Amount" MenuDisabled="false" Sortable="false" Align="Right" Width="90">
                        <Renderer Fn="getFormattedAmount1" />
                    </ext:Column>
                    <ext:Column ID="Column14" runat="server" Text="Addon" DataIndex="M9AddOn" MenuDisabled="false"
                        Sortable="false" Align="Right" Width="90">
                        <Renderer Fn="getFormattedAmount1" />
                    </ext:Column>
                    <ext:Column ID="ColumnM7" runat="server" Text="Magh" StyleSpec="background-color:#FCE4D6"
                        DataIndex="M10Amount" MenuDisabled="false" Sortable="false" Align="Right" Width="90">
                        <Renderer Fn="getFormattedAmount1" />
                    </ext:Column>
                    <ext:Column ID="Column16" runat="server" Text="Addon" DataIndex="M10AddOn" MenuDisabled="false"
                        Sortable="false" Align="Right" Width="90">
                        <Renderer Fn="getFormattedAmount1" />
                    </ext:Column>
                    <ext:Column ID="ColumnM8" runat="server" Text="Falgun" StyleSpec="background-color:#FCE4D6"
                        DataIndex="M11Amount" MenuDisabled="false" Sortable="false" Align="Right" Width="90">
                        <Renderer Fn="getFormattedAmount1" />
                    </ext:Column>
                    <ext:Column ID="Column18" runat="server" Text="Addon" DataIndex="M11AddOn" MenuDisabled="false"
                        Sortable="false" Align="Right" Width="90">
                        <Renderer Fn="getFormattedAmount1" />
                    </ext:Column>
                    <ext:Column ID="ColumnM9" runat="server" Text="Chaitra" StyleSpec="background-color:#FCE4D6"
                        DataIndex="M12Amount" MenuDisabled="false" Sortable="false" Align="Right" Width="90">
                        <Renderer Fn="getFormattedAmount1" />
                    </ext:Column>
                    <ext:Column ID="Column20" runat="server" Text="Addon" DataIndex="M12AddOn" MenuDisabled="false"
                        Sortable="false" Align="Right" Width="90">
                        <Renderer Fn="getFormattedAmount1" />
                    </ext:Column>
                    <ext:Column ID="ColumnM10" runat="server" Text="Baisakh" StyleSpec="background-color:#FCE4D6"
                        DataIndex="M1Amount" MenuDisabled="false" Sortable="false" Align="Right" Width="90">
                        <Renderer Fn="getFormattedAmount1" />
                    </ext:Column>
                    <ext:Column ID="Column22" runat="server" Text="Addon" DataIndex="M1AddOn" MenuDisabled="false"
                        Sortable="false" Align="Right" Width="90">
                        <Renderer Fn="getFormattedAmount1" />
                    </ext:Column>
                    <ext:Column ID="ColumnM11" runat="server" Text="Jestha" StyleSpec="background-color:#FCE4D6"
                        DataIndex="M2Amount" MenuDisabled="false" Sortable="false" Align="Right" Width="90">
                        <Renderer Fn="getFormattedAmount1" />
                    </ext:Column>
                    <ext:Column ID="Column24" runat="server" Text="Addon" DataIndex="M2AddOn" MenuDisabled="false"
                        Sortable="false" Align="Right" Width="90">
                        <Renderer Fn="getFormattedAmount1" />
                    </ext:Column>
                    <ext:Column ID="ColumnM12" runat="server" Text="Ashadh" StyleSpec="background-color:#FCE4D6"
                        DataIndex="M3Amount" MenuDisabled="false" Sortable="false" Align="Right" Width="90">
                        <Renderer Fn="getFormattedAmount1" />
                    </ext:Column>
                    <ext:Column ID="Column26" runat="server" Text="Addon" DataIndex="M3AddOn" MenuDisabled="false"
                        Sortable="false" Align="Right" Width="90">
                        <Renderer Fn="getFormattedAmount1" />
                    </ext:Column>
                </Columns>
            </ColumnModel>
            <View>
                <ext:GridView EnableTextSelection="true" ID="GridView1" runat="server">
                    <GetRowClass Fn="getRowClass" />
                </ext:GridView>
            </View>
        </ext:GridPanel>
    </div>
</div>
