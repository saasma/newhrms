﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MGBranchDepartment.ascx.cs"
    Inherits="Web.UserControls.MGBranchDepartment" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<script type="text/javascript">
        



        function ValidateTextBox1(source, args) {
            args.IsValid = true;
            if ($('#' + source.id).prop('disabled') == false) {
                if (args.Value == "")
                    args.IsValid = false;                   
            }
        }

        //capture window closing event
       <%
        if( IsDisplayedAsPopup)
        {
        
            Response.Write("window.onunload = closePopup;");
        }
         %>
        
        function closePopup() {
            clearUnload();
             if ($.browser.msie == false && typeof (window.opener.parentReloadCallbackFunction) != 'undefined' && ($.browser.chrome ||  $.browser.safari)) {
                window.opener.parentReloadCallbackFunction("ReloadBranch", window, texts);
            } else {
                if (typeof (texts) != 'undefined')
                    window.returnValue = texts;
                window.close();
             }
         }

        function clearUnload() {
            window.onunload = null;
        }
        
     
       
       function handleDelete()
       {            
            if(confirm('Do you want to delete branch?'))
            {
                clearUnload(); 
                return true;
            }
            else
                return false;
       }
       
        
      

       

       
        function onChangeDistrict(ddl) {
            Web.PayrollService.GetVDCByDistrict(ddl.value, onChangeDistrictCallback);
        }

        function onChangeDistrictCallback(result) {

            var list = result;
            document.getElementById(vdcId).options.length = 1;
            for (i = 0; i < list.length; i++) {
                var menu = list[i];
                var htmlContent = String.format("<option value='{0}'>{1}</option>", menu.Key, menu.Value);
                $('#' + vdcId).append(htmlContent);
            }
            DisplayRemoteArea();
        }
        
        function showDetails()
        {
            $("#<%= details.ClientID %>").show("fast");
        }
        function hideDetails()
        {
        }      
     

        function popupEditBrDept(branchId){           
            var ret  = popupBrDept('Id=' + branchId);

            if(typeof(ret) != 'undefined'){
                if(ret == 'Reload'){
                    __doPostBack('Reload', '');
                }
            }

            return false;
        }

        function reloadBrDept(childWindow){
            childWindow.close();
            __doPostBack('Reload', '');
        }


</script>
<style type="text/css">
    h2.popstitle
    {
        color: #333333;
        font: bold 13px Arial;
        padding-bottom: 0px !important;
    }
    
    .marginal td{vertical-align:top;}
</style>
<div align="left">
    <div class="marginal">      
        <div style="clear: both">
        </div>
        <asp:GridView CssClass="table table-primary mb30 table-bordered table-hover" PagerStyle-HorizontalAlign="Center" PagerStyle-CssClass="defaultPagingBar"
            UseAccessibleHeader="true" ID="gvwBranches" CellPadding="3" Width="85%" runat="server"
            DataKeyNames="BranchId" OnRowCreated="gvwEmployees_RowCreated" AutoGenerateColumns="False"
            GridLines="None" AllowPaging="True"
            PageSize="300" OnPageIndexChanging="gvwBranches_PageIndexChanging">
            <Columns>
                <asp:TemplateField HeaderStyle-Width="50" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                    HeaderText="SN">
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    <ItemTemplate>
                        <%# Container.DisplayIndex + 1 %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="200px" ItemStyle-HorizontalAlign="Left"
                    DataField="Name" HeaderText="Branch Name">
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ControlStyle-Width="200px"  HeaderStyle-Width="200px"
                    DataField="Code" HeaderText="Branch Code">
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundField>
                <asp:TemplateField HeaderText="Departments" ControlStyle-Width="400px" HeaderStyle-HorizontalAlign="Left"
                    HeaderStyle-Width="400px">
                    <ItemTemplate>
                        <%# Eval("DepartmentList").ToString().Replace(",","<br/>") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderText="Head Office"  ControlStyle-Width="150px"  HeaderStyle-Width="150px"
                    ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <%# Utils.Web.UIHelper.GetYesImage( Eval("IsHeadOffice"),Page) %>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Change Department" ItemStyle-HorizontalAlign="Center"
                    HeaderStyle-Width="150px">
                    <ItemTemplate>
                        <%--<asp:ImageButton ID="ImageButton1" runat="server" OnClientClick="clearUnload();"
                            CommandName="Select" ImageUrl="~/images/edit.gif" />--%>
                        <asp:ImageButton ID="ImageButton1" runat="server" OnClientClick='<%# "return popupEditBrDept(" +  Eval("BranchId") + ");" %>'
                            CommandName="Select" ImageUrl="~/images/edit.gif" />
                    </ItemTemplate>
                    <HeaderStyle Width="150px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:TemplateField>
            </Columns>
            <RowStyle CssClass="odd" />
            <AlternatingRowStyle CssClass="even" />
            <SelectedRowStyle CssClass="selected" />
            <PagerStyle HorizontalAlign="Center" CssClass="defaultPagingBar"></PagerStyle>
            <EmptyDataTemplate>
                No Branch has been created.
            </EmptyDataTemplate>
        </asp:GridView>
        <uc2:MsgCtl ID="divMsgCtl" Width="920px" EnableViewState="false" Hide="true" runat="server" />
        <uc2:WarningCtl ID="divWarningMsg" Width="920px" EnableViewState="false" Hide="true"
            runat="server" />
        <div id="details" runat="server" class="bevel" style="margin-top: 20px;" visible="false">
            <div class="fields paddpop" style="padding: 10px 0pt 20px 0px ! important;">
                <table cellpadding="4px">
                    <tr>
                        <td colspan="3">
                            <h2 id="branchTitle" class="popstitle" runat="server">
                                Add Departments to
                            </h2>
                        </td>
                    </tr>
                    <tr>
                        <td class="fieldHeader">
                            <strong>Available Departments</strong>
                            <asp:ListBox ID="lstSource" runat="server" SelectionMode="Multiple" Width="300" Height="300"
                                DataValueField="DepartmentId" DataTextField="Name"></asp:ListBox>
                            <asp:RequiredFieldValidator ID="test" runat="server" ValidationGroup="add" ControlToValidate="lstSource"
                                Display="None" ErrorMessage="Department should be selected for addition." />
                        </td>
                        <td style="text-align: center">
                            <asp:Button Text=">" runat="server" Width="50" ToolTip="Add" ID="btnAdd" OnClientClick="valGroup='add';return CheckValidation()"
                                OnClick="btnAdd_Click" />
                            <br />
                            <br />
                            <asp:Button Text="<" runat="server" OnClientClick="valGroup='remove';return CheckValidation()"
                                Width="50" ToolTip="Remove" ID="btnRemove" OnClick="btnRemove_Click" />
                            <br />
                            <br />
                            <asp:Button Text=">>" runat="server" Width="50" CausesValidation="false" ToolTip="Add All"
                                ID="btnAddAll" OnClick="btnAddAll_Click" />
                            <br />
                            <br />
                            <asp:Button Text="<<" runat="server" Width="50" CausesValidation="false" ToolTip="Remove All"
                                ID="btnRemoveAll" OnClick="btnRemoveAll_Click" />
                            <br />
                            <br />
                            <asp:CheckBox ID="chkToAll" runat="server" Text="To All Branch" />
                        </td>
                        <td>
                            <strong>Selected Departments</strong>
                            <br />
                            <asp:ListBox ID="lstDest" SelectionMode="Multiple" runat="server" Width="300" Height="300"
                                DataValueField="DepartmentId" DataTextField="Name"></asp:ListBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="remove"
                                ControlToValidate="lstDest" Display="None" ErrorMessage="Department should be selected for removal." />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="btnSave" CssClass="update" runat="server" Text="Save" ValidationGroup="AEBranch"
                                OnClick="btnSave_Click" />
                            <asp:Button ID="btnCancel" OnClientClick="clearUnload()" CssClass="cancel" runat="server"
                                Text="Cancel" OnClick="btnCancel_Click" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <%--</fieldset>--%>
    </div>
</div>
