using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Base;
using BLL.Manager;
using DAL;
using Utils.Helper;
using Utils;
using Utils.Web;
using System.Text;

namespace Web.UserControls
{
    public partial class MGBranch : BaseUserControl
    {
        CompanyManager compMgr = new CompanyManager();
        BranchManager branchMgr = new BranchManager();
        CommonManager commonMgr = new CommonManager();
        Branch bra = new Branch();
        List<Branch> source = new List<Branch>();

        public bool IsDisplayedAsPopup
        {
            get
            {
                if (Request.QueryString["isPopup"] != null)
                    return true;
                return false;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            divMsgCtl.DataBind();
            divWarningMsg.DataBind();

            LoadDistrict();
            LoadVDC();

            

            if (!IsPostBack)
            {
                LoadEmployees();
                Initialise();
                //chkIsManualAttendance.Visible = AttendanceManager.IsAttendanceDeviceIntegrated();

                
            }

            JavascriptHelper.AttachPopUpCode(Page, "popupBranch", "AEBranch.aspx", 600, 600);
            JavascriptHelper.AttachEnableDisablingJSCode(chkHasSeperateRetirementAC, txtSeperateRetirementAC.ClientID, false);
            JavascriptHelper.AttachEnableDisablingJSCode(chkHasSeperateEmpInsuraceAC, txtSeperateEmpInsuranceAC.ClientID, false);
            JavascriptHelper.AttachEnableDisablingJSCode(chkIsInRemoteArea, divRemoteArea.ClientID, false);
            ScriptControlID();

            if (gvwBranches.HeaderRow != null)
                gvwBranches.HeaderRow.TableSection = TableRowSection.TableHeader;

            Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "clearUnload();");
        }


        /// <summary>
        /// Method to output server control ID 
        /// </summary>
        protected void ScriptControlID()
        {
            Control[] ctls = { ddlDistricts, ddlVDCs };
            this.SaveControlClientIDInJScriptArrayVariable(null, ctls);
        }

        void Initialise()
        {
            string fundName;
            if (SessionManager.CurrentCompany.HasPFRFFund)
                fundName = SessionManager.CurrentCompany.PFRFName;
            else
                fundName = Resources.Messages.DefaultFundFullName;

             chkHasSeperateRetirementAC.Text = string.Format(
                    chkHasSeperateRetirementAC.Text, fundName);

             valCustomRFAC.ErrorMessage = string.Format(
                 valCustomRFAC.ErrorMessage, fundName);


            valRegPhone.ValidationExpression = Config.PhoneRegExp;
            valRegFax.ValidationExpression = Config.FaxRegExp;

            LoadBranches();
            //LoadDistrict();
        }

        void LoadBranches()
        {
            source = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            gvwBranches.DataSource = source;
            gvwBranches.DataBind();
        }

        void LoadDistrict()
        {
            string id = Request.Form[ddlDistricts.UniqueID];
            CommonManager mgr = new CommonManager();
            ddlDistricts.DataSource = mgr.GetRemoteDistricts();
            ddlDistricts.DataBind();
            if (id != null)
                UIHelper.SetSelectedInDropDown(ddlDistricts, id);
        }

        void LoadEmployees()
        {
            cmbBranchHead.DataSource = EmployeeManager.GetAllEmployees();
            cmbBranchHead.DataBind();
        }

        void LoadVDC()
        {
            string id = Request.Form[ddlVDCs.UniqueID];
            CommonManager mgr = new CommonManager();
            ddlVDCs.DataSource = mgr.GetAllVDCs(int.Parse(ddlDistricts.SelectedValue));
            ddlVDCs.DataBind();
            if (id != null)
                UIHelper.SetSelectedInDropDown(ddlVDCs, id);
        }
    


        Branch ProcessBranch(Branch b)
        {
            if (b != null)
            {
                txtBranchName.Text = b.Name;
                txtCode.Text = b.Code == null ? "" : b.Code;
                txtResponsiblePerson.Text = b.ResponsiblePerson;
                txtAddress.Text = b.Address;
                txtPhone.Text = b.Phone;
                txtFax.Text = b.Fax;
                txtEmail.Text = b.Email;
                chkIsInRemoteArea.Checked = b.IsInRemoteArea.Value;
                txtBankName.Text = b.BankName;
                txtBankAccountNo.Text = b.BankAccountNo;

                if (b.IsManualAttendance != null)
                    chkIsManualAttendance.Checked = b.IsManualAttendance.Value;
                else
                    chkIsManualAttendance.Checked = false;

                if (b.IsInRemoteArea.Value)
                {
                    UIHelper.SetSelectedInDropDown(ddlDistricts, b.VDCList.DistrictId.ToString());
                    LoadVDC();
                    //UIHelper.SetSelectedInDropDown(ddlDistricts, b.DistrictId.ToString());
                    UIHelper.SetSelectedInDropDown(ddlVDCs, b.VDCId.ToString());
                    lblRemoteAreaValue.Text = b.VDCList.RemoteArea.Value;
                }

                chkHasSeperateRetirementAC.Checked = b.HasSeperateRetirementFund.Value;
                chkHasSeperateEmpInsuraceAC.Checked = b.HasSeperateEmpInsurance.Value;

                if (b.HasSeperateRetirementFund.Value)
                {                    
                    txtSeperateRetirementAC.Text = b.RetirementFundACNo;
                }
                if (b.HasSeperateEmpInsurance.Value)
                {
                    txtSeperateEmpInsuranceAC.Text = b.EmpInsuranceACNo;
                }
                if (b.BranchHeadId != null)
                {
                    //cmbBranchHead.SelectedItem.Value = b.BranchHeadId.ToString();
                    UIHelper.SetSelectedInDropDown(cmbBranchHead, b.BranchHeadId.ToString());
                }
            }
            else
            {
                b = new Branch();
                b.Name = txtBranchName.Text.Trim();
                b.Code = txtCode.Text.Trim();
                b.ResponsiblePerson = txtResponsiblePerson.Text.Trim();
                b.Address = txtAddress.Text.Trim();
                b.Phone = txtPhone.Text.Trim();
                b.Fax = txtFax.Text.Trim();
                b.Email = txtEmail.Text.Trim();
                b.BankName = txtBankName.Text.Trim();
                b.BankAccountNo = txtBankAccountNo.Text.Trim();
                b.IsInRemoteArea = chkIsInRemoteArea.Checked;
                b.IsManualAttendance = chkIsManualAttendance.Checked;
                if (b.IsInRemoteArea.Value)
                {                   
                    //b.DistrictId = int.Parse(ddlDistricts.SelectedValue);
                    b.VDCId = int.Parse(ddlVDCs.SelectedValue);
                }
                b.CompanyId = SessionManager.CurrentCompanyId;
                b.HasSeperateRetirementFund = chkHasSeperateRetirementAC.Checked;
                b.HasSeperateEmpInsurance = chkHasSeperateEmpInsuraceAC.Checked;

                if (b.HasSeperateRetirementFund.Value)
                {
                    b.RetirementFundACNo = txtSeperateRetirementAC.Text.Trim();
                }
                if (b.HasSeperateEmpInsurance.Value)
                {
                    b.EmpInsuranceACNo = txtSeperateEmpInsuranceAC.Text.Trim();
                }

                if (ddlVDCs.SelectedValue!= "-1")
                {
                    //b.BranchHeadId = int.Parse(cmbBranchHead.SelectedItem.Value);
                    b.BranchHeadId = int.Parse(ddlVDCs.SelectedValue);
                }

                return b;
            }
            return null;
        }
        protected void gvwEmployees_RowCreated(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover",
                "this.originalClass=this.className;this.className='selected'");

                e.Row.Attributes.Add("onmouseout",
                "this.className=this.originalClass;");


            }

        }


        protected void gvwBranches_SelectedIndexChanged(object sender, EventArgs e)
        {
            int selBranchId = (int)gvwBranches.DataKeys[gvwBranches.SelectedIndex][0];
            Branch branch = branchMgr.GetById(selBranchId);
            if (branch != null)
            {
                ProcessBranch(branch);
                btnSave.Text = Resources.Messages.Update;
                details.Visible = true;
                txtBranchName.Focus();
            }
        }

        void ClearBranchFields()
        {
            txtBranchName.Text = "";
            txtCode.Text = "";
            txtResponsiblePerson.Text = "";
            txtAddress.Text = "";
            txtPhone.Text = "";
            txtFax.Text = "";
            txtEmail.Text = "";
            txtBankName.Text = "";
            txtBankAccountNo.Text = "";
            chkIsInRemoteArea.Checked = false;
            chkHasSeperateEmpInsuraceAC.Checked = false;
            chkHasSeperateRetirementAC.Checked = false;
            txtSeperateRetirementAC.Text = "";
            txtSeperateEmpInsuranceAC.Text = "";
            ddlDistricts.SelectedIndex = 0;
            ddlVDCs.SelectedIndex = 0;
            chkIsManualAttendance.Checked = false;

            btnSave.Text = Resources.Messages.Save;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
           
            if (chkIsInRemoteArea.Checked == false)
            {
                valCustDistrict.Enabled = false;
                valCustVDC.Enabled = false;
            }
            Page.Validate("AEBranch");
            if (Page.IsValid)
            {
                Branch b = ProcessBranch(null);

                if (gvwBranches.SelectedIndex != -1)
                {
                    int selBranchId = (int)gvwBranches.DataKeys[gvwBranches.SelectedIndex][0];
                    b.BranchId = selBranchId;
                    b.CompanyId = SessionManager.CurrentCompanyId;
                    if (branchMgr.Update(b))
                    {
                        divMsgCtl.InnerHtml = Resources.Messages.BranchUpdatedMsg;
                        divMsgCtl.Hide = false;
                    }
                    else
                    {
                        divWarningMsg.InnerHtml = "Branch name already exists.";
                        divWarningMsg.Hide = false;
                        return;
                    }
                    //JavascriptHelper.DisplayClientMsg("Branch information updated.", Page);
                }
                else
                {
                    b.CompanyId = SessionManager.CurrentCompanyId;
                    if (branchMgr.Save(b))
                    {
                        divMsgCtl.InnerHtml = Resources.Messages.BranchSavedMsg;
                        divMsgCtl.Hide = false;
                    }
                    else
                    {
                        divWarningMsg.InnerHtml = "Branch name already exists.";
                        divWarningMsg.Hide = false;
                        return;
                    }
                        //JavascriptHelper.DisplayClientMsg("Branch information saved.", Page);
                }
                CommonManager.ResetCache();
                gvwBranches.SelectedIndex = -1;
                LoadBranches();
                ClearBranchFields();
            }
            else
            {
                JavascriptHelper.DisplayClientMsg("Validation fail.", Page);
            }
            valCustDistrict.Enabled = true;
            valCustVDC.Enabled = true;
        }

        protected void gvwBranches_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvwBranches.PageIndex = e.NewPageIndex;
            gvwBranches.SelectedIndex = -1;
            LoadBranches();
            ClearBranchFields();
            
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            gvwBranches.SelectedIndex = -1;
            LoadBranches();
            ClearBranchFields();
            details.Visible = false;
        }

        protected void gvwBranches_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            CommonManager.ResetCache();
            int id = (int)gvwBranches.DataKeys[e.RowIndex][0];            
            if (id != 0)
            {
                string msg = "";
                bra.BranchId = id;
                if (branchMgr.Delete(bra,ref msg) == 1)
                {
                    LoadBranches();
                    ClearBranchFields();
                    divMsgCtl.InnerHtml = Resources.Messages.BranchDeletedMsg;
                    divMsgCtl.Hide = false;
                    details.Visible = false;
                    // JavascriptHelper.DisplayClientMsg("Branch information is deleted.", Page);
                }
                else
                {
                    divWarningMsg.InnerHtml = Resources.Messages.BranchIsInUseMsg;
                    divWarningMsg.Hide = false;
                    //JavascriptHelper.DisplayClientMsg("Branch is in use.", Page);
                }
            }

        }

        
        protected void Page_PreRender(object sender, EventArgs e)
        {
            //output all as js array to be updatable in parent window
            if (IsDisplayedAsPopup)
            {
                //Page.ClientScript.
                StringBuilder str = new StringBuilder("");
                bool first = true;
                foreach (Branch obj in source)
                {
                    if (first == false)
                        str.Append(",");
                    str.Append("'" + obj.BranchId + "$$" + obj.Name + "'");
                    first = false;
                }
                Page.ClientScript.RegisterArrayDeclaration("texts", str.ToString());
            }

            //set enabling/disabling
            if (chkIsInRemoteArea.Checked)
            {
                ddlDistricts.Attributes.Remove("disabled");
                ddlVDCs.Attributes.Remove("disabled");
                lblRemoteAreaValue.Text = commonMgr.GetRemoteAreaValue(int.Parse(ddlVDCs.SelectedValue));
                
            }
            else
            {
                ddlDistricts.Attributes.Add("disabled", "disabled");
                ddlVDCs.Attributes.Add("disabled", "disabled");
            }

            if (lblRemoteAreaValue.Text == string.Empty)
                lblRemoteAreaValue.Style.Add("display", "none");
            else
                lblRemoteAreaValue.Style.Remove("display");

            if (chkHasSeperateRetirementAC.Checked)
                txtSeperateRetirementAC.Attributes.Remove("disabled");
            else
                txtSeperateRetirementAC.Attributes.Add("disabled", "disabled");

            if (chkHasSeperateEmpInsuraceAC.Checked)
                txtSeperateEmpInsuranceAC.Attributes.Remove("disabled");
            else
                txtSeperateEmpInsuranceAC.Attributes.Add("disabled", "disabled");
        }

        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            LoadBranches();
            ClearBranchFields();
            details.Visible = true;
            txtBranchName.Focus();
            
        }
    }
}