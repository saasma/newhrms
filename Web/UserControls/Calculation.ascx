﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Calculation.ascx.cs"
    Inherits="Web.UserControls.Calculation" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/ErrorMsgCtl.ascx" TagName="ErrorMsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="InfoMsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<%@ Register TagPrefix="uc3" Namespace="Web.Controls" %>
<%@ Register Src="../Controls/MultiCheckCombo.ascx" TagName="MultiCheckCombo" TagPrefix="uc3" %>
<style type="text/css">
    th
    {
        text-align: center !important; /*border: 1px solid #DDDDDD !important;
        border-bottom: 0px !important;*/
        background-image: none, linear-gradient(to bottom, #F6F6F6 0px, #EAEAEA 100%);
        overflow: hidden;
    }
    .tableLightColor th
    {
        padding: 2px 2px !important;
        vertical-align: middle;
        padding-left: 1px !important;
        border-top: 1px solid #BFD9FB !important;
        border-left: 1px solid #BFD9FB !important;
        border-right: 1px solid #BFD9FB !important;
        border-bottom: 1px solid #BFD9FB !important;
    }
    .tableLightColor th{font-size: 12px;}
    .tableLightColor td span
    {
        padding: 2px 2px !important;
    }
    .tableLightColor
    {
        font-size: 11px;
    }
    td
    {

      
        padding: 0px !important;
        border: 0px !important;
        margin: 0px !important;
        line-height: 1.6em;
    }
    .odd
    {
        border-top: 1px solid #DFECFD !important;
        border-bottom: 1px solid #DFECFD !important;
    }
     .odd td {
            padding: 2px 2px !important;
        }

    .calculationInput {
        padding: 2px 2px !important;
        padding-left: 1px !important;
        background-color: #CCFF9A;
        border-top: 0px solid !important;
        border-left: 0px solid !important;
        border-bottom: 0px solid #DFECFD !important;
        border-right: 0px solid #DFECFD !important;
    }
    
    .nameHeaderClass
    {
        text-align: left !important;
        padding-left: 6px !important;
    }
    .employeeClass
    {
    	color:Black;
    }
    .tabDiv
    {
    	border-bottom: 2px solid #DFECFD;
        margin-left: 10px;
        clear: both;
        margin-top: 12px;
        margin-bottom: 5px;
        height: 26px;
        margin-right: 13px;
    }
    .tabDiv a
    {
    	display:inline-block;
    	width:100px;
    	color:#003F59;
    }
  .tabDiv div
{
    background-color:#fff;
    display: inline;
    width: 105px;
    padding-top: 8px;
    text-align: center;
    padding-bottom: 8px;
}
.tabDiv .selected
{
    background: white;
    border-bottom: 2px solid white;
    font-weight: bold;
    border-top: 2px solid #DFECFD;
    border-left: 2px solid #DFECFD;
    border-right: 2px solid #DFECFD;
    padding-top: 7px;
}

#wrapperDiv
{
  border: 1px solid #DFECFD;
}
.calculationInputDisabled {
    background-color: #eaf4f9 !important;
}

.bodypart{width:inherit;}
strong{margin-right:5px!important;}
    .employeeClass:hover{color:Black!important;text-decoration:none!important;;}
</style>
<script type="text/javascript">

    function showLoadingContent(element) {

        //alert($('#' + element.id).attr('disabled'));
        if ($('#' + element.id).prop('disabled') == 'disabled')
            return;


        $('#loadingStartingImage').show();
        $('#wrapperDiv').hide();
        $('#' + buttonsDiv + '').hide();
    }

    function requestSignOff() {
        showLoading();
        Web.PayrollService.SendSignOffMail(requestSignOffCallback);
    }

    function requestSignOffCallback(result) {
        hideLoading();
        if (result == true)
            alert("Signoff request sent.");

    }

    function retirementParentRefresh() {

        if (window.opener && !window.opener.closed && typeof (window.opener.refreshPage) != 'undefined') {
            window.opener.refreshPage();
        }
    }

    var buttonsDiv = '<%= pnlButtons.ClientID %>';

    $(document).ready(
            function () {
                ////			 
                //$('.tableLightColor tbody').attr('id', 'scrollMe');
                //var sth = new ScrollTableHeader();
                //sth.dynamicRowIndex = 1;
                //			 
                //sth.addTbody("scrollMe");
                ////                sth.delayAfterScroll = 150;
                ////                sth.minTableRows  = 10;

                $('#loadingStartingImage').hide();
                $('#wrapperDiv').show();
				
				gridView = $('#<%=gvw.ClientID%>').gridviewScroll({
                    width: 1200,
                    height: 650,
                    freezesize: 0,
                    headerrowcount: 2,
                });

                //  $(':text').('.adjustmentIncome').focus(function () { selectAllText($(this)) });

            }
			);


    function refreshSalaryDay() {
        window.location.href = 'Calculation.aspx';
    }
    function showHistory() {
        Web.PayrollService.GetCalculationHistory(document.getElementById('<%=ddlPayrollPeriods.ClientID %>').value, showHistoryCallback);
    }

    function showHistoryCallback(result) {
        document.getElementById("divHistory").innerHTML = result;
    }

    function showReminingToBeSavedList() {
        Web.PayrollService.GetSalarySaveRemainingList(document.getElementById('<%=ddlPayrollPeriods.ClientID %>').value, showReminingToBeSavedListCallback);
    }

    function showReminingToBeSavedListCallback(result) {
        document.getElementById("saveRemList").innerHTML = result;

    }
    function setCurrency(value) {
        if (value != "")
            document.getElementById('<%= currencyValue.ClientID %>').innerHTML = value;
    }

    function ACE_item_selected(source, eventArgs) {
        var value = eventArgs.get_value();
        document.getElementById('<%= hiddenEmployeeID.ClientID %>').value = value;
    }

    function toggleFilterArrow() {
        var ele = document.getElementById("toggleText");
        var image = document.getElementById("imgupdown");
        var text = document.getElementById("displayText");
        if (ele.style.display == "block") {
            ele.style.display = "none";
            //            image.src = "../images/arrowdown.png";
        }
        else {
            ele.style.display = "block";
            //            image.src = "../images/arrowup.png";
        }
    } 

</script>
<img src='<%= Page.ResolveUrl("~/images/saving.gif") %>' id="loadingStartingImage"
    alt="" class="loadingStartingImage" />
<div class="pageheader">
    <div class="media">
        <div class="media-body">
            <h4>
                Salary Calculation
            </h4>
        </div>
    </div>
</div>
<div class="contentpanel">
    <div class="contentArea" style="margin-top: 0px; margin-bottom: 0px">
        <div class="attribute" style="padding: 10px">
            <table>
                <tr>
                    <td>
                        <strong>Payroll </strong>
                    </td>
                    <td style='padding-left: 5px!important;'>
                    </td>
                    <td style='padding-left: 5px!important;'>
                        <strong>Search </strong>
                    </td>
                    <td style='padding-left: 5px!important;'>
                        <strong runat="server" id="lblBranch">Branch</strong>
                    </td>
                    <td style='padding-left: 5px!important;'>
                        <strong>Department</strong>
                    </td>
                   <%-- <td style='padding-left: 5px!important;'>
                        <strong>Sub Dep</strong>
                    </td>--%>
                    <td style='padding-left: 5px!important;'>
                        <strong>Status</strong>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:DropDownList Width="125px" onchange="showLoadingContent(this)" AppendDataBoundItems="true"
                            DataTextField="Name" DataValueField="PayrollPeriodId" ID="ddlPayrollPeriods"
                            runat="server" AutoPostBack="True">
                        </asp:DropDownList>
                    </td>
                    <td style='padding-left: 5px!important;'>
                        <asp:CheckBox ID="chkHasRetiredOrResigned" AutoPostBack="true" OnCheckedChanged="chkHasRetiredOrResigned_Changed"
                            runat="server" Text="Retired only" Visible="false" />
                    </td>
                    <td style='padding-left: 5px!important;'>
                        <asp:TextBox ID="txtEmpSearch" runat="server" OnTextChanged="txtEmpSearch_TextChanged"
                            AutoPostBack="true" Width="160" />
                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtEmpSearch"
                            WatermarkText="Search Employee" WatermarkCssClass="searchBoxText" />
                        <cc1:AutoCompleteExtender EnableCaching="true" ID="AutoCompleteExtenderOrganization"
                            runat="server" MinimumPrefixLength="2" ServiceMethod="GetEmployeeNamesAndEINWithRetiredAlso"
                            ServicePath="~/PayrollService.asmx" TargetControlID="txtEmpSearch" CompletionSetCount="10"
                            CompletionInterval="250" OnClientItemSelected="ACE_item_selected" CompletionListCssClass="AutoExtender"
                            CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
                        </cc1:AutoCompleteExtender>
                    </td>
                    <td style='padding-left: 5px!important;'>
                        <asp:DropDownList ID="ddlBranch" AutoPostBack="true" AppendDataBoundItems="true"
                            DataTextField="Name" DataValueField="BranchId" runat="server" Width="150px" OnSelectedIndexChanged="ddlBranch_SelectedIndexChanged">
                            <asp:ListItem Text="--Select Branch--" Value="-1" Selected="True" />
                        </asp:DropDownList>
                    </td>
                    <td style='padding-left: 5px!important;'>
                        <asp:DropDownList ID="ddlDepartments" AutoPostBack="true" AppendDataBoundItems="true"
                            DataTextField="Name" DataValueField="DepartmentId" runat="server" Width="150px"
                            OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged">
                            <asp:ListItem Text="--Select Department--" Value="-1" Selected="True" />
                        </asp:DropDownList>
                    </td>
                   <%-- <td style='padding-left: 5px!important;'>
                       
                    </td>--%>
                    <td style='padding-left: 5px!important;'>
                        <asp:DropDownList ID="ddlStatus" Style='display: none' AutoPostBack="true" AppendDataBoundItems="true"
                            DataTextField="Name" DataValueField="StatusID" runat="server" Width="120px" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged">
                            <asp:ListItem Text="--Select Status--" Value="-1" Selected="True" />
                        </asp:DropDownList>
                        <uc3:MultiCheckCombo ID="multiStatus" runat="server" />
                        <asp:Button ID="btnLoad" Visible="false" OnClick="btnLoad_Click" runat="server" Style="height: 26px"
                            Text="Load" />
                    </td>
                    <td style='padding-left: 5px!important;'>
                        <asp:Button CssClass="btn btn-default btn-sect btn-sm" ID="btnReload" Width='100px'
                            ToolTip="Reload Calculation of current page" runat="server" Text="Load" Style='margin-left: 10px'>
                        </asp:Button>
                        <div style='display: inline; padding-left: 5px;'>
                            <asp:LinkButton ID="btnExport" runat="server" ToolTip="Export to Excel" Text="" OnClick="btnExport_Click"
                                CssClass=" excel marginRight" Style='float: right; margin-right: 0px; width: 30px;' />
                            <asp:LinkButton ID="btnExportCSV" ToolTip="Export to CSV" runat="server" Text=""
                                OnClick="btnExportCSV_Click" Style='float: right; margin-right: 0px; width: 30px;'>
                                <asp:Image ID="Image1" Style='width: 25px;' ImageUrl='~/images/csv_text.png' runat="server" />
                            </asp:LinkButton>
                        </div>
                    </td>
                    <td style="vertical-align: top">
                        &nbsp;
                        <button type="button" class="btn btn-default dropdown-toggle" onclick="toggleFilterArrow()"
                            data-toggle="dropdown" title="more filter.." style="height: 31px;">
                            <i class="fa fa-caret-down"></i>
                        </button>
                    </td>
                </tr>
                <tr>
                    <td colspan="8">
                        <div id="toggleText" style="display: none">
                            <table>
                                <tr>
                                    <td>
                                        <!--- other filter here-->
                                        <strong>Year</strong><br />
                                        <asp:DropDownList Width="125px" onchange="showLoadingContent(this)" OnSelectedIndexChanged="ddlFinancialYears_Change"
                                            AppendDataBoundItems="true" DataTextField="Name" DataValueField="FinancialDateId"
                                            ID="ddlFinancialYears" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td style='padding-left:10px!important'>
                                        <strong>Sub Dep</strong><br />
                                        <asp:DropDownList ID="ddlSubDepartments" AutoPostBack="true" AppendDataBoundItems="true"
                                            DataTextField="Name" DataValueField="SubDepartmentId" runat="server" Width="100px"
                                            OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged">
                                            <asp:ListItem Text="--Select--" Value="-1" Selected="True" />
                                        </asp:DropDownList>
                                    </td>
                                     <td style='padding-left:10px!important' runat="server" id="tdUnit">
                                        <strong>Unit</strong><br />
                                        <asp:DropDownList ID="ddlUnit" AutoPostBack="true" AppendDataBoundItems="true"
                                            DataTextField="Name" DataValueField="UnitID" runat="server" Width="100px"
                                            OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged">
                                            <asp:ListItem Text="--Select--" Value="-1" Selected="True" />
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
            <asp:HiddenField ID="hdnChangedInClientValues" runat="server" />
            <asp:HiddenField ID="hiddenEmployeeID" runat="server" />
        </div>
        <uc2:MsgCtl ID="msgCtl" EnableViewState="false" Hide="true" runat="server" />
        <uc2:InfoMsgCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />

       <uc2:InfoMsgCtl ID="divZeroizeWarning" EnableViewState="false" Hide="true" runat="server" />

        <uc2:ErrorMsgCtl Hide="true" ID="divErrorMsg" ErrorMessage="Values are not properly set, please resolve these cases first otherwise save button may not appear."
            style='display: none' runat="server" />
        <uc2:InfoMsgCtl Hide="true" ID="warningForNegativeAllowHeadsMsg" ErrorMessage="Values are not properly set so please resolve to proceed forward."
            style='display: none' runat="server" />
        <uc2:InfoMsgCtl Hide="true" ID="signOffWarning" ErrorMessage="Payroll has not been approved/signoff, please request Sign off."
            style='display: none' runat="server" />
    </div>
    <div class="viewhistory" style="float: none" runat="server" id="divPayrollSummary"
        visible="false">
        <div class="viewhistorydiv">
            <h2 style="font-size: 12px; margin-bottom: 2px">
                Payroll Summary
            </h2>
        </div>
        <ul style="margin-top: 4px;">
            <li>Total Employees for payroll - <span runat="server" id="spantotalEmployees"></span>
            </li>
            <li>Salary Saved Employees - <span runat="server" id="spansavedEmployees"></span>
            </li>
            <li>Retiring Employees - <span runat="server" id="spanRetiring"></span></li>
            <li onclick='showReminingToBeSavedList();'><a href='javascript:void(0)'>Salary Remaining
                to be saved</a> - <span runat="server" id="spanRem"></span></li>
        </ul>
        <div id="saveRemList" style="margin-left: 38px; color: lightseagreen;">
        </div>
    </div>
    <span runat="server" id="currencyValue" style='float: right; padding: 4px; margin-bottom: 3px;
        margin-right: 12px; background-color: #9cdaf9'></span><a href="javascript:void(0)"
            onclick="currencyPopup();return false;" runat="server" id="linkCurrencyRate"
            style='float: right; padding: 4px; padding-right: 10px'>Exchange Rate</a>
    <a href="javascript:void(0)" onclick="currencyHistoryPopup();return false;" runat="server"
        id="linkCurrencyHistory" style='float: right; padding: 4px; padding-right: 10px'>
        Exchange History</a>
    <div style="margin-left: 12px; display: none; clear: both" runat="server" id="divContractUptoDate">
        UnitRate Upto Date :
        <asp:Label ID="lblUnitRateToDate" runat="server" />
    </div>
    <div style="margin-left: 12px; display: none; float: right; margin-right: 10px; padding-bottom: 5px;
        clear: both; font-size: 14px;" runat="server" id="divSalaryProcessDay">
        Salary Processing Day :
        <asp:Label ID="lblSalaryProcessDate" runat="server" Style='color: #6AA83C; font-size: 14px;' />
        <a href="javascript:void(0)" onclick="salaryProcessingPopup();return false;" runat="server"
            id="A1" style='float: right; padding: 0px; padding-left: 10px; padding-right: 10px'>
            Change</a>
    </div>
    <div style='display: block; clear: both; margin-left: 15px;' runat="server" id="checkListSection">
        <strong>
            <asp:HyperLink runat="server" ID="checkListLink" Target="_blank" Style='color: red'
                NavigateUrl='~/CP/Extension/ManageCheckList.aspx' />
        </strong>
    </div>
    <%--<div style='clear: both'>
</div>--%>
    <div class="tabDiv" runat="server" id="typesDiv">
        <div class="selected" id="divCombined" runat="server" style='margin-left: 4px'>
            <asp:LinkButton ID="linkCombined" runat="server" Text="Combined" OnClick="linkRegular_Click" />
        </div>
        <div id="divRegular" runat="server">
            <asp:LinkButton ID="linkRegular" runat="server" Text="Regular" OnClick="linkRegular_Click" />
        </div>
        <div id="divAdjustment" runat="server">
            <asp:LinkButton ID="linkAdjustment" runat="server" Text="Adjustment" OnClick="linkRegular_Click" />
        </div>
        <div id="divArrear" runat="server">
            <asp:LinkButton ID="linkArrear" runat="server" Text="Arrear" OnClick="linkRegular_Click" />
        </div>
    </div>
    <div id="wrapperDiv" style="overflow: auto; overflow-y: hidden; display: none; margin-left: 0px;
        margin-right: 13px; clear: both">
        <cc2:EmptyDisplayGridView ID="gvw" CssClass="tableLightColor" UseAccessibleHeader="true"
            runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="CalculationId,EmployeeId,IsRetiredOrResigned,IsFinalSaved"
            GridLines="None" ShowFooterWhenEmpty="False" ShowHeaderWhenEmpty="True" EnableViewState="false"
            OnRowCreated="gvw_RowCreated" OnDataBound="gvw_DataBound">
            <Columns>
                <asp:TemplateField HeaderText="EIN" HeaderStyle-BackColor="#1B93D0" HeaderStyle-Width="42px"
                    ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <asp:Label ID="lblId" Width="40px" runat="server" Text='<%# Eval("EmployeeId") %>'></asp:Label>
                        <asp:HiddenField ID="incomeListId" runat="server" Value="" />
                        <asp:HiddenField ID="deductionListId" runat="server" Value="" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="I No" HeaderStyle-BackColor="#1B93D0" HeaderStyle-Width="72px"
                    ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <asp:Label ID="lblINo" Width="70px" runat="server" Text='<%# Eval("IDCardNo") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-BackColor="#1B93D0" HeaderStyle-CssClass="nameHeaderClass"
                    HeaderText="&nbsp;Name">
                    <ItemTemplate>
                        <%-- <asp:Label Width="150px" ID="Label2" Style='border-right: 1px solid #DDDDDD' runat="server"
                        Text='<%# "&nbsp;" + Eval("Name") %>'></asp:Label>--%>
                        <asp:HyperLink CssClass="employeeClass" Target="_blank" Width="150px" ID="Label2"
                            Style='border-right: 1px solid #DDDDDD' runat="server" NavigateUrl='<%# "~/newhr/EmployeePayroll.aspx?Id=" +  Eval("EmployeeId") %>'
                            Text='<%# "&nbsp;" + Eval("Name") %>'></asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-BackColor="#1B93D0" HeaderStyle-CssClass="nameHeaderClass"
                    HeaderStyle-Width="150px" HeaderText="&nbsp;Account No" HeaderStyle-HorizontalAlign="Right">
                    <ItemTemplate>
                        <asp:Label ID="lblAccountNo" Width="200px" Text='<%# Eval("BankACNo") %>' runat="server" />
                        <%--<asp:TextBox ID="txtNote" Height="20" Enabled='<%# Convert.ToInt32(Eval("CalculationId"))==0 %>'
                        Text='<%#Eval("Note") %>' Style="margin-left: 1px" runat="server" Width="100" />--%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Delete" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                    <HeaderTemplate>
                        <img alt="Delete" title="Delete" src="../images/delet.png" />
                        <br />
                        <asp:CheckBox ID="chkDelete" onclick="selectDeselectAll(this)" runat="server" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:CheckBox ID="chkDelete" ToolTip='<%# Convert.ToBoolean( Eval("IsFinalSaved")) == true ? "Salary already saved." : "" %>'
                            Enabled='<%# !Convert.ToBoolean( Eval("IsFinalSaved")) %>' Visible='<%# Convert.ToInt32(Eval("CalculationId"))!=0 %>'
                            runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <RowStyle CssClass="odd" />
            <AlternatingRowStyle CssClass="odd" />
            <EmptyDataTemplate>
                <b>No salary records.</b>
            </EmptyDataTemplate>
        </cc2:EmptyDisplayGridView>
    </div>
    <div style='padding-left: 0px; margin-right: 13px;'>
        <uc1:PagingCtl ID="pagingCtl" ShowDropDown="true" OnChangePage="ChangePageNumber"
            PageSizeChangedJS="showLoadingContent(this);" OnNextRecord="btnNext_Click" OnPrevRecord="btnPrevious_Click"
            OnDropDownShowRecordsChanged="ddlRecords_SelectedIndexChanged" runat="server" />
        <asp:Panel ID="pnlButtons" CssClass="clear buttonsDiv" Style='text-align: left;'
            runat="server">
            <div style='text-align: left; float: left; padding-left: 10px;'>
                <asp:Button ID="btnApprove" Width="120px" OnClientClick="if(confirm('{0}')==false) return false;"
                    Visible="false" runat="server" CssClass="save" Text="Approve" OnClick="btnApprove_Click" />
                <asp:Button ID="btnSaveFinal" Width="120px" OnClientClick="if(confirm('{0}')==false) return false;"
                    Visible="false" runat="server" CssClass="save" Text="Generate Tax" OnClick="btnSaveFinal_Click" />
                <asp:Button ID="btnGenerateArrearIncrement" Width="120px" OnClientClick="if(confirm('Confirm generate the arrear increment, if already generated then it will be recalculated?')==false) return false;"
                    Visible="true" runat="server" CssClass="save" Text="Generate Arrear" OnClick="btnGenerateArrearIncrement_Click" />
                <asp:HyperLink Text="Review the changes before Approval" runat="server" Visible="false"
                    Style="color: #0FA549; font-weight: bold; padding-right: 4px;" ID="liSignOff"
                    NavigateUrl="~/CP/Report/NewHrReport/VarianceSummary.aspx">
                </asp:HyperLink>
            </div>
            <div style='text-align: right; float: right'>
                <asp:Button ID="btnSave" CssClass="save" OnClientClick="if(confirm('Are you sure, you want to save the salary?')==false) return false; else {showLoadingContent(this);}"
                    runat="server" Text="Save" OnClick="btnSave_Click" />
                <asp:Button ID="btnEdit" Style='display: none' runat="server" Text="Edit" Enabled="false"
                    OnClick="btnEdit_Click" />
                <asp:Button ID="btnCancel" Style='display: none' CssClass="cancel" runat="server"
                    Text="Cancel" OnClick="btnCancel_Click" />
                <asp:Button ID="btnDelete" CssClass="delete" OnClientClick="if(confirm('Are you sure, you want to delete the salary?')==false) return false;"
                    runat="server" Text="Delete" Visible="false" OnClick="btnDelete_Click" />
                <asp:Button ID="btnDeleteAll" CssClass="delete" OnClientClick="if(confirm('Are you sure, you want to delete all salary?')==false) return false;"
                    runat="server" Text="Delete All" Visible="false" OnClick="btnAllDelete_Click" />
                
            </div>
          
        </asp:Panel>
           <div style='text-align: right; float: right;padding-top:10px;'>
                <asp:Button ID="btnArrearPostToAddOn" Width='150px' Visible="false" CssClass="save" OnClientClick="if(confirm('Are you sure, you want to post arrear to Add-On?')==false) return false; else {showLoadingContent(this);}"
                    runat="server" Text="Post Arrear to Add-On" OnClick="btnArrearPostToAddOn_Click" />
                <asp:Button ID="btnSetArrearToZero" Width='150px' Visible="false" CssClass="save" OnClientClick="if(confirm('Are you sure, you want to set all Arrear to Zero?')==false) return false; else {showLoadingContent(this);}"
                    runat="server" Text="Make Arrear Zero" OnClick="btnSetArrearToZero_Click" />
            </div>
        <div class="viewhistory">
            <div class="viewhistorydiv">
                <h2 style="font-size: 12px">
                    Changed History
                </h2>
            </div>
            <asp:LinkButton ID="btnDocumentHistory" OnClientClick="showHistory();return false;"
                runat="server" Text="View History" Icon="BulletArrowDown" Cls="viewbtn">
                <%--<DirectEvents>
                    <Click OnEvent="btnDocumentHistory_Click">
                        <EventMask ShowMask="true">
                        </EventMask>
                    </Click>
                </DirectEvents>--%>
            </asp:LinkButton>
            <div id="divHistory">
            </div>
            <%--<uc1:ViewDocumentHistory ID="ViewDocumentHistory1" runat="server" />--%>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('.container-fluid:first').toggleClass('menu-hidden');
</script>
