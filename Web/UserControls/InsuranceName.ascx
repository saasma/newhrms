﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InsuranceName.ascx.cs" Inherits="Web.UserControls.InsuranceName" %>

<script type="text/javascript">
    function ConfirmInsuranceNameDeletion() {
        var ddllist = document.getElementById('<%= ddlNames.ClientID %>');
        if (ddllist.selectedIndex == -1) {
            alert('No Insurance name to delete.');
            clearUnload(); 
            return false;
        }
        if (confirm('Do you want to delete the insurance name?')) {
            clearUnload(); 
            return true;
        }
        else {
            return false;
        }
    }

    function DisplayInsuranceInTextBox(dropdown_id) {
        var ddllist = document.getElementById(dropdown_id);
        document.getElementById('<%= txtInsuranceRename.ClientID %>').value = ddllist.options[ddllist.selectedIndex].text;
    }

        

    //capture window closing event
    window.onunload = closePopup;
    function closePopup() {
        clearUnload();
        if (typeof (texts) != 'undefined')
        window.returnValue = texts;
        window.close();
    }

    function clearUnload() {
        window.onunload = null;
    }

</script>

<h3 class="blue">
    Manage Insurance Name</h3>
<p>
    <span class="subHeading">New Insurance Name: </span>
    <br />
    <asp:TextBox ID="txtNewInsuranceName" runat="server" Width="159px" MaxLength="100" EnableViewState="false"></asp:TextBox>
    <asp:RequiredFieldValidator ValidationGroup="Name1" ID="RequiredFieldValidator3"
        Display="None" runat="server" ErrorMessage="Insurance name is required." ControlToValidate="txtNewInsuranceName">*</asp:RequiredFieldValidator>
    <asp:Button ID="btnAddInsuranceName" OnClientClick="valGroup='Name1';return CheckValidation()"
        runat="server" Text="Add" OnClick="btnAddInsuranceName_Click" />
</p>
<p>
    <span class="subHeading">Insurance Name List: </span>
    <br />
    <asp:DropDownList ID="ddlNames" runat="server" Width="165px" CssClass="rightMargin"
        DataValueField="Id" DataTextField="InsuranceName" onchange="DisplayInsuranceInTextBox(this.id);">
    </asp:DropDownList>
    <asp:Button ID="btnDelete" runat="server" Text="Delete" OnClientClick="javascript:return ConfirmInsuranceNameDeletion();"
        OnClick="btnDelete_Click" />
</p>
<p>
    <span class="subHeading">Rename Insurance Name:&nbsp;&nbsp;&nbsp;&nbsp; </span>
    <br />
    <asp:TextBox ID="txtInsuranceRename" runat="server" Width="159px" MaxLength="100"></asp:TextBox>
    <asp:RequiredFieldValidator ValidationGroup="Name2" ID="RequiredFieldValidator4"
        runat="server" ErrorMessage="Insurance name is required." ControlToValidate="txtInsuranceRename"
        Display="None">*</asp:RequiredFieldValidator>
    <asp:Button ID="btnRename" OnClientClick="valGroup='Name2';return CheckValidation();"
        runat="server" Text="Update" ValidationGroup="Name2" OnClick="btnRename_Click" />
</p>

<% if(IsDisplayedAsPopup){ %>
<input id="btnClose" type="button" value="Close" onclick="closePopup()" />
<%} %>