﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Base;
using BLL.Manager;
using DAL;
using System.Drawing;
using System.Text;
using Utils.Helper;
using Image = System.Web.UI.WebControls.Image;
using System.Web.Services;
using System.Data;
using Web.Master;
using Web.CP.Report.Templates.Pay;
using DevExpress.XtraReports.UI;
using Web.CP.Report;
using DevExpress.XtraPrinting;
using System.IO;
using BLL.BO;
using Utils.Calendar;

namespace Web.UserControls
{
  
    public partial class Calculation : BaseUserControl
    {

        
        public SalaryType ViewSalaryType
        {
            get
            {
                if (ViewState["SalType"] == null)
                    return SalaryType.Combined;
                return (SalaryType)int.Parse(ViewState["SalType"].ToString());
            }
            set
            {
                ViewState["SalType"] = (int)value;
            }
        }

        public bool IsHoldPaymentListing
        {
            get
            {
                if (Request.QueryString["HoldPayment"] != null)
                    return true;
                return false;
            }
        }
        public int HoldPaymentPayrollPeriodId
        {
            get
            {
                if (Request.QueryString["ID"] != null)
                    return int.Parse(Request.QueryString["ID"]);
                return 0;
            }
        }

        public int? QueryStringEmployeeID
        {
            get
            {
                if (Request.QueryString["EmployeeID"] != null)
                    return int.Parse(Request.QueryString["EmployeeID"]);
                return null;
            }
        }

        //in pixel unit
        public const int ColumnWidth = 72;
        public string ErrorList = "";
        public string WarningList = "";
        CalculationManager calcMgr = new CalculationManager();
        CommonManager commonMgr = new CommonManager();
        public CCalculation calculation = null;
        //public int decimalPlaces = 0;
        // To track if any cell value is invalid, set from "CalcGridViewTemplate
        public bool isValidAllValid = true;
        public bool isNegativeWarningSalaryExists = false;
        public bool isAttendanceNotSaved = false;
        // To track if the page is in edit mode or not, currently edit mode button is hidden so no functionality, as to effect for tax calc
        public bool editMode = false;

        // hold all the incomes types to be displayed in Calculation list
        List<CalcGetHeaderListResult> headers = new List<CalcGetHeaderListResult>();

        public Dictionary<string, string> headerList = new Dictionary<string, string>();
        private int payrollPeriodId = 0;
        private int payrollPeriodType = 0;
        private int newAddOnId = 0;

        public bool isHeaderMissingError=false;
        public int missingType = 0;
        public int missingSourceId = 0;
        public int missingEIN = 0;

        public int missingSavedType = 0;
        public int missingSavedSourceId = 0;

        List<CalcGetCalculationListResult> calculationList = new List<CalcGetCalculationListResult>();

        //public bool isIncomeAdjChecked = false;
        //public bool isDeductionAdjChecked = false;

        //private void RestoreAdjustmentCheckboxState()
        //{
        //    foreach (string key in Request.Form.AllKeys)
        //    {
        //        if (key.Contains(CalculationManager.idIncomeAdjCheckbox) && Request.Form[key].ToLower()=="on")
        //        {
        //            isIncomeAdjChecked = true;
        //        }
        //        if (key.Contains(CalculationManager.idDeductionAdjCheckbox) && Request.Form[key].ToLower() == "on")
        //        {
        //            isDeductionAdjChecked = true;
        //        }
        //    }
        //}


        public void btnGenerateArrearIncrement_Click(object sender, EventArgs e)
        {

            PayrollPeriod lastPeriod = CommonManager.GetLastPayrollPeriod();
            if (lastPeriod != null && lastPeriod.PayrollPeriodId == GetPayrollPeriodId())
            {

                BLL.BaseBiz.PayrollDataContext.GenerateAllArrearIncrement(GetPayrollPeriodId());
                Response.Redirect("Calculation.aspx");
            }
            else
            {
                divWarningMsg.InnerHtml = "Arrear can be generated for only last period.";
                divWarningMsg.Hide = false;
            }
        }


        // ViewState off for this page
        protected void Page_Load(object sender, EventArgs e)
        {
            this.ErrorList = "";
            this.WarningList = "";

            // For D2 Show/Hide Currency Rate
            //int periodId = 0;
            PayrollPeriod period = null;
            if (ddlPayrollPeriods.SelectedItem != null && ddlPayrollPeriods.SelectedItem.Value != null && ddlPayrollPeriods.SelectedItem.Value != "")
            {
                period = CommonManager.GetPayrollPeriod(int.Parse(ddlPayrollPeriods.SelectedItem.Value.Split(new char[]{':'})[0] ));
            }
            if (CommonManager.CompanySetting.IsD2 && period != null && CommonManager.IsD2MonthInDollar(period.Year.Value, period.Month) == true)
            {
                CurrencyRate rate = CommonManager.GetCurrencyRate();
                if (rate != null)
                    currencyValue.InnerHtml = string.Format(Resources.Messages.D2FixedCurrentRateText,
                        rate.FixedRateDollar, rate.CurrentRateDollar);

                linkCurrencyRate.Visible = true;
                currencyValue.Visible = true;
                linkCurrencyHistory.Visible = true;

            }
            else if (CommonManager.CompanySetting.WhichCompany == WhichCompany.BritishSchool )
            {
                CurrencyRate rate = CommonManager.GetCurrencyRate();
                if (rate != null)
                    currencyValue.InnerHtml = string.Format(Resources.Messages.CurrencyFixedRateText,
                        rate.FixedRateDollar);
            }
            else if (CommonManager.CompanySetting.IsAllIncomeInForeignCurrency)
            {
                CurrencyRate rate = CommonManager.GetCurrencyRate();
                if (rate != null)
                    currencyValue.InnerHtml = string.Format("Rate: Rs. {0}/{1}",
                        rate.FixedRateDollar,CalculationManager.CurrencyDetail.Currency);
            }
            else
            {
                linkCurrencyRate.Visible = false;
                currencyValue.Visible = false;
                linkCurrencyHistory.Visible = false;
            }

            //RestoreAdjustmentCheckboxState();

            if (!IsPostBack)
            {
                //CalculationManager.ClearVariableAndIncomeAjdustmentOnFirstLoad();
                List<Branch> branches = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId); ;
                if (branches.Count == 1)
                {
                    lblBranch.Visible = false;
                    ddlBranch.Visible = false;
                    //ddlBranch.SelectedIndex = 1;
                }
                ddlBranch.DataSource = branches;
                ddlBranch.DataBind();

                ddlDepartments.DataSource = new DepartmentManager().GetDepartmentsByCompany(SessionManager.CurrentCompanyId);
                ddlDepartments.DataBind();

                ddlSubDepartments.DataSource = new DepartmentManager().GetSubDepartmentsByCompany(SessionManager.CurrentCompanyId);
                ddlSubDepartments.DataBind();

                // If has EmployeeID in query string then it is called from Retirement page so mark as retired only here
                if (QueryStringEmployeeID != null)
                {
                    this.chkHasRetiredOrResigned.Checked = true;
                }
                //ddlDepartments.DataSource = new DepartmentManager().GetDepartmentsByCompany(SessionManager.CurrentCompanyId);
                //ddlDepartments.DataBind();
            }

            bool loadColumns = false;
            //if dropdown changes
            if (Request.Form["__EventTarget"] != null && 
                (Request.Form["__EventTarget"].Equals(ddlPayrollPeriods.ClientID.Replace("_", "$"))
                ||   Request.Form["__EventTarget"].Equals(txtEmpSearch.ClientID.Replace("_", "$"))
                || Request.Form["__EventTarget"].Equals(ddlDepartments.ClientID.Replace("_","$"))
                 || Request.Form["__EventTarget"].Equals(ddlBranch.ClientID.Replace("_", "$"))
                || Request.Form["__EventTarget"].Equals(btnReload.ClientID.Replace("_", "$"))
                 || Request.Form["__EventTarget"].Equals(btnExport.ClientID.Replace("_", "$"))
                  || Request.Form["__EventTarget"].Equals(btnExportCSV.ClientID.Replace("_", "$"))
                )             
                )
            {
                SplitPeriod(Request.Form[ddlPayrollPeriods.ClientID.Replace("_", "$")], ref payrollPeriodId, ref payrollPeriodType);
                //this.payrollPeriodId = int.Parse(Request.Form[ddlPayrollPeriods.ClientID.Replace("_", "$")]);
                //Initialise();
                _tempCurrentPage = 1;
                loadColumns = true;
            }

            if(IsPayrollPeriodDropDownChanged())
                this.ViewSalaryType = SalaryType.Combined;

            if(!Initialise())
                return;
            ;
            //AddColumns();
            //RegisterLegendColors();
           
            if (!IsPostBack)
            {
                _tempCurrentPage = 1;
                loadColumns = true;
               
            }
          
            //
            
            //if first time load & submitted by Delete/Save button then need to reload as ViewState is disabled
            // and to prevent always double loading when page index is changed

            if (!IsPostBack || (Request.Form["__EVENTTARGET"] != null && 
                ( !Request.Form["__EVENTTARGET"].ToLower().Contains("pagingctl"))
                 ///  ||  !Request.Form["__EVENTTARGET"].ToLower().Contains("txtEmpSearch")
                   )
               // )

                )
                //(Request.Form[btnSave.ClientID.Replace("_", "$")] != null
                //|| Request.Form[btnDelete.ClientID.Replace("_", "$")] != null
                //|| IsExportButtonSubmit()
                //|| IsPayrollPeriodDropDownChanged()
                //|| Request.Form[chkHasRetiredOrResigned.ClientID.Replace("_","$")] != null))
            {
                LoadCalculation(false, loadColumns);
            }

            RegisterScripts();

            JavascriptHelper.AttachPopUpCode(Page, "commentPopup", "PopupHtml.htm", 300, 300);
            JavascriptHelper.AttachNonDialogPopUpCode(Page, "currencyPopup", "CurrencySetting.aspx", 450, 500);

            JavascriptHelper.AttachNonDialogPopUpCode(Page, "currencyHistoryPopup", "CurrencyHistory.aspx", 450, 500);
            JavascriptHelper.AttachNonDialogPopUpCode(Page, "salaryProcessingPopup", "SalaryProcessingDate.aspx", 450, 500);
        }


     
        void SplitPeriod(string value,ref int payrollPeriodId,ref int periodType)
        {
            string[] values = value.Split(new char[] { ':'});
            payrollPeriodId = int.Parse(values[0]);
            periodType = int.Parse(values[1]);
            if (values.Length >= 3)
                newAddOnId = int.Parse(values[2]);
        }

        void RegisterScripts()
        {
            StringBuilder str = new StringBuilder();
            str.AppendFormat("var decimalPlaces = {0};", SessionManager.DecimalPlaces);
            // Generating txt names like txt-4-4, txt-10-10 for the total textboxes,
            //depends upon the naming generated in "CalcGridViewTemplate"
            str.AppendFormat("var txtIncomeSumTxt = 'txtd{0}';",
                             (((int)CalculationColumnType.IncomeGross)) +
                             ((int)CalculationColumnType.IncomeGross).ToString());
            str.AppendFormat("var txtDeductionSumTxt = 'txtd{0}';",
                            (((int)CalculationColumnType.DeductionTotal)) +
                            ((int)CalculationColumnType.DeductionTotal).ToString());

            str.AppendFormat("var txtNetTxt = 'txtd{0}';",
                            (((int)CalculationColumnType.NetSalary)) +
                            ((int)CalculationColumnType.NetSalary).ToString());

            
            str.AppendFormat("var msgCannotUnchecked = '{0}';", Resources.Messages.CalcCannotUncheckedForAdjustment);

            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "rounding",
                                                        str.ToString()
                                                        , true);
        }

        bool Initialise()
        {

            if (!IsPostBack)
            {
                List<UnitList> units = CommonManager.GetAllUnitList();
                if (units.Count >= 1)
                {

                    ddlUnit.DataSource = units;
                    ddlUnit.DataBind();
                    tdUnit.Visible = true;
                }
                else
                {
                    tdUnit.Visible = false;

                }
            }


            //ListItem firstItem = ddlPayrollPeriods.Items[0];
            ddlPayrollPeriods.Items.Clear();
            //ddlPayrollPeriods.Items.Insert(0,firstItem);
            List<PayrollPeriodBO> payrollPeriodList = new List<PayrollPeriodBO>();
            int yearId = 0;
            if (string.IsNullOrEmpty(ddlFinancialYears.SelectedValue))
            {
                payrollPeriodList = CommonManager.GetCurrentYear(SessionManager.CurrentCompanyId);
                yearId = SessionManager.CurrentCompanyFinancialDate.FinancialDateId;
            }
            else
            {
                payrollPeriodList = CommonManager.GetYear(int.Parse(ddlFinancialYears.SelectedValue));
                yearId = int.Parse(ddlFinancialYears.SelectedValue);
            }

            ddlPayrollPeriods.Items.Clear();
            ddlPayrollPeriods.Items.AddRange(CommonManager.AppendMiddleTaxPaidList(payrollPeriodList, true, false).ToArray());

            //CommonManager.AppendMiddleTaxPaidList(payrollPeriodList, true, false);
            //ddlPayrollPeriods.DataSource = payrollPeriodList;
            //ddlPayrollPeriods.DataBind();

            ddlPayrollPeriods.ClearSelection();

            if (ddlPayrollPeriods.Items.Count <= 0)
            {
                divErrorMsg.InnerHtml = Resources.Messages.CalcNoPayrollPeriodDefined;
                HideAll(false);

                return false;
            }


            if (this.payrollPeriodId == 0)
            {
                ListItem item = null;
                for (int i = ddlPayrollPeriods.Items.Count - 1; i >= 0; i--)
                {
                    if (ddlPayrollPeriods.Items[i].Value.EndsWith(":1"))
                    {
                        item = ddlPayrollPeriods.Items[i];
                        break;
                    }
                }

                if (IsHoldPaymentListing == false)
                {
                    if (item != null)
                    {
                        SplitPeriod(item.Value, ref payrollPeriodId, ref payrollPeriodType);
                        //payrollPeriodId = int.Parse(item.Value);
                        item.Selected = true;
                    }
                }
                else // hold payment period selection from QueryString
                {
                    string value = HoldPaymentPayrollPeriodId + ":1";
                    payrollPeriodId = HoldPaymentPayrollPeriodId;
                    payrollPeriodType = 1;
                    ddlPayrollPeriods.SelectedValue = value;
                }
            }
            else
            {
                if (payrollPeriodType == (int)PayrollPeriodType.NewAddOn)
                {
                    ListItem item = ddlPayrollPeriods.Items.FindByValue(payrollPeriodId.ToString() + ":" + payrollPeriodType + ":" + newAddOnId);
                    if (item != null)
                    {
                        SplitPeriod(item.Value, ref payrollPeriodId, ref payrollPeriodType);
                        //payrollPeriodId = int.Parse(item.Value);
                        item.Selected = true;
                    }
                }
                else
                {
                    ListItem item = ddlPayrollPeriods.Items.FindByValue(payrollPeriodId.ToString() + ":" + payrollPeriodType);
                    if (item != null)
                    {
                        SplitPeriod(item.Value, ref payrollPeriodId, ref payrollPeriodType);
                        //payrollPeriodId = int.Parse(item.Value);
                        item.Selected = true;
                    }
                }
            }


            if (!IsPostBack)
            {

                List<FinancialDate> dates = CommonManager.GetAllYears(); ;
                foreach (var item in dates)
                    item.SetName(IsEnglish);
                ddlFinancialYears.DataSource = dates;
                ddlFinancialYears.DataBind();
                ddlFinancialYears.SelectedValue = SessionManager.CurrentCompanyFinancialDate.FinancialDateId.ToString();

                Setting setting = CalculationManager.GetSetting();
                if (setting != null && setting.ContractUptoDay != null)
                {
                    int period = this.GetPayrollPeriodId();
                    PayrollPeriod payroll = CommonManager.GetPayrollPeriod(period);
                    CustomDate payrollCD = CustomDate.GetCustomDateFromString(payroll.StartDate, IsEnglish);
                    CustomDate date = new CustomDate(setting.ContractUptoDay.Value, payrollCD.Month, payrollCD.Year, IsEnglish);

                    lblUnitRateToDate.Text = date.ToString();
                    divContractUptoDate.Style["display"] = "";
                }

                if (CommonManager.CompanySetting.EnableSalaryProcessDay)
                {
                    divSalaryProcessDay.Style["display"] = "";

                    if (setting != null && setting.SalaryProcessUptoDay != null)
                    {
                        int period = this.GetPayrollPeriodId();
                        PayrollPeriod payroll = CommonManager.GetPayrollPeriod(period);
                        CustomDate payrollCD = CustomDate.GetCustomDateFromString(payroll.StartDate, IsEnglish);

                        if (setting.SalaryProcessUptoDay > DateHelper.GetTotalDaysInTheMonth(payrollCD.Year, payrollCD.Month, IsEnglish))
                        {
                            lblSalaryProcessDate.Text = "Day " + setting.SalaryProcessUptoDay + " is not valid for current period month.";
                        }
                        else
                        {
                            CustomDate date = new CustomDate(setting.SalaryProcessUptoDay.Value, payrollCD.Month, payrollCD.Year, IsEnglish);

                            lblSalaryProcessDate.Text = date.ToString();
                        }
                    }
                    else
                        lblSalaryProcessDate.Text = "Salary processing day not defined.";
                }
            }

            //lbl.Text = lastPayrollPeriod.Name;
            //this.payrollPeriodId = lastPayrollPeriod.PayrollPeriodId;
            calculation = CalculationManager.IsCalculationSaved(this.GetPayrollPeriodId());


            chkHasRetiredOrResigned.Visible =
                CalculationManager.HasPayrollPeriodRetirementOrResignedEmp(this.GetPayrollPeriodId());


            // if current payroll period in selection then only process
            if (ddlPayrollPeriods.Items.Count > 0 &&
                (payrollPeriodId.ToString() + ":" + payrollPeriodType) == ddlPayrollPeriods.Items[ddlPayrollPeriods.Items.Count - 1].Value)
            {
                int total = 0, completed = 0;
                checkListSection.Visible = !CommonManager.IsAllCheckListCompleted(ref total, ref completed);
                checkListLink.Text = string.Format("{0} of {1} checklist yet to be reviewed.", completed, total);
            }
            else
                checkListSection.Visible = false;

            if (IsPostBack == false)
            {
                JobStatus statues = new JobStatus();
                List<KeyValue> statusList = statues.GetMembers();

                statusList.RemoveAt(0);

                foreach (KeyValue item in statusList)
                {
                    multiStatus.AddItems(item.Value, item.KeyValueCombined);
                }
                
                
                //ddlStatus.DataSource = statusList;
                //ddlStatus.DataValueField = "KeyValueCombined";
                //ddlStatus.DataTextField = "Value";

                //ddlStatus.DataBind();
            }

            //if no retired/reg then make visible false as its value is always going
            if (chkHasRetiredOrResigned.Visible == false)
                chkHasRetiredOrResigned.Checked = false;

            return true;
        }

        public void ddlFinancialYears_Change(object sender, EventArgs e)
        {
            btnSave.Visible = false;
            btnDelete.Visible = false;
            btnDeleteAll.Visible = false;

            Initialise();
        }

        public void txtEmpSearch_TextChanged(object sender, EventArgs e)
        {
            _tempCurrentPage = 1;
            LoadCalculation(false, false);
        }

        public void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            _tempCurrentPage = 1;
            LoadCalculation(false, false);
        }
        public void ddlBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            _tempCurrentPage = 1;
          

            LoadCalculation(false, false);
        }
        bool IsPayrollPeriodDropDownChanged()
        {
            return Request.Form["__EventTarget"] != null && Request.Form["__EventTarget"].Equals(ddlPayrollPeriods.ClientID.Replace("_", "$"));
        }

        private void HideAll(bool show)
        {
            if (!show)
            {
                pagingCtl.Visible = false;
                gvw.Visible = false;
                //pnlPaging.Visible = false;
                pnlButtons.Visible = false;

                divErrorMsg.Hide = false;

            }
            else
            {

                pagingCtl.Visible = true;
                gvw.Visible = true;
                //pnlPaging.Visible = true;
                pnlButtons.Visible = true;
                divErrorMsg.Hide = true;
            }
        }

        private int GetPayrollPeriodId()
        {
            //return int.Parse(ddlPayrollPeriods.SelectedValue);
            return payrollPeriodId;
        }

        bool IsExportButtonSubmit()
        {
            return
            (
                Request.Form["__EVENTTARGET"] != null && Request.Form["__EVENTTARGET"].Equals(btnExport.ClientID.Replace("_", "$"))
                ||
                Request.Form["__EVENTTARGET"] != null && Request.Form["__EVENTTARGET"].Equals(btnExportCSV.ClientID.Replace("_", "$"))
            );
        }

        void LoadCalculation(bool isEditMode,bool isReloadColumn)
        {

            if (calculation == null)
            {
                gvw.Columns[gvw.Columns.Count - 1].Visible = false;
            }
            else
            {
                gvw.Columns[gvw.Columns.Count - 1].Visible = true;
                //btnDelete.Enabled = true;
                //btnEdit.Enabled = true;
            }

            if (payrollPeriodType == (int)PayrollPeriodType.Normal)
            {
                headers = CalculationManager.GetIncomeHeaderList(SessionManager.CurrentCompanyId, this.GetPayrollPeriodId());

                // then remove SST & TDS header as not needed
                if (IsHoldPaymentListing)
                {
                    headers.RemoveAll(x=>x.ColumnType==CalculationColumnType.SST);
                    headers.RemoveAll(x => x.ColumnType == CalculationColumnType.TDS);
                }
            }
            else if (payrollPeriodType == (int)PayrollPeriodType.NewAddOn)
            {
                headers = CalculationManager.GetAddOnHeaderList(newAddOnId, false);
            }
            else
                headers = CalculationManager.GetPartialTaxPaidHeaderList(this.GetPayrollPeriodId(), false);

            foreach (CalcGetHeaderListResult header in headers)
            {
                string key = header.Type + ":" + header.SourceId;
                if (!headerList.ContainsKey(key) )
                    headerList.Add(key, "");
            }

            AddColumns(headers, gvw, this, isReloadColumn);

            HideAll(true);

            ChangeDisplayState(isEditMode, isReloadColumn);


            // Process for Status Filter
            JobStatus statues = new JobStatus();
            List<KeyValue> statusList = statues.GetMembers();
            string[] statusTexts = multiStatus.Text.Split(new char[] { ',' });
            string statusIDTexts = "";
            foreach (string item in statusTexts)
            {
                string text = item.ToString().ToLower().Trim();
                KeyValue status = statusList.FirstOrDefault(x => x.Value.ToString().ToLower().Trim() == text);
                if (status != null)
                {
                    if (statusIDTexts == "")
                        statusIDTexts = status.Key;
                    else
                        statusIDTexts += "," + status.Key;
                }
            }


            if (this.GetPayrollPeriodId() != 0)
            {

                ///this.ErrorList = "";
                //this.WarningList = "";
                int pageIndex = _tempCurrentPage - 1;
                int pageSize = int.Parse(pagingCtl.DDLRecords.SelectedValue);

                bool isExport = IsExportButtonSubmit();
                //if access from export
                if (isExport)
                {
                    pageSize = 99999;
                    pageIndex = 0;
                }

                string[] value = ddlStatus.SelectedValue.Split(new char[] { ':' });
                int minStatus = int.Parse(value[0]);
                bool statusOnly = ddlStatus.SelectedItem.Text.ToString().ToLower().Contains("only");

                int employeeId = -1;

                if (QueryStringEmployeeID != null)
                    employeeId = QueryStringEmployeeID.Value;
                else if (!string.IsNullOrEmpty(hiddenEmployeeID.Value) && !string.IsNullOrEmpty(txtEmpSearch.Text.Trim()))
                    employeeId = int.Parse(hiddenEmployeeID.Value);
                

                if (IsHoldPaymentListing)
                {
                    calculationList = CalculationManager.GetHoldPaymentCalculationDetails(SessionManager.CurrentCompanyId,
                        this.GetPayrollPeriodId(), pageIndex,
                        pageSize, ref _tempCount);
                    gvw.DataSource = calculationList;

                }
                else if (payrollPeriodType == (int)PayrollPeriodType.Normal)
                {
                    calculationList = CalculationManager.GetCalculationList(SessionManager.CurrentCompanyId,
                        this.GetPayrollPeriodId(), pageIndex,
                        pageSize, ref _tempCount, chkHasRetiredOrResigned.Checked, employeeId, "", (int)this.ViewSalaryType, int.Parse(ddlBranch.SelectedValue), int.Parse(ddlDepartments.SelectedValue), statusIDTexts,int.Parse(ddlSubDepartments.SelectedValue),int.Parse(ddlUnit.SelectedValue));

                    gvw.DataSource = calculationList;
                }
                else if (payrollPeriodType == (int)PayrollPeriodType.NewAddOn)
                {
                    calculationList = CalculationManager.GetAddOnEmployeeList(SessionManager.CurrentCompanyId,
                        this.GetPayrollPeriodId(),newAddOnId, pageIndex,
                        pageSize, ref _tempCount, employeeId, "", false, false, null, int.Parse(ddlBranch.SelectedValue), int.Parse(ddlDepartments.SelectedValue), int.Parse(ddlUnit.SelectedValue));

                    gvw.DataSource = calculationList;
                }
                else
                {
                    string search = txtEmpSearch.Text.Trim();

                    if (search.Contains(" - "))
                        search = search.Remove(search.IndexOf(" - ") - 1);

                    calculationList = CalculationManager.GetPartialPaidEmployeeList(SessionManager.CurrentCompanyId,
                        this.GetPayrollPeriodId(), pageIndex,
                        pageSize, ref _tempCount, -1, search);

                    gvw.DataSource = calculationList;

                }

                if (isExport == false)
                {
                    gvw.DataBind();

                    if (gvw.Rows.Count <= 0)
                    {
                        divErrorMsg.InnerHtml = Resources.Messages.CalcNoEmployees;
                        // divInfoMsg.Style.Remove("display");
                        HideAll(false);
                        return;
                    }
                }
            }
            else
            {

                gvw.DataSource = null;
                gvw.DataBind();

               
                return;
            }

            //show/hide paging panel
            SetPagingSetting(_tempCount);

           

        }


      
        private void ChangeDisplayState(bool isEditMode,bool isReloadColumn)
        {

            int totalEmployees = CalculationManager.GetTotalApplicableForSalary(this.GetPayrollPeriodId(), chkHasRetiredOrResigned.Checked);
            int initialSave = calcMgr.GetTotalEmployeeInitialSave(this.GetPayrollPeriodId(), chkHasRetiredOrResigned.Checked);

            // payroll summary

            PayrollPeriod period = CommonManager.GetLastPayrollPeriod();
            if (period != null)// && period.PayrollPeriodId == payrollPeriodId)
            {
                if (chkHasRetiredOrResigned.Checked)
                {
                    spantotalEmployees.InnerHtml = CalculationManager.GetTotalApplicableForSalary(period.PayrollPeriodId, false)
                        .ToString();
                    spansavedEmployees.InnerHtml = new CalculationManager().GetTotalEmployeeInitialSave(period.PayrollPeriodId, false)
                        .ToString();
                    spanRem.InnerHtml = (totalEmployees - initialSave).ToString();
                }
                else
                {
                    spantotalEmployees.InnerHtml = totalEmployees.ToString();
                    spansavedEmployees.InnerHtml = initialSave.ToString();
                    spanRem.InnerHtml = (totalEmployees - initialSave).ToString();
                }

                CCalculation calculation = CalculationManager.GetCalculation(this.GetPayrollPeriodId());
                if (calculation != null && calculation.IsFinalSaved != null && calculation.IsFinalSaved.Value)
                {
                    spanRetiring.InnerHtml = CalculationManager.CountRetiring(calculation.CalculationId).ToString();
                }
                else
                {
                    spanRetiring.InnerHtml = CalculationManager.CountRetiring(period.StartDateEng.Value.Date,
                        period.EndDateEng.Value.Date).ToString();
                }

                divPayrollSummary.Visible = true;
            }
            else
                divPayrollSummary.Visible = false;



            //DisplayMode mode = DisplayMode.NotSet;
            if (CalculationManager.IsAllEmployeeSavedFinally(this.GetPayrollPeriodId(),chkHasRetiredOrResigned.Checked)
                && payrollPeriodType != (int)PayrollPeriodType.OldAddOn && payrollPeriodType != (int)PayrollPeriodType.NewAddOn)
            {
                pnlButtons.Visible = false;
                gvw.Columns[gvw.Columns.Count - 1].Visible = false;
                divWarningMsg.InnerHtml = Resources.Messages.CalcAllEmployeeFinalSavedMsg;
                divWarningMsg.Hide = false;
               
                return;
            }

            


            if (initialSave >= totalEmployees)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                btnDeleteAll.Visible = true;
                btnEdit.Visible = true;
                btnSaveFinal.Visible = true;

                string msg = string.Format(Resources.Messages.CalcTaxSavingWarningMsg, initialSave);
                btnSaveFinal.OnClientClick
                    = string.Format(btnSaveFinal.OnClientClick, msg);
                
            }
            else if (initialSave > 0)
            {
                gvw.Columns[gvw.Columns.Count - 1].Visible = true;
                
                btnDelete.Visible = true;
                btnDeleteAll.Visible = true;
                btnEdit.Visible = true;
                btnSaveFinal.Visible = false;
                btnSave.Visible = true;
            }
            else
            {
                btnSaveFinal.Visible = false;
                btnSave.Visible = true;
                btnDelete.Visible = false;
                btnDeleteAll.Visible = false;
                btnEdit.Visible = false;
            }

           

            if(editMode)
            {
                btnSaveFinal.Visible = false;
                btnSave.Visible = true;
            }
        }

      

        #region "Event handlers"

        protected void btnApprove_Click(object sender, EventArgs e)
        {

            CalculationManager.ApproveSalary(this.GetPayrollPeriodId());

            msgCtl.InnerHtml = "Salary has been approved, now saving can be done.";
            msgCtl.Hide = false;
        }

        protected void btnSaveFinal_Click(object sender, EventArgs e)
        {

            int totalEmployees= CalculationManager.GetTotalApplicableForSalary(this.GetPayrollPeriodId(),chkHasRetiredOrResigned.Checked);
            int initialSave = calcMgr.GetTotalEmployeeInitialSave(this.GetPayrollPeriodId(),chkHasRetiredOrResigned.Checked);


            if (initialSave < totalEmployees)
            {
                divWarningMsg.InnerHtml = "All valid employees have not been saved for approval, please confirm all valid employees should be saved for approval.";
                divWarningMsg.Hide = false;
                return;
            }

            CalculationManager.SaveFinally(this.GetPayrollPeriodId(),chkHasRetiredOrResigned.Checked);
            LoadCalculation(false, false);
            //JavascriptHelper.DisplayClientMsg("Final calculation saved.", Page);
            divWarningMsg.InnerHtml = Resources.Messages.CalculationFinalSavedMsg;
            divWarningMsg.Hide = false;
        }

        protected void btnLoad_Click(object sender, EventArgs e)
        {

        }



        protected void btnEdit_Click(object sender, EventArgs e)
        {
            editMode = true;
            LoadCalculation(true, false);
        }
        
        protected void btnAllDelete_Click(object sender, EventArgs e)
        {

            if (CalculationManager.IsAllEmployeeSavedFinally(int.Parse(ddlPayrollPeriods.SelectedItem.Value.Split(new char[] { ':' })[0]), false))
            {
                LoadCalculation(false, true);
                return;
            }

            if (calculation != null )
            {
                bool result = calcMgr.DeleteAllCalculation(calculation.CalculationId, ddlPayrollPeriods.SelectedItem.Text);

                if (result)
                {
                    //JavascriptHelper.DisplayClientMsg("Calculation deleted completely.", Page);
                    msgCtl.InnerHtml = Resources.Messages.CalculationCompletelyDeletedMsg;
                    msgCtl.Hide = false;
                    calculation = null;
                }
                else
                {
                    //JavascriptHelper.DisplayClientMsg("Calculation deleted.", Page);
                    msgCtl.InnerHtml = Resources.Messages.CalculationDeletedMsg;
                    msgCtl.Hide = false;
                }



                LoadCalculation(false, true);
                //ChangeDisplayState();

            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            if (CalculationManager.IsAllEmployeeSavedFinally(int.Parse(ddlPayrollPeriods.SelectedItem.Value.Split(new char[] { ':' })[0]), false))
            {
                LoadCalculation(false, true);
                return;
            }

            StringBuilder xmlEmployees = new StringBuilder();
            //xmlEmployees.Append("<root>");
            bool employeeAdded = false;
            for (int i = 0; i < this.gvw.Rows.Count; i++)
            {
                GridViewRow row = gvw.Rows[i];
                if (row.FindControl("lblId") == null)
                    continue;

                if (row.FindControl("chkDelete") == null)
                    continue;

                CheckBox chkDelete = (CheckBox)row.FindControl("chkDelete");

                if (chkDelete.Checked)
                {
                    int empId = int.Parse(((Label)row.FindControl("lblId")).Text);

                    if (xmlEmployees.Length == 0)
                        xmlEmployees.Append(empId);
                    else
                        xmlEmployees.Append("," + empId);
                    //xmlEmployees.AppendFormat("<row Id='{0}' />", empId);
                    employeeAdded = true;
                }
            }
            //xmlEmployees.Append("</root>");

            if (calculation != null && employeeAdded)
            {
                bool result = calcMgr.DeleteCalculation(calculation.CalculationId, xmlEmployees.ToString(),ddlPayrollPeriods.SelectedItem.Text);

                if (result)
                {
                    //JavascriptHelper.DisplayClientMsg("Calculation deleted completely.", Page);
                    msgCtl.InnerHtml = Resources.Messages.CalculationCompletelyDeletedMsg;
                    msgCtl.Hide = false;
                    calculation = null;
                }
                else
                {
                    //JavascriptHelper.DisplayClientMsg("Calculation deleted.", Page);
                    msgCtl.InnerHtml = Resources.Messages.CalculationDeletedMsg;
                    msgCtl.Hide = false;
                }


                
                LoadCalculation(false,true);
                //ChangeDisplayState();

            }
        }

        //private void NoSavedSoInitialState()
        //{
        //    if (gvw.Columns[gvw.Columns.Count - 1].HeaderText == "Delete")
        //        gvw.Columns[gvw.Columns.Count - 1].Visible = false;

        //    btnDelete.Enabled = true;

        //    calculation = null;
        //}

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            editMode = false;
            LoadCalculation(false,false);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            bool employeeSaved = SaveCalculation();
            editMode = false;          

            //editMode = false;


            if (employeeSaved)
            {

                calculation = CalculationManager.IsCalculationSaved(this.GetPayrollPeriodId());
                //load in calculation field
                //Initialise();
                LoadCalculation(false,true);

                msgCtl.InnerHtml = Resources.Messages.CalculationSavedMsg;
                msgCtl.Hide = false;
                
            }
        }

        #region "Save Calculation"

        //public bool IsIncomeAdjustmentChecked()
        //{
        //    if (gvw.HeaderRow == null)
        //        return false;

        //    TableRow rowHeader = (System.Web.UI.WebControls.TableRow) gvw.HeaderRow;
        //    for (int j = 2; j < rowHeader.Cells.Count - 1; j++)
        //    {
        //        if (rowHeader.Cells[j].Controls.Count > 0)
        //        {
        //            Label lblHeader = (Label) rowHeader.Cells[j].Controls[0];

        //            TextBox txtAdjustment = null;
        //            if (lblHeader.Attributes["Type"] == ((int) CalculationColumnType.IncomeAdjustment).ToString())
        //            {
        //                CheckBox chk = (CheckBox) rowHeader.Cells[j].Controls[2];
        //                return chk.Checked;
        //            }
        //        }
        //    }
        //    return false;
        //}


        private bool SaveCalculation()
        {
            int payrollPeriodId = this.GetPayrollPeriodId();
            bool hasAdjIncome = false;
            bool hasAdjDeduction = false;
            string adjIncome = "";
            string adjDeduction = "";
         
            bool hasPF = SessionManager.CurrentCompany.HasPFRFFund;
            double empContribution = 0;
            double companyContribution = 0;
            string pfRFNo = string.Empty;

            if (CalculationManager.IsPayrollFinalSaved(payrollPeriodId))
            {
                divErrorMsg.InnerHtml = "Salary can not be changed.";
                divErrorMsg.Hide = false;
                return false;
            }

            if (calculation == null)
                CalculationManager.ClearHeaderCache(SessionManager.CurrentCompanyId, GetPayrollPeriodId());
           
            if (hasPF)
            {
                empContribution = SessionManager.CurrentCompany.PFRFFunds[0].EmployeeContribution.Value;
                companyContribution = SessionManager.CurrentCompany.PFRFFunds[0].CompanyContribution.Value;
                pfRFNo = SessionManager.CurrentCompany.PFRFFunds[0].PFRFNo;
            }

            StringBuilder xmlHeaders = new StringBuilder();
            StringBuilder xmlEmployees = new StringBuilder();
            StringBuilder xmlCells = new StringBuilder();
            StringBuilder xmlIncludeds = new StringBuilder();
            //StringBuilder xmlCalculationLoanAdvance  = new 

            xmlHeaders.Append("<root>");
            //add income headers
            System.Web.UI.WebControls.TableRow rowHeader = (System.Web.UI.WebControls.TableRow)gvw.HeaderRow;

            if (rowHeader == null)
            {
                divErrorMsg.InnerHtml = "Please reload the page before saving.";
                divErrorMsg.Hide = false;
                return false;
            }

            for (int j = 3; j < rowHeader.Cells.Count - 1; j++)
            {
                if (rowHeader.Cells[j].Controls.Count > 0)
                {
                    Label lblHeader = (Label)rowHeader.Cells[j].Controls[0];
                    TextBox txtAdjustment = null;

                    CalculationColumnType columnType =
                        (CalculationColumnType) int.Parse(lblHeader.Attributes["Type"].ToString());



                    

                    string isIncome = "";
                    if (CalculationValue.IsColumTypeIncome(columnType))
                        isIncome = "1";
                    else if (CalculationValue.IsColumTypeDeduction(columnType))
                        isIncome = "0";

                    xmlHeaders.AppendFormat("<row SourceId='{0}' Type='{1}' HeaderName='{2}' IsIncome='{3}' />",
                                            lblHeader.Attributes["SourceId"], lblHeader.Attributes["Type"],
                                            (txtAdjustment == null)
                                                ? lblHeader.Attributes["HeaderName"]
                                                : (Request.Form[txtAdjustment.UniqueID]),
                                                isIncome);
                }
            }
            xmlHeaders.Append("</root>");

            bool employeeSaved = false;
            decimal grossIncome = 0, totalDeduction = 0, netSalary = 0;
            decimal diff = 0;

            xmlEmployees.Append("<root>");
            //add cell values
            xmlCells.Append("<root>");
            xmlIncludeds.Append("<root>");
            for (int i = 0; i < gvw.Rows.Count; i++)
            {
                GridViewRow gridViewRow = gvw.Rows[i];
                if (gridViewRow.FindControl("lblId") == null)
                    continue;

                int empId = (int)gvw.DataKeys[gridViewRow.RowIndex]["EmployeeId"];  //int.Parse(((Label)gridViewRow.FindControl("lblId")).Text);
                int calculationId = (int)gvw.DataKeys[gridViewRow.RowIndex]["CalculationId"];//int.Parse(((HiddenField)gridViewRow.FindControl("txtCalulationId")).Value);
                HiddenField adjustmentIncome = gridViewRow.FindControl("adjIncome") as HiddenField;
                HiddenField adjustmentDeduction = gridViewRow.FindControl("adjDeduction") as HiddenField;
                //TextBox txtNote = gridViewRow.FindControl("txtNote") as TextBox;
                string isRetiredOrResigned = gvw.DataKeys[gridViewRow.RowIndex]["IsRetiredOrResigned"].ToString();
                string isFinalSaved = gvw.DataKeys[gridViewRow.RowIndex]["IsFinalSaved"].ToString();

                //HiddenField hfRetRegState = (HiddenField)gridViewRow.FindControl("hfRetRegState");
                //HiddenField hfFinalSaved = (HiddenField)gridViewRow.FindControl("hfFinalSaved");

                //If final saved then don't save again
                if (isFinalSaved == "1")
                    continue;
                
                string adjIncomeComment = string.Empty, adjDeductionComment = string.Empty;

                if (hasAdjIncome && adjustmentIncome != null && !string.IsNullOrEmpty(adjustmentIncome.Value))
                {
                    adjIncomeComment = adjustmentIncome.Value;
                }
                if (hasAdjDeduction && adjustmentDeduction != null && !string.IsNullOrEmpty(adjustmentDeduction.Value))
                {
                    adjDeductionComment = adjustmentDeduction.Value;
                }

                if (calculationId == 0 || editMode)
                {
                    employeeSaved = true;

                    //string invalidText = Util.HasInvalidCharacterForXMLParsingAndImport(txtNote.Text.Trim());

                    //if (!string.IsNullOrEmpty(invalidText))
                    //{
                    //    divWarningMsg.InnerHtml = "\"" + invalidText + "\" character can not be set in the note for the employee "
                    //        + EmployeeManager.GetEmployeeById(empId).Name + ".";
                    //    divWarningMsg.Hide = false;
                    //    return false;
                    //}

                    // append Employee info
                    xmlEmployees.AppendFormat(
                        "<row EmployeeId='{0}' AdjustmentIncome='{1}' AdjustmentDeduction='{2}' IsRetiredOrResigned='{3}' Note='{4}' />", empId,
                        adjIncomeComment, adjDeductionComment,isRetiredOrResigned,""
                        );

                    bool isRetrospectNegative = false;
                    grossIncome = 0; totalDeduction = 0; netSalary = 0;
                    diff = 0;
                    for (int j = 3; j < gridViewRow.Cells.Count - 2; j++)
                    {
                        TextBox txt = (TextBox)gridViewRow.Cells[j].Controls[0];
                        if (txt.ID == "txtNote")
                            continue;
                        Label lblHeader = (Label)rowHeader.Cells[j].Controls[0];

                        if (!txt.Text.Equals(Resources.Messages.EmptySalaryAmountText))
                        {
                            //if true then when viewed input doesn't existed later from another page it was associated so don't save
                            if (Request.Form[txt.UniqueID] == null || Request.Form[txt.UniqueID] == Resources.Messages.EmptySalaryAmountText)
                            {
                                continue;
                            }

                            decimal amount = decimal.Parse(Request.Form[txt.UniqueID].ToString());
                            //If adjustment has not been selected then don't add in the list
                            CalculationColumnType columnType =
                                (CalculationColumnType)int.Parse(lblHeader.Attributes["Type"].ToString());

                            

                            //allowed only if emp is retiring/resigning as can be due to loan/adv
                            // temporary disable for hpl
                            if (CommonManager.CompanySetting.WhichCompany != WhichCompany.HPL
                                  && CommonManager.CompanySetting.WhichCompany != WhichCompany.AceTravels
                                && CommonManager.CompanySetting.WhichCompany != WhichCompany.Mega
                                && CommonManager.CompanySetting.WhichCompany != WhichCompany.Yeti
                                && CommonManager.CompanySetting.PayrollAllowNegativeSalarySave == false
                                && columnType != CalculationColumnType.DeemedIncome)
                            {
                                if (amount <= Convert.ToDecimal(-0.5) && isRetiredOrResigned == "0" &&
                                    columnType != CalculationColumnType.DeductionCIT &&// for speicial PSI case cit ccan be negative
                                    columnType != CalculationColumnType.TDS &&
                                    columnType != CalculationColumnType.SST &&
                                    columnType != CalculationColumnType.DeductionTotal &&
                                    //columnType != CalculationColumnType.IncomeResterospect && !isRetrospectNegative &&
                                    columnType != CalculationColumnType.Income) // income could be negative 
                                {
                                    isValidAllValid = false;
                                    txt.BorderColor = Color.Red;
                                    txt.BorderWidth = new Unit(1);
                                    //txt.Style.Add("background-color", "red");
                                    txt.ToolTip = "Invalid value";
                                    return false;
                                }
                            }

                            //Check & append CalculationIncluded information like Special Events id, Income Increments id
                            if (txt.Attributes["CalculationIncluded"] != null)
                            {
                                //Can contain multiple if Special event type
                                string[] inclduedList = txt.Attributes["CalculationIncluded"].Split(new char[] { ',' });
                                if (inclduedList.Length > 0)
                                {
                                    foreach (var s in inclduedList)
                                    {
                                        if (!string.IsNullOrEmpty(s))
                                        {
                                            string[] typeSource = s.Split(new char[] { '=' });
                                            string[] multipleValues = typeSource[1].Split(new char[] { '|' });
                                            foreach (string value in multipleValues)
                                            {
                                                xmlIncludeds.AppendFormat(
                                                "<row EmployeeId='{0}' Type='{1}' SourceId='{2}' />",
                                                empId, typeSource[0], value
                                                );
                                            }
                                            
                                        }
                                    }
                                }
                            }

                            
                            


                            // Set for Gross Income, Total Deduction, Net Salary & Check its summing, if not then
                            // some changes in salary while row being viewed & then tried to save, so prevent if not equal
                            #region "Validation for amount change in other pages while the salary is being viewed & then later save case"
                            if (CalculationValue.IsColumTypeIncome(columnType))
                            {
                                //exclude Bonus=8 income in gross & total
                                if (CommonManager.CompanySetting.IsD2 == false || lblHeader.Attributes["SourceId"] != "8")
                                {
                                    grossIncome += amount;
                                }
                            }
                            else if (columnType == CalculationColumnType.IncomeGross)
                            {
                                diff = grossIncome - amount;
                                if (diff < -1 || diff > 1)
                                {
                                    // then something is wrong so ...
                                    divErrorMsg.InnerHtml =
                                        string.Format("Employee ID \"{0}\" Gross Income doesn't match, please try again.", empId);
                                    divErrorMsg.Hide = false;
                                    return false;
                                }
                            }
                            else if (CalculationValue.IsColumTypeDeduction(columnType))
                            {
                                // don't add negative TDS
                                if (columnType == CalculationColumnType.TDS)
                                {

                                    //if (amount > 0)

                                        totalDeduction += amount;
                                }
                                else
                                    totalDeduction += amount;
                            }
                            else if (columnType == CalculationColumnType.DeductionTotal)
                            {
                                diff = totalDeduction - amount;
                                if (diff < -1 || diff > 1)
                                {
                                    // then something is wrong so ...
                                    divErrorMsg.InnerHtml +=
                                        string.Format("Employee ID \"{0}\" Total Deduction doesn't match, please try again.", empId);
                                    divErrorMsg.Hide = false;
                                    return false;
                                }
                            }
                            #endregion

                            // <row SourceId='{0}' Type='{1}' EmployeeId='{2}' Amount='{3}' />
                            xmlCells.AppendFormat("<row S='{0}' T='{1}' E='{2}' A='{3}' />",
                                                  lblHeader.Attributes["SourceId"], lblHeader.Attributes["Type"], empId,
                                                  //txt.Text
                                                  amount
                                );
                        }
                    }

                }
            }
            xmlCells.Append("</root>");
            xmlEmployees.Append("</root>");
            xmlIncludeds.Append("</root>");



            

            if (employeeSaved)
            {
                calcMgr.SaveUpdateCalculation(payrollPeriodId, hasAdjIncome, adjIncome, hasAdjDeduction, adjDeduction,
                                              hasPF, pfRFNo,empContribution, companyContribution,
                                              xmlHeaders.ToString(), xmlEmployees.ToString(), xmlCells.ToString(),
                                              xmlIncludeds.ToString());


                
            }
            return employeeSaved;
        }

        #endregion

        protected void Page_PreRender(object sender, EventArgs e)
        {

            CalculationManager.RegisterForTextBoxKeysMovement(gvw);
            // If error then hide buttons or middle tax paid salary type then also hide button
            if (!isValidAllValid || payrollPeriodType==(int)PayrollPeriodType.OldAddOn || payrollPeriodType==(int)PayrollPeriodType.NewAddOn)
            {
                btnSaveFinal.Visible = false;
                btnSave.Visible = false;
                btnEdit.Visible = false;

                if (gvw.Columns[gvw.Columns.Count - 1].Visible)
                {
                    btnDeleteAll.Visible = true;
                    btnDelete.Visible = true;
                }
                else
                {
                    btnDeleteAll.Visible = false;
                    btnDelete.Visible = false;
                }

                if (payrollPeriodType != (int)PayrollPeriodType.OldAddOn && payrollPeriodType != (int)PayrollPeriodType.NewAddOn)
                {
                    divErrorMsg.InnerHtml = Resources.Messages.CaluclationInvalidNegativeValueMsg;
                    divErrorMsg.Hide = false;
                }
                //disable all cells
               //CalculationManager.DisableAllCells(gvw);
            }
            if (payrollPeriodType == (int)PayrollPeriodType.OldAddOn || payrollPeriodType == (int)PayrollPeriodType.NewAddOn)
            {
                btnDeleteAll.Visible = false;
                btnDelete.Visible = false;

            }
            hdnChangedInClientValues.Value = "";

            // Register for parent window refresh for Retirement calculation
            if (this.QueryStringEmployeeID != null)
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sfsfd", "window.onunload = function(){ if(typeof (window.opener.refreshPage) != 'undefined') {window.opener.refreshPage();} };", true);

            string jsCodes = string.Format("var payrollId = {0}; var isRetResignedOnly = {1};"
                ,this.GetPayrollPeriodId(),chkHasRetiredOrResigned.Checked?"true":"false");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(),"regpayretreg",jsCodes,true);


            if (chkHasRetiredOrResigned.Visible == true && chkHasRetiredOrResigned.Checked==true)
            {
                btnSaveFinal.Text = Resources.Messages.RetRegFinalSaveText;
            }
            else
                btnSaveFinal.Text = Resources.Messages.GenerateTaxText;

            if (this.ViewSalaryType != SalaryType.Combined)
                this.pnlButtons.Visible = false;

            if (payrollPeriodType == (int)PayrollPeriodType.OldAddOn || payrollPeriodType == (int)PayrollPeriodType.NewAddOn)
                ShowHideType(false,false);
            else
            {
                if (payrollPeriodId != 0)
                {
                    if (CalculationManager.HasAdjustment(payrollPeriodId)
                        || CalculationManager.HasRetrospectIncrement(payrollPeriodId))
                        ShowHideType(CalculationManager.HasAdjustment(payrollPeriodId), CalculationManager.HasRetrospectIncrement(payrollPeriodId));
                    else
                        ShowHideType(false,false);
                }
                else
                    typesDiv.Style["display"] = "none";
            }

            if (IsHoldPaymentListing)
            {
                ddlPayrollPeriods.Enabled = false;
                pnlButtons.Visible = false;
                txtEmpSearch.Visible = false;

                divWarningMsg.Hide = true;
                typesDiv.Style["display"] = "none";
            }

            if (isNegativeWarningSalaryExists)
            {
                divErrorMsg.InnerHtml = Resources.Messages.CaluclationInvalidNegativeValueMsg;
                divErrorMsg.Hide = false;
            }

            if (isAttendanceNotSaved)
            {
                //if (divErrorMsg.InnerHtml != "")
                //    divErrorMsg.InnerHtml += "<br>";
                //divErrorMsg.InnerHtml += "Attendance not saved for some employees.";
                this.ErrorList += "<li>Attendance not saved for some employees.</li>";
                //divErrorMsg.Hide = false;
            }

            DateTime date = DateTime.Now;
            liSignOff.Visible = false;
            // process to display sign off
            if (btnSave.Visible == true && isValidAllValid && CommonManager.CompanySetting.EnablePayrollSignFeature)
            {
                bool isApprovalReqd =PayManager.HasSignOffNotBeingDoneForLatestChanges(ref date);

                if (SessionManager.User.EnablePayrollSignOff != null && SessionManager.User.EnablePayrollSignOff.Value
                    && isApprovalReqd)
                {
                    liSignOff.Visible = true;
                    
                    btnSave.Visible = false;
                    btnDelete.Visible = false;
                    btnDeleteAll.Visible=false;
                    btnSaveFinal.Visible = false;
                    btnApprove.Visible = true;
                    btnApprove.OnClientClick = "if(confirm('Are you sure, you want to Approve the payroll?')==false) return false;";
                }
                else
                {
                    if (isApprovalReqd)
                    {
                        btnSave.Visible = false;
                        signOffWarning.Hide = false;
                        signOffWarning.InnerHtml = "Payroll has not been approved/signoff, please <a href='javascript:void(0)' onclick='requestSignOff()'>Request</a> approval.";
                    }
                    else
                    {
                        signOffWarning.Hide = true;
                        btnSaveFinal.Style["display"] = "";
                    }
                }
            }

            // show/hide arrear generating button
            btnGenerateArrearIncrement.Visible = PayManager.HasArrearIncrementInLastPeriod()
                && GetPayrollPeriodId() == CommonManager.GetLastPayrollPeriod().PayrollPeriodId;


            if (isHeaderMissingError && missingType != 0)
            {

                string header = CalculationManager.GetHeaderName(this.GetPayrollPeriodId(), missingType, missingSourceId);

                // if salary already saved for some employees and new income added or amount added for not saved income then this message
                // appears so instead of showing this message auto insert the header and reload the salary

                

                // for income or deemed income
                if (missingType == 1 || missingType==25)
                {
                    if (missingSavedSourceId != missingType)
                    {
                        CalculationManager.InsertHeaderAfterSalaryAlreadySaved(missingType, missingSourceId, GetPayrollPeriodId());
                        //Initialise();
                        missingSavedSourceId = missingSourceId;
                    }
                }

                CalculationManager.ClearHeaderCache(SessionManager.CurrentCompanyId, GetPayrollPeriodId());

                this.ErrorList = "Header Type=" + missingType + ", Source=" + missingSourceId + ", \"" + header + "\" is missing from the payroll for EIN " + missingEIN + " , please reload the page or click on \"Load\" button as new income head has been added after salary is being saved for some employees.";
                divErrorMsg.Hide=false;

                btnSave.Visible = false;

            }

            if (!string.IsNullOrEmpty(this.ErrorList))
                divErrorMsg.LIList += this.ErrorList;
            else
            {
                divErrorMsg.Hide = true;
                divErrorMsg.LIList = "";
            }


            warningForNegativeAllowHeadsMsg.HideMsgText = true;
            if (!string.IsNullOrEmpty(this.WarningList))
            {
                warningForNegativeAllowHeadsMsg.Hide = false;
                warningForNegativeAllowHeadsMsg.List = this.WarningList;
            }
            else
            {
                warningForNegativeAllowHeadsMsg.List = "";
                warningForNegativeAllowHeadsMsg.Hide = true;
            }


            /// show zeroize warning
            int periodId = GetPayrollPeriodId();
            string incomesList = CalculationManager.GetIncomesZeroizeInCurrentPeriod(periodId);
            if (string.IsNullOrEmpty(incomesList))
                divZeroizeWarning.Hide = true;
            else
            {
                divZeroizeWarning.InnerHtml = incomesList + " incomes has been set as zeroize in add-on, if these incomes have value then may not appear in current salary.";
                divZeroizeWarning.Hide = false;
            }

            // if not last period hide buttons
            PayrollPeriod lastPeriod = CommonManager.GetLastPayrollPeriod();
            if(lastPeriod.PayrollPeriodId != GetPayrollPeriodId())
            {
                btnSave.Visible = false;
                btnDelete.Visible = false;
                btnDeleteAll.Visible = false;
                btnGenerateArrearIncrement.Visible = false;
                btnSaveFinal.Visible = false;
            }
        }

        #endregion


        #region "Grid Manipulation"

        public static void AddColumns(List<CalcGetHeaderListResult> headers, GridView gvw, Calculation calculationControl, bool isReloadColumn)
        {
            //First remove column
            //if (gvw.Columns.Count > 5 && isReloadColumn)
            //{
            //    for (int i = gvw.Columns.Count - 4; i >= 2; i--)
            //    {
            //        gvw.Columns.RemoveAt(i);
            //    }
            //}

            if (gvw.Columns.Count <=5)
            {
                //first sort column list

                headers = CalculationValue.SortHeaders(headers,PayManager.GetIncomeOrderValues(), PayManager.GetDeductionOrderValues());
                PIncome basicIncome =  new PayManager().GetBasicIncome(SessionManager.CurrentCompanyId);
                
                for (int i = 0; i < headers.Count; i++)
                {
                    TemplateField field = new TemplateField();
                    field.ItemTemplate = new CalcGridViewTemplate(DataControlRowType.DataRow, headers[i], calculationControl, basicIncome.IncomeId);
                    field.HeaderTemplate = new CalcGridViewTemplate(DataControlRowType.Header, headers[i], calculationControl, basicIncome.IncomeId);
                    gvw.Columns.Insert(gvw.Columns.Count - 2, field);
                }
            }
            
        }

        

        #endregion

        #region "Paging"

        private int _tempCurrentPage;
        private int? _tempCount = 0;

    //    private int payrollPeriodId = 0;

        public void ShowHideType(bool adjShow,bool retrospectShow)
        {
            divCombined.Visible = adjShow;
            divRegular.Visible = adjShow || retrospectShow;
            divAdjustment.Visible = adjShow;

            divArrear.Visible = retrospectShow;
            // show Combined for Arrear case also
            if (retrospectShow)
                divCombined.Visible = true;

            if (adjShow || retrospectShow)
            {
                typesDiv.Style["border-bottom"] = "2px solid lightgray!important";
                typesDiv.Style["display"] = "";
            }
            else
            {
                typesDiv.Style["display"] = "none";
                typesDiv.Style["border-bottom"] = "0px solid black!important";
            }
        }

        protected override void OnInit(EventArgs e)
        {
            Page.RegisterRequiresControlState(this);
            base.OnInit(e);
        }
        protected override void LoadControlState(object savedState)
        {
            object[] rgState = (object[])savedState;
            base.LoadControlState(rgState[0]);

            _tempCurrentPage = (int)rgState[1];
            payrollPeriodId = (int)rgState[2];
            payrollPeriodType = (int)rgState[4];
            editMode = (bool)rgState[3];
        }
        protected override object SaveControlState()
        {
            object[] rgState = new object[5];
            rgState[0] = base.SaveControlState();

            rgState[1] = _tempCurrentPage;
            rgState[2] = payrollPeriodId;
            rgState[3] = editMode;
            rgState[4] = payrollPeriodType;
            return rgState;
        }

        protected void btnPrevious_Click()
        {
            _tempCurrentPage = int.Parse(pagingCtl.LabelCurrentPage.Text);
            _tempCurrentPage -= 1;
            LoadCalculation(false,false);
        }

        protected void btnNext_Click()
        {
            _tempCurrentPage = int.Parse(pagingCtl.LabelCurrentPage.Text);
            _tempCurrentPage += 1;
            LoadCalculation(false,false);
        }
        protected void ddlRecords_SelectedIndexChanged()
        {
            _tempCurrentPage = 1;
            LoadCalculation(false, false);
        }

        protected void ChangePageNumber()
        {
            _tempCurrentPage = this.pagingCtl.CurrentPage;
            LoadCalculation(false, false);
        }

        private void SetPagingSetting(int? totalRecords)
        {
            if (_tempCount <= 0)
                pagingCtl.Visible = false;
            else
                pagingCtl.Visible = true;

            //calcuation .. of .. page
            int totalPages = (int)Utils.Helper.Util.CalculateTotalPages(_tempCount.Value,
                                                                        int.Parse(pagingCtl.DDLRecords.SelectedValue));
            pagingCtl.LabelTotalPage.Text = (totalPages).ToString();
            pagingCtl.LabelCurrentPage.Text = _tempCurrentPage.ToString();

            // Update Pge numbers
            if (pagingCtl.DDLPageNumber.Items.Count != totalPages)
            {
                pagingCtl.DDLPageNumber.Items.Clear();
                for (int i = 1; i <= totalPages; i++)
                {
                    pagingCtl.DDLPageNumber.Items.Add(i.ToString());
                }
            }
            if (this.pagingCtl.DDLPageNumber.Items.FindByValue(_tempCurrentPage.ToString()) != null)
                this.pagingCtl.DDLPageNumber.SelectedValue = _tempCurrentPage.ToString();

            this.pagingCtl.LabelTotalRecords.Text = "(" + (totalRecords == null ? 0 : totalRecords.Value) + ")";

            //show/hide Next/Previous buttons
            if (_tempCurrentPage <= 1)
                pagingCtl.ButtonPrev.Enabled = false;
            else
                pagingCtl.ButtonPrev.Enabled = true;

            if (_tempCurrentPage < totalPages)
                pagingCtl.ButtonNext.Enabled = true;
            else
                pagingCtl.ButtonNext.Enabled = false;
        }
        #endregion

        protected void gvw_RowCreated(object sender, GridViewRowEventArgs e)
        {
           

            //Display error if attendance is not complete
            if( e.Row.RowType==DataControlRowType.DataRow)
            {
                CalcGetCalculationListResult data = e.Row.DataItem as CalcGetCalculationListResult;

                if (data.IsAttendanceComplete.Value != 1)
                {
                    WebControl ctl = e.Row.FindControl("lblId") as WebControl;
                    ctl.BorderWidth = new Unit(1);
                    ctl.BorderColor = Color.Red;


                    ctl.ToolTip = Resources.Messages.AttendanceNotCompleteForCalculationMsg;


                    isValidAllValid = false;
                }

                if (data.IsSalaryGenerationUnsuccessfull)
                {
                    WebControl ctl = e.Row.FindControl("lblId") as WebControl;
                    ctl.BorderWidth = new Unit(1);
                    ctl.BorderColor = Color.Red;


                    ctl.ToolTip = Resources.Messages.CalcSalaryGenerationFail;


                    isValidAllValid = false;
                }

            }
        }

        protected void gvw_DataBound(object sender, EventArgs e)
        {
             CalculationManager.CreateGroupInHeader(sender
                 , gvw.Columns[gvw.Columns.Count-1 ].Visible );
        }

        #region "Export to Excel"

        protected void btnExport_Click(object sender, EventArgs e)
        {
         
            Export1();
        }

        protected void btnExportCSV_Click(object sender, EventArgs e)
        {

            ExportToExcel();
        }
        #region "Excel Export"
        public void Export1()
        {
            List<CalcGetHeaderListResult> headers = null;

            if (payrollPeriodType == (int)PayrollPeriodType.Normal)
            {
                headers = CalculationManager.GetIncomeHeaderList(SessionManager.CurrentCompanyId, this.GetPayrollPeriodId());


                // then remove SST & TDS header as not needed
                // this code side effect may be hidng ss/tds column sometime randomly so commented
                //if (IsHoldPaymentListing)
                //{
                //    headers.RemoveAll(x => x.ColumnType == CalculationColumnType.SST);
                //    headers.RemoveAll(x => x.ColumnType == CalculationColumnType.TDS);
                //}
            }
            else if (payrollPeriodType == (int)PayrollPeriodType.NewAddOn)
            {
                headers = CalculationManager.GetAddOnHeaderList(newAddOnId, false);
            }
            else
                headers = CalculationManager.GetPartialTaxPaidHeaderList(this.GetPayrollPeriodId(), false);

            string[] value = ddlStatus.SelectedValue.Split(new char[] { ':' });
            int minStatus =  int.Parse(value[0]);
            bool statusOnly = ddlStatus.SelectedItem.Text.ToString().ToLower().Contains("only");

           


            headers = CalculationValue.SortHeaders(headers, PayManager.GetIncomeOrderValues(), PayManager.GetDeductionOrderValues());


            DataTable dataTable = CreateDataTable(calculationList, headers);
            DataSet dset = new DataSet();
            dset.Tables.Add(dataTable);

            SalarySummary mainReport = new SalarySummary();
            mainReport.Bands[BandKind.PageHeader].HeightF = 20;
            //mainReport.labelTitle.Visible = false;

            mainReport.DataSource = dset;
            mainReport.DataMember = "Report";

            CreateUsingLabels(mainReport);

            string name = "";

            if (payrollPeriodId != 0)
            {
                if (payrollPeriodType == (int)PayrollPeriodType.Normal)
                    name = CommonManager.GetPayrollPeriod(payrollPeriodId).Name;
                else if (payrollPeriodType == (int)PayrollPeriodType.NewAddOn)
                {
                    AddOn addon = PayManager.GetAddOn(newAddOnId);
                    if (addon != null)
                        name = addon.Name;
                }
                else
                {
                    PartialTax tax = PayManager.GetPartialTax(payrollPeriodId);
                    if (tax != null)
                        name = tax.Name;
                }

            }

            mainReport.labelTitle.Text = "Month : " + name;

            if (ddlBranch.SelectedValue != "-1")
            {
                mainReport.labelTitle.Text += " , Branch : " + ddlBranch.SelectedItem.Text; 
            }
            if (ddlDepartments.SelectedValue != "-1")
            {
                mainReport.labelTitle.Text += " , Department : " + ddlDepartments.SelectedItem.Text;
            }
            if (ddlStatus.SelectedValue != "-1")
            {
                mainReport.labelTitle.Text += " , Status : " + ddlStatus.SelectedItem.Text;
            }
            using (MemoryStream stream = new MemoryStream())
            {

                mainReport.ExportToXls(stream);
                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                HttpContext.Current.Response.AppendHeader("Content-Type", "application/vnd.ms-excel");
                HttpContext.Current.Response.AddHeader("Content-Length", stream.ToArray().Length.ToString());
                HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=PaySummary " + name + ".xls");
                HttpContext.Current.Response.OutputStream.Write(stream.ToArray(), 0, stream.ToArray().Length);
                HttpContext.Current.Response.End();

            }

        }
        private void CreateUsingLabels(XtraReport report)
        {
            DataTable dataTable = (report.DataSource as DataSet).Tables[0];
            int firstColumnExtraWidth = 60;

            Color colorHeader = Color.FromArgb(201, 214, 237);
            Color colorEven = Color.FromArgb(239, 243, 250);

            int colCount = dataTable.Columns.Count;
            int colWidth = 80;
             int Name_Column_Index = 1;
         int top = 65;

            report.PageWidth = colWidth * colCount;
            (report as SalarySummary).labelTitle.WidthF = report.PageWidth ;
            int currentXLocation = 0;
            int prevWidth = 0;
            int currentWidth = 0;
            // Create header captions
            for (int i = 0; i < colCount; i++)
            {
                
                XRLabel label = new XRLabel();
                
               // label.BeforePrint += new System.Drawing.Printing.PrintEventHandler(Event_BeforePrint);
                label.TextAlignment = TextAlignment.MiddleLeft;
             
                ReportHelper.HeaderLabelStyle(label);
                
             
                
            
                
                if (i == 0 || i == 1)
                {
                    currentWidth = colWidth / 2; //for SN
                    label.TextAlignment = TextAlignment.MiddleCenter;
                }
                //else if( i==1)
                //{
                //    currentWidth = colWidth / 2; //for title

                //}
                else if( i==2)
                {
                    currentWidth = colWidth + firstColumnExtraWidth; //for name
                   
                }
                //else if (i == 3)
                //{
                //    currentWidth = colWidth + 20; //first cost code                   
                //}
                // else if (i == 4) // for Program
                // {
                //     currentWidth = colWidth + 20;                  
                // }
                else
                {
                    currentWidth = colWidth;//for other amount columns
                 
                }
                currentXLocation += (prevWidth);
                prevWidth = currentWidth;

                label.Location = new Point(currentXLocation, top);

                
                label.Text = dataTable.Columns[i].Caption;
                //label.Padding = new PaddingInfo(4, 2, 2, 2);
                
                label.Font = new Font(label.Font.FontFamily, label.Font.Size , FontStyle.Bold);
                label.CanGrow = false;//dont allow to grow as design will be meshed up
               // if (i != Name_Column_Index)
                if (i == 0 || i == 1)
                {
                    label.Borders = DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom;
                    label.Size = new Size(currentWidth, 50);
                }
                else
                {
                    label.Borders = DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom;


                  
                    label.Size = new Size(currentWidth, 50);
                }
                
                report.Bands[BandKind.PageHeader].Controls.Add(label);
            }

           
            currentXLocation = 0;
            prevWidth = 0;
            currentWidth = 0;
            // Create data-bound labels with different odd and even backgrounds
            for (int i = 0; i < colCount; i++)
            {
                XRLabel label = new XRLabel();
               
                label.Text = "-";
                //label.BeforePrint += new System.Drawing.Printing.PrintEventHandler(Event_BeforePrint);
                
                
                if (i == 0 || i==1)
                {
                    currentWidth = colWidth / 2;
                    label.TextAlignment = TextAlignment.MiddleCenter;
                }
                //else if (i == 1)
                //{
                //    currentWidth = colWidth / 2;
                //    label.TextAlignment = TextAlignment.MiddleLeft;

                //}
                else if (i == 2)
                {
                    currentWidth = colWidth + firstColumnExtraWidth; //for name
                    label.TextAlignment = TextAlignment.MiddleLeft;
                }
                //else if (i == 3)
                //{
                //    currentWidth = colWidth + 20; //first cost code
                //    label.TextAlignment = TextAlignment.MiddleLeft;
                //}
                //else if (i == 4)
                //{
                //    currentWidth = colWidth + 20; //first for program
                //    label.TextAlignment = TextAlignment.MiddleLeft;
                //}
                else
                {
                    currentWidth = colWidth;
                    label.TextAlignment = TextAlignment.MiddleRight;
                }

                if(i==colCount-1)
                    label.TextAlignment = TextAlignment.MiddleRight;

                currentXLocation += (prevWidth);
                prevWidth = currentWidth;

               //if (i == Name_Column_Index)
                    label.Location = new Point(currentXLocation, 0);
                //else
                //{
                //    label.Location = new Point((colWidth * i) + firstColumnExtraWidth, 0);                   
                //}
                //label.Padding = new PaddingInfo(2, 2, 2, 0);
                if (i == 0 || i == Name_Column_Index)
                    label.DataBindings.Add("Text", null, dataTable.Columns[i].ColumnName);
                else
                    label.DataBindings.Add("Text", null, dataTable.Columns[i].ColumnName, "{0:n2}");

               
              

                
                label.CanGrow = false;
                
                
              
                //if (i != Name_Column_Index)
                //{
               // 
                if (i == 0 || i == 1)
                {
                    label.Size = new Size(currentWidth, 20);
                    label.Borders = DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom;
                }

                else
                {
                    label.Borders = DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom;

                    label.Size = new Size(currentWidth, 20);
                }
                //}
                //else
                //{
                //    label.Borders = DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom;
                //    label.TextAlignment = TextAlignment.MiddleLeft;
                //    label.Size = new Size(colWidth + firstColumnExtraWidth, 20);
                    
                //}

                    ReportHelper.LabelStyle(label);
                label.Font = new Font(label.Font.FontFamily, label.Font.Size , FontStyle.Regular);

                //label.BackColor = i == 0 ? colorEven : Color.White;
                //label.BorderColor = colorHeader;
                // Place the labels onto a Detail band
                report.Bands[BandKind.Detail].Controls.Add(label);
            }

            currentXLocation = 0;
            prevWidth = 0;
            currentWidth = 0;

            //create footer captions for Total
            for (int i = 0; i < colCount; i++)
            {
                XRLabel label = new XRLabel();
                //set properties for sum/total
                //label.StyleName = "ReportFooter";
                label.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(190)))), ((int)(((byte)(216)))));

               // label.BeforePrint += new System.Drawing.Printing.PrintEventHandler(Event_BeforePrint);

                if (i == 0 || i == 1)
                {
                    currentWidth = colWidth / 2;
                }
                //else if (i == 1)
                //{
                //    currentWidth = colWidth / 2;

                //}
                else if (i == 2)
                {
                    currentWidth = colWidth + firstColumnExtraWidth; //for name

                }
                //else if (i == 3)
                //{
                //    currentWidth = colWidth + 20; //first cost code

                //}
                //else if (i == 4)
                //{
                //    currentWidth = colWidth + 20; //Program

                //}
                else
                {
                    currentWidth = colWidth;

                }
                currentXLocation += (prevWidth);
                prevWidth = currentWidth;

                    label.Location = new Point(currentXLocation, 0);

                //label.Padding = new PaddingInfo(2, 2, 2, 0);


                

                DevExpress.XtraReports.UI.XRSummary summary = new DevExpress.XtraReports.UI.XRSummary();


                //skip for first two columns
                if (i == 1)
                {
                    label.Text = "Total";
                }
                else if (i == colCount - 1)
                {
                }
                else if (i > 1)
                {
                    label.DataBindings.Add("Text", null, dataTable.Columns[i].ColumnName);



                    summary.FormatString = "{0:n2}";

                    summary.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
                    label.Summary = summary;

                }


                label.CanGrow = false;
              
                if( i==0)

                    label.Borders = DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom;

                else
                    label.Borders = DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom; ;

                    label.TextAlignment = TextAlignment.MiddleRight;
                    label.Size = new Size(currentWidth, 20);
                //}
                //else
                //{
                //    label.Text = "Total";
                //    label.Borders = DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom;
                //    label.TextAlignment = TextAlignment.MiddleLeft;
                //    label.Size = new Size(colWidth + firstColumnExtraWidth, 20);
                //}
                label.Font = new Font(label.Font.FontFamily, label.Font.Size, FontStyle.Bold);

                //label.BackColor = i == 0 ? colorEven : Color.White;
                //label.BorderColor = colorHeader;
                // Place the labels onto a Detail band
                report.Bands[BandKind.ReportFooter].Controls.Add(label);
            }
        }

        private DataTable CreateDataTable(List<DAL.CalcGetCalculationListResult> data, List<CalcGetHeaderListResult> headerList)
        {
            bool isHeaderMissingError = false;
            int missingType = 0;
            int missingSourceId = 0;

            DataTable dataTable = new DataTable();
            dataTable.TableName = "Report";

            dataTable.Columns.Add("EIN", typeof(int));
            dataTable.Columns.Add("I No", typeof(string));
            dataTable.Columns.Add("Name", typeof(string));
            // dataTable.Columns.Add("CostCode", typeof(string));
            //dataTable.Columns.Add("Program", typeof(string));

            Dictionary<string, string> headerListDictionary = new Dictionary<string, string>();

            // Create table cells, fill the header cells with text, bind the cells to data
            for (int i = 0; i < headerList.Count; i++)
            {

                DataColumn column = new DataColumn(headerList[i].Type + ":" + headerList[i].SourceId, typeof(decimal));
                column.Caption = headerList[i].HeaderName;

                dataTable.Columns.Add(column);


                string key = headerList[i].Type + ":" + headerList[i].SourceId;
                if (!headerListDictionary.ContainsKey(key))
                    headerListDictionary.Add(key, "");
            }

            foreach (CalcGetHeaderListResult header in headers)
            {

            }

            dataTable.Columns.Add("Account No", typeof(string));

            //add rows
            decimal? value;
            foreach (CalcGetCalculationListResult row in data)
            {
                row.ResetTotal();

                List<object> list = new List<object>();
                list.Add(row.EmployeeId);
                list.Add(row.IDCardNo);
                list.Add(row.Name);
                //  list.Add(row.CostCode);
                // list.Add(row.Program);

                for (int i = 0; i < headerList.Count; i++)
                {
                    value = row.GetCellValue(headerList[i].Type.Value, headerList[i].SourceId.Value, 2, headerListDictionary,ref isHeaderMissingError,ref missingType,ref missingSourceId);
                    //if (value == null)
                    //    list.Add("0");
                    //else
                    list.Add(value);
                }

                list.Add(row.BankACNo);

                dataTable.Rows.Add(list.ToArray());
            }



            return dataTable;


        }

        private string CreateCSV(List<DAL.CalcGetCalculationListResult> data, List<CalcGetHeaderListResult> headerList)
        {
            StringBuilder sb = new StringBuilder();

            bool isHeaderMissingError = false;
            int missingType = 0;
            int missingSourceId = 0;


            sb.Append("EIN");
            sb.Append(",Name");




            Dictionary<string, string> headerListDictionary = new Dictionary<string, string>();

            // Create table cells, fill the header cells with text, bind the cells to data
            for (int i = 0; i < headerList.Count; i++)
            {

              


                sb.Append("," + headerList[i].HeaderName);


                string key = headerList[i].Type + ":" + headerList[i].SourceId;
                if (!headerListDictionary.ContainsKey(key))
                    headerListDictionary.Add(key, "");
            }

            sb.Append("\r\n");



            //add rows
            decimal? value;
            foreach (CalcGetCalculationListResult row in data)
            {

                row.ResetTotal();

                sb.Append(row.EmployeeId.ToString());
                sb.Append("," + row.Name);



                for (int i = 0; i < headerList.Count; i++)
                {
                    value = row.GetCellValue(headerList[i].Type.Value, headerList[i].SourceId.Value, 2, 
                        headerListDictionary,ref isHeaderMissingError,ref missingType,ref missingSourceId);
                    //if (value == null)
                    //    list.Add("0");
                    //else
                    sb.Append("," + value.ToString());

                }

                sb.Append("\r\n");

            }



            return sb.ToString();


        }
        #endregion
        public void ExportToExcel()
        {

            List<CalcGetHeaderListResult> headers = null;

            if (payrollPeriodType == (int)PayrollPeriodType.Normal)
            {
                headers = CalculationManager.GetIncomeHeaderList(SessionManager.CurrentCompanyId, this.GetPayrollPeriodId());

                // then remove SST & TDS header as not needed
                if (IsHoldPaymentListing)
                {
                    headers.RemoveAll(x => x.ColumnType == CalculationColumnType.SST);
                    headers.RemoveAll(x => x.ColumnType == CalculationColumnType.TDS);
                }
            }
            else if (payrollPeriodType == (int)PayrollPeriodType.NewAddOn)
                headers = CalculationManager.GetAddOnHeaderList(newAddOnId, false);
            else
                headers = CalculationManager.GetPartialTaxPaidHeaderList(this.GetPayrollPeriodId(), false);


            headers = CalculationValue.SortHeaders(headers, PayManager.GetIncomeOrderValues(), PayManager.GetDeductionOrderValues());



            PayrollPeriod period = CommonManager.GetPayrollPeriod(this.GetPayrollPeriodId());


            Response.Clear();
            Response.Buffer = true;
            Response.ContentType = "text/csv";
            Response.AppendHeader("Content-Disposition", "attachment; filename=\"PayCalculation for " + period.Name + ".csv\"");
            Response.Write(CreateCSV(calculationList, headers));
            Context.Response.End();



           


        }

        private void ClearControls(Control control)
        {
            for (int i = control.Controls.Count - 1; i >= 0; i--)
            {
                ClearControls(control.Controls[i]);
            }
            //
           // if(control is Table)

            if (!(control is TableCell))
            {
                if (control.GetType().GetProperty("SelectedItem") != null)
                {
                    LiteralControl literal = new LiteralControl();
                    control.Parent.Controls.Add(literal);
                    try
                    {
                        literal.Text =
                            (string)control.GetType().GetProperty("SelectedItem").
                                GetValue(control, null);
                    }
                    catch
                    { }
                    control.Parent.Controls.Remove(control);
                }
                else if (control.GetType().GetProperty("Text") != null)
                {
                    LiteralControl literal = new LiteralControl();
                    control.Parent.Controls.Add(literal);
                    literal.Text =
                        (string)control.GetType().GetProperty("Text").
                            GetValue(control, null);
                    control.Parent.Controls.Remove(control);
                }
            }
            return;
        }

            



        /// <summary>
        /// Replace any of the contained controls with literals
        /// </summary>
        /// <param name="control"></param>
        private void PrepareGridViewForExport(Control gv)
        {



            LinkButton lb = new LinkButton();

            Literal l = new Literal();

            string name = String.Empty;

            for (int i = 0; i < gv.Controls.Count; i++)
            {


                if (gv.Controls[i].GetType() == typeof(LinkButton))
                {

                    l.Text = (gv.Controls[i] as LinkButton).Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                else if (gv.Controls[i].GetType() == typeof(TextBox))
                {
                    
                    l.Text = (gv.Controls[i] as TextBox).Text.ToString();

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);


                }

                if (gv.Controls[i].HasControls())
                {

                    PrepareGridViewForExport(gv.Controls[i]);

                }

            }
        }
        #endregion

        protected void chkHasRetiredOrResigned_Changed(object sender, EventArgs e)
        {
            this.pagingCtl.CurrentPage = 1;
            this._tempCurrentPage = 1;
            LoadCalculation(false, false);
        }

        /// <summary>
        /// when tab like changing Button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void linkRegular_Click(object sender, EventArgs e)
        {
            btnArrearPostToAddOn.Visible = false;
            btnSetArrearToZero.Visible = false;
            if (linkCombined == sender)
            {
                divCombined.Attributes["class"] = "selected";
                divRegular.Attributes["class"] = "";
                divAdjustment.Attributes["class"] = "";
                divArrear.Attributes["class"] = "";
                this.ViewSalaryType = SalaryType.Combined;
                //pnlButtons.Visible = true;
            }
            else if(linkRegular ==sender)
            {
                divCombined.Attributes["class"] = "";
                divRegular.Attributes["class"] = "selected";
                divAdjustment.Attributes["class"] = "";
                divArrear.Attributes["class"] = "";
                this.ViewSalaryType = SalaryType.Regular;
               // pnlButtons.Visible = false;
            }
            else if (linkAdjustment == sender)
            {
                divCombined.Attributes["class"] = "";
                divRegular.Attributes["class"] = "";
                divAdjustment.Attributes["class"] = "selected";
                divArrear.Attributes["class"] = "";
                this.ViewSalaryType = SalaryType.Adjustment;
                //pnlButtons.Visible = false;
            }
            else if (linkArrear == sender)
            {
                divCombined.Attributes["class"] = "";
                divRegular.Attributes["class"] = "";
                divAdjustment.Attributes["class"] = "";
                divArrear.Attributes["class"] = "selected";
                this.ViewSalaryType = SalaryType.RetrospectArrear;
                btnArrearPostToAddOn.Visible = true;
                btnSetArrearToZero.Visible = true;
                //pnlButtons.Visible = false;
            }
            LoadCalculation(false, false);
        }
        
        protected void btnArrearPostToAddOn_Click(object sender, EventArgs e)
        {
            int payrollPeriodId = GetPayrollPeriodId();
            Status status = CommonManager.PostArrearToAddOn(payrollPeriodId);
            if (status.IsSuccess)
            {
                msgCtl.InnerHtml = "Arrear posted to Add-On.";
                msgCtl.Hide = false;
            }
            else
            {
                divWarningMsg.InnerHtml = status.ErrorMessage;
                divWarningMsg.Hide = false;
            }

        }
        protected void btnSetArrearToZero_Click(object sender, EventArgs e)
        {
            int payrollPeriodId = GetPayrollPeriodId();
            Status status = CommonManager.SetArrearZero(payrollPeriodId);
            if (status.IsSuccess)
            {
                LoadCalculation(false, false);
                msgCtl.InnerHtml = "Arrear set to Zero.";
                msgCtl.Hide = false;
            }
            else
            {
                divWarningMsg.InnerHtml = status.ErrorMessage;
                divWarningMsg.Hide = false;
            }

        }

    }

}