﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PayrollPeriodCtl.ascx.cs"
    Inherits="Web.UserControls.PayrollPeriodCtl" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<script type="text/javascript">

    var mId = '<%= ddlMonths.ClientID %>';
    var yId = '<%= ddlYears.ClientID %>';
    var txtP = '<%= txtPeriodName.ClientID %>';
    function createPeriod() {
        var period = document.getElementById(mId).options[document.getElementById(mId).selectedIndex].text
         + "-" + $('#' + yId).val();
        $('#' + txtP).val(period);
    }


    function valDate() {

        return true;
    }

    function validateCP() {
        valGroup = 'AECompany';
        if (valDate()) {
            return CheckValidation();
        }
        else
            return false;
    }
    
</script>
<style>
    .innertable td
    {
        padding-left: 0px !important;
    }
    
    .tableLightColor a:hover{color:Blue!important;}
</style>
<div class="contentArea">
    <h5>
        <asp:Label ID="lblCurrentFinancialYear" Font-Bold="True" runat="server" Text="Financial Year: {0}"></asp:Label>
    </h5>
    <br />
    <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
    <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
    <div style='color:#BF4900;font-size:14px;'> 
        Please review the following for next year creation
        <ul runat="server" id="liMessage">
        </ul>
       
    </div>
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td colspan="2">
                <asp:GridView Width="100%" CssClass="table table-primary mb30 table-bordered table-hover" ID="gvwPayrollPeriods" runat="server"
                    AutoGenerateColumns="False" DataKeyNames="PayrollPeriodId" 
                    GridLines="None" 
                    OnSelectedIndexChanged="gvwPayrollPeriods_SelectedIndexChanged" 
                    onrowcommand="gvwPayrollPeriods_RowCommand">
                    <Columns>
                        <asp:BoundField HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="140px" DataField="Name"
                            HeaderText="Payroll Period" />
                        <asp:BoundField HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="100px" DataField="StartDate"
                            HeaderText="Start Date" />
                        <asp:BoundField HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="100px" DataField="EndDate"
                            HeaderText="End Date" />
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="150px" >
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton1" CommandArgument='<%# Eval("PayrollPeriodId") %>' OnClientClick="return confirm('Conform Delete the payroll period?');" runat="server" Text="Delete" Visible='<%# IsVisible(Eval("PayrollPeriodId")) %>' CommandName="Delete1" />
                                &nbsp;
                                <asp:LinkButton ID="LinkButton2" CommandArgument='<%# Eval("PayrollPeriodId") %>'  OnClientClick="return confirm('Conform UnLock the payroll period?');"  runat="server" Visible='<%# IsVisible(Eval("PayrollPeriodId")) %>'  Text="UnLock-Salary" CommandName="UnLock" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:ImageButton Visible="false" ID="btnEdit" runat="server" CommandName="Select"
                                    ImageUrl="~/images/edit.gif" />
                            </ItemTemplate>
                        </asp:TemplateField>
                      
                    </Columns>
                    <RowStyle CssClass="odd" />
                    <AlternatingRowStyle CssClass="even" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <fieldset style='margin-top: 10px'>
                    <%--<legend>Create new payroll period</legend>--%>
                    <table class="innertable fieldTable " style='padding: 10px'>
                        <tr>
                            <td class="fieldHeader" style="width:100px">
                                Period Name
                            </td>
                            <td valign="top">
                                <table class="fieldTable">
                                    <tr>
                                        <td style="width:120px">
                                            <asp:TextBox Enabled="false" ID="txtPeriodName" Width="110px" runat="server"></asp:TextBox>
                                        </td>
                                         <td style="width:120px">
                                            <asp:DropDownList Enabled="false" ID="ddlMonths" Width="110px" onchange="createPeriod()"
                                                DataTextField="Value" DataValueField="Key" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                        <td style="width:120px">
                                            <asp:DropDownList Enabled="false" ID="ddlYears" Width="110px" onchange="createPeriod()"
                                                DataTextField="Value" DataValueField="Key" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="fieldHeader">
                            </td>
                            <td>
                                <asp:Button ID="btnCreate" CssClass="btn btn-primary btn-sm btn-sect" runat="server" Text="Create Payroll Period" OnClick="btnCreate_Click" />
                                &nbsp;
                                <asp:Button ID="btnCancel" CssClass="btn btn-default btn-sm btn-sect" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
    </table>
</div>
