﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MGTag.ascx.cs" Inherits="Web.UserControls.MGTag" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>

<script type="text/javascript">

    function popupTaxCall() {

        var ret = popupTax();
        if (typeof (ret) != 'undefined') {
            if (ret == 'ReloadTax') {
                refreshTaxList(0);
            }
        }
        return false;
    }

    function popupTaxCall(taxId) {
        if (typeof (EmployeeId) != 'undefined') {
            var ret = popupTax("EId=" + parseInt(EmployeeId));
            if (typeof (ret) != 'undefined') {
                if (ret == 'ReloadTax') {
                    refreshTaxList(0);
                }
            }
        }
        return false;
    }

        //call to delete income
    function deleteTaxCall(taxId) {
       
        if (confirm("Do you want to delete the tax for the employee?")) {
            refreshTaxList(taxId);
        }
        return false;
    }

    function refreshTaxList(taxId) {
        if (typeof (EmployeeId) != 'undefined') {
            Web.PayrollService.GetTaxList(taxId,parseInt(EmployeeId), refreshTaxListCallback);
        }
    }

    function refreshTaxListCallback(result) {
        if (result) {
            document.getElementById('<%=tax.ClientID %>').innerHTML
                = result;
        }
    }
</script>  
<div class="employeSectors" style=" padding-left:20px">
    <h2>Tax</h2>
     <div id="tax" runat="server">
        
    </div>
     <asp:Button ID="btnAddTax" CssClass="addbtns" runat="server" Enabled="false" OnClientClick="return popupTaxCall();"
                Text="Add" />
    
    </div>
