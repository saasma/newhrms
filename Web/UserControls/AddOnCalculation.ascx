﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddOnCalculation.ascx.cs"
    Inherits="Web.UserControls.AddOnCalculation" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/ErrorMsgCtl.ascx" TagName="ErrorMsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="InfoMsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<%@ Register TagPrefix="uc3" Namespace="Web.Controls" %>
<script src="../Scripts/jquery.lightbox_me.js?v=300" type="text/javascript"></script>
<style type="text/css">
    th
    {
        text-align: center !important; /*border: 1px solid #DDDDDD !important;
        border-bottom: 0px !important;*/
        overflow: hidden;
    }
    .tableLightColor th
    {
        padding: 2px 2px !important;
        vertical-align: middle;
        padding-left: 1px !important;
        border-top: 1px solid #BFD9FB !important;
        border-left: 1px solid #BFD9FB !important;
        border-right: 1px solid #BFD9FB !important;
        border-bottom: 1px solid #BFD9FB !important;
    }
    .tableLightColor td span
    {
        padding: 2px 2px !important;
    }
    .tableLightColor
    {
        font-size: 11px;
    }
    td
    {

        height: 25px !important;
        padding: 0px !important;
        border: 0px !important;
        margin: 0px !important;
        line-height: 1.6em;
    }
    .odd
    {
        border-top: 1px solid #DFECFD !important;
        border-bottom: 1px solid #DFECFD !important;
    }
    .calculationInput
    {
        background-color: #FAFCFF;
        border-top: 0px !important;
        border-left: 0px !important;
        border-bottom: 0px solid #DFECFD !important;
        border-right: 1px solid #DFECFD !important;
    }
    
    .nameHeaderClass
    {
        text-align: left !important;
        padding-left: 6px !important;
    }
    .employeeClass
    {
    	color:Black;
    }
    .tabDiv
    {
    	border-bottom: 2px solid #DFECFD;
        margin-left: 10px;
        clear: both;
        margin-top: 12px;
        margin-bottom: 5px;
        height: 23px;
        margin-right: 13px;
    }
    .tabDiv a
    {
    	display:inline-block;
    	width:100px;
    	color:#003F59;
    }
  .tabDiv div
{
    background-color:#fff;
    display: inline;
    width: 105px;
    padding-top: 8px;
    text-align: center;
    padding-bottom: 8px;
}
.tabDiv .selected
{
    background: white;
    border-bottom: 2px solid white;
    border-top: 2px solid #DFECFD;
    border-left: 2px solid #DFECFD;
    border-right: 2px solid #DFECFD;
    padding-top: 7px;
}

#wrapperDiv
{
  border: 1px solid #DFECFD;
}
    .employeeClass:hover{color:Black!important;text-decoration:none!important;;}
</style>
<script type="text/javascript">

    function showLoadingContent(element) {

        //alert($('#' + element.id).prop('disabled'));
        if ($('#' + element.id).prop('disabled') == 'disabled')
            return;


        $('#loadingStartingImage').show();
        $('#wrapperDiv').hide();

    }


    function clicked() {

        var value = "";

        if (document.getElementById('<%= rdbTax1.ClientID %>').checked)
            value = "true";
        else if (document.getElementById('<%= rdbTax2.ClientID %>').checked)
            value = "false";


        document.getElementById('<%= hiddenChecked.ClientID %>').value = value;

        __doPostBack('Generate', '');
    }

    $(document).ready(
            function () {
                //			 
                $('.tableLightColor tbody').attr('id', 'scrollMe');
                var sth = new ScrollTableHeader();
                sth.dynamicRowIndex = 1;
                //			 
                sth.addTbody("scrollMe");
                //                sth.delayAfterScroll = 150;
                //                sth.minTableRows  = 10;

                $('#loadingStartingImage').hide();
                $('#wrapperDiv').show();

                //  $(':text').('.adjustmentIncome').focus(function () { selectAllText($(this)) });

            }
			);


    function changeHeader(btn) {
        var payrollPeriodId = document.getElementById('<%= ddlPayrollPeriods.ClientID %>').value;

        if (payrollPeriodId == 0)
            return false;

        var ret = change("p=" + payrollPeriodId);


        return false;
    }

    function importPopupProcess(btn) {



        var payrollPeriodId = document.getElementById('<%= ddlPayrollPeriods.ClientID %>').value;

        if (payrollPeriodId == 0)
            return false;

        var ret = importPopup("payrollPeriod=" + payrollPeriodId);


        return false;
    }

    function refreshWindow() {
        __doPostBack('Reload', '');
    }

    var selEmpId = null;
    function ACE_item_selected(source, eventArgs) {
        var value = eventArgs.get_value();
        selEmpId = value;
        document.getElementById('<%= hiddenEmployeeID.ClientID %>').value = selEmpId;
    }


</script>
<img src='<%= Page.ResolveUrl("~/images/saving.gif") %>' id="loadingStartingImage"
    alt="" class="loadingStartingImage" />
<div class="pageheader">
    <div class="media">
        <div class="media-body">
            <h4>
                Add-on Pay List
            </h4>
        </div>
    </div>
</div>
<div class="contentpanel">
    <div class="attribute" style="padding:10px">
        <strong>Payroll period </strong>
        <asp:DropDownList Width="140px" onchange="showLoadingContent(this)" AppendDataBoundItems="true"
            DataTextField="Name" DataValueField="PayrollPeriodId" ID="ddlPayrollPeriods"
            runat="server" AutoPostBack="True">
        </asp:DropDownList>
        <asp:HiddenField ID="hiddenEmployeeID" runat="server" />
        <asp:HiddenField ID="hdnChangedInClientValues" runat="server" />
        <asp:TextBox ID="txtEmpSearch" runat="server" OnTextChanged="txtEmpSearch_TextChanged"
            AutoPostBack="true" Width="160" />
        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtEmpSearch"
            WatermarkText="Search Employee" WatermarkCssClass="searchBoxText" />
        <cc1:AutoCompleteExtender EnableCaching="true" ID="AutoCompleteExtenderOrganization"
            runat="server" MinimumPrefixLength="2" ServiceMethod="GetEmployeeNames" ServicePath="~/PayrollService.asmx"
            TargetControlID="txtEmpSearch" OnClientItemSelected="ACE_item_selected" CompletionSetCount="10"
            CompletionInterval="250" CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList"
            CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
        </cc1:AutoCompleteExtender>
        &nbsp; &nbsp;<strong style='display: none'>Department</strong>
        <asp:DropDownList Style='display: none' ID="ddlDepartments" AutoPostBack="true" AppendDataBoundItems="true"
            DataTextField="Name" DataValueField="DepartmentId" runat="server" Width="150px"
            OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged">
            <asp:ListItem Text="--Select Department--" Value="-1" Selected="True" />
        </asp:DropDownList>
        <asp:Button OnClientClick="" Style="display: inline" ID="btnLoad" runat="server"
            CssClass="btn btn-default btn-sect btn-sm" Text="Load" />
        &nbsp;&nbsp;
        <asp:LinkButton ID="btnImport" Style='text-decoration: underline; font-size: 12px'
            runat="server" Text="Import" OnClientClick="importPopupProcess();return false;" />
        &nbsp;&nbsp;
        <asp:LinkButton ID="btnChangeHeader" Style='text-decoration: underline; font-size: 12px'
            runat="server" Text="Select Incomes" OnClientClick="changeHeader();return false;" />
    </div>
    <div style="margin-bottom: 5px">
        <strong>Add-On Name</strong>
        <asp:TextBox ID="txtName" runat="server" Width="150" />
        <asp:RequiredFieldValidator ID="valName" runat="server" ErrorMessage="Name is required."
            ValidationGroup="Balance1" ControlToValidate="txtName" Display="None" />
        <strong>Show After</strong>
        <asp:DropDownList Width="140px" AppendDataBoundItems="true" DataTextField="Name"
            DataValueField="PayrollPeriodId" ID="ddlShowAfterPeriod" runat="server">
        </asp:DropDownList>
        <asp:CheckBox ID="chkShowInPayslip" runat="server" Text="Show in Payslip" />
        &nbsp;&nbsp;<asp:Button ID="btnSaveName1" runat="server" OnClientClick="valGroup='Balance1';return CheckValidation();"
            CssClass="btn btn-primary btn-sect btn-sm" OnClick="btnSaveName_Click" Text="Save Name" />
        &nbsp;&nbsp;<asp:Button ID="btnGenerateTax" runat="server" OnClientClick="$('#divGenerateTax').lightbox_me({ centered: true });return false;"
            CssClass="btn btn-warning btn-sect btn-sm" Text="Generate Tax" />
    </div>
    <asp:HiddenField ID="hiddenChecked" runat="server" />
    <uc2:MsgCtl ID="msgCtl" EnableViewState="false" Hide="true" runat="server" />
    <uc2:InfoMsgCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
    <uc2:ErrorMsgCtl Hide="true" ID="divErrorMsg" ErrorMessage="Values are not properly set so please resolve to proceed forward."
        style='display: none' runat="server" />
    <div id="divGenerateTax" style="display: none; width: 300px; height: 240px; background-color: white;
        border: 4px solid #0587b8; padding: 10px; background-color: #e8f1ff">
        <table>
            <tr>
                <td style="height: 30px">
                    <strong>Add Tax from Beginning incomes will be skipped here for tax calculation
                    </strong>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <br />
                    Pay additional tax in
                    <br />
                    <br />
                    <asp:RadioButton ID="rdbTax1" runat="server" Text="This pay" Checked="true" GroupName="tax" />
                    <br />
                    <br />
                    <asp:RadioButton ID="rdbTax2" runat="server" Text="Remaining months" Checked="false"
                        GroupName="tax" />
                    <br />
                    <br />
                    <asp:RadioButton ID="rdbTax3" runat="server" Text="This pay + remaining months" Checked="false"
                        GroupName="tax" />
                </td>
            </tr>
            <tr>
                <td style="height: 30px">
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Button ID="btnGenerate" runat="server" CssClass="save" OnClientClick="clicked();"
                        CausesValidation="false" OnClick="btnGenerate_Click" Text="Generate" />
                </td>
            </tr>
        </table>
    </div>
</div>
<div id="wrapperDiv" style="overflow: auto; overflow-y: hidden; display: none; margin-left: 10px;
    margin-right: 13px; clear: both">
    <cc2:EmptyDisplayGridView ID="gvw" CssClass="tableLightColor" UseAccessibleHeader="true"
        runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="CalculationId,EmployeeId,IsRetiredOrResigned,IsFinalSaved"
        GridLines="None" ShowFooterWhenEmpty="False" ShowHeaderWhenEmpty="True" EnableViewState="false"
        OnRowCreated="gvw_RowCreated" OnDataBound="gvw_DataBound">
        <Columns>
            <asp:TemplateField HeaderText="EIN" HeaderStyle-BackColor="#1B93D0" HeaderStyle-Width="42px"
                ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <asp:Label ID="lblId" Width="40px" runat="server" Text='<%# Eval("EmployeeId") %>'></asp:Label>
                    <asp:HiddenField ID="incomeListId" runat="server" Value="" />
                    <asp:HiddenField ID="deductionListId" runat="server" Value="" />
                    <%-- Don't include any value,instead use DataKeyNames--%>
                </ItemTemplate>
            </asp:TemplateField>
              <asp:TemplateField HeaderText="I No" HeaderStyle-BackColor="#1B93D0" HeaderStyle-Width="72px"
                    ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <asp:Label ID="lblINo" Width="70px" runat="server" Text='<%# Eval("IdCardNo") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            <asp:TemplateField HeaderStyle-BackColor="#1B93D0" HeaderStyle-CssClass="nameHeaderClass"
                HeaderText="&nbsp;Name">
                <ItemTemplate>
                    <%-- <asp:Label Width="150px" ID="Label2" Style='border-right: 1px solid #DDDDDD' runat="server"
                        Text='<%# "&nbsp;" + Eval("Name") %>'></asp:Label>--%>
                    <asp:HyperLink CssClass="employeeClass" Target="_blank" Width="150px" ID="Label2"
                        Style='border-right: 1px solid #DDDDDD' runat="server" NavigateUrl='<%# "~/newhr/EmployeePayroll.aspx?Id=" +  Eval("EmployeeId") %>'
                        Text='<%# "&nbsp;" + Eval("Name") %>'></asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Delete" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                <HeaderTemplate>
                    <img alt="Delete" title="Delete" src="../images/delet.png" />
                    <br />
                    <asp:CheckBox ID="chkDelete" onclick="selectDeselectAll(this)" runat="server" />
                </HeaderTemplate>
                <ItemTemplate>
                    <%-- <asp:Button ID="btnEdit" runat="server" Text="Edit" CommandName="Edit" Visible='<%# Convert.ToInt32(Eval("CalculationId")) != 0 %>' />
                
                <asp:Button ID="btnUpdate" runat="server" Text="Update" CommandName="Update" Visible='False' />--%>
                    <asp:CheckBox ID="chkDelete" ToolTip='<%# Convert.ToBoolean( Eval("IsFinalSaved")) == true ? "Salary already saved." : "" %>'
                        Enabled='<%# !Convert.ToBoolean( Eval("IsFinalSaved")) %>' Visible='<%# Convert.ToInt32(Eval("CalculationId"))!=0 %>'
                        runat="server" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <RowStyle CssClass="odd" />
        <AlternatingRowStyle CssClass="even" />
        <EmptyDataTemplate>
            <b>No salary records.</b>
        </EmptyDataTemplate>
    </cc2:EmptyDisplayGridView>
</div>
<div style='padding-left: 10px; margin-right: 13px;'>
    <uc1:PagingCtl ID="pagingCtl" ShowDropDown="true" OnChangePage="ChangePageNumber"
        PageSizeChangedJS="showLoadingContent(this);" OnNextRecord="btnNext_Click" OnPrevRecord="btnPrevious_Click"
        OnDropDownShowRecordsChanged="ddlRecords_SelectedIndexChanged" runat="server" />
</div>
