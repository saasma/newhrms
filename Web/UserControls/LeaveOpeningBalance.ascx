﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LeaveOpeningBalance.ascx.cs"
    Inherits="Web.UserControls.LeaveOpeningBalance" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<%@ Register Src="../Controls/MultiCheckCombo.ascx" TagName="MultiCheckCombo" TagPrefix="uc3" %>
<script type="text/javascript">
      <%
       
//        if( IsDisplayedAsPopup)
//        {
//            Response.Write("window.onunload = closePopup;");
//        }
         %>
         
        function closePopup() {
            clearUnload();
            if( typeof(balance) != 'undefined')
            window.returnValue = balance;
             window.close();
        }

        function clearUnload() {
            window.onunload = null;
        }
        
        function onTextboxChanged(txt)
        {
       
            if( txt.value == txt.OriginalText )
            {
                txt.className = '';
            }
            else
            {
                txt.className = 'textboxChangedHightlight';
            }
        }

        
        function importPopupProcess(btn) {


            if ( typeof(isAttendanceSaved) != 'undefined' && isAttendanceSaved == true)
            {
                if(confirm("Attendance already saved, do you really want to change the Opening Balance?")==false)
                return false;
            }

            var ret = importPopup('status=' + getSelectedStatusList());


            return false;
        }


        skipLoadingCheck = true;

</script>
<style>
    .tablforall tr td
    {
        padding-bottom: 0px !important;
    }
</style>
<div class="contentArea">
    <div class="alert alert-danger">
        <asp:Label ID="lbl" Visible="true" runat="server" Text="First period attendance has been already saved, so after the change in opening balance, re-save the first period attendance for the changed or all employees." /></div>
    <div class="attribute">
        <asp:Panel runat="server" DefaultButton="btnLoad" Style="padding: 10px">
            <table cellpadding="0" cellspacing="0" width="100%" class="tablforall">
                <tr>
                    <td width="160px">
                        <strong>Leave name</strong>
                    </td>
                    <td width="160px">
                        <strong>Employee name</strong>
                    </td>
                     <td width="160px">
                        <strong>Status</strong>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:DropDownList DataTextField="Title" AppendDataBoundItems="true" DataValueField="LeaveTypeId"
                            ID="ddlLeaveTypes" runat="server">
                            <asp:ListItem Text="--Select leave name--" Value="-1"></asp:ListItem>
                        </asp:DropDownList>
                        <%-- <asp:RequiredFieldValidator ID="valReqdName" InitialValue="-1" runat="server" ControlToValidate="ddlLeaveTypes"
                            Display="None" ErrorMessage="Select leave type." ValidationGroup="Load"></asp:RequiredFieldValidator>--%>
                    </td>
                    <td width="160px">
                        <asp:TextBox ID="txtSearch" Width="200px" runat="server"></asp:TextBox>
                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtSearch"
                            WatermarkText="Employee" WatermarkCssClass="searchBoxText" />
                        <cc1:AutoCompleteExtender EnableCaching="true" ID="AutoCompleteExtenderOrganization"
                            runat="server" MinimumPrefixLength="1" ServiceMethod="GetEmployeeNames" ServicePath="~/PayrollService.asmx"
                            TargetControlID="txtSearch" CompletionSetCount="10" CompletionInterval="250"
                            CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList"
                            CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
                        </cc1:AutoCompleteExtender>
                    </td>
                    <td>
                     
                        <uc3:MultiCheckCombo ID="multiStatus" runat="server" />
                    </td>
                    <td style='width: 120px'>
                        <asp:Button ID="btnLoad" CssClass="btn btn-default btn-sm btn-sect" CausesValidation="false"
                            OnClientClick="valGroup='Load';return CheckValidation();" ValidationGroup="Load"
                            runat="server" Text="Load" OnClick="btnLoad_Click" />
                    </td>
                    <td>
                        <asp:LinkButton ID="LinkButton1" Style='text-decoration: underline' runat="server"
                            Text="Import" OnClientClick="importPopupProcess();return false;" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
    <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
    <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
    <asp:UpdatePanel ID="upd" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <cc2:EmptyDisplayGridView CssClass="table table-primary mb30 table-bordered table-hover clear"
                Style="margin-bottom: 0px" UseAccessibleHeader="true" ShowHeaderWhenEmpty="True"
                ID="gvw" runat="server" AutoGenerateColumns="False" DataKeyNames="EmployeeId,LeaveTypeId,IsOpeningBalanceSaved,IsAttedanceSaved"
                ShowFooterWhenEmpty="False" GridLines="None" OnRowCreated="gvw_RowCreated" OnDataBound="gvw_DataBound"
                OnRowCancelingEdit="gvw_RowCancelingEdit" OnRowCommand="gvw_RowCommand" OnRowEditing="gvw_RowEditing">
                <Columns>
                    <asp:BoundField HeaderStyle-Width="45px" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"
                        DataField="EmployeeId" HeaderText="&nbsp;EIN"></asp:BoundField>
                    <asp:BoundField HeaderStyle-Width="220px" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                        DataField="Name" HeaderText="Employee name"></asp:BoundField>
                    <asp:BoundField HeaderStyle-Width="110px" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                        DataField="StatusText" HeaderText="Status"></asp:BoundField>
                    <asp:BoundField HeaderStyle-Width="180px" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                        DataField="LeaveName" HeaderText="Leave name"></asp:BoundField>
                    <asp:TemplateField HeaderStyle-Width="130px" HeaderStyle-HorizontalAlign="Right"
                        ItemStyle-HorizontalAlign="Right" HeaderText="Accumulate / Prev Year">
                        <ItemTemplate>
                            <%--<asp:Label Width="50px" Style='text-align: right'  ID="lblOpeningBalance" runat="server" Text='<%# Eval("OpeningBalance") %>' />--%>
                            <%--Visible='<%# !Convert.ToBoolean(Eval("IsPaymentStopped")) %>'--%>
                            <asp:TextBox Width="80px" Enabled="false" OriginalText='<%# Eval("OpeningBalance") %>'
                                Style='text-align: right' onchange="onTextboxChanged(this)" ID="txtOpeningBalance"
                                Text='<%# Eval("OpeningBalance") %>' runat="server"></asp:TextBox>
                            <asp:CompareValidator Visible="false" SetFocusOnError="true" ValueToCompare="0" Display="None"
                                Operator="DataTypeCheck" Type="Double" ID="valOpeningBalance" ControlToValidate="txtOpeningBalance"
                                ValidationGroup="Balance" runat="server" ErrorMessage="Invalid opening balance."></asp:CompareValidator>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="120px" HeaderStyle-HorizontalAlign="Right"
                        ItemStyle-HorizontalAlign="Right" HeaderText="Curr Year Balance">
                        <ItemTemplate>
                            +
                            <asp:TextBox Width="70px" Enabled="false" OriginalText='<%# Eval("CurrentYearBalance") %>'
                                Style='text-align: right' onchange="onTextboxChanged(this)" ID="txtCYBalance"
                                Text='<%# Eval("CurrentYearBalance") %>' runat="server"></asp:TextBox>
                            <asp:CompareValidator Visible="false" SetFocusOnError="true" ValueToCompare="0" Display="None"
                                Operator="DataTypeCheck" Type="Double" ID="valOpeningBalance1" ControlToValidate="txtCYBalance"
                                ValidationGroup="Balance" runat="server" ErrorMessage="Invalid C/Y balance."></asp:CompareValidator>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="120px" HeaderStyle-HorizontalAlign="Right"
                        ItemStyle-HorizontalAlign="Right" HeaderText="Curr Year Taken">
                        <ItemTemplate>
                            -
                            <asp:TextBox Width="70px" Enabled="false" OriginalText='<%# Eval("OpeningTaken") %>'
                                Style='text-align: right' onchange="onTextboxChanged(this)" ID="txtCYTaken" Text='<%# Eval("OpeningTaken") %>'
                                runat="server"></asp:TextBox>
                            <asp:CompareValidator Visible="false" SetFocusOnError="true" ValueToCompare="0" Display="None"
                                Operator="DataTypeCheck" Type="Double" ID="valOpeningBalance11" ControlToValidate="txtCYTaken"
                                ValidationGroup="Balance" runat="server" ErrorMessage="Invalid Current year taken balance."></asp:CompareValidator>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="120px" HeaderStyle-HorizontalAlign="Right"
                        ItemStyle-HorizontalAlign="Right" HeaderText="Total Opening">
                        <ItemTemplate>
                            <%#  Convert.ToDouble(Eval("OpeningBalance")) + Convert.ToDouble(Eval("CurrentYearBalance")) - Convert.ToDouble(Eval("OpeningTaken"))%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="120px">
                        <ItemTemplate>
                            <asp:Button ID="btnEdit" runat="server" Text="Edit" Style='border: 1px solid gray;
                                height: 24px; cursor: pointer' Visible="false" CommandName="Edit" />
                            <asp:Button ID="btnCancel" runat="server" Visible="false" Style='border: 1px solid gray;
                                height: 24px; cursor: pointer' Text="Cancel" CommandName="Cancel" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="" HeaderStyle-Width="320px"></asp:TemplateField>
                </Columns>
                <PagerSettings Mode="NumericFirstLast" NextPageText="Next »" PreviousPageText="« Prev"
                    FirstPageText="First" LastPageText="Last" />
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                <EmptyDataTemplate>
                    <b>No records</b>
                </EmptyDataTemplate>
            </cc2:EmptyDisplayGridView>
            <uc1:PagingCtl ID="pagintCtl" OnNextRecord="btnNext_Click" OnPrevRecord="btnPrevious_Click"
                OnDropDownShowRecordsChanged="ddlRecords_SelectedIndexChanged" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <%--<asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Balance" DisplayMode="SingleParagraph"
        ShowSummary="false" ShowMessageBox="true" runat="server"></asp:ValidationSummary>--%>
    <div class="buttonsDivSection" style="margin-top: 10px;">
        <asp:Button ID="btnSave" OnClientClick="valGroup='Balance';return CheckValidation();"
            CssClass="btn btn-primary btn-sm btn-sect" Visible="false" ValidationGroup="Balance"
            runat="server" Text="Save Changes" OnClick="btnSave_Click" />
    </div>
</div>
