﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ManageBankBranch.ascx.cs"
    Inherits="Web.UserControls.ManageBankBranch" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<style type="text/css">
    table.alignttable tr td
    {
        vertical-align: top;
        padding-bottom: 10px;
    }
    table.alignttable tr td input, table.alignttable tr td select
    {
        float: left;
    }
</style>
<script type="text/javascript">



 

    function handleDelete() {
        if (confirm('Do you want to delete the Branch?')) {
           
            return true;
        }
        else
            return false;
    }

</script>
<div>
    <div class="marginal1">
        <asp:GridView CssClass="table table-primary mb30 table-bordered table-hover" PagerStyle-HorizontalAlign="Center" PagerStyle-CssClass="defaultPagingBar"
            UseAccessibleHeader="true" ID="gvwBranches" CellPadding="3" Width="80%" runat="server"
            DataKeyNames="BankBranchID" AutoGenerateColumns="False" GridLines="None" OnSelectedIndexChanged="gvwBranches_SelectedIndexChanged"
            AllowPaging="True" PageSize="200" OnPageIndexChanging="gvwBranches_PageIndexChanging"
            OnRowDeleting="gvwBranches_RowDeleting">
            <Columns>
                <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                    DataField="BankName" HeaderText="Branch Name">
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                    DataField="Name" HeaderText="Name">
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundField>
             
                <asp:TemplateField HeaderText="Edit" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="30px">
                    <ItemTemplate>
                        <asp:ImageButton ID="ImageButton1" runat="server" OnClientClick="clearUnload();"
                            CommandName="Select" ImageUrl="~/images/edit.gif" />
                    </ItemTemplate>
                    <HeaderStyle Width="30px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Delete" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="30px">
                    <ItemTemplate>
                        <asp:ImageButton ID="ImageButton2" OnClientClick="return handleDelete()" runat="server"
                            CommandName="Delete" ImageUrl="~/images/delet.png" />
                    </ItemTemplate>
                    <HeaderStyle Width="30px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:TemplateField>
            </Columns>
            <RowStyle CssClass="odd" />
            <AlternatingRowStyle CssClass="even" />
            <SelectedRowStyle CssClass="selected" />
            <PagerStyle HorizontalAlign="Center" CssClass="defaultPagingBar"></PagerStyle>
            <EmptyDataTemplate>
                No list has been created.
            </EmptyDataTemplate>
        </asp:GridView>
        <div class="buttonsDivSection" style='clear: both; width: 100%!important;'>
            <asp:LinkButton ID="btnAddNew" runat="server" Style="margin-top: 5px;"
                CssClass="btn btn-primary btn-sect btn-sm" Text="Add New Bank Branch" OnClick="btnAddNew_Click" />
        </div>
        <uc2:MsgCtl ID="divMsgCtl" Width='600px' EnableViewState="false" Hide="true" runat="server" />
        <uc2:WarningCtl ID="divWarningMsg" Width='600px' EnableViewState="false" Hide="true"
            runat="server" />
        <asp:Panel ID="details" runat="server" DefaultButton="btnSave" class="bevel" Style="margin-top: 20px;"
            Visible="false">
            <div class="fields paddpop" style="padding: 10px 0pt 20px 10px ! important;">
                <h2 class="popstitle">
                    Branch Information</h2>
                <table class="fieldTable">
                    <tr>
                        <td class="fieldHeader">
                            <My:Label ID="Label2" Text="Name" runat="server" ShowAstrick="true" />
                        </td>
                        <td>
                            <asp:TextBox ID="txtName" runat="server" Width="180px" />
                            <asp:RequiredFieldValidator ID="valReqdName" runat="server" ControlToValidate="txtName"
                                Display="None" ErrorMessage="Name is required." ValidationGroup="AEBranch"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="fieldHeader">
                            <My:Label ID="Label1" Text="Bank" runat="server" ShowAstrick="false" />
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlBank" runat="server" Width="180px" DataValueField="BankID" DataTextField="Name" />
                             <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlBank"
                                Display="None" ErrorMessage="Bank is required." ValidationGroup="AEBranch"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td style="padding-top: 10px;">
                            <asp:Button ID="btnSave" CssClass="update" OnClientClick="valGroup='AEBranch';return CheckValidation()"
                                runat="server" Text="Save" ValidationGroup="AEBranch" OnClick="btnSave_Click" />
                            <asp:Button ID="btnCancel" OnClientClick="clearUnload()" CssClass="cancel" runat="server"
                                Text="Cancel" OnClick="btnCancel_Click" />
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
        <%--</fieldset>--%>
    </div>
</div>
