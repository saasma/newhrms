﻿using System;
using System.Linq;
using BLL.Manager;
using Utils.Helper;
using BLL;
using DAL;

namespace Web.UserControls.HR
{
    public partial class MGDiscipline : System.Web.UI.UserControl
    {
        private EEmployee employee = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            RegisterEmployeeIDInScript(0);
            JavascriptHelper.AttachPopUpCode(Page, "popupUpdateDiscipline", UrlHelper.GetRootUrl() + "CP/pp/AEDisciplinaryAction.aspx", 480, 420);
            btnAdd.Visible = !SessionManager.IsReadOnlyUser;

            if (SessionManager.CurrentLoggedInEmployeeId != 0)
            {
                btnAdd.Visible = false;
            }
        }
        private void RegisterEmployeeIDInScript(int empId)
        {
            if (empId == 0)
            {
                empId = UrlHelper.GetIdFromQueryString("empId");
            }
            else
            {
                empId = 0;
            }
            if (empId != 0)
            {
                employee = EmployeeManager.GetEmployeeById(empId);
                if (employee != null)
                {
                    Page.ClientScript.RegisterClientScriptBlock(GetType(), "EmpIDKey", "\nEmployeeId=" + empId + ";", true);
                }
            }
        }

        public void DisplayDiscipline(int employeeId)
        {
            var mgr = new HRManager();
            disciplines.InnerHtml = mgr.GetHTMLEmployeeDiscipline(employeeId);
            btnAdd.Enabled = true;
        }
    }
}
