﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using Utils.Web;
using Utils.Helper;
using DAL;

namespace Web.UserControls.HR
{
    public partial class EmpJobDesc : System.Web.UI.UserControl
    {
        CommonManager comm = new CommonManager();
        EmployeeManager empMgr = new EmployeeManager();

        protected void Page_PreRender(object sender, EventArgs e)
        {
            SetPagePermission();
        }

        /// <summary>
        /// Set for Custom Role if is in Readonly Mode or Not
        /// </summary>
        public void SetPagePermission()
        {
            if (SessionManager.IsCustomRole && SessionManager.CustomRole.IsReadOnly != null && SessionManager.CustomRole.IsReadOnly.Value)
            {
                btnAddLocation.Visible = false;
                locationHistory.Visible = false;
                btnAddProgramme.Visible =false;
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            LoadLocations();
            LoadProgrammes();
            LoadProjects();
            JavascriptHelper.AttachPopUpCode(Page, "locationPopup", "ManageLocation.aspx", 440, 230);
            JavascriptHelper.AttachPopUpCode(Page, "programmePopup", "ManageProgramme.aspx", 440, 230);
            JavascriptHelper.AttachPopUpCode(Page, "locationHistoryPopup", "LocationHistory.aspx", 500, 480);
            if (!IsPostBack)
            {
                //calStart.IsEnglishCalendar = SessionManager.CurrentCompany.IsEnglishDate;
                //calStart.SelectTodayDate();

                //calEnd.IsEnglishCalendar = SessionManager.CurrentCompany.IsEnglishDate;
                //calEnd.SelectTodayDate();
                rowJobNature.Visible = CommonManager.CompanySetting.IsD2 == false;
                reqdProgramme.Enabled = CommonManager.CompanySetting.WhichCompany == WhichCompany.PSI;
            }
        }

        void LoadLocations()
        {
            string id = Request.Form[ddlLocation.UniqueID];
            ddlLocation.DataSource = comm.GetAllLocations();
            ddlLocation.DataBind();
            if (id != null)
                UIHelper.SetSelectedInDropDown(ddlLocation, id);

        }

        void LoadProjects()
        {
            int total=0;
            ddlLeaveProject.DataSource = LeaveRequestManager.GetProjectList("", 1, 9999, ref total).OrderBy(x => x.Name).ToList();
            ddlLeaveProject.DataBind();


            ddlVoucherGroup.DataSource = CommonManager.GetVoucherGroupsForEmp();
            ddlVoucherGroup.DataBind();
        }

        void LoadProgrammes()
        {
            string id = Request.Form[ddlProgramme.UniqueID];
            ddlProgramme.DataSource = comm.GetAllProgrammes();
            ddlProgramme.DataBind();
            if (id != null)
                UIHelper.SetSelectedInDropDown(ddlProgramme, id);
        }

        public void DisplayContribution(int employeeId)
        {
            //contributions.InnerHtml = empMgr.GetContribution(employeeId);
        }

        public void UpdateModeDisplayForHistory(int empId)
        {
            if (empId != 0)
            {
                EEmployee emp = EmployeeManager.GetEmployeeById(empId);
                UIHelper.SetSelectedInDropDown(ddlLocation, emp.EHumanResources[0].LocationIdLatest);

                if (emp.EHumanResources[0].LocationId != null)
                {
                    ddlLocation.Enabled = false;
                    locationHistory.Visible = true;
                    //disable validation control as well
                    valLocation1.Enabled = false;
                }

                //sometime the emp.EHumanResources[0].LocationId is null even the emp has location so
                //for that case 
                if (ddlLocation.SelectedValue!="-1")
                {
                    ddlLocation.Enabled = false;
                    locationHistory.Visible = true;
                    //disable validation control as well
                    valLocation1.Enabled = false;
                }
            }


            
        }

        public void Process(EHumanResource entity,bool isDisplay,ref LeaveProjectEmployee leaveEmpProject)
        {
            if (isDisplay)
            {
                txtIDCartNo.Text = entity.IdCardNo;
                txtJobNature.Text = entity.JobNature;
                UIHelper.SetSelectedInDropDown(ddlLocation, entity.LocationIdLatest);
                UIHelper.SetSelectedInDropDown(ddlProgramme, entity.ProgrammeId);
                AfterSave();
                DisplayContribution(entity.EmployeeId);

                LeaveProjectEmployee leave = LeaveRequestManager.GetLeaveProjectForEmployee(entity.EmployeeId);
                if (leave != null)
                {
                    ddlLeaveProject.SelectedValue = leave.LeaveProjectId.ToString();
                }

                if (entity.VoucherDepartmentID != null)
                    UIHelper.SetSelectedInDropDown(ddlVoucherGroup, entity.VoucherDepartmentID);

            }
            else
            {
                leaveEmpProject = new LeaveProjectEmployee();
                leaveEmpProject.LeaveProjectId = int.Parse(ddlLeaveProject.SelectedValue);

                entity.IdCardNo = txtIDCartNo.Text.Trim();
                entity.JobNature = txtJobNature.Text.Trim();
                entity.LocationId = int.Parse(ddlLocation.SelectedValue);
                if (ddlLocation.SelectedValue.Equals("-1") == false)
                    entity.LocationId = int.Parse(ddlLocation.SelectedValue);
                else
                    entity.LocationId = null;
                entity.ProgrammeId = int.Parse(ddlProgramme.SelectedValue);
                entity.VoucherDepartmentID = int.Parse(ddlVoucherGroup.SelectedValue);
            }
        }

        public void AfterSave()
        {
            //btnAdd.Attributes.Remove("disabled");
        }
    }
}