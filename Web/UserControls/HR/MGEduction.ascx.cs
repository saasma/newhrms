﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Manager;
using Utils.Helper;
using BLL;
using DAL;

namespace Web.UserControls.HR
{
    public partial class MGEduction : System.Web.UI.UserControl
    {
        EEmployee employee = null;
        EmployeeManager empMgr = new EmployeeManager();

        protected void Page_Load(object sender, EventArgs e)
        {
            RegisterEmployeeIDInScript(0);
            JavascriptHelper.AttachPopUpCode(Page, "popupUpdateEducation", UrlHelper.GetRootUrl() + "CP/pp/AEEmpEducation.aspx", 510, 450);
            btnAddLeave.Visible = !SessionManager.IsReadOnlyUser;

        }
        void RegisterEmployeeIDInScript(int empId)
        {
            if (empId == 0)
                empId = UrlHelper.GetIdFromQueryString("empId");
            else
                empId = 0;

            if (empId != 0)
            {
                employee = EmployeeManager.GetEmployeeById(empId);
                if (employee != null)
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "EmpIDKey", "\nEmployeeId=" + empId + ";", true);
            }
        }


        public void DisplayEducation(int employeeId)
        {
            HRManager mgr = new HRManager();
            educations.InnerHtml = mgr.GetHTMLEmployeeEduction(employeeId);
            btnAddLeave.Enabled = true;
        }
        public void EnableButton()
        {
            //btnAddLeave.Enabled = true;
        }
    }
}