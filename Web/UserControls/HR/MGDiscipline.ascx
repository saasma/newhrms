﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MGDiscipline.ascx.cs" Inherits="Web.UserControls.HR.MGDiscipline" %>
<script type="text/javascript">


        function popupUpdateDisciplineCall(disciplinaryId) {
            if (typeof (EmployeeId) != 'undefined') {
            var ret = popupUpdateDiscipline('Id=' + disciplinaryId  + "&EId=" + parseInt(EmployeeId));
            if (typeof (ret) != 'undefined') {
                if (ret == 'Reload') {
                    refreshDisciplineList(0);
                }
            }
           }
            return false;
        }

        //call to delete income
        function deleteDisciplineCall(disciplinaryId) {
            if (confirm("Do you want to delete the disciplinary actions for this employee?")) {
                refreshDisciplineList(disciplinaryId);
            }
            return false;
        }

        function refreshDisciplineList(disciplinaryId, popupWindow) {
            if (typeof (popupWindow) != 'undefined')
                popupWindow.close();

         if (typeof (EmployeeId) != 'undefined') {
            Web.PayrollService.GetDisciplinaryActionList(disciplinaryId, parseInt(EmployeeId), refreshDisciplineListCallback);
        }
       }

        function refreshDisciplineListCallback(result) {
            if (result) {
                document.getElementById('<%=disciplines.ClientID %>').innerHTML
                = result;
            }
        }
</script>    
<fieldset class="hrTabFieldset">
    <%--<legend><b>Disciplinary Actions</b></legend>--%>
    
     <div id="disciplines" runat="server">
        
    </div>
     <asp:LinkButton   CssClass="createbtns"  ID="btnAdd" runat="server"  OnClientClick="return popupUpdateDisciplineCall();"
                Text="Add Disciplinary Action" />
    
    </fieldset>