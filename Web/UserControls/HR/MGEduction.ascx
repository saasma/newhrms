﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MGEduction.ascx.cs" Inherits="Web.UserControls.HR.MGEduction" %>

<script type="text/javascript">


        function popupUpdateEducationCall(educationId) {
            if (typeof (EmployeeId) != 'undefined') {
            var ret = popupUpdateEducation('Id=' + educationId + "&EId=" + parseInt(EmployeeId));
            if (typeof (ret) != 'undefined') {
                if (ret == 'Reload') {
                    refreshEducationList(0);
                }
            }
          }
            return false;
        }

        //call to delete income
        function deleteEducationCall(educationId) {
            if (confirm("Do you want to delete the education for the employee?")) {
                refreshEducationList(educationId);
            }
            return false;
        }

        function refreshEducationList(educationId, popupWindow) {
         
         if (typeof (popupWindow) != 'undefined')
                popupWindow.close();

         if (typeof (EmployeeId) != 'undefined') {
            Web.PayrollService.GetEducationList(educationId,parseInt(EmployeeId), refreshEducationListCallback);
        }
      }

        function refreshEducationListCallback(result) {
            if (result) {
                document.getElementById('<%=educations.ClientID %>').innerHTML
                = result;
            }
        }
</script>    
<fieldset class="hrTabFieldset">
  <%--  <legend><b>Education Details</b></legend>--%>
    
     <div id="educations" runat="server">
        
    </div>
     <asp:LinkButton ID="btnAddLeave" runat="server"   CssClass="createbtns"  OnClientClick="return popupUpdateEducationCall();"
                Text="Add Education" />
    
    </fieldset>