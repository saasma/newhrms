﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MGTraining.ascx.cs" Inherits="Web.UserControls.HR.MGTraining" %>


<script type="text/javascript">


        function popupUpdateTrainingCall(trainingId) {
        if (typeof (EmployeeId) != 'undefined') {
            var ret = popupUpdateTraining('Id=' + trainingId + "&EId=" + parseInt(EmployeeId));
            if (typeof (ret) != 'undefined') {
                if (ret == 'Reload') {
                    refreshTrainingList(0);
                }
            }
           }
            return false;
        }

        //call to delete income
        function deleteTrainingCall(trainingId) {
            if (confirm("Do you want to delete the training for this employee?")) {
                refreshTrainingList(trainingId);
            }
            return false;
        }

        function refreshTrainingList(trainingId, popupWindow) {
            if (typeof (popupWindow) != 'undefined')
                popupWindow.close();

        if (typeof (EmployeeId) != 'undefined') {
            Web.PayrollService.GetTrainingList(trainingId, parseInt(EmployeeId), refreshTrainingListCallback);
        }
        }

        function refreshTrainingListCallback(result) {
            if (result) {
                document.getElementById('<%=training.ClientID %>').innerHTML
                = result;
            }
        }
</script>    
<fieldset  class="hrTabFieldset">
   <%-- <legend><b>Trainings</b></legend>--%>
    
     <div id="training" runat="server">
        
    </div>
     <asp:LinkButton ID="btnAddTraining"   CssClass="createbtns"  runat="server"  OnClientClick="return popupUpdateTrainingCall();"
                Text="Add Training" />
    
    </fieldset>