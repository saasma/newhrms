﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MGFamily.ascx.cs" Inherits="Web.UserControls.HR.MGFamily" %>



<script type="text/javascript">


        function popupUpdateFamilyCall(familyId) {
        if (typeof (EmployeeId) != 'undefined') {
            var ret = popupUpdateFamily('Id=' + familyId + "&EId=" + parseInt(EmployeeId));
            if (typeof (ret) != 'undefined') {
                if (ret == 'Reload') {
                    refreshFamilyList(0);
                }
            }
            
          }
            return false;
        }

        //call to delete income
        function deleteFamilyCall(familyId) {
            if (confirm("Do you want to delete the family for the employee?")) {
                refreshFamilyList(familyId);
            }
            return false;
        }

        function refreshFamilyList(familyId, popupWindow) {

            if (typeof (popupWindow) != 'undefined')
                popupWindow.close();


         if (typeof (EmployeeId) != 'undefined')
             {
            Web.PayrollService.GetFamilyList(familyId, parseInt(EmployeeId), refreshFamilyListCallback);
        }
      }

        function refreshFamilyListCallback(result) {
            if (result) {
                document.getElementById('<%=families.ClientID %>').innerHTML
                = result;
            }
        }
</script>    
<fieldset  class="hrTabFieldset">
   <%-- <legend><b>Family Member Details</b></legend>--%>
    
     <div id="families" runat="server">
        
    </div>
     <asp:LinkButton ID="btnAdd"    CssClass="createbtns"  runat="server"  OnClientClick="return popupUpdateFamilyCall();"
                Text="Add Family" />
    
    </fieldset>