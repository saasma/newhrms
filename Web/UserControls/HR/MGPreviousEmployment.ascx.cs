using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using BLL.Manager;
using Utils.Helper;
using BLL;
using DAL;

namespace Web.UserControls.HR
{
    public partial class MGPreviousEmployment : System.Web.UI.UserControl
    {
        EEmployee employee = null;
        EmployeeManager empMgr = new EmployeeManager();

        protected void Page_Load(object sender, EventArgs e)
        {
            RegisterEmployeeIDInScript(0);
            JavascriptHelper.AttachPopUpCode(Page, "popupUpdateEmployment", UrlHelper.GetRootUrl() + "CP/pp/AEPreviousEmployment.aspx", 480, 420);
            btnAdd.Visible = !SessionManager.IsReadOnlyUser;
        }
        void RegisterEmployeeIDInScript(int empId)
        {
            if (empId == 0)
                empId = UrlHelper.GetIdFromQueryString("empId");
            else
                empId = 0;

            if (empId != 0)
            {
                employee = EmployeeManager.GetEmployeeById(empId);
                if (employee != null)
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "EmpIDKey", "\nEmployeeId=" + empId + ";", true);
            }
        }
        public void DisplayPreviousEmployment(int employeeId)
        {
            HRManager mgr = new HRManager();
            employments.InnerHtml = mgr.GetHTMLPreviousEmployment(employeeId);
            btnAdd.Enabled = true;
        }
    }
}