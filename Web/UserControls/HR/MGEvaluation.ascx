﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MGEvaluation.ascx.cs" Inherits="Web.UserControls.HR.MGEvaluation" %>


<script type="text/javascript">


        function popupUpdateEvaluationCall(evaluationId) {
        if (typeof (EmployeeId) != 'undefined')
             {
            var ret = popupUpdateEvaluation('Id=' + evaluationId + "&EId=" + parseInt(EmployeeId));
            if (typeof (ret) != 'undefined') {
                if (ret == 'Reload') {
                    refreshEvaluationList(0);
                }
            }
          }
            return false;
        }

        //call to delete income
        function deleteEvaluationCall(evaluationId) {
            if (confirm("Do you want to delete the evaluations for this employee?")) {
                refreshEvaluationList(evaluationId);
            }
            return false;
        }

        function refreshEvaluationList(evaluationId, popupWindow) {

            if (typeof (popupWindow) != 'undefined')
                popupWindow.close();

           if (typeof (EmployeeId) != 'undefined')
             {
            Web.PayrollService.GetEvaluationList(evaluationId,parseInt(EmployeeId), refreshEvaluationListCallback);
        }
      }

        function refreshEvaluationListCallback(result) {
            if (result) {
                document.getElementById('<%=evaluations.ClientID %>').innerHTML
                = result;
            }
        }
</script>    
<fieldset  class="hrTabFieldset">
    <%--<legend><b>Appraisal/Evaluation</b></legend>--%>
    
     <div id="evaluations" runat="server">
        
    </div>
     <asp:LinkButton   CssClass="createbtns"   ID="btnAdd" runat="server"  OnClientClick="return popupUpdateEvaluationCall();"
                Text="Add Evaluation" />
    
    </fieldset>