﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MGAccident.ascx.cs" Inherits="Web.UserControls.HR.MGAccident" %>


<script type="text/javascript">


        function popupUpdateAccidentCall(accidentId) {
         if (typeof (EmployeeId) != 'undefined') {
            var ret = popupUpdateAccident('Id=' + accidentId + "&EId=" + parseInt(EmployeeId));
            if (typeof (ret) != 'undefined') {
                if (ret == 'Reload') {
                    refreshAccidentList(0);
                }
            }
         }
            return false;
     }

        //call to delete income
        function deleteAccidentCall(accidentId) {
            if (confirm("Do you want to delete this accident for this employee?")) {
                refreshAccidentList(accidentId);
            }
            return false;
        }

        function refreshAccidentList(accidentId, popupWindow) {
            if (typeof (popupWindow) != 'undefined')
                popupWindow.close();
        if (typeof (EmployeeId) != 'undefined')
             {
            Web.PayrollService.GetAccidentList(accidentId, parseInt(EmployeeId), refreshAccidentListCallback);
        }
      }

        function refreshAccidentListCallback(result) {
            if (result) {
                document.getElementById('<%= accidents.ClientID %>').innerHTML
                = result;
            }
        }
</script>    
<fieldset   class="hrTabFieldset">
   <%-- <legend><b>Accident</b></legend>--%>
    
     <div id="accidents" runat="server">
        
    </div>
     <asp:LinkButton   CssClass="createbtns"  ID="btnAdd" runat="server"  OnClientClick="return popupUpdateAccidentCall();"
                Text="Add Accident" />
    
    </fieldset>