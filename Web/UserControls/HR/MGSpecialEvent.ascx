﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MGSpecialEvent.ascx.cs" Inherits="Web.UserControls.HR.MGSpecialEvent" %>


<script type="text/javascript">


        function popupUpdateEventCall(eventId) {
            if (typeof (EmployeeId) != 'undefined')
             {
                var ret = popupUpdateEvent('Id=' + eventId + "&EId=" + parseInt(EmployeeId));
                if (typeof (ret) != 'undefined') {
                    if (ret == 'Reload') {
                        refreshEventList(0);
                }
            }
          }
            return false;
        }

        //call to delete income
        function deleteEventCall(eventId) {
            if (confirm("Do you want to delete this event?")) {
                refreshEventList(eventId);
            }
            return false;
        }


        function refreshEventList(eventId, popupWindow) {
            if (typeof (popupWindow) != 'undefined')
                popupWindow.close();

            if (typeof (EmployeeId) != 'undefined') {
                Web.PayrollService.GetSpecialEventList(eventId, parseInt(EmployeeId), refreshEventListCallback);
            }
        }

        function refreshEventListCallback(result) {
            if (result) {
                document.getElementById('<%=specialEvents.ClientID %>').innerHTML
                = result;
            }
        }
</script>    
<fieldset   class="hrTabFieldset">
   <%-- <legend><b>Special Events</b></legend>--%>
    
     <div id="specialEvents" runat="server">
        
    </div>
     <asp:LinkButton   CssClass="createbtns"   ID="btnAdd" runat="server"  OnClientClick="return popupUpdateEventCall();"
                Text="Add Event" />
    
    </fieldset>