﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using Utils;
using Utils.Helper;
using BLL.Base;

namespace Web.UserControls.HR
{
    public partial class HRDetailsCtl : BaseUserControl
    {

        public bool IsEmployeePage
        {
            get;
            set;
        }

        public int DivHeight
        {
            set
            {
                tabHRDetails.Height = new Unit(value);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                Initialise();

            if (IsEmployeePage)
            {
                JavascriptHelper.AttachPopUpCode(Page, "popImageUpload", "Others/ImageUpload.aspx", 460, 250);
            }
            else
                JavascriptHelper.AttachPopUpCode(Page, "popImageUpload", "ImageUpload.aspx", 460, 250);

            if (this.Request.Form["__EVENTTARGET"] != null && this.Request.Form["__EVENTTARGET"].Equals(updPanel.ClientID))
            {

                DisplayDocuments();
            }
            SetPagePermission();
        }

        /// <summary>
        /// Set for Custom Role if is in Readonly Mode or Not
        /// </summary>
        public void SetPagePermission()
        {
            if (SessionManager.IsCustomRole && SessionManager.CustomRole.IsReadOnly != null && SessionManager.CustomRole.IsReadOnly.Value)
            {

                btnAddPhoto.Visible = false;
                btnSignature.Visible = false;
                btnThumbPrint.Visible = false;
                btnAddDocument.Visible = false;

            }
        }

        void DisplayDocuments()
        {
            if (Request.QueryString["EmpId"] != null)
            {
                int empid;
                if (int.TryParse(Request.QueryString["EmpId"], out empid))
                {
                    DisplayDocuments(empid);


                    HHumanResource entity = new HRManager().GetHumanResource(empid);
                    if (entity != null)
                    {

                        Process(entity,true);
                      
                    }
                }
            }
        }

        protected void gvw_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvw.PageIndex = e.NewPageIndex;

            DisplayDocuments();
        }

        protected void btnDelete_Click(object sender, CommandEventArgs e)
        {
            int documentId = int.Parse(e.CommandArgument.ToString());  //(int)gvw.DataKeys[e.RowIndex][0];
            if (documentId != 0)
            {
                HRManager mgr = new HRManager();
                HDocument doc = new HDocument();
                doc.DocumentId = documentId;
                mgr.DeleteDocument(doc);


                if (SessionManager.CurrentLoggedInEmployeeId != 0)
                    DisplayDocuments(SessionManager.CurrentLoggedInEmployeeId);
                else

                DisplayDocuments();

                
            }
        }

        void Initialise()
        {
            if (this.IsPostBack)
            {
                calPassportIssueDate.IsEnglishCalendar = true;
                calPassportValidUpto.IsEnglishCalendar = true;
                calPassportIssueDate.SelectTodayDate();
                calPassportValidUpto.SelectTodayDate();
            }

             img1.Width = Config.ImageWidth;
             img1.Height = Config.ImageHeight;

            img2.Width = Config.ImageWidth;
            img2.Height = Config.ImageHeight;

            //imgThumbPrint.Width = Config.ImageWidth;
            //imgThumbPrint.Height = Config.ImageHeight;
            img3.Width = Config.ImageWidth;
            img3.Height = Config.ImageHeight;


            //load documents in grid


        }

        public void Process(HHumanResource entity,bool display)
        {
            if (display == false)
            {               
                entity.CitizenshipCertNo = txtCitizenShipCertNo.Text.Trim();
                entity.CitizenshipIssuedAt = txtCitizenIssuedAt.Text.Trim();
                entity.DrivingLicenseNo = txtDriverLicenseNo.Text.Trim();

                entity.PassportNo = txtPassportNo.Text.Trim();
                entity.PassportIssuedAt = txtPassportIssuedAt.Text.Trim();
                entity.PassportIssuedDate = calPassportIssueDate.SelectedDate.ToString();
                entity.PassportValidUpto = calPassportValidUpto.SelectedDate.ToString();

               // entity.Notes = txtNotes.Text.Trim();
             
            }
            else
            {
                txtCitizenShipCertNo.Text = entity.CitizenshipCertNo;
                txtCitizenIssuedAt.Text = entity.CitizenshipIssuedAt;
                txtDriverLicenseNo.Text = entity.DrivingLicenseNo;

                txtPassportNo.Text = entity.PassportNo;
                txtPassportIssuedAt.Text = entity.PassportIssuedAt;

                calPassportIssueDate.SetSelectedDate(entity.PassportIssuedDate, true);
                calPassportValidUpto.SetSelectedDate(entity.PassportValidUpto, true);

                if (entity.UrlPhoto != null)
                {
                    img1.Src = Config.UploadLocation.Replace("~", "../..") + "/" + entity.UrlPhoto;
                    hlImage1.HRef = img1.Src; ;
                }
                if (entity.UrlSignature != null)
                {
                    img2.Src = Config.UploadLocation.Replace("~", "../..") + "/" + entity.UrlSignature;
                    hlImage2.HRef = img2.Src;
                }
                if (entity.UrlThumbPrint != null)
                {
                    //imgThumbPrint.ImageUrl = Config.UploadLocation + "/" + entity.UrlThumbPrint;
                    img3.Src = Config.UploadLocation.Replace("~", "../..") + "/" + entity.UrlThumbPrint;
                    hlImage3.HRef = img3.Src;
                }

               

              // txtNotes.Text = entity.Notes;
            }
        }

        public void DisplayDocuments(int employeeId)
        {
            //HRManager mgr = new HRManager();
            //Literal lit = new Literal();
            //lit.Text = mgr.GetHTMLDocuments(employeeId);
            //pnlDocuments.Controls.Add(lit);


            gvw.DataSource = HRManager.GetDocuments(employeeId);
            gvw.DataBind();

        }

       
    }
}