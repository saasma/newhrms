﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MGPreviousEmployment.ascx.cs" Inherits="Web.UserControls.HR.MGPreviousEmployment" %>


<script type="text/javascript">


        function popupUpdateEmploymentCall(employmentId) {
            if (typeof (EmployeeId) != 'undefined') {
            var ret = popupUpdateEmployment('Id=' + employmentId + "&EId=" + parseInt(EmployeeId));
            if (typeof (ret) != 'undefined') {
                if (ret == 'Reload') {
                    refreshEmploymentList(0);
                }
            }
        }
            return false;
   }

        //call to delete income
        function deleteEmploymentCall(employmentId) {
            if (confirm("Do you want to delete the activities for this employee?")) {
                refreshEmploymentList(employmentId);
            }
            return false;
        }

        function refreshEmploymentList(employmentId, popupWindow) {
            if (typeof (popupWindow) != 'undefined')
                popupWindow.close();

        if (typeof (EmployeeId) != 'undefined')
             {
                Web.PayrollService.GetPreviousEmploymentList(employmentId, parseInt(EmployeeId), refreshEmploymentListCallback);
            }
        }

        function refreshEmploymentListCallback(result) {
            if (result) {
                document.getElementById('<%=employments.ClientID %>').innerHTML
                = result;
            }
        }
</script>    
<fieldset   class="hrTabFieldset">
 <%--   <legend><b>Previous Employment</b></legend>--%>
    
     <div id="employments" runat="server">
        
    </div>
     <asp:LinkButton   CssClass="createbtns"   ID="btnAdd" runat="server"  OnClientClick="return popupUpdateEmploymentCall();"
                Text="Add Employment" />
    
    </fieldset>