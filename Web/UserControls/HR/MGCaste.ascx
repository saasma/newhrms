﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MGCaste.ascx.cs" Inherits="Web.UserControls.HR.MGCaste" %>

<script type="text/javascript">
    function ConfirmLibraryTopicDeletion() {
        var ddllist = document.getElementById('<%= ddlCastes.ClientID %>');
        if (ddllist.selectedIndex == -1) {
            alert('No Caste to delete.');
            return false;
        }
        if (confirm('Do you want to delete the Caste?')) {
            clearUnload()
            return true;
        }
        else {
            return false;
        }
    }

    function DisplayLibraryTopicInTextBox(dropdown_id) {
        var ddllist = document.getElementById(dropdown_id);
        document.getElementById('<%= txtCasteRename.ClientID %>').value = ddllist.options[ddllist.selectedIndex].text;
    }

    //capture window closing event
    window.onunload = closePopup;
    function closePopup() {
        clearUnload();
        // alert(window.opener.parentReloadCallbackFunction)
        if ($.browser.msie == false && typeof (window.opener.parentReloadCallbackFunction1) != 'undefined' && ($.browser.chrome || $.browser.safari)) {
            window.opener.parentReloadCallbackFunction1(window, texts);
        } else {
            if (typeof (texts) != 'undefined')
                window.returnValue = texts;
            window.close();
        }
    }

    function clearUnload() {
        window.onunload = null;
    }
       
</script>

<style type="text/css">
    label.lblingText
    {
        width: 100px;
        padding-right: 10px;
        text-align: left;
        float: left;
    }
    input, .cols, select
    {
        float: left;
    }
    .cols
    {
        margin: 0 0 0 5px !important;
    }
    .popupHeader
    {
        margin: 0px 0 15px 0px;
    }
    .bevel
    {
        margin-left: 20px;
    }
    h2
    {
        font: bold 14px/30px Arial;
        color: #fff;
        padding-left: 30px;
        text-transform: uppercase;
    }
    p
    {
        clear: both;
        margin-bottom: 10px;
        display: block;
        overflow: hidden;
    }
</style>
<div class="popupHeader">
    <h2>
        Manage Caste/Group
    </h2>
</div>
<div class="bevel">
    <div class="fields">
        <p>
            <label class="lblingText">
                New Caste
                <asp:RequiredFieldValidator ValidationGroup="Deposit1" ID="RequiredFieldValidator3"
                    Display="None" runat="server" ErrorMessage="Caste name is required." ControlToValidate="txtNewCaste">*</asp:RequiredFieldValidator></label>
            <asp:TextBox ID="txtNewCaste" runat="server" Width="161px" MaxLength="100" EnableViewState="false"></asp:TextBox>
            <asp:Button ID="btnAddDepsoit" CssClass="smalladd cols" OnClientClick="valGroup='Deposit1';return CheckValidation()"
                runat="server" Text="Add" OnClick="btnAddDepsoit_Click" />
        </p>
        <p>
            <label class="lblingText">
               Update Caste</label>
          <asp:TextBox ID="txtCasteRename" runat="server" Width="159px" Style="margin-right: 5px;"
                MaxLength="100"></asp:TextBox>
                  <asp:RequiredFieldValidator ValidationGroup="Deposit2" ID="RequiredFieldValidator4"
                runat="server" ErrorMessage="Caste is required." ControlToValidate="txtCasteRename"
                Display="None">*</asp:RequiredFieldValidator>
                
            <asp:Button ID="btnRename" OnClientClick="valGroup='Deposit2';return CheckValidation()"
                runat="server" Text="Update" CssClass="smallupdate cols" ValidationGroup="RenameLibraryTopic"
                OnClick="btnRename_Click" />
           </p>
        <p>
            <label class="lblingText">
                 Caste list</label>
                   <asp:DropDownList ID="ddlCastes" runat="server" Width="165px" CssClass="rightMargin"
                DataValueField="CasteId" DataTextField="CasteName" onchange="DisplayLibraryTopicInTextBox(this.id);">
            </asp:DropDownList>
            
                 <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="smallcancel cols"
                OnClientClick="javascript:return ConfirmLibraryTopicDeletion();" OnClick="btnDelete_Click" />
            
        </p>
    </div>
</div>
<input id="btnClose" type="button" visible="false" value="Close" onclick="closePopup()"
    style="display: none;" />
