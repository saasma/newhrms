﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmpJobDesc.ascx.cs"
    Inherits="Web.UserControls.HR.EmpJobDesc" %>
<script type="text/javascript">
    function locationPopupCall() {
        var ret = locationPopup("isPopup=true");
        if (typeof (ret) != 'undefined') {
            ChangeDropDownList('<%= ddlLocation.ClientID %>', ret);
        }
    }
    function programmePopupCall() {
        var ret = programmePopup("isPopup=true");
        if (typeof (ret) != 'undefined') {
            ChangeDropDownList('<%= ddlProgramme.ClientID %>', ret);
        }
    }

    function locationHistoryPopupCall() {
        var ret = locationHistoryPopup("isPopup=true&EIN=" + EmployeeId);
        if (typeof (ret) != 'undefined') {

            $('#<%= ddlLocation.ClientID %>').val(ret);
        }
    }

    function parentReloadLocationProgramCallbackFunction(state, childWindow, data) {
        childWindow.close();
        switch (state) {
            case 'ReloadLocation':
                ChangeDropDownList('<%= ddlLocation.ClientID %>', data);
                break;
            case 'ReloadProgram':
                ChangeDropDownList('<%= ddlProgramme.ClientID %>', data);
                break;

            default:
        }
    }

    function valDate() {

        var start = 'calStart.ClientID';
        var end = 'calEnd.ClientID';

        if (isSecondCalendarGreater(start, end))
            return true;
        else {
            alert("Ending date must be greater than starting date.");
            return false;
        }
    }


    var txtDesc = '#txtDesc.ClientID';
    var txtPercent = '#txtPercent.ClientID';
    var calStart = 'calStart.ClientID';
    var calEnd = 'calEnd.ClientID';

    function added() {
        valGroup = 'Contribution';
        if (CheckValidation() && valDate() && typeof (EmployeeId) != 'undefined') {
            Web.PayrollService.SaveContribution(parseInt(EmployeeId),
                $(txtDesc).val(), $(txtPercent).val(),
                getCalendarSelectedDate(calStart), getCalendarSelectedDate(calEnd), SaveContributionCallback);
        }
    }

    function SaveContributionCallback(result) {
        if (result != "") {
            if (result == "PercentError") {
                alert("Total percent should not exceed 100.");
                return;
            }
            else {
                document.getElementById('contributions.ClientID').innerHTML
                = result;
                $(txtDesc).val('');
                $(txtPercent).val('');
                alert("Contribution saved.");

            }
        }
    }

    function deleteContributionCall(contributionId) {
        if (confirm("Do you want to delete the contribution?") && typeof (EmployeeId) != 'undefined') {
            Web.PayrollService.DeleteEmpContribution(contributionId, parseInt(EmployeeId), deleteContributionCallCallBack);
        }
    }

    function deleteContributionCallCallBack(result) {
        document.getElementById('contributions.ClientID').innerHTML
                = result;
    }
</script>
<style type="text/css">
    .hrinfosinner select
    {
        padding: 0px;
        margin: 0px;
    }
</style>
<div class="employeSectors" style='padding-left: 20px'>
    <h2>
        Job Description</h2>
    <table cellpadding="0" cellspacing="0" width="360px" class="hrinfosinner">
        <tr>
            <td width="150px;" class="labling">
                I No
            </td>
            <td>
                <asp:TextBox Style="width: 148px" ID="txtIDCartNo" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr runat="server" id="rowJobNature">
            <td width="150px">
                Nature of job
            </td>
            <td>
                <asp:TextBox Style="width: 148px" ID="txtJobNature" runat="server"></asp:TextBox>
            </td>
        </tr>
         <tr>
            <td>
                Voucher Group:
              
            </td>
            <td>
                <asp:DropDownList Width="150px" CssClass="clasificaionElement" ID="ddlVoucherGroup"
                    runat="server" DataValueField="VoucherDepartmentID" DataTextField="Name" AppendDataBoundItems="true">
                    <asp:ListItem Value="-1">--Select Group--</asp:ListItem>
                </asp:DropDownList>
              
            </td>
        </tr>
        <tr>
            <td>
                Leave Project:
              
            </td>
            <td>
                <asp:DropDownList Width="150px" CssClass="clasificaionElement" ID="ddlLeaveProject"
                    runat="server" DataValueField="LeaveProjectId" DataTextField="Name" AppendDataBoundItems="true">
                    <asp:ListItem Value="-1">--Select Project--</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator Enabled="false" ID="RequiredFieldValidator1" runat="server"
                    ControlToValidate="ddlLeaveProject" Display="None" ErrorMessage="Leave Project is required."
                    InitialValue="-1" ValidationGroup="AEEmployee"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                Job location
                <%--<span>*</span>--%>
            </td>
            <td>
                <asp:DropDownList Width="150px" CssClass="clasificaionElement" EnableViewState="false"
                    ID="ddlLocation" runat="server" DataValueField="LocationId" DataTextField="Name"
                    AppendDataBoundItems="true">
                    <asp:ListItem Value="-1">--Select location--</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator Enabled="false" ID="valLocation1" runat="server" ControlToValidate="ddlLocation"
                    Display="None" ErrorMessage="Location is required." InitialValue="-1" ValidationGroup="AEEmployee"></asp:RequiredFieldValidator>
                <input onclick="locationPopupCall()" class="browse" id="btnAddLocation" runat="server"
                    type="button" />
                <img alt=""  onclick="locationHistoryPopupCall()" id="locationHistory"
                    runat="server" src="../../images/change.png" style='border: 0px solid white;
                    cursor: pointer; vertical-align: top;' />
            </td>
        </tr>
        <tr>
            <td>
                Programme
            </td>
            <td>
                <asp:DropDownList Width="150px" CssClass="clasificaionElement" EnableViewState="false"
                    ID="ddlProgramme" runat="server" DataValueField="ProgrammeId" DataTextField="Name"
                    AppendDataBoundItems="true">
                    <asp:ListItem Value="-1">--Select programme--</asp:ListItem>
                </asp:DropDownList>
                <input onclick="programmePopupCall()" class="browse" id="btnAddProgramme" type="button"
                    runat="server" />
                <asp:RequiredFieldValidator ID="reqdProgramme" runat="server" ControlToValidate="ddlProgramme"
                    Display="None" ErrorMessage="Please select a Programme." InitialValue="-1" ValidationGroup="AEEmployee"></asp:RequiredFieldValidator>
            </td>
        </tr>
    </table>
</div>
<%--<div class="employeSectors" style="width: 500px">
    <h2>
        Program contribution</h2>
    <table class="hrinfosinner" cellpadding="0" cellspacing="0" width="550px">
        <tr>
            <td width="125px">
                Program name
            </td>
            <td width="155px">
                <asp:TextBox ID="txtDesc" Width="130px" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="valReqdTitle1" runat="server" ControlToValidate="txtDesc"
                    Display="None" ErrorMessage="Description is required." ValidationGroup="Contribution"></asp:RequiredFieldValidator>
            </td>
            <td style="text-align: left" width="200px">
                Contribution Percentage
            </td>
            <td>
                <asp:TextBox Width="50px" ID="txtPercent" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="txtPercent"
                    Display="None" ErrorMessage="Percent is required." ValidationGroup="Contribution"></asp:RequiredFieldValidator>
            
                <asp:RangeValidator runat="server" ErrorMessage="Invalid percent." ValidationGroup="Contribution"
                    Type="Double" ID="RangeValidator1" Display="None" MinimumValue="0" MaximumValue="100"
                    ControlToValidate="txtPercent"></asp:RangeValidator>
            </td>
            <td>
            </td>
        </tr>
    </table>
    <table class="hrinfosinner" cellpadding="0" cellspacing="0" width="550px">
        <tr>
            <td width="75px">
                Start Date
            </td>
            <td width="200px">
                <My:Calendar Id="calStart" runat="server" />
            </td>
            <td width="75px">
                &nbsp;End Date &nbsp;
            </td>
            <td width="200px">
                <My:Calendar Id="calEnd" runat="server" />
            </td>
        </tr>
    </table>
    <table class="hrinfosinner" cellpadding="0" cellspacing="0" width="400px">
        <tr>
            <td>
                <input id="btnAdd" class="addbtns" runat="server" disabled="disabled" onclick="added()"
                    type="button" value="Add" />
            </td>
        </tr>
        <tr>
            <td colspan="5">
                <div id="contributions" runat="server">
                </div>
            </td>
        </tr>
    </table>
</div>--%>
