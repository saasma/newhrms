﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using Utils.Helper;
using Utils.Web;
using System.Text;

namespace Web.UserControls.HR
{
    public partial class MGCaste : System.Web.UI.UserControl
    {
        CommonManager commonMgr = new CommonManager();
        List<HCaste> source = null;

     

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                Initialise();
        }

        void Initialise()
        {
            LoadCaste();
            if (ddlCastes.SelectedItem != null)
                txtCasteRename.Text = ddlCastes.SelectedItem.Text;
            else
                txtCasteRename.Text = "";
        }

        void LoadCaste()
        {
            source = commonMgr.GetAllCastes();
            ddlCastes.DataSource = source;  //payMgr.GetDepositList(SessionManager.CurrentCompanyId);
            ddlCastes.DataBind();
        }

        protected void btnAddDepsoit_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                HCaste entity = new HCaste();
                //entity.CompanyId = SessionManager.CurrentCompanyId;
                entity.CasteName = txtNewCaste.Text.Trim();

                commonMgr.Save(entity);

                txtNewCaste.Text = "";
                Initialise();
                JavascriptHelper.DisplayClientMsg("New Caste added.", Page);
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            if (ddlCastes.SelectedItem != null)
            {
                HCaste entity = new HCaste();
                entity.CasteId = int.Parse(ddlCastes.SelectedValue);

                if (commonMgr.Delete(entity))
                {
                    JavascriptHelper.DisplayClientMsg("Caste deleted.", Page);                    
                }
                else
                {
                    JavascriptHelper.DisplayClientMsg("Caste is in use.", Page);
                }
                Initialise();
            }
        }

        protected void btnRename_Click(object sender, EventArgs e)
        {
            if (Page.IsValid && ddlCastes.SelectedItem != null)
            {
                HCaste entity = new HCaste();
                entity.CasteName = txtCasteRename.Text.Trim();
                entity.CasteId = int.Parse(ddlCastes.SelectedValue);

                commonMgr.Update(entity);
                LoadCaste();
                UIHelper.SetSelectedInDropDown(ddlCastes, entity.CasteId.ToString());
                JavascriptHelper.DisplayClientMsg("Caste updated.", Page);
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (source != null)
            {
                StringBuilder str = new StringBuilder("");
                bool first = true;
                foreach (HCaste obj in source)
                {
                    if (first == false)
                        str.Append(",");
                    str.Append("'" + obj.CasteId + "$$" + obj.CasteName + "'");
                    first = false;
                }
                Page.ClientScript.RegisterArrayDeclaration("texts", str.ToString());
            }
        }
    }
}