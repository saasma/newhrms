﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HRDetailsCtl.ascx.cs"
    Inherits="Web.UserControls.HR.HRDetailsCtl" %>
<style type="text/css">
    .style2
    {
        width: 100%;
    }
    fieldset /*#ctl00_mainContent_ucHRDetails_tabHRDetails */
    {
        margin-top: 10px;
        margin-bottom: 10px;
        border: 0px;
    }
    .createbtns
    {
        margin-top: 15px;
    }
</style>
<script type="text/javascript">
    function popImageUploadCall(type) {
        this.type = type;
        var empId = parseInt(EmployeeId);
        var ret = popImageUpload("type=" + type + "&EmpId=" + empId);
        //process(ret);
    }
    function refreshDocument(returnValue, popupWindow) {
        popupWindow.close();
        __doPostBack('<%= updPanel.ClientID %>', '')
        //process(returnValue);
    }

    function process(ret) {
        if (typeof (ret) != 'undefined') {
            if (type == 1) {
                document.getElementById('<%= img1.ClientID %>').src = ret;
                document.getElementById('<%= hlImage1.ClientID %>').href = ret;
            }
            else if (type == 2) {
                document.getElementById('<%= img2.ClientID %>').src = ret;
                document.getElementById('<%= hlImage2.ClientID %>').href = ret;
            }
            else if (type == 3) {
                document.getElementById('<%= img3.ClientID %>').src = ret;
                document.getElementById('<%= hlImage3.ClientID %>').href = ret;
            }
            else if (type == 4) {
                __doPostBack('<%= updPanel.ClientID %>', '')
            }


        }
    }
   
    //    function confirmDelDoc(docId) {
    //        if (confirm('Do you want to delete the document?')) {
    //            if (typeof (EmployeeId) != 'undefined')
    //                deleteDocument(docId, parseInt(EmployeeId));
    //        }
    //    }
    //    function deleteDocument(docId, employeeId) {
    //        Web.PayrollService.DeleteDocument(docId, employeeId, deleteDocumentCallback);
    //    }
    //    function deleteDocumentCallback(result) {
    //        document.getElementById('pnlDocuments.ClientID').innerHTML = result;
    //    }
</script>
<cc1:TabContainer ID="tabHRDetails" CssClass="yui" Height="350px" runat="server"
    ActiveTabIndex="0">
    <cc1:TabPanel runat="server" ID="sdfdfs">
        <HeaderTemplate>
            Citizenship
        </HeaderTemplate>
        <ContentTemplate>
            <fieldset>
                <table class="hr" style='margin-top: 20px; margin-left: 10px;' cellspacing="0" cellpadding="0"
                    width="400px">
                    <tr>
                        <td width="120px">
                            Citizenship cert No
                        </td>
                        <td>
                            <asp:TextBox ID="txtCitizenShipCertNo" Width="250px" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td width="120px">
                            Issued at
                        </td>
                        <td>
                            <asp:TextBox ID="txtCitizenIssuedAt" Width="250px" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td width="120px">
                            Driving licence No
                        </td>
                        <td>
                            <asp:TextBox ID="txtDriverLicenseNo" Width="250px" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </fieldset>
        </ContentTemplate>
    </cc1:TabPanel>
    <cc1:TabPanel runat="server" ID="TabPanel1">
        <HeaderTemplate>
            Passport
        </HeaderTemplate>
        <ContentTemplate>
            <fieldset>
                <table style='margin-top: 20px; margin-left: 10px;' class="hr" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="120px">
                            Passport No
                        </td>
                        <td>
                            <asp:TextBox ID="txtPassportNo" runat="server" Width="200px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td width="120px">
                            Issued at
                        </td>
                        <td>
                            <asp:TextBox ID="txtPassportIssuedAt" runat="server" Width="200px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td width="120px">
                            Issue date
                        </td>
                        <td>
                            <My:Calendar runat="server" Id="calPassportIssueDate" width="200px" />
                        </td>
                    </tr>
                    <tr>
                        <td width="120px">
                            Valid upto
                        </td>
                        <td>
                            <My:Calendar runat="server" Id="calPassportValidUpto" width="200px" />
                        </td>
                    </tr>
                </table>
            </fieldset>
        </ContentTemplate>
    </cc1:TabPanel>
    <cc1:TabPanel runat="server" ID="TabPanel2">
        <HeaderTemplate>
            Images
        </HeaderTemplate>
        <ContentTemplate>
            <fieldset>
                <table class="style2" style='margin-top: 20px;'>
                    <tr>
                        <td class="centers">
                            <span class="pad">Photo </span><a target="_blank" runat="server" id="hlImage1" style="display: block">
                                <img runat="server" id="img1" src="../../images/sample.jpg" />
                            </a>
                            <input id="btnAddPhoto" type="button" runat="server" class="addbtns" value="Add"
                                style="margin-top: 8px;" onclick="popImageUploadCall(1)" />
                        </td>
                        <td class="centers">
                            <span class="pad">Signature</span> <a target="_blank" runat="server" id="hlImage2"
                                style="display: block">
                                <img runat="server" id="img2" src="../../images/sample.jpg" />
                            </a>
                            <input id="btnSignature" type="button" runat="server" value="Add" style="margin-top: 8px;"
                                class="addbtns" onclick="popImageUploadCall(2)" />
                        </td>
                        <td class="centers">
                            <span class="pad">Thumb print</span> <a target="_blank" runat="server" id="hlImage3"
                                style="display: block">
                                <img runat="server" id="img3" src="../../images/sample.jpg" />
                            </a>
                            <input id="btnThumbPrint" runat="server" type="button" value="Add" style="margin-top: 8px;"
                                class="addbtns" onclick="popImageUploadCall(3)" />
                        </td>
                    </tr>
                </table>
            </fieldset>
        </ContentTemplate>
    </cc1:TabPanel>
    <cc1:TabPanel runat="server" ID="TabPanel411">
        <HeaderTemplate>
            Documents
        </HeaderTemplate>
        <ContentTemplate>
            <fieldset style='margin-top: 20px; margin-left: 10px;'>
                <%-- <asp:Panel ID="pnlDocuments" runat="server" Width="100" Height="100" ScrollBars="Vertical">
                                
                            </asp:Panel>--%>
                <%--  <div id="pnlDocuments" runat="server" style='overflow: x-hidden'>
                </div>--%>
                <asp:UpdatePanel ID="updPanel" runat="server">
                    <ContentTemplate>
                        <asp:GridView DataKeyNames="DocumentId" PagerStyle-HorizontalAlign="Center" PagerStyle-CssClass="defaultPagingBar"
                            OnPageIndexChanging="gvw_PageIndexChanging" ID="gvw" AllowPaging="true" runat="server"
                            AutoGenerateColumns="False" CellPadding="4" GridLines="None" PageSize="10">
                            <Columns>
                                <%-- <asp:BoundField DataField="Name" ItemStyle-Width="300px" HeaderStyle-HorizontalAlign="Left"
                                    HeaderText="Name" />--%>
                                <asp:TemplateField HeaderText="Name" ItemStyle-Width="300px" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:HyperLink runat="server" Text='<%# Eval("Description") %>' NavigateUrl='<%# "~/DocumentHandler.ashx?id=" + System.IO.Path.GetFileNameWithoutExtension(Eval("Url").ToString()) %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="Size" ItemStyle-Width="50px" HeaderStyle-HorizontalAlign="Left"
                                    HeaderText="Size" />
                                <asp:TemplateField HeaderText="Delete" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="30px">
                                    <ItemTemplate>
                                        <asp:ImageButton OnCommand="btnDelete_Click" CommandArgument='<%# Eval("DocumentId") %>'
                                            CausesValidation="False" ID="btnDelete" OnClientClick="return confirm('Do you want to delete the document?')"
                                            runat="server" CommandName="Delete" ImageUrl="~/images/delet.png" />
                                    </ItemTemplate>
                                    <HeaderStyle Width="30px"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                </asp:TemplateField>
                            </Columns>
                            <RowStyle CssClass="odd" />
                            <AlternatingRowStyle CssClass="even" />
                            <SelectedRowStyle CssClass="selected" />
                            <PagerStyle HorizontalAlign="Center" CssClass="defaultPagingBar"></PagerStyle>
                            <EmptyDataTemplate>
                                <b>No document uploaded. </b>
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <input id="btnAddDocument" type="button" runat="server" style='margin-top: 10px'
                    value="Add" class="addbtns" onclick="popImageUploadCall(4)" />
            </fieldset>
        </ContentTemplate>
    </cc1:TabPanel>
    <%--<cc1:TabPanel runat="server" ID="TabPanel3">
        <HeaderTemplate>
            Notes
        </HeaderTemplate>
        <ContentTemplate>
            <fieldset style='margin-top: 20px;'>
                <asp:TextBox ID="txtNotes" TextMode="MultiLine" Width="100%" Height="100px" runat="server"></asp:TextBox>
            </fieldset>
        </ContentTemplate>
    </cc1:TabPanel>--%>
</cc1:TabContainer>
