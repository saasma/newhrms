﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MGActivities.ascx.cs" Inherits="Web.UserControls.HR.MGActivities" %>


<script type="text/javascript">


        function popupUpdateActivityCall(activityId) {
         if (typeof (EmployeeId) != 'undefined')
             {

            var ret = popupUpdateActivity('Id=' + activityId + "&EId=" + parseInt(EmployeeId));
            if (typeof (ret) != 'undefined') {
                if (ret == 'Reload') {
                    refreshActivityList(0);
                }
            }
            
            }
            return false;
        }

        //call to delete income
        function deleteActivityCall(activityId) {
            if (confirm("Do you want to delete the activities for this employee?")) {
                refreshAcitivityList(activityId);
            }
            return false;
        }

        function refreshActivityList(activityId, popupWindow) {
            if (typeof (popupWindow) != 'undefined')
                popupWindow.close();


        if (typeof (EmployeeId) != 'undefined')
             {
            Web.PayrollService.GetActivityList(activityId, parseInt(EmployeeId), refreshActivityListCallback);
        }
      }

        function refreshActivityListCallback(result) {
            if (result) {
                document.getElementById('<%=activities.ClientID %>').innerHTML
                = result;
            }
        }
</script>    
<fieldset   class="hrTabFieldset">
   <%-- <legend><b>Extra Curricular</b></legend>--%>
    
     <div id="activities" runat="server">
        
    </div>
     <asp:LinkButton ID="btnAdd"     CssClass="createbtns" runat="server"  OnClientClick="return popupUpdateActivityCall();"
                Text="Add Activity" />
    
    </fieldset>