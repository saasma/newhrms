﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ManageAddOn.ascx.cs"
    Inherits="Web.UserControls.ManageAddOn" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<style type="text/css">
    table.alignttable tr td
    {
        vertical-align: top;
        padding-bottom: 10px;
    }
    table.alignttable tr td input, table.alignttable tr td select
    {
        float: left;
    }
</style>
<script type="text/javascript">



    //capture window closing event
    window.onunload = closePopup;
    function closePopup() {
        clearUnload();



        // alert(window.opener.parentReloadCallbackFunction)
        //if ($.browser.msie == false && typeof (window.opener.parentReloadCallbackFunction) != 'undefined' && ($.browser.chrome || $.browser.safari)) {
        window.opener.parentReloadCallbackFunction("ReloadDesignation", window, texts);
        //        } else {
        //            if (typeof (texts) != 'undefined')
        //                window.returnValue = texts;
        //            window.close();
        //        }

    }

    function clearUnload() {
        window.onunload = null;
    }

    function handleDelete() {
        if (confirm('Do you want to delete Designation?')) {
            clearUnload();
            return true;
        }
        else
            return false;
    }

</script>
<div align="left">
    <div class="marginal">
        <uc2:MsgCtl ID="divMsgCtl" Width='600px' EnableViewState="false" Hide="true" runat="server" />
        <uc2:WarningCtl ID="divWarningMsg" Width='600px' EnableViewState="false" Hide="true"
            runat="server" />
        <asp:Panel ID="details" runat="server" DefaultButton="btnSave" class="bevel" Style="margin-top: 20px;"
            Visible="false">
            <div class="fields paddpop" style="padding: 10px 0pt 20px 10px ! important;">
                <h2 class="popstitle">
                    Add-On for : {0}</h2>
                <table cellpadding="4px">
                    <tr>
                        <td class="fieldHeader">
                            <My:Label ID="Label2" Text="Add-On Name" runat="server" ShowAstrick="true" />
                        </td>
                        <td>
                            <asp:TextBox ID="txtName" runat="server" Width="180px" />
                            <asp:RequiredFieldValidator ID="valReqdName" runat="server" ControlToValidate="txtName"
                                Display="None" ErrorMessage="Name is required." ValidationGroup="AEBranch"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="fieldHeader">
                            <My:Label ID="Label1" Text="Show After" runat="server" ShowAstrick="false" />
                        </td>
                        <td>
                            <asp:DropDownList Width="140px" AppendDataBoundItems="true" DataTextField="Name"
                                DataValueField="PayrollPeriodId" ID="ddlShowAfterPeriod" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="fieldHeader">
                            <My:Label ID="Label3" Text="Order" runat="server" ShowAstrick="false" />
                        </td>
                        <td>
                            <asp:TextBox autocomplete="off" ID="txtOrder" runat="server" Width="180px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <asp:Button ID="btnSave" CssClass="update" OnClientClick="valGroup='AEBranch';return CheckValidation()"
                                runat="server" Text="Save" ValidationGroup="AEBranch" OnClick="btnSave_Click" />
                            <asp:Button ID="btnCancel" OnClientClick="clearUnload()" CssClass="cancel" runat="server"
                                Text="Cancel" OnClick="btnCancel_Click" />
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
        <%--</fieldset>--%>
    </div>
</div>
