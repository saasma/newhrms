﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using System.Drawing;
using System.Text;
using Utils.Helper;
using Image = System.Web.UI.WebControls.Image;
using Utils.Calendar;


namespace Web.UserControls
{
    // Here is the code for building the template field
    public class CalcGridViewTemplate : ITemplate
    {
        private DataControlRowType templateType;
        private CalcGetHeaderListResult header;
        private CalculationColumnType columnType;
        private Calculation calculation;
        private int basicIncomeId = 0;

         private bool isHeaderMissingError;
         private int missingType; 
        private int missingSourceId;

        private bool isDeemedNegativeWarningSet = false;

        public CalcGridViewTemplate(DataControlRowType type, CalcGetHeaderListResult header, Calculation calculation, int basicIncomeId)
        {
            templateType = type;
            this.header = header;
            this.basicIncomeId = basicIncomeId;
            this.columnType = (CalculationColumnType)header.Type;
            this.calculation = calculation;

            isHeaderMissingError = false;
            missingType = 0;
            missingSourceId = 0;
        }

        private  void SetHeaderColor(Control container)
        {
            string backColor;
           
            if (CalculationValue.IsColumTypeIncome(columnType))
                backColor = CalculationManager.incomeColor;
            else if (CalculationValue.IsColumTypeDeduction(columnType))
                backColor = CalculationManager.deductionColor;
            else if(columnType==CalculationColumnType.IncomeGross)
                backColor = CalculationManager.incomeColor;
            else if( columnType==CalculationColumnType.DeductionTotal)
                backColor = CalculationManager.deductionColor;
            else
            {
                backColor = CalculationManager.netColor;
            }

            ((System.Web.UI.WebControls.WebControl)(container)).BackColor =
                           System.Drawing.ColorTranslator.FromHtml(backColor);
        }

        /// <summary>
        /// Generate dynamic control id,appenid i-income,d-deduction so as to prevent duplicate id
        /// </summary>
        /// <param name="header"></param>
        /// <returns></returns>
        public string GetIDPostfix(CalcGetHeaderListResult header)
        {
            if (CalculationValue.IsColumTypeIncome(header.ColumnType))
                return "i" + header.Type + header.SourceId;
            else
                return "d" + header.Type + header.SourceId;
        }

        public void InstantiateIn(System.Web.UI.Control container)
        {
            switch (templateType)
            {
                case DataControlRowType.Header:

                    Label lbl = new Label();

                    lbl.Width = new Unit(Calculation.ColumnWidth - 3);
                    lbl.Text = header.HeaderName;
                    lbl.ToolTip = header.HeaderName;


                    lbl.ID = "lbl" + GetIDPostfix(header);// header.Type + header.SourceId;
                    //values will be used while saving
                    lbl.Attributes.Add("SourceId", header.SourceId.ToString());
                    lbl.Attributes.Add("Type", ((int)columnType).ToString());
                    lbl.Attributes.Add("HeaderName", header.HeaderName);

                    //Set header color
                    SetHeaderColor(container);




                    container.Controls.Add(lbl);


                    ((System.Web.UI.WebControls.WebControl)(container)).Width = new Unit(25);


                    break;
                case DataControlRowType.DataRow:

                    TextBox txt = new TextBox();

                    

                    txt.ID = "txt" + GetIDPostfix(header);// header.Type + header.SourceId;
                    txt.Style.Add("text-align", "right");
                    // key movement event handler
                    //txt.Attributes.Add("onKeyDown", "return processKey(event,this);");
                    txt.Width = new Unit(Calculation.ColumnWidth);
                    txt.DataBinding += new EventHandler(Text_IncomeBinding);
                    txt.AutoCompleteType = AutoCompleteType.None;
                    txt.CssClass = "calculationInput";


                    txt.ReadOnly = true;
                    txt.CssClass += " calculationInputDisabled";


                    container.Controls.Add(txt);



                    break;
                default:
                    break;
            }
        }

        public void Text_IncomeBinding(object sender, EventArgs e)
        {
            GridViewRow row = ((GridViewRow) (((Control) (sender)).NamingContainer));
            //HiddenField hfList = (HiddenField) row.FindControl("hfList");
            CalcGetCalculationListResult dataSource = (CalcGetCalculationListResult) row.DataItem;
            TextBox txt = (TextBox) sender;
            decimal? amount = null;
            
            //change textbox background color checking the row index to make it white as default is gray
            if (row.RowIndex % 2 == 1)
            {
                txt.Style["background-color"] = "#FFFFFF!important";
            }

            //GridViewRow row = txt.Parent.Parent as GridViewRow;
            row.ToolTip = dataSource.EmployeeId + " : " + dataSource.Name;
            //add column type & source id to be used in client side
            txt.Attributes.Add("columnType", header.Type.ToString());
            txt.Attributes.Add("sourceId", header.SourceId.ToString());
            txt.Attributes.Add("empId", dataSource.EmployeeId.Value.ToString());


            if (dataSource != null && dataSource.IsAttendanceComplete != null && dataSource.IsAttendanceComplete == 0)
            {
                if (this.calculation != null)
                    calculation.isAttendanceNotSaved = true;
            }

            // Get Amount for the cell
            amount = dataSource.GetCellValue(header.Type.Value, header.SourceId.Value, SessionManager.DecimalPlaces, 
                this.calculation == null ? null :this.calculation.headerList,ref isHeaderMissingError,ref missingType,ref missingSourceId);

            if (isHeaderMissingError && this.calculation != null)
            {
                this.calculation.isHeaderMissingError = isHeaderMissingError;
                this.calculation.missingType = missingType;
                this.calculation.missingSourceId = missingSourceId;
                this.calculation.missingEIN = dataSource.EmployeeId == null ? 0 : dataSource.EmployeeId.Value;
            }


            // Check if column value is valid
            CalculationValue calculationValue = dataSource.GetCalculationValue(header.Type.Value, header.SourceId.Value);
            if (calculationValue != null && calculationValue.IsError)
            {

                MarkTextboxInvalid(txt, calculationValue.ErrorMessage);

                calculation.ErrorList += "<li><a href='#" + txt.ClientID + "'>" + dataSource.Name + " </a> : " + dataSource.EmployeeId + " : " + header.HeaderName + " - " + calculationValue.ErrorMessage + "</li>";


                //calculation.ErrorList += "<li><a href='#" + txt.ClientID + "'>" + dataSource.Name + " <a/> : " + dataSource.EmployeeId + " : " + header.HeaderName + " - " + txt.ToolTip + "</li>";


                if(amount != null)
                    txt.Text = Utils.Helper.Util.FormatAmountWithProperDecimal(amount.Value,
                                                                              SessionManager.DecimalPlaces);
                if (this.calculation != null)
                    calculation.isValidAllValid = false;

                if (calculationValue.ErrorCode == CalculationErrorCode.DefaultValueNotSet)
                    txt.Text = "";
                
                return;
            }

            // For adjustment type if has tooltip set it
            if (this.calculation != null && calculation.ViewSalaryType == SalaryType.Adjustment && calculationValue != null && !string.IsNullOrEmpty(calculationValue.ToolTip))
            {
                txt.ToolTip = calculationValue.ToolTip;
            }
			
			// Get the Tooltip help for the cell if of adjustment type
            if (
                header.ColumnType ==CalculationColumnType.Income ||
                //header.ColumnType == CalculationColumnType.IncomeInsurance ||
                header.ColumnType == CalculationColumnType.DeductionInsurance)
            {
                //Add SpecialEvent id in the Hidden field to be passed while saving
                txt.Attributes.Add("CalculationIncluded", dataSource.GetList(header.Type.Value, header.SourceId.Value));
                //hfList.Value = dataSource.GetList(header.Type.Value, header.SourceId.Value);
            }
			
            // If amount is negative & is not Adjustment type then and always allow negative in deemed income
            if (amount != null && amount <= -1 && this.calculation != null 
                && calculation.ViewSalaryType != SalaryType.Adjustment )
            {                
                // For Net salary like being -ve, but allowed if retiring/resining as can be -ve due to loan/adv
                if (amount < -1)
                {
                    txt.Text = Utils.Helper.Util.FormatAmountWithProperDecimal(amount.Value,
                                                                               SessionManager.DecimalPlaces);
                    if (dataSource.IsRetiredOrResigned.Value == 1)
                        txt.ToolTip = Resources.Messages.CalcNegativeAmountDueToLoanMsg;
                    else
                        txt.ToolTip = Resources.Messages.CalcNegativeAmountMsg;

                }
                //For NULL/IsValid = false case only msg is enough
                else if (amount == -1)
                {
                    txt.ToolTip = Resources.Messages.CalcNullAmountMsg;
                }

                MarkTextboxInvalid(txt);


                //if (header.ColumnType != CalculationColumnType.DeemedIncome)
                {
                    
                }

                // if sst/tds is negative then show in warning as it will be negative allowed if AllowNegativeTaxInSalary setting 
                // is set true only so will never need to show in ErrorList

                if ((header.ColumnType == CalculationColumnType.TDS
                    || header.ColumnType == CalculationColumnType.SST
                    || header.ColumnType == CalculationColumnType.DeductionTotal)
                    && CommonManager.Setting.AllowNegativeTaxInSalary != null && CommonManager.Setting.AllowNegativeTaxInSalary.Value)
                {
                    if (!string.IsNullOrEmpty(txt.ToolTip))
                        calculation.WarningList += "<li>" + dataSource.Name + " : " + dataSource.EmployeeId + " : " + header.HeaderName + " - " + txt.ToolTip + "</li>";
                }
                else if (header.ColumnType == CalculationColumnType.DeemedIncome)
                {
                    if (isDeemedNegativeWarningSet == false)
                    {
                        if (!string.IsNullOrEmpty(txt.ToolTip))
                            calculation.WarningList += "<li>" + header.HeaderName + " has negative values for some employee(s), please check again.</li>";
                        isDeemedNegativeWarningSet = true;
                    }
                }
                // For these all income or net salary can be negative but show warning if exists & allow to save
                else if (CommonManager.CompanySetting.WhichCompany == WhichCompany.HPL ||
                    CommonManager.CompanySetting.WhichCompany == WhichCompany.AceTravels ||
                    CommonManager.CompanySetting.WhichCompany==WhichCompany.Mega ||
                    CommonManager.CompanySetting.WhichCompany == WhichCompany.Yeti ||
                    CommonManager.CompanySetting.PayrollAllowNegativeSalarySave
                    )
                {
                    //if (this.calculation != null)
                        //calculation.isNegativeWarningSalaryExists = true;
                    if (!string.IsNullOrEmpty(txt.ToolTip))
                        calculation.WarningList += "<li>" + dataSource.Name + " : " + dataSource.EmployeeId + " : " + header.HeaderName + " - " + txt.ToolTip + "</li>";
                }
                else
                {
                    if (!string.IsNullOrEmpty(txt.ToolTip))
                    {
                        calculation.ErrorList += "<li><a href='#" + txt.ClientID + "'>" + dataSource.Name + " </a> : " + dataSource.EmployeeId + " : " + header.HeaderName + " - " + txt.ToolTip + "</li>";

                    }
                    // if retired or if tds column then neglect
                    if (this.calculation != null && dataSource.IsRetiredOrResigned.Value == 1
                        || header.ColumnType == CalculationColumnType.TDS
                         || header.ColumnType == CalculationColumnType.SST
                        || header.ColumnType==CalculationColumnType.DeductionTotal
                        //|| header.ColumnType == CalculationColumnType.IncomeResterospect
                        //|| (header.ColumnType == CalculationColumnType.NetSalary && dataSource.IsRetrospectIncrementNegative(this.calculation.headerList))
                        //|| (header.ColumnType == CalculationColumnType.IncomeGross && dataSource.IsRetrospectIncrementNegative(this.calculation.headerList))
                        || (header.ColumnType == CalculationColumnType.Income && header.SourceId != basicIncomeId) // Allow for income other than Basic
                        )
                    {
                        if (this.calculation != null)
                            calculation.isNegativeWarningSalaryExists = true;
                    }
                    //if retired & IsValid=false for income/deduction then mark with red
                    else if (dataSource.IsRetiredOrResigned.Value == 1 && (header.ColumnType == CalculationColumnType.Income || header.ColumnType == CalculationColumnType.Deduction))
                    {
                        if (this.calculation != null)
                            calculation.isValidAllValid = false;
                    }
                    else //if (dataSource.IsRetiredOrResigned.Value == 0)
                    {
                        if (this.calculation != null)
                            calculation.isValidAllValid = false;
                    }
                }
                return;

            }

            

            txt.Text = amount == null
                           ? Resources.Messages.EmptySalaryAmountText
                           : Utils.Helper.Util.FormatAmountWithProperDecimal(amount.Value, SessionManager.DecimalPlaces);

            // if no amount value in case if this income is not for the employee or already saved
            // if already final saved
            // if not edit mode, if row is not in edit mode
            if (amount == null || dataSource.IsFinalSaved == 1 || (this.calculation != null && dataSource.CalculationId != 0 && calculation.editMode == false))
            {
                CalculationManager.MakeReadonly(txt);
            }
            //    // if not edit mode, if row is not in edit mode
            //else if (dataSource.CalculationId != 0 && calculation.editMode == false)
            //    //&& calculation.editingId != dataSource.EmployeeId )
            //{
            //    MakeReadonly(txt);
            //}

            // set amount in javascript attribute also to reverse if invalid placed
            //txt.Attributes.Add("amount", txt.Text);

        }

        public static void MarkTextboxInvalid(TextBox txt)
        {
            txt.Style["border"] = "1px solid red!important";
            //txt.BorderColor = Color.Red;
            //txt.BorderWidth = new Unit(1);
        }
        public static void MarkTextboxInvalid(TextBox txt,string errorMessage)
        {
            txt.Style["border"] = "1px solid red!important";
            txt.ToolTip = errorMessage;
            //txt.BorderColor = Color.Red;
            //txt.BorderWidth = new Unit(1);
        }
    }


}
