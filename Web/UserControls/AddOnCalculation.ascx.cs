﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Base;
using BLL.Manager;
using DAL;
using System.Drawing;
using System.Text;
using Utils.Helper;
using Image = System.Web.UI.WebControls.Image;
using System.Web.Services;
using System.Data;
using Web.Master;
using Web.CP.Report.Templates.Pay;
using DevExpress.XtraReports.UI;
using Web.CP.Report;
using DevExpress.XtraPrinting;
using System.IO;
using BLL.BO;

namespace Web.UserControls
{

    public partial class AddOnCalculation : BaseUserControl
    {

        public void btnGenerate_Click(object sender, EventArgs e)
        {
            int payrollPeriodId = 0, payrollPeriodType = 0;

            string payAllTax = hiddenChecked.Value.Trim();
            if (payAllTax == null)
                payAllTax = "";


            bool? payAll = null;


            if (!string.IsNullOrEmpty(payAllTax))
                payAll = payAllTax.ToString().ToLower().Trim() == "true" ? true : false;

            SplitPeriod(ddlPayrollPeriods.SelectedValue, ref payrollPeriodId, ref payrollPeriodType);

            CalculationManager.GenerateAddOnTax(
                payrollPeriodId, payAll);

            msgCtl.InnerHtml = "Tax generated.";
            msgCtl.Hide = false;




        }

        public SalaryType ViewSalaryType
        {
            get
            {
                if (ViewState["SalType"] == null)
                    return SalaryType.Combined;
                return (SalaryType)int.Parse(ViewState["SalType"].ToString());
            }
            set
            {
                ViewState["SalType"] = (int)value;
            }
        }

        public bool IsHoldPaymentListing
        {
            get
            {
                if (Request.QueryString["HoldPayment"] != null)
                    return true;
                return false;
            }
        }
        public int HoldPaymentPayrollPeriodId
        {
            get
            {
                if (Request.QueryString["ID"] != null)
                    return int.Parse(Request.QueryString["ID"]);
                return 0;
            }
        }

      

        //in pixel unit
        public const int ColumnWidth = 69;

        CalculationManager calcMgr = new CalculationManager();
        CommonManager commonMgr = new CommonManager();
        public CCalculation calculation = null;
        //public int decimalPlaces = 0;
        // To track if any cell value is invalid, set from "CalcGridViewTemplate
        public bool isValidAllValid = true;
        // To track if the page is in edit mode or not, currently edit mode button is hidden so no functionality, as to effect for tax calc
        public bool editMode = false;

        // hold all the incomes types to be displayed in Calculation list
        List<CalcGetHeaderListResult> headers = new List<CalcGetHeaderListResult>();

        public Dictionary<string, string> headerList = new Dictionary<string, string>();
        private int payrollPeriodId = 0;
        private int payrollPeriodType = 0;

        public bool isIncomeAdjChecked = false;
        public bool isDeductionAdjChecked = false;

        private void RestoreAdjustmentCheckboxState()
        {
            foreach (string key in Request.Form.AllKeys)
            {
                if (key.Contains(CalculationManager.idIncomeAdjCheckbox) && Request.Form[key].ToLower()=="on")
                {
                    isIncomeAdjChecked = true;
                }
                if (key.Contains(CalculationManager.idDeductionAdjCheckbox) && Request.Form[key].ToLower() == "on")
                {
                    isDeductionAdjChecked = true;
                }
            }
        }

      

        // ViewState off for this page
        protected void Page_Load(object sender, EventArgs e)
        {

            if (this.Visible)
            {



                RestoreAdjustmentCheckboxState();

                if (!IsPostBack)
                {
                    //CalculationManager.ClearVariableAndIncomeAjdustmentOnFirstLoad();
                    ddlDepartments.DataSource = new DepartmentManager().GetDepartmentsByCompany(SessionManager.CurrentCompanyId);
                    ddlDepartments.DataBind();
                }

                bool loadColumns = false;
                //if dropdown changes
                if (Request.Form["__EventTarget"] != null &&
                    (Request.Form["__EventTarget"].Equals(ddlPayrollPeriods.ClientID.Replace("_", "$"))
                    || Request.Form["__EventTarget"].Equals(txtEmpSearch.ClientID.Replace("_", "$"))
                    || Request.Form["__EventTarget"].Equals(ddlDepartments.ClientID.Replace("_", "$"))
                    )
                    )
                {
                    SplitPeriod(Request.Form[ddlPayrollPeriods.ClientID.Replace("_", "$")], ref payrollPeriodId, ref payrollPeriodType);
                    //this.payrollPeriodId = int.Parse(Request.Form[ddlPayrollPeriods.ClientID.Replace("_", "$")]);
                    //Initialise();
                    _tempCurrentPage = 1;
                    loadColumns = true;
                }


                if (!Initialise())
                    return;
                ;
                //AddColumns();
                //RegisterLegendColors();

                if (!IsPostBack)
                {
                    _tempCurrentPage = 1;
                    loadColumns = true;

                }

                //

                //if first time load & submitted by Delete/Save button then need to reload as ViewState is disabled
                // and to prevent always double loading when page index is changed

                if (!IsPostBack || (Request.Form["__EVENTTARGET"] != null &&
                    (!Request.Form["__EVENTTARGET"].ToLower().Contains("pagingctl"))
                    ///  ||  !Request.Form["__EVENTTARGET"].ToLower().Contains("txtEmpSearch")
                       )
                    // )

                    )
                //(Request.Form[btnSave.ClientID.Replace("_", "$")] != null
                //|| Request.Form[btnDelete.ClientID.Replace("_", "$")] != null
                //|| IsExportButtonSubmit()
                //|| IsPayrollPeriodDropDownChanged()
                //|| Request.Form[chkHasRetiredOrResigned.ClientID.Replace("_","$")] != null))
                {
                    LoadCalculation(false, loadColumns);
                }

                if (Request.Form["__EVENTTARGET"] != null && Request.Form["__EVENTTARGET"].ToLower() == "generate")
                {
                    btnGenerate_Click(null, null);
                    LoadCalculation(false, loadColumns);
                }

                RegisterScripts();

                JavascriptHelper.AttachPopUpCode(Page, "commentPopup", "PopupHtml.htm", 300, 300);



                JavascriptHelper.AttachNonDialogPopUpCode(Page, "importPopup", "../ExcelWindow/PartialTaxExcel.aspx", 450, 500);
                JavascriptHelper.AttachNonDialogPopUpCode(Page, "change", "AddOnIncomeSelection.aspx", 1000, 600);
            }
        }


     
        void SplitPeriod(string value,ref int payrollPeriodId,ref int periodType)
        {
            string[] values = value.Split(new char[] { ':'});
            payrollPeriodId = int.Parse(values[0]);
            periodType = int.Parse(values[1]);
        }

        void RegisterScripts()
        {
            StringBuilder str = new StringBuilder();
            str.AppendFormat("var decimalPlaces = {0};", SessionManager.DecimalPlaces);
            // Generating txt names like txt-4-4, txt-10-10 for the total textboxes,
            //depends upon the naming generated in "CalcGridViewTemplate"
            str.AppendFormat("var txtIncomeSumTxt = 'txtd{0}';",
                             (((int)CalculationColumnType.IncomeGross)) +
                             ((int)CalculationColumnType.IncomeGross).ToString());
            str.AppendFormat("var txtDeductionSumTxt = 'txtd{0}';",
                            (((int)CalculationColumnType.DeductionTotal)) +
                            ((int)CalculationColumnType.DeductionTotal).ToString());

            str.AppendFormat("var txtNetTxt = 'txtd{0}';",
                            (((int)CalculationColumnType.NetSalary)) +
                            ((int)CalculationColumnType.NetSalary).ToString());

            
            str.AppendFormat("var msgCannotUnchecked = '{0}';", Resources.Messages.CalcCannotUncheckedForAdjustment);

            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "rounding",
                                                        str.ToString()
                                                        , true);
        }

        bool Initialise()
        {

          

            //ListItem firstItem = ddlPayrollPeriods.Items[0];
            ddlPayrollPeriods.Items.Clear();
           // ddlShowAfterPeriod.Items.Clear();
            //ddlPayrollPeriods.Items.Insert(0,firstItem);
            List<PayrollPeriodBO> payrollPeriodList = CommonManager.GetCurrentYear(SessionManager.CurrentCompanyId);

            //ddlPayrollPeriods.DataSource = payrollPeriodList;
            //ddlPayrollPeriods.DataBind();
            ddlPayrollPeriods.Items.AddRange(CommonManager.AppendMiddleTaxPaidList(payrollPeriodList, false, false).ToArray());


            if( ddlShowAfterPeriod.Items.Count==0)
                ddlShowAfterPeriod.Items.AddRange(CommonManager.AppendMiddleTaxPaidList(payrollPeriodList, false, false).ToArray());
            //CommonManager.AppendMiddleTaxPaidList(payrollPeriodList);
            //ddlPayrollPeriods.DataSource = payrollPeriodList;
            //ddlPayrollPeriods.DataBind();

            ddlPayrollPeriods.ClearSelection();

            if (ddlPayrollPeriods.Items.Count <= 0)
            {
                divErrorMsg.InnerHtml = Resources.Messages.CalcNoPayrollPeriodDefined;
                HideAll(false);

                return false;
            }


            if (this.payrollPeriodId == 0)
            {
                ListItem item = null;
                for (int i = ddlPayrollPeriods.Items.Count - 1; i >= 0; i--)
                {
                    if (ddlPayrollPeriods.Items[i].Value.Contains(":1"))
                    {
                        item = ddlPayrollPeriods.Items[i];
                        break;
                    }
                }

                if (IsHoldPaymentListing == false)
                {
                    if (item != null)
                    {
                        SplitPeriod(item.Value, ref payrollPeriodId, ref payrollPeriodType);
                        //payrollPeriodId = int.Parse(item.Value);
                        item.Selected = true;
                    }
                }
                else // hold payment period selection from QueryString
                {
                    string value = HoldPaymentPayrollPeriodId + ":1";
                    payrollPeriodId = HoldPaymentPayrollPeriodId;
                    payrollPeriodType = 1;
                    ddlPayrollPeriods.SelectedValue = value;
                }
            }
            else
            {
                ListItem item = ddlPayrollPeriods.Items.FindByValue(payrollPeriodId.ToString() + ":" + payrollPeriodType);
                if (item != null)
                {
                    SplitPeriod(item.Value, ref payrollPeriodId, ref payrollPeriodType);
                    //payrollPeriodId = int.Parse(item.Value);
                    item.Selected = true;
                }
                
            }

           // ddlPayrollPeriods.Enabled = false;

            //PayrollPeriod lastPayrollPeriod = CommonManager.GetLastPayrollPeriod();
            //// no payroll period defined
            //if (lastPayrollPeriod == null)
            

            //lbl.Text = lastPayrollPeriod.Name;
            //this.payrollPeriodId = lastPayrollPeriod.PayrollPeriodId;
            calculation = CalculationManager.IsCalculationSaved(this.GetPayrollPeriodId());


           




           

            return true;
        }

        public void txtEmpSearch_TextChanged(object sender, EventArgs e)
        {
            LoadCalculation(false, false);
        }

        public void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadCalculation(false, false);
        }
        bool IsPayrollPeriodDropDownChanged()
        {
            return Request.Form["__EventTarget"] != null && Request.Form["__EventTarget"].Equals(ddlPayrollPeriods.ClientID.Replace("_", "$"));
        }

        private void HideAll(bool show)
        {
            if (!show)
            {
                pagingCtl.Visible = false;
                gvw.Visible = false;
                //pnlPaging.Visible = false;

                divErrorMsg.Hide = false;

            }
            else
            {

                pagingCtl.Visible = true;
                gvw.Visible = true;
                //pnlPaging.Visible = true;
            
                divErrorMsg.Hide = true;
            }
        }

        private int GetPayrollPeriodId()
        {
            //return int.Parse(ddlPayrollPeriods.SelectedValue);
            return payrollPeriodId;
        }

        bool IsExportButtonSubmit()
        {
            return false;
        }

        void LoadCalculation(bool isEditMode, bool isReloadColumn)
        {

            if (calculation == null)
            {
                gvw.Columns[gvw.Columns.Count - 1].Visible = false;
            }
            else
            {
                gvw.Columns[gvw.Columns.Count - 1].Visible = true;
                //btnDelete.Enabled = true;
                //btnEdit.Enabled = true;
            }

            //if (payrollPeriodType == 1)
            //{
            //    headers = CalculationManager.GetIncomeHeaderList(SessionManager.CurrentCompanyId, this.GetPayrollPeriodId());

            //    // then remove SST & TDS header as not needed
            //    if (IsHoldPaymentListing)
            //    {
            //        headers.RemoveAll(x=>x.ColumnType==CalculationColumnType.SST);
            //        headers.RemoveAll(x => x.ColumnType == CalculationColumnType.TDS);
            //    }

            //}
            //else

            PartialTax tax = PayManager.GetPartialTax(this.GetPayrollPeriodId());
            if (tax != null)
            {
                txtName.Text = tax.Name;
                if (tax.ShowAfterPayrollPeriodId != null)
                {
                    ddlShowAfterPeriod.ClearSelection();
                    foreach (ListItem item in ddlShowAfterPeriod.Items)
                    {
                        if (item.Value == tax.ShowAfterPayrollPeriodId.ToString() + ":1")
                        {
                            item.Selected = true;
                            break;
                        }
                    }

                }
                if (tax.ShowInPayslip != null)
                    chkShowInPayslip.Checked = tax.ShowInPayslip.Value;
                else
                    chkShowInPayslip.Checked = false;
            }
            else
            {
                txtName.Text = "";
                ddlShowAfterPeriod.ClearSelection();
                foreach (ListItem item in ddlShowAfterPeriod.Items)
                {
                    if (item.Value == GetPayrollPeriodId() + ":1")
                    {
                        item.Selected = true;
                        break;
                    }
                }
            }

            headers = CalculationManager.GetPartialTaxPaidHeaderList(this.GetPayrollPeriodId(), false);

            foreach (CalcGetHeaderListResult header in headers)
            {
                bool isDeemedIncome = false;
                if (header.Type == 1)
                {
                    PIncome inc = new PayManager().GetIncomeById(header.SourceId.Value);
                    if (inc != null)
                    {
                        if (inc.Calculation == IncomeCalculation.DEEMED_INCOME)
                            isDeemedIncome = true;
                    }
                }


                string key = (isDeemedIncome ? "25" : header.Type.ToString()) + ":" + header.SourceId;
                if (!headerList.ContainsKey(key))
                    headerList.Add(key, "");
            }

            AddColumns(headers, gvw, null, isReloadColumn);

            HideAll(true);

            ChangeDisplayState(isEditMode, isReloadColumn);

            if (CalculationManager.IsAllEmployeeSavedFinally(this.GetPayrollPeriodId(), false))
            {
                //btnImport.Visible = false;
                btnChangeHeader.Visible = false;
                btnSaveName1.Visible = false;
                txtName.Enabled = false;
                btnGenerateTax.Visible = false;
                btnImport.Visible = false;
            }
            else
            {
                {
                    txtName.Enabled = true;
                    btnSaveName1.Visible = true;
                    btnImport.Visible = true;
                    btnChangeHeader.Visible = true;
                    btnGenerateTax.Visible = true;
                    btnImport.Visible = true;
                }
            }

            if (this.GetPayrollPeriodId() != 0)
            {


                int pageIndex = _tempCurrentPage - 1;
                int pageSize = int.Parse(pagingCtl.DDLRecords.SelectedValue);

                //if access from export
                if (IsExportButtonSubmit())
                {
                    pageSize = 99999;
                    pageIndex = 0;
                }

                //gvw.DataSource = null;
                //gvw.DataBind();

                int empId = -1;

                if (!string.IsNullOrEmpty(hiddenEmployeeID.Value.Trim()))
                {
                    empId = int.Parse(hiddenEmployeeID.Value.Trim());
                }
                if (txtEmpSearch.Text.Trim() == "")
                {
                    empId = -1;
                    hiddenEmployeeID.Value = "";
                }
                //if (IsHoldPaymentListing)
                //{
                //    gvw.DataSource = CalculationManager.GetHoldPaymentCalculationDetails(SessionManager.CurrentCompanyId,
                //        this.GetPayrollPeriodId(), pageIndex,
                //        pageSize, ref _tempCount);
                //}
                //else if (payrollPeriodType == 1)
                //    gvw.DataSource = CalculationManager.GetCalculationList(SessionManager.CurrentCompanyId,
                //        this.GetPayrollPeriodId(), pageIndex,
                //        pageSize, ref _tempCount, chkHasRetiredOrResigned.Checked, null, txtEmpSearch.Text.Trim(), (int)this.ViewSalaryType);
                //else
                gvw.DataSource = CalculationManager.GetPartialPaidEmployeeList(SessionManager.CurrentCompanyId,
                    this.GetPayrollPeriodId(), pageIndex,
                    pageSize, ref _tempCount, empId,txtEmpSearch.Text.Trim());


                gvw.DataBind();

                if (gvw.Rows.Count <= 0)
                {
                    divErrorMsg.InnerHtml = "No List";
                    // divInfoMsg.Style.Remove("display");
                    HideAll(false);
                    return;
                }

            }
            else
            {

                gvw.DataSource = null;
                gvw.DataBind();


                return;
            }



            //show/hide paging panel
            SetPagingSetting();



        }


      
        private void ChangeDisplayState(bool isEditMode,bool isReloadColumn)
        {
            
        
        }

      

        #region "Event handlers"



       


     

        //private void NoSavedSoInitialState()
        //{
        //    if (gvw.Columns[gvw.Columns.Count - 1].HeaderText == "Delete")
        //        gvw.Columns[gvw.Columns.Count - 1].Visible = false;

        //    btnDelete.Enabled = true;

        //    calculation = null;
        //}

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            editMode = false;
            LoadCalculation(false,false);
        }

        #region "Save Calculation"

        //public bool IsIncomeAdjustmentChecked()
        //{
        //    if (gvw.HeaderRow == null)
        //        return false;

        //    TableRow rowHeader = (System.Web.UI.WebControls.TableRow) gvw.HeaderRow;
        //    for (int j = 2; j < rowHeader.Cells.Count - 1; j++)
        //    {
        //        if (rowHeader.Cells[j].Controls.Count > 0)
        //        {
        //            Label lblHeader = (Label) rowHeader.Cells[j].Controls[0];

        //            TextBox txtAdjustment = null;
        //            if (lblHeader.Attributes["Type"] == ((int) CalculationColumnType.IncomeAdjustment).ToString())
        //            {
        //                CheckBox chk = (CheckBox) rowHeader.Cells[j].Controls[2];
        //                return chk.Checked;
        //            }
        //        }
        //    }
        //    return false;
        //}


     

        #endregion

        protected void Page_PreRender(object sender, EventArgs e)
        {
            CalculationManager.RegisterForTextBoxKeysMovement(gvw);
            // If error then hide buttons or middle tax paid salary type then also hide button
            if (!isValidAllValid || payrollPeriodType==2)
            {
                if (gvw.Columns[gvw.Columns.Count - 1].Visible)
                {
                   
                }
                else
                {
                 
                }

                if (payrollPeriodType != 2)
                {
                    divErrorMsg.InnerHtml = Resources.Messages.CaluclationInvalidNegativeValueMsg;
                    divErrorMsg.Hide = false;
                }
                //disable all cells
               //CalculationManager.DisableAllCells(gvw);
            }
            if (payrollPeriodType == 2)
            {
                
            }
            hdnChangedInClientValues.Value = "";

        }

        #endregion

        public void btnSaveName_Click(object sender, EventArgs e)
        {
            txtName.Text = Request.Form[txtName.ClientID.Replace("_", "$")];
            string text = Request.Form[ddlShowAfterPeriod.ClientID.Replace("_", "$")];
            string showInPayslip = Request.Form[chkShowInPayslip.ClientID.Replace("_", "$")];
            if (showInPayslip == null)
                showInPayslip = "";
            int showAfterPeriodId = int.Parse(text.Substring(0,text.IndexOf(":")));
            bool show = showInPayslip.ToString().ToLower().Trim() == "on" ? true : false;

            PayManager.SavePartialTax(Request.Form[txtName.ClientID.Replace("_", "$")], 0, this.GetPayrollPeriodId(), showAfterPeriodId, show);

            if (show)
                chkShowInPayslip.Checked = true;
            else
                chkShowInPayslip.Checked = false;

            ddlShowAfterPeriod.ClearSelection();
            foreach (ListItem item in ddlShowAfterPeriod.Items)
            {
                if (item.Value == showAfterPeriodId.ToString() + ":1")
                {
                    item.Selected = true;
                    break;
                }
            }
                  

            msgCtl.InnerHtml = "Name changed.";
            msgCtl.Hide = false;
        }

        #region "Grid Manipulation"

        public static void AddColumns(List<CalcGetHeaderListResult> headers, GridView gvw, Calculation calculationControl, bool isReloadColumn)
        {
            //First remove column
            if (gvw.Columns.Count != 4 && isReloadColumn)
            {
                for (int i = gvw.Columns.Count - 3; i>=3; i--)
                {
                    gvw.Columns.RemoveAt(i);
                }
            }

            if (gvw.Columns.Count == 4)
            {
                //first sort column list

                headers = CalculationValue.SortHeaders(headers,PayManager.GetIncomeOrderValues(), PayManager.GetDeductionOrderValues());
                PIncome basicIncome =  new PayManager().GetBasicIncome(SessionManager.CurrentCompanyId);
                
                for (int i = 0; i < headers.Count; i++)
                {
                    TemplateField field = new TemplateField();
                    field.ItemTemplate = new CalcGridViewTemplate(DataControlRowType.DataRow, headers[i], calculationControl, basicIncome.IncomeId);
                    field.HeaderTemplate = new CalcGridViewTemplate(DataControlRowType.Header, headers[i], calculationControl, basicIncome.IncomeId);
                    gvw.Columns.Insert(gvw.Columns.Count - 1, field);
                }
            }
            
        }

        

        #endregion

        #region "Paging"

        private int _tempCurrentPage;
        private int? _tempCount = 0;

    //    private int payrollPeriodId = 0;

    

        protected override void OnInit(EventArgs e)
        {
            Page.RegisterRequiresControlState(this);
            base.OnInit(e);
        }
        protected override void LoadControlState(object savedState)
        {
            object[] rgState = (object[])savedState;
            base.LoadControlState(rgState[0]);

            _tempCurrentPage = (int)rgState[1];
            payrollPeriodId = (int)rgState[2];
            payrollPeriodType = (int)rgState[4];
            editMode = (bool)rgState[3];
        }
        protected override object SaveControlState()
        {
            object[] rgState = new object[5];
            rgState[0] = base.SaveControlState();

            rgState[1] = _tempCurrentPage;
            rgState[2] = payrollPeriodId;
            rgState[3] = editMode;
            rgState[4] = payrollPeriodType;
            return rgState;
        }

        protected void btnPrevious_Click()
        {
            _tempCurrentPage = int.Parse(pagingCtl.LabelCurrentPage.Text);
            _tempCurrentPage -= 1;
            LoadCalculation(false,false);
        }

        protected void btnNext_Click()
        {
            _tempCurrentPage = int.Parse(pagingCtl.LabelCurrentPage.Text);
            _tempCurrentPage += 1;
            LoadCalculation(false,false);
        }
        protected void ddlRecords_SelectedIndexChanged()
        {
            _tempCurrentPage = 1;
            LoadCalculation(false, false);
        }

        protected void ChangePageNumber()
        {
            _tempCurrentPage = this.pagingCtl.CurrentPage;
            LoadCalculation(false, false);
        }

        private void SetPagingSetting()
        {
            if (_tempCount <= 0)
                pagingCtl.Visible = false;
            else
                pagingCtl.Visible = true;

            //calcuation .. of .. page
            int totalPages = (int)Utils.Helper.Util.CalculateTotalPages(_tempCount.Value,
                                                                        int.Parse(pagingCtl.DDLRecords.SelectedValue));
            pagingCtl.LabelTotalPage.Text = (totalPages).ToString();
            pagingCtl.LabelCurrentPage.Text = _tempCurrentPage.ToString();

            // Update Pge numbers
            if (pagingCtl.DDLPageNumber.Items.Count != totalPages)
            {
                pagingCtl.DDLPageNumber.Items.Clear();
                for (int i = 1; i <= totalPages; i++)
                {
                    pagingCtl.DDLPageNumber.Items.Add(i.ToString());
                }
            }
            if (this.pagingCtl.DDLPageNumber.Items.FindByValue(_tempCurrentPage.ToString()) != null)
                this.pagingCtl.DDLPageNumber.SelectedValue = _tempCurrentPage.ToString();

            //show/hide Next/Previous buttons
            if (_tempCurrentPage <= 1)
                pagingCtl.ButtonPrev.Enabled = false;
            else
                pagingCtl.ButtonPrev.Enabled = true;

            if (_tempCurrentPage < totalPages)
                pagingCtl.ButtonNext.Enabled = true;
            else
                pagingCtl.ButtonNext.Enabled = false;
        }
        #endregion

        protected void gvw_RowCreated(object sender, GridViewRowEventArgs e)
        {
           

            //Display error if attendance is not complete
            if( e.Row.RowType==DataControlRowType.DataRow)
            {
                CalcGetCalculationListResult data = e.Row.DataItem as CalcGetCalculationListResult;

                if (data.IsAttendanceComplete.Value != 1)
                {
                    WebControl ctl = e.Row.FindControl("lblId") as WebControl;
                    ctl.BorderWidth = new Unit(1);
                    ctl.BorderColor = Color.Red;


                    ctl.ToolTip = Resources.Messages.AttendanceNotCompleteForCalculationMsg;


                    isValidAllValid = false;
                }

                if (data.IsSalaryGenerationUnsuccessfull)
                {
                    WebControl ctl = e.Row.FindControl("lblId") as WebControl;
                    ctl.BorderWidth = new Unit(1);
                    ctl.BorderColor = Color.Red;


                    ctl.ToolTip = Resources.Messages.CalcSalaryGenerationFail;


                    isValidAllValid = false;
                }

            }
        }

        protected void gvw_DataBound(object sender, EventArgs e)
        {
             CalculationManager.CreateGroupInHeader(sender
                 , gvw.Columns[gvw.Columns.Count-1 ].Visible );
        }

        
       
      

        //protected void ddlPayrollPeriods_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    this.payrollPeriodId = int.Parse(Request.Form[ddlPayrollPeriods.ClientID.Replace("_", "$")]);
        //    Initialise();
        //    _tempCurrentPage = 1;
        //    ;
        //    LoadCalculation(false, true);

        //}


    }

}