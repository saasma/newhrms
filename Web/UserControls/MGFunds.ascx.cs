﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using DAL;
using BLL;
using Utils.Helper;
using Utils.Web;
using Utils.Calendar;

namespace Web.UserControls
{
    public partial class MGFunds : BaseUserControl
    {
        CommonManager commonMgr = new CommonManager();
        int companyId = 0;
        Company currentEditingCompany = null;
        CompanyManager compMgr = new CompanyManager();

        public Control ParentControl
        {
            get
            {
                return this.fieldSetPFRF;
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            // Load status for PF Effective from
            //ListItem[] items = BizHelper.GetMembersAsListItem(new JobStatus());

            //ddlEffectiveFrom.Items.AddRange(items);

            JobStatus statues = new JobStatus();
            ddlEffectiveFrom.DataSource = statues.GetMembersForHirarchyView();
            ddlEffectiveFrom.DataValueField = "Key";
            ddlEffectiveFrom.DataTextField = "Value";

            ddlEffectiveFrom.DataBind();
            //remove Contract type
            //ddlEffectiveFrom.Items.RemoveAt(2);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            companyId = UrlHelper.GetIdFromQueryString("Id");
            if (companyId != 0)
            {
                currentEditingCompany = compMgr.GetById(companyId);
            }
            if (!IsPostBack)
            {
                if (currentEditingCompany != null)
                    calPFApprovalDate.IsEnglishCalendar = currentEditingCompany.IsEnglishDate;

                if (companyId == 0)
                    calPFApprovalDate.SelectTodayDate();
                //Initialise();



            }
            foreach (ListItem item in rdbListPFApproval.Items)
            {
                item.Attributes.Add("onclick", string.Format("changeForApproval(this,'{0}')", calPFApprovalDate.ClientID));
            }

            //disable as ei & cit has been removed for company   
            if (rdbListPFApproval.Items[0].Selected)
            {
                calPFApprovalDate.Enabled = true;
                EnabledChildControls(calPFApprovalDate);
            }
            else
            {
                calPFApprovalDate.Enabled = false;
                DisableChildControls(calPFApprovalDate);
            }
        }

        public void ChangeDateType(bool isEnglishDate)
        {
            calPFApprovalDate.IsEnglishCalendar = isEnglishDate;
            calPFApprovalDate.SelectTodayDate();
        }

        void Initialise()
        {

            //Set the visibility of Tabs
            //Company currentComp = SessionManager.CurrentCompany;
            if (currentEditingCompany != null)
            {
                lblPFText.Text = string.Format(lblPFText.Text, currentEditingCompany.PFRFAbbreviation);
                //RequiredFieldValidator0.ErrorMessage = string.Format(RequiredFieldValidator0.ErrorMessage, currentEditingCompany.PFRFAbbreviation);

                if (currentEditingCompany.PFRFFunds.Count > 0 && currentEditingCompany.HasPFRFFund)
                {
                    ProcessPF(currentEditingCompany.PFRFFunds[0],currentEditingCompany.IsEnglishDate);
                }
            }


        }

        public PFRFFund ProcessPF(PFRFFund obj,bool isEnglishDate)
        {

            if (currentEditingCompany != null)
                isEnglishDate = currentEditingCompany.IsEnglishDate;

            if (obj == null)
            {
                obj = new PFRFFund();               
                obj.PFRFNo = txtPFNo.Text.Trim();                
                obj.IsApproved = rdbListPFApproval.Items[0].Selected;
                if (obj.IsApproved)
                {

                    obj.ApprovalDate = calPFApprovalDate.SelectedDateAsFirstDay.ToString();
                    obj.ApprovalDateEng = BLL.BaseBiz.GetEngDate(obj.ApprovalDate, isEnglishDate);    
                }
                else
                    obj.ApprovalDateEng = null;

                obj.EmployeeContribution = double.Parse(Util.FormatPercentForInput(txtPFECPercent.Text.Trim()));                
                obj.CompanyContribution = double.Parse(Util.FormatPercentForInput(txtPFCCPercent.Text.Trim()));
                txtPFECPercent.Text = Util.FormatPercentForInput(obj.EmployeeContribution);
                txtPFCCPercent.Text = Util.FormatPercentForInput(obj.CompanyContribution);
                obj.EffectiveFrom = int.Parse(ddlEffectiveFrom.SelectedValue);
                return obj;
            }
            else
            {
                txtPFNo.Text = obj.PFRFNo;                
                if (obj.IsApproved)
                {
                    rdbListPFApproval.ClearSelection();
                    rdbListPFApproval.Items[0].Selected = true;
                    rdbListPFApproval.Items[1].Selected = false;
                    if (!string.IsNullOrEmpty(obj.ApprovalDate))// && currentEditingCompany!= null)
                    {
                        calPFApprovalDate.SetSelectedDate(obj.ApprovalDate, obj.Company.IsEnglishDate);
                    }
                }
                else
                {
                    rdbListPFApproval.Items[1].Selected = true;
                    rdbListPFApproval.Items[0].Selected = false;
                }
                txtPFECPercent.Text = Util.FormatPercentForInput(obj.EmployeeContribution);
                txtPFCCPercent.Text = Util.FormatPercentForInput(obj.CompanyContribution);
                UIHelper.SetSelectedInDropDown(ddlEffectiveFrom, obj.EffectiveFrom);
            }
            return null;
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
           
        }


        public void EnableValControls(string type)
        {
            if (type == "PFRF")
                EnabledChildValidationControls(fieldSetPFRF);           
        }
        public void DisableValControls(string type)
        {
            if (type == "PFRF")
                DisableChildValidationControls(fieldSetPFRF);          
        }

        public void EnableDisableControls(bool hasPFRF)//, bool hasCIT, bool hasEI)
        {
            if (hasPFRF)
            {

                EnabledChildControls(fieldSetPFRF);
                fieldSetPFRF.Style.Remove("display");
            }
            else
            {
                DisableChildControls(fieldSetPFRF);
                fieldSetPFRF.Style.Add("display", "none");

            }
           
        }

        public void DisablePFSectionAfterPFGenerationIsSavedInSalary()
        {
            //calPFApprovalDate.Enabled = false;
            calPFApprovalDate.Enabled = false;
            rdbListPFApproval.Enabled = false;

        }

        protected void valCustomRFAC_ServerValidate(object source, ServerValidateEventArgs args)
        {
            double val;
            if (double.TryParse(txtPFECPercent.Text, out val) == false)
            {
                args.IsValid = true;
                return;
            }
            args.IsValid = (double.Parse(txtPFCCPercent.Text) + double.Parse(txtPFECPercent.Text)) == 0.0 ? false : true;
        }
    }
}