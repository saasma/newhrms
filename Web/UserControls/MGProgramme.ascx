﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MGProgramme.ascx.cs" Inherits="Web.UserControls.MGProgramme" %>

<script type="text/javascript">
    function ConfirmLibraryTopicDeletion() {
        var ddllist = document.getElementById('<%= ddlDeposits.ClientID %>');
        if (ddllist.selectedIndex == -1) {
            alert('No programme to delete.');
            return false;
        }
        if (confirm('Do you want to delete the programme?')) {
            clearUnload();
            return true;
        }
        else {
            return false;
        }
    }

    function DisplayLibraryTopicInTextBox(dropdown_id) {
        var ddllist = document.getElementById(dropdown_id);
        document.getElementById('<%= txtDepositRename.ClientID %>').value = ddllist.options[ddllist.selectedIndex].text;
    }

        

    //capture window closing event
    window.onunload = closePopup;
    function closePopup() {
        clearUnload();
        if ($.browser.msie == false && typeof (window.opener.parentReloadLocationProgramCallbackFunction) != 'undefined' && ($.browser.chrome || $.browser.safari)) {
            window.opener.parentReloadLocationProgramCallbackFunction("ReloadProgram", window, texts);
        } else {
            if (typeof (texts) != 'undefined')
                window.returnValue = texts;
            window.close();
        }
    }

    function clearUnload() {
        window.onunload = null;
    }

</script>
<div class="popupHeader">
    <h2 class="headlinespop">
        Manage Programme</h2>
</div>
<div class="marginal">
    <div class="bevel ">
        <div class="fields paddpop"  style="padding: 10px 0 20px 10px !important;width:390px;">
            <table cellpadding="0" cellspacing="0" class="alignttable" width="600px">
                <tr>
                    <td class="lf" width="120px">
            New Programme
            </td>
            <td>
            <asp:TextBox ID="txtNewDeposit" runat="server" Width="159px" MaxLength="100" EnableViewState="false"></asp:TextBox>
             <asp:RequiredFieldValidator ValidationGroup="Deposit1" ID="RequiredFieldValidator3"
                Display="None" runat="server" ErrorMessage="Programme name is required." ControlToValidate="txtNewDeposit">*</asp:RequiredFieldValidator>
              <asp:Button ID="btnAddDepsoit" Style="margin: 0px 0 0 5px" CssClass="update"  OnClientClick="valGroup='Deposit1';return CheckValidation()"
                runat="server" Text="Add" OnClick="btnAddDepsoit_Click" />
            </td>           
       </tr>
       
       <tr>
             <td class="lf" width="120px">
            Rename Programme
            </td>
            <td>
          <asp:TextBox ID="txtDepositRename" runat="server" Width="159px" MaxLength="100"></asp:TextBox>
            <asp:RequiredFieldValidator ValidationGroup="Deposit2" ID="RequiredFieldValidator4"
                runat="server" ErrorMessage="Programme is required." ControlToValidate="txtDepositRename"
                Display="None">*</asp:RequiredFieldValidator>
            <asp:Button ID="btnRename" OnClientClick="valGroup='Deposit2';return CheckValidation();"
                runat="server" Text="Update" CssClass="save"  Style="margin: 0px 0 0 5px"   ValidationGroup="RenameLibraryTopic" OnClick="btnRename_Click" />
                
             
            </td>           
       </tr>
       
       <tr>
            <td class="lf" width="120px">
            Programme List
            </td>
            <td>
              <asp:DropDownList ID="ddlDeposits" runat="server" Width="165px" CssClass="rightMargin"
                DataValueField="ProgrammeId" DataTextField="Name" onchange="DisplayLibraryTopicInTextBox(this.id);">
            </asp:DropDownList>
            
               <asp:Button ID="btnDelete" Style="margin: 0px 0 0 5px" CssClass="delete"   runat="server" Text="Delete" OnClientClick="javascript:return ConfirmLibraryTopicDeletion();"
                OnClick="btnDelete_Click" />
            </td>
                </tr>
            </table>
        </div>
    </div>

<%--<% if(IsDisplayedAsPopup){ %>
<input id="btnClose" type="button" value="Close" onclick="closePopup()" />
<%} %>--%>