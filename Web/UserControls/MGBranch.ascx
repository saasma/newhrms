﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MGBranch.ascx.cs" Inherits="Web.UserControls.MGBranch" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<script type="text/javascript">
        var divRemoteArea = '#<%= divRemoteArea.ClientID %>';



        function ValidateTextBox1(source, args) {
            args.IsValid = true;
            if ($('#' + source.id).prop('disabled') == false) {
                if (args.Value == "")
                    args.IsValid = false;                   
            }
        }

        //capture window closing event
       <%
        if( IsDisplayedAsPopup)
        {
        
            Response.Write("window.onunload = closePopup;");
        }
         %>
        
        function closePopup() {
            clearUnload();
             if ($.browser.msie == false && typeof (window.opener.parentReloadCallbackFunction) != 'undefined' && ($.browser.chrome ||  $.browser.safari)) {
                window.opener.parentReloadCallbackFunction("ReloadBranch", window, texts);
            } else {
                if (typeof (texts) != 'undefined')
                    window.returnValue = texts;
                window.close();
             }
         }

        function clearUnload() {
            window.onunload = null;
        }
        
     
       
       function handleDelete()
       {            
            if(confirm('Do you want to delete branch?'))
            {
                clearUnload(); 
                return true;
            }
            else
                return false;
       }
       
        
        var districtId ='<%= ddlDistricts.ClientID %>';
        var vdcId = '<%= ddlVDCs.ClientID %>';    
        function DisplayRemoteArea(){
           
            Web.PayrollService.GetRemoveAreaValue($('#' + vdcId).val(),DisplayRemoteAreaCallback);
        }

        function cmbBranchHeadChange(){
           
            var hid = document.getElementById('Hidden_BranchHeadID');
            var cmbHead = document.getElementById('cmbBranchHead');
            hid.value = cmbHead.value; 
        }
       
       function DisplayRemoteAreaCallback(result){
            var ctl = document.getElementById('<%= lblRemoteAreaValue.ClientID %>');
            if(result == "" )   
                ctl.style.display = 'none';
            else
                ctl.style.display = '';
                
            ctl.innerHTML =  result;
       }
       

       
        function onChangeDistrict(ddl) {
            Web.PayrollService.GetVDCByDistrict(ddl.value, onChangeDistrictCallback);
        }

        function onChangeDistrictCallback(result) {

            var list = result;
            document.getElementById(vdcId).options.length = 1;
            for (i = 0; i < list.length; i++) {
                var menu = list[i];
                var htmlContent = String.format("<option value='{0}'>{1}</option>", menu.Key, menu.Value);
                $('#' + vdcId).append(htmlContent);
            }
            DisplayRemoteArea();
        }
        
        function showDetails()
        {
            $("#<%= details.ClientID %>").show("fast");
        }
        function hideDetails()
        {
        }
</script>
<style type="text/css">
    h2.popstitle
    {
        color: #333333;
        font: bold 13px Arial;
        padding-bottom: 0px !important;
    }
</style>
<div align="left">
    <div class="marginal">
        <asp:GridView CssClass="tableLightColor" PagerStyle-HorizontalAlign="Center" PagerStyle-CssClass="defaultPagingBar"
            UseAccessibleHeader="true" ID="gvwBranches" CellPadding="2" Width="100%" runat="server"
            DataKeyNames="BranchId" AutoGenerateColumns="False" GridLines="None" OnSelectedIndexChanged="gvwBranches_SelectedIndexChanged"
            AllowPaging="True" PageSize="100" OnRowCreated="gvwEmployees_RowCreated" OnPageIndexChanging="gvwBranches_PageIndexChanging"
            OnRowDeleting="gvwBranches_RowDeleting">
            <Columns>
                <asp:TemplateField HeaderStyle-Width="50" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                    HeaderText="SN">
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    <ItemTemplate>
                        <%# Container.DisplayIndex + 1 %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                    DataField="Name" HeaderText="Branch Name">
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundField>
                 <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                    DataField="Code" HeaderText="Code">
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                    DataField="Address" HeaderText="Address">
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                    DataField="Phone" HeaderText="Phone">
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundField>
                <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderText="Head Office"
                    ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <%# Utils.Web.UIHelper.GetYesImage( Eval("IsHeadOffice"),Page) %>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Edit" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="30px">
                    <ItemTemplate>
                        <asp:ImageButton ID="ImageButton1" runat="server" OnClientClick="clearUnload();"
                            CommandName="Select" ImageUrl="~/images/edit.gif" />
                    </ItemTemplate>
                    <HeaderStyle Width="30px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Delete" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="30px">
                    <ItemTemplate>
                        <asp:ImageButton ID="ImageButton2" OnClientClick="return handleDelete()" runat="server"
                            CommandName="Delete" ImageUrl="~/images/delet.png" />
                    </ItemTemplate>
                    <HeaderStyle Width="30px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <RowStyle CssClass="odd" />
            <AlternatingRowStyle CssClass="even" />
            <SelectedRowStyle CssClass="selected" />
            <PagerStyle HorizontalAlign="Center" CssClass="defaultPagingBar"></PagerStyle>
            <EmptyDataTemplate>
                No Branch has been created.
            </EmptyDataTemplate>
        </asp:GridView>
        <div class="buttonsDiv" style='clear: both; width: 100%!important;'>
            <asp:LinkButton ID="btnAddNew" runat="server" Style='margin-left: 10px' CssClass="createbtns"
                Text="Create Branch" OnClick="btnAddNew_Click" />
        </div>
        <uc2:MsgCtl ID="divMsgCtl" Width='<%# IsDisplayedAsPopup ? "600px" : "920px" %>'
            EnableViewState="false" Hide="true" runat="server" />
        <uc2:WarningCtl ID="divWarningMsg" Width='<%# IsDisplayedAsPopup ? "600px" : "920px" %>'
            EnableViewState="false" Hide="true" runat="server" />
        <div id="details" runat="server" class="bevel" style="margin-top: 20px;" visible="false">
            <div class="fields paddpop" style="padding: 10px 0pt 20px 10px ! important;">
                <h2 class="popstitle">
                    Branch Information</h2>
                <table cellpadding="4px">
                    <tr>
                        <td class="fieldHeader">
                            <My:Label ID="Label2" Text="Branch Name" runat="server" ShowAstrick="true" />
                        </td>
                        <td>
                            <asp:TextBox ID="txtBranchName" runat="server" Width="180px" />
                            <asp:RequiredFieldValidator ID="valReqdName" runat="server" ControlToValidate="txtBranchName"
                                Display="None" ErrorMessage="Branch name is required." ValidationGroup="AEBranch"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="fieldHeader">
                            <My:Label ID="Label4" Text="Branch Code" runat="server" ShowAstrick="false" />
                        </td>
                        <td>
                            <asp:TextBox ID="txtCode" runat="server" Width="180px" />
                        </td>
                    </tr>
                    <tr class="seperatoring" runat="server" visible="false">
                        <td class="fieldHeader">
                            Responsible Person
                        </td>
                        <td>
                            <asp:TextBox ID="txtResponsiblePerson" runat="server" Width="180px" />
                        </td>
                    </tr>
                    <tr>
                        <td class="fieldHeader">
                            <My:Label ID="Label1" Text="Address" runat="server" ShowAstrick="false" />
                        </td>
                        <td>
                            <asp:TextBox ID="txtAddress" runat="server" Width="180px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="fieldHeader">
                            <My:Label ID="Label3" Text="Branch Head" runat="server" ShowAstrick="false" />
                        </td>
                        <td>
                            <asp:HiddenField ID="Hidden_BranchHeadID" runat="server" />
                            <asp:DropDownList EnableViewState="false" ID="cmbBranchHead" onchange="cmbBranchHeadChange()"
                                DataTextField="Name" Width="185px" CssClass="marginbtns" DataValueField="EmployeeId"
                                runat="server" AppendDataBoundItems="true">
                                <asp:ListItem Value="-1">--Select a Employee--</asp:ListItem>
                            </asp:DropDownList>
                            <%--<asp:RequiredFieldValidator InitialValue="-1" Display="None" ControlToValidate="ddlVDCs" DisplayRemoteArea()
                                            ValidationGroup="AEBranch" ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please select a VDC."></asp:RequiredFieldValidator>--%>
                        </td>
                    </tr>
                    <tr>
                        <td class="fieldHeader">
                            Phone
                        </td>
                        <td>
                            <asp:TextBox ID="txtPhone" runat="server" Width="180px"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="valRegPhone" runat="server" ControlToValidate="txtPhone"
                                ErrorMessage="Phone number is invalid." ValidationGroup="AEBranch" Display="None"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr runat="server" visible="false">
                        <td class="fieldHeader">
                            Fax
                        </td>
                        <td>
                            <asp:TextBox ID="txtFax" runat="server" Width="180px"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="valRegFax" runat="server" ControlToValidate="txtFax"
                                ErrorMessage="Fax is invalid." ValidationGroup="AEBranch" Display="None"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr class="seperatoring">
                        <td class="fieldHeader">
                            Email
                        </td>
                        <td>
                            <asp:TextBox ID="txtEmail" runat="server" Width="180px"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                ControlToValidate="txtEmail" ErrorMessage="Email is invalid." ValidationGroup="AEBranch"
                                Display="None"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr class="up">
                        <td class="fieldHeader">
                            Checking Bank Name
                        </td>
                        <td>
                            <asp:TextBox ID="txtBankName" runat="server" Width="180px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="seperatoring">
                        <td class="fieldHeader">
                            Account No
                        </td>
                        <td>
                            <asp:TextBox ID="txtBankAccountNo" runat="server" Width="180px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="up" style="display:none">
                        <td class="fieldHeader">
                            <asp:CheckBox ID="chkHasSeperateRetirementAC" TextAlign="Right" runat="server" Text="Seperate {0} account" />
                        </td>
                        <td>
                            <asp:TextBox ID="txtSeperateRetirementAC" runat="server" Width="180px"></asp:TextBox>
                            <asp:CustomValidator Display="None" ValidateEmptyText="true" ClientValidationFunction="ValidateTextBox1"
                                ControlToValidate="txtSeperateRetirementAC" ValidationGroup="AEBranch" ID="valCustomRFAC"
                                runat="server" ErrorMessage="{0} account is required."></asp:CustomValidator>
                        </td>
                    </tr>
                    <tr class="seperatoring" style="display:none">
                        <td class="fieldHeader">
                            <asp:CheckBox ID="chkHasSeperateEmpInsuraceAC" TextAlign="Right" runat="server" Text="Seperate Employee Insurance account" />
                        </td>
                        <td>
                            <asp:TextBox ID="txtSeperateEmpInsuranceAC" Width="180px" runat="server"></asp:TextBox>
                            <asp:CustomValidator ValidateEmptyText="true" Display="None" ClientValidationFunction="ValidateTextBox1"
                                ControlToValidate="txtSeperateEmpInsuranceAC" ValidationGroup="AEBranch" ID="CustomValidator2"
                                runat="server" ErrorMessage="Employee insurance account is required."></asp:CustomValidator>
                        </td>
                    </tr>
                    <tr class="up">
                        <td class="fieldHeader" valign="top">
                            <asp:CheckBox ID="chkIsInRemoteArea" runat="server" Text="Remote Area" />
                        </td>
                        <td id="divRemoteArea" style='padding-left: 0px' runat="server">
                            <table>
                                <tr>
                                    <td align="left">
                                        <asp:DropDownList EnableViewState="false" onchange="onChangeDistrict(this)" disabled='disabled'
                                            ID="ddlDistricts" DataTextField="District" DataValueField="DistrictId" runat="server"
                                            AppendDataBoundItems="true" Width="185px" CssClass="marginbtns">
                                            <asp:ListItem Value="-1">--Select District--</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator InitialValue="-1" Display="None" ControlToValidate="ddlDistricts"
                                            ValidationGroup="AEBranch" ID="valCustDistrict" runat="server" ErrorMessage="Please select a district."></asp:RequiredFieldValidator>
                                    </td>
                                    <td valign="top" style='padding: 2px; padding-top: 7px'>
                                        <asp:Label ID="lblRemoteAreaValue" EnableViewState="false" runat="server" Style='border: 1px solid blue' />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:DropDownList EnableViewState="false" onchange="DisplayRemoteArea()" disabled='disabled'
                                            ID="ddlVDCs" DataTextField="Name" Width="185px" CssClass="marginbtns" DataValueField="VDCId"
                                            runat="server" AppendDataBoundItems="true">
                                            <asp:ListItem Value="-1">--Select VDC--</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator InitialValue="-1" Display="None" ControlToValidate="ddlVDCs"
                                            ValidationGroup="AEBranch" ID="valCustVDC" runat="server" ErrorMessage="Please select a VDC."></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                            </table>
                            <%--</div>--%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CheckBox  ID="chkIsManualAttendance" runat="server" Text="Manual Attendance" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <asp:Button ID="btnSave" CssClass="update" OnClientClick="valGroup='AEBranch';return CheckValidation()"
                                runat="server" Text="Save" ValidationGroup="AEBranch" OnClick="btnSave_Click" />
                            <asp:Button ID="btnCancel" OnClientClick="clearUnload()" CssClass="cancel" runat="server"
                                Text="Cancel" OnClick="btnCancel_Click" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <%--</fieldset>--%>
    </div>
</div>
