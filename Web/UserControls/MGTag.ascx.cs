﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Manager;
using Utils.Helper;
using BLL;

namespace Web.UserControls
{
    public partial class MGTag : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            JavascriptHelper.AttachPopUpCode(Page, "popupTax", "AETax.aspx", 400, 300);
            SetPagePermission();
        }

        /// <summary>
        /// Set for Custom Role if is in Readonly Mode or Not
        /// </summary>
        public void SetPagePermission()
        {
            if (SessionManager.IsCustomRole && SessionManager.CustomRole.IsReadOnly != null && SessionManager.CustomRole.IsReadOnly.Value)
            {
                btnAddTax.Visible = false;
            }
        }
        public void DisplayTax(int employeeId)
        {
            TaxManager mgr = new TaxManager();
            tax.InnerHtml = mgr.GetHTMLEmployeeTaxList(employeeId);
            //btnAddIncome.Enabled = true;
            btnAddTax.Enabled = true;
        }

        public void EnableButton()
        {
            btnAddTax.Enabled = true;
        }
    }
}