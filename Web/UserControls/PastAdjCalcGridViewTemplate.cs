﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using System.Drawing;
using System.Text;
using Utils.Helper;
using Image = System.Web.UI.WebControls.Image;


namespace Web.UserControls
{
    // Here is the code for building the template field
    public class PastCalculationAdjCtlTemplate : ITemplate
    {
        private DataControlRowType templateType;
        private CalcGetHeaderListResult header;
        private CalculationColumnType columnType;
        private PastCalculationAdjCtl calculation;
        private int basicIncomeId = 0;

        bool isHeaderMissingError = false;
        int missingType = 0;
        int missingSourceId = 0;


        public PastCalculationAdjCtlTemplate(DataControlRowType type, CalcGetHeaderListResult header, PastCalculationAdjCtl calculation, int basicIncomeId)
        {
            templateType = type;
            this.header = header;
            this.basicIncomeId = basicIncomeId;
            this.columnType = (CalculationColumnType)header.Type;
            this.calculation = calculation;
        }

        private  void SetHeaderColor(Control container)
        {
            string backColor;
            
            if (CalculationValue.IsColumTypeIncome(columnType))
                backColor = CalculationManager.incomeColor;
            else if (CalculationValue.IsColumTypeDeduction(columnType))
                backColor = CalculationManager.deductionColor;
            else if(columnType==CalculationColumnType.IncomeGross)
                backColor = CalculationManager.incomeColor;
            else if( columnType==CalculationColumnType.DeductionTotal)
                backColor = CalculationManager.deductionColor;
            else
            {
                backColor = CalculationManager.netColor;
            }

            ((System.Web.UI.WebControls.WebControl)(container)).BackColor =
                           System.Drawing.ColorTranslator.FromHtml(backColor);
        }

        /// <summary>
        /// Generate dynamic control id,appenid i-income,d-deduction so as to prevent duplicate id
        /// </summary>
        /// <param name="header"></param>
        /// <returns></returns>
        public string GetIDPostfix(CalcGetHeaderListResult header)
        {
            if (CalculationValue.IsColumTypeIncome(header.ColumnType))
                return "i" + header.Type + header.SourceId;
            else
                return "d" + header.Type + header.SourceId;
        }

        public void InstantiateIn(System.Web.UI.Control container)
        {
            switch (templateType)
            {
                case DataControlRowType.Header:

                    Label lbl = new Label();

                    lbl.Width = new Unit(Calculation.ColumnWidth-3);
                    lbl.Text = header.HeaderName;
                    lbl.ToolTip = header.HeaderName;


                    lbl.ID = "lbl" + GetIDPostfix(header);// header.Type + header.SourceId;
                    //values will be used while saving
                    lbl.Attributes.Add("SourceId", header.SourceId.ToString());
                    lbl.Attributes.Add("Type", ((int)columnType).ToString());
                    lbl.Attributes.Add("HeaderName", header.HeaderName);

                    //Set header color
                    SetHeaderColor(container);

                    


                    container.Controls.Add(lbl);

                    //if (header.ColumnType == CalculationColumnType.IncomeAdjustment
                    //    || header.ColumnType == CalculationColumnType.DeductionAdjustment 
                    //    || header.ColumnType==CalculationColumnType.IncomeResterospect)
                    //{
                    //    CheckBox chkIncomeAdj = new CheckBox();
                    //    TextBox txtAdj = new TextBox();
                        

                    //    if (header.ColumnType == CalculationColumnType.IncomeAdjustment)
                    //    {
                    //        txtAdj.ID = "txtIncomeAdj";
                    //        chkIncomeAdj.ID = CalculationManager.idIncomeAdjCheckbox;
                    //        chkIncomeAdj.Attributes.Add("onclick", "adjustmentCheckbox(this,'.adjustmentIncome',event)");

                    //        if ((calculation.calculation != null && calculation.calculation.HasAdjustmentIncome ) || calculation.isIncomeAdjChecked)
                    //            chkIncomeAdj.Checked = true;
                    //    }
                    //    else if( header.ColumnType==CalculationColumnType.DeductionAdjustment)
                    //    {
                    //        txtAdj.ID = "txtDeductionAdj";
                    //        chkIncomeAdj.ID = CalculationManager.idDeductionAdjCheckbox;
                    //        chkIncomeAdj.Attributes.Add("onclick", "adjustmentCheckbox(this,'.adjustmentDeduction',event)");

                    //        if ((calculation.calculation != null && calculation.calculation.HasAdjustmentDeduction.Value) || calculation.isDeductionAdjChecked)
                    //            chkIncomeAdj.Checked = true;
                    //    }
                    //    else
                    //    {
                    //        txtAdj.ID = "txtRetrospectIncrement"; 
                    //    }

                    //    if (calculation.calculation != null && !calculation.isIncomeAdjChecked)
                    //        chkIncomeAdj.Enabled = false;



                    //    txtAdj.Style.Add("display", "none");
                    //    txtAdj.Style.Add("width", "100%");
                    //    txtAdj.Text = lbl.Text;
                    //    txtAdj.Attributes.Add("ondblclick", "showAdjLabel(this)");
                        

                    //    // attach double click when headers are not save only
                    //    if (calculation.calculation == null)
                    //        lbl.Attributes.Add("ondblclick", "showAdjTextbox(this)");
                        
                    //    lbl.Style.Add("display","block");
                    //    container.Controls.Add(txtAdj);

                    //    //don't add checkbox of Reterospect increment
                    //    if (header.ColumnType != CalculationColumnType.IncomeResterospect)
                    //    {
                    //        txtAdj.Attributes.Add("onKeyDown", "return adjustmentText(event,this)");
                    //        container.Controls.Add(chkIncomeAdj);
                    //    }

                        
                    //}
                    ((System.Web.UI.WebControls.WebControl)(container)).Width = new Unit(25);


                    break;
                case DataControlRowType.DataRow:

                    TextBox txt = new TextBox();
                 
                    HiddenField hiddenAdjustmentComment = null;
                    Image imgComment = null;

                    txt.ID = "txt" + GetIDPostfix(header);// header.Type + header.SourceId;
                    txt.Style.Add("text-align", "right");
                    // key movement event handler
                    txt.Attributes.Add("onKeyDown", "return processKey(event,this);");
                    txt.Width = new Unit(Calculation.ColumnWidth);
                    txt.DataBinding += new EventHandler(Text_IncomeBinding);
                    txt.AutoCompleteType = AutoCompleteType.None;
                    txt.CssClass = "calculationInput";

                    ////if total column then bold the textbox
                    //if (header.ColumnType == CalculationColumnType.DeductionTotal ||
                    //    header.ColumnType == CalculationColumnType.NetSalary ||
                    //    header.ColumnType == CalculationColumnType.IncomeGross)
                    //{
                    //    txt.Font.Bold = true;
                    //}

                    //if (CalculationManager.IsAmountChangable(header))
                    //{
                    //    txt.Attributes.Add("onblur", "isValidAmount(this,this.value,this.getAttribute('amount'),event);return false;");
                    //    txt.Attributes.Add("onfocus", "selectAllTextCalculation(this);");
                    //    //add taxable attributes if the variable type is taxable to request for server when values changes
                    //    if( CalculationManager.IsTypeTaxable(header))
                    //        txt.Attributes.Add("taxable", "true");
                    //}
                    //else 
                    //if (header.ColumnType == CalculationColumnType.IncomeAdjustment ||
                    //    header.ColumnType == CalculationColumnType.DeductionAdjustment)
                    //{

                    //    txt.Attributes.Add("onfocus", "selectAllText(this);");

                    //    //if edit mode & this Adjustment are editable then don't disable TextBox
                    //    if (((this.calculation.calculation != null && this.calculation.calculation.HasAdjustmentIncome)
                    //    || calculation.isIncomeAdjChecked
                    //         )
                    //        && header.ColumnType == CalculationColumnType.IncomeAdjustment) ;

                    //    else if (((this.calculation.calculation != null && this.calculation.calculation.HasAdjustmentDeduction.Value)
                    //        || calculation.isDeductionAdjChecked
                    //        )
                    //        && header.ColumnType == CalculationColumnType.DeductionAdjustment) ;
                    //    else
                    //    {
                    //        txt.ReadOnly = true;
                    //        txt.CssClass += " calculationInputDisabled";
                    //    }

                    //    //add taxable attributes if the variable type is taxable to request for server when values changes
                    //    if (header.ColumnType == CalculationColumnType.IncomeAdjustment)
                    //        txt.Attributes.Add("taxable", "true");
                    
                        

                    //    txt.Attributes.Add("onblur", "isValidAmount(this,this.value,this.getAttribute('amount'),event)");
                    //    txt.Attributes.Add("ondblclick", "addComment(this)");
                    //    hiddenAdjustmentComment = new HiddenField();

                    //    if (header.ColumnType == CalculationColumnType.IncomeAdjustment)
                    //    {
                    //        hiddenAdjustmentComment.ID = "adjIncome";
                    //    }
                    //    else
                    //    {
                    //        hiddenAdjustmentComment.ID = "adjDeduction";
                    //    }
                    //    hiddenAdjustmentComment.Value = string.Empty;
                    //}
                    //else
                    {
                        txt.ReadOnly = true;
                        txt.CssClass += " calculationInputDisabled";
                    }

                    // add "adjustment" class if Adjustment column
                    //if (header.ColumnType == CalculationColumnType.IncomeAdjustment)
                    //{
                    //    txt.CssClass += " adjustmentIncome";
                    //}
                    //else if (header.ColumnType == CalculationColumnType.DeductionAdjustment)
                    //{
                    //    txt.CssClass += " adjustmentDeduction";
                    //}


                    container.Controls.Add(txt);

                    if (hiddenAdjustmentComment != null)
                    {
                        container.Controls.Add(hiddenAdjustmentComment);
                    }

                    break;
                default:
                    break;
            }
        }

        public void Text_IncomeBinding(object sender, EventArgs e)
        {
            GridViewRow row = ((GridViewRow) (((Control) (sender)).NamingContainer));
            //HiddenField hfList = (HiddenField) row.FindControl("hfList");
            CalcGetCalculationListResult dataSource = (CalcGetCalculationListResult) row.DataItem;
            TextBox txt = (TextBox) sender;
            decimal? amount = null;

            //change textbox background color checking the row index to make it white as default is gray
            if (row.RowIndex % 2 == 1)
            {
                txt.Style["background-color"] = "#FFFFFF!important";
            }

            txt.ToolTip = dataSource.EmployeeId + " : " + dataSource.Name;
            //add column type & source id to be used in client side
            txt.Attributes.Add("columnType", header.Type.ToString());
            txt.Attributes.Add("sourceId", header.SourceId.ToString());
            txt.Attributes.Add("empId", dataSource.EmployeeId.Value.ToString());
           
           

            // Get Amount for the cell
            amount = dataSource.GetCellValue(header.Type.Value, header.SourceId.Value,SessionManager.DecimalPlaces,this.calculation.headerList
                ,ref isHeaderMissingError,ref missingType,ref missingSourceId);

            // Check if column value is valid
            CalculationValue calculationValue = dataSource.GetCalculationValue(header.Type.Value, header.SourceId.Value);
            if (calculationValue != null && calculationValue.IsError)
            {
                MarkTextboxInvalid(txt, calculationValue.ErrorMessage);
                if(amount != null)
                    txt.Text = Utils.Helper.Util.FormatAmountWithProperDecimal(amount.Value,
                                                                              SessionManager.DecimalPlaces);
                calculation.isValidAllValid = false;
                return;
            }

            if (amount != null && amount <= -1)
            {
                
                // For Net salary like being -ve, but allowed if retiring/resining as can be -ve due to loan/adv
                if (amount < -1)
                {
                    txt.Text = Utils.Helper.Util.FormatAmountWithProperDecimal(amount.Value,
                                                                               SessionManager.DecimalPlaces);
                    if (dataSource.IsRetiredOrResigned.Value == 1)
                        txt.ToolTip = Resources.Messages.CalcNegativeAmountDueToLoanMsg;
                    else
                        txt.ToolTip = Resources.Messages.CalcNegativeAmountMsg;

                }
                //For NULL/IsValid = false case only msg is enough
                else if (amount == -1)
                {
                    txt.ToolTip = Resources.Messages.CalcNullAmountMsg;
                }

                CalculationManager.MakeReadonly(txt);
                MarkTextboxInvalid(txt);
                //txt.Style.Add("background-color", "red");
               
               
                // if retired or if tds column then neglect
                if (dataSource.IsRetiredOrResigned.Value == 1
                    || header.ColumnType == CalculationColumnType.TDS
                    //|| header.ColumnType == CalculationColumnType.IncomeResterospect
                    //|| ( header.ColumnType==CalculationColumnType.NetSalary && dataSource.IsRetrospectIncrementNegative(this.calculation.headerList)  )
                    //|| ( header.ColumnType==CalculationColumnType.IncomeGross && dataSource.IsRetrospectIncrementNegative(this.calculation.headerList) )
                    || (header.ColumnType ==CalculationColumnType.Income && header.SourceId!=basicIncomeId) // Allow for income other than Basic
                    )
                    ;
                     //if retired & IsValid=false for income/deduction then mark with red
                else if( dataSource.IsRetiredOrResigned.Value == 1 && ( header.ColumnType == CalculationColumnType.Income || header.ColumnType==CalculationColumnType.Deduction))
                {
                    calculation.isValidAllValid = false;
                }
                else //if (dataSource.IsRetiredOrResigned.Value == 0)
                    calculation.isValidAllValid = false;

                return;

            }

            // Get the Tooltip help for the cell if of adjustment type
            if (
                header.ColumnType ==CalculationColumnType.Income ||
                //header.ColumnType == CalculationColumnType.IncomeInsurance ||
                header.ColumnType == CalculationColumnType.DeductionInsurance)
            {
                //Add SpecialEvent id in the Hidden field to be passed while saving
                txt.Attributes.Add("CalculationIncluded", dataSource.GetList(header.Type.Value, header.SourceId.Value));
                //hfList.Value = dataSource.GetList(header.Type.Value, header.SourceId.Value);
            }

            txt.Text = amount == null
                           ? Resources.Messages.EmptySalaryAmountText
                           : Utils.Helper.Util.FormatAmountWithProperDecimal(amount.Value, SessionManager.DecimalPlaces);

            // if no amount value in case if this income is not for the employee or already saved
            // if already final saved
            // if not edit mode, if row is not in edit mode
            if (amount == null || dataSource.IsFinalSaved == 1 || (dataSource.CalculationId != 0 && calculation.editMode == false))
            {
                CalculationManager.MakeReadonly(txt);
            }
            //    // if not edit mode, if row is not in edit mode
            //else if (dataSource.CalculationId != 0 && calculation.editMode == false)
            //    //&& calculation.editingId != dataSource.EmployeeId )
            //{
            //    MakeReadonly(txt);
            //}

            // set amount in javascript attribute also to reverse if invalid placed
            txt.Attributes.Add("amount", txt.Text);

        }

        public static void MarkTextboxInvalid(TextBox txt)
        {
            txt.Style["border"] = "1px solid red!important";
            //txt.BorderColor = Color.Red;
            //txt.BorderWidth = new Unit(1);
        }
        public static void MarkTextboxInvalid(TextBox txt,string errorMessage)
        {
            txt.Style["border"] = "1px solid red!important";
            txt.ToolTip = errorMessage;
            //txt.BorderColor = Color.Red;
            //txt.BorderWidth = new Unit(1);
        }
    }


}
