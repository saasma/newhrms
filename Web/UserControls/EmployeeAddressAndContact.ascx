﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployeeAddressAndContact.ascx.cs"
    Inherits="Web.UserControls.EmployeeAddressAndContact" %>
<script type="text/javascript">

    function disableDistrictIfNotNepal(ddlCountry) {
        var text = ddlCountry.options[ddlCountry.selectedIndex].text;
        if (text != "Nepal") {
            disableElement('<%= ddlZones.ClientID %>');
            disableElement('<%= ddlDistricts.ClientID %>');
        }
        else {
            enableElement('<%= ddlZones.ClientID %>');
            enableElement('<%= ddlDistricts.ClientID %>');
        }
    }
</script>
<%--    <fieldset class="field_set">--%>
<div class="sepeartorBg">
    <div class="employeSector" style='width: 280px'>
        <h2>
            Present Address</h2>
        <div class="input_box">
            <label class="emplLbl" style='width: 80px'>
                Locality
            </label>
            <asp:TextBox TextMode="MultiLine" Height="50px" Width="187px" ID="txtPresentLocality"
                runat="server"></asp:TextBox>
        </div>
        <div class="input_box">
            <label class="emplLbl" style='width: 80px'>
                Zones
            </label>
            <asp:DropDownList onchange="onChangeZone(this)" Width="187px" ID="ddlPresentZones"
                AppendDataBoundItems="True" DataTextField="Zone" DataValueField="ZoneId" runat="server"
                CssClass="districtRegion">
                <asp:ListItem Value="-1" Text="--Select zone--"></asp:ListItem>
            </asp:DropDownList>
        </div>
        <div class="input_box">
            <label class="emplLbl" style='width: 80px'>
                District
            </label>
            <asp:DropDownList ID="ddlPresentDistricts" Width="187px" AppendDataBoundItems="True"
                runat="server" DataValueField="DistrictId" DataTextField="District" CssClass="districtRegion">
                <asp:ListItem Value="-1" Selected="True" Text="--Select district--"></asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>
    <%--</fieldset>--%>
    <%--<fieldset class="field_set">--%>
    <div class="employeSector" style='width: 310px'>
        <h2>
            Official Contact Information</h2>
        <div class="input_box">
            <label class="tab_label">
                Email Address
            </label>
            <asp:TextBox ID="txtEmailaddress" runat="server" Width="187px"></asp:TextBox>
            <asp:RegularExpressionValidator ID="valEmail0" runat="server" ControlToValidate="txtEmailaddress"
                Display="None" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                ErrorMessage="Invalid Email." ValidationGroup="AEEmployee"></asp:RegularExpressionValidator>
        </div>
        <div class="input_box">
            <label class="tab_label">
                Phone No
            </label>
            <asp:TextBox ID="txtPhone" Width="187px" runat="server"></asp:TextBox>
        </div>
        <div class="input_box">
            <label class="tab_label">
                Extension
            </label>
            <asp:TextBox ID="txtExtension" Width="187px" runat="server"></asp:TextBox>
        </div>
        <%-- <div class="input_box">
        <label class="tab_label">
            Mobile No
        </label>
        <asp:TextBox ID="txtMobileNo" Width="187px" runat="server"></asp:TextBox>
    </div>
    <div class="input_box">
        <label class="tab_label">
            Emergency No
        </label>
        <asp:TextBox ID="txtEmergency" Width="187px" runat="server"></asp:TextBox>
    </div>--%>
        <h2>
            Personal Contact Information</h2>
        <div class="input_box">
            <label class="tab_label">
                Email Address
            </label>
            <asp:TextBox ID="txtPersonalEmail" runat="server" Width="187px"></asp:TextBox>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator0" runat="server" ControlToValidate="txtPersonalEmail"
                Display="None" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                ErrorMessage="Invalid Email." ValidationGroup="AEEmployee"></asp:RegularExpressionValidator>
        </div>
        <div class="input_box">
            <label class="tab_label">
                Mobile No
            </label>
            <asp:TextBox ID="txtMobileNo" Width="187px" runat="server"></asp:TextBox>
        </div>
        <div class="input_box">
            <label class="tab_label">
                Phone No
            </label>
            <asp:TextBox ID="txtPersionalPhone" Width="187px" runat="server"></asp:TextBox>
        </div>
        <h2>
            Emergency Contact Information</h2>
        <div class="input_box">
            <label class="tab_label">
                Name
            </label>
            <asp:TextBox ID="txtEmergencyName" runat="server" Width="187px"></asp:TextBox>
        </div>
        <div class="input_box">
            <label class="tab_label">
                Relation
            </label>
            <asp:TextBox ID="txtEmergencyRelation" runat="server" Width="187px"></asp:TextBox>
        </div>
        <div class="input_box">
            <label class="tab_label">
                Mobile No
            </label>
            <asp:TextBox ID="txtEmergencyMobile" Width="187px" runat="server"></asp:TextBox>
        </div>
        <div class="input_box">
            <label class="tab_label">
                Phone No
            </label>
            <asp:TextBox ID="txtEmergency" Width="187px" runat="server"></asp:TextBox>
        </div>
    </div>
    <div class="employeSector" style='width: 310px'>
        <%--<fieldset class="field_set">--%>
        <h2>
            Permanent address</h2>
        <div class="input_box">
            <label class="tab_label" style='width: 80px'>
                Country
            </label>
            <asp:DropDownList onchange="disableDistrictIfNotNepal(this)" ID="ddlCountries" runat="server"
                AppendDataBoundItems="True" DataValueField="CountryId" DataTextField="CountryName"
                Width="190px">
                <asp:ListItem Value="-1" Text="--Select country--" Selected="True"></asp:ListItem>
            </asp:DropDownList>
        </div>
        <div class="input_box">
            <label class="tab_label" style='width: 80px'>
                Locality
            </label>
            <asp:TextBox ID="txtLocality" TextMode="MultiLine" Height="50px" Width="187px" runat="server"></asp:TextBox>
        </div>
        <div class="input_box">
            <label class="tab_label" style='width: 80px'>
                Zones
            </label>
            <asp:DropDownList onchange="onChangeZone(this)" Width="190px" DataValueField="ZoneId"
                AppendDataBoundItems="True" DataTextField="Zone" ID="ddlZones" runat="server"
                CssClass="districtRegion">
                <asp:ListItem Value="-1" Text="--Select zone--" Selected="True"></asp:ListItem>
            </asp:DropDownList>
        </div>
        <div class="input_box">
            <label class="tab_label" style='width: 80px'>
                District
            </label>
            <asp:DropDownList ID="ddlDistricts" runat="server" AppendDataBoundItems="True" Width="190px"
                DataValueField="DistrictId" DataTextField="District" CssClass="districtRegion">
                <asp:ListItem Value="-1" Selected="True" Text="--Select district--"></asp:ListItem>
            </asp:DropDownList>
        </div>
        <%--</fieldset>
                           
                           <fieldset  class="field_set">--%>
        <div class="input_box">
            <label class="tab_label" style='width: 80px'>
                House No
            </label>
            <asp:TextBox ID="txtHouseNo" Width="187px" runat="server" />
        </div>
        <div class="input_box">
            <label class="tab_label" style='width: 80px'>
                Street
            </label>
            <asp:TextBox ID="txtStreet" Width="187px" runat="server" />
        </div>
        <div class="input_box">
            <label class="tab_label" style='width: 80px'>
                State
            </label>
            <asp:TextBox ID="txtState" Width="187px" runat="server" />
        </div>
        <div class="input_box">
            <label class="tab_label" style='width: 80px'>
                Zip Code
            </label>
            <asp:TextBox ID="txtZipCode" Width="187px" runat="server" />
        </div>
    </div>
</div>
