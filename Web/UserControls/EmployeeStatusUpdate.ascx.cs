﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL;
using BLL.Manager;
using DAL;
using Utils.Calendar;
using Utils.Helper;

namespace Web.UserControls
{
    public partial class EmployeeStatusUpdate : BaseUserControl
    {
        EmployeeManager empMgr = new EmployeeManager();

        protected void Page_Load(object sender, EventArgs e)
        {
            JavascriptHelper.AttachPopUpCode(Page, "promote", "ChangeEmployeeStatus.aspx", 480, 350);


            if (!IsPostBack)
            {
                //calFrom.IsEnglishCalendar = SessionManager.CurrentCompany.IsEnglishDate;
                //calFrom.SelectTodayDate();
            }
            Initialise();

            //JavascriptHelper.AttachEnableDisablingJSCode(chkChange, tbl.ClientID , false,"enableChangeCallback");
            //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "values",
            //    string.Format("isDownGradeEnable = {0};", CommonManager.CompanySetting.IsStatusDownGradeEnable.ToString().ToLower()),true);

           

            SetPagePermission();
        }

        /// <summary>
        /// Set for Custom Role if is in Readonly Mode or Not
        /// </summary>
        public void SetPagePermission()
        {
            if (SessionManager.IsCustomRole && SessionManager.CustomRole.IsReadOnly != null && SessionManager.CustomRole.IsReadOnly.Value)
            {
                //btnSaveStatus.Visible = false;
                btnPromote.Visible = false;
            }
        }
        void Initialise()
        {
            LoadEmployeeStatus();
        }

        public void ProcessControl(EHumanResource empHR,int employeeId)
        {
            if (empHR == null)// || empHR.JoiningStatus == null)
            {
                this.Visible = false;
            }
            else
            {
                this.Visible = true;

                empStatusList.InnerHtml = empMgr.GetCurrentStatus(employeeId);

                ECurrentStatus lastStatus = empMgr.GetCurrentLastStatus(employeeId);

                //CustomDate date = CustomDate.GetCustomDateFromString(lastStatus.ToDate,
                //                                                     SessionManager.CurrentCompany.IsEnglishDate);

                CustomDate date = CustomDate.GetCustomDateFromString(lastStatus.FromDate,
                                                                     SessionManager.CurrentCompany.IsEnglishDate);

                date = date.IncrementByOneDay();
                //lblFrom.Text = date.ToStringMonthEng().Replace("-", "/"); 
                //hdnFromDate.Value = date.ToString();

                //calFrom.SetSelectedDate(date.ToString(), SessionManager.CurrentCompany.IsEnglishDate);

                //HidePreviousStatus(lastStatus.CurrentStatus.ToString());

                bool isFirstStatusOnly = (empMgr.GetStatusCount(employeeId) == 1);
                
                            

                Page.ClientScript.RegisterClientScriptBlock(this.GetType(),
                                                            "empStatus",
                                                            "var isFirst = " + (isFirstStatusOnly ? "true" : "false") + ";",
                
                                                            true);
                if(!isFirstStatusOnly)
                {
                    //chkChange.Checked = true;
                    //chkChange.Enabled = false;
                }
            }
        }


        private void LoadEmployeeStatus()
        {
            //BizHelper.Load(new JobStatus(), ddlStatus);
            //ddlStatus.Items.AddRange(BizHelper.GetMembersAsListItem(new JobStatus()));            
        }

        //public void HidePreviousStatus(string currentStatus)
        //{

        //    if (!string.IsNullOrEmpty(currentStatus))
        //    {
        //        //now no status exists so disable change
        //        //if (currentStatus == JobStatus.PERMANENT)
        //        //{
        //        //    btnSaveStatus.Visible = false;
        //        //    ddlStatus.Visible = false;
        //        //    valReqdName2.Visible = false;
        //        //    calStatusDate.Visible = false;
        //        //    return;
        //        //}

        //        if (CommonManager.CompanySetting.IsStatusDownGradeEnable == false)
        //        {

        //            for (int i = ddlStatus.Items.Count - 1; i >= 0; i--)
        //            {
        //                ListItem item = ddlStatus.Items[i];
        //                if (item.Value.Equals(currentStatus))
        //                {
        //                    //remove all previous status;
        //                    for (int j = i - 1; j > 0; j--)
        //                    {
        //                        ddlStatus.Items.RemoveAt(j);
        //                    }
        //                    break;
        //                }
        //            }
        //        }
        //        else
        //        {
        //            ddlStatus.Items.RemoveAt(0);// remove single All Employee status only
        //        }
        //    }
        //}

        //protected void Page_PreRender(object sender, EventArgs e)
        //{
        //    if(!chkChange.Checked)
        //    {
        //        ddlStatus.Enabled = false;
        //        //lblFrom.Enabled = false;
        //        calFrom.Enabled = false;
        //    }
        //}
    }
}