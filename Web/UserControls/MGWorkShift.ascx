﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MGWorkShift.ascx.cs"
    Inherits="Web.UserControls.MGWorkShift" %>

<script type="text/javascript">
    function ConfirmLibraryTopicDeletion() {
        var ddllist = document.getElementById('<%= ddlCostCodes.ClientID %>');
        if (ddllist.selectedIndex == -1) {
            alert('No Work shift to delete.');
            return false;
        }
        if (confirm('Do you want to delete the Work shift?')) {
            clearUnload();
            return true;
        }
        else {
            return false;
        }
    }

    function DisplayLibraryTopicInTextBox(dropdown_id) {
        var ddllist = document.getElementById(dropdown_id);
        document.getElementById('<%= txtCostCodeRename.ClientID %>').value = ddllist.options[ddllist.selectedIndex].text;
    }

    //capture window closing event
    window.onunload = closePopup;
    function closePopup() {
        clearUnload();
        // alert(window.opener.parentReloadCallbackFunction)
        if ($.browser.msie == false && typeof (window.opener.parentReloadCallbackFunction) != 'undefined' && ($.browser.chrome || $.browser.safari)) {
            window.opener.parentReloadCallbackFunction("ReloadWorkshift", window, texts);
        } else {
            if (typeof (texts) != 'undefined')
                window.returnValue = texts;
            window.close();
        }
    }

    function clearUnload() {
        window.onunload = null;
    }
       
</script>

<div class="popupHeader">
    <h2 class="headlinespop">
        Manage Work shift</h2>
</div>
<div class="marginal">
    <div class="bevel ">
        <div class="fields paddpop"  style="padding: 10px 0 20px 10px !important;width:390px;">
            <table cellpadding="0" cellspacing="0" class="alignttable" width="600px">
                <tr>
                    <td class="lf" width="120px">
                        New Work shift
                    </td>
                    <td>
                        <asp:TextBox ID="txtNewCostCode" runat="server" Width="161px" MaxLength="100" EnableViewState="false"></asp:TextBox>
                        <asp:RequiredFieldValidator ValidationGroup="Deposit1" ID="RequiredFieldValidator3"
                            Display="None" runat="server" ErrorMessage="Work shift name is required." ControlToValidate="txtNewCostCode">*</asp:RequiredFieldValidator>
                        <asp:Button ID="btnAddDepsoit" OnClientClick="valGroup='Deposit1';return CheckValidation()"
                            runat="server" Text="Add" OnClick="btnAddDepsoit_Click" CssClass="update" Style="margin: 0px 0 0 5px" />
                    </td>
                </tr>
                <tr>
                    <td class="lf" width="120px">
                        Rename Work shift
                    </td>
                    <td>
                       <asp:TextBox ID="txtCostCodeRename" runat="server" Width="161px" MaxLength="100"></asp:TextBox>
                        <asp:RequiredFieldValidator ValidationGroup="Deposit2" ID="RequiredFieldValidator4"
                            runat="server" ErrorMessage="Work shift name is required." ControlToValidate="txtCostCodeRename"
                            Display="None">*</asp:RequiredFieldValidator>
                                                <asp:Button ID="btnRename" OnClientClick="valGroup='Deposit2';return CheckValidation()"
                            runat="server" Text="Update" ValidationGroup="RenameLibraryTopic" CssClass="save"
                            Style="margin: 0px 0 0 5px" OnClick="btnRename_Click" />
                    </td>
                </tr>
                <tr>
                    <td class="lf" width="120px">
                        Work shift List
                    </td>
                    <td>
                     <asp:DropDownList ID="ddlCostCodes" runat="server" Width="165px" CssClass="rightMargin"
                            DataValueField="WorkShiftId" DataTextField="Name" onchange="DisplayLibraryTopicInTextBox(this.id);">
                        </asp:DropDownList>
                        
                            <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="delete" Style="margin: 0px 0 0 5px"
                            OnClientClick="javascript:return ConfirmLibraryTopicDeletion();" OnClick="btnDelete_Click" />

                    </td>
                </tr>
            </table>
        </div>
    </div>
