﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Manager;
using Utils.Helper;
using BLL;
using DAL;
using Utils.Web;
using System.Text;

namespace Web.UserControls
{
    public partial class ManageBankBranch : System.Web.UI.UserControl
    {
        CommonManager commonMgr = new CommonManager();
        List<BankBranch> source = null;
        public bool IsDisplayedAsPopup
        {
            get
            {
                if (Request.QueryString["isPopup"] != null)
                    return true;
                return false;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                Initialise();
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "clearUnload();");
        }

        void Initialise()
        {
            ddlBank.DataSource = PayManager.GetBankList();
            ddlBank.DataBind();

            LoadDesignations();
           
        }

        void LoadDesignations()
        {
            source = PayManager.GetBankBranchList();
            gvwBranches.DataSource = source;
            gvwBranches.DataBind();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            int desigID = 0;
              if (gvwBranches.SelectedIndex != -1)
                  desigID = (int)gvwBranches.DataKeys[gvwBranches.SelectedIndex][0];
              

            if (Page.IsValid)
            {
                BankBranch entity = new BankBranch();

                if (gvwBranches.SelectedIndex == -1)
                {
                    entity.BankID = int.Parse(ddlBank.SelectedValue);
                    entity.Name = txtName.Text.Trim();
                    
                    commonMgr.Save(entity);

                    divMsgCtl.InnerHtml = "Saved";

                }
                else
                {
                    entity.Name = txtName.Text.Trim();
                    entity.BankBranchID = (int)gvwBranches.DataKeys[gvwBranches.SelectedIndex][0];

                

                    commonMgr.Update(entity);

                    divMsgCtl.InnerHtml = "Updated";
                }

                divMsgCtl.Hide = false;

                CommonManager.ResetCache();
                gvwBranches.SelectedIndex = -1;
                LoadDesignations();

                ClearFields();
                
            }
        }

        protected void gvwBranches_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvwBranches.PageIndex = e.NewPageIndex;
            gvwBranches.SelectedIndex = -1;
            LoadDesignations();
            ClearFields();

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            gvwBranches.SelectedIndex = -1;
            LoadDesignations();
            ClearFields();
            details.Visible = false;
        }

        protected void gvwBranches_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            CommonManager.ResetCache();
            int id = (int)gvwBranches.DataKeys[e.RowIndex][0];
            //if (id != 0)
            //{
            //    Bank entity = new Bank();
            //    entity.BankID = id;

            //    if (commonMgr.Delete(entity))
            //    {
            //        divMsgCtl.InnerHtml = "Bank deleted.";
            //        LoadDesignations();
            //        ClearFields();
            //    }
            //    else
            //    {
            //        divMsgCtl.InnerHtml = "Bank is in use.";

            //    }
            //    divMsgCtl.Hide = false;

            //}
           
        }

        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            LoadDesignations();
            ClearFields();
            details.Visible = true;
            txtName.Focus();
        }

        protected void gvwBranches_SelectedIndexChanged(object sender, EventArgs e)
        {
            int bankId = (int)gvwBranches.DataKeys[gvwBranches.SelectedIndex][0];
            BankBranch branch = commonMgr.GetBankBranchById(bankId);
            if (branch != null)
            {
                txtName.Text = branch.Name;
                UIHelper.SetSelectedInDropDown(ddlBank, branch.BankID);
              
                btnSave.Text = Resources.Messages.Update;
                details.Visible = true;
                txtName.Focus();
            }
        }

        void ClearFields()
        {
            txtName.Text = "";
        
        }

        
        protected void Page_PreRender(object sender, EventArgs e)
        {
            //output all as js array to be updatable in parent window
            if (IsDisplayedAsPopup && source != null)
            {
                
            }
        }
        
    }
}