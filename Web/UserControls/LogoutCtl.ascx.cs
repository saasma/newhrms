﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using BLL;

namespace Web.UserControls
{
    public partial class LogoutCtl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnLogout_Click(object sender, EventArgs e)
        {
            int currentLoggedInEmployeeId = SessionManager.CurrentLoggedInEmployeeId;
            Session.Abandon();
            FormsAuthentication.SignOut();
            if (currentLoggedInEmployeeId == 0)           
                Response.Redirect("~/Default.aspx");
            else
                Response.Redirect("~/Employee/Default.aspx");

        }
    }
}