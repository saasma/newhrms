﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Manager;
using Utils.Helper;
using BLL;

namespace Web.UserControls
{
    public partial class MGLeave : System.Web.UI.UserControl
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
            JavascriptHelper.AttachPopUpCode(Page, "popupLeave", "LeaveList.aspx", 480, 450);
            JavascriptHelper.AttachPopUpCode(Page, "popupUpdateLeave", "AELeave.aspx", 600, 720);
            SetPagePermission();
        }

        /// <summary>
        /// Set for Custom Role if is in Readonly Mode or Not
        /// </summary>
        public void SetPagePermission()
        {
            if (SessionManager.IsCustomRole && SessionManager.CustomRole.IsReadOnly != null && SessionManager.CustomRole.IsReadOnly.Value)
            {
                btnAddLeave.Visible = false;
            }
        }
        public void DisplayLeave(int employeeId)
        {
            LeaveAttendanceManager mgr = new LeaveAttendanceManager();
            leaves.InnerHtml = mgr.GetHTMLEmployeeLeaveList(employeeId);
            btnAddLeave.Enabled = true;
        }
        public void EnableButton()
        {
            btnAddLeave.Enabled = true;
        }
    }
}