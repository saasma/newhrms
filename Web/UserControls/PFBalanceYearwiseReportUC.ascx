﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PFBalanceYearwiseReportUC.ascx.cs"
    Inherits="Web.UserControls.PFBalanceYearwiseReportUC" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<script type="text/javascript">

    //capture window closing event, must be placed as close to return values
    <%
    if( IsDisplayedFromPopup)
    {
    Response.Write("window.onunload = closePopup;");
    }
     %>
    
    

    function closePopup() {
        clearUnload();
         // alert(window.opener.parentReloadCallbackFunction)
        if ($.browser.msie == false && typeof (window.opener.parentReloadCallbackFunction) != 'undefined' && ($.browser.chrome ||  $.browser.safari)) {
            window.opener.parentReloadCallbackFunction("ReloadDepartment", window, texts);
        } else {
            if (typeof (texts) != 'undefined')
                window.returnValue = texts;
            window.close();
        }
        
    }

    function clearUnload() {
        window.onunload = null;
    }
    
      function handleDelete()
       {            
            if(confirm('Do you want to delete department?'))
            {
                clearUnload(); 
                return true;
            }
            else
                return false;
       }
</script>
<div class="popupHeader" runat="server" id="div">
    <h2 class="headlinespop">
        Manage Department</h2>
</div>
<%--<asp:HiddenField ID="hiddenEmployee" runat="server" />--%>
<div class="marginal" style='margin-top: 0px'>
    <div class="contentArea">
        <%--<h2>Department Information</h2>--%>
        <label class="" style='margin-right: 10px; display: none'>
            Branch</label>
        <asp:DropDownList Style='margin-bottom: 15px; display: none' onchange="clearUnload()"
            AppendDataBoundItems="true" ID="ddlBranch" runat="server" DataTextField="Name"
            DataValueField="BranchId" AutoPostBack="true" OnSelectedIndexChanged="ddlBranch_SelectedIndexChanged"
            Width="180px">
            <asp:ListItem Text="--Select Branch--" Value="-1"></asp:ListItem>
        </asp:DropDownList>

        <div class="buttonsDiv" style='clear: both; width: 100%!important;'>
            <asp:DropDownList AutoPostBack="false" Width="150" ID="ddlYears" runat="server" DataValueField="FinancialDateId"
                DataTextField="Name"  AppendDataBoundItems="true">
            </asp:DropDownList>
            <asp:RequiredFieldValidator ValidationGroup="Inc" ID="RequiredFieldValidator8" Display="None"
                runat="server" InitialValue="-1" ControlToValidate="ddlYears" ErrorMessage="Year is required."></asp:RequiredFieldValidator>
    
            <%--OnSelectedIndexChanged="ddlYears_Changed"--%>

            <asp:LinkButton ID="btnAddNew" runat="server" Style='margin-left: 10px; width: 120px; height:30px;'
                CssClass="btn btn-default btn-sect btn-sm" Text="Search" OnClick="btnAddNew_Click" />
        </div>

        <div style="width: 100%;  overflow: scroll">
        <asp:GridView ID="gvwDepartments" CssClass="tableLightColor" PagerStyle-HorizontalAlign="Center" 
            PagerStyle-CssClass="defaultPagingBar" runat="server" DataKeyNames="EmployeeId" AutoGenerateColumns="False"
            CellPadding="0" CellSpacing="0" GridLines="None" Width="100%" PageSize="100"  style=" overflow:auto;"
            AllowPaging="True" OnRowCreated="gvwEmployees_RowCreated" OnRowDeleting="gvwDepartments_RowDeleting"
            OnSelectedIndexChanged="gvwDepartments_SelectedIndexChanged" OnPageIndexChanging="gvwDepartments_PageIndexChanging">
            <Columns>
                <%--<asp:TemplateField HeaderStyle-Width="20" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                    HeaderText="SN">
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    <ItemTemplate>
                        <%# Container.DisplayIndex + 1 %>
                    </ItemTemplate>
                </asp:TemplateField>--%>
                
                <asp:BoundField HeaderStyle-Width="50" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                    DataField="EmployeeId" HeaderText="EIN">
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundField>

                <asp:BoundField HeaderStyle-Width="200" ItemStyle-Width="200" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                    DataField="EmpName" HeaderText="Name">
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundField>
                <%-- <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="100px" HeaderText="Department Head"
                    ItemStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <%# Eval("HeadEmployee") %>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left" Width="100px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:TemplateField>--%>

                <%--<asp:BoundField HeaderStyle-Width="90px" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                    DataField="PFOpening" HeaderText="Opening Balance Contribution">
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundField>

                <asp:BoundField HeaderStyle-Width="90px" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                    DataField="PFOpeningInterest" HeaderText="Opening Balance Interest">
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundField>--%>

                <asp:TemplateField HeaderText="Opening Balance Contribution" ItemStyle-Width="125" ItemStyle-HorizontalAlign="Right" >
                    <ItemTemplate>
                        <%# GetAmountOrCurrency(Eval("PFOpening"))%>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Opening Balance Interest" ItemStyle-Width="125" ItemStyle-HorizontalAlign="Right" >
                    <ItemTemplate>
                        <%# GetAmountOrCurrency(Eval("PFOpeningInterest"))%>
                    </ItemTemplate>
                </asp:TemplateField>


                <asp:BoundField HeaderStyle-Width="125" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"
                    DataField="Month1" HeaderText="Shrawan">
                    <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                </asp:BoundField>

                <asp:BoundField HeaderStyle-Width="125" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"
                    DataField="Month2" HeaderText="Bhadra">
                    <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                </asp:BoundField>

                <asp:BoundField HeaderStyle-Width="90px" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"
                    DataField="Month3" HeaderText="Ashwin">
                    <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                </asp:BoundField>

                <asp:BoundField HeaderStyle-Width="90px" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"
                    DataField="Month4" HeaderText="Kartik">
                    <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                </asp:BoundField>

                <asp:BoundField HeaderStyle-Width="90px" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"
                    DataField="Month5" HeaderText="Mangsir">
                    <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                </asp:BoundField>

                <asp:BoundField HeaderStyle-Width="90px" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"
                    DataField="Month6" HeaderText="Poush">
                    <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                
                </asp:BoundField><asp:BoundField HeaderStyle-Width="90px" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"
                    DataField="Month7" HeaderText="Magh">
                    <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                </asp:BoundField>

                <asp:BoundField HeaderStyle-Width="90px" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"
                    DataField="Month8" HeaderText="Falgun">
                    <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                </asp:BoundField>

                <asp:BoundField HeaderStyle-Width="90px" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"
                    DataField="Month9" HeaderText="Chaitra">
                    <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                </asp:BoundField>

                <asp:BoundField HeaderStyle-Width="90px" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"
                    DataField="Month10" HeaderText="Baishakh">
                    <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                </asp:BoundField>

                <asp:BoundField HeaderStyle-Width="90px" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"
                    DataField="Month11" HeaderText="Jestha">
                    <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                </asp:BoundField>

                <asp:BoundField HeaderStyle-Width="90px" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"
                    DataField="Month12" HeaderText="Asadh">
                    <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                </asp:BoundField>

                

                <asp:BoundField HeaderStyle-Width="90px" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"
                    DataField="TotContribution" HeaderText="Total Contribution">
                    <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                </asp:BoundField>

                <asp:BoundField HeaderStyle-Width="90px" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"
                    DataField="InterestThisYear" HeaderText="Interest This Year">
                    <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                </asp:BoundField>

                <asp:BoundField HeaderStyle-Width="90px" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"
                    DataField="ClosingBalContribution" HeaderText="Closing Balance Contribution">
                    <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                </asp:BoundField>

                <asp:BoundField HeaderStyle-Width="90px" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"
                    DataField="ClosingBalInterest" HeaderText="Closing Balance Interest">
                    <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                </asp:BoundField>

                <asp:BoundField HeaderStyle-Width="90px" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"
                    DataField="TotBalance" HeaderText="Total Balance">
                    <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                </asp:BoundField>


                <%--<asp:TemplateField HeaderText="Edit" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="30px">
                    <ItemTemplate>
                        <asp:ImageButton ID="ImageButton1" runat="server" OnClientClick="clearUnload();"
                            CommandName="Select" ImageUrl="~/images/edit.gif" />
                    </ItemTemplate>
                    <HeaderStyle Width="30px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Delete" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="30px">
                    <ItemTemplate>
                        <asp:ImageButton ID="ImageButton2" OnClientClick="return handleDelete()" runat="server"
                            CommandName="Delete" ImageUrl="~/images/delet.png" />
                    </ItemTemplate>
                    <HeaderStyle Width="30px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:TemplateField>--%>
                <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EmptyDataTemplate>
                No Department has been created under this branch.
            </EmptyDataTemplate>
            <RowStyle CssClass="odd" />
            <AlternatingRowStyle CssClass="even" />
            <SelectedRowStyle CssClass="selected" />
        </asp:GridView>
        </div>
        <uc2:MsgCtl Width='<%# IsDisplayedFromPopup ? "600px" : "920px" %>' ID="divMsgCtl"
            EnableViewState="false" Hide="true" runat="server" />
        <uc2:WarningCtl Width='<%# IsDisplayedFromPopup ? "600px" : "920px" %>' ID="divWarningMsg"
            EnableViewState="false" Hide="true" runat="server" />
        <div id="details" runat="server" class="bevel margintops" visible="false">
            <div class="fields paddpop" style="padding: 10px 0pt 20px 10px ! important;">
                <h2 class="popstitle">
                    Department Information</h2>
                <table style="float: left; width: 400px" width="100%" cellspacing="6" cellpadding="6"
                    class="fieldTable firsttdskip">
                    <tr>
                        <td class="fieldHeader">
                            <My:Label ID="Label2" Text="Department Name" runat="server" ShowAstrick="true" />
                        </td>
                        <td>
                            <asp:TextBox ID="txtDepartmentName" runat="server" Width="180px" />
                            <asp:RequiredFieldValidator ID="valReqdName" runat="server" ControlToValidate="txtDepartmentName"
                                Display="None" ErrorMessage="Department name is required." ValidationGroup="AEDep"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td class="fieldHeader">
                            <My:Label ID="Label1" Text="Department Code" runat="server" ShowAstrick="false" />
                        </td>
                        <td>
                            <asp:TextBox ID="txtCode" runat="server" Width="180px" />
                        </td>
                    </tr>
                    <tr style="display: none">
                        <td class="fieldHeader">
                            Department Head
                        </td>
                        <td>
                            <ext:Store ID="storeDepartment" runat="server">
                                <Model>
                                    <ext:Model>
                                        <Fields>
                                            <ext:ModelField Name="Name" />
                                            <ext:ModelField Name="EmployeeId" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                            <ext:ComboBox ID="cmbEmployee" Width="180px" ForceSelection="false" runat="server"
                                StoreID="storeDepartment" Editable="true" DisplayField="Name" ValueField="EmployeeId"
                                Mode="Local" EmptyText="">
                                <Items>
                                    <ext:ListItem Value="-1" Text="&nbsp;" />
                                </Items>
                                <Listeners>
                                    <Focus Handler="if( this.getValue()=='-1' ) this.clearValue();" />
                                    <Select Handler="if( this.getValue()=='-1') this.clearValue(); " />
                                </Listeners>
                            </ext:ComboBox>
                            <%-- <asp:DropDownList ID="ddlDepHead" DataTextField="Name" Width="183px" DataValueField="EmployeeId"
                                runat="server">
                                <asp:ListItem Text="--Select head employee--" Value="-1"></asp:ListItem>
                            </asp:DropDownList>--%>
                        </td>
                    </tr>
                    <tr>
                        <td class="fieldHeader">
                            Description
                        </td>
                        <td>
                            <asp:TextBox ID="txtDesc" runat="server" Width="180px" TextMode="MultiLine" Columns="30"
                                Rows="6"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="fieldHeader">
                            Phone
                        </td>
                        <td>
                            <asp:TextBox ID="txtPhone" runat="server" Width="180px"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="valRegPhone" runat="server" ControlToValidate="txtPhone"
                                ErrorMessage="Phone number is invalid." ValidationGroup="AEDep" Display="None"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr runat="server" visible="false">
                        <td class="fieldHeader">
                            Fax
                        </td>
                        <td>
                            <asp:TextBox ID="txtFax" runat="server" Width="180px"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="valRegFax" runat="server" ControlToValidate="txtFax"
                                ErrorMessage="Fax is invalid." ValidationGroup="AEDep" Display="None"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="fieldHeader">
                            Email
                        </td>
                        <td>
                            <asp:TextBox ID="txtEmail" runat="server" Width="180px"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                ControlToValidate="txtEmail" ErrorMessage="Email is invalid." ValidationGroup="AEDep"
                                Display="None"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <asp:Button ID="btnSave" CssClass="update" OnClientClick="valGroup='AEDep';return CheckValidation()"
                                runat="server" Text="Save" ValidationGroup="AEDep" OnClick="btnSave_Click" />
                            <asp:Button ID="btnCancel" CssClass="cancel" OnClientClick="clearUnload()" runat="server"
                                Text="Cancel" OnClick="btnCancel_Click" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
