﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using Utils.Web;
using BLL.Manager;
using BLL;

namespace Web.UserControls
{
    public partial class EmployeeAddressAndContact : System.Web.UI.UserControl
    {
        CommonManager comm = new CommonManager();

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (ddlCountries.SelectedItem != null)
            {
                bool isPermanentAddressNepal = (ddlCountries.SelectedItem.Text == "Nepal");
                ddlZones.Enabled = isPermanentAddressNepal;
                ddlDistricts.Enabled = isPermanentAddressNepal;
            }
        }


        protected void Page_Init(object sender, EventArgs e)
        {
            //if(!IsPostBack)
            {
                Initialise();
            }
        }
        void Initialise()
        {
            ddlZones.DataSource = comm.GetAllZones();
            ddlZones.DataBind();
            ddlPresentZones.DataSource = comm.GetAllZones();
            ddlPresentZones.DataBind();

            ddlPresentDistricts.DataSource = comm.GetAllDistricts(int.Parse(ddlPresentZones.SelectedValue));
            ddlPresentDistricts.DataBind();

            ddlDistricts.DataSource = comm.GetAllDistricts(int.Parse(ddlZones.SelectedValue));
            ddlDistricts.DataBind();


            ddlCountries.DataSource = comm.GetAllCountries();
            ddlCountries.DataBind();

        


            LoadPresentDistrict();

            LoadPermanentDistrict();

            if (SessionManager.CurrentLoggedInEmployeeId != 0)
            {
                txtEmailaddress.ToolTip = "Not editable";
                txtEmailaddress.Enabled = false;
            }
        }

        private void LoadPermanentDistrict()
        {
            string zoneId = Request.Form[ddlZones.UniqueID];

            if (zoneId != null)
            {
                //string id1 = Request.Form[ddlPresentDistricts.UniqueID.Replace("$", "_")];
                ddlDistricts.DataSource = comm.GetAllDistricts(int.Parse(zoneId));
                ddlDistricts.DataBind();
                string districtId = Request.Form[ddlDistricts.UniqueID];
                if (districtId != null)
                    UIHelper.SetSelectedInDropDown(ddlDistricts, districtId);
            }
        }

        private void LoadPresentDistrict()
        {
            string zoneId = Request.Form[ddlPresentZones.UniqueID];

            if (zoneId != null)
            {
                //string id1 = Request.Form[ddlPresentDistricts.UniqueID.Replace("$", "_")];
                ddlPresentDistricts.DataSource = comm.GetAllDistricts(int.Parse( zoneId));
                ddlPresentDistricts.DataBind();
                string districtId = Request.Form[ddlPresentDistricts.UniqueID];
                if (districtId != null)
                    UIHelper.SetSelectedInDropDown(ddlPresentDistricts, districtId);
            }
        }

        public EAddress Process(EAddress empAddress)
        {
            if (empAddress == null)
            {
                 empAddress = new EAddress();

                //Present address
                empAddress.PSLocality = txtPresentLocality.Text.Trim();
                empAddress.PSDistrictId = int.Parse(ddlPresentDistricts.SelectedValue);
                empAddress.PSZoneId = int.Parse(ddlPresentZones.SelectedValue);

                //Official Contact information
                empAddress.CIEmail = txtEmailaddress.Text.Trim();
                empAddress.CIPhoneNo = txtPhone.Text.Trim();
                empAddress.Extension = txtExtension.Text.Trim();

                //Personal Contact
                empAddress.PersonalEmail = txtPersonalEmail.Text.Trim();
                empAddress.CIMobileNo = txtMobileNo.Text.Trim();
                empAddress.PersonalPhone = txtPersionalPhone.Text.Trim();
                
                //Emergency Contact
                empAddress.EmergencyName = txtEmergencyName.Text.Trim();
                empAddress.EmergencyRelation = txtEmergencyRelation.Text.Trim();
                empAddress.EmergencyMobile = txtEmergencyMobile.Text.Trim();
                empAddress.CIEmergencyNo = txtEmergency.Text.Trim();
                
               
                //Permananent address
                empAddress.PECountryId = int.Parse(ddlCountries.SelectedValue);
                empAddress.PELocality = txtLocality.Text.Trim();
                if (ddlCountries.SelectedItem.Text == "Nepal")
                {
                    empAddress.PEDistrictId = int.Parse(ddlDistricts.SelectedValue);
                    empAddress.PEZoneId = int.Parse(ddlZones.SelectedValue);
                }

                empAddress.PEHouseNo = txtHouseNo.Text.Trim();
                empAddress.PEStreet = txtStreet.Text.Trim();
                empAddress.PEState = txtState.Text.Trim();
                empAddress.PEZipCode = txtZipCode.Text.Trim();

                UIHelper.SetText(txtHouseNo, empAddress.PEHouseNo);
                UIHelper.SetText(txtStreet, empAddress.PEStreet);
                UIHelper.SetText(txtState, empAddress.PEState);
                UIHelper.SetText(txtZipCode, empAddress.PEZipCode);

                return empAddress;
            }
            else
            {
                //Present address
                txtPresentLocality.Text = empAddress.PSLocality;
                UIHelper.SetSelectedInDropDown(ddlPresentZones, empAddress.PSZoneId);
                if (ddlPresentZones.SelectedValue != "-1")
                {
                    ddlPresentDistricts.DataSource = comm.GetAllDistricts(int.Parse(ddlPresentZones.SelectedValue));
                    ddlPresentDistricts.DataBind();
                    UIHelper.SetSelectedInDropDown(ddlPresentDistricts, empAddress.PSDistrictId);
                }
                //Contact information
                //Official Contact information
                txtEmailaddress.Text = empAddress.CIEmail;
                txtPhone.Text = empAddress.CIPhoneNo;
                txtExtension.Text = empAddress.Extension;

                //Personal Contact
                txtPersonalEmail.Text = empAddress.PersonalEmail;
                txtMobileNo.Text = empAddress.CIMobileNo;
                txtPersionalPhone.Text = empAddress.PersonalPhone;

                //Emergency Contact
                txtEmergencyName.Text = empAddress.EmergencyName;
                txtEmergencyRelation.Text = empAddress.EmergencyRelation;
                txtEmergencyMobile.Text = empAddress.EmergencyMobile;
                txtEmergency.Text = empAddress.CIEmergencyNo;



                //Permananent address
                UIHelper.SetSelectedInDropDown(ddlCountries, empAddress.PECountryId);
                txtLocality.Text = empAddress.PELocality;

                UIHelper.SetSelectedInDropDown(ddlZones, empAddress.PEZoneId);
                if (ddlZones.SelectedValue != "-1")
                {
                    ddlDistricts.DataSource = comm.GetAllDistricts(int.Parse(ddlZones.SelectedValue));
                    ddlDistricts.DataBind();
                    UIHelper.SetSelectedInDropDown(ddlDistricts, empAddress.PEDistrictId);
                }

                UIHelper.SetText(txtHouseNo, empAddress.PEHouseNo);
                UIHelper.SetText(txtStreet, empAddress.PEStreet);
                UIHelper.SetText(txtState, empAddress.PEState);
                UIHelper.SetText(txtZipCode, empAddress.PEZipCode);
            }
            return null;
        }
    }
}