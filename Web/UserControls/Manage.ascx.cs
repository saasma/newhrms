﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Manager;
using Utils.Helper;
using BLL;
using DAL;
using Utils.Web;
using System.Text;

namespace Web.UserControls
{
    public partial class Manage : System.Web.UI.UserControl
    {
        CommonManager commonMgr = new CommonManager();
        List<EDesignation> source = null;
        public bool IsDisplayedAsPopup
        {
            get
            {
                if (Request.QueryString["isPopup"] != null)
                    return true;
                return false;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                Initialise();
            Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "clearUnload();");
        }

        void Initialise()
        {
            LoadDesignations();
           
        }

        void LoadDesignations()
        {
            source = commonMgr.GetAllDesignations();
            gvwBranches.DataSource = source;
            gvwBranches.DataBind();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            int desigID = 0;
              if (gvwBranches.SelectedIndex != -1)
                  desigID = (int)gvwBranches.DataKeys[gvwBranches.SelectedIndex][0];
              if (CommonManager.DesignationNameAlreadyExists(txtName.Text.Trim(), desigID) )
              {
                  divWarningMsg.InnerHtml = "Designation already exists.";
                  divWarningMsg.Hide = false;   
                  return;
              }

            if (Page.IsValid)
            {
                EDesignation entity = new EDesignation();

                if (gvwBranches.SelectedIndex == -1)
                {
                    entity.CompanyId = SessionManager.CurrentCompanyId;
                    entity.Name = txtName.Text.Trim();
                    entity.Code = txtCode.Text.Trim();

                    int order =0;
                    if(int.TryParse(txtOrder.Text.Trim(),out order))
                        entity.Order = order;

                    commonMgr.Save(entity);

                    divMsgCtl.InnerHtml = "Saved";

                }
                else
                {
                    entity.CompanyId = SessionManager.CurrentCompanyId;
                    entity.Name = txtName.Text.Trim();
                    entity.Code = txtCode.Text.Trim();
                    entity.DesignationId = (int)gvwBranches.DataKeys[gvwBranches.SelectedIndex][0];

                    int order = 0;
                    if (int.TryParse(txtOrder.Text.Trim(), out order))
                        entity.Order = order;

                    commonMgr.Update(entity);

                    divMsgCtl.InnerHtml = "Updated";
                }

                divMsgCtl.Hide = false;

                CommonManager.ResetCache();
                gvwBranches.SelectedIndex = -1;
                LoadDesignations();

                ClearFields();
                
            }
        }

        protected void gvwBranches_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvwBranches.PageIndex = e.NewPageIndex;
            gvwBranches.SelectedIndex = -1;
            LoadDesignations();
            ClearFields();

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            gvwBranches.SelectedIndex = -1;
            LoadDesignations();
            ClearFields();
            details.Visible = false;
        }

        protected void gvwBranches_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            CommonManager.ResetCache();
            int id = (int)gvwBranches.DataKeys[e.RowIndex][0];
            if (id != 0)
            {
                EDesignation entity = new EDesignation();
                entity.DesignationId = id;

                if (commonMgr.Delete(entity))
                {
                    divMsgCtl.InnerHtml = "Designation deleted.";
                    LoadDesignations();
                    ClearFields();
                }
                else
                {
                    divMsgCtl.InnerHtml = "Designation is in use.";

                }
                divMsgCtl.Hide = false;

            }
           
        }

        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            LoadDesignations();
            ClearFields();
            details.Visible = true;
            txtName.Focus();
        }

        protected void gvwBranches_SelectedIndexChanged(object sender, EventArgs e)
        {
            int selBranchId = (int)gvwBranches.DataKeys[gvwBranches.SelectedIndex][0];
            EDesignation branch = commonMgr.GetDesignationById(selBranchId);
            if (branch != null)
            {
                txtName.Text = branch.Name;
                if (branch.Code != null)
                    txtCode.Text = branch.Code;
                else
                    txtCode.Text = "";

                if (branch.Order != null)
                    txtOrder.Text = branch.Order.ToString();
                else
                    txtOrder.Text = "";

                btnSave.Text = Resources.Messages.Update;
                details.Visible = true;
                txtName.Focus();
            }
        }

        void ClearFields()
        {
            txtName.Text = "";
            txtCode.Text = "";
            txtOrder.Text = "";
        }

        
        protected void Page_PreRender(object sender, EventArgs e)
        {
            //output all as js array to be updatable in parent window
            if (IsDisplayedAsPopup && source != null)
            {
                //Page.ClientScript.
                StringBuilder str = new StringBuilder("");
                bool first = true;
                foreach (EDesignation obj in source)
                {
                    if (first == false)
                        str.Append(",");
                    str.Append("'" +  obj.DesignationId + "$$" + obj.CodeAndName + "'");
                    first = false;
                }
                Page.ClientScript.RegisterArrayDeclaration("texts", str.ToString());
            }
        }
        
    }
}