﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NewCalculation.ascx.cs"
    Inherits="Web.UserControls.NewCalculation" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/ErrorMsgCtl.ascx" TagName="ErrorMsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="InfoMsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<%@ Register TagPrefix="uc3" Namespace="Web.Controls" %>
<style type="text/css">
    th
    {
        text-align: center !important; /*border: 1px solid #DDDDDD !important;
        border-bottom: 0px !important;*/
        background-color: #1B93D0 !important;
        overflow: hidden;
    }
    .tableLightColor th
    {
        padding: 2px 2px !important;
        vertical-align: middle;
        padding-left: 1px !important;
        border-top: 1px solid #DDDDDD !important;
        border-left: 1px solid #DDDDDD !important;
    }
    .tableLightColor td span
    {
        padding: 2px 2px !important;
      
    }
    .tableLightColor
    {
        font-size: 11px;
    }
    td
    {
        padding: 0px !important;
        border: 0px !important;
        margin: 0px !important;
    }
    .odd
    {
        border-top: 1px solid #DDDDDD !important;
        border-bottom: 1px solid #DDDDDD !important;
    }
    .calculationInput
    {
        background-color: #CCFF9A;
        border-top: 0px !important;
        border-left: 0px !important;
        border-bottom: 0px solid #DDDDDD !important;
        border-right: 1px solid #DDDDDD !important;
    }
    
    .nameHeaderClass
    {
        text-align: left !important;
        padding-left: 6px !important;
    }
</style>
<script type="text/javascript">

    function showLoadingContent(element) {

        //alert($('#' + element.id).attr('disabled'));
        if ($('#' + element.id).prop('disabled') == 'disabled')
            return;


        $('#loadingStartingImage').show();
        $('#wrapperDiv').hide();
        $('#' + buttonsDiv + '').hide();
    }


    var buttonsDiv = '<%= pnlButtons.ClientID %>';

    $(document).ready(
            function () {
                //			 
                $('.tableLightColor tbody').attr('id', 'scrollMe');
                var sth = new ScrollTableHeader();
                sth.dynamicRowIndex = 1;
                //			 
                sth.addTbody("scrollMe");
                //                sth.delayAfterScroll = 150;
                //                sth.minTableRows  = 10;

                $('#loadingStartingImage').hide();
                $('#wrapperDiv').show();

                //  $(':text').('.adjustmentIncome').focus(function () { selectAllText($(this)) });

            }
			);
			
    
</script>
<img src='<%= Page.ResolveUrl("~/images/saving.gif") %>' id="loadingStartingImage"
    alt="" class="loadingStartingImage" />
<div class="contentArea">
    <div class="attribute">
        <strong>Payroll period </strong>&nbsp;
        <asp:DropDownList Width="140px" onchange="showLoadingContent(this)" AppendDataBoundItems="true"
            DataTextField="Name" DataValueField="PayrollPeriodId" ID="ddlPayrollPeriods"
            runat="server" AutoPostBack="True">
        </asp:DropDownList>
        <asp:CheckBox ID="chkHasRetiredOrResigned" AutoPostBack="true" runat="server" Text="Retired/Resigned only"
            Visible="false" />
        <asp:Button ID="btnGenerateSalary" CssClass="update" OnClick="btnGenerateSalary_Click" runat="server" Style="height: 26px"
            Text="Generate" />
      
    </div>
    <uc2:MsgCtl ID="msgCtl" EnableViewState="false" Hide="true" runat="server" />
    <uc2:InfoMsgCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
    <uc2:ErrorMsgCtl Hide="true" ID="divErrorMsg" ErrorMessage="Values are not properly set so please resolve to proceed forward."
        style='display: none' runat="server" />
</div>
<div style='clear: both'>
</div>
<div id="wrapperDiv" style="overflow: auto; overflow-y: hidden; display: none">
    <cc2:EmptyDisplayGridView ID="gvw" EnableViewState="false" CssClass="tableLightColor" UseAccessibleHeader="true"
        runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="CalculationId,EmployeeId,IsRetiredOrResigned,IsFinalSaved"
        GridLines="None" ShowFooterWhenEmpty="False" ShowHeaderWhenEmpty="True"
        OnRowCreated="gvw_RowCreated" OnDataBound="gvw_DataBound">
        <Columns>
            <asp:TemplateField HeaderText="EIN" HeaderStyle-BackColor="#1B93D0" HeaderStyle-Width="42px"
                ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <asp:Label ID="lblId" Width="40px" runat="server" Text='<%# Eval("EmployeeId") %>'></asp:Label>
                    <asp:HiddenField ID="incomeListId" runat="server" Value="" />
                    <asp:HiddenField ID="deductionListId" runat="server" Value="" />
                    <%-- Don't include any value,instead use DataKeyNames--%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderStyle-BackColor="#1B93D0" HeaderStyle-CssClass="nameHeaderClass"
                HeaderText="&nbsp;Name">
                <ItemTemplate>
                    <asp:Label Width="150px" ID="Label2" Style='border-right: 1px solid #DDDDDD' runat="server"
                        Text='<%# "&nbsp;" + Eval("Name") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Delete" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                <HeaderTemplate>
                    <img alt="Delete" title="Delete" src="../images/delet.png" />
                    <br />
                    <asp:CheckBox ID="chkDelete" onclick="selectDeselectAll(this)" runat="server" />
                </HeaderTemplate>
                <ItemTemplate>
                    <%-- <asp:Button ID="btnEdit" runat="server" Text="Edit" CommandName="Edit" Visible='<%# Convert.ToInt32(Eval("CalculationId")) != 0 %>' />
                
                <asp:Button ID="btnUpdate" runat="server" Text="Update" CommandName="Update" Visible='False' />--%>
                    <asp:CheckBox ID="chkDelete" ToolTip='<%# Convert.ToBoolean( Eval("IsFinalSaved")) == true ? "Salary already saved." : "" %>'
                        Enabled='<%# !Convert.ToBoolean( Eval("IsFinalSaved")) %>' Visible='<%# Convert.ToInt32(Eval("CalculationId"))!=0 %>'
                        runat="server" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <RowStyle CssClass="odd" />
        <AlternatingRowStyle CssClass="even" />
        <EmptyDataTemplate>
            <b>No salary records.</b>
        </EmptyDataTemplate>
    </cc2:EmptyDisplayGridView>
</div>
<uc1:PagingCtl ID="pagingCtl" PageSizeChangedJS="showLoadingContent(this);" OnNextRecord="btnNext_Click"
    OnPrevRecord="btnPrevious_Click" OnDropDownShowRecordsChanged="ddlRecords_SelectedIndexChanged"
    runat="server" />
<asp:Panel ID="pnlButtons" CssClass="clear buttonsDiv" Style='text-align: left' runat="server">
    <div style='text-align: left; float: left'>
        <asp:Button ID="btnSaveFinal" OnClientClick="if(confirm('{0}')==false) return false;"
            Visible="false" runat="server" CssClass="save" Text="Generate Tax" OnClick="btnSaveFinal_Click" />
    </div>
    <div style='text-align: right; float: right'>
        <asp:Button ID="btnSave" CssClass="save" OnClientClick="if(confirm('Confirm save salary?')==false) return false; else {showLoadingContent(this);}"
            runat="server" Text="Save" OnClick="btnSave_Click" />
      
        <asp:Button ID="btnCancel" Style='display: none' CssClass="cancel" runat="server"
            Text="Cancel" OnClick="btnCancel_Click" />
        <asp:Button ID="btnDelete" CssClass="delete" OnClientClick="if(confirm('Confirm delete salary?')==false) return false;"
            runat="server" Text="Delete" Visible="false" OnClick="btnDelete_Click" />
    </div>
</asp:Panel>
<%-- </div>--%>
<%--   </ContentTemplate>
    </asp:UpdatePanel>--%>
