﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MgCostCode.ascx.cs"
    Inherits="Web.UserControls.MgCostCode" %>

<script type="text/javascript">
    function ConfirmLibraryTopicDeletion() {
        var ddllist = document.getElementById('<%= ddlCostCodes.ClientID %>');
        if (ddllist.selectedIndex == -1) {
            alert('No Cost code to delete.');
            return false;
        }
        if (confirm('Do you want to delete the Cost code?')) {
            clearUnload();
            return true;
        }
        else {
            return false;
        }
    }

    function DisplayLibraryTopicInTextBox(dropdown_id) {
        var ddllist = document.getElementById(dropdown_id);
        document.getElementById('<%= txtCostCodeRename.ClientID %>').value = ddllist.options[ddllist.selectedIndex].text;
    }

  
       
</script>

<%--<div class="popupHeader">
    <h2 class="headlinespop">
        Manage Cost Codes</h2>
</div>--%>

<div class="marginal">
    <div class="bevel ">
        <div class="fields paddpop" style="padding: 10px 0 20px 10px !important;width:390px">
            <table cellpadding="0" cellspacing="0" class="alignttable" width="600px">
                <tr>
                    <td class="lf" width="120px">
                New Cost Code
             </td>
                    <td>
                <asp:TextBox ID="txtNewCostCode" runat="server" Width="159px" MaxLength="100" EnableViewState="false"></asp:TextBox>
                <asp:RequiredFieldValidator ValidationGroup="Deposit1" ID="RequiredFieldValidator3"
                    Display="None" runat="server" ErrorMessage="Cost code name is required." ControlToValidate="txtNewCostCode">*</asp:RequiredFieldValidator>
                <asp:Button ID="btnAddDepsoit"  CssClass="btn btn-success btn-sm btn-sect" Style="margin: 0px 0 0 5px; width:130px; height:25px;" OnClientClick="valGroup='Deposit1';return CheckValidation()"
                    runat="server" Text="Add Cost Code" OnClick="btnAddDepsoit_Click" />
            </td>
        </tr>
        <tr>
             <td class="lf" width="120px">
               Rename Cost Code
            </td>
            <td>
             
                <asp:TextBox ID="txtCostCodeRename" runat="server" Width="159px" MaxLength="100"></asp:TextBox>
                <asp:RequiredFieldValidator ValidationGroup="Deposit2" ID="RequiredFieldValidator4"
                    runat="server" ErrorMessage="Cost code name is required." ControlToValidate="txtCostCodeRename"
                    Display="None">*</asp:RequiredFieldValidator>
                <asp:Button ID="btnRename"  CssClass="btn btn-warning btn-sm btn-sect"
                            Style="margin: 0px 0 0 5px; width:130px; height:25px;"  OnClientClick="valGroup='Deposit2';return CheckValidation()"
                    runat="server" Text="Update Cost Code" ValidationGroup="RenameLibraryTopic" OnClick="btnRename_Click" />
            </td>
        </tr>
        <tr>
            <td class="lf" width="120px">
                 Cost code List
            </td>
            <td>
               <asp:DropDownList ID="ddlCostCodes" runat="server" Width="159px" CssClass="rightMargin"
                    DataValueField="CostCodeId" DataTextField="Name" onchange="DisplayLibraryTopicInTextBox(this.id);">
                </asp:DropDownList>
                    <asp:Button ID="btnDelete"  CssClass="btn btn-default btn-sm btn-sect" Style="margin: 0px 0 0 5px; width:130px; height:25px;" runat="server" Text="Delete Cost Code" OnClientClick="javascript:return ConfirmLibraryTopicDeletion();"
                    OnClick="btnDelete_Click" />
               
            </td>
        </tr>
    </table>
</div>
</div>
</div>

