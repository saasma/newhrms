using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL;
using BLL.Manager;
using DAL;
using Utils.Web;
using Utils.Helper;
using System.Text;
using Utils;

namespace Web.UserControls
{
    public partial class CITBalanceYearwiseReportUC : BaseUserControl
    {
        DepartmentManager depMgr = new DepartmentManager();
        BranchManager branchMgr = new BranchManager();
        EmployeeManager empMgr = new EmployeeManager();
        CompanyManager cmpmgr = new CompanyManager();
        Department dep = new Department();


        public bool IsDisplayedFromPopup
        {
            get
            {
                if (Request.QueryString["isPopup"] != null)
                    return true;
                return false;
            }
        }
        public int BranchId
        {
            get
            {
                if (ViewState["BranchId"] != null)
                    return int.Parse(ViewState["BranchId"].ToString());
                return 0;
            }
            set
            {
                ViewState["BranchId"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            divMsgCtl.DataBind();
            divWarningMsg.DataBind();

            if (!IsPostBack)
            {

                List<FinancialDate> years = new CommonManager().GetAllFinancialDates();
                foreach (FinancialDate date in years)
                {
                    date.SetName(IsEnglish);
                }
                ddlYears.DataSource = years;
                ddlYears.DataBind();

                ddlYears.SelectedIndex = years.Count;

                Initialise();
                if (!IsDisplayedFromPopup)
                    div.Visible = false;

                LoadDepartments();
            }

            Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "clearUnload();");

            LoadEmployees(0);
        }

        public TreeNode GetNode(UPage page)
        {
            TreeNode node = new TreeNode();
            node.NavigateUrl = "";
            node.SelectAction = TreeNodeSelectAction.None; ;

            node.Value = page.PageId.ToString();
            node.Text = page.PageName;
            return node;
        }

        public TreeNode GetNode(Branch page)
        {
            TreeNode node = new TreeNode();
            node.NavigateUrl = "";
            node.SelectAction = TreeNodeSelectAction.None; ;

            node.Value = page.BranchId.ToString();
            node.Text = page.Name;
            return node;
        }

        protected void gvwEmployees_RowCreated(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover",
                "this.originalClass=this.className;this.className='selected'");

                e.Row.Attributes.Add("onmouseout",
                "this.className=this.originalClass;");


            }

        }

        private void UnCheckAllNodes(TreeNode node)
        {
            if (node.ChildNodes == null || node.ChildNodes.Count == 0)
                return;

            foreach (TreeNode childNode in node.ChildNodes)
            {
                childNode.Checked = false;
                childNode.Selected = false;
                UnCheckAllNodes(childNode);
            }
            return;
        }

        


        void Initialise()
        {
           

            List<Branch> parentPages = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);


            


           



            valRegPhone.ValidationExpression = Config.PhoneRegExp;
            valRegFax.ValidationExpression = Config.FaxRegExp;

            ddlBranch.DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            ddlBranch.DataBind();
            btnSave.Text = Resources.Messages.Save;


            if (IsDisplayedFromPopup)
            {
                //maintain branch selection from employee
                this.BranchId = UrlHelper.GetIdFromQueryString("BranchId");
                if (this.BranchId != 0)
                {
                    UIHelper.SetSelectedInDropDown(ddlBranch, this.BranchId.ToString());
                    LoadDepartments();
                }
            }

            
        }

        void LoadDepartments()
        {
            int selYear = 0;
            int selUser = -1;
            if (SessionManager.CurrentLoggedInEmployeeId != null && SessionManager.CurrentLoggedInEmployeeId != 0)
            {
                selUser = SessionManager.CurrentLoggedInEmployeeId;
            }
            

            if (ddlYears.SelectedValue != null && ddlYears.SelectedItem!=null)
            {
                selYear = int.Parse(ddlYears.SelectedValue);
            }

            List<GetAllCITContributionAndOpeningResult> bindList = new List<GetAllCITContributionAndOpeningResult>();
            bindList = PFCITManager.GetBindListForYearWiseCITReport(selYear, selUser);


                gvwDepartments.DataSource = bindList;
                gvwDepartments.DataBind();

        }

        public string GetAmountOrCurrency(object amount)
        {
            if (amount != null)
            {
                return GetCurrency(amount);
            }
            
            return "";
        }


        void LoadEmployees(int departmentId)
        {
           // ListItem item = ddlDepHead.Items[0];
            //ddlDepHead.DataSource = empMgr.GetEmployeesUnderDepartment(departmentId);
            //ddlDepHead.DataBind();
            //ddlDepHead.Items.Insert(0, item);
            int total = 0;
            storeDepartment.DataSource =
                new EmployeeManager().GetEmployees(SessionManager.CurrentCompanyId);
            storeDepartment.DataBind();

        }
        protected void ddlBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            //BranchId = int.Parse(ddlBranch.SelectedValue.ToString());
            LoadDepartments();
        }

        Department ProcessDepartment(Department b)
        {
            if (b != null)
            {
                txtDepartmentName.Text = b.Name;
                txtDesc.Text = b.Description;
                txtPhone.Text = b.Phone;
                txtFax.Text = b.Fax;
                txtEmail.Text = b.Email;
                if (b.Code != null)
                    txtCode.Text = b.Code;
                else
                    txtCode.Text = "";

                
                if (b.HeadEmployeeId != null)
                {
                    cmbEmployee.SetValue(b.HeadEmployeeId.ToString());
                   // hiddenEmployee.Value = b.HeadEmployeeId.ToString();
                }   //UIHelper.SetSelectedInDropDown(ddlDepHead, b.HeadEmployeeId.ToString());


            }
            else
            {
                b = new Department();
                b.Name = txtDepartmentName.Text.Trim();
                //b.BranchId = int.Parse(ddlBranch.SelectedValue);
                b.Description = txtDesc.Text;
                b.Phone = txtPhone.Text.Trim();
                b.Fax = txtFax.Text.Trim();
                b.Email = txtEmail.Text.Trim();
                b.Code = txtCode.Text.Trim();

                int employeeId = 0;


                if (cmbEmployee.SelectedItem == null || cmbEmployee.SelectedItem.Value == "" || cmbEmployee.SelectedItem.Value == "-1")
                    b.HeadEmployeeId = null;
                else
                {
                    if (int.TryParse(cmbEmployee.SelectedItem.Value, out employeeId))
                        b.HeadEmployeeId = int.Parse(cmbEmployee.SelectedItem.Value);
                    else
                        b.HeadEmployeeId = null;
                }
                //if (ddlDepHead.SelectedValue.Equals("-1") == false)
                //    b.HeadEmployeeId = int.Parse(ddlDepHead.SelectedValue);
                //else
                //    b.HeadEmployeeId = null;

                return b;
            }
            return null;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            //if (ddlBranch.SelectedValue == "-1")
            //{
            //    JavascriptHelper.DisplayClientMsg("Please select a branch.", Page);
            //    return;
            //}

            if (Page.IsValid)
            {
                Department d = ProcessDepartment(null);

                if (gvwDepartments.SelectedIndex != -1)
                {
                    int selDepId = (int)gvwDepartments.DataKeys[gvwDepartments.SelectedIndex][0];
                    d.DepartmentId = selDepId;
                    bool status = depMgr.Update(d);
                    if (status == false)
                    {
                        divWarningMsg.InnerHtml = "Department name already exists.";//Resources.Messages.DepartmentUpdatedMsg;
                        divWarningMsg.Hide = false;
                        return;
                    }
                    divMsgCtl.InnerHtml = Resources.Messages.DepartmentUpdatedMsg;
                    divMsgCtl.Hide = false;
                 
                }
                else
                {
                    bool status =  depMgr.Save(d);
                    if (status == false)
                    {
                        divWarningMsg.InnerHtml = "Department name already exists.";//Resources.Messages.DepartmentUpdatedMsg;
                        divWarningMsg.Hide = false;
                        return;
                    }
                    divMsgCtl.InnerHtml = Resources.Messages.DepartmentSavedMsg;
                    divMsgCtl.Hide = false;
                   
                }
                CommonManager.ResetCache();
                gvwDepartments.SelectedIndex = -1;
                LoadDepartments();
                ClearDepartmentFields();
            }
            else
            {
                JavascriptHelper.DisplayClientMsg("Validation fail.", Page);
            }
        }

        void ClearDepartmentFields()
        {
            txtDepartmentName.Text = "";
            cmbEmployee.SetValue("-1");
            //cmbEmployee.ClearValue();
            //ListItem item = ddlDepHead.Items[0];
            //ddlDepHead.Items.Clear();
            //ddlDepHead.Items.Insert(0, item);
            txtDesc.Text = "";
            txtPhone.Text = "";
            txtFax.Text = "";
            txtCode.Text = "";
            txtEmail.Text = "";
            btnSave.Text = Resources.Messages.Save;
        }

        protected void gvwDepartments_SelectedIndexChanged(object sender, EventArgs e)
        {
            int depId = (int)gvwDepartments.DataKeys[gvwDepartments.SelectedIndex][0];
            Department d = depMgr.GetById(depId);
            if (d != null)
            {
                ProcessDepartment(d);
                btnSave.Text = Resources.Messages.Update;
                details.Visible = true;
                txtDepartmentName.Focus();
            }
        }

        protected void gvwDepartments_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            CommonManager.ResetCache();
            int id = (int)gvwDepartments.DataKeys[e.RowIndex][0];

            if (id != 0)
            {
                dep.DepartmentId = id;
                if (depMgr.Delete(dep) == 1)
                {
                   
                    divMsgCtl.InnerHtml = Resources.Messages.DepartmentDeletedMsg;
                    divMsgCtl.Hide = false;
                    ClearDepartmentFields();
                    details.Visible = false;
                }
                else
                {
                    //JavascriptHelper.DisplayClientMsg("Department is in use.", Page);
                    divWarningMsg.InnerHtml = Resources.Messages.DepartmentIsInUseMsg;
                    divWarningMsg.Hide = false;
                }
            }
            LoadDepartments();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ClearDepartmentFields();
            gvwDepartments.SelectedIndex = -1;
            LoadDepartments();
            details.Visible = false;
        }

        protected void gvwDepartments_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvwDepartments.PageIndex = e.NewPageIndex;
            gvwDepartments.SelectedIndex = -1;
            LoadDepartments();
            ClearDepartmentFields();
        }


        protected void Page_PreRender(object sender, EventArgs e)
        {
            //output for the querystring branchId as js array to be updatable in parent/employee window
            if (IsDisplayedFromPopup && this.BranchId != 0)
            {
                //Page.ClientScript.
                StringBuilder str = new StringBuilder("");
                bool first = true;
                List<GetDepartmentListResult> deps= DepartmentManager.GetAllDepartmentsByBranch(this.BranchId);
                foreach (GetDepartmentListResult obj in deps)
                {
                    if (first == false)
                        str.Append(",");
                    str.Append("'" + obj.DepartmentId + "$$" + obj.Name + "'");
                    first = false;
                }
                Page.ClientScript.RegisterArrayDeclaration("texts", str.ToString());
            }

            //if (IsDisplayedFromPopup)
            //{
            //    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdfdsf34", "window.onunload = closePopup;", true);
            //}
        }

        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            /*
            LoadDepartments();
            ClearDepartmentFields();
            details.Visible = true;
            txtDepartmentName.Focus();
            */
            LoadDepartments();
        }
    }
}