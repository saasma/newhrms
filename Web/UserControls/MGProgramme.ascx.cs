﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Manager;
using Utils.Helper;
using BLL;
using DAL;
using Utils.Web;
using System.Text;

namespace Web.UserControls
{
    public partial class MGProgramme : System.Web.UI.UserControl
    {
        CommonManager commonMgr = new CommonManager();
        List<EProgramme> source = null;

        public bool IsDisplayedAsPopup
        {
            get
            {
                if (Request.QueryString["isPopup"] != null)
                    return true;
                return false;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                Initialise();

        }

        void Initialise()
        {
            LoadDesignations();
            if (ddlDeposits.SelectedItem != null)
                txtDepositRename.Text = ddlDeposits.SelectedItem.Text;
            else
                txtDepositRename.Text = "";
        }

        void LoadDesignations()
        {
            source = commonMgr.GetAllProgrammes();
            ddlDeposits.DataSource = source;
            ddlDeposits.DataBind();
        }

        protected void btnAddDepsoit_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                EProgramme entity = new EProgramme();
                entity.CompanyId = SessionManager.CurrentCompanyId;
                entity.Name = txtNewDeposit.Text.Trim();

                commonMgr.Save(entity);
                CommonManager.ResetCache();
                txtNewDeposit.Text = "";
                Initialise();
                JavascriptHelper.DisplayClientMsg("Programme added.", Page);
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            if (ddlDeposits.SelectedItem != null)
            {
                EProgramme entity = new EProgramme();
                entity.ProgrammeId = int.Parse(ddlDeposits.SelectedValue);

                if (commonMgr.Delete(entity))
                {
                    JavascriptHelper.DisplayClientMsg("Programme deleted.", Page);
                    CommonManager.ResetCache();
                }
                else
                {
                    JavascriptHelper.DisplayClientMsg("Programme is in use.", Page);
                }
                Initialise();
            }
        }

        protected void btnRename_Click(object sender, EventArgs e)
        {
            if (Page.IsValid && ddlDeposits.SelectedItem != null)
            {
                EProgramme entity = new EProgramme();
                entity.Name = txtDepositRename.Text.Trim();
                entity.ProgrammeId = int.Parse(ddlDeposits.SelectedValue);

                commonMgr.Update(entity);
                CommonManager.ResetCache();
                LoadDesignations();
                UIHelper.SetSelectedInDropDown(ddlDeposits, entity.ProgrammeId.ToString());
                JavascriptHelper.DisplayClientMsg("Programme updated.", Page);
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //output all as js array to be updatable in parent window
            if (IsDisplayedAsPopup && source != null)
            {
                //Page.ClientScript.
                StringBuilder str = new StringBuilder("");
                bool first = true;
                foreach (EProgramme obj in source)
                {
                    if (first == false)
                        str.Append(",");
                    str.Append("'" + obj.ProgrammeId + "$$" + obj.Name + "'");
                    first = false;
                }
                Page.ClientScript.RegisterArrayDeclaration("texts", str.ToString());
            }
        }
    }
}