﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ChangePasswordCtl.ascx.cs"
    Inherits="Web.UserControls.ChangePasswordCtl" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<uc2:MsgCtl ID="msgCtl" EnableViewState="false" Hide="true" runat="server" />
<uc2:WarningCtl ID="warningMsgCtrl" EnableViewState="false" Hide="true" runat="server" />
<div style="width: 502px; border: 1px solid #428BCA;">
    <div>
        <h4 class="sectionHeading" style="margin-top: 0px; margin-left: -1px;">
            Change Your Password</h4>
    </div>
    <fieldset class="panel-body">
        <div class="alert alert-warning" style="margin-top: 20px;clear:both">
            Your password should have
            <br />
            - at least 6 characters and
            <br />
            - at least 1 Uppercase letter and 1 number</div>
        <table style="width: 400px" class="fieldTable">
            <tr runat="server" id="rowCurrentPassword">
                <td>
                    Current Password
                </td>
                <td>
                    <asp:TextBox TextMode="Password" ID="txtCurrentPwd" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtCurrentPwd"
                        Display="None" ErrorMessage="Please type your current password." ValidationGroup="Change"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    New Password
                </td>
                <td>
                    <asp:TextBox TextMode="Password" ID="txtNewPwd" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="valReqdName" runat="server" ControlToValidate="txtNewPwd"
                        Display="None" ErrorMessage="Please type a new password." ValidationGroup="Change"></asp:RequiredFieldValidator>
                    <%--<asp:RegularExpressionValidator ID="valPwdlength" runat="server" ValidationGroup="Change"
                        ControlToValidate="txtNewPwd" Display="None" ErrorMessage="New password length must be minimum of 5 characters length."
                        ValidationExpression=".{5,}" />--%>
                    <%-- <asp:RegularExpressionValidator ID="Regex1" runat="server" ControlToValidate="txtNewPwd" ValidationGroup="Change" Display="None"
                            ValidationExpression="^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$" ErrorMessage="Password must contain: Minimum 6 characters (8 characters recommended), atleast 1 Alphabet, and 1 Number" 
                            ForeColor="Red" />--%>
                </td>
            </tr>
            <tr>
                <td class="fieldHeader">
                    Retype new Password
                </td>
                <td>
                    <asp:TextBox TextMode="Password" ID="txtConfirmPwd" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtConfirmPwd"
                        Display="None" ErrorMessage="Confirm password is required." ValidationGroup="Change"></asp:RequiredFieldValidator>
                    <asp:CompareValidator Display="None" ControlToValidate="txtConfirmPwd" ControlToCompare="txtNewPwd"
                        Type="String" Operator="Equal" ValidationGroup="Change" ID="CompareValidator1"
                        runat="server" ErrorMessage="Passwords do not match."></asp:CompareValidator>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td class="style2" style='padding-top: 15px;'>
                    <asp:Button ID="btnChange" CssClass="btn btn-warning btn-sect btn-sm" OnClientClick="valGroup='Change';return CheckValidation()"
                        runat="server" Text="Change Password" OnClick="btnChange_Click" />
                    <asp:Button Visible="false" ID="btnCancel" CssClass="cancel" runat="server" Text="Cancel"
                        OnClick="btnCancel_Click" />
                </td>
            </tr>
        </table>
    </fieldset>
</div>
