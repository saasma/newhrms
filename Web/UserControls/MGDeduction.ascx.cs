﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using Utils.Helper;

namespace Web.UserControls
{
    public partial class MGDeduction : System.Web.UI.UserControl
    {
        PayManager payMgr = new PayManager();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialise();
            }
            //else
            //{
            //    if (this.Request.Form["__EVENTTARGET"].Equals(upDeductions.ClientID))
            //    {
            //        LoadDeductions();
            //    }
            //}
            JavascriptHelper.AttachPopUpCode(Page, "popupDeduction", "DeductionList.aspx", 470, 440);
            JavascriptHelper.AttachPopUpCode(Page, "popupUpdateDeduction", "AEDeduction.aspx", 600, 600);

        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
           
            SetPagePermission();
        }
        /// <summary>
        /// Set for Custom Role if is in Readonly Mode or Not
        /// </summary>
        public void SetPagePermission()
        {
            if (SessionManager.IsCustomRole && SessionManager.CustomRole.IsReadOnly != null && SessionManager.CustomRole.IsReadOnly.Value)
            {
                btnAddDeduction.Visible = false;
            }
        }
        void Initialise()
        {
          
            //   BizHelper.Load(new PaymentFrequency(), ddlPayFreq);
            // BizHelper.Load(new PaymentMode(), ddlPayMode);

            //LoadDeductions();
        }

        public void DisplayDeduction(int employeeId)
        {

            deductions.InnerHtml = PayManager.GetHTMLEmployeeDeductionList(employeeId);
            //btnAddIncome.Enabled = true;
            btnAddDeduction.Enabled = true;
        }

        public void EnableButton()
        {
            btnAddDeduction.Enabled = true;
        }

        //void LoadDeductions()
        //{
        //    gvwDeductions.DataSource = payMgr.GetEmployeeDeductionList(SessionManager.CurrentEmployeeId);
        //    gvwDeductions.DataBind();
        //}

        //protected void gvwIncomes_RowDeleting(object sender, GridViewDeleteEventArgs e)
        //{
        //    int deductionId = (int)gvwDeductions.DataKeys[e.RowIndex][0];
        //    int empId = SessionManager.CurrentEmployeeId;

        //    PEmployeeDeduction entity = new PEmployeeDeduction();
        //    entity.EmployeeId = empId;
        //    entity.DeductionId = deductionId;

        //    payMgr.RemoveDeductionFromEmployee(entity);
        //    LoadDeductions();
        //    e.Cancel = true;
        //    upDeductions.Update();
        //}

        //public string GetAmountOrRate(object calculation, object amount, object rate)
        //{
        //    if (amount == null)
        //        return rate.ToString();
        //    return amount.ToString();
        //}
    }
}