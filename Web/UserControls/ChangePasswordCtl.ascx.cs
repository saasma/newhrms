﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Manager;
using BLL.Base;
using BLL;
using Utils;
using Utils.Security;
using Utils.Helper;
using DAL;

namespace Web.UserControls
{
    public partial class ChangePasswordCtl : BaseUserControl
    {
        public string UserName
        {
            get
            {
                if (ViewState["username"] == null)
                    return string.Empty;
                return ViewState["username"].ToString();
            }
            set
            {
                ViewState["username"] = value;
            }
        }
        public string CancelUrl
        {
            get
            {
                if (ViewState["CancelUrl"] == null)
                    return string.Empty;
                return ViewState["CancelUrl"].ToString();
            }
            set
            {
                ViewState["CancelUrl"] = value;
            }
        }


        public bool CheckIfPasswordCanBeChangedForUrl()
        {
            UUser user = SessionManager.User;

            // not allowed for employee role
            if (user.EmployeeId != null)
            {
                return false;
            }

            if (user.URole.Name == Role.Administrator.ToString())
                return true;

            //for other role check if has permission
            string[] pages = UserManager.GetAccessiblePagesForRole(user.RoleId.Value);
            // ManagerUsers.aspx page id
            if (UserManager.IsPageAccessible("User/ManageUsers.aspx"))
                return true;
            //if (pages.Contains("ManageUsers.aspx".ToLower()))
            //    return true;
            return false;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["u"] != null)
                {
                    if (CheckIfPasswordCanBeChangedForUrl())
                    {
                        this.UserName = Request.QueryString["u"].Trim();
                        rowCurrentPassword.Visible = false;
                    }
                    else
                    {
                        msgCtl.InnerHtml = "Not enough permission to change the password.";
                        msgCtl.Hide = false;
                        btnChange.Visible = false;
                    }
                    btnCancel.Visible = false;
                }
                else
                {
                    this.UserName = HttpContext.Current.User.Identity.Name;

                    if (Request.UrlReferrer != null && Request.UrlReferrer.ToString() != "")
                        this.CancelUrl = Request.UrlReferrer.ToString();
                    else
                        this.btnCancel.Visible = false;

                    if (UserManager.GetPasswordChangedHisotryForUser().Count == 0)
                    {
                        warningMsgCtrl.InnerHtml = "You have not changed your initial password. Please change your password to protect your information.";
                        warningMsgCtrl.Hide = false;
                    }

                    int days = 0;
                    if (!UserManager.IsPwdChangedBeforePwdChangingDays(SessionManager.UserName, ref days))
                    {
                        Setting objSetting = UserManager.GetSettingForPasswordChangeRule();
                        if (objSetting.PasswordChangeType == (int)PasswordChangeTypeEnum.RecommendPwdChange)
                        {
                            warningMsgCtrl.InnerHtml = "Your password has expired. Please change your password to protect your information.";
                            warningMsgCtrl.Hide = false;
                        }
                    }
                }

             
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.CancelUrl))
                Response.Redirect(this.CancelUrl);
            else
                Response.Redirect("~/Employee/PaySilp.aspx");
        }
        protected void btnChange_Click(object sender, EventArgs e)
        {
            warningMsgCtrl.Hide = true;

            if (Config.PreventPasswordChange)
            {
                msgCtl.InnerHtml = "Password can not be changed.";
                msgCtl.Hide = false;
                return;
            }

            string msg = "";
            if (!CheckPasswordStrength(txtNewPwd.Text.Trim(), ref msg))
            {
                JavascriptHelper.DisplayClientMsg(msg, Page);
                return;
            }

            if (Page.IsValid)
            {
                UserManager mgr = new UserManager();

                if (rowCurrentPassword.Visible == true)
                {
                    if (mgr.ValidateUser(Page.User.Identity.Name, txtCurrentPwd.Text.Trim()) == false)
                    {
                        JavascriptHelper.DisplayClientMsg("Current password does not match.", Page);
                        return;
                    }
                }

                string userName = string.Empty;
                if (string.IsNullOrEmpty(this.UserName))
                    userName = this.Page.User.Identity.Name;
                else
                    userName = this.UserName;

                if (UserManager.IsMatchedToLastTwoPasswords(userName, txtNewPwd.Text.Trim()))
                {
                    JavascriptHelper.DisplayClientMsg("You have used this password earlier. Please type another password.", Page);
                    return;
                }


                if (string.IsNullOrEmpty(this.UserName))
                {
                    mgr.ChangePassword(this.Page.User.Identity.Name, txtNewPwd.Text.Trim());
                    //msgCtl.InnerHtml = Resources.Messages.PasswordChangedSuccessMsg;
                    //msgCtl.Hide = false;
                    txtNewPwd.Text = string.Empty;
                    txtConfirmPwd.Text = string.Empty;
                }
                else
                {
                    mgr.ChangePassword(this.UserName, txtNewPwd.Text.Trim());
                    //msgCtl.InnerHtml = Resources.Messages.PasswordChangedSuccessMsg;
                    //msgCtl.Width = "400px";
                    //msgCtl.Hide = false;
                    txtNewPwd.Text = string.Empty;
                    txtConfirmPwd.Text = string.Empty;
                }

                if (Request.QueryString["u"] != null)
                {
                    msgCtl.InnerHtml = Resources.Messages.PasswordChangedSuccessMsg;
                    msgCtl.Hide = false;
                    
                }
                else
                {
                    string url = this.Page.AppRelativeVirtualPath;
                    Session["PwdChanged"] = "1";

                    if (url.ToLower().Contains("employee"))
                        Response.Redirect("~/Employee/Dashboard.aspx");
                    else
                        Response.Redirect("~/newhr/DashboardHR.aspx");
                }
            }
        }

        private bool CheckPasswordStrength(string pwd, ref string msg)
        {
            msg = "";
            if ((pwd.Length < 6) || (!pwd.Where(x => Char.IsDigit(x)).Any()) || (!pwd.Where(x => Char.IsUpper(x)).Any()))
            {
                msg =  "Password must contain at least 6 characters containing at least one uppercase letter (A-Z) and at least one number (0-9).";
                return false;
            }          

            return true;    
               
        }
    }
}