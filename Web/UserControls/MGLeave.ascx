﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MGLeave.ascx.cs" Inherits="Web.UserControls.MGLeave" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>

<script type="text/javascript">

    function popupLeaveCall() {
        if (typeof (EmployeeId) != 'undefined') {
            var ret = popupLeave("EId=" + parseInt(EmployeeId));
            if (typeof (ret) != 'undefined') {
                if (ret == 'ReloadLeave') {
                    refreshLeaveList(0);
                }
            }
        }
        return false;
    }

    function reloadLeave(closingWindow) {
        closingWindow.close();
        refreshLeaveList(0); ;
    }

        function popupUpdateLeaveCall(leaveId) {
            if (typeof (EmployeeId) != 'undefined') {

                var ret = popupUpdateLeave('Id=' + leaveId + "&EId=" + parseInt(EmployeeId));
                if (typeof (ret) != 'undefined') {
                    if (ret == 'Reload') {
                        refreshLeaveList(0);
                    }
                }
            }
            return false;
        }

        //call to delete income
        function deleteLeaveCall(leaveId) {
            if (confirm("Do you want to delete the leave for the employee?")) {
                refreshLeaveList(leaveId);
            }
            return false;
        }

        function refreshLeaveList(LeaveId) {
            if (typeof (EmployeeId) != 'undefined') {
                showLoading();
                Web.PayrollService.GetLeaveList(LeaveId,parseInt(EmployeeId), refreshLeaveListCallback);
            }
        }

        function refreshLeaveListCallback(result) {
            if (result) {
                document.getElementById('<%=leaves.ClientID %>').innerHTML
                = result;
            }
            hideLoading();
        }
</script>

<div class="employeSector" style='margin-right: 0; width: 500px;padding-bottom:15px'>
    <h2>
        Leave</h2>
    <div id="leaves" runat="server">
    </div>
    <asp:Button ID="btnAddLeave" style="margin-top:5px;overflow:initial;" runat="server" CssClass="addbtns" Enabled="false" OnClientClick="return popupLeaveCall();"
        Text="Add" />
</div>
