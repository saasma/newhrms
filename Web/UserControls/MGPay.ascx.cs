﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Manager;
using Utils.Helper;
using BLL;
using DAL;
using Utils.Web;
using Utils.Base;

namespace Web.UserControls
{
    /// <summary>
    /// Notes for control creation for employee.aspx page
    /// 1) Init ddl in Page_Init instead of pageload as process will be called from parent
    /// 2) make Process method
    /// 3) js attachment code in no !IsPostBack
    /// </summary>
    public partial class MGPay : BLL.Base.BaseUserControl
    {
        PayManager payMgr = new PayManager();

        protected void Page_Init(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialise();
            }
            JavascriptHelper.AttachPopUpCode(Page, "popupIncome", "IncomeList.aspx", 480, 450);
            JavascriptHelper.AttachPopUpCode(Page, "popupUpdateIncome", "AEIncome.aspx", 600, 700);

        }


        void Initialise()
        {
            
            BizHelper.Load(new PaymentFrequency(), ddlPayFreq);
            BizHelper.Load(new PaymentMode(), ddlPayMode);
            ddlPayMode.SelectedIndex = 1;
            //LoadIncomes();

            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.BritishSchool || CommonManager.CompanySetting.IsAllIncomeInForeignCurrency)
            {
                trCurrency.Visible = true;
            }

            if (PayManager.IsBankDropDown())
            {
                List<Bank> list = PayManager.GetBankList();


                ddlBankName.DataSource = list;
                ddlBankName.DataBind();

                ddlBank1.DataSource = list;
                ddlBank1.DataBind();

                ddlBank2.DataSource = list;
                ddlBank2.DataBind();

                CustomValidator3.Visible = false;

                txtBankName.Visible = false;
            }
            else
            {
                custValidateBankName3.Visible = false;
                txtBankName.Visible = true;
                ddlBankName.Visible = false;
            }

        }


        public PPay Process(ref PPay entity)
        {
            if (entity != null)
            {

                UIHelper.SetSelectedInDropDown(ddlPayFreq, entity.PaymentFrequency);
                UIHelper.SetSelectedInDropDown(ddlPayMode, entity.PaymentMode);
                if (entity.IsCurrencyNotNepali != null)
                    ddlCurrency.SelectedValue = entity.IsCurrencyNotNepali.ToString().ToLower();
                if (entity.PaymentMode == PaymentMode.BANK_DEPOSIT)// || entity.PaymentMode == PaymentMode.CHEQUE)
                {
                    txtBankName.Text = entity.BankName;
                    txtBankACNo.Text = entity.BankACNo;

                    if (PayManager.IsBankDropDown())
                    {
                        if (entity.BankID != null)
                        {
                            UIHelper.SetSelectedInDropDown(ddlBankName, entity.BankID.Value);
                        }
                        //else
                        //    UIHelper.SetSelectedInDropDownFromText(ddlBankName, entity.BankName);
                    }
                }

                if (entity.EnableDualBankPayment != null)
                {
                    chkEnableDualBank.Checked = entity.EnableDualBankPayment.Value;
                    if (entity.EnableDualBankPayment.Value)
                        trDualBank.Style["display"] = "";
                    
                    if (entity.EnableDualBankPayment.Value)
                    {
                        UIHelper.SetSelectedInDropDown(ddlBank1, entity.Bank1ID);
                        UIHelper.SetSelectedInDropDown(ddlBank2, entity.Bank2ID);

                        txtBank1ACNo.Text = entity.Bank1ACNo;
                        txtBank2ACNo.Text = entity.Bank2ACNo;

                        if (entity.IsPercentage.Value)
                            txtBankPercentage.Text = GetCurrency(entity.PaymentAmount);
                        else
                            txtBankAmount.Text = GetCurrency(entity.PaymentAmount);

                    }
                }
            }
            else
            {
                entity = new PPay();
               
                //if (this.PayId != 0)
                  //  pay.PayId = this.PayId;
                entity.PaymentMode = ddlPayMode.SelectedValue;
                entity.PaymentFrequency = ddlPayFreq.SelectedValue;
                entity.IsCurrencyNotNepali = bool.Parse(ddlCurrency.SelectedValue);

                if (entity.PaymentMode == PaymentMode.BANK_DEPOSIT)// || entity.PaymentMode == PaymentMode.CHEQUE)
                {
                    entity.BankName = txtBankName.Text.Trim();
                    entity.BankACNo = txtBankACNo.Text.Trim();

                    if (ddlBankName.SelectedItem != null && PayManager.IsBankDropDown())
                    {
                        entity.BankID = int.Parse(ddlBankName.SelectedItem.Value);
                        entity.BankName = ddlBankName.SelectedItem.Text;
                    }
                }
                else
                {
                    entity.BankName = string.Empty;
                    entity.BankACNo = string.Empty;
                }

                entity.EnableDualBankPayment = chkEnableDualBank.Checked;
                if (entity.EnableDualBankPayment.Value)
                {
                    entity.Bank1ID = int.Parse(ddlBank1.SelectedValue);
                    entity.Bank2ID = int.Parse(ddlBank2.SelectedValue);
                    entity.Bank1ACNo = txtBank1ACNo.Text.Trim();
                    entity.Bank2ACNo = txtBank2ACNo.Text.Trim();

                    if (txtBankPercentage.Text.Trim() != "")
                    {
                        entity.IsPercentage = true;
                        entity.PaymentAmount = decimal.Parse(txtBankPercentage.Text.Trim());
                    }
                    else
                    {
                        entity.IsPercentage = false;
                        entity.PaymentAmount = decimal.Parse(txtBankAmount.Text.Trim());
                    }
                }

                return entity;
                //payMgr.SaveOrUpdatePay(pay);
                //JavascriptHelper.DisplayClientMsg("Pay saved.", Page);
            }
            return null;
        }

        protected void ValidateTextBoxBank_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (ddlPayMode.SelectedValue == PaymentMode.BANK_DEPOSIT)// || ddlPayMode.SelectedValue == PaymentMode.CHEQUE)
            {
                args.IsValid = args.Value.Trim() != "-1";
            }
            else
                args.IsValid = true;
        }
        public void EnableButton()
        {
            btnAddIncome.Enabled = true;
        }

        public void DisplayIncome(int employeeId)
        {
            incomes.InnerHtml = payMgr.GetHTMLEmployeeIncomeList(employeeId);
            btnAddIncome.Enabled = true;
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (ddlPayMode.SelectedValue == PaymentMode.BANK_DEPOSIT)// || ddlPayMode.SelectedValue == PaymentMode.CHEQUE)
            {
                txtBankName.Enabled = true;
                txtBankACNo.Enabled = true;
                rowDualBank.Style["display"] = "";

            }
            else
            {
                txtBankName.Enabled = false;
                txtBankACNo.Enabled = false;
                rowDualBank.Style["display"] = "none";
                chkEnableDualBank.Checked = false;
            }
            SetPagePermission();

            if (chkEnableDualBank.Checked)
                trDualBank.Style["display"] = "";
            else
                trDualBank.Style["display"] = "none";
        }
        /// <summary>
        /// Set for Custom Role if is in Readonly Mode or Not
        /// </summary>
        public void SetPagePermission()
        {
            if (SessionManager.IsCustomRole && SessionManager.CustomRole.IsReadOnly != null && SessionManager.CustomRole.IsReadOnly.Value)
            {
                btnAddIncome.Visible = false;
            }
        }
        protected void valCustomRFAC_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (ddlPayMode.SelectedValue == PaymentMode.BANK_DEPOSIT)// || ddlPayMode.SelectedValue == PaymentMode.CHEQUE)
            {
                args.IsValid = args.Value.Trim() != string.Empty;
            }
            else
                args.IsValid = true;
        }
    }
}