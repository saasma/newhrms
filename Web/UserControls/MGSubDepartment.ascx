﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MGSubDepartment.ascx.cs"
    Inherits="Web.UserControls.MGSubDepartment" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<script type="text/javascript">

    //capture window closing event, must be placed as close to return values
    <%
    if( IsDisplayedFromPopup)
    {
    Response.Write("window.onunload = closePopup;");
    }
     %>
    
    

    function closePopup() {
        clearUnload();
         // alert(window.opener.parentReloadCallbackFunction)
        if ($.browser.msie == false && typeof (window.opener.parentReloadCallbackFunction) != 'undefined' && ($.browser.chrome ||  $.browser.safari)) {
            window.opener.parentReloadCallbackFunction("ReloadSubDepartment", window, texts);
        } else {
            if (typeof (texts) != 'undefined')
                window.returnValue = texts;
            window.close();
        }
        
    }

    function clearUnload() {
        window.onunload = null;
    }
    
      function handleDelete()
       {            
            if(confirm('Do you want to delete the sub department?'))
            {
                clearUnload(); 
                return true;
            }
            else
                return false;
       }
</script>
<div class="popupHeader" runat="server" id="div">
    <h2 class="headlinespop">
        Manage Sub Department</h2>
</div>
<%--<asp:HiddenField ID="hiddenEmployee" runat="server" />--%>
<div class="marginal" style='margin-top: 0px'>
    <div class="contentArea">
         <label class="" style='margin-right: 10px' runat="server" id="lbl">
            Branch</label>
        <asp:DropDownList Style='margin-bottom: 15px' onchange="clearUnload()" AppendDataBoundItems="true"
            ID="ddlBranch" runat="server" DataTextField="Name" DataValueField="BranchId"
            AutoPostBack="true" OnSelectedIndexChanged="ddlBranch_SelectedIndexChanged" Width="180px">
            <asp:ListItem Text="--Select Branch--" Value="-1"></asp:ListItem>
        </asp:DropDownList>

        <label class="" style='margin-right: 10px'>
            Department</label>
        <asp:DropDownList Style='margin-bottom: 15px' onchange="clearUnload()" 
            ID="ddlDepartment" runat="server" DataTextField="Name" DataValueField="DepartmentId"
            AutoPostBack="true" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged" Width="180px">
            <asp:ListItem Text="--Select Department--" Value="-1"></asp:ListItem>
        </asp:DropDownList>
        <asp:GridView CssClass="tableLightColor" PagerStyle-HorizontalAlign="Center" PagerStyle-CssClass="defaultPagingBar"
            ID="gvwDepartments" runat="server" DataKeyNames="SubDepartmentId" AutoGenerateColumns="False"
            CellPadding="0" CellSpacing="0" GridLines="None" Width="100%" PageSize="15" AllowPaging="True"
            OnRowDeleting="gvwDepartments_RowDeleting" OnSelectedIndexChanged="gvwDepartments_SelectedIndexChanged"
            OnPageIndexChanging="gvwDepartments_PageIndexChanging">
            <Columns>
                <asp:BoundField HeaderStyle-Width="120px" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                    DataField="Name" HeaderText="Name">
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundField>
              
                <asp:BoundField HeaderStyle-Width="180px" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                    DataField="Description" HeaderText="Description">
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundField>
                <asp:TemplateField HeaderText="Edit" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"  HeaderStyle-Width="30px">
                    <ItemTemplate>
                        <asp:ImageButton ID="ImageButton1" runat="server" OnClientClick="clearUnload();"
                            CommandName="Select" ImageUrl="~/images/edit.gif" />
                    </ItemTemplate>
                    <HeaderStyle Width="30px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Delete" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"  HeaderStyle-Width="30px">
                    <ItemTemplate>
                        <asp:ImageButton ID="ImageButton2" OnClientClick="return handleDelete()" runat="server"
                            CommandName="Delete" ImageUrl="~/images/delet.png" />
                    </ItemTemplate>
                    <HeaderStyle Width="30px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:TemplateField>
            </Columns>
            <EmptyDataTemplate>
                No Sub Department has been created under this department.
            </EmptyDataTemplate>
            <RowStyle CssClass="odd" />
            <AlternatingRowStyle CssClass="even" />
            <SelectedRowStyle CssClass="selected" />
        </asp:GridView>
        <div class="buttonsDiv" style='clear: both; width: 100%!important;'>
            <asp:LinkButton ID="btnAddNew" runat="server" Style='margin-left: 10px' CssClass="createbtns"
                Text="Create Sub-Dep" OnClick="btnAddNew_Click" />
        </div>
        <uc2:MsgCtl Width='<%# IsDisplayedFromPopup ? "600px" : "920px" %>' ID="divMsgCtl"
            EnableViewState="false" Hide="true" runat="server" />
        <uc2:WarningCtl Width='<%# IsDisplayedFromPopup ? "600px" : "920px" %>' ID="divWarningMsg"
            EnableViewState="false" Hide="true" runat="server" />
        <div id="details" runat="server" class="bevel margintops" visible="false">
            <div class="fields paddpop" style="padding: 10px 0pt 20px 10px ! important;">
                <h2 class="popstitle">
                    Sub-Department Information</h2>
                <table width="100%" cellspacing="10" class="tblexp" cellpadding="10">
                    <tr>
                        <td class="fieldHeader">
                            <My:Label ID="Label2" Text="Sub-Department Name" runat="server" ShowAstrick="true" />
                        </td>
                        <td>
                            <asp:TextBox ID="txtDepartmentName" runat="server" Width="180px" />
                            <asp:RequiredFieldValidator ID="valReqdName" runat="server" ControlToValidate="txtDepartmentName"
                                Display="None" ErrorMessage="Name is required." ValidationGroup="AEDep"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="fieldHeader">
                            Description
                        </td>
                        <td>
                            <asp:TextBox ID="txtDesc" runat="server" Width="180px" TextMode="MultiLine" Columns="30"
                                Rows="6"></asp:TextBox>
                        </td>
                    </tr>
                    
                    <tr>
                        <td>
                        </td>
                        <td>
                            <asp:Button ID="btnSave" CssClass="update" OnClientClick="valGroup='AEDep';return CheckValidation()"
                                runat="server" Text="Save" ValidationGroup="AEDep" OnClick="btnSave_Click" />
                            <asp:Button ID="btnCancel" CssClass="cancel" OnClientClick="clearUnload()" runat="server"
                                Text="Cancel" OnClick="btnCancel_Click" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
