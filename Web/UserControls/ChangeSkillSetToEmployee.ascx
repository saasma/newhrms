﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ChangeSkillSetToEmployee.ascx.cs"
    Inherits="Web.UserControls.ChangeSkillSetToEmployee" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<style type="text/css">
    table.alignttable tr td
    {
        vertical-align: top;
        padding-bottom: 10px;
    }
    table.alignttable tr td input, table.alignttable tr td select
    {
        float: left;
    }
</style>
<script type="text/javascript">



    //capture window closing event
    window.onunload = closePopup;
    function closePopup() {
       // clearUnload();



        // alert(window.opener.parentReloadCallbackFunction)
       
       // if ($.browser.msie == false && typeof (window.opener.ReloadEmployeeSkillSet) != 'undefined' && ($.browser.chrome || $.browser.safari)) 
       // {
            window.opener.ReloadEmployeeSkillSet( window, data);
//        } else {
//            if (typeof (texts) != 'undefined')
//                window.returnValue = data;
//            window.close();
//        }

    }

    function clearUnload() {
        window.onunload = null;
    }

    function handleDelete() {
        if (confirm('Do you want to delete the Skill Set?')) {
            clearUnload();
            return true;
        }
        else
            return false;
    }

</script>
<div align="left">
    <ext:ResourceManager ID="ResourceManager1" DisableViewState="false"  runat="server" ShowWarningOnAjaxFailure="false"
        ScriptMode="Release" />
    <div class="marginal">
        <asp:GridView CssClass="tableLightColor" PagerStyle-HorizontalAlign="Center" PagerStyle-CssClass="defaultPagingBar"
            UseAccessibleHeader="true" ID="gvwBranches" CellPadding="3" Width="100%" runat="server"
            DataKeyNames="SkillSetId,EmployeeId" AutoGenerateColumns="False" GridLines="None"
            OnSelectedIndexChanged="gvwBranches_SelectedIndexChanged" AllowPaging="True"
            PageSize="15" OnPageIndexChanging="gvwBranches_PageIndexChanging" OnRowDeleting="gvwBranches_RowDeleting">
            <Columns>
                <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                    DataField="SkillSetName" HeaderText="Skill Set">
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundField>
                <asp:TemplateField HeaderText="Edit" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="30px">
                    <ItemTemplate>
                        <asp:ImageButton ID="ImageButton1" runat="server" OnClientClick="clearUnload();"
                            CommandName="Select" ImageUrl="~/images/edit.gif" />
                    </ItemTemplate>
                    <HeaderStyle Width="30px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Delete" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="30px">
                    <ItemTemplate>
                        <asp:ImageButton ID="ImageButton2" OnClientClick="return handleDelete()" runat="server"
                            CommandName="Delete" ImageUrl="~/images/delet.png" />
                    </ItemTemplate>
                    <HeaderStyle Width="30px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:TemplateField>
            </Columns>
            <RowStyle CssClass="odd" />
            <AlternatingRowStyle CssClass="even" />
            <SelectedRowStyle CssClass="selected" />
            <PagerStyle HorizontalAlign="Center" CssClass="defaultPagingBar"></PagerStyle>
            <EmptyDataTemplate>
                No items found.
            </EmptyDataTemplate>
        </asp:GridView>
        <div class="buttonsDiv" style='clear: both; width: 100%!important;'>
            <asp:LinkButton ID="btnAddNew" runat="server" Style='margin-left: 10px' CssClass="createbtns"
                Text="Add Skill Set" OnClick="btnAddNew_Click" />
        </div>
        <uc2:MsgCtl ID="divMsgCtl" Width='600px' EnableViewState="false" Hide="true" runat="server" />
        <uc2:WarningCtl ID="divWarningMsg" Width='600px' EnableViewState="false" Hide="true"
            runat="server" />
        <div id="details" runat="server" class="bevel" style="margin-top: 20px;" visible="false">
            <div class="fields paddpop" style="padding: 10px 0pt 20px 10px ! important;">
                <h2 class="popstitle">
                    Skill Set Information</h2>
                <table cellpadding="4px">
                    <tr>
                        <td class="fieldHeader">
                            <My:Label ID="Label2" Text="Skill Set" runat="server" ShowAstrick="true" />
                        </td>
                        <td>
                            <ext:Store ID="storeSkillSet" runat="server">
                                <Model>
                                    <ext:Model runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Name" />
                                            <ext:ModelField Name="SkillSetId" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                            <ext:ComboBox ID="ddlSkillSet" Width="180px" ForceSelection="false" runat="server"
                                StoreID="storeSkillSet" Editable="true" DisplayField="Name" ValueField="SkillSetId"
                                Mode="Local" EmptyText="">
                                <Listeners>
                                    <Blur Handler="if( this.getValue() == this.getText()) this.clearValue();" />
                                </Listeners>
                            </ext:ComboBox>

                             <asp:RequiredFieldValidator ID="RequiredFieldValidator1" SetFocusOnError="true" ValidationGroup="AEBranch"
                                ControlToValidate="ddlSkillSet" Display="None" ErrorMessage="Skill Set is required."
                                runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="fieldHeader">
                            <My:Label ID="Label1" Text="Competency Level" runat="server" ShowAstrick="false" />
                        </td>
                        <td style="padding-top:10px">
                            <ext:Store ID="storeLevel" runat="server">
                                <Model>
                                    <ext:Model>
                                        <Fields>
                                            <ext:ModelField Name="Name" />
                                            <ext:ModelField Name="LevelID" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                            <ext:ComboBox ID="ddlLevel" Width="180px" ForceSelection="false" runat="server" StoreID="storeLevel"
                                Editable="true" DisplayField="Name" ValueField="LevelID" Mode="Local">
                                <Listeners>
                                    <Blur Handler="if( this.getValue() == this.getText()) this.clearValue();" />
                                </Listeners>
                            </ext:ComboBox>
                           
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td style="padding-top:15px">
                            <asp:Button ID="btnSave" CssClass="update" OnClientClick="valGroup='AEBranch';return CheckValidation()"
                                runat="server" Text="Save" ValidationGroup="AEBranch" OnClick="btnSave_Click" />
                            <asp:Button ID="btnCancel" OnClientClick="clearUnload()" CssClass="cancel" runat="server"
                                Text="Cancel" OnClick="btnCancel_Click" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <%--</fieldset>--%>
    </div>
</div>
