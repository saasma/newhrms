using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL;
using BLL.Manager;
using DAL;
using Utils.Web;
using Utils.Helper;
using System.Text;
using Utils;

namespace Web.UserControls
{
    public partial class MGSubDepartment : BaseUserControl
    {
        DepartmentManager depMgr = new DepartmentManager();
        BranchManager branchMgr = new BranchManager();
        EmployeeManager empMgr = new EmployeeManager();
        CompanyManager cmpmgr = new CompanyManager();
        SubDepartment dep = new SubDepartment();


        public bool IsDisplayedFromPopup
        {
            get
            {
                if (Request.QueryString["isPopup"] != null)
                    return true;
                return false;
            }
        }
        public int DepartmentId
        {
            get
            {
                if (ViewState["DepartmentId"] != null)
                    return int.Parse(ViewState["DepartmentId"].ToString());
                return 0;
            }
            set
            {
                ViewState["DepartmentId"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            divMsgCtl.DataBind();
            divWarningMsg.DataBind();

            if (!IsPostBack)
            {
                Initialise();
                if (!IsDisplayedFromPopup)
                    div.Visible = false;
            }

            Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "clearUnload();");

            
        }

        void Initialise()
        {
            List<Branch> branchList = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);

            ddlBranch.DataSource = branchList;
            ddlBranch.DataBind();

            if (branchList.Count <= 1)
            {
                lbl.Visible = false;
                ddlBranch.Visible = false;
                ddlBranch.SelectedIndex = 1;

                ddlDepartment.DataSource = depMgr.GetDepartmentsByCompany(SessionManager.CurrentCompanyId);
                ddlDepartment.DataBind();
            }
            else
            {

            }
            

           
            btnSave.Text = Resources.Messages.Save;


            if (IsDisplayedFromPopup)
            {
                //maintain branch selection from employee
                this.DepartmentId = UrlHelper.GetIdFromQueryString("DepartmentId");
                if (this.DepartmentId != 0)
                {
                    UIHelper.SetSelectedInDropDown(ddlDepartment, this.DepartmentId.ToString());
                    LoadDepartments();
                }
            }

            
        }

        void LoadDepartments()
        {
            //if (ddlBranch.SelectedValue.Equals("-1") == false)
            {

                gvwDepartments.DataSource = DepartmentManager.GetAllSubDepartmentsByDepartment(int.Parse(ddlDepartment.SelectedValue));
                gvwDepartments.DataBind();

                
            }
        }

        protected void ddlBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListItem item = ddlDepartment.Items[0];
            ddlDepartment.DataSource = DepartmentManager.GetAllDepartmentsByBranch(int.Parse(ddlBranch.SelectedValue));
            ddlDepartment.DataBind();

            ddlDepartment.Items.Insert(0, item);

            gvwDepartments.DataSource = null;
            gvwDepartments.DataBind();
        }

        protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            //BranchId = int.Parse(ddlBranch.SelectedValue.ToString());
            LoadDepartments();
        }

        SubDepartment ProcessSubDepartment(SubDepartment b)
        {
            if (b != null)
            {
                txtDepartmentName.Text = b.Name;
                txtDesc.Text = b.Description;
               


            }
            else
            {
                b = new SubDepartment();
                b.Name = txtDepartmentName.Text.Trim();
                b.DepartmentId = int.Parse(ddlDepartment.SelectedValue);
                b.Description = txtDesc.Text;
               
                int employeeId = 0;


                return b;
            }
            return null;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (ddlDepartment.SelectedValue == "-1")
            {
                JavascriptHelper.DisplayClientMsg("Please select a department.", Page);
                return;
            }

            if (Page.IsValid)
            {
                SubDepartment d = ProcessSubDepartment(null);

                if (gvwDepartments.SelectedIndex != -1)
                {
                    int selDepId = (int)gvwDepartments.DataKeys[gvwDepartments.SelectedIndex][0];
                    d.SubDepartmentId = selDepId;
                    bool status =  depMgr.Update(d);

                    if (status == false)
                    {
                        divWarningMsg.InnerHtml = "Sub department name already exists.";//Resources.Messages.DepartmentUpdatedMsg;
                        divWarningMsg.Hide = false;
                        return;
                    }

                    divMsgCtl.InnerHtml = "Sub department updated.";//Resources.Messages.DepartmentUpdatedMsg;
                    divMsgCtl.Hide = false;
                 
                }
                else
                {
                    bool status = depMgr.Save(d);
                    if (status == false)
                    {
                        divWarningMsg.InnerHtml = "Sub department name already exists.";//Resources.Messages.DepartmentUpdatedMsg;
                        divWarningMsg.Hide = false;
                        return;
                    }
                    divMsgCtl.InnerHtml = "Sub department inserted.";//Resources.Messages.DepartmentSavedMsg;
                    divMsgCtl.Hide = false;
                   
                }
                CommonManager.ResetCache();
                gvwDepartments.SelectedIndex = -1;
                LoadDepartments();
                ClearDepartmentFields();
            }
            else
            {
                JavascriptHelper.DisplayClientMsg("Validation fail.", Page);
            }
        }

        void ClearDepartmentFields()
        {
            txtDepartmentName.Text = "";
            
            txtDesc.Text = "";
           
            btnSave.Text = Resources.Messages.Save;
        }

        protected void gvwDepartments_SelectedIndexChanged(object sender, EventArgs e)
        {
            int depId = (int)gvwDepartments.DataKeys[gvwDepartments.SelectedIndex][0];
            SubDepartment d = depMgr.GetSubDepartmentById(depId);
            if (d != null)
            {
                ProcessSubDepartment(d);
                btnSave.Text = Resources.Messages.Update;
                details.Visible = true;
                txtDepartmentName.Focus();
            }
        }

        protected void gvwDepartments_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            CommonManager.ResetCache();
            int id = (int)gvwDepartments.DataKeys[e.RowIndex][0];

            if (id != 0)
            {
                dep.SubDepartmentId = id;
                if (depMgr.Delete(dep) == 1)
                {

                    divMsgCtl.InnerHtml = "Sub department deleted."; //Resources.Messages.DepartmentDeletedMsg;
                    divMsgCtl.Hide = false;
                    ClearDepartmentFields();
                    details.Visible = false;
                }
                else
                {
                    //JavascriptHelper.DisplayClientMsg("Department is in use.", Page);
                    divWarningMsg.InnerHtml = "Sub department is in use.";//Resources.Messages.DepartmentIsInUseMsg;
                    divWarningMsg.Hide = false;
                }
            }
            LoadDepartments();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ClearDepartmentFields();
            gvwDepartments.SelectedIndex = -1;
            LoadDepartments();
            details.Visible = false;
        }

        protected void gvwDepartments_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvwDepartments.PageIndex = e.NewPageIndex;
            gvwDepartments.SelectedIndex = -1;
            LoadDepartments();
            ClearDepartmentFields();
        }


        protected void Page_PreRender(object sender, EventArgs e)
        {
            //output for the querystring branchId as js array to be updatable in parent/employee window
            if (IsDisplayedFromPopup && this.DepartmentId != 0)
            {
                //Page.ClientScript.
                StringBuilder str = new StringBuilder("");
                bool first = true;
                List<SubDepartment> deps = DepartmentManager.GetAllSubDepartmentsByDepartment(this.DepartmentId);
                foreach (SubDepartment obj in deps)
                {
                    if (first == false)
                        str.Append(",");
                    str.Append("'" + obj.SubDepartmentId + "$$" + obj.Name + "'");
                    first = false;
                }
                Page.ClientScript.RegisterArrayDeclaration("texts", str.ToString());
            }

            //if (IsDisplayedFromPopup)
            //{
            //    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdfdsf34", "window.onunload = closePopup;", true);
            //}
        }

        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            LoadDepartments();
            ClearDepartmentFields();
            details.Visible = true;
            txtDepartmentName.Focus();
        }
    }
}