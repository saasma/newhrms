﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL.Entity;
using Utils.Calendar;
using System.Text;
using Utils.Helper;
using BLL.Base;
using DAL;

namespace Web.UserControls
{
    public partial class LeaveOpeningBalance : BaseUserControl
    {
        private LeaveAttendanceManager mgr = new LeaveAttendanceManager();
        public bool isEditable = false;

        public LeaveOpeningBalEntityCol DataList
        {
            get
            {
                if (ViewState["result1"] == null)
                {
                    return null;
                }
                return ViewState["result1"] as LeaveOpeningBalEntityCol;
            }
            set
            {
                ViewState["result1"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            isEditable = true;

            if (!IsPostBack)
            {
                this.pagintCtl.Visible = false;
                Intialise();

                // if atte already saved then show Message
                if (BLL.BaseBiz.PayrollDataContext.Attendences.Any())
                    lbl.Visible = true;
            }

            JavascriptHelper.AttachNonDialogPopUpCode(Page, "importPopup", "../ExcelWindow/OpeningLeaveExcel.aspx", 450, 500);
            var isAttendanceSaved = LeaveAttendanceManager.IsAttendanceSavedForCompany();

            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdfdsfs",
                string.Format("var isAttendanceSaved = {0};", isAttendanceSaved.ToString().ToLower()), true);
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (isEditable == false)
            {
                btnSave.Visible = false;
                foreach (GridViewRow row in gvw.Rows)
                {
                    var IsAttedanceSaved = (bool)gvw.DataKeys[row.RowIndex]["IsAttedanceSaved"];
                    if (IsAttedanceSaved == false)
                    {
                        btnSave.Visible = true;
                        break;
                    }
                }
            }
        }

        private void Intialise()
        {
            JobStatus statues = new JobStatus();
            List<KeyValue> statusList = statues.GetMembers();

            statusList.RemoveAt(0);

            foreach (KeyValue item in statusList)
            {
                multiStatus.AddItems(item.Value, item.KeyValueCombined);
            }


            ddlLeaveTypes.DataSource = LeaveAttendanceManager.GetAllLeaves(SessionManager.CurrentCompanyId)
                .Where(x => x.IsParentGroupLeave == null || x.IsParentGroupLeave == false).ToList();
            ddlLeaveTypes.DataBind();

            var empId = UrlHelper.GetIdFromQueryString("EmpId");
            if (empId != 0)
            {
                var emp = EmployeeManager.GetEmployeeById(empId);
                if (emp != null)
                {
                    txtSearch.Text = emp.Name;
                    btnLoad_Click(null, null);
                }
            }
        }

        private void BindData()
        {
            gvw.DataSource = this.DataList;
            gvw.DataBind();

            if (DataList.Count <= 0)
            {
                btnSave.Visible = false;
            }
            else
            {
                btnSave.Visible = true;
            }
        }

        protected void btnLoad_Click(object sender, EventArgs e)
        {
            pagintCtl.CurrentPage = 1;

            // Process for Status Filter
            JobStatus statues = new JobStatus();
            List<KeyValue> statusList = statues.GetMembers();
            string[] statusTexts = multiStatus.Text.Split(new char[] { ',' });
            string statusIDTexts = "";
            foreach (string item in statusTexts)
            {
                string text = item.ToString().ToLower().Trim();
                KeyValue status = statusList.FirstOrDefault(x => x.Value.ToString().ToLower().Trim() == text);
                if (status != null)
                {
                    if (statusIDTexts == "")
                        statusIDTexts = status.Key;
                    else
                        statusIDTexts += "," + status.Key;
                }
            }


            var totalRecords = 0;
            var balances =
               mgr.GetLeaveOpeningBalance(SessionManager.CurrentCompanyId,
               int.Parse(ddlLeaveTypes.SelectedValue), txtSearch.Text.Trim(), pagintCtl.CurrentPage - 1, int.Parse(pagintCtl.DDLRecords.SelectedValue), ref totalRecords, statusIDTexts);

            var la = new LeaveAttendanceManager();

            var leaveType = la.GetLeaveType(int.Parse(ddlLeaveTypes.SelectedValue));
            gvw.Columns[3].Visible = true;

            this.DataList = balances;
            BindData();

            if (totalRecords > 0)
            {
                this.pagintCtl.Visible = true;
            }
            else
            {
                this.pagintCtl.Visible = false;
            }
            pagintCtl.UpdatePagingBar(totalRecords);
        }

        public object GetMonthName(object month)
        {
            return DateHelper.GetMonthShortName((int)month, SessionManager.CurrentCompany.IsEnglishDate);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            Page.Validate("Balance");
            var isSavedReqd = false;

            if (Page.IsValid)
            {
                var str = new StringBuilder("<Root>");
                var companyId = SessionManager.CurrentCompanyId;
                foreach (GridViewRow row in gvw.Rows)
                {
                    var rowObject = this.DataList[row.RowIndex] as LeaveOpeningBalEntity;

                    var employeeId =     (int)gvw.DataKeys[ row.RowIndex][0];
                    var leaveTypeId =        (int)gvw.DataKeys[ row.RowIndex][1];


                    float balance, cyBalance,openingTaken;
                    var txt = row.FindControl("txtOpeningBalance") as TextBox;
                    var txtCYBalance = row.FindControl("txtCYBalance") as TextBox;
                    var txtCYTaken = row.FindControl("txtCYTaken") as TextBox;


                    if (!rowObject.IsEditMode && rowObject.IsOpeningBalanceSaved)
                    {
                        continue;
                    }
                    isSavedReqd = true;

                    if (float.TryParse(txt.Text, out balance) == false)
                    {
                        balance = 0;
                    }
                    if (float.TryParse(txtCYBalance.Text, out cyBalance) == false)
                    {
                        cyBalance = 0;
                    }
                    if (float.TryParse(txtCYTaken.Text, out openingTaken) == false)
                    {
                        openingTaken = 0;
                    }
                    str.Append(
                        string.Format("<Balance EmployeeId=\"{0}\" LeaveTypeId=\"{1}\" OpeningBalance=\"{2}\" CYBalance=\"{3}\" OpeningTaken=\"{4}\" /> ",
                             employeeId, leaveTypeId, balance, cyBalance,openingTaken)
                    );
                }
                str.Append("</Root>");

                if (isSavedReqd)
                {
                    mgr.SaveLeaveOpeningBalance(str.ToString());

                    divMsgCtl.InnerHtml = "Opening balance has been saved, but first period attendance need to be resaved for the effect of opening balance change.";
                    divMsgCtl.Hide = false;
                }
            }

            btnLoad_Click(null, null);
        }


        protected void gvw_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowIndex < 0)
            {
                return;
            }
            var isOpeningBalanceSaved = (bool)gvw.DataKeys[e.Row.RowIndex]["IsOpeningBalanceSaved"];

            var IsAttedanceSaved = (bool)gvw.DataKeys[e.Row.RowIndex]["IsAttedanceSaved"];


            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var rowObject = e.Row.DataItem as LeaveOpeningBalEntity;

                if (rowObject == null)
                {
                    return;
                }
                var txt = e.Row.FindControl("txtOpeningBalance") as TextBox;
                var val = e.Row.FindControl("valOpeningBalance") as CompareValidator;
                var btnEdit = e.Row.FindControl("btnEdit") as Button;
                var btnCancel = e.Row.FindControl("btnCancel") as Button;
                var txtCYBalance = e.Row.FindControl("txtCYBalance") as TextBox;
                var txtCYTaken = e.Row.FindControl("txtCYTaken") as TextBox;

                if (rowObject.IsEditMode == true)
                {
                    txtCYTaken.Enabled = true;
                    txtCYBalance.Enabled = true;
                    txt.Enabled = true;
                    txt.Visible = true;
                    val.Visible = true;
                    btnEdit.Visible = false;
                    btnCancel.Visible = true;
                }

                else
                {
                    if (IsAttedanceSaved == false || isEditable == true)
                    {
                        val.Visible = false;
                        btnEdit.Visible = true;
                    }

                    else
                    {
                        val.Visible = true;
                        btnEdit.Visible = false;
                    }
                }
            }
        }

        protected void gvw_DataBound(object sender, EventArgs e)
        {
        }

        protected void gvw_RowCommand(object sender, GridViewCommandEventArgs e)
        {
        }

        protected void gvw_RowEditing(object sender, GridViewEditEventArgs e)
        {
            e.Cancel = true;
            var obj = this.DataList[e.NewEditIndex];
            obj.IsEditMode = true;
            BindData();
        }

        protected void gvw_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            e.Cancel = true;
            var obj = this.DataList[e.RowIndex];
            obj.IsEditMode = false;
            BindData();
        }

        protected void btnPrevious_Click()
        {
            pagintCtl.CurrentPage -= 1;
            btnLoad_Click(null, null);
        }

        protected void btnNext_Click()
        {
            pagintCtl.CurrentPage += 1;
            btnLoad_Click(null, null);
        }

        protected void ddlRecords_SelectedIndexChanged()
        {
            pagintCtl.CurrentPage = 1;
            btnLoad_Click(null, null);
        }
    }
}
