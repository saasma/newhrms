﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddOnCalculationNew.ascx.cs"
    Inherits="Web.UserControls.AddOnCalculationNew" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/ErrorMsgCtl.ascx" TagName="ErrorMsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="InfoMsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<%@ Register TagPrefix="uc3" Namespace="Web.Controls" %>
<script src="../Scripts/jquery.lightbox_me.js?v=300" type="text/javascript"></script>
<style type="text/css">
    th
    {
        text-align: center !important; /*border: 1px solid #DDDDDD !important;
        border-bottom: 0px !important;*/
        overflow: hidden;
    }
    .tableLightColor th
    {
        padding: 2px 2px !important;
        vertical-align: middle;
        padding-left: 1px !important;
        border-top: 1px solid #BFD9FB !important;
        border-left: 1px solid #BFD9FB !important;
        border-right: 1px solid #BFD9FB !important;
        border-bottom: 1px solid #BFD9FB !important;
    }
    .tableLightColor td span
    {
        padding: 2px 2px !important;
    }
    .tableLightColor
    {
        font-size: 11px;
    }
    td
    {

        height: 25px !important;
        padding: 0px !important;
        border: 0px !important;
        margin: 0px !important;
        line-height: 1.6em;
    }
    .odd
    {
        border-top: 1px solid #DFECFD !important;
        border-bottom: 1px solid #DFECFD !important;
    }
    .calculationInput
    {
        background-color: #FAFCFF;
        border-top: 0px !important;
        border-left: 0px !important;
        border-bottom: 0px solid #DFECFD !important;
        border-right: 1px solid #DFECFD !important;
    }
    
    .nameHeaderClass
    {
        text-align: left !important;
        padding-left: 6px !important;
    }
    .employeeClass
    {
    	color:Black;
    }
    .tabDiv
    {
    	border-bottom: 2px solid #DFECFD;
        margin-left: 10px;
        clear: both;
        margin-top: 12px;
        margin-bottom: 5px;
        height: 23px;
        margin-right: 13px;
    }
    .tabDiv a
    {
    	display:inline-block;
    	width:100px;
    	color:#003F59;
    }
  .tabDiv div
{
    background-color:#fff;
    display: inline;
    width: 105px;
    padding-top: 8px;
    text-align: center;
    padding-bottom: 8px;
}
.tabDiv .selected
{
    background: white;
    border-bottom: 2px solid white;
    border-top: 2px solid #DFECFD;
    border-left: 2px solid #DFECFD;
    border-right: 2px solid #DFECFD;
    padding-top: 7px;
}

#wrapperDiv
{
  border: 1px solid #DFECFD;
}
.x-form-trigger-wrap .x-form-text
{
    height:25px;
}
    .employeeClass:hover{color:Black!important;text-decoration:none!important;;}
</style>
<script type="text/javascript">

    function showLoadingContent(element) {

        //alert($('#' + element.id).prop('disabled'));
        if ($('#' + element.id).prop('disabled') == 'disabled')
            return;


        $('#loadingStartingImage').show();
        $('#wrapperDiv').hide();

    }


    function clicked() {

        if (generateConfirmation() == false)
            return false;

        var value = "";

        if (document.getElementById('<%= rdbTax1.ClientID %>').checked)
            value = "true";
        else if (document.getElementById('<%= rdbTax2.ClientID %>').checked)
            value = "false";

        var useOtherAddon = false;
        if (document.getElementById('<%= chkUseOtherAddOnOfThisMonthInTax.ClientID %>').checked)
            useOtherAddon = "true";
        document.getElementById('<%=chkUseOtherAddOnOfThisMonthInTax1.ClientID %>').value = useOtherAddon;


        document.getElementById('<%= hiddenChecked.ClientID %>').value = value;

        __doPostBack('Generate', '');
    }

    function saveUpdateAddOnPopup(useAddId) {
        var payrollPeriodId = document.getElementById('<%= ddlPayrollPeriods.ClientID %>').value;


        var addOnId = 0;
        if (payrollPeriodId == 0 || payrollPeriodId == "")

            return false;

        if(payrollPeriodId.toString().indexOf(':') >= 0)
            payrollPeriodId = payrollPeriodId.substring(0,payrollPeriodId.toString().indexOf(':'));

        var ein = document.getElementById('<%=hiddenEmployeeID.ClientID %>').value;

        if (useAddId) {
            addOnId = document.getElementById('<%= ddlAddOnName.ClientID %>').value;
        }

        if (useAddId && (addOnId == 0 || addOnId == "")) {
            alert("Create / Select Add-On first.");
            return false;
        }

        saveUpdateAddOn1("Id=" + addOnId + "&PId=" + payrollPeriodId + "&ein=" + ein);


        return false;
    }

    function addEmployee(useAddId) {
        var payrollPeriodId = document.getElementById('<%= ddlPayrollPeriods.ClientID %>').value;


        var addOnId = 0;
        if (payrollPeriodId == 0 || payrollPeriodId == "")

            return false;


       
        if (useAddId) {
            addOnId = document.getElementById('<%= ddlAddOnName.ClientID %>').value;
        }

        if (useAddId && (addOnId == 0 || addOnId == "")) {
            alert("Create / Select Add-On first.");
            return false;
        }

        <%= btnLoadAddEmployee.ClientID %>.fireEvent('click');


        return false;
    }

    $(document).ready(
            function () {
                try {
                    //			 
                    $('.tableLightColor tbody').attr('id', 'scrollMe');
                    var sth = new ScrollTableHeader();
                    sth.dynamicRowIndex = 1;
                    //			 
                    sth.addTbody("scrollMe");
                    //                sth.delayAfterScroll = 150;
                    //                sth.minTableRows  = 10;

                    $('#loadingStartingImage').hide();
                    $('#wrapperDiv').show();
                }
                catch (ee) { }
                //  $(':text').('.adjustmentIncome').focus(function () { selectAllText($(this)) });

            }
			);


    function changeHeader(btn) {
        var payrollPeriodId = document.getElementById('<%= ddlPayrollPeriods.ClientID %>').value;

        if (payrollPeriodId == 0)
            return false;

        if(payrollPeriodId.toString().indexOf(':') >= 0)
            payrollPeriodId = payrollPeriodId.substring(0,payrollPeriodId.toString().indexOf(':'));

        var addOnId = document.getElementById('<%= ddlAddOnName.ClientID %>').value;

        if ((addOnId == 0 || addOnId == "")) {
            alert("Create / Select Add-On first.");
            return false;
        }

        var ein = document.getElementById('<%= hiddenEmployeeID.ClientID %>').value;

        var ret = change1("p=" + payrollPeriodId + "&id=" + addOnId + "&ein=" + ein);


        return false;
    }

    function generateConfirmation() {

        var ein = document.getElementById('<%= hiddenEmployeeID.ClientID %>').value;

        if (ein == "")
            return confirm("Confirm generate tax for all employees?");
        else
            return confirm("Confirm generate tax for single employee having EIN : " + ein + "?");
    }

    function importPopupProcess(btn) {



        var payrollPeriodId = document.getElementById('<%= ddlPayrollPeriods.ClientID %>').value;
        var unitId = document.getElementById('<%= ddlUnit.ClientID %>').value;
        
        if (payrollPeriodId == 0 || payrollPeriodId == "") {

            return false;
        }

        var addonId = document.getElementById('<%= ddlAddOnName.ClientID %>').value;

        if (addonId == 0 || addonId == "") {
            alert("Create / Select Add-On first.");
            return false;
        }

        var ein = document.getElementById('<%= hiddenEmployeeID.ClientID %>').value;
        var ret = importPopup1("payrollPeriod=" + payrollPeriodId + "&id=" + addonId + "&ein=" + ein + "&unitid=" + unitId);


        return false;
    }
    function refreshWindow1(window) {
        window.close();
        __doPostBack('Reload', '');
    }
    function refreshWindow() {
        __doPostBack('Reload', '');
    }

    var selEmpId = null;
    function ACE_item_selected(source, eventArgs) {
        var value = eventArgs.get_value();
        selEmpId = value;
        document.getElementById('<%= hiddenEmployeeID.ClientID %>').value = selEmpId;
    }

    function processEdit(ein) {
        <%=hdnEmployeeId.ClientID %>.setValue(ein);
        <%= btnLoadHead.ClientID %>.fireEvent('click');
    }
</script>
<img src='<%= Page.ResolveUrl("~/images/saving.gif") %>' id="loadingStartingImage"
    alt="" class="loadingStartingImage" />
<div class="pageheader">
    <div class="media">
        <div class="media-body">
            <h4>
                Add-on Pay List
            </h4>
        </div>
    </div>
</div>
<div class="contentpanel">
    <ext:ResourceManager runat="server" ScriptMode="Release" DisableViewState="false" />
    <div class="attribute" style="padding: 10px">
        <strong>Payroll period </strong>
        <asp:DropDownList OnSelectedIndexChanged='ddlPayrollPeriod_Change' Width="140px"
            onchange="showLoadingContent(this)" AppendDataBoundItems="true" DataTextField="Name"
            DataValueField="PayrollPeriodId" ID="ddlPayrollPeriods" runat="server" AutoPostBack="True">
        </asp:DropDownList>
        <span style="padding-left:10px" runat="server" id="spanUnit">
            <strong>Unit</strong>
            <asp:DropDownList ID="ddlUnit" AutoPostBack="true" AppendDataBoundItems="true"
                DataTextField="Name" DataValueField="UnitID" runat="server" Width="100px">
                <asp:ListItem Text="--Select--" Value="-1" Selected="True" />
            </asp:DropDownList></span>
        <asp:HiddenField ID="hiddenEmployeeID" runat="server" />
        <asp:HiddenField ID="chkUseOtherAddOnOfThisMonthInTax1" runat="server" />
        <asp:HiddenField ID="hdnChangedInClientValues" runat="server" />
        <asp:TextBox style="padding-left:5px;" ID="txtEmpSearch" runat="server" OnTextChanged="txtEmpSearch_TextChanged"
            AutoPostBack="true" Width="160" />
        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtEmpSearch"
            WatermarkText="Search Employee" WatermarkCssClass="searchBoxText" />
        <cc1:AutoCompleteExtender EnableCaching="true" ID="AutoCompleteExtenderOrganization"
            runat="server" MinimumPrefixLength="2" ServiceMethod="GetEmployeeNamesAndEINWithRetiredAlso"
            ServicePath="~/PayrollService.asmx" TargetControlID="txtEmpSearch" OnClientItemSelected="ACE_item_selected"
            CompletionSetCount="10" CompletionInterval="250" CompletionListCssClass="AutoExtender"
            CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
        </cc1:AutoCompleteExtender>
        &nbsp; &nbsp;<strong style='display: none'>Department</strong>
        <asp:DropDownList Style='display: none' ID="ddlDepartments" AutoPostBack="true" AppendDataBoundItems="true"
            DataTextField="Name" DataValueField="DepartmentId" runat="server" Width="150px"
            OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged">
            <asp:ListItem Text="--Select Department--" Value="-1" Selected="True" />
        </asp:DropDownList>
        <asp:Button OnClientClick="" Width="100px" Style="display: inline" ID="btnLoad" runat="server"
            CssClass="btn btn-default btn-sect btn-sm" Text="Load" />
        &nbsp;&nbsp;
        <asp:LinkButton ID="btnImport" Style='text-decoration: underline; font-size: 12px'
            runat="server" Text="Import" OnClientClick="importPopupProcess();return false;" />
        &nbsp;&nbsp;
        <asp:LinkButton ID="btnChangeHeader" Style='text-decoration: underline; font-size: 12px'
            runat="server" Text="Select Incomes" OnClientClick="changeHeader();return false;" />
        &nbsp;&nbsp;
        <asp:LinkButton ID="btnExportTaxCalculation" OnClick="btnExportTaxCalculation_Click"
            Visible="false" Style='text-decoration: underline; font-size: 12px' runat="server"
            Text="Export Tax Details" />
    </div>
    <div style="margin-bottom: 5px">
        <strong>Add-On</strong>
        <asp:DropDownList ID="ddlAddOnName" OnSelectedIndexChanged='ddlAddOnName_Change'
            AutoPostBack="true" runat="server" Width="180px" DataTextField="Name" DataValueField="AddOnId" />
        <strong style='padding-left: 10px;'>Date Filter</strong>
        <asp:DropDownList ID="ddlAddOnDateFilter" AutoPostBack="true" runat="server" Width="180px"
            DataTextField="Text" DataValueField="Text">
        </asp:DropDownList>
         <strong>Show After : </strong>
        <asp:Label runat="server" ID="lblShowAfter" />
        <asp:CheckBox ID="chkShowInPayslip" Enabled="false" runat="server" Text="Show in Payslip" />
        &nbsp;&nbsp;<asp:Button ID="btnSaveName1" runat="server" OnClientClick="return saveUpdateAddOnPopup(false);"
            CssClass="btn btn-primary btn-sect btn-sm" Text="Create Add-On" />
        &nbsp;&nbsp;<asp:Button ID="btnEdit" runat="server" OnClientClick="return saveUpdateAddOnPopup(true);"
            CssClass="btn btn-primary btn-sect btn-sm" Text="Edit Add-On" />
        &nbsp;&nbsp;<asp:Button ID="btnGenerateTax" runat="server" OnClientClick=" {$('#divGenerateTax').lightbox_me({ centered: true });}return false;"
            CssClass="btn btn-warning btn-sect btn-sm" Text="Generate Tax for Add-On" />
         &nbsp;&nbsp;<asp:Button ID="btnAddEmployee" runat="server" OnClientClick="return addEmployee(true);"
            CssClass="btn btn-primary btn-sect btn-sm" Text="Add Employee" />
    </div>
    <asp:HiddenField ID="hiddenChecked" runat="server" />
    <uc2:MsgCtl ID="msgCtl" EnableViewState="false" Hide="true" runat="server" />
    <uc2:InfoMsgCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
    <uc2:ErrorMsgCtl Hide="true" ID="divErrorMsg" ErrorMessage="Values are not properly set so please resolve to proceed forward."
        style='display: none' runat="server" />
    <div id="divGenerateTax" style="display: none; width: 300px; height: 320px; background-color: white;
        border: 4px solid #0587b8; padding: 10px; background-color: #e8f1ff">
        <table>
            <tr>
                <td style="height: 30px">
                    <strong>Add Tax from Beginning incomes will be skipped here for tax calculation
                    </strong>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <br />
                    Pay additional tax in
                    <br />
                    <br />
                    <asp:RadioButton ID="rdbTax1" runat="server" Text="This pay" Checked="true" GroupName="tax" />
                    <br />
                    <br />
                    <asp:RadioButton ID="rdbTax2" runat="server" Text="Remaining months" Checked="false"
                        GroupName="tax" />
                    <br />
                    <br />
                    <asp:RadioButton ID="rdbTax3" runat="server" Text="This pay + remaining months" Checked="false"
                        GroupName="tax" />
                </td>
            </tr>
            <tr>
                <td colspan="2" style="padding-top: 15px; display: none">
                    <br />
                    <br />
                    <asp:CheckBox runat="server" ID="chkUseOtherAddOnOfThisMonthInTax" Text="Use other Add-on of this month for tax" />
                </td>
            </tr>
            <tr>
                <td style="height: 30px">
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Button ID="btnGenerate" runat="server" CssClass="save" OnClientClick="return clicked();"
                        CausesValidation="false" OnClick="btnGenerate_Click" Text="Generate" />
                </td>
            </tr>
        </table>
    </div>
</div>
<div id="wrapperDiv" style="overflow: auto; overflow-y: hidden; display: none; margin-left: 10px;
    margin-right: 13px; clear: both">
    <cc2:EmptyDisplayGridView ID="gvw" CssClass="tableLightColor" UseAccessibleHeader="true"
        runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="CalculationId,EmployeeId,IsRetiredOrResigned,IsFinalSaved"
        GridLines="None" ShowFooterWhenEmpty="False" ShowHeaderWhenEmpty="True" EnableViewState="false"
        OnRowCreated="gvw_RowCreated" OnDataBound="gvw_DataBound">
        <Columns>
            <asp:TemplateField HeaderText="EIN" HeaderStyle-BackColor="#1B93D0" HeaderStyle-Width="42px"
                ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <asp:Label ID="lblId" Width="40px" runat="server" Text='<%# Eval("EmployeeId") %>'></asp:Label>
                    <asp:HiddenField ID="incomeListId" runat="server" Value="" />
                    <asp:HiddenField ID="deductionListId" runat="server" Value="" />
                    <%-- Don't include any value,instead use DataKeyNames--%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="I No" HeaderStyle-BackColor="#1B93D0" HeaderStyle-Width="72px"
                ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <asp:Label ID="lblINo" Width="70px" runat="server" Text='<%# Eval("IdCardNo") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderStyle-BackColor="#1B93D0" HeaderStyle-CssClass="nameHeaderClass"
                HeaderText="&nbsp;Name">
                <ItemTemplate>
                    <asp:HyperLink CssClass="employeeClass" Target="_blank" Width="150px" ID="Label2"
                        Style='border-right: 1px solid #DDDDDD' runat="server" NavigateUrl='<%# "~/newhr/EmployeePayroll.aspx?Id=" +  Eval("EmployeeId") %>'
                        Text='<%# "&nbsp;" + Eval("Name") %>'></asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderStyle-BackColor="#1B93D0" HeaderStyle-CssClass="nameHeaderClass"
                HeaderText="&nbsp;" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <asp:HyperLink CssClass="employeeClass" Target="_blank" Width="50px" ID="Label21"
                        Style='border-right: 1px solid #DDDDDD' runat="server" NavigateUrl='javascript:void(0)'
                        onclick='<%# "processEdit(" + Eval("EmployeeId")  + ");return false;"%>' Text='Edit'></asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <RowStyle CssClass="odd" />
        <AlternatingRowStyle CssClass="even" />
        <EmptyDataTemplate>
            <b>No salary records.</b>
        </EmptyDataTemplate>
    </cc2:EmptyDisplayGridView>
</div>
<div style='padding-left: 10px; margin-right: 13px;'>
    <uc1:PagingCtl ID="pagingCtl" ShowDropDown="true" OnChangePage="ChangePageNumber"
        PageSizeChangedJS="showLoadingContent(this);" OnNextRecord="btnNext_Click" OnPrevRecord="btnPrevious_Click"
        OnDropDownShowRecordsChanged="ddlRecords_SelectedIndexChanged" runat="server" />
</div>
<ext:Button ID="btnLoadHead" Hidden="true" runat="server" Cls="btn btn-success" Text="LoadHead"
    Width="120">
    <DirectEvents>
        <Click OnEvent="btnLoadHead_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:Button>
<ext:Button ID="btnLoadAddEmployee" Hidden="true" runat="server" Cls="btn btn-success" Text="LoadHead"
    Width="120">
    <DirectEvents>
        <Click OnEvent="btnLoadAddEmployee_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:Button>
<ext:Window ID="window" runat="server" ButtonAlign="Left" Title="Edit Amount" Icon="Application"
    Width="480" Height="250" BodyPadding="15" Hidden="true" Modal="true">
    <Content>
        <table class="fieldTable">
            <tr>
                <td>
                    <ext:DisplayField ID="lblEmployee" LabelSeparator="" FieldStyle='font-size:16px;color:#428BCA;' FieldLabel="" LabelAlign="Top"
                        runat="server" Width="200px" />
                    <ext:Hidden runat="server" ID="hdnEmployeeId" />
                </td>
            </tr>
            <tr>
                <td style='padding-top:10px'>
                    <ext:ComboBox ID="cmbHead" LabelSeparator="" Width="180px" runat="server" ValueField="TypeSourceId"
                        DisplayField="HeaderName" FieldLabel="Head" LabelAlign="top" ForceSelection="true"
                        QueryMode="Local">
                        <Store>
                            <ext:Store ID="Store4" runat="server">
                                <Model>
                                    <ext:Model ID="Model3" runat="server" IDProperty="TypeSourceId">
                                        <Fields>
                                            <ext:ModelField Name="TypeSourceId" Type="String" />
                                            <ext:ModelField Name="HeaderName" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <DirectEvents>
                            <Select OnEvent="cmbHead_Select" >
                                <EventMask ShowMask="true" />
                            </Select>
                        </DirectEvents>
                        <Listeners>
                            <Focus Handler="this.lastQuery='';this.store.clearFilter();" />
                        </Listeners>
                    </ext:ComboBox>
                    <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator3" runat="server"
                        ValidationGroup="SaveAmount" ControlToValidate="cmbHead" ErrorMessage="Please select Head." />
                </td>
            
               <td style='padding-top:10px'>
                    <ext:TextField ID="txtNewAmount" LabelSeparator="" FieldLabel="Amount" LabelAlign="Top"
                        runat="server" Width="120px">
                    </ext:TextField>
                    <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator1" runat="server"
                        ValidationGroup="SaveAmount" ControlToValidate="txtNewAmount" ErrorMessage="Please place amount." />
                    <asp:CompareValidator Display="None" ID="RequiredFieldValidator22" runat="server"
                        ValidationGroup="SaveAmount" ControlToValidate="txtNewAmount" ErrorMessage="Please place valid amount."
                        Type="Double" Operator="DataTypeCheck" />
                </td>
            </tr>
        </table>
    </Content>
    <Buttons>
        <ext:Button runat="server" ID="btnChangeAmount" Cls="btn btn-primary" Text="<i></i>Change"
            runat="server">
            <DirectEvents>
                <Click OnEvent="btnChangeAmount_Click">
                    <Confirmation ConfirmRequest="true" Message="Confirm change the amount?" />
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
            <Listeners>
                <Click Handler="valGroup = 'SaveAmount'; return CheckValidation();">
                </Click>
            </Listeners>
        </ext:Button>
        <ext:LinkButton runat="server" Cls="btnFlatLeftGap" ID="LinkButton4" Text="<i></i>Cancel"
            runat="server">
            <Listeners>
                <Click Handler="#{window}.hide();">
                </Click>
            </Listeners>
        </ext:LinkButton>
    </Buttons>
</ext:Window>
<ext:Window ID="windowAdd" runat="server" ButtonAlign="Left" Title="Add Amount" Icon="Application"
    Width="380" Height="320" BodyPadding="15" Hidden="true" Modal="true">
    <Content>
        <table class="fieldTable">
            <tr>
                <td>
                    <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                        <Proxy>
                            <ext:AjaxProxy Url="../Handler/EmpSearchID.ashx">
                                <ActionMethods Read="GET" />
                                <Reader>
                                    <ext:JsonReader Root="plants" TotalProperty="total" />
                                </Reader>
                            </ext:AjaxProxy>
                        </Proxy>
                        <Model>
                            <ext:Model ID="Model5" IDProperty="EmployeeId" Name="ItemLineModel" runat="server">
                                <Fields>
                                    <ext:ModelField Name="Name" Type="String" />
                                    <ext:ModelField Name="EmployeeId" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                    <ext:ComboBox LabelSeparator="" ID="cmbEmpSearch" FieldLabel="Search Employee" EmptyText="Employee Name"
                        LabelWidth="80" LabelAlign="Top" runat="server" DisplayField="Name" ValueField="EmployeeId"
                        StoreID="storeSearch" TypeAhead="false" Width="250" PageSize="9999" HideBaseTrigger="true"
                        MinChars="1" TriggerAction="All" ForceSelection="false">
                        <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                            <ItemTpl ID="ItemTpl2" runat="server">
                                <Html>
                                    <tpl>
                                                        <div class="search-item">
                                                                        <span>{Name}</span>  
                                                         </div>
					                            </tpl>
                                </Html>
                            </ItemTpl>
                        </ListConfig>
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                        </Triggers>
                        <Listeners>
                            <Select Handler="this.getTrigger(0).show(); SelEmpSearch();" />
                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                            <TriggerClick Handler="if (index == 0) { 
                                                       this.clearValue(); 
                                                       this.getTrigger(0).hide();
                                                   }" />
                        </Listeners>
                    </ext:ComboBox>
                </td>
            </tr>
            <tr>
                <td>
                    <ext:ComboBox ID="cmbHeadAdd" LabelSeparator="" Width="200px" runat="server" ValueField="TypeSourceId"
                        DisplayField="HeaderName" FieldLabel="Head" LabelAlign="top" ForceSelection="true"
                        QueryMode="Local">
                        <Store>
                            <ext:Store ID="Store1" runat="server">
                                <Model>
                                    <ext:Model ID="Model1" runat="server" IDProperty="TypeSourceId">
                                        <Fields>
                                            <ext:ModelField Name="TypeSourceId" Type="String" />
                                            <ext:ModelField Name="HeaderName" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <Listeners>
                            <Focus Handler="this.lastQuery='';this.store.clearFilter();" />
                        </Listeners>
                    </ext:ComboBox>
                    <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator2" runat="server"
                        ValidationGroup="SaveAmountAdd" ControlToValidate="cmbHeadAdd" ErrorMessage="Please select Head." />
                </td>
            </tr>
            <tr>
                <td>
                    <ext:TextField ID="txtAmountAdd" LabelSeparator="" FieldLabel="Amount" LabelAlign="Top"
                        runat="server" Width="200px">
                    </ext:TextField>
                    <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator4" runat="server"
                        ValidationGroup="SaveAmountAdd" ControlToValidate="txtAmountAdd" ErrorMessage="Please place amount." />
                    <asp:CompareValidator Display="None" ID="CompareValidator1" runat="server" ValidationGroup="SaveAmountAdd"
                        ControlToValidate="txtAmountAdd" ErrorMessage="Please place valid amount." Type="Double"
                        Operator="DataTypeCheck" />
                </td>
            </tr>
        </table>
    </Content>
    <Buttons>
        <ext:Button runat="server" ID="btnSaveAdd" Cls="btn btn-primary" Text="<i></i>Save"
            runat="server">
            <DirectEvents>
                <Click OnEvent="btnChangeAmount_Click">
                    <Confirmation ConfirmRequest="true" Message="Confirm change the amount?" />
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
            <Listeners>
                <Click Handler="valGroup = 'SaveAmountAdd'; return CheckValidation();">
                </Click>
            </Listeners>
        </ext:Button>
        <ext:LinkButton runat="server" Cls="btnFlatLeftGap" ID="LinkButton1" Text="<i></i>Cancel"
            runat="server">
            <Listeners>
                <Click Handler="#{windowAdd}.hide();">
                </Click>
            </Listeners>
        </ext:LinkButton>
    </Buttons>
</ext:Window>
