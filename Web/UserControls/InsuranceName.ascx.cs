﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Manager;
using Utils.Helper;
using BLL;
using DAL;
using Utils.Web;
using System.Text;

namespace Web.UserControls
{
    public partial class InsuranceName : System.Web.UI.UserControl
    {
        InsuranceManager insMgr = new InsuranceManager();
        List<IInsuranceName> source = new List<IInsuranceName>();
        public bool IsDisplayedAsPopup
        {
            get
            {
                if (Request.QueryString["isPopup"] != null)
                    return true;
                return false;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                Initialise();

        }

        void Initialise()
        {
            Load();
            if (ddlNames.SelectedItem != null)
                txtInsuranceRename.Text = ddlNames.SelectedItem.Text;
            else
                txtInsuranceRename.Text = "";
        }

        new void Load()
        {
            source = insMgr.GetInsuranceName();
            ddlNames.DataSource = source;
            ddlNames.DataBind();
        }

        protected void btnAddInsuranceName_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                IInsuranceName entity = new IInsuranceName();               
                entity.InsuranceName = txtNewInsuranceName.Text.Trim();

                insMgr.Save(entity);

                txtNewInsuranceName.Text = "";
                Initialise();
                JavascriptHelper.DisplayClientMsg("Insurance name added.", Page);
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            if (ddlNames.SelectedItem != null)
            {
                IInsuranceName entity = new IInsuranceName();
                entity.Id = int.Parse(ddlNames.SelectedValue);

                if (insMgr.Delete(entity))
                {
                    JavascriptHelper.DisplayClientMsg("Insurance name deleted.", Page);
                    
                }
                else
                {
                    JavascriptHelper.DisplayClientMsg("Insurance name is in use.", Page);
                }
                Initialise();
            }
        }

        protected void btnRename_Click(object sender, EventArgs e)
        {
            if (Page.IsValid && ddlNames.SelectedItem != null)
            {
                IInsuranceName entity = new IInsuranceName();
                entity.InsuranceName = txtInsuranceRename.Text.Trim();
                entity.Id = int.Parse(ddlNames.SelectedValue);

                insMgr.Update(entity);
                Load();
                UIHelper.SetSelectedInDropDown(ddlNames, entity.Id.ToString());
                JavascriptHelper.DisplayClientMsg("Insurance name updated.", Page);
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //output all as js array to be updatable in parent window
            if (IsDisplayedAsPopup && source !=null)
            {
                //Page.ClientScript.
                StringBuilder str = new StringBuilder("");
                bool first = true;
                foreach (IInsuranceName obj in source)
                {
                    if (first == false)
                        str.Append(",");
                    str.Append("'" + obj.Id + "$$" + obj.InsuranceName + "'");
                    first = false;
                }
                Page.ClientScript.RegisterArrayDeclaration("texts", str.ToString());
            }
        }
        
    }
}