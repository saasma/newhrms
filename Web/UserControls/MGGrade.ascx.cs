﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using Utils.Helper;
using Utils.Web;
using System.Text;

namespace Web.UserControls
{
    public partial class MGGrade : System.Web.UI.UserControl
    {
        CommonManager commonMgr = new CommonManager();
        List<EGrade> source = null;

        public bool IsDisplayedAsPopup
        {
            get
            {
                if (Request.QueryString["isPopup"] != null)
                    return true;
                return false;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                Initialise();
        }

        void Initialise()
        {
            LoadGrade();
            if (ddlDeposits.SelectedItem != null)
                txtDepositRename.Text = ddlDeposits.SelectedItem.Text;
            else
                txtDepositRename.Text = "";
        }

        void LoadGrade()
        {
            source = commonMgr.GetAllGrades();
            ddlDeposits.DataSource = source;  //payMgr.GetDepositList(SessionManager.CurrentCompanyId);
            ddlDeposits.DataBind();
        }

        protected void btnAddDepsoit_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                EGrade entity = new EGrade();
                entity.CompanyId = SessionManager.CurrentCompanyId;
                entity.Name = txtNewDeposit.Text.Trim();

                commonMgr.Save(entity);
                CommonManager.ResetCache();
                txtNewDeposit.Text = "";
                Initialise();
                JavascriptHelper.DisplayClientMsg("Grade added.", Page);
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            if (ddlDeposits.SelectedItem != null)
            {
                EGrade entity = new EGrade();
                entity.GradeId = int.Parse(ddlDeposits.SelectedValue);

                if (commonMgr.Delete(entity))
                {
                    JavascriptHelper.DisplayClientMsg("Grade deleted.", Page);
                    
                }
                else
                {
                    JavascriptHelper.DisplayClientMsg("Grade is in use.", Page);
                }
                CommonManager.ResetCache();
                Initialise();
            }
        }

        protected void btnRename_Click(object sender, EventArgs e)
        {
            if (Page.IsValid && ddlDeposits.SelectedItem != null)
            {
                EGrade entity = new EGrade();
                entity.Name = txtDepositRename.Text.Trim();
                entity.GradeId = int.Parse(ddlDeposits.SelectedValue);

                commonMgr.Update(entity);
                CommonManager.ResetCache();
                LoadGrade();
                UIHelper.SetSelectedInDropDown(ddlDeposits, entity.GradeId.ToString());
                JavascriptHelper.DisplayClientMsg("Grade updated.", Page);
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //output all as js array to be updatable in parent window
            if (IsDisplayedAsPopup && source != null)
            {
                //Page.ClientScript.
                StringBuilder str = new StringBuilder("");
                bool first = true;
                foreach (EGrade obj in source)
                {
                    if (first == false)
                        str.Append(",");
                    str.Append("'" + obj.GradeId + "$$" + obj.Name + "'");
                    first = false;
                }
                Page.ClientScript.RegisterArrayDeclaration("texts", str.ToString());
            }
        }
    }
}