﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Base;
using BLL.Manager;
using DAL;
using System.Drawing;
using System.Text;
using Utils.Helper;
using Image = System.Web.UI.WebControls.Image;
using System.Web.Services;
using System.Data;
using Web.Master;
using Web.CP.Report.Templates.Pay;
using DevExpress.XtraReports.UI;
using Web.CP.Report;
using DevExpress.XtraPrinting;
using System.IO;
using BLL.Entity;
using BLL.BO;

namespace Web.UserControls
{

    public partial class PastCalculationAdjCtl : BaseUserControl
    {
     

        //in pixel unit
        public const int ColumnWidth = 69;

        CalculationManager calcMgr = new CalculationManager();
        CommonManager commonMgr = new CommonManager();
        public CCalculation calculation = null;
        //public int decimalPlaces = 0;
        // To track if any cell value is invalid, set from "CalcGridViewTemplate
        public bool isValidAllValid = true;
        // To track if the page is in edit mode or not, currently edit mode button is hidden so no functionality, as to effect for tax calc
        public bool editMode = false;

        // hold all the incomes types to be displayed in Calculation list
        List<CalcGetHeaderListResult> headers = new List<CalcGetHeaderListResult>();

        public Dictionary<string, string> headerList = new Dictionary<string, string>();
        private int payrollPeriodId = 0;

        public bool isIncomeAdjChecked = false;
        public bool isDeductionAdjChecked = false;

        bool isHeaderMissingError = false;
        int missingType = 0;
        int missingSourceId = 0;


        private void RestoreAdjustmentCheckboxState()
        {
            foreach (string key in Request.Form.AllKeys)
            {
                if (key.Contains(CalculationManager.idIncomeAdjCheckbox) && Request.Form[key].ToLower()=="on")
                {
                    isIncomeAdjChecked = true;
                }
                if (key.Contains(CalculationManager.idDeductionAdjCheckbox) && Request.Form[key].ToLower() == "on")
                {
                    isDeductionAdjChecked = true;
                }
            }
        }

        public void txtEmpSearch_TextChanged(object sender, EventArgs e)
        {
            LoadCalculation(false, false);
        }
      

        // ViewState off for this page
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Page.User.IsInRole(Role.Administrator.ToString()))
            {

            }
            else
            {
                //if not accessible then redirect to Default page
                Response.Redirect("~/Default.aspx");
            }

            JavascriptHelper.AttachNonDialogPopUpCode(Page, "importPopup", "../ExcelWindow/PastSalaryExcel.aspx", 450, 500);


            // For D2 Show/Hide Currency Rate
            if (CommonManager.CompanySetting.IsD2)
            {
                CurrencyRate rate = CommonManager.GetCurrencyRate();
                if (rate != null)
                    currencyValue.InnerHtml = string.Format(Resources.Messages.D2FixedCurrentRateText,
                        rate.FixedRateDollar, rate.CurrentRateDollar);
            }
            else
            {
                linkCurrencyRate.Visible = false;
                currencyValue.Visible = false;
                linkCurrencyHistory.Visible = false;
            }

            //RestoreAdjustmentCheckboxState();

            //if (!IsPostBack)
            //    CalculationManager.ClearVariableAndIncomeAjdustmentOnFirstLoad();

            bool loadColumns = false;
            //if dropdown changes
            if (Request.Form["__EventTarget"] != null && 
                (Request.Form["__EventTarget"].Equals(ddlPayrollPeriods.ClientID.Replace("_", "$"))
              ))
            {
                this.payrollPeriodId = int.Parse(Request.Form[ddlPayrollPeriods.ClientID.Replace("_", "$")]);
                //Initialise();
                _tempCurrentPage = 1;
                loadColumns = true;
            }


            if(!Initialise())
                return;
            ;
            //AddColumns();
            //RegisterLegendColors();
           
            if (!IsPostBack)
            {
                _tempCurrentPage = 1;
                loadColumns = true;
               
            }
          
            //
            
            //if first time load & submitted by Delete/Save button then need to reload as ViewState is disabled
            // and to prevent always double loading when page index is changed

            if (!IsPostBack || (Request.Form["__EVENTTARGET"] != null && 
                ( !Request.Form["__EVENTTARGET"].ToLower().Contains("pagingctl"))
                 ///  ||  !Request.Form["__EVENTTARGET"].ToLower().Contains("txtEmpSearch")
                   )
               // )

                )
                //(Request.Form[btnSave.ClientID.Replace("_", "$")] != null
                //|| Request.Form[btnDelete.ClientID.Replace("_", "$")] != null
                //|| IsExportButtonSubmit()
                //|| IsPayrollPeriodDropDownChanged()
                //|| Request.Form[chkHasRetiredOrResigned.ClientID.Replace("_","$")] != null))
            {
                LoadCalculation(false, loadColumns);
            }

            RegisterScripts();

            JavascriptHelper.AttachPopUpCode(Page, "commentPopup", "PopupHtml.htm", 300, 300);
            JavascriptHelper.AttachNonDialogPopUpCode(Page, "currencyPopup", "CurrencySetting.aspx", 450, 500);

            JavascriptHelper.AttachNonDialogPopUpCode(Page, "currencyHistoryPopup", "CurrencyHistory.aspx", 450, 500);
        }


     

        void RegisterScripts()
        {
            StringBuilder str = new StringBuilder();
            str.AppendFormat("var decimalPlaces = {0};", SessionManager.DecimalPlaces);
            // Generating txt names like txt-4-4, txt-10-10 for the total textboxes,
            //depends upon the naming generated in "CalcGridViewTemplate"
            str.AppendFormat("var txtIncomeSumTxt = 'txtd{0}';",
                             (((int)CalculationColumnType.IncomeGross)) +
                             ((int)CalculationColumnType.IncomeGross).ToString());
            str.AppendFormat("var txtDeductionSumTxt = 'txtd{0}';",
                            (((int)CalculationColumnType.DeductionTotal)) +
                            ((int)CalculationColumnType.DeductionTotal).ToString());

            str.AppendFormat("var txtNetTxt = 'txtd{0}';",
                            (((int)CalculationColumnType.NetSalary)) +
                            ((int)CalculationColumnType.NetSalary).ToString());

            
            str.AppendFormat("var msgCannotUnchecked = '{0}';", Resources.Messages.CalcCannotUncheckedForAdjustment);

            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "rounding",
                                                        str.ToString()
                                                        , true);
        }

        bool Initialise()
        {


            //ListItem firstItem = ddlPayrollPeriods.Items[0];
            ddlPayrollPeriods.Items.Clear();
            //ddlPayrollPeriods.Items.Insert(0,firstItem);
            List<PayrollPeriodBO> payrollList = CommonManager.GetAllYear(SessionManager.CurrentCompanyId);
            List<PayrollPeriodBO> tempList = new List<PayrollPeriodBO>();
            foreach (PayrollPeriodBO bo in payrollList)
            {
                tempList.Add(new PayrollPeriodBO { PayrollPeriodId = bo.PayrollPeriodId , Name = bo.Name });
            }
            if (tempList.Count > 0)
            {
                if (CalculationManager.IsAllEmployeeSavedFinally(payrollList[tempList.Count - 1].PayrollPeriodId, false) == false)
                {
                    tempList.RemoveAt(tempList.Count - 1);
                }
            }

            ddlPayrollPeriods.DataSource = tempList;
            ddlPayrollPeriods.DataBind();

            ddlPayrollPeriods.ClearSelection();

            if (ddlPayrollPeriods.Items.Count <= 0)
            {
                divErrorMsg.InnerHtml = Resources.Messages.CalcNoPayrollPeriodDefined;
                HideAll(false);

                return false;
            }


            if (this.payrollPeriodId == 0)
            {
                ListItem item = ddlPayrollPeriods.Items[ddlPayrollPeriods.Items.Count - 1];
                if (item != null)
                {
                    payrollPeriodId = int.Parse(item.Value);
                    item.Selected = true;
                }
            }
            else
            {
                ListItem item = ddlPayrollPeriods.Items.FindByValue(payrollPeriodId.ToString());
                if (item != null)
                {
                    payrollPeriodId = int.Parse(item.Value);
                    item.Selected = true;
                }
                
            }

           // ddlPayrollPeriods.Enabled = false;

            //PayrollPeriod lastPayrollPeriod = CommonManager.GetLastPayrollPeriod();
            //// no payroll period defined
            //if (lastPayrollPeriod == null)
            

            //lbl.Text = lastPayrollPeriod.Name;
            //this.payrollPeriodId = lastPayrollPeriod.PayrollPeriodId;
            calculation = CalculationManager.IsCalculationSaved(this.GetPayrollPeriodId());


            


            // if current payroll period in selection then only process
            if (ddlPayrollPeriods.Items.Count > 0 &&
                payrollPeriodId.ToString() == ddlPayrollPeriods.Items[ddlPayrollPeriods.Items.Count - 1].Value)
            {
                int total = 0, completed = 0;
                //checkListSection.Visible = !CommonManager.IsAllCheckListCompleted(ref total,ref completed);
                //checkListLink.Text = string.Format("{0} of {1} checklist yet to be reviewed." ,completed,total );
            }
            else
                checkListSection.Visible = false;


            //if (!IsPostBack || IsPayrollPeriodDropDownChanged())
            //{
            //    if (chkHasRetiredOrResigned.Visible)
            //    {
            //        chkHasRetiredOrResigned.Checked = true;
            //        //chkHasRetiredOrResigned.Enabled = false;
            //    }
            //}

            //if no retired/reg then make visible false as its value is always going
          

            return true;
        }

        //public void txtEmpSearch_TextChanged(object sender, EventArgs e)
        //{
        //    LoadCalculation(false, false);
        //}

        protected void btnExport_Click(object sender, EventArgs e)
        {

            //ExportToExcel();
            Export1();
        }
             #region "Excel Export"
        public void Export1()
        {
            List<CalcGetHeaderListResult> headers = null;

           
                headers = CalculationManager.GetIncomeHeaderList(SessionManager.CurrentCompanyId, this.GetPayrollPeriodId());


               

            List<CalcGetCalculationListResult> list = null;

            int pageIndex = _tempCurrentPage - 1;
                int pageSize = int.Parse(pagingCtl.DDLRecords.SelectedValue);

                //if access from export
            if (IsExportButtonSubmit())
            {
                pageSize = 99999;
                pageIndex = 0;
            }

            //gvw.DataSource = null;
            //gvw.DataBind();

            int? employeeId = null;
            if (hiddenEmployeeId.Value != "")
                employeeId = int.Parse(hiddenEmployeeId.Value);

            list = CalculationManager.GetCalculationList(SessionManager.CurrentCompanyId,
                this.GetPayrollPeriodId(), pageIndex,
                pageSize, ref _tempCount, false, employeeId, txtEmpSearch.Text.Trim(), (int)SalaryType.Combined, -1, -1, "", -1,-1);
            




            headers = CalculationValue.SortHeaders(headers, PayManager.GetIncomeOrderValues(), PayManager.GetDeductionOrderValues());


            DataTable dataTable = CreateDataTable(list, headers);
            DataSet dset = new DataSet();
            dset.Tables.Add(dataTable);

            SalarySummary mainReport = new SalarySummary();
            mainReport.Bands[BandKind.PageHeader].HeightF = 20;
            mainReport.labelTitle.Visible = false;

            mainReport.DataSource = dset;
            mainReport.DataMember = "Report";

            CreateUsingLabels(mainReport);

            string name = "";

            if (payrollPeriodId != 0)
            {
                
                    name = CommonManager.GetPayrollPeriod(payrollPeriodId).Name;
               

            }

            using (MemoryStream stream = new MemoryStream())
            {

                mainReport.ExportToXls(stream);
                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                HttpContext.Current.Response.AppendHeader("Content-Type", "application/vnd.ms-excel");
                HttpContext.Current.Response.AddHeader("Content-Length", stream.ToArray().Length.ToString());
                HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=PaySummary " + name + ".xls");
                HttpContext.Current.Response.OutputStream.Write(stream.ToArray(), 0, stream.ToArray().Length);
                HttpContext.Current.Response.End();

            }

        }
        private void CreateUsingLabels(XtraReport report)
        {
            DataTable dataTable = (report.DataSource as DataSet).Tables[0];
            int firstColumnExtraWidth = 60;

            Color colorHeader = Color.FromArgb(201, 214, 237);
            Color colorEven = Color.FromArgb(239, 243, 250);

            int colCount = dataTable.Columns.Count;
            int colWidth = 80;
             int Name_Column_Index = 1;
         int top = 65;

            report.PageWidth = colWidth * colCount;
            (report as SalarySummary).labelTitle.WidthF = report.PageWidth ;
            int currentXLocation = 0;
            int prevWidth = 0;
            int currentWidth = 0;
            // Create header captions
            for (int i = 0; i < colCount; i++)
            {
                
                XRLabel label = new XRLabel();
                
               // label.BeforePrint += new System.Drawing.Printing.PrintEventHandler(Event_BeforePrint);
                label.TextAlignment = TextAlignment.MiddleLeft;
             
                ReportHelper.HeaderLabelStyle(label);
                
             
                
            
                
                 if (i == 0)
                {
                    currentWidth = colWidth / 2; //for SN
                    label.TextAlignment = TextAlignment.MiddleCenter;
                }
                //else if( i==1)
                //{
                //    currentWidth = colWidth / 2; //for title

                //}
                else if( i==1)
                {
                    currentWidth = colWidth + firstColumnExtraWidth; //for name
                   
                }
                //else if (i == 3)
                //{
                //    currentWidth = colWidth + 20; //first cost code                   
                //}
                // else if (i == 4) // for Program
                // {
                //     currentWidth = colWidth + 20;                  
                // }
                else
                {
                    currentWidth = colWidth;//for other amount columns
                 
                }
                currentXLocation += (prevWidth);
                prevWidth = currentWidth;

                label.Location = new Point(currentXLocation, top);

                
                label.Text = dataTable.Columns[i].Caption;
                //label.Padding = new PaddingInfo(4, 2, 2, 2);
                
                label.Font = new Font(label.Font.FontFamily, label.Font.Size , FontStyle.Bold);
                label.CanGrow = false;//dont allow to grow as design will be meshed up
               // if (i != Name_Column_Index)
                if (i == 0)
                {
                    label.Borders = DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom;
                    label.Size = new Size(currentWidth, 50);
                }
                else
                {
                    label.Borders = DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom;


                  
                    label.Size = new Size(currentWidth, 50);
                }
                
                report.Bands[BandKind.PageHeader].Controls.Add(label);
            }

           
            currentXLocation = 0;
            prevWidth = 0;
            currentWidth = 0;
            // Create data-bound labels with different odd and even backgrounds
            for (int i = 0; i < colCount; i++)
            {
                XRLabel label = new XRLabel();
               
                label.Text = "-";
                //label.BeforePrint += new System.Drawing.Printing.PrintEventHandler(Event_BeforePrint);
                
               
                if (i == 0)
                {
                    currentWidth = colWidth / 2;
                    label.TextAlignment = TextAlignment.MiddleCenter;
                }
                //else if (i == 1)
                //{
                //    currentWidth = colWidth / 2;
                //    label.TextAlignment = TextAlignment.MiddleLeft;

                //}
                else if (i == 1)
                {
                    currentWidth = colWidth + firstColumnExtraWidth; //for name
                    label.TextAlignment = TextAlignment.MiddleLeft;
                }
                //else if (i == 3)
                //{
                //    currentWidth = colWidth + 20; //first cost code
                //    label.TextAlignment = TextAlignment.MiddleLeft;
                //}
                //else if (i == 4)
                //{
                //    currentWidth = colWidth + 20; //first for program
                //    label.TextAlignment = TextAlignment.MiddleLeft;
                //}
                else
                {
                    currentWidth = colWidth;
                    label.TextAlignment = TextAlignment.MiddleRight;

                    

                    //label.EvaluateBinding += new BindingEventHandler(BindingEventHandler);
                }
                currentXLocation += (prevWidth);
                prevWidth = currentWidth;

               //if (i == Name_Column_Index)
                    label.Location = new Point(currentXLocation, 0);
                //else
                //{
                //    label.Location = new Point((colWidth * i) + firstColumnExtraWidth, 0);                   
                //}
                //label.Padding = new PaddingInfo(2, 2, 2, 0);
                if (i == 0 || i == Name_Column_Index)
                    label.DataBindings.Add("Text", null, dataTable.Columns[i].ColumnName);
                else
                    label.DataBindings.Add("Text", null, dataTable.Columns[i].ColumnName, "{0:n2}");

               
              

                
                label.CanGrow = false;
                
                
              
                //if (i != Name_Column_Index)
                //{
               // 
                if (i == 0)
                {
                    label.Size = new Size(currentWidth, 20);
                    label.Borders = DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom;
                }

                else
                {
                    label.Borders = DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom;

                    label.Size = new Size(currentWidth, 20);
                }
                //}
                //else
                //{
                //    label.Borders = DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom;
                //    label.TextAlignment = TextAlignment.MiddleLeft;
                //    label.Size = new Size(colWidth + firstColumnExtraWidth, 20);
                    
                //}

                    ReportHelper.LabelStyle(label);
                label.Font = new Font(label.Font.FontFamily, label.Font.Size , FontStyle.Regular);

                //label.BackColor = i == 0 ? colorEven : Color.White;
                //label.BorderColor = colorHeader;
                // Place the labels onto a Detail band
                report.Bands[BandKind.Detail].Controls.Add(label);
            }

            currentXLocation = 0;
            prevWidth = 0;
            currentWidth = 0;

            //create footer captions for Total
            for (int i = 0; i < colCount; i++)
            {
                XRLabel label = new XRLabel();
                //set properties for sum/total
                //label.StyleName = "ReportFooter";
                label.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(190)))), ((int)(((byte)(216)))));

               // label.BeforePrint += new System.Drawing.Printing.PrintEventHandler(Event_BeforePrint);

                if (i == 0)
                {
                    currentWidth = colWidth / 2;
                }
                //else if (i == 1)
                //{
                //    currentWidth = colWidth / 2;

                //}
                else if (i == 1)
                {
                    currentWidth = colWidth + firstColumnExtraWidth; //for name

                }
                //else if (i == 3)
                //{
                //    currentWidth = colWidth + 20; //first cost code

                //}
                //else if (i == 4)
                //{
                //    currentWidth = colWidth + 20; //Program

                //}
                else
                {
                    currentWidth = colWidth;

                }
                currentXLocation += (prevWidth);
                prevWidth = currentWidth;

                    label.Location = new Point(currentXLocation, 0);

                //label.Padding = new PaddingInfo(2, 2, 2, 0);


                

                DevExpress.XtraReports.UI.XRSummary summary = new DevExpress.XtraReports.UI.XRSummary();


                //skip for first two columns
                if (i == 1)
                {
                    label.Text = "Total";
                }
                else if (i >1)
                {
                    label.DataBindings.Add("Text", null, dataTable.Columns[i].ColumnName);



                    summary.FormatString = "{0:n2}";
                    
                    summary.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
                    label.Summary = summary;

                }


                label.CanGrow = false;
              
                if( i==0)

                    label.Borders = DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom;

                else
                    label.Borders = DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom; ;

                    label.TextAlignment = TextAlignment.MiddleRight;
                    label.Size = new Size(currentWidth, 20);
                //}
                //else
                //{
                //    label.Text = "Total";
                //    label.Borders = DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom;
                //    label.TextAlignment = TextAlignment.MiddleLeft;
                //    label.Size = new Size(colWidth + firstColumnExtraWidth, 20);
                //}
                label.Font = new Font(label.Font.FontFamily, label.Font.Size, FontStyle.Bold);

                //label.BackColor = i == 0 ? colorEven : Color.White;
                //label.BorderColor = colorHeader;
                // Place the labels onto a Detail band
                report.Bands[BandKind.ReportFooter].Controls.Add(label);
            }
        }
        private DataTable CreateDataTable(List<DAL.CalcGetCalculationListResult> data, List<CalcGetHeaderListResult> headerList)
        {
            DataTable dataTable = new DataTable();
            dataTable.TableName = "Report";

            dataTable.Columns.Add("EIN", typeof(int));
           // dataTable.Columns.Add("Title", typeof(string));
            dataTable.Columns.Add("Name", typeof(string));
           // dataTable.Columns.Add("CostCode", typeof(string));
            //dataTable.Columns.Add("Program", typeof(string));

            Dictionary<string, string> headerListDictionary = new Dictionary<string, string>();

            // Create table cells, fill the header cells with text, bind the cells to data
            for (int i = 0; i < headerList.Count; i++)
            {

                DataColumn column = new DataColumn(headerList[i].Type + ":" + headerList[i].SourceId, typeof(decimal));
                column.Caption = headerList[i].HeaderName;

                dataTable.Columns.Add(column);


                string key = headerList[i].Type + ":" + headerList[i].SourceId;
                if (!headerListDictionary.ContainsKey(key))
                    headerListDictionary.Add(key, "");
            }

            foreach (CalcGetHeaderListResult header in headers)
            {
               
            }

            dataTable.Columns.Add("Note", typeof(string));

            //add rows
            decimal? value;
            foreach (CalcGetCalculationListResult row in data)
            {
                List<object> list = new List<object>();
                list.Add(row.EmployeeId);
            //    list.Add(row.Title);
                list.Add(row.Name);
              //  list.Add(row.CostCode);
               // list.Add(row.Program);

                for (int i = 0; i < headerList.Count; i++)
                {
                    value = row.GetCellValue(headerList[i].Type.Value, headerList[i].SourceId.Value, 2, headerListDictionary
                        ,ref isHeaderMissingError,ref missingType,ref missingSourceId);
                    //if (value == null)
                    //    list.Add("0");
                    //else
                    list.Add(value);
                }

                list.Add(row.Note);

                dataTable.Rows.Add(list.ToArray());
            }



            return dataTable;


        }
        #endregion
      
        bool IsPayrollPeriodDropDownChanged()
        {
            return Request.Form["__EventTarget"] != null && Request.Form["__EventTarget"].Equals(ddlPayrollPeriods.ClientID.Replace("_", "$"));
        }

        private void HideAll(bool show)
        {
            if (!show)
            {
                pagingCtl.Visible = false;
                gvw.Visible = false;
                //pnlPaging.Visible = false;
                pnlButtons.Visible = false;

                divErrorMsg.Hide = false;

            }
            else
            {

                pagingCtl.Visible = true;
                gvw.Visible = true;
                //pnlPaging.Visible = true;
                pnlButtons.Visible = true;
                divErrorMsg.Hide = true;
            }
        }

        private int GetPayrollPeriodId()
        {
            //return int.Parse(ddlPayrollPeriods.SelectedValue);
            return payrollPeriodId;
        }

        bool IsExportButtonSubmit()
        {
            return false;
        }

        void LoadCalculation(bool isEditMode,bool isReloadColumn)
        {

            if (calculation == null)
            {
                gvw.Columns[gvw.Columns.Count - 1].Visible = false;
            }
            else
            {
                gvw.Columns[gvw.Columns.Count - 1].Visible = true;
                //btnDelete.Enabled = true;
                //btnEdit.Enabled = true;
            }

            headers = CalculationManager.GetIncomeHeaderList(SessionManager.CurrentCompanyId, this.GetPayrollPeriodId());
            foreach (CalcGetHeaderListResult header in headers)
            {
                string key = header.Type + ":" + header.SourceId;
                if (!headerList.ContainsKey(key) )
                    headerList.Add(key, "");
            }

            AddColumns(headers, gvw, this, isReloadColumn);

            HideAll(true);

            ChangeDisplayState(isEditMode, isReloadColumn);

            if (this.GetPayrollPeriodId() != 0)
            {


                int pageIndex = _tempCurrentPage - 1;
                int pageSize = int.Parse(pagingCtl.DDLRecords.SelectedValue);

                //if access from export
                if (IsExportButtonSubmit())
                {
                    pageSize = 99999;
                    pageIndex = 0;
                }

                //string[] value = ddlStatus.SelectedValue.Split(new char[] { ':' });
                //int minStatus = int.Parse(value[0]);
                //bool statusOnly = ddlStatus.SelectedItem.Text.ToString().ToLower().Contains("only");

                int? employeeId = null;
                if (hiddenEmployeeId.Value != "")
                    employeeId = int.Parse(hiddenEmployeeId.Value);

                gvw.DataSource = CalculationManager.GetCalculationList(SessionManager.CurrentCompanyId,
                    this.GetPayrollPeriodId(), pageIndex,
                    pageSize, ref _tempCount, false, employeeId, txtEmpSearch.Text.Trim(),(int)SalaryType.Combined,-1,-1,"",-1,-1);
                gvw.DataBind();

                if( gvw.Rows.Count<=0)
                {
                    divErrorMsg.InnerHtml = Resources.Messages.CalcNoEmployees;
                   // divInfoMsg.Style.Remove("display");
                    HideAll(false);
                    return;
                }

            }
            else
            {

                gvw.DataSource = null;
                gvw.DataBind();

               
                return;
            }

            //show/hide paging panel
            SetPagingSetting();

           

        }


      
        private void ChangeDisplayState(bool isEditMode,bool isReloadColumn)
        {
            //DisplayMode mode = DisplayMode.NotSet;
            if (CalculationManager.IsAllEmployeeSavedFinally(this.GetPayrollPeriodId(),false))
            {
                pnlButtons.Visible = false;
                gvw.Columns[gvw.Columns.Count - 1].Visible = false;
                if (!IsPostBack)
                {
                    divWarningMsg.InnerHtml = "Please be sure to change the values in needed case only.";
                    divWarningMsg.Hide = false;
                }
                return;
            }

            int totalEmployees= CalculationManager.GetTotalApplicableForSalary(this.GetPayrollPeriodId(),false);
            int initialSave = calcMgr.GetTotalEmployeeInitialSave(this.GetPayrollPeriodId(),false);
                      
            
            if( totalEmployees==initialSave)
            {
                //btnSave.Visible = false;
               

        
                
            }
            else if (initialSave > 0)
            {
                gvw.Columns[gvw.Columns.Count - 1].Visible = true;
                
               
                btnSave.Visible = true;
            }
            else
            {
                
                btnSave.Visible = true;
             
            }

           

            if(editMode)
            {
                
                btnSave.Visible = true;
            }
        }

      

        #region "Event handlers"




        protected void btnSave_Click(object sender, EventArgs e)
        {
            //if (hiddenEmployeeId.Value == "")
            //{
            //    divErrorMsg.InnerHtml = "Employee must be selected.";
            //    divErrorMsg.Hide = false;
            //    return;
            //}

            //decimal newValue = 0;

            //if (decimal.TryParse(txtNewValue.Text.Trim(), out newValue) == false)
            //{
            //    divErrorMsg.InnerHtml = "Invalid amount";
            //    divErrorMsg.Hide = false;
            //    return;
            //}

            //if (newValue < 0)
            //{
            //    divErrorMsg.InnerHtml = "Invalid negative value.";
            //    divErrorMsg.Hide = false;
            //    return;
            //}

            //string type = ddlTypes.SelectedValue;
            //if (ddlPayrollPeriods.SelectedIndex == -1)
            //    return;


            //ResponseStatus status = CalculationManager.ChangePastSavedSalary(
            //    int.Parse(hiddenEmployeeId.Value), int.Parse(ddlPayrollPeriods.SelectedValue),
            //    int.Parse(type), int.Parse(type), newValue);

         
            //if (status.IsSuccessType)
            //{
            //    msgCtl.InnerHtml = "Salary has been changed.";
            //    msgCtl.Hide = false;

            //    LoadCalculation(false, false);
            //}
            //else
            //{
            //    divErrorMsg.InnerHtml = status.ErrorMessage;
            //    divErrorMsg.Hide = false;
            //}

        }



        protected void btnEdit_Click(object sender, EventArgs e)
        {
            editMode = true;
            LoadCalculation(true, false);
        }
        
        protected void btnAllDelete_Click(object sender, EventArgs e)
        {
            if (calculation != null )
            {
                bool result = calcMgr.DeleteAllCalculation(calculation.CalculationId, ddlPayrollPeriods.SelectedItem.Text);

                if (result)
                {
                    //JavascriptHelper.DisplayClientMsg("Calculation deleted completely.", Page);
                    msgCtl.InnerHtml = Resources.Messages.CalculationCompletelyDeletedMsg;
                    msgCtl.Hide = false;
                    calculation = null;
                }
                else
                {
                    //JavascriptHelper.DisplayClientMsg("Calculation deleted.", Page);
                    msgCtl.InnerHtml = Resources.Messages.CalculationDeletedMsg;
                    msgCtl.Hide = false;
                }



                LoadCalculation(false, true);
                //ChangeDisplayState();

            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            StringBuilder xmlEmployees = new StringBuilder();
            //xmlEmployees.Append("<root>");
            bool employeeAdded = false;
            for (int i = 0; i < this.gvw.Rows.Count; i++)
            {
                GridViewRow row = gvw.Rows[i];
                if (row.FindControl("lblId") == null)
                    continue;

                if (row.FindControl("chkDelete") == null)
                    continue;

                CheckBox chkDelete = (CheckBox)row.FindControl("chkDelete");

                if (chkDelete.Checked)
                {
                    int empId = int.Parse(((Label)row.FindControl("lblId")).Text);

                    if (xmlEmployees.Length == 0)
                        xmlEmployees.Append(empId);
                    else
                        xmlEmployees.Append("," + empId);
                    //xmlEmployees.AppendFormat("<row Id='{0}' />", empId);
                    employeeAdded = true;
                }
            }
            //xmlEmployees.Append("</root>");

            if (calculation != null && employeeAdded)
            {
                bool result = calcMgr.DeleteCalculation(calculation.CalculationId, xmlEmployees.ToString(),ddlPayrollPeriods.SelectedItem.Text);

                if (result)
                {
                    //JavascriptHelper.DisplayClientMsg("Calculation deleted completely.", Page);
                    msgCtl.InnerHtml = Resources.Messages.CalculationCompletelyDeletedMsg;
                    msgCtl.Hide = false;
                    calculation = null;
                }
                else
                {
                    //JavascriptHelper.DisplayClientMsg("Calculation deleted.", Page);
                    msgCtl.InnerHtml = Resources.Messages.CalculationDeletedMsg;
                    msgCtl.Hide = false;
                }


                
                LoadCalculation(false,true);
                //ChangeDisplayState();

            }
        }

        //private void NoSavedSoInitialState()
        //{
        //    if (gvw.Columns[gvw.Columns.Count - 1].HeaderText == "Delete")
        //        gvw.Columns[gvw.Columns.Count - 1].Visible = false;

        //    btnDelete.Enabled = true;

        //    calculation = null;
        //}

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            editMode = false;
            LoadCalculation(false,false);
        }

        
        protected void Page_PreRender(object sender, EventArgs e)
        {
            CalculationManager.RegisterForTextBoxKeysMovement(gvw);
            // If error then hide buttons
            if (!isValidAllValid)
            {
               
                btnSave.Visible = false;
            


                divErrorMsg.InnerHtml = Resources.Messages.CaluclationInvalidNegativeValueMsg;
                divErrorMsg.Hide = false;
                //disable all cells
               CalculationManager.DisableAllCells(gvw);
            }
            //hdnChangedInClientValues.Value = "";

            string jsCodes = string.Format("var payrollId = {0}; var isRetResignedOnly = {1}"
                ,this.GetPayrollPeriodId(),"false");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(),"regpayretreg",jsCodes,true);


         
        }

        #endregion


        #region "Grid Manipulation"

        public static void AddColumns(List<CalcGetHeaderListResult> headers, GridView gvw, PastCalculationAdjCtl calculationControl, bool isReloadColumn)
        {
            //First remove column
            if (gvw.Columns.Count != 3 && isReloadColumn)
            {
                for (int i = gvw.Columns.Count - 2; i>=2; i--)
                {
                    gvw.Columns.RemoveAt(i);
                }
            }

            if (gvw.Columns.Count == 3)
            {
                //first sort column list

                headers = CalculationValue.SortHeaders(headers,PayManager.GetIncomeOrderValues(), PayManager.GetDeductionOrderValues());
                PIncome basicIncome =  new PayManager().GetBasicIncome(SessionManager.CurrentCompanyId);
                
                for (int i = 0; i < headers.Count; i++)
                {
                    TemplateField field = new TemplateField();
                    field.ItemTemplate = new PastCalculationAdjCtlTemplate(DataControlRowType.DataRow, headers[i], calculationControl, basicIncome.IncomeId);
                    field.HeaderTemplate = new PastCalculationAdjCtlTemplate(DataControlRowType.Header, headers[i], calculationControl, basicIncome.IncomeId);
                    gvw.Columns.Insert(gvw.Columns.Count - 1, field);
                }
            }
            
        }

        

        #endregion

        #region "Paging"

        private int _tempCurrentPage;
        private int? _tempCount = 0;

    //    private int payrollPeriodId = 0;

        protected override void OnInit(EventArgs e)
        {
            Page.RegisterRequiresControlState(this);
            base.OnInit(e);
        }
        protected override void LoadControlState(object savedState)
        {
            object[] rgState = (object[])savedState;
            base.LoadControlState(rgState[0]);

            _tempCurrentPage = (int)rgState[1];
            payrollPeriodId = (int)rgState[2];
            editMode = (bool)rgState[3];
        }
        protected override object SaveControlState()
        {
            object[] rgState = new object[4];
            rgState[0] = base.SaveControlState();

            rgState[1] = _tempCurrentPage;
            rgState[2] = payrollPeriodId;
            rgState[3] = editMode;

            return rgState;
        }

        protected void btnPrevious_Click()
        {
            _tempCurrentPage = int.Parse(pagingCtl.LabelCurrentPage.Text);
            _tempCurrentPage -= 1;
            LoadCalculation(false,false);
        }

        protected void btnNext_Click()
        {
            _tempCurrentPage = int.Parse(pagingCtl.LabelCurrentPage.Text);
            _tempCurrentPage += 1;
            LoadCalculation(false,false);
        }
        protected void ddlRecords_SelectedIndexChanged()
        {
            _tempCurrentPage = 1;
            LoadCalculation(false, false);
        }

        protected void ChangePageNumber()
        {
            _tempCurrentPage = this.pagingCtl.CurrentPage;
            LoadCalculation(false, false);
        }

        private void SetPagingSetting()
        {
            if (_tempCount <= 0)
                pagingCtl.Visible = false;
            else
                pagingCtl.Visible = true;

            //calcuation .. of .. page
            int totalPages = (int)Utils.Helper.Util.CalculateTotalPages(_tempCount.Value,
                                                                        int.Parse(pagingCtl.DDLRecords.SelectedValue));
            pagingCtl.LabelTotalPage.Text = (totalPages).ToString();
            pagingCtl.LabelCurrentPage.Text = _tempCurrentPage.ToString();

            // Update Pge numbers
            if (pagingCtl.DDLPageNumber.Items.Count != totalPages)
            {
                pagingCtl.DDLPageNumber.Items.Clear();
                for (int i = 1; i <= totalPages; i++)
                {
                    pagingCtl.DDLPageNumber.Items.Add(i.ToString());
                }
            }
            if (this.pagingCtl.DDLPageNumber.Items.FindByValue(_tempCurrentPage.ToString()) != null)
                this.pagingCtl.DDLPageNumber.SelectedValue = _tempCurrentPage.ToString();

            //show/hide Next/Previous buttons
            if (_tempCurrentPage <= 1)
                pagingCtl.ButtonPrev.Enabled = false;
            else
                pagingCtl.ButtonPrev.Enabled = true;

            if (_tempCurrentPage < totalPages)
                pagingCtl.ButtonNext.Enabled = true;
            else
                pagingCtl.ButtonNext.Enabled = false;
        }
        #endregion

        protected void gvw_RowCreated(object sender, GridViewRowEventArgs e)
        {
            //Display error if attendance is not complete
            if( e.Row.RowType==DataControlRowType.DataRow)
            {
                CalcGetCalculationListResult data = e.Row.DataItem as CalcGetCalculationListResult;

                if (data.IsAttendanceComplete.Value != 1)
                {
                    WebControl ctl = e.Row.FindControl("lblId") as WebControl;
                    ctl.BorderWidth = new Unit(1);
                    ctl.BorderColor = Color.Red;


                    ctl.ToolTip = Resources.Messages.AttendanceNotCompleteForCalculationMsg;


                    isValidAllValid = false;
                }

                if (data.IsSalaryGenerationUnsuccessfull)
                {
                    WebControl ctl = e.Row.FindControl("lblId") as WebControl;
                    ctl.BorderWidth = new Unit(1);
                    ctl.BorderColor = Color.Red;


                    ctl.ToolTip = Resources.Messages.CalcSalaryGenerationFail;


                    isValidAllValid = false;
                }

            }
        }

        protected void gvw_DataBound(object sender, EventArgs e)
        {
             CalculationManager.CreateGroupInHeader(sender
                 , gvw.Columns[gvw.Columns.Count-1 ].Visible );
        }

       
        


    }

}