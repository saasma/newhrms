﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Manager;
using Utils.Helper;
using BLL;
using DAL;
using Utils.Web;
using System.Text;
using BLL.BO;

namespace Web.UserControls
{
    public partial class ChangeSkillSetToEmployee : System.Web.UI.UserControl
    {
        CommonManager commonMgr = new CommonManager();
        List<EmployeeSkillSetBO> source = null;
        public bool IsDisplayedAsPopup
        {
            get
            {
                if (Request.QueryString["isPopup"] != null)
                    return true;
                return false;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            BindData();

            if (!IsPostBack)
                Initialise();
            Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "clearUnload();");
        }

        void Initialise()
        {
            LoadDesignations();
           
        }

        void BindData()
        {
            CommonManager commonMgr = new CommonManager();
            storeSkillSet.DataSource = commonMgr.GetAllSkillSet().OrderBy(x => x.Name).ToList();
            storeSkillSet.DataBind();

            storeLevel.DataSource = CommonManager.GetCompetencyLevels().OrderBy(x => x.Order).ToList();
            storeLevel.DataBind();
        }

        public int GetEmployeeId()
        {
            if (SessionManager.CurrentLoggedInEmployeeId != 0)
                return SessionManager.CurrentLoggedInEmployeeId;
            else
                return int.Parse(Request.QueryString["EmployeeId"].ToString());
        }

        void LoadDesignations()
        {
            source = CommonManager.GetEmployeesSkillSet(GetEmployeeId());
            gvwBranches.DataSource = source;
            gvwBranches.DataBind();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                SkillSetEmployee entity = new SkillSetEmployee();

                if (gvwBranches.SelectedIndex == -1)
                {
                    //entity.Name = txtName.Text.Trim();

                    entity.EmployeeId = GetEmployeeId();
                    entity.SkillSetId = int.Parse(ddlSkillSet.SelectedItem.Value);

                    if (ddlLevel.SelectedItem == null || ddlLevel.SelectedItem.Value == null || ddlLevel.SelectedItem.Value == "")
                        entity.LevelID = null;
                    else
                        entity.LevelID = int.Parse(ddlLevel.SelectedItem.Value);

                    CommonManager.AddEditSkillSetToEmployee(entity);

                    divMsgCtl.InnerHtml = "Saved";

                }
                else
                {
                    entity.EmployeeId = GetEmployeeId();
                    entity.SkillSetId = int.Parse(ddlSkillSet.SelectedItem.Value);

                    if (ddlLevel.SelectedItem == null || ddlLevel.SelectedItem.Value == null)
                        entity.LevelID = null;
                    else
                        entity.LevelID = int.Parse(ddlLevel.SelectedItem.Value);

                    CommonManager.AddEditSkillSetToEmployee(entity);

                    divMsgCtl.InnerHtml = "Updated";
                }

                divMsgCtl.Hide = false;

                
                gvwBranches.SelectedIndex = -1;
                LoadDesignations();

                ClearFields();
                
            }
        }

        protected void gvwBranches_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvwBranches.PageIndex = e.NewPageIndex;
            gvwBranches.SelectedIndex = -1;
            LoadDesignations();
            ClearFields();

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            gvwBranches.SelectedIndex = -1;
            LoadDesignations();
            ClearFields();
            details.Visible = false;
        }

        protected void gvwBranches_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            CommonManager.ResetCache();
            int id = (int)gvwBranches.DataKeys[e.RowIndex][0];
            int empId = (int)gvwBranches.DataKeys[e.RowIndex][1];
            if (id != 0)
            {
                SkillSetEmployee entity = new SkillSetEmployee();
                entity.SkillSetId = id;
                entity.EmployeeId = empId;

                 CommonManager.DeleteSkillSetToEmployee(entity);


              
                    divMsgCtl.InnerHtml = "Skill Set deleted.";
                    LoadDesignations();
                    ClearFields();
               
                divMsgCtl.Hide = false;

            }
           
        }

        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            LoadDesignations();
            ClearFields();
            details.Visible = true;
            ddlLevel.SelectedItem.Value = "";
            ddlSkillSet.SelectedItem.Value = "";
        }

        protected void gvwBranches_SelectedIndexChanged(object sender, EventArgs e)
        {
          

             int id = (int)gvwBranches.DataKeys[gvwBranches.SelectedIndex][0];
            int empId = (int)gvwBranches.DataKeys[gvwBranches.SelectedIndex][1];

            SkillSetEmployee branch = CommonManager.GetEmployeeSkillSetId(id,empId);
            if (branch != null)
            {

                ddlSkillSet.SetValue(branch.SkillSetId);
                if (branch.LevelID == null)
                    ddlLevel.SelectedItem.Value = "";
                else
                    ddlLevel.SetValue(branch.LevelID);
                
                
                btnSave.Text = Resources.Messages.Update;
                details.Visible = true;
              //  ddlSkillSet.Focus();
            }
        }

        void ClearFields()
        {
            ddlSkillSet.SelectedItem.Value = "";
            ddlLevel.SelectedItem.Value = "";
            ddlLevel.SelectedItem.Text = "";
            btnSave.Text = Resources.Messages.Save;
        }

        
        protected void Page_PreRender(object sender, EventArgs e)
        {
            //output all as js array to be updatable in parent window
            //if (IsDisplayedAsPopup && source != null)
            {
                //Page.ClientScript.

                string code =  string.Format("var data='{0}';",CommonManager.GetEmployeeSkillSetHTML(GetEmployeeId()));

                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdfdsf", code,true);


            }
        }
        
    }
}