﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ManualAttendanceCtl.ascx.cs"
    Inherits="Web.UserControls.ManualAttendanceCtl" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<div class="contentArea">
    <style>
        .custom-icon1
        {
            background-image: url(arrow-down.gif) !important;
        }
        
        .x-label-value
        {
            font-size: 16px;
            color: #47759E;
            text-shadow: 1px 1px 1px rgba(255, 255, 255, 0.50);
        }
        .x-column-header-inner
        {
            background: #F9F9F9;
            color: #575655;
        }
        
        .x-grid-cell-inner
        {
            height: 30px;
            vertical-align: central;
        }
        
        .x-btn-default-small
        {
            background: #70AD47;
            border: 1px solid #548235;
            font-size: 18px;
            font-weight: normal;
            border-radius:0px;
        }
        
        
        h1
        {
            padding: 30px 0px 10px 0px;
        }
        
        #ctl00_mainContent_ctl00_btnNoteAdd1
        {
            width: 70px;
        }
        
        #txtNote
        {
            height: 40px !important;
        }
        
        #ctl00_mainContent_ctl00_txtInNote-inputEl, #ctl00_mainContent_ctl00_txtOutNote-inputEl
        {
            height: 40px !important;
            border-radius:0px;
        }
        
        #ClockButton
        {
            font-family: Arial;
            color: #ffffff;
            font-size: 20px;
            background: #F9F9F9;
            background-image: none !important;
            padding: 11px 20px 10px 20px;
            border: solid #D9D9D9 1px;
            text-decoration: none;
            border-radius:0px;
        }

      
        #GridPanelMYTimesheet .x-column-header
        {
            display: none !important;
        }

        /*#ctl00_mainContent_ctl00_btn2, #ctl00_mainContent_ctl00_Button1{
            margin-right:-53px;
            padding-left: 15px;
            padding-right: 10px;
            margin-left: 22px;
            padding-top:10px;*/

        }


       

        .smallbuttonOnLeft {
            background: #F2F2F2;
            height: 38px;
            padding: 12px 15px;

            top: 1px;
            text-decoration: none;
            color: gray;
            left: 21px;
            border-radius:0px;
            font-weight:normal;
        }
           


        #ctl00_mainContent_ctl00_btn2, #ctl00_mainContent_ctl00_btn1{
            left: 21px;
            padding: 11px 15px!important;
            
        }

        #ctl00_mainContent_ctl00_btnCheckOut, #ctl00_mainContent_ctl00_btnCheckIn{
            left: -80px;
        }

        .x-column-header-text{
            color: #428BCA;
            font-size:13px;
            font-weight:bold;
            /*text-transform: uppercase;*/
        }

        #ctl00_mainContent_ctl00_Toolbar1{
            background: #D1EDFF;
            border-bottom: 1px solid #0D99CD;
        }
        
    </style>
    <script type="text/javascript">

       var CommandHandler = function (command, record) {
           var LineID = record.data.LineID;
           var Note = record.data.Note;
           if (command == "EDITIN") {
                <%=Hidden_CINoteID.ClientID %>.setValue(LineID);
                <%=txtInNote.ClientID %>.setValue(Note);
           }
           else { 
                <%=Hidden_CONoteID.ClientID %>.setValue(LineID);
                <%=txtOutNote.ClientID %>.setValue(Note);
           }

           //Ext.Msg.alert(command, <%=Hidden_CONoteID.ClientID %>.getValue());
       };


       var CommandHandler1 = function (command, record) {
            
            
            

            var DateEng = record.data.DateEng;
            var ActualDate = record.data.ActualDate;
            <%= SelectedDate.ClientID %>.setText(ActualDate);
            <%= Hidden_SelectedDate.ClientID %>.setValue(DateEng);

            <%= btnViewActivity.ClientID %>.fireEvent('click');


          
       };
    </script>
    <ext:ResourceManager ID="ResourceManager1" runat="server" />
    <h3>
        Your Attendance</h3>
    <div style="width: 100%;">
        <div style="width: 45%; float: left;">
            <ext:LinkButton ID="btnViewActivity" runat="server" Hidden="true">
                <DirectEvents>
                    <Click OnEvent="btnViewActivity_Click">
                        <EventMask ShowMask="true" />
                    </Click>
                </DirectEvents>
            </ext:LinkButton>
            <ext:Hidden ID="Hidden_CINoteID" runat="server" />
            <ext:Hidden ID="Hidden_CONoteID" runat="server" />
            <table>
                <tr>
                    <td colspan="2">
                        <div style="width: 470px; background: #D1EDFF; padding: 10px 15px 2px 15px; margin-bottom: 15px;
                            border-bottom: 1px solid #C0C0C0;">
                            <ext:Label ID="lblCurrentDateTIme1" runat="server">
                            </ext:Label>
                            <ext:Label ID="lblCurrentTIme1" runat="server">
                            </ext:Label>
                            <ext:Hidden ID="Hidden_lblCurrentDateTIme1" runat="server">
                            </ext:Hidden>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:Button ID="btn1" runat="server" Text=" " Height="40" Width="50" Icon="Clock"
                            Cls="ClockButton" OverCls="ClockButton" PaddingSpec="0 0 0 21">
                        </ext:Button>
                    </td>
                    <td>
                        <ext:Button ID="btnCheckIn" runat="server" Text="Clock In" Width="100" Height="40">
                            <DirectEvents>
                                <Click OnEvent="btnCheckIn_Click">
                                    <EventMask ShowMask="true">
                                    </EventMask>
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div style="width: 470px; padding: 0px;">
                            <ext:GridPanel ID="InNoteGridPanel" runat="server" Header="false" HeaderPosition="None">
                                <Store>
                                    <ext:Store ID="storeInNote" runat="server">
                                        <Model>
                                            <ext:Model ID="modelInNote" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="LineID" Type="String">
                                                    </ext:ModelField>
                                                    <ext:ModelField Name="ChkInOutID" Type="String">
                                                    </ext:ModelField>
                                                    <ext:ModelField Name="Note" Type="String">
                                                    </ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:Column ID="ColumnIN" runat="server" DataIndex="Note" Text=" " Width="471" MenuDisabled="true">
                                        </ext:Column>
                                        <ext:ImageCommandColumn ID="CommandColumnIN" runat="server" Width="25" Sortable="false">
                                            <Commands>
                                                <ext:ImageCommand Icon="ApplicationEdit" ToolTip-Text="Edit Comment" CommandName="EDITIN">
                                                    <ToolTip Text="Edit" />
                                                </ext:ImageCommand>
                                            </Commands>
                                            <Listeners>
                                                <Command Handler="CommandHandler(command,record);" />
                                            </Listeners>
                                        </ext:ImageCommandColumn>
                                    </Columns>
                                </ColumnModel>
                                <BottomBar>
                                    <ext:StatusBar ID="StatusBar1" runat="server" PaddingSpec="25 0 25 0">
                                        <Items>
                                            <ext:Button ID="Button2" runat="server" Text=" " Height="40" Width="50" Icon="Note"
                                                Cls="smallbuttonOnLeft" OverCls="ClockButton">
                                            </ext:Button>
                                            <ext:TextField ID="txtInNote" FieldStyle="border:1px solid lightblue!important;"
                                                runat="server" Width="275" >
                                            </ext:TextField>
                                            <ext:Button ID="btnNoteAdd1" runat="server" Width="100" Height="30" Text="Save Note" Cls="smallbuttonOnLeft">
                                                <DirectEvents>
                                                    <Click OnEvent="CheckInComment_Click">
                                                        <EventMask ShowMask="true">
                                                        </EventMask>
                                                    </Click>
                                                </DirectEvents>
                                            </ext:Button>
                                        </Items>
                                    </ext:StatusBar>
                                </BottomBar>
                            </ext:GridPanel>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <%--<ext:LinkButton ID="btn22" runat="server" Text=" " Icon="Clock">
                                    </ext:LinkButton>--%>
                        <ext:Button ID="btn2" runat="server" Text=" " Height="40" Width="50" Icon="Clock"
                            Cls="ClockButton" OverCls="ClockButton">
                        </ext:Button>
                    </td>
                    <td>
                        <ext:Button ID="btnCheckOut" runat="server" Text="Clock Out" Width="100" Height="40">
                            <DirectEvents>
                                <Click OnEvent="btnCheckOut_Click">
                                    <EventMask ShowMask="true">
                                    </EventMask>
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div style="width: 470px; padding-top: 0px;">
                            <ext:GridPanel ID="OutNoteGridPanel" runat="server" Header="false" HeaderPosition="None">
                                <Store>
                                    <ext:Store ID="storeOutNote" runat="server">
                                        <Model>
                                            <ext:Model ID="model1" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="LineID" Type="String">
                                                    </ext:ModelField>
                                                    <ext:ModelField Name="ChkInOutID" Type="String">
                                                    </ext:ModelField>
                                                    <ext:ModelField Name="Note" Type="String">
                                                    </ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:Column ID="ColumnOut" runat="server" DataIndex="Note" Text=" " Width="471" MenuDisabled="true">
                                        </ext:Column>
                                        <ext:ImageCommandColumn ID="CommandColumnOut" runat="server" Width="25" Sortable="false">
                                            <Commands>
                                                <ext:ImageCommand Icon="ApplicationEdit" ToolTip-Text="Edit Comment" CommandName="EDITOUT">
                                                    <ToolTip Text="Edit" />
                                                </ext:ImageCommand>
                                            </Commands>
                                            <Listeners>
                                                <Command Handler="CommandHandler(command,record);" />
                                            </Listeners>
                                        </ext:ImageCommandColumn>
                                    </Columns>
                                </ColumnModel>
                                <BottomBar>
                                    <ext:StatusBar ID="StatusBar2" runat="server" PaddingSpec="20 0 20 0">
                                        <Items>
                                            <ext:Button ID="Button3" runat="server" Text=" " Height="40" Width="50" Icon="Note"
                                                Cls="smallbuttonOnLeft" OverCls="ClockButton">
                                            </ext:Button>
                                            <ext:TextField ID="txtOutNote" runat="server" FieldStyle="border:1px solid lightblue!important;" Width="275">
                                            </ext:TextField>
                                            <ext:Button ID="Button1" runat="server" Width="100" Text="Save Note" Cls="smallbuttonOnLeft">
                                                <DirectEvents>
                                                    <Click OnEvent="CheckOutComment_Click">
                                                        <EventMask ShowMask="true">
                                                        </EventMask>
                                                    </Click>
                                                </DirectEvents>
                                            </ext:Button>
                                        </Items>
                                    </ext:StatusBar>
                                </BottomBar>
                            </ext:GridPanel>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div style="width: 55%; float: left;">
            <table>
                <tr>
                    <td colspan="2">
                        <ext:GridPanel ID="GridPanelMYTimesheet" runat="server" Width="520" Header="true">
                            <TopBar>
                                <ext:Toolbar ID="Toolbar1" runat="server">
                                    <Items>
                                        <ext:ComboBox ID="cmbLastXDays" runat="server" LabelAlign="Left" Width="300" FieldLabel="My Timesheet"
                                            StyleSpec="margin-left:90px; margin-bottom:7px;">
                                            <Items>
                                                <ext:ListItem Text="Last 7 Days" Value="7" />
                                                <ext:ListItem Text="This Month" Value="30" />
                                                <ext:ListItem Text="Last Month" Value="60" />
                                            </Items>
                                            <DirectEvents>
                                                <Change OnEvent="xDays_Change">
                                                    <EventMask ShowMask="true">
                                                    </EventMask>
                                                </Change>
                                            </DirectEvents>
                                        </ext:ComboBox>
                                    </Items>
                                </ext:Toolbar>
                            </TopBar>
                            <Store>
                                <ext:Store ID="storeMYTimesheet" runat="server">
                                    <Model>
                                        <ext:Model ID="model2" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="DateEng" Type="String">
                                                </ext:ModelField>
                                                <ext:ModelField Name="ActualDate" Type="String">
                                                </ext:ModelField>
                                                <ext:ModelField Name="RefinedInRemarks" Type="String">
                                                </ext:ModelField>
                                                <ext:ModelField Name="RefinedOutRemarks" Type="String">
                                                </ext:ModelField>
                                                <ext:ModelField Name="RefinedWorkHour" Type="String">
                                                </ext:ModelField>
                                                <ext:ModelField Name="DayType" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <ColumnModel>
                                <Columns>
                                    <ext:Column ID="Column5" runat="server" DataIndex="DayType" Text="Type" Width="100"
                                        MenuDisabled="true">
                                    </ext:Column>
                                    <ext:Column ID="Column1" runat="server" DataIndex="ActualDate" Text="Day" Width="120"
                                        MenuDisabled="true">
                                    </ext:Column>
                                    <ext:Column ID="Column2" runat="server" DataIndex="RefinedInRemarks" Text="In" Width="80"
                                        MenuDisabled="true">
                                    </ext:Column>
                                    <ext:Column ID="Column3" runat="server" DataIndex="RefinedOutRemarks" Text="Out"
                                        Width="80" MenuDisabled="true">
                                    </ext:Column>
                                    <ext:Column ID="Column4" runat="server" DataIndex="RefinedWorkHour" Text="Work Hour"
                                        Width="110" MenuDisabled="true">
                                    </ext:Column>
                                    <ext:ImageCommandColumn ID="ImageCommandColumn3" runat="server" Width="25" Sortable="false">
                                        <Commands>
                                            <ext:ImageCommand Icon="ApplicationEdit" ToolTip-Text="Edit Comment" CommandName="EDITIN">
                                                <ToolTip Text="Add Comment" />
                                            </ext:ImageCommand>
                                        </Commands>
                                        <Listeners>
                                            <Command Handler="CommandHandler1(command,record);" />
                                        </Listeners>
                                    </ext:ImageCommandColumn>
                                </Columns>
                            </ColumnModel>
                        </ext:GridPanel>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <ext:Hidden ID="Hidden_CIN" runat="server">
    </ext:Hidden>
    <ext:Hidden ID="Hidden_COUT" runat="server">
    </ext:Hidden>
    <uc2:MsgCtl ID="msgCtl" EnableViewState="false" Hide="true" runat="server" />
    <uc2:MsgCtl ID="WarningCtl" EnableViewState="false" Hide="true" runat="server" />
    <fieldset class="manage_changepassword" style='background: #E7F1FD; visibility: hidden;'>
        <table class="" style="width: 400px">
            <tr runat="server" id="rowCurrentPassword">
                <td class="fieldHeader">
                    <strong>Check In/Out</strong>
                </td>
                <td>
                    <asp:DropDownList Width="120" ID="ddlCheckInOut" runat="server">
                        <asp:ListItem Value="0" Text="Check In">
                        </asp:ListItem>
                        <asp:ListItem Value="1" Text="Check Out">
                        </asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td height="10">
                </td>
            </tr>
            <tr>
                <td class="fieldHeader">
                    <strong>Date</strong>
                </td>
                <td>
                    <asp:Label ID="lblDate" runat="server" />
                </td>
            </tr>
            <tr>
                <td height="10">
                </td>
            </tr>
            <tr>
                <td class="fieldHeader">
                    <strong>Time</strong>
                </td>
                <td>
                    <asp:Label ID="lblTime" runat="server" />
                </td>
            </tr>
            <tr>
                <td height="10">
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td class="style2" style='padding-top: 15px;'>
                    <asp:Button ID="btnChange" CssClass="save" OnClientClick="return confirm('Confirm save the attendance');"
                        runat="server" Text="Save" OnClick="btnChange_Click" />
                </td>
            </tr>
        </table>
    </fieldset>
</div>
<ext:TaskManager ID="TaskManager1" runat="server">
    <Tasks>
        <ext:Task>
            <Listeners>
                <Update Handler="#{lblCurrentTIme1}.setText(Ext.Date.format(new Date(), 'H:i A')); #{Hidden_lblCurrentDateTIme1}.setText(Ext.Date.format(new Date(), 'H:i A'));" />
            </Listeners>
        </ext:Task>
    </Tasks>
</ext:TaskManager>
<ext:Window ID="windAddComment" MinWidth="480" MinHeight="320" Title="Attendance Comment"
    Closable="true" Resizable="false" Hidden="false" X="-1000" Y="-1000" runat="server"
    AutoScroll="true">
    <Content>
        <div>
            <ext:Hidden ID="Hidden_SelectedDate" runat="server" />
            <table>
                <tr>
                    <td colspan="4">
                        <div style="width: 100%; margin: 7px; background: #F9F9F9;">
                            <ext:Label runat="server" ID="SelectedDate" Text="Date" MarginSpec="17 7 7 7" Cls="Date"
                                Style="font: 5em !important; font-weight: bold;" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div>
                            <ext:Label runat="server" ID="CheckInTime" Text="Check In Time" MarginSpec="7 7 7 7" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <ext:TextField ID="txtNote1" FieldStyle="border:1px solid lightblue" runat="server"
                            Width="275" Height="40" MarginSpec="7 7 7 7" Cls="txtNote" FieldBodyCls="txtNote"
                            FormItemCls="txtNote">
                        </ext:TextField>
                    </td>
                    <td>
                        <ext:Button ID="btnComment1" runat="server" Width="100" Text="Save Note" MarginSpec="7 7 7 7"
                            Height="40">
                            <DirectEvents>
                                <Click OnEvent="CheckInCommentNew_Click">
                                    <EventMask ShowMask="true">
                                    </EventMask>
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div>
                            <ext:Label runat="server" ID="CheckOutTime" Text="Check Out Time" MarginSpec="7 7 7 7" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <ext:TextField ID="txtNote2" FieldStyle="border:1px solid lightblue" runat="server"
                            Width="275" Height="40" MarginSpec="7 7 7 7" Cls="txtNote" FieldBodyCls="txtNote"
                            FormItemCls="txtNote">
                        </ext:TextField>
                    </td>
                    <td>
                        <ext:Button ID="btnComment2" runat="server" Width="100" Text="Save Note" MarginSpec="7 7 7 7"
                            Height="40">
                            <DirectEvents>
                                <Click OnEvent="CheckInCommentNew_Click">
                                    <EventMask ShowMask="true">
                                    </EventMask>
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                    </td>
                </tr>
            </table>
        </div>
    </Content>
</ext:Window>
