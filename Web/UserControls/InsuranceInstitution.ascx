﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InsuranceInstitution.ascx.cs"
    Inherits="Web.UserControls.InsuranceInstitution" %>

<script type="text/javascript">
    function ConfirmInstitutionDeletion() {
        var ddllist = document.getElementById('<%= ddlInstitution.ClientID %>');
        if (ddllist.selectedIndex == -1) {
            alert('No Insurance company to delete.');
            clearUnload();
            return false;
        }
        if (confirm('Do you want to delete the insurance company?')) {
            clearUnload();
            return true;
        }
        else {
            return false;
        }
    }

    function DisplayInstitutionInTextBox(dropdown_id) {
        var ddllist = document.getElementById(dropdown_id);
        document.getElementById('<%= txtInstitutionRename.ClientID %>').value = ddllist.options[ddllist.selectedIndex].text;
    }



    //capture window closing event
    window.onunload = closePopup;

    function closePopup() {
        clearUnload();
       // if ($.browser.msie == false && typeof (window.opener.parentReloadCallbackFunction) != 'undefined' && ($.browser.chrome || $.browser.safari)) {
            window.opener.parentReloadCallbackFunction(window, texts);
//        } else {
//            if (typeof (texts) != 'undefined')
//                window.returnValue = texts;
//            window.close();
//        }
    }

    function clearUnload() {
        window.onunload = null;
    }
</script>

<div class="popupHeader">
    <span style='margin-left: 20px; padding-top: 8px; display: block'>Manage Insurance Company</span>
</div>
<div class="marginal">
    <%--<div class="bevel ">--%>
        <table cellpadding="0" cellspacing="0" class="alignttable" width="600px">
            <tr>
                <td class="lblingText" width="120px">
                    New Insurance company
                </td>
                <td>
                    <asp:TextBox ID="txtNewInstitution" runat="server" Width="159px" MaxLength="100"
                        EnableViewState="false"></asp:TextBox>
                    <asp:RequiredFieldValidator ValidationGroup="Name1" ID="RequiredFieldValidator3"
                        Display="None" runat="server" ErrorMessage="Insurance company is required." ControlToValidate="txtNewInstitution">*</asp:RequiredFieldValidator>
                    <asp:Button ID="btnAddInstitutionName"  CssClass="smalladd" style="margin:0px 0 0 5px"  OnClientClick="valGroup='Name1';return CheckValidation()"
                        runat="server" Text="Add" OnClick="btnAddInstitution_Click" />
                </td>
            </tr>
            <tr>
                <td class="lblingText" width="120px">
                    Rename Insurance company
                </td>
                <td>
                    <asp:TextBox ID="txtInstitutionRename" runat="server" Width="159px" MaxLength="100"></asp:TextBox>
                    <asp:RequiredFieldValidator ValidationGroup="Name2" ID="RequiredFieldValidator4"
                        runat="server" ErrorMessage="Insurance company is required." ControlToValidate="txtInstitutionRename"
                        Display="None">*</asp:RequiredFieldValidator>
                    <asp:Button ID="btnRename"  CssClass="smallupdate" style="margin:0px 0 0 5px;"  OnClientClick="valGroup='Name2';return CheckValidation();"
                        runat="server" Text="Update" ValidationGroup="Name2" OnClick="btnRename_Click" />
                </td>
            </tr>
            <tr>
                <td class="lblingText" width="120px">
                    Insurance company list
                </td>
                <td>
                 <asp:DropDownList ID="ddlInstitution" runat="server" Width="165px" CssClass="rightMargin"
                        DataValueField="Id" DataTextField="InstitutionName" onchange="DisplayInstitutionInTextBox(this.id);">
                    </asp:DropDownList>
                   
                        <asp:Button ID="btnDelete" runat="server"  CssClass="delete" style="margin:0px 0 0 5px" Text="Delete" OnClientClick="javascript:return ConfirmInstitutionDeletion();"
                        OnClick="btnDelete_Click" />
                    
                </td>
            </tr>
        </table>
        <%--<% if(IsDisplayedAsPopup){ %>
<input id="btnClose" type="button" value="Close" onclick="closePopup()" />
<%} %>--%>
    <%--</div>--%>
</div>
