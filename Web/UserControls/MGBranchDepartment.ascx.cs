using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Base;
using BLL.Manager;
using DAL;
using Utils.Helper;
using Utils;
using Utils.Web;
using System.Text;

namespace Web.UserControls
{
    public partial class MGBranchDepartment : BaseUserControl
    {
        CompanyManager compMgr = new CompanyManager();
        BranchManager branchMgr = new BranchManager();
        CommonManager commonMgr = new CommonManager();
        Branch bra = new Branch();
        List<Branch> source = new List<Branch>();
        List<Department> departmentList = new List<Department>();

        public bool IsDisplayedAsPopup
        {
            get
            {
                if (Request.QueryString["isPopup"] != null)
                    return true;
                return false;
            }
        }

        protected void gvwEmployees_RowCreated(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover",
                "this.originalClass=this.className;this.className='selected'");

                e.Row.Attributes.Add("onmouseout",
                "this.className=this.originalClass;");


            }

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            departmentList = DepartmentManager.GetAllDepartments().OrderBy(x => x.Name).ToList();

            divMsgCtl.DataBind();
            divWarningMsg.DataBind();

       

            if (!IsPostBack)
            {
                Initialise();

            }

            if (Request.Form["__EVENTTARGET"] != null && Request.Form["__EVENTTARGET"].Equals("Reload"))
                LoadBranches();

            JavascriptHelper.AttachPopUpCode(Page, "popupBrDept", "ManageBranchDeptPopup.aspx", 700, 500);

            if (gvwBranches.HeaderRow != null)
                gvwBranches.HeaderRow.TableSection = TableRowSection.TableHeader;

            Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "clearUnload();");
        }


   


        void Initialise()
        {
            


            LoadBranches();
            //LoadDistrict();
        }

        void LoadBranches()
        {
            source = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);

            foreach (Branch branch in source)
            {
                branch.DepartmentList = BranchManager.GetDepartmentListTextForBranch(branch.BranchId);
                branch.DepartmentList = branch.DepartmentList.Replace(",", ", ");
            }

            gvwBranches.DataSource = source;
            gvwBranches.DataBind();
        }

        



        


        protected void gvwBranches_SelectedIndexChanged(object sender, EventArgs e)
        {
            int selBranchId = (int)gvwBranches.DataKeys[gvwBranches.SelectedIndex][0];
            Branch branch = branchMgr.GetById(selBranchId);
            if (branch != null)
            {

                List<Department> source = departmentList;

                List<GetDepartmentListResult> dest = new List<GetDepartmentListResult>();

                foreach (BranchDepartment department in branch.BranchDepartments)
                {
                    Department dep = source.FirstOrDefault(x => x.DepartmentId == department.DepartmentID);
                    if (dep != null)
                    {
                        dest.Add(new GetDepartmentListResult { DepartmentId = dep.DepartmentId, Name = dep.Name });

                        source.Remove(dep);
                    }
                }

                branchTitle.InnerHtml = "Add Departments to branch \"" + branch.Name + "\"";

                lstSource.DataSource = source;
                lstSource.DataBind();
                lstDest.DataSource = dest;
                lstDest.DataBind();


                btnSave.Text = Resources.Messages.Update;
                details.Visible = true;
                

            }
        }

        void ClearBranchFields()
        {
            chkToAll.Checked = false;

            lstSource.DataSource = departmentList;
            lstSource.DataBind();

            lstDest.DataSource = new List<GetDepartmentListResult>();
            lstDest.DataBind();

            btnSave.Text = Resources.Messages.Save;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {





            if (gvwBranches.SelectedIndex != -1)
            {
                Branch b = new Branch();
                int selBranchId = (int)gvwBranches.DataKeys[gvwBranches.SelectedIndex][0];
                b.BranchId = selBranchId;
                b.CompanyId = SessionManager.CurrentCompanyId;

                foreach (ListItem item in lstDest.Items)
                {
                    b.BranchDepartments.Add(new BranchDepartment { DepartmentID = int.Parse(item.Value) });
                }

                Status status = BranchManager.SaveUpdateDepartmentsToBranch(b,chkToAll.Checked);

                if (status.IsSuccess)
                {
                    divMsgCtl.InnerHtml = Resources.Messages.BranchUpdatedMsg;
                    divMsgCtl.Hide = false;
                }
                else
                {
                    divWarningMsg.InnerHtml = status.ErrorMessage;
                    divWarningMsg.Hide = false;
                    return;
                }
                //JavascriptHelper.DisplayClientMsg("Branch information updated.", Page);
            }


            CommonManager.ResetCache();
            gvwBranches.SelectedIndex = -1;
            LoadBranches();
            ClearBranchFields();


        }

        protected void gvwBranches_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvwBranches.PageIndex = e.NewPageIndex;
            gvwBranches.SelectedIndex = -1;
            LoadBranches();
            ClearBranchFields();
            
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            gvwBranches.SelectedIndex = -1;
            LoadBranches();
            ClearBranchFields();
            details.Visible = false;
        }

       



        protected void Page_PreRender(object sender, EventArgs e)
        {
           


        }

        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            LoadBranches();
            ClearBranchFields();
            details.Visible = true;

        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            for (int i = lstSource.Items.Count - 1; i >= 0; i--)
            {
                ListItem item = lstSource.Items[i];
                if (item.Selected)
                {
                    lstSource.Items.Remove(item);

                    lstDest.Items.Add(item);
                }
            }
        }

        protected void btnRemove_Click(object sender, EventArgs e)
        {
            for (int i = lstDest.Items.Count - 1; i >= 0; i--)
            {
                ListItem item = lstDest.Items[i];
                if (item.Selected)
                {
                    lstDest.Items.Remove(item);

                    lstSource.Items.Add(item);
                }
            }
        }

        protected void btnAddAll_Click(object sender, EventArgs e)
        {
            lstSource.DataSource = new List<GetDepartmentListResult>();
            lstSource.DataBind();

            lstDest.DataSource = departmentList;
            lstDest.DataBind();
        }

        protected void btnRemoveAll_Click(object sender, EventArgs e)
        {
            lstSource.DataSource = departmentList; 
            lstSource.DataBind();

            lstDest.DataSource = new List<GetDepartmentListResult>();
            lstDest.DataBind();
        }
    }
}