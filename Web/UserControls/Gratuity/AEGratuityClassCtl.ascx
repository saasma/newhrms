﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AEGratuityClassCtl.ascx.cs"
    Inherits="Web.UserControls.Gratuity.AEGratuityClassCtl" %>
<%@ Register Src="~/Controls/ErrorMsgCtl.ascx" TagName="ErrorMsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningMsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<script type="text/javascript">

    function closePopup() {
        window.returnValue = "Reload";
        window.close();

        // alert(window.opener.parentReloadCallbackFunction)
        if (isNonModalBrowser() && typeof (window.opener.parentIncomeCallbackFunction)) {
            window.opener.refresh(window);
        } else {
            window.returnValue = "Reload";
            window.close();
        }
    }

    
</script>
<div class="popupHeader">
    <h2 class="headlinespop">
        Gratuity Class</h2>
</div>
<div style="margin-left: 15px">
    <uc2:WarningMsgCtl Width="300px" ID="divWarningMsg" EnableViewState="false" Hide="true"
        runat="server" />
</div>
<fieldset style='margin-top: 15px'>
    <table>
        <tr>
            <td class="lf">
                Name
            </td>
            <td>
                <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ValidationGroup="Gratuity" ID="RequiredFieldValidator3"
                    Display="None" runat="server" ErrorMessage="Name is required." ControlToValidate="txtName">*</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="lf">
                Default
            </td>
            <td>
                <asp:CheckBox ID="chkIsDefault" runat="server" />
            </td>
        </tr>
    </table>
    <br />
    <asp:Button ID="btnSave" CssClass="save" ValidationGroup="Gratuity" OnClientClick="valGroup='Gratuity';return CheckValidation();"
        runat="server" Text="Save" OnClick="btnSave_Click" />
    <input id="Button1" type="button" class="cancel" value="Cancel" onclick="window.close();" />
    <%--for displaying __doPostBack--%>
    <asp:LinkButton Style="display: none" ID="LinkButton1" runat="server">LinkButton</asp:LinkButton>
</fieldset>
