﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Base;
using BLL.Manager;
using Utils.Web;
using DAL;
using Utils.Helper;

namespace Web.UserControls.Gratuity
{
    public partial class AEAutoOvertimeGroup : BaseUserControl
    {
        PayManager payMgr = new PayManager();
        CommonManager commonMgr = new CommonManager();
        

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialise();
            }
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {

            rowMinHourForWorkday.Style["display"] = "none";
            rowMinHourForHoliday.Style["display"] = "none";
            rowGraceMin.Style["display"] = "none";
            rowStandardMin.Style["display"] = "none";

            if (ddlCalculationType.SelectedItem.Value == "1")
            {
                rowMinHourForWorkday.Style["display"] = "";
                rowMinHourForHoliday.Style["display"] = "";
            }
            else
            {
                rowGraceMin.Style["display"] = "";
                rowStandardMin.Style["display"] = "";
            }
        }
        void Initialise()
        {

            cboLevelList.DataSource = NewPayrollManager.GetAllParentLevelList().OrderBy(x => x.Order).ToList();
            cboLevelList.DataBind();
          

            int gid = UrlHelper.GetIdFromQueryString("Id");
            if (gid != 0)
            {
                OvertimeAutoGroup gratuity = commonMgr.GetOTGroup(gid);
                if (gratuity != null)
                {
                    this.ControlID = gid;
                    Process(ref gratuity);
                    btnSave.Text = Resources.Messages.Update;
                }
            }

        }

        void Process(ref OvertimeAutoGroup entity)
        {
            if (entity == null)
            {
                entity = new OvertimeAutoGroup();
                entity.Name = txtName.Text.Trim();
                entity.IsOTAllowed = chkIsOTAllowed.Checked;
                entity.GraceMinuteToBeAddedInStandardHour = int.Parse(txtGraceMinute.Text.Trim() == "" ? "0" : txtGraceMinute.Text.Trim());

                entity.CalculationType = int.Parse(ddlCalculationType.SelectedValue);


                entity.MaxOTMinuteInADay = int.Parse(txtMaxOTMin.Text.Trim() == "" ? "0" : txtMaxOTMin.Text.Trim());
                entity.MaxOTMinuteInHoliday = int.Parse(txtMaxOTMinForHoliday.Text.Trim() == "" ? "0" : txtMaxOTMinForHoliday.Text.Trim());
                entity.StandardWorkMinOverridingFromShiftForOT = int.Parse(txtOverrideStandardWorkMinute.Text.Trim() == "" ? "0" : txtOverrideStandardWorkMinute.Text.Trim());

                entity.MinOTHourInADay = int.Parse(txtMinHour.Text.Trim() == "" ? "0" : txtMinHour.Text.Trim());
                entity.MinOTHourInHoliday = int.Parse(txtMinHourForHoliday.Text.Trim() == "" ? "0" : txtMinHourForHoliday.Text.Trim());

                foreach (ListItem item in cboLevelList.Items)
                {
                    if (item.Selected)
                    {
                        entity.OvertimeAutoGroupLevels.Add(new OvertimeAutoGroupLevel { LevelId = int.Parse(item.Value) });
                    }
                }


            }
            else
            {
                txtName.Text = entity.Name;
                chkIsOTAllowed.Checked = entity.IsOTAllowed;
                txtGraceMinute.Text = entity.GraceMinuteToBeAddedInStandardHour.ToString();
                txtMaxOTMin.Text = entity.MaxOTMinuteInADay.ToString();

                if (entity.MaxOTMinuteInHoliday == null)
                    txtMaxOTMinForHoliday.Text = "0";
                else
                    txtMaxOTMinForHoliday.Text = entity.MaxOTMinuteInHoliday.ToString();

                txtOverrideStandardWorkMinute.Text = entity.StandardWorkMinOverridingFromShiftForOT.ToString();

                if (entity.CalculationType != null)
                    UIHelper.SetSelectedInDropDown(ddlCalculationType, entity.CalculationType.ToString());

                if (entity.MinOTHourInADay == null)
                    txtMinHour.Text = "0";
                else
                    txtMinHour.Text = entity.MinOTHourInADay.ToString();

                if (entity.MinOTHourInHoliday == null)
                    txtMinHourForHoliday.Text = "0";
                else
                    txtMinHourForHoliday.Text = entity.MinOTHourInHoliday.ToString();

                foreach (OvertimeAutoGroupLevel item in entity.OvertimeAutoGroupLevels.ToList())
                {
                    ListItem levelItem = cboLevelList.Items.FindByValue(item.LevelId.ToString());
                    if (levelItem != null)
                        levelItem.Selected = true;
                }
            }
        }

       

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {

                OvertimeAutoGroup entity = null;
                Process(ref entity);

                string msg = "";

                if (this.ControlID == 0)
                {
                    msg=CommonManager.IsOTGroupValid(entity);
                    if (!string.IsNullOrEmpty(msg))
                    {
                        divWarningMsg.InnerHtml = msg;
                        divWarningMsg.Hide = false;
                        return;
                    }

                    commonMgr.SaveOTGroup(entity);
                    JavascriptHelper.DisplayClientMsg("Group saved.", Page, "closePopup();");
                }
                else
                {
                    entity.OTGroupId = this.ControlID;

                    msg = CommonManager.IsOTGroupValid(entity);
                    if (!string.IsNullOrEmpty(msg))
                    {
                        divWarningMsg.InnerHtml = msg;
                        divWarningMsg.Hide = false;
                        return;
                    }

                    commonMgr.UpdateOTGroup(entity);
                    JavascriptHelper.DisplayClientMsg("Group updated.", Page, "closePopup();");
                }
            }
        }
    }
}