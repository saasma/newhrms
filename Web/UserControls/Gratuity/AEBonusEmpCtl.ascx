﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AEBonusEmpCtl.ascx.cs"
    Inherits="Web.UserControls.Gratuity.AEBonusEmpCtl" %>
<%@ Register Src="~/Controls/ErrorMsgCtl.ascx" TagName="ErrorMsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningMsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<script type="text/javascript">

    function closePopup() {
        //        window.returnValue = "Reload";
        //        window.close();

        // alert(window.opener.parentReloadCallbackFunction)
        //if (isNonModalBrowser() && typeof (window.opener.parentIncomeCallbackFunction)) 
        {
            window.opener.refresh(window);
        }

        //        else {
        //            window.returnValue = "Reload";
        //            window.close();
        //        }
    }

    
</script>
<style type="text/css">
    #ctl00_mainContent_ctl00_dateDistribution-triggerWrap td, th
    {
        padding: 0px !important;
    }
    body
    {
        background: #E9F0FB !important;
    }
</style>
<div class="popupHeader">
    <h2 class="headlinespop">
        Bonus Change</h2>
</div>
<div style="margin-left: 15px">
    <ext:ResourceManager runat="server" />
    <uc2:WarningMsgCtl Width="300px" ID="divWarningMsg" EnableViewState="false" Hide="true"
        runat="server" />
</div>
<fieldset style='margin-top: 15px'>
    <table>
        <tr>
            <td class="lf">
                Name
            </td>
            <td style="padding: 0px; padding-left: 10px;">
                <asp:Label ID="lblName" runat="server" />
            </td>
        </tr>
         <tr>
            <td class="lf">
                Unpaid Leave
            </td>
            <td>
                <asp:TextBox ID="txtUnapidLeave" runat="server"></asp:TextBox>
                <asp:CompareValidator Operator="GreaterThanEqual" ValueToCompare="0" ValidationGroup="Gratuity"
                    Type="Currency" ID="CompareValidator1" Display="None" ControlToValidate="txtUnapidLeave"
                    runat="server" ErrorMessage="Invalid Amount."></asp:CompareValidator>
            </td>
        </tr>

         <tr>
            <td class="lf">
                Sto Pay Days
            </td>
            <td>
                <asp:TextBox ID="txtOtherDeductDays" runat="server"></asp:TextBox>
                <asp:CompareValidator Operator="GreaterThanEqual" ValueToCompare="0" ValidationGroup="Gratuity"
                    Type="Currency" ID="CompareValidator3" Display="None" ControlToValidate="txtOtherDeductDays"
                    runat="server" ErrorMessage="Invalid Amount."></asp:CompareValidator>
            </td>
        </tr>
        <tr>
            <td class="lf">
                Annual Salary
            </td>
            <td>
                <asp:TextBox ID="txtAnnualSalary" runat="server"></asp:TextBox>
                <asp:CompareValidator Operator="GreaterThanEqual" ValueToCompare="0" ValidationGroup="Gratuity"
                    Type="Currency" ID="CompareValidator2" Display="None" ControlToValidate="txtAnnualSalary"
                    runat="server" ErrorMessage="Invalid Amount."></asp:CompareValidator>
            </td>
        </tr>
    </table>
    <br />
    <asp:Button ID="btnSave" CssClass="save" ValidationGroup="Gratuity" OnClientClick="valGroup='Gratuity';return CheckValidation();"
        runat="server" Text="Save" OnClick="btnSave_Click" />
    <input id="Button1" type="button" class="cancel" value="Cancel" onclick="window.close();" />
    <%--for displaying __doPostBack--%>
    <asp:LinkButton Style="display: none" ID="LinkButton1" runat="server">LinkButton</asp:LinkButton>
</fieldset>
