﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AEAutoOvertimeGroup.ascx.cs"
    Inherits="Web.UserControls.Gratuity.AEAutoOvertimeGroup" %>
<%@ Register Src="~/Controls/ErrorMsgCtl.ascx" TagName="ErrorMsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningMsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<script type="text/javascript">

    function closePopup() {
        //        window.returnValue = "Reload";
        //        window.close();

        // alert(window.opener.parentReloadCallbackFunction)
        //if (isNonModalBrowser() && typeof (window.opener.parentIncomeCallbackFunction)) 
        {
            window.opener.refresh(window);
        }

        //        else {
        //            window.returnValue = "Reload";
        //            window.close();
        //        }
    }

    var rowMinHourForWorkday = '#<%= rowMinHourForWorkday.ClientID %>';
    var rowMinHourForHoliday = '#<%= rowMinHourForHoliday.ClientID %>';
    var rowGraceMin = '#<%= rowGraceMin.ClientID %>';
    var rowStandardMin = '#<%= rowStandardMin.ClientID %>';

    function changeCalculationDisplay(ddl) {
        var value = ddl.value;

        $(rowMinHourForWorkday).hide();
        $(rowMinHourForHoliday).hide();
        $(rowGraceMin).hide();
       
        $(rowStandardMin).hide();


        if (value == "1") {
            $(rowMinHourForWorkday).show();
            $(rowMinHourForHoliday).show();
        }
        else {
            $(rowGraceMin).show();
            $(rowStandardMin).show();
        }

    }
</script>
<style type="text/css">
</style>
<div class="popupHeader">
    <h2 class="headlinespop">
        OT Group</h2>
</div>
<div style="margin-left: 15px">
    <uc2:WarningMsgCtl ID="divWarningMsg" EnableViewState="false" Hide="true"
        runat="server" />
</div>
<fieldset style='margin-top: 15px'>
    <table>
        <tr>
            <td valign="top">
                <table>
                    <tr>
                        <td class="lf">
                            <asp:Label runat="server" Text='Name' Width="150" />
                        </td>
                        <td>
                            <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ValidationGroup="Gratuity" ID="RequiredFieldValidator3"
                                Display="None" runat="server" ErrorMessage="Name is required." ControlToValidate="txtName">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="lf">
                            OT Allowed
                        </td>
                        <td>
                            <asp:CheckBox ID="chkIsOTAllowed" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="lf">
                            Calculation Type
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlCalculationType" runat="server" 
                                AppendDataBoundItems="true"  onchange="changeCalculationDisplay(this)">
                                <asp:ListItem Value="-1">Default</asp:ListItem>
                                <asp:ListItem Value="1">Ignore Minute with Minimum eligibility hour</asp:ListItem>
                            </asp:DropDownList>
                          
                        </td>
                    </tr>
                    <tr id="rowMinHourForWorkday" runat="server">
                        <td class="lf">
                            Min OT hour for Working day
                        </td>
                        <td>
                            <asp:TextBox ID="txtMinHour" runat="server"></asp:TextBox>                        
                        </td>
                    </tr>
                    <tr id="rowMinHourForHoliday" runat="server">
                        <td class="lf">
                            Min OT hour for Weekly / Public holiday
                        </td>
                        <td>
                            <asp:TextBox ID="txtMinHourForHoliday" runat="server"></asp:TextBox>
                           
                        </td>
                    </tr>
                    
                    <tr runat="server" id="rowGraceMin">
                        <td class="lf">
                            Grace In/Out Minute to be deduct/add in Office In/Out time 
                        </td>
                        <td>
                            <asp:TextBox ID="txtGraceMinute" Text="0"  runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ValidationGroup="Gratuity" ID="RequiredFieldValidator1"
                                Display="None" runat="server" ErrorMessage="Grace minute is required." ControlToValidate="txtGraceMinute">*</asp:RequiredFieldValidator>
                            <asp:CompareValidator ValueToCompare="0" ValidationGroup="Gratuity" Type="Integer"
                                ID="CompareValidator1" Display="None" ControlToValidate="txtGraceMinute" runat="server"
                                Operator="GreaterThanEqual" ErrorMessage="Invalid grace minute."></asp:CompareValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="lf">
                            Max OT minute for Working day
                        </td>
                        <td>
                            <asp:TextBox ID="txtMaxOTMin" Text="0"  runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ValidationGroup="Gratuity" ID="RequiredFieldValidator2"
                                Display="None" runat="server" ErrorMessage="Max OT minute is required." ControlToValidate="txtMaxOTMin">*</asp:RequiredFieldValidator>
                            <asp:CompareValidator ValueToCompare="0" ValidationGroup="Gratuity" Type="Integer"
                                ID="CompareValidator3" Display="None" ControlToValidate="txtMaxOTMin" runat="server"
                                Operator="GreaterThanEqual" ErrorMessage="Invalid max OT minute."></asp:CompareValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="lf">
                            Max OT minute for Weekly / Public holiday
                        </td>
                        <td>
                            <asp:TextBox ID="txtMaxOTMinForHoliday" Text="0"  runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ValidationGroup="Gratuity" ID="RequiredFieldValidator5"
                                Display="None" runat="server" ErrorMessage="Max OT minute for holiday is required." ControlToValidate="txtMaxOTMinForHoliday">*</asp:RequiredFieldValidator>
                            <asp:CompareValidator ValueToCompare="0" ValidationGroup="Gratuity" Type="Integer"
                                ID="CompareValidator4" Display="None" ControlToValidate="txtMaxOTMinForHoliday" runat="server"
                                Operator="GreaterThanEqual" ErrorMessage="Invalid max OT minute for holiday."></asp:CompareValidator>
                        </td>
                    </tr>
                    <tr runat="server" id="rowStandardMin">
                        <td class="lf">
                            Override Standard Work Minute / Any extra minute is OT
                        </td>
                        <td>
                            <asp:TextBox ID="txtOverrideStandardWorkMinute" Text="0"  runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ValidationGroup="Gratuity" ID="RequiredFieldValidator4"
                                Display="None" runat="server" ErrorMessage="Standard minute is required." ControlToValidate="txtOverrideStandardWorkMinute">*</asp:RequiredFieldValidator>
                            <asp:CompareValidator ValueToCompare="0" ValidationGroup="Gratuity" Type="Integer"
                                ID="CompareValidator2" Display="None" ControlToValidate="txtOverrideStandardWorkMinute"
                                runat="server" Operator="GreaterThanEqual" ErrorMessage="Invalid standard work minute."></asp:CompareValidator>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-top: 15px">
                            <asp:Button ID="btnSave" CssClass="save" ValidationGroup="Gratuity" OnClientClick="valGroup='Gratuity';return CheckValidation();"
                                runat="server" Text="Save" OnClick="btnSave_Click" />
                        </td>
                        <td style="padding-top: 15px">
                            <input id="Button1" type="button" class="cancel" value="Cancel" onclick="window.close();" />
                        </td>
                    </tr>
                </table>
            </td>
            <td valign="top">
                Applicable Level/Position List
                <br />
                <asp:CheckBoxList ID="cboLevelList"  RepeatColumns="3" Width="480px" runat="server"
                    DataValueField="LevelId" DataTextField="Name">
                </asp:CheckBoxList>
            </td>
        </tr>
    </table>
    <br />
    <%--for displaying __doPostBack--%>
    <asp:LinkButton Style="display: none" ID="LinkButton1" runat="server">LinkButton</asp:LinkButton>
</fieldset>
