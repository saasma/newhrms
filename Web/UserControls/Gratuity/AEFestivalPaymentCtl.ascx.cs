﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Base;
using BLL.Manager;
using Utils.Web;
using DAL;
using Utils.Helper;

namespace Web.UserControls.Gratuity
{
    public partial class AEFestivalPaymentCtl : BaseUserControl
    {
        PayManager payMgr = new PayManager();
        CommonManager commonMgr = new CommonManager();


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialise();
            }
        }

        void Initialise()
        {

            ddlIncomes.DataSource = PayManager.GetFestivalIncomeList(SessionManager.CurrentCompanyId);
            ddlIncomes.DataBind();

            ddlPeriod.DataSource = BLL.BaseBiz.PayrollDataContext.PayrollPeriods.ToList();
            ddlPeriod.DataBind();


            List<FestivalPaymentPeriod> periods = BLL.BaseBiz.PayrollDataContext.FestivalPaymentPeriods
                .Where(x => (x.IsArrearType == null || x.IsArrearType == false)).OrderByDescending(x=>x.FestivalId).ToList();

            ddlPastDashianForArrear.DataSource = periods;
            ddlPastDashianForArrear.DataBind();


            UIHelper.SetSelectedInDropDown(ddlPeriod,
                CommonManager.GetLastPayrollPeriod().PayrollPeriodId);

            int gid = UrlHelper.GetIdFromQueryString("Id");
            if (gid != 0)
            {
                FestivalPaymentPeriod gratuity = commonMgr.GetFestivalPaymentPeriod(gid);
                if (gratuity != null)
                {
                    this.ControlID = gid;
                    Process(ref gratuity);
                    btnSave.Text = Resources.Messages.Update;
                }
            }

        }

        FestivalPaymentPeriod Process(ref FestivalPaymentPeriod entity)
        {
            if (entity == null)
            {
                entity = new FestivalPaymentPeriod();
                entity.Name = txtName.Text.Trim();



                entity.PayrollPeriodId = int.Parse(ddlPeriod.SelectedValue);
                entity.Status = int.Parse(ddlDistrbuted.SelectedValue);
                entity.IncomeId = int.Parse(ddlIncomes.SelectedValue);
                entity.IsArrearType = chkArrearType.Checked;

                if (entity.IsArrearType != null && entity.IsArrearType.Value)
                {
                    entity.ArrearFestivalRef_ID = int.Parse(ddlPastDashianForArrear.SelectedValue);

                    if (CommonManager.CompanySetting.WhichCompany == WhichCompany.NIBL)
                    {
                        if (entity.ArrearFestivalRef_ID == -1)
                        {
                            JavascriptHelper.DisplayClientMsg("Old festival type need to be selected.", Page);
                            return null;
                        }
                    }
                }

            }
            else
            {
                txtName.Text = entity.Name;

                if (entity.AddOnId != null && entity.AddOnId != -1 && entity.AddOnId != 0)
                {
                    ddlPeriod.Enabled = false;
                }

                UIHelper.SetSelectedInDropDown(ddlPeriod, entity.PayrollPeriodId);

                UIHelper.SetSelectedInDropDown(ddlDistrbuted, entity.Status);

                UIHelper.SetSelectedInDropDown(ddlIncomes, entity.IncomeId);

                ddlIncomes.Enabled = false;

                if (entity.IsArrearType != null)
                {
                    chkArrearType.Checked = entity.IsArrearType.Value;
                    if (entity.ArrearFestivalRef_ID != null)
                        UIHelper.SetSelectedInDropDown(ddlPastDashianForArrear, entity.ArrearFestivalRef_ID.Value);
                }

            }

            return new FestivalPaymentPeriod();
        }



        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {

                FestivalPaymentPeriod entity = null;
                FestivalPaymentPeriod test = Process(ref entity);

                if (test == null)
                    return;

                if (this.ControlID == 0)
                {
                    //if (!CommonManager.IsOvertimeValid(entity))
                    //{
                    //    divWarningMsg.InnerHtml = Resources.Messages.GratuityRuleInvalid;
                    //    divWarningMsg.Hide = false;
                    //    return;
                    //}

                    Status status = commonMgr.SaveFestivalPeriod(entity);

                    if (status.IsSuccess == false)
                    {
                        JavascriptHelper.DisplayClientMsg(status.ErrorMessage, Page);
                        return;
                    }

                    JavascriptHelper.DisplayClientMsg("Period created.", Page, "closePopup();");
                }
                else
                {
                    entity.FestivalId = this.ControlID;

                    //if (!CommonManager.IsGratuityRuleValid(entity))
                    //{
                    //    divWarningMsg.InnerHtml = Resources.Messages.GratuityRuleInvalid;
                    //    divWarningMsg.Hide = false;
                    //    return;
                    //}

                    bool status = commonMgr.UpdateFestivalPeriod(entity);
                    if (status)
                        JavascriptHelper.DisplayClientMsg("Period updated.", Page, "closePopup();");
                    else
                    {
                        divWarningMsg.InnerHtml = "Period is not editable.";
                        divWarningMsg.Hide = false;
                    }
                }
            }
        }
    }
}