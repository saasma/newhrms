﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Base;
using BLL.Manager;
using Utils.Web;
using DAL;
using Utils.Helper;

namespace Web.UserControls.Gratuity
{
    public partial class AEGratuityClassCtl : BaseUserControl
    {
        PayManager payMgr = new PayManager();
        CommonManager commonMgr = new CommonManager();
        

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialise();
            }
        }

        void Initialise()
        {

           

            int gid = UrlHelper.GetIdFromQueryString("Id");
            if (gid != 0)
            {
                GratuityClass gratuity = commonMgr.GetGratuityClass(gid);
                if (gratuity != null)
                {
                    this.ControlID = gid;
                    Process(ref gratuity);
                    btnSave.Text = Resources.Messages.Update;
                }
            }

        }

        void Process(ref GratuityClass entity)
        {
            if (entity == null)
            {
                entity = new GratuityClass();
                entity.Name = txtName.Text.Trim();
                entity.IsDefault = chkIsDefault.Checked;

             
            }
            else
            {
                txtName.Text = entity.Name;
                chkIsDefault.Checked = entity.IsDefault;

            }
        }

       

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {

                GratuityClass entity = null;
                Process(ref entity);

                if (this.ControlID == 0)
                {
                    if (!CommonManager.IsGratuityClassValid(entity))
                    {
                        divWarningMsg.InnerHtml = "Default class already defined.";
                        divWarningMsg.Hide = false;
                        return;
                    }

                    commonMgr.SaveGratuityClass(entity);
                    JavascriptHelper.DisplayClientMsg("New gratuity class saved.", Page, "closePopup();");
                }
                else
                {
                    entity.GratuityClassID = this.ControlID;

                    if (!CommonManager.IsGratuityClassValid(entity))
                    {
                        divWarningMsg.InnerHtml = "Default class already defined.";
                        divWarningMsg.Hide = false;
                        return;
                    }

                    commonMgr.UpdateGratuityClass(entity);
                    JavascriptHelper.DisplayClientMsg("Gratuity rule updated.", Page, "closePopup();");
                }
            }
        }
    }
}