﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AEBonusCtl.ascx.cs"
    Inherits="Web.UserControls.Gratuity.AEBonusCtl" %>
<%@ Register Src="~/Controls/ErrorMsgCtl.ascx" TagName="ErrorMsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningMsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>
<script type="text/javascript">

    function closePopup() {
        //        window.returnValue = "Reload";
        //        window.close();

        // alert(window.opener.parentReloadCallbackFunction)
        //if (isNonModalBrowser() && typeof (window.opener.parentIncomeCallbackFunction)) 
        {
            window.opener.refresh(window);
        }

        //        else {
        //            window.returnValue = "Reload";
        //            window.close();
        //        }
    }

    
</script>
<style type="text/css">
    #ctl00_mainContent_ctl00_dateDistribution-triggerWrap td, th,
     #ctl00_mainContent_ctl00_dateStart-triggerWrap td, th,
      #ctl00_mainContent_ctl00_dateEnd-triggerWrap td, th
    {
        padding: 0px !important;
    }
    body
    {
        background: #E9F0FB !important;
    }
</style>
<div class="popupHeader">
    <h2 class="headlinespop">
        Bonus</h2>
</div>
<div style="margin-left: 15px">
    <ext:ResourceManager runat="server" DisableViewState="false" />
    <uc2:WarningMsgCtl Width="300px" ID="divWarningMsg" EnableViewState="false" Hide="true"
        runat="server" />
</div>
<fieldset style='margin-top: 15px'>
    <table>
        <tr>
            <td class="lf">
                Name
            </td>
            <td style="padding: 0px; padding-left: 10px;">
                <asp:TextBox runat="server" ID="txtName" />
                <asp:RequiredFieldValidator ValidationGroup="Gratuity" ID="RequiredFieldValidator4"
                    Display="None" runat="server" ErrorMessage="Name is required." ControlToValidate="txtName">*</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="lf">
                Distribution Date
            </td>
            <td style="padding: 0px;">
                <ext:DateField runat="server" ID="dateDistribution" />
                <asp:RequiredFieldValidator ValidationGroup="Gratuity" ID="RequiredFieldValidator3"
                    Display="None" runat="server" ErrorMessage="Date is required." ControlToValidate="dateDistribution">*</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="lf">
                Bonus Year
            </td>
            <td>
                <asp:DropDownList ID="ddlFinancialYear" runat="server" Width="125" DataValueField="FinancialDateId"
                    AppendDataBoundItems="true" DataTextField="Name" CssClass="gapbelow">
                    <asp:ListItem Value="-1">--Select Year--</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ValidationGroup="Gratuity" ID="RequiredFieldValidator1"
                    Display="None" runat="server" ErrorMessage="Year to is required." ControlToValidate="ddlFinancialYear">*</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="lf">
                Start Date
            </td>
            <td style="padding: 0px;">
                <ext:DateField runat="server" ID="dateStart">
                    <Plugins>
                        <ext:GenericPlugin ID="HighlightDates1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                    </Plugins>
                </ext:DateField>
                <asp:RequiredFieldValidator ValidationGroup="Gratuity" ID="dateStart1" Display="None"
                    runat="server" ErrorMessage="Start date is required." ControlToValidate="dateStart">*</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="lf">
                End Date
            </td>
            <td style="padding: 0px;">
                <ext:DateField runat="server" ID="dateEnd">
                    <Plugins>
                        <ext:GenericPlugin ID="GenericPlugin1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                    </Plugins>
                </ext:DateField>
                <asp:RequiredFieldValidator ValidationGroup="Gratuity" ID="dateEnd2" Display="None"
                    runat="server" ErrorMessage="End Date is required." ControlToValidate="dateEnd">*</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="lf">
                Payroll Period
            </td>
            <td>
                <asp:DropDownList Enabled="false" AppendDataBoundItems="true" ID="ddlPeriod" DataValueField="PayrollPeriodId"
                    DataTextField="Name" runat="server">
                    <asp:ListItem Text="--Select Period--" Value="-1"></asp:ListItem>
                </asp:DropDownList>
                <%--  <asp:RequiredFieldValidator InitialValue="-1" ValidationGroup="Gratuity" ID="RequiredFieldValidator4"
                    Display="None" runat="server" ErrorMessage="Please select applicable to." ControlToValidate="ddlApplicableTo">*</asp:RequiredFieldValidator>--%>
            </td>
        </tr>
        <tr>
            <td class="lf">
                Bonus Amount
            </td>
            <td>
                <asp:TextBox ID="txtAmount" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ValidationGroup="Gratuity" ID="RequiredFieldValidator2"
                    Display="None" runat="server" ErrorMessage="Amount is required." ControlToValidate="txtAmount">*</asp:RequiredFieldValidator>
                <asp:CompareValidator Operator="GreaterThanEqual" ValueToCompare="0" ValidationGroup="Gratuity"
                    Type="Currency" ID="CompareValidator2" Display="None" ControlToValidate="txtAmount"
                    runat="server" ErrorMessage="Invalid Amount."></asp:CompareValidator>
            </td>
        </tr>
        <tr>
            <td class="lf">
                Bonus Eligible Status
            </td>
            <td>
                <div id="divStatusList" runat="server" style="overflow-y: scroll; height: 85px; width: 450px;
                    border: 1px solid #C1D6F3; padding-top: 5px; padding: 0px">
                    <asp:CheckBoxList RepeatColumns="4" RepeatDirection="Horizontal" ID="chkStatusList"
                        runat="server" DataTextField="Value" DataValueField="Key">
                    </asp:CheckBoxList>
                </div>
            </td>
        </tr>
        <tr>
            <td class="lf">
                Salary for Bonus Calculation
            </td>
            <td>
                <div id="div1" runat="server" style="overflow-y: scroll; height: 120px; width: 450px;
                    border: 1px solid #C1D6F3; padding-top: 5px; padding: 0px">
                    <asp:CheckBoxList ID="chkListSalaries" RepeatColumns="3" RepeatDirection="Horizontal"
                        runat="server" DataTextField="Title" DataValueField="IncomeId">
                    </asp:CheckBoxList>
                </div>
            </td>
        </tr>
        <tr>
            <td class="lf">
                Bonus Status
            </td>
            <td>
                <asp:DropDownList ID="ddlDistrbuted" runat="server" Width="125" CssClass="gapbelow">
                    <asp:ListItem Value="-1">--Select--</asp:ListItem>
                    <asp:ListItem Value="1">Distributed</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="lf">
                Bonus Type
            </td>
            <td>
                <asp:DropDownList ID="ddlFinalBonus" runat="server" Width="125" CssClass="gapbelow">
                    <asp:ListItem Value="-1">--Select--</asp:ListItem>
                    <asp:ListItem Value="true">Final</asp:ListItem>
                    <asp:ListItem Value="false">Advance</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="lf">
                Advance Bonus
            </td>
            <td>
                <asp:DropDownList ID="ddlAdvanceBonusList" AppendDataBoundItems="true" DataValueField="BonusId" DataTextField="Name"
                    runat="server" Width="125" CssClass="gapbelow">
                    <asp:ListItem Value="-1">--Select--</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
          <tr>
            <td class="lf">
                Income
            </td>
            <td>
                <asp:DropDownList  AppendDataBoundItems="true" ID="ddlIncomes" DataValueField="IncomeId"
                    DataTextField="Title" runat="server">
                    <asp:ListItem Text="--Select Income--" Value="-1"></asp:ListItem>
                </asp:DropDownList>
                  <asp:RequiredFieldValidator InitialValue="-1" ValidationGroup="Gratuity" ID="RequiredFieldValidator5"
                    Display="None" runat="server" ErrorMessage="Please select income." ControlToValidate="ddlIncomes">*</asp:RequiredFieldValidator>
            </td>
        </tr>
    </table>
    <br />
    <asp:Button ID="btnSave" CssClass="save" ValidationGroup="Gratuity" OnClientClick="valGroup='Gratuity';return CheckValidation();"
        runat="server" Text="Save" OnClick="btnSave_Click" />
    <input id="Button1" type="button" class="cancel" value="Cancel" onclick="window.close();" />
    <%--for displaying __doPostBack--%>
    <asp:LinkButton Style="display: none" ID="LinkButton1" runat="server">LinkButton</asp:LinkButton>
</fieldset>
