﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Base;
using BLL.Manager;
using Utils.Web;
using DAL;
using Utils.Helper;

namespace Web.UserControls.Gratuity
{
    public partial class EditFestivalPayment : BaseUserControl
    {
        PayManager payMgr = new PayManager();
        CommonManager commonMgr = new CommonManager();

        int festivalId = 0;
        int employeeId = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            festivalId = int.Parse(Request.QueryString["fid"]);
            employeeId = int.Parse(Request.QueryString["ein"]);

            if (!IsPostBack)
            {
                Initialise();
            }
        }

        void Initialise()
        {


            FestivalPaymentEmployee entity = BLL.BaseBiz.PayrollDataContext.FestivalPaymentEmployees
                .FirstOrDefault(x => x.EmployeeId == employeeId && x.FestivalId == festivalId);


            txtName.Text = EmployeeManager.GetEmployeeName(entity.EmployeeId) + " - " + entity.EmployeeId;
            txtWorkDay.Text = entity.TotalWorkDays == null ? "0" : entity.TotalWorkDays.ToString();
            txtAmount.Text = entity.Amount == null ? "0" : GetCurrency(entity.Amount);
            txtRemarks.Text = entity.Remarks;

            if (entity.PreventReset != null && entity.PreventReset.Value)
                ddlExclude.SelectedIndex = 1;
            else
                ddlExclude.SelectedIndex = 0;

            

            btnSave.Text = Resources.Messages.Update;

        }

        



        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {

                FestivalPaymentEmployee entity = new FestivalPaymentEmployee();

                entity.EmployeeId = employeeId;
                entity.FestivalId = festivalId;
                entity.Amount = decimal.Parse(txtAmount.Text);
                entity.TotalWorkDays = double.Parse(txtWorkDay.Text);
                entity.Remarks = txtRemarks.Text.Trim();
                entity.PreventReset = bool.Parse(ddlExclude.SelectedValue);

                //if (this.Id == 0)
                //{
                //    //if (!CommonManager.IsOvertimeValid(entity))
                //    //{
                //    //    divWarningMsg.InnerHtml = Resources.Messages.GratuityRuleInvalid;
                //    //    divWarningMsg.Hide = false;
                //    //    return;
                //    //}

                //    Status status = commonMgr.SaveOvertimePeriod(entity);

                //    if (status.IsSuccess == false)
                //    {
                //        JavascriptHelper.DisplayClientMsg(status.ErrorMessage, Page);
                //        return;
                //    }

                //    JavascriptHelper.DisplayClientMsg("Overtime created.", Page, "closePopup();");
                //}
                //else
                {
                    //entity.OvertimeID = this.Id;

                    //if (!CommonManager.IsGratuityRuleValid(entity))
                    //{
                    //    divWarningMsg.InnerHtml = Resources.Messages.GratuityRuleInvalid;
                    //    divWarningMsg.Hide = false;
                    //    return;
                    //}

                    bool status = commonMgr.UpdateFestivalEmployee(entity);
                    if (status)
                        JavascriptHelper.DisplayClientMsg("Information updated.", Page, "closePopup();");
                    else
                    {
                        divWarningMsg.InnerHtml = "Information is not editable.";
                        divWarningMsg.Hide = false;
                    }
                }
            }
        }
    }
}