﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Base;
using BLL.Manager;
using Utils.Web;
using DAL;
using Utils.Helper;

namespace Web.UserControls.Gratuity
{
    public partial class AEGratuityCtl : BaseUserControl
    {
        PayManager payMgr = new PayManager();
        CommonManager commonMgr = new CommonManager();
        

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialise();
            }
        }

        void Initialise()
        {

            ddlClass.DataSource = commonMgr.GetAllGratuityClass(SessionManager.CurrentCompanyId);
            ddlClass.DataBind();
          

            int gid = UrlHelper.GetIdFromQueryString("Id");
            if (gid != 0)
            {
                GratuityRule gratuity = commonMgr.GetGratuityRule(gid);
                if (gratuity != null)
                {
                    this.ControlID = gid;
                    Process(ref gratuity);
                    btnSave.Text = Resources.Messages.Update;
                }
            }

        }

        void Process(ref GratuityRule entity)
        {
            if (entity == null)
            {
                entity = new GratuityRule();
                entity.CompanyId = SessionManager.CurrentCompanyId;
                entity.FromServiceYear = int.Parse(txtFrom.Text);
                entity.ToServiceYear = int.Parse(txtTo.Text);
                entity.MonthsSalary = decimal.Parse(txtMonthsSalary.Text);
                if (ddlClass.SelectedIndex != 0)
                    entity.GratuityClassRef_ID = int.Parse(ddlClass.SelectedValue);


             
            }
            else
            {
                txtFrom.Text = entity.FromServiceYear.ToString();
                txtTo.Text = entity.ToServiceYear.ToString();
                txtMonthsSalary.Text = entity.MonthsSalary.ToString();

                if(entity.GratuityClassRef_ID != null)
                    UIHelper.SetSelectedInDropDown(ddlClass, entity.GratuityClassRef_ID.Value);


            }
        }

       

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {

                GratuityRule entity = null;
                Process(ref entity);

                if (this.ControlID == 0)
                {
                    if (!CommonManager.IsGratuityRuleValid(entity))
                    {
                        divWarningMsg.InnerHtml = Resources.Messages.GratuityRuleInvalid;
                        divWarningMsg.Hide = false;
                        return;
                    }

                    commonMgr.SaveGratuityRule(entity);
                    JavascriptHelper.DisplayClientMsg("New gratuity rule saved.", Page, "closePopup();");
                }
                else
                {
                    entity.GratuityRuleId = this.ControlID;

                    if (!CommonManager.IsGratuityRuleValid(entity))
                    {
                        divWarningMsg.InnerHtml = Resources.Messages.GratuityRuleInvalid;
                        divWarningMsg.Hide = false;
                        return;
                    }

                    commonMgr.UpdateGratuityRule(entity);
                    JavascriptHelper.DisplayClientMsg("Gratuity rule updated.", Page, "closePopup();");
                }
            }
        }
    }
}