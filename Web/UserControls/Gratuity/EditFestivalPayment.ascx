﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditFestivalPayment.ascx.cs"
    Inherits="Web.UserControls.Gratuity.EditFestivalPayment" %>
<%@ Register Src="~/Controls/ErrorMsgCtl.ascx" TagName="ErrorMsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningMsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<script type="text/javascript">

    function closePopup() {
        //        window.returnValue = "Reload";
        //        window.close();

        // alert(window.opener.parentReloadCallbackFunction)
        //if (isNonModalBrowser() && typeof (window.opener.parentIncomeCallbackFunction)) 
        {
            window.opener.refresh(window);
        }

        //        else {
        //            window.returnValue = "Reload";
        //            window.close();
        //        }
    }

    
</script>
<script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>
<style type="text/css">
    #ctl00_mainContent_ctl00_dateStart-triggerWrap td, th
    {
        padding: 0px !important;
    }
    #ctl00_mainContent_ctl00_dateEnd-triggerWrap td, th
    {
        padding: 0px !important;
    }
    body
    {
        background: #E9F0FB !important;
    }
</style>
<div class="popupHeader">
    <h2 class="headlinespop">
        Change Dashain</h2>
</div>
<div style="margin-left: 15px">
    <ext:ResourceManager ID="ResourceManager1" runat="server" DisableViewState="false" />
    <uc2:WarningMsgCtl Width="300px" ID="divWarningMsg" EnableViewState="false" Hide="true"
        runat="server" />
</div>
<fieldset style='margin-top: 15px'>
    <table>
        <tr>
            <td class="lf">
                Name
            </td>
            <td>
                <asp:TextBox Enabled="false" ID="txtName" runat="server"></asp:TextBox>
                
            </td>
        </tr>
      
        <tr>
            <td class="lf">
                Workdays
            </td>
            <td>
                <asp:TextBox  ID="txtWorkDay" runat="server"></asp:TextBox>
                <asp:CompareValidator Operator="GreaterThanEqual" ValueToCompare="0" ValidationGroup="Gratuity"
                    Type="Double" ID="CompareValidator2" Display="None" ControlToValidate="txtWorkDay"
                    runat="server" ErrorMessage="Invalid workday."></asp:CompareValidator>
            </td>
        </tr>
           <tr>
            <td class="lf">
                Dashain Amount
            </td>
            <td>
                <asp:TextBox ID="txtAmount" runat="server"></asp:TextBox>
                <asp:CompareValidator Operator="GreaterThanEqual" ValueToCompare="0" ValidationGroup="Gratuity"
                    Type="Currency" ID="CompareValidator1" Display="None" ControlToValidate="txtAmount"
                    runat="server" ErrorMessage="Invalid amount."></asp:CompareValidator>
            </td>
        </tr>
        <tr>
            <td class="lf">
                Exclude in ReGenerate
            </td>
            <td>
                <asp:DropDownList ID="ddlExclude" runat="server" Width="125" CssClass="gapbelow">
                    <asp:ListItem Value="false">--Select--</asp:ListItem>
                    <asp:ListItem Value="true">Exclude</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
         <tr>
            <td class="lf">
                Remarks
            </td>
            <td>
                <asp:TextBox TextMode="MultiLine" Width="250px"  ID="txtRemarks" runat="server"></asp:TextBox>
                
            </td>
        </tr>
    </table>
    <br />
    <asp:Button ID="btnSave" CssClass="save" ValidationGroup="Gratuity" OnClientClick="valGroup='Gratuity';return CheckValidation();"
        runat="server" Text="Save" OnClick="btnSave_Click" />
    <input id="Button1" type="button" class="cancel" value="Cancel" onclick="window.close();" />
    <%--for displaying __doPostBack--%>
    <asp:LinkButton Style="display: none" ID="LinkButton1" runat="server">LinkButton</asp:LinkButton>
</fieldset>
