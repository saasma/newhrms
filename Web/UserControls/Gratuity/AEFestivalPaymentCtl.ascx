﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AEFestivalPaymentCtl.ascx.cs"
    Inherits="Web.UserControls.Gratuity.AEFestivalPaymentCtl" %>
<%@ Register Src="~/Controls/ErrorMsgCtl.ascx" TagName="ErrorMsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningMsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<script type="text/javascript">

    function closePopup() {
        //        window.returnValue = "Reload";
        //        window.close();

        // alert(window.opener.parentReloadCallbackFunction)
        //if (isNonModalBrowser() && typeof (window.opener.parentIncomeCallbackFunction)) 
        {
            window.opener.refresh(window);
        }

        //        else {
        //            window.returnValue = "Reload";
        //            window.close();
        //        }
    }

    
</script>
<script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>
<style type="text/css">
    #ctl00_mainContent_ctl00_dateStart-triggerWrap td, th
    {
        padding: 0px !important;
    }
    #ctl00_mainContent_ctl00_dateEnd-triggerWrap td, th
    {
        padding: 0px !important;
    }
    body
    {
        background: #E9F0FB !important;
    }
    td
    {
        padding-top:10px!important;
    }
</style>
<div class="popupHeader">
    <h2 class="headlinespop">
        Dashain</h2>
</div>
<div style="margin-left: 15px">
    <ext:ResourceManager ID="ResourceManager1" runat="server" DisableViewState="false" />
    <uc2:WarningMsgCtl Width="300px" ID="divWarningMsg" EnableViewState="false" Hide="true"
        runat="server" />
</div>
<fieldset style='margin-top: 15px'>
    <table>
        <tr>
            <td class="lf">
                Name
            </td>
            <td>
                <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ValidationGroup="Gratuity" ID="RequiredFieldValidator3"
                    Display="None" runat="server" ErrorMessage="Name is required." ControlToValidate="txtName">*</asp:RequiredFieldValidator>
            </td>
        </tr>
        
        <tr>
            <td class="lf">
                Payroll Period
            </td>
            <td>
                <asp:DropDownList AppendDataBoundItems="true" ID="ddlPeriod" DataValueField="PayrollPeriodId"
                    DataTextField="Name" runat="server">
                    <asp:ListItem Text="--Select Period--" Value="-1"></asp:ListItem>
                </asp:DropDownList>
                <%--  <asp:RequiredFieldValidator InitialValue="-1" ValidationGroup="Gratuity" ID="RequiredFieldValidator4"
                    Display="None" runat="server" ErrorMessage="Please select applicable to." ControlToValidate="ddlApplicableTo">*</asp:RequiredFieldValidator>--%>
            </td>
        </tr>
         <tr>
            <td class="lf">
                Arrear Type
            </td>
            <td>
               <asp:CheckBox  runat="server" Text="Arrear Type" ID="chkArrearType" />
            </td>
        </tr>
         <tr>
            <td class="lf">
                Arrear For
            </td>
            <td>
               <asp:DropDownList AppendDataBoundItems="true" ID="ddlPastDashianForArrear" DataValueField="FestivalId"
                    DataTextField="Name" runat="server">
                    <asp:ListItem Text="--Select--" Value="-1"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="lf">
                Income
            </td>
            <td>
                <asp:DropDownList  AppendDataBoundItems="true" ID="ddlIncomes" DataValueField="IncomeId"
                    DataTextField="Title" runat="server">
                    <asp:ListItem Text="--Select Income--" Value="-1"></asp:ListItem>
                </asp:DropDownList>
                  <asp:RequiredFieldValidator InitialValue="-1" ValidationGroup="Gratuity" ID="RequiredFieldValidator4"
                    Display="None" runat="server" ErrorMessage="Please select income." ControlToValidate="ddlIncomes">*</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="lf">
                Distributed
            </td>
            <td>
                <asp:DropDownList ID="ddlDistrbuted" runat="server" Width="125" CssClass="gapbelow">
                    <asp:ListItem Value="-1">--Select--</asp:ListItem>
                    <asp:ListItem Value="1">Distributed</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
    </table>
    <br />
    <asp:Button ID="btnSave" CssClass="save" ValidationGroup="Gratuity" OnClientClick="valGroup='Gratuity';return CheckValidation();"
        runat="server" Text="Save" OnClick="btnSave_Click" />
    <input id="Button1" type="button" class="cancel" value="Cancel" onclick="window.close();" />
    <%--for displaying __doPostBack--%>
    <asp:LinkButton Style="display: none" ID="LinkButton1" runat="server">LinkButton</asp:LinkButton>
</fieldset>
