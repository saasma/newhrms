﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Base;
using BLL.Manager;
using Utils.Web;
using DAL;
using Utils.Helper;

namespace Web.UserControls.Gratuity
{
    public partial class AEBonusEmpCtl : BaseUserControl
    {
        PayManager payMgr = new PayManager();
        CommonManager commonMgr = new CommonManager();

        int bonusId = 0;
        int empId = 0;

    
        protected void Page_Load(object sender, EventArgs e)
        {
            bonusId = UrlHelper.GetIdFromQueryString("bonusid");
            empId = UrlHelper.GetIdFromQueryString("empId");

            if (!IsPostBack)
            {
                Initialise();
            }
        }

        void Initialise()
        {

            


                BonusEmployee gratuity = commonMgr.GetBonus(bonusId,empId);
                if (gratuity != null)
                {
                    lblName.Text = EmployeeManager.GetEmployeeName(empId);

                    Process(ref gratuity);
                    btnSave.Text = Resources.Messages.Update;
                }
            

        }

        void Process(ref BonusEmployee entity)
        {
            if (entity == null)
            {
                entity = new BonusEmployee();
                entity.BonusId = bonusId;
                entity.EmployeeId = empId;
                if (!string.IsNullOrEmpty(txtUnapidLeave.Text))
                    entity.UnpaidLeave = double.Parse(txtUnapidLeave.Text);
                if (!string.IsNullOrEmpty(txtOtherDeductDays.Text))
                    entity.OtherUneligibleDays = double.Parse(txtOtherDeductDays.Text);
                if (!string.IsNullOrEmpty(txtAnnualSalary.Text))
                    entity.AnnualSalary = decimal.Parse(txtAnnualSalary.Text);

             
            }
            else
            {
                if (entity.UnpaidLeave != null)
                    txtUnapidLeave.Text = entity.UnpaidLeave.ToString();

                if (entity.OtherUneligibleDays != null)
                    txtOtherDeductDays.Text = entity.OtherUneligibleDays.ToString();

                if (entity.AnnualSalary != null)
                    txtAnnualSalary.Text = GetCurrency(entity.AnnualSalary.Value);
            }
        }

       

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                

                BonusEmployee entity = null;
                Process(ref entity);

                {


                    commonMgr.UpdateBonusEmp(entity);
                    JavascriptHelper.DisplayClientMsg("Bonus updated.", Page, "closePopup();");
                }
            }
        }
    }
}