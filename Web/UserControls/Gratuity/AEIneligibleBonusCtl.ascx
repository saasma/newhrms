﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AEIneligibleBonusCtl.ascx.cs"
    Inherits="Web.UserControls.Gratuity.AEIneligibleBonusCtl" %>
<%@ Register Src="~/Controls/ErrorMsgCtl.ascx" TagName="ErrorMsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningMsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<script type="text/javascript">

    function closePopup() {
        //        window.returnValue = "Reload";
        //        window.close();

        // alert(window.opener.parentReloadCallbackFunction)
        //if (isNonModalBrowser() && typeof (window.opener.parentIncomeCallbackFunction)) 
        {
            window.opener.refresh(window);
        }

        //        else {
        //            window.returnValue = "Reload";
        //            window.close();
        //        }
    }


    var selEmpId = null;


    function ACE_item_selected(source, eventArgs) {
        var value = eventArgs.get_value();
        selEmpId = value;
        //window.location = "Employee.aspx?Id=" + value; 
        //alert(value);
        document.getElementById('<%= hdn.ClientID%>').value = value;

       
    }

</script>
<style type="text/css">
    #ctl00_mainContent_ctl00_dateDistribution-triggerWrap td, th
    {
        padding: 0px !important;
    }
    body
    {
        background: #E9F0FB !important;
    }
</style>
<div class="popupHeader">
    <h2 class="headlinespop">
        Ineligible Employee</h2>
</div>
<div style="margin-left: 15px">
    <ext:ResourceManager runat="server" />
    <uc2:WarningMsgCtl Width="300px" ID="divWarningMsg" EnableViewState="false" Hide="true"
        runat="server" />
</div>
<fieldset style='margin-top: 15px'>
    <table>
        <tr>
            <td class="lf">
                Employee
            </td>
            <td style="padding: 0px; padding-left: 10px;">
                <asp:HiddenField ID="hdn" runat="server" />
                <asp:TextBox   Width="180px"  ID="txtEmpSearchText"
                    runat="server"></asp:TextBox>
                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtEmpSearchText"
                    WatermarkText="Employee Name" WatermarkCssClass="searchBoxText" />
                <cc1:AutoCompleteExtender EnableCaching="true" ID="AutoCompleteExtenderOrganization"
                    runat="server" MinimumPrefixLength="2" ServiceMethod="GetEmployeeNamesWithRetiredAndID" ServicePath="~/PayrollService.asmx"
                    TargetControlID="txtEmpSearchText" OnClientItemSelected="ACE_item_selected" CompletionSetCount="10" CompletionInterval="250"
                    CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList"
                    CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
                </cc1:AutoCompleteExtender>
                <asp:RequiredFieldValidator ValidationGroup="Gratuity" ID="RequiredFieldValidator4"
                    Display="None" runat="server" ErrorMessage="Employee is required." ControlToValidate="txtEmpSearchText">*</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="lf" style="padding-top:10px;">
                Reason
            </td>
            <td style="padding-top: 10px; padding-left: 10px;">
                <asp:TextBox ID="txtReason" runat="server" />
            </td>
        </tr>
    </table>
    <br />
    <asp:Button ID="btnSave" CssClass="save" ValidationGroup="Gratuity" OnClientClick="valGroup='Gratuity';return CheckValidation();"
        runat="server" Text="Save" OnClick="btnSave_Click" />
    <input id="Button1" type="button" class="cancel" value="Cancel" onclick="window.close();" />
    <%--for displaying __doPostBack--%>
    <asp:LinkButton Style="display: none" ID="LinkButton1" runat="server">LinkButton</asp:LinkButton>
</fieldset>
