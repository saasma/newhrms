﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AEGratuityCtl.ascx.cs"
    Inherits="Web.UserControls.Gratuity.AEGratuityCtl" %>
<%@ Register Src="~/Controls/ErrorMsgCtl.ascx" TagName="ErrorMsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningMsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<script type="text/javascript">

    function closePopup() {
//        window.returnValue = "Reload";
//        window.close();

        // alert(window.opener.parentReloadCallbackFunction)
        //if (isNonModalBrowser() && typeof (window.opener.parentIncomeCallbackFunction)) 
        {
            window.opener.refresh(window);
        } 
        
//        else {
//            window.returnValue = "Reload";
//            window.close();
//        }
    }

    
</script>

<div class="popupHeader">
    <h2 class="headlinespop">
        Gratuity/Severance Rule</h2>
</div>

<div style="margin-left:15px">
    <uc2:WarningMsgCtl Width="300px" ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
  </div>
<fieldset style='margin-top: 15px'>
    <table>
        <tr>
            <td class="lf">
                Service Year From
            </td>
            <td>
                <asp:TextBox ID="txtFrom" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ValidationGroup="Gratuity" ID="RequiredFieldValidator3"
                    Display="None" runat="server" ErrorMessage="Service year from is required." ControlToValidate="txtFrom">*</asp:RequiredFieldValidator>
                <asp:CompareValidator Operator="GreaterThanEqual" ValueToCompare="0" ValidationGroup="Gratuity"
                    Type="Integer" ID="valCompCutOff1" Display="None" ControlToValidate="txtFrom"
                    runat="server" ErrorMessage="Invalid service year from."></asp:CompareValidator>
            </td>
        </tr>
        <tr>
            <td class="lf">
                Service Year To
            </td>
            <td>
                <asp:TextBox ID="txtTo" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ValidationGroup="Gratuity" ID="RequiredFieldValidator1"
                    Display="None" runat="server" ErrorMessage="Service year to is required." ControlToValidate="txtTo">*</asp:RequiredFieldValidator>
                <asp:CompareValidator Operator="GreaterThan" ValueToCompare="0" ValidationGroup="Gratuity"
                    Type="Integer" ID="CompareValidator1" Display="None" ControlToValidate="txtTo"
                    runat="server" ErrorMessage="Invalid service year to."></asp:CompareValidator>
            </td>
        </tr>
        <tr>
            <td class="lf">
                Month's Salary
            </td>
            <td>
                <asp:TextBox ID="txtMonthsSalary" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ValidationGroup="Gratuity" ID="RequiredFieldValidator2"
                    Display="None" runat="server" ErrorMessage="Months salary is required." ControlToValidate="txtMonthsSalary">*</asp:RequiredFieldValidator>
                <asp:CompareValidator Operator="GreaterThan" ValueToCompare="0" ValidationGroup="Gratuity"
                    Type="Double" ID="CompareValidator2" Display="None" ControlToValidate="txtMonthsSalary"
                    runat="server" ErrorMessage="Invalid months salary."></asp:CompareValidator>
            </td>
        </tr>
        <tr>
            <td class="lf">
                Applicable To
            </td>
            <td>
                <asp:DropDownList AppendDataBoundItems="true" ID="ddlClass" DataValueField="GratuityClassID" DataTextField="Name" runat="server">
                    <asp:ListItem Text="--Select Class To--" Value="-1"></asp:ListItem>
                </asp:DropDownList>
              <%--  <asp:RequiredFieldValidator InitialValue="-1" ValidationGroup="Gratuity" ID="RequiredFieldValidator4"
                    Display="None" runat="server" ErrorMessage="Please select applicable to." ControlToValidate="ddlApplicableTo">*</asp:RequiredFieldValidator>--%>
            </td>
        </tr>
    </table>
    <br />
    <asp:Button ID="btnSave" CssClass="save" ValidationGroup="Gratuity" OnClientClick="valGroup='Gratuity';return CheckValidation();"
        runat="server" Text="Save" OnClick="btnSave_Click" />
    <input id="Button1" type="button" class="cancel" value="Cancel" onclick="window.close();" />
    <%--for displaying __doPostBack--%>
    <asp:LinkButton Style="display: none" ID="LinkButton1" runat="server">LinkButton</asp:LinkButton>
</fieldset>
