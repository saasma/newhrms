﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Base;
using BLL.Manager;
using Utils.Web;
using DAL;
using Utils.Helper;

namespace Web.UserControls.Gratuity
{
    public partial class AEBonusCtl : BaseUserControl
    {
        PayManager payMgr = new PayManager();
        CommonManager commonMgr = new CommonManager();


        protected void Page_Init(object sender, EventArgs e)
        {
            List<FinancialDate> financialYearList = new CommonManager().GetAllFinancialDates();
            foreach (FinancialDate date in financialYearList)
            {
                date.SetName(IsEnglish);
            }

            ddlFinancialYear.DataSource = financialYearList;
            ddlFinancialYear.DataBind();



            JobStatus obJs = new JobStatus();
            List<KeyValue> list = obJs.GetMembers();
            list.RemoveAt(0);
            chkStatusList.DataSource = list;
            chkStatusList.DataBind();


            List<PIncome> incomelist = PayManager.GetIncomeListByCompany(SessionManager.CurrentCompanyId)
                .Where(x => x.Calculation != IncomeCalculation.DEEMED_INCOME && x.Calculation==IncomeCalculation.FIXED_AMOUNT).ToList();

            //incomelist.Add(new PIncome { Title = "PF", IncomeId = 0 });
            chkListSalaries.DataSource = incomelist.OrderBy(x => x.Title).ToList();
            chkListSalaries.DataBind();


            int gid = UrlHelper.GetIdFromQueryString("Id");
            ddlAdvanceBonusList.DataSource = CommonManager.GetBonusList().Where(x => x.BonusId != gid).ToList();
            ddlAdvanceBonusList.DataBind();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            int gid = UrlHelper.GetIdFromQueryString("Id");
            this.ControlID = gid;

            if (!IsPostBack)
            {
                Initialise();
            }
        }

        void Initialise()
        {

            ddlIncomes.DataSource = PayManager.GetVariableIncomeList(SessionManager.CurrentCompanyId,false);
            ddlIncomes.DataBind();

            ddlPeriod.DataSource = BLL.BaseBiz.PayrollDataContext.PayrollPeriods.ToList();
            ddlPeriod.DataBind();

            UIHelper.SetSelectedInDropDown(ddlPeriod,
                CommonManager.GetLastPayrollPeriod().PayrollPeriodId);


            int gid = UrlHelper.GetIdFromQueryString("Id");
            this.ControlID = gid;
            if (gid != 0)
            {
                Bonus gratuity = commonMgr.GetBonus(gid);
                if (gratuity != null)
                {
                    this.ControlID = gid;
                    Process(ref gratuity);
                    btnSave.Text = Resources.Messages.Update;
                }
            }

        }

        void Process(ref Bonus entity)
        {
            if (entity == null)
            {
                entity = new Bonus();
                entity.YearID = int.Parse(ddlFinancialYear.SelectedValue);

                entity.PaidPayrollPeriodId = int.Parse(ddlPeriod.SelectedValue);

                entity.DistributionDateEng = dateDistribution.SelectedDate;
                entity.DistributionDate = BLL.BaseBiz.GetAppropriateDate(entity.DistributionDateEng);
                entity.BonusAmount = decimal.Parse(txtAmount.Text.Trim());
                entity.Name = txtName.Text.Trim();
                entity.Status = int.Parse(ddlDistrbuted.SelectedValue);
                entity.StartDate = dateStart.SelectedDate;
                entity.EndDate = dateEnd.SelectedDate;

                entity.IncomeId = int.Parse(ddlIncomes.SelectedValue);

                if (ddlFinalBonus.SelectedValue != "-1")
                    entity.IsFinalBonus = bool.Parse(ddlFinalBonus.SelectedValue);

                if (entity.IsFinalBonus != null && entity.IsFinalBonus.Value
                    && ddlAdvanceBonusList.SelectedValue != "-1")
                {
                    entity.AdvanceBonusRef_Id = int.Parse(ddlAdvanceBonusList.SelectedValue);
                }

                foreach (ListItem item in chkStatusList.Items)
                {
                    if (item.Selected)
                    {
                        entity.BonusStatus.Add(new BonusStatus { Status = int.Parse(item.Value) });
                    }
                }

                foreach (ListItem item in chkListSalaries.Items)
                {
                    if (item.Selected)
                    {
                        entity.BonusIncomes.Add(new BonusIncome { IncomeId = int.Parse(item.Value) });
                    }
                }
             
            }
            else
            {

                UIHelper.SetSelectedInDropDown(ddlPeriod, entity.PaidPayrollPeriodId);

                if (entity.AddOnId == null || entity.AddOnId == -1)
                    ddlPeriod.Enabled = true;
                else
                    ddlPeriod.Enabled = false;

                UIHelper.SetSelectedInDropDown(ddlFinancialYear, entity.YearID);
                dateDistribution.SelectedDate = entity.DistributionDateEng;
                txtAmount.Text = GetCurrency(entity.BonusAmount);
                txtName.Text = entity.Name == null ? "" : entity.Name;

                if (entity.IncomeId != null)
                    UIHelper.SetSelectedInDropDown(ddlIncomes, entity.IncomeId);

                if (entity.StartDate != null)
                    dateStart.SelectedDate = entity.StartDate.Value;
                if (entity.EndDate != null)
                    dateEnd.SelectedDate = entity.EndDate.Value;

                if (entity.IsFinalBonus != null)
                    UIHelper.SetSelectedInDropDown(ddlFinalBonus, entity.IsFinalBonus.ToString().ToLower());
                else
                    ddlFinalBonus.SelectedValue = "false";

                if (entity.AdvanceBonusRef_Id != null)
                    UIHelper.SetSelectedInDropDown(ddlAdvanceBonusList, entity.AdvanceBonusRef_Id.ToString());

                UIHelper.SetSelectedInDropDown(ddlDistrbuted, entity.Status);

                foreach (BonusStatus status in entity.BonusStatus)
                {
                    ListItem item = chkStatusList.Items.FindByValue(status.Status.ToString());
                    item.Selected = true;
                }

                foreach (BonusIncome status in entity.BonusIncomes)
                {
                    ListItem item = chkListSalaries.Items.FindByValue(status.IncomeId.ToString());
                    item.Selected = true;
                }
            }
        }

       

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {

                if (string.IsNullOrEmpty(dateDistribution.RawText))
                {
                    NewMessage.ShowWarningMessage("Distribution date is required.");
                    return;
                }

                Bonus entity = null;
                Process(ref entity);


                if (entity.IsFinalBonus != null && entity.IsFinalBonus.Value)
                {
                    if (entity.AdvanceBonusRef_Id == null || entity.AdvanceBonusRef_Id == -1)
                    {
                        divWarningMsg.InnerHtml = "Advance bonus should be selected for final bonus.";
                        divWarningMsg.Hide = false;
                        return;
                    }
                }

                if (entity.YearID == -1)
                {
                    divWarningMsg.InnerHtml = "Invalid year selection.";
                    divWarningMsg.Hide = false;
                    return;
                }

                if (this.ControlID == 0)
                {

                    commonMgr.SaveBonus(entity);
                    JavascriptHelper.DisplayClientMsg("Bonus saved.", Page, "closePopup();");
                }
                else
                {
                    entity.BonusId = this.ControlID;



                    bool status = commonMgr.UpdateBonus(entity);
                    if (status)
                        JavascriptHelper.DisplayClientMsg("Bonus updated.", Page, "closePopup();");
                    else
                    {
                        divWarningMsg.InnerHtml = "Bonus is not editable.";
                        divWarningMsg.Hide = false;
                    }
                }
            }
        }
    }
}