﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Base;
using BLL.Manager;
using Utils.Web;
using DAL;
using Utils.Helper;

namespace Web.UserControls.Gratuity
{
    public partial class AEOvertimeCtl : BaseUserControl
    {
        PayManager payMgr = new PayManager();
        CommonManager commonMgr = new CommonManager();


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialise();
            }
        }

        void Initialise()
        {


            ddlPeriod.DataSource = BLL.BaseBiz.PayrollDataContext.PayrollPeriods.ToList();
            ddlPeriod.DataBind();

            //UIHelper.SetSelectedInDropDown(ddlPeriod,
            //    CommonManager.GetLastPayrollPeriod().PayrollPeriodId);

            int gid = UrlHelper.GetIdFromQueryString("Id");
            if (gid != 0)
            {
                OvertimePeriod gratuity = commonMgr.GetOvertimePeriod(gid);
                if (gratuity != null)
                {
                    this.ControlID = gid;
                    Process(ref gratuity);
                    btnSave.Text = Resources.Messages.Update;
                }
            }

        }

        void Process(ref OvertimePeriod entity)
        {
            if (entity == null)
            {
                entity = new OvertimePeriod();
                entity.Name = txtName.Text.Trim();
                entity.StartDate = dateStart.SelectedDate;
                entity.EndDate = dateEnd.SelectedDate;
                entity.PaidPayrollPeriodId = int.Parse(ddlPeriod.SelectedValue);
                entity.Status = int.Parse(ddlDistrbuted.SelectedValue);


            }
            else
            {
                txtName.Text = entity.Name;
                UIHelper.SetSelectedInDropDown(ddlPeriod, entity.PaidPayrollPeriodId);

                if (entity.AddOnId == null || entity.AddOnId == -1)
                    ddlPeriod.Enabled = true;
                else
                    ddlPeriod.Enabled = false;

                dateStart.SelectedDate = entity.StartDate;
                dateEnd.SelectedDate = entity.EndDate;
                UIHelper.SetSelectedInDropDown(ddlDistrbuted, entity.Status);




            }
        }



        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {

                OvertimePeriod entity = null;
                Process(ref entity);


                if (entity.EndDate <= entity.StartDate)
                {
                    divWarningMsg.InnerHtml = "End date should be greater than start date.";
                    divWarningMsg.Hide = false;
                    return;
                }

                if (this.ControlID == 0)
                {
                    //if (!CommonManager.IsOvertimeValid(entity))
                    //{
                    //    divWarningMsg.InnerHtml = Resources.Messages.GratuityRuleInvalid;
                    //    divWarningMsg.Hide = false;
                    //    return;
                    //}

                    Status status = commonMgr.SaveOvertimePeriod(entity);

                    if (status.IsSuccess == false)
                    {
                        JavascriptHelper.DisplayClientMsg(status.ErrorMessage, Page);
                        return;
                    }

                    JavascriptHelper.DisplayClientMsg("Overtime created.", Page, "closePopup();");
                }
                else
                {
                    entity.OvertimeID = this.ControlID;

                    //if (!CommonManager.IsGratuityRuleValid(entity))
                    //{
                    //    divWarningMsg.InnerHtml = Resources.Messages.GratuityRuleInvalid;
                    //    divWarningMsg.Hide = false;
                    //    return;
                    //}

                    bool status = commonMgr.UpdateOvertimePeriod(entity);
                    if (status)
                        JavascriptHelper.DisplayClientMsg("Overtime updated.", Page, "closePopup();");
                    else
                    {
                        divWarningMsg.InnerHtml = "Overtime is not editable.";
                        divWarningMsg.Hide = false;
                    }
                }
            }
        }
    }
}