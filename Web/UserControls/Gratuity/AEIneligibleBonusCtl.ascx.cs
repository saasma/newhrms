﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Base;
using BLL.Manager;
using Utils.Web;
using DAL;
using Utils.Helper;

namespace Web.UserControls.Gratuity
{
    public partial class AEIneligibleBonusCtl : BaseUserControl
    {


        PayManager payMgr = new PayManager();
        CommonManager commonMgr = new CommonManager();

        int bonusId = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            bonusId = int.Parse(Request.QueryString["bonusid"]);


            //AjaxControlToolkit.TextBoxWatermarkExtender
            //AjaxControlToolkit.TextBoxWatermarkExtender t = new AjaxControlToolkit.TextBoxWatermarkExtender();


            if (!IsPostBack)
            {
                Initialise();
            }
        }

        void Initialise()
        {





        }



        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {


                BonusIneligibleEmployee entity = new BonusIneligibleEmployee();
                entity.EmployeeId = int.Parse(hdn.Value);
                entity.Reason = txtReason.Text.Trim();
                entity.BonusId = bonusId;



                commonMgr.SaveIneligibleBonus(entity);
                JavascriptHelper.DisplayClientMsg("Employee saved.", Page, "closePopup();");

            }
        }
    }
}