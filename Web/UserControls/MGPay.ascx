﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MGPay.ascx.cs" Inherits="Web.UserControls.MGPay" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%--<%@ Register Src="../UserControls/EmployeeDeduction.ascx" TagName="EmployeeDeduction"
    TagPrefix="uc1" %>
<%@ Register Src="../UserControls/EmployeeTax.ascx" TagName="EmployeeTax" TagPrefix="uc2" %>--%>
<script type="text/javascript">

    function popupIncomeCall() {
        if (typeof (EmployeeId) != 'undefined') {
            var ret = popupIncome("EId=" + parseInt(EmployeeId));
            if (typeof (ret) != 'undefined') {
                if (ret == 'ReloadIncome') {
                    refreshIncomeList(0);
                }
            }
        }
        return false;
    }
    function ValidateTextBoxBank(source, args) {
        var ddl = document.getElementById('<%= ddlPayMode.ClientID %>');

        args.IsValid = true;
        if (ddl.value == "Bank Deposit" || ddl.value == "Cheque") {
            var str = new String(args.Value);
            if (str.trim() == "-1")
                args.IsValid = false;
        }
    }
    function popupUpdateIncomeCall(empIncomeId) {
        var ret = popupUpdateIncome('Id=' + empIncomeId);
        if (typeof (ret) != 'undefined') {
            if (ret == 'ReloadIncome') {
                refreshIncomeList(0);
            }
        }
        return false;
    }

    function parentIncomeCallbackFunction(text, closingWindow) {
        closingWindow.close();
        refreshIncomeList(0);
    }

    function onChangeForPayMode(ddl) {
        var txtBankACNo = ('<%= txtBankACNo.ClientID %>');
        var txtBankName = '<%= txtBankName.ClientID %>';
        var chk = document.getElementById('<%=chkEnableDualBank.ClientID %>');
        var rowDualBank = document.getElementById('<%=rowDualBank.ClientID %>');

        if (ddl.value == "Bank Deposit" || ddl.value == "Cheque") {
            enableElement(txtBankACNo);
            enableElement(txtBankName);
            rowDualBank.style.display = "";
        }
        else {
            disableElement(txtBankACNo);
            disableElement(txtBankName);
            rowDualBank.style.display = "none";
            chk.checked = false;
        }
    }
    function ValidateBankAmountOrPercentage(source, args) {

        var chk = document.getElementById('<%=chkEnableDualBank.ClientID %>');
        args.IsValid = true;
        var ddlBank1 = document.getElementById('<%=ddlBank1.ClientID %>');
        var ddlBank2 = document.getElementById('<%=ddlBank2.ClientID %>');
        var txtBankAmount = document.getElementById('<%=txtBankAmount.ClientID %>')
        var txtBankPercentage = document.getElementById('<%=txtBankPercentage.ClientID %>');
        if (chk.checked == true) {

            if (ddlBank1.value == "-1") {
                args.IsValid = false;
                alert("First bank is required.");
                ddlBank1.focus();
                return;
            }
            if (ddlBank2.value == "-1") {
                args.IsValid = false;
                alert("Second bank is required.");
                ddlBank2.focus();
                return;
            }

            if (ddlBank1.value == ddlBank2.value) {
                args.IsValid = false;
                alert("Both dual bank can not be same.");
                ddlBank1.focus();
                return;
            }

            if (txtBankAmount.value == ""
                  && txtBankPercentage.value == "") {
                txtBankPercentage.focus();
                alert("Bank amount or percentage is required.");
                args.IsValid = false;
            }

            if (txtBankAmount.value != ""
                  && txtBankPercentage.value != "") {
                txtBankPercentage.focus();
                alert("Both Bank amount and percentage can not be set.");
                args.IsValid = false;
            }
        }

    }
    function ValidateTextBox1(source, args) {
        var ddl = document.getElementById('<%= ddlPayMode.ClientID %>');

        args.IsValid = true;
        if (ddl.value == "Bank Deposit" || ddl.value == "Cheque") {
            var str = new String(args.Value);
            if (str.trim() == "")
                args.IsValid = false;
        }
    }

    //call to delete income
    function deleteIncomeCall(empIncomeId) {
        if (confirm("Do you want to delete the income for the employee?")) {
            refreshIncomeList(empIncomeId);
        }
        return false;
    }

    function refreshIncomeList(empIncomeId) {
        if (typeof (EmployeeId) != 'undefined') {
            showLoading();
            Web.PayrollService.GetIncomeList(empIncomeId, parseInt(EmployeeId), refreshIncomeListCallback);
        }
    }

    function refreshIncomeListCallback(result) {
        if (result) {
            document.getElementById('<%=incomes.ClientID %>').innerHTML
                = result;
        }
        hideLoading();
    }

    function dualBankChange(chk) {
        var tr = document.getElementById('<%=trDualBank.ClientID %>');
        if (chk.checked == true)
            tr.style.display = "";
        else
            tr.style.display = "none";
    }

</script>
<style>
    .straight strong
    {
        display: block;
        padding-top: 10px;
        padding-bottom: 2px;
    }
</style>
<div class="employeSectors" style='padding-left: 20px;'>
    <h2>
        Salary payment</h2>
    <table cellpadding="0" cellspacing="0" width="350px" class="hrinfosinner">
        <tr>
            <td class="lbling" width="150px!important">
                Pay Frequency
            </td>
            <td>
                <asp:DropDownList Style="width: 180px" ID="ddlPayFreq" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="lbling">
                Payment Mode
            </td>
            <td>
                <asp:DropDownList Style="width: 180px" AppendDataBoundItems="true" ID="ddlPayMode"
                    runat="server" onchange="onChangeForPayMode(this)">
                    <asp:ListItem Text="--Select Payment Mode--" Value="-1"></asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator runat="server" ValidationGroup="AEEmployee" ControlToValidate="ddlPayMode"
                    InitialValue="-1" Display="None" ErrorMessage="Please select payment mode" ID="valReqdPayMode3" />
            </td>
        </tr>
        <%--  <div id="divBankAC" runat="server">--%>
        <tr>
            <td class="lbling">
                Bank Name
            </td>
            <td>
                <asp:TextBox Style="width: 178px" ID="txtBankName" runat="server"></asp:TextBox>
                <asp:CustomValidator Display="None" ValidateEmptyText="true" ClientValidationFunction="ValidateTextBox1"
                    ControlToValidate="txtBankName" ValidationGroup="AEEmployee" ID="CustomValidator3"
                    runat="server" ErrorMessage="Bank name is required." OnServerValidate="valCustomRFAC_ServerValidate"></asp:CustomValidator>
            
                 <asp:DropDownList AppendDataBoundItems="true"  ID="ddlBankName" DataTextField="Name" DataValueField="BankID" runat="server"
                    Width="178px">
                    <asp:ListItem Value="-1" Text="--Select Bank--" />
                </asp:DropDownList>
                <asp:CustomValidator Display="None" ValidateEmptyText="true" ClientValidationFunction="ValidateTextBoxBank"
                    ControlToValidate="ddlBankName" ValidationGroup="AEEmployee" ID="custValidateBankName3"
                    runat="server" ErrorMessage="Bank is required." OnServerValidate="ValidateTextBoxBank_ServerValidate"></asp:CustomValidator>
            </td>
        </tr>
        <tr>
            <td class="lbling">
                Bank a/c No
            </td>
            <td>
                <asp:TextBox Style="width: 178px" ID="txtBankACNo" runat="server"></asp:TextBox>
                <asp:CustomValidator Display="None" ValidateEmptyText="true" ClientValidationFunction="ValidateTextBox1"
                    ControlToValidate="txtBankACNo" ValidationGroup="AEEmployee" ID="valCustomRFAC3"
                    runat="server" ErrorMessage="Bank account no is required." OnServerValidate="valCustomRFAC_ServerValidate"></asp:CustomValidator>
            </td>
        </tr>
        <tr runat="server" id="trCurrency" visible="false">
            <td class="lbling">
                Currency
            </td>
            <td>
                <asp:DropDownList Style="width: 178px" ID="ddlCurrency" runat="server">
                    <asp:ListItem Text="NRS" Value="false" />
                    <asp:ListItem Text="GBP" Value="true" />
                </asp:DropDownList>
            </td>
        </tr>
        <tr runat="server" id="rowDualBank">
            <td>
                <asp:CheckBox ID="chkEnableDualBank" onclick="dualBankChange(this)" runat="server"
                    Text="Enable Dual Bank" />
            </td>
            <td>
            </td>
        </tr>
        <tr runat="server" id="trDualBank" style="display: none">
            <td colspan="2">
                <table>
                    <tr>
                        <td>
                            <strong>Primary Bank Name *</strong>
                        </td>
                        <td>
                            <asp:DropDownList AppendDataBoundItems="true" ID="ddlBank1" DataTextField="Name"
                                DataValueField="BankID" runat="server" Width="178px">
                                <asp:ListItem Value="-1" Text="--Select Bank--" />
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Bank a/c No</strong>
                        </td>
                        <td>
                            <asp:TextBox Style="width: 178px" ID="txtBank1ACNo" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Percentage</strong>
                        </td>
                        <td>
                            <asp:TextBox Style="width: 100px" ID="txtBankPercentage" runat="server"></asp:TextBox>
                            &nbsp;or
                            <asp:RangeValidator runat="server" Type="Double" MinimumValue="1" MaximumValue="100"
                                ValidationGroup="AEEmployee" ControlToValidate="txtBankPercentage" Display="None"
                                ErrorMessage="Invalid percentage." ID="RequiredsdfdfFieldValidator3" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Amount</strong>
                        </td>
                        <td>
                            <asp:TextBox Style="width: 100px" ID="txtBankAmount" runat="server"></asp:TextBox>
                            <asp:CompareValidator runat="server" Type="Currency" Operator="DataTypeCheck" ValidationGroup="AEEmployee"
                                ControlToValidate="txtBankAmount" Display="None" ErrorMessage="Invalid amount."
                                ID="CompareValidator38" />
                            <asp:CustomValidator Display="None" ValidateEmptyText="true" ClientValidationFunction="ValidateBankAmountOrPercentage"
                                ControlToValidate="txtBankAmount" ValidationGroup="AEEmployee" ID="CustomVsdfalidator3"
                                runat="server" ErrorMessage=""></asp:CustomValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Pay Remaining Amount *</strong>
                        </td>
                        <td>
                            <asp:DropDownList AppendDataBoundItems="true" ID="ddlBank2" DataTextField="Name"
                                DataValueField="BankID" runat="server" Width="178px">
                                <asp:ListItem Value="-1" Text="--Select Bank--" />
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Bank a/c No</strong>
                        </td>
                        <td>
                            <asp:TextBox Style="width: 178px" ID="txtBank2ACNo" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
<div class="employeSectors" style='margin-right: 0; width: 500px;'>
    <h2>
        Incomes</h2>
    <div id="incomes" runat="server">
    </div>
    <asp:Button ID="btnAddIncome" Enabled="false" CssClass="addbtns" runat="server" OnClientClick="return popupIncomeCall();"
        Text="Add" />
</div>
<br style="clear: both" />
