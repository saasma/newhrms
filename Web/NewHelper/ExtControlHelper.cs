﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ext.Net;
using DAL;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BLL.BO;

namespace Web
{
    public class ExtControlHelper
    {

        /// <summary>
        /// ComboBox selection for first load, in Ext postback use SetValue
        /// </summary>
        /// <param name="value"></param>
        /// <param name="comboBox"></param>
        public static void ComboBoxSetSelected(object value, ComboBox comboBox)
        {
            if (value != null)
            {
                string sel = "";

                sel = value.ToString();
                comboBox.SelectedItems.Clear();
                Ext.Net.ListItem item = new Ext.Net.ListItem("", sel);
                comboBox.SelectedItems.Clear();
                comboBox.SelectedItems.Add(item);
            }
            //    comboBox.SetValue(value.ToString());
        }



        public static void LoadStatus(System.Web.UI.WebControls.Table tableContainer, Type enumType)
        {
            #region "ALL-BUTTON"
            TableRow row = new TableRow();
            TableCell cell = new TableCell();
            Ext.Net.LinkButton btn = new Ext.Net.LinkButton();


            btn.Icon = Icon.NoteGo;
            btn.Text = "All";
            btn.ID = "statusButton0";

            btn.CustomConfig.Add(new ConfigItem("value", "0"));
            btn.Listeners.Click.Handler = "searchListCallback(this);";

            cell.Controls.Add(btn);
            row.Cells.Add(cell);

            tableContainer.Rows.Add(row);

            #endregion

            #region "STATUS-BUTTONS"
            List<TextValue> list = GetTextValues(enumType);
            foreach (var textValue in list)
            {
                row = new TableRow();
                cell = new TableCell();
                btn = new Ext.Net.LinkButton();

                btn.Icon = Icon.NoteGo;
                btn.Text = textValue.Text;
                btn.ID = "statusButton" + textValue.Value;


                btn.CustomConfig.Add(new ConfigItem("value", textValue.Value));
                btn.Listeners.Click.Handler = "searchListCallback(this);";

                cell.Controls.Add(btn);
                row.Cells.Add(cell);

                tableContainer.Rows.Add(row);
            }
            #endregion
        }


        public static void LoadStatusForDocument(HtmlGenericControl container, Type enumType)
        {        
            HtmlGenericControl li = null;            
            HtmlAnchor a = null;
        
            List<TextValue> list = GetTextValues(enumType);
          
            foreach (var textValue in list)
            {

                AddStatus(container, textValue,textValue.Value);

           
            }           
        }

        public static void LoadStatusForDocument(HtmlGenericControl container, Type enumType,Dictionary<string,string> statusCount
            ,params string[] skipValues)
        {
            HtmlGenericControl li = null;
            HtmlAnchor a = null;

            List<TextValue> list = GetTextValues(enumType);

            foreach (var textValue in list)
            {
                if (skipValues.Contains(textValue.Value) == false)
                {
                    if (statusCount.ContainsKey(textValue.Value))
                        textValue.Text += string.Format(" ({0})", statusCount[textValue.Value]);
                    else
                        textValue.Text += " (0)";

                    AddStatus(container, textValue, textValue.Value);

                }
            }
        }

        public static void AddStatus(HtmlGenericControl container, TextValue textValue,string id)
        {
            HtmlGenericControl li = null;
            HtmlAnchor a = null;

            a = new HtmlAnchor();
            li = new HtmlGenericControl();
            li.TagName = "li";
            a.Attributes["class"] = "link1";

            //If all then make it selected as default
            if (textValue.Value.Equals("0"))
                a.Attributes["class"] += " activeState";

            a.InnerText = textValue.Text;
            a.Attributes["value"] = textValue.Value;
            a.ID = "a" + id;
            a.Attributes["href"] = "javascript:searchListCallback(" + textValue.Value + ");";
            li.Controls.Add(a);

            container.Controls.Add(li);
        }

        public static List<TextValue> GetTextValues(Type enumType)
        {
            List<TextValue> optionList = new List<TextValue>();

            foreach (object optionEnum in Enum.GetValues(enumType))
            {
                TextValue obj = new TextValue();

                string option = optionEnum.ToString();
                object optionName = HttpContext.GetGlobalResourceObject("ResourceEnums", option);//.ToString();

                obj.Text = optionName == null ? option : optionName.ToString();
                obj.Value = ((int)optionEnum).ToString();
                optionList.Add(obj);
            }

            return optionList;
        }

        public static String GetTextValue(string enumValue)
        {
            object optionName = HttpContext.GetGlobalResourceObject("ResourceEnums", enumValue);//.ToString();
            return optionName == null ? enumValue : optionName.ToString();
        }
    }
}
