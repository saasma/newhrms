<%@ Page Title="Beta Dashboard" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="DashboardBeta.aspx.cs" Inherits="Web.CP.DashboardBeta" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .dbTitle
        {
            display: inline-block;
            background-color: #E7EFFC;
            width: 97%;
            position: relative;
            border-bottom: thin solid #B2D0E8;
            line-height: 30px;
            vertical-align: middle;
            padding-left: 5px;
            padding-right: 6px;
            border-right: 1px solid #B2D0E8;
        }
        
        .dbTitle a
        {
            float: right;
            text-decoration: none; /* border:thin solid gray;*/
            cursor: pointer;
        }
        .dbTitle h2
        {
            color: #0070C0;
            float: left;
            font-size: 12px;
            margin-top: 4px;
            margin-bottom: 4px;
        }
        
        .subTitle
        {
            color: #616161;
            font-weight: bold;
            font-size: 12px !important;
            background: #F9F9F9;
            line-height: 20px;
            width: 99%;
            margin-top: 2px;
            margin-bottom: 2px;
        }
        .dbPanel
        {
            display: none;
            border: thin solid #B2D0E8;
            margin: 10px 0px 15px 0px;
        }
        
        .dbPanelContent
        {
            padding: 3px;
            display: inline-block;
            width: 100%;
            overflow: hidden; /*width:100%;*/
        }
        
        .Column
        {
            width: 33%;
        }
        
        .contentWrapper > table
        {
            /* padding-left: 8px;
            padding-right: 8px;*/
            width: 100%;
        }
        .contentWrapper > table td
        {
            vertical-align: top; /* width:100%;*/ /* width:33%;*/
            padding-right: 5px;
        }
        
        .hideHeader .x-grid-header-ct
        {
            background-image: none !important;
            display: none !important;
            border: none !important;
        }
        
        .contentWrapper .x-grid-cell-last > .x-grid-cell-inner
        {
            text-align: right !important;
        }
        
        .roundCorner
        {
            border: thin solid #5DA5ED;
            -moz-border-radius: 4px 4px 4px 4px;
            -webkit-border-radius: 4px 4px 4px 4px;
            padding: 0px;
            background: -moz-linear-gradient(center top, #999999 0%,#ffffff 0%);
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0, #999999),color-stop(0, #ffffff));
        }
        
        /*options cases*/
        .option
        {
            padding: 10px;
        }
        .option a, .option span
        {
            text-align: center;
            text-decoration: none;
            font-weight: bold;
        }
        .option img, .option span
        {
            display: block;
            text-decoration: none;
            font-size: 15px;
        }
        .option img
        {
            width: 80px;
            height: 80px;
            padding-left: 12px;
        }
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
        .reportBlock hr
        {
            margin: 7px 0 0 35px;
        }
        .reportArrow
        {
            margin: 3px 0 0 10px;
            width: 15px;
            height: 15px;
            float: left;
        }
        .reports
        {
            padding: 5px 0;
        }
        .panel-heading
        {
            height: 40px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Beta Dashboard
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div>
            <div style="width: 100%; visibility: visible" class="contentWrapper" id="container">
                <table>
                    <tr style="display:none;">
                        <td class="Column" style="width: 70%">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <!-- panel-btns -->
                                    <h3 class="panel-title">
                                        Regular</h3>
                                </div>
                                <div class="panel-body">
                                    <ul role="menu" style="vertical-align:top" class="dropdown-menu dropdown-demo-only icongap">
                                        <li>
                                            <asp:HyperLink ID="HyperLink1" Visible='<%# IsAccessible("CPAttendance/EmpDailyAttedanceReport.aspx") %>'
                                                runat="server" NavigateUrl="~/Attendance/EmpDailyAttedanceReport.aspx">
                                                         <i class="fa fa-bars"></i>Daily Attendance Reports
                                            </asp:HyperLink>
                                        </li>
                                        <li>
                                            <asp:HyperLink ID="HyperLink2" Visible='<%# IsAccessible("Attendance/EmployeeBranchAttendance.aspx") %>'
                                                runat="server" NavigateUrl="~/Attendance/EmployeeBranchAttendance.aspx">
                                                         <i class="fa fa-bars"></i>Branch Attendance Card
                                            </asp:HyperLink>
                                        </li>
                                        <li>
                                            <asp:HyperLink ID="HyperLink3" Visible='<%# IsAccessible("NewHr/EmpRetiringDetailsList.aspx") %>'
                                                runat="server" NavigateUrl="~/NewHr/EmpRetiringDetailsList.aspx">
                                                         <i class="fa fa-bars"></i>Employee Retirement Details
                                            </asp:HyperLink>
                                        </li>
                                    </ul>
                                 
                                   
                                </div>
                            </div>
                          
                            <div style="clear: both">
                            </div>
                        </td>
                        <td class="Column" style="width: 30%">
                            <div class="panel panel-info" runat="server" id="divOvertime">
                                <div class="panel-heading" style="height: 50px!important;    padding-left: 7px!important;">
                                    
                                    
                                </div>
                                
                            </div>
                        </td>
                
                    </tr>

                    <tr>
                        <td class="Column" style="width: 70%">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <!-- panel-btns -->
                                    <h3 class="panel-title">
                                        Regular</h3>
                                </div>
                                <div class="panel-body">
                                    <ul role="menu" style="vertical-align:top" class="dropdown-menu dropdown-demo-only icongap">
                                        <li>
                                            <asp:HyperLink ID="HyperLink4" Visible='<%# IsAccessible("newhr/TrainingMod/TrainingPlanning.aspx") %>'
                                                runat="server" NavigateUrl="~/newhr/TrainingMod/TrainingPlanning.aspx">
                                                         <i class="fa fa-bars"></i>Training Planning
                                            </asp:HyperLink>
                                        </li>
                                        <li>
                                            <asp:HyperLink ID="HyperLink5" Visible='<%# IsAccessible("newhr/TrainingMod/TrainingAdministration.aspx") %>'
                                                runat="server" NavigateUrl="~/newhr/TrainingMod/TrainingAdministration.aspx">
                                                         <i class="fa fa-bars"></i>Training Administration
                                            </asp:HyperLink>
                                        </li>
                                        <li>
                                            <asp:HyperLink ID="HyperLink6" Visible='<%# IsAccessible("newhr/TrainingMod/TrainingEmployee.aspx") %>'
                                                runat="server" NavigateUrl="~/newhr/TrainingMod/TrainingEmployee.aspx">
                                                         <i class="fa fa-bars"></i>Training Employee List
                                            </asp:HyperLink>
                                        </li>
                                    </ul>
                                 
                                   
                                </div>
                            </div>
                          
                            <div style="clear: both">
                            </div>
                        </td>
                        <td class="Column" style="width: 30%">
                            <div class="panel panel-info" runat="server" id="div1">
                                <div class="panel-heading" style="height: 50px!important;    padding-left: 7px!important;">
                                    
                                    
                                </div>
                                
                            </div>
                        </td>
                
                    </tr>
                </table>
            </div>
        </div>
    </div>
    </div>
</asp:Content>
