﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
//using System.Web.UI.DataVisualization.Charting;
using BLL.Base;
using System.Globalization;
using Utils.Calendar;
using BLL.BO;

namespace Web.CP
{
    public partial class DashboardPR : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.Payroll;
            }
        }

        private bool moduleAccessible = false;
        public new bool IsAccessible(string url)
        {
            if (moduleAccessible)
                return true;

            if (SessionManager.User.URole.URoleModules.Any(x => x.ModuleRef_ID == (int)ModuleEnum.HumanResource))
            {
                moduleAccessible = true;
                return true;
            }

            return UserManager.IsPageAccessible(url);

        }

        CommonManager commonMgr = new CommonManager();
        private double totalSalarySummary = 0;

        protected void Page_PreRender(object sender, EventArgs e)
        {
            linkAdditionBasic.Visible = CommonManager.CompanySetting.HasLevelGradeSalary;
            linkReward.Visible = CommonManager.CompanySetting.HasLevelGradeSalary;
            nabilPF.Visible = CommonManager.CompanySetting.WhichCompany != WhichCompany.NIBL;
            niblDashainTD.Visible = CommonManager.CompanySetting.WhichCompany == WhichCompany.NIBL;
            tdNIBLBOnus.Visible = CommonManager.CompanySetting.WhichCompany == WhichCompany.NIBL;

            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.PSI
                || CommonManager.CompanySetting.WhichCompany == WhichCompany.Heifer)
            {

                psiVoucher.Visible = true;
            }
            else if (CommonManager.CompanySetting.WhichCompany == WhichCompany.HPL)
            {
                hplVoucher.Visible = true;
            }
            else if (CommonManager.CompanySetting.WhichCompany == WhichCompany.Oxfam)
                oxfamVoucher.Visible = true;
            else
                normalVoucher.Visible = true;

            linkExcludePFIncome.Visible = CommonManager.CompanySetting.WhichCompany == WhichCompany.Jyoti;

            if (CommonManager.Setting.EnableWeeklyLikePay != null && CommonManager.Setting.EnableWeeklyLikePay.Value)
                tdWeeklyPay.Visible = true;

            //if (CommonManager.CompanySetting.WhichCompany == WhichCompany.LRPB)
            //    liYearEndTaxReport.Visible = true;
            //else
            //    liYearEndTaxReport.Visible = false;


            ulinsurance.Visible = CommonManager.CompanySetting.WhichCompany == WhichCompany.SaveTheChidlren;



        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialise();
                this.DataBind();
                //LoadChart();
            }
        }

        void Initialise()
        {

            // First parepare this month & next month dates
            CustomDate today = CustomDate.GetTodayDate(IsEnglish);
            int currentYear = today.Year;
            DateTime FromThisDate, FromNextDate;
            DateTime ToThisDate, ToNextDate;
            CustomDate firstDateOfThisMonth = new CustomDate(1, today.Month, currentYear, IsEnglish);
            CustomDate lastDateOfThisMonth = firstDateOfThisMonth.GetLastDateOfThisMonth();

            FromThisDate = DateManager.GetStartDate(firstDateOfThisMonth.EnglishDate);
            ToThisDate = DateManager.GetEndDate(lastDateOfThisMonth.EnglishDate);

            firstDateOfThisMonth = lastDateOfThisMonth.IncrementByOneDay();
            lastDateOfThisMonth = firstDateOfThisMonth.GetLastDateOfThisMonth();

            FromNextDate = DateManager.GetStartDate(firstDateOfThisMonth.EnglishDate);
            ToNextDate = DateManager.GetEndDate(lastDateOfThisMonth.EnglishDate);



            PayrollPeriod period = CommonManager.GetLastPayrollPeriod();
            if (period != null)
            {
                currentPeriod.InnerHtml = period.Name;
                if (this.IsEnglish == false)
                {
                    currentPeriod.InnerHtml += " (" +
                        period.StartDateEng.Value.ToString("dd MMM yyyy") + "/" +
                         period.EndDateEng.Value.ToString("dd MMM yyyy") + ")";
                }

                totalEmployees.InnerHtml = CalculationManager.GetTotalApplicableForSalary(period.PayrollPeriodId, false)
                    .ToString();
                savedEmployees.InnerHtml = new CalculationManager().GetTotalEmployeeInitialSave(period.PayrollPeriodId, false)
                    .ToString();


            }




            // CIT Chnage request
            int count = BLL.BaseBiz.PayrollDataContext.CITChangeRequests.Count(
                x => x.Status == (int)CITRequestStatus.SaveAndSend);
            if (count > 0 || CommonManager.CompanySetting.WhichCompany == WhichCompany.Prabhu)
                spanRequests.InnerHtml = count.ToString();
            else
                divCITRequest.Visible = false;

            // export with huge data is slow and timeout appears, so hide for these company for now
            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.NIBL || CommonManager.CompanySetting
                .WhichCompany == WhichCompany.Prabhu)
                liYearEndSummaryReport.Visible = true;

            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.Oxfam)
            {
                linkOptimum1.HRef = "~/cp/OxfamOptimumPFAndCIT.aspx";

            }

        }


        //private void LoadChart()
        //{

        //    List<SalaryValues> listResult = new List<SalaryValues>();
        //    string[] spl = new string[] { };

        //    int i = 0;
        //    SalaryValues obj = new SalaryValues();

        //    List<DashboardSalaryDetailsOfLastSixMonthsResult> list = CalculationManager.GetDashboardSalaryOfLastSixMonths();
        //    if (list.Count == 0)
        //        divChart.Visible = false;
        //    else
        //    {
        //        foreach (var item in list)
        //        {
        //            i++;

        //            if (i == 1)
        //            {
        //                spl = item.Period.Split('/');
        //                obj.Name = spl[0].ToString();
        //            }

        //            if (item.Type == "Gross")
        //            {
        //                obj.GrossSalary = item.Amount.Value/1000;
        //            }
        //            else if (item.Type == "Tax")
        //            {
        //                obj.Tax = obj.Tax + item.Amount.Value/1000;
        //            }
        //            else
        //                obj.Net = item.Amount.Value/1000;


        //            if (i == 4)
        //            {
        //                listResult.Add(obj);
        //                i = 0;
        //                obj = new SalaryValues();
        //            }


        //        }
        //    }


        //    listResult.Reverse();
        //    Chart2.GetStore().DataSource = listResult;
        //    Chart2.GetStore().DataBind();
        //}


    }

    public class SalaryValues
    {
        public string Name { get; set; }
        public decimal GrossSalary { get; set; }
        public decimal Tax { get; set; }
        public decimal Net { get; set; }
    }


}
