﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
//using System.Web.UI.DataVisualization.Charting;
using BLL.Base;
using System.Globalization;
using Utils.Calendar;
using BLL.BO;
using Ext.Net;

namespace Web.CP
{
    public partial class DashboardBeta : BasePage
    {
        CommonManager commonMgr = new CommonManager();
        private double totalSalarySummary = 0;

        private bool moduleAccessible = false;
        public new bool IsAccessible(string url)
        {
            if (moduleAccessible)
                return true;

            if (SessionManager.User.URole.URoleModules.Any(x => x.ModuleRef_ID == (int)ModuleEnum.ProjectAndTimesheet))
            {
                moduleAccessible = true;
                return true;
            }

            return UserManager.IsPageAccessible(url);

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialise();
                this.DataBind();
            }
        }

        void Initialise()
        {
            

        }

       
    }
}
