<%@ Page Title="Payroll Dashboard" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="DashboardPR.aspx.cs" Inherits="Web.CP.DashboardPR" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .dbTitle {
            display: inline-block;
            background-color: #E7EFFC;
            width: 97%;
            position: relative;
            border-bottom: thin solid #B2D0E8;
            line-height: 30px;
            vertical-align: middle;
            padding-left: 5px;
            padding-right: 6px;
            border-right: 1px solid #B2D0E8;
        }

            .dbTitle a {
                float: right;
                text-decoration: none; /* border:thin solid gray;*/
                cursor: pointer;
            }

        .panel-heading {
            padding: 10px 20px 10px 10px !important;
        }

        .dbTitle h2 {
            color: #0070C0;
            float: left;
            font-size: 12px;
            margin-top: 4px;
            margin-bottom: 4px;
        }

        .subTitle {
            color: #616161;
            font-weight: bold;
            font-size: 12px !important;
            background: #F9F9F9;
            line-height: 20px;
            width: 99%;
            margin-top: 2px;
            margin-bottom: 2px;
        }

        .dbPanel {
            display: none;
            border: thin solid #B2D0E8;
            margin: 10px 0px 15px 0px;
        }

        .dbPanelContent {
            padding: 3px;
            display: inline-block;
            width: 100%;
            overflow: hidden; /*width:100%;*/
        }

        .Column {
            width: 33%;
        }

        .contentWrapper > table {
            /* padding-left: 8px;
            padding-right: 8px;*/
            width: 100%;
        }

            .contentWrapper > table td {
                vertical-align: top; /* width:100%;*/ /* width:33%;*/
                padding-right: 5px;
            }

        .hideHeader .x-grid-header-ct {
            background-image: none !important;
            display: none !important;
            border: none !important;
        }

        .contentWrapper .x-grid-cell-last > .x-grid-cell-inner {
            text-align: right !important;
        }

        .roundCorner {
            border: thin solid #5DA5ED;
            -moz-border-radius: 4px 4px 4px 4px;
            -webkit-border-radius: 4px 4px 4px 4px;
            padding: 0px;
            background: -moz-linear-gradient(center top, #999999 0%,#ffffff 0%);
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0, #999999),color-stop(0, #ffffff));
        }

        /*options cases*/
        .option {
            padding: 8px;
        }

            .option a, .option span {
                text-align: center;
                text-decoration: none;
                font-weight: bold;
            }

            .option img, .option span {
                width: 110px;
                display: block;
                text-decoration: none;
                font-size: 15px;
            }

            .option img {
            }

        .reportBlockdiv {
            padding: 5px;
        }

        .reportBlock hr {
            margin: 7px 0 0 35px;
        }

        .reportArrow {
            margin: 0 0 0 10px;
            width: 15px;
            height: 15px;
            float: left;
        }

        .reports {
            padding: 5px 0;
        }
    </style>
    <script type="text/javascript">
        var tipRenderer1 = function (data1, data2) {

            //this.setTitle(data2.value[1].toString());
            this.setTitle(getFormattedAmount((data2.value[1] * 1000)).toString());

        };

    </script>
    <ext:ResourcePlaceHolder ID="ResourcePlaceHolder1" runat="server" Mode="Script" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>Payroll Dashboard
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel" style="padding-left: 10px">
        <div>
            <div>
                <div style="width: 100%; visibility: visible;" class="contentWrapper" id="container">
                    <table>
                        <tr>
                            <td class="Column" style="width: 70%">
                                <%-- <div id="divChart" runat="server" class="col-sm-12">
                                    <div class="panel panel-primary" style="border: 0px">
                                        <div class="panel-body panel-body-small" style="margin-top: 15px">
                                            <ext:Panel ID="pnlChart" Height="300" Width="700" runat="server">
                                                <Items>
                                                    <ext:Chart ID="Chart2" runat="server" Height="300" Width="700" Region="Center" Shadow="true"
                                                        StyleSpec="background:#fff" Animate="true">
                                                        <LegendConfig Position="Bottom" BoxStroke="false">
                                                        </LegendConfig>
                                                        <Store>
                                                            <ext:Store ID="ProfitAndLossStore" runat="server" AutoDataBind="true">
                                                                <Model>
                                                                    <ext:Model ID="Model1" runat="server">
                                                                        <Fields>
                                                                            <ext:ModelField Name="Name" Type="String" />
                                                                            <ext:ModelField Name="GrossSalary" Type="Float" />
                                                                            <ext:ModelField Name="Tax" Type="Float" />
                                                                            <ext:ModelField Name="Net" Type="Float" />
                                                                        </Fields>
                                                                    </ext:Model>
                                                                </Model>
                                                            </ext:Store>
                                                        </Store>
                                                        <Axes>
                                                            <ext:NumericAxis Fields="Net" Grid="true" Title="Thousands">
                                                                <Label>
                                                                    <Renderer Handler="return Ext.util.Format.number(value, '0,0');" />
                                                                </Label>
                                                            </ext:NumericAxis>
                                                            <ext:CategoryAxis Position="Bottom" Fields="Name" Grid="true">
                                                            </ext:CategoryAxis>
                                                        </Axes>
                                                        <Series>
                                                            <ext:ColumnSeries Axis="Left" Highlight="true" XField="Name" YField="GrossSalary,Tax,Net"
                                                                GroupGutter="10">
                                                                <Tips TrackMouse="true" Width="120" Height="28" ID="columnTips">
                                                                    <Renderer Fn="tipRenderer1" />
                                                                </Tips>
                                                            </ext:ColumnSeries>
                                                        </Series>
                                                    </ext:Chart>
                                                </Items>
                                            </ext:Panel>
                                        </div>
                                    </div>
                                </div>--%>
                                <div class="btn-list" style="padding-left: 10px">
                                    <a id="A2" class="btn btn-default btn-metro" href="~/newhr/PREmployeeListing.aspx?Mode=1"
                                        runat="server" visible='<%# IsAccessible("newhr/PREmployeeListing.aspx?Mode=1") %>'>
                                        <span><span class="glyphicon dashboard">
                                            <img src="../css/DashboardIcons/employee_list.png" alt="" /></span> Employee List</span></a>
                                    <a id="A16" href="~/CP/Calculation.aspx" runat="server" visible='<%# IsAccessible("CP/Calculation.aspx") %>'>
                                        <span class="btn btn-default btn-metro"><span class="glyphicon dashboard">
                                            <img src="../css/DashboardIcons/pay_calculation.png" alt="" /></span>Pay Calculation
                                        </span></a><a class="btn btn-default btn-metro" visible='<%# IsAccessible("~/CP/AddOnPayCalculation.aspx") %>'
                                            id="A19" runat="server" href="~/CP/AddOnPayCalculation.aspx"><span class="glyphicon dashboard">
                                                <img src="../css/DashboardIcons/mass_increment.png" alt="" /></span>Add-On Pay</a>
                                    <a id="A17" href="~/CP/HoldPaymentPG.aspx" runat="server" visible='<%# IsAccessible("CP/HoldPaymentPG.aspx") %>'>
                                        <span class="btn btn-default btn-metro"><span class="glyphicon dashboard">
                                            <img src="../css/DashboardIcons/laptop_chart.png" alt="" /></span>Hold Payment</span></a>
                                    <a id="A12" href="~/CP/StopPaymentPG.aspx" runat="server" visible='<%# IsAccessible("CP/StopPaymentPG.aspx") %>'>
                                        <span class="btn btn-default btn-metro"><span class="glyphicon dashboard">
                                            <img src="../css/DashboardIcons/laptop_chart.png" alt="" /></span>Stop Payment</span></a>
                                    <a class="btn btn-default btn-metro" visible='<%# IsAccessible("~/CP/MassIncrement.aspx") %>'
                                        id="levelSalaryChange" runat="server" href="~/CP/MassIncrement.aspx"><span class="glyphicon dashboard">
                                            <img src="../css/DashboardIcons/mass_increment.png" alt="" /></span>Mass Increment</a>
                                    <a class="btn btn-default btn-metro" visible='<%# IsAccessible("~/cp/PayReportSendIntoMail.aspx") %>'
                                        id="A54" runat="server" href="~/cp/PayReportSendIntoMail.aspx"><span class="glyphicon dashboard">
                                            <img src="../css/DashboardIcons/mass_increment.png" alt="" /></span>Mail Payslip</a>
                                    <a class="btn btn-default btn-metro" visible='<%# IsAccessible("~/newhr/AdditionBasicList.aspx") %>'
                                        id="linkAdditionBasic" runat="server" href="~/newhr/AdditionBasicList.aspx"><span
                                            class="glyphicon dashboard">
                                            <img src="../css/DashboardIcons/mass_increment.png" alt="" /></span>Additional
                                        Income </a><a class="btn btn-default btn-metro" visible='<%# IsAccessible("~/newhr/GradeRewardList.aspx") %>'
                                            id="linkReward" runat="server" href="~/newhr/GradeRewardList.aspx"><span class="glyphicon dashboard">
                                                <img src="../css/DashboardIcons/mass_increment.png" alt="" /></span>Reward</a>
                                </div>
                                <div class="col-sm-12" style="display: none">
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <!-- panel-btns -->
                                            <h3 class="panel-title">Regular</h3>
                                        </div>
                                        <div class="panel-body panel-body-small">
                                            <ul role="menu" class="dropdown-menu dropdown-demo-only custommenulist">
                                                <li>
                                                    <asp:HyperLink ID="HyperLink15" Visible='<%# IsAccessible("newhr/PREmployeeListing.aspx?Mode=1") %>'
                                                        runat="server" NavigateUrl="~/newhr/PREmployeeListing.aspx?Mode=1">
                                                         <i class="fa fa-bars"></i>Employee List
                                                    </asp:HyperLink>
                                                </li>
                                                <li>
                                                    <asp:HyperLink ID="HyperLink16" Visible='<%# IsAccessible("CP/Calculation.aspx") %>'
                                                        runat="server" NavigateUrl="~/CP/Calculation.aspx">
                                                         <i class="fa fa-bars"></i>Pay Calculation
                                                    </asp:HyperLink>
                                                </li>
                                                <li>
                                                    <asp:HyperLink ID="HyperLink19" Visible='<%# IsAccessible("CP/Extension/VariableSalaryImport.aspx") %>'
                                                        runat="server" NavigateUrl="~/CP/Extension/VariableSalaryImport.aspx">
                                                         <i class="fa fa-bars"></i>Salary Import
                                                    </asp:HyperLink>
                                                </li>
                                                <li>
                                                    <asp:HyperLink ID="HyperLink20" Visible='<%# IsAccessible("CP/Extension/VariableDeductionImport.aspx") %>'
                                                        runat="server" NavigateUrl="~/CP/Extension/VariableDeductionImport.aspx">
                                                         <i class="fa fa-bars"></i>Deduction Import
                                                    </asp:HyperLink>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="panel panel-primary" style="display: none">
                                        <div class="panel-heading">
                                            <!-- panel-btns -->
                                            <h3 class="panel-title">Common Tasks</h3>
                                        </div>
                                        <div class="panel-body panel-body-small">
                                            <ul role="menu" class="dropdown-menu dropdown-demo-only custommenulist">
                                                <li>
                                                    <asp:HyperLink ID="HyperLink1" Visible='<%# IsAccessible("newhr/AdditionBasicList.aspx") %>'
                                                        runat="server" NavigateUrl="~/newhr/AdditionBasicList.aspx">
                                                         <i class="fa fa-bars"></i>Additional
                                                                Income
                                                    </asp:HyperLink>
                                                </li>
                                                <li>
                                                    <asp:HyperLink ID="HyperLink2" Visible='<%# IsAccessible("CP/MassIncrement.aspx") %>'
                                                        runat="server" NavigateUrl="~/CP/MassIncrement.aspx">
                                                         <i class="fa fa-bars"></i>Mass Increment
                                                    </asp:HyperLink>
                                                </li>
                                                <li>
                                                    <asp:HyperLink ID="h1" Visible='<%# IsAccessible("CP/OptimumPFAndCIT.aspx") %>'
                                                        runat="server" NavigateUrl="~/CP/OptimumPFAndCIT.aspx">
                                                         <i class="fa fa-bars"></i>Optimum
                                                                CIT
                                                    </asp:HyperLink>
                                                </li>
                                                <li>
                                                    <asp:HyperLink ID="HyperLink4" Visible='<%# IsAccessible("CP/AddOnPayCalculation.aspx") %>'
                                                        runat="server" NavigateUrl="~/CP/AddOnPayCalculation.aspx">
                                                         <i class="fa fa-bars"></i>Add-On
                                                                Pay
                                                    </asp:HyperLink>
                                                </li>
                                            </ul>
                                            <div style="clear: both">
                                            </div>
                                            <ul role="menu" class="dropdown-menu dropdown-demo-only custommenulist">
                                                <li>
                                                    <asp:HyperLink ID="HyperLink5" Visible='<%# IsAccessible("newhr/GradeRewardList.aspx") %>'
                                                        runat="server" NavigateUrl="~/newhr/GradeRewardList.aspx">
                                                         <i class="fa fa-bars"></i>Reward
                                                    </asp:HyperLink>
                                                </li>
                                                <li>
                                                    <asp:HyperLink ID="HyperLink7" Visible='<%# IsAccessible("CP/PrivisionalCIT.aspx") %>'
                                                        runat="server" NavigateUrl="~/CP/PrivisionalCIT.aspx">
                                                         <i class="fa fa-bars"></i>Employee
                                                                CIT
                                                    </asp:HyperLink>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div id="Div1" class="panel panel-dark-head no-radius" runat="server">
                                        <div class="panel-heading no-radius">
                                            <!-- panel-btns -->
                                            <h3 class="panel-title">Common Tasks</h3>
                                        </div>
                                        <div class="panel-body panel-body-small">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <ul role="menu" class="dropdown-menu dropdown-demo-only custommenugap">
                                                            <li class="dashboardMenuHeader"><a>Import</a></li>
                                                            <li class="divider fulldivider" role="presentation"></li>
                                                            <li>
                                                                <asp:HyperLink ID="HyperLink22" Visible='<%# IsAccessible("CP/Extension/VariableSalaryImport.aspx") %>'
                                                                    runat="server" NavigateUrl="~/CP/Extension/VariableSalaryImport.aspx">
                                                                    <i class="fa fa-bars"></i> Income Import
                                                                </asp:HyperLink>
                                                            </li>
                                                            <li>
                                                                <asp:HyperLink ID="HyperLink23" Visible='<%# IsAccessible("CP/Extension/VariableDeductionImport.aspx") %>'
                                                                    runat="server" NavigateUrl="~/CP/Extension/VariableDeductionImport.aspx">
                                                                    <i class="fa fa-bars"></i>Deduction Import
                                                                </asp:HyperLink>
                                                            </li>
                                                            <li>
                                                                <asp:HyperLink ID="HyperLink24" Visible='<%# IsAccessible("CP/Extension/DeemedSalaryImport.aspx") %>'
                                                                    Text="" runat="server" NavigateUrl="~/CP/Extension/DeemedSalaryImport.aspx" pageid="48">
                                                                    <i class="fa fa-bars"></i>Deemed Income Import
                                                                </asp:HyperLink>
                                                            </li>
                                                            <li>
                                                                <asp:HyperLink ID="HyperLink25" Visible='<%# IsAccessible("newhr/SILoanImport.aspx") %>'
                                                                    runat="server" NavigateUrl="~/newhr/SILoanImport.aspx">
                                                                    <i class="fa fa-bars"></i>Simple Interest Loan Import
                                                                </asp:HyperLink>
                                                            </li>
                                                            <li>
                                                                <asp:HyperLink ID="HyperLink3" Visible='<%# IsAccessible("newhr/EMILoanImport.aspx") %>'
                                                                    runat="server" NavigateUrl="~/newhr/EMILoanImport.aspx">
                                                                    <i class="fa fa-bars"></i>EMI Loan Import
                                                                </asp:HyperLink>
                                                            </li>
                                                            <li><a id="A49" runat="server" visible='<%# IsAccessible("newhr/LoanRepayment.aspx") %>'
                                                                href="~/newhr/LoanRepayment.aspx"><i class="fa fa-bars"></i>Loan Repayment Import</a>
                                                            </li>
                                                            <li><a id="A72" runat="server" visible='<%# IsAccessible("newhr/EmpMaritalAndTaxStatusDetails.aspx") %>'
                                                                href="~/newhr/EmpMaritalAndTaxStatusDetails.aspx"><i class="fa fa-bars"></i>Tax Status Import</a>
                                                            </li>

                                                        </ul>
                                                    </td>
                                                    <td>
                                                        <ul role="menu" class="dropdown-menu dropdown-demo-only custommenugap">
                                                            <li class="dashboardMenuHeader"><a>Adjustment</a></li>
                                                            <li class="divider fulldivider" role="presentation"></li>
                                                            <li><a id="A60" runat="server" visible='<%# IsAccessible("CP/IncomeDeductionAdjustment.aspx") %>'
                                                                href="~/CP/IncomeDeductionAdjustment.aspx"><i class="fa fa-bars"></i>Income Adjustment</a>
                                                            </li>
                                                            <li><a id="A3" runat="server" visible='<%# IsAccessible("CP/MassIncrement.aspx") %>'
                                                                href="~/CP/MassIncrement.aspx"><i class="fa fa-bars"></i>Mass Increment</a>
                                                            </li>
                                                            <li><a id="A32" runat="server" visible='<%# IsAccessible("CP/LoanAdjustment.aspx") %>'
                                                                href="~/CP/LoanAdjustment.aspx"><i class="fa fa-bars"></i>Loan Adjustment</a>
                                                            </li>
                                                            <li><a id="A35" runat="server" visible='<%# IsAccessible("CP/EMIAdjustment.aspx") %>'
                                                                href="~/CP/EMIAdjustment.aspx"><i class="fa fa-bars"></i>EMI Adjustment</a>
                                                            </li>
                                                            <li><a id="A36" runat="server" visible='<%# IsAccessible("CP/AdvanceAdjustment.aspx") %>'
                                                                href="~/CP/AdvanceAdjustment.aspx"><i class="fa fa-bars"></i>Advance Adjustment</a>
                                                            </li>
                                                            <li>
                                                                <a id="A57" runat="server" visible='<%# IsAccessible("CP/FestivalIncomeAdjustment.aspx") %>'
                                                                    href="~/CP/FestivalIncomeAdjustment.aspx"><i class="fa fa-bars"></i>Festival Income Adjustment</a>
                                                            </li>
                                                            <li>
                                                                <a id="A68" runat="server" visible='<%# IsAccessible("CP/pastregularincomeadjustment.aspx") %>'
                                                                    href="~/CP/pastregularincomeadjustment.aspx"><i class="fa fa-bars"></i>Past Regular Adjustment</a>
                                                            </li>
                                                        </ul>
                                                    </td>
                                                    <td>
                                                        <ul role="menu" class="dropdown-menu dropdown-demo-only custommenugap">
                                                            <li class="dashboardMenuHeader"><a>Tax Planning</a></li>
                                                            <li class="divider fulldivider" role="presentation"></li>
                                                            <li><a id="linkOptimum1" runat="server" visible='<%# IsAccessible("CP/OptimumPFAndCIT.aspx") %>'
                                                                href="~/CP/OptimumPFAndCIT.aspx"><i class="fa fa-bars"></i>Optimum CIT</a></li>
                                                            <li><a id="A40" runat="server" visible='<%# IsAccessible("CP/PrivisionalCIT.aspx") %>'
                                                                href="~/CP/PrivisionalCIT.aspx"><i class="fa fa-bars"></i>Define CIT</a></li>
                                                            <li><a id="A50" runat="server" visible='<%# IsAccessible("newhr/CharityList.aspx") %>'
                                                                href="~/newhr/CharityList.aspx"><i class="fa fa-bars"></i>Charity and Donation</a></li>
                                                            <li><a id="A29" runat="server" visible='<%# IsAccessible("CP/InsuranceList.aspx") %>'
                                                                href="~/CP/InsuranceList.aspx"><i class="fa fa-bars"></i>Life Insurance</a></li>
                                                            <li><a id="A51" runat="server" visible='<%# IsAccessible("CP/ManageMedicalTaxCredit.aspx") %>'
                                                                href="~/CP/ManageMedicalTaxCredit.aspx"><i class="fa fa-bars"></i>Medical Tax Credit</a></li>
                                                        </ul>
                                                    </td>
                                                    <td runat="server" id="ulinsurance">
                                                        <ul role="menu" class="dropdown-menu dropdown-demo-only custommenugap">
                                                            <li class="dashboardMenuHeader"><a>Insurance</a></li>
                                                            <li class="divider fulldivider" role="presentation"></li>
                                                            <li><a id="liInsurancClaimL" runat="server" visible='<%# IsAccessible("CP/InsurenceClaim.aspx") %>' href="~/CP/InsurenceClaim.aspx"><i class="fa fa-bars"></i>Insurance Claim List</a></li>
                                                            <li><a id="liLot" runat="server" visible='<%# IsAccessible("CP/InsurenceClaimLot.aspx") %>' href="~/CP/InsurenceClaimLot.aspx"><i class="fa fa-bars"></i>Create Lot</a></li>
                                                        </ul>
                                                    </td>
                                                    <td runat="server" id="niblDashainTD">
                                                        <ul role="menu" class="dropdown-menu dropdown-demo-only custommenugap">
                                                            <li class="dashboardMenuHeader"><a>Dashain</a></li>
                                                            <li class="divider fulldivider" role="presentation"></li>
                                                            <li><a id="A33" runat="server" visible='<%# IsAccessible("CP/FestivalPeriodList.aspx") %>'
                                                                href="~/CP/FestivalPeriodList.aspx"><i class="fa fa-bars"></i>Dashain Bonus</a></li>

                                                        </ul>
                                                    </td>
                                                    <td runat="server" id="tdWeeklyPay" visible="false">
                                                        <ul role="menu" class="dropdown-menu dropdown-demo-only custommenugap">
                                                            <li class="dashboardMenuHeader"><a>Periodic Pay</a></li>
                                                            <li class="divider fulldivider" role="presentation"></li>
                                                            <li><a id="A64" runat="server" visible='<%# IsAccessible("newhr/payGroupManager.aspx") %>'
                                                                href="~/newhr/payGroupManager.aspx"><i class="fa fa-bars"></i>Pay Groups</a></li>
                                                            <li><a id="A65" runat="server" visible='<%# IsAccessible("newhr/PeriodicPay/PayList.aspx") %>'
                                                                href="~/newhr/PeriodicPay/PayList.aspx"><i class="fa fa-bars"></i>Pay List</a></li>

                                                            <li><a id="A63" runat="server" visible='<%# IsAccessible("cp/PeriodicPayDetails.aspx") %>'
                                                                href="~/cp/PeriodicPayDetails.aspx"><i class="fa fa-bars"></i>Periodic Pay</a></li>

                                                        </ul>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="panel panel-dark-head no-radius" runat="server">
                                        <div class="panel-heading no-radius">
                                            <!-- panel-btns -->
                                            <h3 class="panel-title">Reports</h3>
                                        </div>
                                        <div class="panel-body panel-body-small">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <ul role="menu" class="dropdown-menu dropdown-demo-only custommenugap">
                                                            <li class="dashboardMenuHeader"><a>Monthly Pay Reports</a></li>
                                                            <li class="divider fulldivider" role="presentation"></li>
                                                            <li>
                                                                <asp:HyperLink ID="HyperLink8" Visible='<%# IsAccessible("CP/Report/PaySummary.aspx") %>'
                                                                    runat="server" NavigateUrl="~/CP/Report/PaySummary.aspx">
                                                                    <i class="fa fa-bars"></i> Pay Summary
                                                                </asp:HyperLink>
                                                            </li>
                                                            <li>
                                                                <asp:HyperLink ID="HyperLink6" Visible='<%# IsAccessible("CP/Report/NIBLGrossSalaryReport.aspx") %>'
                                                                    runat="server" NavigateUrl="~/CP/Report/NIBLGrossSalaryReport.aspx">
                                                                    <i class="fa fa-bars"></i> Pay Summary Tax Only
                                                                </asp:HyperLink>
                                                            </li>
                                                            <li><a id="A21" runat="server" visible='<%# IsAccessible("CP/Report/SalaryExtract.aspx") %>'
                                                                href="~/CP/Report/SalaryExtract.aspx"><i class="fa fa-bars"></i>Salary Extract</a>
                                                            </li>
                                                            <li>
                                                                <asp:HyperLink ID="HyperLink9" Visible='<%# IsAccessible("CP/Report/PayBank.aspx") %>'
                                                                    Text="" runat="server" NavigateUrl="~/CP/Report/PayBank.aspx" pageid="48">
                                                                    <i class="fa fa-bars"></i>Bank Payment
                                                                </asp:HyperLink>
                                                            </li>
                                                            <li>
                                                                <asp:HyperLink ID="HyperLink10" Visible='<%# IsAccessible("CP/Report/PayCash.aspx") %>'
                                                                    runat="server" NavigateUrl="~/CP/Report/PayCash.aspx">
                                                                 <i class="fa fa-bars"></i>Cash Payment
                                                                </asp:HyperLink>
                                                            </li>
                                                            <li id="normalVoucher" visible="false" runat="server"><a id="normalVoucher11" runat="server"
                                                                visible='<%# IsAccessible("CP/VoucherList.aspx") %>' href="~/CP/VoucherList.aspx">
                                                                <i class="fa fa-bars"></i>Accounting Voucher</a> </li>
                                                            <li><a id="A20" runat="server" visible='<%# IsAccessible("CP/Report/TaxDetails.aspx") %>'
                                                                href="~/CP/Report/TaxDetails.aspx"><i class="fa fa-bars"></i>Tax Details</a>
                                                            </li>
                                                            <%-- <li><a id="A66" runat="server" visible='<%# IsAccessible("CP/Report/YearlyIncomeVerification.aspx") %>'
                                                                href="~/CP/Report/YearlyIncomeVerification.aspx"><i class="fa fa-bars"></i>Yearly Income Verification</a>
                                                            </li>--%>
                                                        </ul>
                                                        <ul role="menu" class="dropdown-menu dropdown-demo-only custommenugap">
                                                            <li class="dashboardMenuHeader"><a>Employee Service</a></li>
                                                            <li class="divider fulldivider" role="presentation"></li>
                                                            <li><a id="A22" runat="server" visible='<%# IsAccessible("CP/Report/PaySlipDetail.aspx") %>'
                                                                href="~/CP/Report/PaySlipDetail.aspx"><i class="fa fa-bars"></i>Pay Slip</a>
                                                            </li>
                                                            <li><a id="A43" runat="server" visible='<%# IsAccessible("CP/Report/RetiredPaySlipDetail.aspx") %>'
                                                                href="~/CP/Report/RetiredPaySlipDetail.aspx"><i class="fa fa-bars"></i>Retired Payslip</a>
                                                            </li>
                                                            <li>
                                                                <asp:HyperLink ID="HyperLink21" Visible='<%# IsAccessible("cp/PayReportSendIntoMail.aspx") %>'
                                                                    runat="server" NavigateUrl="~/cp/PayReportSendIntoMail.aspx">
                                                                        <i class="fa fa-bars"></i>Mail Payslip
                                                                </asp:HyperLink>
                                                            </li>
                                                            <%--<li>
                                                                <asp:HyperLink ID="HyperLink18" Visible='<%# IsAccessible("CP/TaxDetailsSummary.aspx") %>'
                                                                    runat="server" NavigateUrl="~/CP/TaxDetailsSummary.aspx">
                                                                     <i class="fa fa-bars"></i>Tax Details Summary
                                                                </asp:HyperLink>
                                                            </li>--%>
                                                        </ul>
                                                        <ul role="menu" class="dropdown-menu dropdown-demo-only custommenugap">
                                                            <li class="dashboardMenuHeader"><a>Yearly Reports</a></li>
                                                            <li class="divider fulldivider" role="presentation"></li>
                                                            <li><a id="A1" runat="server" visible='<%# IsAccessible("CP/Report/PaySummaryMultiple.aspx") %>'
                                                                href="~/CP/Report/PaySummaryMultiple.aspx"><i class="fa fa-bars"></i>Consolidated
                                                                Summary</a>
                                                            </li>
                                                            <li runat="server" id="liYearEndTaxReport">
                                                                <a id="A59" runat="server" visible='<%# IsAccessible("cp/yearendtaxreport.aspx") %>'
                                                                    href="~/cp/yearendtaxreport.aspx"><i class="fa fa-bars"></i>Year End Tax Report</a>
                                                            </li>
                                                            <li runat="server" id="liYearEndSummaryReport">
                                                                <a id="A62" runat="server" visible='<%# IsAccessible("cp/report/newhrreport/YearEndSummaryReport.aspx") %>'
                                                                    href="~/cp/report/newhrreport/YearEndSummaryReport.aspx"><i class="fa fa-bars"></i>Year End Summary Report</a>
                                                            </li>
                                                            <li runat="server" id="li1">
                                                                <a id="A61" runat="server" visible='<%# IsAccessible("cp/report/addonreport.aspx") %>'
                                                                    href="~/cp/report/addonreport.aspx"><i class="fa fa-bars"></i>Multi Add on Report</a>
                                                            </li>
                                                            <li><a id="Add66" runat="server" visible='<%# IsAccessible("CP/Report/PayVariance.aspx") %>'
                                                                href="~/CP/Report/PayVariance.aspx?withtax=true"><i class="fa fa-bars"></i>Income and Tax Calculation</a>
                                                            </li>
                                                            <li><a id="A67" runat="server" visible='<%# IsAccessible("CP/Report/PayVariance.aspx") %>'
                                                                href="~/CP/Report/PayVariance.aspx?taxonly=true"><i class="fa fa-bars"></i>Tax Payment Details</a>
                                                            </li>
                                                            <li><a id="A66" runat="server" visible='<%# IsAccessible("CP/Report/TaxCertificate.aspx") %>'
                                                                href="~/CP/Report/TaxCertificate.aspx"><i class="fa fa-bars"></i>Tax Certificate</a>
                                                            </li>
                                                            <li><a id="A73" runat="server" visible='<%# IsAccessible("CP/TaxableIncomeAndIRDComparisionReport") %>'
                                                                href="~/CP/TaxableIncomeAndIRDComparisionReport.aspx"><i class="fa fa-bars"></i>Taxable Income Deposited</a>
                                                            </li>
                                                        </ul>
                                                    </td>
                                                    <td>
                                                        <ul role="menu" class="dropdown-menu dropdown-demo-only custommenugap">
                                                            <li class="dashboardMenuHeader"><a>Statutory Reports</a></li>
                                                            <li class="divider fulldivider" role="presentation"></li>
                                                            <li><a id="A25" runat="server" visible='<%# IsAccessible("CP/Report/PaySST.aspx") %>'
                                                                href="~/CP/Report/PaySST.aspx"><i class="fa fa-bars"></i>Social Security Tax (SST)</a></li>
                                                            <li><a id="A4" runat="server" visible='<%# IsAccessible("CP/Report/PayTDS.aspx") %>'
                                                                href="~/CP/Report/PayTDS.aspx"><i class="fa fa-bars"></i>Tax Deducted at Source
                                                                (TDS)</a></li>
                                                            <li><a id="A56" runat="server" visible='<%# IsAccessible("CP/Report/SSTTDSDetails.aspx") %>'
                                                                href="~/CP/Report/SSTTDSDetails.aspx"><i class="fa fa-bars"></i>SST/TDS Details</a></li>

                                                            <li><a id="A70" runat="server" visible='<%# IsAccessible("CP/Report/SSTTDSConsolidatedReport.aspx") %>'
                                                                href="~/CP/Report/SSTTDSConsolidatedReport.aspx"><i class="fa fa-bars"></i>Consoldated SST/TDS</a></li>

                                                            <li><a id="A5" runat="server" visible='<%# IsAccessible("CP/Report/PayPFStatement.aspx") %>'
                                                                href="~/CP/Report/PayPFStatement.aspx"><i class="fa fa-bars"></i>Provident Fund
                                                                Statement (PF)</a></li>
                                                            <li><a id="A27" runat="server" visible='<%# IsAccessible("CP/Report/PayCIT.aspx") %>'
                                                                href="~/CP/Report/PayCIT.aspx"><i class="fa fa-bars"></i>Citizen Investment Trust
                                                                (CIT)</a></li>
                                                            <li><a id="A55" runat="server" visible='<%# IsAccessible("CP/Report/CitDetails.aspx") %>'
                                                                href="~/CP/Report/CitDetails.aspx"><i class="fa fa-bars"></i>CIT Monthly Details</a></li>
                                                            <li><a id="A28" runat="server" visible='<%# IsAccessible("CP/Report/PayOtherFund.aspx") %>'
                                                                href="~/CP/Report/PayOtherFund.aspx"><i class="fa fa-bars"></i>Other Retirement
                                                                Fund</a>
                                                            <li>
                                                            <li><a id="nabilPF" runat="server" visible='<%# IsAccessible("CP/Report/PayNIBLPF.aspx") %>'
                                                                href="~/CP/Report/PayNIBLPF.aspx"><i class="fa fa-bars"></i>Nabil / Other PF</a></li>
                                                        </ul>
                                                        <ul role="menu" class="dropdown-menu dropdown-demo-only custommenugap">
                                                            <li class="dashboardMenuHeader"><a>Additional Pay Reports</a></li>
                                                            <li class="divider fulldivider" role="presentation"></li>
                                                            <li><a id="A71" runat="server" visible='<%# IsAccessible("CP/Report/PaySummaryCostCodeDesignationWise.aspx") %>'
                                                                href="~/CP/Report/PaySummaryCostCodeDesignationWise.aspx"><i class="fa fa-bars"></i>Pay
                                                                - Cost Code - Designations</a>
                                                            <li>
                                                            <li><a id="A46" runat="server" visible='<%# IsAccessible("CP/Report/PaySummaryDesignationWise.aspx") %>'
                                                                href="~/CP/Report/PaySummaryDesignationWise.aspx"><i class="fa fa-bars"></i>Pay
                                                                Calculation - Designations</a>
                                                            <li>

                                                            <li><a id="A26" runat="server" visible='<%# IsAccessible("CP/Report/SalaryExtractDepartmentWise.aspx") %>'
                                                                href="~/CP/Report/SalaryExtractDepartmentWise.aspx"><i class="fa fa-bars"></i>Salary
                                                                Extract - Departments</a> </li>
                                                            <li><a id="A48" runat="server" visible='<%# IsAccessible("newhr/Report/StopHoldPayReport.aspx") %>'
                                                                href="~/newhr/Report/StopHoldPayReport.aspx"><i class="fa fa-bars"></i>Stop / Hold
                                                                Pay</a></li>
                                                            <li><a id="A30" runat="server" visible='<%# IsAccessible("cp/report/newhrreport/ReportPayBranchDepartmentWise.aspx") %>'
                                                                href="~/cp/report/newhrreport/ReportPayBranchDepartmentWise.aspx"><i class="fa fa-bars"></i>Branch / Dep Wise Salary</a></li>
                                                        </ul>
                                                    </td>
                                                    <td>
                                                        <ul role="menu" class="dropdown-menu dropdown-demo-only custommenugap">
                                                            <li class="dashboardMenuHeader"><a>Audit & Reconciliation Reports</a></li>
                                                            <li class="divider fulldivider" role="presentation"></li>
                                                            <li><a id="A52" runat="server" visible='<%# IsAccessible("CP/TaxDetailsSummary.aspx") %>'
                                                                href="~/CP/TaxDetailsSummary.aspx"><i class="fa fa-bars"></i>Tax Calculation Summary</a></li>
                                                            <li><a id="A58" runat="server" visible='<%# IsAccessible("cp/AnnualTaxReportNew.aspx") %>'
                                                                href="~/CP/AnnualTaxReportNew.aspx"><i class="fa fa-bars"></i>Annual Tax Details (Beta)</a></li>
                                                            <li><a id="A11" runat="server" visible='<%# IsAccessible("CP/Report/PayVariance.aspx") %>'
                                                                href="~/CP/Report/PayVariance.aspx"><i class="fa fa-bars"></i>Employee Variance</a>
                                                            <li><a id="A37" runat="server" visible='<%# IsAccessible("CP/Report/NewHrReport/PayVariance.aspx") %>'
                                                                href="~/CP/Report/NewHrReport/PayVariance.aspx"><i class="fa fa-bars"></i>Salary
                                                                Comparision</a>
                                                            <li>
                                                            <li><a id="A34" runat="server" visible='<%# IsAccessible("CP/Report/PrevMonthAttendanceDeductionReport.aspx") %>'
                                                                href="~/CP/Report/PrevMonthAttendanceDeductionReport.aspx"><i class="fa fa-bars"></i>Last Month LWP Deduction</a>
                                                            <li>
                                                            <li><a id="A41" runat="server" visible='<%# IsAccessible("CP/Report/ReportAdjustment.aspx") %>'
                                                                href="~/CP/Report/ReportAdjustment.aspx"><i class="fa fa-bars"></i>Income Adjustments</a>
                                                            <li>
                                                            <li><a id="A45" runat="server" visible='<%# IsAccessible("CP/Report/SalaryChangeList.aspx") %>'
                                                                href="~/CP/Report/SalaryChangeList.aspx"><i class="fa fa-bars"></i>Salary Changes</a></li>
                                                        </ul>
                                                        <ul role="menu" class="dropdown-menu dropdown-demo-only custommenugap">
                                                            <li class="dashboardMenuHeader"><a>Provision</a></li>
                                                            <li class="divider fulldivider" role="presentation"></li>
                                                            <li><a id="A9" runat="server" visible='<%# IsAccessible("CP/Report/PayLeaveProvision.aspx") %>'
                                                                href="~/CP/Report/PayLeaveProvision.aspx"><i class="fa fa-bars"></i>Leave Encashment</a></li>
                                                            <li><a id="A10" runat="server" visible='<%# IsAccessible("CP/Report/PayGratuity.aspx") %>'
                                                                href="~/CP/Report/PayGratuity.aspx"><i class="fa fa-bars"></i>Gratuity / Severance</a></li>
                                                            <li><a id="A39" runat="server" visible='<%# IsAccessible("CP/Report/PayLeaveEncashmet.aspx") %>'
                                                                href="~/CP/Report/PayLeaveEncashmet.aspx"><i class="fa fa-bars"></i>Estimated Leave
                                                                Pay</a></li>
                                                        </ul>
                                                    </td>
                                                    <td>
                                                        <ul role="menu" class="dropdown-menu dropdown-demo-only custommenugap">
                                                            <li class="dashboardMenuHeader"><a>Other Reports</a></li>
                                                            <li class="divider fulldivider" role="presentation"></li>
                                                            <li><a id="A6" runat="server" visible='<%# IsAccessible("CP/Report/PayEmpLoan.aspx") %>'
                                                                href="~/CP/Report/PayEmpLoan.aspx"><i class="fa fa-bars"></i>Loan Recovery</a></li>
                                                            <li><a id="A31" runat="server" visible='<%# IsAccessible("NewHR/LoanRepaymentreport.aspx") %>'
                                                                href="~/NewHR/LoanRepaymentreport.aspx"><i class="fa fa-bars"></i>Loan Repayment</a></li>
                                                            <li><a id="A7" runat="server" visible='<%# IsAccessible("CP/Report/PayEmpAdvance.aspx") %>'
                                                                href="~/CP/Report/PayEmpAdvance.aspx"><i class="fa fa-bars"></i>Advance Recovery</a></li>
                                                            <li><a id="A42" runat="server" visible='<%# IsAccessible("newhr/EmployeehomeSalary.aspx") %>'
                                                                href="~/newhr/EmployeehomeSalary.aspx"><i class="fa fa-bars"></i>Take Home Salary</a></li>

                                                            <li><a id="A8" runat="server" visible='<%# IsAccessible("CP/Report/PayComponentDetails.aspx") %>'
                                                                href="~/CP/Report/PayComponentDetails.aspx"><i class="fa fa-bars"></i>Pay Component</a></li>
                                                            <li><a id="A23" runat="server" visible='<%# IsAccessible("CP/Report/PayDetail.aspx") %>'
                                                                href="~/CP/Report/PayDetail.aspx"><i class="fa fa-bars"></i>Detailed Payslip</a>
                                                            </li>
                                                            <li runat="server" visible="false" id="psiVoucher"><a id="A47" runat="server" href="~/CP/Report/VoucherReport.aspx">
                                                                <i class="fa fa-bars"></i>Voucher Report</a> </li>
                                                            <li runat="server" visible="false" id="oxfamVoucher"><a id="A69" runat="server" href="~/CP/Report/OxfamProjectReport.aspx">
                                                                <i class="fa fa-bars"></i>Voucher Report</a> </li>
                                                            <li runat="server" visible="false" id="hplVoucher"><a id="A38" runat="server" href="~/CP/HPLVoucherList.aspx">
                                                                <i class="fa fa-bars"></i>Voucher Report</a> </li>
                                                        </ul>
                                                        <ul role="menu" class="dropdown-menu dropdown-demo-only custommenugap">
                                                            <li class="dashboardMenuHeader"><a>Project Reports</a></li>
                                                            <li class="divider fulldivider" role="presentation"></li>
                                                            <li><a id="A14" runat="server" visible='<%# IsAccessible("CP/Report/ProjectReport.aspx") %>'
                                                                href="~/CP/Report/ProjectReport.aspx"><i class="fa fa-bars"></i>Project Report</a></li>
                                                            <li><a id="A15" runat="server" visible='<%# IsAccessible("CP/Report/ProjectConsolidatedReport.aspx") %>'
                                                                href="~/CP/Report/ProjectConsolidatedReport.aspx"><i class="fa fa-bars"></i>Consolidated
                                                                Project</a></li>
                                                            <li><a id="A24" runat="server" visible='<%# IsAccessible("CP/Report/ProjectContributionPaySummary.aspx") %>'
                                                                href="~/CP/Report/ProjectContributionPaySummary.aspx"><i class="fa fa-bars"></i>
                                                                Cost Allocation</a> </li>
                                                        </ul>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <div id="Div2" class="panel panel-dark-head no-radius" runat="server">
                                        <div class="panel-heading no-radius">
                                            <!-- panel-btns -->
                                            <h3 class="panel-title">Regular Tasks</h3>
                                        </div>
                                        <div class="panel-body panel-body-small">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <ul role="menu" class="dropdown-menu dropdown-demo-only custommenugap">
                                                            <li class="dashboardMenuHeader"><a>Settings</a></li>
                                                            <li class="divider fulldivider" role="presentation"></li>
                                                            <li>
                                                                <asp:HyperLink ID="HyperLink11" Visible='<%# IsAccessible("CP/ManageIncomes.aspx") %>'
                                                                    runat="server" NavigateUrl="~/CP/ManageIncomes.aspx">
                                                                     <i class="fa fa-bars"></i>List of Incomes
                                                                </asp:HyperLink>
                                                            </li>
                                                            <li>
                                                                <asp:HyperLink ID="HyperLink12" Visible='<%# IsAccessible("cp/ManageDeductions.aspx") %>'
                                                                    runat="server" NavigateUrl="~/cp/ManageDeductions.aspx">
                                                                     <i class="fa fa-bars"></i>List of Deductions
                                                                </asp:HyperLink>
                                                            </li>
                                                            <li>
                                                                <asp:HyperLink ID="HyperLink13" Visible='<%# IsAccessible("cp/AllEmployeeIncomeList.aspx") %>'
                                                                    runat="server" NavigateUrl="~/cp/AllEmployeeIncomeList.aspx">
                                                                     <i class="fa fa-bars"></i>Employee Income Summary
                                                                </asp:HyperLink>
                                                            </li>
                                                            <li>
                                                                <asp:HyperLink ID="HyperLink14" Visible='<%# IsAccessible("cp/extension/ManageCheckList.aspx") %>'
                                                                    runat="server" NavigateUrl="~/cp/extension/ManageCheckList.aspx">
                                                                     <i class="fa fa-bars"></i> Payroll CheckList
                                                                </asp:HyperLink>
                                                            </li>

                                                            <li>
                                                                <asp:HyperLink ID="linkExcludePFIncome" Visible='<%# IsAccessible("CP/Extension/ExculdePFImport.aspx") %>'
                                                                    runat="server" NavigateUrl="~/CP/Extension/ExculdePFImport.aspx">
                                                                     <i class="fa fa-bars"></i>Exclude PF Income
                                                                </asp:HyperLink>
                                                            </li>

                                                        </ul>
                                                    </td>
                                                    <td>
                                                        <ul role="menu" class="dropdown-menu dropdown-demo-only custommenugap">
                                                            <li class="dashboardMenuHeader"><a>Additional Settings</a></li>
                                                            <li class="divider fulldivider" role="presentation"></li>
                                                            <li><a id="A18" runat="server" visible='<%# IsAccessible("CP/EmployeeGratuityClass.aspx") %>'
                                                                href="~/CP/EmployeeGratuityClass.aspx"><i class="fa fa-bars"></i>Employee Gratuity
                                                                Class</a></li>
                                                            <li><a id="A13" runat="server" visible='<%# IsAccessible("cp/FixedPFDeduction.aspx") %>'
                                                                href="~/cp/FixedPFDeduction.aspx"><i class="fa fa-bars"></i>Fixed Provident Fund</a></li>
                                                            <%-- <li><a id="A12" runat="server" visible='<%# IsAccessible("CP/InsuranceList.aspx") %>'
                                                                href="~/CP/InsuranceList.aspx"><i class="fa fa-bars"></i>Insurance</a></li>--%>
                                                        </ul>
                                                    </td>
                                                    <td runat="server" id="tdNIBLBOnus">
                                                        <ul role="menu" class="dropdown-menu dropdown-demo-only custommenugap">
                                                            <li class="dashboardMenuHeader"><a>Bonus</a></li>
                                                            <li class="divider fulldivider" role="presentation"></li>
                                                            <li><a id="A44" runat="server" visible='<%# IsAccessible("cp/BonusList.aspx") %>'
                                                                href="~/cp/BonusList.aspx"><i class="fa fa-bars"></i>Bonus Distribution</a></li>
                                                            <li><a id="A53" runat="server" visible='<%# IsAccessible("cp/BonusIneligibleList.aspx") %>'
                                                                href="~/cp/BonusIneligibleList.aspx"><i class="fa fa-bars"></i>Bonus Ineligible</a></li>
                                                        </ul>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <div style="clear: both">
                                    </div>
                            </td>
                            <td class="Column" style="width: 30%">
                                <div class="panel panel-info-alt" runat="server" id="prDetails">
                                    <div class="panel-heading">
                                        <!-- panel-btns -->
                                        <h3 class="panel-title">Current Period</h3>
                                    </div>
                                    <div class="panel-body panel-body-small">
                                        <ul style="margin-left: -23px">
                                            <li><span runat="server" id="currentPeriod"></span></li>
                                            <li>Total Employees - <span runat="server" id="totalEmployees"></span></li>
                                            <li>Salary Saved Employees - <span runat="server" id="savedEmployees"></span></li>
                                            <li><a style="display: block" id="A3011" runat="server" visible='<%# IsAccessible("CP/Report/NewHrReport/VarianceSummary.aspx") %>'
                                                href="~/CP/Report/NewHrReport/VarianceSummary.aspx">Major Changes</a> </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="panel panel-info-alt" runat="server" id="divCITRequest" visible="true">
                                    <div class="panel-heading">
                                        <div class="panel-btns" style="display: none;">
                                            <a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel">
                                                <i class="fa fa-minus"></i></a>
                                        </div>
                                        <h3 class="panel-title">CIT Requests</h3>
                                        <a style="display: block" href="../cp/CITRequestList.aspx" class="details pull-right viewall">View All</a>
                                    </div>
                                    <div class="panel-body panel-body-small">
                                        <ul class="nav nav-pills nav-stacked dashboardNotificatioin">
                                            <li><a href='../cp/CITRequestList.aspx'>Request<span runat="server" id="spanRequests"
                                                class="badge badge-success pull-right">0</span></a> </li>

                                        </ul>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
