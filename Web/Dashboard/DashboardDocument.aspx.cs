﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
//using System.Web.UI.DataVisualization.Charting;
using BLL.Base;
using System.Globalization;
using Utils.Calendar;
using BLL.BO;
using Ext.Net;
using Web.Helper;
using Bll;

namespace Web.CP
{
    public partial class DashboardDocument : BasePage
    {
        CommonManager commonMgr = new CommonManager();
        private double totalSalarySummary = 0;

        private bool moduleAccessible = false;
        public new bool IsAccessible(string url)
        {
            if (moduleAccessible)
                return true;

            if (SessionManager.User.URole.URoleModules.Any(x => x.ModuleRef_ID == (int)ModuleEnum.DocumentManagement))
            {
                moduleAccessible = true;
                return true;
            }

            bool isPageAccessible = UserManager.IsPageAccessible(url);

            if (SessionManager.IsCustomRole == true && isPageAccessible)
            {
                isPageAccessible = false;
                UUser objUUser = UserManager.GetUserByUserID(SessionManager.CurrentLoggedInUserID);
                if (objUUser != null && objUUser.DocumentIsViewOnly != null)
                {
                    if (url.ToLower().Contains("documentlist.aspx") || url.ToLower().Contains("dealerlist.aspx") || url.ToLower().Contains("painterlist.aspx"))
                        isPageAccessible = true;
                    else
                    {
                        isPageAccessible = false;

                        if (!objUUser.DocumentIsViewOnly.Value)
                            isPageAccessible = true;
                    }
                }
            }

            return isPageAccessible;

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialise();
                this.DataBind();
            }
        }

        void Initialise()
        {
            // First parepare this month & next month dates
            CustomDate today = CustomDate.GetTodayDate(IsEnglish);
            int currentYear = today.Year;
            DateTime FromThisDate, FromNextDate;
            DateTime ToThisDate, ToNextDate;
            CustomDate firstDateOfThisMonth = new CustomDate(1, today.Month, currentYear, IsEnglish);
            CustomDate lastDateOfThisMonth = firstDateOfThisMonth.GetLastDateOfThisMonth();

            FromThisDate = DateManager.GetStartDate(firstDateOfThisMonth.EnglishDate);
            ToThisDate = DateManager.GetEndDate(lastDateOfThisMonth.EnglishDate);

            firstDateOfThisMonth = lastDateOfThisMonth.IncrementByOneDay();
            lastDateOfThisMonth = firstDateOfThisMonth.GetLastDateOfThisMonth();

            FromNextDate = DateManager.GetStartDate(firstDateOfThisMonth.EnglishDate);
            ToNextDate = DateManager.GetEndDate(lastDateOfThisMonth.EnglishDate);

            BindExpiringDocDocuments("");
            BindRecentDocDocuments("");
            BindExpiredDocDocuments("");
            BindDocComments("");
        }

        private void BindExpiringDocDocuments(string searchText)
        {
            DateTime fromDate = DateTime.Today.AddDays(1).Date;
            DateTime toDate = DateTime.Today.AddMonths(1).Date;

            List<GetExpiringDocumentListResult> list = BLL.BaseBiz.PayrollDataContext.GetExpiringDocumentList(searchText, fromDate, toDate, SessionManager.CurrentLoggedInUserID).ToList();
           
            gridExpiringDocuments.Store[0].DataSource = list;
            gridExpiringDocuments.Store[0].DataBind();
        }

        private void BindRecentDocDocuments(string searchText)
        {
            DateTime fromDate = DateTime.Today.AddMonths(-1);
            DateTime toDate = DateTime.Today.Date;

            List<GetRecentDocumentListResult> list = BLL.BaseBiz.PayrollDataContext.GetRecentDocumentList(searchText, fromDate, toDate, SessionManager.CurrentLoggedInUserID).ToList();
           
            gridRecentDocuments.Store[0].DataSource = list;
            gridRecentDocuments.Store[0].DataBind();
        }

        private void BindExpiredDocDocuments(string searchText)
        {
            List<GetExpiredDocumentListResult> list = BLL.BaseBiz.PayrollDataContext.GetExpiredDocumentList(searchText, DateTime.Today.Date , SessionManager.CurrentLoggedInUserID).ToList();
            
            gridExpiredDocuments.Store[0].DataSource = list;
            gridExpiredDocuments.Store[0].DataBind();
        }

        private void BindDocComments(string searchText)
        {
            List<BODocumentClass> list = DocumentManager.GetDocDocumentComments(searchText);
            foreach (var item in list)
            {
                item.TimeDetails = WebHelper.TimeAgoWithMinSecond(item.DateValue);
            }
            gridComments.Store[0].DataSource = list;
            gridComments.Store[0].DataBind();
        }

        protected void txtSearchPartyDocument_Change(object sender, DirectEventArgs e)
        {
            string partyNameSearch = "";
            BindExpiringDocDocuments(partyNameSearch);
            BindRecentDocDocuments(partyNameSearch);
            BindExpiredDocDocuments(partyNameSearch);
            BindDocComments(partyNameSearch);            
        }

        protected void cmbSearchDocument_Click(object sender, DirectEventArgs e)
        {
            if (cmbSearchDocument.SelectedItem == null || cmbSearchDocument.SelectedItem.Value == null)
                return;

            string documentID = cmbSearchDocument.SelectedItem.Value;

            Response.Redirect("~/NewHr/DocumentManagement/DocumentView.aspx?DocumentId=" + documentID);
        }

        protected void btnExportRecentDoc_Click(object sender, EventArgs e)
        {
            DateTime fromDate = DateTime.Today.AddMonths(-1);
            DateTime toDate = DateTime.Today.Date;

            List<GetRecentDocumentListResult> list = BLL.BaseBiz.PayrollDataContext.GetRecentDocumentList("", fromDate, toDate, SessionManager.CurrentLoggedInUserID).ToList();

            Bll.ExcelHelper.ExportToExcel("Recent Documents", list,
               new List<string> { "DocumentID", "PartyRef_ID" },
           new List<String>() { },
           new Dictionary<string, string>() { { "PartyName", "Party" },
            { "Title", "Document Title" }, { "CreatedOnDate", "Created On" }},
           new List<string>() { }
           , new List<string> { }
           , new List<string> { }
            , new Dictionary<string, string>() {
              {"Recent Documents",""}
            }
            , new List<string> { "PartyName", "Title", "CreatedOnDate" });
        }

        protected void btnExportExpiringDoc_Click(object sender, EventArgs e)
        {
            DateTime fromDate = DateTime.Today.AddDays(1).Date;
            DateTime toDate = DateTime.Today.AddMonths(1).Date;

            List<GetExpiringDocumentListResult> list = BLL.BaseBiz.PayrollDataContext.GetExpiringDocumentList("", fromDate, toDate, SessionManager.CurrentLoggedInUserID).ToList();
           
            Bll.ExcelHelper.ExportToExcel("Expiring Documents", list,
               new List<string> { "DocumentID", "PartyRef_ID", "ExpiryDateEng"  },
           new List<String>() { },
           new Dictionary<string, string>() { { "PartyName", "Party" },
            { "Title", "Document Title" }, { "ExpiryDate", "Expiring In" }, { "DaysCount", "Days"}},
           new List<string>() { }
           , new List<string> { }
           , new List<string> {}
            , new Dictionary<string, string>() {
              {"Expiring Documents",""}
            }
            , new List<string> { "PartyName", "Title", "ExpiryDate", "DaysCount" });
        }

        protected void btnExportExpiredDoc_Click(object sender, EventArgs e)
        {
            List<GetExpiredDocumentListResult> list = BLL.BaseBiz.PayrollDataContext.GetExpiredDocumentList("", DateTime.Today.Date, SessionManager.CurrentLoggedInUserID).ToList();
            
            Bll.ExcelHelper.ExportToExcel("Expired Document List", list,
               new List<string> { "DocumentID", "PartyRef_ID", "ExpiryDateEng" },
           new List<String>() { },
           new Dictionary<string, string>() { { "PartyName", "Party" },
            { "Title", "Document Title" }, { "ExpiryDate", "Expiring In" }, { "DaysCount", "Days"}},
           new List<string>() { }
           , new List<string> { }
           , new List<string> { "JoinDateText" }
            , new Dictionary<string, string>() {
              {"Expired Document List",""}
            }
            , new List<string> { "PartyName", "Title", "ExpiryDate", "DaysCount" });
        }

    }
}
