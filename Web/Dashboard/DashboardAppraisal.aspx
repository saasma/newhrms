<%@ Page Title="Appraisal Dashboard" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="DashboardAppraisal.aspx.cs" Inherits="Web.CP.DashboardAppraisal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .dbTitle {
            display: inline-block;
            background-color: #E7EFFC;
            width: 97%;
            position: relative;
            border-bottom: thin solid #B2D0E8;
            line-height: 30px;
            vertical-align: middle;
            padding-left: 5px;
            padding-right: 6px;
            border-right: 1px solid #B2D0E8;
        }

            .dbTitle a {
                float: right;
                text-decoration: none; /* border:thin solid gray;*/
                cursor: pointer;
            }

            .dbTitle h2 {
                color: #0070C0;
                float: left;
                font-size: 12px;
                margin-top: 4px;
                margin-bottom: 4px;
            }

        .subTitle {
            color: #616161;
            font-weight: bold;
            font-size: 12px !important;
            background: #F9F9F9;
            line-height: 20px;
            width: 99%;
            margin-top: 2px;
            margin-bottom: 2px;
        }

        .dbPanel {
            display: none;
            border: thin solid #B2D0E8;
            margin: 10px 0px 15px 0px;
        }

        .dbPanelContent {
            padding: 3px;
            display: inline-block;
            width: 100%;
            overflow: hidden; /*width:100%;*/
        }

        .Column {
            width: 33%;
        }

        .contentWrapper > table {
            /* padding-left: 8px;
            padding-right: 8px;*/
            width: 100%;
        }

            .contentWrapper > table td {
                vertical-align: top; /* width:100%;*/ /* width:33%;*/
                padding-right: 5px;
            }

        .hideHeader .x-grid-header-ct {
            background-image: none !important;
            display: none !important;
            border: none !important;
        }

        .contentWrapper .x-grid-cell-last > .x-grid-cell-inner {
            text-align: right !important;
        }

        .roundCorner {
            border: thin solid #5DA5ED;
            -moz-border-radius: 4px 4px 4px 4px;
            -webkit-border-radius: 4px 4px 4px 4px;
            padding: 0px;
            background: -moz-linear-gradient(center top, #999999 0%,#ffffff 0%);
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0, #999999),color-stop(0, #ffffff));
        }

        /*options cases*/
        .option {
            padding: 10px;
        }

            .option a, .option span {
                text-align: center;
                text-decoration: none;
                font-weight: bold;
            }

            .option img, .option span {
                display: block;
                text-decoration: none;
                font-size: 15px;
            }

            .option img {
                width: 80px;
                height: 80px;
                padding-left: 12px;
            }

        .hideLeftBlockCssInPage {
            margin: 0px !important;
            padding-left: 20px !important;
        }

        #menu {
            display: none;
        }

        .reportBlock hr {
            margin: 7px 0 0 35px;
        }

        .reportArrow {
            margin: 3px 0 0 10px;
            width: 15px;
            height: 15px;
            float: left;
        }

        .reports {
            padding: 5px 0;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>Appraisal Dashboard
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div>
            <div>
                <div style="width: 100%; visibility: visible" class="contentWrapper" id="container">
                    <table>
                        <tr>
                            <td class="Column" style="width: 70%">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <!-- panel-btns -->
                                        <h3 class="panel-title">Regular</h3>
                                    </div>
                                    <div class="panel-body">
                                        <ul role="menu" class="dropdown-menu dropdown-demo-only custommenulist">
                                            <li>
                                                <asp:HyperLink ID="HyperLink5" Visible='<%# IsAccessible("Appraisal/Form/general_settings_1.aspx") %>'
                                                    runat="server" NavigateUrl="~/Appraisal/Form/general_settings_1.aspx">
                                                         <i class="fa fa-bars"></i> Create New Form
                                                </asp:HyperLink>
                                            </li>
                                            <li>
                                                <asp:HyperLink ID="HyperLink6" Visible='<%# IsAccessible("Appraisal/AppraisalList.aspx") %>'
                                                    runat="server" NavigateUrl="~/Appraisal/AppraisalList.aspx">
                                                         <i class="fa fa-bars"></i> Appraisal
                                                                Forms
                                                </asp:HyperLink>
                                            </li>
                                            <li>
                                                <asp:HyperLink ID="HyperLink3" Visible='<%# IsAccessible("Appraisal/Form/RolloutMega.aspx") %>'
                                                    runat="server" NavigateUrl="~/Appraisal/Form/RolloutMega.aspx">
                                                         <i class="fa fa-bars"></i> Start Individual Rollout
                                                </asp:HyperLink>
                                            </li>
                                            <li>
                                                <asp:HyperLink ID="HyperLink4" Visible='<%# IsAccessible("Appraisal/Form/Rollout.aspx") %>'
                                                    runat="server" NavigateUrl="~/Appraisal/Form/Rollout.aspx">
                                                         <i class="fa fa-bars"></i> Appraisal
                                                                Rollout List
                                                </asp:HyperLink>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <!-- panel-btns -->
                                        <h3 class="panel-title">Reports</h3>
                                    </div>
                                    <div class="panel-body">
                                        <ul role="menu" class="dropdown-menu dropdown-demo-only custommenulist">
                                            <li>
                                                <asp:HyperLink ID="HyperLink1" Visible='<%# IsAccessible("Appraisal/EmployeeListing.aspx") %>'
                                                    runat="server" NavigateUrl="~/Appraisal/EmployeeListing.aspx">
                                                         <i class="fa fa-bars"></i> Forms Received
                                                </asp:HyperLink>
                                            </li>
                                            <li>
                                                <asp:HyperLink ID="HyperLink2" Visible='<%# IsAccessible("Appraisal/AppraisalDetailsReport.aspx") %>'
                                                    runat="server" NavigateUrl="~/Appraisal/AppraisalDetailsReport.aspx">
                                                         <i class="fa fa-bars"></i> Appraisal Report
                                                </asp:HyperLink>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <!-- panel-btns -->
                                        <h3 class="panel-title">Settings</h3>
                                    </div>
                                    <div class="panel-body">
                                        <table>
                                            <tr>
                                                <td>
                                                    <ul role="menu" class="dropdown-menu dropdown-demo-only custommenugap">

                                                        <li>
                                                            <asp:HyperLink ID="HyperLink7" Visible='<%# IsAccessible("Appraisal/RatingScale.aspx") %>'
                                                                runat="server" NavigateUrl="~/Appraisal/RatingScale.aspx">
                                                         <i class="fa fa-bars"></i> Rating
                                                                Scale
                                                            </asp:HyperLink>
                                                        </li>
                                                        <li>
                                                            <asp:HyperLink ID="HyperLink8" Visible='<%# IsAccessible("Appraisal/ManageCompetancy.aspx") %>'
                                                                runat="server" NavigateUrl="~/Appraisal/ManageCompetancy.aspx">
                                                         <i class="fa fa-bars"></i> Manage
                                                                Competancy
                                                            </asp:HyperLink>
                                                        </li>
                                                        <li>
                                                            <asp:HyperLink ID="HyperLink10" Visible='<%# IsAccessible("Appraisal/ManagePeriod.aspx") %>'
                                                                runat="server" NavigateUrl="~/Appraisal/ManagePeriod.aspx">
                                                                     <i class="fa fa-bars"></i> Manage
                                                                            Periods
                                                            </asp:HyperLink>
                                                        </li>
                                                        <li>
                                                            <asp:HyperLink ID="HyperLink11" Visible='<%# IsAccessible("Appraisal/ManageRecommendFor.aspx") %>'
                                                                runat="server" NavigateUrl="~/Appraisal/ManageRecommendFor.aspx">
                                                                     <i class="fa fa-bars"></i> Manage
                                                                            Recommend
                                                            </asp:HyperLink>
                                                        </li>
                                                    </ul>
                                                </td>
                                                <td>
                                                    <ul role="menu" class="dropdown-menu dropdown-demo-only custommenugap">
                                                        <li>
                                                            <asp:HyperLink ID="HyperLink12" Visible='<%# IsAccessible("Appraisal/ManageTarget.aspx") %>'
                                                                runat="server" NavigateUrl="~/Appraisal/ManageTarget.aspx">
                                                                     <i class="fa fa-bars"></i> Manage
                                                                            Targets
                                                            </asp:HyperLink>
                                                        </li>
                                                        <li>
                                                            <asp:HyperLink ID="HyperLink9" Visible='<%# IsAccessible("CP/Extension/TargetsAchievementList.aspx") %>'
                                                                runat="server" NavigateUrl="~/CP/Extension/TargetsAchievementList.aspx">
                                                                     <i class="fa fa-bars"></i> Targets and Achievement
                                                            </asp:HyperLink>
                                                        </li>
                                                        <li>
                                                            <asp:HyperLink ID="HyperLink13" Visible='<%# IsAccessible("CP/Extension/TargetsImport.aspx") %>'
                                                                runat="server" NavigateUrl="~/CP/Extension/TargetsImport.aspx">
                                                                     <i class="fa fa-bars"></i> Employee
                                                                            Targets
                                                            </asp:HyperLink>
                                                        </li>
                                                        <li>
                                                            <asp:HyperLink ID="HyperLink14" Visible='<%# IsAccessible("CP/Extension/TargetsImportOtherDetails.aspx") %>'
                                                                runat="server" NavigateUrl="~/CP/Extension/TargetsImportOtherDetails.aspx">
                                                                     <i class="fa fa-bars"></i> Weight, Targets and Achievement
                                                            </asp:HyperLink>
                                                        </li>
                                                    </ul>
                                                </td>
                                                <td>
                                                    <ul role="menu" class="dropdown-menu dropdown-demo-only custommenugap">
                                                        <li>
                                                            <asp:HyperLink ID="HyperLink16" Visible='<%# IsAccessible("Appraisal/AppraisalRolloutGroup.aspx") %>'
                                                                runat="server" NavigateUrl="~/Appraisal/AppraisalRolloutGroup.aspx">
                                                                     <i class="fa fa-bars"></i> Rollout Groups 
                                                            </asp:HyperLink>
                                                        </li>

                                                        <li>
                                                            <asp:HyperLink ID="HyperLink17" Visible='<%# IsAccessible("Appraisal/EmployeeAppraisalRolloutGroup.aspx") %>'
                                                                runat="server" NavigateUrl="~/Appraisal/EmployeeAppraisalRolloutGroup.aspx">
                                                                     <i class="fa fa-bars"></i> Employee Rollout Groups 
                                                            </asp:HyperLink>
                                                        </li>
                                                    </ul>
                                                </td>
                                                <td>
                                                    <ul role="menu" class="dropdown-menu dropdown-demo-only custommenugap">
                                                        <li>
                                                            <asp:HyperLink ID="HyperLink15" Visible='<%# IsAccessible("CP/Extension/EduLocationAndSeniorityScoreImport.aspx") %>'
                                                                runat="server" NavigateUrl="~/CP/Extension/EduLocationAndSeniorityScoreImport.aspx">
                                                                     <i class="fa fa-bars"></i> Education,Location and Seniority Score 
                                                                
                                                            </asp:HyperLink>
                                                        </li>


                                                    </ul>
                                                </td>

                                            </tr>
                                        </table>


                                    </div>
                                </div>
                                <div style="clear: both">
                                </div>
                                <div class="dbPanel" runat="server" id="Div1">
                                    <div class="dbTitle">
                                        <h2></h2>
                                    </div>
                                    <div class="dbPanelContent">
                                        <h2 class="subTitle">This Month</h2>
                                        <div style="padding-left: 10px;">
                                            <asp:Repeater runat="server" ID="rptRetiringThisMonth">
                                                <HeaderTemplate>
                                                    <table>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td style="width: 170px">
                                                            <%# Eval("Text") %>
                                                        </td>
                                                        <td style="text-align: right; width: 140px;">
                                                            <%# Eval("Date", "{0:dd MMM yyyy}").ToString().Replace("12:00AM","")%>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </table>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </div>
                                        <h2 class="subTitle">Next Month</h2>
                                        <div style="padding-left: 10px;">
                                            <asp:Repeater runat="server" ID="rptRetiringNextMonth">
                                                <HeaderTemplate>
                                                    <table>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td style="width: 170px">
                                                            <%# Eval("Text") %>
                                                        </td>
                                                        <td style="text-align: right; width: 140px;">
                                                            <%# Eval("Date", "{0:dd MMM yyyy}").ToString().Replace("12:00AM","")%>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </table>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <%-- <td class="Column">
                                <div class="dbPanel" runat="server" id="Div2">
                                    <div class="dbTitle">
                                        <h2>
                                            Employee Count</h2>
                                    </div>
                                    <div class="dbPanelContent">
                                        <ext:Panel ID="Panel1" runat="server" Title="Branch Count" Width="380" Height="370"
                                            Layout="FitLayout">
                                            <Items>
                                                <ext:Chart ID="chartBranchEmployeeCount" runat="server" Shadow="true" Animate="true">
                                                    <Store>
                                                        <ext:Store ID="chartBranchEmployeeCountStore" runat="server" AutoDataBind="true">
                                                            <Model>
                                                                <ext:Model ID="Model1" runat="server">
                                                                    <Fields>
                                                                        <ext:ModelField Name="Text" />
                                                                        <ext:ModelField Name="Count" />
                                                                    </Fields>
                                                                </ext:Model>
                                                            </Model>
                                                        </ext:Store>
                                                    </Store>
                                                    <Background>
                                                        <Gradient GradientID="backgroundGradient" Angle="45">
                                                            <Stops>
                                                                <ext:GradientStop Offset="0" Color="#ffffff" />
                                                                <ext:GradientStop Offset="100" Color="#eaf1f8" />
                                                            </Stops>
                                                        </Gradient>
                                                    </Background>
                                                    <Axes>
                                                        <ext:NumericAxis Grid="false" Fields="Count" Position="Bottom" Minimum="0">
                                                            <Label>
                                                                <Renderer Handler="return Ext.util.Format.number(value, '0,0');" />
                                                            </Label>
                                                        </ext:NumericAxis>
                                                        <ext:CategoryAxis Fields="Text" Position="Left" />
                                                    </Axes>
                                                    <Series>
                                                        <ext:BarSeries Axis="Bottom" Highlight="true" XField="Text" YField="Count">
                                                            <Tips TrackMouse="true" Width="140" Height="28">
                                                                <Renderer Handler="this.setTitle(storeItem.get('Text') + ': ' + storeItem.get('Count') + ' views');" />
                                                            </Tips>
                                                            <Label Display="InsideEnd" Field="Count" Orientation="Horizontal" Color="#333" TextAnchor="middle" />
                                                        </ext:BarSeries>
                                                    </Series>
                                                </ext:Chart>
                                            </Items>
                                        </ext:Panel>
                                    </div>
                                </div>
                            </td>--%>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
