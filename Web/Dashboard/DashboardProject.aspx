<%@ Page Title="Project/Timesheet Dashboard" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="DashboardProject.aspx.cs" Inherits="Web.CP.DashboardProject" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .dbTitle
        {
            display: inline-block;
            background-color: #E7EFFC;
            width: 97%;
            position: relative;
            border-bottom: thin solid #B2D0E8;
            line-height: 30px;
            vertical-align: middle;
            padding-left: 5px;
            padding-right: 6px;
            border-right: 1px solid #B2D0E8;
        }
        
        .dbTitle a
        {
            float: right;
            text-decoration: none; /* border:thin solid gray;*/
            cursor: pointer;
        }
        .dbTitle h2
        {
            color: #0070C0;
            float: left;
            font-size: 12px;
            margin-top: 4px;
            margin-bottom: 4px;
        }
        
        .subTitle
        {
            color: #616161;
            font-weight: bold;
            font-size: 12px !important;
            background: #F9F9F9;
            line-height: 20px;
            width: 99%;
            margin-top: 2px;
            margin-bottom: 2px;
        }
        .dbPanel
        {
            display: none;
            border: thin solid #B2D0E8;
            margin: 10px 0px 15px 0px;
        }
        
        .dbPanelContent
        {
            padding: 3px;
            display: inline-block;
            width: 100%;
            overflow: hidden; /*width:100%;*/
        }
        
        .Column
        {
            width: 33%;
        }
        
        .contentWrapper > table
        {
            /* padding-left: 8px;
            padding-right: 8px;*/
            width: 100%;
        }
        .contentWrapper > table td
        {
            vertical-align: top; /* width:100%;*/ /* width:33%;*/
            padding-right: 5px;
        }
        
        .hideHeader .x-grid-header-ct
        {
            background-image: none !important;
            display: none !important;
            border: none !important;
        }
        
        .contentWrapper .x-grid-cell-last > .x-grid-cell-inner
        {
            text-align: right !important;
        }
        
        .roundCorner
        {
            border: thin solid #5DA5ED;
            -moz-border-radius: 4px 4px 4px 4px;
            -webkit-border-radius: 4px 4px 4px 4px;
            padding: 0px;
            background: -moz-linear-gradient(center top, #999999 0%,#ffffff 0%);
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0, #999999),color-stop(0, #ffffff));
        }
        
        /*options cases*/
        .option
        {
            padding: 10px;
        }
        .option a, .option span
        {
            text-align: center;
            text-decoration: none;
            font-weight: bold;
        }
        .option img, .option span
        {
            display: block;
            text-decoration: none;
            font-size: 15px;
        }
        .option img
        {
            width: 80px;
            height: 80px;
            padding-left: 12px;
        }
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
        .reportBlock hr
        {
            margin: 7px 0 0 35px;
        }
        .reportArrow
        {
            margin: 3px 0 0 10px;
            width: 15px;
            height: 15px;
            float: left;
        }
        .reports
        {
            padding: 5px 0;
        }
        .panel-heading
        {
            height: 40px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Project and Timesheet Dashboard
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div>
            <div style="width: 100%; visibility: visible" class="contentWrapper" id="container">
                <table>
                    <tr>
                        <td class="Column" style="width: 70%">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <!-- panel-btns -->
                                    <h3 class="panel-title">
                                        Regular</h3>
                                </div>
                                <div class="panel-body">
                                    <ul role="menu" style="vertical-align:top" class="dropdown-menu dropdown-demo-only icongap">
                                        <li>
                                            <asp:HyperLink ID="HyperLink1" Visible='<%# IsAccessible("CP/Extension/ManageProjectTimeSheet.aspx") %>'
                                                runat="server" NavigateUrl="~/CP/Extension/ManageProjectTimeSheet.aspx">
                                                         <i class="fa fa-bars"></i>Manage Projects
                                            </asp:HyperLink>
                                        </li>
                                    </ul>
                                    <ul role="menu" class="dropdown-menu dropdown-demo-only icongap">
                                        <li>
                                            <asp:HyperLink ID="linkProjectAssociation" Visible='<%# IsAccessible("CP/Extension/projectassociation.aspx") %>'
                                                runat="server" NavigateUrl="~/CP/Extension/projectassociation.aspx">
                                                         <i class="fa fa-bars"></i>Assign Projects
                                            </asp:HyperLink>
                                        </li>
                                        <li>
                                            <asp:HyperLink ID="linkTimeSheet" Visible='<%# IsAccessible("NewHR/TimesheetList.aspx") %>'
                                                runat="server" NavigateUrl="~/NewHR/TimesheetList.aspx">
                                                         <i class="fa fa-bars"></i>Timesheet List
                                            </asp:HyperLink>
                                        </li>
                                    </ul>
                                    <ul role="menu" class="dropdown-menu dropdown-demo-only icongap" visible="false" runat="server" id="careProjectContribution">
                                        <li>
                                            <asp:HyperLink ID="HyperLink7" Visible='<%# IsAccessible("CP/Extension/ProjectRateInput.aspx") %>'
                                                runat="server" NavigateUrl="~/CP/Extension/ProjectRateInput.aspx">
                                                         <i class="fa fa-bars"></i>Employee Contribution Percent
                                            </asp:HyperLink>
                                        </li>
                                        <li>
                                            <asp:HyperLink ID="HyperLink2" Visible='<%# IsAccessible("CP/Extension/ProjectActivityInput.aspx") %>'
                                                runat="server" NavigateUrl="~/CP/Extension/ProjectActivityInput.aspx">
                                                         <i class="fa fa-bars"></i>Employee Project Activity
                                            </asp:HyperLink>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <!-- panel-btns -->
                                    <h3 class="panel-title">
                                        Reports</h3>
                                </div>
                                <div class="panel-body">
                                    <ul role="menu" class="dropdown-menu dropdown-demo-only icongap" visible="false" runat="server" id="careProjectContributionReport">
                                        <li>
                                            <asp:HyperLink ID="HyperLink3" Visible='<%# IsAccessible("NewHR/EmployeeProjectContributionList.aspx") %>'
                                                runat="server" NavigateUrl="~/NewHR/EmployeeProjectContributionList.aspx">
                                                         <i class="fa fa-bars"></i>Employee Project Contribution Report
                                            </asp:HyperLink>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div style="clear: both">
                            </div>
                        </td>
                        <td class="Column" style="width: 30%">
                            <div class="panel panel-info" runat="server" id="divOvertime">
                                <div class="panel-heading" style="height: 50px!important;    padding-left: 7px!important;">
                                    <!-- panel-btns -->
                                    <h3 class="panel-title" id="title" runat="server">
                                        Timesheet submit</h3>
                                    <asp:HyperLink Style="display: block" ID="linkTimesheetSubmitted" Text=" view all"
                                        runat="server" NavigateUrl="~/NewHR/TimesheetList.aspx" class="details pull-right">
                                    </asp:HyperLink>
                                    
                                </div>
                                <div class="panel-body panel-body-small">
                                    <div style="margin-bottom: 10px; margin-left: 10px;">
                                        <table>
                                            <tr>
                                                <td style="padding-top: 5px">
                                                    <ext:DateField LabelSeparator="" FieldLabel="Select Month" LabelAlign="Left" ID="timesheetMonth"
                                                        runat="server">
                                                    </ext:DateField>
                                                </td>
                                                <td>
                                                    <ext:Button runat="server" ID="btnLoad" AutoPostBack="true" OnClick="btnLoad_Click"
                                                        Cls="btn btn-default" Text="Load">
                                                        <%--<DirectEvents>
                                                            <Click OnEvent="btnLoad_Click">
                                                                <EventMask ShowMask="true" />
                                                            </Click>
                                                        </DirectEvents>--%>
                                                    </ext:Button>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <ul class="nav nav-pills nav-stacked dashboardNotificatioin">
                                        <li><a href='javascript:void(0)'>Not Filled<span runat="server" id="spanNotFilled"
                                            class="badge badge-success pull-right">0</span> </a></li>
                                        <li><a href='javascript:void(0)'>Draft <span class="badge badge-warning pull-right"
                                            runat="server" id="spanDraft">0</span></a> </li>
                                        <li><a href='javascript:void(0)'>Awaiting Approval <span class="badge badge-danger pull-right"
                                            runat="server" id="spanAwaitingApproval">0</span></a> </li>
                                        <li><a href='javascript:void(0)'>Approved <span class="badge badge-primary pull-right"
                                            runat="server" id="spanApproved">0</span></a> </li>
                                        <li><a href='javascript:void(0)'>Rejected <span class="badge badge-info pull-right"
                                            runat="server" id="spanRejected">0</span></a> </li>
                                        <li runat="server" visible="false" id="liReviewedPSI"><a href='javascript:void(0)'>Reviewed
                                            <span class="badge badge-warning pull-right" runat="server" id="spanReviewedPSI">0</span></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </td>
                        <%-- <td class="Column">
                                <div class="dbPanel" runat="server" id="Div2">
                                    <div class="dbTitle">
                                        <h2>
                                            Employee Count</h2>
                                    </div>
                                    <div class="dbPanelContent">
                                        <ext:Panel ID="Panel1" runat="server" Title="Branch Count" Width="380" Height="370"
                                            Layout="FitLayout">
                                            <Items>
                                                <ext:Chart ID="chartBranchEmployeeCount" runat="server" Shadow="true" Animate="true">
                                                    <Store>
                                                        <ext:Store ID="chartBranchEmployeeCountStore" runat="server" AutoDataBind="true">
                                                            <Model>
                                                                <ext:Model ID="Model1" runat="server">
                                                                    <Fields>
                                                                        <ext:ModelField Name="Text" />
                                                                        <ext:ModelField Name="Count" />
                                                                    </Fields>
                                                                </ext:Model>
                                                            </Model>
                                                        </ext:Store>
                                                    </Store>
                                                    <Background>
                                                        <Gradient GradientID="backgroundGradient" Angle="45">
                                                            <Stops>
                                                                <ext:GradientStop Offset="0" Color="#ffffff" />
                                                                <ext:GradientStop Offset="100" Color="#eaf1f8" />
                                                            </Stops>
                                                        </Gradient>
                                                    </Background>
                                                    <Axes>
                                                        <ext:NumericAxis Grid="false" Fields="Count" Position="Bottom" Minimum="0">
                                                            <Label>
                                                                <Renderer Handler="return Ext.util.Format.number(value, '0,0');" />
                                                            </Label>
                                                        </ext:NumericAxis>
                                                        <ext:CategoryAxis Fields="Text" Position="Left" />
                                                    </Axes>
                                                    <Series>
                                                        <ext:BarSeries Axis="Bottom" Highlight="true" XField="Text" YField="Count">
                                                            <Tips TrackMouse="true" Width="140" Height="28">
                                                                <Renderer Handler="this.setTitle(storeItem.get('Text') + ': ' + storeItem.get('Count') + ' views');" />
                                                            </Tips>
                                                            <Label Display="InsideEnd" Field="Count" Orientation="Horizontal" Color="#333" TextAnchor="middle" />
                                                        </ext:BarSeries>
                                                    </Series>
                                                </ext:Chart>
                                            </Items>
                                        </ext:Panel>
                                    </div>
                                </div>
                            </td>--%>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    </div>
</asp:Content>
