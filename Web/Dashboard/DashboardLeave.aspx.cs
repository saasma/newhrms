﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
//using System.Web.UI.DataVisualization.Charting;
using BLL.Base;
using System.Globalization;
using Utils.Calendar;
using BLL.BO;
using Ext.Net;
using Utils;

namespace Web.CP
{
    public partial class DashboardLeave : BasePage
    {
        CommonManager commonMgr = new CommonManager();
        private double totalSalarySummary = 0;

        private bool moduleAccessible = false;
        public new bool IsAccessible(string url)
        {
            if (moduleAccessible)
                return true;

            if (SessionManager.User.URole.URoleModules.Any(x => x.ModuleRef_ID == (int)ModuleEnum.HumanResource))
            {
                moduleAccessible = true;
                return true;
            }
            
            return UserManager.IsPageAccessible(url);

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                 try
                {
                    Initialise();
                    this.DataBind();
                    LoadLeaveCounts();
                    //LoadChart(DateTime.Now);
                }
                catch (Exception ee)
                {
                    Log.log("Dashboard leave error.", ee);
                }
            }

            liCumulativeSummary.Visible = CommonManager.CompanySetting.WhichCompany == WhichCompany.Jyoti;
            menuOxfamLikeMannualCarryForward.Visible = CommonManager.CompanySetting.WhichCompany == WhichCompany.Oxfam;
            
            liTimeReqAttReport.Visible = CommonManager.CompanySetting.WhichCompany == WhichCompany.WDN;
        }

        void Initialise()
        {
           
         
            
            //LoadLeaveGrid2(DateTime.Now);


            // First parepare this month & next month dates
            CustomDate today = CustomDate.GetTodayDate(IsEnglish);
            int currentYear = today.Year;
            DateTime FromThisDate,FromNextDate;
            DateTime ToThisDate,ToNextDate;
            CustomDate firstDateOfThisMonth = new CustomDate(1, today.Month, currentYear, IsEnglish);
            CustomDate lastDateOfThisMonth = firstDateOfThisMonth.GetLastDateOfThisMonth();

            FromThisDate = DateManager.GetStartDate(firstDateOfThisMonth.EnglishDate);
            ToThisDate = DateManager.GetEndDate(lastDateOfThisMonth.EnglishDate);

            firstDateOfThisMonth = lastDateOfThisMonth.IncrementByOneDay();
            lastDateOfThisMonth = firstDateOfThisMonth.GetLastDateOfThisMonth();

            FromNextDate = DateManager.GetStartDate(firstDateOfThisMonth.EnglishDate);
            ToNextDate = DateManager.GetEndDate(lastDateOfThisMonth.EnglishDate);


            List<TextValue> list = null;// DashboardManager.GetEmployeeOnLeaveToday(LeaveRequestStatusEnum.Approved);
            //rptLeaveToday.DataSource = list;
            //rptLeaveToday.DataBind();
            //if (list.Count > 0)
            //    countLeaveToday.InnerHtml = list.Count.ToString();
            //else
            //    countLeaveToday.Visible = false;


            List<TextValue> listWeek = DashboardManager.GetEmployeeOnLeaveThisWeek(LeaveRequestStatusEnum.Approved);
         

            
            // List<AttendanceAllEmployeeReportResult> ListDailyAttendance = PayrollDataContext.AttendanceAllEmployeeReport(CurrentDate, "", null).ToList();

            
            


            List<int> removeIDList = null;// list.Select(x => x.ID).ToList(); listWeek.RemoveAll(x => removeIDList.Contains(x.ID));
            //rptLeaveThisWeek.DataSource = listWeek;
            //rptLeaveThisWeek.DataBind();
            //if (listWeek.Count > 0)
            //    countLeaveThisWeek.InnerHtml = listWeek.Count.ToString();
            //else
            //    countLeaveThisWeek.Visible = false;

            // Leave Pending Request
            //List<TextValue> leaveRequestList =  DashboardManager.GetEmployeeOnLeaveThisWeek(LeaveRequestStatusEnum.Request); ;            
            //rptPendingLeaveThisWeek.DataSource = leaveRequestList;
            //rptPendingLeaveThisWeek.DataBind();

           
            
            //List<TextValue> thisMonthPending =  DashboardManager.GetEmployeeOnLeaveThisMonth(LeaveRequestStatusEnum.Request); 
            //removeIDList = leaveRequestList.Select(x => x.ID).ToList();
            //thisMonthPending.RemoveAll(x => removeIDList.Contains(x.ID));
            //rptPendingLeaveThisMonth.DataSource = thisMonthPending;
            //rptPendingLeaveThisMonth.DataBind();

            LoadChart(DateTime.Now);
            LoadTimeAttendanceReqCount();
            LoadAttendanceCommentReqCount();
            DashBoardGraphicalData();
        }


        private void DashBoardGraphicalData()
        {
            try
            {
                List<AttendanceDashBoardAllEmployeeReportResult> ListTodayAttendance = AttendanceManager.getDailyAttedanceForDashBoard().ToList();
                List<DateTime> _listDateTime = new List<DateTime>();
                EWorkShift _DefaultShift = AttendanceManager.GetDefaultShift();
                if (ListTodayAttendance[0].AvgInTime == null && ListTodayAttendance[0].AvgOutTime == null)
                {
                    PanelTimeTrend.Hide();
                    PanelTimeTrend.Collapsed = true;

                    PanelPieChart.Hide();
                    PanelPieChart.Collapsed = true;
                    X.AddScript("document.getElementById('divNotAttendanceLog').style.display='block';");
                }

                if (_DefaultShift != null)
                {
                    InTimeStatisticsChart(ListTodayAttendance, _DefaultShift);
                    InOutTrendChart(ListTodayAttendance, _DefaultShift);
                }
                else
                {
                    DefaultInTimeStatIsTicsChart(ListTodayAttendance);
                    InOutTrendChartDefault(ListTodayAttendance);
                }


                if (ListTodayAttendance.Any())
                {
                    spnAverageInTime.InnerHtml = ListTodayAttendance[0].AvgInTime;
                    spnAverangeOutTime.InnerHtml = ListTodayAttendance[0].AvgOutTime;
                }
            }

            catch (Exception exp)
            {
                Log.log("Error while getting attendance data for Leave Dashboard", exp);
            }
            finally
            {

            }
        }


        private void InTimeStatisticsChart(List<AttendanceDashBoardAllEmployeeReportResult> ListTodayAttendance, EWorkShift _DefaultShift)
        {

           
           TimeSpan DT1 = new TimeSpan(0, 0, 0, 0, 0), DT2 = new TimeSpan(0, 0, 0, 0, 0), DT3=new TimeSpan(0, 0, 0, 0, 0)
            , DT4 = new TimeSpan(0, 0, 0, 0, 0), DT5 = new TimeSpan(0, 0, 0, 0, 0);

           if (_DefaultShift != null)
           {

               AttendanceInOutTime _dbAttendanceInOutTime = AttendanceManager.GetDefaultOfficeTime(_DefaultShift.WorkShiftId);
               if (_dbAttendanceInOutTime.OfficeInTime != null)
               {
                   DT1 = _dbAttendanceInOutTime.OfficeInTime.Value;
                   DT2 = DT1.Add(new TimeSpan(0, 0,30, 0, 0));
                   DT3 = DT2.Add(new TimeSpan(0, 0, 30, 0, 0));
                   DT4 = DT3.Add(new TimeSpan(0, 0, 30, 0, 0));
                   DT5 = DT4.Add(new TimeSpan(0, 0, 30, 0, 0));
               }
           }
           else
               return;

            DateTime Date1 = DateTime.Parse("2017/3/3 " + DT1.ToString()); //Office In Time like 10:00 AM
            DateTime Date2 = DateTime.Parse("2017/3/3 " + DT2.ToString());//Next Hour 10:30 AM
            DateTime Date3 = DateTime.Parse("2017/3/3 " + DT3.ToString());//Next Hour 11:00 AM
            DateTime Date4 = DateTime.Parse("2017/3/3 " + DT4.ToString());//Next Hour 11:30 AM

            List<TextValue> _ListChartData = new List<TextValue>();

            //Before 10:00
            int T4 = ListTodayAttendance.Where(x => x.InTime < Date1.TimeOfDay).ToList().Count();
            if (T4 > 0)
            {
                TextValue _TextValue4 = new TextValue();
                _TextValue4.Text = "Before " + DT1.Hours.ToString() + ":" + (DT1.Minutes == 0 ? "00" : DT1.Minutes.ToString());
                _TextValue4.Value = T4.ToString();
                _ListChartData.Add(_TextValue4);
            }

            //10 - 10:30 between Date1 and Date2
            int T1 = ListTodayAttendance.Where(x => x.InTime >= Date1.TimeOfDay && x.InTime <= Date2.TimeOfDay).ToList().Count();
            if (T1 > 0)
            {
                TextValue _TextValue1 = new TextValue();
                _TextValue1.Text = DT1.Hours.ToString() + ":" + (DT1.Minutes == 0 ? "00" : DT1.Minutes.ToString()) + " - " + DT2.Hours + ":" + (DT2.Minutes == 0 ? "00" : DT2.Minutes.ToString());
                _TextValue1.Value = T1.ToString();
                _ListChartData.Add(_TextValue1);
            }
            //10:00 - 10:30 between Date2 and Date3
            int T2 = ListTodayAttendance.Where(x => x.InTime >= Date2.TimeOfDay && x.InTime <= Date3.TimeOfDay).ToList().Count();
            if (T2 > 0)
            {
                TextValue _TextValue2 = new TextValue();
                _TextValue2.Text = DT2.Hours.ToString() + ":" + (DT2.Minutes == 0 ? "00" : DT2.Minutes.ToString()) + " - " + DT3.Hours + ":" + (DT3.Minutes == 0 ? "00" : DT3.Minutes.ToString());
                _TextValue2.Value = T2.ToString();
                _ListChartData.Add(_TextValue2);
            }
            //After 11 
            int T3 = ListTodayAttendance.Where(x => x.InTime > Date3.TimeOfDay).ToList().Count();
            if (T3 > 0)
            {
                TextValue _TextValue3 = new TextValue();
                _TextValue3.Text = "After " + DT3.Hours.ToString() + ":" + (DT3.Minutes == 0 ? "00" : DT3.Minutes.ToString());
                _TextValue3.Value = T3.ToString();
                _ListChartData.Add(_TextValue3);
            }

            

            if (T1 == 0 && T2 == 0 && T3 == 0 && T4 == 0)
                ChartInTimeStatistics.Hide();
            else
            {
                StoreInTimeStatistics.DataSource = _ListChartData;
                StoreInTimeStatistics.DataBind();
            }
        }


        private void InOutTrendChart(List<AttendanceDashBoardAllEmployeeReportResult> ListTodayAttendance, EWorkShift _DefaultShift)
        {

            TimeSpan DT1 = new TimeSpan(0, 0, 0, 0, 0), DT2 = new TimeSpan(0, 0, 0, 0, 0), DT3 = new TimeSpan(0, 0, 0, 0, 0)
           , DT4 = new TimeSpan(0, 0, 0, 0, 0), DT5 = new TimeSpan(0, 0, 0, 0, 0), DT6 = new TimeSpan(0, 0, 0, 0, 0);

            if (_DefaultShift != null)
            {
                AttendanceInOutTime _dbAttendanceInOutTime = AttendanceManager.GetDefaultOfficeTime(_DefaultShift.WorkShiftId);
                if (_dbAttendanceInOutTime.OfficeInTime != null)
                {

                    

                    DT1 = _dbAttendanceInOutTime.OfficeInTime.Value;
                   

                    //DT2 = DT1.Add(new TimeSpan(0, 2, 0, 0, 0));
                    //DT3 = DT2.Add(new TimeSpan(0, 2, 0, 0, 0));
                    //DT4 = DT3.Add(new TimeSpan(0, 2, 0, 0, 0));
                    //DT5 = DT4.Add(new TimeSpan(0, 2, 0, 0, 0));
                    //DT6 = DT5.Add(new TimeSpan(0, 2, 0, 0, 0));
                }
            }
            else
                return;

            DateTime Date1 = DateTime.Now.Date.AddHours(DT1.Hours).AddMinutes(DT1.Minutes);
            DateTime Date2 = Date1.AddHours(2);
            DateTime Date3 = Date2.AddHours(2);
            DateTime Date4 = Date3.AddHours(2);
            DateTime Date5 = Date4.AddHours(2);
            DateTime Date6 = Date5.AddHours(2);

            //DateTime Date1 = DateTime.Parse("2017/3/3 " + DT1.ToString()); //Office In Time like 8:30 AM
            //DateTime Date2 = DateTime.Parse("2017/3/3 " + DT2.ToString());//Next Hour 10:30 AM
            //DateTime Date3 = DateTime.Parse("2017/3/3 " + DT3.ToString());//Next Hour 12:00 AM
            //DateTime Date4 = DateTime.Parse("2017/3/3 " + DT4.ToString());//Next Hour 14:00 AM
            //DateTime Date5 = DateTime.Parse("2017/3/3 " + DT5.ToString());//Next Hour 16:00 AM
            //DateTime Date6 = DateTime.Parse("2017/3/3 " + DT6.ToString());//Next Hour 18:00 AM

            List<TextValue> _ListChartData = new List<TextValue>();
            //08:00
            int T1 = ListTodayAttendance.Where(x => x.InTime <= Date1.TimeOfDay).ToList().Count() - ListTodayAttendance.Where(x => x.OutTime <= Date1.TimeOfDay).ToList().Count();


            TextValue _TextValue1 = new TextValue();
            _TextValue1.Text = Date1.Hour.ToString() + ":" + (Date1.Minute == 0 ? "00" : Date1.Minute.ToString());
            _TextValue1.Value = T1.ToString();
            _ListChartData.Add(_TextValue1);
            //10:00

            int T2 = ListTodayAttendance.Where(x => x.InTime >= Date1.TimeOfDay && x.InTime <= Date2.TimeOfDay).ToList().Count() - ListTodayAttendance.Where(x => x.OutTime >= Date1.TimeOfDay && x.OutTime <= Date2.TimeOfDay).ToList().Count();
            TextValue _TextValue2 = new TextValue();
            _TextValue2.Text = Date2.Hour.ToString() + ":" + (Date2.Minute == 0 ? "00" : Date2.Minute.ToString());
            _TextValue2.Value = T2.ToString();
            _ListChartData.Add(_TextValue2);

            // 12:00
            int T3 = ListTodayAttendance.Where(x => x.InTime >=Date1.TimeOfDay && x.InTime <= Date3.TimeOfDay).ToList().Count() - ListTodayAttendance.Where(x => x.OutTime >=Date1.TimeOfDay && x.OutTime <= Date3.TimeOfDay).ToList().Count();
            TextValue _TextValue3 = new TextValue();
            _TextValue3.Text = Date3.Hour.ToString() + ":" + (Date3.Minute == 0 ? "00" : Date3.Minute.ToString());
            _TextValue3.Value = T3.ToString();
            _ListChartData.Add(_TextValue3);

            //Before 14:00
            int T4 = ListTodayAttendance.Where(x => x.InTime >=Date1.TimeOfDay && x.InTime <= Date4.TimeOfDay).ToList().Count() - ListTodayAttendance.Where(x => x.OutTime >=Date1.TimeOfDay && x.OutTime <= Date4.TimeOfDay).ToList().Count();
            TextValue _TextValue4 = new TextValue();
            _TextValue4.Text = Date4.Hour.ToString() + ":" + (Date4.Minute == 0 ? "00" : Date4.Minute.ToString());
            _TextValue4.Value = T4.ToString();
            _ListChartData.Add(_TextValue4);


            //Before 16:00
            int T5 = ListTodayAttendance.Where(x => x.InTime >=Date1.TimeOfDay && x.InTime <= Date5.TimeOfDay).ToList().Count() - ListTodayAttendance.Where(x => x.OutTime >=Date1.TimeOfDay && x.OutTime <= Date5.TimeOfDay).ToList().Count();
            TextValue _TextValue5 = new TextValue();
            _TextValue5.Text = Date5.Hour.ToString() + ":" + (Date5.Minute == 0 ? "00" : Date5.Minute.ToString());
            _TextValue5.Value = T5.ToString();
            _ListChartData.Add(_TextValue5);

            //Before 18:00
            int T6 = ListTodayAttendance.Where(x => x.InTime >= Date1.TimeOfDay && x.InTime <= Date6.TimeOfDay).ToList().Count() - ListTodayAttendance.Where(x => x.OutTime >= Date1.TimeOfDay && x.OutTime <= Date6.TimeOfDay).ToList().Count();
            TextValue _TextValue6 = new TextValue();
            _TextValue6.Text = Date6.Hour.ToString() + ":" + (Date6.Minute == 0 ? "00" : Date6.Minute.ToString());
            _TextValue6.Value = T6.ToString();
            _ListChartData.Add(_TextValue6);

            StoreInOutTrend.DataSource = _ListChartData;
            StoreInOutTrend.DataBind();

        }

        private void DefaultInTimeStatIsTicsChart(List<AttendanceDashBoardAllEmployeeReportResult> ListTodayAttendance)
        {


            List<TextValue> _ListChartData = new List<TextValue>();
            //09:00 - 10:00
            int T1 = ListTodayAttendance.Where(x => x.InTime >= DateTime.Parse("2017/3/3 9:00:00").TimeOfDay && x.InTime <= DateTime.Parse("2017/3/3 10:00:00").TimeOfDay).ToList().Count();
            if (T1 > 0)
            {
                TextValue _TextValue1 = new TextValue();
                _TextValue1.Text = "09:00 - 10:00";
                _TextValue1.Value = T1.ToString();
                _ListChartData.Add(_TextValue1);
            }
            //10:00 - 11:00

            int T2 = ListTodayAttendance.Where(x => x.InTime >= DateTime.Parse("2017/3/3 10:00:00").TimeOfDay && x.InTime <= DateTime.Parse("2017/3/3 11:00:00").TimeOfDay).ToList().Count();
            if (T2 > 0)
            {
                TextValue _TextValue2 = new TextValue();
                _TextValue2.Text = "10:00 - 11:00";
                _TextValue2.Value = T2.ToString();
                _ListChartData.Add(_TextValue2);
            }

            //After 11:00
            int T3 = ListTodayAttendance.Where(x => x.InTime > DateTime.Parse("2017/3/3 11:00:00").TimeOfDay).ToList().Count();
            if (T3 > 0)
            {
                TextValue _TextValue3 = new TextValue();
                _TextValue3.Text = "After 11:00";
                _TextValue3.Value = T3.ToString();
                _ListChartData.Add(_TextValue3);
            }


            //Before 9:00
            int T4 = ListTodayAttendance.Where(x => x.InTime < DateTime.Parse("2017/3/3 9:00:00").TimeOfDay).ToList().Count();

            if (T4 > 0)
            {
                 TextValue _TextValue4 = new TextValue();
                _TextValue4.Text = "Before 9:00";
                _TextValue4.Value = T4.ToString();
                _ListChartData.Add(_TextValue4);
            }

            if (T1 == 0 && T2 == 0 && T3 == 0 && T4 == 0)
                ChartInTimeStatistics.Hide();
            else
            {
                StoreInTimeStatistics.DataSource = _ListChartData;
                StoreInTimeStatistics.DataBind();
            }
        }



        private void InOutTrendChartDefault(List<AttendanceDashBoardAllEmployeeReportResult> ListTodayAttendance)
        {
            List<TextValue> _ListChartData = new List<TextValue>();
            //08:00
            int T1 = ListTodayAttendance.Where(x => x.InTime <= DateTime.Parse("2017/3/3 8:00:00").TimeOfDay).ToList().Count()-
                ListTodayAttendance.Where(x => x.OutTime <= DateTime.Parse("2017/3/3 8:00:00").TimeOfDay).ToList().Count();
            TextValue _TextValue1 = new TextValue();
            _TextValue1.Text = "08:00";
            _TextValue1.Value = T1.ToString();
            _ListChartData.Add(_TextValue1);
            //10:00

            int T2 = ListTodayAttendance.Where(x => x.InTime >= DateTime.Parse("2017/3/3 8:00:00").TimeOfDay && x.InTime <= DateTime.Parse("2017/3/3 10:00:00").TimeOfDay).ToList().Count()-
            ListTodayAttendance.Where(x => x.OutTime >= DateTime.Parse("2017/3/3 8:00:00").TimeOfDay && x.OutTime <= DateTime.Parse("2017/3/3 10:00:00").TimeOfDay).ToList().Count();
             TextValue _TextValue2 = new TextValue();
            _TextValue2.Text = "10:00";
            _TextValue2.Value = T2.ToString();
            _ListChartData.Add(_TextValue2);

            // 12:00
            int T3 = ListTodayAttendance.Where(x => x.InTime >=DateTime.Parse("2017/3/3 8:00:00").TimeOfDay && x.InTime <= DateTime.Parse("2017/3/3 12:00:00").TimeOfDay).ToList().Count() -
                ListTodayAttendance.Where(x => x.OutTime >= DateTime.Parse("2017/3/3 8:00:00").TimeOfDay && x.OutTime <= DateTime.Parse("2017/3/3 12:00:00").TimeOfDay).ToList().Count();

            TextValue _TextValue3 = new TextValue();
            _TextValue3.Text = "12:00";
            _TextValue3.Value = T3.ToString();
            _ListChartData.Add(_TextValue3);

            //Before 14:00
            int T4 = ListTodayAttendance.Where(x => x.InTime >=DateTime.Parse("2017/3/3 8:00:00").TimeOfDay && x.InTime <= DateTime.Parse("2017/3/3 14:00:00").TimeOfDay).ToList().Count() -
                ListTodayAttendance.Where(x => x.OutTime >=DateTime.Parse("2017/3/3 8:00:00").TimeOfDay && x.OutTime <= DateTime.Parse("2017/3/3 14:00:00").TimeOfDay).ToList().Count(); 

            TextValue _TextValue4 = new TextValue();
            _TextValue4.Text = "14:00";
            _TextValue4.Value = T4.ToString();
            _ListChartData.Add(_TextValue4);


            //Before 16:00
            int T5 = ListTodayAttendance.Where(x => x.InTime >= DateTime.Parse("2017/3/3 8:00:00").TimeOfDay && x.InTime <= DateTime.Parse("2017/3/3 16:00:00").TimeOfDay).ToList().Count() -
                ListTodayAttendance.Where(x => x.OutTime >= DateTime.Parse("2017/3/3 8:00:00").TimeOfDay && x.OutTime <= DateTime.Parse("2017/3/3 16:00:00").TimeOfDay).ToList().Count();

            TextValue _TextValue5 = new TextValue();
            _TextValue5.Text = "16:00";
            _TextValue5.Value = T5.ToString();
            _ListChartData.Add(_TextValue5);

            //Before 18:00
            int T6 = ListTodayAttendance.Where(x => x.InTime >= DateTime.Parse("2017/3/3 8:00:00").TimeOfDay && x.InTime <= DateTime.Parse("2017/3/3 18:00:00").TimeOfDay).ToList().Count() -
                ListTodayAttendance.Where(x => x.OutTime >= DateTime.Parse("2017/3/3 8:00:00").TimeOfDay && x.OutTime <= DateTime.Parse("2017/3/3 18:00:00").TimeOfDay).ToList().Count(); 

            TextValue _TextValue6 = new TextValue();
            _TextValue6.Text = "18:00";
            _TextValue6.Value = T6.ToString();
            _ListChartData.Add(_TextValue6);

            StoreInOutTrend.DataSource = _ListChartData;
            StoreInOutTrend.DataBind();

        }

        private void LoadChart(DateTime givenDayParam)
        {
            //List<DashboardEmployeeOnLeaveResult> listForChart = DashboardManager.getEmpOnLeaveForDashboardList(-1, givenDayParam).Where(x => x.Num > 0).ToList();

            List<DashboardEmployeeOnLeaveTypeCountsResult> listForChart = LeaveAttendanceManager.GetEmployeeOnLeaveTypeCounts().Where(x => x.Num > 0).ToList();

            if (listForChart.Count == 0)
            {
                chartEmpLeaveTypeCount.Hidden = true;
            }                

            chartEmpLeaveTypeCountStore.DataSource = listForChart;
            chartEmpLeaveTypeCountStore.DataBind();
        }

      



       
      

        private void LoadLeaveCounts()
        {
            //spanPending.InnerHtml = DashboardManager.CountLeaveRequest(LeaveRequestStatusEnum.Request).ToString();
            //spanRecommended.InnerHtml = DashboardManager.CountLeaveRequest(LeaveRequestStatusEnum.Recommended).ToString();



            List<int> leaveEmployee = EmployeeManager.LeaveToday();
            
           

            List<int> atWorkEmployee = EmployeeManager.PresentToday();
            List<int> totalEmployees = EmployeeManager.ActiveTodayEmployeeCount();

            int absentEmployee = totalEmployees.Count - leaveEmployee.Count - atWorkEmployee.Count;

            if (absentEmployee < 0)
                absentEmployee = 0;

            //foreach (var val1 in bindList)
            //{
            //    if (val1.AtteState == 4 || val1.AtteState == 1)
            //        leaveEmployee++;
            //    else if (val1.AtteState == 9)
            //        absentEmployee++;
            //}

            //atWorkEmployee = totalEmployees - (leaveEmployee + absentEmployee);

            spanTotalEmployee.InnerHtml = totalEmployees.Count.ToString();
            spanAtWork.InnerHtml = atWorkEmployee.Count.ToString();
            spanOnLeave.InnerHtml = leaveEmployee.Count.ToString();
            spanAbsent.InnerHtml = absentEmployee.ToString();

            int pendingRequest = 0;
            int recommendedRequest = 0;

            pendingRequest = DashboardManager.CountLeaveRequest(LeaveRequestStatusEnum.Request);
            recommendedRequest = DashboardManager.CountLeaveRequest(LeaveRequestStatusEnum.Recommended);



            countRequested.InnerHtml = pendingRequest.ToString();

            countRecommended.InnerHtml = recommendedRequest.ToString();


            spanPendingLeaves.InnerHtml = (pendingRequest + recommendedRequest).ToString();
            spanPending.InnerHtml = pendingRequest.ToString();
            spanRecommended.InnerHtml = recommendedRequest.ToString();

            
        }

        

        private void LoadAttendanceCommentReqCount()
        {
            spanTimeAttCommentReq.InnerHtml = AttendanceManager.GetAttendanceCommentRequestsCount().ToString();
        }

        private void LoadTimeAttendanceReqCount()
        {
            int count = AttendanceManager.GetTimeRequestsCount();
            spanTimeAttReq.InnerHtml = count.ToString();

            if (count == 0)
                blockTimeAttendanceRequests.Visible = false;
        }

    }
}
