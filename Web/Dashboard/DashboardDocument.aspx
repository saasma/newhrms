<%@ Page Title="Document Dashboard" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="DashboardDocument.aspx.cs" Inherits="Web.CP.DashboardDocument" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .dbTitle
        {
            display: inline-block;
            background-color: #E7EFFC;
            width: 97%;
            position: relative;
            border-bottom: thin solid #B2D0E8;
            line-height: 30px;
            vertical-align: middle;
            padding-left: 5px;
            padding-right: 6px;
            border-right: 1px solid #B2D0E8;
        }
        
        .dbTitle a
        {
            float: right;
            text-decoration: none; /* border:thin solid gray;*/
            cursor: pointer;
        }
        .dbTitle h2
        {
            color: #0070C0;
            float: left;
            font-size: 12px;
            margin-top: 4px;
            margin-bottom: 4px;
        }
        
        .subTitle
        {
            color: #616161;
            font-weight: bold;
            font-size: 12px !important;
            background: #F9F9F9;
            line-height: 20px;
            width: 99%;
            margin-top: 2px;
            margin-bottom: 2px;
        }
        .dbPanel
        {
            display: none;
            border: thin solid #B2D0E8;
            margin: 10px 0px 15px 0px;
        }
        
        .dbPanelContent
        {
            padding: 3px;
            display: inline-block;
            width: 100%;
            overflow: hidden; /*width:100%;*/
        }
        
        .Column
        {
            width: 33%;
        }
        
        .contentWrapper > table
        {
            /* padding-left: 8px;
            padding-right: 8px;*/
            width: 100%;
        }
        .contentWrapper > table td
        {
            vertical-align: top; /* width:100%;*/ /* width:33%;*/
            padding-right: 5px;
        }
        
        .hideHeader .x-grid-header-ct
        {
            background-image: none !important;
            display: none !important;
            border: none !important;
        }
        
        <%--.contentWrapper .x-grid-cell-last > .x-grid-cell-inner
        {
            text-align: right !important;
        }--%>
        
        .roundCorner
        {
            border: thin solid #5DA5ED;
            -moz-border-radius: 4px 4px 4px 4px;
            -webkit-border-radius: 4px 4px 4px 4px;
            padding: 0px;
            background: -moz-linear-gradient(center top, #999999 0%,#ffffff 0%);
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0, #999999),color-stop(0, #ffffff));
        }
        
        /*options cases*/
        .option
        {
            padding: 10px;
        }
        .option a, .option span
        {
            text-align: center;
            text-decoration: none;
            font-weight: bold;
        }
        .option img, .option span
        {
            display: block;
            text-decoration: none;
            font-size: 15px;
        }
        .option img
        {
            width: 80px;
            height: 80px;
            padding-left: 12px;
        }
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
        .reportBlock hr
        {
            margin: 7px 0 0 35px;
        }
        .reportArrow
        {
            margin: 3px 0 0 10px;
            width: 15px;
            height: 15px;
            float: left;
        }
        .reports
        {
            padding: 5px 0;
        }
        .panel-heading
        {
            height: 40px !important;
        }
        
        .btn-list
        {
            width:640px;
        }
        
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Document Dashboard
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div>
            <div style="width: 100%; visibility: visible" class="contentWrapper" id="container">
                <table>
                    <tr>
                        <td>
                            <div class="btn-list" style="padding-left: 0px">
                                <a id="A1" href="~/NewHr/DocumentManagement/Document.aspx" runat="server" visible='<%# IsAccessible("NewHr/DocumentManagement/Document.aspx") %>'>
                                    <span class="btn btn-default btn-metro"><span class="glyphicon dashboard">
                                        <img src="../css/DashboardIcons/DocDocument.png" alt="" /></span><div style="margin-top: 8px !important;">
                                            Add Document</div>
                                    </span></a><a id="A2" href="~/NewHr/DocumentManagement/Dealer.aspx?type=1" runat="server"
                                        visible='<%# IsAccessible("NewHr/DocumentManagement/Dealer.aspx?type=1") %>'><span
                                            class="btn btn-default btn-metro"><span class="glyphicon dashboard">
                                                <img src="../css/DashboardIcons/DocDocument.png" alt="" /></span><div style="margin-top: 8px !important;">
                                                    Add Dealer</div>
                                        </span></a><a id="A3" href="~/NewHr/DocumentManagement/Dealer.aspx?type=2" runat="server"
                                            visible='<%# IsAccessible("NewHr/DocumentManagement/Dealer.aspx?type=2") %>'><span
                                                class="btn btn-default btn-metro"><span class="glyphicon dashboard">
                                                    <img src="../css/DashboardIcons/DocDocument.png" alt="" /></span><div style="margin-top: 8px !important;">
                                                        Add Painter</div>
                                            </span></a><a id="A7" href="~/NewHr/DocumentManagement/DocumentList.aspx" runat="server"
                                                visible='<%# IsAccessible("NewHr/DocumentManagement/DocumentList.aspx") %>'><span
                                                    class="btn btn-default btn-metro"><span class="glyphicon dashboard">
                                                        <img src="../css/DashboardIcons/DocList.png" alt="" /></span><div style="margin-top: 8px !important;">
                                                            Documents</div>
                                                </span></a><a id="A6" href="~/NewHr/DocumentManagement/DealerList.aspx" runat="server"
                                                    visible='<%# IsAccessible("NewHr/DocumentManagement/DealerList.aspx") %>'><span
                                                        class="btn btn-default btn-metro"><span class="glyphicon dashboard">
                                                            <img src="../css/DashboardIcons/DocList.png" alt="" /></span><div style="margin-top: 8px !important;">
                                                                Dealers</div>
                                                    </span></a><a id="A5" href="~/NewHr/DocumentManagement/PainterList.aspx" runat="server"
                                                        visible='<%# IsAccessible("NewHr/DocumentManagement/PainterList.aspx") %>'><span
                                                            class="btn btn-default btn-metro"><span class="glyphicon dashboard">
                                                                <img src="../css/DashboardIcons/DocList.png" alt="" /></span><div style="margin-top: 8px !important;">
                                                                    Painters</div>
                                                        </span></a>
                            </div>
                            <br />
                            <table>
                                <tr>
                                    <td>
                                        <div class="panel panel-primary" style="border-color: #4E5154;">
                                            <div class="panel-heading" style="background-color: #4E5154; border: none;">
                                                <!-- panel-btns -->
                                                <h3 class="panel-title">
                                                    Expiring Documents</h3>
                                            </div>
                                            <div class="panel-body" style="border: none; padding: 0 0 0 0;">
                                                <ext:GridPanel ID="gridExpiringDocuments" runat="server" Cls="itemgrid" MarginSpec="0 0 0 0"
                                                    Header="true" Width="600">
                                                    <Store>
                                                        <ext:Store ID="storeParty" runat="server">
                                                            <Model>
                                                                <ext:Model ID="Model4" runat="server">
                                                                    <Fields>
                                                                        <ext:ModelField Name="DocumentID" Type="String" />
                                                                        <ext:ModelField Name="PartyRef_ID" Type="String" />
                                                                        <ext:ModelField Name="Title" Type="String" />
                                                                        <ext:ModelField Name="DaysCount" Type="String" />
                                                                        <ext:ModelField Name="PartyName" Type="String" />
                                                                        <ext:ModelField Name="ExpiryDate" Type="String" />
                                                                    </Fields>
                                                                </ext:Model>
                                                            </Model>
                                                        </ext:Store>
                                                    </Store>
                                                    <ColumnModel>
                                                        <Columns>
                                                            <ext:TemplateColumn ID="TemplateColumn1" runat="server" Text="Party" Width="210"
                                                                MenuDisabled="false" Sortable="false" DataIndex="PartyName" TemplateString='<a href="../NewHr/DocumentManagement/DocumentList.aspx?PartyID={PartyRef_ID}"> {PartyName}</a>' />
                                                            <ext:TemplateColumn ID="TemplateColumn4" runat="server" Text="Document Title" Width="210"
                                                                MenuDisabled="false" Sortable="false" DataIndex="Title" TemplateString='<a href="../NewHr/DocumentManagement/DocumentView.aspx?DocumentId={DocumentID}"> {Title}</a>' />
                                                            <ext:Column ID="Column1" Sortable="false" MenuDisabled="true" runat="server" Text="Expiring in"
                                                                Width="120" DataIndex="ExpiryDate" />
                                                            <ext:Column ID="Column6" Sortable="false" MenuDisabled="true" runat="server" Text="Days"
                                                                Width="60" DataIndex="DaysCount" Align="Center" />
                                                        </Columns>
                                                    </ColumnModel>
                                                    <SelectionModel>
                                                        <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                                                    </SelectionModel>
                                                    <BottomBar>
                                                        <ext:Toolbar ID="Toolbar2" runat="server">
                                                            <Items>
                                                                <ext:ToolbarFill ID="ToolbarFill2" runat="server" />
                                                                <ext:Button ID="btnExportExpiringDoc" runat="server" Text="To Excel" AutoPostBack="true" OnClick="btnExportExpiringDoc_Click" Icon="PageExcel">
                                                                </ext:Button>
                                                            </Items>
                                                        </ext:Toolbar>
                                                    </BottomBar>
                                                </ext:GridPanel>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="panel panel-primary" style="border-color: #4E5154;">
                                            <div class="panel-heading" style="background-color: #4E5154; border: none;">
                                                <!-- panel-btns -->
                                                <h3 class="panel-title">
                                                    Expired Documents</h3>
                                            </div>
                                            <div class="panel-body" style="border: none; padding: 0 0 0 0;">
                                                <ext:GridPanel ID="gridExpiredDocuments" runat="server" Cls="itemgrid" MarginSpec="0 0 0 00"
                                                    Title="" Header="true" Width="600">
                                                    <Store>
                                                        <ext:Store ID="store2" runat="server">
                                                            <Model>
                                                                <ext:Model ID="Model2" runat="server">
                                                                    <Fields>
                                                                        <ext:ModelField Name="DocumentID" Type="String" />
                                                                        <ext:ModelField Name="PartyRef_ID" Type="String" />
                                                                        <ext:ModelField Name="Title" Type="String" />
                                                                        <ext:ModelField Name="DaysCount" Type="String" />
                                                                        <ext:ModelField Name="ExpiryDate" Type="String" />
                                                                        <ext:ModelField Name="PartyName" Type="String" />
                                                                    </Fields>
                                                                </ext:Model>
                                                            </Model>
                                                        </ext:Store>
                                                    </Store>
                                                    <ColumnModel>
                                                        <Columns>
                                                            <ext:TemplateColumn ID="TemplateColumn3" runat="server" Text="Party" Width="210"
                                                                MenuDisabled="false" Sortable="false" DataIndex="PartyName" TemplateString='<a href="../NewHr/DocumentManagement/DocumentList.aspx?PartyID={PartyRef_ID}"> {PartyName}</a>' />
                                                            <ext:TemplateColumn ID="TemplateColumn5" runat="server" Text="Document Title" Width="210"
                                                                MenuDisabled="false" Sortable="false" DataIndex="Title" TemplateString='<a href="../NewHr/DocumentManagement/DocumentView.aspx?DocumentId={DocumentID}"> {Title}</a>' />
                                                            <ext:Column ID="Column2" Sortable="false" MenuDisabled="true" runat="server" Text="Expired On"
                                                                Width="120" DataIndex="ExpiryDate">
                                                            </ext:Column>
                                                            <ext:Column ID="Column8" Sortable="false" MenuDisabled="true" runat="server" Text="Days"
                                                                Width="60" DataIndex="DaysCount" Align="Center">
                                                            </ext:Column>
                                                        </Columns>
                                                    </ColumnModel>
                                                    <SelectionModel>
                                                        <ext:RowSelectionModel ID="RowSelectionModel3" runat="server" Mode="Single" />
                                                    </SelectionModel>
                                                    <BottomBar>
                                                        <ext:Toolbar ID="Toolbar3" runat="server">
                                                            <Items>
                                                                <ext:ToolbarFill ID="ToolbarFill3" runat="server" />
                                                                <ext:Button ID="btnExportExpiredDoc" runat="server" Text="To Excel" AutoPostBack="true" OnClick="btnExportExpiredDoc_Click" Icon="PageExcel">
                                                                </ext:Button>
                                                            </Items>
                                                        </ext:Toolbar>
                                                    </BottomBar>
                                                </ext:GridPanel>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style='padding-left: 10px; display: none; border-color: #4E5154;'>
                                        <div class="panel panel-primary">
                                            <div class="panel-heading" style="background-color: #4E5154; border: none;">
                                                <!-- panel-btns -->
                                                <h3 class="panel-title">
                                                    Comments</h3>
                                            </div>
                                            <div class="panel-body" style="border: none; padding: 0 0 0 0;">
                                                <ext:GridPanel ID="gridComments" runat="server" Cls="itemgrid" MarginSpec="0 0 0 0"
                                                    Title="Comments" Header="false" HideHeaders="true" Width="550">
                                                    <Store>
                                                        <ext:Store ID="store3" runat="server">
                                                            <Model>
                                                                <ext:Model ID="Model3" runat="server">
                                                                    <Fields>
                                                                        <ext:ModelField Name="DocumentID" Type="String" />
                                                                        <ext:ModelField Name="Title" Type="String" />
                                                                        <ext:ModelField Name="Comment" Type="String" />
                                                                        <ext:ModelField Name="TimeDetails" Type="string" />
                                                                        <ext:ModelField Name="CommentedBy" Type="String" />
                                                                    </Fields>
                                                                </ext:Model>
                                                            </Model>
                                                        </ext:Store>
                                                    </Store>
                                                    <ColumnModel>
                                                        <Columns>
                                                            <ext:Column ID="Column10" Sortable="false" MenuDisabled="true" runat="server" Text="Commented By"
                                                                Align="Left" Width="200" DataIndex="CommentedBy" />
                                                            <ext:Column ID="Column7" Sortable="false" MenuDisabled="true" runat="server" Text="Comment"
                                                                Width="200" DataIndex="Comment">
                                                            </ext:Column>
                                                            <ext:Column ID="Column11" Sortable="false" MenuDisabled="true" runat="server" Text="Commented Before"
                                                                Align="Left" Width="150" DataIndex="TimeDetails" />
                                                        </Columns>
                                                    </ColumnModel>
                                                    <SelectionModel>
                                                        <ext:RowSelectionModel ID="RowSelectionModel4" runat="server" Mode="Single" />
                                                    </SelectionModel>
                                                </ext:GridPanel>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <ext:Store runat="server" ID="storeDocumentSearch" AutoLoad="false">
                                <Proxy>
                                    <ext:AjaxProxy Url="../Handler/DocumentSearch.ashx">
                                        <ActionMethods Read="GET" />
                                        <Reader>
                                            <ext:JsonReader Root="plants" TotalProperty="total" />
                                        </Reader>
                                    </ext:AjaxProxy>
                                </Proxy>
                                <Model>
                                    <ext:Model ID="Model5" IDProperty="Value" Name="ItemLineModel" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Text" Type="String" />
                                            <ext:ModelField Name="Value" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                            <ext:ComboBox LabelSeparator="" ID="cmbSearchDocument" runat="server" DisplayField="Text"
                                MarginSpec="0 0 0 0" EmptyText="Search Party, Document Title, Document ID" LabelAlign="Top"
                                ValueField="Value" StoreID="storeDocumentSearch" TypeAhead="false" Width="300"
                                HideBaseTrigger="true" MinChars="1" TriggerAction="All" ForceSelection="true">
                                <ListConfig LoadingText="Searching..." MinWidth="450" StyleSpec="border-top:1px solid #98c0f4;">
                                    <ItemTpl ID="ItemTpl1" runat="server">
                                        <Html>
                                            <tpl>
                                                    <div class="search-item">
                                                                    <span>{Text}</span>  
                                                     </div>
					                        </tpl>
                                        </Html>
                                    </ItemTpl>
                                </ListConfig>
                                <Listeners>
                                    <Select Handler="this.getTrigger(0).show();" />
                                    <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                </Listeners>
                                <DirectEvents>
                                    <Select OnEvent="cmbSearchDocument_Click" />
                                </DirectEvents>
                            </ext:ComboBox>
                            <br />
                            <div class="panel panel-primary" style="border-color: #4E5154; width: 477px;">
                                <div class="panel-heading" style="background-color: #4E5154; border: none;">
                                    <!-- panel-btns -->
                                    <h3 class="panel-title">
                                        Recent Documents</h3>
                                </div>
                                <div class="panel-body" style="border: none; padding: 0 0 0 0;">
                                    <ext:GridPanel ID="gridRecentDocuments" runat="server" Cls="itemgrid" MarginSpec="0 0 0 0"
                                        Title="" Header="false" Width="475">
                                        <Store>
                                            <ext:Store ID="store1" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model1" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="DocumentID" Type="String" />
                                                            <ext:ModelField Name="PartyRef_ID" Type="String" />
                                                            <ext:ModelField Name="Title" Type="String" />
                                                            <ext:ModelField Name="PartyName" Type="String" />
                                                            <ext:ModelField Name="CreatedOnDate" Type="String" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:TemplateColumn ID="TemplateColumn2" runat="server" Text="Party" Width="175"
                                                    MenuDisabled="false" Sortable="false" DataIndex="PartyName" TemplateString='<a href="../NewHr/DocumentManagement/DocumentList.aspx?PartyID={PartyRef_ID}"> {PartyName}</a>' />
                                                <ext:TemplateColumn ID="TemplateColumn6" runat="server" Text="Document Title" Width="200"
                                                    MenuDisabled="false" Sortable="false" DataIndex="Title" TemplateString='<a href="../NewHr/DocumentManagement/DocumentView.aspx?DocumentId={DocumentID}"> {Title}</a>' />
                                                <ext:Column ID="Column9" Sortable="false" MenuDisabled="true" runat="server" Text="Created On"
                                                    Width="100" DataIndex="CreatedOnDate">
                                                </ext:Column>
                                            </Columns>
                                        </ColumnModel>
                                        <SelectionModel>
                                            <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                                        </SelectionModel>
                                        <BottomBar>
                                            <ext:Toolbar ID="Toolbar1" runat="server">
                                                <Items>
                                                    <ext:ToolbarFill ID="ToolbarFill1" runat="server" />
                        
                                                    <ext:Button ID="btnExportRecentDoc" runat="server" Text="To Excel" AutoPostBack="true" OnClick="btnExportRecentDoc_Click" Icon="PageExcel">
                                                        
                                                    </ext:Button>
                                                </Items>
                                            </ext:Toolbar>
                                        </BottomBar>
                                    </ext:GridPanel>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
                <br />
                <div class="panel panel-dark-head">
                    <div class="panel-heading">
                        <!-- panel-btns -->
                        <h3 class="panel-title">
                            Settings</h3>
                    </div>
                    <div class="panel-body panel-body-small">
                        <ul role="menu" class="dropdown-menu dropdown-demo-only custommenulist">
                            <li>
                                <asp:HyperLink ID="HyperLink1" Visible='<%# IsAccessible("NewHr/DocumentManagement/DocPartyList.aspx") %>'
                                    runat="server" NavigateUrl="~/NewHr/DocumentManagement/DocPartyList.aspx">
                                                         <i class="fa fa-bars"></i>Parties
                                </asp:HyperLink>
                            </li>
                            <li>
                                <asp:HyperLink ID="HyperLink36" Visible='<%# IsAccessible("NewHr/DocumentManagement/DocumentCategories.aspx") %>'
                                    runat="server" NavigateUrl="~/NewHr/DocumentManagement/DocumentCategories.aspx">
                                                         <i class="fa fa-bars"></i>Categories
                                </asp:HyperLink>
                            </li>
                            <li>
                                <asp:HyperLink ID="HyperLink39" Visible='<%# IsAccessible("NewHr/DocumentManagement/DocumentAdditionalInformation.aspx") %>'
                                    runat="server" NavigateUrl="~/NewHr/DocumentManagement/DocumentAdditionalInformation.aspx">
                                                         <i class="fa fa-bars"></i> Additional Information
                                </asp:HyperLink>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
