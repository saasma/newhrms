﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
//using System.Web.UI.DataVisualization.Charting;
using BLL.Base;
using System.Globalization;
using Utils.Calendar;
using BLL.BO;

namespace Web.CP
{
    public partial class DashboardAppraisal : BasePage
    {
        CommonManager commonMgr = new CommonManager();
        //private double totalSalarySummary = 0;

        private bool moduleAccessible = false;
        public new bool IsAccessible(string url)
        {
            if (moduleAccessible)
                return true;

            if (SessionManager.User != null
                && SessionManager.User.URole.URoleModules.Any(x => x.ModuleRef_ID == (int)ModuleEnum.HumanResource))
            {
                moduleAccessible = true;
                return true;
            }

            return UserManager.IsPageAccessible(url);

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialise();
                this.DataBind();
            }
        }

        void Initialise()
        {
            // First parepare this month & next month dates
            CustomDate today = CustomDate.GetTodayDate(IsEnglish);
            int currentYear = today.Year;
            DateTime FromThisDate, FromNextDate;
            DateTime ToThisDate, ToNextDate;
            CustomDate firstDateOfThisMonth = new CustomDate(1, today.Month, currentYear, IsEnglish);
            CustomDate lastDateOfThisMonth = firstDateOfThisMonth.GetLastDateOfThisMonth();

            FromThisDate = DateManager.GetStartDate(firstDateOfThisMonth.EnglishDate);
            ToThisDate = DateManager.GetEndDate(lastDateOfThisMonth.EnglishDate);

            firstDateOfThisMonth = lastDateOfThisMonth.IncrementByOneDay();
            lastDateOfThisMonth = firstDateOfThisMonth.GetLastDateOfThisMonth();

            FromNextDate = DateManager.GetStartDate(firstDateOfThisMonth.EnglishDate);
            ToNextDate = DateManager.GetEndDate(lastDateOfThisMonth.EnglishDate);


            List<TextValue> list = null;



            List<TextValue> listWeek = DashboardManager.GetEmployeeOnLeaveThisWeek(LeaveRequestStatusEnum.Approved);
            //for (int i = listWeek.Count - 1; i >= 0; i--)
            //{
            //    if (list.Select(x => x.ID).Contains(listWeek[i].ID))
            //    {
            //        listWeek.RemoveAt(i);
            //    }
            //}

            List<int> removeIDList = new List<int>();// list.Select(x => x.ID).ToList(); listWeek.RemoveAll(x => removeIDList.Contains(x.ID));




            // Retriring
            //rptRetiringThisMonth.DataSource = DashboardManager.GetEmployeeRetiring(FromThisDate, ToThisDate);
            //rptRetiringThisMonth.DataBind();
            //countRetiringThisMonth.InnerHtml = DashboardManager.GetEmployeeRetiring(FromThisDate, ToThisDate).Count.ToString();
            //if (countRetiringThisMonth.InnerHtml == "0")
            //    countRetiringThisMonth.Visible = false;

            ////rptRetiringNextMonth.DataSource = DashboardManager.GetEmployeeRetiring(FromNextDate, ToNextDate);
            ////rptRetiringNextMonth.DataBind();
            //countRetiringNextMonth.InnerHtml = DashboardManager.GetEmployeeRetiring(FromNextDate, ToNextDate).Count.ToString();
            //if (countRetiringNextMonth.InnerHtml == "0")
            //    countRetiringNextMonth.Visible = false;








        }



    }
}
