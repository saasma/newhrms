﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
//using System.Web.UI.DataVisualization.Charting;
using BLL.Base;
using System.Globalization;
using Utils.Calendar;
using BLL.BO;
using Ext.Net;

namespace Web.CP
{
    public partial class DashboardProject : BasePage
    {
        CommonManager commonMgr = new CommonManager();
        private double totalSalarySummary = 0;

        private bool moduleAccessible = false;
        public new bool IsAccessible(string url)
        {
            if (moduleAccessible)
                return true;

            if (SessionManager.User.URole.URoleModules.Any(x => x.ModuleRef_ID == (int)ModuleEnum.ProjectAndTimesheet))
            {
                moduleAccessible = true;
                return true;
            }

            return UserManager.IsPageAccessible(url);

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialise();
                this.DataBind();

                timesheetMonth.SelectedDate = DateTime.Now;
                btnLoad_Click(null, null);
            }
        }

        void Initialise()
        {
            // First parepare this month & next month dates
            CustomDate today = CustomDate.GetTodayDate(IsEnglish);
            int currentYear = today.Year;
            DateTime FromThisDate, FromNextDate;
            DateTime ToThisDate, ToNextDate;
            CustomDate firstDateOfThisMonth = new CustomDate(1, today.Month, currentYear, IsEnglish);
            CustomDate lastDateOfThisMonth = firstDateOfThisMonth.GetLastDateOfThisMonth();

            FromThisDate = DateManager.GetStartDate(firstDateOfThisMonth.EnglishDate);
            ToThisDate = DateManager.GetEndDate(lastDateOfThisMonth.EnglishDate);

            firstDateOfThisMonth = lastDateOfThisMonth.IncrementByOneDay();
            lastDateOfThisMonth = firstDateOfThisMonth.GetLastDateOfThisMonth();

            FromNextDate = DateManager.GetStartDate(firstDateOfThisMonth.EnglishDate);
            ToNextDate = DateManager.GetEndDate(lastDateOfThisMonth.EnglishDate);

            Setting _Setting = CalculationManager.GetSetting();

            if (_Setting.IsMonthlyTimeSheet != null && _Setting.IsMonthlyTimeSheet == true)
            {
                linkTimeSheet.NavigateUrl = "~/CP/MonthWiseTimesheetList.aspx";
                linkTimesheetSubmitted.NavigateUrl = "~/CP/MonthWiseTimesheetList.aspx";

                linkProjectAssociation.NavigateUrl = "~/CP/Extension/MonthlyProjectAssociation.aspx";
            }
            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.Care)
            {
                careProjectContribution.Visible = true;
                careProjectContributionReport.Visible = true;
            }
            //else
            //    linkTimeSheet.NavigateUrl = "~/NewHR/TimesheetList.aspx";
            
            //linkTimeSheet.NavigateUrl = "";

           // List<TextValue> list = null;



            //List<TextValue> listWeek = DashboardManager.GetEmployeeOnLeaveThisWeek(LeaveRequestStatusEnum.Approved);
            //for (int i = listWeek.Count - 1; i >= 0; i--)
            //{
            //    if (list.Select(x => x.ID).Contains(listWeek[i].ID))
            //    {
            //        listWeek.RemoveAt(i);
            //    }
            //}

            List<int> removeIDList = new List<int>();// list.Select(x => x.ID).ToList(); listWeek.RemoveAll(x => removeIDList.Contains(x.ID));



        }

        class ExportBO
        {
            public string Status { get; set; }
            public int Count { get; set; }
        }
        //public void btnExport_Click(object sender, EventArgs e)
        //{
        //    if (DateTime.MinValue == timesheetMonth.SelectedDate)
        //    {
        //        NewMessage.ShowWarningMessage("Date selection is required.");
        //    }
        //    else
        //    {
                
        //        DateTime monthStartdate = NewTimeSheetManager.GetMonthStartDate(timesheetMonth.SelectedDate);
        //        List<GetTimeSheetListResult> list = NewTimeSheetManager.GetTimeSheetList(
        //            0, 99999, -1, monthStartdate.ToString(), monthStartdate.ToString(), -2);

        //        title.InnerHtml = "Timesheet for : " +
        //            monthStartdate.Year + " - " + DateHelper.GetMonthName(monthStartdate.Month, IsEnglish);

        //        List<ExportBO> list1= new List<ExportBO>();

        //        list1.Add(new ExportBO { Status= "Not Filled", Count = list.Where(x => x.Status == (int)TimeSheetStatus.NotSubmitted).Count() });
        //        list1.Add(new ExportBO { Status = "Draft", Count = list.Where(x => x.Status == (int)TimeSheetStatus.Draft).Count() });
        //        list1.Add(new ExportBO { Status = "Awaiting Approval", Count = list.Where(x => x.Status == (int)TimeSheetStatus.AwaitingApproval).Count() });
        //        list1.Add(new ExportBO { Status = "Approved", Count = list.Where(x => x.Status == (int)TimeSheetStatus.Approved).Count() });
        //        list1.Add(new ExportBO { Status = "Rejected", Count = list.Where(x => x.Status == (int)TimeSheetStatus.Rejected).Count() });

        //        if (CommonManager.CompanySetting.WhichCompany == WhichCompany.PSI)
        //            list1.Add(new ExportBO { Status = "Reviewed", Count = list.Where(x => x.Status == (int)TimeSheetStatus.Reviewed).Count() });





        //    }
        //}
        public void btnLoad_Click(object sender, EventArgs e)
        {
            if (DateTime.MinValue == timesheetMonth.SelectedDate)
            {
                NewMessage.ShowWarningMessage("Date selection is required.");
            }
            else
            {
                int RowsCount = 0;
                DateTime monthStartdate = NewTimeSheetManager.GetMonthStartDate(timesheetMonth.SelectedDate);
                List<GetTimeSheetListResult> list = NewTimeSheetManager.GetTimeSheetList(
                    0, 99999, -1, monthStartdate.ToString(), monthStartdate.ToString(), -2);

                title.InnerHtml = "Timesheet submit : " +
                    monthStartdate.Year + " - " + DateHelper.GetMonthName(monthStartdate.Month,IsEnglish);

                spanNotFilled.InnerHtml = list.Where(x => x.Status == (int)TimeSheetStatus.NotSubmitted).Count().ToString();
                spanDraft.InnerHtml = list.Where(x => x.Status == (int)TimeSheetStatus.Draft).Count().ToString();
                spanAwaitingApproval.InnerHtml = list.Where(x => x.Status == (int)TimeSheetStatus.AwaitingApproval).Count().ToString();
                spanApproved.InnerHtml = list.Where(x => x.Status == (int)TimeSheetStatus.Approved).Count().ToString();
                spanRejected.InnerHtml = list.Where(x => x.Status == (int)TimeSheetStatus.Rejected).Count().ToString();

                if (CommonManager.CompanySetting.WhichCompany == WhichCompany.PSI)
                {
                    liReviewedPSI.Visible = true;
                    spanReviewedPSI.InnerHtml = list.Where(x => x.Status == (int)TimeSheetStatus.Reviewed).Count().ToString();
                }
            }
        }


    }
}
