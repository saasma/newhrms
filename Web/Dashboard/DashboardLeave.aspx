<%@ Page Title="Leave Dashboard" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="DashboardLeave.aspx.cs" Inherits="Web.CP.DashboardLeave" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .Column {
        }

        .contentWrapper > table {
            /* padding-left: 8px;
            padding-right: 8px;*/
            width: 100%;
        }

            .contentWrapper > table td {
                vertical-align: top; /* width:100%;*/ /* width:33%;*/
                padding-right: 5px;
            }

        .borderRadius {
            border-radius: 0px;
        }

        .style6 {
            width: 200px;
            height: 33px;
        }

        .style14 {
            height: 16px;
        }

        .style15 {
            height: 16px;
            width: 296px;
        }

        .style16 {
            width: 153px;
            height: 33px;
        }

        .style17 {
            height: 30px;
            width: 153px;
        }

        .style19 {
            height: 56px;
            width: 177px;
        }

        .style20 {
            height: 45px;
            width: 177px;
        }

        .style21 {
            width: 190px;
            height: 16px;
        }

        .w3-note {
            background-color: #ffffcc;
            border-left: 6px solid #ffeb3b;
        }

        w3.css:230 .w3-panel {
            padding: 0.01em 16px;
            margin-top: 16px !important;
            margin-bottom: 16px !important;
        }

        .divNoDataCls {
            height: 27px;
            padding-top: 7px;
            padding-left: 10px;
            width: 836px;
            margin-top: 10px;
        }

        .col-sm-12{padding-left:0px!important;}
    </style>
    <script type="text/javascript">
        var tipRenderer = function (storeItem, item) {
            //calculate percentage.
            var total = 0;
            <%=chartEmpLeaveTypeCount.ClientID %>.getStore().each(function (rec) {
                total += rec.get('Num');
            });
            this.setTitle(storeItem.get('Abbreviation') + ': ' + Math.round(storeItem.get('Num') / total * 100) + '%');
        };

        var tipRendererInTimeStatIsTics = function (storeItem, item) {
            //calculate percentage.
            var total = 0;
            <%=ChartInTimeStatistics.ClientID%>.getStore().each(function (rec) {
                total += rec.get('Value');
            });
            
            this.setTitle(Math.round(storeItem.get('Value') / total * 100) + '%');
        };


        var labelRenderer = function(item){
         
            var total = 0;
            var percentage = 0;
            <%=chartEmpLeaveTypeCountStore.ClientID %>.each(function (rec) {
                total += rec.get('Num');
            });
            

            if( total != 0)
            {
                <%=chartEmpLeaveTypeCountStore.ClientID %>.each(function (rec) {
                

                    if(rec.get('Abbreviation')==item)
                    {
                        percentage = Math.round(rec.get('Num') / total * 100);
                    }
                });

            }
            return percentage+'%';        
        };

            
        function segmentRenderer (sprite, record, attr, index, store) {
            var value =index,//0 none,1 green.2blue,3orange,4blue
                color = ["#A1C436", "#5FB4EF","#F2BC02", "#F23A02", "#ffd13e", "#a61187", "#24ad9a", "#7c7474", "#a66111"][value];

                
            return Ext.apply(attr, {
                fill: color
            });
        };


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>Leave and Attendance Dashboard
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel" style="padding-left: 10px">
        <div class="innerLR">
            <div>
                <div>
                    <div style="width: 100%; visibility: visible" class="contentWrapper" id="container">
                        <table>
                            <tr>
                                <td class="Column" style="width: 60%">
                                    <table>
                                        <tr>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td colspan="2" style="background-color: #5B9BD5; color: White; vertical-align: middle; padding-left: 5px;"
                                                            class="style6">Total Employees
                                                        </td>
                                                        <td colspan="2" style="padding: 5px; background-color: #5B9BD5; color: White; text-align: center; font-size: 20px">
                                                            <span id="spanTotalEmployee" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="background-color: #A9D08E; height: 30px; padding: 5px; text-align: center">At Work
                                                        </td>
                                                        <td style="background-color: #FFD966; padding: 5px; text-align: center">On Leave
                                                        </td>
                                                        <td style="background-color: #F4B084; padding: 5px; text-align: center">Absent
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="background-color: #E2EFDA; height: 30px; padding: 5px; text-align: center; color: #2F75C2">
                                                            <span id="spanAtWork" runat="server" />
                                                        </td>
                                                        <td style="background-color: #FFF2CC; text-align: center; padding-top: 5px; color: #2F75C2">
                                                            <span id="spanOnLeave" runat="server" />
                                                        </td>
                                                        <td style="background-color: #FCE4D6; text-align: center; padding-top: 5px; color: #2F75C2">
                                                            <span id="spanAbsent" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td class="style14"></td>
                                            <td class="style14" style="width: 208px">
                                                <table>
                                                    <tr>
                                                        <td style="background-color: #5B9BD5; color: White; vertical-align: middle; padding-left: 5px;"
                                                            class="style16">Pending Leaves
                                                        </td>
                                                        <td style="padding: 5px; background-color: #5B9BD5; color: White; text-align: center; font-size: 20px">
                                                            <span id="spanPendingLeaves" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="background-color: #FFD966; padding: 5px; text-align: center" class="style17">Requested
                                                        </td>
                                                        <td style="background-color: #A9D08E; padding: 5px; text-align: center">Recommended
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="background-color: #FFF2CC; padding: 5px; text-align: center; color: #2F75C2"
                                                            class="style17">
                                                            <span id="spanPending" runat="server" />
                                                        </td>
                                                        <td style="background-color: #E2EFDA; text-align: center; padding-top: 5px; color: #2F75C2">
                                                            <span id="spanRecommended" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="border-style: solid; border: 1px solid #5B9BD5; padding-right: 0px">
                                                <table>
                                                    <tr>
                                                        <td style="background-color: white; color: #ED7D31; vertical-align: middle; padding-left: 5px; font-size: 28px; text-align: center;"
                                                            class="style20">
                                                            <a href="../Attendance/AttendanceDailyReport.aspx"><span id="spnAverageInTime" runat="server" /></a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="background-color: #DDEBF7; text-align: center; color: Black; height: 50px; font-size: 14px; padding-top: 15px">Average In-Time
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td>&nbsp;
                                            </td>
                                            <td style="border-style: solid; border: 1px solid #5B9BD5; padding-right: 0px;">
                                                <table>
                                                    <tr>
                                                        <td style="background-color: white; color: #7030A0; vertical-align: middle; padding-left: 5px; font-size: 28px; text-align: center;"
                                                            class="style20">
                                                            <a href="../Attendance/AttendanceDailyReport.aspx"><span id="spnAverangeOutTime" runat="server" /></a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="background-color: #DDEBF7; text-align: center; color: Black; height: 50px; font-size: 14px; padding-top: 15px">Average Out-Time
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <table>
                                        <tr>
                                            <td>
                                                <div class="w3-panel w3-note divNoDataCls" id="divNotAttendanceLog" style="display: none">
                                                    <p>
                                                        No attendance data available.
                                                    </p>
                                                </div>
                                                <ext:Panel ID="PanelPieChart" runat="server" Width="420" Height="320" Layout="FitLayout">
                                                    <Items>
                                                        <ext:Chart ID="ChartInTimeStatistics" runat="server" Animate="true" Shadow="false"
                                                            InsetPadding="30" Theme="Base:gradients">
                                                            <LegendConfig Position="Bottom" BoxStroke="0" />
                                                            <Store>
                                                                <ext:Store ID="StoreInTimeStatistics" runat="server" AutoDataBind="true">
                                                                    <Model>
                                                                        <ext:Model ID="Model1" runat="server">
                                                                            <Fields>
                                                                                <ext:ModelField Name="Text" />
                                                                                <ext:ModelField Name="Value" Type="Int" />
                                                                            </Fields>
                                                                        </ext:Model>
                                                                    </Model>
                                                                </ext:Store>
                                                            </Store>
                                                            <Series>
                                                                <ext:PieSeries AngleField="Value" ShowInLegend="false" Donut="0" Highlight="true"
                                                                    HighlightSegmentMargin="20">
                                                                    <Label Field="Text" Display="InsideEnd" Contrast="true" Font="11px Arial" Width="400" />
                                                                    <Tips ID="Tips1" TrackMouse="true" Width="100" Height="20" runat="server">
                                                                        <Renderer Fn="tipRendererInTimeStatIsTics" />
                                                                    </Tips>
                                                                    <Renderer Fn="segmentRenderer" />
                                                                </ext:PieSeries>
                                                            </Series>
                                                        </ext:Chart>
                                                    </Items>
                                                </ext:Panel>
                                            </td>
                                            <td style="padding-top: 20px; padding-left: 10px">
                                                <ext:Panel ID="PanelTimeTrend" runat="server" Layout="FitLayout" Width="430" Height="260">
                                                    <Items>
                                                        <ext:Chart ID="ChartInOutFlow" runat="server" StyleSpec="background:#fff;" Shadow="false"
                                                            StandardTheme="Green" Animate="true">
                                                            <LegendConfig Position="Right" />
                                                            <Store>
                                                                <ext:Store ID="StoreInOutTrend" runat="server" AutoDataBind="true">
                                                                    <Model>
                                                                        <ext:Model ID="Model2" runat="server">
                                                                            <Fields>
                                                                                <ext:ModelField Name="Text" />
                                                                                <ext:ModelField Name="Value" Type="Int" />
                                                                            </Fields>
                                                                        </ext:Model>
                                                                    </Model>
                                                                </ext:Store>
                                                            </Store>
                                                            <Axes>
                                                                <ext:NumericAxis Fields="Value" Title="" MinorTickSteps="1" Minimum="0">
                                                                    <GridConfig>
                                                                        <Odd Opacity="0" Fill="#ddd" Stroke="#bbb" StrokeWidth="1" />
                                                                    </GridConfig>
                                                                </ext:NumericAxis>
                                                                <ext:CategoryAxis Position="Bottom" Fields="Text" Title="" />
                                                            </Axes>
                                                            <Series>
                                                                <ext:LineSeries Axis="Left" Smooth="3" Fill="true" XField="Text" YField="Value" ShowInLegend="false">
                                                                    <HighlightConfig Size="7" Radius="7" />
                                                                    <MarkerConfig Type="Circle" Size="4" Radius="4" StrokeWidth="0" />
                                                                </ext:LineSeries>
                                                            </Series>
                                                        </ext:Chart>
                                                    </Items>
                                                </ext:Panel>
                                            </td>
                                        </tr>
                                    </table>
                                    <div id="divUpperBlock" style="display: none">
                                        <div class="col-md-4">
                                            <div class="panel panel-dark noborder">
                                                <div class="panel-heading noborder" style="border-radius: 0px;">
                                                    <div class="panel-btns">
                                                        <%--<a href="#" class="panel-close tooltips" data-toggle="tooltip" title="Close Panel"><i
                                                        class="fa fa-times"></i></a>--%>
                                                    </div>
                                                    <!-- panel-btns -->
                                                    <div class="panel-icon">
                                                        <i class="fa fa-users"></i>
                                                    </div>
                                                    <div class="media-body">
                                                        <h5 class="md-title nomargin">EMPLOYEES</h5>
                                                        <h1 class="mt5"></h1>
                                                    </div>
                                                    <!-- media-body -->
                                                    <hr>
                                                    <div class="clearfix mt20">
                                                        <div class="pull-left">
                                                            <h5 class="md-title nomargin">AT WORK</h5>
                                                            <h4 class="nomargin"></h4>
                                                        </div>
                                                        <div class="pull-left" style="padding-left: 30px;">
                                                            <h5 class="md-title nomargin">ON LEAVE</h5>
                                                            <h4 class="nomargin"></h4>
                                                        </div>
                                                        <div class="pull-right">
                                                            <h5 class="md-title nomargin">ABSENT</h5>
                                                            <h4 class="nomargin"></h4>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- panel-body -->
                                            </div>
                                            <!-- panel -->
                                        </div>
                                        <!-- col-md-4 -->
                                        <div class="col-md-4">
                                            <div class="panel panel-primary noborder">
                                                <div class="panel-heading noborder" style="border-radius: 0px;">
                                                    <div class="panel-btns">
                                                        <%--<a href="#" class="panel-close tooltips" data-toggle="tooltip" title="Close Panel"><i
                                                        class="fa fa-times"></i></a>--%>
                                                    </div>
                                                    <!-- panel-btns -->
                                                    <div class="panel-icon">
                                                        <i class="fa fa-users"></i>
                                                    </div>
                                                    <div class="media-body">
                                                        <h5 class="md-title nomargin">PENDING LEAVES</h5>
                                                        <h1 class="mt5"></h1>
                                                    </div>
                                                    <!-- media-body -->
                                                    <hr>
                                                    <div class="clearfix mt20">
                                                        <div class="pull-left">
                                                            <h5 class="md-title nomargin">PENDING</h5>
                                                            <h4 class="nomargin"></h4>
                                                        </div>
                                                        <div class="pull-right">
                                                            <h5 class="md-title nomargin">RECOMMENDED</h5>
                                                            <h4 class="nomargin"></h4>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- panel-body -->
                                            </div>
                                            <!-- panel -->
                                        </div>
                                        <!-- col-md-4 -->
                                        <div class="col-md-4">
                                            <ext:Panel ID="pnlChart" runat="server" Layout="FitLayout" Width="255" Height="162"
                                                Title="Leave taken this month">
                                                <Items>
                                                    <ext:Chart ID="chartEmpLeaveTypeCount" Region="Center" runat="server" Animate="true"
                                                        Shadow="false">
                                                        <LegendConfig Position="Right" />
                                                        <Store>
                                                            <ext:Store ID="chartEmpLeaveTypeCountStore" runat="server" AutoDataBind="true">
                                                                <Model>
                                                                    <ext:Model ID="Model7" runat="server">
                                                                        <Fields>
                                                                            <ext:ModelField Name="Abbreviation" />
                                                                            <ext:ModelField Name="Num" />
                                                                        </Fields>
                                                                    </ext:Model>
                                                                </Model>
                                                            </ext:Store>
                                                        </Store>
                                                        <Series>
                                                            <ext:PieSeries AngleField="Num" ShowInLegend="true" Donut="0" Highlight="true" HighlightSegmentMargin="20">
                                                                <Label Field="Abbreviation" Display="Rotate" Contrast="true" Font="10px Arial">
                                                                    <Renderer Fn="labelRenderer" />
                                                                </Label>
                                                                <Tips TrackMouse="true" Width="140" Height="28" runat="server">
                                                                    <Renderer Fn="tipRenderer" />
                                                                </Tips>
                                                            </ext:PieSeries>
                                                        </Series>
                                                    </ext:Chart>
                                                </Items>
                                            </ext:Panel>
                                        </div>
                                    </div>



                                    <div class="col-sm-12">
                                    <div class="panel panel-dark-head" style="padding-top:10px">
                                        <div class="panel-heading">
                                            <!-- panel-btns -->
                                            <h3 class="panel-title">Common Tasks</h3>
                                        </div>
                                        <div class="panel-body panel-body-small">

                                            <table>

                                                <tr>
                                                    <td>
                                                        <ul role="menu" class="dropdown-menu dropdown-demo-only custommenugap">
                                                            <li class="dashboardMenuHeader"><a>Leave</a></li>
                                                            <li class="divider fulldivider" role="presentation"></li>

                                                            <li>
                                                                <asp:HyperLink ID="HyperLink6" Visible='<%# IsAccessible("newhr/LeaveRequestList.aspx") %>'
                                                                    Text="" runat="server" NavigateUrl="~/newhr/LeaveRequestList.aspx"
                                                                    pageid="48">
                                                         <i class="fa fa-bars"></i>Leave Requests
                                                                </asp:HyperLink>
                                                            </li>
                                                            <li>
                                                                <asp:HyperLink ID="HyperLink9" Visible='<%# IsAccessible("CP/LeaveAdjustmentPage.aspx") %>'
                                                                    Text="" runat="server" NavigateUrl="~/CP/LeaveAdjustmentPage.aspx"
                                                                    pageid="48">
                                                         <i class="fa fa-bars"></i>Leave Adjustment
                                                                </asp:HyperLink>
                                                            </li>



                                                        </ul>
                                                    </td>
                                                    <td>
                                                        <ul role="menu" class="dropdown-menu dropdown-demo-only custommenugap">
                                                            <li class="dashboardMenuHeader"><a>Attendance</a></li>
                                                            <li class="divider fulldivider" role="presentation"></li>
                                                            <li>
                                                                <asp:HyperLink ID="HyperLink15" Visible='<%# IsAccessible("CP/AttendanceTracking.aspx") %>'
                                                                    Text="" runat="server" NavigateUrl="~/CP/AttendanceTracking.aspx"
                                                                    pageid="48">
                                                         <i class="fa fa-bars"></i>Attendance Register
                                                                </asp:HyperLink>
                                                            </li>
                                                            <li>
                                                                <asp:HyperLink ID="HyperLink16" Visible='<%# IsAccessible("Attendance/AttendanceDailyReport.aspx") %>'
                                                                    Text="" runat="server" NavigateUrl="~/Attendance/AttendanceDailyReport.aspx"
                                                                    pageid="48">
                                                         <i class="fa fa-bars"></i>Daily Attendance
                                                                </asp:HyperLink>
                                                            </li>


                                                            <li>
                                                                <asp:HyperLink ID="HyperLink1" Visible='<%# IsAccessible("Attendance/LateProcessingDetails.aspx") %>'
                                                                    Text="" runat="server" NavigateUrl="~/Attendance/LateProcessingDetails.aspx"
                                                                    pageid="48">
                                                         <i class="fa fa-bars"></i> Late Entry Deduction
                                                                </asp:HyperLink>
                                                            </li>
                                                            <li>
                                                                <asp:HyperLink ID="HyperLink2" Visible='<%# IsAccessible("newhr/ManageOvernightShift.aspx") %>'
                                                                    Text="" runat="server" NavigateUrl="~/newhr/ManageOvernightShift.aspx" pageid="48">
                                                         <i class="fa fa-bars"></i> Overnight work check
                                                                </asp:HyperLink>
                                                            </li>
                                                            <li runat="server" id="menuOxfamLikeMannualCarryForward">
                                                                <asp:HyperLink ID="HyperLink8" Visible='<%# IsAccessible("CP/MannualLeaveCarryForward.aspx") %>'
                                                                    Text="" runat="server" NavigateUrl="~/CP/MannualLeaveCarryForward.aspx" pageid="48">
                                                         <i class="fa fa-bars"></i> Manual Carry Forward
                                                                </asp:HyperLink>
                                                            </li>

                                                        </ul>
                                                    </td>
                                                    <td>
                                                        <ul role="menu" class="dropdown-menu dropdown-demo-only custommenugap">
                                                            <li class="dashboardMenuHeader"><a>Other</a></li>
                                                            <li class="divider fulldivider" role="presentation"></li>

                                                            <li runat="server" id="Li1">
                                                                <asp:HyperLink ID="HyperLink26" Visible='<%# IsAccessible("attendance/AttendanceExceptionReport.aspx") %>'
                                                                    Text="" runat="server" NavigateUrl="~/attendance/AttendanceExceptionReport.aspx" pageid="48">
                                                         <i class="fa fa-bars"></i> Exception Entry
                                                                </asp:HyperLink>
                                                            </li>

                                                            <li runat="server" id="Li2">
                                                                <asp:HyperLink ID="HyperLink27" Visible='<%# IsAccessible("attendance/AttendanceAbsentLeaveMarking.aspx") %>'
                                                                    Text="" runat="server" NavigateUrl="~/attendance/AttendanceAbsentLeaveMarking.aspx" pageid="48">
                                                         <i class="fa fa-bars"></i> Absent Leave Marking
                                                                </asp:HyperLink>
                                                            </li>

                                                            <li runat="server" id="Li3">
                                                                <asp:HyperLink ID="HyperLink28" Visible='<%# IsAccessible("newhr/EmployeesOnLeaveList.aspx") %>'
                                                                    Text="" runat="server" NavigateUrl="~/newhr/EmployeesOnLeaveList.aspx" pageid="48">
                                                         <i class="fa fa-bars"></i> Employees on Leave
                                                                </asp:HyperLink>
                                                            </li>
                                                        </ul>
                                                    </td>
                                                </tr>
                                            </table>






                                        </div>
                                    </div>

                                    
                                        <div class="panel panel-dark-head">
                                            <div class="panel-heading">
                                                <!-- panel-btns -->
                                                <h3 class="panel-title">Attendance Reports</h3>
                                            </div>
                                            <div class="panel-body panel-body-small">

                                                <table>

                                                    <tr>
                                                        <td>
                                                            <ul role="menu" class="dropdown-menu dropdown-demo-only custommenugap">
                                                                <li class="dashboardMenuHeader"><a>Attendance Record</a></li>
                                                                <li class="divider fulldivider" role="presentation"></li>
                                                                <li>
                                                                    <asp:HyperLink ID="HyperLink3" Visible='<%# IsAccessible("Attendance/HRAttendanceEmpMontlySummary.aspx") %>'
                                                                        Text="" runat="server" NavigateUrl="~/Attendance/HRAttendanceEmpMontlySummary.aspx"
                                                                        pageid="48">
                                                         <i class="fa fa-bars"></i>Attendance Chart (monthly)
                                                                    </asp:HyperLink>
                                                                </li>
                                                                <li>
                                                                    <asp:HyperLink ID="HyperLink4" Visible='<%# IsAccessible("Attendance/HRAttendanceEmpMonthDetails.aspx") %>'
                                                                        Text="" runat="server" NavigateUrl="~/Attendance/HRAttendanceEmpMonthDetails.aspx"
                                                                        pageid="48">
                                                         <i class="fa fa-bars"></i>Employee Attendance Detail
                                                                    </asp:HyperLink>
                                                                </li>


                                                                <li>
                                                                    <asp:HyperLink ID="HyperLink7" Visible='<%# IsAccessible("Attendance/EmployeeBranchAttendance.aspx") %>'
                                                                        runat="server" NavigateUrl="~/Attendance/EmployeeBranchAttendance.aspx">
                                                         <i class="fa fa-bars"></i>Branch Employee Attendance
                                                                    </asp:HyperLink>
                                                                </li>
                                                                <li>
                                                                    <asp:HyperLink ID="liTimeReqAttReport" Visible='<%# IsAccessible("Attendance/TimeReqAttendanceReport.aspx") %>'
                                                                        runat="server" NavigateUrl="~/Attendance/TimeReqAttendanceReport.aspx">
                                                         <i class="fa fa-bars"></i>Time Request Attendance Report
                                                                    </asp:HyperLink>
                                                                </li>
                                                            </ul>
                                                        </td>
                                                        <td>
                                                            <ul role="menu" class="dropdown-menu dropdown-demo-only custommenugap">
                                                                <li class="dashboardMenuHeader"><a>Work Days (hours)</a></li>
                                                                <li class="divider fulldivider" role="presentation"></li>
                                                                <li>
                                                                    <asp:HyperLink ID="HyperLink14" Visible='<%# IsAccessible("Attendance/HREmployeeAttSummary.aspx") %>'
                                                                        Text="" runat="server" NavigateUrl="~/Attendance/HREmployeeAttSummary.aspx" pageid="48">
                                                         <i class="fa fa-bars"></i>Work Days
                                                                    </asp:HyperLink>
                                                                </li>
                                                                <li>
                                                                    <asp:HyperLink ID="HyperLink18" Visible='<%# IsAccessible("newhr/GenerateAttendanceReportData.aspx") %>'
                                                                        Text="" runat="server" NavigateUrl="~/newhr/GenerateAttendanceReportData.aspx"
                                                                        pageid="48">
                                                         <i class="fa fa-bars"></i>Generate Work Hours
                                                                    </asp:HyperLink>
                                                                </li>
                                                                <li>
                                                                    <asp:HyperLink ID="HyperLink5" Visible='<%# IsAccessible("Attendance/AttendanceSummary.aspx") %>'
                                                                        Text="" runat="server" NavigateUrl="~/Attendance/AttendanceSummary.aspx" pageid="48">
                                                         <i class="fa fa-bars"></i>Work Hours / Attendance (summary)
                                                                    </asp:HyperLink>
                                                                </li>
                                                                <li>
                                                                    <asp:HyperLink ID="HyperLink17" Visible='<%# IsAccessible("Attendance/AttendanceDetails.aspx") %>'
                                                                        Text="" runat="server" NavigateUrl="~/Attendance/AttendanceDetails.aspx" pageid="48">
                                                         <i class="fa fa-bars"></i>Work Hours / Attendance (details)
                                                                    </asp:HyperLink>
                                                                </li>


                                                            </ul>
                                                        </td>
                                                        <td>
                                                            <ul role="menu" class="dropdown-menu dropdown-demo-only custommenugap">
                                                                <li class="dashboardMenuHeader"><a>Absentism</a></li>
                                                                <li class="divider fulldivider" role="presentation"></li>
                                                                <li>
                                                                    <asp:HyperLink ID="HyperLink20" Visible='<%# IsAccessible("newhr/AttendanceDeductionReport.aspx") %>'
                                                                        runat="server" NavigateUrl="~/newhr/AttendanceDeductionReport.aspx">
                                                         <i class="fa fa-bars"></i>Absent Deduction Summary
                                                                    </asp:HyperLink>
                                                                </li>
                                                                <li>
                                                                    <asp:HyperLink ID="HyperLink21" Visible='<%# IsAccessible("newhr/AttendanceDeductionDetails.aspx") %>'
                                                                        runat="server" NavigateUrl="~/newhr/AttendanceDeductionDetails.aspx">
                                                         <i class="fa fa-bars"></i>Absent Deduction Detail
                                                                    </asp:HyperLink>
                                                                </li>
                                                                <li runat="server" id="liCumulativeSummary">
                                                                    <asp:HyperLink ID="HyperLink22" Visible='<%# IsAccessible("newhr/CumulativeAttendanceSummary.aspx") %>'
                                                                        runat="server" NavigateUrl="~/newhr/CumulativeAttendanceSummary.aspx">
                                                         <i class="fa fa-bars"></i>Cumulative Summary
                                                                    </asp:HyperLink>
                                                                </li>
                                                            </ul>
                                                        </td>
                                                    </tr>
                                                </table>



                                            </div>
                                        </div>
                                        <div class="panel panel-dark-head">
                                            <div class="panel-heading">
                                                <!-- panel-btns -->
                                                <h3 class="panel-title">Leave Reports</h3>
                                            </div>
                                            <div class="panel-body panel-body-small">

                                                <table>

                                                    <tr>
                                                        <td>
                                                            <ul role="menu" class="dropdown-menu dropdown-demo-only custommenugap">
                                                                <li class="dashboardMenuHeader"><a>Leave Balances</a></li>
                                                                <li class="divider fulldivider" role="presentation"></li>

                                                                <li>
                                                                    <asp:HyperLink ID="HyperLink36" Visible='<%# IsAccessible("NewHR/YearLeaveReport.aspx") %>'
                                                                        runat="server" NavigateUrl="~/NewHR/YearLeaveReport.aspx">
                                                         <i class="fa fa-bars"></i>Yearly Leave Report
                                                                    </asp:HyperLink>
                                                                </li>
                                                                <li>
                                                                    <asp:HyperLink ID="HyperLink39" Visible='<%# IsAccessible("CP/Report/HRLeaveBalance.aspx") %>'
                                                                        runat="server" NavigateUrl="~/CP/Report/HRLeaveBalance.aspx">
                                                         <i class="fa fa-bars"></i>Monthly Leave Report
                                                                    </asp:HyperLink>
                                                                </li>
                                                                <li>
                                                                    <asp:HyperLink ID="HyperLink40" Visible='<%# IsAccessible("CP/Report/ConsolidatedLeaveReport.aspx") %>'
                                                                        runat="server" NavigateUrl="~/CP/Report/ConsolidatedLeaveReport.aspx">
                                                         <i class="fa fa-bars"></i>Consolidated Leave Bal
                                                                    </asp:HyperLink>
                                                                </li>
                                                                <li>
                                                                    <asp:HyperLink ID="HyperLink38" Visible='<%# IsAccessible("CP/Report/HRLeaveBalanceSummary.aspx") %>'
                                                                        runat="server" NavigateUrl="~/CP/Report/HRLeaveBalanceSummary.aspx">
                                                         <i class="fa fa-bars"></i>Leave Balances for a Period
                                                                    </asp:HyperLink>
                                                                </li>

                                                            </ul>
                                                        </td>
                                                        <td>
                                                            <ul role="menu" class="dropdown-menu dropdown-demo-only custommenugap">
                                                                <li class="dashboardMenuHeader"><a>Leave Taken</a></li>
                                                                <li class="divider fulldivider" role="presentation"></li>
                                                                <li>
                                                                    <asp:HyperLink ID="HyperLink37" Visible='<%# IsAccessible("NewHR/PeriodicLeaveTakenReport.aspx") %>'
                                                                        runat="server" NavigateUrl="~/NewHR/PeriodicLeaveTakenReport.aspx">
                                                         <i class="fa fa-bars"></i>Leave Taken in a Period
                                                                    </asp:HyperLink>
                                                                </li>
                                                                <li>
                                                                    <asp:HyperLink ID="HyperLink41" Visible='<%# IsAccessible("CP/Report/LeaveInADayReport.aspx") %>'
                                                                        runat="server" NavigateUrl="~/CP/Report/LeaveInADayReport.aspx">
                                                         <i class="fa fa-bars"></i>Leave Taken Report
                                                                    </asp:HyperLink>
                                                                </li>
                                                            </ul>
                                                        </td>
                                                    </tr>
                                                </table>






                                            </div>
                                        </div>
                                        <div class="panel panel-info-alt">
                                            <div class="panel-heading">
                                                <!-- panel-btns -->
                                                <h3 class="panel-title">Settings</h3>
                                            </div>
                                            <div class="panel-body panel-body-small">
                                                <div style="clear: both">
                                                </div>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <ul role="menu" class="dropdown-menu dropdown-demo-only custommenugap">
                                                                <li class="dashboardMenuHeader"><a>Leave Management</a></li>
                                                                <li class="divider fulldivider" role="presentation"></li>
                                                                <li>
                                                                    <asp:HyperLink ID="HyperLink12" Visible='<%# IsAccessible("CP/ManageLeaves.aspx") %>'
                                                                        Text="" runat="server" NavigateUrl="~/CP/ManageLeaves.aspx" pageid="48">
                                                         <i class="fa fa-bars"></i>Leaves Names
                                                                    </asp:HyperLink>

                                                                </li>
                                                                <li>
                                                                    <asp:HyperLink ID="HyperLink24" Visible='<%# IsAccessible("cp/AttendanceTime.aspx") %>'
                                                                        Text="" runat="server" NavigateUrl="~/cp/AttendanceTime.aspx">
                                                         <i class="fa fa-bars"></i>Office Timings
                                                                    </asp:HyperLink>
                                                                </li>
                                                                <li>
                                                                    <asp:HyperLink ID="HyperLink11" Visible='<%# IsAccessible("CP/Holiday.aspx") %>'
                                                                        Text="" runat="server" NavigateUrl="~/CP/Holiday.aspx" pageid="48">
                                                         <i class="fa fa-bars"></i>Organizations Holiday
                                                                    </asp:HyperLink>
                                                                </li>
                                                                <li>
                                                                    <asp:HyperLink ID="HyperLink19" Visible='<%# IsAccessible("attendance/LateEntrySkipDate.aspx") %>'
                                                                        Text="" runat="server" NavigateUrl="~/attendance/LateEntrySkipDate.aspx">
                                                         <i class="fa fa-bars"></i>Ignore Late In
                                                                    </asp:HyperLink>
                                                                </li>
                                                                <li>
                                                                    <asp:HyperLink ID="HyperLink25" Visible='<%# IsAccessible("cp/AttendanceDeviceMapping.aspx") %>'
                                                                        Text="" runat="server" NavigateUrl="~/cp/AttendanceDeviceMapping.aspx">
                                                         <i class="fa fa-bars"></i>Attendance Device Mapping
                                                                    </asp:HyperLink>
                                                                </li>
                                                            </ul>
                                                        </td>
                                                        <td>
                                                            <ul role="menu" class="dropdown-menu dropdown-demo-only custommenugap">
                                                                <li class="dashboardMenuHeader"><a>Team Management</a></li>
                                                                <li class="divider fulldivider" role="presentation"></li>

                                                                <li>
                                                                    <asp:HyperLink ID="HyperLink10" Visible='<%# IsAccessible("CP/Extension/ManageEmployeeProjects.aspx") %>'
                                                                        Text="" runat="server" NavigateUrl="~/CP/Extension/ManageEmployeeProjects.aspx"
                                                                        pageid="48">
                                                         <i class="fa fa-bars"></i>Team Employee
                                                                    </asp:HyperLink>
                                                                </li>
                                                                <li>
                                                                    <asp:HyperLink ID="HyperLink13" Visible='<%# IsAccessible("newhr/ManageTeams.aspx") %>'
                                                                        Text="" runat="server" NavigateUrl="~/newhr/ManageTeams.aspx" pageid="48">
                                                         <i class="fa fa-bars"></i>Leave
                                                                    Teams
                                                                    </asp:HyperLink>
                                                                </li>

                                                            </ul>
                                                        </td>
                                                        <td>
                                                            <ul role="menu" class="dropdown-menu dropdown-demo-only custommenugap">
                                                                <li class="dashboardMenuHeader"><a>Logs</a></li>
                                                                <li class="divider fulldivider" role="presentation"></li>
                                                                <li>
                                                                    <asp:HyperLink ID="HyperLink23" Visible='<%# IsAccessible("cp/AttendanceUploadStatus.aspx") %>'
                                                                        Text="" runat="server" NavigateUrl="~/cp/AttendanceUploadStatus.aspx">
                                                         <i class="fa fa-bars"></i>Attendance
                                                                Sync Status
                                                                    </asp:HyperLink>
                                                                </li>
                                                            </ul>
                                                        </td>
                                                    </tr>
                                                </table>




                                            </div>
                                        </div>
                                    </div>
                                    <div style="clear: both">
                                    </div>
                                    <%--Contract Expiring--%>
                                </td>
                                <td class="Column" style="width: 40%" runat="server" id="rightTD">
                                    <div class="panel panel-info-alt">
                                        <div class="panel-heading">
                                            <!-- panel-btns -->
                                            <h3 class="panel-title">Pending Leave Requests</h3>
                                            <a id="A14" href="~/newhr/LeaveRequestList.aspx" runat="server" class="details pull-right">view all</a>
                                        </div>
                                        <div class="panel-body panel-body-small">
                                            <ul class="nav nav-pills nav-stacked dashboardNotificatioin">
                                                <li><a id="A15" runat="server" href="~/newhr/LeaveRequestList.aspx?status=1"><span
                                                    runat="server" id="countRequested" class="badge badge-success pull-right">0</span>
                                                    Requested </a></li>
                                                <li><a id="A16" runat="server" href="~/newhr/LeaveRequestList.aspx?status=6">Recommended
                                                    <span class="badge badge-warning pull-right" runat="server" id="countRecommended">0</span>
                                                </a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="panel panel-info-alt" runat="server" id="blockTimeAttendanceRequests">
                                        <div class="panel-heading">
                                            <!-- panel-btns -->
                                            <h3 class="panel-title">Time Attendance Requests</h3>
                                            <%-- <a id="A10" href="~/newhr/AttRequestsApproval.aspx" runat="server" class="details pull-right">
                                                view all</a>--%>
                                        </div>
                                        <div class="panel-body panel-body-small">
                                            <ul class="nav nav-pills nav-stacked dashboardNotificatioin">
                                                <li><a id="A11" runat="server" href="~/newhr/AttRequestsApproval.aspx"><span runat="server"
                                                    id="spanTimeAttReq" class="badge badge-success pull-right">0</span> Requested </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="panel panel-info-alt">
                                        <div class="panel-heading">
                                            <!-- panel-btns -->
                                            <h3 class="panel-title">Late Attendance Comment Requests</h3>
                                        </div>
                                        <div class="panel-body panel-body-small">
                                            <ul class="nav nav-pills nav-stacked dashboardNotificatioin">
                                                <li><a id="A10" runat="server" href="~/newhr/AttEmpCommentsRequestsApproval.aspx"><span
                                                    runat="server" id="spanTimeAttCommentReq" class="badge badge-success pull-right">0</span> Requested </a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        Ext.onReady(
            function () {
                Ext.defer(function () {
                    document.getElementById('collapseOne2').className = 'panel-collapse collapse';
                }, 50);

            }
        );
        
    </script>
</asp:Content>
