$(function () {
    // main menu -> submenus
    $('#menu .collapse').on('show', function () {
        $(this).parents('.hasSubmenu:first').addClass('active');
    })
	.on('hidden', function () {
	    $(this).parents('.hasSubmenu:first').removeClass('active');
	});

    // main menu visibility toggle
    $('.navbar.main .btn-navbar').click(function () {
        $('.container-fluid:first').toggleClass('menu-hidden');
        $('#menu').toggleClass('hidden-phone');

        if (typeof masonryGallery != 'undefined')
            masonryGallery();
    });



});

//stop enter press form submit in input controls
$(function () {
    var keyStop = {
        8: ":not(input:text, textarea, input:file, input:password)", // stop backspace = back
        13: "input:text, input:password", // stop enter = submit 

        end: null
    };
    $(document).bind("keydown", function (event) {
        var selector = keyStop[event.which];

        if (selector !== undefined && $(event.target).is(selector)) {
            event.preventDefault(); //stop event
        }
        return true;
    });
});
