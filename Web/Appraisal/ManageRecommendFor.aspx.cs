﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using BLL;
using DAL;
using BLL.Manager;

namespace Web.NewHR
{
    public partial class ManageRecommendFor : System.Web.UI.Page
    {

        static bool btnEditIsClicked = false;
        static bool isReadOnly = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!X.IsAjaxRequest)
            {
                Initialize();
            }
        }

        //private void ReadOnlyMode(bool isReadOnly)        
        //{
        //    if (isReadOnly)            
        //    {
        //        this.txtCompetencyName.ReadOnly = true;
        //        this.txtDescription.ReadOnly = true;
        //        this.cmbCategory.ReadOnly = true;
        //        this.cmbCore.ReadOnly = true;

        //        this.btnAddRow.Hide();
        //        this.btnAddPosition.Hide();

        //        X.Js.AddScript("readOnlyMode=true;");
        //    }
        //    else
        //    {
        //        this.txtCompetencyName.ReadOnly = false;
        //        this.txtDescription.ReadOnly = false;
        //        this.cmbCategory.ReadOnly = false;
        //        this.cmbCore.ReadOnly = false;

        //        this.btnAddRow.Show();
        //        this.btnAddPosition.Show();

        //        X.Js.AddScript("readOnlyMode=false;");
        //    }
        //}

        protected void btnCreateCompetency_Click(object sender, DirectEventArgs e)
        {
            btnEditIsClicked = false;

            //isReadOnly = false;
            //this.ReadOnlyMode(isReadOnly);

            ClearCompetencyFields();
           

            WindowCompetency.Show();
        }

        protected void btnMoveSelected_Click(object sender, DirectEventArgs e)
        {
 
        }

        protected void btnDeleteSelected_Click(object sender, DirectEventArgs e)
        {
 
        }

        protected void btnEditCompetency_Click(object sender, DirectEventArgs e)
        {
           


            btnEditIsClicked = true;

            int targetID = int.Parse(hiddenValue.Text.Trim());

            AppraisalRecommendFor entityCompetency = AppraisalManager.GetRecommendForByID(targetID);

            txtCompetencyName.Text = entityCompetency.Name;
           

            
            // Finally show the competency window
            WindowCompetency.Show();
        }

        protected void btnDeleteCompetency_Click(object sender, DirectEventArgs e)
        {
            int competencyID = int.Parse(hiddenValue.Text.Trim());

            Status status = AppraisalManager.DeleteRecommendFor(competencyID);

            if (status.IsSuccess)
            {
                LoadCompetencies();
                NewMessage.ShowNormalMessage("Recommend Deleted.");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        protected void btnCompetencySaveUpdate_Click(object sender, DirectEventArgs e)
        {
            if (txtCompetencyName.Text == "")
            {
                NewMessage.ShowWarningMessage("Name cannot be empty.");
                txtCompetencyName.Focus();
            }
           
            else
            {
                bool competencyAlreadyExists = false;
                if (!btnEditIsClicked)
                {
                    // Check if the competency is already in the database
                    List<string> competencies = AppraisalManager.GetAllRecommendNames();
                    string txtCompetencyTrimmed = txtCompetencyName.Text.ToString().Trim().ToLower();
                    foreach (string competency in competencies)
                    {
                        if (competency.Trim().ToLower() == txtCompetencyTrimmed)
                        {
                            competencyAlreadyExists = true;
                            break;
                        }
                    }
                }

                if (competencyAlreadyExists)
                {
                    NewMessage.ShowWarningMessage("The Recommend you entered already exist.\nEnter a different name.");
                    txtCompetencyName.Focus();
                }
                else
                {
                    // save to the database
                    SaveUpdateCompeteney(e);
                }
            }
        }

     
        public void Initialize()
        {
            LoadCompetencies();
          

           // LoadLevels();
        }

        public void LoadCompetencies()
        {
            GridCompetency.Store[0].DataSource = AppraisalManager.GetAllRecommendFor();
            GridCompetency.Store[0].DataBind();
        }

        public void SaveUpdateCompeteney(DirectEventArgs e)
        {
            // saving to the AppraisalCompetency table
            AppraisalRecommendFor entity = new AppraisalRecommendFor();

            bool isInsert = true;
            if (!string.IsNullOrEmpty(hiddenValue.Text))
            {
                isInsert = false;
                entity.RecommendForId = int.Parse(hiddenValue.Text.Trim());
            }

          

            entity.Name = txtCompetencyName.Text.Trim();



            Status status = AppraisalManager.InsertUpdateRecommend(entity, isInsert);

            if (status.IsSuccess)
            {
                WindowCompetency.Hide();
                LoadCompetencies();
                NewMessage.ShowNormalMessage("Recommend Saved.");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        public void ClearCompetencyFields()
        {
            hiddenValue.Text = "";

            txtCompetencyName.Text = "";
           
        }


    }
}
