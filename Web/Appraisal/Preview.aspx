﻿<%@ Page Title="Preview" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="Preview.aspx.cs" Inherits="Web.Appraisal.Preview" %>

<%@ Register Src="~/Controls/HRAppraisalFormWizard.ascx" TagPrefix="ucHRW" TagName="ucHRFormWizard" %>
<%@ Register Src="UserControls/CompetencyBlock.ascx" TagName="CompetencyBlock" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../css/ratingsrc/jquery.rateit.min.js" type="text/javascript"></script>
    <link href="../css/ratingsrc/rateit.css" rel="stylesheet" type="text/css" />
    <link href="../css/newcss.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">


       
        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Appraisal Report
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel" style="margin-top:0px !important; padding-top:0px !important;">
    <ext:Hidden runat="server" ID="hiddenValue" />
     <ucHRW:ucHRFormWizard Id="ucAppraisalWizard" runat="server" />
    <div class="separator bottom">
    </div>
    <div class="innerLR">

        <h4 class="heading" style="margin-left: 0px">
            Preview Form</h4>
        <div runat="server" id="mainTitle" class="AppraisalMainTitle">
        </div>
        <table style="margin-left: 0px; margin-top: 10px; margin-bottom: 0px;">
            <tr>
                <td>
                    <asp:Image ID="image" runat="server" ImageUrl="~/images/sample.jpg" Width="150px"
                        Height="150px" />
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
        </table>
        <div runat="server" id="blockIntroduction">
            <span class="ratingBlockTitleCls" runat="server" id="spanIntroductionName"></span>
            <div runat="server" class='ratingContainerBlockDescription' id="divIntroductionDescription">
            </div>
        </div>
        <div runat="server" id="blockObjective">
            <span class="ratingBlockTitleCls" runat="server" id="spanObjectiveName"></span>
            <div runat="server" class='ratingContainerBlockDescription' id="divObjectiveDescription">
            </div>
        </div>
        <uc1:CompetencyBlock Id="CompetencyBlock1" AppraisalFormMode="Saved" runat="server" />
        <div runat="server" id="blockComment">
            <span class="ratingBlockTitleCls" runat="server" id="spanCommentName"></span>
            <div runat="server" class='ratingContainerBlockDescription' id="divCommentDescription">
            </div>
        </div>
        <div runat="server" id="blockSignature">
            <span class="ratingBlockTitleCls" runat="server" id="spanSignatureName"></span>
            <div runat="server" class='ratingContainerBlockDescription' id="divSignatureDescription">
            </div>
        </div>
    </div>
    </div>
    <script type="text/javascript">
        $('.rateit-reset').prop('title', 'Clear');
    </script>
</asp:Content>
