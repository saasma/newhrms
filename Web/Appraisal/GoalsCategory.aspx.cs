﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using System.Xml;

namespace Web.Appraisal
{
    public partial class GoalsCategory : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
              
            }
        }

        public void Initialise()
        {
            LoadLevels();
        }
        
        private void LoadLevels()
        {
            GridLevels.GetStore().DataSource = AppraisalManager.getallGoalsCategory();
            GridLevels.GetStore().DataBind();

        }


        
        public void ClearLevelFields()
        {
            hiddenValue.Text = "";
            txtName.Text = "";
            

        }

      
        
        protected void btnAddLevel_Click(object sender, DirectEventArgs e)
        {
            

            ClearLevelFields();
            WindowLevel.Show();
        }


        protected void btnDeleteLevel_Click(object sender, DirectEventArgs e)
        {
            int goalCategory = int.Parse(hiddenValue.Text.Trim());
            Status status = AppraisalManager.DeleteGoalCategory(goalCategory);
            if (status.IsSuccess)
            {
                LoadLevels();
                NewMessage.ShowNormalMessage("Goal Category deleted.");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        

        protected void btnEditLevel_Click(object sender, DirectEventArgs e)
        {
            int levelId = int.Parse(hiddenValue.Text.Trim());
            AppraisalGoalsCategory entity = AppraisalManager.GetCategoryGoalById(levelId);
            WindowLevel.Show();
            txtName.Text = entity.Name;

        }

        protected void btnLevelSaveUpdate_Click(object sender, DirectEventArgs e)
        {

            
            Page.Validate("InsertUpdateLevel");
            if (Page.IsValid)
            {
                AppraisalGoalsCategory categoryInstance = new AppraisalGoalsCategory();
                categoryInstance.Name = txtName.Text;

                
                 if(!string.IsNullOrEmpty(hiddenValue.Text))
                 {
                     categoryInstance.CategoryID = int.Parse(hiddenValue.Text);
                 }

                 Status status = NewHRManager.InsertUpdateGoalCategory(categoryInstance);
                if (status.IsSuccess)
                {
                    WindowLevel.Hide();
                    LoadLevels();
                    NewMessage.ShowNormalMessage("Goal Category saved.");
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }
                
            }
             
             
        }





        
    }
}