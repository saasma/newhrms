﻿<%@ Page Title="Manage Comptency" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="ManageCompetancy.aspx.cs" Inherits="Web.NewHR.ManageCompetancy" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .x-tab-bar-default
        {
            background-color: inherit;
        }
    </style>
    <style type="text/css">
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
        #content
        {
            margin: inherit;
        }
    </style>
    <script type="text/javascript">
    
        var CommandHandler1 = function(command, record){
            <%= hiddenValue.ClientID %>.setValue(record.data.CategoryID);
                if(command=="Edit")
                {
                    <%= btnEditLevel.ClientID %>.fireEvent('click');
                }
                else
                {
                
                    <%= btnDeleteLevel.ClientID %>.fireEvent('click');
                }

             }
             

    var CommandHandlerMainGrid = function(command, record) {
        <%= hiddenValue.ClientID %>.setValue(record.data.CompetencyID);

        if(command=="Edit")
        {
            <%= btnEditCompetency.ClientID %>.fireEvent('click');
        }
        else
        {
            <%= btnDeleteCompetency.ClientID %>.fireEvent('click');
        }
    };

    var CommandHandlerTeaserGrid=function(command,record) {
        <%= hiddenValueForInnerGrids.ClientID %>.setValue(record.data.TeaserID);
        <%= btnEditTeaser.ClientID %>.fireEvent('click');
    };

    var CommandHandlerPositionGrid = function(command, record) {
        <%= hiddenValueForInnerGrids.ClientID %>.setValue(record.data.DesignationId);
        <%= btnEditPosition.ClientID %>.fireEvent('click');
    };
    </script>
    <script type="text/javascript">

    var readOnlyMode = false;   // to disable grid change in edit mode

    var TeaserStore = null;
    var GridTeaser = null;

    var gridColumn = new Object();
    gridColumn.Name = "Name";

    function addNewRow() {
        TeaserStore = <%=TeaserStore.ClientID %>;
        var newRow = new TeaserItem();
        newRow.data.Name = "";

        var rowIndex = TeaserStore.data.items.length;
        TeaserStore.insert(rowIndex, newRow);
    }

    var onDeleteCommand = function(column, command, record, recordIndex, cellIndex) {
        var store = this.grid.store;
        store.remove(record);
        };

    function cellSelectionChanged(e1, e2, e3, e4) {
//        var sel = e1.lastSelection;

//        if(sel != 'undefined' && isTabPressed()) {
//            var row = sel.row;
//            var column = sel.column;

//            TeaserStore = <%=TeaserStore.ClientID %>;
//            var storeItems = TeaserStore.data.items;
//            var rowCount = storeItems.length;

//            if(row==rowCount - 1 && column>=1) {
//                GridTeaser = <%= GridTeaser.ClientID %>
//                gridTeaser.view.blur();
//                btnAddRow.focus();
//                }
//            }
        }

    var afterEdit = function( e1, e2) {
        var teaserName = e2.record.data.Name;
        
        e2.record.data.Name = teaserName;
        e2.record.commit();
        selectCurrrentCell();
        };

    function selectCurrentCell() {
        GridTeaser = <%=GridTeaser.ClientID %>
        GridTeaser.getView().focus();
        var row = GridTeaser.getSelectionModel().getCurrentPosition().row;
        var column = GridTeaser.getSelectionModel().getCurrentPosition().column;
        GridTeaser.getSelectionModel().setCurrentPosition( { row: row, column:column });
    }

    function isTabPressed() {
        if(typeof(window.event)!='undefined')
            return window.event.keyCode == 9;
        return false;
        }

    var beforeEdit = function(e1, e) {
        if(readOnlyMode)
        {
            e.cancel = true;
            return;
        }
    };
    </script>
    <script type="text/javascript">

    // global variables
    var GridPosition = null;
    var StorePosition = null;

    var StoreCmbPosition = null;

    var positionRenderer = function(value, metaData, record, rowIndex, colIndex, store) 
    {
        var r = <%=StoreCmbPosition.ClientID %>.getById(value.toString().toLowerCase());
        if(Ext.isEmpty(r)) 
        {
            return "";
        }

        return r.data.Name;
    };

    function addNewPosition() {
        StorePosition = <%= StorePosition.ClientID %>;
        var newRow = new PositionItem();
        newRow.data.Name = "";

        var rowIndex = StorePosition.data.items.length;
        StorePosition.insert(rowIndex,newRow);
    }

    var onPositionDeleteCommand = function (column, command, record, recordIndex, cellIndex) {
        var store = this.grid.store;
        store.remove(record);
    };

    var afterEditPosition = function(e1, e2) {
        e2.record.commit();
        selectCurrentPositionCell();
    };

    function selectCurrentPositionCell() {
        GridPosition = <%= GridPosition.ClientID %>
        GridPosition.getView().focus();
        var row = GridPosition.getSelectionModel().getCurrentPosition().row;
        var column = GridPosition.getSelectionModel().getCurrentPosition().column;
        GridPosition.getSelectionModel().setCurrentPosition( { row: row, column: column});
    }

    var SetPositionType = function(value, rowIndex) {
        var currentRow = StorePosition.data.items[rowIndex];
        var row = StoreCmbPosition.getById(value.toString().toLowerCase());
        if(row != null && typeof(row) != 'undefined')
            currentRow.data.AssociateType = row.data.AssociateType.toString();

            currentRow.commit();
        };

    var positionCellSelectionChanged = function(e1, e2, e3, e4) {

    };

    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Manage Competancy
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel" style="margin-top: 0px !important; padding-top: 0px !important;">
        <ext:LinkButton runat="server" Hidden="true" ID="btnEditLevel">
            <DirectEvents>
                <Click OnEvent="btnEditLevel_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:LinkButton>
        <ext:Button ID="btnDeleteLevel" runat="server" Hidden="true">
            <DirectEvents>
                <Click OnEvent="btnDeleteLevel_Click">
                    <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the Goal Category?" />
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:Button>
        <ext:Hidden ID="hiddenValue" runat="server" />
        <ext:Hidden ID="hiddenValueForInnerGrids" runat="server" />
        <ext:LinkButton runat="server" ID="btnEditCompetency" Hidden="true">
            <DirectEvents>
                <Click OnEvent="btnEditCompetency_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:LinkButton>
        <ext:LinkButton runat="server" ID="btnDeleteCompetency" Hidden="true">
            <DirectEvents>
                <Click OnEvent="btnDeleteCompetency_Click">
                    <Confirmation ConfirmRequest="true" Message="Confirm for deletion?" />
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:LinkButton>
        <ext:LinkButton ID="btnEditTeaser" runat="server" Hidden="true">
            <DirectEvents>
                <Click OnEvent="btnEditTeaser_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:LinkButton>
        <ext:LinkButton ID="btnEditPosition" runat="server" Hidden="true">
            <DirectEvents>
                <Click OnEvent="btnEditPosition_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:LinkButton>
        <div class="separator bottom">
        </div>
        <div class="innerLR">
            <h4 class="heading">
                Competency Category</h4>
            <ext:Button runat="server" ID="btnAddLevel" Cls="btn btn-primary" Text="<i></i>Add New Category">
                <DirectEvents>
                    <Click OnEvent="btnAddLevel_Click">
                        <EventMask ShowMask="true" />
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:GridPanel StyleSpec="margin-top:15px;" ID="GridLevels" runat="server" Cls="itemgrid">
                <Store>
                    <ext:Store ID="Store3" runat="server">
                        <Model>
                            <ext:Model ID="Model4" runat="server" IDProperty="CategoryID">
                                <Fields>
                                    <ext:ModelField Name="CategoryID" Type="String" />
                                    <ext:ModelField Name="Name" Type="string" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:Column ID="Column5" Sortable="false" MenuDisabled="true" runat="server" Text="Name"
                            Width="300" Align="Left" DataIndex="Name" />
                        <ext:CommandColumn ID="CommandColumn1" runat="server" Text="Actions" Align="Center"
                            Width="63">
                            <Commands>
                                <ext:GridCommand Icon="NoteEdit" CommandName="Edit" />
                                <ext:GridCommand Icon="Delete" CommandName="Delete" />
                            </Commands>
                            <Listeners>
                                <Command Handler="CommandHandler1(command,record);" />
                            </Listeners>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
                <SelectionModel>
                    <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                </SelectionModel>
            </ext:GridPanel>
            <ext:Window ID="WindowLevel" runat="server" Title="Add/Edit Goal Category" Icon="Application"
                Width="450" Height="190" BodyPadding="5" Hidden="true" Modal="true">
                <Content>
                    <table class="fieldTable">
                        <tr>
                            <td>
                                <ext:TextField ID="txtName" LabelSeparator="" runat="server" FieldLabel="Name *"
                                    Width="400" LabelAlign="Top">
                                </ext:TextField>
                                <asp:RequiredFieldValidator Display="None" ID="valcmbFaculty" runat="server" ValidationGroup="SaveUpdateLevel"
                                    ControlToValidate="txtName" ErrorMessage="Name is required." />
                            </td>
                        </tr>
                        <tr>
                            <td valign="bottom" colspan="2">
                                <div class="popupButtonDiv">
                                    <ext:Button runat="server" ID="btnLevelSaveUpdate" Cls="btn btn-primary" Text="<i></i>Save">
                                        <DirectEvents>
                                            <Click OnEvent="btnLevelSaveUpdate_Click">
                                                <EventMask ShowMask="true" />
                                            </Click>
                                        </DirectEvents>
                                        <Listeners>
                                            <Click Handler="valGroup = 'SaveUpdateLevel'; return CheckValidation();">
                                            </Click>
                                        </Listeners>
                                    </ext:Button>
                                    <div class="btnFlatOr">
                                        or</div>
                                    <ext:LinkButton runat="server" StyleSpec="padding:0px;" Cls="btnFlatLeftGap" ID="LinkButton1"
                                        Text="<i></i>Cancel" runat="server">
                                        <Listeners>
                                            <Click Handler="#{WindowLevel}.hide();">
                                            </Click>
                                        </Listeners>
                                    </ext:LinkButton>
                                </div>
                            </td>
                        </tr>
                    </table>
                </Content>
            </ext:Window>
            <h4 class="heading" style="margin-top: 20px">
                Competency List</h4>
            <ext:Store ID="StoreCmbPosition" runat="server">
                <Model>
                    <ext:Model ID="Model3" Name="ModelCmbPosition" IDProperty="DesignationId" runat="server">
                        <Fields>
                            <ext:ModelField Name="DesignationId" Type="String" />
                            <ext:ModelField Name="Name" Type="String" />
                        </Fields>
                    </ext:Model>
                </Model>
            </ext:Store>
            <table width="700">
                <tr>
                    <td align="left">
                        <ext:Button runat="server" Text="New Competency" Cls="btn btn-primary">
                            <DirectEvents>
                                <Click OnEvent="btnCreateCompetency_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                    </td>
                    <td align="left" style="padding-left: 504px">
                        
                    </td>
                </tr>
            </table>
            <ext:GridPanel ID="GridCompetency" runat="server" StyleSpec="margin-top:15px;" Width="1100"
                Cls="itemgrid">
                <Store>
                    <ext:Store runat="server">
                        <Model>
                            <ext:Model runat="server" ID="Model1" IDProperty="CompetencyID">
                                <Fields>
                                    <ext:ModelField Name="CompetencyID" Type="String" />
                                    <ext:ModelField Name="Name" Type="String" />
                                    <ext:ModelField Name="CategoryName" Type="String" />
                                    <ext:ModelField Name="IsCore" Type="Boolean" />
                                    <ext:ModelField Name="Order" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:Column ID="ColumnName" Sortable="false" MenuDisabled="true" runat="server" Text="Name"
                            Align="Left" DataIndex="Name" Flex="1" />
                        <ext:Column ID="ColumnCategory" Sortable="false" MenuDisabled="true" runat="server"
                            Text="Category"  Width="200" Align="Left" DataIndex="CategoryName"  />
                        <%--<ext:Column ID="ColumnIsCore" Sortable="false" MenuDisabled="true" runat="server" Text=" Is Core" Align="Center" DataIndex="IsCore" Flex="1" />--%>
                        <ext:CheckColumn ID="ColumnCore" Sortable="false" MenuDisabled="true" runat="server"
                            Align="Left" DataIndex="IsCore"  Width="100" Text="Competency Core Type" />
                        <ext:CommandColumn runat="server" Width="60" Align="Left">
                            <Commands>
                                <ext:GridCommand Icon="NoteEdit" CommandName="Edit" ToolTip-Text="Edit">
                                </ext:GridCommand>
                                <ext:GridCommand Icon="Delete" CommandName="Delete" ToolTip-Text="Delete">
                                </ext:GridCommand>
                            </Commands>
                            <Listeners>
                                <Command Handler="CommandHandlerMainGrid(command, record);" />
                            </Listeners>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
                <SelectionModel>
                    <ext:CheckboxSelectionModel runat="server" Mode="Multi" />
                </SelectionModel>
            </ext:GridPanel>
            <ext:Window ID="WindowCompetency" runat="server" Title="Create/Add Competency" Width="550"
                Height="400" Hidden="true" Modal="true" Closable="true" DefaultAnchor="100%"
                AutoScroll="true">
                <Content>
                    <table class="fieldTable">
                        <tr>
                            <td colspan="2">
                                <ext:TextField ID="txtCompetencyName" Width="450" runat="server" FieldLabel="Competency Name"
                                    LabelAlign="Top" LabelSeparator="">
                                </ext:TextField>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <ext:TextArea ID="txtDescription" Width="450" runat="server" FieldLabel="Description"
                                    LabelAlign="Top" LabelSeparator="">
                                </ext:TextArea>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <ext:ComboBox ID="cmbCategory" Width="300" runat="server" FieldLabel="Category" LabelAlign="Top"
                                    LabelSeparator="" ValueField="CategoryID" DisplayField="Name" ForceSelection="true"
                                    QueryMode="Local" EmptyText="Select a Category">
                                    <Store>
                                        <ext:Store ID="Store2" runat="server">
                                            <Model>
                                                <ext:Model runat="server" ID="Model2">
                                                    <Fields>
                                                        <ext:ModelField Name="CategoryID" Type="String" />
                                                        <ext:ModelField Name="Name" />
                                                    </Fields>
                                                </ext:Model>
                                            </Model>
                                        </ext:Store>
                                    </Store>
                                </ext:ComboBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <ext:ComboBox ID="cmbCore" Width="300" runat="server" FieldLabel="Core Competency"
                                    LabelAlign="Top" LabelSeparator="" EmptyText="Select Core" ForceSelection="true">
                                    <SelectedItems>
                                        <ext:ListItem Text="Yes" Value="True">
                                        </ext:ListItem>
                                    </SelectedItems>
                                    <Items>
                                        <ext:ListItem Text="Yes" Value="True">
                                        </ext:ListItem>
                                    </Items>
                                </ext:ComboBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <ext:TabPanel ID="TabPanel1" runat="server" Width="500" Border="false" Header="false"
                                    Hidden="true">
                                    <Items>
                                        <ext:GridPanel ID="GridTeaser" runat="server" Border="false" Title="Performance Details">
                                            <Store>
                                                <ext:Store ID="TeaserStore" runat="server">
                                                    <Model>
                                                        <ext:Model runat="server" ID="ModelTeaser" Name="TeaserItem">
                                                            <Fields>
                                                                <ext:ModelField Name="TeaserID" Type="Int" />
                                                                <ext:ModelField Name="Name" Type="String" />
                                                            </Fields>
                                                        </ext:Model>
                                                    </Model>
                                                </ext:Store>
                                            </Store>
                                            <ColumnModel>
                                                <Columns>
                                                    <ext:Column runat="server" ID="ColumnTeaserName" Flex="1" DataIndex="Name" Sortable="false"
                                                        MenuDisabled="true" Text="Teaser">
                                                        <Editor>
                                                            <ext:TextField SelectOnFocus="true" ID="txtTeaserName" StyleSpec="text-align:right"
                                                                runat="server">
                                                            </ext:TextField>
                                                        </Editor>
                                                    </ext:Column>
                                                    <ext:CommandColumn runat="server" Width="22" MenuDisabled="true" Sortable="false">
                                                        <Commands>
                                                            <ext:GridCommand Icon="Delete" CommandName="Delete" ToolTip-Text="Delete">
                                                            </ext:GridCommand>
                                                        </Commands>
                                                        <Listeners>
                                                            <Command Fn="onDeleteCommand">
                                                            </Command>
                                                        </Listeners>
                                                    </ext:CommandColumn>
                                                    <ext:CommandColumn Width="22" runat="server" MenuDisabled="true" Sortable="false"
                                                        Resizable="false">
                                                        <Commands>
                                                            <ext:GridCommand Icon="NoteEdit" CommandName="Edit" ToolTip-Text="Edit">
                                                            </ext:GridCommand>
                                                        </Commands>
                                                        <Listeners>
                                                            <Command Handler="CommandHandlerTeaserGrid(command,record);" />
                                                        </Listeners>
                                                    </ext:CommandColumn>
                                                    <%--<ext:ImageCommandColumn ID="CommandColumn1" runat="server" Width="22"
                                                    Sortable="false" MenuDisabled="true">
                                                    <Commands>
                                                        <ext:ImageCommand Cls="icon-delete-new" CommandName="Delete" Icon="Delete">
                                                        </ext:ImageCommand>
                                                    </Commands>
                                                    <Listeners>
                                                        <Command Fn="onDeleteCommand" />
                                                    </Listeners>
                                                </ext:ImageCommandColumn>--%>
                                                </Columns>
                                            </ColumnModel>
                                            <Plugins>
                                                <ext:CellEditing ID="CellEditing1" runat="server" ClicksToEdit="1">
                                                    <Listeners>
                                                        <Edit Fn="afterEdit" />
                                                    </Listeners>
                                                </ext:CellEditing>
                                            </Plugins>
                                            <View>
                                                <ext:GridView ID="GridView_GridTeaser" runat="server" TrackOver="false">
                                                </ext:GridView>
                                            </View>
                                            <Listeners>
                                                <BeforeEdit Fn="beforeEdit" />
                                            </Listeners>
                                            <SelectionModel>
                                                <ext:CellSelectionModel runat="server" ID="gridTeaserSelectionModel">
                                                    <Listeners>
                                                        <SelectionChange Fn="cellSelectionChanged" />
                                                    </Listeners>
                                                </ext:CellSelectionModel>
                                            </SelectionModel>
                                            <BottomBar>
                                                <ext:StatusBar runat="server">
                                                    <Items>
                                                        <ext:Button runat="server" ID="btnAddRow" Text="Add Teaser" Icon="Add">
                                                            <Listeners>
                                                                <Click Handler="addNewRow();" />
                                                            </Listeners>
                                                        </ext:Button>
                                                    </Items>
                                                </ext:StatusBar>
                                            </BottomBar>
                                        </ext:GridPanel>
                                        <ext:GridPanel ID="GridPosition" runat="server" Border="false" Title="Assigned To Job Positions"
                                            Resizable="false">
                                            <Store>
                                                <ext:Store ID="StorePosition" runat="server">
                                                    <Model>
                                                        <ext:Model ID="ModelPosition" runat="server" Name="PositionItem">
                                                            <Fields>
                                                                <ext:ModelField Name="DesignationId" Type="Int" />
                                                                <ext:ModelField Name="Name" Type="String" />
                                                            </Fields>
                                                        </ext:Model>
                                                    </Model>
                                                </ext:Store>
                                            </Store>
                                            <ColumnModel>
                                                <Columns>
                                                    <ext:Column ID="ColumnPosition" DataIndex="DesignationId" runat="server" Resizable="false"
                                                        MenuDisabled="true" Sortable="false" Flex="1" Text="Position">
                                                        <Renderer Fn="positionRenderer" />
                                                        <Editor>
                                                            <ext:ComboBox ID="cmbPosition" StoreID="StoreCmbPosition" runat="server" DisplayField="Name"
                                                                ValueField="DesignationId" QueryMode="Local" ForceSelection="true">
                                                                <Listeners>
                                                                    <Select Handler="SetPositionType(this.getValue(), #{GridPosition}.getSelectionModel().getCurrentPosition().row);" />
                                                                    <BeforeSelect Handler="return record.data.inEmpty != true; ">
                                                                    </BeforeSelect>
                                                                </Listeners>
                                                            </ext:ComboBox>
                                                        </Editor>
                                                    </ext:Column>
                                                    <ext:CommandColumn runat="server" Width="22" Align="Center">
                                                        <Commands>
                                                            <ext:GridCommand Icon="Delete" CommandName="Delete">
                                                            </ext:GridCommand>
                                                        </Commands>
                                                        <Listeners>
                                                            <Command Fn="onPositionDeleteCommand">
                                                            </Command>
                                                        </Listeners>
                                                    </ext:CommandColumn>
                                                    <ext:CommandColumn runat="server" Width="22" Align="Center" Resizable="false" Sortable="false"
                                                        MenuDisabled="true">
                                                        <Commands>
                                                            <ext:GridCommand Icon="NoteEdit" CommandName="Edit" ToolTip-Text="Edit">
                                                            </ext:GridCommand>
                                                        </Commands>
                                                        <Listeners>
                                                            <Command Handler="CommandHandlerPositionGrid(command,record);" />
                                                        </Listeners>
                                                    </ext:CommandColumn>
                                                </Columns>
                                            </ColumnModel>
                                            <Listeners>
                                                <BeforeEdit Fn="beforeEdit" />
                                            </Listeners>
                                            <Plugins>
                                                <ext:CellEditing ID="CellEditing2" runat="server" ClicksToEdit="1">
                                                    <Listeners>
                                                        <Edit Fn="afterEditPosition" />
                                                    </Listeners>
                                                </ext:CellEditing>
                                            </Plugins>
                                            <SelectionModel>
                                                <ext:CellSelectionModel ID="gridPositionSelectionModel" runat="server">
                                                    <Listeners>
                                                        <SelectionChange Fn="positionCellSelectionChanged" />
                                                    </Listeners>
                                                </ext:CellSelectionModel>
                                            </SelectionModel>
                                            <BottomBar>
                                                <ext:StatusBar runat="server">
                                                    <Items>
                                                        <ext:Button ID="btnAddPosition" runat="server" Text="Add Position" Icon="Add">
                                                            <Listeners>
                                                                <Click Handler="addNewPosition();" />
                                                            </Listeners>
                                                        </ext:Button>
                                                    </Items>
                                                </ext:StatusBar>
                                            </BottomBar>
                                        </ext:GridPanel>
                                    </Items>
                                </ext:TabPanel>
                            </td>
                        </tr>
                        <tr>
                            <td valign="bottom" colspan="2">
                                <div class="popupButtonDiv">
                                    <ext:Button runat="server" ID="btnSaveCompetency" Cls="btn btn-primary" Text="<i></i>Save">
                                        <DirectEvents>
                                            <Click OnEvent="btnCompetencySaveUpdate_Click">
                                                <ExtraParams>
                                                    <ext:Parameter Name="TeaserExtraParam" Value="Ext.encode(#{GridTeaser}.getRowsValues({selectedOnly : false}))"
                                                        Mode="Raw" />
                                                    <ext:Parameter Name="PositionExtraParam" Value="Ext.encode(#{GridPosition}.getRowsValues({selectedOnly:false}))"
                                                        Mode="Raw" />
                                                </ExtraParams>
                                                <EventMask ShowMask="true" />
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <div class="btnFlatOr">
                                        or</div>
                                    <ext:LinkButton runat="server" ID="btnCancel" StyleSpec="padding:0px;" Cls="btnFlatLeftGap"
                                        Text="<i></i>Cancel">
                                        <Listeners>
                                            <Click Handler="#{WindowCompetency}.hide();">
                                            </Click>
                                        </Listeners>
                                    </ext:LinkButton>
                                </div>
                            </td>
                        </tr>
                    </table>
                </Content>
            </ext:Window>
            <ext:Window ID="WindowTeaser" runat="server" Width="300" Height="150" Title="Teaser"
                Closable="true" Resizable="false" Hidden="true">
                <Content>
                    <table style="width: 300px; height: 100px;">
                        <tr>
                            <td colspan="2">
                                <ext:TextField ID="txtWinTeaserName" runat="server" FieldLabel="Teaser Name" LabelSeparator="">
                                </ext:TextField>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" valign="bottom">
                                <ext:Button ID="btnUpdateTeaser" runat="server" Text="Update" Icon="Disk">
                                    <DirectEvents>
                                        <Click OnEvent="btnUpdateTeaser_Click">
                                            <EventMask ShowMask="true" />
                                        </Click>
                                    </DirectEvents>
                                </ext:Button>
                            </td>
                            <td align="center" valign="bottom">
                                <ext:Button ID="btnCancelTeaser" runat="server" Text="Cancel" Icon="Cancel">
                                    <Listeners>
                                        <Click Handler="#{WindowTeaser}.hide();" />
                                    </Listeners>
                                </ext:Button>
                            </td>
                        </tr>
                    </table>
                </Content>
            </ext:Window>
            <ext:Window ID="WindowPosition" runat="server" Width="300" Height="150" Hidden="true"
                Closable="true" Resizable="false">
                <Content>
                    <table style="width: 300px; height: 100px;">
                        <tr>
                            <td colspan="2">
                                <ext:ComboBox ID="cmbWinPosition" runat="server" FieldLabel="Position" LabelSeparator=""
                                    DataIndex="DesignationId" ValueField="DesignationId" DisplayField="Name" QueryMode="Local"
                                    ForceSelection="true">
                                    <Store>
                                        <ext:Store runat="server">
                                            <Model>
                                                <ext:Model runat="server">
                                                    <Fields>
                                                        <ext:ModelField Name="DesignationId" />
                                                        <ext:ModelField Name="Name" />
                                                    </Fields>
                                                </ext:Model>
                                            </Model>
                                        </ext:Store>
                                    </Store>
                                </ext:ComboBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" valign="bottom">
                                <ext:Button ID="btnUpadtePosition" Text="Update" Icon="Disk" runat="server" runat="server">
                                    <DirectEvents>
                                        <Click OnEvent="btnUpdatePosition_Click">
                                            <EventMask ShowMask="true" />
                                        </Click>
                                    </DirectEvents>
                                </ext:Button>
                            </td>
                            <td align="center" valign="bottom">
                                <ext:Button ID="btnCancelPosition" runat="server" Text="Cancel" Icon="Cancel">
                                    <Listeners>
                                        <Click Handler="#{WindowPosition}.hide();" />
                                    </Listeners>
                                </ext:Button>
                            </td>
                        </tr>
                    </table>
                </Content>
            </ext:Window>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
