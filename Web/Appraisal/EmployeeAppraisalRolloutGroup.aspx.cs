﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using BLL;
using DAL;
using BLL.Manager;
using Utils.Helper;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Text;
using Utils;
using System.Xml.Linq;
using Utils.Calendar;
using BLL.Entity;
using BLL.BO;

namespace Web.NewHR
{
    public partial class EmployeeAppraisalRolloutGroup : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialize();
            }
            JavascriptHelper.AttachNonDialogPopUpCode(Page, "shiftPopup", "../ExcelWindow/RolloutGroupImportExcel.aspx", 450, 500);
        }
        public void Initialize()
        {
            storeGroup.DataSource = AppraisalManager.GetAllRolloutGroup();
            storeGroup.DataBind();
        }

        protected void btnEditGroup_Click(object sender, DirectEventArgs e)
        {
            clearPopup();
            if (hiddenValueGroupId.Text.Trim() != null)
                cmbEditGroupName.SetValue(hiddenValueGroupId.Text.Trim().ToString());
            txtEmpName.Text = hiddenValueEmpName.Text.Trim();
            WindowEmpRolloutGroup.Show();
        }

        protected void clearPopup()
        {
            txtEmpName.Text = "";
        }
        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            if (txtEmpName.Text == "")
            {
                NewMessage.ShowWarningMessage("Employee name cannot be empty.");
                txtEmpName.Focus();
                return;
            }
            AppraisalRolloutGroupEmployee _dbobj = new AppraisalRolloutGroupEmployee();
            if (!string.IsNullOrEmpty(hiddenValueEmpId.Text))
            {
                _dbobj.EmployeeID = int.Parse(hiddenValueEmpId.Text.Trim());
            }

            if (cmbEditGroupName.SelectedItem.Value == null)
                _dbobj.RolloutGroupRef_ID = int.Parse(hiddenValueGroupId.Text.Trim());
            else
                _dbobj.RolloutGroupRef_ID = int.Parse(cmbEditGroupName.SelectedItem.Value);

            Status status = AppraisalManager.UpdateEmpRolloutGroup(_dbobj);
            if (status.IsSuccess == true)
            {
                NewMessage.ShowNormalMessage("Sucessfully updated !");
                WindowEmpRolloutGroup.Hide();
                PagingToolbar1.DoRefresh();
                return;
            }
            else
            {
                NewMessage.ShowWarningMessage("Unable to edit !");
                return;
            }


        }
        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            int total = 0, pageSize = 0, employeeId = -1, GroupId = -1;
            string employeeName = cmbEmpSearch.Text;

            if (cmbEmpSearch.SelectedItem != null && cmbEmpSearch.SelectedItem.Value != null)
            {
                int.TryParse(cmbEmpSearch.SelectedItem.Value, out employeeId);
                if (employeeId != 0)
                    employeeName = "";
                else
                    employeeId = -1;
            }

            if (cmbRolloutGroup.SelectedItem != null && cmbRolloutGroup.SelectedItem.Value != null)
                GroupId = int.Parse(cmbRolloutGroup.SelectedItem.Value);

            pageSize = int.Parse(cmbPageSize.SelectedItem.Value);

            List<GetEmployeeAppraisalRolloutGroupListResult> list = AppraisalManager.GetEmployeeAppraisalRolloutGroup(e.Start / pageSize, pageSize, employeeId, employeeName, GroupId);

            gridEmpGroupsStore.DataSource = list;
            gridEmpGroupsStore.DataBind();

            if (list.Count > 0)
                total = list[0].TotalRows.Value;

            e.Total = total;
        }

    }
}
