﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using BLL;
using DAL;
using BLL.Manager;
using Utils.Calendar;

namespace Web.NewHR
{
    public partial class ManagePeriod : System.Web.UI.Page
    {

        static bool btnEditIsClicked = false;
        static bool isReadOnly = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!X.IsAjaxRequest)
            {
                Initialize();
            }
        }

        //private void ReadOnlyMode(bool isReadOnly)        
        //{
        //    if (isReadOnly)            
        //    {
        //        this.txtCompetencyName.ReadOnly = true;
        //        this.txtDescription.ReadOnly = true;
        //        this.cmbCategory.ReadOnly = true;
        //        this.cmbCore.ReadOnly = true;

        //        this.btnAddRow.Hide();
        //        this.btnAddPosition.Hide();

        //        X.Js.AddScript("readOnlyMode=true;");
        //    }
        //    else
        //    {
        //        this.txtCompetencyName.ReadOnly = false;
        //        this.txtDescription.ReadOnly = false;
        //        this.cmbCategory.ReadOnly = false;
        //        this.cmbCore.ReadOnly = false;

        //        this.btnAddRow.Show();
        //        this.btnAddPosition.Show();

        //        X.Js.AddScript("readOnlyMode=false;");
        //    }
        //}

        protected void btnCreateCompetency_Click(object sender, DirectEventArgs e)
        {
            btnEditIsClicked = false;

            //isReadOnly = false;
            //this.ReadOnlyMode(isReadOnly);

            ClearCompetencyFields();
           

            WindowCompetency.Show();
        }

        protected void btnMoveSelected_Click(object sender, DirectEventArgs e)
        {
 
        }

        protected void btnDeleteSelected_Click(object sender, DirectEventArgs e)
        {
 
        }

        protected void btnEditCompetency_Click(object sender, DirectEventArgs e)
        {
           


            btnEditIsClicked = true;

            int targetID = int.Parse(hiddenValue.Text.Trim());

            AppraisalPeriod entityCompetency = AppraisalManager.GetPeriodByID(targetID);

            txtName.Text = entityCompetency.Name;
            txtStartDate.SelectedDate = entityCompetency.StartDate.Value;
            txtEndDate.SelectedDate = entityCompetency.EndDate.Value;
            txtDueDate.SelectedDate = entityCompetency.DueDate.Value;

            cmbStatus.SetValue("0");
            if (entityCompetency.Closed != null)
            {
                if (entityCompetency.Closed.Value)
                    cmbStatus.SetValue("1");
            }

            // Finally show the competency window
            WindowCompetency.Show();
        }

        protected void btnDeleteCompetency_Click(object sender, DirectEventArgs e)
        {
            int competencyID = int.Parse(hiddenValue.Text.Trim());

            Status status = AppraisalManager.DeleteTarget(competencyID);

            if (status.IsSuccess)
            {
                LoadCompetencies();
                NewMessage.ShowNormalMessage("Target Deleted.");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        protected void btnCompetencySaveUpdate_Click(object sender, DirectEventArgs e)
        {
          
            {
              
                {
                    // save to the database
                    SaveUpdateCompeteney(e);
                }
            }
        }

     
        public void Initialize()
        {
            LoadCompetencies();

          

           // LoadLevels();
        }

        public void LoadCompetencies()
        {
            List<AppraisalPeriod> list = AppraisalManager.GetAllPeriods();

            CustomDate date = null;
            foreach (AppraisalPeriod item in list)
            {
                date = new CustomDate(item.StartDate.Value.Day, item.StartDate.Value.Month,
                    item.StartDate.Value.Year, true);
                date = CustomDate.ConvertEngToNep(date);
                item.StartNepDate = DateHelper.GetMonthName(date.Month, false);
                item.Start = item.StartDate.Value.ToString("yyyy-MMM-dd") + " (" + date.ToStringShortMonthName() + ")";

                date = new CustomDate(item.EndDate.Value.Day, item.EndDate.Value.Month,
                   item.EndDate.Value.Year, true);
                date = CustomDate.ConvertEngToNep(date);
                item.EndNepDate = DateHelper.GetMonthName(date.Month, false);
                item.End = item.EndDate.Value.ToString("yyyy-MMM-dd") + " (" + date.ToStringShortMonthName() + ")";

                if (item.Closed != null && item.Closed.Value)
                    item.StatusName = "Closed";
                else
                    item.StatusName = "Open";
            }

            GridCompetency.Store[0].DataSource = list;
            GridCompetency.Store[0].DataBind();
        }

        public void SaveUpdateCompeteney(DirectEventArgs e)
        {
            // saving to the AppraisalCompetency table
            AppraisalPeriod entity = new AppraisalPeriod();

            bool isInsert = true;
            if (!string.IsNullOrEmpty(hiddenValue.Text))
            {
                isInsert = false;
                entity.PeriodId = int.Parse(hiddenValue.Text.Trim());
            }

          

            entity.Name = txtName.Text.Trim();
            entity.StartDate = txtStartDate.SelectedDate;
            entity.EndDate = txtEndDate.SelectedDate;
            entity.DueDate = txtDueDate.SelectedDate;

            if (cmbStatus.SelectedItem.Value == "1")
                entity.Closed = true;
            else
                entity.Closed = false;

            Status status = AppraisalManager.InsertUpdatePeroid(entity, isInsert);

            if (status.IsSuccess)
            {
                WindowCompetency.Hide();
                LoadCompetencies();
                NewMessage.ShowNormalMessage("Peirod Saved.");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        public void ClearCompetencyFields()
        {
            hiddenValue.Text = "";

            txtName.Text = "";
            cmbStatus.SetValue("0");
           
        }


    }
}
