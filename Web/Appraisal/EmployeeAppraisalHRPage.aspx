﻿<%@ Page Title="Employee Appraisal" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="EmployeeAppraisalHRPage.aspx.cs" Inherits="Web.Appraisal.EmployeeAppraisalHRPage" %>

<%@ Register Src="~/Employee/Appraisal/ReviewAllCtl.ascx" TagName="review" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../css/ratingsrc/jquery.rateit.min.js" type="text/javascript"></script>
    <link href="../css/ratingsrc/rateit.css" rel="stylesheet" type="text/css" />
    <link href="../css/newcss.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="contentpanel">
        <uc2:review runat="server" id="ctl1" />
    </div>
</asp:Content>
