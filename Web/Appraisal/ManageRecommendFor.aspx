﻿<%@ Page Title="Manage Recomendation" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="ManageRecommendFor.aspx.cs" Inherits="Web.NewHR.ManageRecommendFor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .x-tab-bar-default
        {
            background-color: inherit;
        }
    </style>
    <style type="text/css">
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
        #content
        {
            margin: inherit;
        }
    </style>
    <script type="text/javascript">
    
      

    var CommandHandlerMainGrid = function(command, record) {
        <%= hiddenValue.ClientID %>.setValue(record.data.RecommendForId);

        if(command=="Edit")
        {
            <%= btnEditCompetency.ClientID %>.fireEvent('click');
        }
        else
        {
            <%= btnDeleteCompetency.ClientID %>.fireEvent('click');
        }
    };

 
    </script>
    <script type="text/javascript">

   
    </script>
    <script type="text/javascript">

   

    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Manage Recommendation
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel" style="margin-top: 0px !important; padding-top: 0px !important;">
        <ext:Hidden ID="hiddenValue" runat="server" />
        <ext:Hidden ID="hiddenValueForInnerGrids" runat="server" />
        <ext:LinkButton runat="server" ID="btnEditCompetency" Hidden="true">
            <DirectEvents>
                <Click OnEvent="btnEditCompetency_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:LinkButton>
        <ext:LinkButton runat="server" ID="btnDeleteCompetency" Hidden="true">
            <DirectEvents>
                <Click OnEvent="btnDeleteCompetency_Click">
                    <Confirmation ConfirmRequest="true" Message="Confirm for deletion?" />
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:LinkButton>
        <div class="separator bottom">
        </div>
        <div class="innerLR">
           
            <table width="700">
                <tr>
                    <td align="left" style="padding-top:10px">
                        <ext:Button runat="server" Text="New Recommend" Cls="btn btn-primary">
                            <DirectEvents>
                                <Click OnEvent="btnCreateCompetency_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                    </td>
                    <td align="left" style="padding-left: 504px">
                        
                    </td>
                </tr>
            </table>
            <ext:GridPanel ID="GridCompetency" runat="server" StyleSpec="margin-top:15px;" Width="700"
                Cls="itemgrid">
                <Store>
                    <ext:Store runat="server">
                        <Model>
                            <ext:Model runat="server" ID="Model1" IDProperty="RecommendForId">
                                <Fields>
                                    <ext:ModelField Name="RecommendForId" Type="String" />
                                    <ext:ModelField Name="Name" Type="String" />
                                    <ext:ModelField Name="Order" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:Column ID="ColumnName" Sortable="false" MenuDisabled="true" runat="server" Text="Name"
                            Align="Left" DataIndex="Name" Flex="1" />
                        <ext:CommandColumn runat="server" Width="60" Align="Left">
                            <Commands>
                                <ext:GridCommand Icon="NoteEdit" CommandName="Edit" ToolTip-Text="Edit">
                                </ext:GridCommand>
                                <ext:GridCommand Icon="Delete" CommandName="Delete" ToolTip-Text="Delete">
                                </ext:GridCommand>
                            </Commands>
                            <Listeners>
                                <Command Handler="CommandHandlerMainGrid(command, record);" />
                            </Listeners>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
                <SelectionModel>
                    <ext:CheckboxSelectionModel runat="server" Mode="Multi" />
                </SelectionModel>
            </ext:GridPanel>
            <ext:Window ID="WindowCompetency" runat="server" Title="New Target" Width="450"
                Height="250" Hidden="true" Modal="true" Closable="true" DefaultAnchor="100%"
                AutoScroll="true">
                <Content>
                    <table class="fieldTable">
                        <tr>
                            <td colspan="2">
                                <ext:TextField ID="txtCompetencyName" Width="350" runat="server" FieldLabel="Name"
                                    LabelAlign="Top" LabelSeparator="">
                                </ext:TextField>
                            </td>
                        </tr>
                        <tr>
                            <td valign="bottom" colspan="2">
                                <div class="popupButtonDiv">
                                    <ext:Button runat="server" ID="btnSaveCompetency" Cls="btn btn-primary" Text="<i></i>Save">
                                        <DirectEvents>
                                            <Click OnEvent="btnCompetencySaveUpdate_Click">
                                                <EventMask ShowMask="true" />
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <div class="btnFlatOr">
                                        or</div>
                                    <ext:LinkButton runat="server" ID="btnCancel" StyleSpec="padding:0px;" Cls="btnFlatLeftGap"
                                        Text="<i></i>Cancel">
                                        <Listeners>
                                            <Click Handler="#{WindowCompetency}.hide();">
                                            </Click>
                                        </Listeners>
                                    </ext:LinkButton>
                                </div>
                            </td>
                        </tr>
                    </table>
                </Content>
            </ext:Window>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
