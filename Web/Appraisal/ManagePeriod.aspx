﻿<%@ Page Title="Manage Period" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="ManagePeriod.aspx.cs" Inherits="Web.NewHR.ManagePeriod" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .x-tab-bar-default
        {
            background-color: inherit;
        }
    </style>
    <style type="text/css">
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
        #content
        {
            margin: inherit;
        }
    </style>
    <script type="text/javascript">
    
      

    var CommandHandlerMainGrid = function(command, record) {
        <%= hiddenValue.ClientID %>.setValue(record.data.PeriodId);

        if(command=="Edit")
        {
            <%= btnEditCompetency.ClientID %>.fireEvent('click');
        }
        else
        {
            <%= btnDeleteCompetency.ClientID %>.fireEvent('click');
        }
    };

 
    </script>
    <script type="text/javascript">

   
    </script>
    <script type="text/javascript">

   

    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
  <script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Manage Periods
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel" style="margin-top: 0px !important; padding-top: 0px !important;">
        <ext:Hidden ID="hiddenValue" runat="server" />
        <ext:Hidden ID="hiddenValueForInnerGrids" runat="server" />
        <ext:LinkButton runat="server" ID="btnEditCompetency" Hidden="true">
            <DirectEvents>
                <Click OnEvent="btnEditCompetency_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:LinkButton>
        <ext:LinkButton runat="server" ID="btnDeleteCompetency" Hidden="true">
            <DirectEvents>
                <Click OnEvent="btnDeleteCompetency_Click">
                    <Confirmation ConfirmRequest="true" Message="Confirm for deletion?" />
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:LinkButton>
        <div class="separator bottom">
        </div>
        <div class="innerLR">
            <table width="700">
                <tr>
                    <td align="left" style="padding-top:10px;">
                        <ext:Button runat="server" Text="New Period" Cls="btn btn-primary">
                            <DirectEvents>
                                <Click OnEvent="btnCreateCompetency_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                    </td>
                    <td align="left" style="padding-left: 504px">
                    </td>
                </tr>
            </table>
            <ext:GridPanel ID="GridCompetency" runat="server" StyleSpec="margin-top:15px;" 
                Cls="itemgrid">
                <Store>
                    <ext:Store runat="server">
                        <Model>
                            <ext:Model runat="server" ID="Model1" IDProperty="PeriodId">
                                <Fields>
                                    <ext:ModelField Name="PeriodId" Type="String" />
                                    <ext:ModelField Name="Start" Type="String" />
                                    <ext:ModelField Name="End" Type="String" />
                                    <ext:ModelField Name="DueDate" Type="Date" />
                                    <ext:ModelField Name="Name" Type="String" />
                                    <ext:ModelField Name="StartNepDate" Type="String" />
                                     <ext:ModelField Name="EndNepDate" Type="String" />
                                     <ext:ModelField Name="StatusName" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:Column ID="ColumnName" Sortable="false" MenuDisabled="true" runat="server" Text="Name"
                            Align="Left" DataIndex="Name" Flex="1" />
                        <ext:Column ID="Column1" Sortable="false"  Width="300" MenuDisabled="true" runat="server" Text="Start Date"
                            Align="Left" DataIndex="Start" Flex="1" />
                         <ext:Column ID="Column4" Sortable="false" MenuDisabled="true" runat="server" Text="Start Month"
                            Align="Left" DataIndex="StartNepDate" />
                        <ext:Column ID="Column2" Sortable="false"   Width="300"   MenuDisabled="true" runat="server" Text="End Date"
                            Align="Left" DataIndex="End" Flex="1" />
                         <ext:Column ID="Column5" Sortable="false" MenuDisabled="true" runat="server" Text="End Month"
                            Align="Left" DataIndex="EndNepDate" />
                        <ext:Column ID="Column6" Sortable="false" MenuDisabled="true" runat="server" Text="Status"
                            Align="Left" DataIndex="StatusName" />
                        <ext:DateColumn ID="Column3" Sortable="false" Format="dd MMM yyyy"  MenuDisabled="true" runat="server" Text="Due Date"
                            Align="Left" DataIndex="DueDate" Flex="1" />
                        <ext:CommandColumn runat="server" Width="60" Align="Left">
                            <Commands>
                                <ext:GridCommand Icon="NoteEdit" CommandName="Edit" ToolTip-Text="Edit">
                                </ext:GridCommand>
                                <%--  <ext:GridCommand Icon="Delete" CommandName="Delete" ToolTip-Text="Delete">
                                </ext:GridCommand>--%>
                            </Commands>
                            <Listeners>
                                <Command Handler="CommandHandlerMainGrid(command, record);" />
                            </Listeners>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
              <%--  <SelectionModel>
                    <ext:CheckboxSelectionModel runat="server" Mode="Multi" />
                </SelectionModel>--%>
            </ext:GridPanel>
            <ext:Window ID="WindowCompetency" runat="server" Title="New Period" Width="450" Height="400"
                Hidden="true" Modal="true" Closable="true" DefaultAnchor="100%" AutoScroll="true">
                <Content>
                    <table class="fieldTable">
                        <tr>
                            <td colspan="2">
                                <ext:TextField ID="txtName" Width="350" runat="server" FieldLabel="Name" LabelAlign="Top"
                                    LabelSeparator="">
                                </ext:TextField>
                                <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator1" runat="server"
                                    ValidationGroup="SaveUpdateLevel" ControlToValidate="txtName" ErrorMessage="Name is required." />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <ext:DateField ID="txtStartDate" runat="server" FieldLabel="Start Date" LabelAlign="Top"
                                    Width="176" LabelSeparator="">
                                     <Plugins>
                                        <ext:GenericPlugin ID="GenericPlugin2" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                                    </Plugins>
                                </ext:DateField>
                                <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator3" runat="server"
                                    ValidationGroup="SaveUpdateLevel" ControlToValidate="txtStartDate" ErrorMessage="Start date is required." />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <ext:DateField ID="txtEndDate" runat="server" FieldLabel="End Date" LabelAlign="Top"
                                    Width="176" LabelSeparator="">
                                     <Plugins>
                                        <ext:GenericPlugin ID="GenericPlugin1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                                    </Plugins>
                                </ext:DateField>
                                <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator2" runat="server"
                                    ValidationGroup="SaveUpdateLevel" ControlToValidate="txtEndDate" ErrorMessage="End date is required." />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <ext:DateField ID="txtDueDate" runat="server" FieldLabel="Due Date" LabelAlign="Top"
                                    Width="176" LabelSeparator="">
                                     <Plugins>
                                        <ext:GenericPlugin ID="HighlightDates1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                                    </Plugins>
                                </ext:DateField>
                                <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator4" runat="server"
                                    ValidationGroup="SaveUpdateLevel" ControlToValidate="txtDueDate" ErrorMessage="Due date is required." />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <ext:ComboBox ID="cmbStatus" runat="server" FieldLabel="Status" LabelAlign="Top" Width="176" LabelSeparator="">
                                    <Items>
                                        <ext:ListItem Text="Open" Value="0" />
                                        <ext:ListItem Text="Closed" Value="1" />
                                    </Items>
                                </ext:ComboBox>
                            </td>
                        </tr>
                        <tr>
                            <td valign="bottom" colspan="2">
                                <div class="popupButtonDiv">
                                    <ext:Button runat="server" ID="btnSaveCompetency" Cls="btn btn-primary" Text="<i></i>Save">
                                        <DirectEvents>
                                            <Click OnEvent="btnCompetencySaveUpdate_Click">
                                                <EventMask ShowMask="true" />
                                            </Click>
                                        </DirectEvents>
                                        <Listeners>
                                            <Click Handler="valGroup = 'SaveUpdateLevel'; return CheckValidation();">
                                            </Click>
                                        </Listeners>
                                    </ext:Button>
                                    <div class="btnFlatOr">
                                        or</div>
                                    <ext:LinkButton runat="server" ID="btnCancel" StyleSpec="padding:0px;" Cls="btnFlatLeftGap"
                                        Text="<i></i>Cancel">
                                        <Listeners>
                                            <Click Handler="#{WindowCompetency}.hide();">
                                            </Click>
                                        </Listeners>
                                    </ext:LinkButton>
                                </div>
                            </td>
                        </tr>
                    </table>
                </Content>
            </ext:Window>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
