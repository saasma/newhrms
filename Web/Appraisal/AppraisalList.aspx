﻿<%@ Page Title="Appraisal Listing" Language="C#" MasterPageFile="~/Master/NewDetails.master"
    AutoEventWireup="true" CodeBehind="AppraisalList.aspx.cs" Inherits="Web.NewHR.AppraisalList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        var CommandHandler = function(command, record){
        <%= hdn.ClientID %>.setValue(record.data.AppraisalFormID);
            if(command=="Edit")
            {
                <%= btnEdit.ClientID %>.fireEvent('click');
            }
            else if(command=='Duplicate')
            {
                <%=WindowCopy.ClientID %>.show();
            }
            else
            {
                <%= btnDelete.ClientID %>.fireEvent('click');
            }

            }

             function searchList() {
            <%=PagingToolbar1.ClientID %>.doRefresh();
        }

    </script>
    <style type="text/css">
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
        #content
        {
            margin: inherit;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Appraisal Forms
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel" style="margin-top: 0px !important; padding-top: 0px !important;">
        <ext:LinkButton runat="server" Hidden="true" ID="btnEdit">
            <DirectEvents>
                <Click OnEvent="btnEdit_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:LinkButton>
        <ext:Button ID="btnDelete" runat="server" Hidden="true">
            <DirectEvents>
                <Click OnEvent="btnDelete_Click">
                    <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the Appraisal?" />
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:Button>
        <ext:Hidden runat="server" ID="hdn">
        </ext:Hidden>
        <div class="separator bottom">
        </div>
        <div class="innerLR">
            <div class="separator bottom">
            </div>
            <ext:Label ID="lblMsg" runat="server" />
            <div class="buttonBlock">
                <div class="left">
                    <ext:Button runat="server" ID="btnAdd" Cls="btn btn-primary" Text="<i></i>New Form">
                        <DirectEvents>
                            <Click OnEvent="btnAdd_Click">
                                <EventMask ShowMask="true" />
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </div>
            </div>
            <div style="clear: both">
            </div>
            <ext:GridPanel StyleSpec="margin-top:25px;" ID="gridAppraisalList" runat="server"
                Cls="itemgrid" Width="1000">
                <Store>
                    <ext:Store ID="gridAppraisalStore" AutoLoad="true" PageSize="50" runat="server">
                        <Proxy>
                            <ext:AjaxProxy Json="true" Url="../Handler/AppraisalListAdminHandler.ashx">
                                <ActionMethods Read="GET" />
                                <Reader>
                                    <ext:JsonReader Root="data" TotalProperty="total" />
                                </Reader>
                            </ext:AjaxProxy>
                        </Proxy>
                        <AutoLoadParams>
                            <ext:Parameter Name="start" Value="0" Mode="Raw" />
                        </AutoLoadParams>
                        <Model>
                            <ext:Model Root="data" IDProperty="AppraisalFormID">
                                <Fields>
                                    <ext:ModelField Name="AppraisalFormID" Type="string" />
                                    <ext:ModelField Name="Name" Type="string" />
                                    <ext:ModelField Name="CreatedDate" Type="Date" />
                                    <ext:ModelField Name="RollOutTimes" Type="string" />
                                    <ext:ModelField Name="ActivityName" Type="string" />
                                    <ext:ModelField Name="CompentencyName" Type="string" />
                                    <ext:ModelField Name="TotalReview" Type="string" />
                                    <ext:ModelField Name="TotalCompetency" Type="string" />
                                    <ext:ModelField Name="TotalQuestion" Type="string" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:Column ID="Column_Employee" Sortable="true" runat="server" Text="Form Name"
                            Align="Left" Width="300" DataIndex="Name" Hideable="false" />
                        <ext:DateColumn ID="Column81" Sortable="false" MenuDisabled="true" runat="server"
                            Text="Created On" Align="Left" Width="90" DataIndex="CreatedDate" Format="yyyy/MM/dd" />
                        <ext:Column ID="Column2" Width="90" Sortable="false" MenuDisabled="true" runat="server"
                            Text="Roll Out" Align="Left" DataIndex="RollOutTimes">
                        </ext:Column>
                        <ext:Column ID="Column1" Width="100" Sortable="false" MenuDisabled="true" runat="server"
                            Text="Total Review" Align="Left" DataIndex="TotalReview">
                        </ext:Column>
                        <ext:Column ID="Column3" Width="120" Sortable="false" MenuDisabled="true" runat="server"
                            Text="Total Competency" Align="Left" DataIndex="TotalCompetency">
                        </ext:Column>
                        <ext:Column ID="Column4" Width="100" Sortable="false" MenuDisabled="true" runat="server"
                            Text="Total Question" Align="Left" DataIndex="TotalQuestion">
                        </ext:Column>
                        <ext:CommandColumn ID="CommandColumn1" Flex="1" runat="server" Width="120" Text="Actions"
                            Align="Center" MenuDisabled="true" Sortable="false">
                            <Commands>
                                <ext:CommandSeparator />
                                <ext:GridCommand Icon="Pencil" CommandName="Edit" ToolTip-Text="Edit" />
                                <ext:GridCommand Icon="ControlRepeat" ToolTip-Text="Duplicate Form" CommandName="Duplicate" />
                                <ext:GridCommand Icon="Decline" CommandName="Delete" ToolTip-Text="Delete" />
                            </Commands>
                            <Listeners>
                                <Command Handler="CommandHandler(command,record);" />
                            </Listeners>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
                <View>
                    <ext:GridView ID="GridView1" runat="server">
                        <%--<GetRowClass Fn="getRowClass" />--%>
                    </ext:GridView>
                </View>
                <SelectionModel>
                    <ext:RowSelectionModel ID="RowSelectionModel2111" runat="server" Mode="Single" />
                </SelectionModel>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar1" runat="server" PageSize="50" DisplayInfo="true"
                        DisplayMsg="Displaying Requests {0} - {1} of {2}" EmptyMsg="No Records to display">
                        <Items>
                            <ext:ComboBox Hidden="true" runat="server" SelectedIndex="0" Width="80" ID="cmbPageSize">
                                <Items>
                                    <ext:ListItem Text="20" Value="20" />
                                    <ext:ListItem Text="30" Value="30" />
                                    <ext:ListItem Text="50" Value="50" />
                                    <ext:ListItem Text="100" Value="100" />
                                </Items>
                                <Listeners>
                                    <Select Handler="#{PagingToolbar1}.pageSize = parseInt( '50'); searchList();" />
                                </Listeners>
                            </ext:ComboBox>
                        </Items>
                    </ext:PagingToolbar>
                </BottomBar>
            </ext:GridPanel>
            <div style="clear: both">
            </div>
        </div>
        <ext:Window ID="WindowCopy" runat="server" Title="Duplicate Form" Icon="Application"
            Width="340" Height="220" BodyPadding="5" Hidden="true" Modal="true">
            <Content>
                <table class="fieldTable">
                    <tr>
                        <td>
                            Form Name
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ext:TextField runat="server" ID="txtName"  Width="300px" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom" colspan="2">
                            <div class="popupButtonDiv">
                                <ext:Button runat="server" ID="btnSaveCopy" Cls="btn btn-primary" Text="<i></i>Create Form">
                                    <DirectEvents>
                                        <Click OnEvent="btnSaveCopy_Click">
                                            <Confirmation ConfirmRequest="true" Message="Are you sure, you want to create new form?" />
                                            <EventMask ShowMask="true" />
                                        </Click>
                                    </DirectEvents>
                                </ext:Button>
                                <div class="btnFlatOr">
                                    or</div>
                                <ext:LinkButton runat="server" StyleSpec="padding:0px;" Cls="btnFlatLeftGap" ID="LinkButton1"
                                    Text="<i></i>Cancel" runat="server">
                                    <Listeners>
                                        <Click Handler="#{WindowCopy}.hide();">
                                        </Click>
                                    </Listeners>
                                </ext:LinkButton>
                            </div>
                        </td>
                    </tr>
                </table>
            </Content>
        </ext:Window>
    </div>
</asp:Content>
