﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using BLL;
using DAL;
using BLL.Manager;

namespace Web.NewHR
{
    public partial class ManageCompetancy : System.Web.UI.Page
    {

        static bool btnEditIsClicked = false;
        static bool isReadOnly = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!X.IsAjaxRequest)
            {
                Initialize();
            }
        }

        //private void ReadOnlyMode(bool isReadOnly)        
        //{
        //    if (isReadOnly)            
        //    {
        //        this.txtCompetencyName.ReadOnly = true;
        //        this.txtDescription.ReadOnly = true;
        //        this.cmbCategory.ReadOnly = true;
        //        this.cmbCore.ReadOnly = true;

        //        this.btnAddRow.Hide();
        //        this.btnAddPosition.Hide();

        //        X.Js.AddScript("readOnlyMode=true;");
        //    }
        //    else
        //    {
        //        this.txtCompetencyName.ReadOnly = false;
        //        this.txtDescription.ReadOnly = false;
        //        this.cmbCategory.ReadOnly = false;
        //        this.cmbCore.ReadOnly = false;

        //        this.btnAddRow.Show();
        //        this.btnAddPosition.Show();

        //        X.Js.AddScript("readOnlyMode=false;");
        //    }
        //}

        protected void btnCreateCompetency_Click(object sender, DirectEventArgs e)
        {
            btnEditIsClicked = false;

            //isReadOnly = false;
            //this.ReadOnlyMode(isReadOnly);

            ClearCompetencyFields();
            cmbCore.SelectedItem.Index = 0;
          

            GridTeaser.GetStore().DataSource = GetEmptyTeaserLines();
            GridTeaser.GetStore().DataBind();

            GridPosition.GetStore().DataSource = GetEmptyPositionLines();
            GridPosition.GetStore().DataBind();

            WindowCompetency.Show();
        }

        protected void btnMoveSelected_Click(object sender, DirectEventArgs e)
        {
 
        }

        protected void btnDeleteSelected_Click(object sender, DirectEventArgs e)
        {
 
        }

        protected void btnEditCompetency_Click(object sender, DirectEventArgs e)
        {
           


            btnEditIsClicked = true;

            int competencyID = int.Parse(hiddenValue.Text.Trim());

            AppraisalCompetency entityCompetency = AppraisalManager.GetCompetencyByID(competencyID);

            txtCompetencyName.Text = entityCompetency.Name;
            txtDescription.Text = entityCompetency.Description;
            cmbCategory.SetValue(entityCompetency.CategoryID.ToString());
            cmbCore.SetValue(entityCompetency.IsCore.ToString());

            // Bind GridTeaser
            List<int> lstTeaserIDs = AppraisalManager.GetTeaserIDsByCompetencyID(competencyID);
            List<AppraisalCompetencyTeaser> lstTeasers = new List<AppraisalCompetencyTeaser>();

            foreach (int teaserID in lstTeaserIDs)
            {
                string teaser = AppraisalManager.GetTeaserNameByID(teaserID);
                lstTeasers.Add(new AppraisalCompetencyTeaser { TeaserID = teaserID, Name = teaser });
            }

            TeaserStore.DataSource = lstTeasers;
            TeaserStore.DataBind();

            // Bind GridPosition
            List<int> lstDesignationIDs = AppraisalManager.GetDesignationIDsByCompetencyID(competencyID);
            List<EDesignation> lstPositions = new List<EDesignation>();


            foreach (int id in lstDesignationIDs)
            {
                lstPositions.Add(new EDesignation { DesignationId = id });
            }
            StorePosition.DataSource = lstPositions;
            StorePosition.DataBind();
            

            // Finally show the competency window
            WindowCompetency.Show();
        }

        protected void btnDeleteCompetency_Click(object sender, DirectEventArgs e)
        {
            int competencyID = int.Parse(hiddenValue.Text.Trim());

            Status status = AppraisalManager.DeleteCompetency(competencyID);

            if (status.IsSuccess)
            {
                LoadCompetencies();
                NewMessage.ShowNormalMessage("Competency Deleted.");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        protected void btnCompetencySaveUpdate_Click(object sender, DirectEventArgs e)
        {
            if (txtCompetencyName.Text == "")
            {
                NewMessage.ShowWarningMessage("Competency field cannot be empty.");
                txtCompetencyName.Focus();
            }
            else if (cmbCategory.Text == "")
            {
                NewMessage.ShowWarningMessage("Category field cannot be empty.");
            }
            else if (cmbCore.Text == "")
            {
                NewMessage.ShowWarningMessage("Core must be selected.");
            }
            else
            {
                bool competencyAlreadyExists = false;
                if (!btnEditIsClicked)
                {
                    // Check if the competency is already in the database
                    List<string> competencies = AppraisalManager.GetAllCompetencyNames();
                    string txtCompetencyTrimmed = txtCompetencyName.Text.ToString().Trim().ToLower();
                    foreach (string competency in competencies)
                    {
                        if (competency.Trim().ToLower() == txtCompetencyTrimmed)
                        {
                            competencyAlreadyExists = true;
                            break;
                        }
                    }
                }

                if (competencyAlreadyExists)
                {
                    NewMessage.ShowWarningMessage("The competency you entered already exist.\nEnter a different Competency.");
                    txtCompetencyName.Focus();
                }
                else
                {
                    // save to the database
                    SaveUpdateCompeteney(e);
                }
            }
        }

        protected void btnEditTeaser_Click(object sender, DirectEventArgs e)
        {
            int teaserID = int.Parse(hiddenValueForInnerGrids.Text.Trim());

            // getting the teaser name and displaying in the textbox
            txtWinTeaserName.Text = AppraisalManager.GetTeaserNameByID(teaserID);

            WindowTeaser.Show();
            txtWinTeaserName.Focus();
        }

        protected void btnUpdateTeaser_Click(object sender, DirectEventArgs e)
        {
            int teaserID = int.Parse(hiddenValueForInnerGrids.Text.Trim());

            Status status = AppraisalManager.UpdateTeaserName(teaserID, txtWinTeaserName.Text.Trim());

            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Teaser Updated.");

                // change the teaser value in the GridTeaser
                RefreshGridTeaser();
                
                WindowTeaser.Hide();
            }
            else
            {
                NewMessage.ShowWarningMessage("Could not update teaser.");
            }
        }

        protected void btnEditPosition_Click(object sender, DirectEventArgs e)
        {
            cmbWinPosition.Store[0].DataSource = AppraisalManager.GetPositions();
            cmbWinPosition.Store[0].DataBind();

            int designationID = int.Parse(hiddenValueForInnerGrids.Text.Trim());
            cmbWinPosition.SetValue(designationID);

            cmbWinPosition.Focus();
            WindowPosition.Show();
        }

        protected void btnUpdatePosition_Click(object sender, DirectEventArgs e)
        {
            int competencyID = int.Parse(hiddenValue.Text.Trim());
            int designationID = int.Parse(hiddenValueForInnerGrids.Text.Trim());
            int comPosID = AppraisalManager.GetCompetencyPositionID(competencyID, designationID);

            int newDesignationID = Convert.ToInt32(cmbWinPosition.SelectedItem.Value);

            Status status = AppraisalManager.UpdatePosition(comPosID, newDesignationID);

            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Position Updated.");

                // Change the Position value in the GridPosition
                RefreshGridPosition();

                WindowPosition.Hide();
            }
            else
            {
                NewMessage.ShowWarningMessage("Could not update position.");
            }
        }

        public void Initialize()
        {
            LoadCompetencies();

            cmbCategory.Store[0].DataSource = AppraisalManager.GetCategories();
            cmbCategory.Store[0].DataBind();

            StoreCmbPosition.DataSource = AppraisalManager.GetPositions();
            StoreCmbPosition.DataBind();

            LoadLevels();
        }

        public void LoadCompetencies()
        {
            GridCompetency.Store[0].DataSource = AppraisalManager.GetAllCompetencies();
            GridCompetency.Store[0].DataBind();
        }

        public void SaveUpdateCompeteney(DirectEventArgs e)
        {
            // saving to the AppraisalCompetency table
            AppraisalCompetency entity = new AppraisalCompetency();

            bool isInsert = true;
            if (!string.IsNullOrEmpty(hiddenValue.Text))
            {
                isInsert = false;
                entity.CompetencyID = int.Parse(hiddenValue.Text.Trim());
            }

            List<AppraisalCompetencyTeaser> TeaserLines = new List<AppraisalCompetencyTeaser>();
            List<AppraisalCompetencyPosition> PositionLines = new List<AppraisalCompetencyPosition>();

            entity.Name = txtCompetencyName.Text.Trim();
            entity.Description = txtDescription.Text.Trim();
            entity.CategoryID = int.Parse(cmbCategory.SelectedItem.Value);
            entity.IsCore = Convert.ToBoolean(cmbCore.SelectedItem.Value);
            


            string jsonTeaser = e.ExtraParams["TeaserExtraParam"];
            string jsonPosition = e.ExtraParams["PositionExtraParam"];


            if (string.IsNullOrEmpty(jsonTeaser) && string.IsNullOrEmpty(jsonPosition))
                return;

            TeaserLines = JSON.Deserialize<List<AppraisalCompetencyTeaser>>(jsonTeaser);
            PositionLines = JSON.Deserialize<List<AppraisalCompetencyPosition>>(jsonPosition);


            // making  lists and adding line items in the list to avoid InvalidOperationException
            List<AppraisalCompetencyTeaser> lstTeasersToRemove = new List<AppraisalCompetencyTeaser>();
            List<AppraisalCompetencyPosition> lstPositionsToRemove = new List<AppraisalCompetencyPosition>();

            // adding the empty row references to the lists

            foreach (var line in TeaserLines)
            {
                if (string.IsNullOrEmpty(line.Name))
                    lstTeasersToRemove.Add(line);
            }

            foreach (var line in PositionLines)
            {
                if (line.DesignationID==0)
                    lstPositionsToRemove.Add(line);
            }

            // now removing the empty rows
            foreach (var line in lstTeasersToRemove)
            {
                TeaserLines.Remove(line);
            }
            foreach (var line in lstPositionsToRemove)
            {
                PositionLines.Remove(line);
            }

            Status status = AppraisalManager.InsertUpdateCompetency(entity, TeaserLines, PositionLines, isInsert);

            if (status.IsSuccess)
            {
                WindowCompetency.Hide();
                LoadCompetencies();
                NewMessage.ShowNormalMessage("Competency Saved.");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        public void ClearCompetencyFields()
        {
            hiddenValue.Text = "";

            txtCompetencyName.Text = "";
            txtDescription.Text = "";
            cmbCategory.ClearValue();
            //cmbCore.ClearValue();
        }

        public List<AppraisalCompetencyTeaser> GetEmptyTeaserLines()
        {
            List<AppraisalCompetencyTeaser> returnList = new List<AppraisalCompetencyTeaser>();
            returnList.Add(new AppraisalCompetencyTeaser { });
            returnList.Add(new AppraisalCompetencyTeaser { });
            returnList.Add(new AppraisalCompetencyTeaser { });

            return returnList;
        }

        public List<AppraisalCompetencyPosition> GetEmptyPositionLines()
        {
            List<AppraisalCompetencyPosition> returnList = new List<AppraisalCompetencyPosition>();
            returnList.Add(new AppraisalCompetencyPosition { });
            returnList.Add(new AppraisalCompetencyPosition { });
            returnList.Add(new AppraisalCompetencyPosition { });

            return returnList;
        }

        public void RefreshGridTeaser()
        {
            int competencyID = int.Parse(hiddenValue.Text.Trim());

            // Bind GridTeaser
            List<int> lstTeaserIDs = AppraisalManager.GetTeaserIDsByCompetencyID(competencyID);
            List<AppraisalCompetencyTeaser> lstTeasers = new List<AppraisalCompetencyTeaser>();

            foreach (int teaserID in lstTeaserIDs)
            {
                string teaser = AppraisalManager.GetTeaserNameByID(teaserID);
                lstTeasers.Add(new AppraisalCompetencyTeaser { TeaserID = teaserID, Name = teaser });
            }

            TeaserStore.DataSource = lstTeasers;
            TeaserStore.DataBind();
        }

        public void RefreshGridPosition()
        {
            int competencyID = int.Parse(hiddenValue.Text.Trim());

            List<int> lstDesignationIDs = AppraisalManager.GetDesignationIDsByCompetencyID(competencyID);
            List<EDesignation> lstPositions = new List<EDesignation>();


            foreach (int id in lstDesignationIDs)
            {
                lstPositions.Add(new EDesignation { DesignationId = id });
            }
            StorePosition.DataSource = lstPositions;
            StorePosition.DataBind();
        }


        #region "Competency Category List"

        private void LoadLevels()
        {
            GridLevels.GetStore().DataSource = AppraisalManager.getallAppraisalCategory();
            GridLevels.GetStore().DataBind();

        }



        public void ClearLevelFields()
        {
            hiddenValue.Text = "";
            txtName.Text = "";


        }



        protected void btnAddLevel_Click(object sender, DirectEventArgs e)
        {


            ClearLevelFields();
            WindowLevel.Show();
        }


        protected void btnDeleteLevel_Click(object sender, DirectEventArgs e)
        {
            int goalCategory = int.Parse(hiddenValue.Text.Trim());
            Status status = AppraisalManager.DeleteAppraisalCategory(goalCategory);
            if (status.IsSuccess)
            {
                LoadLevels();
                NewMessage.ShowNormalMessage("Appraisal Category deleted.");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }



        protected void btnEditLevel_Click(object sender, DirectEventArgs e)
        {
            int levelId = int.Parse(hiddenValue.Text.Trim());
            AppraisalCategory entity = AppraisalManager.GetAppraisalCategoryById(levelId);
            WindowLevel.Show();
            txtName.Text = entity.Name;

        }

        protected void btnLevelSaveUpdate_Click(object sender, DirectEventArgs e)
        {



            Page.Validate("InsertUpdateLevel");
            if (Page.IsValid)
            {
                AppraisalCategory categoryInstance = new AppraisalCategory();
                categoryInstance.Name = txtName.Text;


                if (!string.IsNullOrEmpty(hiddenValue.Text))
                {
                    categoryInstance.CategoryID = int.Parse(hiddenValue.Text);
                }

                Status status = NewHRManager.InsertUpdateAppraisalCategory(categoryInstance);
                if (status.IsSuccess)
                {
                    WindowLevel.Hide();
                    LoadLevels();

                    cmbCategory.Store[0].DataSource = AppraisalManager.GetCategories();
                    cmbCategory.Store[0].DataBind();


                    NewMessage.ShowNormalMessage("Appraisal Category saved.");
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }

            }


        }



        #endregion
    }
}
