﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using Utils.Calendar;
using Utils.Helper;

namespace Web.NewHR
{
    public partial class AppraisalList : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.EmployeeAppraisal;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMsg.Text = "";
            if (!X.IsAjaxRequest)
            {
                Initialise();

            }
        }

        public void Initialise()
        {
            //LoadData();
        }

        protected void LoadData()
        {

            PagingToolbar1.DoRefresh();
           // List<AppraisalForm> _ListAppraisalForm = NewHRManager.GetALLAppraisalList();
            //gridAppraisalStore.DataSource = _ListAppraisalForm;
            //gridAppraisalStore.DataBind();

        }


        protected void btnSaveCopy_Click(object sender, DirectEventArgs e)
        {
            if (string.IsNullOrEmpty(txtName.Text))
            {
                NewMessage.ShowWarningMessage("Name is required.");
                return;
            }

            if (BLL.BaseBiz.PayrollDataContext.AppraisalForms.Any(x => x.Name.Trim().ToLower()
                == txtName.Text.Trim().ToLower()))
            {
                NewMessage.ShowWarningMessage("Form Name already exists.");
                return;
            }

            AppraisalManager.CreateNewForm(txtName.Text.Trim(), int.Parse(hdn.Text));

            NewMessage.ShowNormalMessage("New form has been created.");
            LoadData();
            WindowCopy.Hide();
        }
        protected void btnEdit_Click(object sender, DirectEventArgs e)
        {
            int AppraisalFormID = int.Parse(hdn.Text.Trim());
            Response.Redirect("~/Appraisal/form/general_settings_1.aspx?fid=" + AppraisalFormID.ToString());

        }
        protected void btnAdd_Click(object sender, DirectEventArgs e)
        {
            Response.Redirect("~/Appraisal/form/general_settings_1.aspx");
        }
        protected void btnDelete_Click(object sender, DirectEventArgs e)
        {
            int AppraisalFormID = int.Parse(hdn.Text.Trim());
            Boolean inUse;
            bool result = NewHRManager.DeleteAppraisalByFormID(AppraisalFormID, out inUse);

            if (!inUse)
            {
                if (result)
                {
                    LoadData();
                    return;
                        
                }
                else
                    SetWarning(lblMsg,"Appraisal form is in Use");
                X.Js.AddScript("window.scrollTo(0,0);");
            }
            else
            {
                SetWarning(lblMsg, "Appraisal form is in Use");
                X.Js.AddScript("window.scrollTo(0,0);");
            }
            
           

        }
        
       
    }
}