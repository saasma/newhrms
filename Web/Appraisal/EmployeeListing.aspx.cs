﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using System.Xml;
using Utils.Calendar;

namespace Web.Appraisal
{
    public partial class EmployeeListing : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.EmployeeAppraisal;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!X.IsAjaxRequest)
            {
                Initialise();
              
            }

           
        }

        public void Initialise()
        {
            
            storeForm.DataSource = AppraisalManager.GetAllForms();
            storeForm.DataBind();

            StoreGroup.DataSource = BLL.BaseBiz.PayrollDataContext.AppraisalRolloutGroups.OrderBy(x => x.Name).ToList();
            StoreGroup.DataBind();

            List<KeyValue> list1 = new JobStatus().GetMembers();
             list1.RemoveAt(0);

             cmbServiceStatus.Store[0].DataSource = list1;
            cmbServiceStatus.Store[0].DataBind();

            LoadLevels();

            List<ApprovalFlow> list =
                TravelAllowanceManager.GetDefaultApprovalFlowList((int)FlowTypeEnum.Appraisal);
          
            list.Insert(0, new ApprovalFlow { StepID = 0, StepName = "Pending (Not Submitted)" });
            list.Insert(0, new ApprovalFlow { StepID = -1, StepName = "All" });
            cmbStatus.SelectedItems.Add(new Ext.Net.ListItem { Index = 0 });


            cmbStatus.Store[0].DataSource = list;
            cmbStatus.Store[0].DataBind();

            StoreLevel.DataSource = NewPayrollManager.GetAllParentLevels();
            StoreLevel.DataBind();


            StoreBranch.DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            storeSearch.DataBind();

            //Appraisal Periods
            List<AppraisalPeriod> listAppraisalPeriods = AppraisalManager.GetAllPeriods().ToList();

            foreach (var item in listAppraisalPeriods)
            {
                item.Start = string.Format("{0} ({1} {2} - {3} {4})", item.Name, item.StartDate.Value.Year, DateHelper.GetMonthShortName(item.StartDate.Value.Month, true),
                    item.EndDate.Value.Year, DateHelper.GetMonthShortName(item.EndDate.Value.Month, true));
            }

            AppraisalPeriod obj = new AppraisalPeriod() { Start = "All", PeriodId = -1, Closed = true };
            listAppraisalPeriods.Insert(0, obj);
            cmbPeriod.Store[0].DataSource = listAppraisalPeriods;
            cmbPeriod.Store[0].DataBind();

            List<AppraisalPeriod> listOpenOnly = listAppraisalPeriods.Where(x => x.Closed == null || x.Closed == false).ToList();
            foreach (var item in listOpenOnly)
                cmbPeriod.SelectedItems.Add(new Ext.Net.ListItem(item.PeriodId.ToString()));           

        }

        

        public void btnDeleteForms_Click(object sender, DirectEventArgs e)
        {
            //if (Convert.ToDateTime(txtStartDate.Text.Trim()).Month != Convert.ToDateTime(txtEndDate.Text.Trim()).Month)
            //{
            //    NewMessage.ShowNormalMessage("Please select single month.");
            //    return;
            //}

            //if (Convert.ToDateTime(txtStartDate.Text.Trim()).Year != Convert.ToDateTime(txtEndDate.Text.Trim()).Year)
            //{
            //    NewMessage.ShowNormalMessage("Please select single month.");
            //    return;
            //}

            List<int> formId = new List<int>();

            RowSelectionModel sm = this.GridLevels.GetSelectionModel() as RowSelectionModel;
            foreach (SelectedRow row in sm.SelectedRows)
            {

                formId.Add(int.Parse(row.RecordID));
            }


            Status status =  AppraisalManager.DeleteForms(formId);

            if (status.IsSuccess)
            {
                
                NewMessage.ShowNormalMessage("Appraisal deleted.");
                X.Js.AddScript("searchList();");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
            


        }

        private void LoadLevels()
        {
            
            //GridLevels.GetStore().DataSource = AppraisalManager.GetAllRollouts();
            //GridLevels.GetStore().DataBind();


        }

        public void ClearLevelFields()
        {

          

            

        }

        protected void btnUpdateTargetValue_Click(object sender, DirectEventArgs e)
        {
            // change later
            AppraisalManager.UpdateTargetValue(43);
        }

        protected void btnReverseStatus_Click(object sender, DirectEventArgs e)
        {
            AppraisalEmployeeForm empForm = AppraisalManager.GetEmployeeAppraisaleForm(int.Parse(hiddenValue.Text));

            windowRevert.Title = "Appraisal Revert for : " + EmployeeManager.GetEmployeeName(empForm.EmployeeId);
            disp.Text = empForm.StatusText == null ? "" : empForm.StatusText + " ( Step " + empForm.Status + ")";


            List<ApprovalFlow> list =
               TravelAllowanceManager.GetApprovalFlowListForEmployeeAppraisal(empForm.AppraisalEmployeeFormID);
            //foreach (var item in list)
            //{
            //    item.StepName += " ( Step " + item.StepID + ")";
            //}
            list.Insert(0, new ApprovalFlow { StepID = 0, StepName = "Employee Submit (Start)" });
            //list.Insert(0, new ApprovalFlow { StepID = -1, StepName = "All" });
            //cmbStatus.SelectedItems.Add(new Ext.Net.ListItem { Index = 0 });

            cmbRevertStatus.Store[0].DataSource = list;
            cmbRevertStatus.Store[0].DataBind();


            windowRevert.Show();
            cmbRevertStatus.ClearValue();
            //int empAppraisalForm = int.Parse(hiddenValue.Text);

            //Status status = AppraisalManager.ReverseAppraisalStatus(empAppraisalForm);

            //if (status.IsSuccess)
            //{
            //    NewMessage.ShowNormalPopup("Appraisal reverted.");
            //    X.Js.AddScript("searchList();");
            //}
            //else
            //{
            //    NewMessage.ShowWarningPopup(status.ErrorMessage);
            //}

        }

        protected void btnConfirmRevert_Click(object sender, DirectEventArgs e)
        {

            int empAppraisalForm = int.Parse(hiddenValue.Text);

            Status status = AppraisalManager.ReverseAppraisalStatus(empAppraisalForm,int.Parse(cmbRevertStatus.SelectedItem.Value)
                ,txtNotes.Text.Trim());

            if (status.IsSuccess)
            {
                windowRevert.Hide();
                NewMessage.ShowNormalPopup("Appraisal reverted.");
                X.Js.AddScript("searchList();");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }

        }

        protected void btnAddLevel_Click(object sender, DirectEventArgs e)
        {
            hiddenValueRow.Text = "";
            ClearLevelFields();

        }



        public void btnExport_Click(object sender, EventArgs e)
        {
            int start = 0;
            int pagesize = 999999;
            int EmoloyeeID = -1;
            int statusId = -1;
            int levelId = -1;
            int branchId = -1;
            int formId = -1;
            int apprasailgroupId = -1;
            string periodIds = "";


            if (cmbSearch.SelectedItem != null && cmbSearch.SelectedItem.Value != null)
                EmoloyeeID = Convert.ToInt32(cmbSearch.SelectedItem.Value);
            if (cmbLevelList.SelectedItem != null && cmbLevelList.SelectedItem.Value != null)
                levelId = Convert.ToInt32(cmbLevelList.SelectedItem.Value);
            if (cmbStatus.SelectedItem != null && cmbStatus.SelectedItem.Value != null)
                statusId = Convert.ToInt32(cmbStatus.SelectedItem.Value);
            if (cmbBranch.SelectedItem != null && cmbBranch.SelectedItem.Value != null)
                branchId = Convert.ToInt32(cmbBranch.SelectedItem.Value);
            if (cmbForm.SelectedItem != null && cmbForm.SelectedItem.Value != null)
                formId = Convert.ToInt32(cmbForm.SelectedItem.Value);

            if (cmbAppraisalGroup.SelectedItem != null && cmbAppraisalGroup.SelectedItem.Value != null)
                apprasailgroupId = Convert.ToInt32(cmbAppraisalGroup.SelectedItem.Value);

            //periodIds
            foreach (Ext.Net.ListItem item in cmbPeriod.SelectedItems)
                periodIds += item.Value.ToString() + ",";

            if (periodIds != "")
                periodIds = periodIds.TrimEnd(',');


           List<GetRolloutEmployeesListResult> resultSet = AppraisalManager.GetEmployeeRolloutList(start, pagesize, EmoloyeeID
                , levelId, statusId, branchId, formId, Hidden_statslist.Text.Trim(), periodIds, apprasailgroupId);


            Dictionary<string, string> renameList = new Dictionary<string, string>{
                { "StatusText", "Status" },

                { "SelfSubmittedOn", "Submitted On" },
                { "TargetScore", "Target Score" },


                 {"EmpActivityScore", "Employee Activity Score" },
                 {"EmpCompetencyScore", "Employee Competency Score" },
                 {"EmpQuestionScore", "Employee Question Score" },
                 {"EmpTotalScore", "Employee Total Score" },
                 {"EmpGrade", "Employee Grade" },
                 {"EmpGradeSummary", "Employee Grade Summary" },


                 {"SupActivityScore","Supervisor Activity Score"}
                ,{"SupCompetencyScore","Supervisor Competency Score"},
                {"SupQuestionScore","Supervisor Question Score"}
                ,{"SupTotalScore","Supervisor Total Score"},
                {"SupGrade","Supervisor Grade"},
                {"SupGradeSummary","Supervisor Grade Summary"},

                {"ActivitySupervisorComment","Activity Supervisor Comment"}
            };



            List<string> hiddenList = new List<string> { "TotalRows", "RowNumber", "Comments","DueDate","DaysRemaining","SelfScoreText",
                "SupervisorScoreText","EmployeeCompetencyGrade","SupervisorCompetencyGrade","AppraisalEmployeeFormID","AppraisalFormRef_ID",
                "CreatedOn","CreatedBy","CreatedByUserID","Status","ModifiedOn","ModifiedBy","ActivitySummary","ActivitySelfRating",
                "ActivitySupervisorRating","TargetScore","ActivitySupervisorComment"

                ,"SelfCommentedOn","SupervisorSubmittedOn","SupervisorEmployeeId"    ,"SupervisorReviewAgreed"  ,"RolloutID","RolloutDate"
                ,"IsActivitySaved","IsCompetencySaved"   ,"IsQuestionSaved" ,"EmployeeActivityScore"   ,"SupervisorActivityScore" 
                ,"EmployeeCompetencyScore" ,"SupervisorCompetencyScore"   ,"EmployeeQuestionScore","SupervisorQuestionScore"
                ,"EmployeeActivityTotalScore","SupervisorActivityTotalScore","EmployeeCompetencyTotalScore","SupervisorCompetencyTotalScore"
                ,"EmployeeQuestionTotalScore","SupervisorQuestionTotalScore","Activity","Supervisor","Comment" 
                ,"SupervisorStep2Comment","RevertedBy","RevertedOn","IsTargetSaved","PeriodId","RecommendForId"
                ,"RecommendedByEmpId","Target","Score","TargetTotalScore","FinalEmpCompetencyScore","FinalTargetScore"
                ,"FinalEmpQuestionScore","FinalSupCompetencyScore","FinalSupQuestionScore","EmployeeGrade","SupervisorGrade"
                ,"CurrentApprovalSelectedEmployeeId","NextStepOrStatusId","RevertNotes","SupervisorTargetScore","FinalSupervisorTargetScore"
                ,"StatusId","StartDate","StatusId1","Name","IsContractStatus"

            };
            List<ApprovalFlow> appraisalFlowSteps = TravelAllowanceManager.GetDefaultApprovalFlowList((int)FlowTypeEnum.Appraisal);
            //show/hide comment column
            for (int i = 3; i <= 11; i++)
            {
                ApprovalFlow flow = appraisalFlowSteps.FirstOrDefault(x => x.StepID == i);
                if (flow == null)
                {
                    hiddenList.Add("Step" + i + "Comment");
                }
                else
                {
                    renameList.Add("Step" + i + "Comment", flow.AuthorityTypeDisplayName + "'s Comment");
                }
            }

            ApprovalFlow lastflow = appraisalFlowSteps.FirstOrDefault(x => x.StepID == (int)FlowStepEnum.Step15End);
            renameList.Add("Step15Comment", lastflow.AuthorityTypeDisplayName + "'s Comment");

            Bll.ExcelHelper.ExportToExcel<GetRolloutEmployeesListResult>(
                "Appraisal List", resultSet,
                hiddenList,
                new List<string> { },
                renameList, new List<string>() { },
                new Dictionary<string, string> { },
                new List<string> {"EmployeeId","IdCardNo","EmployeeName","ReviewYear","Branch","Level","ServiceStatus","FormName",
                    "SelfSubmittedOn","StatusText","WaitingFrom",

                    "SelfEmployeeRatingScore","SupervisorRatingScore",
                    "EmployeeGradeSummary","SupervisorGradeSummary"


                    //"Designation","StatusText","SelfSubmittedOn","Recommendation","TargetScore",

                    //"EmpActivityScore","EmpCompetencyScore",
                    //"EmpQuestionScore","EmpTotalScore",
                    //"EmpGrade","EmpGradeSummary",


                    //"SupActivityScore","SupCompetencyScore",
                    //"SupQuestionScore","SupTotalScore",
                    //"SupGrade","SupGradeSummary",


                    //"ActivitySupervisorComment",

                    //"Step3Comment","Step4Comment","Step5Comment","Step6Comment","Step7Comment",
                    //"Step8Comment","Step9Comment","Step10Comment","Step11Comment","Step15Comment"

                }
                );

        }
        protected void btnDeleteLevel_Click(object sender, DirectEventArgs e)
        {
            int RowID = int.Parse(hiddenValueRow.Text.Trim());
            Status status = AppraisalManager.DeleteRolloutByID(RowID);
            if (status.IsSuccess)
            {
                LoadLevels();
                NewMessage.ShowNormalMessage("Rollout deleted.");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }


     

      



        


 

        




        
    }
}