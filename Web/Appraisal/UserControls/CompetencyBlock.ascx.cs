﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using BLL.BO;
using BLL.Manager;
using System.Web.UI.HtmlControls;
using BLL;
using DAL;
using BLL.Base;
using Utils.Web;
using System.IO;

namespace Web.Appraisal.UserControls
{
    public partial class CompetencyBlock : BaseUserControl
    {
        private int _formId;
        public int AppraisalFormID
        {
            get
            {
                return _formId;
            }
            set
            {
                _formId = value;
            }
        }
        private AppraisalStatus _mode = AppraisalStatus.Saved;
        public AppraisalStatus AppraisalFormMode
        {
            get
            {
                return _mode;
            }
            set
            {
                _mode = value;
            }
        }

        //int activityBlockNumber = 3;
        //int competencyBlockNumber = 4;
        //int questionnareBlockNumber = 5;
        //int reviewBlockNumber = 6;


        public string GetActivityBlockID
        {
            get
            {
                return blockActivity.ClientID;
            }
        }
        public string GetCompetencyBlockID
        {
            get
            {
                return block.ClientID;
            }
        }
        public string GetTargetBlockID
        {
            get
            {
                return blockTarget.ClientID;
            }
        }
       
        public string GetQuestionnareBlockID
        {
            get
            {
                return blockQuestionnare.ClientID;
            }
        }
        public string GetReviewBlockID
        {
            get
            {
                return blockReview.ClientID;
            }
        }
        

        List<AppraisalRatingBO> targetRatings = new List<AppraisalRatingBO>();

        AppraisalEmployeeForm empForm = null;

        protected void Page_Load(object sender, EventArgs e)
        {

            

        }
        public int GetEmployeeAppraisalFormID()
        {
            if (!string.IsNullOrEmpty(Request.QueryString["efid"]))
                return int.Parse(Request.QueryString["efid"]);

            return 0;
        }
        public int GeteAppraisalFormID()
        {
            if (!string.IsNullOrEmpty(Request.QueryString["fid"]))
                return int.Parse(Request.QueryString["fid"]);

            return 0;
        }

        public List<AppraisalValue> GetEmployeeReviewList()
        {
            List<AppraisalValue> inputList = new List<AppraisalValue>();
            AppraisalForm form = AppraisalManager.getFormInstanceByID(this.AppraisalFormID);

            if (form == null)
                return inputList;


            AppraisalEmployeeForm empForm = AppraisalManager.GetEmployeeAppraisaleForm
              (GetEmployeeAppraisalFormID());

            List<AppraisalDynamicFormReview> reviewList = AppraisalManager.GetFormReview(this.AppraisalFormID);

            AppraisalRatingScale ratingScale = null;
            if (form.ReviewRatingScaleRef_ID != null)
                ratingScale = AppraisalManager.GetRatingScaleById(form.ReviewRatingScaleRef_ID.Value);

            foreach (AppraisalDynamicFormReview item in reviewList)
            {

                AppraisalValue input = new AppraisalValue();
                input.ReviewId = item.ReviewID;
                input.SupervisorComment = ((TextBox)this.FindControl("commentInputReviewSupervisor" + item.ReviewID)).Text.Trim();
                inputList.Add(input);
            }

            return inputList;

        }

        protected void btnSetTarget_Click(object sender, DirectEventArgs e)
        {
            if (cmbTarget.SelectedItem == null || cmbTarget.SelectedItem.Value == null)
            {
                NewMessage.ShowWarningMessage("Target is required.");
                return;
            }

            if (string.IsNullOrEmpty(txtWeight.Text))
            {
                NewMessage.ShowWarningMessage("Weight is required.");
                return;
            }

            if (string.IsNullOrEmpty(txtTargetValue.Text))
            {
                NewMessage.ShowWarningMessage("Target value is required.");
                return;
            }

            if (string.IsNullOrEmpty(txtAchievement.Text))
            {
                NewMessage.ShowWarningMessage("Achievement is required.");
                return;
            }

            double val = 0;
            if(double.TryParse(txtWeight.Text,out val)==false)
            {
                NewMessage.ShowWarningMessage("Invalid Weight value.");
                return;
            }

            AppraisalEmployeeFormTarget target = new AppraisalEmployeeFormTarget();
            target.TargetRef_ID = int.Parse(cmbTarget.SelectedItem.Value);
            target.AppraisalEmployeeFormRef_ID = GetEmployeeAppraisalFormID();
            target.Weight = double.Parse(string.IsNullOrEmpty(txtWeight.Text) ? "0" : txtWeight.Text);
            target.AssignedTargetTextValue = txtTargetValue.Text.Trim();
            target.AchievementTextValue = txtAchievement.Text.Trim();


            Status status = AppraisalManager.InsertUpdateEmpTarget(target);


            Response.Redirect(Request.RawUrl);
        }


        protected void gvw_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            
            int targetId = int.Parse(gridTargets.DataKeys[e.RowIndex][0].ToString());
            AppraisalEmployeeFormTarget target = new AppraisalEmployeeFormTarget();
            target.TargetRef_ID = targetId;
            target.AppraisalEmployeeFormRef_ID = GetEmployeeAppraisalFormID();


            AppraisalManager.DeleteEmpTarget(target);

            Response.Redirect(Request.RawUrl);
            
        }

        public AppraisalValue GetEmployeeActivityList()
        {
            AppraisalForm form = AppraisalManager.getFormInstanceByID(this.AppraisalFormID);

            if (form == null)
                return null;


            AppraisalEmployeeForm empForm = AppraisalManager.GetEmployeeAppraisaleForm
              (GetEmployeeAppraisalFormID());


            AppraisalRatingScale ratingScale = null;
            if (form.ActivityRatingScaleRef_ID != null)
                ratingScale = AppraisalManager.GetRatingScaleById(form.ActivityRatingScaleRef_ID.Value);



            AppraisalValue input = new AppraisalValue();

            if (form.AllowActivitySelfRating.Value)
                //input.SelfRating = int.Parse(((TextBox)this.FindControl("ratingValueActivityEmployee1")).Text.Trim());
                input.SelfRating = double.Parse(Request.Form[this.FindControl("ratingValueActivityEmployee1").UniqueID]); ;

            if (this.FindControl("ratingValueActivitySupervisor1") != null)
                //input.SupervisorRating = int.Parse(((TextBox)this.FindControl("ratingValueActivitySupervisor1")).Text.Trim());
                input.SupervisorRating = double.Parse(Request.Form[this.FindControl("ratingValueActivitySupervisor1").UniqueID]); ;


            return input;

        }

        public List<AppraisalValue> GetEmployeeQuestionList()
        {
            List<AppraisalValue> inputList = new List<AppraisalValue>();
            AppraisalForm form = AppraisalManager.getFormInstanceByID(this.AppraisalFormID);

            if (form == null)
                return inputList;


            AppraisalEmployeeForm empForm = AppraisalManager.GetEmployeeAppraisaleForm
              (GetEmployeeAppraisalFormID());

            List<AppraisalDynamicFormQuestionnaire> questionList = AppraisalManager.GetFormQuestionnaire(this.AppraisalFormID);

            AppraisalRatingScale ratingScale = null;
            if (form.QuestionnareRatingScaleRef_ID != null)
                ratingScale = AppraisalManager.GetRatingScaleById(form.QuestionnareRatingScaleRef_ID.Value);

            // if self employee is saving & Question has no self rating & comment then save all questions at once
            if (SessionManager.CurrentLoggedInEmployeeId == empForm.EmployeeId && empForm.Status == (int)AppraisalStatus.Saved
                && (form.AllowQuestionSelfRating != null && form.AllowQuestionSelfRating.Value == false)
                && (form.AllowQuestionSelfComment != null && form.AllowQuestionSelfComment.Value == false))
            {
                foreach (AppraisalDynamicFormQuestionnaire item in questionList)
                {
                    AppraisalValue input = new AppraisalValue();
                    input.QuestionnaireId = item.QuestionnareID;

                    inputList.Add(input);
                }
            }

            else
            {
                foreach (AppraisalDynamicFormQuestionnaire item in questionList)
                {

                    AppraisalValue input = new AppraisalValue();
                    input.QuestionnaireId = item.QuestionnareID;

                    HiddenField hiddenID = this.FindControl("Question_" + item.QuestionnareID) as HiddenField;

                    if (hiddenID == null)
                        continue;

                    TextBox txtComment = this.FindControl("commentInputQuestionEmployee" + item.QuestionnareID) as TextBox;

                    // if question not exists in this page
                    if (txtComment != null)
                        input.SelfComment = txtComment.Text.Trim();

                    if (form.AllowQuestionSelfRating.Value)
                    {
                        //input.SelfRating = int.Parse(((TextBox)this.FindControl("ratingValueQuestionEmployee" + item.QuestionnareID)).Text.Trim());
                        input.SelfRating = double.Parse(Request.Form[this.FindControl("ratingValueQuestionEmployee" + item.QuestionnareID).UniqueID]); ;
                        if (input.SelfRating == -1)
                            input.SelfRating = null;
                    }
                    if (this.FindControl("commentInputQuestionSupervisor" + item.QuestionnareID) != null)
                        input.SupervisorComment = ((TextBox)this.FindControl("commentInputQuestionSupervisor" + item.QuestionnareID)).Text.Trim();
                    
                    if (form.AllowQuestionSupRating != null && form.AllowQuestionSupRating.Value &&
                            this.FindControl("ratingValueQuestionSupervisor" + item.QuestionnareID) != null)
                    {
                        //input.SupervisorRating = int.Parse(((TextBox)this.FindControl("ratingValueQuestionSupervisor" + item.QuestionnareID)).Text.Trim());
                        input.SupervisorRating = double.Parse(Request.Form[this.FindControl("ratingValueQuestionSupervisor" + item.QuestionnareID).UniqueID]); ;
                        if (input.SupervisorRating == -1)
                            input.SupervisorRating = null;
                    }
                    inputList.Add(input);
                }
            }

            return inputList;

        }

        public List<AppraisalValue> GetEmployeeCompetencyList()
        {
            List<AppraisalValue> inputList = new List<AppraisalValue>();

            AppraisalForm form = AppraisalManager.getFormInstanceByID(this.AppraisalFormID);
            List<AppraisalDynamicFormCompetency> comptencyList = AppraisalManager.GetFormComptency(this.AppraisalFormID);
            AppraisalRatingScale ratingScale = null;
            if (form.CompentencyRatingScaleRef_ID != null)
                ratingScale = AppraisalManager.GetRatingScaleById(form.CompentencyRatingScaleRef_ID.Value);


            foreach (AppraisalDynamicFormCompetency item in comptencyList)
            {

                AppraisalValue input = new AppraisalValue();
                input.ComptencyId = item.CompetencyID;

                if (this.FindControl("commentInputCompetencyEmployee" + item.CompetencyID) != null)
                    input.SelfComment = ((TextBox)this.FindControl("commentInputCompetencyEmployee" + item.CompetencyID)).Text.Trim();

                if (form.AllowCompetencySelfRating.Value)
                    //input.SelfRating = int.Parse(((TextBox)this.FindControl("ratingValueCompetencyEmployee" + item.CompetencyID)).Text.Trim());
                    input.SelfRating = double.Parse(Request.Form[this.FindControl("ratingValueCompetencyEmployee" + item.CompetencyID).UniqueID]);

                if (this.FindControl("commentInputCompetencySupervisor" + item.CompetencyID) != null)
                    input.SupervisorComment = ((TextBox)this.FindControl("commentInputCompetencySupervisor" + item.CompetencyID)).Text.Trim();
                
                if (this.FindControl("ratingValueCompetencySupervisor" + item.CompetencyID) != null)
                    //input.SupervisorRating = int.Parse(((TextBox)this.FindControl("ratingValueCompetencySupervisor" + item.CompetencyID)).Text.Trim());
                    input.SupervisorRating = double.Parse(Request.Form[this.FindControl("ratingValueCompetencySupervisor" + item.CompetencyID).UniqueID]);
                
                inputList.Add(input);
            }

            return inputList;
        }

        public List<AppraisalValue> GetEmployeeTargetList()
        {
            List<AppraisalValue> inputList = new List<AppraisalValue>();

            AppraisalForm form = AppraisalManager.getFormInstanceByID(this.AppraisalFormID);
            List<AppraisalDynamicFormTarget> targetList = AppraisalManager.GetFormTarget(this.AppraisalFormID);
            AppraisalRatingScale ratingScale = null;
            if (form.CompentencyRatingScaleRef_ID != null)
                ratingScale = AppraisalManager.GetRatingScaleById(form.TargetRatingScaleRef_ID.Value);

           
            foreach (GridViewRow row in gridTargets.Rows)
            {
                DropDownList ddlYourRating = (DropDownList)row.FindControl("ddlYourRating");
                DropDownList ddlSupervisorRating = (DropDownList)row.FindControl("ddlSupervisorRating");
                TextBox txtYourComment = (TextBox)row.FindControl("txtYourComment");
                TextBox txtSupervisorComment = (TextBox)row.FindControl("txtSupervisorComment");
                System.Web.UI.WebControls.Label lblWeight = (System.Web.UI.WebControls.Label)row.FindControl("lblWeight");
                //TextBox txtAchievement = (TextBox)row.FindControl("txtAchievement");
                
                AppraisalValue input = null;

               

                if (txtYourComment.Visible && txtYourComment.Enabled)
                {
                    input = new AppraisalValue();

                    //double achievement = 0;
                    //if (double.TryParse(txtAchievement.Text.Trim(), out achievement))
                    //    input.TargetAchievement = achievement;

                    input.SelfRating = int.Parse(ddlYourRating.SelectedValue);
                    input.SelfComment = txtYourComment.Text.Trim();

                }
                else if (txtSupervisorComment.Visible && txtSupervisorComment.Enabled)
                {
                    input = new AppraisalValue();

                    input.SupervisorRating = int.Parse(ddlSupervisorRating.SelectedValue);
                    input.SupervisorComment = txtSupervisorComment.Text.Trim();
                }


                if (input != null)
                {
                    input.TargetID = int.Parse(gridTargets.DataKeys[row.RowIndex][0].ToString());

                    double weight;
                    if (double.TryParse(lblWeight.Text, out weight))
                        input.Weight = weight;

                    inputList.Add(input);

                }
            }


            return inputList;
        }
        public void LoadCompetencyDetails()
        {
            SetCompetencyTitle();

            AppraisalEmployeeForm empForm = AppraisalManager.GetEmployeeAppraisaleForm
               (GetEmployeeAppraisalFormID());
            AppraisalStatus mode = GetStepState(empForm);

            List<AppraisalCategory> comptencyCategoryList = NewHRManager.GetAppraisalFromCompetencyCategory(this.AppraisalFormID);
            List<AppraisalDynamicFormCompetency> comptencyList = AppraisalManager.GetFormComptency(this.AppraisalFormID);
            List<AppraisalEmployeeFormCompetency> employeeComptencyList = AppraisalManager.GetEmployeeCompetenchList(GetEmployeeAppraisalFormID());

            AppraisalRatingScale ratingScale = AppraisalManager.GetFormCompetencyRating(this.AppraisalFormID);

            if (comptencyCategoryList.Count > 0 && ratingScale == null)
            {
                NewMessage.ShowWarningMessage("Rating scale not defined for Comptency.");
                return;
            }

            int categoryIndex = 0;

            foreach (AppraisalCategory competencyCategory in comptencyCategoryList)
            {

                // Category Block
                HtmlGenericControl categoryBlock = new HtmlGenericControl("div");
                categoryBlock.ID = "categoryBlockCompetencyBlock" + competencyCategory.CategoryID;
                categoryBlock.Attributes["class"] = "categoryBlockCompetencyBlock";
                categoryBlock.InnerHtml = (++categoryIndex) + "&nbsp;&nbsp;&nbsp;" + competencyCategory.Name;
                block.Controls.Add(categoryBlock);

                int index = 1;
                foreach (AppraisalDynamicFormCompetency item in comptencyList.Where(x=>x.CategoryID==competencyCategory.CategoryID).ToList())
                {
                    AppraisalEmployeeFormCompetency empCompetency = employeeComptencyList.FirstOrDefault(x => x.CompetencyRef_ID == item.CompetencyID);

                    // then called from Preview form
                    if (empCompetency == null)
                    {
                        DisplayDynamic(empForm, ratingScale, block
                                 , null, null, null, null, "", "",
                                 "Competency", item.CompetencyID,

                                 index.ToString(), index + "&nbsp;&nbsp;&nbsp;" + item.Name,item.Description, AppraisalBlockType.Competency,null,""
                              );
                    }
                    else
                        DisplayDynamic(empForm, ratingScale, block
                                 , AppraisalManager.GetRatingIndex(ratingScale, empCompetency.SelfRating),
                                 empCompetency.SelfRating
                                 , AppraisalManager.GetRatingIndex(ratingScale, empCompetency.ManagerRating),
                                 empCompetency.ManagerRating,
                                 empCompetency.SelfComment, empCompetency.ManagerComment,
                                 "Competency", item.CompetencyID,

                                 index.ToString(), index + "&nbsp;&nbsp;&nbsp;" + item.Name, item.Description, AppraisalBlockType.Competency, null, ""
                              );


                    index += 1;
                }
            }
        }

        public void LoadTargetDetails()
        {
            SetTargetTitle();

            empForm =  AppraisalManager.GetEmployeeAppraisaleForm (GetEmployeeAppraisalFormID());
            AppraisalStatus mode = GetStepState(empForm);

            // show/hide target columns, for fields it will be controlled from DataBound event
            AppraisalForm form = AppraisalManager.getFormInstanceByID(GeteAppraisalFormID());
            if (form != null && form.TargetHideAssignedShowRating != null && form.TargetHideAssignedShowRating.Value)
            {
                //gridTargets.Columns[1].Visible = false;
                //gridTargets.Columns[2].Visible = false;

                gridTargets.Columns[3].Visible = true;
                gridTargets.Columns[4].Visible = true;
                gridTargets.Columns[6].Visible = true;
            }

            List<AppraisalDynamicFormTarget> targetList = AppraisalManager.GetFormTarget(this.AppraisalFormID);
            List<int> targetIDList = targetList.Select(x => x.TargetD).ToList();
            List<AppraisalEmployeeFormTarget> employeeComptencyList =
                AppraisalManager.GetEmployeeTargetList(GetEmployeeAppraisalFormID())
                .Where(x => x.TargetRef_ID != null && targetIDList.Contains(x.TargetRef_ID.Value)).ToList();

            // For Preivew create employee target list
            if (empForm == null && employeeComptencyList.Count==0)
            {
                foreach (AppraisalDynamicFormTarget item in targetList)
                {
                    employeeComptencyList.Add(new AppraisalEmployeeFormTarget { Name = item.Name, TargetRef_ID = item.TargetD  });
                }
            }

            foreach (AppraisalEmployeeFormTarget item in employeeComptencyList)
            {
                AppraisalDynamicFormTarget target = targetList.FirstOrDefault(x => x.TargetD == item.TargetRef_ID);

                if (target != null)
                {
                    item.Name = target.Name;
                    //item.Weight = target.Weight;
                }
                if (item.SelfRating == null)
                    item.SelfRating = -1;
                if (item.ManagerRating == null)
                    item.ManagerRating = -1;


                if (form.TargetHideAssignedShowRating != null && form.TargetHideAssignedShowRating.Value)
                {
                    item.TargetForDisplay = item.AssignedTargetTextValue;
                    item.AchievementForDisplay = item.AchievementTextValue;
                }
                else
                {
                    item.TargetForDisplay= item.AssignedTargetValue == null ? "" : item.AssignedTargetValue.ToString();
                    item.AchievementForDisplay = item.SelfAchievementValue == null ? "" : item.SelfAchievementValue.ToString();
                }
            }

            AppraisalRatingScale ratingScale = AppraisalManager.GetFormTargetRating(this.AppraisalFormID);

            targetRatings = AppraisalManager.GetTargetRating(GeteAppraisalFormID());
            targetRatings.Insert(0,new AppraisalRatingBO { Score = -1, Label = "" });

            cmbTarget.Store[0].DataSource = targetList;
            cmbTarget.Store[0].DataBind();

            gridTargets.DataSource = employeeComptencyList;
            gridTargets.DataBind();

            // for employee form and rating type then show assign/change target
            if (empForm != null && (empForm.Status ==null || empForm.Status == (int)AppraisalStatus.Saved)
                && SessionManager.CurrentLoggedInEmployeeId == empForm.EmployeeId)
            {

                if (form.TargetHideAssignedShowRating != null && form.TargetHideAssignedShowRating.Value)
                {
                    btnAddTarget.Visible = true;
                    window.Visible = true;
                    //resourceMgr.Visible = true;

                    gridTargets.Columns[8].Visible = true;
                }
            }
           

        }
        public void LoadActivityDetails(bool hideTitle)
        {
            if (hideTitle == false)
                SetActivityTitle();
            else
            {
                //spanActivityBlockNumber.Visible = false;
                spanActivityTitleName.Visible = false;
                divActivityTitleDescription.Visible = false;

            }

            //blockActivity.Visible = true;

            AppraisalForm form = AppraisalManager.getFormInstanceByID(this.AppraisalFormID);

            AppraisalEmployeeForm empForm = AppraisalManager.GetEmployeeAppraisaleForm
               (GetEmployeeAppraisalFormID());
            AppraisalStatus mode = GetStepState(empForm);

            AppraisalRatingScale ratingScale = AppraisalManager.GetFormActivityRating(this.AppraisalFormID);

            if (form.AllowActivitySelfRating != null && form.AllowActivitySelfRating.Value)
            {
                appraiseeBlock.Visible = true;
            }
            else
                appraiseeBlock.Visible = false;

            if (empForm != null)
            {
              

                appraiseeMarkingValue.Enabled = false;
                supervisorMarkingValue.Enabled = false;

                if (empForm.ActivitySelfRating != null)
                    appraiseeMarkingValue.Text = empForm.ActivitySelfRating.ToString();
                if (empForm.ActivitySupervisorRating != null)
                    supervisorMarkingValue.Text = empForm.ActivitySupervisorRating.ToString();

                supervisorCommentText.InnerHtml = empForm.ActivitySupervisorComment;
                spanRatingLevel.InnerHtml = new EmployeeManager().GetById(empForm.EmployeeId).Name + "'s Marks";
            }


            // for no marking case
            if (form.ActivityMarkingType == 0)
            {
                appraiseeBlock.Visible = false;
                supervisorBlock.Visible = false;
                //ClientScript("sdfdsfdsfs","document.getElementById('ctl00_mainContent_ctl02_containerBlockActivity1').style.display='none';");
            }




            //if (empForm == null)
            //{
            //    DisplayDynamic(empForm, ratingScale, blockActivity
            //             , null, null, null, null, "", "",
            //             "Activity", 1,

            //             "", "", BlockType.Activity
            //          );
            //}
            //else
            //    DisplayDynamic(empForm, ratingScale, blockActivity
            //             , AppraisalManager.GetRatingIndex(ratingScale, empForm.ActivitySelfRating),
            //             empForm.ActivitySelfRating,
            //             AppraisalManager.GetRatingIndex(ratingScale, empForm.ActivitySupervisorRating),
            //             empForm.ActivitySupervisorRating, "", "",
            //             "Activity", 1,

            //             "", "", BlockType.Activity
            //          );




        }
        public void LoadReviewDetails()
        {
            SetReviewTitle();

            AppraisalEmployeeForm empForm = AppraisalManager.GetEmployeeAppraisaleForm
               (GetEmployeeAppraisalFormID());

            AppraisalStatus mode = GetStepState(empForm);
            List<AppraisalDynamicFormReview> reviewList = AppraisalManager.GetFormReview(this.AppraisalFormID);
            List<AppraisalEmployeeFormReview> employeeReviewList = AppraisalManager.GetEmployeeReviewList(GetEmployeeAppraisalFormID());

            AppraisalRatingScale ratingScale = AppraisalManager.GetFormReviewRating(this.AppraisalFormID);

            int index = 1;
            foreach (AppraisalDynamicFormReview item in reviewList)
            {
                AppraisalEmployeeFormReview empReview = employeeReviewList.FirstOrDefault(x => x.ReviewRef_ID == item.ReviewID);

                // then called from Preview form
                if (empReview == null)
                {
                    DisplayDynamic(empForm, ratingScale, blockReview
                             , null, null, null, null, "", "",
                             "Review", item.ReviewID,

                             index.ToString(), index + "&nbsp;&nbsp;&nbsp;" + item.Name, "", AppraisalBlockType.Reivew, null, ""
                          );
                }
                else
                    DisplayDynamic(empForm, ratingScale, blockReview
                             , 0,0, AppraisalManager.GetRatingIndex(ratingScale, empReview.ManagerRating), 
                              empReview.ManagerRating,
                             "", empReview.ManagerComment,
                             "Review", item.ReviewID,

                             index.ToString(), index + "&nbsp;&nbsp;&nbsp;" + item.Name, "", AppraisalBlockType.Reivew, null, ""
                          );


                index += 1;
            }
        }

        private AppraisalForm SetCompetencyTitle()
        {
            //spanblockNumber.InnerHtml = competencyBlockNumber.ToString();
            block.Visible = true;

            AppraisalForm form = AppraisalManager.getFormInstanceByID(this.AppraisalFormID);
            //if (form == null)
            //    return;

            if (form != null)
            {
                spantitleName.InnerHtml = form.CompentencyName;
                spantitleDescription.InnerHtml = form.CompentencyDescription;
            }
            return form;
        }
        private AppraisalForm SetTargetTitle()
        {
            
            //spanblockNumber.InnerHtml = competencyBlockNumber.ToString();
            

            AppraisalForm form = AppraisalManager.getFormInstanceByID(this.AppraisalFormID);

            if (form.HideTarget != null && form.HideTarget.Value)
                blockTarget.Visible = false;
            else
                blockTarget.Visible = true;
            //if (form == null)
            //    return;

            if (form != null)
            {
                spanTargetTitleName.InnerHtml = form.TargetName;
                spanTargetTitleDescription.InnerHtml = form.TargetDescription;
            }
            return form;
        }
        public void SetActivitySummary()
        {
            AppraisalEmployeeForm empForm = AppraisalManager.GetEmployeeAppraisaleForm(GetEmployeeAppraisalFormID());

           
            if (AppraisalManager.HasActivitySummaryForForm(empForm.AppraisalFormRef_ID))
                blockActivity.Visible = true;
            else
                blockActivity.Visible = false;

            if (empForm != null)
            {
                acitivtySummary.InnerHtml = empForm.ActivitySummary;
                acitivtySummary.Visible = true;

                if (empForm.ActivitySummaryFileName != null)
                    btnDownload.Text = empForm.ActivitySummaryFileName;
                else
                    btnDownload.Hide();
                //spanActivityEmpSummary.Visible = true;
            }
        }
        private AppraisalForm SetActivityTitle()
        {
            //spanActivityBlockNumber.InnerHtml = activityBlockNumber.ToString();
            blockActivity.Visible = true;

            AppraisalForm form = AppraisalManager.getFormInstanceByID(this.AppraisalFormID);

            if (form.HideActivitySummary != null && form.HideActivitySummary.Value)
                blockActivity.Visible = false;
            else
                blockActivity.Visible = true;

            if (form != null)
            {
                spanActivityTitleName.InnerHtml = form.ActivityName;
                divActivityTitleDescription.InnerHtml = form.ActivityDescription;
            }
            return form;
        }

        protected void btnDownload_Click(object sendeer, DirectEventArgs e)
        {
            DownloadFile();
        }

        protected void DownloadFile()
        {
            AppraisalEmployeeForm empForm = AppraisalManager.GetEmployeeAppraisaleForm(GetEmployeeAppraisalFormID());
            AppraisalEmployeeForm obj = AppraisalManager.GetEmployeeAppraisaleForm(empForm.EmployeeId, empForm.AppraisalFormRef_ID);
            string path = Context.Server.MapPath(obj.ActivitySummaryFileUrl + obj.ActivitySummaryServerFileName);
            string contentType = obj.FileType;
            string name = obj.ActivitySummaryFileName;
            byte[] bytes = File.ReadAllBytes(path);
            Context.Response.Clear();
            Context.Response.ClearHeaders();
            Context.Response.AppendHeader("Content-Disposition", "attachment; filename=\"" + name + "\"");
            Context.Response.ContentType = contentType;
            Context.Response.BinaryWrite(bytes);
            Context.Response.End();
        }
        private AppraisalForm SetReviewTitle()
        {
            //spanReviewBlockNumber.InnerHtml = reviewBlockNumber.ToString();
            blockReview.Visible = true;

            AppraisalForm form = AppraisalManager.getFormInstanceByID(this.AppraisalFormID);

            if (form.HideSupervisorReview != null && form.HideSupervisorReview.Value)
                blockReview.Visible = false;
            else
                blockReview.Visible = true;

            if (form != null)
            {
                spanReviewTitleName.InnerHtml = form.ReviewName;
                divReviewTitleDescription.InnerHtml = form.ReviewDescription;
            }
            return form;
        }
        public string GetCommenterName(bool isSupervisor, AppraisalEmployeeForm form, bool? isComment)
        {
            string text = "";
            if (isComment == null)
                text = "Achievement";
            else if (isComment.Value)
                text = "Comment";
            else
                text = "Rating";



            if (isSupervisor == false)
            {
                if (form == null)
                    return "Appraisee's " + text;

                return  EmployeeManager.GetEmployeeName(form.EmployeeId) + "'s " + text;
            }
            else
                return "Supervisor's " + text;

            
        }

        public AppraisalStatus GetStepState(AppraisalEmployeeForm form)
        {
            if (form == null)
                return AppraisalStatus.Saved;

            if (form.EmployeeId == SessionManager.CurrentLoggedInEmployeeId)
            {
                if (form.Status == null || form.Status == 0)
                {
                    return AppraisalStatus.Saved;
                }
                else if (form.Status == (int)AppraisalStatus.EmployeeReviewed)
                    return AppraisalStatus.EmployeeReviewed;
                else
                    return AppraisalStatus.NotEditable;
            }
            else
            {
                return AppraisalStatus.SupervisorManagerCommented;
            }
        }

        public void ClearQuestionBlock()
        {
            blockQuestionnareList.Controls.Clear();
        }
        public void LoadQuestionnaireDetails(int start, int limit)
        {
            SetQuestionTitle();

            start = (start * limit);

            AppraisalEmployeeForm empForm = AppraisalManager.GetEmployeeAppraisaleForm(GetEmployeeAppraisalFormID());

            List<AppraisalFormQuestionnaireCategory> categoryList = AppraisalManager.GetQuestionCategoryList(this.AppraisalFormID);
            List<AppraisalDynamicFormQuestionnaire> questionList = AppraisalManager.GetFormQuestionnaire(this.AppraisalFormID);
            List<AppraisalEmployeeFormQuestionnaire> empQuestionList = AppraisalManager.GetEmployeeQuestionList(GetEmployeeAppraisalFormID());
            List<AppraisalDynamicFormQuestionnaire> groupedSortedQuestionList = new List<AppraisalDynamicFormQuestionnaire>();

            AppraisalStatus mode = GetStepState(empForm);
            AppraisalRatingScale ratingScale = AppraisalManager.GetFormQuestionRating(this.AppraisalFormID);

            if (categoryList.Count > 0 && ratingScale == null)
            {
                NewMessage.ShowWarningMessage("Rating scale not defined for questionnaire.");
                return;
            }

            //set question number
            int categoryIndex = 1;
            foreach (AppraisalFormQuestionnaireCategory category in categoryList)
            {
                List<AppraisalDynamicFormQuestionnaire> questionListing =
                    questionList.Where(x => x.CategoryID == category.CategoryID).OrderBy(x => x.Order).ToList();

                if (questionListing.Count <= 0)
                    continue;

                int index = 1;

                category.Number = (categoryIndex.ToString());

                foreach (AppraisalDynamicFormQuestionnaire item in questionListing)
                {
                    item.Number = (categoryIndex) + "." + index;
                    index += 1;
                }
                categoryIndex += 1;

                // group all question using category ordering for Paging
                groupedSortedQuestionList.AddRange(questionListing);
            }

           


            //Paging code
            if (start >= 0 && limit > 0)
            {
                groupedSortedQuestionList = groupedSortedQuestionList.Skip(start).Take(limit).ToList();
            }

            categoryIndex = 1;
            blockQuestionnareList.Controls.Clear();

            foreach (AppraisalFormQuestionnaireCategory category in categoryList)
            {
                List<AppraisalDynamicFormQuestionnaire> questionListing =
                    groupedSortedQuestionList.Where(x => x.CategoryID == category.CategoryID).OrderBy(x => x.Order).ToList();

                if (questionListing.Count <= 0)
                    continue;

                // Category Block
                HtmlGenericControl categoryBlock = new HtmlGenericControl("div");
                categoryBlock.ID = "categoryBlockQuestionBlock" + category.CategoryID;
                categoryBlock.Attributes["class"] = "categoryBlockQuestionBlock";
                categoryBlock.InnerHtml = category .Number + "&nbsp;&nbsp;&nbsp;" + category.Name;
                blockQuestionnareList.Controls.Add(categoryBlock);

                int index = 1;

                foreach (AppraisalDynamicFormQuestionnaire item in questionListing)
                {
                    AppraisalEmployeeFormQuestionnaire empQuestion = empQuestionList.FirstOrDefault(x => x.QuestionnaireRef_ID == item.QuestionnareID);


                    if (empQuestion == null)
                    {
                        DisplayDynamic(empForm, ratingScale, blockQuestionnareList
                        , null, null, null, null, "", "",
                        "Question", item.QuestionnareID,

                        item.Number,
                        item.Number + "&nbsp;&nbsp;&nbsp;" + item.Name, "", AppraisalBlockType.Question, null, item.Description
                     );
                    }
                    else
                        DisplayDynamic(empForm, ratingScale, blockQuestionnareList
                            , AppraisalManager.GetRatingIndex(ratingScale, empQuestion.SelfRating),
                            empQuestion.SelfRating,
                            AppraisalManager.GetRatingIndex(ratingScale, empQuestion.ManagerRating),
                            empQuestion.ManagerRating,
                            empQuestion.SelfComment, empQuestion.ManagerComment,
                            "Question", item.QuestionnareID,

                            item.Number,
                            item.Number + "&nbsp;&nbsp;&nbsp;" + item.Name, "", AppraisalBlockType.Question, null,item.Description
                         );

                    index += 1;
                }

                categoryIndex += 1;
            }
        }

         private HtmlGenericControl DisplayDynamic(AppraisalEmployeeForm empForm, AppraisalRatingScale ratingScale,
             HtmlGenericControl block,
             double? selfRating,double? selfRatingIndex, double? supervisorRating,double? supervisorRatingIndex,
             string selfComment, string supervisorComment,
             string blockIDText, int blockID,string numberText, 
             string titleText,string description,AppraisalBlockType blockType,double? selfTargetAchivement
             ,string bodyOrInnerDescription)
         {

             HtmlGenericControl containerBlock = new HtmlGenericControl("div");
             containerBlock.ID = "containerBlock" + blockIDText + blockID;
             containerBlock.Attributes["class"] = "ratingContainerBlock";
             block.Controls.Add(containerBlock);


             HtmlGenericControl title = new HtmlGenericControl("div");
             title.ID = "title" + blockIDText + blockID;
             title.Attributes["class"] = "ratingTitleBlock";
             title.InnerHtml = titleText;
             containerBlock.Controls.Add(title);

             if (!string.IsNullOrEmpty(description))
             {
                 HtmlGenericControl desc = new HtmlGenericControl("div");
                 desc.ID = "description" + blockIDText + blockID;
                 desc.Attributes["class"] = "descriptionBlock";
                 desc.InnerHtml = description;
                 containerBlock.Controls.Add(desc);
             }

             if (!string.IsNullOrEmpty(bodyOrInnerDescription))
             {
                 HtmlGenericControl desc = new HtmlGenericControl("div");
                 desc.ID = "innerdescription" + blockIDText + blockID;
                 desc.Attributes["class"] = "innerdescriptionBlock";
                 desc.InnerHtml = bodyOrInnerDescription;
                 containerBlock.Controls.Add(desc);
             }

             HiddenField hiddenID = new HiddenField();
             hiddenID.ID = blockIDText + "_" + blockID;
             block.Controls.Add(hiddenID);

             // for Review type it will be visible in Second level only
             if (blockType == AppraisalBlockType.Reivew)
             {
                 // if not status 0 then only show
                 if (empForm != null && (empForm.Status == null || empForm.Status == 0))
                 {
                     // not visible
                 }
                 else
                 {
                     DisplayDynamicCommentRatingBlock(empForm,
                      ratingScale, supervisorRating,supervisorRatingIndex, supervisorComment,
                      blockIDText + "Supervisor" + blockID, containerBlock, true, blockType, numberText,selfTargetAchivement);
                 }
             }
             else
             {
                 DisplayDynamicCommentRatingBlock(empForm,
                     ratingScale, selfRating,selfRatingIndex, selfComment,
                     blockIDText + "Employee" + blockID, containerBlock, false, blockType, numberText, selfTargetAchivement);

                 // if not status 0 then only show
                 if (empForm == null || (empForm.Status != null && empForm.Status != 0))
                     DisplayDynamicCommentRatingBlock(empForm,
                         ratingScale, supervisorRating,supervisorRatingIndex, supervisorComment,
                         blockIDText + "Supervisor" + blockID, containerBlock, true, blockType, numberText, selfTargetAchivement);
             }

             HtmlGenericControl clearDiv = new HtmlGenericControl("div");
             clearDiv.ID = "clearDiv" + blockIDText + blockID;
             clearDiv.Attributes["class"] = "ratingClear";
             containerBlock.Controls.Add(clearDiv);

             return title;

         }

         private void DisplayDynamicCommentRatingBlock(AppraisalEmployeeForm empForm,
             AppraisalRatingScale ratingScale, double? selfRating, double? selfRatingIndex,
             string selfComment, string blockIDText, HtmlGenericControl containerBlock, bool isSupervisor, AppraisalBlockType blockType
             , string numberText, double? selfTargetAchivement)
         {
             // Comment BLock
             HtmlGenericControl commentBlock = new HtmlGenericControl("div");
             commentBlock.Attributes["class"] = "ratingCommentBlock";
             commentBlock.ID = "commentBlock" + blockIDText;
             containerBlock.Controls.Add(commentBlock);

             HtmlGenericControl rating = null;      
            // Rating dropdown or marks required field validation      
             RequiredFieldValidator valQuestionRating = null;
            // Marks textbox range validation
            RangeValidator valQuestionRange = null;
            // Rating dropdown 
             System.Web.UI.WebControls.DropDownList ddl = null;
            // Marks textbox
             TextBox txt = null;
             HtmlGenericControl ratingDisplayLabeling = null;
            

             AppraisalForm form = AppraisalManager.getFormInstanceByID(this.AppraisalFormID);
             bool allowSelfRating = true;
             bool allowSelfComment = true;

             bool allowSupRating = true;
             bool allowSupComment = true;

             if (blockType == AppraisalBlockType.Activity && isSupervisor == false)
                 allowSelfRating = form.AllowActivitySelfRating.Value;
             else if (blockType == AppraisalBlockType.Competency && isSupervisor == false)
                 allowSelfRating = form.AllowCompetencySelfRating.Value;
             else if (blockType == AppraisalBlockType.Question && isSupervisor == false)
                 allowSelfRating = form.AllowQuestionSelfRating.Value;

             if (blockType == AppraisalBlockType.Competency && isSupervisor == false && form.AllowCompetencySelfComment != null)
                 allowSelfComment = form.AllowCompetencySelfComment.Value;
             else if (blockType == AppraisalBlockType.Question && isSupervisor == false && form.AllowQuestionSelfComment != null)
                 allowSelfComment = form.AllowQuestionSelfComment.Value;

             if (blockType == AppraisalBlockType.Question && isSupervisor == true)
                 allowSupRating = form.AllowQuestionSupRating == null ? false : form.AllowQuestionSupRating.Value;

             if (blockType == AppraisalBlockType.Question && isSupervisor == true && form.AllowQuestionSupComment != null)
                 allowSupComment = form.AllowQuestionSupComment.Value;

             // Achievement Input Box for employee only mode
             if (blockType == AppraisalBlockType.Target)
             {
                 if (isSupervisor == false)
                 {
                     HtmlGenericControl achievementTitle = new HtmlGenericControl("span");
                     achievementTitle.Attributes["class"] = "ratingLabelBlock";
                     achievementTitle.ID = "achievementTitle" + blockIDText;
                     commentBlock.Controls.Add(achievementTitle);
                     achievementTitle.InnerHtml = GetCommenterName(isSupervisor, empForm, null);
                     achievementTitle.Style["margin-top"] = GetCommentBoxTopMargin(allowSelfRating);

                     // Comment
                     TextBox achievementInput = new TextBox();
                     //achievementInput.Attributes["class"] = "ratingInputBlock";
                     achievementInput.Style["margin-bottom"] = "10px";
                     achievementInput.ID = "achievementInput" + blockIDText;
                     commentBlock.Controls.Add(achievementInput);
                     if (selfTargetAchivement != null)
                         achievementInput.Text = selfTargetAchivement.ToString();

                     if (empForm != null && empForm.Status != null && empForm.Status != 0)
                     {
                         achievementInput.Enabled = false;
                     }
                     
                 }
                 else
                 {
                     // place top margin for blank Supervisor Achievement
                     HtmlGenericControl achievementMarginTitle = new HtmlGenericControl("div");
                     achievementMarginTitle.Style["margin-bottom"] = "48px";
                     achievementMarginTitle.InnerHtml = "&nbsp;";
                     commentBlock.Controls.Add(achievementMarginTitle);

                 }
             }
            if (blockType != AppraisalBlockType.Reivew &&
                // in employee form mode check Allow Self rating & only display emp rating 
                (
                   (isSupervisor == true && allowSupRating)
                   ||
                   (isSupervisor == false && allowSelfRating)
                )
                )
            {
                // Rating Level
                HtmlGenericControl spanRatingLevel = new HtmlGenericControl("span");
                spanRatingLevel.Attributes["class"] = "spanRatingLevelClass";
                spanRatingLevel.ID = "spanRatingLevel" + blockIDText;
                commentBlock.Controls.Add(spanRatingLevel);
                spanRatingLevel.InnerHtml = GetCommenterName(isSupervisor, empForm, false);

                // in case of Marks type for Questionannire set marking min-max value in the label also
                if (blockType == AppraisalBlockType.Question &&
                  empForm != null && empForm.AppraisalForm != null && empForm.AppraisalForm.QuestionaireMarkingType != null &&
                  (empForm.AppraisalForm.QuestionaireMarkingType == (int)AppraisalMarkingTypeEnum.Marks || empForm.AppraisalForm.QuestionaireMarkingType == (int)AppraisalMarkingTypeEnum.PercentWithEachQuestionWeightage ) && ratingScale != null)
                {
                    spanRatingLevel.InnerHtml = spanRatingLevel.InnerHtml.Replace("Rating", "Marks");

                    spanRatingLevel.InnerHtml += string.Format(" ({0}-{1})",
                            ratingScale.AppraisalRatingScaleLines.OrderBy(x => x.Score.Value).FirstOrDefault().Score,
                            ratingScale.AppraisalRatingScaleLines.OrderByDescending(x => x.Score.Value).FirstOrDefault().Score);
                }

                CreateDynamicStarRatingControl(ratingScale, ref ddl, ref ratingDisplayLabeling, ref selfRating, ref selfRatingIndex, blockIDText, containerBlock, blockType, numberText, commentBlock,
                 ref rating, ref valQuestionRating, empForm,ref txt,ref valQuestionRange);


            }

             TextBox commentInput = null;
             if (blockType != AppraisalBlockType.Activity)
             {

                 if (
                    (isSupervisor == true && allowSupComment)
                    ||
                    (isSupervisor == false && allowSelfComment)
                 )
                 {
                     // Comment label
                     HtmlGenericControl commentTitle = new HtmlGenericControl("span");
                     commentTitle.Attributes["class"] = "ratingLabelBlock";
                     commentTitle.ID = "commentTitle" + blockIDText;
                     commentBlock.Controls.Add(commentTitle);
                     commentTitle.InnerHtml = GetCommenterName(isSupervisor, empForm, true);
                     commentTitle.Style["margin-top"] = GetCommentBoxTopMargin(allowSelfRating);

                     // Comment
                     commentInput = new TextBox();
                     commentInput.Attributes["class"] = "ratingInputBlock";
                     commentInput.ID = "commentInput" + blockIDText;
                     commentBlock.Controls.Add(commentInput);
                     commentInput.TextMode = TextBoxMode.MultiLine;
                     commentInput.Text = selfComment;

                     if (valQuestionRating != null && commentInput != null)
                     {
                         valQuestionRating.Attributes["txtCommentID"] = commentInput.ClientID;
                         valQuestionRating.Text = commentInput.ClientID;
                     }

                    if (valQuestionRange != null && commentInput != null)
                    {
                        valQuestionRange.Attributes["txtCommentID"] = commentInput.ClientID;
                        valQuestionRange.Text = commentInput.ClientID;
                    }
                }
             }


             if (empForm != null)
             {
                 // for Employee Block
                 if (isSupervisor == false)
                 {
                     if ((empForm.Status != null && empForm.Status != 0) ||
                            SessionManager.CurrentLoggedInEmployeeId == 0)
                     {
                         if (rating != null)
                             rating.Visible = true;
                        
                         if (ddl != null)
                             ddl.Style["display"] = "none";
                        if (txt != null)
                        {
                            txt.ReadOnly = true;//.Enabled = false;//Style["display"] = "none";
                            txt.Style["background-color"] = "#F3F3F3";
                        }
                        if (ratingDisplayLabeling != null)
                             ratingDisplayLabeling.Visible = true;

                         if (rating != null)
                             rating.Attributes["data-rateit-readonly"] = "true";
                         if (commentInput != null)
                             commentInput.Style["display"] = "none";
                         if (valQuestionRating != null)
                             valQuestionRating.Visible = false;
                        if (valQuestionRange != null)
                            valQuestionRange.Visible = false;
                        // Comment
                        HtmlGenericControl commentDiv = new HtmlGenericControl("span");
                         commentDiv.ID = "commentDiv" + blockIDText;
                         commentBlock.Controls.Add(commentDiv);
                         commentDiv.InnerHtml = selfComment;
                     }
                 }
                 else
                 {
                     // Supervisor block will readonly for Self employee or if not in SavedAndSend status
                     if (
                         (empForm != null && empForm.EmployeeId == SessionManager.CurrentLoggedInEmployeeId)
                         ||
                         empForm.Status != (int)AppraisalStatus.SaveAndSend
                         ||
                         SessionManager.CurrentLoggedInEmployeeId == 0
                    )
                     {

                         if (rating != null)
                             rating.Visible = true;
                         
                         if (ddl != null)
                             ddl.Style["display"] = "none";
                        if (txt != null)
                        {
                            txt.Style["background-color"] = "#F3F3F3";
                            txt.ReadOnly = true; //Enabled = false;//Style["display"] = "none";
                        }
                        if (ratingDisplayLabeling != null && !string.IsNullOrEmpty(ratingDisplayLabeling.InnerHtml))
                             ratingDisplayLabeling.Visible = true;

                         if (rating != null)
                             rating.Attributes["data-rateit-readonly"] = "true";
                         if (commentInput != null)
                             commentInput.Style["display"] = "none";
                         if (valQuestionRating != null)
                             valQuestionRating.Visible = false;
                        if (valQuestionRange != null)
                            valQuestionRange.Visible = false;

                        // Comment
                        HtmlGenericControl commentDiv = new HtmlGenericControl("span");
                         commentDiv.ID = "commentDiv" + blockIDText;
                         commentBlock.Controls.Add(commentDiv);
                         commentDiv.InnerHtml = selfComment;
                     }
                 }

             }
         }


         #region "Grid Data Binding Events"
         protected void gridTargets_RowDataBound(object sender, GridViewRowEventArgs e)
         {
            

             if (e.Row.RowType == DataControlRowType.DataRow)
             {

                 //int employeeId = (int)gridTargets.DataKeys[e.Row.RowIndex]["EmployeeId"];
                 AppraisalEmployeeFormTarget target = e.Row.DataItem as AppraisalEmployeeFormTarget;

                 DropDownList ddlYourRating = (DropDownList)e.Row.FindControl("ddlYourRating");
                 TextBox txtYourComment = (TextBox)e.Row.FindControl("txtYourComment");
                 System.Web.UI.WebControls.Label lblYourComment = (System.Web.UI.WebControls.Label)e.Row.FindControl("lblTargetYourComment");

                 ddlYourRating.DataSource = targetRatings;
                 ddlYourRating.DataBind();
                 UIHelper.SetSelectedInDropDown(ddlYourRating, target.SelfRating == null ? 0 : (int)target.SelfRating);


                  DropDownList ddlSupervisorRating = (DropDownList)e.Row.FindControl("ddlSupervisorRating");
                  TextBox txtSupervisorComment = (TextBox)e.Row.FindControl("txtSupervisorComment");
                  System.Web.UI.WebControls.Label lblSupervisorComment = (System.Web.UI.WebControls.Label)e.Row.FindControl("lblTargetSupervisorComment");

                  ddlSupervisorRating.DataSource = targetRatings;
                  ddlSupervisorRating.DataBind();
                  UIHelper.SetSelectedInDropDown(ddlSupervisorRating, target.ManagerRating == null ? 0 : (int)target.ManagerRating);
                

                 //TextBox txtAchievement = (TextBox)e.Row.FindControl("txtAchievement");

                 if (empForm != null)
                 {
                     // Emp not editable and Supervisor editable
                     if (empForm.Status == (int)AppraisalStatus.Saved && SessionManager.CurrentLoggedInEmployeeId == empForm.EmployeeId)
                     {
                         ddlYourRating.Visible = true;

                     }
                     else if (empForm.Status == (int)AppraisalStatus.SaveAndSend)
                     {
                         ddlYourRating.Visible = true;
                         //txtAchievement.Enabled = false;
                         ddlYourRating.Enabled = false;
                         //txtYourComment.Enabled = false;
                         txtYourComment.Visible = false;
                         lblYourComment.Visible = true;

                         ddlSupervisorRating.Visible = true;
                         txtSupervisorComment.Visible = true;

                         // if same employee then hide it supervisor block
                         if (SessionManager.CurrentLoggedInEmployeeId == empForm.EmployeeId)
                         {
                             ddlSupervisorRating.Visible = false;
                             txtSupervisorComment.Visible = false;
                         }
                     }
                         // Both Not Editable
                     else
                     {
                         //txtAchievement.Enabled = false;
                         ddlYourRating.Enabled = false;
                         //txtYourComment.Enabled = false;
                         txtYourComment.Visible = false;
                         lblYourComment.Visible = true;

                         ddlYourRating.Visible = true;
                         ddlSupervisorRating.Visible = true;
                         txtSupervisorComment.Visible = true;

                         ddlSupervisorRating.Enabled = false;
                         //txtSupervisorComment.Enabled = false;
                         txtSupervisorComment.Visible = false;
                         lblSupervisorComment.Visible = true;
                     }
                 }

                 
                 //LeaveProjectEmployee emp = LeaveRequestManager.GetLeaveProjectForEmployee(employeeId);
                 //if (emp != null)
                 //{
                 //    ListItem item = ddList.Items.FindByValue(emp.LeaveProjectId.ToString());
                 //    if (item != null)
                 //        item.Selected = true;
                 //}

             }
         }
         #endregion


        /// <summary>
        /// Display Rating scale dropdown or marking field for block like Questionaire
        /// </summary>
         private void CreateDynamicStarRatingControl(AppraisalRatingScale ratingScale,
             ref System.Web.UI.WebControls.DropDownList ddl, ref HtmlGenericControl ratingDisplayLabeling,
             ref double? selfRating, ref double? selfRatingIndex, string blockIDText, HtmlGenericControl containerBlock, 
             AppraisalBlockType blockType, string numberText, HtmlGenericControl commentBlock
             , ref HtmlGenericControl ratingControl, ref RequiredFieldValidator valQuestionRating,AppraisalEmployeeForm empForm
             ,ref TextBox txt, ref RangeValidator valQuestionRange)
         {

            // for Questionnare if Marking is set instead of Rating scale then show textbox instead of dropdown
            if (blockType == AppraisalBlockType.Question &&
                   empForm != null && empForm.AppraisalForm != null && empForm.AppraisalForm.QuestionaireMarkingType != null &&
                   ( empForm.AppraisalForm.QuestionaireMarkingType == (int)AppraisalMarkingTypeEnum.Marks || empForm.AppraisalForm.QuestionaireMarkingType == (int)AppraisalMarkingTypeEnum.PercentWithEachQuestionWeightage))
            {
                txt = new TextBox();
                txt.ID = "ratingValue" + blockIDText;
                commentBlock.Controls.Add(txt);

                if (selfRatingIndex != null)
                    txt.Text = selfRatingIndex.ToString();

                // Required field validator
                valQuestionRating = new RequiredFieldValidator();
                valQuestionRating.ID = "ratingValidation" + blockIDText;
                valQuestionRating.Display = ValidatorDisplay.None;
                valQuestionRating.ErrorMessage = "Marks is required for the " + blockType.ToString().ToLower() + " number " + numberText + ".";
                valQuestionRating.ControlToValidate = "ratingValue" + blockIDText;
                valQuestionRating.ValidationGroup = "Appraisal";
                containerBlock.Controls.Add(valQuestionRating);

                // Range validation for Marks field
                valQuestionRange = new RangeValidator();
                valQuestionRange.ID = "rangeValidation" + blockIDText;
                valQuestionRange.Display = ValidatorDisplay.None;
                valQuestionRange.ErrorMessage = "Invalid marks for the " + blockType.ToString().ToLower() + " number " + numberText + ".";
                valQuestionRange.ControlToValidate = "ratingValue" + blockIDText;
                valQuestionRange.ValidationGroup = "Appraisal";
                valQuestionRange.MinimumValue = "0";// ratingScale.AppraisalRatingScaleLines.OrderBy(x => x.Score).FirstOrDefault().Score.ToString();
                valQuestionRange.MaximumValue = ratingScale.AppraisalRatingScaleLines.OrderByDescending(x => x.Score).FirstOrDefault().Score.ToString();
                valQuestionRange.Type = ValidationDataType.Double;
                containerBlock.Controls.Add(valQuestionRange);
            }
            else
            {
                ddl = new System.Web.UI.WebControls.DropDownList();
                ddl.ID = "ratingValue" + blockIDText;
                List<AppraisalRatingScaleLine> lines = ratingScale.AppraisalRatingScaleLines.OrderByDescending(x => x.Score.Value).ToList();
                commentBlock.Controls.Add(ddl);

                ddl.Items.Clear();
                ddl.Items.Add(new System.Web.UI.WebControls.ListItem { Text = "", Value = "-1" });
                for (int i = 0; i < lines.Count; i++)
                {

                    string text = lines[i].Label + " (" + lines[i].Score.ToString() + ")";
                    ddl.Items.Add(new System.Web.UI.WebControls.ListItem { Text = text, Value = lines[i].Score.ToString() });
                }
                if (selfRating != null)
                    ddl.SelectedValue = selfRatingIndex.ToString();


                // selected rating display value
                ratingDisplayLabeling = new HtmlGenericControl("span");
                ratingDisplayLabeling.Visible = false;
                ratingDisplayLabeling.ID = "ratingDisplayLabel" + blockIDText;
                ratingDisplayLabeling.Style["font-weight"] = "bold";
                ratingDisplayLabeling.Attributes["class"] = "ratingReadonlyText";
                commentBlock.Controls.Add(ratingDisplayLabeling);


                string ratingDisplayValue1 = GetRatingDisplayValue(ratingScale, selfRatingIndex);

                if (!string.IsNullOrEmpty(ratingDisplayValue1))
                    ratingDisplayLabeling.InnerHtml = ratingDisplayValue1 + " (" + (selfRatingIndex == null ? "" : selfRatingIndex.ToString()) + ")";
                else
                    ratingDisplayLabeling.InnerHtml = "";

                if (string.IsNullOrEmpty(ratingDisplayLabeling.InnerHtml))
                    ratingDisplayLabeling.Visible = false;


                // in preview mode do not add validation control as Citizen appraisal form error coming in preview mode, but in other mode it is ok
                if (GetEmployeeAppraisalFormID() != 0)
                {

                    valQuestionRating = new RequiredFieldValidator();
                    valQuestionRating.ID = "ratingValidation" + blockIDText;
                    valQuestionRating.Display = ValidatorDisplay.None;
                    valQuestionRating.ErrorMessage = "Rating is required for the " + blockType.ToString().ToLower() + " number " + numberText + ".";
                    valQuestionRating.ControlToValidate = "ratingValue" + blockIDText;
                    valQuestionRating.InitialValue = "-1";
                    valQuestionRating.ValidationGroup = "Appraisal";
                    containerBlock.Controls.Add(valQuestionRating);
                }



                // Rating
                //http://www.radioactivethinking.com/rateit/example/example.htm
                ratingControl = new HtmlGenericControl("div");
                ratingControl.Visible = false;
                ratingControl.ID = "rating" + blockIDText;
                ratingControl.Attributes["class"] = "rateit";
                commentBlock.Controls.Add(ratingControl);
                ratingControl.Attributes["data-rateit-value"] = selfRatingIndex == null ? "0" : selfRatingIndex.ToString();
                //ratingControl.Attributes["data-rateit-backingfld"] = "#" + ratingControlValue.UniqueID.Replace("$", "_");
                ratingControl.Attributes["data-rateit-ispreset"] = "true";
                ratingControl.Attributes["data-rateit-readonly"] = "true";

            }


            
         }

        /// <summary>
        /// Return top margin for preview
        /// </summary>
        /// <param name="allowSelfRating"></param>
        /// <returns></returns>
         public string GetCommentBoxTopMargin(bool allowSelfRating)
         {
             if (string.IsNullOrEmpty(Request.QueryString["efid"]))
             {
                 if (allowSelfRating == false)
                     return "56px;";
             }
             return "";
         }
        public string GetRatingDisplayValue(AppraisalRatingScale ratingScale, double? selfRating)
        {
            if (ratingScale == null || selfRating == null)
                return "";


            if (ratingScale != null)
            {

                AppraisalRatingScaleLine item =
                    ratingScale.AppraisalRatingScaleLines.FirstOrDefault(x => x.Score.ToString() == selfRating.ToString());
                if (item != null)
                    return item.Label;
            }

            return "";
        }
        public void SetQuestionTitle()
        {
            //spanQuestionBlockNumber.InnerHtml = questionnareBlockNumber.ToString();
            blockQuestionnare.Visible = true;

            AppraisalForm form = AppraisalManager.getFormInstanceByID(this.AppraisalFormID);

            if (form == null)
                return;

            if (form != null)
            {
                spanQuestionTitleName.InnerHtml = form.QuestionnareName;
                divQuestionTitleDescription.InnerHtml = form.QuestionnareDescription;
            }
        }


        private void RegisterRatingTooltip(string registerID, AppraisalRatingScale ratingScale, HtmlGenericControl rating)
        {
            bool added = false;
            string array = "[";

            if (ratingScale != null)
            {
                foreach (AppraisalRatingScaleLine item in ratingScale.AppraisalRatingScaleLines.OrderBy(x => x.Score))
                {
                    if (added)
                    {
                        array += ",";
                    }

                    array += "'" + item.Label + "'";

                    added = true;
                }
            }

            array += "]";

            string jsCode =
                @"var {1} = {2};
                    $('{0}').bind('over', function (event, value) {{
                    
                        if(value == 0)
                            $(this).attr('title', 'Clear Tooltip'); 
                        else 
                            $(this).attr('title', {1}[value-1]); 
                        
                    }});
                    ";
            jsCode = string.Format(jsCode, "#" + rating.UniqueID.Replace("$", "_"), registerID, array);

            //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "JS" + item.CompetencyID, jsCode, true);
            Page.ClientScript.RegisterStartupScript(this.GetType(), registerID, jsCode, true);
        }
    }
}