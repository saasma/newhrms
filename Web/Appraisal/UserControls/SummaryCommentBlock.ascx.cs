﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using BLL.BO;
using BLL.Manager;
using System.Web.UI.HtmlControls;
using BLL;
using DAL;
using BLL.Base;

namespace Web.Appraisal.UserControls
{
    public partial class SummaryCommentBlock : BaseUserControl
    {

        public TextBox CommentTextBox
        {
            get
            {
                return txtComments;
            }
        }
        public HtmlGenericControl CommentBlock
        {
            get
            {
                return commentBox;
            }
        }
        public double? GetAdditionalQuestionnairePoints()
        {


            if (!string.IsNullOrEmpty(txtAdditionalMarks.Text))
            {
                double value = 0;
                string str = txtAdditionalMarks.Text.Trim();

                if (double.TryParse(str, out value))
                    return value;
            }

            if (Convert.ToBoolean(hiddenAdditionPointsMode.Text.Trim()))
                return 0;

            //return ddlRecommendationList;

            return null;
        }

        public DropDownList RecommendDropDown
        {
            get
            {
                return ddlRecommendationList;
            }
        }

        public string GetSummaryBlockID
        {
            get
            {
                return blockSummary.ClientID;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest && !IsPostBack)
            {
                Initialise();

            }
        }
        public int GetFormID()
        {
            if (string.IsNullOrEmpty(Request.QueryString["fid"]))
                return 0;
            return int.Parse(Request.QueryString["fid"]);
        }
        public int GetEmployeeFormID()
        {
            if (string.IsNullOrEmpty(Request.QueryString["efid"]))
                return 0;
            return int.Parse(Request.QueryString["efid"]);
        }

        


        public void Initialise()
        {
            if(CommonManager.CompanySetting.WhichCompany==WhichCompany.Civil)
            {
                spanAdditionMarksInformation.InnerHtml = "&nbsp; (0-12 only)";
            }

            ddlRecommendationList.DataSource = AppraisalManager.GetAllRecommendFor();
            ddlRecommendationList.DataBind();

            AppraisalForm form = AppraisalManager.getFormInstanceByID(GetFormID());
            if (form != null)
            {
                // Introudction

                spanSummaryCommenttitleName.InnerHtml = form.SummaryName;
                spanSummaryCommentDescription.InnerHtml = (form.SummaryDescription);

                //if (string.IsNullOrEmpty(form.SummaryDescription))
                spanSummaryCommentDescription.Visible = false;
            }

            AppraisalEmployeeForm empForm = AppraisalManager.GetEmployeeAppraisaleForm(GetEmployeeFormID());

            if (empForm.Status == (int)AppraisalStatus.Saved)
            {
                mainCommentBlock.Visible = false;
                //return;
            }
            else if (empForm.Status >= (int)AppraisalStatus.SupervisorManagerCommented && empForm.NextStepOrStatusId != null)
            {
                int? additionStep = AppraisalManager.GetAdditionalSteps(empForm.NextStepOrStatusId.Value, FlowTypeEnum.Appraisal, empForm.AppraisalEmployeeFormID);
                if(additionStep != null)
                {
                    AppraisalAdditionalStep stepEnum = (AppraisalAdditionalStep)additionStep.Value;

                    if (stepEnum ==  AppraisalAdditionalStep.RecommendationBlock || (stepEnum == AppraisalAdditionalStep.AddAdditionPointsAndRecommendationBoth))
                        recommendBlock.Visible = true;

                    if (stepEnum == AppraisalAdditionalStep.AddAdditionPoints || (stepEnum == AppraisalAdditionalStep.AddAdditionPointsAndRecommendationBoth))
                    {
                        blockAddAdditionalMarks.Visible = true;
                        hiddenAdditionPointsMode.Text = "true";
                    }
                }

               
            }

            AppraisalEmployeeSummaryComment comment = AppraisalManager.GetAppraisalSelfEmployeeSummaryComment(GetEmployeeFormID());


            //if(form.ShowAgreeDisAgreeButtons  != null && form.ShowAgreeDisAgreeButtons.Value)
            //{
            //    divIAgreeDisAgreeBlock.Visible = true;

            //    if (empForm != null && empForm.SupervisorReviewAgreed != null)
            //    {
            //        if (empForm.SupervisorReviewAgreed.Value)
            //        {
            //            rdbIAgree.Visible = true;
            //        }
            //        else
            //            rdbIDisAgree.Visible = true;
            //    }
            //}

            //if (form.HideIAgreeDisAgreeButtons != null && form.HideIAgreeDisAgreeButtons.Value)
            //{
            //    rdbIAgree.Visible = false;
            //    rdbIDisAgree.Visible = false;
            //    divIAgreeDisAgreeBlock.Visible = false;
            //}
            //else
            //{
            //    if (empForm.SupervisorReviewAgreed == null)
            //    {
            //        rdbIAgree.Visible = false;
            //        rdbIDisAgree.Visible = false;
            //    }
            //    else
            //    {
            //        if (empForm.SupervisorReviewAgreed == true)
            //        {
            //            //rdbIAgree.Checked = true;
            //        }
            //        else
            //        {
            //            rdbIAgree.Visible = false;
            //            rdbIDisAgree.Visible = true;
            //            //rdbIAgree.Checked = false;
            //            //rdbIDisAgree.Checked = true;
            //        }
            //    }
            //}

            //supervisorComments.InnerHtml = empForm.SupervisorStep2Comment;

            //spanEmployeeSignature.InnerHtml = new EmployeeManager().GetById(empForm.EmployeeId).Name + " Signed on ";
            //if (comment != null && comment.CommentedOn != null)
            //    spanEmployeeSignature.InnerHtml += comment.CommentedOn.Value.ToString();
                            

            //spanEmployeeComment.InnerHtml = new EmployeeManager().GetById(empForm.EmployeeId).Name + "'s Comment";
            //if (comment != null)
            //    acitivtySummary.InnerHtml = comment.Comment;

            // list for other comment
            List<AppraisalEmployeeSummaryComment> list = AppraisalManager.GetAppraisalCommentList(empForm.AppraisalEmployeeFormID);
            string commenter = "";
            foreach (AppraisalEmployeeSummaryComment item in list)
            {
                commenter = item.CommenterDisplayTitle;

                HtmlGenericControl signature = new HtmlGenericControl("span");
                signature.ID = "signature" + item.SummaryCommentId;
                signature.Attributes["class"] = "summaryCommmentSignature";
                signature.Attributes.CssStyle["margin-top"] = "20px";
                signature.InnerHtml = commenter + " Signed on ";
                if (item.CommentedOn != null)
                    signature.InnerHtml += item.CommentedOn.Value.ToString();    

                HtmlGenericControl label = new HtmlGenericControl("span");
                label.ID = "label" + item.SummaryCommentId;
                label.Attributes["class"] = "summaryCommmentLabel";

                HtmlGenericControl divRecommendation = null;

                label.InnerHtml = commenter + "'s Comments";

                if (item.RecommendForId != null && item.RecommendForId != -1 && item.RecommendForId != 0)
                {
                    
                    divRecommendation = new HtmlGenericControl("div");
                    divRecommendation.ID = "divID" + item.SummaryCommentId;
                    divRecommendation.Attributes["class"] = "summaryRecommendation";

                    divRecommendation.InnerHtml = "Recommended : " + AppraisalManager.GetRecommendationName(item.RecommendForId);

                }
                 if (item.Type == (int)AppraisalSummaryCommentTypeEnum.HasIAgreeOrDisagree && empForm.SupervisorReviewAgreed != null)
                {
                   
                    HtmlGenericControl block = new HtmlGenericControl("div");
                    block.ID = "divID" + item.SummaryCommentId;
                    block.Attributes["class"] = "summaryRecommendation";

                    block.InnerHtml = (empForm.SupervisorReviewAgreed.Value ? "I agree" : "I do not agree");
                    label.Controls.Add(block);


                }
                if (item.Type == (int)AppraisalSummaryCommentTypeEnum.HasAdditionalMarks && empForm.AdditionalQuestionnaireScore != null)
                {
                    
                    HtmlGenericControl block = new HtmlGenericControl("div");
                    block.ID = "divID" + item.SummaryCommentId;
                    block.Attributes["class"] = "summaryRecommendation";

                    block.InnerHtml = "Additional Points : " + empForm.AdditionalQuestionnaireScore.ToString();
                    label.Controls.Add(block);
                }
                

                HtmlGenericControl div = new HtmlGenericControl("div");
                div.ID = "div" + item.SummaryCommentId;
                div.Attributes["class"] = "summaryCommmentDiv";
                div.InnerHtml = item.Comment;

                commentList.Controls.Add(signature);

                if (item.StatusStepID != (int)FlowStepEnum.Step1SaveAndSend)
                {
                    commentList.Controls.Add(label);
                    if (divRecommendation != null)
                        commentList.Controls.Add(divRecommendation);
                    commentList.Controls.Add(div);
                }
            }

            //if (SessionManager.CurrentLoggedInEmployeeId == 0)
            //    commentBox.Visible = false;
        }
    }
}