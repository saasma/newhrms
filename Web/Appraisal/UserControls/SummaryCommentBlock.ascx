﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SummaryCommentBlock.ascx.cs"
    Inherits="Web.Appraisal.UserControls.SummaryCommentBlock" %>
<script type="text/javascript">
    function displayRatingValue(spanId, value) {
        if (value.toString() == "")
            document.getElementById(spanId).innerHTML = "&nbsp;";
        else
            document.getElementById(spanId).innerHTML = "(" + value + ")";
    }
</script>
<style type="text/css">
    .summaryCommmentSignature
    {
        display: block;
        margin-left: 0px;
        color: gray;
        margin-bottom: 10px;
        border-bottom: 1px solid lightgray;
    }
</style>
<div runat="server" id="blockSummary">
    <span class="ratingBlockTitleCls" runat="server" id="spanSummaryCommenttitleName">
    </span>
    <div runat="server" class='ratingContainerBlockDescription' id="spanSummaryCommentDescription">
    </div>
</div>
<ext:Hidden runat="server" ID="hiddenAdditionPointsMode" Text="false" />
<div runat="server" id="mainCommentBlock">
   <%-- <div runat="server" id="divSupervisorComments" style="margin-top: 10px;">
        <span class="summaryCommmentLabel">Supervisor's Comment</span>
        <div class="summaryCommmentDiv" runat="server" id="supervisorComments">
        </div>
    </div>--%>
   
    <div id="commentList" runat="server" style="margin-top: 10px;">
       <%-- <span class="summaryCommmentSignature" runat="server" id="spanEmployeeSignature">
        </span><span class="summaryCommmentLabel" runat="server" id="spanEmployeeComment">Summary
            Comment</span>
        <div runat="server" class='summaryCommmentDiv' id="acitivtySummary">
        </div>--%>
    </div>
    <div class="ratingButtons" runat="server" id="commentBox">
       <%-- <div class="ratingButtons" runat="server" visible="false" id="divIAgreeDisAgreeBlock" style="margin-top: 10px;
            margin-bottom: 20px">
            <asp:Label runat="server" Visible="false"  Text="I agree" ID="rdbIAgree" />
            &nbsp;&nbsp;
            <asp:Label Visible="false"  runat="server" Text="I do not agree" ID="rdbIDisAgree" />
        </div>--%>
        <div runat="server" id="recommendBlock" visible="false" style="margin-bottom: 10px;">
            Your Recommendation<br />
            <asp:DropDownList AppendDataBoundItems="true" DataValueField="RecommendForId" DataTextField="Name"
                ID="ddlRecommendationList" runat="server" Width="200px">
                <asp:ListItem Text="" Value="-1"></asp:ListItem>
            </asp:DropDownList>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlRecommendationList"
                Display="None" ErrorMessage="Recomendation is required." InitialValue="-1" ValidationGroup="Appraisal" />
        </div>
         <div runat="server" id="blockAddAdditionalMarks" visible="false" style="margin-bottom: 10px;">
            Additional Marks<span runat="server" id="spanAdditionMarksInformation"></span><br />
            <asp:TextBox 
                ID="txtAdditionalMarks" runat="server" Width="200px">
            </asp:TextBox>
            <asp:CompareValidator Type="Double" Operator="DataTypeCheck" ID="validationMarks" runat="server" ControlToValidate="txtAdditionalMarks"
                Display="None" ErrorMessage="Invalid additional marks." ValidationGroup="Appraisal" />
            <%-- here 12 is the max value given in case of civil for other it may be changed, use setting in future--%>
              <asp:RangeValidator Type="Double" MinimumValue="0" MaximumValue="12" ID="validationMarksRange" runat="server" ControlToValidate="txtAdditionalMarks"
                Display="None" ErrorMessage="Invalid additional marks,it should be between 0-12 only." ValidationGroup="Appraisal" />
        </div>
        Your Comment<br />
        <asp:TextBox TextMode="MultiLine" ID="txtComments" runat="server" Width="700px" Height="85px">
        </asp:TextBox>
        <asp:RequiredFieldValidator ID="valReqd" runat="server" ControlToValidate="txtComments"
            Display="None" ErrorMessage="Comment is required." ValidationGroup="Appraisal" />
    </div>
</div>
