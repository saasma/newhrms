﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RatingEmployeeBlock.ascx.cs"
    Inherits="Web.Appraisal.UserControls.RatingEmployeeBlock" %>
<%@ Register Src="~/Employee/Appraisal/EmployeeDetailsScoreCtl.ascx" TagName="Score"
    TagPrefix="uc1" %>
<style type="text/css">
    .appraisalOverallDiv
    {
        padding-top: 10px;
        width: 250px;
        height: 135px;
        margin-left: 50px;
    }
    .appraisalOverallDiv span
    {
        display: block;
        text-align: center;
        background-color: #E8F1FF;
        border: 1px solid #BAD5ED;
    }
    .appraisalOverallDiv div
    {
        padding-top: 10px;
        padding-left: 5px;
        padding-right: 5px;
        text-align: center;
        background-color: #E8F1FF;
        border: 1px solid #BAD5ED;
    }
    .ratingScore
    {
        font-weight: bold;
        font-size: 20px;
        border: 0px solid red !important;
    }
    
        .items
        {
            padding-left: 20px;
        }
        .items span
        {
            
            display: block;
           
            padding-bottom: 2px;
            font-weight:normal;
            font-size:12px;
        }
        .items img
        {
            float:left;
            width:18px;height:18px;
            
margin-right: 10px;
        }
        .fieldTable td, table tr.up td{padding-top:0px;}
    /*header block */
    h1 { font-size: 2em;padding:0px;margin;0px; }
h2 { font-size: 1.5em; padding:0px;margin;0px; }
h3 { font-size: 1.17em; padding:0px;margin;0px; }
h5 { font-size: .83em; padding:0px;margin;0px; }
h6 { font-size: .75em; padding:0px;margin;0px; }
h1,h2,h3,h4,h5,h6{float:none;}
strong{ font-size:14px!important;font-weight:bold!important;}
</style>
<style type="text/css">
    .x-form-item-label
    {
        /* font-style: italic; */
        color: #414141 !important;
        font-weight: bold !important;
        font-style: normal !important;
    }
</style>
<div runat="server" id="mainTitle" class="AppraisalMainTitle">
</div>
<table style="margin-left: 18px; margin-top: 10px; margin-bottom: 20px;">
    <tr>
        <td>
            <%-- <asp:Image ID="image" runat="server" ImageUrl="~/images/sample.jpg" Width="150px"
                Height="150px" />--%>
            <div style="margin-bottom: 10px">
                <asp:Image ID="image" CssClass="left" runat="server" ImageUrl="~/images/sample.jpg"
                    Width="150px" Height="150px" />
                <div class="left items">
                    <h3 runat="server" class="heading" style="color: #214B71; margin: 0; margin-top: 2px;
                        margin-bottom: 3px;" id="title">
                    </h3>
                    <span runat="server" style="font-size: 15px;" id="positionDesignation"></span>
                    <div style="clear: both">
                    </div>
                    <img runat="server" id="groupLevelImg" runat="server" src="~/Styles/images/emp_position.png"
                        style="margin-top: 8px;" />
                    <span runat="server" style="padding-top: 10px;" id="groupLevel"></span>
                    <div style="clear: both">
                    </div>
                    <img runat="server" src="~/Styles/images/emp_br_dept.png" />
                    <span runat="server" id="branch"></span>
                    <div style="clear: both">
                    </div>
                    <img runat="server" src="~/Styles/images/emp_work.png" />
                    <span runat="server" id="workingFor" style="width: 600px"></span>
                    <div style="clear: both">
                    </div>
                </div>
                <div style="clear: both">
                </div>
            </div>
        </td>
        <td style="display: none">
            <div class="appraisalOverallDiv" runat="server" id="divScore">
                <div style="float: left; padding: 8px; margin-top: 15px; height: 80px;" runat="server"
                    id="employeeRating">
                    Appraisee Rating
                    <br />
                    <br />
                    <span class="ratingScore" runat="server" id="employeeRatingScore">40.34 </span>
                </div>
                <div style="float: left; padding: 5px; margin-top: 15px; margin-left: 20px; height: 84px;
                    padding-top: 8px;" runat="server" id="supervisorDiv">
                    Supervisor Rating
                    <br />
                    <br />
                    <span class="ratingScore" runat="server" id="supervisorRatingScore">40.34 </span>
                </div>
            </div>
        </td>
        <td>
        </td>
    </tr>
</table>
<uc1:Score Id="Score" runat="server" />
