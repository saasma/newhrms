﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CompetencyBlock.ascx.cs"
    Inherits="Web.Appraisal.UserControls.CompetencyBlock" %>
<script type="text/javascript">
    function displayRatingValue(spanId, value) {
        if (value.toString() == "")
            document.getElementById(spanId).innerHTML = "&nbsp;";
        else
            document.getElementById(spanId).innerHTML = "(" + value + ")";
    }
</script>
<div runat="server" id="blockActivity" visible="false">
    <span class="ratingBlockTitleCls" runat="server" id="spanActivityTitleName"></span>
    <div runat="server" class='ratingContainerBlockDescription' id="divActivityTitleDescription">
    </div>
    <div runat="server" style="margin-left: 0px" visible="false" class='ratingContainerBlockDescription'
        id="acitivtySummary">
    </div>
    <div id="Div1" runat="server">
        <div class="ratingContainerBlock" style="padding-top: 0px" id="ctl00_mainContent_ctl02_containerBlockActivity1">
            <div runat="server" class="ratingCommentBlock" id="appraiseeBlock">
                <span runat="server" class="spanRatingLevelClass" id="spanRatingLevel">Appraisee's Marks
                </span>
                <asp:TextBox runat="server" value="0" max="5" min="0" step="1" ID="appraiseeMarkingValue" />
            </div>
            <div runat="server" class="ratingCommentBlock" id="supervisorBlock">
                <span runat="server" class="spanRatingLevelClass" id="span2">Supervisor's Marks
                </span>
                <asp:TextBox runat="server" value="0" max="5" min="0" step="1" ID="supervisorMarkingValue" />
            </div>
            <div style="clear: both">
            </div>
        </div>
        <span id="supervisorCommentLevel" class="summaryCommmentLabel" runat="server">Supervisor's
            Comment</span>
        <div id="supervisorCommentText" class="summaryCommmentDiv" runat="server">
        </div>
        <div style="clear: both">
        </div>
    </div>
    <ext:LinkButton ID="btnDownload" runat="server" Icon="Attach" StyleSpec="margin:10px 30px 15px 0px">
        <DirectEvents>
            <Click OnEvent="btnDownload_Click">
            </Click>
        </DirectEvents>
    </ext:LinkButton>
</div>
<%--Competency BLock--%>
<div runat="server" id="block" visible="false">
    <span class="ratingBlockTitleCls" runat="server" id="spantitleName"></span>
    <div runat="server" class='ratingContainerBlockDescription' id="spantitleDescription">
    </div>
    <%--<div id="containerBlock1" class="ratingContainerBlock" runat="server">--%>
    <%-- <div runat="server" class="ratingTitleBlock" id="title1">
        4.1 &nbsp;&nbsp;&nbsp; What is the capital of Nepal?
    </div>
    <div runat="server" class="ratingCommentBlock" id="commentBlock1">
        <span runat="server" class="spanRatingLevelClass" id="spanRatingLevel">Supervisor 's
            Rating </span>--%>
    <%--  <div class="rateit" data-rateit-value="2.5" data-rateit-ispreset="true" data-rateit-readonly="true"></div>--%>
    <%-- <span runat="server" class="ratingLabelBlock" id="commentTitle">Supervisor 's
            Comment </span>
        <asp:TextBox runat="server" CssClass="ratingInputBlock" ID="commentValue" TextMode="MultiLine">
            
        </asp:TextBox>
    </div>--%>
    <%--       <div runat="server" class="ratingCommentBlock" id="Div1">
            <span runat="server" class="spanRatingLevelClass" id="span1">Supervisor 's Rating
            </span>
            <asp:TextBox runat="server" value="0" max="5" min="0" step="1" ID="TextBox1" />
            <span class="rateit" runat="server" id="Span2" data-rateit-backingfld="#ctl00_ContentPlaceHolder_Main_CompetencyBlock1_ratingValue1">
            </span><span runat="server" class="ratingLabelBlock" id="Span3">Supervisor 's Comment
            </span>
            <asp:TextBox runat="server" CssClass="ratingInputBlock" ID="TextBox2" TextMode="MultiLine">
            
            </asp:TextBox>
        </div>
        <div style="clear:both">
        </div>
    </div>--%>
</div>
<%--Target BLock--%>
<%----%>
<ext:Window ID="window" runat="server" Visible="false" ButtonAlign="Right" Title="Target"
    Width="550" Height="350" BodyPadding="10" Hidden="false" X="-1000" Y="-1000">
    <Content>
        <table class="fieldTable">
            <tr>
                <td>
                    <ext:ComboBox ID="cmbTarget" LabelSeparator="" Width="200px" runat="server" ValueField="TargetD"
                        DisplayField="Name" FieldLabel="Target *" LabelAlign="top" ForceSelection="true"
                        QueryMode="Local">
                        <Store>
                            <ext:Store ID="Store4" runat="server">
                                <Model>
                                    <ext:Model ID="Model3" runat="server" IDProperty="TargetD">
                                        <Fields>
                                            <ext:ModelField Name="TargetD" Type="String" />
                                            <ext:ModelField Name="Name" />
                                            <ext:ModelField Name="Weight" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                    <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator3" runat="server"
                        ValidationGroup="Target" ControlToValidate="cmbTarget" ErrorMessage="Target is required." />
                </td>
            </tr>
            <tr>
                <td>
                    <ext:TextField ID="txtTargetValue" LabelSeparator="" FieldLabel="Target Value *"
                        LabelAlign="Top" runat="server" Width="400px">
                    </ext:TextField>
                </td>
            </tr>
            <tr>
                <td>
                    <ext:TextField ID="txtAchievement" LabelSeparator="" FieldLabel="Achievement *" LabelAlign="Top"
                        runat="server" Width="400px">
                    </ext:TextField>
                </td>
            </tr>
            <tr>
                <td>
                    <ext:TextField ID="txtWeight" LabelSeparator="" FieldLabel="Weight *" LabelAlign="Top"
                        runat="server" Width="200px">
                    </ext:TextField>
                    <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator1" runat="server"
                        ValidationGroup="Target" ControlToValidate="txtWeight" ErrorMessage="Weight is required." />
                    <asp:CompareValidator Display="None" ID="RequiredFieldValidator22" runat="server"
                        ValidationGroup="Target" ControlToValidate="txtWeight" ErrorMessage="Please place valid amount."
                        Type="Double" Operator="DataTypeCheck" />
                </td>
            </tr>
        </table>
    </Content>
    <Buttons>
        <ext:Button runat="server" ID="btnSetTarget" Cls="btn btn-primary" Text="<i></i>Save"
            runat="server">
            <DirectEvents>
                <Click OnEvent="btnSetTarget_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
            <Listeners>
                <Click Handler="valGroup = 'Target'; return CheckValidation();">
                </Click>
            </Listeners>
        </ext:Button>
        <ext:LinkButton runat="server" Cls="btnFlatLeftGap" ID="LinkButton4" Text="<i></i>Cancel"
            runat="server">
            <Listeners>
                <Click Handler="#{window}.hide();">
                </Click>
            </Listeners>
        </ext:LinkButton>
    </Buttons>
</ext:Window>
<div runat="server" id="blockTarget" visible="false">
    <span class="ratingBlockTitleCls" runat="server" id="spanTargetTitleName"></span>
    <div runat="server" class='ratingContainerBlockDescription' id="spanTargetTitleDescription">
    </div>
    <asp:GridView CssClass="table table-primary mb30 table-bordered table-hover" UseAccessibleHeader="true"
        ShowHeaderWhenEmpty="True" ID="gridTargets" runat="server" AutoGenerateColumns="False"
        CellPadding="0" DataKeyNames="TargetRef_ID" GridLines="None" AllowSorting="false"
        ShowFooterWhenEmpty="False" OnRowDeleting="gvw_RowDeleting" OnRowDataBound="gridTargets_RowDataBound">
        <Columns>
            <asp:TemplateField HeaderText="Identifier" HeaderStyle-HorizontalAlign="Left">
                <ItemTemplate>
                    <asp:Label ID="lblName" runat="server" Width="150" Text='<%# Eval("Name") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Target" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <asp:Label ID="lblAssginedTarget" Style='text-align: left;' runat="server" Width="100"
                        Text='<%# Eval("TargetForDisplay") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Achievement" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <asp:Label ID="lblAchievement" Style='text-align: left;' runat="server" Width="100"
                        Text='<%# Eval("AchievementForDisplay") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Weight" Visible="false" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <asp:Label ID="lblWeight" Style='text-align: right;' runat="server" Width="50" Text='<%# Eval("Weight") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Your Rating" Visible="false" HeaderStyle-HorizontalAlign="Left">
                <ItemTemplate>
                    <asp:DropDownList ID="ddlYourRating" Width="120" Visible="false" runat="server" DataTextField="LabelScore"
                        DataValueField="Score">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator runat="server" InitialValue="-1" ErrorMessage="Rating is required."
                        ID="reqdYourRating" Display="None" ControlToValidate="ddlYourRating" ValidationGroup="Appraisal" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Your Comment" HeaderStyle-HorizontalAlign="Left">
                <ItemTemplate>
                    <asp:TextBox ID="txtYourComment" runat="server" Width="150" Text='<%# Eval("SelfComment") %>' />
                    <asp:Label Visible="false" runat="server" ID="lblTargetYourComment" Width="150" Text='<%# Eval("SelfComment") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Supervisor Rating" Visible="false" HeaderStyle-HorizontalAlign="Left">
                <ItemTemplate>
                    <asp:DropDownList ID="ddlSupervisorRating" Visible="false" Width="120" runat="server"
                        DataTextField="LabelScore" DataValueField="Score">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator runat="server" InitialValue="-1" ErrorMessage="Rating is required."
                        ID="reqdSupervisorRating" Display="None" ControlToValidate="ddlSupervisorRating"
                        ValidationGroup="Appraisal" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Supervisor Comment" HeaderStyle-HorizontalAlign="Left">
                <ItemTemplate>
                    <asp:TextBox Visible="false" ID="txtSupervisorComment" runat="server" Width="150"
                        Text='<%# Eval("ManagerComment") %>' />
                    <asp:Label Visible="false" runat="server" ID="lblTargetSupervisorComment" Width="150"
                        Text='<%# Eval("ManagerComment") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:CommandField ButtonType="Image" Visible="false" DeleteImageUrl="~/images/retdelete.png"
                ShowCancelButton="False" ShowDeleteButton="True" />
        </Columns>
    </asp:GridView>
</div>
<ext:LinkButton runat="server" Visible="false" Text="Assign / Change Target" ID="btnAddTarget">
    <Listeners>
        <Click Handler="#{window}.center();#{window}.show();" />
    </Listeners>
</ext:LinkButton>

<div runat="server" id="blockQuestionnare" visible="false">
    <span class="ratingBlockTitleCls" runat="server" id="spanQuestionTitleName"></span>
    <div runat="server" class='ratingContainerBlockDescription' id="divQuestionTitleDescription">
    </div>
    <div runat="server" id="blockQuestionnareList">
    </div>
</div>

<div runat="server" id="blockReview" visible="false">
    <span class="ratingBlockTitleCls" runat="server" id="spanReviewTitleName"></span>
    <div runat="server" class='ratingContainerBlockDescription' id="divReviewTitleDescription">
    </div>
</div>
