﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using Utils;
using System.IO;
using BLL;

namespace Web.Appraisal.UserControls
{
    public partial class RatingEmployeeBlock : BaseUserControl
    {

        public int GetEmployeeAppraisalFormID()
        {
            if (!string.IsNullOrEmpty(Request.QueryString["efid"]))
                return int.Parse(Request.QueryString["efid"]);

            return 0;
        }

        public int GetFormID()
        {
            if (string.IsNullOrEmpty(Request.QueryString["fid"]))
                return 0;
            return int.Parse(Request.QueryString["fid"]);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!X.IsAjaxRequest && !this.IsPostBack)
                LoadEmployeeDetails();
        }

        public void LoadEmployeeDetails()
        {
            AppraisalEmployeeForm empForm = AppraisalManager.GetEmployeeAppraisaleForm
                (GetEmployeeAppraisalFormID());

            if (empForm != null)
            {
                if (GetFormID() != 0)
                {
                    // if Emp form ID doesn't match with query string Appraisal form ID then
                    if (GetFormID() != empForm.AppraisalFormRef_ID)
                    {
                        NotPermissionContent();
                        return;
                    }
                }

                EEmployee emp = new EmployeeManager().GetById(empForm.EmployeeId);
                mainTitle.InnerHtml =
                    empForm.AppraisalForm.Name;

                if (empForm.EmployeeId != SessionManager.CurrentLoggedInEmployeeId)
                    mainTitle.InnerHtml += " for " + emp.Name;



                title.InnerHtml = emp.Title + " " + emp.Name + " - " + emp.EmployeeId;
                // Designation/Position
                if (emp.EDesignation != null)
                    positionDesignation.InnerHtml = emp.EDesignation.Name;
                // Group/Level
                if (emp.PEmployeeIncomes.Count > 0 && emp.PEmployeeIncomes[0].LevelId != null)
                {
                    BLevel level = NewPayrollManager.GetLevelById(emp.PEmployeeIncomes[0].LevelId.Value);
                    BLevel actingLevel = NewPayrollManager.GetActingLevelInTodayDate(emp.EmployeeId);
                    groupLevel.InnerHtml = level.BLevelGroup.Name + " (Level - " + level.Name +
                         (actingLevel == null ? "" : ", Acting Level - " + actingLevel.Name)
                        + ")";
                }
                else
                    groupLevelImg.Visible = false;

                // Branch
                GetEmployeeCurrentBranchDepartmentResult currentBranchDep = EmployeeManager.GetEmployeeCurrentBranchDepartment(emp.EmployeeId);
                if (currentBranchDep != null)
                {
                    branch.InnerHtml = currentBranchDep.Department + ", " + currentBranchDep.Branch;
                }

                // Contact
                //if (emp.EAddresses.Count > 0)
                //{
                //    EAddress entity = emp.EAddresses[0];
                //    if (!string.IsNullOrEmpty(entity.CIPhoneNo) || !string.IsNullOrEmpty(entity.CIMobileNo) || !string.IsNullOrEmpty(entity.Extension))
                //        contact.InnerHtml += string.Format("{0} / {1} ({2})", entity.CIPhoneNo, entity.CIMobileNo, entity.Extension);
                //    email.InnerHtml += string.Format("<a href='mailto:{0}'>{0}</a> ", entity.CIEmail);
                //}
                workingFor.InnerHtml = "";
                // Working for since
                int years, months, days, hours;
                int minutes, seconds, milliseconds;
                ServicePeroid firstStatus = EmployeeManager.GetServicePeroidDate(emp.EmployeeId);
                if (firstStatus != null)
                {
                    NewHelper.GetElapsedTime(firstStatus.FromDateEng, DateTime.Now, out years, out months, out days, out hours, out minutes, out seconds, out milliseconds);
                    string text = "";
                    if (years != 0)
                        text = " " + years + " years";
                    if (months != 0)
                        text += " " + months + " months";
                    if (days != 0)
                        text += " " + days + " days";

                    workingFor.InnerHtml += text;

                    workingFor.InnerHtml += ", Since " +
                        (IsEnglish ? firstStatus.FromDateEng.ToShortDateString() : firstStatus.FromDate + " (" + firstStatus.FromDateEng.ToShortDateString() + ")");

                }


                // Photo
                bool hasPhoto = false;
                if (emp.HHumanResources.Count > 0 && !string.IsNullOrEmpty(emp.HHumanResources[0].UrlPhoto))
                {
                    if (!string.IsNullOrEmpty(emp.HHumanResources[0].UrlPhotoThumbnail))
                    {
                        image.ImageUrl = Page.ResolveUrl(Config.UploadLocation + "/" + emp.HHumanResources[0].UrlPhotoThumbnail);
                        if (File.Exists(Server.MapPath(image.ImageUrl)))
                            hasPhoto = true;
                        image.Attributes.Remove("width");
                        image.Attributes.Remove("height");
                    }
                    else
                    {
                        image.ImageUrl = Page.ResolveUrl(Config.UploadLocation + "/" + emp.HHumanResources[0].UrlPhoto);
                        if (File.Exists(Server.MapPath(image.ImageUrl)))
                            hasPhoto = true;
                    }
                }
                if (!hasPhoto)
                {
                    image.ImageUrl = Page.ResolveUrl(Web.Helper.WebHelper.GetEmployeeNoImage(emp.EmployeeId));
                }


                LoadScore();

            }
        }

        public void LoadScore()
        {
            AppraisalEmployeeForm empForm = AppraisalManager.GetEmployeeAppraisaleForm
               (GetEmployeeAppraisalFormID());


            bool hasEmployeeRatingEnabled = false;
            AppraisalForm form = empForm.AppraisalForm;
            
            //if (form.AllowActivitySelfRating.Value || form.AllowCompetencySelfRating.Value || form.AllowQuestionSelfRating.Value)
            //    hasEmployeeRatingEnabled = true;

            //if (!hasEmployeeRatingEnabled)
            //{
            //    employeeRating.Visible = false;
            //    divScore.Style["width"] = "150px";
            //    supervisorDiv.Style["float"] = "";
            //}

            //if (empForm.SelfEmployeeRatingScore == null)
            //    employeeRatingScore.InnerHtml = "0";
            //else
            //    employeeRatingScore.InnerHtml = empForm.SelfEmployeeRatingScore.Value.ToString("N2");

            //if (empForm.SupervisorRatingScore == null)
            //    supervisorRatingScore.InnerHtml = "0";
            //else
            //    supervisorRatingScore.InnerHtml = empForm.SupervisorRatingScore.Value.ToString("N2");

            Score.LoadScore();


            if (this.CurrentPageUrl.ToLower() == "employeesignature.aspx" ||
                this.CurrentPageUrl.ToLower() == "employeeappraisalreviewpage.aspx" ||
                this.CurrentPageUrl.ToLower()== "employeeappraisalhrpage.aspx")
            {
                Score.Visible = true;
            }
            else
            {
                Score.Visible = false;
            }

            //if (empForm.Status == (int)AppraisalStatus.Saved)
            //    Score.Visible = false;
            //else if (empForm.Status == (int)AppraisalStatus.SaveAndSend)
            //{
            //    Score.Visible = true;
            //    Score.HideSupervisorScore();
            //}
            //else
            //{
            //    Score.Visible = true;
            //    Score.ShowSupervisorScore();
            //}

            
        }

    }
}