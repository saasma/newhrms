﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using System.Xml;

namespace Web.Appraisal
{
    public partial class Goals : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
              
            }
        }

        public void Initialise()
        {
            cmbVisibility.Store[0].DataSource = new Visibility().GetMembers();
            cmbVisibility.Store[0].DataBind();

            cmbCategory.Store[0].DataSource = AppraisalManager.getallGoalsCategory();
            cmbCategory.Store[0].DataBind();

            LoadLevels();
        }
        
        private void LoadLevels()
        {
            GridLevels.GetStore().DataSource = AppraisalManager.GetAllRatingScale();
            GridLevels.GetStore().DataBind();

        }


        
        public void ClearLevelFields()
        {
            hiddenValue.Text = "";
        }

      
        
        protected void btnAddLevel_Click(object sender, DirectEventArgs e)
        {
            

            ClearLevelFields();
            WindowLevel.Show();
        }


        protected void btnDeleteLevel_Click(object sender, DirectEventArgs e)
        {
            int ratingScale = int.Parse(hiddenValue.Text.Trim());
            Status status = AppraisalManager.DeleteRatingScale(ratingScale);
            if (status.IsSuccess)
            {
                LoadLevels();
                NewMessage.ShowNormalMessage("Rating Scale deleted.");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        

        protected void btnEditLevel_Click(object sender, DirectEventArgs e)
        {
            int levelId = int.Parse(hiddenValue.Text.Trim());
            AppraisalRatingScale entity = AppraisalManager.GetRatingScaleById(levelId);
            List<AppraisalRatingScaleLine> lines = new List<AppraisalRatingScaleLine>();
            WindowLevel.Show();

            lines = entity.AppraisalRatingScaleLines.ToList();


            

        }

        protected void btnLevelSaveUpdate_Click(object sender, DirectEventArgs e)
        {

            Page.Validate("InsertUpdateLevel");
            if (Page.IsValid)
            {
                AppraisalGoal goalInstance = new AppraisalGoal();
                int GoalID;
                goalInstance.GoalName = txtGoalName.Text;
                goalInstance.Metric = txtMetric.Text;

                if(cmbVisibility.SelectedItem!=null)
                    goalInstance.VisibilityText = cmbVisibility.SelectedItem.Text;

                if (cmbCategory.SelectedItem != null)
                {
                    goalInstance.CategoryID = int.Parse(cmbCategory.SelectedItem.Value);
                    goalInstance.CategoryText = cmbCategory.SelectedItem.Text;
                }
                if (!string.IsNullOrEmpty(txtWeight.Text))
                {
                    goalInstance.Weight = decimal.Parse(txtWeight.Text);
                }
                if (!string.IsNullOrEmpty(txtComplete.Text))
                {
                    goalInstance.Complete = decimal.Parse(txtComplete.Text);
                }
                if (!string.IsNullOrEmpty(txtStartDate.Text))
                {
                    goalInstance.StartDate = DateTime.Parse(txtStartDate.Text);
                }
                if (!string.IsNullOrEmpty(txtDueDate.Text))
                {
                    goalInstance.DueDate = DateTime.Parse(txtDueDate.Text);
                }

                bool isInsert = true;
                
                if (!string.IsNullOrEmpty(hiddenValue.Text))
                {
                    goalInstance.GoalID = new Guid(hiddenValue.Text.Trim());
                    isInsert = false;
                }


                Status status = AppraisalManager.InsertUpdateGoal(goalInstance, isInsert);
                if (status.IsSuccess)
                {
                    WindowLevel.Hide();
                    LoadLevels();
                    NewMessage.ShowNormalMessage("Goal saved.");
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }

            }
             
             
        }
 
    }
}