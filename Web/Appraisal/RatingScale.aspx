﻿<%@ Page Title="Rating Scale" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="RatingScale.aspx.cs" Inherits="Web.Appraisal.RatingScale" %>

<%--<%@ Register Src="~/NewHR/UserControls/EmployeeDetailsCtl.ascx" TagName="EmployeeDetailsCtl"
    TagPrefix="uc1" %>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

      var CommandHandler = function(command, record){
           
                if(command=="Edit")
                {
                    <%= btnEditLevel.ClientID %>.fireEvent('click');
                }
                else
                {
                
                    <%=Store1.ClientID %>.remove(record);
                }

             }

        
        var CommandHandler1 = function(command, record){
            <%= hiddenValue.ClientID %>.setValue(record.data.RatingScaleID);
                if(command=="Edit")
                {
                    <%= btnEditLevel.ClientID %>.fireEvent('click');
                }
                else
                {
                
                    <%= btnDeleteLevel.ClientID %>.fireEvent('click');
                }

             }
             
            var skipRender = function(value)
            {
                if(value==null)
                    return "";

                if(value==true)
                    return "Yes";

                return "";
            }

        
    </script>
    <style type="text/css">
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
        #content
        {
            margin: inherit;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Rating Scale
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel" style="margin-top: 0px !important; padding-top: 0px !important;">
        <ext:Hidden runat="server" ID="hiddenValue" />
        <ext:LinkButton runat="server" Hidden="true" ID="btnEditLevel" runat="server">
            <DirectEvents>
                <Click OnEvent="btnEditLevel_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:LinkButton>
        <ext:Button ID="btnDeleteLevel" runat="server" Hidden="true">
            <DirectEvents>
                <Click OnEvent="btnDeleteLevel_Click">
                    <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the Rating Scale?" />
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:Button>
        <div class="separator bottom">
        </div>
        <div class="innerLR">
            <%--<uc1:EmployeeDetailsCtl Id="EmployeeDetailsCtl1" runat="server" />--%>
            <div class="widget">
                <div class="widget-body">
                    <ext:GridPanel StyleSpec="margin-top:15px;" ID="GridLevels" runat="server" Cls="itemgrid">
                        <Store>
                            <ext:Store ID="Store3" runat="server">
                                <Model>
                                    <ext:Model ID="Model4" runat="server" IDProperty="RatingScaleID">
                                        <Fields>
                                            <ext:ModelField Name="RatingScaleID" Type="String" />
                                            <ext:ModelField Name="Name" Type="string" />
                                            <ext:ModelField Name="Description" Type="string" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:Column ID="Column5" Sortable="false" MenuDisabled="true" runat="server" Text="Name"
                                    Width="300" Align="Left" DataIndex="Name" />
                                <ext:Column ID="Column6" Sortable="false" MenuDisabled="true" runat="server" Text="Description"
                                    Width="500" Align="Left" DataIndex="Description">
                                </ext:Column>
                                <ext:CommandColumn runat="server" Text="Actions" Align="Center" Width="63">
                                    <Commands>
                                        <ext:GridCommand Icon="Pencil" CommandName="Edit" />
                                        <ext:GridCommand Icon="Decline" CommandName="Delete" />
                                    </Commands>
                                    <Listeners>
                                        <Command Handler="CommandHandler1(command,record);" />
                                    </Listeners>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <SelectionModel>
                            <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                        </SelectionModel>
                    </ext:GridPanel>
                    <div class="buttonBlock">
                        <ext:Button runat="server" ID="btnAddLevel" Cls="btn btn-primary" Text="<i></i>Add New Rating Scale">
                            <DirectEvents>
                                <Click OnEvent="btnAddLevel_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                    </div>
                </div>
            </div>
            <ext:Window ID="WindowLevel" runat="server" Title="Add/Edit Rating Scale" Icon="Application"
                Width="800" Height="555" BodyPadding="5" Hidden="true" Modal="true">
                <Content>
                    <table class="fieldTable">
                        <tr>
                            <td>
                                <ext:TextField ID="txtName" LabelSeparator="" runat="server" FieldLabel="Name *"
                                    Width="200" LabelAlign="Top">
                                </ext:TextField>
                                <asp:RequiredFieldValidator Display="None" ID="valcmbFaculty" runat="server" ValidationGroup="SaveUpdateLevel"
                                    ControlToValidate="txtName" ErrorMessage="Name is required." />
                            </td>
                            <td>
                                <ext:TextField ID="txtBoxDes" LabelSeparator="" runat="server" FieldLabel="Description"
                                    Width="425" LabelAlign="Top">
                                </ext:TextField>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <ext:RadioGroup ID="RadioGroup1" runat="server" FieldLabel="Select a pre-built rating scale or built your own from scratch"
                                    Width="660" LabelWidth="200" Cls="x-check-group-alt">
                                    <Items>
                                        <ext:Radio ID="Radio3" runat="server" BoxLabel="1 - 3" Checked="true" AutoPostBack="false">
                                            <DirectEvents>
                                                <Focus OnEvent="RadioSelect_Change">
                                                </Focus>
                                            </DirectEvents>
                                        </ext:Radio>
                                        <ext:Radio ID="Radio5" runat="server" BoxLabel="1 - 5" AutoPostBack="false">
                                            <DirectEvents>
                                                <Focus OnEvent="RadioSelect_Change">
                                                </Focus>
                                            </DirectEvents>
                                        </ext:Radio>
                                        <ext:Radio ID="Radio7" runat="server" BoxLabel="1 - 7" AutoPostBack="false">
                                            <DirectEvents>
                                                <Focus OnEvent="RadioSelect_Change">
                                                </Focus>
                                            </DirectEvents>
                                        </ext:Radio>
                                        <ext:Radio ID="Radio0" runat="server" BoxLabel="Define" AutoPostBack="false">
                                            <DirectEvents>
                                                <Focus OnEvent="RadioSelect_Change">
                                                </Focus>
                                            </DirectEvents>
                                        </ext:Radio>
                                    </Items>
                                </ext:RadioGroup>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <ext:GridPanel Width="750" StyleSpec="margin-top:15px;" ID="GridPanel1" runat="server"
                                    Cls="itemgrid" ClicksToEdit="1">
                                    <Store>
                                        <ext:Store ID="Store1" runat="server" AutoLoad="true">
                                            <Model>
                                                <ext:Model ID="Model1" runat="server" IDProperty="RatingScaleID">
                                                    <Fields>
                                                        <ext:ModelField Name="Score" Type="string" />
                                                        <ext:ModelField Name="Label" Type="string" />
                                                        <ext:ModelField Name="Description" Type="string" />
                                                        <ext:ModelField Name="RatingScaleID" Type="string" />
                                                        <ext:ModelField Name="SkipInPointCalculation" Type="Boolean" NullConvert="false" />
                                                    </Fields>
                                                </ext:Model>
                                            </Model>
                                        </ext:Store>
                                    </Store>
                                    <Plugins>
                                        <ext:CellEditing ID="CellEditing1" ClicksToEdit="1" runat="server">
                                            <Listeners>
                                            </Listeners>
                                        </ext:CellEditing>
                                    </Plugins>
                                    <ColumnModel>
                                        <Columns>
                                            <%--<ext:Column ID="Column2" Sortable="false" MenuDisabled="true" runat="server" Text="Score"
                                            Width="125" Align="Center" DataIndex="Score" />
                                        <ext:Column ID="Column3" Sortable="false" MenuDisabled="true" runat="server" Text="Label"
                                            Width="175" Align="Center" DataIndex="Label" />
                                        <ext:Column ID="Column4" Sortable="false" MenuDisabled="true" runat="server" Text="Description"
                                            Width="300" Align="Center" DataIndex="Description">
                                        </ext:Column>--%>
                                            <ext:Column ID="Column_Score" TdCls="wrap" runat="server" Text="Score" Sortable="false"
                                                MenuDisabled="true" Width="125" DataIndex="Score" Border="false">
                                                <Editor>
                                                    <ext:TextField ID="txtBoxScore" SelectOnFocus="true" runat="server" />
                                                </Editor>
                                            </ext:Column>
                                            <ext:Column ID="Column_Label" TdCls="wrap" runat="server" Text="Label" Sortable="false"
                                                MenuDisabled="true" Width="200" DataIndex="Label" Border="false">
                                                <Editor>
                                                    <ext:TextField ID="txtBoxLabel" SelectOnFocus="true" runat="server" />
                                                </Editor>
                                            </ext:Column>
                                            <ext:Column ID="Column_Description" TdCls="wrap" runat="server" Text="Description"
                                                Sortable="false" MenuDisabled="true" Width="300" DataIndex="Description" Border="false">
                                                <Editor>
                                                    <ext:TextArea ID="txtDescription" SelectOnFocus="true" Height="50" runat="server" />
                                                </Editor>
                                            </ext:Column>
                                            <ext:Column ID="Column1" Align="Center" TdCls="wrap" runat="server" Text="Skip In Points"
                                                Sortable="false" MenuDisabled="true" Width="100" DataIndex="SkipInPointCalculation"
                                                Border="false">
                                                <Editor>
                                                    <ext:Checkbox runat="server" />
                                                </Editor>
                                                <Renderer Fn="skipRender" />
                                            </ext:Column>
                                            <ext:CommandColumn ID="CommandColumn1" runat="server" Width="25">
                                                <Commands>
                                                    <ext:GridCommand Icon="Decline" CommandName="Delete" />
                                                </Commands>
                                                <Listeners>
                                                    <Command Handler="CommandHandler(command,record);" />
                                                </Listeners>
                                            </ext:CommandColumn>
                                        </Columns>
                                    </ColumnModel>
                                    <SelectionModel>
                                        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                                    </SelectionModel>
                                </ext:GridPanel>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <ext:Button ID="btnAddRow" runat="server" Icon="Add" Height="26" Text="Add New Row"
                                    Cls="btnblock bluecolor">
                                    <Listeners>
                                        <Click Handler="#{Store1}.add({});" />
                                    </Listeners>
                                </ext:Button>
                            </td>
                        </tr>
                        <tr>
                            <td valign="bottom" colspan="2">
                                <div class="popupButtonDiv">
                                    <ext:Button runat="server" ID="btnLevelSaveUpdate" Cls="btn btn-primary" Text="<i></i>Save">
                                        <DirectEvents>
                                            <Click OnEvent="btnLevelSaveUpdate_Click">
                                                <EventMask ShowMask="true" />
                                                <ExtraParams>
                                                    <ext:Parameter Name="Values" Value="Ext.encode(#{GridPanel1}.getRowsValues({selectedOnly : false}))"
                                                        Mode="Raw" />
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                        <Listeners>
                                            <Click Handler="valGroup = 'SaveUpdateLevel'; return CheckValidation();">
                                            </Click>
                                        </Listeners>
                                    </ext:Button>
                                    <div class="btnFlatOr">
                                        or</div>
                                    <ext:LinkButton runat="server" StyleSpec="padding:0px;" Cls="btnFlatLeftGap" ID="LinkButton1"
                                        Text="<i></i>Cancel" runat="server">
                                        <Listeners>
                                            <Click Handler="#{WindowLevel}.hide();">
                                            </Click>
                                        </Listeners>
                                    </ext:LinkButton>
                                </div>
                            </td>
                        </tr>
                    </table>
                </Content>
            </ext:Window>
        </div>
    </div>
    <br />
</asp:Content>
