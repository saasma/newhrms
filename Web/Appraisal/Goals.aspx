﻿<%@ Page Title="Rating Scale" Language="C#" MasterPageFile="~/Master/HR.Master" AutoEventWireup="true"
    CodeBehind="Goals.aspx.cs" Inherits="Web.Appraisal.Goals" %>

<%--<%@ Register Src="~/NewHR/UserControls/EmployeeDetailsCtl.ascx" TagName="EmployeeDetailsCtl"
    TagPrefix="uc1" %>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">


        
        var CommandHandler1 = function(command, record){
            <%= hiddenValue.ClientID %>.setValue(record.data.RatingScaleID);
                if(command=="Edit")
                {
                    <%= btnEditLevel.ClientID %>.fireEvent('click');
                }
                else
                {
                
                    <%= btnDeleteLevel.ClientID %>.fireEvent('click');
                }

             }
             

        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <ext:Hidden runat="server" ID="hiddenValue" />
    <ext:LinkButton runat="server" Hidden="true" ID="btnEditLevel" runat="server">
        <DirectEvents>
            <Click OnEvent="btnEditLevel_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:Button ID="btnDeleteLevel" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnDeleteLevel_Click">
                <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the Rating Scale?" />
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>

    <div class="separator bottom">
    </div>
    <div class="innerLR">
        <%--<uc1:EmployeeDetailsCtl Id="EmployeeDetailsCtl1" runat="server" />--%>
        <div class="widget">
            <div class="widget-head">
                <h4 class="heading">
                    Rating Scale</h4>
            </div>
            <div class="widget-body">
                <ext:GridPanel StyleSpec="margin-top:15px;" ID="GridLevels" runat="server" Cls="itemgrid">
                    <Store>
                        <ext:Store ID="Store3" runat="server">
                            <Model>
                                <ext:Model ID="Model4" runat="server" IDProperty="RatingScaleID">
                                    <Fields>
                                        <ext:ModelField Name="RatingScaleID" Type="String" />
                                        <ext:ModelField Name="Name" Type="string" />
                                        <ext:ModelField Name="Description" Type="string" />

                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                    </Store>
                    <ColumnModel>
                        <Columns>
                            <ext:Column ID="Column5" Sortable="false" MenuDisabled="true" runat="server" Text="Name"
                                Width="300" Align="Left" DataIndex="Name" />
                            <ext:Column ID="Column6" Sortable="false" MenuDisabled="true" runat="server" Text="Description"
                                Width="500" Align="Left" DataIndex="Description">
                            </ext:Column>
                            <ext:CommandColumn runat="server" Text="Actions" Align="Center" Width="63">
                                <Commands>
                                    <ext:GridCommand Text="<i></i>" Cls="btn-action glyphicons pencil btn-success" CommandName="Edit" />
                                    <ext:GridCommand Text="<i></i>" Cls="btn-action glyphicons remove_2 btn-danger" CommandName="Delete" />
                                </Commands>
                                <Listeners>
                                    <Command Handler="CommandHandler1(command,record);" />
                                </Listeners>
                            </ext:CommandColumn>
                        </Columns>
                    </ColumnModel>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                    </SelectionModel>
                </ext:GridPanel>
                <div class="buttonBlock">
                    <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="btnAddLevel" Cls="btnFlat"
                        BaseCls="btnFlat" Text="<i></i>Add New Rating Scale" runat="server">
                        <DirectEvents>
                            <Click OnEvent="btnAddLevel_Click">
                                <EventMask ShowMask="true" />
                            </Click>
                        </DirectEvents>
                    </ext:LinkButton>
                </div>
            </div>
        </div>
        <ext:Window ID="WindowLevel" runat="server" Title="Add/Edit Rating Scale" Icon="Application"
            Width="475" Height="555" BodyPadding="5" Hidden="true" Modal="true">
            <Content>
                <table class="fieldTable">
                    <tr>
                        <td>
                            <ext:ComboBox ForceSelection="true" QueryMode="Local" ID="cmbVisibility" FieldLabel="Visibility"
                                runat="server" LabelAlign="Top" DisplayField="Key" Width="180" LabelSeparator=""
                                ValueField="Value">
                                <Store>
                                    <ext:Store ID="Store2" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Key" />
                                            <ext:ModelField Name="Value" />
                                        </Fields>
                                    </ext:Store>
                                </Store>
                            </ext:ComboBox>
                        </td>
                        <td>
                            <ext:ComboBox ForceSelection="true" QueryMode="Local" ID="cmbCategory" FieldLabel="Category"
                                runat="server" LabelAlign="Top" DisplayField="Name" Width="180" LabelSeparator=""
                                ValueField="CategoryID">
                                <Store>
                                    <ext:Store ID="Store1" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="CategoryID" />
                                            <ext:ModelField Name="Name" />
                                        </Fields>
                                    </ext:Store>
                                </Store>
                            </ext:ComboBox>
                        </td>
                    </tr>
                    <tr>
                            <td colspan="2">
                                <ext:TextField ID="txtGoalName" runat="server" FieldLabel="Goal Name" LabelAlign="Top" Width="425">
                                </ext:TextField>
                            </td>
                    </tr>
                    <tr>
                            <td colspan="2">
                                <ext:TextField ID="txtMetric" runat="server" FieldLabel="Metric" LabelAlign="Top" Width="425">
                                </ext:TextField>
                            </td>
                    </tr>
                    <tr>
                            <td colspan="2">
                                <ext:TextField ID="txtWeight" LabelSeparator="" runat="server" FieldLabel="Weight in %"
                                Width="200" LabelAlign="Top">
                            </ext:TextField>
                            </td>
                    </tr>
                    <tr>
                        <td>
                            <pr:CalendarExtControl ID="txtStartDate" LabelSeparator="" runat="server" FieldLabel="Start Date"
                                Width="200" LabelAlign="Top">
                            </pr:CalendarExtControl>
                        </td>
                        <td>
                            <pr:CalendarExtControl ID="txtDueDate" LabelSeparator="" runat="server" FieldLabel="Due Date"
                                Width="200" LabelAlign="Top">
                            </pr:CalendarExtControl>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ext:TextField ID="txtComplete" LabelSeparator="" runat="server" FieldLabel="% Complete"
                                Width="200" LabelAlign="Top">
                            </ext:TextField>
                        </td>
                        <td>
                            <ext:TextField ID="txtStatus" LabelSeparator="" runat="server" FieldLabel="Status"
                                Width="200" LabelAlign="Top">
                            </ext:TextField>
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom" colspan="2">
                            <div class="popupButtonDiv">
                                <ext:LinkButton runat="server" StyleSpec="padding:0px;
                                " ID="btnLevelSaveUpdate" Cls="btnFlat" BaseCls="btnFlat" Text="<i></i>Save" runat="server">
                                    <DirectEvents>
                                        <Click OnEvent="btnLevelSaveUpdate_Click">
                                            <EventMask ShowMask="true" />
                                            <ExtraParams>
                                                <ext:Parameter Name="Values" Value="Ext.encode(#{GridPanel1}.getRowsValues({selectedOnly : false}))"
                                                    Mode="Raw" />
                                            </ExtraParams>
                                        </Click>
                                    </DirectEvents>
                                    <Listeners>
                                        <Click Handler="valGroup = 'SaveUpdateLevel'; return CheckValidation();">
                                        </Click>
                                    </Listeners>
                                </ext:LinkButton>
                                <div class="btnFlatOr">
                                    or</div>
                                <ext:LinkButton runat="server" StyleSpec="padding:0px;" Cls="btnFlatLeftGap" ID="LinkButton1"
                                    Text="<i></i>Cancel" runat="server">
                                    <Listeners>
                                        <Click Handler="#{WindowLevel}.hide();">
                                        </Click>
                                    </Listeners>
                                </ext:LinkButton>
                            </div>
                        </td>
                    </tr>
                </table>
            </Content>
        </ext:Window>
    </div>
    <br />
</asp:Content>
