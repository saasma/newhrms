﻿<%@ Page Title="Appraisal Detail Report" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="AppraisalDetailsReport.aspx.cs" Inherits="Web.Appraisal.AppraisalDetailsReport" %>

<%--<%@ Register Src="~/NewHR/UserControls/EmployeeDetailsCtl.ascx" TagName="EmployeeDetailsCtl"
    TagPrefix="uc1" %>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

      

             
    </script>
    <style type="text/css">
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
        #content
        {
            margin: inherit;
        }
         .completed, .completed a, .completed td
        {
            color: #469146;
            background-color: #F0FFF0 !important;
        }

    </style>
    <script type="text/javascript">


        //        var Hidden_PageSize = null;
        //        var Hidden_EmployeeId = null;
        //        var PagingToolbar_AdjustmentList = null;
        //        var Hidden_CurrentPage = null;


        var getRowClass = function (record) {
    
            var status = record.data.Status;
        

            if(status  == "15")
            {
                return "completed";
            }
        
            return "";

        };

         var prepareToolbar = function (grid, toolbar, rowIndex, record) {

            if(record.data.Status =='1' || record.data.Status =='2')
            {
            }
            else{
                toolbar.hide();
            }

        
        };


        //to track if dash board mode or customer details listing


        Ext.onReady(function () {



        });

        function renderView(e1,e2,record)
        {
            return '<a href="EmployeeAppraisalHRPage.aspx?efid='+ record.data.AppraisalEmployeeFormID + '&fid='+ record.data.AppraisalFormRef_ID +'">View</a>';
        }


        function searchList() {
            <%=PagingToolbar1.ClientID %>.doRefresh();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
<div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Appraisal Report
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel" style="margin-top:0px !important; padding-top:0px !important;">
    <ext:Hidden runat="server" ID="hiddenValue" />
    <ext:Hidden runat="server" ID="hiddenValueRow" />
    <ext:Hidden ID="Hidden_EmployeeId" Text="-1" runat="server">
    </ext:Hidden>
    <ext:Hidden ID="Hidden_CurrentPage" runat="server" Text="1">
    </ext:Hidden>
    <ext:Hidden ID="Hidden_searchText" runat="server">
    </ext:Hidden>
    <ext:Hidden ID="Hidden_PageSize" runat="server" Text="20">
    </ext:Hidden>
   
    <div class="separator bottom">
    </div>
    <ext:Store ID="StorePaging" runat="server">
        <Model>
            <ext:Model ID="StorePagingModel" runat="server" IDProperty="Value">
                <Fields>
                    <ext:ModelField Name="Text" Type="Int">
                    </ext:ModelField>
                    <ext:ModelField Name="Value" Type="Int">
                    </ext:ModelField>
                </Fields>
            </ext:Model>
        </Model>
    </ext:Store>
    <div class="separator bottom">
    </div>
    <div class="innerLR">
        <h4 class="heading">
            
        </h4>
        <%--<uc1:EmployeeDetailsCtl Id="EmployeeDetailsCtl1" runat="server" />--%>
        <div>
            <table class="fieldTable firsttdskip">
                <tr>
                    <td>
                        <ext:ComboBox FieldLabel="Status" ID="cmbStatus" Width="180px" runat="server" ValueField="StepID"
                            DisplayField="StepName" LabelAlign="Top" LabelSeparator="" ForceSelection="true"
                            QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store1" runat="server">
                                    <Model>
                                        <ext:Model ID="Model1" IDProperty="StepID" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="StepID" />
                                                <ext:ModelField Name="StepName" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                    </td>
                    <td>
                        <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                            <Proxy>
                                <ext:AjaxProxy Url="../Handler/EmpSearch.ashx">
                                    <ActionMethods Read="GET" />
                                    <Reader>
                                        <ext:JsonReader Root="plants" TotalProperty="total" />
                                    </Reader>
                                </ext:AjaxProxy>
                            </Proxy>
                            <Model>
                                <ext:Model ID="Model2" IDProperty="Name" Name="ItemLineModel" runat="server">
                                    <Fields>
                                        <ext:ModelField Name="Name" Type="String" />
                                        <ext:ModelField Name="EmployeeId" Type="String" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                        <ext:ComboBox LabelSeparator="" ID="cmbSearch" FieldLabel="Search Employee" LabelWidth="70"
                            LabelAlign="Top" runat="server" DisplayField="Name" ValueField="EmployeeId" StoreID="storeSearch"
                            TypeAhead="false" Width="160" PageSize="9999" HideBaseTrigger="true" MinChars="1"
                            TriggerAction="All" ForceSelection="true">
                            <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                                <ItemTpl ID="ItemTpl1" runat="server">
                                    <Html>
                                        <tpl>
                                            <div class="search-item">
                                                            <span>{Name}</span>  
                                             </div>
					                </tpl>
                                    </Html>
                                </ItemTpl>
                            </ListConfig>
                            <Triggers>
                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                            </Triggers>
                            <Listeners>
                                <Select Handler="this.getTrigger(0).show();" />
                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                            </Listeners>
                        </ext:ComboBox>
                    </td>
                    <td>
                        <ext:Store ID="StoreLevel" runat="server">
                            <Model>
                                <ext:Model ID="Model3" runat="server" IDProperty="LevelId">
                                    <Fields>
                                        <ext:ModelField Name="LevelId" Type="String" />
                                        <ext:ModelField Name="GroupLevel" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                        <ext:Store ID="StoreBranch" runat="server">
                            <Model>
                                <ext:Model ID="Model5" runat="server" IDProperty="BranchId">
                                    <Fields>
                                        <ext:ModelField Name="BranchId" Type="String" />
                                        <ext:ModelField Name="Name" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                        <ext:ComboBox FieldLabel="Level" ID="cmbLevelList" Width="160px" runat="server" ValueField="LevelId"
                            DisplayField="GroupLevel" StoreID="StoreLevel" LabelAlign="Top" LabelSeparator=""
                            ForceSelection="true" QueryMode="Local">
                            <Triggers>
                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                            </Triggers>
                            <Listeners>
                                <Select Handler="this.getTrigger(0).show();" />
                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                            </Listeners>
                        </ext:ComboBox>
                    </td>
                    <td>
                        <ext:ComboBox FieldLabel="Branch" ID="cmbBranch" Width="160px" runat="server" ValueField="BranchId"
                            DisplayField="Name" StoreID="StoreBranch" LabelAlign="Top" LabelSeparator=""
                            ForceSelection="true" QueryMode="Local">
                            <Triggers>
                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                            </Triggers>
                            <Listeners>
                                <Select Handler="this.getTrigger(0).show();" />
                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                            </Listeners>
                        </ext:ComboBox>
                    </td>
                    <td>
                        <ext:ComboBox ForceSelection="true" QueryMode="Local" ID="cmbForm" FieldLabel="Select Form"
                            runat="server" LabelAlign="Top" DisplayField="Name" Width="176" LabelSeparator=""
                            ValueField="AppraisalFormID">
                            <Store>
                                <ext:Store ID="storeForm" runat="server">
                                    <Fields>
                                        <ext:ModelField Name="AppraisalFormID" Type="String" />
                                        <ext:ModelField Name="Name" />
                                    </Fields>
                                </ext:Store>
                            </Store>
                            <Triggers>
                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                            </Triggers>
                            <Listeners>
                                <Select Handler="this.getTrigger(0).show();" />
                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                            </Listeners>
                        </ext:ComboBox>
                    </td>
                    <td>
                            <ext:MultiCombo Width="150" LabelSeparator="" SelectionMode="All" LabelWidth="50" LabelAlign="Top"
                                ID="cmbPeriod" DisplayField="Start" ValueField="PeriodId"
                                runat="server" FieldLabel="Period">
                                <Store>
                                    <ext:Store ID="Store5" runat="server">
                                        <Model>
                                            <ext:Model ID="Model8" IDProperty="PeriodId" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="PeriodId" />
                                                    <ext:ModelField Name="Start" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                            </ext:MultiCombo>
                        </td>
                    <td style="padding-top: 20px">
                        <ext:Button runat="server" ID="btnLoad" Cls="btn btn-primary" Text="<i></i>Load" >
                            <Listeners>
                                <Click Handler="searchList();" />
                            </Listeners>
                        </ext:Button>
                    </td>
                    <td style="padding-top: 20px">
                        <ext:Button runat="server" AutoPostBack="true" OnClick="btnExport_Click" 
                            ID="btnExport" Cls="btn btn-primary" Text="<i></i>Export" runat="server">
                        </ext:Button>
                    </td>
                </tr>
            </table>
            <div>
                <ext:GridPanel StyleSpec="margin-top:15px;" AutoScroll="true"  ID="GridLevels"
                    runat="server">
                    <Store>
                        <ext:Store ID="Store3" runat="server" AutoLoad="true" PageSize="50">
                            <Proxy>
                                <ext:AjaxProxy Json="true" Url="../Handler/AppraisalReportHandler.ashx">
                                    <ActionMethods Read="GET" />
                                    <Reader>
                                        <ext:JsonReader Root="data" TotalProperty="total" />
                                    </Reader>
                                </ext:AjaxProxy>
                            </Proxy>
                            <AutoLoadParams>
                                <ext:Parameter Name="start" Value="0" Mode="Raw" />
                            </AutoLoadParams>
                            <Parameters>
                                <ext:StoreParameter Name="EmployeeId" Value="#{cmbSearch}.getValue()" Mode="Raw"
                                    ApplyMode="Always" />
                                <ext:StoreParameter Name="StatusId" Value="#{cmbStatus}.getValue()" Mode="Raw" ApplyMode="Always" />
                                <ext:StoreParameter Name="LevelId" Value="#{cmbLevelList}.getValue()" Mode="Raw"
                                    ApplyMode="Always" />
                                <ext:StoreParameter Name="BranchId" Value="#{cmbBranch}.getValue()" Mode="Raw" ApplyMode="Always" />
                                <ext:StoreParameter Name="FormId" Value="#{cmbForm}.getValue()" Mode="Raw" ApplyMode="Always" />
                                <ext:StoreParameter Name="PeriodId" Value="#{cmbPeriod}.getValue()" Mode="Raw"
                                    ApplyMode="Always" />
                            </Parameters>
                            <Model>
                                <ext:Model ID="Model4" runat="server">
                                    <Fields>
                                        <ext:ModelField Name="EmployeeId" Type="String" />
                                        <ext:ModelField Name="IdCardNo" Type="String" />
                                        <ext:ModelField Name="EmployeeName" Type="String" />
                                        <ext:ModelField Name="ReviewYear" Type="String" />
                                        <ext:ModelField Name="Branch" Type="string" />
                                        <ext:ModelField Name="Level" Type="string" />
                                        <ext:ModelField Name="FormName" Type="String" />
                                        <ext:ModelField Name="Designation" Type="String" />
                                        <ext:ModelField Name="StatusText" Type="String" />
                                        <ext:ModelField Name="SelfSubmittedOn" Type="Date" />
                                        
                                        <ext:ModelField Name="Recommendation" Type="String" />
                                        <ext:ModelField Name="TargetScore" Type="String" />

                                        <ext:ModelField Name="EmpActivityScore" Type="String" />
                                        <ext:ModelField Name="EmpCompetencyScore" Type="String" />
                                        <ext:ModelField Name="EmpQuestionScore" Type="String" />
                                        <ext:ModelField Name="EmpTotalScore" Type="String" />
                                        <ext:ModelField Name="EmpGrade" Type="String" />
                                        <ext:ModelField Name="EmpGradeSummary" Type="String" />
                                       

                                       
                                        <ext:ModelField Name="SupActivityScore" Type="String" />
                                        <ext:ModelField Name="SupCompetencyScore" Type="String" />
                                        <ext:ModelField Name="SupQuestionScore" Type="String" />
                                        <ext:ModelField Name="SupTotalScore" Type="String" />
                                        <ext:ModelField Name="SupGrade" Type="String" />
                                        <ext:ModelField Name="SupGradeSummary" Type="String" />
                                         
                                         <ext:ModelField Name="ActivitySupervisorComment" Type="String" />

                                        <ext:ModelField Name="Step3Comment" Type="String" />
                                        <ext:ModelField Name="Step4Comment" Type="String" />
                                        <ext:ModelField Name="Step5Comment" Type="String" />
                                        <ext:ModelField Name="Step6Comment" Type="String" />
                                        <ext:ModelField Name="Step7Comment" Type="String" />
                                        <ext:ModelField Name="Step8Comment" Type="String" />
                                        <ext:ModelField Name="Step9Comment" Type="String" />
                                        <ext:ModelField Name="Step10Comment" Type="String" />
                                        <ext:ModelField Name="Step11Comment" Type="String" />
                                        <ext:ModelField Name="Step15Comment" Type="String" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                    </Store>
                    <ColumnModel ID="columnList" runat="server">
                        <Columns>
                            <ext:Column ID="Column4" Locked="true"  Sortable="false" MenuDisabled="true" runat="server" Text="EIN"
                                Width="40" Align="Left" DataIndex="EmployeeId" />
                            <ext:Column ID="Column10" Locked="true"  Sortable="false" MenuDisabled="true" runat="server" Text="I No"
                                Width="40" Align="Left" DataIndex="IdCardNo" />
                            <ext:Column ID="Column5" Locked="true"  Sortable="false" MenuDisabled="true" runat="server" Text="Employee Name"
                                Width="180" Align="Left" DataIndex="EmployeeName" />
                            <ext:Column ID="Column13" Sortable="false" MenuDisabled="true" runat="server" Text="Review Year"
                                Width="100" Align="Left" DataIndex="ReviewYear" />
                            <ext:Column ID="Column7" Sortable="false" MenuDisabled="true" runat="server" Text="Branch"
                                Width="110" Align="Left" DataIndex="Branch" />
                            <ext:Column ID="Column8" Sortable="false" MenuDisabled="true" runat="server" Text="Level"
                                Width="110" Align="Left" DataIndex="Level" />
                            <ext:Column ID="Column6" Sortable="false" MenuDisabled="true" runat="server" Text="Form Name"
                                Width="150" Align="Left" DataIndex="FormName" />
                            <ext:Column ID="Column14" Sortable="false" MenuDisabled="true" runat="server" Text="Designation"
                                Width="150" Align="Left" DataIndex="Designation" />
                            <ext:Column ID="Column1" Sortable="false" MenuDisabled="true" runat="server" Text="Status"
                                Width="80" Align="Left" DataIndex="StatusText" />
                            <ext:DateColumn ID="colRolloutDate" runat="server" Align="Left" Text="Date Submitted"
                                MenuDisabled="true" Sortable="false" Format="yyyy/MM/dd" DataIndex="SelfSubmittedOn"
                                Resizable="false" Draggable="false" Width="103">
                            </ext:DateColumn>

                            <ext:Column ID="Column26" Sortable="false" MenuDisabled="true" runat="server" Text="Recommendation"
                                Width="100" Align="Center" DataIndex="Recommendation" />
                            <ext:Column ID="Column27" Sortable="false" MenuDisabled="true" runat="server" Text="Target Score"
                                Width="100" Align="Center" DataIndex="TargetScore" />


                            <ext:Column ID="Column2" Sortable="false" MenuDisabled="true" runat="server" Text="Employee Activity Score"
                                Width="100" Align="Center" DataIndex="EmpActivityScore" />
                            <ext:Column ID="Column3" Sortable="false" MenuDisabled="true" runat="server" Text="Employee Competency Score"
                                Width="100" Align="Center" DataIndex="EmpCompetencyScore" />
                            <ext:Column ID="Column9" Sortable="false" MenuDisabled="true" runat="server" Text="Employee Question Score"
                                Width="100" Align="Center" DataIndex="EmpQuestionScore" />
                            <ext:Column ID="Column18" Sortable="false" MenuDisabled="true" runat="server" Text="Employee Total Score"
                                Width="100" Align="Center" DataIndex="EmpTotalScore" />
                            <ext:Column ID="Column19" Sortable="false" MenuDisabled="true" runat="server" Text="Employee Grade"
                                Width="100" Align="Center" DataIndex="EmpGrade" />
                            <ext:Column ID="Column20" Sortable="false" MenuDisabled="true" runat="server" Text="Employee Grade Summary"
                                Width="100" Align="Center" DataIndex="EmpGradeSummary" />

                            
                            <ext:Column ID="Column23" Sortable="false" MenuDisabled="true" runat="server" Text="Supervisor Activity Score"
                                Width="100" Align="Center" DataIndex="SupActivityScore" />
                            <ext:Column ID="Column24" Sortable="false" MenuDisabled="true" runat="server" Text="Supervisor Competency Score"
                                Width="100" Align="Center" DataIndex="SupCompetencyScore" />
                            <ext:Column ID="Column11" Sortable="false" MenuDisabled="true" runat="server" Text="Supervisor Question Score"
                                Width="100" Align="Center" DataIndex="SupQuestionScore" />
                            <ext:Column ID="Column12" Sortable="false" MenuDisabled="true" runat="server" Text="Supervisor Total Score"
                                Width="100" Align="Center" DataIndex="SupTotalScore" />
                            <ext:Column ID="Column15" Sortable="false" MenuDisabled="true" runat="server" Text="Supervisor Grade"
                                Width="100" Align="Center" DataIndex="SupGrade" />
                            <ext:Column ID="Column16" Sortable="false" MenuDisabled="true" runat="server" Text="Supervisor Grade Summary"
                                Width="100" Align="Center" DataIndex="SupGradeSummary" />


                           <ext:Column ID="Column32" Sortable="false" MenuDisabled="true" runat="server" Text="Activity Supervisor Comment"
                                Width="100" Align="Center" DataIndex="ActivitySupervisorComment" />
                            <ext:Column ID="Step3Comment" Hidden="true" Sortable="false" MenuDisabled="true"
                                runat="server" Text="Step3Comment" Width="120" Align="Left" DataIndex="Step3Comment">
                            </ext:Column>
                            <ext:Column ID="Step4Comment" Hidden="true" Sortable="false" MenuDisabled="true"
                                runat="server" Text="Step4Comment" Width="120" Align="Left" DataIndex="Step4Comment">
                            </ext:Column>
                            <ext:Column ID="Step5Comment" Hidden="true" Sortable="false" MenuDisabled="true"
                                runat="server" Text="Step5Comment" Width="120" Align="Left" DataIndex="Step5Comment">
                            </ext:Column>
                            <ext:Column ID="Step6Comment" Hidden="true" Sortable="false" MenuDisabled="true"
                                runat="server" Text="Step6Comment" Width="120" Align="Left" DataIndex="Step6Comment">
                            </ext:Column>
                            <ext:Column ID="Step7Comment" Hidden="true" Sortable="false" MenuDisabled="true"
                                runat="server" Text="Step7Comment" Width="120" Align="Left" DataIndex="Step7Comment">
                            </ext:Column>
                            <ext:Column ID="Step8Comment" Hidden="true" Sortable="false" MenuDisabled="true"
                                runat="server" Text="Step8Comment" Width="120" Align="Left" DataIndex="Step8Comment">
                            </ext:Column>
                            <ext:Column ID="Step9Comment" Hidden="true" Sortable="false" MenuDisabled="true"
                                runat="server" Text="Step9Comment" Width="120" Align="Left" DataIndex="Step9Comment">
                            </ext:Column>
                            <ext:Column ID="Step10Comment" Hidden="true" Sortable="false" MenuDisabled="true"
                                runat="server" Text="Step10Comment" Width="120" Align="Left" DataIndex="Step10Comment">
                            </ext:Column>
                            <ext:Column ID="Step11Comment" Hidden="true" Sortable="false" MenuDisabled="true"
                                runat="server" Text="Step11Comment" Width="120" Align="Left" DataIndex="Step11Comment">
                            </ext:Column>
                            <ext:Column ID="Step15Comment" Sortable="false" MenuDisabled="true" runat="server"
                                Text="Step15Comment" Width="120" Align="Left" DataIndex="Step15Comment">
                            </ext:Column>
                        </Columns>
                    </ColumnModel>
                    
 <View>
                    <ext:GridView EnableTextSelection="true" ID="GridView1" runat="server">
                        <Listeners>
                        </Listeners>
                        <GetRowClass Fn="getRowClass" />
                    </ext:GridView>
                </View>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                    </SelectionModel>
                    <BottomBar>
                        <ext:PagingToolbar ID="PagingToolbar1" runat="server" PageSize="50" DisplayInfo="true"
                            DisplayMsg="Displaying employees {0} - {1} of {2}" EmptyMsg="No Records to display">
                            <Items>
                                <ext:ComboBox Hidden="true" runat="server" SelectedIndex="0" Width="80" ID="cmbPageSize">
                                    <Items>
                                        <ext:ListItem Text="20" Value="20" />
                                        <ext:ListItem Text="30" Value="30" />
                                        <ext:ListItem Text="50" Value="50" />
                                    </Items>
                                    <Listeners>
                                        <Select Handler="#{PagingToolbar1}.pageSize = parseInt( '50'); searchList();" />
                                    </Listeners>
                                </ext:ComboBox>
                            </Items>
                        </ext:PagingToolbar>
                    </BottomBar>
                </ext:GridPanel>
            </div>
        </div>
    </div>
    </div>
    <br />
</asp:Content>
