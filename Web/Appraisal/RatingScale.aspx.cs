﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using System.Xml;

namespace Web.Appraisal
{
    public partial class RatingScale : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.EmployeeAppraisal;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
              
            }
        }

        public void Initialise()
        {
            LoadLevels();
        }
        
        private void LoadLevels()
        {
            GridLevels.GetStore().DataSource = AppraisalManager.GetAllRatingScale();
            GridLevels.GetStore().DataBind();

            GridPanel1.GetStore().DataSource = gridData().Take(3);
            GridPanel1.GetStore().DataBind();

            Radio3.Checked = true;
        }


        
        public void ClearLevelFields()
        {
            hiddenValue.Text = "";
            txtName.Text = "";
            txtBoxDes.Text = "";
            /*txtName.Text = "";
            txtRelation.Text = "";
            txtRemarks.Text = "";
            txtDOB.Text = "";
            chkHasDependent.Checked = false;
            txtSPDate.Text = "";
            txtAgeOnSPDate.Text = "";
             * */

        }

      
        
        protected void btnAddLevel_Click(object sender, DirectEventArgs e)
        {
            

            ClearLevelFields();
            WindowLevel.Show();
        }


        protected void btnDeleteLevel_Click(object sender, DirectEventArgs e)
        {
            int ratingScale = int.Parse(hiddenValue.Text.Trim());
            Status status = AppraisalManager.DeleteRatingScale(ratingScale);
            if (status.IsSuccess)
            {
                LoadLevels();
                NewMessage.ShowNormalMessage("Rating Scale deleted.");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        

        protected void btnEditLevel_Click(object sender, DirectEventArgs e)
        {
            int levelId = int.Parse(hiddenValue.Text.Trim());
            AppraisalRatingScale entity = AppraisalManager.GetRatingScaleById(levelId);
            List<AppraisalRatingScaleLine> lines = new List<AppraisalRatingScaleLine>();
            WindowLevel.Show();

            txtName.Text = entity.Name;
            txtBoxDes.Text = entity.Description;
            lines = entity.AppraisalRatingScaleLines.ToList();

            GridPanel1.Store[0].DataSource = lines;
            GridPanel1.Store[0].DataBind();

            

        }

        protected void btnLevelSaveUpdate_Click(object sender, DirectEventArgs e)
        {

            
            Page.Validate("InsertUpdateLevel");
            if (Page.IsValid)
            {
                AppraisalRatingScale ratingScale = new AppraisalRatingScale();
                List<AppraisalRatingScaleLine> lines = new List<AppraisalRatingScaleLine>();
                ratingScale.Name = txtName.Text;
                ratingScale.Description = txtBoxDes.Text;

                string json = e.ExtraParams["Values"];

                if (string.IsNullOrEmpty(json))
                {
                    return;
                }
                 if(!string.IsNullOrEmpty(hiddenValue.Text))
                 {
                     ratingScale.RatingScaleID = int.Parse(hiddenValue.Text);
                 }

                lines = JSON.Deserialize<List<AppraisalRatingScaleLine>>(json);

                lines = lines.Where(x => x.Score.ToString().Trim() !="" && x.Label.Trim() != "").ToList();

                Status status = AppraisalManager.InsertUpdateRatingScale(ratingScale, lines);
                if (status.IsSuccess)
                {
                    WindowLevel.Hide();
                    LoadLevels();
                    NewMessage.ShowNormalMessage("Rating Scale saved.");
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }
                
            }
             
             
        }




        protected void RadioSelect_Change(object sender, DirectEventArgs e)
        {

            Radio radio = (Radio)sender;
            Store store = this.GridPanel1.GetStore();
           
            if (radio.ID == "Radio3")
            {

                store.DataSource = gridData().Take(3);
                store.DataBind();
            }
            else if (radio.ID == "Radio5")
            {

                store.DataSource = gridData().Take(5);
                store.DataBind();
            }
            else if (radio.ID == "Radio7")
            {

                store.DataSource = gridData().Take(7);
                store.DataBind();
            }
            else
            {

                store.DataSource = gridData().Where(x => x.Score.ToString() == "");


                store.DataBind();
            }

        }


        public List<AppraisalRatingScaleLine> gridData()
        {
            List<AppraisalRatingScaleLine> retList = new List<AppraisalRatingScaleLine>();
            retList.Add(new AppraisalRatingScaleLine{Score= 1 });
            retList.Add(new AppraisalRatingScaleLine { Score = 2 });
            retList.Add(new AppraisalRatingScaleLine { Score = 3 });
            retList.Add(new AppraisalRatingScaleLine { Score = 4 });
            retList.Add(new AppraisalRatingScaleLine { Score = 5 });
            retList.Add(new AppraisalRatingScaleLine { Score = 6 });
            retList.Add(new AppraisalRatingScaleLine { Score = 7 });
            return retList;
        }



        
    }
}