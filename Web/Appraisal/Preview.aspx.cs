﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using System.Xml;

namespace Web.Appraisal
{
    public partial class Preview : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();

            }
        }
        public int GetFormID()
        {
            return int.Parse(Request.QueryString["fid"]);
        }
        public void Initialise()
        {

            CompetencyBlock1.AppraisalFormID = GetFormID();
            CompetencyBlock1.LoadActivityDetails(false);
            CompetencyBlock1.LoadCompetencyDetails();
            CompetencyBlock1.LoadTargetDetails();
            CompetencyBlock1.LoadQuestionnaireDetails(0,int.MaxValue);
            CompetencyBlock1.LoadReviewDetails();

            AppraisalForm form = AppraisalManager.getFormInstanceByID(GetFormID());
            if (form != null)
            {
                
                mainTitle.InnerHtml =
                    form.Name + " for Employee";

                // Introudction
                //spanIntroductionNumber.InnerHtml = "1";
                spanIntroductionName.InnerHtml = form.IntroductionName;
                divIntroductionDescription.InnerHtml = form.IntroductionDescription;

                // Objective
                //spanObjectiveNumber.InnerHtml = "2";
                if (form.HideObjectiveBlock != null && form.HideObjectiveBlock.Value)
                    blockObjective.Visible = false;
                spanObjectiveName.InnerHtml = form.ObjectiveName;
                divObjectiveDescription.InnerHtml = form.ObjectiveDescription;

                // Comment
                spanCommentName.InnerHtml = form.SummaryName;
                divCommentDescription.InnerHtml = form.SummaryDescription;



                // Singature
                spanSignatureName.InnerHtml = form.SignationName;
                divSignatureDescription.InnerHtml = form.SignationDescription;

            }
        }

        private void LoadLevels()
        {



        }



        public void ClearLevelFields()
        {
            hiddenValue.Text = "";




        }



        protected void btnAddLevel_Click(object sender, DirectEventArgs e)
        {


            ClearLevelFields();


        }








    }
}