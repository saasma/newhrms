﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using BLL;
using DAL;
using BLL.Manager;

namespace Web.NewHR
{
    public partial class AppraisalRolloutGroup : System.Web.UI.Page
    {
        static bool btnEditIsClicked = false;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!X.IsAjaxRequest)
            {
                Initialize();
            }
        }
        public void Initialize()
        {
            LoadGroups();
        }
        public void LoadGroups()
        {
            GridRolloutGroups.Store[0].DataSource = AppraisalManager.GetAllRolloutGroup();
            GridRolloutGroups.Store[0].DataBind();
        }
        protected void btnCreateGroup_Click(object sender, DirectEventArgs e)
        {
            btnEditIsClicked = false;
            ClearGroupFields();
            WindowGroup.Show();
        }
        protected void btnEditGroup_Click(object sender, DirectEventArgs e)
        {
            btnEditIsClicked = true;
            int GroupID = int.Parse(hiddenValue.Text.Trim());
            DAL.AppraisalRolloutGroup entity = AppraisalManager.GetGroupByID(GroupID);
            txtGroupName.Text = entity.Name;
            WindowGroup.Show();
        }
        protected void btnDeleteGroup_Click(object sender, DirectEventArgs e)
        {
            int GroupID = int.Parse(hiddenValue.Text.Trim());
            Status status = AppraisalManager.DeleteRolloutGroup(GroupID);
            if (status.IsSuccess)
            {
                LoadGroups();
                NewMessage.ShowNormalMessage("Rollout Group Deleted.");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }
        protected void btnSaveGroupSaveUpdate_Click(object sender, DirectEventArgs e)
        {
            if (txtGroupName.Text == "")
            {
                NewMessage.ShowWarningMessage("Group name cannot be empty.");
                txtGroupName.Focus();
            }

            else
            {
                bool GroupNameAlreadyExists = false;
                if (!btnEditIsClicked)
                {
                    List<string> groupList = AppraisalManager.GetAllRolloutGroupNames();
                    string txtGroupTrimmed = txtGroupName.Text.ToString().Trim().ToLower();
                    foreach (string lst in groupList)
                    {
                        if (lst.Trim().ToLower() == txtGroupTrimmed)
                        {
                            GroupNameAlreadyExists = true;
                            break;
                        }
                    }
                }

                if (GroupNameAlreadyExists)
                {
                    NewMessage.ShowWarningMessage("The Group you entered already exist.\nEnter a different group name.");
                    txtGroupName.Focus();
                }
                else
                {
                    // save to the database
                    SaveUpdateGroup(e);
                }
            }
        }
        public void SaveUpdateGroup(DirectEventArgs e)
        {
            DAL.AppraisalRolloutGroup ent = new DAL.AppraisalRolloutGroup();
            bool isInsert = true;
            if (!string.IsNullOrEmpty(hiddenValue.Text))
            {
                isInsert = false;
                ent.RolloutGroupID = int.Parse(hiddenValue.Text.Trim());
            }
            ent.Name = txtGroupName.Text.Trim();

            Status status = AppraisalManager.InsertUpdateRolloutGroup(ent, isInsert);
            if (status.IsSuccess)
            {
                WindowGroup.Hide();
                LoadGroups();
                NewMessage.ShowNormalMessage("Group Saved.");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }
        public void ClearGroupFields()
        {
            hiddenValue.Text = "";
            txtGroupName.Text = "";
        }
    }
}
