﻿<%@ Page Language="C#" AutoEventWireup="true" Title="Appraisal Points Calculation" CodeBehind="RecalculatePoints.aspx.cs"
    Inherits="Web.NewHR.RecalculatePoints" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript">
       
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <ext:ResourceManager DisableViewState="false" GZip="true" ID="ResourceManager1" ShowWarningOnAjaxFailure="false"
            Namespace="" runat="server">
        </ext:ResourceManager>
        <ext:Store runat="server" ID="storeAccount">
            <Model>
                <ext:Model ID="Model_Account" runat="server" IDProperty="AccountID">
                    <Fields>
                        <ext:ModelField Name="Text" />
                        <ext:ModelField Name="Value" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
        <div class="page-title">
            <div class="titleBlock">
                <h2 id="header" runat="server">
                    Re-Calculate Appraisal Points</h2>
            </div>
        </div>
        <div id="container">
       
        </div>
      
    </div>
    </form>
</body>
</html>
