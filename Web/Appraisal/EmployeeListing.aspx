﻿<%@ Page Title="Appraisal Submitted Form List" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="EmployeeListing.aspx.cs" Inherits="Web.Appraisal.EmployeeListing" %>

<%--<%@ Register Src="~/NewHR/UserControls/EmployeeDetailsCtl.ascx" TagName="EmployeeDetailsCtl"
    TagPrefix="uc1" %>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

      

             
    </script>
    <style type="text/css">
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
        #content
        {
            margin: inherit;
        }
         .completed, .completed a, .completed td
        {
            color: #469146;
            background-color: #F0FFF0 !important;
        }
    </style>
    <script type="text/javascript">


        //        var Hidden_PageSize = null;
        //        var Hidden_EmployeeId = null;
        //        var PagingToolbar_AdjustmentList = null;
        //        var Hidden_CurrentPage = null;


        var getRowClass = function (record) {
    
            var status = record.data.Status;
        

            if(status  == "15")
            {
                return "completed";
            }
        
            return "";

        };
         var prepareToolbar = function (grid, toolbar, rowIndex, record) {

//            if(record.data.Status =='1' || record.data.Status =='2' || record.data.Status =='3')
//            {
//            }
//            else{
//                toolbar.hide();
//            }

        
        };

         var CommandHandler1 = function(command, record){
            <%= hiddenValue.ClientID %>.setValue(record.data.AppraisalEmployeeFormID);
           
                <%= btnReverseStatus.ClientID %>.fireEvent('click');
           
            }

        //to track if dash board mode or customer details listing


        Ext.onReady(function () {



        });

        function getStatusFilter()
        {
            var cmb = <%= cmbServiceStatus.ClientID %>;
            var text = "";
            for(i=0;i<cmb.getSelectedValues().length;i++)
            {
                if(text=="")    
                    text = cmb.getSelectedValues()[i];
                else
                    text += "," + cmb.getSelectedValues()[i];
            }

            return text;
        }

        function processExport()
        {
            var value = getStatusFilter();
            <%= Hidden_statslist.ClientID %>.setValue(value);
        }

        function renderView(e1,e2,record)
        {
            return '<a href="EmployeeAppraisalHRPage.aspx?efid='+ record.data.AppraisalEmployeeFormID + '&fid='+ record.data.AppraisalFormRef_ID +'">View</a>';
        }


        function searchList() {
            <%=PagingToolbar1.ClientID %>.doRefresh();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Employee Form Received
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel" style="margin-top: 0px !important; padding-top: 0px !important;">
        <ext:Hidden runat="server" ID="hiddenValue" />
        <ext:Hidden runat="server" ID="hiddenValueRow" />
        <ext:Hidden ID="Hidden_EmployeeId" Text="-1" runat="server">
        </ext:Hidden>
        <ext:Hidden ID="Hidden_CurrentPage" runat="server" Text="1">
        </ext:Hidden>
        <ext:Hidden ID="Hidden_searchText" runat="server">
        </ext:Hidden>
        <ext:Hidden ID="Hidden_PageSize" runat="server" Text="20">
        </ext:Hidden>
         <ext:Hidden ID="Hidden_statslist" runat="server" Text="20">
        </ext:Hidden>
        <ext:LinkButton runat="server" Hidden="true" ID="btnReverseStatus" runat="server">
            <DirectEvents>
                <Click OnEvent="btnReverseStatus_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:LinkButton>
        <div class="separator bottom">
        </div>
        <ext:Store ID="StorePaging" runat="server">
            <Model>
                <ext:Model ID="StorePagingModel" runat="server" IDProperty="Value">
                    <Fields>
                        <ext:ModelField Name="Text" Type="Int">
                        </ext:ModelField>
                        <ext:ModelField Name="Value" Type="Int">
                        </ext:ModelField>
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
        <div class="separator bottom">
        </div>
        <div class="innerLR">
            <%--<uc1:EmployeeDetailsCtl Id="EmployeeDetailsCtl1" runat="server" />--%>
            <div>
                <table class="fieldTable firsttdskip">
                    <tr>
                        <td>
                            <ext:ComboBox FieldLabel="Status" ID="cmbStatus" Width="150" runat="server" ValueField="StepID"
                                DisplayField="StepName" LabelAlign="Top" LabelSeparator="" ForceSelection="true"
                                QueryMode="Local">
                                <Store>
                                    <ext:Store ID="Store1" runat="server">
                                        <Model>
                                            <ext:Model ID="Model1" IDProperty="StepID" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="StepID" />
                                                    <ext:ModelField Name="StepName" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                            </ext:ComboBox>
                        </td>
                        <td>
                            <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                                <Proxy>
                                    <ext:AjaxProxy Url="../Handler/EmpSearch.ashx">
                                        <ActionMethods Read="GET" />
                                        <Reader>
                                            <ext:JsonReader Root="plants" TotalProperty="total" />
                                        </Reader>
                                    </ext:AjaxProxy>
                                </Proxy>
                                <Model>
                                    <ext:Model ID="Model2" IDProperty="Name" Name="ItemLineModel" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Name" Type="String" />
                                            <ext:ModelField Name="EmployeeId" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                            <ext:ComboBox LabelSeparator="" ID="cmbSearch" FieldLabel="Search Employee" LabelWidth="70"
                                LabelAlign="Top" runat="server" DisplayField="Name" ValueField="EmployeeId" StoreID="storeSearch"
                                TypeAhead="false" Width="150" PageSize="9999" HideBaseTrigger="true" MinChars="1"
                                TriggerAction="All" ForceSelection="true">
                                <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                                    <ItemTpl ID="ItemTpl1" runat="server">
                                        <Html>
                                            <tpl>
                                            <div class="search-item">
                                                            <span>{Name}</span>  
                                             </div>
					                </tpl>
                                        </Html>
                                    </ItemTpl>
                                </ListConfig>
                                <Triggers>
                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                </Triggers>
                                <Listeners>
                                    <Select Handler="this.getTrigger(0).show();" />
                                    <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                    <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                                </Listeners>
                            </ext:ComboBox>
                        </td>
                        <td>
                            <ext:Store ID="StoreLevel" runat="server">
                                <Model>
                                    <ext:Model ID="Model3" runat="server" IDProperty="LevelId">
                                        <Fields>
                                            <ext:ModelField Name="LevelId" Type="String" />
                                            <ext:ModelField Name="GroupLevel" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                            <ext:Store ID="StoreBranch" runat="server">
                                <Model>
                                    <ext:Model ID="Model5" runat="server" IDProperty="BranchId">
                                        <Fields>
                                            <ext:ModelField Name="BranchId" Type="String" />
                                            <ext:ModelField Name="Name" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                             <ext:Store ID="StoreGroup" runat="server">
                                <Model>
                                    <ext:Model ID="Model9" runat="server" IDProperty="RolloutGroupID">
                                        <Fields>
                                            <ext:ModelField Name="RolloutGroupID" Type="String" />
                                            <ext:ModelField Name="Name" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                            <ext:ComboBox FieldLabel="Level" ID="cmbLevelList" Width="150" runat="server" ValueField="LevelId"
                                DisplayField="GroupLevel" StoreID="StoreLevel" LabelAlign="Top" LabelSeparator=""
                                ForceSelection="true" QueryMode="Local">
                                <Triggers>
                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                </Triggers>
                                <Listeners>
                                    <Select Handler="this.getTrigger(0).show();" />
                                    <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                    <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                                </Listeners>
                            </ext:ComboBox>
                        </td>
                        <td>
                            <ext:ComboBox FieldLabel="Branch" ID="cmbBranch" Width="150" runat="server" ValueField="BranchId"
                                DisplayField="Name" StoreID="StoreBranch" LabelAlign="Top" LabelSeparator=""
                                ForceSelection="true" QueryMode="Local">
                                <Triggers>
                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                </Triggers>
                                <Listeners>
                                    <Select Handler="this.getTrigger(0).show();" />
                                    <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                    <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                                </Listeners>
                            </ext:ComboBox>
                        </td>
                        <td>
                            <ext:ComboBox FieldLabel="Appraisal Group" ID="cmbAppraisalGroup" Width="150" runat="server" ValueField="RolloutGroupID"
                                DisplayField="Name" StoreID="StoreGroup" LabelAlign="Top" LabelSeparator=""
                                ForceSelection="true" QueryMode="Local">
                                <Triggers>
                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                </Triggers>
                                <Listeners>
                                    <Select Handler="this.getTrigger(0).show();" />
                                    <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                    <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                                </Listeners>
                            </ext:ComboBox>
                        </td>
                        <td>
                            <ext:ComboBox ForceSelection="true" QueryMode="Local" ID="cmbForm" FieldLabel="Select Form"
                                runat="server" LabelAlign="Top" DisplayField="Name" Width="150" LabelSeparator=""
                                ValueField="AppraisalFormID">
                                <Store>
                                    <ext:Store ID="storeForm" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="AppraisalFormID" Type="String" />
                                            <ext:ModelField Name="Name" />
                                        </Fields>
                                    </ext:Store>
                                </Store>
                                <Triggers>
                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                </Triggers>
                                <Listeners>
                                    <Select Handler="this.getTrigger(0).show();" />
                                    <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                    <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                                </Listeners>
                            </ext:ComboBox>
                        </td>
                        <td>
                            <ext:MultiCombo ID="cmbServiceStatus" runat="server" Width="150" SelectOnFocus="true" Selectable="true"
                LabelAlign="Top" FieldLabel="Service Status" ValueField="Key" DisplayField="Value" ForceSelection="true"
                AllowBlank="false" LabelSeparator="">
                <Store>
                    <ext:Store ID="Store4" runat="server">
                        <Model>
                            <ext:Model ID="Model7" runat="server">
                                <Fields>
                                    <ext:ModelField Name="Value" Type="String" />
                                    <ext:ModelField Name="Key" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
            </ext:MultiCombo>
                        </td>

                        <td>
                            <ext:MultiCombo Width="150" LabelSeparator="" SelectionMode="All" LabelWidth="50" LabelAlign="Top"
                                ID="cmbPeriod" DisplayField="Start" ValueField="PeriodId"
                                runat="server" FieldLabel="Period">
                                <Store>
                                    <ext:Store ID="Store5" runat="server">
                                        <Model>
                                            <ext:Model ID="Model8" IDProperty="PeriodId" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="PeriodId" />
                                                    <ext:ModelField Name="Start" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                            </ext:MultiCombo>
                        </td>
				
                         </tr>
                </table>
                <table>
                    <tr>
                        
                        <td style="padding-top: 20px">
                            <ext:Button runat="server" Text="Delete Forms" Cls="btn btn-primary" ID="btnDeleteForms">
                                <DirectEvents>
                                    <Click OnEvent="btnDeleteForms_Click">
                                        <EventMask ShowMask="true" />
                                        <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the Assinged Forms?" />
                                        <ExtraParams>
                                            <ext:Parameter Name="gridItems" Value="Ext.encode(#{GridLevels}.getRowsValues({selectedOnly:true}))"
                                                Mode="Raw" />
                                        </ExtraParams>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </td>
                        <td style="padding-top: 20px;padding-left:15px;">
                            <ext:Button runat="server" AutoPostBack="true" OnClick="btnExport_Click"
                                ID="btnExport" Cls="btn btn-primary" Text="<i></i>Export" runat="server">
                                <Listeners>
                                    <Click Handler="processExport();" />
                                </Listeners>
                            </ext:Button>
                        </td>
                        <td style="padding-top: 20px;padding-left:15px;">

                            <ext:Button runat="server" ID="btnLoad" Cls="btn btn-primary" Text="<i></i>Load"
                                runat="server">
                                <Listeners>
                                    <Click Handler="searchList();" />
                                </Listeners>
                            </ext:Button>
                        </td>
                    </tr>
                </table>
                <ext:GridPanel StyleSpec="margin-top:15px;" ID="GridLevels" runat="server" Cls="itemgrid">
                    <Store>
                        <ext:Store ID="Store3" runat="server" AutoLoad="true" PageSize="50">
                            <Proxy>
                                <ext:AjaxProxy Json="true" Url="../Handler/RolloutEmployeesListHandler.ashx">
                                    <ActionMethods Read="GET" />
                                    <Reader>
                                        <ext:JsonReader Root="data" TotalProperty="total" />
                                    </Reader>
                                </ext:AjaxProxy>
                            </Proxy>
                            <AutoLoadParams>
                                <ext:Parameter Name="start" Value="0" Mode="Raw" />
                            </AutoLoadParams>
                            <Parameters>
                                <ext:StoreParameter Name="EmployeeId" Value="#{cmbSearch}.getValue()" Mode="Raw"
                                    ApplyMode="Always" />
                                <ext:StoreParameter Name="StatusId" Value="#{cmbStatus}.getValue()" Mode="Raw" ApplyMode="Always" />
                                <ext:StoreParameter Name="LevelId" Value="#{cmbLevelList}.getValue()" Mode="Raw"
                                    ApplyMode="Always" />
                                <ext:StoreParameter Name="BranchId" Value="#{cmbBranch}.getValue()" Mode="Raw" ApplyMode="Always" />
                                <ext:StoreParameter Name="FormId" Value="#{cmbForm}.getValue()" Mode="Raw" ApplyMode="Always" />
                                <ext:StoreParameter Name="statusFilter" Value="getStatusFilter()" Mode="Raw" ApplyMode="Always" />
                                <ext:StoreParameter Name="PeriodId" Value="#{cmbPeriod}.getValue()" Mode="Raw"
                                    ApplyMode="Always" />
                                 <ext:StoreParameter Name="AppraisalGroupID" Value="#{cmbAppraisalGroup}.getValue()" Mode="Raw"
                                    ApplyMode="Always" />
                            </Parameters>
                            <Model>
                                <ext:Model ID="Model4" IDProperty="AppraisalEmployeeFormID" runat="server">
                                    <Fields>
                                        <ext:ModelField Name="TotalRows" Type="String" />
                                        <ext:ModelField Name="RowNumber" Type="String" />
                                        <ext:ModelField Name="AppraisalEmployeeFormID" Type="String" />
                                        <ext:ModelField Name="AppraisalFormRef_ID" Type="Date" />
                                        <ext:ModelField Name="FormName" Type="String" />
                                        <ext:ModelField Name="EmployeeId" Type="String" />
                                        <ext:ModelField Name="EmployeeName" Type="String" />
                                        <ext:ModelField Name="Status" Type="String" />
                                        <ext:ModelField Name="StatusText" Type="String" />
                                        <ext:ModelField Name="RolloutID" Type="String" />
                                        <ext:ModelField Name="RolloutDate" Type="Date" />
                                        <ext:ModelField Name="SelfSubmittedOn" Type="Date" />
                                        <ext:ModelField Name="AppraisalFormRef_ID" Type="String" />
                                        <ext:ModelField Name="DueDate" Type="Date" />
                                        <ext:ModelField Name="DaysRemaining" Type="string" />
                                        <ext:ModelField Name="Branch" Type="string" />
                                        <ext:ModelField Name="Level" Type="string" />
                                        <ext:ModelField Name="SelfEmployeeRatingScore" Type="string" />
                                        <ext:ModelField Name="SupervisorRatingScore" Type="string" />
                                        <ext:ModelField Name="EmployeeGradeSummary" Type="string" />
                                        <ext:ModelField Name="SupervisorGradeSummary" Type="string" />
                                        <ext:ModelField Name="WaitingFrom" Type="String" />
                                        <ext:ModelField Name="ServiceStatus" Type="String" />
                                        <ext:ModelField Name="AppraisalGroup" Type="String" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                    </Store>
                    
 <View>
                    <ext:GridView ID="GridView1" EnableTextSelection="true" runat="server">
                        <Listeners>
                        </Listeners>
                        <GetRowClass Fn="getRowClass" />
                    </ext:GridView>
                </View>
                    <ColumnModel>
                        <Columns>
                            <ext:Column ID="Column4" Locked="true" Sortable="false" MenuDisabled="true" runat="server"
                                Text="EIN" Width="40" Align="Left" DataIndex="EmployeeId" />
                            <ext:Column ID="Column5" Locked="true" Sortable="false" MenuDisabled="true" runat="server"
                                Text="Employee Name" Width="180" Align="Left" DataIndex="EmployeeName" />
                            <ext:Column ID="Column7" Sortable="false" MenuDisabled="true" runat="server" Text="Branch"
                                Width="110" Align="Left" DataIndex="Branch" />
                            <ext:Column ID="Column8" Sortable="false" MenuDisabled="true" runat="server" Text="Level"
                                Width="110" Align="Left" DataIndex="Level" />
                        <ext:Column ID="Column14" Sortable="false" MenuDisabled="true" runat="server" Text="Appraisal Group"
                                Width="110" Align="Left" DataIndex="AppraisalGroup" />
                            <ext:Column ID="Column13" Sortable="false" MenuDisabled="true" runat="server" Text="Service Status"
                                Width="110" Align="Left" DataIndex="ServiceStatus" />
                            <ext:DateColumn ID="colRolloutDate" runat="server" Align="Left" Text="Date Submitted"
                                MenuDisabled="true" Sortable="false" Format="yyyy/MM/dd" DataIndex="SelfSubmittedOn"
                                Resizable="false" Draggable="false" Width="103">
                            </ext:DateColumn>
                            <ext:Column ID="Column1" Sortable="false" MenuDisabled="true" runat="server" Text="Status"
                                Width="80" Align="Left" DataIndex="StatusText" />
                            <ext:Column ID="Column10" Sortable="false" MenuDisabled="true" runat="server" Text="Waiting for"
                                Width="200" Align="Left" DataIndex="WaitingFrom" />
                            <ext:Column ID="Column6" Sortable="false" MenuDisabled="true" runat="server" Text="Form Name"
                                Width="150" Align="Left" DataIndex="FormName" />
                            <ext:Column ID="Column11" Sortable="false" MenuDisabled="true" runat="server" Text="Self Score"
                                Width="75" Align="Center" DataIndex="SelfEmployeeRatingScore">
                                <Renderer Fn="getFormattedAmount" />
                            </ext:Column>
                            <ext:Column ID="Column12" Sortable="false" MenuDisabled="true" runat="server" Text="Sup. Score"
                                Width="75" Align="Center" DataIndex="SupervisorRatingScore">
                                <Renderer Fn="getFormattedAmount" />
                            </ext:Column>
                            <ext:Column ID="Column3" Sortable="false" MenuDisabled="true" runat="server" Text="Self Grade"
                                Width="75" Align="Center" DataIndex="EmployeeGradeSummary">
                            </ext:Column>
                            <ext:Column ID="Column9" Sortable="false" MenuDisabled="true" runat="server" Text="Sup. Grade"
                                Width="75" Align="Center" DataIndex="SupervisorGradeSummary">
                            </ext:Column>
                            <ext:Column ID="Column2" Sortable="false" MenuDisabled="true" runat="server" Text=""
                                Width="50" Align="Left">
                                <Renderer Fn="renderView" />
                            </ext:Column>
                            <ext:CommandColumn ID="CommandColumn1" runat="server" Text="Revert" Align="Center"
                                Width="70">
                                <Commands>
                                    <ext:CommandSeparator />
                                    <ext:GridCommand Text="Revert<i></i>" CommandName="Edit" />
                                </Commands>
                                <Listeners>
                                    <Command Handler="CommandHandler1(command,record);" />
                                </Listeners>
                                <PrepareToolbar Fn="prepareToolbar" />
                            </ext:CommandColumn>
                        </Columns>
                    </ColumnModel>
                     <SelectionModel>
                                <ext:CheckboxSelectionModel ID="CheckboxSelectionModel1" runat="server" Mode="Simple" />
                            </SelectionModel>
                    <BottomBar>
                        <ext:PagingToolbar ID="PagingToolbar1" runat="server" PageSize="50" DisplayInfo="true"
                            DisplayMsg="Displaying employees {0} - {1} of {2}" EmptyMsg="No Records to display">
                            <Items>
                                <ext:ComboBox Hidden="true" runat="server" SelectedIndex="0" Width="80" ID="cmbPageSize">
                                    <Items>
                                        <ext:ListItem Text="20" Value="20" />
                                        <ext:ListItem Text="30" Value="30" />
                                        <ext:ListItem Text="50" Value="50" />
                                    </Items>
                                    <Listeners>
                                        <Select Handler="#{PagingToolbar1}.pageSize = parseInt( '20'); searchList();" />
                                    </Listeners>
                                </ext:ComboBox>
                            </Items>
                        </ext:PagingToolbar>
                    </BottomBar>
                </ext:GridPanel>
            </div>
        </div>
        <br />
    </div>
    <ext:Window ID="windowRevert" runat="server" Title="Appraisal Revert for : " Icon="Application"
        Height="320" Width="500" BodyPadding="5" Hidden="true" Modal="true">
        <Content>
            <table class="fieldTable">
                <tr>
                    <td>
                        <ext:DisplayField ID="disp" runat="server" FieldLabel="Current Status" />
                    </td>
                </tr>
                <tr>
                    <td runat="server" id="employeeTD">
                        <ext:ComboBox ID="cmbRevertStatus" Width="200" runat="server" ValueField="StepID" DisplayField="StepName"
                            FieldLabel="Revert to" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                            QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store2" runat="server">
                                    <Model>
                                        <ext:Model ID="Model6" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="StepID" Type="String" />
                                                <ext:ModelField Name="StepName" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator Display="None" ID="valcmbContition" runat="server" ValidationGroup="SaveUpdate"
                            ControlToValidate="cmbRevertStatus" ErrorMessage="Revert to is required." />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <ext:TextArea ID="txtNotes" runat="server" FieldLabel="Notes" LabelSeparator="" LabelAlign="Top"
                            Rows="3" Cols="55" />
                        <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator1" runat="server" ValidationGroup="SaveUpdate"
                            ControlToValidate="txtNotes" ErrorMessage="Notes is required." />
                    </td>
                </tr>
                <tr>
                    <td valign="bottom" colspan="2">
                        <div class="popupButtonDiv">
                            <ext:Button runat="server" Width="100" Height="25" ID="btnConfirmRevert" Text="<i></i>Revert">
                                <DirectEvents>
                                    <Click OnEvent="btnConfirmRevert_Click">
                                        <Confirmation ConfirmRequest="true" Message="Are you sure, you want to revert the appraisal?" />
                                        <EventMask ShowMask="true" />
                                    </Click>
                                </DirectEvents>
                                <Listeners>
                                    <Click Handler="valGroup = 'SaveUpdate'; return CheckValidation();">
                                    </Click>
                                </Listeners>
                            </ext:Button>
                            <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="LinkButton2" Cls="btnFlatLeftGap"
                                Text="<i></i>Cancel">
                                <Listeners>
                                    <Click Handler="#{windowRevert}.hide();">
                                    </Click>
                                </Listeners>
                            </ext:LinkButton>
                        </div>
                    </td>
                </tr>
            </table>
        </Content>
    </ext:Window>
</asp:Content>
