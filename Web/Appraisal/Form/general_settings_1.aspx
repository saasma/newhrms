﻿<%@ Page Title="General Settings" Language="C#" MasterPageFile="~/Master/NewDetails.master"
    ValidateRequest="false" AutoEventWireup="true" CodeBehind="general_settings_1.aspx.cs"
    Inherits="Web.Appraisal.Form.general_settings_1" %>

<%@ Register Src="~/Controls/HRAppraisalFormWizard.ascx" TagPrefix="ucHRW" TagName="ucHRFormWizard" %>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script>
        Ext.onReady(function () {
            CKEDITOR.config.toolbar = [
               ['Styles', 'Format', 'Font', 'FontSize'],

               ['Bold', 'Italic', 'Underline', 'StrikeThrough', '-', 'Undo', 'Redo', '-', 'Cut', 'Copy', 'Paste', 'Find', 'Replace', '-', 'Outdent', 'Indent', '-'],
    
               ['NumberedList', 'BulletedList', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
               ['Image', 'Table', '-', 'Link', 'TextColor', 'BGColor', 'Maximize']
            ];
        
  

        });
            function SetEditorValue()
        {
        <%=hdnEditorDescription.ClientID%>.setValue(CKEDITOR.instances.ctl00_ContentPlaceHolder_Main_txtEditorDescription.getData());
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Review Form Name
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel" style="margin-top: 0px !important; padding-top: 0px !important;">
        <ext:Hidden runat="server" ID="hdnEditorDescription" />
        <div class="innerLR">
            <div class="separator bottom">
            </div>
            <ucHRW:ucHRFormWizard Id="ucAppraisalWizard" runat="server" />
            <ext:Label ID="lblMsg" runat="server" />
            <div style="font-weight: bold; padding-top: 10px">
                Choose a Name for your Review Form</div>
            <div style="font-style: italic">
                The name you choose shows up as the title on the Review Form followed by employee
                name</div>
            <table class="fieldTable firsttdskip">
                <tr>
                    <td>
                        <ext:TextField ID="txtFormName" FieldLabel="Review Form Name *" runat="server" LabelAlign="Top"
                            Width="400" LabelSeparator="">
                        </ext:TextField>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Review Form Name is required"
                            Display="None" ControlToValidate="txtFormName" ValidationGroup="SaveFrom" runat="server">
                        </asp:RequiredFieldValidator>
                    </td>
                    <td style="padding-left: 229px">
                        <ext:Button ID="btnCopy" Cls="btn btn-primary" Hidden="true" runat="server" Text="Copy from Another Form"
                            ValidationGroup="InsertUpdate">
                            <Listeners>
                                <Click Handler="#{WindowCopy}.show();" />
                            </Listeners>
                        </ext:Button>
                    </td>
                </tr>
               
                <tr>
                    <td colspan="2">
                        <div class="components">
                            <div class="grids">
                                <p>
                                    Description</p>
                                <CKEditor:CKEditorControl ExtraPlugins="autogrow" ID="txtEditorDescription" Width="800px"
                                    Height="200px" runat="server" BasePath="~/ckeditor/">
                                </CKEditor:CKEditorControl>
                            </div>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Description is required"
                            Display="None" ControlToValidate="txtEditorDescription" ValidationGroup="SaveFrom"
                            runat="server">
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
            </table>
            <div class="buttonBlock" style="padding-top: 50px">
                <table>
                    <tr>
                        <td>
                            <ext:Button ID="btnSave" Cls="btn btn-primary" runat="server" Text="&nbsp;&nbsp;Save&nbsp;&nbsp;"
                                ValidationGroup="InsertUpdate">
                                <DirectEvents>
                                    <Click OnEvent="ButtonNext_Click">
                                        <EventMask ShowMask="true" />
                                    </Click>
                                </DirectEvents>
                                <Listeners>
                                    <Click Handler="SetEditorValue();valGroup = 'SaveFrom'; return CheckValidation();">
                                    </Click>
                                </Listeners>
                            </ext:Button>
                        </td>
                        <td>
                            &nbsp;&nbsp;&nbsp;
                        </td>
                        <td>
                            <ext:Button ID="btnNext" Cls="btn btn-primary" runat="server" Text="&nbsp;&nbsp;Save and Next&nbsp;&nbsp;"
                                ValidationGroup="InsertUpdate">
                                <DirectEvents>
                                    <Click OnEvent="ButtonNext_Click">
                                        <EventMask ShowMask="true" />
                                    </Click>
                                </DirectEvents>
                                <Listeners>
                                    <Click Handler="SetEditorValue();valGroup = 'SaveFrom'; return CheckValidation();">
                                    </Click>
                                </Listeners>
                            </ext:Button>
                        </td>
                        <td>
                            &nbsp;&nbsp;&nbsp;
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <ext:Window ID="WindowCopy" runat="server" Title="Copy Section" Icon="Application"
            Width="640" Height="520" BodyPadding="5" Hidden="true" Modal="true">
            <Content>
                <table class="fieldTable">
                    <tr>
                        <td>
                            Copy these sections
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ext:ComboBox ForceSelection="true" QueryMode="Local" ID="cmbForm" FieldLabel="Select Form"
                                runat="server" LabelAlign="Top" DisplayField="Name" Width="300" LabelSeparator=""
                                ValueField="AppraisalFormID">
                                <Store>
                                    <ext:Store ID="storeForm" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="AppraisalFormID" Type="String" />
                                            <ext:ModelField Name="Name" />
                                        </Fields>
                                    </ext:Store>
                                </Store>
                            </ext:ComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ext:Checkbox runat="server" ID="chkIntroduction" BoxLabel="Introduction" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ext:Checkbox runat="server" ID="chkObjective" BoxLabel="Objectives" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ext:Checkbox runat="server" ID="chkPerformanceSummary" BoxLabel="PerformanceSummary" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ext:Checkbox runat="server" ID="chkActivity" BoxLabel="Activity Summary" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ext:Checkbox runat="server" ID="chkCompetency" BoxLabel="Competencies" />
                        </td>
                    </tr>
                     <tr>
                        <td>
                            <ext:Checkbox runat="server" ID="chkTarget" BoxLabel="Targets" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ext:Checkbox runat="server" ID="chkQuestions" BoxLabel="Questionnaire" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ext:Checkbox runat="server" ID="chkSupervisorReview" BoxLabel="Supervisor Review" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ext:Checkbox runat="server" ID="chkComments" BoxLabel="Comments" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ext:Checkbox runat="server" ID="chkSignature" BoxLabel="Signature" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom" colspan="2">
                            <div class="popupButtonDiv">
                                <ext:Button runat="server" ID="btnSaveCopy" Cls="btn btn-primary" Text="<i></i>Copy">
                                    <DirectEvents>
                                        <Click OnEvent="btnSaveCopy_Click">
                                            <Confirmation ConfirmRequest="true" Message="Are you sure, you want to copy to the current form,all values for the selected section will be replaced?" />
                                            <EventMask ShowMask="true" />
                                        </Click>
                                    </DirectEvents>
                                </ext:Button>
                                <div class="btnFlatOr">
                                    or</div>
                                <ext:LinkButton runat="server" StyleSpec="padding:0px;" Cls="btnFlatLeftGap" ID="LinkButton1"
                                    Text="<i></i>Cancel" runat="server">
                                    <Listeners>
                                        <Click Handler="#{WindowCopy}.hide();">
                                        </Click>
                                    </Listeners>
                                </ext:LinkButton>
                            </div>
                        </td>
                    </tr>
                </table>
            </Content>
        </ext:Window>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
