﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using System.Xml;
using Utils.Helper;

namespace Web.Appraisal
{
    public partial class Questionnaire : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            hiddenValue.Text = Request.QueryString["fid"];

            if (!X.IsAjaxRequest)
            {
                Initialise();
              
            }

            JavascriptHelper.AttachNonDialogPopUpCode(Page, "employeePopup", "../../ExcelWindow/QuestionImport.aspx", 450, 500);
        }

        public void Initialise()
        {
            cmbRatingScale.Store[0].DataSource = NewHRManager.GetAllRatingScale();
            cmbRatingScale.Store[0].DataBind();

            LoadLevels();
            AppraisalForm formInst = new AppraisalForm();
            if (!string.IsNullOrEmpty(Request.QueryString["fid"]))
            {
                formInst = AppraisalManager.getFormInstanceByID(int.Parse(hiddenValue.Text));
                if (formInst != null)
                {
                    txtSectionName.Text = formInst.QuestionnareName;
                    txtEditorDescription.Text = formInst.QuestionnareDescription;
                    chkAllowSelfRating.Checked = formInst.AllowQuestionSelfRating.Value;
                    if (formInst.AllowQuestionSelfComment != null)
                        chkAllowSelfComment.Checked = formInst.AllowQuestionSelfComment.Value;

                    if (formInst.QuestionnareRatingScaleRef_ID != null)
                        ExtControlHelper.ComboBoxSetSelected(formInst.QuestionnareRatingScaleRef_ID.ToString(), cmbRatingScale);

                    if (formInst.AllowQuestionSupRating != null)
                        chkSupRating.Checked = formInst.AllowQuestionSupRating.Value;
                    if (formInst.AllowQuestionSupComment != null)
                        chkSupComment.Checked = formInst.AllowQuestionSupComment.Value;


                    if (formInst.QuestionaireMarkingType != null)
                    {
                        ExtControlHelper.ComboBoxSetSelected(formInst.QuestionaireMarkingType.ToString(), cmbMarkingType);

                       

                    }

                }
            }
        }

        public void btnLoadQuestion_Click(object sender, DirectEventArgs e)
        {
            LoadLevels();
        }
        private void LoadLevels()
        {
            if (!string.IsNullOrEmpty(hiddenValue.Text))
            {

                storeQuestCategory.DataSource = AppraisalManager.getallCategoriesByFormID(int.Parse(hiddenValue.Text));
                storeQuestCategory.DataBind();
            }

            //GridLevels.GetStore().DataSource = AppraisalManager.getallGoalsCategory();
            //GridLevels.GetStore().DataBind();
            if (!string.IsNullOrEmpty(Request.QueryString["fid"]))
            {

                //if (NewHRManager.IsAppraisalQuestionnariesUsedInEmployee(int.Parse(Request.QueryString["fid"])))
                //{
                //    btnAddCategory.Disabled = true;
                //    btnAddLevel.Disabled = true;
                //    SetWarning(lblMsg, "You can't add and delete Question as this data has been used in employee from.");
                //}

                int FormID = int.Parse(hiddenValue.Text);
                Store3.DataSource = AppraisalManager.GetAllQuestionByFormID(FormID);
                Store3.DataBind();
            }

        }

        public void ClearLevelFields()
        {
            
            txtQuestion.Text = "";
            //txtWeight.Text = "0";
            cmbQuestCategory.Clear();

            txtDescription.Text = "";
        }

      
        protected void btnAddLevel_Click(object sender, DirectEventArgs e)
        {
            hiddenValueRow.Text = "";
            ClearLevelFields();
            WindowLevel.Center();
            WindowLevel.Show();
        }

        protected void btnAddCategory_Click(object sender, DirectEventArgs e)
        {
            hiddenValueRow.Text = "";
            txtCategoryName.Text = "";
            WindowCategory.Center();
            WindowCategory.Show();
        }


        protected void btnDeleteLevel_Click(object sender, DirectEventArgs e)
        {
            int RowID = int.Parse(hiddenValueRow.Text.Trim());
            int AppraisalFormID = int.Parse(Request.QueryString["fid"]);
            Boolean inUse;
            bool result = AppraisalManager.DeleteQuestionByID(RowID, AppraisalFormID, out inUse);

            if (!inUse)
            {
                if (result)
                {
                    LoadLevels();
                    NewMessage.ShowNormalMessage("Questionnaire deleted.");

                }
                else
                    NewMessage.ShowNormalMessage("Questionnaire is in Use");

            }
            else
            {
                NewMessage.ShowNormalMessage("Questionnaire is in Use");

            }

        }

        protected void btnDeleteCategory_Click(object sender, DirectEventArgs e)
        {
            int RowID = int.Parse(hiddenValueRow.Text.Trim());
           
            Status status = AppraisalManager.DeleteCategory(RowID);

            if (status.IsSuccess)
            {
                LoadLevels();
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }

        }
        protected void btnGroupEdit_Click(object sender, DirectEventArgs e)
        {
            //X.Msg.Alert("", hiddenValueRow.Text).Show();
            txtCategoryName.Text = "";
            int categoryID = int.Parse(hiddenValueRow.Text);

            AppraisalFormQuestionnaireCategory cateInst = new AppraisalFormQuestionnaireCategory();
            cateInst = AppraisalManager.getallCategoriesByFormID(int.Parse(hiddenValue.Text)).Where(x => x.CategoryID == categoryID).FirstOrDefault();
            if (cateInst != null)
            {
                txtCategoryName.Text = cateInst.Name;
                if(cateInst.Sequence!=null)
                txtCategorySequence.Text = cateInst.Sequence.ToString();
            }
            WindowCategory.Center();
            WindowCategory.Show();

        }

        protected void btnEditLevel_Click(object sender, DirectEventArgs e)
        {
            ClearLevelFields();
            int RowID = int.Parse(hiddenValueRow.Text.Trim());
            AppraisalFormQuestionnaire entity = AppraisalManager.GetQuestionByFormID(RowID);
            WindowLevel.Center();
            WindowLevel.Show();
            //txtWeight.Text = entity.Weight.ToString();
            txtQuestion.Text = entity.Question;
            txtDescription.Text = entity.Description;
            if(entity.CategoryID!=null)
                cmbQuestCategory.SetValue(entity.CategoryID.ToString());

          

        }
        protected void lnkBtnDivideWeight_Click(object sender, DirectEventArgs e)
        {
            int FormID = int.Parse(hiddenValue.Text);
            List<AppraisalFormQuestionnaire> _ListAppraisalFormQuestionnaire =
                AppraisalManager.GetAllQuestionByFormID(FormID).Where(x => x.IsCategory == false).ToList();


            //string SumOfAllWeight = _ListAppraisalFormQuestionnaire.Sum(x => x.Weight).ToString();
            decimal sum = 100;

            decimal NowEquallyDividedWeigth = sum / _ListAppraisalFormQuestionnaire.Count();
            NowEquallyDividedWeigth = Convert.ToDecimal(NowEquallyDividedWeigth.ToString("N2"));

            AppraisalFormQuestionnaire _AppraisalFormQuestionnaire = new AppraisalFormQuestionnaire();
            _AppraisalFormQuestionnaire.AppraisalFormRef_ID = FormID;
            _AppraisalFormQuestionnaire.Weight = (double)NowEquallyDividedWeigth;
            Status status = AppraisalManager.DivieQuestionnariesWeightEqually(_AppraisalFormQuestionnaire);
            if (status.IsSuccess)
            {
                WindowLevel.Hide();
                LoadLevels();
                NewMessage.ShowNormalMessage("weight divided equally.");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }

        }



        protected void btnCategorySaveUpdate_Click(object sender, DirectEventArgs e)
        {


            Page.Validate("SaveUpdateCategory");
            if (Page.IsValid)
            {

                AppraisalFormQuestionnaireCategory inst = new AppraisalFormQuestionnaireCategory();
                
                inst.Name = txtCategoryName.Text;
                inst.AppraisalFormRef_ID = int.Parse(hiddenValue.Text);
                inst.Sequence = int.Parse(txtCategorySequence.Text);

                bool isEdit = false;
                if (!string.IsNullOrEmpty(hiddenValueRow.Text))
                {
                    isEdit = true;
                    inst.CategoryID = int.Parse(hiddenValueRow.Text);
                   
                }

                
                Status status = AppraisalManager.InsertUpdateFormCategory(inst, isEdit);
                if (status.IsSuccess)
                {
                    WindowCategory.Hide();
                    LoadLevels();
                    NewMessage.ShowNormalMessage("Category saved.");
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }

            }


        }



        protected void btnLevelSaveUpdate_Click(object sender, DirectEventArgs e)
        {


            Page.Validate("InsertUpdateLevel");
            if (Page.IsValid)
            {

                AppraisalFormQuestionnaire inst = new AppraisalFormQuestionnaire();
                inst.Question = txtQuestion.Text.Trim();
                inst.Description = txtDescription.Text.Trim();
                //inst.Weight = double.Parse(txtWeight.Text);
                inst.AppraisalFormRef_ID = int.Parse(hiddenValue.Text);
                int CategoryID = int.Parse(cmbQuestCategory.SelectedItem.Value);
                inst.AppraisalFormQuestionnaireCategory = AppraisalManager.getallCategoriesByFormID(int.Parse(hiddenValue.Text)).Where(x => x.CategoryID == CategoryID).FirstOrDefault();


                bool isEdit = false;
                if (!string.IsNullOrEmpty(hiddenValueRow.Text))
                {
                    isEdit = true;
                    inst.QuestionnareID = int.Parse(hiddenValueRow.Text);
                }
                inst.Sequence = 1;

                AppraisalFormQuestionnaire question =
                    AppraisalManager.GetAllQuestionByFormID(int.Parse(hiddenValue.Text)).OrderBy(x => x.Sequence).LastOrDefault();
                if (question != null)
                    inst.Sequence = question.Sequence + 1;

                Status status = AppraisalManager.InsertUpdateFormQuetion(inst, isEdit);
                if (status.IsSuccess)
                {
                    WindowLevel.Hide();
                    LoadLevels();
                    SetMessage(lblMsg, "Appraisal updated.");
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }

            }


        }

        protected void SaveQuestionnaireForm_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveFormData");

            string entryLineJson = e.ExtraParams["QuestionList"];
            List<AppraisalFormQuestionnaire> QuestionList = JSON.Deserialize<List<AppraisalFormQuestionnaire>>(entryLineJson);

            if (Page.IsValid)
            {
                AppraisalForm inst = new AppraisalForm();
                inst = AppraisalManager.getFormInstanceByID(int.Parse(hiddenValue.Text));
                inst.QuestionnareName = txtSectionName.Text.Trim();
                inst.QuestionnareDescription = hdnEditorDescription.Text.Trim();
                inst.AllowQuestionSelfRating = chkAllowSelfRating.Checked;
                inst.AllowQuestionSelfComment = chkAllowSelfComment.Checked;

                inst.AllowQuestionSupRating = chkSupRating.Checked;
                inst.AllowQuestionSupComment = chkSupComment.Checked;

                inst.QuestionaireMarkingType = int.Parse(cmbMarkingType.SelectedItem.Value);

                inst.QuestionnareRatingScaleRef_ID = int.Parse(cmbRatingScale.SelectedItem.Value);

                Status status = AppraisalManager.UpdateQuestionnaireFormData(inst,QuestionList);
                if (status.IsSuccess)
                {
                    WindowLevel.Hide();
                    LoadLevels();
                    NewMessage.ShowNormalMessage("Question saved.");
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }
            }

        }

        protected void btnChangeOrder_Click(object sender, DirectEventArgs e)
        {
            int sourceId = int.Parse(hdnSource.Text.Trim());
            int CategoryID = int.Parse(hdnCategoryID.Text.Trim());
            int destId = int.Parse(hdnDest.Text.Trim());
            string mode = hdnDropMode.Text.Trim();
            NewHRManager.ChangeQuestionnareOrder(sourceId, destId, mode, CategoryID);
            LoadLevels();
        }

    }
}