﻿<%@ Page Title="Rollout" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="RolloutMega.aspx.cs" Inherits="Web.Appraisal.RolloutMega" %>

<%@ Register Src="~/NewHR/UserControls/EducationCtl.ascx" TagName="EducationCtl"
    TagPrefix="ucEducation" %>
<%--<%@ Register Src="~/NewHR/UserControls/EmployeeDetailsCtl.ascx" TagName="EmployeeDetailsCtl"
    TagPrefix="uc1" %>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">


        var beforeEdit = function (e1, e, e2, e3) {


            if ((e.field == "EmployeeId") && e.record.data.AuthorityType == "1") {
                e.cancel = true;
                return;
            }


        }

         var EmpRenderer = function (value, metaData, record, rowIndex, colIndex, store) {
                    
                    var r = <%= storeEmployee.ClientID %>.getById(value.toString().toLowerCase());
                    if (Ext.isEmpty(r)) {
                        return "";
                    }
                    return r.data.NameEIN;
                };
        
          var RecommendRenderer = function (value, metaData, record, rowIndex, colIndex, store) {
                    
                   if(value==true)
                    return "Yes";
                  
                   return "";
                };
        var RemoveItemLine = function (column, command, record, recordIndex, cellIndex) {
            var store = this.grid.store;
            store.remove(record);
        };

         var prepareToolbar = function (grid, toolbar, rowIndex, record) {
     
                if(record.data.IsDeletable==false)
                {

                    toolbar.hide();
                }
            
            
        };

        var additionalRenderer = function (value, metaData, record, rowIndex, colIndex, store) {
                    var r = <%= storeAdditionalStep.ClientID %>.getById(value.toString().toLowerCase());
                    if (Ext.isEmpty(r)) {
                        return "";
                    }
                    return r.data.Text;
                };
    </script>
    <style type="text/css">
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
        #content
        {
            margin: inherit;
        }
        .x-form-item-label
        {
            color: #414141 !important;
            font-style: normal !important;
        }
        .fieldTable > tbody > tr > td
        {
            padding-bottom: 10px;
        }
        .items
        {
            padding-left: 20px;
        }
        .items span
        {
            display: block;
            padding-bottom: 2px;
            font-weight: normal;
            font-size: 12px;
        }
        .items img
        {
            float: left;
            width: 18px;
            height: 18px;
            margin-right: 10px;
        }
        .innerLR
        {
            padding-left: 0px;
        }
        .buttonBlock
        {
            display: none !important;
        }
         .x-form-display-field
        {
            color:#428BCA!important;
            font-weight:bold;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4 runat="server" id="titleText">
                    Appraisal Form Rollout
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <ext:Hidden runat="server" ID="hiddenValue" />
        <ext:Hidden runat="server" ID="hiddenValueRow" />
        <ext:Store runat="server" ID="storeAdditionalStep">
            <Model>
                <ext:Model ID="Model3" IDProperty="Value" runat="server">
                    <Fields>
                        <ext:ModelField Name="Value" />
                        <ext:ModelField Name="Text" Type="String" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
        <ext:Store runat="server" ID="storeEmployee">
            <Model>
                <ext:Model ID="Model7" IDProperty="EmployeeId" runat="server">
                    <Fields>
                        <ext:ModelField Name="NameEIN" />
                        <ext:ModelField Name="EmployeeId" Type="String" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
        <div class="innerLR1">
            <%--<uc1:EmployeeDetailsCtl Id="EmployeeDetailsCtl1" runat="server" />--%>
            <table>
                <tr>
                    <td>
                        <ext:LinkButton Width="140px" runat="server" StyleSpec="padding:0px;margin:0px;margin-left:10px"
                            ID="btnSaveUpdateNew" ValidationGroup="SaveUpdateLevel" Cls="btn btn-primary"
                            Text="<i></i>New Employee">
                            <Listeners>
                                <Click Handler='window.location = "RolloutMega.aspx";' />
                            </Listeners>
                        </ext:LinkButton>
                    </td>
                </tr>
            </table>
            <ext:Label ID="lblMsg" runat="server" />
            <table class="fieldTable">
                <tbody>
                    <tr>
                        <td>
                            <ext:Store runat="server" ID="storeSearch" AutoLoad="true">
                                <Proxy>
                                    <ext:AjaxProxy Url="../../Handler/EmpSearchID.ashx">
                                        <ActionMethods Read="GET" />
                                        <Reader>
                                            <ext:JsonReader Root="plants" TotalProperty="total" />
                                        </Reader>
                                    </ext:AjaxProxy>
                                </Proxy>
                                <Model>
                                    <ext:Model ID="Model2" IDProperty="EmployeeId" Name="ItemLineModel" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Name" Type="String" />
                                            <ext:ModelField Name="EmployeeId" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                            <ext:ComboBox LabelSeparator="" ID="cmbSearch" FieldLabel="Search Employee" LabelWidth="70"
                                LabelAlign="Top" runat="server" DisplayField="Name" ValueField="EmployeeId" StoreID="storeSearch"
                                TypeAhead="false" Width="365px" PageSize="9999" HideBaseTrigger="true" MinChars="1"
                                TriggerAction="All" ForceSelection="true">
                                <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                                    <ItemTpl ID="ItemTpl1" runat="server">
                                        <Html>
                                            <tpl>
                                            <div class="search-item">
                                                            <span>{Name}</span>  
                                             </div>
					                </tpl>
                                        </Html>
                                    </ItemTpl>
                                </ListConfig>
                                <DirectEvents>
                                    <Select OnEvent="cmbGradeEmployee_Select">
                                        <EventMask ShowMask="true" />
                                    </Select>
                                </DirectEvents>
                            </ext:ComboBox>
                            <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator6" runat="server"
                                ValidationGroup="SaveUpdateLevel" ControlToValidate="cmbSearch" ErrorMessage="Employee is required." />
                        </td>
                        <td valign="top" rowspan="4">
                            <div style="margin-bottom: 10px; display: none" id="empDetailsBlock" style="">
                                <ext:Image ID="image" Cls="left" runat="server" ImageUrl="~/images/sample.jpg" Width="150px"
                                    Height="150px" />
                                <div class="left items" style="padding-left: 2px;">
                                    <ext:DisplayField runat="server" class="heading" Style="color: #214B71; margin: 0;
                                        margin-top: 2px; margin-bottom: 3px;" ID="title">
                                    </ext:DisplayField>
                                    <ext:DisplayField runat="server" Style="font-size: 15px;" ID="positionDesignation">
                                    </ext:DisplayField>
                                    <div style="clear: both">
                                    </div>
                                    <img runat="server" id="groupLevelImg" runat="server" src="~/Styles/images/emp_position.png"
                                        style="margin-top: 8px;" />
                                    <ext:DisplayField runat="server" StyleSpec="padding-top: 10px;margin-top:7px;" ID="groupLevel">
                                    </ext:DisplayField>
                                    <div style="clear: both">
                                    </div>
                                    <img id="Img1" runat="server" src="~/Styles/images/emp_br_dept.png" />
                                    <ext:DisplayField runat="server" ID="branch">
                                    </ext:DisplayField>
                                    <div style="clear: both">
                                    </div>
                                    <img id="Img2" runat="server" src="~/Styles/images/emp_work.png" />
                                    <ext:DisplayField runat="server" ID="workingFor" Style="width: 600px">
                                    </ext:DisplayField>
                                    <div style="clear: both">
                                    </div>
                                </div>
                                <div style="clear: both">
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr id="row0" style="display: none">
                        <td>
                            <ext:ComboBox ForceSelection="true" QueryMode="Local" ID="cmbPeriod" FieldLabel="Appraisal Period"
                                runat="server" LabelAlign="Top" DisplayField="Name" Width="365px" LabelSeparator=""
                                ValueField="PeriodId">
                                <Store>
                                    <ext:Store ID="store4" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PeriodId" Type="String" />
                                            <ext:ModelField Name="Name" />
                                        </Fields>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>
                                    <Select OnEvent="cmbPeriod_Change">
                                        <EventMask ShowMask="true" />
                                    </Select>
                                </DirectEvents>
                            </ext:ComboBox>
                            <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator9" runat="server"
                                ValidationGroup="SaveUpdateLevel" ControlToValidate="cmbPeriod" ErrorMessage="Period is required." />
                        </td>
                    </tr>
                    <tr id="row1" style="display: none">
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <ext:DateField ReadOnly="true" ID="dateFrom" runat="server" FieldLabel="Appraisal From"
                                            LabelAlign="Top" Width="176" LabelSeparator="">
                                        </ext:DateField>
                                        <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator4" runat="server"
                                            ValidationGroup="SaveUpdateLevel" ControlToValidate="dateFrom" ErrorMessage="From date is required." />
                                    </td>
                                    <td style="width: 10px">
                                    </td>
                                    <td>
                                        <ext:DateField ReadOnly="true" ID="dateTo" runat="server" FieldLabel="Appraisal To"
                                            LabelAlign="Top" Width="176" LabelSeparator="">
                                        </ext:DateField>
                                        <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator7" runat="server"
                                            ValidationGroup="SaveUpdateLevel" ControlToValidate="dateTo" ErrorMessage="To date is required." />
                                    </td>
                                </tr>
                            </table>
                            <%-- </td>
                        <td colspan="4">--%>
                        </td>
                    </tr>
                    <tr id="row2" style="display: none">
                        <td>
                            <ext:DateField ID="dateDueDate" runat="server" FieldLabel="Due Date" LabelAlign="Top"
                                Width="176" LabelSeparator="">
                            </ext:DateField>
                            <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator5" runat="server"
                                ValidationGroup="SaveUpdateLevel" ControlToValidate="dateDueDate" ErrorMessage="Due date is required." />
                        </td>
                    </tr>
                    <tr id="row3" style="display: none">
                        <td>
                            <ext:ComboBox ForceSelection="true" QueryMode="Local" ID="cmbForm" FieldLabel="Select Appraisal Form"
                                runat="server" LabelAlign="Top" DisplayField="Name" Width="365px" LabelSeparator=""
                                ValueField="AppraisalFormID">
                                <Store>
                                    <ext:Store ID="store1" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="AppraisalFormID" Type="String" />
                                            <ext:ModelField Name="Name" />
                                        </Fields>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>
                                    <Select OnEvent="cmbForm_Change">
                                        <EventMask ShowMask="true" />
                                    </Select>
                                </DirectEvents>
                            </ext:ComboBox>
                            <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator8" runat="server"
                                ValidationGroup="SaveUpdateLevel" ControlToValidate="cmbForm" ErrorMessage="Form is required." />
                        </td>
                    </tr>
                    <tr>
                        <td>
                        <div id="divEduLocationScore"  style="display:none">
                           <table class="fieldTable">
                                <tr>
                                    <td>
                                        Location Score
                                    </td>
                                    <td>
                                        
                                        <ext:DisplayField runat="server" ID="spnLocationScore" ></ext:DisplayField>
                                    </td>
                                    <td>
                                        Education Score
                                    </td>
                                    <td>
                                     <ext:DisplayField runat="server" ID="spnEducationScore"></ext:DisplayField>
                                        
                                    </td>
                                    <td>
                                        Seniority Score
                                    </td>
                                    <td>
                                    <ext:DisplayField runat="server" ID="spnSeniorityScore"></ext:DisplayField>
                                        
                                    </td>
                                </tr>
                            </table>
                            </div>
                        </td>
                        <%-- <tr id="row4" style="display: none">
                        <td>
                            <ext:Store ID="storeAllEmp" runat="server">
                                <Model>
                                    <ext:Model ID="Model3" runat="server" IDProperty="EmployeeId">
                                        <Fields>
                                            <ext:ModelField Name="EmployeeId" Type="String" />
                                            <ext:ModelField Name="Name" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                            <ext:ComboBox LabelSeparator="" TypeAhead="true" ForceSelection="true" QueryMode="Local"
                                FieldLabel="Supervisor (Step 2)" StoreID="storeAllEmp" ValueField="EmployeeId"
                                DisplayField="Name" runat="server" ID="cmbSupervisor" LabelWidth="120" Width="365">
                                <Listeners>
                                    <Focus Handler="#{storeAllEmp}.clearFilter();">
                                    </Focus>
                                </Listeners>
                            </ext:ComboBox>
                            <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator1" runat="server"
                                ValidationGroup="SaveUpdateLevel" ControlToValidate="cmbSupervisor" ErrorMessage="Supervisor is required." />
                        </td>
                    </tr>--%>
                        <tr>
                            <td colspan="2">
                                <ext:GridPanel Hidden="true" Title="Approval Flow" Width="900" Header="true" StyleSpec="margin-top:15px;"
                                    ID="gridFlow" runat="server" Cls="itemgrid">
                                    <Store>
                                        <ext:Store ID="storeAllowances" runat="server">
                                            <Model>
                                                <ext:Model ID="Model6" Name="SettingModel" runat="server">
                                                    <Fields>
                                                        <ext:ModelField Name="StepID" Type="String" />
                                                        <ext:ModelField Name="StepIDName" Type="String" />
                                                        <ext:ModelField Name="StepName" Type="String" />
                                                        <ext:ModelField Name="AuthorityType" Type="String" />
                                                        <ext:ModelField Name="AuthorityTypeName" Type="String" />
                                                        <ext:ModelField Name="AuthorityTypeDisplayName" Type="String" />
                                                        <ext:ModelField Name="EmployeeId" Type="String" />
                                                        <ext:ModelField Name="ShowAdditionalStep" Type="String" />
                                                        <ext:ModelField Name="IsDeletable" Type="Boolean" />
                                                    </Fields>
                                                </ext:Model>
                                            </Model>
                                        </ext:Store>
                                    </Store>
                                    <Plugins>
                                        <ext:CellEditing ID="CellEditing1" ClicksToEdit="1" runat="server">
                                        </ext:CellEditing>
                                    </Plugins>
                                    <Listeners>
                                        <BeforeEdit Fn="beforeEdit" />
                                    </Listeners>
                                    <ColumnModel>
                                        <Columns>
                                            <ext:RowNumbererColumn ID="RowNumbererColumn1" DataIndex="SequenceNo" runat="server"
                                                Width="35" />
                                            <ext:Column ID="colTravelOrder" runat="server" Text="Step" Sortable="false" MenuDisabled="true"
                                                Width="80" DataIndex="StepIDName" Border="false">
                                            </ext:Column>
                                            <ext:Column ID="Column14" runat="server" Text="Status Name" Sortable="false" MenuDisabled="true"
                                                Width="120" DataIndex="StepName" Border="false">
                                                <Editor>
                                                    <ext:TextField ID="TextField2" runat="server" />
                                                </Editor>
                                            </ext:Column>
                                            <ext:Column ID="Column16" runat="server" Text="Authority" Sortable="false" MenuDisabled="true"
                                                Width="120" DataIndex="AuthorityTypeName" Border="false">
                                            </ext:Column>
                                            <ext:Column ID="Column17" runat="server" Text="Authority Display Name" Sortable="false"
                                                MenuDisabled="true" Width="150" DataIndex="AuthorityTypeDisplayName" Border="false">
                                                <Editor>
                                                    <ext:TextField ID="TextField3" runat="server" />
                                                </Editor>
                                            </ext:Column>
                                            <ext:Column ID="Column20" runat="server" Flex="1" Text="Employee" Sortable="false"
                                                MenuDisabled="true" Width="180" DataIndex="EmployeeId" Border="false">
                                                <Renderer Fn="EmpRenderer" />
                                                <Editor>
                                                    <ext:ComboBox DisplayField="NameEIN" ForceSelection="true" QueryMode="Local" ValueField="EmployeeId"
                                                        StoreID="storeEmployee" ID="ComboBox2" runat="server">
                                                    </ext:ComboBox>
                                                </Editor>
                                            </ext:Column>
                                            <ext:Column ID="Column13" runat="server" Text="Additional Step" Sortable="false"
                                                MenuDisabled="true" Width="200" DataIndex="ShowAdditionalStep" Border="false">
                                                <Renderer Fn="additionalRenderer" />
                                                <Editor>
                                                    <ext:ComboBox DisplayField="Text" QueryMode="Local" ForceSelection="true" ValueField="Value"
                                                        StoreID="storeAdditionalStep" ID="ComboBox5" runat="server">
                                                        <Triggers>
                                                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                                        </Triggers>
                                                        <Listeners>
                                                            <Select Handler="this.getTrigger(0).show();" />
                                                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                                            <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                                                        </Listeners>
                                                    </ext:ComboBox>
                                                </Editor>
                                            </ext:Column>
                                            <ext:CommandColumn ID="CommandColumn1" runat="server" Sortable="false" MenuDisabled="true"
                                                Width="30" Align="Center">
                                                <Commands>
                                                    <ext:GridCommand Icon="Delete" CommandName="Delete" Cls="deleteicon" />
                                                </Commands>
                                                <PrepareToolbar Fn="prepareToolbar" />
                                                <Listeners>
                                                    <Command Fn="RemoveItemLine">
                                                    </Command>
                                                </Listeners>
                                            </ext:CommandColumn>
                                        </Columns>
                                    </ColumnModel>
                                    <SelectionModel>
                                        <ext:RowSelectionModel ID="RowSelectionModel3" runat="server" Mode="Single" />
                                    </SelectionModel>
                                </ext:GridPanel>
                            </td>
                        </tr>
                        <%--<tr id="row5" style="display: none">
                        <td>
                            <ext:ComboBox LabelSeparator="" TypeAhead="true" ForceSelection="true" QueryMode="Local"
                                FieldLabel="Reviewer (Step 4)" StoreID="storeAllEmp" ValueField="EmployeeId"
                                DisplayField="Name" runat="server" ID="cmbReviewer" LabelWidth="120" Width="365">
                                <Listeners>
                                    <Focus Handler="#{storeAllEmp}.clearFilter();">
                                    </Focus>
                                </Listeners>
                            </ext:ComboBox>
                            <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator2" runat="server"
                                ValidationGroup="SaveUpdateLevel" ControlToValidate="cmbReviewer" ErrorMessage="Reviewer is required." />
                        </td>
                    </tr>--%>
                        <%--<tr id="row6" style="display: none">
                        <td>
                            <ext:ComboBox LabelSeparator="" TypeAhead="true" ForceSelection="true" QueryMode="Local"
                                FieldLabel="Chief (Step 5)" StoreID="storeAllEmp" ValueField="EmployeeId" DisplayField="Name"
                                runat="server" ID="cmbChief" LabelWidth="120" Width="365">
                                <Listeners>
                                    <Focus Handler="#{storeAllEmp}.clearFilter();">
                                    </Focus>
                                </Listeners>
                            </ext:ComboBox>
                            <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator3" runat="server"
                                ValidationGroup="SaveUpdateLevel" ControlToValidate="cmbChief" ErrorMessage="Chief is required." />
                        </td>
                    </tr>--%>
                        <tr>
                            <td colspan="2">
                                <ext:GridPanel runat="server" ID="gridTargets" Hidden="true" PreventHeader="true"
                                    Frame="false" AutoScroll="true" Width="700" Cls="gridheight" ButtonAlign="Left">
                                    <Store>
                                        <ext:Store ID="StoreTargets" runat="server">
                                            <Model>
                                                <ext:Model runat="server" IDProperty="TargetRef_ID" ID="Model5">
                                                    <Fields>
                                                        <ext:ModelField Name="TargetRef_ID" Type="String" />
                                                        <ext:ModelField Name="Name" />
                                                        <ext:ModelField Name="DefaultTargetValue" Type="String" />
                                                        <ext:ModelField Name="Achievement" Type="String" />
                                                        <ext:ModelField Name="NewWeight" Type="String" />
                                                        <ext:ModelField Name="TargetTextValue" Type="String" />
                                                        <ext:ModelField Name="AchievementTextValue" Type="String" />
                                                    </Fields>
                                                </ext:Model>
                                            </Model>
                                        </ext:Store>
                                    </Store>
                                    <%--<Plugins>
                                    <ext:CellEditing ID="CellEditing1" ClicksToEdit="1" runat="server">
                                       
                                    </ext:CellEditing>
                                </Plugins>--%>
                                    <ColumnModel ID="ColumnModel1" runat="server">
                                        <Columns>
                                            <ext:Column ID="Column9" runat="server" DataIndex="Name" Text="Selected Targets"
                                                MenuDisabled="true" Sortable="false" Flex="1" />
                                            <ext:Column ID="Column10" runat="server" DataIndex="TargetRef_ID" Text="ID" Hidden="false"
                                                Width="0" MenuDisabled="true" Sortable="false" />
                                            <ext:Column ID="ColumnTargetValue" runat="server" Text="Target Value" DataIndex="DefaultTargetValue"
                                                Width="100" MenuDisabled="true" Sortable="false">
                                                <Editor>
                                                    <ext:TextField ID="txtWeight" runat="server" MinValue="0">
                                                    </ext:TextField>
                                                </Editor>
                                            </ext:Column>
                                            <ext:Column ID="ColummAchievementValue" runat="server" Text="Achievement" DataIndex="Achievement"
                                                Width="100" MenuDisabled="true" Sortable="false">
                                                <Editor>
                                                    <ext:TextField ID="TextField1" runat="server" MinValue="0">
                                                    </ext:TextField>
                                                </Editor>
                                            </ext:Column>
                                            <ext:Column ID="ColumnWeight" Hidden="true" runat="server" DataIndex="NewWeight"
                                                Text="Weight" Width="100" MenuDisabled="true" Sortable="false" />
                                            <ext:Column ID="ColumnTargetText" Hidden="true" runat="server" DataIndex="TargetTextValue"
                                                Text="Target" Width="200" MenuDisabled="true" Sortable="false" />
                                            <ext:Column ID="ColumnAchievementText" Hidden="true" runat="server" DataIndex="AchievementTextValue"
                                                Text="Achievement" Width="200" MenuDisabled="true" Sortable="false" />
                                        </Columns>
                                    </ColumnModel>
                                    <SelectionModel>
                                        <ext:RowSelectionModel ID="CheckboxSelectionModel2a" runat="server" Mode="Single" />
                                    </SelectionModel>
                                </ext:GridPanel>
                            </td>
                        </tr>
                        <tr id="row7" style="display: none">
                            <td colspan='2'>
                                <span class="x-form-item-label">Employee Status History</span>
                                <ext:Label ID="lblStatus" runat="server">
                                </ext:Label>
                            </td>
                        </tr>
                        <tr id="row8" style="display: none">
                            <td colspan='2'>
                                <span class="x-form-item-label">Education Details</span>
                                <%--  <ext:TextArea ID="txtComment" Hidden="true" runat="server" Width="500px">
                            </ext:TextArea>--%>
                                <ucEducation:EducationCtl IsDisplayMode="true" ID="EducationCtl" runat="server" />
                            </td>
                        </tr>
                </tbody>
            </table>
            <div id="divBlocks" style="display: none">
                <%-- <div style="padding-left: 15px">
                    <span class="x-form-item-label">Salary Details</span>
                    <ext:Label ID="lblSalaryDetails" runat="server">
                    </ext:Label>
                </div>--%>
                <table class="fieldTable">
                    <tr>
                        <td>
                            <span class="x-form-item-label">Transfer History</span>
                            <ext:GridPanel ID="gridTransferList" runat="server" Width="700px">
                                <Store>
                                    <ext:Store ID="Store3" runat="server">
                                        <Model>
                                            <ext:Model ID="Model4" runat="server" IDProperty="BranchDepartmentId">
                                                <Fields>
                                                    <ext:ModelField Name="Type" Type="string" />
                                                    <ext:ModelField Name="FromBranch" Type="string" />
                                                    <ext:ModelField Name="ToBranch" Type="string" />
                                                    <ext:ModelField Name="FromDate" Type="string" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:Column ID="Column8" Sortable="false" MenuDisabled="true" runat="server" Text="Type"
                                            Align="Left" Width="200" DataIndex="Type" />
                                        <ext:Column ID="Column4" Width="200" Sortable="false" MenuDisabled="true" runat="server"
                                            Text="From" Align="Left" DataIndex="FromBranch">
                                        </ext:Column>
                                        <ext:Column ID="Column5" Sortable="false" MenuDisabled="true" runat="server" Text="To"
                                            Align="Left" Width="200" DataIndex="ToBranch" />
                                        <ext:Column ID="Column1" Sortable="false" MenuDisabled="true" runat="server" Text="Date"
                                            Align="Left" Width="100" DataIndex="FromDate" />
                                    </Columns>
                                </ColumnModel>
                                <SelectionModel>
                                    <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                                </SelectionModel>
                            </ext:GridPanel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="x-form-item-label">Training History</span>
                            <ext:GridPanel ID="gridTraining" runat="server" Width="700px">
                                <Store>
                                    <ext:Store ID="Store2" runat="server">
                                        <Model>
                                            <ext:Model ID="Model1" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="TrainingName" Type="string" />
                                                    <ext:ModelField Name="InstitutionName" Type="string" />
                                                    <ext:ModelField Name="DurationText" Type="string" />
                                                    <ext:ModelField Name="TrainingFrom" Type="string" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:Column ID="Column2" Sortable="false" MenuDisabled="true" runat="server" Text="Training Name"
                                            Align="Left" Width="300" DataIndex="TrainingName" />
                                        <ext:Column ID="Column3" Width="200" Sortable="false" MenuDisabled="true" runat="server"
                                            Text="Institution" Align="Left" DataIndex="InstitutionName">
                                        </ext:Column>
                                        <ext:Column ID="Column6" Sortable="false" MenuDisabled="true" runat="server" Text="Duration"
                                            Align="Left" Width="100" DataIndex="DurationText" />
                                        <ext:Column ID="Column7" Sortable="false" MenuDisabled="true" runat="server" Text="Date"
                                            Align="Left" Width="100" DataIndex="TrainingFrom" />
                                    </Columns>
                                </ColumnModel>
                                <SelectionModel>
                                    <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                                </SelectionModel>
                            </ext:GridPanel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <span class="x-form-item-label">Note</span>
                            <ext:TextArea ID="txtNotes" runat="server" Width="500px">
                            </ext:TextArea>
                        </td>
                    </tr>
                </table>
                <div class="popupButtonDiv" style="padding-left: 15px">
                    <ext:Button Width="120px" runat="server" ID="btnLevelSaveUpdate" ValidationGroup="SaveUpdateLevel"
                        Cls="btn btn-primary" Text="<i></i>Save">
                        <DirectEvents>
                            <Click OnEvent="btnLevelSaveUpdate_Click">
                                <EventMask ShowMask="true" />
                                <ExtraParams>
                                    <ext:Parameter Name="TargetList" Value="Ext.encode(#{gridTargets}.getRowsValues({selectedOnly : false}))"
                                        Mode="Raw" />
                                    <ext:Parameter Name="FlowList" Value="Ext.encode(#{gridFlow}.getRowsValues({selectedOnly : false}))"
                                        Mode="Raw" />
                                </ExtraParams>
                            </Click>
                        </DirectEvents>
                        <Listeners>
                            <Click Handler="valGroup = 'SaveUpdateLevel'; return CheckValidation();">
                            </Click>
                        </Listeners>
                    </ext:Button>
                    <div class="btnFlatOr">
                        or</div>
                    <ext:LinkButton runat="server" StyleSpec="padding:0px;" Cls="btnFlatLeftGap" ID="LinkButton1"
                        Text="<i></i>Cancel" runat="server">
                        <Listeners>
                            <Click Handler="window.location = 'Rollout.aspx';">
                            </Click>
                        </Listeners>
                    </ext:LinkButton>
                </div>
            </div>
            <br />
            <br />
            <br />
            <br />
        </div>
    </div>
</asp:Content>
