﻿<%@ Page Title="Competencies" Language="C#" MasterPageFile="~/Master/NewDetails.master"
    ValidateRequest="false" AutoEventWireup="true" CodeBehind="CoreCompetencies_5.aspx.cs"
    Inherits="Web.Appraisal.Form.CoreCompetencies_5" %>

<%@ Register Src="~/Controls/HRAppraisalFormWizard.ascx" TagPrefix="ucHRW" TagName="ucHRFormWizard" %>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script>
        Ext.onReady(function () {
            CKEDITOR.config.toolbar = [
               ['Styles', 'Format', 'Font', 'FontSize'],

               ['Bold', 'Italic', 'Underline', 'StrikeThrough', '-', 'Undo', 'Redo', '-', 'Cut', 'Copy', 'Paste', 'Find', 'Replace', '-', 'Outdent', 'Indent', '-'],
    
               ['NumberedList', 'BulletedList', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
               ['Image', 'Table', '-', 'Link', 'TextColor', 'BGColor', 'Maximize']
            ];

        });

         var afterEdit = function (editor, e) {
            e.record.commit();
        }
        
        function process(e1,e2,e3,e4,e5)
        {
        
            var sourceId = e2.records[0].data.CompetencyID;
            <%=hdnSource.ClientID %>.setValue(sourceId);
            var CompetencyID = e3.data.CompetencyID;
            <%=hdnDest.ClientID %>.setValue(CompetencyID);
            <%=hdnDropMode.ClientID %>.setValue(e4);
//             Ext.net.Notification.show({title:e2.records[0].data.Name,html: e3.data.Name});
            
        }

            function SetEditorValue()
        {
        <%=hdnEditorDescription.ClientID%>.setValue(CKEDITOR.instances.ctl00_ContentPlaceHolder_Main_txtEditorDescription.getData());
        }

        function toggle_visibility(id) {
       var e = document.getElementById(id);
       var divArrowID =  id.replace('a', 'arrow');
       var eArrow = document.getElementById(divArrowID);

       if(e.style.display == 'block')
       {    
          e.style.display = 'none';
          eArrow.innerHTML='►';
          }
       else
       {
          e.style.display = 'block';
          eArrow.innerHTML='▼';
          }

    }


    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Appraisal Report
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel" style="margin-top: 0px !important; padding-top: 0px !important;">
        <ext:Hidden runat="server" ID="hiddenValue" />
        <ext:Hidden runat="server" ID="hdnEditorDescription" />
        <ext:Hidden runat="server" ID="hdnDropMode" />
        <ext:Hidden runat="server" ID="hdnSource" />
        <ext:Hidden runat="server" ID="hdnDest" />
        <%--    <ext:LinkButton runat="server" Hidden="true" ID="btnChangeDesignationOrder" runat="server">
        <DirectEvents>
            <Click OnEvent="btnChangeDesignationOrder_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>--%>
        <div class="innerLR">
            <div class="separator bottom">
            </div>
            <ucHRW:ucHRFormWizard Id="ucAppraisalWizard" runat="server" />
            <ext:Label ID="lblMsg" runat="server" />
            <h4 class="heading">
                Competencies</h4>
            <table class="fieldTable firsttdskip">
                <tr>
                    <td>
                    <ext:Checkbox StyleSpec="margin-top:2px" ID="chkHideBlock" runat="server" BoxLabel="Hide This Block" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:TextField ID="txtSectionName" FieldLabel="Section Name *" runat="server" LabelAlign="Top"
                            Width="400" LabelSeparator="">
                        </ext:TextField>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Section Name is required"
                            Display="None" ControlToValidate="txtSectionName" ValidationGroup="SaveFrom"
                            runat="server">
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="components">
                            <div class="grids">
                                <p>
                                    Description</p>
                                <CKEditor:CKEditorControl ExtraPlugins="autogrow" ID="txtEditorDescription" Width="800px"
                                    Height="200px" runat="server" BasePath="~/ckeditor/">
                                </CKEditor:CKEditorControl>
                            </div>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Description is required"
                            Display="None" ControlToValidate="txtEditorDescription" ValidationGroup="SaveFrom"
                            runat="server">
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
            </table>
            <div style="font-style: italic; padding-top: 30px">
                Select the rating scale from the available rating scale. You may create a new rating
                scale from Evaluation Settings</div>
            <ext:ComboBox ForceSelection="true" QueryMode="Local" ID="cmbRatingScale" FieldLabel="Rating Scale *"
                runat="server" LabelAlign="Top" DisplayField="Name" Width="180" LabelSeparator=""
                ValueField="RatingScaleID">
                <Store>
                    <ext:Store ID="Store1" runat="server">
                        <Fields>
                            <ext:ModelField Name="Name" Type="String" />
                            <ext:ModelField Name="RatingScaleID" Type="String" />
                        </Fields>
                    </ext:Store>
                </Store>
            </ext:ComboBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Rating scale is required"
                            Display="None" ControlToValidate="cmbRatingScale" ValidationGroup="SaveFrom"
                            runat="server">
                        </asp:RequiredFieldValidator>

            <div style="font-style: italic; padding-top: 10px">
                Select self rating if you want your employees to Rate for their activities themselves</div>
            <ext:Checkbox StyleSpec="margin-top:2px" ID="chkAllowSelfRating" runat="server" BoxLabel="Allow Self Rating" />
            <ext:Checkbox StyleSpec="margin-top:2px" ID="chkAllowSelfComment" runat="server"
                BoxLabel="Allow Self Comment" />
            <div style="font-weight: bold; padding-top: 30px; font-size: 18px;">
                Add Competencies to the form</div>
            <div style="font-style: italic">
                Company values are the core competencies you have created in the competencis list</div>
            <div style="padding-top: 30px; float: left">
                Select the competencies you want to use</div>
            <div style="padding-top: 30px; margin-left: 900px;">
                <ext:Button runat="server" Hidden="true" Cls="btn btn-primary" ID="lnkBtnDivideWeight"
                    Text="<i></i>Distribute Weight">
                    <DirectEvents>
                        <Click OnEvent="lnkBtnDivideWeight_Click">
                            <Confirmation ConfirmRequest="true" Message="Confirm divide the weight equally between all competencies?" />
                            <EventMask ShowMask="true" />
                        </Click>
                    </DirectEvents>
                </ext:Button>
            </div>
            <div style="clear: both">
            </div>
            <table class="fieldTable firsttdskip">
                <tr>
                    <td valign="top">
                        <ext:GridPanel runat="server" ID="gridCompetencies" PreventHeader="true" Frame="false"
                            Height="200" AutoScroll="true" Region="Center" Cls="gridheight" ButtonAlign="Left">
                            <Store>
                                <ext:Store ID="StoregridCompetencies" runat="server">
                                    <Model>
                                        <ext:Model runat="server" IDProperty="CategoryID" ID="Model1">
                                            <Fields>
                                                <ext:ModelField Name="CategoryID" Type="String" />
                                                <ext:ModelField Name="Name" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                    <%-- <Sorters>
                                    <ext:DataSorter Property="Name" Direction="ASC" />
                                </Sorters>--%>
                                </ext:Store>
                            </Store>
                            <ColumnModel ID="ColumnModel2" runat="server">
                                <Columns>
                                    <ext:Column ID="Column3" runat="server" DataIndex="Name" Text="Available competencies Category"
                                        MenuDisabled="true" Sortable="false" Flex="1" />
                                    <ext:Column ID="Column4" runat="server" DataIndex="CategoryID" Text="ID" Hidden="true"
                                        Width="0" MenuDisabled="true" Sortable="false" />
                                </Columns>
                            </ColumnModel>
                            <View>
                                <ext:GridView ID="GridView2" runat="server" StripeRows="false" Width="400">
                                </ext:GridView>
                            </View>
                            <SelectionModel>
                                <ext:RowSelectionModel ID="rowSelectionModel1" runat="server" Mode="Multi" />
                            </SelectionModel>
                        </ext:GridPanel>
                    </td>
                    <td style="vertical-align: top">
                        <ext:Button ID="btnAddCompetencies" runat="server" Icon="Add" Text="&nbsp;Add&nbsp;&nbsp;"
                            Width="100">
                            <DirectEvents>
                                <Click OnEvent="btnAddCompetencies_Click">
                                    <EventMask ShowMask="true" />
                                    <ExtraParams>
                                        <ext:Parameter Name="CompetencyList" Value="Ext.encode(#{gridCompetencies}.getRowsValues({selectedOnly : false}))"
                                            Mode="Raw" />
                                    </ExtraParams>
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                        <br />
                        <ext:Button ID="btnRemoveCompetencies" runat="server" Width="100" Icon="Delete" Text="&nbsp;Remove&nbsp;&nbsp">
                            <DirectEvents>
                                <Click OnEvent="btnRemoveCompetencies_Click">
                                    <Confirmation ConfirmRequest="true" Message="Confirm delete the selected Competency?" />
                                    <EventMask ShowMask="true" />
                                    <ExtraParams>
                                        <ext:Parameter Name="CompetencyListSelected" Value="Ext.encode(#{GridAddedCompetencies}.getRowsValues({selectedOnly : false}))"
                                            Mode="Raw" />
                                    </ExtraParams>
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                        <br />
                        <ext:Button ID="btnRemoveAllCompetencies" runat="server" Icon="BasketDelete" Text="&nbsp;Remove All&nbsp;&nbsp;"
                            Width="100">
                            <DirectEvents>
                                <Click OnEvent="btnRemoveAllCompetencies_Click">
                                    <Confirmation ConfirmRequest="true" Message="Confirm delete all Competency?" />
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                    </td>
                    <td valign="top">
                       
                        <ext:GridPanel runat="server" ID="GridAddedCompetencies" PreventHeader="true" Frame="false" MinHeight="300" MaxHeight="700" AutoScroll="true" 
                             Width="500" Region="Center" Cls="gridheight" ButtonAlign="Left"
                            EnableDragDrop="true" DDGroup="ddGroup">
                            <Store>
                                <ext:Store ID="StoreGridAddedCompetencies" runat="server">
                                    <Model>
                                        <ext:Model runat="server" IDProperty="CategoryID" ID="Model2">
                                            <Fields>
                                                <ext:ModelField Name="CategoryID" Type="String" />
                                                <ext:ModelField Name="Name" />
                                                <ext:ModelField Name="FormattedName" />
                                                <ext:ModelField Name="Weight" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                    <%-- <Sorters>
                                    <ext:DataSorter Property="Name" Direction="ASC" />
                                </Sorters>--%>
                                </ext:Store>
                            </Store>
                            <Plugins>
                                <ext:CellEditing ID="CellEditing1" ClicksToEdit="1" runat="server">
                                    <Listeners>
                                        <Edit Fn="afterEdit" />
                                    </Listeners>
                                </ext:CellEditing>
                            </Plugins>
                            <ColumnModel ID="ColumnModel1" runat="server">
                                <Columns>
                                    <ext:Column ID="Column1" runat="server" DataIndex="FormattedName" Text="Selected competencies Category"
                                        MenuDisabled="true" Sortable="false" Flex="1" />
                                    <ext:Column ID="Column2" runat="server" DataIndex="CategoryID" Text="ID" Hidden="true"
                                        Width="0" MenuDisabled="true" Sortable="false" />
                                    <ext:Column ID="Column5" runat="server" Text="Weight" DataIndex="Weight" Width="100"
                                        MenuDisabled="true" Sortable="false">
                                        <Editor>
                                            <ext:TextField ID="txtWeight" MaskRe="[0-9]|\.|%" runat="server" MinValue="0">
                                            </ext:TextField>
                                        </Editor>
                                    </ext:Column>
                                </Columns>
                            </ColumnModel>
                            <View>
                                <ext:GridView ID="GridView1" runat="server" MinHeight="100" StripeRows="false">
                                    <Plugins>
                                        <ext:GridDragDrop ID="GridDragDrop1" runat="server" DragGroup="ddGroup" DropGroup="ddGroup" />
                                    </Plugins>
                                    <Listeners>
                                        <Drop Fn="process" />
                                        <%-- <AfterRender Handler="this.plugins[0].dragZone.getDragText = getDragDropText;" />--%>
                                        <%--  <Drop Handler="var dropOn = overModel ? ' ' + dropPosition + ' ' + overModel.get('Name') : ' on empty view'; 
                                               Ext.net.Notification.show({title:'Drag from right to left', html:'Dropped ' + data.records[0].get('Name') + dropOn});" />--%>
                                    </Listeners>
                                </ext:GridView>
                            </View>
                            <SelectionModel>
                                <ext:RowSelectionModel ID="CheckboxSelectionModel2a" runat="server" Mode="Single" />
                            </SelectionModel>
                        </ext:GridPanel>
                    </td>
                </tr>
            </table>
            <div class="buttonBlock">
                <table>
                    <tr>
                        <td>
                            <ext:Button ID="btnSave" Cls="btn btn-primary" runat="server" Text="&nbsp;&nbsp;Save&nbsp;&nbsp;"
                                ValidationGroup="InsertUpdate">
                                <DirectEvents>
                                    <Click OnEvent="ButtonNext_Click">
                                        <EventMask ShowMask="true" />
                                        <ExtraParams>
                                            <ext:Parameter Name="CompetencyList" Value="Ext.encode(#{GridAddedCompetencies}.getRowsValues({selectedOnly : false}))"
                                                Mode="Raw" />
                                        </ExtraParams>
                                    </Click>
                                </DirectEvents>
                                <Listeners>
                                    <Click Handler="SetEditorValue();valGroup = 'SaveFrom'; return CheckValidation();">
                                    </Click>
                                </Listeners>
                            </ext:Button>
                        </td>
                        <td>
                            &nbsp;&nbsp;&nbsp;
                        </td>
                        <td>
                            <ext:Button ID="btnNext" Cls="btn btn-primary" runat="server" Text="&nbsp;&nbsp;Save and Next&nbsp;&nbsp;"
                                ValidationGroup="InsertUpdate">
                                <DirectEvents>
                                    <Click OnEvent="ButtonNext_Click">
                                        <EventMask ShowMask="true" />
                                        <ExtraParams>
                                            <ext:Parameter Name="CompetencyList" Value="Ext.encode(#{GridAddedCompetencies}.getRowsValues({selectedOnly : false}))"
                                                Mode="Raw">
                                            </ext:Parameter>
                                        </ExtraParams>
                                    </Click>
                                </DirectEvents>
                                <Listeners>
                                    <Click Handler="SetEditorValue();valGroup = 'SaveFrom'; return CheckValidation();">
                                    </Click>
                                </Listeners>
                            </ext:Button>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
