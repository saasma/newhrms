﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Entity;
using BLL;
using BLL.BO;
using Utils.Helper;
using Utils.Calendar;


namespace Web.Appraisal.Form
{
    public partial class Targets_6 : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();

            }
        }

        public void Initialise()
        {

            List<AppraisalTarget> _ListAppraisalCompetencyCategory = NewHRManager.GetAllAppraisalTargets();

            if (_ListAppraisalCompetencyCategory.Any())
            {
                StoregridCompetencies.DataSource = _ListAppraisalCompetencyCategory;
                StoregridCompetencies.DataBind();
            }
            if (!string.IsNullOrEmpty(Request.QueryString["fid"]))
            {
                LoadData(int.Parse(Request.QueryString["fid"]));

                //if(NewHRManager.IsAppraisalCompetencyUsedInEmployee(int.Parse(Request.QueryString["fid"])))
                //{
                //   btnAddCompetencies.Disabled=true;
                //   btnRemoveAllCompetencies.Disabled=true;
                //   btnRemoveCompetencies.Disabled=true;
                //   SetWarning(lblMsg, "You can't add and Remove Target as this data has been used in employee from.");
                //}
               
            }

            cmbRatingScale.Store[0].DataSource = NewHRManager.GetAllRatingScale();
            cmbRatingScale.Store[0].DataBind();

        }

        protected void LoadData(int ID)
        {
            AppraisalForm _AppraisalForm = NewHRManager.GetAppraisal_GeneralInfoByID(ID);
            if (_AppraisalForm != null)
            {
                if(!string.IsNullOrEmpty(_AppraisalForm.TargetName))
                    txtSectionName.Text = _AppraisalForm.TargetName;
                if (!string.IsNullOrEmpty(_AppraisalForm.TargetDescription))
                    txtEditorDescription.Text = _AppraisalForm.TargetDescription;
                if (_AppraisalForm.TargetRatingScaleRef_ID != null)
                    ExtControlHelper.ComboBoxSetSelected(_AppraisalForm.TargetRatingScaleRef_ID.ToString(), cmbRatingScale);

                if (_AppraisalForm.HideTarget != null)
                    chkHideBlock.Checked = _AppraisalForm.HideTarget.Value;

                if (_AppraisalForm.AllowTargetSelfRating != null)
                    chkAllowSelfRating.Checked = _AppraisalForm.AllowTargetSelfRating.Value;

                if (_AppraisalForm.AllowTargetSelfComment != null)
                {
                    chkAllowSelfComment.Checked = _AppraisalForm.AllowTargetSelfComment.Value;
                }
                if (_AppraisalForm.TargetHideAssignedShowRating != null)
                    chkHideAssignAchievmentShowRating.Checked = _AppraisalForm.TargetHideAssignedShowRating.Value;

                List<AppraisalTarget> _ListAppraisalCompetency = NewHRManager.GetAllAppraisalTargets();

                List<AppraisalFormTarget> _ListAppraisalFormCompetency = NewHRManager.GetAppraisalFromTargets(ID);

                foreach (AppraisalFormTarget item in _ListAppraisalFormCompetency)
                {
                    item.Name = _ListAppraisalCompetency.FirstOrDefault(x => x.TargetID == item.TargetRef_ID).Name;
                    item.TargetID = item.TargetRef_ID;
                }

                if (_ListAppraisalFormCompetency.Any())
                {
                    StoreGridAddedCompetencies.DataSource = _ListAppraisalFormCompetency;
                    StoreGridAddedCompetencies.DataBind();


                    List<string> _ListAdded = new List<string>();

                    _ListAdded = (
                       (from _inst in _ListAppraisalFormCompetency
                        select _inst.TargetRef_ID.ToString()).ToList()
                          );

                    List<AppraisalTarget> _RemainingList = null;
                    _RemainingList = _ListAppraisalCompetency.Where(i => !_ListAdded.Contains(i.TargetID.ToString())).ToList();

                    StoregridCompetencies.DataSource = _RemainingList;
                    StoregridCompetencies.DataBind();


                }

                

            }

        }


        protected void ButtonBack_Click(object sender, DirectEventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["fid"]))
                Response.Redirect("CoreCompetencies_5.aspx?fid=" + Request.QueryString["fid"]);
        
        }


        protected void lnkBtnDivideWeight_Click(object sender, DirectEventArgs e)
        {
            //int FormID = int.Parse(Request.QueryString["fid"]);
            //List<AppraisalFormCompetency> _ListAppraisalFormCompetency = NewHRManager.GetAppraisalFromCompetency_ByID(FormID);

            ////string SumOfAllWeight = _ListAppraisalFormCompetency.Sum(x => x.Weight).ToString();
            //if (_ListAppraisalFormCompetency.Count() <= 0)
            //{
            //    NewMessage.ShowWarningMessage("Compentency should be saved before applying distribution.");
            //    return;
            //}
            //decimal sum = 100;
            //decimal NowEquallyDividedWeigth = sum / _ListAppraisalFormCompetency.Count();
            //AppraisalFormCompetency _AppraisalFormQuestionnaire = new AppraisalFormCompetency();
            //_AppraisalFormQuestionnaire.AppraisalFormRef_ID = FormID;
            //_AppraisalFormQuestionnaire.Weight = NowEquallyDividedWeigth;
            //Status status = AppraisalManager.DivieCompetencyWeightEqually(_AppraisalFormQuestionnaire);
            //if (status.IsSuccess)
            //{
            //    NewMessage.ShowNormalMessage("weight divided equally.");
            //    LoadData(int.Parse(Request.QueryString["fid"]));
            //}
            //else
            //{
            //    NewMessage.ShowWarningMessage(status.ErrorMessage);
            //}

        }

        protected void btnAddCompetencies_Click(object sender, DirectEventArgs e)
        {
            string entryLineJson = e.ExtraParams["CompetencyList"];

            List<AppraisalFormTarget> _LineList = JSON.Deserialize<List<AppraisalFormTarget>>(entryLineJson);
            List<AppraisalTarget> _ListAppraisalCompetency = new List<AppraisalTarget>();
            RowSelectionModel sm = this.gridCompetencies.GetSelectionModel() as RowSelectionModel;


            foreach (SelectedRow row in sm.SelectedRows)
            {
                AppraisalTarget _AppraisalCompetency = new AppraisalTarget();
                string RowID = row.RecordID;
                _AppraisalCompetency.TargetID = int.Parse(row.RecordID);
                _AppraisalCompetency.Name = _LineList.SingleOrDefault(x => x.TargetID == _AppraisalCompetency.TargetID).Name;
                _ListAppraisalCompetency.Add(_AppraisalCompetency);
            }

            if (_ListAppraisalCompetency.Any())
            {
                StoreGridAddedCompetencies.Add(_ListAppraisalCompetency);
                //StoreGridAddedCompetencies.DataBind();

                List<string> _ListAdded = new List<string>();

                _ListAdded = (
                   (from _inst in _ListAppraisalCompetency
                    select _inst.TargetID.ToString()).ToList()
                      );

                List<AppraisalFormTarget> _RemainingList = null;
                _RemainingList = _LineList.Where(i => !_ListAdded.Contains(i.TargetID.ToString())).ToList();

                sm.ClearSelection();
                sm.SelectedRows.Clear();
                sm.UpdateSelection();

                StoregridCompetencies.DataSource = _RemainingList;
                StoregridCompetencies.DataBind();

            }

        }

        protected void btnRemoveCompetencies_Click(object sender, DirectEventArgs e)
        {

            string entryLineJson = e.ExtraParams["CompetencyListSelected"];

            List<AppraisalTarget> _LineList = JSON.Deserialize<List<AppraisalTarget>>(entryLineJson);
            List<AppraisalTarget> _ListAppraisalCompetency = new List<AppraisalTarget>();
            RowSelectionModel sm = this.GridAddedCompetencies.GetSelectionModel() as RowSelectionModel;


            foreach (SelectedRow row in sm.SelectedRows)
            {
                AppraisalTarget _AppraisalCompetency = new AppraisalTarget();
                string RowID = row.RecordID;
                _AppraisalCompetency.TargetID = int.Parse(row.RecordID);
                _AppraisalCompetency.Name = _LineList.SingleOrDefault(x => x.TargetID == _AppraisalCompetency.TargetID).Name;
                _ListAppraisalCompetency.Add(_AppraisalCompetency);
            }

            if (_ListAppraisalCompetency.Any())
            {
                StoregridCompetencies.Add(_ListAppraisalCompetency);
                //StoreGridAddedCompetencies.DataBind();

                List<string> _ListAdded = new List<string>();

                _ListAdded = (
                   (from _inst in _ListAppraisalCompetency
                    select _inst.TargetID.ToString()).ToList()
                      );

                List<AppraisalTarget> _RemainingList = null;
                _RemainingList = _LineList.Where(i => !_ListAdded.Contains(i.TargetID.ToString())).ToList();

                sm.ClearSelection();
                sm.SelectedRows.Clear();
                sm.UpdateSelection();

                StoreGridAddedCompetencies.DataSource = _RemainingList;
                StoreGridAddedCompetencies.DataBind();

            }

        }

        protected void btnRemoveAllCompetencies_Click(object sender, DirectEventArgs e)
        {
            List<AppraisalTarget> _ListAppraisalCompetency = NewHRManager.GetAllAppraisalTargets();
            if (_ListAppraisalCompetency.Any())
            {
                StoregridCompetencies.DataSource = _ListAppraisalCompetency;
                StoregridCompetencies.DataBind();
            }

            List<AppraisalTarget> _AppraisalFrom = new List<AppraisalTarget>();
            StoreGridAddedCompetencies.DataSource = _AppraisalFrom;
            StoreGridAddedCompetencies.DataBind();
            RowSelectionModel sm = this.GridAddedCompetencies.GetSelectionModel() as RowSelectionModel;
            sm.ClearSelection();
            sm.SelectedRows.Clear();
            sm.UpdateSelection();

        }
     
        protected void ButtonNext_Click(object sender, DirectEventArgs e)
        {
            AppraisalForm _AppraisalForm = new AppraisalForm();
             string entryLineJson = e.ExtraParams["CompetencyList"];
             List<AppraisalFormTarget> _LineList = JSON.Deserialize<List<AppraisalFormTarget>>(entryLineJson);
            List<AppraisalFormTarget> _ListAppraisalFromCompetency = new List<AppraisalFormTarget>();

            int seq = 0;
            foreach (AppraisalFormTarget _List in _LineList)
            {
                seq++;
                AppraisalFormTarget _AppraisalFormCompetency = new AppraisalFormTarget();
                //if(double.Parse(string.IsNullOrEmpty(_List.Weight)?"0":_List.Weight.ToString())<=0)
                //{
                //    SetWarning(lblMsg, "Weight Can't be empty for competency selected.");
                //    X.Js.AddScript("window.scrollTo(0,0);");
                //    return;
                //}
                //if (_List.Weight == "")
                //    _List.Weight = "0";
                _AppraisalFormCompetency.TargetID = _List.TargetID;
                _AppraisalFormCompetency.TargetRef_ID = _List.TargetID == null ? 0 : _List.TargetID.Value;
                _AppraisalFormCompetency.AppraisalFormRef_ID = int.Parse(Request.QueryString["fid"]);
                _AppraisalFormCompetency.Weight = _List.Weight == null ? 0 : _List.Weight.Value;
                //_AppraisalFormCompetency.Weight = _List.Weight== null ? 0 :  decimal.Parse(_List.Weight);
                _AppraisalFormCompetency.Sequence = seq;
                _ListAppraisalFromCompetency.Add(_AppraisalFormCompetency);

            }

            _AppraisalForm.TargetName = txtSectionName.Text.Trim();
            _AppraisalForm.TargetDescription = hdnEditorDescription.Text.Trim();
            _AppraisalForm.TargetRatingScaleRef_ID = int.Parse(cmbRatingScale.SelectedItem.Value);
            _AppraisalForm.HideTarget = chkHideBlock.Checked;
            _AppraisalForm.AppraisalFormID = int.Parse(Request.QueryString["fid"]);
            _AppraisalForm.AllowTargetSelfRating = chkAllowSelfRating.Checked;
            _AppraisalForm.AllowTargetSelfComment = chkAllowSelfComment.Checked;
            _AppraisalForm.TargetHideAssignedShowRating = chkHideAssignAchievmentShowRating.Checked;
            Status respStatus;

            respStatus = NewHRManager.UpdateAppraisalForm_Targets(_AppraisalForm, _ListAppraisalFromCompetency);
            if (respStatus.IsSuccess)
            {
                if (sender == btnSave)
                    SetMessage(lblMsg, "Appraisal updated.");
                else
                    Response.Redirect("Questionnaire.aspx?fid=" + _AppraisalForm.AppraisalFormID);

              
            }
            else
            {
                SetWarning(lblMsg, respStatus.ErrorMessage);
            }
            X.Js.AddScript("window.scrollTo(0,0);");
        }
    }
}