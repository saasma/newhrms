﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Entity;
using BLL;
using BLL.BO;
using Utils.Helper;
using Utils.Calendar;


namespace Web.Appraisal.Form
{
    public partial class Objectives_3 : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();

            }
        }

        public void Initialise()
        {
            if (!string.IsNullOrEmpty(Request.QueryString["fid"]))
            {
                LoadData(int.Parse(Request.QueryString["fid"]));
            }
        }

        protected void LoadData(int ID)
        {
            AppraisalForm _AppraisalForm = NewHRManager.GetAppraisal_GeneralInfoByID(ID);
            if (_AppraisalForm != null)
            {
                if(!string.IsNullOrEmpty(_AppraisalForm.ObjectiveName))
                txtSectionName.Text = _AppraisalForm.ObjectiveName;
                if (!string.IsNullOrEmpty(_AppraisalForm.ObjectiveDescription))
                txtEditorDescription.Text = _AppraisalForm.ObjectiveDescription;

                if (_AppraisalForm.HideObjectiveBlock != null)
                    chkHideBlock.Checked = _AppraisalForm.HideObjectiveBlock.Value;
            }

        }

        protected void ButtonBack_Click(object sender, DirectEventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["fid"]))
                Response.Redirect("Introduction_2.aspx?fid=" + Request.QueryString["fid"]);
        
        }

        protected void ButtonNext_Click(object sender, DirectEventArgs e)
        {
            AppraisalForm _AppraisalForm = new AppraisalForm();

            _AppraisalForm.ObjectiveName = txtSectionName.Text.Trim();
            _AppraisalForm.ObjectiveDescription = hdnEditorDescription.Text.Trim();
            _AppraisalForm.AppraisalFormID = int.Parse(Request.QueryString["fid"]);
            _AppraisalForm.HideObjectiveBlock = chkHideBlock.Checked;
            Status respStatus;

            respStatus = NewHRManager.UpdateAppraisalForm_Objectives(_AppraisalForm);
            if (respStatus.IsSuccess)
            {

                if (sender == btnSave)
                    SetMessage(lblMsg, "Appraisal updated.");
                else
                    Response.Redirect("PerformanceSummary.aspx?fid=" + Request.QueryString["fid"]);

            }
            else
            {
                SetWarning(lblMsg, respStatus.ErrorMessage);
            }
            X.Js.AddScript("window.scrollTo(0,0);");
        }
    }
}