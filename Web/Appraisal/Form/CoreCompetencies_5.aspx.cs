﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Entity;
using BLL;
using BLL.BO;
using Utils.Helper;
using Utils.Calendar;


namespace Web.Appraisal.Form
{
    public partial class CoreCompetencies_5 : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

       

        public void Initialise()
        {
            
            List<AppraisalCategory> _ListAppraisalCompetencyCategory = NewHRManager.GetAllAppraisalCompetencyCategory();
            if (_ListAppraisalCompetencyCategory.Any())
            {
                StoregridCompetencies.DataSource = _ListAppraisalCompetencyCategory;
                StoregridCompetencies.DataBind();
            }
            if (!string.IsNullOrEmpty(Request.QueryString["fid"]))
            {
                LoadData(int.Parse(Request.QueryString["fid"]));

                if(NewHRManager.IsAppraisalCompetencyUsedInEmployee(int.Parse(Request.QueryString["fid"])))
                {
                   btnAddCompetencies.Disabled=true;
                   btnRemoveAllCompetencies.Disabled=true;
                   btnRemoveCompetencies.Disabled=true;
                   SetWarning(lblMsg, "You can't add and Remove Competencies as this data has been used in employee from.");
                }
               
            }

            cmbRatingScale.Store[0].DataSource = NewHRManager.GetAllRatingScale();
            cmbRatingScale.Store[0].DataBind();

        }

        protected void LoadData(int ID)
        {
            AppraisalForm _AppraisalForm = NewHRManager.GetAppraisal_GeneralInfoByID(ID);
            if (_AppraisalForm != null)
            {
                if(!string.IsNullOrEmpty(_AppraisalForm.CompentencyName))
                    txtSectionName.Text = _AppraisalForm.CompentencyName;
                if (!string.IsNullOrEmpty(_AppraisalForm.CompentencyDescription))
                    txtEditorDescription.Text = _AppraisalForm.CompentencyDescription;
                if (_AppraisalForm.CompentencyRatingScaleRef_ID != null)
                ExtControlHelper.ComboBoxSetSelected(_AppraisalForm.CompentencyRatingScaleRef_ID.ToString(), cmbRatingScale);
                chkAllowSelfRating.Checked = _AppraisalForm.AllowCompetencySelfRating.Value;
                chkHideBlock.Checked = (_AppraisalForm.HideCompetencyCoreValues == null ? false : _AppraisalForm.HideCompetencyCoreValues.Value);
                
                if (_AppraisalForm.AllowCompetencySelfComment != null)
                {
                    chkAllowSelfComment.Checked = _AppraisalForm.AllowCompetencySelfComment.Value;
                }

                List<AppraisalCategory> _ListAppraisalCompetency = NewHRManager.GetAllAppraisalCompetencyCategory();
                List<AppraisalCategory> _ListAppraisalFormCompetency = NewHRManager.GetAppraisalFromCompetencyCategoryFormatted(ID);

                if (_ListAppraisalFormCompetency.Any())
                {
                    StoreGridAddedCompetencies.DataSource = _ListAppraisalFormCompetency;
                    StoreGridAddedCompetencies.DataBind();


                    List<string> _ListAdded = new List<string>();

                    _ListAdded = (
                       (from _inst in _ListAppraisalFormCompetency
                        select _inst.CategoryID.ToString()).ToList()
                          );

                    List<AppraisalCategory> _RemainingList = null;
                    _RemainingList = _ListAppraisalCompetency.Where(i => !_ListAdded.Contains(i.CategoryID.ToString())).ToList();

                    StoregridCompetencies.DataSource = _RemainingList;
                    StoregridCompetencies.DataBind();


                }

                

            }

        }


        protected void ButtonBack_Click(object sender, DirectEventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["fid"]))
                Response.Redirect("Activities_4.aspx?fid=" + Request.QueryString["fid"]);
        
        }


        protected void lnkBtnDivideWeight_Click(object sender, DirectEventArgs e)
        {
            //int FormID = int.Parse(Request.QueryString["fid"]);
            //List<AppraisalFormCompetency> _ListAppraisalFormCompetency = NewHRManager.GetAppraisalFromCompetency_ByID(FormID);

            ////string SumOfAllWeight = _ListAppraisalFormCompetency.Sum(x => x.Weight).ToString();
            //if (_ListAppraisalFormCompetency.Count() <= 0)
            //{
            //    NewMessage.ShowWarningMessage("Compentency should be saved before applying distribution.");
            //    return;
            //}
            //decimal sum = 100;
            //decimal NowEquallyDividedWeigth = sum / _ListAppraisalFormCompetency.Count();
            //AppraisalFormCompetency _AppraisalFormQuestionnaire = new AppraisalFormCompetency();
            //_AppraisalFormQuestionnaire.AppraisalFormRef_ID = FormID;
            //_AppraisalFormQuestionnaire.Weight = NowEquallyDividedWeigth;
            //Status status = AppraisalManager.DivieCompetencyWeightEqually(_AppraisalFormQuestionnaire);
            //if (status.IsSuccess)
            //{
            //    NewMessage.ShowNormalMessage("weight divided equally.");
            //    LoadData(int.Parse(Request.QueryString["fid"]));
            //}
            //else
            //{
            //    NewMessage.ShowWarningMessage(status.ErrorMessage);
            //}

        }

        protected void btnAddCompetencies_Click(object sender, DirectEventArgs e)
        {
            string entryLineJson = e.ExtraParams["CompetencyList"];

            List<AppraisalCategory> _LineList = JSON.Deserialize<List<AppraisalCategory>>(entryLineJson);
            List<AppraisalCategory> _ListAppraisalCompetency = new List<AppraisalCategory>();
            RowSelectionModel sm = this.gridCompetencies.GetSelectionModel() as RowSelectionModel;
            List<AppraisalCompetency> _ALLAppraisalCompetency = NewHRManager.GetAllAppraisalCompetency();


            foreach (SelectedRow row in sm.SelectedRows)
            {
                AppraisalCategory _AppraisalCompetency = new AppraisalCategory();
                string RowID = row.RecordID;
                _AppraisalCompetency.CategoryID = int.Parse(row.RecordID);

                string NewGuid = Guid.NewGuid().ToString();
                string divid = "'" + "a" + NewGuid + "'";
                string TextWithHTML = "<div onclick=toggle_visibility(" + divid + ") unselectable='on' style='text-align:left;'>" +
                    "<div id=" + "arrow" + NewGuid + " style='width:0px;float:left;color:#2B652A'>►</div><img src='data:image/gif;base64,R0lGODlhAQABAID/AMDAwAAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==' class=' x-tree-elbow-img x-tree-elbow-plus x-tree-expander'><img src='data:image/gif;base64,R0lGODlhAQABAID/AMDAwAAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==' class=' x-tree-icon x-tree-icon-parent '><span class='x-tree-node-text '>" + _LineList.SingleOrDefault(x => x.CategoryID == _AppraisalCompetency.CategoryID).Name + "</span></div>";
                string ChildHtmlItem = "<div style='display:none' id=" + divid + "><ul style='list-style-type: none;'>";
                List<AppraisalCompetency> _AppraisalCompetencyList = _ALLAppraisalCompetency.Where(x => x.CategoryID == _AppraisalCompetency.CategoryID).ToList();
                foreach (var item in _AppraisalCompetencyList)
                {
                    // ChildHtmlItem+= "<li style='padding-left:20px'>&gt;&gt; " + item.Name + "</li>";
                    ChildHtmlItem += "<li><img src='data:image/gif;base64,R0lGODlhAQABAID/AMDAwAAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==' class='x-tree-icon x-tree-icon-leaf '><span class='x-tree-node-text ' style='line-height:20px'>" + item.Name + "</span></li>";
                }
                ChildHtmlItem += "</ul></div>";
                TextWithHTML = TextWithHTML + ChildHtmlItem;
                _AppraisalCompetency.Name = _LineList.SingleOrDefault(x => x.CategoryID == _AppraisalCompetency.CategoryID).Name;
                _AppraisalCompetency.FormattedName = TextWithHTML;
                _ListAppraisalCompetency.Add(_AppraisalCompetency);
            }

            if (_ListAppraisalCompetency.Any())
            {
                StoreGridAddedCompetencies.Add(_ListAppraisalCompetency);
                //StoreGridAddedCompetencies.DataBind();

                List<string> _ListAdded = new List<string>();

                _ListAdded = (
                   (from _inst in _ListAppraisalCompetency
                    select _inst.CategoryID.ToString()).ToList()
                      );

                List<AppraisalCategory> _RemainingList = null;
                _RemainingList = _LineList.Where(i => !_ListAdded.Contains(i.CategoryID.ToString())).ToList();

                sm.ClearSelection();
                sm.SelectedRows.Clear();
                sm.UpdateSelection();

                StoregridCompetencies.DataSource = _RemainingList;
                StoregridCompetencies.DataBind();

            }

        }

        protected void btnRemoveCompetencies_Click(object sender, DirectEventArgs e)
        {

            string entryLineJson = e.ExtraParams["CompetencyListSelected"];

            List<AppraisalCategory> _LineList = JSON.Deserialize<List<AppraisalCategory>>(entryLineJson);
            List<AppraisalCategory> _ListAppraisalCompetency = new List<AppraisalCategory>();
            RowSelectionModel sm = this.GridAddedCompetencies.GetSelectionModel() as RowSelectionModel;


            foreach (SelectedRow row in sm.SelectedRows)
            {
                AppraisalCategory _AppraisalCompetency = new AppraisalCategory();
                string RowID = row.RecordID;
                _AppraisalCompetency.CategoryID = int.Parse(row.RecordID);
                _AppraisalCompetency.Name = _LineList.SingleOrDefault(x => x.CategoryID == _AppraisalCompetency.CategoryID).Name;
                _ListAppraisalCompetency.Add(_AppraisalCompetency);
            }

            if (_ListAppraisalCompetency.Any())
            {
                StoregridCompetencies.Add(_ListAppraisalCompetency);
                //StoreGridAddedCompetencies.DataBind();

                List<string> _ListAdded = new List<string>();

                _ListAdded = (
                   (from _inst in _ListAppraisalCompetency
                    select _inst.CategoryID.ToString()).ToList()
                      );

                List<AppraisalCategory> _RemainingList = null;
                _RemainingList = _LineList.Where(i => !_ListAdded.Contains(i.CategoryID.ToString())).ToList();

                sm.ClearSelection();
                sm.SelectedRows.Clear();
                sm.UpdateSelection();

                StoreGridAddedCompetencies.DataSource = _RemainingList;
                StoreGridAddedCompetencies.DataBind();

            }

        }

        protected void btnRemoveAllCompetencies_Click(object sender, DirectEventArgs e)
        {
            List<AppraisalCompetency> _ListAppraisalCompetency = NewHRManager.GetAllAppraisalCompetency();
            if (_ListAppraisalCompetency.Any())
            {
                StoregridCompetencies.DataSource = _ListAppraisalCompetency;
                StoregridCompetencies.DataBind();
            }

            List<AppraisalForm> _AppraisalFrom = new List<AppraisalForm>();
            StoreGridAddedCompetencies.DataSource = _AppraisalFrom;
            StoreGridAddedCompetencies.DataBind();
            RowSelectionModel sm = this.GridAddedCompetencies.GetSelectionModel() as RowSelectionModel;
            sm.ClearSelection();
            sm.SelectedRows.Clear();
            sm.UpdateSelection();

        }
     
        protected void ButtonNext_Click(object sender, DirectEventArgs e)
        {
            AppraisalForm _AppraisalForm = new AppraisalForm();
             string entryLineJson = e.ExtraParams["CompetencyList"];
            List<AppraisalCategory> _LineList = JSON.Deserialize<List<AppraisalCategory>>(entryLineJson);
            List<AppraisalFormCompetency> _ListAppraisalFromCompetency = new List<AppraisalFormCompetency>();
            List<AppraisalFormCompetencyCategory> catList = new List<AppraisalFormCompetencyCategory>();

            int seq = 0;
            foreach (AppraisalCategory _List in _LineList)
            {
                seq++;
                AppraisalFormCompetency _AppraisalFormCompetency = new AppraisalFormCompetency();
                
                //if(double.Parse(string.IsNullOrEmpty(_List.Weight)?"0":_List.Weight.ToString())<=0)
                //{
                //    SetWarning(lblMsg, "Weight Can't be empty for competency selected.");
                //    X.Js.AddScript("window.scrollTo(0,0);");
                //    return;
                //}
                //if (_List.Weight == "")
                //    _List.Weight = "0";
                
                _AppraisalFormCompetency.CompetencyCatRef_ID = _List.CategoryID;
                _AppraisalFormCompetency.AppraisalFormRef_ID = int.Parse(Request.QueryString["fid"]);
                //_AppraisalFormCompetency.Weight = _List.Weight== null ? 0 :  decimal.Parse(_List.Weight);
                _AppraisalFormCompetency.Sequence = seq;
                _ListAppraisalFromCompetency.Add(_AppraisalFormCompetency);


                AppraisalFormCompetencyCategory cat = new AppraisalFormCompetencyCategory();
                cat.CompetencyCategoryID = _List.CategoryID;
                cat.Name = _List.Name;
                cat.Sequence = seq;
                cat.Weight = _List.Weight;
                catList.Add(cat);

            }

            _AppraisalForm.CompentencyName = txtSectionName.Text.Trim();
            _AppraisalForm.CompentencyDescription = hdnEditorDescription.Text.Trim();
            _AppraisalForm.CompentencyRatingScaleRef_ID = int.Parse(cmbRatingScale.SelectedItem.Value);
            _AppraisalForm.AppraisalFormID = int.Parse(Request.QueryString["fid"]);
            _AppraisalForm.AllowCompetencySelfRating = chkAllowSelfRating.Checked;
            _AppraisalForm.AllowCompetencySelfComment = chkAllowSelfComment.Checked;
            _AppraisalForm.HideCompetencyCoreValues = chkHideBlock.Checked;
            Status respStatus;

            respStatus = NewHRManager.UpdateAppraisalForm_Competencies(_AppraisalForm, _ListAppraisalFromCompetency, catList);
            if (respStatus.IsSuccess)
            {
                if (sender == btnSave)
                    SetMessage(lblMsg, "Appraisal updated.");
                else
                    Response.Redirect("Targets_6.aspx?fid=" + _AppraisalForm.AppraisalFormID);

              
            }
            else
            {
                SetWarning(lblMsg, respStatus.ErrorMessage);
            }
            X.Js.AddScript("window.scrollTo(0,0);");
        }
    }
}