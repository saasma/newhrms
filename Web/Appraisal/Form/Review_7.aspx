﻿<%@ Page Title="Review" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    ValidateRequest="false" AutoEventWireup="true" CodeBehind="Review_7.aspx.cs"
    Inherits="Web.Appraisal.Review_7" %>

<%@ Register Src="~/Controls/HRAppraisalFormWizard.ascx" TagPrefix="ucHRW" TagName="ucHRFormWizard" %>
<%--<%@ Register Src="~/NewHR/UserControls/EmployeeDetailsCtl.ascx" TagName="EmployeeDetailsCtl"
    TagPrefix="uc1" %>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">


        
        var CommandHandler1 = function(command, record){
            <%= hiddenValueRow.ClientID %>.setValue(record.data.QuestionnareID);
                if(command=="Edit")
                {
                    <%= btnEditLevel.ClientID %>.fireEvent('click');
                }
                else
                {
                
                    <%= btnDeleteLevel.ClientID %>.fireEvent('click');
                }

             }
             
                
        Ext.onReady(function () {
            CKEDITOR.config.toolbar = [
               ['Styles', 'Format', 'Font', 'FontSize'],

               ['Bold', 'Italic', 'Underline', 'StrikeThrough', '-', 'Undo', 'Redo', '-', 'Cut', 'Copy', 'Paste', 'Find', 'Replace', '-', 'Outdent', 'Indent', '-'],
    
               ['NumberedList', 'BulletedList', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
               ['Image', 'Table', '-', 'Link', 'TextColor', 'BGColor', 'Maximize']
            ];
        });
        
        function SetEditorValue()
        {
        <%=hdnEditorDescription.ClientID%>.setValue(CKEDITOR.instances.ctl00_ContentPlaceHolder_Main_txtEditorDescription.getData());
        }

         function process(e1,e2,e3,e4,e5)
        {
            var sourceId = e2.records[0].data.QuestionnareID;
            <%=hdnSource.ClientID %>.setValue(sourceId);
            var QuestionnareID = e3.data.QuestionnareID;
            <%=hdnDest.ClientID %>.setValue(QuestionnareID);
            <%=hdnDropMode.ClientID %>.setValue(e4);
//             Ext.net.Notification.show({title:e2.records[0].data.Name,html: e3.data.Name});
            <%=btnChangeDesignationOrder.ClientID %>.fireEvent('click');
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Supervisor Review
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel" style="margin-top: 0px !important; padding-top: 0px !important;">
        <ext:Hidden runat="server" ID="hiddenValue" />
        <ext:Hidden runat="server" ID="hdnDropMode" />
        <ext:Hidden runat="server" ID="hdnSource" />
        <ext:Hidden runat="server" ID="hdnDest" />
        <ext:Hidden runat="server" ID="hdnEditorDescription" />
        <ext:Hidden runat="server" ID="hiddenValueRow" />
        <ext:Button runat="server" Cls="btn btn-primary" Hidden="true" ID="btnEditLevel">
            <DirectEvents>
                <Click OnEvent="btnEditLevel_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:Button>
        <ext:Button runat="server" Hidden="true" ID="btnChangeDesignationOrder" Cls="btn btn-primary">
            <DirectEvents>
                <Click OnEvent="btnChangeOrder_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:Button>
        <ext:Button ID="btnDeleteLevel" runat="server" Hidden="true" Cls="btn btn-primary">
            <DirectEvents>
                <Click OnEvent="btnDeleteLevel_Click">
                    <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete?" />
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:Button>
        <div class="separator bottom">
        </div>
        <div class="innerLR">
            <%--<uc1:EmployeeDetailsCtl Id="EmployeeDetailsCtl1" runat="server" />--%>
            <div>
                <div class="separator bottom">
                </div>
                <ucHRW:ucHRFormWizard Id="ucAppraisalWizard" runat="server" />
                <ext:Label ID="lblMsg" runat="server" />
                <div>
                    <table class="fieldTable firsttdskip">
                        <tr>
                            <td>
                                <ext:Checkbox StyleSpec="margin-top:2px" ID="chkHideBlock" runat="server" BoxLabel="Hide Review Block" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <ext:TextField ID="txtSectionName" runat="server" Width="400" LabelAlign="Top" FieldLabel="Section Name"
                                    LabelSeparator="">
                                </ext:TextField>
                                <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator2" runat="server"
                                    ValidationGroup="SaveFormData" ControlToValidate="txtSectionName" ErrorMessage="Section Name is required." />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="components">
                                    <div class="grids">
                                        <p>
                                            <span>Description</span></p>
                                        <CKEditor:CKEditorControl ExtraPlugins="autogrow" ID="txtEditorDescription" Width="800px"
                                            Height="200px" runat="server" BasePath="~/ckeditor/">
                                        </CKEditor:CKEditorControl>
                                    </div>
                                </div>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Description is required"
                                    Display="None" ControlToValidate="txtEditorDescription" ValidationGroup="SaveFrom"
                                    runat="server">
                                </asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <ext:ComboBox ForceSelection="true" QueryMode="Local" ID="cmbRatingScale" FieldLabel="Rating Scale *"
                                    Hidden="true" runat="server" LabelAlign="Top" DisplayField="Name" Width="180"
                                    LabelSeparator="" ValueField="RatingScaleID">
                                    <Store>
                                        <ext:Store ID="Store1" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="Name" Type="String" />
                                                <ext:ModelField Name="RatingScaleID" Type="String" />
                                            </Fields>
                                        </ext:Store>
                                    </Store>
                                </ext:ComboBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="buttonBlock">
                                    <ext:Button runat="server" ID="btnAddLevel" Cls="btn btn-primary" Text="<i></i>Add Topic">
                                        <DirectEvents>
                                            <Click OnEvent="btnAddLevel_Click">
                                                <EventMask ShowMask="true" />
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <div>
                                    <ext:GridPanel StyleSpec="margin-top:15px;" ID="GridLevels" runat="server" Cls="itemgrid"
                                        EnableDragDrop="true">
                                        <Store>
                                            <ext:Store ID="Store3" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model4" runat="server" IDProperty="QuestionnareID">
                                                        <Fields>
                                                            <ext:ModelField Name="QuestionnareID" Type="String" />
                                                            <ext:ModelField Name="Question" Type="string" />
                                                            <ext:ModelField Name="Weight" Type="string" />
                                                            <ext:ModelField Name="Sequence" Type="string" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:Column ID="Column5" Sortable="false" MenuDisabled="true" runat="server" Text="Topic"
                                                    Width="670" Align="Left" DataIndex="Question" />
                                                <ext:Column Hidden="true" ID="Column6" Sortable="false" MenuDisabled="true" runat="server"
                                                    Text="Weight" Width="100" Align="Left" DataIndex="Weight">
                                                </ext:Column>
                                                <ext:Column ID="Column1" Sortable="false" MenuDisabled="true" runat="server" Text="Order"
                                                    Width="60" Align="Left" DataIndex="Sequence">
                                                </ext:Column>
                                                <ext:CommandColumn ID="CommandColumn1" runat="server" Text="Actions" Align="Center"
                                                    Width="70">
                                                    <Commands>
                                                        <ext:CommandSpacer>
                                                        </ext:CommandSpacer>
                                                        <ext:GridCommand Icon="Pencil" CommandName="Edit" />
                                                        <ext:GridCommand Icon="Decline" CommandName="Delete" />
                                                    </Commands>
                                                    <Listeners>
                                                        <Command Handler="CommandHandler1(command,record);" />
                                                    </Listeners>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <SelectionModel>
                                            <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                                        </SelectionModel>
                                        <View>
                                            <ext:GridView ID="GridView1" runat="server">
                                                <Plugins>
                                                    <ext:GridDragDrop ID="GridDragDrop1" runat="server" DragGroup="ddGroup" DropGroup="ddGroup" />
                                                </Plugins>
                                                <Listeners>
                                                    <Drop Fn="process" />
                                                    <%-- <AfterRender Handler="this.plugins[0].dragZone.getDragText = getDragDropText;" />--%>
                                                    <%--  <Drop Handler="var dropOn = overModel ? ' ' + dropPosition + ' ' + overModel.get('Name') : ' on empty view'; 
                                               Ext.net.Notification.show({title:'Drag from right to left', html:'Dropped ' + data.records[0].get('Name') + dropOn});" />--%>
                                                </Listeners>
                                            </ext:GridView>
                                        </View>
                                    </ext:GridPanel>
                                </div>
                            </td>
                        </tr>
                        <%--<tr>
                        <td>
                            <div class="buttonBlock">
                            <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="lnkBtnDivideWeight" Cls="btnFlat"
                                BaseCls="btnFlat" Text="<i></i>Divide Weight equally">
                                <DirectEvents>
                                    <Click OnEvent="lnkBtnDivideWeight_Click">
                                        <Confirmation ConfirmRequest="true" Message="Confirm divide the weight equally between all Review?" />
                                        <EventMask ShowMask="true" />
                                    </Click>
                                </DirectEvents>
                            </ext:LinkButton>
                        </div>
                        </td>
                    </tr>--%>
                        <tr>
                            <td>
                                <div class="buttonBlock">
                                    <ext:Button runat="server" ID="btnSaveAndFinnishLater" ValidationGroup="SaveFormData"
                                        Cls="btn btn-primary" Text="<i></i>Save">
                                        <DirectEvents>
                                            <Click OnEvent="SaveQuestionnaireForm_Click">
                                                <EventMask ShowMask="true" />
                                            </Click>
                                        </DirectEvents>
                                        <Listeners>
                                            <Click Handler="SetEditorValue();valGroup = 'SaveFormData'; return CheckValidation();">
                                            </Click>
                                        </Listeners>
                                    </ext:Button>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <ext:Window ID="WindowLevel" runat="server" Title="Review Topic" Icon="Application"
                Width="640" Height="225" BodyPadding="5" Hidden="true" Modal="true">
                <Content>
                    <table class="fieldTable">
                        <tr>
                            <td>
                                <ext:TextArea ID="txtQuestion" LabelSeparator="" runat="server" FieldLabel="Review Topic *"
                                    Width="600" LabelAlign="Top">
                                </ext:TextArea>
                                <asp:RequiredFieldValidator Display="None" ID="valcmbFaculty" runat="server" ValidationGroup="SaveUpdateLevel"
                                    ControlToValidate="txtQuestion" ErrorMessage="Question is required." />
                            </td>
                        </tr>
                        <tr style="display: none">
                            <td>
                                <ext:NumberField ID="txtWeight" LabelSeparator="" runat="server" FieldLabel="Weight *"
                                    Width="400" LabelAlign="Top">
                                </ext:NumberField>
                                <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator1" runat="server"
                                    ValidationGroup="SaveUpdateLevel" ControlToValidate="txtWeight" ErrorMessage="Weight is required." />
                            </td>
                        </tr>
                        <tr>
                            <td valign="bottom" colspan="2">
                                <div class="popupButtonDiv">
                                    <ext:Button runat="server" ID="btnLevelSaveUpdate" ValidationGroup="SaveUpdateLevel"
                                        Cls="btn btn-primary" Text="<i></i>Save">
                                        <DirectEvents>
                                            <Click OnEvent="btnLevelSaveUpdate_Click">
                                                <EventMask ShowMask="true" />
                                            </Click>
                                        </DirectEvents>
                                        <Listeners>
                                            <Click Handler="valGroup = 'SaveUpdateLevel'; return CheckValidation();">
                                            </Click>
                                        </Listeners>
                                    </ext:Button>
                                    <div class="btnFlatOr">
                                        or</div>
                                    <ext:LinkButton runat="server" StyleSpec="padding:0px;" Cls="btnFlatLeftGap" ID="LinkButton1"
                                        Text="<i></i>Cancel" runat="server">
                                        <Listeners>
                                            <Click Handler="#{WindowLevel}.hide();">
                                            </Click>
                                        </Listeners>
                                    </ext:LinkButton>
                                </div>
                            </td>
                        </tr>
                    </table>
                </Content>
            </ext:Window>
        </div>
    </div>
    <br />
</asp:Content>
