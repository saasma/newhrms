﻿<%@ Page Title="Activities" Language="C#" MasterPageFile="~/Master/NewDetails.master"
    ValidateRequest="false" AutoEventWireup="true" CodeBehind="QualificationExpirenceHistory.aspx.cs"
    Inherits="Web.Appraisal.Form.QualificationExpirenceHistory" %>

<%@ Register Src="~/Controls/HRAppraisalFormWizard.ascx" TagPrefix="ucHRW" TagName="ucHRFormWizard" %>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script>
        Ext.onReady(function () {
           CKEDITOR.config.toolbar = [
               ['Styles', 'Format', 'Font', 'FontSize'],

               ['Bold', 'Italic', 'Underline', 'StrikeThrough', '-', 'Undo', 'Redo', '-', 'Cut', 'Copy', 'Paste', 'Find', 'Replace', '-', 'Outdent', 'Indent', '-'],
    
               ['NumberedList', 'BulletedList', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
               ['Image', 'Table', '-', 'Link', 'TextColor', 'BGColor', 'Maximize']
            ];

        });

          function SetEditorValue()
        {
        <%=hdnEditorDescription.ClientID%>.setValue(CKEDITOR.instances.ctl00_ContentPlaceHolder_Main_txtEditorDescription.getData());
        }

    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Activity Summary
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel" style="margin-top: 0px !important; padding-top: 0px !important;">
        <ext:Hidden runat="server" ID="hdnEditorDescription" />
        <div class="innerLR">
            <div class="separator bottom">
            </div>
            <ucHRW:ucHRFormWizard ID="ucAppraisalWizard" runat="server" />
            <ext:Label ID="lblMsg" runat="server" />
            <table class="fieldTable firsttdskip">
                <td>
                    <ext:Checkbox StyleSpec="margin-top:2px" ID="chkHideBlock" runat="server" BoxLabel="Hide This Block" />
                </td>
                <tr>
                    <td>
                        <ext:TextField ID="txtSectionName" FieldLabel="Section Name *" runat="server" LabelAlign="Top"
                            Width="400" LabelSeparator="">
                        </ext:TextField>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Section Name is required"
                            Display="None" ControlToValidate="txtSectionName" ValidationGroup="SaveFrom"
                            runat="server">
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="components">
                            <div class="grids">
                                <p>
                                   <span> Descriptions</span></p>
                                <CKEditor:CKEditorControl ExtraPlugins="autogrow" ID="txtEditorDescription" Width="800px"
                                    Height="200px" runat="server" BasePath="~/ckeditor/">
                                </CKEditor:CKEditorControl>
                            </div>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Description is required"
                            Display="None" ControlToValidate="txtEditorDescription" ValidationGroup="SaveFrom"
                            runat="server">
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                       <%-- <table class="fieldTable firsttdskip">
                            <tr>
                                <td>
                                    Qualification and Work Experience</td>
                              
                                <td style="padding-left: 10px">
                                    <ext:ComboBox ForceSelection="true" QueryMode="Local" ID="cmbRatingScaleQualification" 
                                        runat="server"  DisplayField="Name" Width="180" LabelSeparator="" EmptyText="Rating Scale"
                                        ValueField="RatingScaleID">
                                        <Store>
                                            <ext:Store ID="Store1" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="Name" Type="String" />
                                                    <ext:ModelField Name="RatingScaleID" Type="String" />
                                                </Fields>
                                            </ext:Store>
                                        </Store>
                                    </ext:ComboBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Rating Scale for  Qualification and Work Experience  is required"
                                        Display="None" ControlToValidate="cmbRatingScaleQualification" ValidationGroup="SaveFrom"
                                        runat="server">
                                    </asp:RequiredFieldValidator>
                                  
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Transfer History
                                </td>
                                <td style="padding-left: 10px">
                                    <ext:ComboBox ForceSelection="true" QueryMode="Local" ID="cmbRatingScaleTransfer" EmptyText="Rating Scale"
                                        runat="server"  DisplayField="Name" Width="180" LabelSeparator=""
                                        ValueField="RatingScaleID">
                                        <Store>
                                            <ext:Store ID="Store2" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="Name" Type="String" />
                                                    <ext:ModelField Name="RatingScaleID" Type="String" />
                                                </Fields>
                                            </ext:Store>
                                        </Store>
                                    </ext:ComboBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="Rating Scale for  Transfer History is required"
                                        Display="None" ControlToValidate="cmbRatingScaleTransfer" ValidationGroup="SaveFrom"
                                        runat="server">
                                    </asp:RequiredFieldValidator>
                                  
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Work History
                                </td>
                                <td style="padding-left: 10px">
                                    <ext:ComboBox ForceSelection="true" QueryMode="Local" ID="cmbRatingScaleWorkExp" EmptyText="Rating Scale"
                                        runat="server"  DisplayField="Name" Width="180" LabelSeparator=""
                                        ValueField="RatingScaleID">
                                        <Store>
                                            <ext:Store ID="Store3" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="Name" Type="String" />
                                                    <ext:ModelField Name="RatingScaleID" Type="String" />
                                                </Fields>
                                            </ext:Store>
                                        </Store>
                                    </ext:ComboBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ErrorMessage="Rating Scale for Working Experience required"
                                        Display="None" ControlToValidate="cmbRatingScaleWorkExp" ValidationGroup="SaveFrom"
                                        runat="server">
                                    </asp:RequiredFieldValidator>
                                   
                                </td>
                            </tr>
                        </table>
                      --%>
                    </td>
                </tr>
            
            </table>
            <div class="buttonBlock" style="padding-top: 50px">
                <table>
                    <tr>
                        <td>
                            <ext:Button ID="btnSave" Cls="btn btn-primary" runat="server" Text="&nbsp;&nbsp;Save&nbsp;&nbsp;"
                                ValidationGroup="InsertUpdate">
                                <DirectEvents>
                                    <Click OnEvent="ButtonNext_Click">
                                        <EventMask ShowMask="true" />
                                    </Click>
                                </DirectEvents>
                                <Listeners>
                                    <Click Handler="SetEditorValue();valGroup = 'SaveFrom'; return CheckValidation();">
                                    </Click>
                                </Listeners>
                            </ext:Button>
                        </td>
                        <td>
                            &nbsp;&nbsp;&nbsp;
                        </td>
                        <td>
                            <ext:Button ID="btnNext" Cls="btn btn-primary" runat="server" Text="&nbsp;&nbsp;Save and  Next&nbsp;&nbsp;"
                                ValidationGroup="InsertUpdate">
                                <DirectEvents>
                                    <Click OnEvent="ButtonNext_Click">
                                        <EventMask ShowMask="true" />
                                    </Click>
                                </DirectEvents>
                                <Listeners>
                                    <Click Handler="SetEditorValue();valGroup = 'SaveFrom'; return CheckValidation();">
                                    </Click>
                                </Listeners>
                            </ext:Button>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
