﻿<%@ Page Title="Signatures" Language="C#" MasterPageFile="~/Master/NewDetails.master"
    ValidateRequest="false" AutoEventWireup="true" CodeBehind="Signatures_9.aspx.cs"
    Inherits="Web.Appraisal.Form.Signatures_9" %>

<%@ Register Src="~/Controls/HRAppraisalFormWizard.ascx" TagPrefix="ucHRW" TagName="ucHRFormWizard" %>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script>
        Ext.onReady(function () {
             CKEDITOR.config.toolbar = [
               ['Styles', 'Format', 'Font', 'FontSize'],

               ['Bold', 'Italic', 'Underline', 'StrikeThrough', '-', 'Undo', 'Redo', '-', 'Cut', 'Copy', 'Paste', 'Find', 'Replace', '-', 'Outdent', 'Indent', '-'],
    
               ['NumberedList', 'BulletedList', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
               ['Image', 'Table', '-', 'Link', 'TextColor', 'BGColor', 'Maximize']
            ];
        });

          function SetEditorValue()
        {
        <%=hdnEditorDescription.ClientID%>.setValue(CKEDITOR.instances.ctl00_ContentPlaceHolder_Main_txtEditorDescription.getData());
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Signature
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel" style="margin-top: 0px !important; padding-top: 0px !important;">
        <ext:Hidden runat="server" ID="hdnEditorDescription" />
        <div class="innerLR">
            <div class="separator bottom">
            </div>
            <ucHRW:ucHRFormWizard Id="ucAppraisalWizard" runat="server" />
            <ext:Label ID="lblMsg" runat="server" />
            <table class="fieldTable firsttdskip">
                <tr>
                    <td>
                        <ext:TextField ID="txtSectionName" FieldLabel="Section Name *" runat="server" LabelAlign="Top"
                            Width="400" LabelSeparator="">
                        </ext:TextField>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Section Name is required"
                            Display="None" ControlToValidate="txtSectionName" ValidationGroup="SaveFrom"
                            runat="server">
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="components">
                            <div class="grids">
                                <p>
                                    Description</span></p>
                                <CKEditor:CKEditorControl ExtraPlugins="autogrow" ID="txtEditorDescription" Width="800px"
                                    Height="200px" runat="server" BasePath="~/ckeditor/">
                                </CKEditor:CKEditorControl>
                            </div>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Description is required"
                            Display="None" ControlToValidate="txtEditorDescription" ValidationGroup="SaveFrom"
                            runat="server">
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
            </table>
            <div class="buttonBlock" style="padding-top: 50px">
                <div style="float: left">
                    <ext:Button ID="btnNext" Cls="btn btn-primary" runat="server" Text="&nbsp;&nbsp;Save&nbsp;&nbsp;"
                        ValidationGroup="InsertUpdate">
                        <DirectEvents>
                            <Click OnEvent="ButtonNext_Click">
                                <EventMask ShowMask="true" />
                            </Click>
                        </DirectEvents>
                        <Listeners>
                            <Click Handler="SetEditorValue();valGroup = 'SaveFrom'; return CheckValidation();">
                            </Click>
                        </Listeners>
                    </ext:Button>
                </div>
            </div>
        </div>
        </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
