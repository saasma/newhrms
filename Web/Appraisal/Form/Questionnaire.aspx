﻿<%@ Page Title="Questionnaire" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    ValidateRequest="false" AutoEventWireup="true" CodeBehind="Questionnaire.aspx.cs"
    Inherits="Web.Appraisal.Questionnaire" %>

<%@ Register Src="~/Controls/HRAppraisalFormWizard.ascx" TagPrefix="ucHRW" TagName="ucHRFormWizard" %>
<%--<%@ Register Src="~/NewHR/UserControls/EmployeeDetailsCtl.ascx" TagName="EmployeeDetailsCtl"
    TagPrefix="uc1" %>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        Ext.onReady(function () {
            CKEDITOR.config.toolbar = [
               ['Styles', 'Format', 'Font', 'FontSize'],

               ['Bold', 'Italic', 'Underline', 'StrikeThrough', '-', 'Undo', 'Redo', '-', 'Cut', 'Copy', 'Paste', 'Find', 'Replace', '-', 'Outdent', 'Indent', '-'],
    
               ['NumberedList', 'BulletedList', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
               ['Image', 'Table', '-', 'Link', 'TextColor', 'BGColor', 'Maximize']
            ];
        });
        
        var prepareToolbar = function (grid, toolbar, rowIndex, record) {

            if(record.data.IsCategory)
            {
            }
            else{
                toolbar.hide();
            }

        
        };

        var CommandHandler1 = function(command, record){
            <%= hiddenValueRow.ClientID %>.setValue(record.data.QuestionnareID);
                
            if(command=="Import")
            {
                if(record.data.IsCategory)
                {
                    employeePopup("id=" + record.data.QuestionnareID);
                }
            }
            else
            {

                if(command=="Edit")
                {
                    if(record.data.IsCategory)
                        <%= btnGroupEdit.ClientID %>.fireEvent('click');
                        else
                            <%= btnEditLevel.ClientID %>.fireEvent('click');
                    }
                    else
                    {
                        if(record.data.IsCategory)
                            <%= btnDeleteCategory.ClientID %>.fireEvent('click');
                        else
                            <%= btnDeleteLevel.ClientID %>.fireEvent('click');
                    }
                }
        }
      

            function processQuestionnare(e1,e2,e3,e4,e5)
            {
                var sourceId = e2.records[0].data.QuestionnareID;
                var CategoryID = e2.records[0].data.CategoryID;
                <%=hdnCategoryID.ClientID %>.setValue(CategoryID);
             <%=hdnSource.ClientID %>.setValue(sourceId);
             var QuestionnareID = e3.data.QuestionnareID;
             <%=hdnDest.ClientID %>.setValue(QuestionnareID);
             <%=hdnDropMode.ClientID %>.setValue(e4);
             //             Ext.net.Notification.show({title:e2.records[0].data.Name,html: e3.data.Name});
             <%=btnChageQuestionOrder.ClientID %>.fireEvent('click');
         }

         function SetEditorValue()
         {
             <%=hdnEditorDescription.ClientID%>.setValue(CKEDITOR.instances.ctl00_ContentPlaceHolder_Main_txtEditorDescription.getData());
        }

        function refreshWindow()
        {
            <%=btnLoadQuestion.ClientID %>.fireEvent('click');
        }
        
        var afterEdit = function (editor, e) {
            e.record.commit();
        }

        var BeforeEditHandler = function(e1,e) {

           

            //if(e.record.data.CategoryName=="")
            //{
            //    e.cancel = true;
            //    return;
            //}
            
        }

        var renderSeq = function(value)
        {
            if(parseFloat(value)==0)
                return "";
            return value;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>Questionnaire
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel" style="margin-top: 0px !important; padding-top: 0px !important;">
        <ext:Hidden runat="server" ID="hiddenValue" />
        <ext:Hidden runat="server" ID="hdnEditorDescription" />
        <ext:Hidden runat="server" ID="hdnDropMode" />
        <ext:Hidden runat="server" ID="hdnSource" />
        <ext:Hidden runat="server" ID="hdnDest" />
        <ext:Hidden runat="server" ID="hdnCategoryID" />
        <ext:Hidden runat="server" ID="hiddenValueRow" />
        <ext:Button Cls="btn btn-primary" runat="server" Hidden="true" ID="btnLoadQuestion">
            <DirectEvents>
                <Click OnEvent="btnLoadQuestion_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:Button>
        <ext:Button runat="server" Hidden="true" ID="btnEditLevel" Cls="btn btn-primary">
            <DirectEvents>
                <Click OnEvent="btnEditLevel_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:Button>
        <ext:Button Cls="btn btn-primary" runat="server" Hidden="true" ID="btnGroupEdit">
            <DirectEvents>
                <Click OnEvent="btnGroupEdit_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:Button>
        <ext:Button Cls="btn btn-primary" ID="btnDeleteLevel" runat="server" Hidden="true">
            <DirectEvents>
                <Click OnEvent="btnDeleteLevel_Click">
                    <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the Question?" />
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:Button>
        <ext:Button Cls="btn btn-primary" ID="btnDeleteCategory" runat="server" Hidden="true">
            <DirectEvents>
                <Click OnEvent="btnDeleteCategory_Click">
                    <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the Category?" />
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:Button>
        <ext:Button Cls="btn btn-primary" runat="server" Hidden="true" ID="btnChageQuestionOrder"
            runat="server">
            <DirectEvents>
                <Click OnEvent="btnChangeOrder_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:Button>
        <div class="separator bottom">
        </div>
        <div class="innerLR">
            <div class="separator bottom">
            </div>
            <ucHRW:ucHRFormWizard ID="ucAppraisalWizard" runat="server" />
            <ext:Label ID="lblMsg" runat="server" />
            <table class="fieldTable firsttdskip">
                <tr>
                    <td>
                        <ext:TextField ID="txtSectionName" runat="server" Width="400" LabelAlign="Top" FieldLabel="Section Name">
                        </ext:TextField>
                        <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator2" runat="server"
                            ValidationGroup="SaveFormData" ControlToValidate="txtSectionName" ErrorMessage="Section Name is required." />
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="components">
                            <div class="grids">
                                <p>
                                    Description
                                </p>
                                <CKEditor:CKEditorControl ExtraPlugins="autogrow" ID="txtEditorDescription" Width="800px"
                                    Height="200px" runat="server" BasePath="~/ckeditor/">
                                </CKEditor:CKEditorControl>
                            </div>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ErrorMessage="Description is required"
                            Display="None" ControlToValidate="txtEditorDescription" ValidationGroup="SaveFrom"
                            runat="server">
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>

                        <ext:ComboBox ForceSelection="true" QueryMode="Local" ID="cmbMarkingType" FieldLabel="Marking Method *"
                            runat="server" LabelAlign="Top" Width="300" LabelSeparator="">
                            <Items>

                                <ext:ListItem Text="Rating Scale" Value="1">
                                </ext:ListItem>
                                <%--Civil like client--%>
                                <ext:ListItem Text="Marks" Value="2">
                                </ext:ListItem>
                                <%--Prabhu like client--%>
                                <ext:ListItem Text="Percent with each Question weightage" Value="3">
                                </ext:ListItem>
                            </Items>
                            <SelectedItems>
                                <ext:ListItem Index="0" />
                            </SelectedItems>

                        </ext:ComboBox>

                    </td>

                </tr>
                <tr>
                    <td>
                        <ext:ComboBox ForceSelection="true" QueryMode="Local" ID="cmbRatingScale" FieldLabel="Rating Scale *"
                            runat="server" LabelAlign="Top" DisplayField="Name" Width="300" LabelSeparator=""
                            ValueField="RatingScaleID">
                            <Store>
                                <ext:Store ID="Store1" runat="server">
                                    <Fields>
                                        <ext:ModelField Name="Name" Type="String" />
                                        <ext:ModelField Name="RatingScaleID" Type="String" />
                                    </Fields>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator6" runat="server"
                            ValidationGroup="SaveFormData" ControlToValidate="cmbRatingScale" ErrorMessage="Rating scale is required." />
                        <table>
                            <tr>
                                <td>
                                    <ext:Checkbox StyleSpec="margin-top:10px" ID="chkAllowSelfRating" runat="server"
                                        BoxLabel="Allow Self Rating" />
                                    <ext:Checkbox StyleSpec="margin-top:2px" ID="chkAllowSelfComment" runat="server"
                                        BoxLabel="Allow Self Comment" />
                                </td>
                                <td style="padding-left: 10px;">
                                    <ext:Checkbox StyleSpec="margin-top:10px" ID="chkSupRating" runat="server" BoxLabel="Allow Supervisor Rating" />
                                    <ext:Checkbox StyleSpec="margin-top:2px" ID="chkSupComment" runat="server" BoxLabel="Allow Supervisor Comment" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="buttonBlock" style="float: left">
                            <ext:Button Cls="btn btn-primary" runat="server" ID="btnAddLevel" Text="<i></i>Add New Question">
                                <DirectEvents>
                                    <Click OnEvent="btnAddLevel_Click">
                                        <EventMask ShowMask="true" />
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </div>
                        <div class="buttonBlock" style="margin-left: 50px; float: left">
                            <ext:Button Cls="btn btn-primary" runat="server" ID="btnAddCategory" Text="<i></i>Add New Category">
                                <DirectEvents>
                                    <Click OnEvent="btnAddCategory_Click">
                                        <EventMask ShowMask="true" />
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </div>
                        <div style="float: left; margin-left: 389px; padding-top: 30px; display: none">
                            <ext:Button Cls="btn btn-primary" runat="server" ID="lnkBtnDivideWeight" Text="<i></i>Distribute Weight">
                                <DirectEvents>
                                    <Click OnEvent="lnkBtnDivideWeight_Click">
                                        <Confirmation ConfirmRequest="true" Message="Confirm divide the weight equally between all questions?" />
                                        <EventMask ShowMask="true" />
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td></td>
                </tr>
            </table>
            <ext:GridPanel StyleSpec="margin-top:15px;" ID="GridLevels" runat="server" Cls="itemgrid"
                EnableDragDrop="true">
                <Store>
                    <ext:Store ID="Store3" runat="server">
                        <Model>
                            <ext:Model ID="Model4" runat="server">
                                <Fields>
                                    <ext:ModelField Name="SN" Type="String" />
                                    <ext:ModelField Name="QuestionnareID" Type="String" />
                                    <ext:ModelField Name="Question" Type="string" />
                                    <ext:ModelField Name="Weight" Type="string" />
                                    <ext:ModelField Name="Sequence" Type="string" />
                                    <ext:ModelField Name="Description" Type="string" />
                                    <ext:ModelField Name="CategoryName" Type="string">
                                    </ext:ModelField>
                                    <ext:ModelField Name="CategoryID" Type="string" />
                                    <ext:ModelField Name="IsCategory" Type="Boolean" />
                                </Fields>
                            </ext:Model>
                        </Model>
                        <%--<Groupers>
                                        <ext:DataSorter Property="CategoryName">
                                            <Transform Fn="myGrouper" />
                                        </ext:DataSorter>
                                    </Groupers>--%>
                    </ext:Store>
                </Store>
                <Plugins>
                    <ext:CellEditing ID="CellEditing1" ClicksToEdit="1" runat="server">
                        <Listeners>
                            <Edit Fn="afterEdit" />
                            <BeforeEdit Fn="BeforeEditHandler" />
                        </Listeners>
                    </ext:CellEditing>
                </Plugins>
                <ColumnModel>
                    <Columns>
                        <ext:Column ID="Column3" Sortable="false" MenuDisabled="true" runat="server" Text="SN"
                            Width="50" Align="Left" DataIndex="SN" />
                        <ext:Column ID="Column1" Sortable="false" MenuDisabled="true" runat="server" Text="Category"
                            Width="170" Align="Left" DataIndex="CategoryName" />
                        <ext:Column ID="Column5" Sortable="false" MenuDisabled="true" runat="server" Text="Question"
                            Width="500" Align="Left" DataIndex="Question" />
                        <ext:Column ID="Column4" Sortable="false" MenuDisabled="true" runat="server" Text="Description"
                            Width="200" Align="Left" DataIndex="Description" />
                        <ext:Column ID="Column6" Sortable="false" MenuDisabled="true" runat="server" Text="Weight"
                            Width="55" Align="Right" DataIndex="Weight">
                            <Editor>
                                <ext:TextField ID="TextField1" MaskRe="[0-9]|\.|%" runat="server" MinValue="0">
                                </ext:TextField>
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="Column2" Sortable="false" MenuDisabled="true" runat="server" Text="Order"
                            Width="55" Align="Left" DataIndex="Sequence">
                            <Renderer Fn="renderSeq" />
                        </ext:Column>
                        <ext:CommandColumn ID="CommandColumn1" runat="server" Text="Actions" Align="Center"
                            Width="110">
                            <GroupCommands>
                                <ext:GridCommand Text="<i></i>" Cls="btn-action glyphicons pencil btn-success" CommandName="GroupEdit" />
                            </GroupCommands>
                            <Commands>
                                <ext:CommandSpacer>
                                </ext:CommandSpacer>
                                <ext:GridCommand Cls="editGridButton" ToolTip-Text="Edit" Text="<i class='fa fa-pencil'></i>"
                                    CommandName="Edit" />
                                <ext:GridCommand Cls="editGridButton" ToolTip-Text="Delete" Text="<i class='fa fa-trash-o'></i>"
                                    CommandName="Delete" />
                                <ext:GridCommand Icon="PageWhiteDatabase" CommandName="Import" />
                            </Commands>
                            <Listeners>
                                <Command Handler="CommandHandler1(command,record);" />
                                <%-- <GroupCommand Fn="groupCommandHandler" />--%>
                            </Listeners>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
                <SelectionModel>
                    <ext:CellSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                </SelectionModel>
                <View>
                    <ext:GridView ID="GridView1" runat="server">
                        <Plugins>
                            <ext:GridDragDrop ID="GridDragDrop1" runat="server" DragGroup="ddGroup" DropGroup="ddGroup" />
                        </Plugins>
                        <Listeners>
                            <Drop Fn="processQuestionnare" />
                            <%-- <AfterRender Handler="this.plugins[0].dragZone.getDragText = getDragDropText;" />--%>
                            <%--  <Drop Handler="var dropOn = overModel ? ' ' + dropPosition + ' ' + overModel.get('Name') : ' on empty view'; 
                                               Ext.net.Notification.show({title:'Drag from right to left', html:'Dropped ' + data.records[0].get('Name') + dropOn});" />--%>
                        </Listeners>
                    </ext:GridView>
                </View>
            </ext:GridPanel>
            <div class="buttonBlock">
                <ext:Button Cls="btn btn-primary" runat="server" ID="btmSave" ValidationGroup="SaveFormData"
                    Text="<i></i>Save">
                    <DirectEvents>
                        <Click OnEvent="SaveQuestionnaireForm_Click">
                            <EventMask ShowMask="true" />
                            <ExtraParams>
                                <ext:Parameter Name="QuestionList" Value="Ext.encode(#{GridLevels}.getRowsValues({selectedOnly : false}))"
                                    Mode="Raw" />
                            </ExtraParams>
                        </Click>
                    </DirectEvents>
                    <Listeners>
                        <Click Handler="SetEditorValue();valGroup = 'SaveFormData'; return CheckValidation();">
                        </Click>
                    </Listeners>
                </ext:Button>
            </div>
            <ext:Window ID="WindowLevel" runat="server" Title="Add/Edit Question" Icon="Application"
                Width="640" Height="450" BodyPadding="5" Hidden="true" Modal="true">
                <Content>
                    <table class="fieldTable">
                        <tr>
                            <td>
                                <ext:TextArea ID="txtQuestion" LabelSeparator="" runat="server" FieldLabel="Name *"
                                    Width="600" LabelAlign="Top">
                                </ext:TextArea>
                                <asp:RequiredFieldValidator Display="None" ID="valcmbFaculty" runat="server" ValidationGroup="SaveUpdateLevel"
                                    ControlToValidate="txtQuestion" ErrorMessage="Question is required." />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <ext:TextArea ID="txtDescription" LabelSeparator="" runat="server" FieldLabel="Description"
                                    Width="600" LabelAlign="Top">
                                </ext:TextArea>
                                Use &lt;br&gt; for line separation 
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <ext:ComboBox ForceSelection="true" QueryMode="Local" ID="cmbQuestCategory" FieldLabel="Category"
                                    runat="server" LabelAlign="Top" DisplayField="Name" Width="176" LabelSeparator=""
                                    ValueField="CategoryID">
                                    <Store>
                                        <ext:Store ID="storeQuestCategory" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="CategoryID" Type="String" />
                                                <ext:ModelField Name="Name" />
                                            </Fields>
                                        </ext:Store>
                                    </Store>
                                </ext:ComboBox>
                                <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator3" runat="server"
                                    ValidationGroup="SaveUpdateLevel" ControlToValidate="cmbQuestCategory" ErrorMessage="Category is required." />
                            </td>
                        </tr>
                        <tr>
                            <td valign="bottom" colspan="2">
                                <div class="popupButtonDiv">
                                    <ext:Button Cls="btn btn-primary" runat="server" ID="btnLevelSaveUpdate" ValidationGroup="SaveUpdateLevel"
                                        Text="<i></i>Save">
                                        <DirectEvents>
                                            <Click OnEvent="btnLevelSaveUpdate_Click">
                                                <EventMask ShowMask="true" />
                                            </Click>
                                        </DirectEvents>
                                        <Listeners>
                                            <Click Handler="valGroup = 'SaveUpdateLevel'; return CheckValidation();">
                                            </Click>
                                        </Listeners>
                                    </ext:Button>
                                    <div class="btnFlatOr">
                                        or
                                    </div>
                                    <ext:LinkButton runat="server" StyleSpec="padding:0px;" Cls="btnFlatLeftGap" ID="LinkButton1"
                                        Text="<i></i>Cancel" runat="server">
                                        <Listeners>
                                            <Click Handler="#{WindowLevel}.hide();">
                                            </Click>
                                        </Listeners>
                                    </ext:LinkButton>
                                </div>
                            </td>
                        </tr>
                    </table>
                </Content>
            </ext:Window>
            <ext:Window ID="WindowCategory" runat="server" Title="Add/Edit Category" Icon="Application"
                Width="480" Height="250" BodyPadding="5" Hidden="true" Modal="true">
                <Content>
                    <table class="fieldTable">
                        <tr>
                            <td>
                                <ext:TextField ID="txtCategoryName" LabelSeparator="" runat="server" FieldLabel="Name *"
                                    Width="400" LabelAlign="Top">
                                </ext:TextField>
                                <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator4" runat="server"
                                    ValidationGroup="SaveUpdateCategory" ControlToValidate="txtCategoryName" ErrorMessage="Category Name is required." />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <ext:NumberField ID="txtCategorySequence" MinValue="1" LabelSeparator="" runat="server"
                                    FieldLabel="Sequence *" Width="176" LabelAlign="Top">
                                </ext:NumberField>
                                <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator5" runat="server"
                                    ValidationGroup="SaveUpdateCategory" ControlToValidate="txtCategorySequence"
                                    ErrorMessage="Sequence is required." />
                            </td>
                        </tr>
                        <tr>
                            <td valign="bottom" colspan="2">
                                <div class="popupButtonDiv">
                                    <ext:Button Cls="btn btn-primary" runat="server" ID="LinkButton2" ValidationGroup="SaveUpdateCategory"
                                        Text="<i></i>Save">
                                        <DirectEvents>
                                            <Click OnEvent="btnCategorySaveUpdate_Click">
                                                <EventMask ShowMask="true" />
                                            </Click>
                                        </DirectEvents>
                                        <Listeners>
                                            <Click Handler="valGroup = 'SaveUpdateCategory'; return CheckValidation();">
                                            </Click>
                                        </Listeners>
                                    </ext:Button>
                                    <div class="btnFlatOr">
                                        or
                                    </div>
                                    <ext:LinkButton runat="server" StyleSpec="padding:0px;" Cls="btnFlatLeftGap" ID="LinkButton5"
                                        Text="<i></i>Cancel">
                                        <Listeners>
                                            <Click Handler="#{WindowCategory}.hide();">
                                            </Click>
                                        </Listeners>
                                    </ext:LinkButton>
                                </div>
                            </td>
                        </tr>
                    </table>
                </Content>
            </ext:Window>
        </div>
    </div>
    <br />
</asp:Content>
