﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Entity;
using BLL;
using BLL.BO;
using Utils.Helper;
using Utils.Calendar;


namespace Web.Appraisal.Form
{
    public partial class QualificationExpirenceHistory : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();

            }
        }

        public void Initialise()
        {
            List<AppraisalRatingScale> dbListRating = NewHRManager.GetAllRatingScale();
            //cmbRatingScaleQualification.Store[0].DataSource = dbListRating;
            //cmbRatingScaleQualification.Store[0].DataBind();

            //cmbRatingScaleWorkExp.Store[0].DataSource = dbListRating;
            //cmbRatingScaleWorkExp.Store[0].DataBind();

            //cmbRatingScaleTransfer.Store[0].DataSource = dbListRating;
            //cmbRatingScaleTransfer.Store[0].DataBind();

            if (!string.IsNullOrEmpty(Request.QueryString["fid"]))
            {
                LoadData(int.Parse(Request.QueryString["fid"]));
            }
        }

        protected void LoadData(int ID)
        {
            AppraisalForm _AppraisalForm = NewHRManager.GetAppraisal_GeneralInfoByID(ID);
            if (_AppraisalForm != null)
            {
                if (!string.IsNullOrEmpty(_AppraisalForm.HistoryName))
                    txtSectionName.Text = _AppraisalForm.HistoryName;

                if (!string.IsNullOrEmpty(_AppraisalForm.HistoryDescription))
                    txtEditorDescription.Text = _AppraisalForm.HistoryDescription;

                //if (_AppraisalForm.HistoryEducationRatingScaleRef_ID != null)
                //    ExtControlHelper.ComboBoxSetSelected(_AppraisalForm.HistoryEducationRatingScaleRef_ID.ToString(), cmbRatingScaleQualification);

                //if (_AppraisalForm.TransferRatingScaleRef_ID != null)
                //    ExtControlHelper.ComboBoxSetSelected(_AppraisalForm.TransferRatingScaleRef_ID.ToString(), cmbRatingScaleTransfer);

                //if (_AppraisalForm.WorkHistoryRatingScaleRef_ID != null)
                //    ExtControlHelper.ComboBoxSetSelected(_AppraisalForm.WorkHistoryRatingScaleRef_ID.ToString(), cmbRatingScaleWorkExp);

                if (_AppraisalForm.HideHistorySection)
                    chkHideBlock.Checked = _AppraisalForm.HideHistorySection;
            }
        }

        protected void ButtonBack_Click(object sender, DirectEventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["fid"]))
                Response.Redirect("Objectives_3.aspx?fid=" + Request.QueryString["fid"]);
        
        }

        protected void ButtonNext_Click(object sender, DirectEventArgs e)
        {
            AppraisalForm _AppraisalForm = new AppraisalForm();

            _AppraisalForm.HistoryName = txtSectionName.Text.Trim();
            _AppraisalForm.HistoryDescription = hdnEditorDescription.Text.Trim();
            //if(!string.IsNullOrEmpty(cmbRatingScaleQualification.SelectedItem.Value))
            //    _AppraisalForm.HistoryEducationRatingScaleRef_ID = int.Parse(cmbRatingScaleQualification.SelectedItem.Value);

            //if (!string.IsNullOrEmpty(cmbRatingScaleTransfer.SelectedItem.Value))
            //    _AppraisalForm.TransferRatingScaleRef_ID = int.Parse(cmbRatingScaleTransfer.SelectedItem.Value);

            //if (!string.IsNullOrEmpty(cmbRatingScaleWorkExp.SelectedItem.Value))
            //    _AppraisalForm.WorkHistoryRatingScaleRef_ID = int.Parse(cmbRatingScaleWorkExp.SelectedItem.Value);

            _AppraisalForm.AppraisalFormID = int.Parse(Request.QueryString["fid"]);
            _AppraisalForm.HideHistorySection = chkHideBlock.Checked;

            Status respStatus;

            respStatus = NewHRManager.UpdateAppraisalForm_QualificationExpirence(_AppraisalForm);
            if (respStatus.IsSuccess)
            {
                if (sender == btnSave)
                    SetMessage(lblMsg, "Appraisal updated.");
                else
                Response.Redirect("CoreCompetencies_5.aspx?fid=" + Request.QueryString["fid"]);

            }
            else
            {
                SetWarning(lblMsg, respStatus.ErrorMessage);
            }
            X.Js.AddScript("window.scrollTo(0,0);");
        }
    }
}