﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using System.Xml;

namespace Web.Appraisal
{
    public partial class Review_7 : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            hiddenValue.Text = Request.QueryString["fid"];
            if (!X.IsAjaxRequest)
            {
                Initialise();
              
            }
        }

        public void Initialise()
        {
            LoadLevels();
            //cmbRatingScale.Store[0].DataSource = NewHRManager.GetAllRatingScale();
            //cmbRatingScale.Store[0].DataBind();
            AppraisalForm formInst = new AppraisalForm();
            if (!string.IsNullOrEmpty(Request.QueryString["fid"]))
            {
                formInst = AppraisalManager.getFormInstanceByID(int.Parse(hiddenValue.Text));
                if (formInst != null)
                {
                    txtSectionName.Text = formInst.ReviewName;
                    txtEditorDescription.Text = formInst.ReviewDescription;
                    //if (formInst.ReviewRatingScaleRef_ID != null)
                    //    ExtControlHelper.ComboBoxSetSelected(formInst.ReviewRatingScaleRef_ID.ToString(), cmbRatingScale);
                    if (formInst.HideSupervisorReview != null && formInst.HideSupervisorReview.Value)
                        chkHideBlock.Checked = true;
                }
            }
        }
        
        private void LoadLevels()
        {
            //GridLevels.GetStore().DataSource = AppraisalManager.getallGoalsCategory();
            //GridLevels.GetStore().DataBind();
            if (!string.IsNullOrEmpty(Request.QueryString["fid"]))
            {

                if (NewHRManager.IsAppraisalQuestionnariesUsedInEmployee(int.Parse(Request.QueryString["fid"])))
                {
                    //btnAddLevel.Disabled = true;
                    //SetWarning(lblMsg, "You can't add and delete Review as this data has been used in employee from.");
                }

                int FormID = int.Parse(hiddenValue.Text);
                Store3.DataSource = AppraisalManager.GetAllReviewQuestionByFormID(FormID);
                Store3.DataBind();
            }

        }

        public void ClearLevelFields()
        {
            hiddenValueRow.Text = "";
            txtQuestion.Text = "";
            txtWeight.Text = "0";
            

        }

      
        protected void btnAddLevel_Click(object sender, DirectEventArgs e)
        {
            
            ClearLevelFields();
            WindowLevel.Show();
        }


        protected void btnDeleteLevel_Click(object sender, DirectEventArgs e)
        {
            int RowID = int.Parse(hiddenValueRow.Text.Trim());
            int AppraisalFormID = int.Parse(Request.QueryString["fid"]);
            Boolean inUse;
            bool result = AppraisalManager.DeleteReviewQuestionByID(RowID, AppraisalFormID, out inUse);

            if (!inUse)
            {
                if (result)
                {
                    LoadLevels();
                    NewMessage.ShowNormalMessage("Review deleted.");

                }
                else
                    NewMessage.ShowNormalMessage("Review is in Use");

            }
            else
            {
                NewMessage.ShowNormalMessage("Review is in Use");

            }

        }


        protected void lnkBtnDivideWeight_Click(object sender, DirectEventArgs e)
        {
            int FormID = int.Parse(Request.QueryString["fid"]);
            List<AppraisalFormReview> _listAppraisalFormReview = AppraisalManager.GetAllReviewQuestionByFormID(FormID);

            string SumOfAllWeight = _listAppraisalFormReview.Sum(x => x.Weight).ToString();

            decimal NowEquallyDividedWeigth = decimal.Parse(SumOfAllWeight) / _listAppraisalFormReview.Count();
            AppraisalFormReview _AppraisalFormQuestionnaire = new AppraisalFormReview();
            _AppraisalFormQuestionnaire.AppraisalFormRef_ID = FormID;
            _AppraisalFormQuestionnaire.Weight = NowEquallyDividedWeigth;
            Status status = AppraisalManager.DivieReviewWeightEqually(_AppraisalFormQuestionnaire);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Weight divided equally.");
                LoadLevels();
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }

        }

        protected void btnEditLevel_Click(object sender, DirectEventArgs e)
        {
            int RowID = int.Parse(hiddenValueRow.Text.Trim());
            AppraisalFormReview entity = AppraisalManager.GetReviewQuestionByFormID(RowID);
            WindowLevel.Show();
            txtWeight.Text = entity.Weight.ToString();
            txtQuestion.Text = entity.Question;

        }

        protected void btnLevelSaveUpdate_Click(object sender, DirectEventArgs e)
        {

            
            Page.Validate("InsertUpdateLevel");
            if (Page.IsValid)
            {

                AppraisalFormReview inst = new AppraisalFormReview();
                inst.Question = txtQuestion.Text;
                inst.Weight = decimal.Parse(txtWeight.Text);
                inst.AppraisalFormRef_ID = int.Parse(hiddenValue.Text);

                List<AppraisalFormReview> lines = new List<AppraisalFormReview>();
                
                bool isEdit=false;
                if (!string.IsNullOrEmpty(hiddenValueRow.Text))
                {
                    isEdit = true;
                    inst.QuestionnareID = int.Parse(hiddenValueRow.Text);
                }
                inst.Sequence = 1;

                AppraisalFormReview question =
                    AppraisalManager.GetAllReviewQuestionByFormID(int.Parse(hiddenValue.Text)).OrderBy(x => x.Sequence).LastOrDefault();
                if (question != null)
                    inst.Sequence = question.Sequence + 1;

                Status status = AppraisalManager.InsertUpdateReviewFormQuetion(inst, isEdit);
                if (status.IsSuccess)
                {
                    WindowLevel.Hide();
                    LoadLevels();
                    NewMessage.ShowNormalMessage("Question saved.");
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }
                
            }
             
             
        }

        protected void SaveQuestionnaireForm_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveFormData");
             if (Page.IsValid)
             {
                 AppraisalForm inst = new AppraisalForm();
                 inst = AppraisalManager.getFormInstanceByID(int.Parse(hiddenValue.Text));
                 inst.ReviewName = txtSectionName.Text;
                 inst.ReviewDescription = hdnEditorDescription.Text;
                 inst.HideSupervisorReview = chkHideBlock.Checked;
                 //inst.ReviewRatingScaleRef_ID = int.Parse(cmbRatingScale.SelectedItem.Value);
                 Status status = AppraisalManager.UpdateReviewFormData(inst);
                 if (status.IsSuccess)
                 {
                     WindowLevel.Hide();
                     LoadLevels();
                     NewMessage.ShowNormalMessage("Review saved.");
                 }
                 else
                 {
                     NewMessage.ShowWarningMessage(status.ErrorMessage);
                 }
             }

        }

        protected void btnChangeOrder_Click(object sender, DirectEventArgs e)
        {
            int formId = int.Parse(Request.QueryString["fid"]);

            int sourceId = int.Parse(hdnSource.Text.Trim());
            int destId = int.Parse(hdnDest.Text.Trim());
            string mode = hdnDropMode.Text.Trim();
            NewHRManager.ChangeReviewQuestionOrder(sourceId, destId, mode,formId);
            LoadLevels();
        }





        
    }
}