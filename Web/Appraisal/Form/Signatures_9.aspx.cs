﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Entity;
using BLL;
using BLL.BO;
using Utils.Helper;
using Utils.Calendar;


namespace Web.Appraisal.Form
{
    public partial class Signatures_9 : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();

            }
        }

        public void Initialise()
        {
            if (!string.IsNullOrEmpty(Request.QueryString["fid"]))
            {
                LoadData(int.Parse(Request.QueryString["fid"]));
            }
        }

        protected void LoadData(int ID)
        {
            AppraisalForm _AppraisalForm = NewHRManager.GetAppraisal_GeneralInfoByID(ID);
            if (_AppraisalForm != null)
            {
                if(!string.IsNullOrEmpty(_AppraisalForm.SignationName))
                    txtSectionName.Text = _AppraisalForm.SignationName;
                if (!string.IsNullOrEmpty(_AppraisalForm.SignationDescription))
                txtEditorDescription.Text = _AppraisalForm.SignationDescription;
            }

        }

        protected void ButtonBack_Click(object sender, DirectEventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["fid"]))
                Response.Redirect("PerformanceSummary.aspx?fid=" + Request.QueryString["fid"]);
        
        }

        protected void ButtonNext_Click(object sender, DirectEventArgs e)
        {
            AppraisalForm _AppraisalForm = new AppraisalForm();

            _AppraisalForm.SignationName = txtSectionName.Text.Trim();
            _AppraisalForm.SignationDescription = hdnEditorDescription.Text.Trim();
            _AppraisalForm.AppraisalFormID = int.Parse(Request.QueryString["fid"]);
            Status respStatus;

            respStatus = NewHRManager.UpdateAppraisalForm_Signature(_AppraisalForm);
            if (respStatus.IsSuccess)
            {

                SetMessage(lblMsg, "Appraisal updated.");

            }
            else
            {
                SetWarning(lblMsg, respStatus.ErrorMessage);
            }
            X.Js.AddScript("window.scrollTo(0,0);");
        }
    }
}