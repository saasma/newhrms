﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using System.Xml;
using BLL.BO;
using Utils.Calendar;
namespace Web.Appraisal
{
    public partial class Rollout : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.EmployeeAppraisal;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!X.IsAjaxRequest)
            {
                Initialise();
              
            }
        }

        public void btnLoad_Click(object sender, DirectEventArgs e)
        {
            LoadLevels();
        }

        public void Initialise()
        {
            List<AppraisalPeriod> periods = AppraisalManager.GetAllPeriods(); ;
            cmbPeriod.Store[0].DataSource = periods;
            cmbPeriod.Store[0].DataBind();

            cmbPeriod2.Store[0].DataSource = periods;
            cmbPeriod2.Store[0].DataBind();


            cmbRolloutGroup.Store[0].DataSource = AppraisalManager.GetAllRolloutGroups();
            cmbRolloutGroup.Store[0].DataBind();


            storeLevel.DataSource = NewPayrollManager.GetAllParentLevels();
            storeLevel.DataBind();

            cmbDepartment.Store[0].DataSource = DepartmentManager.GetAllDepartments();
            cmbDepartment.Store[0].DataBind();

            List<AppraisalForm> forms = AppraisalManager.GetAllForms(); ;

            storeForm.DataSource = forms;
            storeForm.DataBind();

            storeForm2.DataSource = forms;
            storeForm2.DataBind();

            cmbFilterForm.Store[0].DataSource = AppraisalManager.GetAllForms();
            cmbFilterForm.Store[0].DataBind();

            storeAdditionalStep.DataSource = TravelAllowanceManager.GetAdditionalSteps((int)FlowTypeEnum.Appraisal);
            storeAdditionalStep.DataBind();

            storeEmployee.DataSource = EmployeeManager.GetAllEmployeesForEmployeeImportExport().OrderBy(x => x.Name).ToList();
            storeEmployee.DataBind();


            //Appraisal Periods
            List<AppraisalPeriod> listAppraisalPeriods = AppraisalManager.GetAllPeriods().ToList();

            foreach (var item in listAppraisalPeriods)
            {
                item.Start = string.Format("{0} ({1} {2} - {3} {4})", item.Name, item.StartDate.Value.Year, DateHelper.GetMonthShortName(item.StartDate.Value.Month, true),
                    item.EndDate.Value.Year, DateHelper.GetMonthShortName(item.EndDate.Value.Month, true));
            }

            AppraisalPeriod obj = new AppraisalPeriod() { Start = "All", PeriodId = -1, Closed = true };
            listAppraisalPeriods.Insert(0, obj);
            cmbPeriodFilter.Store[0].DataSource = listAppraisalPeriods;
            cmbPeriodFilter.Store[0].DataBind();

            List<AppraisalPeriod> listOpenOnly = listAppraisalPeriods.Where(x => x.Closed == null || x.Closed == false).ToList();
            foreach (var item in listOpenOnly)
                cmbPeriodFilter.SelectedItems.Add(new Ext.Net.ListItem(item.PeriodId.ToString()));
        }
        
        private void LoadLevels()
        {
            //int empId = -1;
            //int formId = -1;

            //if (cmbFilterForm.SelectedItem != null && cmbFilterForm.SelectedItem.Value != null)
            //    formId = int.Parse(cmbFilterForm.SelectedItem.Value);

            //if (cmbSearch.SelectedItem != null && cmbSearch.SelectedItem.Value != null)
            //    empId = int.Parse(cmbSearch.SelectedItem.Value);

            //GridLevels.GetStore().DataSource = AppraisalManager.GetAllRollouts(empId, formId);
            //GridLevels.GetStore().DataBind();
            PagingToolbar1.DoRefresh();
           
        }

        public void ClearLevelFields()
        {

            txtMessage.Text = "";
            //txtRolloutDate.SelectedDate = DateTime.Now;
            txtSubmissionDate.SelectedDate = DateTime.Now.AddDays(1);

            cmbForm.Enable(true);
            cmbLevel.Enable(true);
            cmbDepartment.Enable(true);
           // txtRolloutDate.Enable(true);
            cmbPeriod.Enable(true);

            hiddenValueRow.Text = "";

        }

      
        protected void btnAddLevel_Click(object sender, DirectEventArgs e)
        {
            hiddenValueRow.Text = "";
            ClearLevelFields();
            WindowLevel.Show();
        }




        protected void btnDeleteLevel_Click(object sender, DirectEventArgs e)
        {
            int RowID = int.Parse(hiddenValueRow.Text.Trim());
            Status status = AppraisalManager.DeleteRolloutByID(RowID);
            if (status.IsSuccess)
            {
                LoadLevels();
                NewMessage.ShowNormalMessage("Rollout deleted.");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }


     

        protected void btnEditLevel_Click(object sender, DirectEventArgs e)
        {
           // ClearLevelFields();
            int RowID = int.Parse(hiddenValueRow.Text.Trim());
            AppraisalRollout entity = AppraisalManager.GetRolloutByFormID(RowID);

            if (entity.RolloutGroupRef_ID != null)
            {

                if (entity.RolloutGroupRef_ID != null)
                    cmbRolloutGroup.SetValue(entity.RolloutGroupRef_ID.ToString());

                cmbRolloutGroup.Disable(true);
               

                cmbForm2.SetValue(entity.FormID.ToString());
                cmbForm2.Disable(true);

                cmbPeriod2.Disable(true);
                cmbPeriod2.SetValue(entity.PeriodId.ToString());
                //txtRolloutDate.Disable(true);
                //txtRolloutDate.SelectedDate = entity.RolloutDate.Value;
                txtSubmissionDate2.SelectedDate = entity.SubmissionDate.Value;

                LoadFlowEvent(entity);

                WindowGroup.Show();
            }

            else
            {
                if (entity.LevelID != null)
                    cmbLevel.SetValue(entity.LevelID.ToString());
                else
                    cmbLevel.ClearValue();

                if (entity.DepartmentID != null)
                    cmbDepartment.SetValue(entity.DepartmentID.ToString());
                else
                    cmbDepartment.ClearValue();


                cmbLevel.Disable(true);
                cmbDepartment.Disable(true);
                cmbForm.SetValue(entity.FormID.ToString());
                cmbForm.Disable(true);

                cmbPeriod.Disable(true);
                cmbPeriod.SetValue(entity.PeriodId.ToString());
                //txtRolloutDate.Disable(true);
                //txtRolloutDate.SelectedDate = entity.RolloutDate.Value;
                txtSubmissionDate.SelectedDate = entity.SubmissionDate.Value;

                WindowLevel.Show();
            }
        }


        public void LoadFlowEvent(object sender,DirectEventArgs e)
        {
                        
            if (cmbPeriod2.SelectedItem == null || cmbPeriod2.SelectedItem.Value == null)
                return;
            
            int periodId = int.Parse(cmbPeriod2.SelectedItem.Value);

            List<ApprovalFlow> authorityTypeNameList = TravelAllowanceManager.getAuthorityTypeListForCombo((int)FlowTypeEnum.Appraisal);
            List<TextValue> additionSteps = TravelAllowanceManager.GetAdditionalSteps((int)FlowTypeEnum.Appraisal);
            List<ApprovalFlow> list = TravelAllowanceManager.GetDefaultApprovalFlowList((int)FlowTypeEnum.Appraisal);
            //LeaveApprovalEmployee leaveTeam = LeaveRequestManager.GetEmployeeTeam(empId);
            List<ApprovalFlow> stepIDList = TravelAllowanceManager.getTravelOrderListForCombo();
            foreach (ApprovalFlow item in list)
            {
                ApprovalFlow authorityName = authorityTypeNameList.FirstOrDefault(x => x.AuthorityType == item.AuthorityType);
                item.AuthorityTypeName = authorityName == null ? "" : authorityName.AuthorityTypeName;

                TravelOrderAuthorityEnum authority = (TravelOrderAuthorityEnum)item.AuthorityType;
                switch (authority)
                {
                    //case TravelOrderAuthorityEnum.Employee:
                    //    item.EmployeeId = empId;
                    //    break;
                    //case TravelOrderAuthorityEnum.Recommend1:
                    //    if (leaveTeam != null && leaveTeam.CC4 != null)
                    //        item.EmployeeId = leaveTeam.CC4;
                    //    break;
                    case TravelOrderAuthorityEnum.SpecificPerson:
                        if (item.Person1ID != null)
                            item.EmployeeId = item.Person1ID;
                        else if (item.Person2ID != null)
                            item.EmployeeId = item.Person2ID;
                        break;
                    
                }


                ApprovalFlow stepID = stepIDList.FirstOrDefault(x => x.StepID == item.StepID);
                if (stepID != null)
                    item.StepIDName = stepID.StepName;


                //if (item.ShowAdditionalStep != null)
                //{
                //    TextValue value = additionSteps.FirstOrDefault(x => x.Value == item.ShowAdditionalStep.ToString());
                //    if (value != null)
                //        item.AdditionStep = value.Text;
                //}

                // allow to delete after 3rd step
                if (item.StepID != (int)FlowStepEnum.Step1SaveAndSend && item.StepID != (int)FlowStepEnum.Step2
                    && item.StepID != (int)FlowStepEnum.Step15End)
                {
                    item.IsDeletable = true;
                }
            }

            gridFlow.Store[0].DataSource = list;
            gridFlow.Store[0].DataBind();

            gridFlow.Show();

          


        }

        public void LoadFlowEvent(AppraisalRollout rollout)
        {

            List<ApprovalFlow> stepIDList = TravelAllowanceManager.getTravelOrderListForCombo();

            //List<ApprovalFlow> authorityTypeNameList = TravelAllowanceManager.getAuthorityTypeListForCombo((int)FlowTypeEnum.Appraisal);
            //List<TextValue> additionSteps = TravelAllowanceManager.GetAdditionalSteps((int)FlowTypeEnum.Appraisal);
            //List<ApprovalFlow> list = TravelAllowanceManager.GetDefaultApprovalFlowList((int)FlowTypeEnum.Appraisal);


            ////LeaveApprovalEmployee leaveTeam = LeaveRequestManager.GetEmployeeTeam(empId);
            //List<ApprovalFlow> stepIDList = TravelAllowanceManager.getTravelOrderListForCombo();
            //foreach (ApprovalFlow item in list)
            //{
            //    ApprovalFlow authorityName = authorityTypeNameList.FirstOrDefault(x => x.AuthorityType == item.AuthorityType);
            //    item.AuthorityTypeName = authorityName == null ? "" : authorityName.AuthorityTypeName;

            //    TravelOrderAuthorityEnum authority = (TravelOrderAuthorityEnum)item.AuthorityType;
            //    switch (authority)
            //    {
            //        //case TravelOrderAuthorityEnum.Employee:
            //        //    item.EmployeeId = empId;
            //        //    break;
            //        //case TravelOrderAuthorityEnum.Recommend1:
            //        //    if (leaveTeam != null && leaveTeam.CC4 != null)
            //        //        item.EmployeeId = leaveTeam.CC4;
            //        //    break;
            //        case TravelOrderAuthorityEnum.SpecificPerson:
            //            if (item.Person1ID != null)
            //                item.EmployeeId = item.Person1ID;
            //            else if (item.Person2ID != null)
            //                item.EmployeeId = item.Person2ID;
            //            break;

            //    }


            //    ApprovalFlow stepID = stepIDList.FirstOrDefault(x => x.StepID == item.StepID);
            //    if (stepID != null)
            //        item.StepIDName = stepID.StepName;


            //    //if (item.ShowAdditionalStep != null)
            //    //{
            //    //    TextValue value = additionSteps.FirstOrDefault(x => x.Value == item.ShowAdditionalStep.ToString());
            //    //    if (value != null)
            //    //        item.AdditionStep = value.Text;
            //    //}

            //    // allow to delete after 3rd step
            //    if (item.StepID != (int)FlowStepEnum.Step1SaveAndSend && item.StepID != (int)FlowStepEnum.Step2
            //        && item.StepID != (int)FlowStepEnum.Step15End)
            //    {
            //        item.IsDeletable = true;
            //    }
            //}

            foreach (var item in rollout.AppraisalRolloutFlows)
            {
                item.StepIDName = stepIDList.FirstOrDefault(x => x.StepID == item.StepRef_ID).StepName;
                item.StepID = item.StepRef_ID;
            }

            gridFlow.Store[0].DataSource = rollout.AppraisalRolloutFlows.OrderBy(x => x.StepRef_ID).ToList();
            gridFlow.Store[0].DataBind();

            gridFlow.Show();




        }

        protected void btnSaveRolloutGroup_Click(object sender, DirectEventArgs e)
        {


            Page.Validate("InsertUpdateLevel");
            if (Page.IsValid)
            {
                //if (txtSubmissionDate.SelectedDate <= txtRolloutDate.SelectedDate)
                //{
                //    X.Msg.Alert("error", "Submission Date cannot be earlier than Rollout Date").Show();
                //    return;
                //}

                AppraisalRollout inst = new AppraisalRollout();

                bool isEdit = false;
                if (!string.IsNullOrEmpty(hiddenValueRow.Text))
                {
                    isEdit = true;
                    inst.RolloutID = int.Parse(hiddenValueRow.Text);
                }


                // Flow list
                string flowJson = e.ExtraParams["FlowList"];
                List<ApprovalFlow> flowList = JSON.Deserialize<List<ApprovalFlow>>(flowJson).ToList();


                //if (cmbLevel.SelectedItem != null && cmbLevel.SelectedItem.Value != null)
                //    inst.LevelID = int.Parse(cmbLevel.SelectedItem.Value);

                if (cmbRolloutGroup.SelectedItem != null && cmbRolloutGroup.SelectedItem.Value != null)
                    inst.RolloutGroupRef_ID = int.Parse(cmbRolloutGroup.SelectedItem.Value);

                inst.FormID = int.Parse(cmbForm2.SelectedItem.Value);
                inst.PeriodId = int.Parse(cmbPeriod2.SelectedItem.Value);
                inst.SubmissionDate = txtSubmissionDate2.SelectedDate;
                inst.RolloutMessage = txtMessage.Text;

                // add flow

                int index = 1;

                foreach (ApprovalFlow item in flowList)
                {
                    AppraisalRolloutFlow flow = new AppraisalRolloutFlow();
                    flow.StepRef_ID = item.StepID;
                    flow.EmployeeId = item.EmployeeId;
                    flow.ShowAdditionalStep = item.ShowAdditionalStep;
                    flow.StepName = item.StepName;
                    flow.AuthorityTypeDisplayName = item.AuthorityTypeDisplayName;
                    flow.AuthorityType = item.AuthorityType;

                    if (flow.AuthorityType == (int)TravelOrderAuthorityEnum.Employee)
                        flow.EmployeeId = null;
                    else if(flow.EmployeeId == null)
                    {
                        NewMessage.ShowWarningMessage("Employee should be selected for the row " + index + " in approval flow.");
                        return;
                    }

                   // flow.FlowType = item.FlowType;
                    inst.AppraisalRolloutFlows.Add(flow);
                }


                Status status = AppraisalManager.InsertUpdateRollout(inst, isEdit);
                if (status.IsSuccess)
                {
                    WindowGroup.Hide();
                    LoadLevels();
                    NewMessage.ShowNormalMessage("Rollout saved.");

                    ClearLevelFields();
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);

                }

            }


        }

        protected void btnLevelSaveUpdate_Click(object sender, DirectEventArgs e)
        {

            
            Page.Validate("InsertUpdateLevel");
            if (Page.IsValid)
            {
                //if (txtSubmissionDate.SelectedDate <= txtRolloutDate.SelectedDate)
                //{
                //    X.Msg.Alert("error", "Submission Date cannot be earlier than Rollout Date").Show();
                //    return;
                //}

                AppraisalRollout inst = new AppraisalRollout();
               
                bool isEdit=false;
                if (!string.IsNullOrEmpty(hiddenValueRow.Text))
                {
                    isEdit = true;  
                    inst.RolloutID = int.Parse(hiddenValueRow.Text);
                }

                bool hasLevel = false;
                if (cmbLevel.SelectedItem != null && cmbLevel.SelectedItem.Value != null)
                    hasLevel = true;

                bool hasDep = false;
                if (cmbDepartment.SelectedItem != null && cmbDepartment.SelectedItem.Value != null)
                    hasDep = true;

                if (chkAllEmp.Checked==false)
                {

                    if (hasLevel == false && hasDep == false)
                    {
                        NewMessage.ShowWarningMessage("Level or Department selection is required.");
                        return;
                    }
                }

                if (cmbLevel.SelectedItem != null && cmbLevel.SelectedItem.Value != null)
                    inst.LevelID = int.Parse(cmbLevel.SelectedItem.Value);

                if (cmbDepartment.SelectedItem != null && cmbDepartment.SelectedItem.Value != null)
                    inst.DepartmentID = int.Parse(cmbDepartment.SelectedItem.Value);

                inst.FormID = int.Parse(cmbForm.SelectedItem.Value);
                inst.PeriodId = int.Parse(cmbPeriod.SelectedItem.Value);
                inst.SubmissionDate = txtSubmissionDate.SelectedDate;
                inst.RolloutMessage = txtMessage.Text;
                
                Status status = AppraisalManager.InsertUpdateRollout(inst, isEdit);
                if (status.IsSuccess)
                {
                    WindowLevel.Hide();
                    LoadLevels();
                    NewMessage.ShowNormalMessage("Rollout saved.");

                    ClearLevelFields();
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                    
                }
                
            }
             
             
        }

        


        
    }
}

