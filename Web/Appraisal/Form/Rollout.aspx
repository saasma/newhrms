﻿<%@ Page Title="Rollout" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="Rollout.aspx.cs" Inherits="Web.Appraisal.Rollout" %>

<%--<%@ Register Src="~/NewHR/UserControls/EmployeeDetailsCtl.ascx" TagName="EmployeeDetailsCtl"
    TagPrefix="uc1" %>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        
        
        var CommandHandler1 = function(command, record){
            <%= hiddenValueRow.ClientID %>.setValue(record.data.RolloutID);
            
                if(command=="Edit")
                {

                    if(record.data.EmployeeName != '')
                    {

                        window.location = 'RolloutMega.aspx?ID=' + record.data.RolloutID;
                        
                    }
                    else
                        <%= btnEditLevel.ClientID %>.fireEvent('click');
                }
                else
                {
                
                    <%= btnDeleteLevel.ClientID %>.fireEvent('click');
                }

             }
             
             function searchList() {
            <%=PagingToolbar1.ClientID %>.doRefresh();
        }


        function process(val)
        {
            if(val==true) 
            {
                <%= cmbLevel.ClientID %>.disable();
                <%= cmbDepartment.ClientID %>.disable();
            } 
            else 
            {
                <%= cmbLevel.ClientID %>.enable();
                <%= cmbDepartment.ClientID %>.enable();
            }
        }

                var additionalRenderer = function (value, metaData, record, rowIndex, colIndex, store) {
                    var r = <%= storeAdditionalStep.ClientID %>.getById(value.toString().toLowerCase());
                    if (Ext.isEmpty(r)) {
                        return "";
                    }
                    return r.data.Text;
                };

        var RemoveItemLine = function (column, command, record, recordIndex, cellIndex) {
            var store = this.grid.store;
            store.remove(record);
        };


             var EmpRenderer = function (value, metaData, record, rowIndex, colIndex, store) {
                    
                    var r = <%= storeEmployee.ClientID %>.getById(value.toString().toLowerCase());
                    if (Ext.isEmpty(r)) {
                        return "";
                    }
                    return r.data.NameEIN;
             };

        
        var beforeEdit = function (e1, e, e2, e3) {


            if ((e.field == "EmployeeId") && e.record.data.AuthorityType == "1") {
                e.cancel = true;
                return;
            }


        }

        var prepareToolbar = function (grid, toolbar, rowIndex, record) {
     
            if(record.data.IsDeletable==false)
            {

                toolbar.hide();
            }
            
            
        };

        var prepareToolbar = function (grid, toolbar, rowIndex, record) {
     
            if(record.data.EmployeeName== "")
            {
                debugger;
                toolbar.hide();
            }
            
            
        };

    </script>
    <style type="text/css">
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
        #content
        {
            margin: inherit;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Appraisal Rollout
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel" style="margin-top: 0px !important; padding-top: 0px !important;">
        <ext:Hidden runat="server" ID="hiddenValue" />
        <ext:Hidden runat="server" ID="hiddenValueRow" />
         <ext:Store runat="server" ID="storeAdditionalStep">
            <Model>
                <ext:Model ID="Model3" IDProperty="Value" runat="server">
                    <Fields>
                        <ext:ModelField Name="Value" />
                        <ext:ModelField Name="Text" Type="String" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
        <ext:Store runat="server" ID="storeEmployee">
            <Model>
                <ext:Model ID="Model7" IDProperty="EmployeeId" runat="server">
                    <Fields>
                        <ext:ModelField Name="NameEIN" />
                        <ext:ModelField Name="EmployeeId" Type="String" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
        <ext:LinkButton runat="server" Hidden="true" ID="btnEditLevel">
            <DirectEvents>
                <Click OnEvent="btnEditLevel_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:LinkButton>
        <ext:Button ID="btnDeleteLevel" runat="server" Hidden="true">
            <DirectEvents>
                <Click OnEvent="btnDeleteLevel_Click">
                    <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the Rollout form?" />
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:Button>
        <div class="separator bottom">
        </div>
        <div class="innerLR">
            <%--<uc1:EmployeeDetailsCtl Id="EmployeeDetailsCtl1" runat="server" />--%>
            <div>
                <table>
                    <tr>
                        <td>
                            <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                                <Proxy>
                                    <ext:AjaxProxy Url="../../Handler/EmpSearch.ashx">
                                        <ActionMethods Read="GET" />
                                        <Reader>
                                            <ext:JsonReader Root="plants" TotalProperty="total" />
                                        </Reader>
                                    </ext:AjaxProxy>
                                </Proxy>
                                <Model>
                                    <ext:Model ID="Model2" IDProperty="Name" Name="ItemLineModel" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Name" Type="String" />
                                            <ext:ModelField Name="EmployeeId" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                            <ext:ComboBox LabelSeparator="" ID="cmbSearch" FieldLabel="Search Employee" LabelWidth="70"
                                LabelAlign="Top" runat="server" DisplayField="Name" ValueField="EmployeeId" StoreID="storeSearch"
                                TypeAhead="false" Width="180" PageSize="9999" HideBaseTrigger="true" MinChars="1"
                                TriggerAction="All" ForceSelection="true">
                                <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                                    <ItemTpl ID="ItemTpl1" runat="server">
                                        <Html>
                                            <tpl>
                                            <div class="search-item">
                                                            <span>{Name}</span>  
                                             </div>
					                </tpl>
                                        </Html>
                                    </ItemTpl>
                                </ListConfig>
                                <Triggers>
                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                </Triggers>
                                <Listeners>
                                    <Select Handler="this.getTrigger(0).show();" />
                                    <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                    <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                                </Listeners>
                            </ext:ComboBox>
                        </td>
                        <td style="padding-left: 10px">
                            <ext:ComboBox ForceSelection="true" QueryMode="Local" ID="cmbFilterForm" FieldLabel="Select Form"
                                runat="server" LabelAlign="Top" DisplayField="Name" Width="300" LabelSeparator=""
                                ValueField="AppraisalFormID">
                                <Store>
                                    <ext:Store ID="store2" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="AppraisalFormID" Type="String" />
                                            <ext:ModelField Name="Name" />
                                        </Fields>
                                    </ext:Store>
                                </Store>
                                <Triggers>
                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                </Triggers>
                                <Listeners>
                                    <Select Handler="this.getTrigger(0).show();" />
                                    <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                    <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                                </Listeners>
                            </ext:ComboBox>
                        </td>
                         <td>
                            <ext:MultiCombo Width="200" LabelSeparator="" SelectionMode="All" StyleSpec="margin-left:10px" LabelWidth="50" LabelAlign="Top"
                                ID="cmbPeriodFilter" DisplayField="Start" ValueField="PeriodId"
                                runat="server" FieldLabel="Period">
                                <Store>
                                    <ext:Store ID="Store6" runat="server">
                                        <Model>
                                            <ext:Model ID="Model8" IDProperty="PeriodId" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="PeriodId" />
                                                    <ext:ModelField Name="Start" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                            </ext:MultiCombo>
                        </td>
                        <td style="padding-top: 20px">
                            <ext:Button runat="server" ID="btnLoad" StyleSpec="margin-left:7px;" Cls="btn btn-primary"
                                Text="<i></i>Load">
                                <DirectEvents>
                                    <Click OnEvent="btnLoad_Click">
                                        <EventMask ShowMask="true" />
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </td>
                        <td style="padding-top: 20px">
                            <ext:Button runat="server" ID="btnBulkRollout" StyleSpec="margin-left:7px;" Cls="btn btn-primary"
                                Text="<i></i>Bulk Rollout">
                                <Listeners>
                                    <Click Handler="#{hiddenValueRow}.setValue('');#{cmbLevel}.clearValue();#{WindowLevel}.show();" />
                                </Listeners>
                            </ext:Button>
                        </td>
                        <td style="padding-top: 20px">
                            <ext:Button runat="server" ID="btnGroupRollout" StyleSpec="margin-left:7px;" Cls="btn btn-primary"
                                Text="<i></i>Group Rollout">
                                <Listeners>
                                    <Click Handler="#{hiddenValueRow}.clear();#{cmbRolloutGroup}.clearValue();#{cmbRolloutGroup}.enable();#{cmbForm2}.enable();#{cmbPeriod2}.enable();#{WindowGroup}.center();#{WindowGroup}.show();" />
                                </Listeners>
                            </ext:Button>
                        </td>
                    </tr>
                </table>
                <table style="margin-left: 0px;">
                    <tr style="display: none">
                        <td>
                            <div class="buttonBlock">
                                <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="btnAddLevel" Cls="btnFlat"
                                    BaseCls="btnFlat" Text="<i></i>Add Rollout">
                                    <DirectEvents>
                                        <Click OnEvent="btnAddLevel_Click">
                                            <EventMask ShowMask="true" />
                                        </Click>
                                    </DirectEvents>
                                </ext:LinkButton>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <div>
                                <ext:GridPanel StyleSpec="margin-top:15px;" Width="1200px" ID="GridLevels" runat="server"
                                    Cls="itemgrid">
                                    <Store>
                                        <ext:Store ID="Store3" runat="server" AutoLoad="true" PageSize="50">
                                            <Proxy>
                                                <ext:AjaxProxy Json="true" Url="../../Handler/AppraisalRolloutHandler.ashx">
                                                    <ActionMethods Read="GET" />
                                                    <Reader>
                                                        <ext:JsonReader Root="data" TotalProperty="total" />
                                                    </Reader>
                                                </ext:AjaxProxy>
                                            </Proxy>
                                            <AutoLoadParams>
                                                <ext:Parameter Name="start" Value="0" Mode="Raw" />
                                            </AutoLoadParams>
                                            <Parameters>
                                                <ext:StoreParameter Name="empId" Value="#{cmbSearch}.getValue()" Mode="Raw" ApplyMode="Always" />
                                                <ext:StoreParameter Name="formId" Value="#{cmbFilterForm}.getValue()" Mode="Raw"
                                                    ApplyMode="Always" />
                                                 <ext:StoreParameter Name="PeriodId" Value="#{cmbPeriodFilter}.getValue()" Mode="Raw"
                                    ApplyMode="Always" />
                                            </Parameters>
                                            <Model>
                                                <ext:Model ID="Model4" runat="server" IDProperty="RolloutID">
                                                    <Fields>
                                                        <ext:ModelField Name="RolloutID" Type="String" />
                                                        <ext:ModelField Name="FormName" Type="string" />
                                                        <ext:ModelField Name="LevelName" Type="string" />
                                                        <ext:ModelField Name="FromDate" Type="Date" />
                                                        <ext:ModelField Name="ToDate" Type="Date" />
                                                        <ext:ModelField Name="NumEmployees" Type="string" />
                                                        <ext:ModelField Name="StatusModified" Type="string" />
                                                        <ext:ModelField Name="EmployeeName" Type="String" />
                                                        <ext:ModelField Name="Department" Type="String" />
                                                         <ext:ModelField Name="Period" Type="String" />
                                                         <ext:ModelField Name="AppraisalGroup" Type="String" />
                                                    </Fields>
                                                </ext:Model>
                                            </Model>
                                        </ext:Store>
                                    </Store>
                                    <ColumnModel>
                                        <Columns>
                                            <ext:DateColumn ID="colRolloutDate" runat="server" Align="Left" Text="From Date"
                                                MenuDisabled="true" Sortable="false" Format="yyyy/MM/dd" DataIndex="FromDate"
                                                Resizable="false" Draggable="false" Width="100">
                                            </ext:DateColumn>
                                            <ext:DateColumn ID="DateColumn1" runat="server" Align="Left" Text="To Date" MenuDisabled="true"
                                                Sortable="false" Format="yyyy/MM/dd" DataIndex="ToDate" Resizable="false" Draggable="false"
                                                Width="100">
                                            </ext:DateColumn>
                                            <ext:Column ID="Column5" Sortable="false" MenuDisabled="true" runat="server" Text="Form"
                                                Width="250" Align="Left" DataIndex="FormName" />
                                            <ext:Column ID="Column6" Sortable="false" MenuDisabled="true" runat="server" Text="Employee"
                                                Width="120" Align="Left" DataIndex="EmployeeName" />
                                            <ext:Column ID="Column11" Sortable="false" MenuDisabled="true" runat="server" Text="Level"
                                                Width="120" Align="Left" DataIndex="LevelName" />
                                            <ext:Column ID="Column1" Sortable="false" MenuDisabled="true" runat="server" Text="Department"
                                                Width="120" Align="Left" DataIndex="Department" />
                                            <ext:Column ID="Column7" Sortable="false" MenuDisabled="true" runat="server" Text="No. of Emps"
                                                Width="90" Align="Left" DataIndex="NumEmployees">
                                            </ext:Column>
                                            <ext:Column ID="Column2" Sortable="false" MenuDisabled="true" runat="server" Text="Period"
                                                Width="120" Align="Left" DataIndex="Period" />
                                            <ext:Column ID="Column3" Sortable="false" MenuDisabled="true" runat="server" Text="Group"
                                                Width="120" Align="Left" DataIndex="AppraisalGroup" />
                                            <ext:CommandColumn ID="CommandColumn1" runat="server" Text="" Align="Center"
                                                Width="30">
                                                <Commands>
                                                    <ext:GridCommand Icon="Pencil" CommandName="Edit" />
                                                </Commands>
                                                
                                                <Listeners>
                                                    <Command Handler="CommandHandler1(command,record);" />
                                                </Listeners>
                                            </ext:CommandColumn>
                                            <ext:CommandColumn ID="CommandColumn3" runat="server" Text="" Align="Center"
                                                Width="30">
                                                <Commands>
                                                    <ext:GridCommand Icon="Delete" CommandName="Delete" />
                                                </Commands>
                                              
                                                <Listeners>
                                                    <Command Handler="CommandHandler1(command,record);" />
                                                </Listeners>
                                            </ext:CommandColumn>
                                        </Columns>
                                    </ColumnModel>
                                    <SelectionModel>
                                        <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                                    </SelectionModel>
                                    <BottomBar>
                                        <ext:PagingToolbar ID="PagingToolbar1" runat="server" PageSize="50" DisplayInfo="true"
                                            DisplayMsg="Displaying Requests {0} - {1} of {2}" EmptyMsg="No Records to display">
                                            <Items>
                                                <ext:ComboBox Hidden="true" runat="server" SelectedIndex="0" Width="80" ID="cmbPageSize">
                                                    <Items>
                                                        <ext:ListItem Text="20" Value="20" />
                                                        <ext:ListItem Text="30" Value="30" />
                                                        <ext:ListItem Text="50" Value="50" />
                                                        <ext:ListItem Text="100" Value="100" />
                                                    </Items>
                                                    <Listeners>
                                                        <Select Handler="#{PagingToolbar1}.pageSize = parseInt( '50'); searchList();" />
                                                    </Listeners>
                                                </ext:ComboBox>
                                            </Items>
                                        </ext:PagingToolbar>
                                    </BottomBar>
                                </ext:GridPanel>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <ext:Window ID="WindowLevel" runat="server" Title="Rollout Form" Icon="Application"
                Width="500" Height="450" BodyPadding="5" Hidden="true" Modal="true">
                <Content>
                    <table class="fieldTable">
                        <tr>
                            <td>
                                <ext:ComboBox ForceSelection="true" QueryMode="Local" ID="cmbLevel" FieldLabel="Select Employee Level"
                                    runat="server" LabelAlign="Top" DisplayField="Name" Width="180" LabelSeparator=""
                                    ValueField="LevelId">
                                    <Store>
                                        <ext:Store ID="storeLevel" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="LevelId" Type="String" />
                                                <ext:ModelField Name="Name" />
                                            </Fields>
                                        </ext:Store>
                                    </Store>
                                </ext:ComboBox>
                                <%--<asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator2" runat="server"
                                    ValidationGroup="SaveUpdateLevel" ControlToValidate="cmbLevel" ErrorMessage="Employee Level is required." />--%>
                            </td>
                            <td>
                               <ext:Checkbox ID="chkAllEmp" runat="server" BoxLabel="All Remaining Employees">
                                    <Listeners>
                                        <Change Handler="process(this.getValue())" />
                                    </Listeners>
                               </ext:Checkbox>
                            </td>
                        </tr>
                        <tr>
                             <td>
                                <ext:ComboBox ForceSelection="true" QueryMode="Local" ID="cmbDepartment" FieldLabel="Select Department"
                                    runat="server" LabelAlign="Top" DisplayField="Name" Width="180" LabelSeparator=""
                                    ValueField="DepartmentId">
                                    <Store>
                                        <ext:Store ID="store1" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="DepartmentId" Type="String" />
                                                <ext:ModelField Name="Name" />
                                            </Fields>
                                        </ext:Store>
                                    </Store>
                                </ext:ComboBox>
                               <%-- <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator3" runat="server"
                                    ValidationGroup="SaveUpdateLevel" ControlToValidate="cmbLevel" ErrorMessage="Employee Level is required." />--%>
                            </td>
                            <td rowspan="4" valign="top" style="display: none">
                                <ext:TextArea ID="txtMessage" runat="server" Width="400" Height="75" FieldLabel="Message"
                                    LabelAlign="Top">
                                </ext:TextArea>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <ext:ComboBox ForceSelection="true" QueryMode="Local" ID="cmbForm" FieldLabel="Select Rollout Form"
                                    runat="server" LabelAlign="Top" DisplayField="Name" Width="250" LabelSeparator=""
                                    ValueField="AppraisalFormID">
                                    <Store>
                                        <ext:Store ID="storeForm" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="AppraisalFormID" Type="String" />
                                                <ext:ModelField Name="Name" />
                                            </Fields>
                                        </ext:Store>
                                    </Store>
                                </ext:ComboBox>
                                <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator1" runat="server"
                                    ValidationGroup="SaveUpdateLevel" ControlToValidate="cmbForm" ErrorMessage="Form Name is required." />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <ext:ComboBox ForceSelection="true" QueryMode="Local" ID="cmbPeriod" FieldLabel="Appraisal Period"
                                    runat="server" LabelAlign="Top" DisplayField="Name" Width="180" LabelSeparator=""
                                    ValueField="PeriodId">
                                    <Store>
                                        <ext:Store ID="store4" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="PeriodId" Type="String" />
                                                <ext:ModelField Name="Name" />
                                            </Fields>
                                        </ext:Store>
                                    </Store>
                                    <%-- <DirectEvents>
                                    <Select OnEvent="cmbPeriod_Change">
                                        <EventMask ShowMask="true" />
                                    </Select>
                                </DirectEvents>--%>
                                </ext:ComboBox>
                                <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator9" runat="server"
                                    ValidationGroup="SaveUpdateLevel" ControlToValidate="cmbPeriod" ErrorMessage="Period is required." />
                            </td>
                        </tr>
                        <%-- <tr>
                        <td>
                            <ext:DateField ID="txtRolloutDate" runat="server" FieldLabel="Select Rollout Date"
                                LabelAlign="Top" Width="176" LabelSeparator="">
                            </ext:DateField>
                            <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator3" runat="server"
                                ValidationGroup="SaveUpdateLevel" ControlToValidate="txtRolloutDate" ErrorMessage="Rollout date is required." />
                        </td>
                    </tr>--%>
                        <tr>
                            <td>
                                <ext:DateField ID="txtSubmissionDate" runat="server" FieldLabel="Last Date of submission"
                                    LabelAlign="Top" Width="180" LabelSeparator="">
                                </ext:DateField>
                                <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator5" runat="server"
                                    ValidationGroup="SaveUpdateLevel" ControlToValidate="txtSubmissionDate" ErrorMessage="Submission date is required." />
                            </td>
                        </tr>
                        <tr>
                            <td valign="bottom" colspan="2">
                                <div class="popupButtonDiv">
                                    <ext:Button runat="server" Cls="btn btn-primary" ID="btnLevelSaveUpdate" ValidationGroup="SaveUpdateLevel"
                                        Text="<i></i>Save">
                                        <DirectEvents>
                                            <Click Timeout="999999" OnEvent="btnLevelSaveUpdate_Click">
                                                <EventMask ShowMask="true" />
                                            </Click>
                                        </DirectEvents>
                                        <Listeners>
                                            <Click Handler="valGroup = 'SaveUpdateLevel'; return CheckValidation();">
                                            </Click>
                                        </Listeners>
                                    </ext:Button>
                                    <div class="btnFlatOr">
                                        or</div>
                                    <ext:LinkButton runat="server" StyleSpec="padding:0px;" Cls="btnFlatLeftGap" ID="LinkButton1"
                                        Text="<i></i>Cancel" runat="server">
                                        <Listeners>
                                            <Click Handler="#{WindowLevel}.hide();">
                                            </Click>
                                        </Listeners>
                                    </ext:LinkButton>
                                </div>
                            </td>
                        </tr>
                    </table>
                </Content>
            </ext:Window>

             <ext:Window ID="WindowGroup" runat="server" Title="Rollout Group" Icon="Application"
                Width="950" Height="550" BodyPadding="5" Hidden="true" Modal="true" AutoScroll="true">
                <Content>
                    <table class="fieldTable">
                        <tr>
                            <td>
                                <ext:ComboBox ForceSelection="true" QueryMode="Local" ID="cmbRolloutGroup" FieldLabel="Select Rollout Group"
                                    runat="server" LabelAlign="Top" DisplayField="Name" Width="180" LabelSeparator=""
                                    ValueField="RolloutGroupID">
                                    <Store>
                                        <ext:Store ID="storeRolloutGroup" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="RolloutGroupID" Type="String" />
                                                <ext:ModelField Name="Name" />
                                            </Fields>
                                        </ext:Store>
                                    </Store>
                                </ext:ComboBox>
                                <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator222" runat="server"
                                    ValidationGroup="SaveUpdateGroup" ControlToValidate="cmbRolloutGroup" ErrorMessage="Rollout Group is required." />
                            </td>
                           
                        </tr>
                        
                        <tr>
                            <td>
                                <ext:ComboBox ForceSelection="true" QueryMode="Local" ID="cmbForm2" FieldLabel="Select Rollout Form"
                                    runat="server" LabelAlign="Top" DisplayField="Name" Width="250" LabelSeparator=""
                                    ValueField="AppraisalFormID">
                                    <Store>
                                        <ext:Store ID="storeForm2" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="AppraisalFormID" Type="String" />
                                                <ext:ModelField Name="Name" />
                                            </Fields>
                                        </ext:Store>
                                    </Store>
                                </ext:ComboBox>
                                <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator122" runat="server"
                                    ValidationGroup="SaveUpdateGroup" ControlToValidate="cmbForm2" ErrorMessage="Form Name is required." />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <ext:ComboBox ForceSelection="true" QueryMode="Local" ID="cmbPeriod2" FieldLabel="Appraisal Period"
                                    runat="server" LabelAlign="Top" DisplayField="Name" Width="180" LabelSeparator=""
                                    ValueField="PeriodId">
                                    <Store>
                                        <ext:Store ID="store5" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="PeriodId" Type="String" />
                                                <ext:ModelField Name="Name" />
                                            </Fields>
                                        </ext:Store>
                                    </Store>
                                     <DirectEvents>
                                    <Select OnEvent="LoadFlowEvent">
                                        <EventMask ShowMask="true" />
                                    </Select>
                                </DirectEvents>
                                </ext:ComboBox>
                                <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator2" runat="server"
                                    ValidationGroup="SaveUpdateGroup" ControlToValidate="cmbPeriod2" ErrorMessage="Period is required." />
                            </td>
                        </tr>
                        <%-- <tr>
                        <td>
                            <ext:DateField ID="txtRolloutDate" runat="server" FieldLabel="Select Rollout Date"
                                LabelAlign="Top" Width="176" LabelSeparator="">
                            </ext:DateField>
                            <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator3" runat="server"
                                ValidationGroup="SaveUpdateLevel" ControlToValidate="txtRolloutDate" ErrorMessage="Rollout date is required." />
                        </td>
                    </tr>--%>
                        <tr>
                            <td>
                                <ext:DateField ID="txtSubmissionDate2" runat="server" FieldLabel="Last Date of submission"
                                    LabelAlign="Top" Width="180" LabelSeparator="">
                                </ext:DateField>
                                <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator3" runat="server"
                                    ValidationGroup="SaveUpdateGroup" ControlToValidate="txtSubmissionDate2" ErrorMessage="Submission date is required." />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                 <ext:GridPanel Hidden="true" Title="Approval Flow" Width="900" Header="true" StyleSpec="margin-top:15px;"
                                    ID="gridFlow" runat="server" Cls="itemgrid">
                                    <Store>
                                        <ext:Store ID="storeAllowances" runat="server">
                                            <Model>
                                                <ext:Model ID="Model6" Name="SettingModel" runat="server">
                                                    <Fields>
                                                        <ext:ModelField Name="StepID" Type="String" />
                                                        <ext:ModelField Name="StepIDName" Type="String" />
                                                        <ext:ModelField Name="StepName" Type="String" />
                                                        <ext:ModelField Name="AuthorityType" Type="String" />
                                                        <ext:ModelField Name="AuthorityTypeName" Type="String" />
                                                        <ext:ModelField Name="AuthorityTypeDisplayName" Type="String" />
                                                        <ext:ModelField Name="EmployeeId" Type="String" />
                                                        <ext:ModelField Name="ShowAdditionalStep" Type="String" />
                                                        <ext:ModelField Name="IsDeletable" Type="Boolean" />
                                                    </Fields>
                                                </ext:Model>
                                            </Model>
                                        </ext:Store>
                                    </Store>
                                    <Plugins>
                                        <ext:CellEditing ID="CellEditing1" ClicksToEdit="1" runat="server">
                                        </ext:CellEditing>
                                    </Plugins>
                                    <Listeners>
                                        <BeforeEdit Fn="beforeEdit" />
                                    </Listeners>
                                    <ColumnModel>
                                        <Columns>
                                            <ext:RowNumbererColumn ID="RowNumbererColumn1" DataIndex="SequenceNo" runat="server"
                                                Width="35" />
                                            <ext:Column ID="colTravelOrder" runat="server" Text="Step" Sortable="false" MenuDisabled="true"
                                                Width="80" DataIndex="StepIDName" Border="false">
                                            </ext:Column>
                                            <ext:Column ID="Column14" runat="server" Text="Appraisal Status Name" Sortable="false" MenuDisabled="true"
                                                Width="160" DataIndex="StepName" Border="false">
                                                <Editor>
                                                    <ext:TextField ID="TextField2" runat="server" />
                                                </Editor>
                                            </ext:Column>
                                            <ext:Column Hidden="true" ID="Column16" runat="server" Text="Authority" Sortable="false" MenuDisabled="true"
                                                Width="120" DataIndex="AuthorityTypeName" Border="false">
                                            </ext:Column>
                                            <ext:Column ID="Column17" runat="server" Text="Commenter Display Name" Sortable="false"
                                                MenuDisabled="true" Width="180" DataIndex="AuthorityTypeDisplayName" Border="false">
                                                <Editor>
                                                    <ext:TextField ID="TextField3" runat="server" />
                                                </Editor>
                                            </ext:Column>
                                            <ext:Column ID="Column20" runat="server" Flex="1" Text="Employee" Sortable="false"
                                                MenuDisabled="true" Width="180" DataIndex="EmployeeId" Border="false">
                                                <Renderer Fn="EmpRenderer" />
                                                <Editor>
                                                    <ext:ComboBox DisplayField="NameEIN" ForceSelection="true" QueryMode="Local" ValueField="EmployeeId"
                                                        StoreID="storeEmployee" ID="ComboBox2" runat="server">
                                                    </ext:ComboBox>
                                                </Editor>
                                            </ext:Column>
                                            <ext:Column ID="Column13" runat="server" Text="Additional Step" Sortable="false"
                                                MenuDisabled="true" Width="200" DataIndex="ShowAdditionalStep" Border="false">
                                                <Renderer Fn="additionalRenderer" />
                                                <Editor>
                                                    <ext:ComboBox DisplayField="Text" QueryMode="Local" ForceSelection="true" ValueField="Value"
                                                        StoreID="storeAdditionalStep" ID="ComboBox5" runat="server">
                                                        <Triggers>
                                                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                                        </Triggers>
                                                        <Listeners>
                                                            <Select Handler="this.getTrigger(0).show();" />
                                                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                                            <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                                                        </Listeners>
                                                    </ext:ComboBox>
                                                </Editor>
                                            </ext:Column>
                                            <ext:CommandColumn ID="CommandColumn2" runat="server" Sortable="false" MenuDisabled="true"
                                                Width="30" Align="Center">
                                                <Commands>
                                                    <ext:GridCommand Icon="Delete" CommandName="Delete" Cls="deleteicon" />
                                                </Commands>
                                                <PrepareToolbar Fn="prepareToolbar" />
                                                <Listeners>
                                                    <Command Fn="RemoveItemLine">
                                                    </Command>
                                                </Listeners>
                                            </ext:CommandColumn>
                                        </Columns>
                                    </ColumnModel>
                                    <SelectionModel>
                                        <ext:RowSelectionModel ID="RowSelectionModel3" runat="server" Mode="Single" />
                                    </SelectionModel>
                                </ext:GridPanel>
                            </td>
                        </tr>
                        <tr>
                            <td valign="bottom" colspan="2">
                                <div class="popupButtonDiv">
                                    <ext:Button runat="server" Cls="btn btn-primary" ID="btnSaveRolloutGroup" ValidationGroup="SaveUpdateGroup"
                                        Text="<i></i>Save">
                                        <DirectEvents>
                                            <Click Timeout="999999" OnEvent="btnSaveRolloutGroup_Click">
                                                <EventMask ShowMask="true" />
                                                 <ExtraParams>

                                                       <ext:Parameter Name="FlowList" Value="Ext.encode(#{gridFlow}.getRowsValues({selectedOnly : false}))"
                                        Mode="Raw" />
                                                     </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                        <Listeners>
                                            <Click Handler="valGroup = 'SaveUpdateGroup'; return CheckValidation();">
                                            </Click>
                                        </Listeners>
                                    </ext:Button>
                                    <div class="btnFlatOr">
                                        or</div>
                                    <ext:LinkButton runat="server" StyleSpec="padding:0px;" Cls="btnFlatLeftGap" ID="btnCancelGroup"
                                        Text="<i></i>Cancel" runat="server">
                                        <Listeners>
                                            <Click Handler="#{WindowGroup}.hide();">
                                            </Click>
                                        </Listeners>
                                    </ext:LinkButton>
                                </div>
                            </td>
                        </tr>
                    </table>
                </Content>
            </ext:Window>

        </div>
    </div>
    <br />
</asp:Content>
