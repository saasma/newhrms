﻿<%@ Page Title="Activities" Language="C#" MasterPageFile="~/Master/NewDetails.master"
    ValidateRequest="false" AutoEventWireup="true" CodeBehind="Activities_4.aspx.cs"
    Inherits="Web.Appraisal.Form.Activities_4" %>

<%@ Register Src="~/Controls/HRAppraisalFormWizard.ascx" TagPrefix="ucHRW" TagName="ucHRFormWizard" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script>
        Ext.onReady(function () {
            CKEDITOR.config.toolbar = [
                ['Styles', 'Format', 'Font', 'FontSize'],

                ['Bold', 'Italic', 'Underline', 'StrikeThrough', '-', 'Undo', 'Redo', '-', 'Cut', 'Copy', 'Paste', 'Find', 'Replace', '-', 'Outdent', 'Indent', '-'],
    
                ['NumberedList', 'BulletedList', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
                ['Image', 'Table', '-', 'Link', 'TextColor', 'BGColor', 'Maximize']
            ];

        });

        function SetEditorValue()
        {
            <%=hdnEditorDescription.ClientID%>.setValue(CKEDITOR.instances.ctl00_ContentPlaceHolder_Main_txtEditorDescription.getData());
          }

    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>Activity Summary
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel" style="margin-top: 0px !important; padding-top: 0px !important;">
        <ext:Hidden runat="server" ID="hdnEditorDescription" />
        <div class="innerLR">
            <div class="separator bottom">
            </div>
            <ucHRW:ucHRFormWizard ID="ucAppraisalWizard" runat="server" />
            <ext:Label ID="lblMsg" runat="server" />

            <table class="fieldTable firsttdskip">
                <td>
                    <ext:Checkbox StyleSpec="margin-top:2px" ID="chkHideBlock" runat="server" BoxLabel="Hide This Block" />
                </td>
                <tr>
                    <td>
                        <ext:TextField ID="txtSectionName" FieldLabel="Section Name *" runat="server" LabelAlign="Top"
                            Width="400" LabelSeparator="">
                        </ext:TextField>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Section Name is required"
                            Display="None" ControlToValidate="txtSectionName" ValidationGroup="SaveFrom"
                            runat="server">
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="components">
                            <div class="grids">
                                <p>
                                    Descriptions</span>
                                </p>
                                <CKEditor:CKEditorControl ExtraPlugins="autogrow" ID="txtEditorDescription" Width="800px"
                                    Height="200px" runat="server" BasePath="~/ckeditor/">
                                </CKEditor:CKEditorControl>
                            </div>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Description is required"
                            Display="None" ControlToValidate="txtEditorDescription" ValidationGroup="SaveFrom"
                            runat="server">
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td>
                                    <ext:ComboBox ForceSelection="true" QueryMode="Local" ID="cmbMarkingType" FieldLabel="Marking Method *"
                                        runat="server" LabelAlign="Top" Width="180" LabelSeparator="">
                                        <Items>
                                            <ext:ListItem Text="No Marking" Value="0">
                                            </ext:ListItem>
                                            <ext:ListItem Text="Rating" Value="1">
                                            </ext:ListItem>
                                            <ext:ListItem Text="Marks" Value="2">
                                            </ext:ListItem>
                                        </Items>
                                        <SelectedItems>
                                            <ext:ListItem Index="0" />
                                        </SelectedItems>
                                        <Listeners>
                                            <Select Handler="if(this.getValue()=='1') {#{cmbRatingScale}.show();#{marksMax}.hide();} else if(this.getValue()=='2') {#{cmbRatingScale}.hide();#{marksMax}.show();} else  {#{cmbRatingScale}.hide();#{marksMax}.hide();}" />
                                        </Listeners>
                                    </ext:ComboBox>
                                </td>
                                <td style="padding-left: 10px">
                                    <ext:ComboBox ForceSelection="true" QueryMode="Local" ID="cmbRatingScale" FieldLabel="Rating Scale *"
                                        runat="server" LabelAlign="Top" DisplayField="Name" Width="180" LabelSeparator=""
                                        ValueField="RatingScaleID">
                                        <Store>
                                            <ext:Store ID="Store1" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="Name" Type="String" />
                                                    <ext:ModelField Name="RatingScaleID" Type="String" />
                                                </Fields>
                                            </ext:Store>
                                        </Store>
                                    </ext:ComboBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Rating Scale is required"
                                        Display="None" ControlToValidate="cmbRatingScale" ValidationGroup="SaveFrom"
                                        runat="server">
                                    </asp:RequiredFieldValidator>
                                    <ext:NumberField ID="marksMax" AllowDecimals="false" Hidden="true" Width="180px" MinValue="1" runat="server"
                                        FieldLabel="Maximum allowed marks" LabelAlign="Top">
                                    </ext:NumberField>
                                </td>
                            </tr>
                        </table>
                        <%--   </td>
                <td>--%>
                    </td>
                </tr>
                <tr>
                    <td style="padding-top: 10px">
                        <ext:Checkbox ID="chkAllowSelfRating" runat="server" BoxLabel="Allow Self Marking" />
                    </td>
                </tr>
            </table>
            <div class="buttonBlock" style="padding-top: 50px">
                <table>
                    <tr>
                        <td>
                            <ext:Button ID="btnSave" Cls="btn btn-primary" runat="server" Text="&nbsp;&nbsp;Save&nbsp;&nbsp;"
                                ValidationGroup="InsertUpdate">
                                <DirectEvents>
                                    <Click OnEvent="ButtonNext_Click">
                                        <EventMask ShowMask="true" />
                                    </Click>
                                </DirectEvents>
                                <Listeners>
                                    <Click Handler="SetEditorValue();valGroup = 'SaveFrom'; return CheckValidation();">
                                    </Click>
                                </Listeners>
                            </ext:Button>
                        </td>
                        <td>&nbsp;&nbsp;&nbsp;
                        </td>
                        <td>
                            <ext:Button ID="btnNext" Cls="btn btn-primary" runat="server" Text="&nbsp;&nbsp;Save and  Next&nbsp;&nbsp;"
                                ValidationGroup="InsertUpdate">
                                <DirectEvents>
                                    <Click OnEvent="ButtonNext_Click">
                                        <EventMask ShowMask="true" />
                                    </Click>
                                </DirectEvents>
                                <Listeners>
                                    <Click Handler="SetEditorValue();valGroup = 'SaveFrom'; return CheckValidation();">
                                    </Click>
                                </Listeners>
                            </ext:Button>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
