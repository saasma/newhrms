﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Entity;
using BLL;
using BLL.BO;
using Utils.Helper;
using Utils.Calendar;


namespace Web.Appraisal.Form
{
    public partial class general_settings_1 : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.EmployeeAppraisal;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();

            }
        }

        public void Initialise()
        {
            if (!string.IsNullOrEmpty(Request.QueryString["fid"]))
            {
                LoadData(int.Parse(Request.QueryString["fid"]));

                int formId = int.Parse(Request.QueryString["fid"]);
                storeForm.DataSource = AppraisalManager.GetAllForms().Where(x => x.AppraisalFormID != formId);
                storeForm.DataBind();

            }
        }

        protected void LoadData(int ID)
        {
            AppraisalForm _AppraisalForm = NewHRManager.GetAppraisal_GeneralInfoByID(ID);
            if (_AppraisalForm != null)
            {
                if (!string.IsNullOrEmpty(_AppraisalForm.Name))
                txtFormName.Text = _AppraisalForm.Name;
                if (!string.IsNullOrEmpty(_AppraisalForm.Description))
                    txtEditorDescription.Text = _AppraisalForm.Description;

                //if (_AppraisalForm.HideRecommendBlock != null)
                //    chkReccommendBlock.Checked = _AppraisalForm.HideRecommendBlock.Value;
                

                btnCopy.Show();
            }

        }

        public void btnSaveCopy_Click(object sender, DirectEventArgs e)
        {

            if (cmbForm.SelectedItem == null || cmbForm.SelectedItem.Value == null)
            {
                NewMessage.ShowWarningMessage("Form must be selected.");
                return;
            }

            int destFormId = int.Parse(Request.QueryString["fid"]);
            int sourceFormId = int.Parse(cmbForm.SelectedItem.Value);

            Status status = AppraisalManager.CopyForm(sourceFormId, destFormId,chkIntroduction.Checked, chkObjective.Checked, chkPerformanceSummary.Checked,
                chkActivity.Checked,
                chkCompetency.Checked,chkTarget.Checked, chkQuestions.Checked, chkSupervisorReview.Checked, chkComments.Checked,
                chkSignature.Checked);

            if (status.IsSuccess == false)
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
            else
            {
                WindowCopy.Hide();
                NewMessage.ShowNormalMessage("Section copied to this form.");
            }

        }

        protected void ButtonNext_Click(object sender, DirectEventArgs e)
        {
            AppraisalForm _AppraisalForm = new AppraisalForm();

            bool isInsert;
            _AppraisalForm.Name = txtFormName.Text.Trim();
            _AppraisalForm.Description = hdnEditorDescription.Text.Trim();
            //_AppraisalForm.HideRecommendBlock = chkReccommendBlock.Checked;
            if (string.IsNullOrEmpty(Request.QueryString["fid"]))
            {
                isInsert = true;
            }
            else
            {
                isInsert = false;
                _AppraisalForm.AppraisalFormID = int.Parse(Request.QueryString["fid"]);
          
            }

            Status respStatus;

            respStatus = NewHRManager.InsertUpdateAppraisalForm_GeneralSetting(_AppraisalForm, isInsert);
            if (respStatus.IsSuccess)
            {
                btnCopy.Show();

                if (sender == btnSave)
                {
                    SetMessage(lblMsg, "Appraisal saved.");

                    Response.Redirect("general_settings_1.aspx?fid=" + _AppraisalForm.AppraisalFormID);
                }
                else
                    Response.Redirect("Introduction_2.aspx?fid=" + _AppraisalForm.AppraisalFormID);
            }
            else
            {
                SetWarning(lblMsg, respStatus.ErrorMessage);
            }
            X.Js.AddScript("window.scrollTo(0,0);");
        }
    }
}