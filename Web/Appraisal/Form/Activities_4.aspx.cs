﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Entity;
using BLL;
using BLL.BO;
using Utils.Helper;
using Utils.Calendar;


namespace Web.Appraisal.Form
{
    public partial class Activities_4 : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();

            }
        }

        public void Initialise()
        {
            cmbRatingScale.Store[0].DataSource = NewHRManager.GetAllRatingScale();
            cmbRatingScale.Store[0].DataBind();
            if (!string.IsNullOrEmpty(Request.QueryString["fid"]))
            {
                LoadData(int.Parse(Request.QueryString["fid"]));
            }
        }

        protected void LoadData(int ID)
        {
            AppraisalForm _AppraisalForm = NewHRManager.GetAppraisal_GeneralInfoByID(ID);
            if (_AppraisalForm != null)
            {
                if (!string.IsNullOrEmpty(_AppraisalForm.ActivityName))
                    txtSectionName.Text = _AppraisalForm.ActivityName;
                if (!string.IsNullOrEmpty(_AppraisalForm.ActivityDescription))
                    txtEditorDescription.Text = _AppraisalForm.ActivityDescription;
                if (_AppraisalForm.ActivityRatingScaleRef_ID != null)
                    ExtControlHelper.ComboBoxSetSelected(_AppraisalForm.ActivityRatingScaleRef_ID.ToString(), cmbRatingScale);

                if (_AppraisalForm.HideActivitySummary != null && _AppraisalForm.HideActivitySummary.Value)
                    chkHideBlock.Checked = _AppraisalForm.HideActivitySummary.Value;

                chkAllowSelfRating.Checked = _AppraisalForm.AllowActivitySelfRating.Value;

                if (_AppraisalForm.ActivityMarkingType != null)
                {
                    ExtControlHelper.ComboBoxSetSelected(_AppraisalForm.ActivityMarkingType.ToString(), cmbMarkingType);

                    if (_AppraisalForm.ActivityMarkingType == (int)AppraisalMarkingTypeEnum.RatingScale)
                    {
                        cmbRatingScale.Show();
                        marksMax.Hide();
                    }
                    else if(_AppraisalForm.ActivityMarkingType == (int)AppraisalMarkingTypeEnum.Marks)
                    {
                        cmbRatingScale.Hide();
                        marksMax.Show();
                    }
                    else
                    {
                        cmbRatingScale.Hide();
                        marksMax.Hide();
                    }

                }
                if (_AppraisalForm.ActivityMaximumMarks != null)
                    marksMax.Number = _AppraisalForm.ActivityMaximumMarks.Value;

            }

        }

        protected void ButtonBack_Click(object sender, DirectEventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["fid"]))
                Response.Redirect("Objectives_3.aspx?fid=" + Request.QueryString["fid"]);
        
        }

        protected void ButtonNext_Click(object sender, DirectEventArgs e)
        {
            AppraisalForm _AppraisalForm = new AppraisalForm();

            _AppraisalForm.ActivityName = txtSectionName.Text.Trim();
            _AppraisalForm.ActivityDescription = hdnEditorDescription.Text.Trim();
            if(!string.IsNullOrEmpty(cmbRatingScale.SelectedItem.Value))
            _AppraisalForm.ActivityRatingScaleRef_ID = int.Parse(cmbRatingScale.SelectedItem.Value);
            _AppraisalForm.AppraisalFormID = int.Parse(Request.QueryString["fid"]);
            _AppraisalForm.AllowActivitySelfRating = chkAllowSelfRating.Checked;

            _AppraisalForm.ActivityMarkingType = int.Parse(cmbMarkingType.SelectedItem.Value);
            _AppraisalForm.ActivityMaximumMarks = (int)marksMax.Number;

            _AppraisalForm.HideActivitySummary = chkHideBlock.Checked;

            Status respStatus;

            respStatus = NewHRManager.UpdateAppraisalForm_Activities(_AppraisalForm);
            if (respStatus.IsSuccess)
            {
                if (sender == btnSave)
                    SetMessage(lblMsg, "Appraisal updated.");
                else
                    Response.Redirect("QualificationExpirenceHistory.aspx?fid=" + Request.QueryString["fid"]);

            }
            else
            {
                SetWarning(lblMsg, respStatus.ErrorMessage);
            }
            X.Js.AddScript("window.scrollTo(0,0);");
        }
    }
}