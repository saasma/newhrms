﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using System.Xml;
using System.IO;
using Utils;
using BLL.BO;   

namespace Web.Appraisal
{
    public partial class RolloutMega : BasePage
    {

        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.EmployeeAppraisal;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!X.IsAjaxRequest)
            {
                Initialise();
              
            }
        }

        public void cmbGradeEmployee_Select(object sender, DirectEventArgs e)
        {

            int employeeId = int.Parse(cmbSearch.SelectedItem.Value);

            LoadDetails(employeeId);

            gridFlow.Show();

            

          
			
			if (cmbForm.SelectedItem == null || cmbForm.SelectedItem.Value == null)
                return;

            int formId = int.Parse(cmbForm.SelectedItem.Value);

            if (AppraisalManager.HasTargetForForm(formId))
            {

                AppraisalPeriod period = AppraisalManager.GetPeriodByID(int.Parse(cmbPeriod.SelectedItem.Value));

                List<AppraisalFormTarget> formTargets = NewHRManager.GetAppraisalFromTargetsForRollOut(formId, employeeId, period.StartDate.Value, period.EndDate.Value);
                foreach (AppraisalFormTarget item in formTargets)
                {
                    item.Name = NewHRManager.GetTargetName(item.TargetRef_ID);
                }

                StoreTargets.DataSource = formTargets;
                StoreTargets.DataBind();

                gridTargets.Show();

               
            }
            else
            {
                gridTargets.Hide();
            }


            // load appraisal flow
            LoadFlow();
        }
        public void LoadFlowForEdit(AppraisalRollout rollout)
        {

            int empId = int.Parse(cmbSearch.SelectedItem.Value);
            int periodId = int.Parse(cmbPeriod.SelectedItem.Value);
            int formId = rollout.FormID.Value;

            AppraisalEmployeeForm form = AppraisalManager.GetEmployeeAppraisaleForm(empId, formId);
            //List<TextValue> additionSteps = TravelAllowanceManager.GetAdditionalSteps((int)FlowTypeEnum.Appraisal);
            List<ApprovalFlow> authorityTypeNameList = TravelAllowanceManager.getAuthorityTypeListForCombo((int)FlowTypeEnum.Appraisal);
            List<AppraisalEmployeeFlow> flowList = new List<AppraisalEmployeeFlow>();

            if(form != null)
            flowList = AppraisalManager.GetFlowList(form.AppraisalEmployeeFormID);

            List<ApprovalFlow> list = TravelAllowanceManager.GetDefaultApprovalFlowList((int)FlowTypeEnum.Appraisal);

            // remove steps not saved
            for (int i = list.Count - 1; i >= 0; i--)
            {
                if (flowList.Any(x => x.StepRef_ID == list[i].StepID) == false)
                {
                    list.RemoveAt(i);
                }
            }

            List<ApprovalFlow> stepIDList = TravelAllowanceManager.getTravelOrderListForCombo();

            foreach (ApprovalFlow item in list)
            {
                ApprovalFlow authorityName = authorityTypeNameList.FirstOrDefault(x => x.AuthorityType == item.AuthorityType);
                item.AuthorityTypeName = authorityName == null ? "" : authorityName.AuthorityTypeName;

                

                AppraisalEmployeeFlow flow = flowList.FirstOrDefault(x => x.StepRef_ID == item.StepID);

                if (flow != null && flow.EmployeeID != null)
                    item.EmployeeId = flow.EmployeeID;
                if (flow.ShowAdditionalStep != null)
                    item.ShowAdditionalStep = flow.ShowAdditionalStep;
                else
                    item.ShowAdditionalStep = null;

                ApprovalFlow stepID = stepIDList.FirstOrDefault(x => x.StepID == item.StepID);
                if (stepID != null)
                    item.StepIDName = stepID.StepName;

                if (flow != null)
                {
                    item.StepName = flow.StepName;
                    item.AuthorityTypeDisplayName
                         = flow.AuthorityTypeDisplayName;
                }
                //if (item.ShowAdditionalStep != null)
                //{
                //    TextValue value = additionSteps.FirstOrDefault(x => x.Value == item.ShowAdditionalStep.ToString());
                //    if (value != null)
                //        item.AdditionStep = value.Text;
                //}

                // allow to delete after 3rd step
                if (item.StepID != (int)FlowStepEnum.Step1SaveAndSend && item.StepID != (int)FlowStepEnum.Step2
                    && item.StepID != (int)FlowStepEnum.Step15End)
                {
                    item.IsDeletable = true;
                }
            }

            gridFlow.Store[0].DataSource = list;
            gridFlow.Store[0].DataBind();

            gridFlow.Show();

           

        }
        public void LoadFlow()
        {
            if (cmbSearch.SelectedItem == null || cmbSearch.SelectedItem.Value == null)
                return;

            if (cmbPeriod.SelectedItem == null || cmbPeriod.SelectedItem.Value == null)
                return;

            int empId = int.Parse(cmbSearch.SelectedItem.Value);
            int periodId = int.Parse(cmbPeriod.SelectedItem.Value);

            List<ApprovalFlow> authorityTypeNameList = TravelAllowanceManager.getAuthorityTypeListForCombo((int)FlowTypeEnum.Appraisal);
            List<TextValue> additionSteps = TravelAllowanceManager.GetAdditionalSteps((int)FlowTypeEnum.Appraisal);
            List<ApprovalFlow> list = TravelAllowanceManager.GetDefaultApprovalFlowList((int)FlowTypeEnum.Appraisal);
            LeaveApprovalEmployee leaveTeam = LeaveRequestManager.GetEmployeeTeam(empId);
            List<ApprovalFlow> stepIDList = TravelAllowanceManager.getTravelOrderListForCombo();
            foreach (ApprovalFlow item in list)
            {
                ApprovalFlow authorityName = authorityTypeNameList.FirstOrDefault(x=>x.AuthorityType == item.AuthorityType);
                item.AuthorityTypeName = authorityName == null ? "" : authorityName.AuthorityTypeName;

                TravelOrderAuthorityEnum authority = (TravelOrderAuthorityEnum)item.AuthorityType;
                switch (authority)
                {
                    case TravelOrderAuthorityEnum.Employee:
                        item.EmployeeId = empId;
                        break;
                    case TravelOrderAuthorityEnum.Recommend1:
                        if (leaveTeam != null && leaveTeam.CC4 != null)
                            item.EmployeeId = leaveTeam.CC4;
                        break;
                    case TravelOrderAuthorityEnum.Approval1:
                        if (leaveTeam != null && leaveTeam.ApplyTo != null)
                            item.EmployeeId = leaveTeam.ApplyTo;
                        break;
                    case TravelOrderAuthorityEnum.SpecificPerson:
                        if (item.Person1ID != null)
                            item.EmployeeId = item.Person1ID;
                        else if (item.Person2ID != null)
                            item.EmployeeId = item.Person2ID;
                        break;
                    case TravelOrderAuthorityEnum.DepartmentHead:

                        item.EmployeeId = TravelAllowanceManager.GetEmployeeCurrentDepartmentHead(empId);

                        break;
                }


                ApprovalFlow stepID = stepIDList.FirstOrDefault(x => x.StepID == item.StepID);
                if (stepID != null)
                    item.StepIDName = stepID.StepName;


                //if (item.ShowAdditionalStep != null)
                //{
                //    TextValue value = additionSteps.FirstOrDefault(x => x.Value == item.ShowAdditionalStep.ToString());
                //    if (value != null)
                //        item.AdditionStep = value.Text;
                //}

                // allow to delete after 3rd step
                if (item.StepID != (int)FlowStepEnum.Step1SaveAndSend && item.StepID != (int)FlowStepEnum.Step2
                    && item.StepID != (int)FlowStepEnum.Step15End)
                {
                    item.IsDeletable = true;
                }
            }

            gridFlow.Store[0].DataSource = list;
            gridFlow.Store[0].DataBind();

            gridFlow.Show();

               //new ApprovalFlow{AuthorityType= (int)TravelOrderAuthorityEnum.DepartmentHead, AuthorityTypeName = "Department Head"},
               //     new ApprovalFlow{AuthorityType= (int)TravelOrderAuthorityEnum.Employee, AuthorityTypeName = "Employee"},
               //     new ApprovalFlow{AuthorityType= (int)TravelOrderAuthorityEnum.Recommend1, AuthorityTypeName = "Recommend 1"},
               //     new ApprovalFlow{AuthorityType= (int)TravelOrderAuthorityEnum.SpecificPerson, AuthorityTypeName = "Specific Person"},

            // supervisor
            //LeaveApprovalEmployee leaveTeam = LeaveRequestManager.GetEmployeeTeam(emp.EmployeeId);
            //if (leaveTeam != null)
            //{
            //    if (leaveTeam.CC4 != null)
            //        cmbSupervisor.SetValue(leaveTeam.CC4.ToString());
            //    else
            //        cmbSupervisor.ClearValue();

            //    //if (leaveTeam.ApplyTo != null)
            //    //    cmbReviewer.SetValue(leaveTeam.ApplyTo.ToString());
            //    //else
            //    //    cmbReviewer.ClearValue();
            //}
            //else
            //{
            //    cmbSupervisor.ClearValue();
            //    //cmbReviewer.ClearValue();
            //}



        }
        public void cmbPeriod_Change(object sender, DirectEventArgs e)
        {
            int periodId = int.Parse(cmbPeriod.SelectedItem.Value);
            AppraisalPeriod period = AppraisalManager.GetPeriodByID(periodId);

            dateFrom.SelectedDate = period.StartDate.Value;
            dateTo.SelectedDate = period.EndDate.Value;
            dateDueDate.SelectedDate = period.DueDate.Value;

            LoadFlow();
            //displaying EducationLocation Score--
            int employeeId = int.Parse(cmbSearch.SelectedItem.Value);
            AppraisalEmployeeEducationLocationScore _dbScore =  AppraisalManager.GetAppraisalEducationLocationScore(employeeId, periodId);
            if (_dbScore != null)
            {
                X.Js.AddScript("document.getElementById('divEduLocationScore').style.display='block';");
                //if (_dbScore.EducationScore>0.0)
                    spnEducationScore.Text = _dbScore.EducationScore.ToString();
                //if (_dbScore.LocationScore != null)
                    spnLocationScore.Text = _dbScore.LocationScore.ToString();
                //if (_dbScore.SeniorityScore != null)
                    spnSeniorityScore.Text = _dbScore.SeniorityScore.ToString();
            }
             
            //----------------------------------
        }

        public void cmbForm_Change(object sender, DirectEventArgs e)
        {
            int formId = int.Parse(cmbForm.SelectedItem.Value);
            if (cmbSearch.SelectedItem == null || cmbSearch.SelectedItem.Value == null)
                return;

            if (cmbPeriod.SelectedItem == null || cmbPeriod.SelectedItem.Value == null)
            {
                cmbForm.ClearValue();
                NewMessage.ShowWarningMessage("Period should be selected first.");
                return;
            }


            int empId  = int.Parse(cmbSearch.SelectedItem.Value);
            if (AppraisalManager.HasTargetForForm(formId))
            {

                AppraisalPeriod period = AppraisalManager.GetPeriodByID(int.Parse(cmbPeriod.SelectedItem.Value));

                List<AppraisalFormTarget> formTargets = NewHRManager.GetAppraisalFromTargetsForRollOut(formId, empId,period.StartDate.Value,period.EndDate.Value);
                foreach (AppraisalFormTarget item in formTargets)
                {
                    item.Name = NewHRManager.GetTargetName(item.TargetRef_ID);
                }

                StoreTargets.DataSource = formTargets;
                StoreTargets.DataBind();

                AppraisalForm form = AppraisalManager.getFormInstanceByID(formId);
                if (form.TargetHideAssignedShowRating != null && form.TargetHideAssignedShowRating.Value)
                {
                    ColumnWeight.Show();
                    ColumnTargetText.Show();
                    ColumnAchievementText.Show();

                    ColumnTargetValue.Hide();
                    ColummAchievementValue.Hide();
                }
                else
                {
                    ColumnWeight.Hide();
                    ColumnTargetText.Hide();
                    ColumnAchievementText.Hide();

                    ColumnTargetValue.Show();
                    ColummAchievementValue.Show();
                }


                gridTargets.Show();
            }
            else
            {
                gridTargets.Hide();
            }
        }

        public void LoadEmployeeDetails(int employeeId)
        {

            EEmployee emp = new EmployeeManager().GetById(employeeId);
           

            title.Html = emp.Title + " " + emp.Name + " - " + emp.EmployeeId;
            // Designation/Position
            if (emp.EDesignation != null)
                positionDesignation.Text = emp.EDesignation.Name;
            // Group/Level
            if (emp.PEmployeeIncomes.Count > 0 && emp.PEmployeeIncomes[0].LevelId != null)
            {
                BLevel level = NewPayrollManager.GetLevelById(emp.PEmployeeIncomes[0].LevelId.Value);
                groupLevel.Text = level.BLevelGroup.Name + " (Level - " + level.Name + ")";
            }
            else
                groupLevelImg.Visible = false;

          
            // Branch
            GetEmployeeCurrentBranchDepartmentResult currentBranchDep = EmployeeManager.GetEmployeeCurrentBranchDepartment(emp.EmployeeId);
            if (currentBranchDep != null)
            {
                branch.Text = currentBranchDep.Department + ", " + currentBranchDep.Branch;
            }

            // Contact
            //if (emp.EAddresses.Count > 0)
            //{
            //    EAddress entity = emp.EAddresses[0];
            //    if (!string.IsNullOrEmpty(entity.CIPhoneNo) || !string.IsNullOrEmpty(entity.CIMobileNo) || !string.IsNullOrEmpty(entity.Extension))
            //        contact.InnerHtml += string.Format("{0} / {1} ({2})", entity.CIPhoneNo, entity.CIMobileNo, entity.Extension);
            //    email.InnerHtml += string.Format("<a href='mailto:{0}'>{0}</a> ", entity.CIEmail);
            //}

            // Working for since
            int years, months, days, hours;
            int minutes, seconds, milliseconds;
            workingFor.Text = "";
            ServicePeroid firstStatus = EmployeeManager.GetServicePeroidDate(employeeId);
            if (firstStatus != null)
            {
                NewHelper.GetElapsedTime(firstStatus.FromDateEng, BLL.BaseBiz.GetCurrentDateAndTime(), out years, out months, out days, out hours, out minutes, out seconds, out milliseconds);
                string text = "";
                if (years != 0)
                    text = " " + years + " years";
                if (months != 0)
                    text += " " + months + " months";
                if (days != 0)
                    text += " " + days + " days";

                workingFor.Text += text;

                workingFor.Text += ", Since " +
                    (IsEnglish ? firstStatus.FromDateEng.ToShortDateString() : firstStatus.FromDate + " (" + firstStatus.FromDateEng.ToShortDateString() + ")");

            }


            // Photo
            bool hasPhoto = false;
            if (emp.HHumanResources.Count > 0 && !string.IsNullOrEmpty(emp.HHumanResources[0].UrlPhoto))
            {
                if (!string.IsNullOrEmpty(emp.HHumanResources[0].UrlPhotoThumbnail))
                {
                    image.ImageUrl = Page.ResolveUrl(Config.UploadLocation + "/" + emp.HHumanResources[0].UrlPhotoThumbnail);
                    if (File.Exists(Server.MapPath(image.ImageUrl)))
                        hasPhoto = true;
                    image.Attributes.Remove("width");
                    image.Attributes.Remove("height");
                }
                else
                {
                    image.ImageUrl = Page.ResolveUrl(Config.UploadLocation + "/" + emp.HHumanResources[0].UrlPhoto);
                    if (File.Exists(Server.MapPath(image.ImageUrl)))
                        hasPhoto = true;
                }
            }
            if (!hasPhoto)
            {
                image.ImageUrl = Page.ResolveUrl(Web.Helper.WebHelper.GetEmployeeNoImage(emp.EmployeeId));
            }
        }
        private void LoadDetails(int employeeId)
        {

            EEmployee emp = EmployeeManager.GetEmployeeById(employeeId);

            //dispBranch.Text = emp.Branch.Name;
            //dispDepartment.Text = emp.Department.Name;
            LoadEmployeeDetails(employeeId);

            X.Js.AddScript("document.getElementById('empDetailsBlock').style.display='';");

            X.Js.AddScript("document.getElementById('row0').style.display='';");
            X.Js.AddScript("document.getElementById('row1').style.display='';");
            X.Js.AddScript("document.getElementById('row2').style.display='';");
            X.Js.AddScript("document.getElementById('row3').style.display='';");
            //X.Js.AddScript("document.getElementById('row4').style.display='';");

            //X.Js.AddScript("document.getElementById('row5').style.display='';");
            //X.Js.AddScript("document.getElementById('row6').style.display='';");
            X.Js.AddScript("document.getElementById('row7').style.display='';");
            X.Js.AddScript("document.getElementById('row8').style.display='';");

            X.Js.AddScript("document.getElementById('divBlocks').style.display='';");
           

            //divEduLocationScore.Style["display"] = "block";
            EmployeeManager empMgr = new EmployeeManager();
            lblStatus.Html = empMgr.GetAllCurrentStatusForAppraisal(emp.EmployeeId);
            EducationCtl.ShowEmpty = true;
            EducationCtl.Initialise(emp.EmployeeId);

            // salary details

            //lblSalaryDetails.Html = new PayManager().GetHTMLSalaryDetailsForAppraisal(emp.EmployeeId);

            // branch Transfer details
            gridTransferList.Store[0].DataSource = BranchManager.GetTransferListForAppraisal(emp.EmployeeId);
            gridTransferList.Store[0].DataBind();

            List<HTraining> _HTraining = NewHRManager.GetTrainingByEmployeeID(emp.EmployeeId);
            foreach (HTraining item in _HTraining)
            {
                if (item.TrainingFromEngDate != null)
                {
                    item.TrainingFrom = item.TrainingFromEngDate.Value.ToShortDateString();
                }
                else
                    item.TrainingFrom = "";
            }
            this.Store2.DataSource = _HTraining;
            this.Store2.DataBind();



        }

        public void Page_PreRender(object sender, EventArgs e)
        {
             string rolloutID = Request.QueryString["id"];
             if (!string.IsNullOrEmpty(rolloutID))
             {
                 AppraisalRollout rollout = AppraisalManager.GetRolloutByFormID(int.Parse(rolloutID));
                 

                 EmployeeManager empMgr = new EmployeeManager();
                 //lblStatus.Html = empMgr.GetAllCurrentStatusForAppraisal(emp.EmployeeId);
                 EducationCtl.ShowEmpty = true;
                 if (rollout.EmployeeId != null)
                     EducationCtl.Initialise(rollout.EmployeeId.Value);
             }
        }
        public void Initialise()
        {

            if (!string.IsNullOrEmpty(Request.QueryString["type"]))
            {
                SetMessage(lblMsg, "Rollout saved for the employee.");
            }

            cmbForm.Store[0].DataSource = AppraisalManager.GetAllForms();
            cmbForm.Store[0].DataBind();

            storeAdditionalStep.DataSource = TravelAllowanceManager.GetAdditionalSteps((int)FlowTypeEnum.Appraisal);
            storeAdditionalStep.DataBind();


            cmbPeriod.Store[0].DataSource = AppraisalManager.GetAllPeriods();
            cmbPeriod.Store[0].DataBind();


            storeEmployee.DataSource = EmployeeManager.GetAllEmployeesForEmployeeImportExport().OrderBy(x => x.Name).ToList();
            storeEmployee.DataBind();

            string rolloutID = Request.QueryString["id"];
            if (!string.IsNullOrEmpty(rolloutID))
            {
                

                hiddenValueRow.Text = rolloutID;

                cmbSearch.ClearValue();

                AppraisalRollout rollout = AppraisalManager.GetRolloutByFormID(int.Parse(rolloutID));
                EmployeeManager empMgr = new EmployeeManager();

                if (rollout.EmployeeId != null)
                {
                    cmbSearch.Disable(true);

                    Ext.Net.ListItem cItem = new Ext.Net.ListItem();
                    cItem.Text = empMgr.GetById(rollout.EmployeeId.Value).Name;
                    cItem.Value = rollout.EmployeeId.ToString();
                    cItem.Index = 0;

                    cmbSearch.Items.Add(cItem);
                    cmbSearch.SelectedItems.Add(cItem);
                    //ExtControlHelper.ComboBoxSetSelected(cItem.Value, cmbSearch);

                    cmbSearch.Text = cItem.Text;

                    LoadDetails(rollout.EmployeeId.Value);
                }
                else
                {
                    storeSearch.Proxy.Clear();
                    cmbSearch.PageSize = 0;
                    cmbSearch.HideBaseTrigger = false;

                    storeSearch.DataSource = AppraisalManager.GetRolloutEmployeeList(int.Parse(rolloutID));
                    storeSearch.DataBind();

                    gridTargets.Hide();

                    btnSaveUpdateNew.Hide();

                    


                    titleText.InnerHtml += " : " + rollout.AppraisalForm.Name;
                    //cmbSearch.Disable(false);
                }

                LoadEditData(rollout);

                cmbForm.Disable(true);
            }

            //LoadFlow();

            
        }
        
        private void LoadLevels()
        {
            
         



        }


      
        protected void btnAddLevel_Click(object sender, DirectEventArgs e)
        {
            hiddenValueRow.Text = "";
           


        }


        public void LoadEditData(AppraisalRollout rollout)
        {
            dateFrom.SelectedDate = rollout.FromDate.Value;
            dateTo.SelectedDate = rollout.ToDate.Value;
            if (rollout.SubmissionDate != null)
                dateDueDate.SelectedDate = rollout.SubmissionDate.Value;

            ExtControlHelper.ComboBoxSetSelected(rollout.FormID.ToString(), cmbForm);

            //txtComment.Text = rollout.EducationDetails;

            //ExtControlHelper.ComboBoxSetSelected(rollout.Step5ChiefEmployeeId.ToString(), cmbChief);

           


            ExtControlHelper.ComboBoxSetSelected(rollout.PeriodId, cmbPeriod);

            cmbPeriod.Disable(true);

            if (AppraisalManager.HasTargetForForm(rollout.FormID.Value))
            {

                cmbForm_Change(null, null);



                gridTargets.Show();
            }
            else
                gridTargets.Hide();

            txtNotes.Text = rollout.Notes;

            LoadFlowForEdit(rollout);

            btnLevelSaveUpdate.Text = "Update";

        }

        protected void btnDeleteLevel_Click(object sender, DirectEventArgs e)
        {
            int RowID = int.Parse(hiddenValueRow.Text.Trim());
            Status status = AppraisalManager.DeleteRolloutByID(RowID);
            if (status.IsSuccess)
            {
                LoadLevels();
                NewMessage.ShowNormalMessage("Rollout deleted.");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }


     

       



        


        protected void btnLevelSaveUpdate_Click(object sender, DirectEventArgs e)
        {

            
            Page.Validate("InsertUpdateLevel");
            if (Page.IsValid)
            {
               


                AppraisalRollout inst = new AppraisalRollout();
               
                bool isEdit=false;
                if (!string.IsNullOrEmpty(hiddenValueRow.Text))
                {
                    isEdit = true;
                    inst.RolloutID = int.Parse(hiddenValueRow.Text);
                }
                inst.EmployeeId = int.Parse(cmbSearch.SelectedItem.Value);
                inst.FromDate = dateFrom.SelectedDate;
                inst.ToDate = dateTo.SelectedDate;
                inst.PeriodId = int.Parse(cmbPeriod.SelectedItem.Value);
                inst.SubmissionDate = dateDueDate.SelectedDate;
                if (inst.ToDate < inst.FromDate)
                {
                    SetWarning (lblMsg, "From date can not be after to date.");
                    return;
                }

                inst.FormID = int.Parse(cmbForm.SelectedItem.Value);
                //inst.EducationDetails = txtComment.Text.Trim();

                //AppraisalRollout dbRollout = AppraisalManager.GetRolloutByFormID(inst.RolloutID);
                //if (dbRollout != null)
                //{
                //    int empId = 0;
                //    //as in submit Combobox employee is not retriving for first two if value is not changed
                //    if (int.TryParse(cmbSupervisor.SelectedItem.Value, out empId))
                //        inst.Step2SupervisorEmployeeId = int.Parse(cmbSupervisor.SelectedItem.Value);
                //    else
                //        inst.Step2SupervisorEmployeeId = dbRollout.Step2SupervisorEmployeeId;
                //}
                //else
                //{
                //    inst.Step2SupervisorEmployeeId = int.Parse(cmbSupervisor.SelectedItem.Value);
                //}
                
                
                
                
                inst.Notes = txtNotes.Text.Trim();

              
                // Target value change
                string entryLineJson = e.ExtraParams["TargetList"];
                List<AppraisalEmployeeFormTarget> targetList = JSON.Deserialize<List<AppraisalEmployeeFormTarget>>(entryLineJson)
                    //.Where(x => x.DefaultTargetValue != 0)
                    .ToList();
                foreach (AppraisalEmployeeFormTarget item in targetList)
                {
                    item.SelfAchievementValue = item.Achievement;
                    item.AssignedTargetValue = item.DefaultTargetValue;

                    item.Weight = item.NewWeight;
                    item.AssignedTargetTextValue = item.TargetTextValue;
                    item.AchievementTextValue = item.AchievementTextValue;
                }

                // Flow list
                string flowJson = e.ExtraParams["FlowList"];
                List<ApprovalFlow> flowList = JSON.Deserialize<List<ApprovalFlow>>(flowJson).ToList();


                Status status = new Status();

                if (isEdit == false)
                    status = AppraisalManager.InsertMegaRollout(inst, isEdit, targetList, flowList);
                else
                    status = AppraisalManager.UpdateMegaRollout(inst, dateDueDate.SelectedDate, targetList, flowList);

                if (status.IsSuccess)
                {
                    //WindowLevel.Hide();
                    //LoadLevels();
                    //NewMessage.ShowNormalMessage("Rollout saved.");
                    if (isEdit == false)
                        Response.Redirect("RolloutMega.aspx?type=1&ID=" + inst.RolloutID);
                    else
                    {
                        SetMessage(lblMsg, "Rollout updated.");
                        X.Js.AddScript("window.scrollTo(0,0);");
                    }
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }
                
            }
             
             
        }

        




        
    }
}