﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using System.Xml;
using BLL.BO;

namespace Web.Appraisal
{
    public partial class PerformanceSummary : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();

            }
        }

        public void Initialise()
        {
            if (!string.IsNullOrEmpty(Request.QueryString["fid"]))
            {
                LoadData(int.Parse(Request.QueryString["fid"]));
            }
        }

        protected void LoadData(int ID)
        {
            AppraisalForm _AppraisalForm = NewHRManager.GetAppraisal_GeneralInfoByID(ID);
            if (_AppraisalForm != null)
            {
                if (!string.IsNullOrEmpty(_AppraisalForm.PerformanceSummaryName))
                    txtSectionName.Text = _AppraisalForm.PerformanceSummaryName;
                if (!string.IsNullOrEmpty(_AppraisalForm.PerformanceSummaryDescription))
                    txtEditorDescription.Text = _AppraisalForm.PerformanceSummaryDescription;

                

                if (_AppraisalForm.HideRecommendBlock != null && _AppraisalForm.HideRecommendBlock.Value)
                    chkReccommendBlock.Checked = _AppraisalForm.HideRecommendBlock.Value;

                LoadDistribution(_AppraisalForm);


                LoadPoints(_AppraisalForm);
            }

            
        }

        public void LoadDistribution(AppraisalForm _AppraisalForm)
        {
            //if (_AppraisalForm.PerformanceCoreValue != null)
            //    txtPerformanceCoreValue.Text = decimal.Parse(_AppraisalForm.PerformanceCoreValue.ToString()).ToString("0.00");
            //if (_AppraisalForm.PerformanceQuestionnaireValue != null)
            //    txtPerformanceQuestionnaireValue.Text = decimal.Parse(_AppraisalForm.PerformanceQuestionnaireValue.ToString()).ToString("0.00");
            //if (_AppraisalForm.PerformanceActivityValue != null)
            //    txtPerformanceActivityValue.Text = decimal.Parse(_AppraisalForm.PerformanceActivityValue.ToString()).ToString("0.00");
            //if (_AppraisalForm.PerformanceTargetValue != null)
            //    txtPerformanceTargetValue.Text = decimal.Parse(_AppraisalForm.PerformanceTargetValue.ToString()).ToString("0.00");


            decimal PerformanceTarget = decimal.Parse((txtPerformanceTargetValue.Text == "") ? "0" : txtPerformanceTargetValue.Text.ToString());
            decimal PerformanceCore = decimal.Parse((txtPerformanceCoreValue.Text == "") ? "0" : txtPerformanceCoreValue.Text.ToString());
            decimal PerformanceQuestionnaireValue = decimal.Parse((txtPerformanceQuestionnaireValue.Text == "") ? "0" : txtPerformanceQuestionnaireValue.Text.ToString());
            decimal PerformanceReviewValue = decimal.Parse((txtPerformanceActivityValue.Text == "") ? "0" : txtPerformanceActivityValue.Text.ToString());

            lblTotal.Text = (PerformanceCore + PerformanceQuestionnaireValue + PerformanceReviewValue + PerformanceTarget).ToString() + "%";


            List<AppraisalFormDistribution> distribution =
                    BLL.BaseBiz.PayrollDataContext.AppraisalFormDistributions.Where(x => x.AppraisalFormRef_ID == _AppraisalForm.AppraisalFormID).OrderBy(x => x.Order).ToList();

            AppraisalFormDistribution total = new AppraisalFormDistribution { Name = "<b>Total</b>", BlockType=-1, Percent = 0 };

            foreach (var item in distribution)
            {
                if (item.BlockType == (int)AppraisalBlockType.Activity)
                    item.Name = "Activity Values";
                else if (item.BlockType == (int)AppraisalBlockType.Competency)
                    item.Name = "Competencies Values";
                else if (item.BlockType == (int)AppraisalBlockType.Target)
                    item.Name = "Target Values";
                else if (item.BlockType == (int)AppraisalBlockType.Question)
                    item.Name = "Questionnaire Values";

                else if (item.BlockType == (int)AppraisalBlockType.QualificationAndEducation)
                    item.Name = "Education Values";
                else if (item.BlockType == (int)AppraisalBlockType.TransferHistory)
                    item.Name = "Transfer History Values";
                else if (item.BlockType == (int)AppraisalBlockType.LevelDesignationWorkHistory)
                    item.Name = "Work History Values";

                total.Percent += Convert.ToDecimal(item.Percent);
            }

            // in case of distriubtion not saved in the table
            if (distribution.Count <= 0)
            {
                distribution = new List<AppraisalFormDistribution>();

                distribution.Add(new AppraisalFormDistribution { Name = "Activity Values", BlockType = (int)AppraisalBlockType.Activity,
                        Percent = Convert.ToDecimal(_AppraisalForm.PerformanceActivityValue) });
                distribution.Add(new AppraisalFormDistribution { Name = "Competencies Values", BlockType = (int)AppraisalBlockType.Competency,
                    Percent = Convert.ToDecimal(_AppraisalForm.PerformanceCoreValue)});

                distribution.Add(new AppraisalFormDistribution { Name = "Target Values", BlockType = (int)AppraisalBlockType.Target,
                    Percent = Convert.ToDecimal(_AppraisalForm.PerformanceTargetValue)
                });
                distribution.Add(new AppraisalFormDistribution { Name = "Questionnaire Values", BlockType = (int)AppraisalBlockType.Question,
                    Percent = Convert.ToDecimal(_AppraisalForm.PerformanceQuestionnaireValue)
                });

                total.Percent = Convert.ToDecimal(_AppraisalForm.PerformanceActivityValue) + Convert.ToDecimal(_AppraisalForm.PerformanceCoreValue)
                    + Convert.ToDecimal(_AppraisalForm.PerformanceTargetValue) + Convert.ToDecimal(_AppraisalForm.PerformanceQuestionnaireValue);


                distribution.Add(new AppraisalFormDistribution { Name = "Education Qualification", BlockType = (int)AppraisalBlockType.QualificationAndEducation, });
                distribution.Add(new AppraisalFormDistribution { Name = "Transfer History", BlockType = (int)AppraisalBlockType.TransferHistory, });
                distribution.Add(new AppraisalFormDistribution { Name = "Work History", BlockType = (int)AppraisalBlockType.LevelDesignationWorkHistory, });
            }


            distribution.Add(total);

            gridDistribution.Store[0].DataSource = distribution;
            gridDistribution.Store[0].DataBind();
        }

        public void LoadPoints(AppraisalForm form)
        {
            //List<AppraisalFormPoint> points = AppraisalManager.GetPoints(form.AppraisalFormID);

            List<AppraisalFormPoint> empPoints = new List<AppraisalFormPoint>();
            //List<AppraisalFormPoint> supPoints = new List<AppraisalFormPoint>();

            List<AppraisalFormPoint> emp = new List<AppraisalFormPoint>();
            //List<AppraisalFormPoint> sup = new List<AppraisalFormPoint>();

            // Activity
            AppraisalFormPoint activityEmp = new AppraisalFormPoint { Name = "Activity", IsParent = true,IsEditable=true };
            empPoints.Add(activityEmp);
            //AppraisalFormPoint activitySup = new AppraisalFormPoint { Name = "Activity", IsParent = true, IsEditable = true };
            //supPoints.Add(activitySup);

            // add Competencies
            List<AppraisalCategory> comptencyCategoryList = NewHRManager.GetAppraisalFromCompetencyCategory(form.AppraisalFormID);
           
            AppraisalFormPoint comptencyCatEmp = new AppraisalFormPoint { Name = "Competencies",ShowEmptyInPointColumn=true, IsParent = true };
            empPoints.Add(comptencyCatEmp);
            //AppraisalFormPoint comptencyCatSup = new AppraisalFormPoint { Name = "Competencies", IsParent = true };
            //supPoints.Add(comptencyCatSup);

            int index = 1;
            double total = 0;
            foreach (AppraisalCategory competencyCategory in comptencyCategoryList)
            {


                AppraisalFormPoint compEmp = new AppraisalFormPoint
                {
                    Name = index + ". " + competencyCategory.Name,
                    Type = (int)AppraisalBlockType.Competency,
                    SourceId = competencyCategory.CategoryID,
                    Points = competencyCategory.Weight,
                    IsEditable = true
                };

                total += competencyCategory.Weight;

                empPoints.Add(compEmp);
                //supPoints.Add(compSup);

                index++;
            }

            AppraisalFormPoint comptencyTotalCatEmp = new AppraisalFormPoint { Name = "Total", Points=total, IsParent = false, IsCompetencyTotal = true };
            empPoints.Add(comptencyTotalCatEmp);
            //AppraisalFormPoint comptencyTotalCatSup = new AppraisalFormPoint { Name = "Total", IsParent = false, IsCompetencyTotal = true };
            //supPoints.Add(comptencyTotalCatSup);


            // add Quistionate
            List<AppraisalFormQuestionnaireCategory> categoryList = AppraisalManager.GetQuestionCategoryList(form.AppraisalFormID);
            index = 1;

            AppraisalFormPoint questionCatEmp = new AppraisalFormPoint { Name = "Questionnaire",ShowEmptyInPointColumn=true, IsParent = true };
            empPoints.Add(questionCatEmp);
            //AppraisalFormPoint questionCatSup = new AppraisalFormPoint { Name = "Questionnaire", IsParent = true };
            //supPoints.Add(questionCatSup);

            total = 0;
            foreach (AppraisalFormQuestionnaireCategory category in categoryList)
            {
                AppraisalFormPoint compEmp = new AppraisalFormPoint
                {
                    Name = index + ". " + category.Name,
                    Type = (int)AppraisalBlockType.Question,
                    SourceId = category.CategoryID,
                    Points = category.Weight == null ? 0 : category.Weight.Value,
                    IsEditable = true
                };
                //AppraisalFormPoint compSup = new AppraisalFormPoint
                //{
                //    Name = index + ". " + category.Name,
                //    Type = (int)AppraisalBlockType.Question,
                //    SourceId = category.CategoryID,
                //    IsEditable = true
                //};
                total += (category.Weight == null ? 0 : category.Weight.Value);
                empPoints.Add(compEmp);
                //supPoints.Add(compSup);

                index++;
            }

            AppraisalFormPoint TotalCatEmp = new AppraisalFormPoint { Name = "Total", Points=total, IsParent = false, IsQuestionaireTotal = true };
            empPoints.Add(TotalCatEmp);
            //AppraisalFormPoint TotalCatSup = new AppraisalFormPoint { Name = "Total", IsParent = false, IsQuestionaireTotal = true };
            //supPoints.Add(TotalCatSup);


            // Targets
            List<AppraisalDynamicFormTarget> targetList = AppraisalManager.GetFormTarget(form.AppraisalFormID);
            total = targetList.Sum(x => x.Weight);
            AppraisalFormPoint targetEmp = new AppraisalFormPoint { Name = "Target",Points=total, IsParent = true, IsEditable = true };
            empPoints.Add(targetEmp);
            //AppraisalFormPoint targetSup = new AppraisalFormPoint { Name = "Target", IsParent = true, IsEditable = true };
            //supPoints.Add(targetSup);

            empRatingScore.Store[0].DataSource = empPoints;
            empRatingScore.Store[0].DataBind();

            //gridSupRating.Store[0].DataSource = supPoints;
            //gridSupRating.Store[0].DataBind();

            List<AppraisalFormGrading> dbGradeList =
                BLL.BaseBiz.PayrollDataContext.AppraisalFormGradings.Where(x => x.AppraisalFormRef_ID == form.AppraisalFormID)
                .OrderBy(x => x.Order).ToList();

            if (dbGradeList.Count <= 0)
            {
                dbGradeList = new List<AppraisalFormGrading>();

                dbGradeList.Add(new AppraisalFormGrading { Percentage = 100, Grade = "A", Summary = "Extra Ordinary" });
                dbGradeList.Add(new AppraisalFormGrading { Percentage = 80, Grade = "B", Summary = "Excellent" });
                dbGradeList.Add(new AppraisalFormGrading { Percentage = 60, Grade = "C", Summary = "Good" });
                dbGradeList.Add(new AppraisalFormGrading { Percentage = 40, Grade = "D", Summary = "Average" });
                dbGradeList.Add(new AppraisalFormGrading { Percentage = 20, Grade = "E", Summary = "Poor" });
            }



            gridGrading.Store[0].DataSource = dbGradeList;
            gridGrading.Store[0].DataBind();
        }

        protected void ButtonBack_Click(object sender, DirectEventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["fid"]))
                Response.Redirect("Activities_4.aspx?fid=" + Request.QueryString["fid"]);

        }

        protected void Save_Click(object sender, DirectEventArgs e)
        {

            string entryLineJson = e.ExtraParams["CompetencyList"];
            List<AppraisalFormGrading> gradeList = JSON.Deserialize<List<AppraisalFormGrading>>(entryLineJson);

            string gridDistributionJson = e.ExtraParams["gridDistribution"];
            List<AppraisalFormDistribution> distributionList = JSON.Deserialize<List<AppraisalFormDistribution>>(gridDistributionJson);
            List<AppraisalFormDistribution> distributionListNew = new List<AppraisalFormDistribution>();


            AppraisalForm _AppraisalForm = new AppraisalForm();
            _AppraisalForm.AppraisalFormID = int.Parse(Request.QueryString["fid"]);

            int order = 1;
            foreach (var item in distributionList)
            {
                if (item.BlockType != -1)
                {
                    item.Order = order++;
                    item.AppraisalFormRef_ID = _AppraisalForm.AppraisalFormID;

                    distributionListNew.Add(item);
                }
            }

            //decimal PerformanceCore = decimal.Parse((txtPerformanceCoreValue.Text=="")?"0":txtPerformanceCoreValue.Text.ToString());
            //decimal PerformanceQuestionnaireValue = decimal.Parse((txtPerformanceQuestionnaireValue.Text=="")?"0":txtPerformanceQuestionnaireValue.Text.ToString());
            //decimal PerformanceActivityValue = decimal.Parse((txtPerformanceActivityValue.Text == "") ? "0" : txtPerformanceActivityValue.Text.ToString());
            //decimal PerformanceTargetValue = decimal.Parse((txtPerformanceTargetValue.Text == "") ? "0" : txtPerformanceTargetValue.Text.ToString());

            decimal total = distributionListNew.Sum(x => Convert.ToDecimal(x.Percent));
            //(PerformanceCore + PerformanceQuestionnaireValue + PerformanceActivityValue + PerformanceTargetValue);
            if ( total!=100 && total != (decimal)99.99)
            {
                SetWarning(lblMsg, "Total values must be 100%.");
                X.Js.AddScript("window.scrollTo(0,0);");
                return;
            }

         

            _AppraisalForm.PerformanceSummaryName = txtSectionName.Text.Trim();
            _AppraisalForm.PerformanceSummaryDescription = hdnEditorDescription.Text.Trim();
            _AppraisalForm.HideRecommendBlock = chkReccommendBlock.Checked;

            //if (!string.IsNullOrEmpty(txtPerformanceCoreValue.Text))
            //    _AppraisalForm.PerformanceCoreValue = decimal.Parse(txtPerformanceCoreValue.Text);

            //if (!string.IsNullOrEmpty(txtPerformanceActivityValue.Text))
            //    _AppraisalForm.PerformanceActivityValue = decimal.Parse(txtPerformanceActivityValue.Text);

            //if (!string.IsNullOrEmpty(txtPerformanceQuestionnaireValue.Text))
            //    _AppraisalForm.PerformanceQuestionnaireValue = decimal.Parse(txtPerformanceQuestionnaireValue.Text);

            //if (!string.IsNullOrEmpty(txtPerformanceTargetValue.Text))
            //    _AppraisalForm.PerformanceTargetValue = decimal.Parse(txtPerformanceTargetValue.Text);

           
            Status respStatus;

            respStatus = NewHRManager.UpdateAppraisalForm_PerformanceSummary(_AppraisalForm, gradeList, distributionListNew);
            if (respStatus.IsSuccess)
            {

                //if (sender == btnSave)
                    SetMessage(lblMsg, "Appraisal updated.");

            }
            else
            {
                SetWarning(lblMsg, respStatus.ErrorMessage);
            }
            X.Js.AddScript("window.scrollTo(0,0);");
        }

        protected void lnkBtnDivideWeight_Click(object sender, DirectEventArgs e)
        {
            
            decimal sum = 100;

            decimal NowEquallyDividedWeigth = sum / 4;


            
            txtPerformanceCoreValue.Text = NowEquallyDividedWeigth.ToString("N2");
            txtPerformanceQuestionnaireValue.Text = NowEquallyDividedWeigth.ToString("N2");
            txtPerformanceActivityValue.Text = NowEquallyDividedWeigth.ToString("N2");//"33.34";
            txtPerformanceTargetValue.Text = NowEquallyDividedWeigth.ToString("N2");
        }

    }
}