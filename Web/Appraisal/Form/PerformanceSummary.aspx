﻿<%@ Page Title="Performance Summary" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    ValidateRequest="false" AutoEventWireup="true" CodeBehind="PerformanceSummary.aspx.cs"
    Inherits="Web.Appraisal.PerformanceSummary" %>

<%@ Register Src="~/Controls/HRAppraisalFormWizard.ascx" TagPrefix="ucHRW" TagName="ucHRFormWizard" %>
<%--<%@ Register Src="~/NewHR/UserControls/EmployeeDetailsCtl.ascx" TagName="EmployeeDetailsCtl"
    TagPrefix="uc1" %>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .header
        {
            font-weight: bolder;
            color: #1D74A2;
        }
    </style>
    <script>
        Ext.onReady(function () {
        
            
 CKEDITOR.config.toolbar = [
               ['Styles', 'Format', 'Font', 'FontSize'],

               ['Bold', 'Italic', 'Underline', 'StrikeThrough', '-', 'Undo', 'Redo', '-', 'Cut', 'Copy', 'Paste', 'Find', 'Replace', '-', 'Outdent', 'Indent', '-'],
    
               ['NumberedList', 'BulletedList', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
               ['Image', 'Table', '-', 'Link', 'TextColor', 'BGColor', 'Maximize']
            ];
        });


        var afterEditDistribution  = function(editor,e)
        {
            var total = 0;
            for(i=0;i<e.store.data.items.length - 1;i++)
            {
                
                total += parseFloat( e.store.data.items[i].data.Percent);

            }

            e.store.data.items[i].data.Percent = total;
            e.store.data.items[i].commit();
        }

         var afterEdit = function (editor, e) {
            

            var competencyTotalRow = null;
            var questionTotalRow = null;

            for(i=0;i<e.store.data.items.length;i++)
            {
                if(e.store.data.items[i].data.IsQuestionaireTotal == true)
                    questionTotalRow = e.store.data.items[i];

                if(e.store.data.items[i].data.IsCompetencyTotal == true)
                    competencyTotalRow = e.store.data.items[i];
            }

            var compPoints = 0;
            var questPoints = 0;
            // sum competency
            for(i=0;i<e.store.data.items.length;i++)
            {
                if(e.store.data.items[i].data.Type == 1 && e.store.data.items[i].data.Points != null )
                    compPoints += parseFloat(e.store.data.items[i].data.Points);

                    if(e.store.data.items[i].data.Type == 2 && e.store.data.items[i].data.Points != null)
                    questPoints += parseFloat(e.store.data.items[i].data.Points);
                    //questionTotalRow.data.Points = e.reocr.data.items[i];

            }

            competencyTotalRow.data.Points = compPoints;
            competencyTotalRow.commit();

             questionTotalRow.data.Points = questPoints;
            questionTotalRow.commit();

            e.record.commit();
        }
        

        function setTotalCoreValues() {

            var CoreValues = <%=txtPerformanceCoreValue.ClientID%>.getValue();
            var PerformanceQuestionnaireValue = <%=txtPerformanceQuestionnaireValue.ClientID%>.getValue();
            var txtPerformanceActivityValue = <%=txtPerformanceActivityValue.ClientID%>.getValue();
            var txtPerformanceTargetValue = <%= txtPerformanceTargetValue.ClientID %>.getValue();

            var totalAmount = 0;
            if(CoreValues!='')
                totalAmount = totalAmount + parseFloat(CoreValues);
            if(PerformanceQuestionnaireValue!='')
                totalAmount = totalAmount + parseFloat(PerformanceQuestionnaireValue);
            if(txtPerformanceActivityValue!='')
                totalAmount = totalAmount + parseFloat(txtPerformanceActivityValue);
            
            if(txtPerformanceTargetValue!='')
                totalAmount = totalAmount + parseFloat(txtPerformanceTargetValue);
            
            var finalValue = totalAmount  + '%';
             <%=lblTotal.ClientID%>.setValue(finalValue);


        }
        function SetEditorValue()
        {
        <%=hdnEditorDescription.ClientID%>.setValue(CKEDITOR.instances.ctl00_ContentPlaceHolder_Main_txtEditorDescription.getData());
        }

        var showPoints = function(v1,v2,v3)
        {
             if(v3.data.ShowEmptyInPointColumn)
                return "";

            return v1;
        }

        var percentRender = function(v1,v2,v3)
        {
            if(parseFloat(v1)==0)
                return "-";

            return parseFloat(v1)+"%";
        }

        var nameRender = function(v1,v2,v3)
        {
            if(v3.data.IsParent)
                return "<span class='header'>" + v3.data.Name + "</span>";

            return "&nbsp;&nbsp;" + v3.data.Name;
        }

         var CommandHandler = function(command, record){
           
               
                
                    <%=Store2.ClientID %>.remove(record);
                

         }

        var beforeEdit = function (e1, e, e2, e3) {

            if(e.record.data.BlockType == -1)
            {
                e.cancel = true;
                return;
            }


               
            
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Performance Summary
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel" style="margin-top: 0px !important; padding-top: 0px !important;">
        <ext:Hidden runat="server" ID="hiddenValue" />
        <ext:Hidden runat="server" ID="hdnEditorDescription" />
        <ext:Hidden runat="server" ID="hiddenValueRow" />
        <div class="separator bottom">
        </div>
        <div class="innerLR">
            <ucHRW:ucHRFormWizard Id="ucAppraisalWizard" runat="server" />
            <ext:Label ID="lblMsg" runat="server" />
            <%--<uc1:EmployeeDetailsCtl Id="EmployeeDetailsCtl1" runat="server" />--%>
            <div>
                <div class="widget-head">
                </div>
                <div>
                    <table class="fieldTable firsttdskip">
                        <tr>
                            <td>
                                <ext:TextField ID="txtSectionName" runat="server" Width="400" LabelAlign="Top" FieldLabel="Section Name"
                                    LabelSeparator="">
                                </ext:TextField>
                                <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator2" runat="server"
                                    ValidationGroup="SaveFormData" ControlToValidate="txtSectionName" ErrorMessage="Section Name is required." />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div>
                                    <div class="grids">
                                        <p>
                                            Description</p>
                                        <CKEditor:CKEditorControl ExtraPlugins="autogrow" ID="txtEditorDescription" Width="800px"
                                            Height="200px" runat="server" BasePath="~/ckeditor/">
                                        </CKEditor:CKEditorControl>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <%--   <tr>
                        <td>
                            <div style="padding-top: 25px;">
                                <ext:Checkbox ID="chkMutualRating" runat="server" FieldLabel="Allow Mutual Rating"
                                    LabelAlign="Right" LabelWidth="150">
                                </ext:Checkbox>
                            </div>
                        </td>
                    </tr>--%>
                        <tr>
                            <td style="padding-top: 20px">
                                <table>
                                    <tr>
                                        <td>
                                            <ext:GridPanel Title="Points Distribution" runat="server" ID="empRatingScore" PreventHeader="false"
                                                Frame="false" AutoScroll="true" Width="500" Region="Center" Cls="gridheight"
                                                ButtonAlign="Left">
                                                <Store>
                                                    <ext:Store ID="StoreGridAddedCompetencies" runat="server">
                                                        <Model>
                                                            <ext:Model runat="server" ID="Model2">
                                                                <Fields>
                                                                    <ext:ModelField Name="Type" Type="String" />
                                                                    <ext:ModelField Name="SourceId" />
                                                                    <ext:ModelField Name="Name" />
                                                                    <ext:ModelField Name="Points" />
                                                                    <ext:ModelField Name="Percentage" />
                                                                    <ext:ModelField Name="IsParent" Type="Boolean" />
                                                                    <ext:ModelField Name="IsCompetencyTotal" Type="Boolean" />
                                                                    <ext:ModelField Name="IsQuestionaireTotal" Type="Boolean" />
                                                                    <ext:ModelField Name="ShowEmptyInPointColumn" Type="Boolean" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                    </ext:Store>
                                                </Store>
                                                <Plugins>
                                                    <ext:CellEditing ID="CellEditing1" ClicksToEdit="1" runat="server">
                                                        <Listeners>
                                                            <Edit Fn="afterEdit" />
                                                        </Listeners>
                                                    </ext:CellEditing>
                                                </Plugins>
                                                
                                                <ColumnModel ID="ColumnModel1" runat="server">
                                                    <Columns>
                                                        <ext:Column ID="Column1" runat="server" DataIndex="Name" Text="Section" MenuDisabled="true"
                                                            Sortable="false" Flex="1">
                                                            <Renderer Fn="nameRender" />
                                                        </ext:Column>
                                                        <ext:Column ID="Column5" runat="server" Text="Points" DataIndex="Points" Width="100"
                                                            MenuDisabled="true" Sortable="false">
                                                            <Renderer Fn="showPoints" />
                                                            <%--<Editor>
                                                                <ext:NumberField ID="txtPoints" MaskRe="[0-9]|\.|%" runat="server" MinValue="0">
                                                                </ext:NumberField>
                                                            </Editor>--%>
                                                        </ext:Column>
                                                    </Columns>
                                                </ColumnModel>
                                                <SelectionModel>
                                                    <ext:CellSelectionModel ID="CheckboxSelectionModel2a" runat="server" Mode="Single" />
                                                </SelectionModel>
                                            </ext:GridPanel>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                  <%--  <div style="padding-top: 25px;">
                        <ext:Button runat="server" Cls="btn btn-primary" ID="lnkBtnDivideWeight" Text="<i></i>Divide Weight equally">
                            <DirectEvents>
                                <Click OnEvent="lnkBtnDivideWeight_Click">
                                    <Confirmation ConfirmRequest="true" Message="Confirm divide the weight equally?" />
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                    </div>--%>
                    <table style='margin-top:20px;'>
                        <tr>
                            <td valign="top">
                                 <ext:GridPanel runat="server" ID="gridDistribution" PreventHeader="false" Frame="false"
                                    AutoScroll="true" Width="300" Region="Center" Cls="gridheight" ButtonAlign="Left">
                                    <Store>
                                        <ext:Store ID="Store1" runat="server">
                                            <Model>
                                                <ext:Model runat="server" ID="Model1">
                                                    <Fields>
                                                        <ext:ModelField Name="Name" />
                                                        <ext:ModelField Name="Percent" />
                                                        <ext:ModelField Name="BlockType" Type="Int" />
                                                    </Fields>
                                                </ext:Model>
                                            </Model>
                                        </ext:Store>
                                    </Store>
                                     <Listeners>
                            <BeforeEdit Fn="beforeEdit" />
                        </Listeners>
                                    <Plugins>
                                        <ext:CellEditing ID="CellEditing2" ClicksToEdit="1" runat="server">
                                             <Listeners>
                                                            <Edit Fn="afterEditDistribution" />
                                                        </Listeners>
                                        </ext:CellEditing>
                                    </Plugins>
                                    <ColumnModel ID="ColumnModel2" runat="server">
                                        <Columns>
                                             <ext:Column ID="Column3" runat="server" Text="Name" DataIndex="Name" Width="150"
                                                MenuDisabled="true" Sortable="false">
                                               
                                            </ext:Column>
                                           
                                            <ext:Column ID="Column2" runat="server" Align="Center" Text="Percentage(%)" DataIndex="Percent"
                                                Width="150" MenuDisabled="true" Sortable="false">
                                                <Editor>
                                                    <ext:NumberField ID="NumberField1" MaskRe="[0-9]|\.|%" runat="server" MinValue="0">
                                                    </ext:NumberField>
                                                </Editor>
                                                <Renderer Fn="percentRender" />
                                            </ext:Column>
                                           
                                            
                                        </Columns>
                                    </ColumnModel>
                                    <SelectionModel>
                                        <ext:CellSelectionModel ID="CellSelectionModel1" runat="server" Mode="Single" />
                                    </SelectionModel>
                                </ext:GridPanel>

                                <table style="display:none">
                                    <tr>
                                        <td>
                                            <div style="padding: 5px;">
                                                <ext:TextField ID="txtPerformanceActivityValue" runat="server" MaskRe="[0-9]|\.|%"
                                                    LabelSeparator="" FieldLabel="Activity Values" LabelWidth="175" LabelAlign="Left">
                                                    <Listeners>
                                                        <Change Fn="setTotalCoreValues">
                                                        </Change>
                                                    </Listeners>
                                                </ext:TextField>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div style="padding: 5px;">
                                                <ext:TextField ID="txtPerformanceCoreValue" runat="server" MaskRe="[0-9]|\.|%" FieldLabel="Competencies Values"
                                                    LabelSeparator="" LabelWidth="175" LabelAlign="Left">
                                                    <Listeners>
                                                        <Change Fn="setTotalCoreValues">
                                                        </Change>
                                                    </Listeners>
                                                </ext:TextField>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div style="padding: 5px;">
                                                <ext:TextField ID="txtPerformanceTargetValue" runat="server" MaskRe="[0-9]|\.|%"
                                                    LabelSeparator="" FieldLabel="Target Values" LabelWidth="175" LabelAlign="Left">
                                                    <Listeners>
                                                        <Change Fn="setTotalCoreValues">
                                                        </Change>
                                                    </Listeners>
                                                </ext:TextField>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div style="padding: 5px;">
                                                <ext:TextField ID="txtPerformanceQuestionnaireValue" runat="server" MaskRe="[0-9]|\.|%"
                                                    LabelSeparator="" FieldLabel="Questionnaire Values" LabelWidth="175" LabelAlign="Left">
                                                    <Listeners>
                                                        <Change Fn="setTotalCoreValues">
                                                        </Change>
                                                    </Listeners>
                                                </ext:TextField>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div style="float: left; font-weight: lighter; padding-left: 5px">
                                                Total
                                            </div>
                                            <div style="padding-left: 185px">
                                                <ext:DisplayField ID="lblTotal" runat="server" Text="0.00" FieldCls="boldLabel">
                                                </ext:DisplayField>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td valign="top" style='padding-left:20px;'>
                                <ext:GridPanel runat="server" ID="gridGrading" PreventHeader="false" Frame="false"
                                    AutoScroll="true" Width="500" Region="Center" Cls="gridheight" ButtonAlign="Left">
                                    <Store>
                                        <ext:Store ID="Store2" runat="server">
                                            <Model>
                                                <ext:Model runat="server" ID="Model3">
                                                    <Fields>
                                                        <ext:ModelField Name="Summary" />
                                                        <ext:ModelField Name="Grade" />
                                                        <ext:ModelField Name="Percentage" />
                                                    </Fields>
                                                </ext:Model>
                                            </Model>
                                        </ext:Store>
                                    </Store>
                                    <Plugins>
                                        <ext:CellEditing ID="CellEditing3" ClicksToEdit="1" runat="server">
                                            <Listeners>
                                                <Edit Fn="afterEdit" />
                                            </Listeners>
                                        </ext:CellEditing>
                                    </Plugins>
                                    <ColumnModel ID="ColumnModel3" runat="server">
                                        <Columns>
                                            <ext:Column ID="Column9" runat="server" Text="Percentage(%)" DataIndex="Percentage"
                                                Width="150" MenuDisabled="true" Sortable="false">
                                                <Editor>
                                                    <ext:NumberField ID="NumberField4" MaskRe="[0-9]|\.|%" runat="server" MinValue="0">
                                                    </ext:NumberField>
                                                </Editor>
                                            </ext:Column>
                                            <ext:Column ID="Column8" runat="server" Text="Grade" DataIndex="Grade" Width="150"
                                                MenuDisabled="true" Sortable="false">
                                                <Editor>
                                                    <ext:TextField ID="NumberField3" runat="server" MinValue="0">
                                                    </ext:TextField>
                                                </Editor>
                                            </ext:Column>
                                            <ext:Column ID="Column10" runat="server" Flex="1" Text="Summary" DataIndex="Summary" Width="150"
                                                MenuDisabled="true" Sortable="false">
                                                <Editor>
                                                    <ext:TextField ID="TextField1" runat="server" MinValue="0">
                                                    </ext:TextField>
                                                </Editor>
                                            </ext:Column>
                                             <ext:CommandColumn ID="CommandColumn1" runat="server" Width="25">
                                                <Commands>
                                                    <ext:GridCommand Icon="Decline" CommandName="Delete" />
                                                </Commands>
                                                <Listeners>
                                                    <Command Handler="CommandHandler(command,record);" />
                                                </Listeners>
                                            </ext:CommandColumn>
                                        </Columns>
                                    </ColumnModel>
                                    <SelectionModel>
                                        <ext:CellSelectionModel ID="CellSelectionModel2" runat="server" Mode="Single" />
                                    </SelectionModel>
                                </ext:GridPanel>
                                <ext:Button StyleSpec='margin-top:10px;' runat="server" ID="Button1" ValidationGroup="SaveFormData"
                                    Text="<i></i>Add Row">
                                    <Listeners>
                                        <Click Handler="#{Store2}.add({});" />
                                    </Listeners>
                                </ext:Button>
                                <div style='padding-top: 10px; color: lightgray;'>
                                    Note : Place lowest score at the bottom</div>
                            </td>
                        </tr>
                    </table>
                    <div style='display: none'>
                        <div style="padding-top: 10px; padding-bottom: 10px;">
                            <ext:Checkbox StyleSpec="margin-top:2px" ID="chkReccommendBlock" runat="server" BoxLabel="Do Not Show Recommendation Block" />
                        </div>
                    </div>
                    <div class="buttonBlock">
                        <ext:Button runat="server" ID="btnSaveAndFinnishLater" Cls="btn btn-primary" ValidationGroup="SaveFormData"
                            Text="<i></i>Save">
                            <DirectEvents>
                                <Click OnEvent="Save_Click">
                                    <EventMask ShowMask="true" />
                                    <ExtraParams>
                                        <ext:Parameter Name="CompetencyList" Value="Ext.encode(#{gridGrading}.getRowsValues({selectedOnly : false}))"
                                            Mode="Raw" />
                                        <ext:Parameter Name="gridDistribution" Value="Ext.encode(#{gridDistribution}.getRowsValues({selectedOnly : false}))"
                                            Mode="Raw" />
                                    </ExtraParams>
                                </Click>
                            </DirectEvents>
                            <Listeners>
                                <Click Handler="SetEditorValue();valGroup = 'SaveFormData';return CheckValidation();">
                                </Click>
                            </Listeners>
                        </ext:Button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br />
</asp:Content>
