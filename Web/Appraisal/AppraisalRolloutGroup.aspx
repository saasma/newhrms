﻿<%@ Page Title="Rollout Group" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="AppraisalRolloutGroup.aspx.cs" Inherits="Web.NewHR.AppraisalRolloutGroup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .x-tab-bar-default {
            background-color: inherit;
        }

        .hideLeftBlockCssInPage {
            margin: 0px !important;
            padding-left: 20px !important;
        }

        #menu {
            display: none;
        }

        #content {
            margin: inherit;
        }
    </style>
    <script type="text/javascript">
        var CommandHandlerMainGrid = function(command, record) {
            <%= hiddenValue.ClientID %>.setValue(record.data.RolloutGroupID);
            if(command=="Edit")
            {
                <%= btnEditGroup.ClientID %>.fireEvent('click');
            }
            else
            {
                <%= btnDeleteGroup.ClientID %>.fireEvent('click');
            }
        };
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>Appraisal Rollout Group
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel" style="margin-top: 0px !important; padding-top: 0px !important;">
        <ext:Hidden ID="hiddenValue" runat="server" />
        <ext:LinkButton runat="server" ID="btnEditGroup" Hidden="true">
            <DirectEvents>
                <Click OnEvent="btnEditGroup_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:LinkButton>
        <ext:LinkButton runat="server" ID="btnDeleteGroup" Hidden="true">
            <DirectEvents>
                <Click OnEvent="btnDeleteGroup_Click">
                    <Confirmation ConfirmRequest="true" Message="Are you sure you want to delete ?" />
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:LinkButton>
        <div class="separator bottom">
        </div>
        <div class="innerLR">
            <h4 class="heading" style="margin-top: 20px">Group List</h4>
            <table width="700">
                <tr>
                    <td align="left">
                        <ext:Button runat="server" Text="New Group" Cls="btn btn-primary">
                            <DirectEvents>
                                <Click OnEvent="btnCreateGroup_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                    </td>
                    <td align="left" style="padding-left: 504px"></td>
                </tr>
            </table>
            <ext:GridPanel ID="GridRolloutGroups" runat="server" StyleSpec="margin-top:15px;" Width="500"
                Cls="itemgrid">
                <Store>
                    <ext:Store runat="server">
                        <Model>
                            <ext:Model runat="server" ID="Model1" IDProperty="TargetID">
                                <Fields>
                                    <ext:ModelField Name="RolloutGroupID" Type="String" />
                                    <ext:ModelField Name="Name" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="RowNumbererColumn1" runat="server" Width="35" />
                        <ext:Column ID="ColumnName" Sortable="false" MenuDisabled="true" runat="server" Text="Name"
                            Align="Left" DataIndex="Name" Flex="1" />
                        <ext:CommandColumn runat="server" Width="60" Align="Left">
                            <Commands>
                                <ext:GridCommand Icon="NoteEdit" CommandName="Edit" ToolTip-Text="Edit">
                                </ext:GridCommand>
                                <ext:GridCommand Icon="Delete" CommandName="Delete" ToolTip-Text="Delete">
                                </ext:GridCommand>
                            </Commands>
                            <Listeners>
                                <Command Handler="CommandHandlerMainGrid(command, record);" />
                            </Listeners>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
                <SelectionModel>
                    <ext:RowSelectionModel runat="server" Mode="Single" />
                </SelectionModel>
            </ext:GridPanel>
            <ext:Window ID="WindowGroup" runat="server" Title="New Group" Width="450"
                Height="250" Hidden="true" Modal="true" Closable="true" DefaultAnchor="100%"
                AutoScroll="true">
                <Content>
                    <table class="fieldTable">
                        <tr>
                            <td colspan="2">
                                <ext:TextField ID="txtGroupName" Width="350" runat="server" FieldLabel="Group Name"
                                    LabelAlign="Top" LabelSeparator="">
                                </ext:TextField>
                            </td>
                        </tr>
                        <tr>
                            <td valign="bottom" colspan="2">
                                <div class="popupButtonDiv">
                                    <ext:Button runat="server" ID="btnSaveGroup" Cls="btn btn-primary" Text="<i></i>Save">
                                        <DirectEvents>
                                            <Click OnEvent="btnSaveGroupSaveUpdate_Click">
                                                <EventMask ShowMask="true" />
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <div class="btnFlatOr">
                                        or
                                    </div>
                                    <ext:LinkButton runat="server" ID="btnCancel" StyleSpec="padding:0px;" Cls="btnFlatLeftGap"
                                        Text="<i></i>Cancel">
                                        <Listeners>
                                            <Click Handler="#{WindowGroup}.hide();">
                                            </Click>
                                        </Listeners>
                                    </ext:LinkButton>
                                </div>
                            </td>
                        </tr>
                    </table>
                </Content>
            </ext:Window>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
