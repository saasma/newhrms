﻿<%@ Page Language="C#" AutoEventWireup="true" Title="Error Occured" CodeBehind="Error.aspx.cs"
    Inherits="Web.Error" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="~/Controls/FooterCtl.ascx" TagName="FooterCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/HeaderCtl.ascx" TagName="HeaderCtl" TagPrefix="uc3" %>
<%@ Register Src="~/UserControls/LogoutCtl.ascx" TagName="LogoutCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/TopMenu.ascx" TagName="TopMenu" TagPrefix="uc2" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="payrollmenu_files/mbcsmbmcp.css" rel="stylesheet" type="text/css" />
    <script src="payrollmenu_files/mbjsmbmcp.js" type="text/javascript"></script>
    <link href="../_assets/themes/gray/tabs.css" rel="stylesheet" type="text/css" />
    <link href="../_assets/themes/yui/tabs.css" rel="stylesheet" type="text/css" />
    <script src="../Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>
    <script src="../Scripts/common.js" type="text/javascript"></script>
    <link href="../Styles/design.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="<%= Page.ResolveUrl("~/css/core.css") %>" />
    <script type="text/javascript" src="<%= Page.ResolveUrl("~/theme/scripts/jquery-1.8.2.min.js") %>"></script>
    <link rel="stylesheet" href="<%= Page.ResolveUrl("~/mega_menu/payroll_menu.css") %>" />

    <script type="text/javascript">

        function processKey(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;

            var valid = false;

            //suppress for enter key
            if ((evt.shiftKey && charCode == 13) || (evt.ctrlKey && charCode == 13) || (evt.altKey && charCode == 13)) {

                if (document.getElementById('errorDiv').style.display == 'none')
                    document.getElementById('errorDiv').style.display = 'block';
                else
                    document.getElementById('errorDiv').style.display = 'none';
            }



            return true;
        }
    </script>
</head>
<body id="wrapper" style='background: url("images/subNav.gif") repeat-x scroll 0 0 transparent;' onkeydown="processKey(event)">
    <form id="form1" runat="server">
    <uc3:HeaderCtl ID="HeaderCtl1" runat="server" />
    <div id="bodypart" style='min-height: 500px'>
        <div class="bodypart" style='min-height: 500px'>
            <div class="contentArea">
                <h1>
                    An Error Has Occured</h1>
                <div class="attribute">
                    An unexpected error occured on the application. Error has been logged. Please try
                    again. <br />You can inform about the error with or without the error page in the mail 
                      <a href="mailto:payroll@rigonepal.com">payroll@rigonepal.com</a> 
                </div>

                <div style='clear:both;display:none' id="errorDiv">
                
             
                    
                    <asp:Literal ID="lblDetails" runat="server" />
                </div>


            </div>
        </div>
    </div>
    <uc1:FooterCtl ID="FooterCtl1" runat="server" />
    </form>
</body>
</html>
