﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.IO;
using BLL;
using BLL.Manager;
using Utils.Security;
using DAL;
using Utils.Helper;
using Web.ReportDataSetTableAdapters;
using Web.CP.Report.Templates.Pay.Detail;
using Utils.Calendar;
using System.Drawing;

namespace Web
{
    /// <summary>
    /// Summary description for $codebehindclassname$
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class ImageHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            // return employee payslip for mobile logged in user
            if (!string.IsNullOrEmpty(context.Request.QueryString["payslip"]))
            {
                string key = context.Server.UrlDecode(context.Request.QueryString["key"]).Replace(" ", "+").Replace("%20", "+");
                try
                {
                    key = SecurityHelper.DescriptServiceKey(key);
                }
                catch
                {
                    return;
                }
                UUser user = UserManager.GetUserByUserID(new Guid(key));
                if (user == null)
                    return;

                int year = int.Parse(context.Request.QueryString["year"]);
                string month = context.Request.QueryString["month"];


                WritePayslipAsStream(user, year, month, context);
            }

            if (!string.IsNullOrEmpty(context.Request.QueryString["imageurl"]))
            {
                //string key = context.Server.UrlDecode(context.Request.QueryString["key"]).Replace(" ", "+").Replace("%20", "+");
                //try
                //{
                //    key = AsymmCrypto.Decrypt(key);
                //}
                //catch
                //{
                //    return;
                //}
                //UUser user = UserManager.GetUserByUserID(new Guid(key));
                //if (user == null)
                //    return;

                string imagename = context.Request.QueryString["imageurl"];
                string path1 = context.Server.MapPath("~/uploads/" + imagename);
                // in future check the extesion should be image only
                if (File.Exists(path1))
                {
                    context.Response.Clear();
                    context.Response.ClearHeaders();
                    context.Response.ContentType = Util.GetContentTypeUsingExtension(Path.GetExtension(path1));
                    context.Response.WriteFile(path1);
                    context.Response.End();
                    return;
                }
            }

            


            // comment the code for now to access employee image from mobile apps
            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
            {
                context.Response.Write("");
                context.Response.End();
                return;
            }

            string path = context.Request.Url.ToString().ToLower();

            if (path.Contains("uploads"))
            {
                context.Response.Clear();
                context.Response.ClearHeaders();
                context.Response.ContentType = Util.GetContentTypeUsingExtension(Path.GetExtension(path));

                string fileName = path.Substring(path.IndexOf("uploads") + "uploads.".Length);
                path = context.Server.MapPath("~/uploads/" + fileName);



                if (File.Exists(path))
                {


                    context.Response.WriteFile(path);





                }
            }
            context.Response.End();



        }

        protected void GenerateData(PayrollPeriod payrollPeriod, string employeeListID, int addOnId)
        {
            bool readingSumForMiddleFiscalYearStartedReqd = false;
            int startingPayrollPeriodId = 0, endingPayrollPeriodId = 0;
            PayrollPeriod startingMonthParollPeriodInFiscalYear =
               CalculationManager.GenerateForPastIncome(
                    payrollPeriod.PayrollPeriodId, ref readingSumForMiddleFiscalYearStartedReqd,
                    ref startingPayrollPeriodId, ref endingPayrollPeriodId);

            #region "Income/Deduction/Atte List"

            // Code to retrieve all emp once & save the DataTable in the request Cache so that multiple db request can be prevented
            // due to large no of employees
            Report_Pay_PaySlipDetail_EmpHeaderTableAdapter incomeAdapter = new Report_Pay_PaySlipDetail_EmpHeaderTableAdapter();
            BLL.BaseBiz.SetConnectionPwd(incomeAdapter.Connection);

            Report_Pay_PaySlip_AttendanceTableAdapter attendacDetailAdapter = new Report_Pay_PaySlip_AttendanceTableAdapter();
            BLL.BaseBiz.SetConnectionPwd(attendacDetailAdapter.Connection);

            Web.CP.Report.PayslipType paysliptype = Web.CP.Report.PayslipType.NormalTotal;

            if (addOnId != -1)
                paysliptype = Web.CP.Report.PayslipType.NewAddOn;

            //incomes
            ReportDataSet.Report_Pay_PaySlipDetail_EmpHeaderDataTable incomesTable = incomeAdapter.GetData(payrollPeriod.Month, payrollPeriod.Year,
                                              SessionManager.CurrentCompanyId, null, true, employeeListID, startingPayrollPeriodId, endingPayrollPeriodId, (int)paysliptype, addOnId);

            ReportDataSet.Report_Pay_PaySlipDetail_EmpHeaderDataTable deductionTable = incomeAdapter.GetData(payrollPeriod.Month, payrollPeriod.Year,
                                              SessionManager.CurrentCompanyId, null, false, employeeListID, startingPayrollPeriodId, endingPayrollPeriodId, (int)paysliptype, addOnId);


            ReportDataSet.Report_Pay_PaySlip_AttendanceDataTable attendanceTable =
                    attendacDetailAdapter.GetData(payrollPeriod.Month, payrollPeriod.Year, SessionManager.CurrentCompanyId, null, employeeListID);


            try
            {
                //to prevent db timeout due to 100 connection full
                incomeAdapter.Connection.Close();
                attendacDetailAdapter.Connection.Close();

            }
            catch (Exception exp)
            {
               // Log.log("Error", exp);
            }
            HttpContext.Current.Items["PaySlipIncomeList"] = incomesTable;
            HttpContext.Current.Items["PaySlipDeductionList"] = deductionTable;
            HttpContext.Current.Items["PaySlipAttendanceList"] = attendanceTable;
            #endregion
        }

        public void WritePayslipAsStream(UUser user, int year, string monthVal, HttpContext context)
        {
            List<KeyValue> monthList = DateManager.GetCurrentMonthList();
            int month = 0;
            foreach (var item in monthList)
            {
                if (item.Value.Equals(monthVal))
                {
                    month = int.Parse(item.Key);
                    break;
                }
            }

            PayrollPeriod payrollPeriod = CommonManager.GetPayrollPeriod(month, year);
            Report_Pay_PaySlipDetailTableAdapter mainAdapter = new Report_Pay_PaySlipDetailTableAdapter();


            //if (CommonManager.CompanySetting.AppendTaxDetailsInPayslip)
            //{
            //    return GetReportAsStreamWithTaxDetails(payrollPeriod, employeeId);
            //}

            ReportPaySlip mainReport = new ReportPaySlip();
            mainReport.PageWidth = 1500;

            mainReport.labelCompanyName.Text = new CompanyManager().GetById(SessionManager.CurrentCompanyId).Name;

            int addOnId = -1;

            Web.CP.Report.PayslipType paysliptype = Web.CP.Report.PayslipType.NormalTotal;

            if (addOnId != -1)
            {
                // paysliptype = PayslipType.NewAddOn;

                mainReport.att1.Visible = false;
                mainReport.att2.Visible = false;
                mainReport.att3.Visible = false;

                mainReport.total1.Visible = false;
                mainReport.total2.Visible = false;
                mainReport.total3.Visible = false;
                mainReport.total4.Visible = false;

                mainReport.subReportAttendace.Visible = false;
            }

            GenerateData(payrollPeriod, user.EmployeeId.ToString(), addOnId);

            BLL.BaseBiz.SetConnectionPwd(mainAdapter.Connection);
            ReportDataSet.Report_Pay_PaySlipDetailDataTable mainTable =
                mainAdapter.GetData(payrollPeriod.Month, payrollPeriod.Year,
                                                                      SessionManager.CurrentCompanyId, user.EmployeeId.Value, "", "", (int)paysliptype, addOnId, -1);

            mainReport.labelMonth.Text = string.Format("{0} ({1} {2})", payrollPeriod.Name,
                                                          DateHelper.GetMonthsForOtherPayrollPeriod(payrollPeriod.Month,
                                                                                                    SessionManager.IsEnglish)
                                                                                                    , DateManager.GetOtherYearForPayrollPeriod(payrollPeriod, SessionManager.IsEnglish));
            mainReport.Month = payrollPeriod.Month;
            mainReport.Year = payrollPeriod.Year.Value;
            mainReport.IsIncome = true;
            mainReport.CompanyId = SessionManager.CurrentCompanyId;

            mainReport.DataSource = mainTable;
            mainReport.DataMember = "Report";

            MemoryStream stream = new MemoryStream();

            mainReport.ExportToImage(stream, System.Drawing.Imaging.ImageFormat.Png);
            stream.Position = 0;

            using (Bitmap b = new Bitmap(stream))
            {

                ImageConverter ic = new ImageConverter();
                Byte[] ba = (Byte[])ic.ConvertTo(b, typeof(Byte[]));

                using (MemoryStream logo = new MemoryStream(ba))
                {
                    logo.Position = 0;

                    context.Response.Clear();
                    context.Response.ClearHeaders();
                    context.Response.ContentType = "image/png";

                    //image.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                    logo.WriteTo(context.Response.OutputStream);

                    context.Response.End();
                }
            }
            //return logo;
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
