﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using BLL.Entity;
using Utils.Helper;
using Utils.Calendar;

namespace Web
{

    public class LeaveModel : EventModel
    {
        public int? LeaveTypeId2 { get; set; }
        public float Days1 { get; set; }
        public float Days2 { get; set; }
    }

    //public class MyEventCollection : List<MyEvent>
    //{
    //    public MyEventCollection() { }
    //}
    //public class MyEvent : Event
    //{
    //    public bool IsHalfDay { get; set; }
    //}

    /// <summary>
    /// Summary description for LeaveRequestService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
     [System.Web.Script.Services.ScriptService]
    public class LeaveRequestService : System.Web.Services.WebService
    {

        #region "Leave Request"

        [WebMethod(true)]
        public Paging<GetLeaveRequestListForHRResult> GetLeaveRequestListForHR(
            int start, int limit,
            DateTime? startDate, DateTime? endDate,
            int Status, string Branch, string Employee,int page,bool useModifiedDate,int? leaveTypeId,string sort)
        {

            if (HttpContext.Current.User.Identity.IsAuthenticated == false || SessionManager.CurrentLoggedInEmployeeId != 0)
                return null;

            useModifiedDate = !useModifiedDate;

            List<DAL.GetLeaveRequestListForHRResult> list = LeaveAttendanceManager.GetLeaveRequestListForHR
                (startDate, endDate, Status, Branch, Employee, start, limit, useModifiedDate, leaveTypeId,sort);

            foreach (GetLeaveRequestListForHRResult item in list)
            {
                item.CreatedDate =item.CreatedOn == null ? "" : item.CreatedOn.Value.ToString("dd-MMM-yyyy h:m tt");
                item.FromDate = item.FromDateEng.Value.ToString("dd-MMM-yyyy");
                item.ToDate = item.ToDateEng.Value.ToString("dd-MMM-yyyy");
            }

            int total = 0;
            if (list.Count > 0)
                total = list[0].TotalRows.Value;

            return new Paging<GetLeaveRequestListForHRResult>(list, total);


        }



        [WebMethod(true)]
        public EventModelCollection GetLeaveListForApprovalDashboard(DateTime? start, DateTime? end, int leaveRequestFrom,
            int Status, string Branch, string Employee, DateTime? StartDate, DateTime? EndDate)
        {

            start = null;
            end = null;

            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
                return new EventModelCollection();

            List<DAL.GetAllLeaveRequestDetailResult> list = LeaveAttendanceManager.GetUnApprovedLeaveRequestForApproval(SessionManager.CurrentLoggedInEmployeeId,
            start, end, 0, 0, "", null).Take(5).ToList();

            EventModelCollection events = new EventModelCollection();
            foreach (DAL.GetAllLeaveRequestDetailResult request in list)
            {
                EventModel entity = new EventModel();
                entity.EventId = request.LeaveRequestId;


                if (request.LeaveTypeId == 0)
                    entity.CalendarId = -1;
                else
                    entity.CalendarId = request.LeaveTypeId;

                entity.Notes = request.Name + " - " + request.Reason;

                if (entity.CalendarId.Equals(-1))
                    entity.Title = Resources.Messages.UnpaidLeaveTitleForLeaveRequest;
                else
                    entity.Title = request.CalendarTitle;

                entity.Url = Decimal.Parse(request.DaysOrHours.ToString()).ToString("0");
                entity.StartDate = request.FromDateEng.Value;
                entity.EndDate = request.ToDateEng.Value;
                entity.IsAllDay = false;
                //entity.IsNew = false;

                entity.Location = request.IsHalfDayLeave.ToString();

                entity.Reminder = request.IsHour.ToString();



                events.Add(entity);
            }
            return events;
        }


        [WebMethod(true)]
        public EventModelCollection GetLeaveListForApproval(DateTime? start, DateTime? end, int leaveRequestFrom,
            int Status,string Branch,string Employee, DateTime? StartDate, DateTime? EndDate)
        {
            
            start = StartDate;
            end = EndDate;

            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
                return new EventModelCollection();

            List<DAL.GetAllLeaveRequestDetailResult> list = LeaveAttendanceManager.GetUnApprovedLeaveRequestForApproval(SessionManager.CurrentLoggedInEmployeeId,
            start.Value, end.Value, leaveRequestFrom,Status,Branch,Employee);

            EventModelCollection events = new EventModelCollection();
            foreach (DAL.GetAllLeaveRequestDetailResult request in list)
            {
                EventModel entity = new EventModel();
                entity.EventId = request.LeaveRequestId;
               
                
                if (request.LeaveTypeId == 0)
                    entity.CalendarId = -1;
                else
                    entity.CalendarId = request.LeaveTypeId;

                entity.Notes = request.Name + " - " + request.Reason;

                if (entity.CalendarId.Equals(-1))
                    entity.Title = Resources.Messages.UnpaidLeaveTitleForLeaveRequest;
                else
                    entity.Title = request.CalendarTitle;

                entity.Url = Decimal.Parse(request.DaysOrHours.ToString()).ToString("0");
                entity.StartDate = request.FromDateEng.Value;
                entity.EndDate = request.ToDateEng.Value;
                entity.IsAllDay = false;
                //entity.IsNew = false;

                entity.Location = request.IsHalfDayLeave.ToString();
                
                 entity.Reminder = request.IsHour.ToString();

               
              
                events.Add(entity);
            }
            return events;
        }

        [WebMethod]
        public EventModelCollection GetShiftList(DateTime? start, DateTime? end, DateTime? StartDate, DateTime? EndDate, string Employee
            ,int page,int limit)
        {
            

            start = StartDate;
            end = EndDate;

            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
                return new EventModelCollection();

            List<DAL.GetEmployeeShiftListResult> list =
                ShiftManager.GetEmployeeShiftList(
                start.Value, end.Value, Employee);

            EventModelCollection events = new EventModelCollection();
            foreach (DAL.GetEmployeeShiftListResult request in list)
            {
                EventModel entity = new EventModel();
                entity.EventId = request.WorkShiftHistoryId;


                entity.CalendarId = request.WorkShiftId;

                entity.Notes = request.Name + " - " + request.WorkShift;



                entity.Title = request.WorkShift;



                entity.StartDate = request.FromDateEngWithShiftTime.Value;
                entity.EndDate = request.ToDateEngWithShiftTime.Value;
                entity.IsAllDay = false;
                //entity.IsNew = false;



                events.Add(entity);
            }
            return events;
        }

        [WebMethod]
        public EventModelCollection GetEvents(DateTime? start, DateTime? end, DateTime? StartDate, DateTime? EndDate)
        {
            

            start = StartDate;
            end = EndDate;

            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
                return new EventModelCollection();

            List<DAL.GetUnApprovedLeaveRequestResult> list = LeaveAttendanceManager.GetUnApprovedLeaveRequest(SessionManager.CurrentLoggedInEmployeeId, start.Value, end.Value);
            EventModelCollection events = new EventModelCollection();
            foreach (GetUnApprovedLeaveRequestResult request in list)
            {
                EventModel entity = new EventModel();
                entity.EventId = request.LeaveRequestId;
                if (request.LeaveTypeId == 0)
                    entity.CalendarId = -1;
                else
                    entity.CalendarId = request.LeaveTypeId;

                entity.Title = request.Reason;

                DateTime startTime = new DateTime(1, 1, 1,0,0,0);
                DateTime endTime = new DateTime(1, 1, 1, 1, 0, 0);

                if (!string.IsNullOrEmpty(request.StartTime))
                    startTime = DateTime.Parse(request.StartTime);
                if (!string.IsNullOrEmpty(request.EndTime))
                    endTime = DateTime.Parse(request.EndTime);

                entity.StartDate = new DateTime(request.FromDateEng.Value.Year, request.FromDateEng.Value.Month, request.FromDateEng.Value.Day, startTime.Hour, startTime.Minute, startTime.Second);
                entity.EndDate = new DateTime(request.ToDateEng.Value.Year, request.ToDateEng.Value.Month, request.ToDateEng.Value.Day, endTime.Hour, endTime.Minute, endTime.Second);//request.ToDateEng.Value;
                entity.IsAllDay = false;
                //entity.IsNew = false;

                entity.Location = request.IsHalfDayLeave.ToString();
                //if (entity.CalendarId.Equals(-1))
                //    entity.Notes = Resources.Messages.UnpaidLeaveTitleForLeaveRequest;
                //else
                entity.Notes = entity.Notes = request.CalendarTitle;

                entity.Url = Decimal.Parse(request.DaysOrHours.ToString()).ToString("0");

                // ASCII CODE FOR ô is Alt+147
                entity.Url +=
                        ";" + request.EditSequence.ToString() + ";" + 
                        (LeaveRequestStatusEnum)(int)request.Status + ";" + 
                        request.IsAttendanceSaved + ";" +
                        (string.IsNullOrEmpty(request.HalfDayType) ? "AM" : request.HalfDayType) + ";" +
                        (request.SupervisorEmployeeIdInCaseOfSelection == null ? "" : request.SupervisorEmployeeIdInCaseOfSelection.ToString()) + ";" +
                        (request.StartTime == null ? "" : request.StartTime) + ";" +
                        (request.EndTime == null ? "" : request.EndTime);

                entity.Reminder = request.IsHour.ToString();
             
                events.Add(entity);
                
            }
            return events;
        }

        //[WebMethod]
        //public void SaveAll(List<Event> events)
        //{
        //    //var uc = ((EventsViewer)UserControlRenderer.LoadControl("/Examples/Calendar/Overview/Shared/Common/EventsViewer.ascx"));
        //    //uc.Render(events);
        //}

        /// <summary>
        /// Returns newly saved event id
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        [WebMethod]
        public ResponseStatus SaveEvent(EventModel entity,string halfDayType,string selectedSupervisor,string startTime,string endTime)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
            {
                return null;
            }

            ResponseStatus status = new ResponseStatus();
            //DAL.LeaveRequest leave = new LeaveRequest();

            //if (HttpContext.Current.User.Identity.IsAuthenticated == false)
            //{
            //    status.IsSuccess = "false";
            //    status.ErrorMessage = "Not Authenticated";
            //    return status;
            //}

            //// take precause for xss, need to do Decode while displaying
            //entity.Title = HttpUtility.HtmlEncode(entity.Title);
            //entity.Notes = HttpUtility.HtmlEncode(entity.Notes);

            //leave.StartTime = startTime;
            //leave.EndTime = endTime;

            //status = LeaveRequestManager.GetLeaveRequestFromCalendarEntity(entity, leave,true,SessionManager.CurrentLoggedInEmployeeId);

            

            //if (!string.IsNullOrEmpty(selectedSupervisor))
            //    leave.SupervisorEmployeeIdInCaseOfSelection = int.Parse(selectedSupervisor);

            //if (status.IsSuccess == "true")
            //{
            //    leave.HalfDayType = halfDayType;
            //    LeaveAttendanceManager.SaveUpdateLeaveRequest(leave);

            //    NotifyThroughMessageAndEmail(leave.LeaveRequestId, leave.IsHour,
            //        leave.DaysOrHours.ToString(), leave.Reason, leave.FromDateEng.ToString(), leave.ToDateEng.ToString(), ((LeaveRequestStatusEnum)leave.Status).ToString(), leave.SupervisorEmployeeIdInCaseOfSelection);

            //    status.ErrorMessage = "Leave requested successfully.";
                
            //}

           
            
            return status;

            
        }

        

       

      

        [WebMethod]
        public ResponseStatus UpdateEvent(EventModel entity, string halfDayType, string selectedSupervisor, string startTime, string endTime)
        {

            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
            {
                return new ResponseStatus();
            }

            ResponseStatus status = new ResponseStatus();
            //DAL.LeaveRequest leave = new DAL.LeaveRequest();

            //if (HttpContext.Current.User.Identity.IsAuthenticated == false)
            //{
            //    status.IsSuccess = "false";
            //    status.ErrorMessage = "Not Authenticated";
            //    return status;
            //}

            //// take precause for xss, need to do Decode while displaying
            //entity.Title = HttpUtility.HtmlEncode(entity.Title);
            //entity.Notes = HttpUtility.HtmlEncode(entity.Notes);


            //leave.LeaveRequestId = entity.EventId.Value;

            ////ASCII CODE for ô is Alt+147
            //if (!LeaveAttendanceManager.CheckEditSequence(int.Parse(entity.Url.Split(';')[1]), leave.LeaveRequestId))
            //{
            //    status.ErrorMessage = Resources.Messages.LeaveConcurrencyError;
            //    status.IsSuccess = "false";
            //    return status;
            //}

            //leave.StartTime = startTime;
            //leave.EndTime = endTime;

            //status = LeaveRequestManager.GetLeaveRequestFromCalendarEntity(entity, leave, false, SessionManager.CurrentLoggedInEmployeeId);

            

            //if (!string.IsNullOrEmpty(selectedSupervisor))
            //    leave.SupervisorEmployeeIdInCaseOfSelection = int.Parse(selectedSupervisor);

            //if (status.IsSuccess == "true")
            //{
            //    leave.HalfDayType = halfDayType;
            //    LeaveAttendanceManager.SaveUpdateLeaveRequest(leave);

            //    NotifyThroughMessageAndEmail(leave.LeaveRequestId, leave.IsHour, leave.DaysOrHours.ToString(), leave.Reason, leave.FromDateEng.ToString(), leave.ToDateEng.ToString(), ((LeaveRequestStatusEnum)leave.Status).ToString()
            //        , leave.SupervisorEmployeeIdInCaseOfSelection);


            //    status.ErrorMessage = "Leave changed successfully.";             
            //}

           
            return status;
        }



        [WebMethod]
        public List<GetHolidaysForAttendenceResult> GetHolidays(int month, int year)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
            {
                return new List<GetHolidaysForAttendenceResult>();
            }
            month += 1;

            DateTime startDateEng = new DateTime(year, month, 1);
            DateTime endDateEng = new DateTime(year, month, DateTime.DaysInMonth(year, month));
            List<GetHolidaysForAttendenceResult> list = null;

            HolidayManager holidayMgr = new HolidayManager();
            list = holidayMgr.GetMonthsHolidaysForAttendence(0, 0, SessionManager.CurrentCompanyId, 0, startDateEng, endDateEng,SessionManager.CurrentLoggedInEmployeeId);

            List<GetHolidaysForAttendenceResult> nonbranchholidaylist = new List<GetHolidaysForAttendenceResult>();
            // check & remove if branch holiday applies & not in that branch
            foreach (GetHolidaysForAttendenceResult branchHoliday in list.Where(x => x.BranchIDList != ""))
            {
                if (branchHoliday.DateEng != null && !string.IsNullOrEmpty(branchHoliday.BranchIDList))
                {
                    int branchId = BranchManager.GetEmployeeCurrentBranch(SessionManager.CurrentLoggedInEmployeeId, branchHoliday.DateEng.Value);
                    //if (branchId != branchHoliday.BranchId)
                    if (!branchHoliday.BranchList.Contains(branchId))
                        nonbranchholidaylist.Add(branchHoliday);
                }
            }

            // remove female holiday for male-
            if (SessionManager.CurrentLoggedInEmployeeId != 0)
            {
                EEmployee emp = new EmployeeManager().GetById(SessionManager.CurrentLoggedInEmployeeId);
                // if gender not femaly then remove femaly holidays from list
                if (emp.Gender != 0)
                {
                    list.RemoveAll(x => x.Type == 0 && (x.IsWeekly == null || x.IsWeekly == false));
                }
            }

            foreach (GetHolidaysForAttendenceResult holidayToDelete in nonbranchholidaylist)
            {
                list.Remove(holidayToDelete);
            }


            // If calendar is nepali then set nepali date for current month
            if (SessionManager.IsEnglish == false)
            {
                GetHolidaysForAttendenceResult dateRange = new GetHolidaysForAttendenceResult();

                dateRange.Id = 0;

                try
                {

                    CustomDate date1 = new CustomDate(startDateEng.Day, startDateEng.Month, startDateEng.Year, true);
                    date1 = CustomDate.ConvertEngToNep(date1);


                    CustomDate date2 = new CustomDate(endDateEng.Day, endDateEng.Month, endDateEng.Year, true);
                    date2 = CustomDate.ConvertEngToNep(date2);


                    string[] yearNos = new string[4];
                    yearNos[0] = date1.Year.ToString()[0].ToString();
                    yearNos[1] = date1.Year.ToString()[1].ToString();
                    yearNos[2] = date1.Year.ToString()[2].ToString();
                    yearNos[3] = date1.Year.ToString()[3].ToString();

                    dateRange.Abbreviation
                        = Resources.NepaliCalendar.ResourceManager.GetString(date1.Day.ToString()) + " " + Resources.NepaliCalendar.ResourceManager.GetString(DateHelper.GetMonthName(date1.Month, false))
                        + ", " +
                        Resources.NepaliCalendar.ResourceManager.GetString("N" + yearNos[0]) +
                        Resources.NepaliCalendar.ResourceManager.GetString("N" + yearNos[1]) +
                        Resources.NepaliCalendar.ResourceManager.GetString("N" + yearNos[2]) +
                        Resources.NepaliCalendar.ResourceManager.GetString("N" + yearNos[3])
                        ;

                    yearNos[0] = date2.Year.ToString()[0].ToString();
                    yearNos[1] = date2.Year.ToString()[1].ToString();
                    yearNos[2] = date2.Year.ToString()[2].ToString();
                    yearNos[3] = date2.Year.ToString()[3].ToString();

                    dateRange.Abbreviation
                        += "  -  " + Resources.NepaliCalendar.ResourceManager.GetString(date1.Day.ToString()) + " " + Resources.NepaliCalendar.ResourceManager.GetString(DateHelper.GetMonthName(date2.Month, false))
                        + ", " +
                        Resources.NepaliCalendar.ResourceManager.GetString("N" + yearNos[0]) +
                        Resources.NepaliCalendar.ResourceManager.GetString("N" + yearNos[1]) +
                        Resources.NepaliCalendar.ResourceManager.GetString("N" + yearNos[2]) +
                        Resources.NepaliCalendar.ResourceManager.GetString("N" + yearNos[3])
                        ;
                }
                catch { }
                list.Add(dateRange);

                // add nep date start date
                DateTime firstDay = FirstDay(month, year);
                CustomDate date = new CustomDate(firstDay.Day, firstDay.Month, firstDay.Year, true);
                date = CustomDate.ConvertEngToNep(date);
                GetHolidaysForAttendenceResult nepStartDate = new GetHolidaysForAttendenceResult();
                nepStartDate.Id = -1;
                nepStartDate.Abbreviation = date.ToString();
                nepStartDate.MaxMonthDays = DateHelper.GetTotalDaysInTheMonth(date.Year, date.Month, false);
                list.Add(nepStartDate);
            }

            return list;
        }


        [WebMethod]
        public List<GetHolidaysForAttendenceResult> GetTimesheet(int month, int year)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
            {
                return new List<GetHolidaysForAttendenceResult>();
            }
            month += 1;

            //DateTime startDateEng = new DateTime(year, month, 1);
            //DateTime endDateEng = new DateTime(year, month, DateTime.DaysInMonth(year, month));
            List<GetHolidaysForAttendenceResult> list = new List<GetHolidaysForAttendenceResult>();

            List<DateTime> sundayList = NewTimeSheetManager.getSundays(month, year);
            string notes = "";
            DAL.Timesheet timesheet = null;
            foreach (DateTime sunday in sundayList)
            {
                GetHolidaysForAttendenceResult newHoliday = new GetHolidaysForAttendenceResult();
                //newHoliday.Id = weekHoliday.Id;
                TimeSheetStatus status = NewTimeSheetManager.GetTimesheetStatus(sunday, SessionManager.CurrentLoggedInEmployeeId, ref notes,ref timesheet);
                newHoliday.Name = "Week : " + NewTimeSheetManager.GetIso8601WeekOfYear(sunday) + ", Timesheet : " + status;

                //newHoliday.IsWeekly = weekHoliday.IsWeekly;
                newHoliday.Type = status == TimeSheetStatus.NotSubmitted ? 0 : 1;
                //newHoliday.BranchIDList = "";
                newHoliday.DateEng = sunday;

                list.Add(newHoliday);
            }


            return list;
        }


        public DateTime FirstDay(int month,int year)
        {
            var date = new DateTime(year, month, 1);
            while (true)
            {
                if (date.DayOfWeek == DayOfWeek.Sunday)
                    return date;
                date = date.AddDays(-1);
            }
           
        }

        [WebMethod]
        public Paging<LeaveRequestBO> GetEmployeePendingLeaveRequestList(int start, int limit)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
            {
                return new Paging<LeaveRequestBO>();
            }

            int count;
            List<LeaveRequestBO> list = LeaveAttendanceManager.GetUnApprovedLeaveRequest(
                SessionManager.CurrentLoggedInEmployeeId);
            count = list.Count;

            if ((start + limit) > count)
            {
                limit = count - start;
            }

            List<LeaveRequestBO> rangeList = (start < 0 || limit < 0) ? list : list.GetRange(start, limit);

            return new Paging<LeaveRequestBO>(rangeList, list.Count); 
            //return LeaveAttendanceManager.GetUnApprovedLeaveRequest(SessionManager.CurrentLoggedInEmployeeId);
        }

        [WebMethod]
        public Paging<LeaveRequestBO> GetEmployeeApprovedLeaveRequestList(int start, int limit)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
            {
                return new Paging<LeaveRequestBO>();
            }

            int count;
            List<LeaveRequestBO> list =  LeaveAttendanceManager.GetApprovedLeaveRequest(SessionManager.CurrentLoggedInEmployeeId,out count);
            if ((start + limit) > count)
            {
                limit = count - start;
            }
            List<LeaveRequestBO> rangeList = (start < 0 || limit < 0) ? list : list.GetRange(start, limit);

            return new Paging<LeaveRequestBO>(rangeList, list.Count); 
        }


        /// <summary>
        /// Used to cancel the leave instead of delete, actual delete can't happen
        /// </summary>
        /// <param name="leaveRequestId"></param>
        /// <returns></returns>
        [WebMethod]
        public ResponseStatus DeleteLeaveRequest(int leaveRequestId)
        {

            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
                return null;

            ResponseStatus status = new ResponseStatus();// LeaveRequestManager.ValidateLeaveRequestDelete(leaveRequestId);

            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
            {
                return status;
            }
            LeaveRequest leave = LeaveAttendanceManager.GetLeaveRequestById(leaveRequestId);

            if (leave.EmployeeId != SessionManager.CurrentLoggedInEmployeeId)
            {
                status.ErrorMessage = "Other's leave can not be changed.";
                status.IsSuccess = "false";
                return status;
            }

            if (leave.Status != (int)LeaveRequestStatusEnum.Request && leave.Status != (int)LeaveRequestStatusEnum.ReDraft)
            {
                status.ErrorMessage = "Only Request or ReDraft status leave can be deleted.";
                status.IsSuccess = "false";
                return status;
            }

            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
            {
                status.ErrorMessage = "Not enough permission.";
                status.IsSuccess = "false";
                return status;
            }

            if (status.IsSuccess == "false")
                return status;

            if (LeaveRequestManager.DeleteSelfLeave(leaveRequestId))
            {
                status.IsSuccess = "true";
                //LeaveRequestManager.NotifyThroughMessageAndEmail(LeaveAttendanceManager.GetLeaveRequestById(leaveRequestId));
            }
            else
                status.IsSuccess = "false";

            //if (LeaveAttendanceManager.DeleteEmployeeLeaveRequest(leaveRequestId, SessionManager.CurrentLoggedInEmployeeId))
            //{
            //    status.ErrorMessage = "Leave request deleted.";
            //    status.IsSuccess = "true";
            //}
            //else
            //{
            //    status.ErrorMessage = "Leave request delete failed.";
            //    status.IsSuccess = "false";
            //}
            return status;
        }

        //[WebMethod]
        //public ResponseStatus CancelledApprovedLeaveRequest(int leaveRequestId)
        //{
        //    ResponseStatus status = new ResponseStatus();

        //    if (HttpContext.Current.User.Identity.IsAuthenticated == false)
        //    {
        //        status.ErrorMessage = "Not enough permission.";
        //        status.IsSuccess = "false";
        //        return status;
        //    }

        //    status = LeaveRequestManager.CancelledApprovedLeave(leaveRequestId,"");

        //    if (status.IsSuccess=="true")
        //    {
        //        status.IsSuccess = "true";
        //        LeaveRequestManager.NotifyThroughMessageAndEmail(LeaveAttendanceManager.GetLeaveRequestById(leaveRequestId));
        //    }
        //    else
        //        status.IsSuccess = "false";
               

        //    return status;
        //}

        //[WebMethod]
        //public Paging<GetLeaveByDateResult> GetLeaveDetails( DateTime FromDateEng, int Status, int companyId, bool isYearly, int start, int limit)
        //{
        //    if (HttpContext.Current.User.Identity.IsAuthenticated == false)
        //    {
        //        return new Paging<GetLeaveByDateResult>();
        //    }

        //    //used for get Leave Request
        //    //-1 signifies that it will fetch data only for current and next month.
        //    // else wise fetch data for starting payrollperioddate till date.
        //    return LeaveAttendanceManager.GetLeaveByDate(Status, companyId, isYearly, -1, start, limit);
        //}

        //[WebMethod]
        //public Paging<GetAllLeaveByDateAndEmployeeResult> GetAllLeaveByDateAndEmployee
        //    (DateTime FromDateEng, int employeeId, bool isYearly, int payrollPeriodId, int start, int limit)
        //{
        //    if (HttpContext.Current.User.Identity.IsAuthenticated == false)
        //        return new Paging<GetAllLeaveByDateAndEmployeeResult>();

        //    return LeaveAttendanceManager.GetAllLeaveByDateAndEmployee(employeeId, isYearly, payrollPeriodId, start, limit);
        //}

        [WebMethod]
        public Paging<GetLeaveByDateApprovalResult> GetLeaveByDateApproval
            (DateTime FromDateEng, string Status, int companyId, int start, int limit, int leaveRequestEmployeeId, 
            int ExcludeLeaveRequestId,string Branch,string Employee)
        {

            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
                return new Paging<GetLeaveByDateApprovalResult>();

            //used for get Leave Request
            //-1 signifies that it will fetch data only for current and next month.
            // else wise fetch data for starting payrollperioddate till date.
            List<GetLeaveByDateApprovalResult> list = LeaveAttendanceManager
                .GetLeaveByDateApproval(Status, companyId, -1, start, limit, leaveRequestEmployeeId, ExcludeLeaveRequestId, Branch, Employee, SessionManager.CurrentLoggedInEmployeeId);

            foreach (GetLeaveByDateApprovalResult item in list)
            {
                item.Created = item.CreatedOn == null ? "" : item.CreatedOn.Value.ToString("yyyy-MMM-dd");
                item.From = item.FromDate.Value.ToString("yyyy-MMM-dd");
                item.To = item.ToDate.Value.ToString("yyyy-MMM-dd");
            }

            int total = 0;
            if (list.Count > 0)
                total = list[0].TotalRows.Value;

            return new Paging<GetLeaveByDateApprovalResult>(list, total);
        }


      

        //[WebMethod]
        //public Paging<GetLeaveByDateResult> GetApprovedLeaveDetails(DateTime FromDateEng, int Status, int companyId, bool isYearly, int start, int limit)
        //{
        //    if (HttpContext.Current.User.Identity.IsAuthenticated == false)
        //        return new Paging<GetLeaveByDateResult>();
           
        //    int payrollPeriodId = -1, startingPayrollPeriodId = -1, endingPayrollPeriodId = -1;
        //    bool readingSumForMiddleFiscalYearStartedReqd = false;
        //    PayrollPeriod payrollPeriod = CommonManager.GetLastPayrollPeriod();

        //    if (payrollPeriod != null)
        //    {
        //        payrollPeriodId = payrollPeriod.PayrollPeriodId;
        //        PayrollPeriod startingMonthParollPeriodInFiscalYear = CalculationManager.GenerateForPastIncome(
        //                 payrollPeriodId, ref readingSumForMiddleFiscalYearStartedReqd, ref startingPayrollPeriodId, ref endingPayrollPeriodId);

        //        return LeaveAttendanceManager.GetLeaveByDate(
        //            Status, companyId, isYearly, startingPayrollPeriodId, start, limit);
        //    }
        //    else
        //    {
        //        return new Paging<GetLeaveByDateResult>(new List<GetLeaveByDateResult>(), 0);
        //    }

        //}

        
        [WebMethod]
        public EventModelCollection GetTrainings(DateTime? start, DateTime? end, DateTime? StartDate, DateTime? EndDate)
        {
            //start = StartDate;
            //end = EndDate;

            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
                return new EventModelCollection();

            List<Training> list = TrainingManager.GetTrainingsByStartEndDate(start.Value, end.Value);
            EventModelCollection events = new EventModelCollection();

            foreach (var request in list)
            {
                EventModel entity = new EventModel();
                entity.EventId = request.TrainingID;
                if (request.Type == 0)
                    entity.CalendarId = -1;
                else
                {
                    if (request.Status == (int)TrainingStatusEnum.Publish)
                        entity.CalendarId = 1;
                    else if (request.Status == (int)TrainingStatusEnum.Saved)
                        entity.CalendarId = 2;
                    else
                        entity.CalendarId = 3;
                }

                entity.Title = request.Name;

                DateTime startTime = new DateTime(1, 1, 1, 0, 0, 0);
                DateTime endTime = new DateTime(1, 1, 1, 1, 0, 0);

                if (request.StartTime != null)
                    startTime = DateTime.Parse(request.StartTime.ToString());
                if (request.EndTime != null)
                    endTime = DateTime.Parse(request.EndTime.ToString());

                entity.StartDate = new DateTime(request.FromDate.Value.Year, request.FromDate.Value.Month, request.FromDate.Value.Day, startTime.Hour, startTime.Minute, startTime.Second);
                entity.EndDate = new DateTime(request.ToDate.Value.Year, request.ToDate.Value.Month, request.ToDate.Value.Day, endTime.Hour, endTime.Minute, endTime.Second);//request.ToDateEng.Value;
                //entity.IsNew = false;

           
                //if (entity.CalendarId.Equals(-1))
                //    entity.Notes = Resources.Messages.UnpaidLeaveTitleForLeaveRequest;
                //else
            

             


                events.Add(entity);

            }
            return events;
        }


       
        #endregion


        [WebMethod]
        public EventModelCollection GetTimeRequests(DateTime? start, DateTime? end, DateTime? StartDate, DateTime? EndDate)
        {
            start = StartDate;
            end = EndDate;

            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
                return new EventModelCollection();

            DAL.AttendanceMapping map = BLL.BaseBiz.PayrollDataContext.AttendanceMappings
                                 .SingleOrDefault(x => x.EmployeeId == SessionManager.CurrentLoggedInEmployeeId);

            string deviceID = ""; //map.DeviceId;

            if (map != null)
                deviceID = map.DeviceId;

            List<TimeRequest> listTimeRequest = AttendanceManager.GetTimeRequestListByDateRange(StartDate.Value, EndDate.Value);
            EventModelCollection events = new EventModelCollection();

            TimeSpan ts = end.Value - start.Value;
            int totalDays = ts.Days;

            string IPAddress = HttpContext.Current.Request.UserHostAddress;

            List<DAL.AttendanceReportResult> atteList =
                BLL.BaseBiz.PayrollDataContext.AttendanceReport(SessionManager.CurrentLoggedInEmployeeId, 0, start, end, 0,0,99999).ToList();

            string value = "";

            for (int i = 0; i < totalDays; i++)
            {
                //List<AttendanceCheckInCheckOut> list = AttendanceManager.GetAttendanceCheckInCheckOutByDeviceIdAndDate(deviceID, start.Value.AddDays(i));
                //List<AttendanceCheckInCheckOut> list = AttendanceManager.GetAttendanceCheckInCheckOutByIPAddressAndDate(IPAddress, start.Value.AddDays(i));

                AttendanceReportResult item = atteList.FirstOrDefault(x => x.DateEng.Value.Date == start.Value.AddDays(i).Date);

                value = "";

                if (item != null)
                {

                    EventModel entity = new EventModel();
                    entity.EventId = 0;


                    if (item.Type != null)
                    {
                        if (item.Type == (int)TimeAttendenceDayType.Holiday)
                            value = "H";

                        else if (item.Type == (int)TimeAttendenceDayType.WeeklyHoliday
                            && item.DayType.ToLower().Contains("half"))
                        {

                            if (item.WorkHour != null)
                                value = AttendanceManager.MinuteToHourMinuteConverterWithOutLabelDisplay(Math.Abs((int)item.WorkHour));
                        }

                        else if (item.Type == (int)TimeAttendenceDayType.WeeklyHoliday)
                            value = "WH";
                        else if (item.Type == (int)TimeAttendenceDayType.Leave && item.DayType.ToLower().Contains("half"))
                        {
                            if (item.WorkHour != null)
                                value = AttendanceManager.MinuteToHourMinuteConverterWithOutLabelDisplay(Math.Abs((int)item.WorkHour));
                        }
                        else if (item.Type == (int)TimeAttendenceDayType.Leave)
                            value = item.LeaveName;
                        else if (item.DayType.ToLower().Contains("working day"))
                        {
                            if (item.WorkHour != null)
                                value = AttendanceManager.MinuteToHourMinuteConverterWithOutLabelDisplay(Math.Abs((int)item.WorkHour));
                        }
                            // for absent or UPl
                        else
                            value = item.DayType;
                    }
                    else
                        value = "";

                    entity.Title = value;
                    entity.Notes = value;
                    entity.CalendarId = 1;

                    entity.IsAllDay = false;

                    DateTime dtValue = start.Value.AddDays(i);

                    entity.StartDate = new DateTime(dtValue.Year, dtValue.Month, dtValue.Day, 0, 0, 0);
                    entity.EndDate = new DateTime(dtValue.Year, dtValue.Month, dtValue.Day, 0, 0, 0);//request.ToDateEng.Value;
                    events.Add(entity);
                }

            }

            foreach (var item in listTimeRequest)
            {
                EventModel entity = new EventModel();
                entity.EventId = item.RequestID;

                if (item.RequestID == 0)
                {
                    entity.Title = "";
                    entity.Notes = "";
                    entity.CalendarId = -1;
                }
              
                if (item.Status.Value == (int)AttendanceRequestStatusEnum.Saved)
                {
                    entity.Title = "Requested";
                    entity.Notes = "Requested";
                    entity.CalendarId = 2;
                }
                else if (item.Status.Value == (int)AttendanceRequestStatusEnum.Approved)
                {
                    entity.Title = "Approved";
                    entity.Notes = "Approved";
                    entity.CalendarId = 3;
                }
                else if (item.Status.Value == (int)AttendanceRequestStatusEnum.Recommended)
                {
                    entity.Title = "Recommend";
                    entity.Notes = "Recommend";
                    entity.CalendarId = 3;
                }
                else
                {
                    entity.Title = "Rejected";
                    entity.Notes = "Rejected";
                    entity.CalendarId = 4;
                }

                DateTime startTime = new DateTime(1, 1, 1, 0, 0, 0);
                DateTime endTime = new DateTime(1, 1, 1, 1, 0, 0);

                if (item.StartDateEng != null)
                    startTime = DateTime.Parse(item.StartDateEng.Value.ToString());
                if (item.EndDateEng != null)
                    endTime = DateTime.Parse(item.EndDateEng.Value.ToString());

                entity.StartDate = new DateTime(item.StartDateEng.Value.Year, item.StartDateEng.Value.Month, item.StartDateEng.Value.Day, startTime.Hour, startTime.Minute, startTime.Second);
                entity.EndDate = new DateTime(item.EndDateEng.Value.Year, item.EndDateEng.Value.Month, item.EndDateEng.Value.Day, endTime.Hour, endTime.Minute, endTime.Second);//request.ToDateEng.Value;

                entity.IsAllDay = true;

                events.Add(entity);

            }
            return events;
        }

    }
}
