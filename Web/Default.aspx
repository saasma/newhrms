﻿<%@ Page Language="C#" AutoEventWireup="true" Title="Rigo : Human Resource Management System"
    CodeBehind="Default.aspx.cs" Inherits="Web.Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="stylesheet" href="css/core.css?v=1" type="text/css" />
    <link href="css/style.default.css" rel="stylesheet" />
    <style type="text/css">
        a
        {
            color: #FFFFFF;
        }
        a:hover
        {
            color: #FF5500;
        }
        
        .submit
        {
            text-align: center;
            font-weight: bold;
        }
        #bodypart
        {
            background-color: White !important;
        }
        .invalid
        {
            border: 0px;
        }
    </style>
    <script type="text/javascript">
        
    </script>
</head>
<body id="wrapper" style="background-color: #FBFDFE!important">
    <form id="form1" runat="server">
    <div id="bodypart" style="min-height: 580px; background-color: #FBFDFE!important;
        '">
        <asp:Label ID="lblDemoMsg" runat="server" EnableViewState="true" CssClass="invalid"
            ForeColor="Red"></asp:Label>
        <section>
             <div class="logo text-center">
                        
                    </div>
           

            <div class="panel panel-signin" style='border:1px solid #CCCCCC'>
                  <div style='text-align:center'>
                  <img id="imgCompanyLogo" runat="server" src="images/company_logo.png" alt=" Company Logo"
                    style="" />
                    </div>
                <div class="panel-body" style='padding-left:25px;padding-right:25px;'>
                   
                
                   
                    <h4 class="text-center mb5">Login to Your Account</h4>
                  
                       <asp:Label ID="lblMsg" style='clear:inherit;' runat="server" EnableViewState="False" CssClass="invalid"
                ForeColor="Red"></asp:Label>
                    <div class="mb30"></div>
                    
                  
                        <div class="input-group mb15">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                            <asp:TextBox ID="txtUserName" placeholder="Username"  class="form-control" runat="server" ></asp:TextBox>
                            <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtUserName"
                                Display="Dynamic" ErrorMessage="RequiredFieldValidator" ToolTip="User name is required.">*</asp:RequiredFieldValidator>--%>
                        </div><!-- input-group -->
                        <div class="input-group mb15">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                             <asp:TextBox AutoComplete="off"  placeholder="Password" class="form-control" ID="txtPassword" TextMode="Password" runat="server"
                              ></asp:TextBox>
                             <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtPassword"
                                Display="Dynamic" ErrorMessage="RequiredFieldValidator" ToolTip="Password is required.">*</asp:RequiredFieldValidator>--%>
                        </div><!-- input-group -->
                        
                        <div class="clearfix">
                           
                            <div class="pull-right">
                                <asp:Button ID="btnLogin"  style='background-color:#88B55C' class="btn btn-success" Text="Login"  Width='348px' OnClick="btnLogin_Click" runat="server" />
                            </div>
                        </div>                      
                    
                    
                </div><!-- panel-body -->
                <div class="panel-footer">
                 <a id="linkForgotPwd" style='color:#428bca;text-align:center;' class="btn-block" runat="server" href="~/Employee/ForgotPassword.aspx" >
                                Forgotten password?</a>

                </div><!-- panel-footer -->
            </div><!-- panel -->
            <div style='text-align:center;padding-top: 15px;'>
                <asp:Label runat="server" ID="lblYear" Text="©2010 - " />
                <a style='color:#428bca;' href="http://rigonepal.com">Rigo Technologies </a> v.<asp:Label runat="server" ID="version" />
            </div>
        </section>
    </div>
    </form>
    <script type="text/javascript" src="Scripts/jquery-1.3.2.min.js"></script>
    <%--    <script>
        var $buoop = { vs: { i: 9, f: 25, o: 17, s: 6, c: 30 } }; 
        function $buo_f() {
            var e = document.createElement("script");
            e.src = "scripts/update.js";
            document.body.appendChild(e);
        };
        try { document.addEventListener("DOMContentLoaded", $buo_f, false) }
        catch (e) { window.attachEvent("onload", $buo_f) }
</script> --%>
</body>
<style type="text/css">
    #bodypart
    {
        background-color: white !important;
    }
    #wrapper
    {
        background-color: #428BCA !important;
    }
</style>
</html>
