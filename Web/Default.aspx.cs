﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Manager;
using DAL;
using BLL;
using Utils.Security;
using System.Web.Security;
using System.Data.Linq;
using Utils;
using System.IO;
using Utils.Helper;
using Web.Helper;
namespace Web
{

    public class BrutalForceAttackPrevention
    {
        private static int AccountLockedMinutes = Config.MinsToLockUserNameForInvalidAttempts;
        private static int AccountAccessTimeBeforeLock = Config.MaxInvalidPasswordAttempts;

        public class HitInfo
        {
            public DateTime LastInvalidPasswordDateTime { get; set; }
            public int TotalInvalidPasswordCount { get; set; }
            public bool IsAccountInLockedState { get; set; }

        }

        //private Dictionary<string, HitInfo> users = new Dictionary<string, HitInfo>();

        public static Dictionary<string, HitInfo> UserList
        {
            get
            {
                if (HttpContext.Current.Cache["UserList"] == null)
                {

                    Dictionary<string, HitInfo> list = new Dictionary<string, HitInfo>();
                    HttpContext.Current.Cache["UserList"] = list;

                    return list;
                }

                return HttpContext.Current.Cache["UserList"] as Dictionary<string, HitInfo>;
            }
        }


        public static bool IsAcccountLocked(string userName, ref string remTime)
        {
            HitInfo info = null;

            if (UserList.ContainsKey(userName.ToLower()))
            {
                info = UserList[userName.ToLower()];

                TimeSpan lockedTimeSpan = (DateTime.Now - info.LastInvalidPasswordDateTime);

                int minutes = lockedTimeSpan.Minutes;

                if (info.IsAccountInLockedState && minutes >= 0 && minutes < AccountLockedMinutes)
                {

                    int remMin = 0, remSec = 0;
                    remMin = AccountLockedMinutes - lockedTimeSpan.Minutes;
                    remSec = 60 - lockedTimeSpan.Seconds;

                    if (remSec > 0)
                        remMin -= 1;

                    remTime = remMin + ":" + remSec;


                    return true;
                }
                else if (info.IsAccountInLockedState && minutes >= 0 && minutes >= AccountLockedMinutes)
                {
                    info.IsAccountInLockedState = false;
                }

            }


            return false;
        }

        public static void ClearLockedForUserName(string username)
        {
            if (UserList.ContainsKey(username.ToLower()))
            {
                UserList.Remove(username.ToLower());
            }

        }

        public static void IncrementInvalidPasswordLockTimes(string userName)
        {
            HitInfo info = null;

            if (UserList.ContainsKey(userName.ToLower()))
            {
                info = UserList[userName.ToLower()];
                info.LastInvalidPasswordDateTime = DateTime.Now;
                info.TotalInvalidPasswordCount += 1;

            }
            else
            {
                info = new HitInfo();
                info.LastInvalidPasswordDateTime = DateTime.Now;
                info.TotalInvalidPasswordCount = 1;
            }


            int minutes = (DateTime.Now - info.LastInvalidPasswordDateTime).Minutes;

            // Account is locked
            if (minutes >= 0 && minutes < AccountLockedMinutes && info.TotalInvalidPasswordCount >= AccountAccessTimeBeforeLock)
            {
                info.IsAccountInLockedState = true;
                info.TotalInvalidPasswordCount = 0;
            }

            UserList[userName.ToLower()] = info;


        }

    }

    public partial class Default : System.Web.UI.Page
    {

       
       
        protected void Page_Load(object sender, EventArgs e)
        {

            txtUserName.Focus();
            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.Care)
            {
                this.Page.Title = "Care HRIS";
            }
            
            if (User.Identity.IsAuthenticated)
            {
                //Checck for Windows authentication
                if (User.Identity.Name.Contains("\\")) { }
                //ProcessWindowAuthLogin(User.Identity.Name);
                else
                {
                     string role = "";
                    int roleId = 0;
                    if (new UserManager().SetUserInfo(User.Identity.Name, ref role, ref roleId))
                    {
                        SuccessLogin(User.Identity.Name, role, roleId, SessionManager.IsLDAPLogin);
                    }
                }
            }

            // if LDAP is enabled in license or (no license and has enabled LDAP)
            if (LdapHelper.IsLDAPAccessible())
                linkForgotPwd.Visible = false;

            //For Load Testing
            //SecurityHelper.CreateCookie("hari", false, "Employee", false, false);
            //Response.Redirect("~/Employee/PersonalDetails.aspx");

            version.Text = WebHelper.Version.ToString();
           
			//check if logo exists or not
            string loc = Server.MapPath(imgCompanyLogo.Src);
            if (!File.Exists(loc))
                imgCompanyLogo.Visible = false;

            if (!IsPostBack)
                lblYear.Text += DateTime.Now.Year;
        }

        public string GetClientIP()
        {
            HttpRequest currentRequest = HttpContext.Current.Request;
            string ipAddress = currentRequest.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (ipAddress == null || ipAddress.ToLower() == "unknown")
                ipAddress = currentRequest.ServerVariables["REMOTE_ADDR"];

            return ipAddress;
        }


        protected void btnLogin_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                string pwd = (txtPassword.Text.Trim());
                string userName = txtUserName.Text.Trim();
                Process(userName, pwd);
            }


            

        }

        public bool IsIPAllowableToAccess(string ipList)
        {
            string[] IPs = ipList.Split(new char[] { ',' });
            string userIP = GetClientIP();

            foreach (string serverIP in IPs)
            {
                if (userIP.StartsWith(serverIP))// && Request.Browser.Browser.ToLower().Equals("ie"))
                {
                    return true;
                }
            }

            if (this.Request.IsLocal)
                return true;
            return false;
        }
        protected void Process(string username,string pwd)
        {
            UserManager mgr = new UserManager();
            string role = "";
            UUser user = null;
            int roleId = 0;


            //user = UserManager.GetUserByUserName(username, false);
            //if (user != null && !string.IsNullOrEmpty(user.AllowableIPList))
            //{
            //    //if not not in the allowable IP list then don't allow to login
            //    if (!IsIPAllowableToAccess(user.AllowableIPList))
            //    {
            //        BLL.BaseBiz.PayrollDataContext.ChangeUserActivities.InsertOnSubmit(
            //        BLL.BaseBiz.GetChangeUserActivityLog("Logging fail for user " + username + " from IP " + GetClientIP() + ", access from inaccessible IP address.",
            //            username ));
            //        BLL.BaseBiz.PayrollDataContext.SubmitChanges();

                 
            //        lblMsg.Style.Add("display", "block");
            //        lblMsg.Text = "Inaccessible IP address.";
            //        return;
            //    }

            //}

            string remTime = "";
            if (BrutalForceAttackPrevention.IsAcccountLocked(username, ref remTime))
            {
                BLL.BaseBiz.PayrollDataContext.ChangeUserActivities.InsertOnSubmit(
                  BLL.BaseBiz.GetChangeUserActivityLog("Locked account access for " + username + " from IP " + GetClientIP() + ".", username));
                BLL.BaseBiz.PayrollDataContext.SubmitChanges();

             
                lblMsg.Style.Add("display", "block");
                lblMsg.Text = "Account is locked for " + remTime + " mins. Please try after sometime.";
                return;
            }
            bool isEmpRetired = false;
            var isEmpPortalDisabled = false;
            if (mgr.ValidateUser(username, pwd, ref role, ref roleId, ref user, false, ref isEmpRetired,ref isEmpPortalDisabled))
            {
                BrutalForceAttackPrevention.ClearLockedForUserName(username);
                SuccessLogin(username, role, roleId,false);
            }
            else
            {
                if (isEmpPortalDisabled)
                {
                    lblMsg.Style.Add("display", "block");
                    lblMsg.Text = "Employee portal login has been disabled, please contact HR for details.";
                    return;
                }


                BrutalForceAttackPrevention.IncrementInvalidPasswordLockTimes(username);

                BLL.BaseBiz.PayrollDataContext.ChangeUserActivities.InsertOnSubmit(
                    BLL.BaseBiz.GetChangeUserActivityLog("Logging fail for user " + username + " from IP " + GetClientIP() + ".", username));
                BLL.BaseBiz.PayrollDataContext.SubmitChanges();

                Log.log("User Logging fail for user " + username + " at with " + DateTime.Now, "");
                lblMsg.Style.Add("display", "block");
                lblMsg.Text = "Invalid user name or password.";



                //if trying to login from empoloyee then rediect to actual login page
                //if (UserManager.IsEmployeeUserName(username))
                //{
                //    string code = "setTimeout(function() {window.location = 'Employee/Default.aspx';},3000);";
                //    lblMsg.Text = "Employee can not login from this page, redirecting to employee login.";
                //    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdfsdf23",
                //        code,true);
                //    //return;
                //}


            }

        }

        public void SessionStartLoggedIn()
        {
            Application.Lock();
            List<Web.Global.LoggedInUser> list = Application["UsersOnline"] as List<Web.Global.LoggedInUser>;
            if (list != null && !string.IsNullOrEmpty(User.Identity.Name))
            {
                string userName = User.Identity.Name;
                if (list.Any(x => x.UserName == userName) == false)
                {
                    UUser user = UserManager.GetUserByUserName(userName);
                    Web.Global.LoggedInUser log = new Web.Global.LoggedInUser();
                    log.IsEmployee = user.IsEmployeeType == null ? false : user.IsEmployeeType.Value;
                    log.UserName = userName;
                    log.IPAddress = GetClientIP();
                    list.Add(log);
                }


                Session["UserName"] = User.Identity.Name;
            }
            //Application["UsersOnline"] = (int)Application["UsersOnline"] + 1;
            Application.UnLock();
        }




        protected void SuccessLogin(string username, string role, int roleId, bool isADLogin)
        {
            BLL.BaseBiz.PayrollDataContext.ChangeUserActivities.InsertOnSubmit(
                  BLL.BaseBiz.GetChangeUserActivityLog("User logged in " + username + " from IP " + GetClientIP() + ".",username));
            BLL.BaseBiz.PayrollDataContext.SubmitChanges();

            //Log.log("User logged in " + username + " at " + DateTime.Now, "");

            SecurityHelper.CreateCookie(username, true, role, false, isADLogin);

            Role userRole = (Role)roleId;

            //If no default company as happens at starting, then redirect to
            //AECompany to create new one
            if (SessionManager.CurrentCompanyId == 0)
            {
                Response.Redirect("~/CP/AddEditCompany.aspx", true);
                return;
            }

            switch (userRole)
            {
                case Role.Administrator:

                    Response.Redirect("~/newhr/DashboardHR.aspx", true);
                    break;
                case Role.Employee:

                    Response.Redirect("~/Employee/Dashboard.aspx", true);
                    break;
                default:
                    UserManager.RedirectToProperModuleDashboardIfHasOnlySingleModulePermission(roleId, username);
                   
                    Response.Redirect("~/newhr/DashboardHR.aspx", true);
                    break;
            }
        }
        protected void ReturnUrl()
        {
            this.Response.Redirect(FormsAuthentication.LoginUrl +
               "?ReturnUrl=" + this.Request.Url.PathAndQuery);
        }
    }
}
