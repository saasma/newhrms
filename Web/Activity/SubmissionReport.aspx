﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SubmissionReport.aspx.cs"
    Inherits="Web.Activity.SubmissionReport" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Submission Report</title>
    <link href="/resources/css/examples.css" rel="stylesheet" />

    <script type="text/javascript">

        var CommandHandler = function (command, record) {
            var employeeID = record.data.EmployeeId;
            var FromName = record.data.TrainingName;
            var Details = record.data.Details;
            var Important = record.data.Importance;
            var RecommendedBy = record.data.RecommendedBy;
            //window.open("TraningRequestForm.aspx?id=" + employeeID);

            Ext.Msg.alert('Command', 'From Name = ' + FromName + '<br/><br/>' + 'Details = ' + Details + '<br/><br/>' + ' Why is this mportant? ' + Important + '<br/><br/>' + 'Recomended By = ' + RecommendedBy);


            // console.log(employeeID);
            // console.log(employeeID);
        };
        
    </script>
</head>
<body>
    <ext:ResourceManager ID="ResourceManager1" runat="server" />
    <form runat="server" id="form1">
    <ext:Store ID="storeBranches" runat="server">
        <Model>
            <ext:Model ID="modelBranches" runat="server">
                <Fields>
                    <ext:ModelField Name="Name" Type="String" />
                    <ext:ModelField Name="BranchId" Type="Int" />
                </Fields>
            </ext:Model>
        </Model>
    </ext:Store>
    <ext:Store ID="storegrid" runat="server">
        <Model>
            <ext:Model ID="modelgrid" runat="server">
                <Fields>
                    <ext:ModelField Name="EmployeeId" Type="Int" />
                    <ext:ModelField Name="Employee" Type="String" />
                    <ext:ModelField Name="Details" Type="String" />
                    <ext:ModelField Name="Importance" Type="String" />
                    <ext:ModelField Name="Branch" Type="String" />
                    <ext:ModelField Name="Position" Type="String" />
                    <ext:ModelField Name="TrainingName" Type="String" />
                    <ext:ModelField Name="SubmittedOnEng" Type="Date" />
                    <ext:ModelField Name="RecommendedBy" Type="String" />
                    <ext:ModelField Name="RecommendedOn" Type="Date" />
                </Fields>
            </ext:Model>
        </Model>
    </ext:Store>
    <ext:Panel ID="Panel3" runat="server" Frame="false" PaddingSummary="5px 5px 0" Width="1000"
        ButtonAlign="Center">
        <Items>
            <ext:Label ID="Label1" runat="server" Text="Narrow Your List:" />
        </Items>
        <Items>
            <ext:Container ID="Container1" runat="server" Layout="Column">
                <Items>
                    <ext:Container ID="Container2" runat="server" Layout="FormLayout" ColumnWidth=".25"
                        Padding="5">
                        <Items>
                            <ext:SelectBox ID="ddlBranch" runat="server" DisplayField="Name" ValueField="BranchId"
                                StoreID="storeBranches" EmptyText="Select Branch...">
                            </ext:SelectBox>
                            <ext:Label ID="Label2" runat="server" Text="Group By:" />
                            <ext:SelectBox ID="ddlGroupBy" runat="server" DisplayField="Name" ValueField="ID"
                                >
                                 <Store>
                                    <ext:Store ID="Store1" runat="server">
                                        <Model>
                                            <ext:Model ID="Model1" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="Name" />
                                                    <ext:ModelField Name="ID" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>            
                                    </ext:Store>    
                                 </Store>    
                            </ext:SelectBox>
                            <%--<ext:SelectBox ID="ddlLevel" runat="server" DisplayField="Level" ValueField="LevelId"
                                EmptyText="None">
                            </ext:SelectBox>--%>
                        </Items>
                    </ext:Container>
                    <ext:Container ID="Container3" runat="server" Layout="FormLayout" ColumnWidth=".25"
                        Padding="5">
                        <Items>
                            <ext:SelectBox ID="ddlPosition" runat="server" DisplayField="Level" ValueField="LevelId"
                                EmptyText="Select Position.">
                            </ext:SelectBox>
                            <ext:DateField ID="TxtStartDate" runat="server" FieldLabel="Start Date:" LabelAlign="Top" />
                        </Items>
                    </ext:Container>
                    <ext:Container ID="Container4" runat="server" Layout="FormLayout" ColumnWidth=".25"
                        Padding="5">
                        <Items>
                            <ext:TextField ID="txtName" runat="server" EmptyText="Enter Name" />
                            <ext:DateField ID="TxtEndDate" runat="server" FieldLabel="End Date:" LabelAlign="Top" />
                        </Items>
                    </ext:Container>
                    <ext:Container ID="Container5" runat="server" Layout="FormLayout" ColumnWidth=".25"
                        Padding="5">
                        <Items>
                            <ext:Button ID="BtnShow" runat="server" Text="Show">
                                <DirectEvents>
                                    <Click OnEvent="BtnShow_Click" />
                                </DirectEvents>
                            </ext:Button>
                        </Items>
                    </ext:Container>
                </Items>
            </ext:Container>
        </Items>
        <Items>
            <ext:GridPanel runat="server" ID="Grid" Frame="true" Width="1000" Height="295" StoreID="storegrid">
                <ColumnModel runat="server">
                    <Columns>
                        <ext:Column runat="server" Text="EmployeeName" Flex="1" DataIndex="Employee" />
                        <ext:Column runat="server" Text="Branch" Flex="1" DataIndex="Branch" />
                        <ext:Column runat="server" Text="Position" Flex="1" DataIndex="position" />
                        <ext:Column runat="server" Text="Training Name" Flex="1" DataIndex="TrainingName" />
                        <ext:Column runat="server" Text="Submitted On" Flex="1" DataIndex="SubmittedOnEng" />
                        <ext:Column runat="server" Text="Recomended BY" Flex="1" DataIndex="RecommendedBy" />
                        <ext:Column runat="server" Text="Recomended On" Flex="1" DataIndex="RecommendedOn" />

                        <ext:ImageCommandColumn ID="ImageCommandColumn1" runat="server" Width="30" Sortable="false">
                        <Commands>
                            <ext:ImageCommand Icon="Accept" ToolTip-Text="View Details" CommandName="Details"> 
                            <ToolTip Text="Edit"  />
                                                       
                            </ext:ImageCommand>
                        </Commands>
                        <Listeners>
                        
                            <Command Handler="CommandHandler(command,record);" />
                        </Listeners>
                    </ext:ImageCommandColumn>

                    </Columns>
                </ColumnModel>
            </ext:GridPanel>
        </Items>
    </ext:Panel>
    </form>
    </body>
</html>
