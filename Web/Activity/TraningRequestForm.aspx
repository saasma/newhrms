﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TraningRequestForm.aspx.cs" Inherits="Web.Activity.TraningRequestForm" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Circulation For Traning</title>
    <link href="/resources/css/examples.css" rel="stylesheet" />
</head>
<body>
    
        <ext:ResourceManager ID="ResourceManager1" runat="server" />
        
        <ext:Store ID="storeEmployee" runat="server">
        <Model>
            <ext:Model ID="modelEployee" runat="server">
                <Fields>
                    <ext:ModelField Name="Name" Type="String" />
                    <ext:ModelField Name="EmployeeId" Type = "Int" />
                </Fields>
            </ext:Model>
        </Model>
        </ext:Store>

        <form runat="server" id="form1">
 
         <ext:FormPanel 
            ID="Panel1" 
            runat="server" 
            Title="Traning Request Form"
            BodyPaddingSummary="5 5 0"
            Width="1000"
            Frame="true"
            ButtonAlign="Center"
            Layout="FormLayout">
            <FieldDefaults MsgTarget="Side" LabelWidth="75" />
            <Plugins>
                <ext:DataTip ID="DataTip1" runat="server" />
            </Plugins>
            <Items>
                <ext:Container ID="Container1" runat="server" Layout="Column" >
                    <Items>
                        <ext:Container ID="Container2" runat="server" Layout="FormLayout" ColumnWidth="1" Padding="5">
                            <Items>
                                  <ext:TextField ID="txtTraningName" runat="server" Width="400" FieldLabel="Traning Name" LabelAlign="Top" />
                                  <ext:TextArea ID="txtDetail" runat="server" TextMode="multiline" FieldLabel="Provide Some Detail" LabelAlign="Top" />
                                 <ext:TextArea ID="txtwhyImportant" runat="server" TextMode="multiline" FieldLabel="Why do You Think This Traning is Important for you?" LabelAlign="Top" />
                                 <ext:Label ID="Label1" runat="server" Text="Ask For Reccomendation:" />
                                 <ext:SelectBox ID="ddlEmployee" runat="server" Width="230" DisplayField="Name" ValueField="EmployeeId" EmptyText="Must Have Worked For." StoreID = "storeEmployee">  </ext:SelectBox>
                            </Items>
                    </ext:Container>
                 </Items>
            </ext:Container>
      
                <ext:Button ID="BtnSaveAndSend" runat="server" Text="Save and Send" >
                    <DirectEvents>
                            <Click OnEvent="BtnSaveAndSend_Click" />
                    </DirectEvents>
                </ext:Button>

                <ext:Button ID="BtnCancel" runat="server" Text="Cancel" >
                   <%-- <DirectEvents>
                        <Click OnEvent = "BtnCancel_Click" />
                    </DirectEvents>--%>
                </ext:Button>
               </Items>
       

        </ext:FormPanel>
    </form>
</body>

</html>
