﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using DAL;
using BLL.Manager;

namespace Web.Activity
{
    public partial class SubmissionReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Store store = this.ddlGroupBy.GetStore();
            store.DataSource = new object[]
                {
                    new object[]  {"None", "0"},
                    new object[] {"Branch", "1"},
                    new object[] {"Position", "1"}
                };

           // Grid.Visible = false;
            this.storeBranches.DataSource = BindData();
            this.storeBranches.DataBind();
           
        }

        protected void BtnShow_Click(object sender, DirectEventArgs e)
        {
            //DateTime dt = Convert.ToDateTime(TxtEndDate.Text);
            this.storegrid.DataSource = BindGrid();
            this.storegrid.DataBind();
            //GetSubmissionReportResult entity = TrainingManager.GetSubmissionReport(Convert.ToInt32(ddlBranch.SelectedItem.Value), txtName.Text, Convert.ToDateTime(DateField1.Text), Convert.ToDateTime(DateField2.Text));

        }

        private List<GetSubmissionReportResult> BindGrid()
        {
            
            PayrollDataContext db = new PayrollDataContext();
            string datetime = TxtEndDate.Text;
            
            DateTime dt = DateTime.Parse(TxtEndDate.Text);
            List<GetSubmissionReportResult> listReport = db.GetSubmissionReport(Convert.ToInt32(ddlBranch.SelectedItem.Value), txtName.Text, Convert.ToDateTime(TxtStartDate.Text), Convert.ToDateTime(TxtEndDate.Text), Convert.ToInt32(ddlPosition.SelectedItem.Value)).ToList();
            if (ddlGroupBy.SelectedItem.Value == "1")
            {
                return listReport;
            }
            else if (ddlGroupBy.SelectedItem.Value == "2")
            {
                return listReport;
            }
            else
            {
                return listReport;
            }
            
            //TrainingManager.GetSubmissionReport(Convert.ToInt32(ddlBranch.SelectedItem.Value), txtName.Text, Convert.ToDateTime(DateField1.Text), Convert.ToDateTime(DateField2.Text));
        }


        private List<Branch> BindData()
        {
            PayrollDataContext db = new PayrollDataContext();

            List<Branch> lstBranches = db.Branches.OrderBy(x => x.BranchId).ToList();

            return lstBranches;

        }

        protected void GridCell_Click(Object sender, CommandColumn e)
        {
 
        }
    }
}