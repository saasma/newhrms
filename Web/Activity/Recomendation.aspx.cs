﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Manager;
using BLL;
using DAL;
using Ext.Net;

namespace Web.Activity
{
    public partial class Recomendation : System.Web.UI.Page
    {
        int Id;
        protected void Page_Load(object sender, EventArgs e)
        {
            int EmployeeId = SessionManager.CurrentLoggedInEmployeeId;

            GetcallForRecommendResult entity = TrainingManager.GetCallForRecommend(13);
            Id = entity.ID;
            LblTraningName.Text = entity.TrainingName.ToString();
            LblDetails.Text = entity.Details.ToString();
            LblJustification.Text = entity.Importance.ToString();

        }

        protected void BtnReccomendation_Click(object sender, DirectEventArgs e)
        {
            
            TrainingManager.Update(Id, txtYourComment.Text,true,DateTime.Now);
        }

        protected void BtnDonotRecommend_Click(Object sender, DirectEventArgs e)
        {
            TrainingManager.Update(Id, txtYourComment.Text, false, DateTime.Now);
        }
    }
}