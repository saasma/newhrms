﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TraningCircular.aspx.cs" Inherits="Web.Activity.TraningCircular" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Training Circular</title>
</head>

<%--<ext:Store ID="storeEmp" runat="server">
        <Model>
            <ext:Model ID="modelEmp" runat="server">
                <Fields>
                    <ext:ModelField Name="FromName" Type="String" />
                    <ext:ModelField Name="DueDateEng" Type = "String" />
                    <ext:ModelField Name="Body" Type = "String" />
                </Fields>
            </ext:Model>
        </Model>
    </ext:Store>--%>

<body>
    <form id="form1" runat="server">
    <ext:ResourceManager ID="ResourceManager1" runat="server" />
   <ext:Panel 
            ID="Panel3"
            runat="server" 
            Title="Traning Need Assesment Circulation Template" 
            Frame="true"
            PaddingSummary="5px 5px 0"
            Width="1000"
            ButtonAlign="Center">
   <Items>
    <ext:Container ID="Container4" runat="server" Layout="FormLayout">
        <Items>
            
            <ext:Label ID="Label2" runat="server" Text = "Message From"  />
            <ext:Label ID="lblMessageFrom" runat="server" FieldLabel="Message From" LabelAlign="Left" />
            
            <ext:Label ID="Label3" runat="server" Text = "Due Date"  />
            <ext:Label ID="LblDueDate" runat="server"  />

            <ext:HtmlEditor ID="HtmlEditor1" runat="server" Height="200"  LabelAlign="Top">
                        
            </ext:HtmlEditor>
                
        </Items>
    </ext:Container>
    </Items>

    <Buttons>
        <ext:Button ID = "BtnRequest" runat ="server" Text = "Click Here For Request">
            <DirectEvents>
                <Click OnEvent = "BtnRequest_Click" />
            </DirectEvents>
        </ext:Button>
    </Buttons>
    </ext:Panel>
            
    </form>
</body>
</html>
