﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TraningNeedCirculation.aspx.cs"
    Inherits="Web.Activity.Try" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head id="Head1" runat="server">
    <title>Circulation For Traning</title>
    <link href="/resources/css/examples.css" rel="stylesheet" />
</head>
<body>
    <ext:ResourceManager ID="ResourceManager1" runat="server" />
    <form runat="server" id="form1">
    <ext:Store ID="storeBranches" runat="server">
        <Model>
            <ext:Model ID="modelBranches" runat="server">
                <Fields>
                    <ext:ModelField Name="Name" Type="String" />
                    <ext:ModelField Name="BranchId" Type = "Int" />
                </Fields>
            </ext:Model>
        </Model>
    </ext:Store>

    <ext:Store ID="StorePosition" runat="server">
        <Model>
            <ext:Model ID="modelPosition" runat="server">
                <Fields>
                    <ext:ModelField Name="Name" Type="String" />
                    <ext:ModelField Name="DesignationId" Type = "Int" />
                </Fields>
            </ext:Model>
        </Model>
    </ext:Store>

    <ext:Store ID="StoreLevel" runat="server">
        <Model>
            <ext:Model ID="modelLevel" runat="server">
                <Fields>
                    <ext:ModelField Name="Name" Type="String" />
                    <ext:ModelField Name="LevelId" Type = "Int" />
                </Fields>
            </ext:Model>
        </Model>
    </ext:Store>

    <ext:Store ID="storeDepartment" runat="server">
        <Model>
            <ext:Model ID="modeldepartment" runat="server">
                <Fields>
                    <ext:ModelField Name="Name" Type="String" />
                    <ext:ModelField Name="DepartmentId" Type = "int" />
                </Fields>
            </ext:Model>
        </Model>
    </ext:Store>
    <ext:Store ID="storegrid" runat="server">
            <Model>
                <ext:Model ID="modelgrid" runat="server">
                    <Fields>
                        <ext:ModelField Name="Name" Type="String" />
                        <ext:ModelField Name="Department" Type="String" />
                        <ext:ModelField Name="Branch" Type="String" />
                        <ext:ModelField Name="EmployeeId" Type="String" />
                       

                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
    
    <Buttons>
            <ext:Button ID="BtnEditTemplate" runat="server" Text=" Edit Circulation Template" >
             <DirectEvents>
                    <Click OnEvent="BtnEditTemplate_Click" />
            </DirectEvents>
            </ext:Button>
     </Buttons>

    <ext:Panel ID="Panel3" runat="server" Frame="false" PaddingSummary="5px 5px 0" Width="1000" 
        ButtonAlign="Center">
        <Items>
            <ext:Container ID="Container1" runat="server" Layout="Column" Height="100">
                <Items>
                    <ext:Container ID="Container2" runat="server" Layout="FormLayout" ColumnWidth=".25"
                        Padding="5">
                        <Items>
                            <ext:SelectBox ID="ddlBranch" runat="server" DisplayField="Name" ValueField="BranchId"
                                EmptyText="Select Branch..." StoreID="storeBranches">
                            </ext:SelectBox>
                            <ext:Label ID="Label1" runat="server" Text="Send Circular To:" />
                            <ext:SelectBox ID="ddlWorkedFor" runat="server" DisplayField="Name" ValueField="ID"
                                EmptyText="Must Have Worked For.">
                                 <Store>
                                    <ext:Store ID="Store1" runat="server">
                                        <Model>
                                            <ext:Model ID="Model1" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="Name" />
                                                    <ext:ModelField Name="ID" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>            
                                    </ext:Store>    
                                 </Store>    
                            </ext:SelectBox>
                        </Items>
                    </ext:Container>
                    <ext:Container ID="Container3" runat="server" Layout="FormLayout" ColumnWidth=".25"
                        Padding="5">
                        <Items>
                            <ext:SelectBox ID="ddlDepartment" runat="server" DisplayField="Name" ValueField="DepartmentId"
                                EmptyText="Select Department..." StoreID="storeDepartment">
                            </ext:SelectBox>
                            <ext:Label ID="Label2" runat="server" Text="Months:" />
                            <ext:TextField ID="txtMonth" runat="server" Name="Month" />
                        </Items>
                    </ext:Container>
                    <ext:Container ID="Container4" runat="server" Layout="FormLayout" ColumnWidth=".25"
                        Padding="5">
                        <Items>
                            <ext:SelectBox ID="ddlLevel" runat="server" DisplayField="Name" ValueField="LevelId" StoreID ="storeLevel"
                                EmptyText="Select Level...">
                            </ext:SelectBox>
                            <ext:Label ID="Label3" runat="server" Text=" " />
                            <ext:DateField ID="TxtDate" runat="server" FieldLabel = "DueDate" LabelAlign="Top"/>
                            
                        </Items>
                    </ext:Container>
                    <ext:Container ID="Container5" runat="server" Layout="FormLayout" ColumnWidth=".25"
                        Padding="5">
                        <Items>
                            
                            <ext:SelectBox ID="ddlPosition" runat="server" DisplayField="Name" ValueField="DesignationId" StoreID ="StorePosition"
                                EmptyText="Select Position...">
                            </ext:SelectBox>
                            <ext:Button ID="BtnShow" runat="server" Text="Show">
                                <DirectEvents>
                                    <Click OnEvent="BtnShow_Click" />
                                </DirectEvents>
                            </ext:Button>
                        </Items>
                    </ext:Container>

                   
                </Items>
            </ext:Container>
        </Items>
        <Items>
        <ext:GridPanel 
            runat="server"
            ID ="Grid"
            Frame="true"
            Width="1000"
            Height="295"
            storeId = "storegrid">
     
            <ColumnModel ID="ColumnModel1" runat="server">
                <Columns>
                    <ext:Column ID="Column1" runat="server" Text="EEd" Flex="1" DataIndex="EmployeeId" />
                    <ext:Column ID="Column2" runat="server" Text="Employee Name" Flex="1" DataIndex="Name" />
                    <ext:Column ID="Column3" runat="server" Text="Branch" DataIndex="Branch" />
                    <ext:Column ID="Column4" runat="server" Text="Department" Flex="1" DataIndex="Department" />
                    <ext:Column ID="Column5" runat="server" Text="Position" Flex="1" DataIndex="Position" />

                
                    <ext:ImageCommandColumn ID="ImageCommandColumn1" runat="server" Width="30" Sortable="false">
                        <Commands>
                            <ext:ImageCommand Icon="Decline" ToolTip-Text="Delete Plant" CommandName="delete">                            
                            </ext:ImageCommand>
                        </Commands>
                        <Listeners>
                            <Command Handler="this.up('gridpanel').store.removeAt(recordIndex);" />
                        </Listeners>
                    </ext:ImageCommandColumn>

                          
                  
                </Columns>
            </ColumnModel>

             <SelectionModel>
                <ext:CellSelectionModel ID="CellSelectionModel1" runat="server" />
            </SelectionModel>
            <Plugins>
                <ext:CellEditing ID="CellEditing1" runat="server" ClicksToEdit="1" />
            </Plugins>
        </ext:GridPanel>
    </Items>

     <Buttons>
            <ext:Button ID="BtnSaveEmployeeCircular" runat="server" Text="Send Circulation">
            <%--<DirectEvents>
                <Click OnEvent = BtnSaveEmployeeCircular_CLick />
            </DirectEvents>--%>
            <DirectEvents>
                <Click OnEvent="BtnSaveEmployeeCircular_CLick">
                    <Confirmation ConfirmRequest="true" Message="Are you sure, you want to save the changes?" />
                    <ExtraParams>
                        <ext:Parameter Name="gridItems" Value="Ext.encode(#{Grid}.getRowsValues({ selectedOnly: false }))"
                            Mode="Raw" />
                    </ExtraParams>
                    <%--server side event with mask/loading showing--%>
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
            </ext:Button>
         </Buttons>

    </ext:Panel>
             
    </form>
</html>
