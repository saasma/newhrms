﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HRViewCtl.ascx.cs" Inherits="Web.Activity.UserControls.HRViewCtl" %>
<style type="text/css">
    .tableWrapper
    {
        margin-bottom: 10px;
        width: 1136px;
        border-radius: 3px;
        border: 1px solid #BFD9FB;
        background: #E8F1FF;
        padding: 10px;
    }
    .tableWrapper table
    {
        margin: 4px 20px 0 0;
    }
    .gridWrapper
    {
        border: 1px solid silver;
        border-radius: 2px;
        width: 1156px;
    }
    p
    {
        margin: 10px 0;
    }
    .windowContentWrapper td
    {
        vertical-align: middle;
        margin-right: 0;
        padding: 10px 10px 10px 0;
    }
    .windowContentWrapper table
    {
    }
    .x-window-body-default
    {
        background: #E8F1FF;
    }
    .windowContentWrapper
    {
        margin: 0;
        padding: 5px;
    }
    .bolds
    {
        padding: 0 100px 0 0;
    }
    .bolds .x-label-value
    {
        color: Gray;
        font-size: large;
        font-weight: bold;
    }
    .boldLabels
    {
        font-weight: bold;
        font-size: 15px;
    }
    .darkOrange
    {
        color: #ff8c00;
        margin-left: 5px;
    }
    .load
    {
        padding: 9px 45px;
    }
</style>
<script type="text/javascript">
        var CommandHandler = function (command, record) {
            <%= hiddenValue.ClientID %>.setValue(record.data.ActivityID);
            if (command == "view") {
                // view specific activity
                <%= btnViewActivity.ClientID %>.fireEvent('click');
            }
            else {
                // verify activity
                <%= btnVerifyActivity.ClientID %>.fireEvent('click');
            }
        };
</script>
<%--<ext:ResourceManager ID="ResourceManager1" runat="server" />--%>
<ext:Hidden ID="hiddenValue" runat="server" />
<ext:LinkButton ID="btnViewActivity" runat="server" Hidden="true">
    <DirectEvents>
        <Click OnEvent="btnViewActivity_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:LinkButton>
<ext:LinkButton ID="btnVerifyActivity" runat="server" Hidden="true">
    <DirectEvents>
        <Click OnEvent="btnVerifyActivity_Click">
            <Confirmation ConfirmRequest="true" Message="Are you sure you want to verify this activity?" />
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:LinkButton>
<ext:Store ID="storeActivity" runat="server" PageSize="50" OnReadData="BindActivityData">
    <Model>
        <ext:Model ID="modelActivity" runat="server" IDProperty="ActivityID">
            <Fields>
                <ext:ModelField Name="ActivityDateEng" Type="Date" />
                <ext:ModelField Name="SubmissionDate" Type="Date" />
                <ext:ModelField Name="AttachmentName" Type="String" />
                <ext:ModelField Name="ActivityID" Type="Int" />
                <ext:ModelField Name="Level" Type="String" />
                <ext:ModelField Name="Name" Type="String" />
            </Fields>
        </ext:Model>
    </Model>
</ext:Store>
<ext:Store ID="storeBranch" runat="server">
    <Model>
        <ext:Model ID="modelBranch" runat="server">
            <Fields>
                <ext:ModelField Name="BranchId" Type="Int" />
                <ext:ModelField Name="Name" Type="String" />
            </Fields>
        </ext:Model>
    </Model>
</ext:Store>
<ext:Store ID="storeLevel" runat="server">
    <Model>
        <ext:Model ID="modelLevel" runat="server">
            <Fields>
                <ext:ModelField Name="LevelId" Type="Int" />
                <ext:ModelField Name="Name" Type="String" />
            </Fields>
        </ext:Model>
    </Model>
</ext:Store>



<div id="bodyPart">
    <div class="contentArea">
        <div class="tableWrapper">
            <table>
                <tr>
                    <td colspan="3">
                        Narrow your list
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:ComboBox ID="cmbBranch" runat="server" FieldLabel="" Width="150px" DisplayField="Name"
                            ValueField="BranchId" StoreID="storeBranch" Editable="false" EmptyText="" QueryMode="Local">
                        </ext:ComboBox>
                    </td>
                    <td>
                        <ext:ComboBox ID="cmbLevel" runat="server" FieldLabel="" Width="150px" DisplayField="Name"
                            ValueField="LevelId" StoreID="storeLevel" Editable="false" EmptyText="" QueryMode="Local">
                        </ext:ComboBox>
                    </td>
                    <td>
                        <ext:TextField ID="txtName" runat="server" FieldLabel="" EmptyText="Type a Name"
                            Width="150px">
                        </ext:TextField>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:ComboBox ID="cmbGroupBy" runat="server" FieldLabel="Sort by" LabelAlign="Top"
                            Width="150px" AllowBlank="false">
                            <Items>
                                <ext:ListItem Text="Date" Index="0" />
                                <ext:ListItem Text="Employee Name" Index="1" />
                            </Items>
                            <SelectedItems>
                                <ext:ListItem Index="0" />
                            </SelectedItems>
                        </ext:ComboBox>
                    </td>
                    <td>
                        <ext:DateField ID="dateStart" runat="server" FieldLabel="Start Date" LabelAlign="Top"
                            Width="150px" Vtype="daterange" EndDateField="dateEnd">
                        </ext:DateField>
                    </td>
                    <td>
                        <ext:DateField ID="dateEnd" runat="server" FieldLabel="End Date" LabelAlign="Top"
                            Width="150px" Vtype="daterange" StartDateField="dateStart">
                        </ext:DateField>
                    </td>
                    <td style="vertical-align: bottom; padding-bottom: 9px; width:100px;">
                        <ext:Button ID="btnSearch" runat="server" Text="Search"  StyleSpec="margin-top:10px" Height="30">
                            <DirectEvents>
                                <Click OnEvent="btnSearch_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                    </td>
                    <td style="vertical-align: bottom; padding-bottom: 9px;">
                        <ext:Button ID="btnVerifyList" runat="server" Text="Verify" StyleSpec="margin-top:10px" Height="30">
                            <DirectEvents>
                                <Click OnEvent="btnVerifyList_Click">
                                    <Confirmation ConfirmRequest="true" Message="Confirm verify the activities?" />
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                    </td>
                </tr>
            </table>
        </div>
        <div>
            <p>
                Daily Activity Reports Awaiting Verification</p>
            <div class="gridWrapper">
                <ext:GridPanel ID="gridActivities" runat="server" Width="1156px" StoreID="storeActivity"
                    Header="false" Resizable="false" MinHeight="600" AutoScroll="true">
                    <ColumnModel>
                        <Columns>
                            <ext:DateColumn ID="colActivityDate" runat="server" Flex="1" Align="Left" Text="Activity Date"
                                MenuDisabled="true" Sortable="false" Format="ddd - dd MMM" DataIndex="ActivityDateEng"
                                Resizable="false" Draggable="false">
                            </ext:DateColumn>
                            <ext:Column ID="colEmpName" runat="server" Flex="1" Align="Left" Text="Employee Name"
                                MenuDisabled="true" Sortable="false" Draggable="false" DataIndex="Name" Resizable="false">
                            </ext:Column>
                            <ext:Column ID="colLevel" runat="server" Flex="1" Align="Left" Text="Level" MenuDisabled="true"
                                Sortable="false" Draggable="false" DataIndex="Level" Resizable="false">
                            </ext:Column>
                            <ext:DateColumn ID="colSubmissionDate" Align="Left" runat="server" DataIndex="SubmissionDate"
                                Format="ddd - dd MMM H:i:s" MenuDisabled="true" Sortable="false" Flex="1" Resizable="false"
                                Draggable="false">
                            </ext:DateColumn>
                            <ext:Column ID="colAttachment" Align="Left" runat="server" Text="Attachment" MenuDisabled="true"
                                Sortable="false" Flex="1" DataIndex="AttachmentName" Resizable="false" Draggable="false">
                            </ext:Column>
                            <ext:CommandColumn ID="colCommands" Align="Left" Flex="1" runat="server" Resizable="false"
                                Draggable="false" MenuDisabled="true" Sortable="false">
                                <Commands>
                                    <ext:GridCommand Text="View" Icon="PageGo" CommandName="view">
                                    </ext:GridCommand>
                                    <%--   <ext:GridCommand Text="Verify" Icon="Tick" CommandName="verify"></ext:GridCommand>--%>
                                </Commands>
                                <Listeners>
                                    <Command Handler="CommandHandler(command,record);" />
                                </Listeners>
                            </ext:CommandColumn>
                        </Columns>
                    </ColumnModel>
                    <SelectionModel>
                        <ext:CheckboxSelectionModel ID="CheckboxSelectionModel1" runat="server" Mode="Multi" />
                    </SelectionModel>
                    <BottomBar>
                        <ext:PagingToolbar ID="paging1" runat="server">
                        </ext:PagingToolbar>
                    </BottomBar>
                </ext:GridPanel>
            </div>
        </div>
        <div>
            <ext:Window ID="winActivity" MinWidth="940" MinHeight="200" Title="Daily Activity Report"
                Closable="true" Resizable="false" Hidden="true" runat="server" AutoScroll="true">
                <Content>
                    <div class="windowContentWrapper">
                        <table style="margin-left: 10px;">
                            <tr>
                                <td colspan="2">
                                    <span class="bolds">
                                        <ext:Label ID="lblEmpName" runat="server" Text="">
                                        </ext:Label>
                                    </span><span class="bolds">
                                        <ext:Label ID="lblEmpPosition" runat="server" Text="">
                                        </ext:Label>
                                    </span><span class="bolds">
                                        <ext:Label ID="lblEmpBranch" runat="server" Text="">
                                        </ext:Label>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="boldLabels">Activity Date:</span>
                                    <ext:Label ID="lblActivityDate" runat="server" Text="Date Here" />
                                </td>
                                <td align="left">
                                    <span class="boldLabels">Submission Date:</span>
                                    <ext:Label ID="lblSubmissionDate" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <span class="boldLabels" style="color: #6495ED;">Key Learnings</span>
                                    <hr />
                                    <div style="margin-left: 15px;">
                                        <ext:Label ID="lblLearnings" runat="server" Text="[Learnings]">
                                        </ext:Label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <span class="boldLabels" style="color: #6495ED;">Key Achievements</span>
                                    <hr />
                                    <div style="margin-left: 15px;">
                                        <ext:Label ID="lblAchievements" runat="server" Text="[Achievements]">
                                        </ext:Label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <span class="boldLabels" style="color: #6495ED;">Details</span>
                                    <hr />
                                    <div style="background-color: #ffffff; padding: 10px; border: 1px solid silver; max-height: 150px;
                                        overflow-x: scroll;">
                                        <ext:TextArea ID="txtDetails" runat="server" Width="860px" MinHeight="0" AutoScroll="true">
                                        </ext:TextArea>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div style="display: flex;">
                                        <span class="boldLabels">Attachment:</span>
                                        <ext:LinkButton ID="btnAttachment" runat="server" Cls="darkOrange">
                                            <DirectEvents>
                                                <Click OnEvent="btnAttachment_Click" IsUpload="true" Success="Ext.net.DirectMethods.Download({ isUpload : true });">
                                                    <EventMask ShowMask="false" />
                                                </Click>
                                            </DirectEvents>
                                        </ext:LinkButton>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="buttonBlock buttonLeft">
                                        <ext:Button ID="btnVerify" runat="server" Text="Verify" Cls="btn btn-primary" StyleSpec="margin-top:10px" Height="30">
                                            <DirectEvents>
                                                <Click OnEvent="btnVerifyActivity_Click">
                                                    <Confirmation ConfirmRequest="true" Message="Are you sure you want to verify this activity?" />
                                                    <EventMask ShowMask="true" />
                                                </Click>
                                            </DirectEvents>
                                        </ext:Button>
                                        <div class="btnFlatOr">
                                            or
                                        </div>
                                        <ext:LinkButton ID="lnkBtnCancel" runat="server" Cls="btnFlatLeftGap" BaseCls="btnFlatLeftGap"
                                            Text="Cancel">
                                            <Listeners>
                                                <Click Handler="#{winActivity}.hide();" />
                                            </Listeners>
                                        </ext:LinkButton>
                                    </div>
                                </td>
                                <td>
                                    <%--<ext:Button ID="btnCancel" runat="server" Text="Cancel" Cls="btnFlat" BaseCls="btnFlat">
                                    <Listeners>
                                        <Click Handler="#{winActivity}.hide();" />
                                    </Listeners>
                                </ext:Button>--%>
                                </td>
                            </tr>
                        </table>
                    </div>
                </Content>
            </ext:Window>
        </div>
    </div>
    <!-- end of contentArea class -->
</div>
