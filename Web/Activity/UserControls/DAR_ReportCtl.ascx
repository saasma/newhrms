﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DAR_ReportCtl.ascx.cs"
    Inherits="Web.Activity.UserControls.DAR_ReportCtl" %>
<style type="text/css">
  
    
 
</style>
<script type="text/javascript">
        Ext.define('MyOverrides', {
            override: "Ext.ux.grid.column.ProgressBar",

            renderer: function (value, meta) {
                var me = this,
            text,
            cls = me.progressCls,
            pCls,
            cWidth = me.getWidth(true) - 2;

                if (me.hideIfEmpty && (!value && value !== 0 || value < 0)) {
                    return "";
                }

                value = value || 0;

                text = Ext.String.format(me.progressText, Math.round(value)); //you should change this line

                pCls = cls + ' ' + cls + '-' + me.ui;
                meta.tdCls = "x-progress-column-cell";
                meta.style = "padding:0px;margin:0px;";
                v = '<div class="' + pCls + '" style="margin:1px 1px 1px 1px;width:' + cWidth + 'px;"><div class="' + cls + '-text ' + cls + '-text-back" style="width:' + (cWidth - 2) + 'px;">' +
                text +
            '</div>' +
            '<div class="' + cls + '-bar ' + cls + '-bar-' + me.ui + '" style="width: ' + value + '%;">' +
                '<div class="' + cls + '-text" style="width:' + (cWidth - 2) + 'px;">' +
                    '<div>' + text + '</div>' +
                '</div>' +
            '</div></div>'
                return v;
            }
        });


      
        var CommandHandler = function (command, record) {
            <%= hiddenValue.ClientID %>.setValue(record.data.EmployeeId);

            // view specific activity
            <%= btnViewActivity.ClientID %>.fireEvent('click');
        };
</script>
<%--<ext:ResourceManager ID="ResourceManager1" runat="server" />--%>
<ext:Hidden ID="hiddenValue" runat="server" />
<ext:LinkButton ID="btnViewActivity" runat="server" Hidden="true">
    <DirectEvents>
        <Click OnEvent="btnViewActivity_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:LinkButton>
<ext:Store ID="storeDailyReport" runat="server" PageSize="50" OnReadData="RefreshGrid">
    <Model>
        <ext:Model ID="modelDailyReport" runat="server">
            <Fields>
                <ext:ModelField Name="EmployeeId" Type="Int" />
                <ext:ModelField Name="Name" Type="String" />
                <ext:ModelField Name="DesignationId" Type="Int" />
                <ext:ModelField Name="BranchId" Type="Int" />
                <ext:ModelField Name="DepartmentId" Type="Int" />
                <ext:ModelField Name="BranchName" Type="String" />
                <ext:ModelField Name="DepartmentName" Type="String" />
                <ext:ModelField Name="PositionName" Type="String" />
                <ext:ModelField Name="WorkDays" Type="Int" />
                <ext:ModelField Name="Submitted" Type="Int" />
                <ext:ModelField Name="NotSubmitted" Type="Int" />
                <ext:ModelField Name="SubmitPercentage" Type="Int" />
                <ext:ModelField Name="LevelName" Type="String" />
            </Fields>
        </ext:Model>
    </Model>
</ext:Store>
<ext:Store ID="storeBranch" runat="server">
    <Model>
        <ext:Model ID="modelBranch" runat="server">
            <Fields>
                <ext:ModelField Name="BranchId" Type="Int" />
                <ext:ModelField Name="Name" Type="String" />
            </Fields>
        </ext:Model>
    </Model>
</ext:Store>
<ext:Store ID="storeLevel" runat="server">
    <Model>
        <ext:Model ID="modelLevel" runat="server">
            <Fields>
                <ext:ModelField Name="LevelId" Type="Int" />
                <ext:ModelField Name="Name" Type="String" />
            </Fields>
        </ext:Model>
    </Model>
</ext:Store>
<div id="bodyPart">
    <div class="contentArea">
        <div class="alert alert-info">
            <table class="fieldTable">
                <tr>
                    <td colspan="4">
                        Narrow your list
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:ComboBox ID="cmbBranch" runat="server" Width="150px" FieldLabel="" DisplayField="Name"
                            ValueField="BranchId" QueryMode="Local" StoreID="storeBranch" Editable="false"
                            EmptyText="">
                        </ext:ComboBox>
                    </td>
                    <td>
                        <ext:ComboBox ID="cmbLevel" runat="server" Width="150px" FieldLabel="" DisplayField="Name"
                            ValueField="LevelId" StoreID="storeLevel" Editable="false" QueryMode="Local"
                            EmptyText="">
                        </ext:ComboBox>
                    </td>
                    <td>
                        <ext:TextField ID="txtName" EmptyText="Type a Name" Width="150px" runat="server">
                        </ext:TextField>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:ComboBox ID="cmbGroupBy" runat="server" Width="150px" FieldLabel="Sort by" LabelSeparator=""
                            LabelAlign="Top" ForceSelection="true">
                            <Items>
                                <ext:ListItem Index="0" Text="None" />
                                <ext:ListItem Index="1" Text="Position" />
                                <ext:ListItem Index="2" Text="Level" />
                                <ext:ListItem Index="3" Text="Branch" />
                            </Items>
                            <SelectedItems>
                                <ext:ListItem Index="0" />
                            </SelectedItems>
                        </ext:ComboBox>
                    </td>
                    <td>
                        <ext:DateField ID="dateStart" runat="server" Width="150px" FieldLabel="Start Date"
                            LabelAlign="Top" LabelSeparator="">
                        </ext:DateField>
                    </td>
                    <td>
                        <ext:DateField ID="dateEnd" runat="server" Width="150px" FieldLabel="End Date" LabelAlign="Top"
                            LabelSeparator="">
                        </ext:DateField>
                    </td>
                    <td style="vertical-align: bottom; padding-bottom: 0px;">
                        <ext:Button ID="btnSearch" Text="Search" runat="server">
                            <DirectEvents>
                                <Click OnEvent="btnSearch_Click" Timeout="99999999">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                    </td>
                    <td style="vertical-align: bottom; padding-bottom: 0px; padding-left: 75px;">
                        <ext:Button runat="server" AutoPostBack="true" OnClick="btnExport_Click" 
                            ID="btnAdd"  Text="<i></i>Export">
                        </ext:Button>
                    </td>
                </tr>
            </table>
        </div>
        <div>
            <ext:GridPanel ID="gridDailyReport" runat="server" Header="false" Resizable="false"
                StoreID="storeDailyReport" ColumnLines="true" MinHeight="600" AutoScroll="true">
                <ColumnModel>
                    <Columns>
                        <ext:Column ID="colEmpID" runat="server" Text="EID" Width="50px" Sortable="false"
                            MenuDisabled="true" Resizable="false" Align="Left" DataIndex="EmployeeId" Draggable="false">
                        </ext:Column>
                        <ext:Column ID="colEmpName" runat="server" Text="Name" Flex="1" Sortable="false"
                            MenuDisabled="true" Resizable="false" Align="Left" DataIndex="Name" Draggable="false">
                        </ext:Column>
                        <ext:Column ID="colPosition" runat="server" Text="Position" Flex="1" Sortable="false"
                            MenuDisabled="true" Resizable="false" Align="Left" DataIndex="PositionName" Draggable="false">
                        </ext:Column>
                        <ext:Column ID="colBranch" runat="server" Text="Branch" Flex="1" Sortable="false"
                            MenuDisabled="true" Resizable="false" Align="Left" DataIndex="BranchName" Draggable="false">
                        </ext:Column>
                        <ext:Column ID="colDepartment" runat="server" Text="Department" Flex="1" Sortable="false"
                            MenuDisabled="true" Resizable="false" Align="Left" DataIndex="DepartmentName"
                            Draggable="false">
                        </ext:Column>
                        <ext:Column ID="colLevel" runat="server" Text="Level" Flex="1" Sortable="false" MenuDisabled="true"
                            Resizable="false" Align="Left" DataIndex="LevelName" Draggable="false">
                        </ext:Column>
                        <ext:Column ID="colWorkDays" runat="server" Text="Work Days" Width="80" Sortable="false"
                            MenuDisabled="true" Resizable="false" Align="Left" Draggable="false" DataIndex="WorkDays">
                        </ext:Column>
                        <ext:Column ID="colSubmitted" runat="server" Text="Submitted" Width="80" Sortable="false"
                            MenuDisabled="true" Resizable="false" Align="Left" Draggable="false" DataIndex="Submitted">
                        </ext:Column>
                        <ext:Column ID="colNotSubmitted" runat="server" Text="Not Submitted" Width="100"
                            Sortable="false" MenuDisabled="true" Resizable="false" Align="Left" Draggable="false"
                            DataIndex="NotSubmitted">
                        </ext:Column>
                        <ext:ProgressBarColumn ID="colTestProgress" runat="server" Width="50" Align="Left"
                            DataIndex="SubmitPercentage" Text="%" Resizable="false" Draggable="false" Sortable="false">
                        </ext:ProgressBarColumn>
                        <ext:CommandColumn ID="colCommands" Text="Not Submitted" Align="Left" Width="75px"
                            runat="server" Resizable="false" Draggable="false" MenuDisabled="true" Sortable="false">
                            <Commands>
                                <ext:GridCommand Text="View" Icon="PageGo" CommandName="view" MinWidth="50">
                                </ext:GridCommand>
                            </Commands>
                            <Listeners>
                                <Command Handler="CommandHandler(command,record);" />
                            </Listeners>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="paging1" runat="server">
                    </ext:PagingToolbar>
                </BottomBar>
            </ext:GridPanel>
        </div>
    </div>
    <ext:Window ID="Window1" runat="server" AutoScroll="true" Title="Not Submitted List"
        Icon="Application" Width="740" Height="600" BodyPadding="5" Hidden="true" Modal="true">
        <Content>
            <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridHistory" runat="server" Cls="itemgrid"
                Width="640" ClicksToEdit="1">
                <Store>
                    <ext:Store ID="storeHistory" runat="server" AutoLoad="true">
                        <Model>
                            <ext:Model ID="Model2" runat="server">
                                <Fields>
                                    <ext:ModelField Name="SN" Type="String" />
                                    <ext:ModelField Name="DateEng" Type="Date" />
                                    <ext:ModelField Name="Draft" Type="string" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:Column ID="Column1" Align="Left" runat="server" Text="SN" MenuDisabled="true"
                            Sortable="false" DataIndex="SN" Width="50" Resizable="false" Draggable="false">
                        </ext:Column>
                        <ext:DateColumn ID="colSubmissionDate" Align="Left" runat="server" Text="Date" MenuDisabled="true"
                            Sortable="false" DataIndex="DateEng" Format="dd MMM - ddd" Flex="1" Resizable="false"
                            Draggable="false">
                        </ext:DateColumn>
                        <ext:Column ID="DateColumn1" Align="Left" runat="server" Text="Draft" MenuDisabled="true"
                            Sortable="false" DataIndex="Draft" Flex="1" Resizable="false" Draggable="false">
                        </ext:Column>
                    </Columns>
                </ColumnModel>
                <SelectionModel>
                    <ext:CellSelectionModel ID="CellSelectionModel2" runat="server" Mode="Single" />
                </SelectionModel>
            </ext:GridPanel>
        </Content>
    </ext:Window>
</div>
