﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using BLL.Manager;
using DAL;
using BLL;
using Bll;

namespace Web.Activity.UserControls
{
    public partial class DAR_ReportCtl : System.Web.UI.UserControl
    {
        public bool IsAdmin
        {
            get;
            set;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialize();
            }
        }

        protected void btnSearch_Click(object s, DirectEventArgs e)
        {
            BindData();
        }





        #region Methods...

        private void Initialize()
        {
            storeBranch.DataSource = ActivityManager.GetAllBranches();
            storeBranch.DataBind();

            this.cmbBranch.Items.Insert(0, new Ext.Net.ListItem("-- Select Branch --", null));
            this.cmbBranch.SelectedItem.Index = 0;

            storeLevel.DataSource = ActivityManager.GetLevels();
            storeLevel.DataBind();

            this.cmbLevel.Items.Insert(0, new Ext.Net.ListItem("-- Select Level --", null));
            this.cmbLevel.SelectedItem.Index = 0;

            DateTime today = DateTime.Today;

            DateTime startOfMonth = new DateTime(today.Year, today.Month, 1);

            this.dateStart.SelectedDate = startOfMonth;
            this.dateEnd.SelectedDate = DateTime.Today;

            BindData();
        }

        private void BindData()
        {
            int? branchId;
            string ename = txtName.Text;
            DateTime startDate;
            DateTime endDate;
            int? levelId;

            int orderBy = cmbGroupBy.SelectedItem.Index;

            if (cmbBranch.SelectedItem.Value != null)
            {
                branchId = int.Parse(cmbBranch.SelectedItem.Value);
            }
            else
            {
                branchId = null;
            }

            if (cmbLevel.SelectedItem.Value == null)
            {
                levelId = null;
            }
            else
            {
                levelId = int.Parse(cmbLevel.SelectedItem.Value);
            }

            if (dateStart.IsEmpty || dateEnd.IsEmpty)
            {
                NewMessage.ShowWarningMessage("Start and End date must be selected.");
                dateStart.Focus();
                return;
            }
            else
            {
                startDate = dateStart.SelectedDate;
                endDate = dateEnd.SelectedDate;

                //if (startDate == endDate && !X.IsAjaxRequest)
                //{
                //    return;
                //}
                //else if (startDate == endDate)
                //{
                //    NewMessage.ShowWarningMessage("Start and end date can not be same.");
                //    return;
                //}
            }


            this.storeDailyReport.DataSource = ActivityManager.GetEmployeesDailyReport(ename, branchId, levelId, startDate, endDate, orderBy,IsAdmin);
            this.storeDailyReport.DataBind();
        }

        protected void RefreshGrid(object s, StoreReadDataEventArgs e)
        {
            BindData();
        }


        protected void btnViewActivity_Click(object s, DirectEventArgs e)
        {
            int empId = int.Parse(hiddenValue.Text.Trim());

            DateTime start = DateTime.Now;
            DateTime end = DateTime.Now;

            start = dateStart.SelectedDate;
            end = dateEnd.SelectedDate;

            storeHistory.DataSource = ActivityManager.GetNotSubmittedList(empId, start, end);
            storeHistory.DataBind();
            Window1.Show();
        }

        #endregion



        public void btnExport_Click(object sender, EventArgs e)
        {

            int? branchId;
            string ename = txtName.Text;
            DateTime startDate;
            DateTime endDate;
            int? levelId;

            int orderBy = cmbGroupBy.SelectedItem.Index;

            if (cmbBranch.SelectedItem.Value != null)
            {
                branchId = int.Parse(cmbBranch.SelectedItem.Value);
            }
            else
            {
                branchId = null;
            }

            if (cmbLevel.SelectedItem.Value == null)
            {
                levelId = null;
            }
            else
            {
                levelId = int.Parse(cmbLevel.SelectedItem.Value);
            }

            if (dateStart.IsEmpty || dateEnd.IsEmpty)
            {
                NewMessage.ShowWarningMessage("Start and End date must be selected.");
                dateStart.Focus();
                return;
            }
            else
            {
                startDate = dateStart.SelectedDate;
                endDate = dateEnd.SelectedDate;

                
            }


            List<DARGetEmployeesResult> _ListAppraisalForm = ActivityManager.GetEmployeesDailyReport(ename, branchId, levelId, startDate, endDate, orderBy, IsAdmin);
            

            Dictionary<string, string> title = new Dictionary<string, string>();
            title["Daily Activity Report"] = "";



            ExcelHelper.ExportToExcel(
                           "Daily Activity Report",
                           _ListAppraisalForm, new List<string> { }, new List<string> { }, new Dictionary<string, string> { }
                           , new List<string> { }, title
                           );
        }

    }
}