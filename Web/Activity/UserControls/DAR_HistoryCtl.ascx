﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DAR_HistoryCtl.ascx.cs"
    Inherits="Web.Activity.UserControls.DAR_HistoryCtl" %>
<style type="text/css">
    
</style>
<script type="text/javascript">
        var CommandHandler = function (command, record) {
            <%= hiddenValue.ClientID %>.setValue(record.data.ActivityID);

            // view specific activity
            <%= btnViewActivity.ClientID %>.fireEvent('click');
        };
</script>
<%--  <ext:ResourceManager ID="ResourceManager1" runat="server" />--%>
<ext:Hidden ID="hiddenValue" runat="server" />
<ext:LinkButton ID="btnViewActivity" runat="server" Hidden="true">
    <DirectEvents>
        <Click OnEvent="btnViewActivity_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:LinkButton>
<ext:Store ID="storeActivity" runat="server" PageSize="50" OnReadData="RefreshGrid">
    <Model>
        <ext:Model ID="modelActivity" runat="server">
            <Fields>
                <ext:ModelField Name="ActivityDateEng" Type="Date" />
                <ext:ModelField Name="SubmissionDate" Type="Date" />
                <ext:ModelField Name="AttachmentName" Type="String" />
                <ext:ModelField Name="ActivityID" Type="Int" />
                <ext:ModelField Name="Level" Type="String" />
                <ext:ModelField Name="Name" Type="String" />
            </Fields>
        </ext:Model>
    </Model>
</ext:Store>
<ext:Store ID="storeBranch" runat="server">
    <Model>
        <ext:Model ID="modelBranch" runat="server">
            <Fields>
                <ext:ModelField Name="BranchId" Type="Int" />
                <ext:ModelField Name="Name" Type="String" />
            </Fields>
        </ext:Model>
    </Model>
</ext:Store>
<ext:Store ID="storeLevel" runat="server">
    <Model>
        <ext:Model ID="modelLevel" runat="server">
            <Fields>
                <ext:ModelField Name="LevelId" Type="Int" />
                <ext:ModelField Name="Name" Type="String" />
            </Fields>
        </ext:Model>
    </Model>
</ext:Store>
<div class="pageheader">
    <div class="media">
        <div class="media-body">
            <h4>
                Activity History
            </h4>
        </div>
    </div>
</div>
<div class="contentpanel">
    <div class="contentArea">
        <div class="alert alert-info">
            <table class="fieldTable">
                <tr>
                    <td colspan="3">
                        Narrow your list
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:ComboBox ID="cmbBranch" runat="server" FieldLabel="" Width="150px" DisplayField="Name"
                            ValueField="BranchId" StoreID="storeBranch" Editable="false" EmptyText="" QueryMode="Local">
                        </ext:ComboBox>
                    </td>
                    <td>
                        <ext:ComboBox ID="cmbLevel" runat="server" FieldLabel="" Width="150px" DisplayField="Name"
                            ValueField="LevelId" StoreID="storeLevel" Editable="false" EmptyText="" QueryMode="Local">
                        </ext:ComboBox>
                    </td>
                    <td>
                        <ext:TextField ID="txtName" runat="server" FieldLabel="" EmptyText="Type a Name"
                            Width="150px">
                        </ext:TextField>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:ComboBox ID="cmbGroupBy" runat="server" FieldLabel="Sort by" LabelAlign="Top"
                            Width="150px" AllowBlank="false">
                            <Items>
                                <ext:ListItem Text="Date" Index="0" />
                                <ext:ListItem Text="Employee Name" Index="1" />
                            </Items>
                            <SelectedItems>
                                <ext:ListItem Index="0" />
                            </SelectedItems>
                        </ext:ComboBox>
                    </td>
                    <td>
                        <ext:DateField ID="dateStart" runat="server" FieldLabel="Start Date" LabelAlign="Top"
                            Width="150px">
                        </ext:DateField>
                    </td>
                    <td>
                        <ext:DateField ID="dateEnd" runat="server" FieldLabel="End Date" LabelAlign="Top"
                            Width="150px">
                        </ext:DateField>
                    </td>
                    <td style="vertical-align: bottom; padding-bottom: 0px;">
                        <ext:Button ID="btnSearch" runat="server" Text="Search" >
                            <DirectEvents>
                                <Click OnEvent="btnSearch_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                    </td>
                </tr>
            </table>
        </div>
        <div>
            <p>
                Daily Activity Reports Already Verified</p>
            <div class="gridWrapper">
                <ext:GridPanel ID="gridActivities" runat="server" StoreID="storeActivity"
                    Header="false" Resizable="false" MinHeight="600" AutoScroll="true">
                    <ColumnModel>
                        <Columns>
                            <ext:DateColumn ID="colActivityDate" runat="server" Flex="1" Align="Left" Text="Activity Date"
                                MenuDisabled="true" Sortable="false" Format="ddd - dd MMM" DataIndex="ActivityDateEng"
                                Resizable="false" Draggable="false">
                            </ext:DateColumn>
                            <ext:Column ID="colEmpName" runat="server" Flex="1" Align="Left" Text="Employee Name"
                                MenuDisabled="true" Sortable="false" Draggable="false" DataIndex="Name" Resizable="false">
                            </ext:Column>
                            <ext:Column ID="colLevel" runat="server" Flex="1" Align="Left" Text="Level" MenuDisabled="true"
                                Sortable="false" Draggable="false" DataIndex="Level" Resizable="false">
                            </ext:Column>
                            <ext:DateColumn ID="colSubmissionDate" Align="Left" runat="server" Text="Submission Date"
                                MenuDisabled="true" Sortable="false" DataIndex="SubmissionDate" Format="ddd - dd MMM H:i:s"
                                Flex="1" Resizable="false" Draggable="false">
                            </ext:DateColumn>
                            <ext:Column ID="colAttachment" Align="Left" runat="server" Text="Attachment" MenuDisabled="true"
                                Sortable="false" Flex="1" DataIndex="AttachmentName" Resizable="false" Draggable="false">
                            </ext:Column>
                            <ext:CommandColumn ID="colCommands" Align="Left" Width="75px" runat="server" Resizable="false"
                                Draggable="false" MenuDisabled="true" Sortable="false">
                                <Commands>
                                    <ext:GridCommand Text="View" Icon="PageGo" CommandName="view" MinWidth="50">
                                    </ext:GridCommand>
                                </Commands>
                                <Listeners>
                                    <Command Handler="CommandHandler(command,record);" />
                                </Listeners>
                            </ext:CommandColumn>
                        </Columns>
                    </ColumnModel>
                    <BottomBar>
                        <ext:PagingToolbar ID="paging1" runat="server">
                        </ext:PagingToolbar>
                    </BottomBar>
                </ext:GridPanel>
            </div>
        </div>
        <div>
            <ext:Window ID="winActivity" MinWidth="940" MinHeight="200" Title="Daily Activity Report"
                Closable="true" Resizable="false" Hidden="true" runat="server" AutoScroll="true">
                <Content>
                    <div class="windowContentWrapper">
                        <table style="margin-left: 10px;">
                            <tr>
                                <td colspan="2">
                                    <span class="bolds">
                                        <ext:Label ID="lblEmpName" runat="server" Text="">
                                        </ext:Label>
                                    </span><span class="bolds">
                                        <ext:Label ID="lblEmpPosition" runat="server" Text="">
                                        </ext:Label>
                                    </span><span class="bolds">
                                        <ext:Label ID="lblEmpBranch" runat="server" Text="">
                                        </ext:Label>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="boldLabels">Activity Date:</span>
                                    <ext:Label ID="lblActivityDate" runat="server" Text="Date Here" />
                                </td>
                                <td align="left">
                                    <span class="boldLabels">Submission Date:</span>
                                    <ext:Label ID="lblSubmissionDate" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <span class="boldLabels" style="color: #6495ED;">Key Learnings</span>
                                    <hr />
                                    <div style="margin-left: 15px;">
                                        <ext:Label ID="lblLearnings" runat="server" Text="[Learnings]">
                                        </ext:Label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <span class="boldLabels" style="color: #6495ED;">Key Achievements</span>
                                    <hr />
                                    <div style="margin-left: 15px;">
                                        <ext:Label ID="lblAchievements" runat="server" Text="[Achievements]">
                                        </ext:Label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <span class="boldLabels" style="color: #6495ED;">Details</span>
                                    <hr />
                                    <div style="background-color: #ffffff; padding: 10px; border: 1px solid silver; max-height: 150px;
                                        overflow-x: scroll;">
                                        <%--<ext:TextArea ID="txtDetails" runat="server" Rows="20" Height="100px" Width="860px"
                                                MinHeight="100" AutoScroll="true">
                                            </ext:TextArea>--%>
                                        <ext:TextArea ID="txtDetails" runat="server" Width="770px" Height="200px" AutoScroll="true">
                                        </ext:TextArea>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div style="display: flex;">
                                        <span class="boldLabels">Attachment:</span>
                                        <ext:LinkButton ID="btnAttachment" runat="server" Cls="darkOrange">
                                            <DirectEvents>
                                                <Click OnEvent="btnAttachment_Click" IsUpload="true" Success="Ext.net.DirectMethods.Download({ isUpload : true });">
                                                    <EventMask ShowMask="false" />
                                                </Click>
                                            </DirectEvents>
                                        </ext:LinkButton>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="buttonBlock buttonLeft">
                                        <ext:LinkButton ID="btnCancel" runat="server" Text="Cancel" Cls="btnFlatLeftGap"
                                            BaseCls="btnFlatLeftGap">
                                            <Listeners>
                                                <Click Handler="#{winActivity}.hide();" />
                                            </Listeners>
                                        </ext:LinkButton>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </Content>
            </ext:Window>
        </div>
    </div>
    <!-- end of contentArea class -->
</div>
