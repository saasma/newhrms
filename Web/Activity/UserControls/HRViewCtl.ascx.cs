﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using BLL;
using DAL;
using BLL.Manager;
using System.IO;
using System.Text;
using BLL.Base;

namespace Web.Activity.UserControls
{
    public partial class HRViewCtl : BaseUserControl
    {
        public bool IsAdmin
        {
            get;
            set;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialize();
            }
        }

        protected void btnSearch_Click(object s, DirectEventArgs e)
        {
            BindActivityGrid();
        }

        protected void btnViewActivity_Click(object s, DirectEventArgs e)
        {
            BindWindowData();
            winActivity.Show();
        }

        protected void btnVerifyList_Click(object sender, DirectEventArgs e)
        {

            StringBuilder result = new StringBuilder();
            List<int> activityList = new List<int>();

            RowSelectionModel sm = this.gridActivities.GetSelectionModel() as RowSelectionModel;

            foreach (SelectedRow row in sm.SelectedRows)
            {
                activityList.Add(int.Parse(row.RecordID));
            }

            int statusEnum = Convert.ToInt32(DARStatusEnum.Verified);

            if (activityList.Count > 0)
            {

                Status status = ActivityManager.VerifyActivity(activityList, statusEnum);

                if (status.IsSuccess)
                {
                    NewMessage.ShowNormalMessage("Verified!");

                    BindActivityGrid();
                }
                else
                {
                    NewMessage.ShowWarningMessage("Sorry! could not verify. Try again.");
                }
            }
        }

        protected void btnVerifyActivity_Click(object s, DirectEventArgs e)
        {
            int statusEnum = Convert.ToInt32(DARStatusEnum.Verified);
            int ActivityID = Convert.ToInt32(this.hiddenValue.Text.Trim());

            Status status = ActivityManager.VerifyActivity(ActivityID, statusEnum);

            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Verified!");
                winActivity.Close();
                BindActivityGrid();
            }
            else
            {
                NewMessage.ShowWarningMessage("Sorry! could not verify. Try again.");
            }
        }

        protected void btnAttachment_Click(object s, DirectEventArgs e)
        {
            int ActivityID = Convert.ToInt32(hiddenValue.Text.Trim());
            string queryFile = ActivityManager.GetAttachmentURLFromID(ActivityID);
            string originalName = ActivityManager.GetAttachmentFromID(ActivityID);

            string filePath = Server.MapPath(@"~/Uploads/" + queryFile);

            FileInfo file = new FileInfo(filePath);
            if (file.Exists)
            {
                //Response.Clear();
                //Response.AddHeader("Content-Disposition", "attachment; filename=" + originalName);
                //Response.AddHeader("Content-Length", file.Length.ToString());
                //Response.ContentType = "application/octet-stream";
                //Context.Response.WriteFile(file.FullName);
                //Response.End();

                //write file 
                HttpContext.Current.Response.ClearHeaders();
                HttpContext.Current.Response.ContentType = Utils.Helper.Util.GetMimeType(Path.GetExtension(filePath));
                HttpContext.Current.Response.AddHeader("Content-Disposition", String.Format("attachment;filename=\"{0}\"", originalName));

                HttpContext.Current.Response.WriteFile(file.FullName);

                HttpContext.Current.Response.End();
            }
            else
            {
                NewMessage.ShowWarningMessage("This file does not exist.");
            }
        }

        #region Methods

        private void Initialize()
        {
            this.storeBranch.DataSource = ActivityManager.GetAllBranches();
            this.storeBranch.DataBind();

            this.cmbBranch.Items.Insert(0, new Ext.Net.ListItem("-- Select Branch --", null));
            this.cmbBranch.SelectedItem.Index = 0;

            this.storeLevel.DataSource = ActivityManager.GetLevels();
            this.storeLevel.DataBind();

            this.cmbLevel.Items.Insert(0, new Ext.Net.ListItem("-- Select Level --", null));
            this.cmbLevel.SelectedItem.Index = 0;

            DateTime today = DateTime.Today;

            DateTime startOfMonth = new DateTime(today.Year, today.Month, 1);
            // DateTime endOfMonth = new DateTime(today.Year, today.Month, DateTime.DaysInMonth(today.Year, today.Month));

            dateStart.SelectedDate = startOfMonth;
            dateEnd.SelectedDate = DateTime.Today;

            //dateStart.SelectedDate = new DateTime(2014, 1, 1);
            //dateEnd.SelectedDate = new DateTime(2014, 4, 1);

            BindActivityGrid();
        }

        private void BindWindowData()
        {
            int ActivityID = Convert.ToInt32(hiddenValue.Text.Trim());

            //lblActivityDate.Text = ActivityManager.GetActivityDateFromID(ActivityID);
            //txtDetails.Html = ActivityManager.GetDetailsFromID(ActivityID);
            //btnAttachment.Text = ActivityManager.GetAttachmentFromID(ActivityID);

            DAL.Activiti entity = ActivityManager.GetActivityFromID(ActivityID);

            BindEmployeeDetails(entity.EmployeeId);

            lblActivityDate.Text = entity.ActivityDateEng.Value.ToShortDateString();
            txtDetails.Text = entity.Details;
            lblSubmissionDate.Text = entity.SubmissionDate.Value.ToShortDateString();

            string fileName = entity.AttachmentName;
            if (!string.IsNullOrEmpty(fileName))
            {
                btnAttachment.Text = fileName;
                btnAttachment.Enable();
            }
            else
            {
                btnAttachment.Text = " NONE";
                btnAttachment.Disable();
            }

            lblLearnings.Text = "";
            List<ActivityLearning> lstLearnings = ActivityManager.GetLearningsFromID(ActivityID);
            foreach (ActivityLearning learning in lstLearnings)
            {
                lblLearnings.AppendLine("\u2022 " + learning.Text);
                lblLearnings.AppendLine("\n");
            }

            lblAchievements.Text = "";
            List<ActivityAchvement> lstAchievements = ActivityManager.GetAchievementsFromID(ActivityID);
            foreach (ActivityAchvement achievement in lstAchievements)
            {
                lblAchievements.AppendLine("\u2022 " + achievement.Text);
                lblAchievements.AppendLine("\n");
            }
        }

        private void BindActivityGrid()
        {
            string name = txtName.Text;
            int? branchId, levelId;
            DateTime startDate, endDate;
            int groupBy;

            if (dateStart.IsEmpty || dateEnd.IsEmpty)
            {
                NewMessage.ShowWarningMessage("Both start and end date must be selected.");
                return;
            }
            else
            {
                startDate = dateStart.SelectedDate;
                endDate = dateEnd.SelectedDate;
            }

            if (cmbBranch.SelectedItem.Value == null)
            {
                branchId = null;
            }
            else
            {
                branchId = int.Parse(cmbBranch.SelectedItem.Value);
            }

            if (cmbLevel.SelectedItem.Value == null)
            {
                levelId = null;
            }
            else
            {
                levelId = int.Parse(cmbLevel.SelectedItem.Value);
            }

            groupBy = cmbGroupBy.SelectedItem.Index;

            this.storeActivity.DataSource = ActivityManager.GetNotVerifiedActivities(name, branchId, levelId, groupBy, startDate, endDate,IsAdmin);
            this.storeActivity.DataBind();
        }

        private void BindEmployeeDetails(int empId)
        {
            EEmployee entityEmp = ActivityManager.GetEmployee(empId);

            int branchId = Convert.ToInt32(entityEmp.BranchId);
            int designationId = Convert.ToInt32(entityEmp.DesignationId);

            Branch entityBranch = ActivityManager.GetBranch(branchId);
            EDesignation entityDesignation = ActivityManager.GetDesignation(designationId);

            lblEmpName.Text = entityEmp.Name;
            lblEmpBranch.Text = entityBranch.Name;
            lblEmpPosition.Text = entityDesignation.Name;
        }

        protected void BindActivityData(object s, StoreReadDataEventArgs e)
        {
            BindActivityGrid();
        }

        #endregion
    }
}