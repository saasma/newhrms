﻿<%@ Page Title="HR View" MasterPageFile="~/Master/NewDetails.Master " Language="C#" AutoEventWireup="true" CodeBehind="HRView.aspx.cs" Inherits="Web.Activity.HRView" %>
<%@ Register Src="~/Activity/UserControls/HRViewCtl.ascx" TagName="HRViewCtl" TagPrefix="uc" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="head">
  
</asp:Content>
    
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="ContentPlaceHolder_Main"><%--<ext:ResourceManager ID="ResourceManager1" runat="server"  ScriptMode="Release" />--%>
 <div class="contentpanel">
   <uc:HRViewCtl runat="server" id="h1"  IsAdmin="true"/>
</div>
</asp:Content>
