﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Recomendation.aspx.cs" Inherits="Web.Activity.Recomendation" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Recommendation</title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:ResourceManager ID="ResourceManager1" runat="server" />
    <ext:FormPanel
    ID="Panel1" 
            runat="server" 
            Title="Recomendation"
            BodyPaddingSummary="5 5 0"
            Width="1000"
            Frame="true"
            ButtonAlign="Center"
            Layout="FormLayout">
            <FieldDefaults MsgTarget="Side" LabelWidth="75" />
            <Plugins>
                <ext:DataTip ID="DataTip1" runat="server" />
            </Plugins>
            
            <Items>
            
                <ext:Container ID="Container1" runat="server" Layout="Column" >
                    <Items>
                        <ext:Container ID="Container2" runat="server" Layout="FormLayout" ColumnWidth=".15" Padding="5">
                            <Items>
                                <ext:Label ID = "Label1" runat = "server" Text ="TraningName"/>
                                <ext:Label ID = "Label2" runat = "server" Text ="Details"/>
                                 <ext:Label ID = "Label3" runat = "server" Text ="Justification"/>
                            </Items>
                    </ext:Container>

                    <ext:Container ID="Container3" runat="server" Layout="FormLayout" ColumnWidth=".85" Padding="5">
                            <Items>
                                 <ext:Label ID="LblTraningName" runat="server" Width="400" FieldLabel="Traning Name" LabelAlign="Top" />
                                  <ext:Label ID="LblDetails" runat="server" TextMode="multiline" FieldLabel="Provide Some Detail" LabelAlign="Top" />
                                 <ext:Label ID="LblJustification" runat="server" TextMode="multiline" FieldLabel="Why do You Think This Traning is Important for you?" LabelAlign="Top" />
                                
                            </Items>
                    </ext:Container>
                </Items>
                </ext:Container>

                <ext:Container ID="Container4" runat="server" Layout="Column">
                    <Items>
                        <ext:Container ID="Container5" runat="server" Layout="FormLayout" ColumnWidth="1" Padding="5">
                            <Items>
                                <ext:TextArea ID="txtYourComment" runat="server" TextMode="multiline" FieldLabel="Your Comment" LabelAlign="Top" />
                                
                            </Items>
                        </ext:Container>
                    </Items>
                </ext:Container>
            </Items>
            <Buttons>
                     <ext:Button ID="BtnReccomendation" runat="server"  Text="Recommend">
                        <DirectEvents>
                            <Click OnEvent = "BtnReccomendation_Click" />
                        </DirectEvents>
                     </ext:Button>
                    <ext:Button ID="BtnDonotRecommend" runat="server"  Text="Do Not Recommend">
                        <DirectEvents>
                            <Click OnEvent = "BtnDonotRecommend_Click" />
                        </DirectEvents>
                    </ext:Button>
               
            </Buttons>

    </ext:FormPanel>
    </form>
</body>
</html>
