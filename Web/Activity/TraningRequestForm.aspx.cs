﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using Ext.Net;
using BLL.Manager;
using BLL;

namespace Web.Activity
{
    public partial class TraningRequestForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.storeEmployee.DataSource = BindData();
            this.storeEmployee.DataBind();
        }

        private List<EEmployee> BindData()
        {
            PayrollDataContext db = new PayrollDataContext();

            List<EEmployee> LstEmployee = db.EEmployees.OrderBy(x => x.EmployeeId).ToList();

            return LstEmployee;

        }

        protected void BtnSaveAndSend_Click(object sender, DirectEventArgs e)
        {
            TrainingAssesmentRequest entity = new TrainingAssesmentRequest();
            entity.TrainingName = txtTraningName.Text;
            entity.Details = txtDetail.Text;
            entity.EmployeeId = SessionManager.CurrentLoggedInEmployeeId;
            entity.Importance = txtwhyImportant.Text;
            entity.RecommendEmployeeId = Convert.ToInt32(ddlEmployee.SelectedItem.Value);
            entity.SubmittedOnEng = DateTime.Now;

            //entity.EmployeeId = SessionManager.CurrentLoggedInEmployeeId;
            //entity.AssessmentId = 1;

            bool isSave = TrainingManager.SaveRequest(entity);

            Response.Write("Success");
        }

        protected void BtnCancel_Click(object sender, DirectEventArgs e)
        {
            Response.Redirect("TraningNeedCirculation");
        }
    }
}