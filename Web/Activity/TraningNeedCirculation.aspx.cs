﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using BLL.Manager;
using BLL;
using DAL;
using Newtonsoft.Json.Linq;

namespace Web.Activity
{
    public partial class Try : System.Web.UI.Page
    {
        int TrainingAssesmentId;
        double months;
        //TraningCircular tcir = new TraningCircular();

        List<TrainingAssessment> Traning;

        public string DepartmentName { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Store store = this.ddlWorkedFor.GetStore();
                store.DataSource = new object[]
                {
                    new object[]  {"All", "0"},
                    new object[] {"Worked For", "1"}
                };

                Grid.Visible = true;
                this.storeBranches.DataSource = Branch();
                this.storeBranches.DataBind();

                this.storeDepartment.DataSource = Department();
                this.storeDepartment.DataBind();

                this.StorePosition.DataSource = Position();
                this.StorePosition.DataBind();

                this.StoreLevel.DataSource = Level();
                this.StoreLevel.DataBind();
            }
        }

        protected void BtnEditTemplate_Click(object sender, DirectEventArgs e)
        {
            Session["Name"] = ddlDepartment.SelectedItem.Text;
            Response.Redirect("CirculationTemplate.aspx");
        }


        protected void BtnShow_Click(object sender, DirectEventArgs e)
        {
            Grid.Visible = true;

            if (String.IsNullOrEmpty(txtMonth.Text) == true)
            {
                months = 0;
            }
            else
            {
                months = Convert.ToDouble(txtMonth.Text);
            }
           
            //Double.Parse 
            this.storegrid.DataSource = TrainingManager.GetAllEmployees(Convert.ToInt32(ddlDepartment.SelectedItem.Value), Convert.ToInt32(ddlBranch.SelectedItem.Value), Convert.ToInt32(ddlPosition.SelectedItem.Value), months, Convert.ToInt32(ddlLevel.SelectedItem.Value));

            this.storegrid.DataBind();


        }

        private List<Branch> Branch()
        {
            PayrollDataContext db = new PayrollDataContext();

            List<Branch> lstBranches = db.Branches.OrderBy(x => x.BranchId).ToList();

            return lstBranches;

        }

        private List<Department> Department()
        {
            PayrollDataContext db = new PayrollDataContext();

            List<Department> lstdepartment = db.Departments.OrderBy(x => x.DepartmentId).ToList();

            return lstdepartment;
        }

        private List<BLevel> Level()
        {
            PayrollDataContext db = new PayrollDataContext();

            List<BLevel> lstLevel = db.BLevels.OrderBy(x => x.LevelId).ToList();

            return lstLevel;
        }

        private List<EDesignation> Position()
        {
            PayrollDataContext db = new PayrollDataContext();

            List<EDesignation> lstPosition = db.EDesignations.OrderBy(x => x.DesignationId).ToList();

            return lstPosition;
        }

        protected void BtnSaveEmployeeCircular_CLick(object sender, DirectEventArgs e)
        {


            SaveAssesment();
            GetMaxData();

            string gridJSON = e.ExtraParams["gridItems"];

            List<TrainingAssessmentEmployee>  tList = Ext.Net.JSON.Deserialize<List<TrainingAssessmentEmployee>>(gridJSON);


            TrainingAssessmentEmployee entity = new TrainingAssessmentEmployee();
            entity.AssessmentId = TrainingAssesmentId;

            bool isSuccess = TrainingManager.SaveTraning(tList, entity);
           // JArray jsonArray = Ext.Net.JSON.Deserialize(gridJSON) as JArray;

            
            //for(int i=0; i<Grid.Store.Count; i++)
            //{

            //     bool isSave = TrainingManager.SaveTraning(entity);
            // }
            //for (int i = 0; i < jsonArray.Count; i++)
            //{
            //    JToken row = jsonArray[i];

            //tList.First().EmployeeId;

            
                //entity.EmployeeId = int.Parse(row["EmployeeId"].Value<string>());
                //bool isSave = TrainingManager.SaveTraning(entity);

            
        }

        private void SaveAssesment()
        {
            TrainingAssessment entity = new TrainingAssessment();
            entity.FromName = Session["From"].ToString();
            entity.Subject = Session["Subject"].ToString();
            entity.Body = Session["Body"].ToString();
            entity.DueDateEng = Convert.ToDateTime(TxtDate.Text);
            entity.CreatedOn = DateTime.Now;
            entity.CreatedBy = SessionManager.CurrentLoggedInUserID;
            entity.SendCircualtionType = Convert.ToInt32(ddlWorkedFor.SelectedItem.Value);
            if (String.IsNullOrEmpty(txtMonth.Text) == true)
            {
                entity.MinMonths = 0;
            }
            else
            {
                entity.MinMonths = Convert.ToDouble(txtMonth.Text);
            }
            
            bool isSave = TrainingManager.Save(entity);

            Response.Write("Success");
        }

        private void GetMaxData()
        {
            GetMaxValueResult entity = TrainingManager.GetMax();
            TrainingAssesmentId = Convert.ToInt32(entity.Column1);
        }
    }
}