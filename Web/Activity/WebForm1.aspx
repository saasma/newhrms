﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Activity.Master" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="Web.Activity.WebForm1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

<body>
    <%--<form id="Form1" runat="server">--%>
    <%--<ext:ResourceManager ID="ResourceManager1" runat="server" />--%>
   <ext:Panel 
            ID="Panel3"
            runat="server" 
            Title="Traning Need Assesment Circulation Template" 
            Frame="true"
            PaddingSummary="5px 5px 0"
            Width="1000"
            ButtonAlign="Center">
            <Items>
                <ext:Container ID="Container1" runat="server" Layout="Column" Height="100">
                    <Items>
                        <ext:Container ID="Container2" runat="server" Layout="FormLayout" ColumnWidth=".5" Padding="5">
                            <Items>
                                <ext:TextField ID="txtFrom" runat="server" FieldLabel="Form Name" LabelAlign="Top" />
                                    
                                <ext:TextField ID="TextField3" runat="server" FieldLabel="Subject" Text="Traning Need Circular" LabelAlign="Top" />
                            </Items>
                        </ext:Container>
                        
                    </Items>
                </ext:Container>
                <ext:Container ID="Container4" runat="server" Layout="FormLayout">

                    <Items>
                        <ext:HtmlEditor ID="HtmlEditor1" runat="server" Height="200"  LabelAlign="Top">
                        
                         </ext:HtmlEditor>
                    </Items>
                </ext:Container>
            </Items>
            <Buttons>
                <ext:Button ID="Button1" runat="server" Text="Save" />
                <ext:Button ID="Button2" runat="server" Text="Cancel" >
                 <DirectEvents>
                    <Click OnEvent="Button2_Click" />
                </DirectEvents>
                </ext:Button>

            </Buttons>
        </ext:Panel>
        
</body>

</asp:Content>
