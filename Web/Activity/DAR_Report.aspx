﻿<%@ Page Title="DAR Report" MasterPageFile="~/Master/NewDetails.Master" Language="C#"
    AutoEventWireup="true" CodeBehind="DAR_Report.aspx.cs" Inherits="Web.Activity.DAR_Report" %>

<%@ Register Src="~/Activity/UserControls/DAR_ReportCtl.ascx" TagName="HRViewCtl"
    TagPrefix="uc" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="head">
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="ContentPlaceHolder_Main">
    
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    DAR Report
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <uc:HRViewCtl runat="server" ID="h1" IsAdmin="true" />
    </div>
</asp:Content>
