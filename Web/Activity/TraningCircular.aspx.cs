﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BLL.Manager;
using BLL;
using DAL;
using Ext.Net;

namespace Web.Activity
{
    public partial class TraningCircular : System.Web.UI.Page
    {
        int EmployeeId;
        protected void Page_Load(object sender, EventArgs e)
        {
            EmployeeId = SessionManager.CurrentLoggedInEmployeeId;
            //DataTable dt = new DataTable();
            //dt = TrainingManager.GetTraningAssesmentEmployee(EmployeeId);
            //lblMessageFrom.Text = dt.Rows[0]["FromName"].ToString();

            GetTrainingAssesmentEmployeeResult entity = TrainingManager.GetTraningAssesmentEmployee(1);
            lblMessageFrom.Text = entity.FromName.ToString();
            LblDueDate.Text = entity.DueDate.ToString();
            HtmlEditor1.Text = entity.Body.ToString();

        }

        protected void BtnRequest_Click(object sender, DirectEventArgs e)
        {
            Session["Subject"] = EmployeeId;
            Response.Redirect("TraningRequestForm.aspx");
        }
    }
}