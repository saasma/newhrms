﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CirculationTemplate.aspx.cs" Inherits="Web.Activity.CirculationTemplate" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    
 </script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form runat="server">
    <ext:ResourceManager ID="ResourceManager1" runat="server" />
   <ext:Panel 
            ID="Panel3"
            runat="server" 
            Title="Traning Need Assesment Circulation Template" 
            Frame="true"
            PaddingSummary="5px 5px 0"
            Width="1000"
            ButtonAlign="Center">
            <Items>
                <ext:Container ID="Container1" runat="server" Layout="Column" Height="150">
                    <Items>
                        <ext:Container ID="Container2" runat="server" Layout="FormLayout" ColumnWidth=".5" Padding="5">
                            <Items>
                                <ext:TextField ID="txtFrom" runat="server" FieldLabel="Form Name" Text="Human Resources" LabelAlign="Top" />
                                    
                                <ext:TextField ID="txtsubject" runat="server" FieldLabel="Subject" Text="Traning Need Circular" LabelAlign="Top" />

                                
                            </Items>
                        </ext:Container>
                        
                    </Items>
                </ext:Container>
                <ext:Container ID="Container4" runat="server" Layout="FormLayout">

                    <Items>
                        <ext:HtmlEditor ID="HtmlEditor1" runat="server" Height="200"  LabelAlign="Top">
                        <%--<a HREF="TraningRequestForm.aspx">Traning Need</a>--%>
                         </ext:HtmlEditor>
                    </Items>
                </ext:Container>
            </Items>
            <Buttons>
                <ext:Button ID="Button1" runat="server" Text="Save">
                    <DirectEvents>
                        <Click OnEvent="BtnSave_Click">
                            <EventMask ShowMask="true" />
                        </Click>
                    </DirectEvents>
                </ext:Button>
                <ext:Button ID="Button2" runat="server" Text="Cancel" >
                 <DirectEvents>
                    <Click OnEvent="Button2_Click" />
                </DirectEvents>
                </ext:Button>

            </Buttons>
        </ext:Panel>
        </form>
</body>
</html>
