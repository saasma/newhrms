﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Details.aspx.cs" Inherits="Web.Activity.Details" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    
        <ext:ResourceManager ID="ResourceManager1" runat="server" />
        
        <ext:Store ID="storeEmployee" runat="server">
        <Model>
            <ext:Model ID="modelEployee" runat="server">
                <Fields>
                    <ext:ModelField Name="Name" Type="String" />
                    <ext:ModelField Name="EmployeeId" Type = "Int" />
                </Fields>
            </ext:Model>
        </Model>
        </ext:Store>

        <form runat="server" id="form1">
 
         <ext:FormPanel 
            ID="Panel1" 
            runat="server" 
            Title="Traning Request Form"
            BodyPaddingSummary="5 5 0"
            Width="1000"
            Frame="true"
            ButtonAlign="Center"
            Layout="FormLayout">
            <FieldDefaults MsgTarget="Side" LabelWidth="75" />
            <Plugins>
                <ext:DataTip ID="DataTip1" runat="server" />
            </Plugins>
            <Items>
                <ext:Container ID="Container1" runat="server" Layout="Column" >
                    <Items>
                        <ext:Container ID="Container2" runat="server" Layout="FormLayout" ColumnWidth="1" Padding="5">
                            <Items>
                                  <ext:TextField ID="txtTraningName" runat="server" Width="400" ReadOnly ="true" FieldLabel="Traning Name" LabelAlign="Top" />
                                  <ext:TextArea ID="txtDetail" runat="server" TextMode="multiline" ReadOnly ="true" FieldLabel="Provide Some Detail" LabelAlign="Top" />
                                 <ext:TextArea ID="txtwhyImportant" runat="server" TextMode="multiline" ReadOnly ="true" FieldLabel="Why do You Think This Traning is Important for you?" LabelAlign="Top" />
                                 
                                 <ext:TextField ID="txtRecommendedBy" runat="server" Width="400" ReadOnly ="true" FieldLabel="Recommended By" LabelAlign="Top" />
                            </Items>
                    </ext:Container>
                 </Items>
            </ext:Container>
      
                <ext:Button ID="BtnClose" runat="server" Text="Close" >
                    <DirectEvents>
                            <Click OnEvent="BtnClose_Click" />
                    </DirectEvents>
                </ext:Button>

               
               </Items>
       

        </ext:FormPanel>
    </form>
</body>
</html>
