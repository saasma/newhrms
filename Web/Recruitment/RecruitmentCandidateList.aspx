﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/HR.Master" AutoEventWireup="true" CodeBehind="RecruitmentCandidateList.aspx.cs" Inherits="Web.Recruitment.RecruitmentCandidateList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

<script type="text/javascript">

        var CommandHandler1 = function(command, record){
            <%= hiddenValue.ClientID %>.setValue(record.data.CandidateID);
                if(command=="Edit")
                {
                    <%= btnEdit.ClientID %>.fireEvent('click');
                }
                else
                {
                    <%= btnDelete.ClientID %>.fireEvent('click');
                }

             }
             
        
    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">

<ext:Hidden runat="server" ID="hiddenValue" />
    <ext:LinkButton runat="server" Hidden="true" ID="btnEdit">
        <DirectEvents>
            <Click OnEvent="btnEdit_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:Button ID="btnDelete" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnDelete_Click">
                <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the candidate?" />
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>

<div class="separator bottom">
    </div>



    <div class="innerLR">
        <%--<uc1:EmployeeDetailsCtl Id="EmployeeDetailsCtl1" runat="server" />--%>
        <div class="widget">
            <div class="widget-head">
                <h4 class="heading">
                    Recruitment Candidate List</h4>
            </div>
            <div class="widget-body">
                <ext:GridPanel StyleSpec="margin-top:15px;" ID="GridRecruitmentCandidate" runat="server" Cls="itemgrid">
                    <Store>
                        <ext:Store ID="storeRecruitmentCandidate" runat="server">
                            <Model>
                                <ext:Model ID="Model4" runat="server" IDProperty="CandidateID">
                                    <Fields>
                                        <ext:ModelField Name="CandidateID" Type="Int" />
                                        <ext:ModelField Name="CandidateName" Type="String" />
                                        <ext:ModelField Name="ContactAddress" Type="String" />
                                        <ext:ModelField Name="EmailID" Type="String" />
                                        <ext:ModelField Name="ContactNumber" Type="String" />
                                        <ext:ModelField Name="CitizenshipNo" Type="String" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                    </Store>
                    <ColumnModel>
                        <Columns>
                            <ext:Column ID="colCandidateID" Sortable="false" Visible="false" MenuDisabled="true" runat="server" Text="Candidate Id"
                                Width="70" Align="Left" DataIndex="CandidateID" />

                            <ext:Column ID="Column1" Sortable="false" MenuDisabled="true" runat="server" Text="Name"
                                Width="200" Align="Left" DataIndex="CandidateName" />

                            <ext:Column ID="Column2" Sortable="false" MenuDisabled="true" runat="server" Text="Address"
                                Width="100" Align="Left" DataIndex="ContactAddress" />

                            <ext:Column ID="Column3" Sortable="false" MenuDisabled="true" runat="server" Text="Email"
                                Width="150" Align="Left" DataIndex="EmailID" />

                            <ext:Column ID="Column4" Sortable="false" MenuDisabled="true" runat="server" Text="Contact Number"
                                Width="100" Align="Left" DataIndex="ContactNumber" />

                            <ext:Column ID="Column5" Sortable="false" MenuDisabled="true" runat="server" Text="Citizenship No"
                                Width="100" Align="Left" DataIndex="CitizenshipNo" />
                            
                           
                            <ext:CommandColumn ID="CommandColumn1" runat="server" Text="Actions" Align="Center" Width="63">
                                <Commands>
                                    <ext:GridCommand Text="<i></i>" Cls="btn-action glyphicons pencil btn-success" CommandName="Edit" />
                                    <ext:GridCommand Text="<i></i>" Cls="btn-action glyphicons remove_2 btn-danger" CommandName="Delete" />
                                </Commands>
                                <Listeners>
                                    <Command Handler="CommandHandler1(command,record);" />
                                </Listeners>
                            </ext:CommandColumn>
                        </Columns>
                    </ColumnModel>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                    </SelectionModel>
                </ext:GridPanel>

                <div class="buttonBlock">
                    <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="btnAdd" Cls="btnFlat"
                        BaseCls="btnFlat" Text="<i></i>Add New Recruitment Candidate">
                        <DirectEvents>
                            <Click OnEvent="btnAdd_Click">
                                <EventMask ShowMask="true" />
                            </Click>
                        </DirectEvents>
                    </ext:LinkButton>
                </div>

            </div>
        </div>
        
    </div>
    <br />



</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
