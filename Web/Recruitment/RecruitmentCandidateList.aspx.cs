﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using System.Xml;

namespace Web.Recruitment
{
    public partial class RecruitmentCandidateList : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        private void Initialise()
        {
            LoadRecruitmentCandidates();
        }

        private void LoadRecruitmentCandidates()
        {
            GridRecruitmentCandidate.GetStore().DataSource = RecruitmentManager.GetRecruitmentCandidateList();
            GridRecruitmentCandidate.GetStore().DataBind();
        }

        protected void btnDelete_Click(object sender, DirectEventArgs e)
        {
            int candidateId = int.Parse(hiddenValue.Text.Trim());

            Candidate candidate = RecruitmentManager.GetRecruitmentCandidateById(candidateId);
            string path = candidate.AttachedResumeUrl;
            Status status = RecruitmentManager.DeleteRecruitmentCandidate(candidateId);
            if (status.IsSuccess)
            {
                if (File.Exists(path))
                {
                    File.Delete(path);
                }

                LoadRecruitmentCandidates();
                NewMessage.ShowNormalMessage("Recruitment Candidate deleted.");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        protected void btnEdit_Click(object sender, DirectEventArgs e)
        {
            int candidateId = int.Parse(hiddenValue.Text.Trim());
            Response.Redirect("~/Recruitment/RecruitmentApplicationForm.aspx?candidateId=" + candidateId.ToString());
        }

        protected void btnAdd_Click(object sender, DirectEventArgs e)
        {
            Response.Redirect("~/Recruitment/RecruitmentApplicationForm.aspx");
        }
    }
}