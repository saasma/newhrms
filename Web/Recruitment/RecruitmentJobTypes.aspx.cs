﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using System.Xml;

namespace Web.Recruitment
{
    public partial class RecruitmentJobTypes : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        private void Initialise()
        {
            LoadRecruitmentJobTypes();
        }

        private void LoadRecruitmentJobTypes()
        {
            GridRecruitmentJobType.GetStore().DataSource = RecruitmentManager.GetRecruitmentJobTypes();
            GridRecruitmentJobType.GetStore().DataBind();
        }

        private void ClearFields()
        {
            hiddenValue.Text = "";
            txtName.Text = "";
        }

        protected void btnAdd_Click(object sender, DirectEventArgs e)
        {
            ClearFields();
            WindowRecruitmentJobType.Show();
        }


        protected void btnDelete_Click(object sender, DirectEventArgs e)
        {
            int jobTypeID = int.Parse(hiddenValue.Text.Trim());
            Status status = RecruitmentManager.DeleteRecruitmentJobType(jobTypeID);
            if (status.IsSuccess)
            {
                LoadRecruitmentJobTypes();
                NewMessage.ShowNormalMessage("Recruitment Job Type deleted.");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        protected void btnEdit_Click(object sender, DirectEventArgs e)
        {
            int jobTypeId = int.Parse(hiddenValue.Text.Trim());
            WindowRecruitmentJobType.Show();
            txtName.Text = RecruitmentManager.GetRecruitmentJobTypeName(jobTypeId);
        }

        protected void btnSaveUpdate_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("Save");
            if (Page.IsValid)
            {
                RecruitmentJobType recruitmentJobType = new RecruitmentJobType();
                recruitmentJobType.Name = txtName.Text.Trim();

                if (!string.IsNullOrEmpty(hiddenValue.Text.Trim()))
                    recruitmentJobType.JobTypeID = int.Parse(hiddenValue.Text.Trim());

                Status status = RecruitmentManager.InsertUpdateRecruitmentJobType(recruitmentJobType);
                if (status.IsSuccess)
                {
                    WindowRecruitmentJobType.Hide();
                    LoadRecruitmentJobTypes();
                    NewMessage.ShowNormalMessage("Recruitment Job Type saved.");
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }
            }


        }

    }
}