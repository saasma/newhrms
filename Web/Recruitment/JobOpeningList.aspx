﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/HR.Master" AutoEventWireup="true" CodeBehind="JobOpeningList.aspx.cs" Inherits="Web.Recruitment.JobOpeningList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

<script type="text/javascript">

        var CommandHandler1 = function(command, record){
            <%= hiddenValue.ClientID %>.setValue(record.data.OpeningID);
                if(command=="Edit")
                {
                    <%= btnEdit.ClientID %>.fireEvent('click');
                }
                else
                {
                    <%= btnDelete.ClientID %>.fireEvent('click');
                }

             }
             
        
    </script>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">

<ext:Hidden runat="server" ID="hiddenValue" />
    <ext:LinkButton runat="server" Hidden="true" ID="btnEdit">
        <DirectEvents>
            <Click OnEvent="btnEdit_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:Button ID="btnDelete" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnDelete_Click">
                <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the job opening?" />
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>

<div class="separator bottom">
    </div>
    <div class="innerLR">
        <%--<uc1:EmployeeDetailsCtl Id="EmployeeDetailsCtl1" runat="server" />--%>
        <div class="widget">
            <div class="widget-head">
                <h4 class="heading">
                    Recruitment Job Opening List</h4>
            </div>
            <div class="widget-body">
                <ext:GridPanel StyleSpec="margin-top:15px;" ID="GridJobOpeningList" runat="server" Cls="itemgrid">
                    <Store>
                        <ext:Store ID="storeJobOpeningList" runat="server">
                            <Model>
                                <ext:Model ID="Model4" runat="server" IDProperty="OpeningID">
                                    <Fields>
                                        <ext:ModelField Name="OpeningID" Type="Int" />
                                        <ext:ModelField Name="Branch" Type="String" />
                                        <ext:ModelField Name="Department" Type="String" />
                                        <ext:ModelField Name="Location" Type="String" />
                                        <ext:ModelField Name="Designation" Type="String" />
                                        <ext:ModelField Name="NumberOfPositions" Type="Int" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                    </Store>
                    <ColumnModel>
                        <Columns>
                            <ext:Column ID="colOpeningID" Sortable="false" MenuDisabled="true" runat="server" Text="Opening Id"
                                Width="100" Align="Left" DataIndex="OpeningID" />

                            <ext:Column ID="Column1" Sortable="false" MenuDisabled="true" runat="server" Text="Branch"
                                Width="100" Align="Left" DataIndex="Branch" />

                            <ext:Column ID="Column2" Sortable="false" MenuDisabled="true" runat="server" Text="Department"
                                Width="100" Align="Left" DataIndex="Department" />

                            <ext:Column ID="Column3" Sortable="false" MenuDisabled="true" runat="server" Text="Location"
                                Width="100" Align="Left" DataIndex="Location" />

                            <ext:Column ID="Column4" Sortable="false" MenuDisabled="true" runat="server" Text="Designation"
                                Width="100" Align="Left" DataIndex="Designation" />

                            <ext:Column ID="Column5" Sortable="false" MenuDisabled="true" runat="server" Text="No of Positions"
                                Width="100" Align="Left" DataIndex="NumberOfPositions" />
                            
                           
                            <ext:CommandColumn ID="CommandColumn1" runat="server" Text="Actions" Align="Center" Width="63">
                                <Commands>
                                    <ext:GridCommand Text="<i></i>" Cls="btn-action glyphicons pencil btn-success" CommandName="Edit" />
                                    <ext:GridCommand Text="<i></i>" Cls="btn-action glyphicons remove_2 btn-danger" CommandName="Delete" />
                                </Commands>
                                <Listeners>
                                    <Command Handler="CommandHandler1(command,record);" />
                                </Listeners>
                            </ext:CommandColumn>
                        </Columns>
                    </ColumnModel>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                    </SelectionModel>
                </ext:GridPanel>

                <div class="buttonBlock">
                    <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="btnAdd" Cls="btnFlat"
                        BaseCls="btnFlat" Text="<i></i>Add New Job Opening">
                        <DirectEvents>
                            <Click OnEvent="btnAdd_Click">
                                <EventMask ShowMask="true" />
                            </Click>
                        </DirectEvents>
                    </ext:LinkButton>
                </div>

            </div>
        </div>
        
    </div>
    <br />

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
