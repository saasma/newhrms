﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/HR.Master" AutoEventWireup="true" CodeBehind="RecruitmentJobTypes.aspx.cs" Inherits="Web.Recruitment.RecruitmentJobTypes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

<script type="text/javascript">

        var CommandHandler1 = function(command, record){
            <%= hiddenValue.ClientID %>.setValue(record.data.JobTypeID);
                if(command=="Edit")
                {
                    <%= btnEdit.ClientID %>.fireEvent('click');
                }
                else
                {
                    <%= btnDelete.ClientID %>.fireEvent('click');
                }

             }
             
        
    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">

<ext:Hidden runat="server" ID="hiddenValue" />
    <ext:LinkButton runat="server" Hidden="true" ID="btnEdit">
        <DirectEvents>
            <Click OnEvent="btnEdit_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:Button ID="btnDelete" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnDelete_Click">
                <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the recruitment job type?" />
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>

<div class="separator bottom">
    </div>
    <div class="innerLR">
        <%--<uc1:EmployeeDetailsCtl Id="EmployeeDetailsCtl1" runat="server" />--%>
        <div class="widget">
            <div class="widget-head">
                <h4 class="heading">
                    Recruitment Job Type</h4>
            </div>
            <div class="widget-body">
                <ext:GridPanel StyleSpec="margin-top:15px;" ID="GridRecruitmentJobType" runat="server" Cls="itemgrid">
                    <Store>
                        <ext:Store ID="storeRecruitmentJobType" runat="server">
                            <Model>
                                <ext:Model ID="Model4" runat="server" IDProperty="JobTypeID">
                                    <Fields>
                                        <ext:ModelField Name="JobTypeID" Type="Int" />
                                        <ext:ModelField Name="Name" Type="string" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                    </Store>
                    <ColumnModel>
                        <Columns>
                            <ext:Column ID="Column5" Sortable="false" MenuDisabled="true" runat="server" Text="Name"
                                Width="300" Align="Left" DataIndex="Name" />
                           
                            <ext:CommandColumn ID="CommandColumn1" runat="server" Text="Actions" Align="Center" Width="63">
                                <Commands>
                                    <ext:GridCommand Text="<i></i>" Cls="btn-action glyphicons pencil btn-success" CommandName="Edit" />
                                    <ext:GridCommand Text="<i></i>" Cls="btn-action glyphicons remove_2 btn-danger" CommandName="Delete" />
                                </Commands>
                                <Listeners>
                                    <Command Handler="CommandHandler1(command,record);" />
                                </Listeners>
                            </ext:CommandColumn>
                        </Columns>
                    </ColumnModel>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                    </SelectionModel>
                </ext:GridPanel>
                <div class="buttonBlock">
                    <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="btnAdd" Cls="btnFlat"
                        BaseCls="btnFlat" Text="<i></i>Add New Recruitment Job Type">
                        <DirectEvents>
                            <Click OnEvent="btnAdd_Click">
                                <EventMask ShowMask="true" />
                            </Click>
                        </DirectEvents>
                    </ext:LinkButton>
                </div>
            </div>
        </div>
        <ext:Window ID="WindowRecruitmentJobType" runat="server" Title="Add/Edit Recruitment Job Type" Icon="Application"
            Width="450" Height="190" BodyPadding="5" Hidden="true" Modal="true">
            <Content>
                <table class="fieldTable">
                    <tr>
                        <td>
                            <ext:TextField ID="txtName" LabelSeparator="" runat="server" FieldLabel="Name *"
                                Width="400" LabelAlign="Top">
                            </ext:TextField>
                            <asp:RequiredFieldValidator Display="None" ID="rfvName" runat="server" ValidationGroup="Save"
                                ControlToValidate="txtName" ErrorMessage="Name is required." />
                        </td>
                    </tr>
                   
                    <tr>
                        <td valign="bottom" colspan="2">
                            <div class="popupButtonDiv">
                                <ext:LinkButton runat="server" StyleSpec="padding:0px;"
                                 ID="btnSaveUpdate" Cls="btnFlat" BaseCls="btnFlat" Text="<i></i>Save">
                                    <DirectEvents>
                                        <Click OnEvent="btnSaveUpdate_Click">
                                            <EventMask ShowMask="true" />
                                        </Click>
                                    </DirectEvents>
                                    <Listeners>
                                        <Click Handler="valGroup = 'Save'; return CheckValidation();">
                                        </Click>
                                    </Listeners>
                                </ext:LinkButton>
                                <div class="btnFlatOr">
                                    or</div>
                                <ext:LinkButton runat="server" StyleSpec="padding:0px;" Cls="btnFlatLeftGap" ID="LinkButton1"
                                    Text="<i></i>Cancel">
                                    <Listeners>
                                        <Click Handler="#{WindowRecruitmentJobType}.hide();">
                                        </Click>
                                    </Listeners>
                                </ext:LinkButton>
                            </div>
                        </td>
                    </tr>
                </table>
            </Content>
        </ext:Window>
    </div>
    <br />


</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
