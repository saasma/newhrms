﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using System.Xml;

namespace Web.Recruitment
{
    public partial class RecruitmentHiringManagers : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        private void Initialise()
        {
            LoadRecruitmentHiringManagers();
        }

        private void LoadRecruitmentHiringManagers()
        {
            GridRecruitmentHriringManager.GetStore().DataSource = RecruitmentManager.GetRecruitmentHiringManagers();
            GridRecruitmentHriringManager.GetStore().DataBind();
        }

        private void ClearFields()
        {
            hiddenValue.Text = "";
            txtName.Text = "";
        }


        protected void btnAdd_Click(object sender, DirectEventArgs e)
        {
            ClearFields();
            WindowHiringManager.Show();
        }


        protected void btnDelete_Click(object sender, DirectEventArgs e)
        {    
            int managerId = int.Parse(hiddenValue.Text.Trim());
            Status status = RecruitmentManager.DeleteRecruitmentHiringManager(managerId);
            if (status.IsSuccess)
            {
                LoadRecruitmentHiringManagers();
                NewMessage.ShowNormalMessage("Recruitment Hiring Manager deleted.");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        protected void btnEdit_Click(object sender, DirectEventArgs e)
        {
            int managerId = int.Parse(hiddenValue.Text.Trim());            
            WindowHiringManager.Show();
            txtName.Text = RecruitmentManager.GetRecruitmentHiringManagerById(managerId);
        }

        protected void btnSaveUpdate_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("Save");
            if (Page.IsValid)
            {
                RecruitmentHiringManager recruitmentHiringManager = new RecruitmentHiringManager();
                recruitmentHiringManager.Name = txtName.Text.Trim();

                if(!string.IsNullOrEmpty(hiddenValue.Text.Trim()))
                    recruitmentHiringManager.ManagerID = int.Parse(hiddenValue.Text.Trim());

                Status status = RecruitmentManager.InsertUpdateRecruitmentHiringManager(recruitmentHiringManager);
                if (status.IsSuccess)
                {
                    WindowHiringManager.Hide();
                    LoadRecruitmentHiringManagers();
                    NewMessage.ShowNormalMessage("Recruitment Hiring Manager saved.");
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }
            }


        }
    }
}