﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/HR.Master" AutoEventWireup="true" CodeBehind="RecruitmentApplicationForm.aspx.cs" Inherits="Web.Recruitment.RecruitmentApplicationForm" %>


<%@ Register Src="~/Recruitment/UserControls/CandidateEducationalDetails.ascx" TagName="EducationalDetlCtrl"
    TagPrefix="ucEducatoinalDetl" %>

<%@ Register Src="~/Recruitment/UserControls/CandidateTrainigDetial.ascx" TagName="TrainingDtlCtrl"
    TagPrefix="ucTrainingDetl" %>

<%@ Register Src="~/Recruitment/UserControls/CandidateEmploymentDetails.ascx" TagName="EmploymentDetlCtrl"
    TagPrefix="ucEmploymentDetl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">


<style type="text/css">

.rightAlign
{
    text-align:right;
}

</style>



</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
<ext:Hidden runat="server" ID="hdnValue">
</ext:Hidden>
<div class="innerLR">

 <div class="widget">
            <div class="widget-head">
                <h4 class="heading">
                    Basic Information</h4>
            </div>

             <div class="widget-body">
               <table class="fieldTable">
                    <tr>
                        <td>
                            <ext:TextField ID="txtCandidateID" Disabled="true" runat="server" FieldLabel="Candidate ID" LabelAlign="Left" LabelSeparator=""
                                Width="400">
                            </ext:TextField>

                        </td>
                    </tr>

                    <tr>
                        <td>
                            <ext:TextField ID="txtFirstName" runat="server" FieldLabel="First Name *" LabelAlign="Left" LabelSeparator=""
                                Width="400">
                            </ext:TextField>
                             <asp:RequiredFieldValidator Display="None" ID="rfvFirstName" runat="server" ValidationGroup="Save"
                                ControlToValidate="txtFirstName" ErrorMessage="First Name is required." /> 
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <ext:TextField ID="txtMiddleName" runat="server" FieldLabel="Middle Name" LabelAlign="Left" LabelSeparator=""
                                Width="400">
                            </ext:TextField>        
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <ext:TextField ID="txtLastName" runat="server" FieldLabel="Last Name *" LabelAlign="Left"
                                Width="400">
                            </ext:TextField>
                            <asp:RequiredFieldValidator Display="None" ID="rfvLastName" runat="server" ValidationGroup="Save"
                                ControlToValidate="txtLastName" ErrorMessage="Last Name is required." /> 
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <ext:TextField ID="txtContactAddress" runat="server" FieldLabel="Contact Address *" LabelAlign="Left" LabelSeparator=""
                                Width="400">
                            </ext:TextField>
                            <asp:RequiredFieldValidator Display="None" ID="rfvContactAddress" runat="server" ValidationGroup="Save"
                                ControlToValidate="txtContactAddress" ErrorMessage="Contact Address is required." /> 
                          
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <ext:TextField ID="txtEmailID" runat="server" FieldLabel="Email Id *" LabelAlign="Left" LabelSeparator=""
                                Width="400">
                            </ext:TextField>
                            <asp:RequiredFieldValidator Display="None" ID="rfvEmailId" runat="server" ValidationGroup="Save"
                                ControlToValidate="txtEmailID" ErrorMessage="Email Id is required." />
                              <asp:RegularExpressionValidator ID="revEmailId" runat="server" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                ControlToValidate="txtEmailID" ErrorMessage="Email is invalid." ValidationGroup="Save"
                                Display="None"></asp:RegularExpressionValidator>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <ext:TextField ID="txtContactNumber" runat="server" FieldLabel="Contact Number *" LabelAlign="Left" LabelSeparator=""
                                Width="400">
                            </ext:TextField>

                            <asp:RegularExpressionValidator ID="revContactNumber" runat="server" ValidationGroup="Save" Display="None"
                                SetFocusOnError="true" ErrorMessage="Contact number is invalid"
                                ControlToValidate="txtContactNumber" ValidationExpression="\d*"></asp:RegularExpressionValidator>
                            <asp:RequiredFieldValidator Display="None" ID="rfvContactNumber" runat="server" ValidationGroup="Save"
                                ControlToValidate="txtContactNumber" ErrorMessage="Contact Number is required." />
                        </td>
                    </tr>

                     <tr>
                        <td>
                             <ext:TextField ID="txtCreateOn" runat="server" Disabled="true" FieldLabel="Create On" LabelAlign="Left" LabelSeparator=""
                                Width="400">
                            </ext:TextField>
                              <%--<pr:CalendarExtControl FieldLabel="Create On" Width="200" ID="calCreateOn" runat="server"
                                    LabelSeparator="" LabelAlign="Left" />
                                <asp:RequiredFieldValidator Display="None" ID="valcalCreateOn" runat="server" ValidationGroup="Save"
                                    ControlToValidate="calCreateOn" ErrorMessage="Create on date is required." />--%>

                        </td>
                    </tr>

                    <tr>
                        <td>
                            <ext:TextField ID="txtSource" runat="server" FieldLabel="Source" LabelAlign="Left" LabelSeparator=""
                                Width="400">
                            </ext:TextField>
                            <asp:RequiredFieldValidator Display="None" ID="rfvSource" runat="server" ValidationGroup="Save"
                                ControlToValidate="txtSource" ErrorMessage="Source is required." />
                        </td>
                    </tr>

               </table>
             </div>

              <div class="widget-head">
                <h4 class="heading">
                    Family Information</h4>
            </div>

              <div class="widget-body">
               <table class="fieldTable">
                
                    <tr>
                        <td>
                             <ext:TextField ID="txtFatherName" runat="server" FieldLabel="Father's Name *" LabelAlign="Left" LabelSeparator=""
                                Width="400">
                            </ext:TextField>
                            <asp:RequiredFieldValidator Display="None" ID="rfvFatherName" runat="server" ValidationGroup="Save"
                                ControlToValidate="txtFatherName" ErrorMessage="Father's Name is required." />
                        </td>
                    </tr>

                    <tr>
                        <td>
                             <ext:TextField ID="txtMotherName" runat="server" FieldLabel="Mother's Name *" LabelAlign="Left" LabelSeparator=""
                                Width="400">
                            </ext:TextField>
                            <asp:RequiredFieldValidator Display="None" ID="rfvMotherName" runat="server" ValidationGroup="Save"
                                ControlToValidate="txtMotherName" ErrorMessage="Mother's Name is required." />
                        </td>
                    </tr>

                    <tr>
                        <td>
                             <ext:TextField ID="txtCitizenshipNo" runat="server" FieldLabel="Citizenship No" LabelAlign="Left" LabelSeparator=""
                                Width="400">
                            </ext:TextField>
                            <asp:RequiredFieldValidator Display="None" ID="rfvCitizenshipNo" runat="server" ValidationGroup="Save"
                                ControlToValidate="txtCitizenshipNo" ErrorMessage="Citizenship No is required." />
                        </td>
                    </tr>

                    <tr>
                        <td>
                             <ext:TextField ID="txtIssuedFrom" runat="server" FieldLabel="Issued From" LabelAlign="Left" LabelSeparator=""
                                Width="400">
                            </ext:TextField>
                            <asp:RequiredFieldValidator Display="None" ID="rfvIssuedFrom" runat="server" ValidationGroup="Save"
                                ControlToValidate="txtIssuedFrom" ErrorMessage="Issued From is required." />
                        </td>
                    </tr>

               </table>
             </div>

             <div id="divSkillSet">
                <div class="widget-head">
                <h4 class="heading">
                    Skill Sets</h4>
                </div>

                <div class="widget-body">
                   <table class="fieldTable">
                
                        <tr>
                            <td>
                                <ext:MultiCombo Width="300"  LabelSeparator="" SelectionMode="All" FieldLabel=""  ID="MultiCombo1" DisplayField="Name"
                                        ValueField="SkillSetId"  runat="server">
                                        <Store>
                                            <ext:Store ID="Store1" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model1" IDProperty="SkillSetId" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="SkillSetId" />
                                                            <ext:ModelField Name="Name" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <Listeners>
                                            
                                        </Listeners>
                                    </ext:MultiCombo>
                                   
                            </td>
                        </tr>
                    </table>

             </div>

             <div id="divHide" runat="server">

              <div id="divEducatoinalDetails">
                <div class="widget">
                    <div class="widget-head">
                        <h4 class="heading">
                            Educational Details</h4>
                    </div>
                    <ucEducatoinalDetl:EducationalDetlCtrl Id="EducationalDetlCtrl1" runat="server" />
                </div>
            </div>

            <div id="divTrainingDetail">
                <div class="widget">
                    <div class="widget-head">
                        <h4 class="heading">
                            Training Details</h4>
                    </div>
                    <ucTrainingDetl:TrainingDtlCtrl Id="TrainingDtlCtrl1" runat="server" />
                </div>
            </div>

            
                    <div class="widget">
                        <div class="widget-head">
                            <h4 class="heading">
                                Employment Details</h4>
                        </div>
                        <ucEmploymentDetl:EmploymentDetlCtrl Id="EmploymentDetlCtrl1s" runat="server" />
                    </div>
               </div>

               <div class="widget-body">
               <table class="fieldTable">
                    <tr>
                        <td>
                            <ext:TextField ID="txtCurrentSalary" FieldStyle="text-align:right;" runat="server" FieldLabel="Current Salary" LabelAlign="Left" LabelSeparator=""
                                Width="250">
                            </ext:TextField>
                            <asp:CompareValidator SetFocusOnError="true" ValueToCompare="0" Display="None" Operator="DataTypeCheck"
                                    Type="Currency" ID="cvCurrentSalary" ControlToValidate="txtCurrentSalary" ValidationGroup="Save"
                                    runat="server" ErrorMessage="Invalid current salary amount."></asp:CompareValidator>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <ext:TextField ID="txtExpectedSalary" FieldStyle="text-align:right;"  runat="server" FieldLabel="Expected Salary" LabelAlign="Left" LabelSeparator=""
                                Width="250">
                            </ext:TextField>
                            <asp:CompareValidator SetFocusOnError="true" ValueToCompare="0" Display="None" Operator="DataTypeCheck"
                                    Type="Currency" ID="cvExpectedSalary" ControlToValidate="txtExpectedSalary" ValidationGroup="Save"
                                    runat="server" ErrorMessage="Invalid current salary amount."></asp:CompareValidator>
                        </td>
                    </tr>

                    <tr>
                        <td>
                             <ext:FileUploadField ID="upload1" StyleSpec="cursor:pointer" FieldLabel="Attach Resume" LabelAlign="Left" runat="server" Text="Attach File" ButtonOnly="true"
                                Cls="btnFlat" BaseCls="btnFlat" Width="120px">                                         
                            </ext:FileUploadField>


                        </td>
                        <td>
                        
                            <ext:Label ID="lblUploadedFile" runat="server" Hidden="true">
                                
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </ext:Label>

                                <ext:LinkButton Icon="Delete" runat="server" ID="lnkDeleteFile" OnDirectClick="lnkDeleteFile_Click"
                                    Text="Delete file" Hidden="true">
                                    <DirectEvents>
                                        <Click OnEvent="lnkDeleteFile_Click">
                                         <Confirmation ConfirmRequest="true" Message="Do you want to delete the file?" />
                                            <EventMask ShowMask="true" />
                                        </Click>
                                    </DirectEvents>
                                </ext:LinkButton>
                        </td>
                    </tr>

               </table>
               </div>
               

               <div class="widget-head">
                <h4 class="heading">
                    Additional Info</h4>
                 </div>
                  
                    <div class="widget-body">
                        <table class="fieldTable">
                            <tr>
                                <td>
                                    <ext:TextArea ID="txtAdditionalInfo" runat="server" Width="500" AutoScroll="true">
                                            </ext:TextArea>
                                </td>
                            </tr>
                             <tr>
                                    <td>
                                        <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="btnSave" ValidationGroup="Save" Cls="btnFlat" BaseCls="btnFlat" Text="<i></i>Save" >
                                                    <DirectEvents>
                                                        <Click OnEvent="btnSave_Click">
                                                            <EventMask ShowMask="true" />
                                                        </Click>
                                                    </DirectEvents>
                                                    <Listeners>
                                                        <Click Handler="valGroup = 'Save'; return CheckValidation();">
                                                        </Click>
                                                    </Listeners>
                                                </ext:LinkButton>
                                    </td>
                                </tr>

                        </table>
                    </div>
        
              
       

</div>

    </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
