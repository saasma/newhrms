﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/HR.Master" AutoEventWireup="true" CodeBehind="JobOpeningDetails.aspx.cs" Inherits="Web.Recruitment.JobOpeningDetails" %>


<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

<script>
    Ext.onReady(function () {
        CKEDITOR.config.toolbar = [
   ['Styles', 'Format', 'Font', 'FontSize'],

   ['Bold', 'Italic', 'Underline', 'StrikeThrough', '-', 'Undo', 'Redo', '-', 'Cut', 'Copy', 'Paste', 'Find', 'Replace', '-', 'Outdent', 'Indent', '-'],
   '/',
   ['NumberedList', 'BulletedList', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
   ['Image', 'Table', '-', 'Link', 'TextColor', 'BGColor']
];

       
    });

</script>
<script>
    var updateButtonText = function (item, checked) {
        var text = [];

        item.parentMenu.items.each(function (item) {
            if (item.checked) {
                text.push(item.text);
            }
        });

        if (text.length == 0) {
            App.Button1.setText("[Select Colors]");
        } else {
            App.Button1.setText("[" + text.join(",") + "]");
        }
    };
    </script>

  

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
<ext:Hidden runat="server" ID="hdnValue">
</ext:Hidden>
<div class="innerLR">
       

        
        <div class="widget">
            <div class="widget-head">
                <h4 class="heading">
                    Recruitment Job Opening Details</h4>
            </div>
            <div class="widget-body">
               <table class="fieldTable">
                <tr>
                    <td>
                        Job Opening ID
                    </td>
                    <td>
                         <ext:TextField ID="txtJobOpeningID" Disabled="true" runat="server"
                                Width="200">
                            </ext:TextField>
                    </td>
                </tr>

                <tr>
                     <td>
                        Branch *
                    </td>
                    <td>
                         <ext:ComboBox ForceSelection="true" Hidden="false" QueryMode="Local" ID="cmbBranch" 
                                runat="server" DisplayField="Name" Width="200" ValueField="BranchId">
                                <Store>
                                    <ext:Store ID="storeBranch" runat="server" >
                                    
                                        <Fields>
                                            <ext:ModelField Name="BranchId"/>
                                            <ext:ModelField Name="Name" />
                                        </Fields>
                                    </ext:Store>
                                </Store>
                            </ext:ComboBox>
                            <asp:RequiredFieldValidator Display="None" ID="rfvBranch" runat="server" ValidationGroup="Save"
                                ControlToValidate="cmbBranch"  ErrorMessage="Please select branch." />
                    </td>
                </tr>

                <tr>
                    <td>
                        Department *
                    </td>

                    <td>
                         <ext:ComboBox ForceSelection="true" QueryMode="Local" ID="cmbDepartment"
                                runat="server" DisplayField="Name" Width="200" ValueField="DepartmentId">
                                <Store>
                                    <ext:Store ID="storeDepartment" runat="server" >
                                    
                                        <Fields>
                                            <ext:ModelField Name="DepartmentId"/>
                                            <ext:ModelField Name="Name" />
                                        </Fields>
                                    </ext:Store>
                                </Store>
                            </ext:ComboBox>
                            <asp:RequiredFieldValidator Display="None" ID="rfvDepartment" runat="server" ValidationGroup="Save"
                                ControlToValidate="cmbDepartment"  ErrorMessage="Please select department." />
                            
                    </td>
                </tr>

                <tr>
                    <td>
                        Location *
                    </td>
                    <td>
                         <ext:TextField ID="txtLocation" LabelSeparator="" runat="server" 
                                Width="200">
                            </ext:TextField>
                        <asp:RequiredFieldValidator Display="None" ID="rfvLocation" runat="server" ValidationGroup="Save"
                                ControlToValidate="txtLocation"  ErrorMessage="Location is required." />
                    </td>
                </tr>

                <tr>
                    <td>
                        Designation *
                    </td>
                    <td>
                         <ext:ComboBox ForceSelection="true" QueryMode="Local" ID="cmbDesignation"
                                runat="server" DisplayField="Name" Width="200"
                                ValueField="DesignationId">
                                <Store>
                                    <ext:Store ID="storeDesingnation" runat="server" >
                                    
                                        <Fields>
                                            <ext:ModelField Name="DesignationId"/>
                                            <ext:ModelField Name="Name" />
                                        </Fields>
                                    </ext:Store>
                                </Store>
                            </ext:ComboBox>
                           <asp:RequiredFieldValidator Display="None" ID="rfvDesignation" runat="server" ValidationGroup="Save"
                                ControlToValidate="cmbDesignation"  ErrorMessage="Please select designation." /> 
                    </td>
                </tr>

                <tr>
                    <td>
                        Job Description *
                    </td>
                    <td>
                        
                            <CKEditor:CKEditorControl ExtraPlugins="autogrow" ID="txtEditorDescription" Width="400" Height="200"
                                runat="server" BasePath="~/ckeditor/"></CKEditor:CKEditorControl>
                             <asp:RequiredFieldValidator Display="None" ID="rfvJobDescription" runat="server" ValidationGroup="Save"
                                ControlToValidate="txtEditorDescription"  ErrorMessage="Job Description is required." /> 
                   
                    </td>
                </tr>

                 <tr>
                    <td>
                        Hiring Manager *
                    </td>
                    <td>
                         <ext:ComboBox ForceSelection="true" QueryMode="Local" ID="cmbHiringManager"
                                runat="server" DisplayField="Name" Width="200"
                                ValueField="ManagerID">
                                <Store>
                                    <ext:Store ID="storeHiringManager" runat="server" >
                                    
                                        <Fields>
                                            <ext:ModelField Name="ManagerID"/>
                                            <ext:ModelField Name="Name" />
                                        </Fields>
                                    </ext:Store>
                                </Store>
                            </ext:ComboBox>
                           <asp:RequiredFieldValidator Display="None" ID="rfvHiringManager" runat="server" ValidationGroup="Save"
                                ControlToValidate="cmbHiringManager"  ErrorMessage="Please select hiring manager." /> 
                    </td>
                </tr>

                <tr>
                    <td>
                        Assigned Recruiter *
                    </td>
                    <td>
                         <ext:TextField ID="txtAssignedRecruiter" LabelSeparator="" runat="server"
                                Width="200">
                            </ext:TextField>
                         <asp:RequiredFieldValidator Display="None" ID="rfvAssignedRecruiter" runat="server" ValidationGroup="Save"
                                ControlToValidate="txtAssignedRecruiter"  ErrorMessage="Assigned recruiter is required." /> 
                    </td>
                </tr>

                <tr>
                    <td>
                        Date Opened *
                    </td>
                    <td>
                        <pr:CalendarExtControl  Width="200" ID="calDateOpened" runat="server" 
                                    LabelSeparator=""/>
                                <asp:RequiredFieldValidator Display="None" ID="valcalDateOpened" runat="server" ValidationGroup="Save"
                                    ControlToValidate="calDateOpened" ErrorMessage="Opened date is required." />
                    </td>
                </tr>

                <tr>
                    <td>
                        Last Date of Submission *
                    </td>
                    <td>

                        <pr:CalendarExtControl  Width="200" ID="calLastDateOfSub" runat="server" 
                                    LabelSeparator=""/>
                                <asp:RequiredFieldValidator Display="None" ID="rfvcalLastDateofSub" runat="server" ValidationGroup="Save"
                                    ControlToValidate="calLastDateOfSub" ErrorMessage="Last date of submission is required." />
                      
                    </td>
                </tr>

                 <tr>
                     <td>
                       Number of Positions *
                    </td>
                    <td>   
                            <ext:TextField ID="txtNoOfPositions" runat="server" Width="200">
                                </ext:TextField>
                            <asp:CompareValidator SetFocusOnError="true" ValueToCompare="0" Display="None" Operator="DataTypeCheck"
                                    Type="Integer" ID="cvNoOfPositions" ControlToValidate="txtNoOfPositions" ValidationGroup="Save"
                                    runat="server" ErrorMessage="Invalid no of positions."></asp:CompareValidator>
                            <asp:RequiredFieldValidator Display="None" ID="rfvNoOfPositions" runat="server" ValidationGroup="Save"
                                ControlToValidate="txtNoOfPositions"  ErrorMessage="No of positions is required." />

                    </td>
                </tr>

                 <tr>
                    <td>
                       Job Type *
                    </td>
                    <td>
                         <ext:ComboBox ForceSelection="true" QueryMode="Local" ID="cmbJobType"
                                runat="server" DisplayField="Name" Width="200" 
                                ValueField="JobTypeID">
                                <Store>
                                    <ext:Store ID="storeJobType" runat="server" >
                                    
                                        <Fields>
                                            <ext:ModelField Name="JobTypeID"/>
                                            <ext:ModelField Name="Name" />
                                        </Fields>
                                    </ext:Store>
                                </Store>
                            </ext:ComboBox>
                            <asp:RequiredFieldValidator Display="None" ID="rfvJobType" runat="server" ValidationGroup="Save"
                                ControlToValidate="cmbJobType"  ErrorMessage="Please select job type." />    
                    </td>
                </tr>

                <tr>
                    <td>
                        Country *
                    </td>
                    <td>
                        <ext:ComboBox ID="cmbCountry" runat="server" Width="200" ValueField="CountryName" DisplayField="CountryName"
                         ForceSelection="true"
                        QueryMode="Local">
                        <Store>
                            <ext:Store ID="Store2" runat="server">
                                <Model>
                                    <ext:Model ID="Model4" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="CountryName" />
                                            <ext:ModelField Name="CountryName" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                    <asp:RequiredFieldValidator Display="None" ID="valcmbCountry" runat="server" ValidationGroup="SaSaveveED"
                        ControlToValidate="cmbCountry" ErrorMessage="Please select the Country." />

                         <%--<ext:TextField ID="txtCountry"  runat="server"
                                Width="200">
                            </ext:TextField>
                        <asp:RequiredFieldValidator Display="None" ID="rfvCountry" runat="server" ValidationGroup="Save"
                                ControlToValidate="txtCountry" ErrorMessage="Country is required." />           --%>              
                    </td>
                </tr>

                <tr>
                    <td>
                        Posted on *
                    </td>
                    <td>
                         <ext:TextField ID="txtPostedOn"  runat="server"
                                Width="200">
                            </ext:TextField>
                        <asp:RequiredFieldValidator Display="None" ID="rfvPostedOn" runat="server" ValidationGroup="Save"
                                ControlToValidate="txtPostedOn" ErrorMessage="Posted on is required." />       
                    </td>
                </tr>

                <tr>
                    <td>
                        Posted Date *
                    </td>
                    <td>
                        <pr:CalendarExtControl  Width="200" ID="calPostedDate" runat="server" 
                                    LabelSeparator=""/>
                                <asp:RequiredFieldValidator Display="None" ID="rfvcalPostedDate" runat="server" ValidationGroup="Save"
                                    ControlToValidate="calPostedDate" ErrorMessage="Posted date is required." />
                    </td>
                </tr>

               </table>
            </div>
        </div>
      

       <div class="widget">
            <div class="widget-head">
                <h4 class="heading">
                    Required Qualification</h4>
            </div>
            <div class="widget-body">
               <table class="fieldTable">
                <%--<tr>
                    <td>
                        Skill Set 
                    </td>
                    <td>
                        <ext:CheckboxGroup ID="chkBGSkillTest"  runat="server" ColumnsNumber="1" >
                           
                        </ext:CheckboxGroup>    

                                                        
                        
                    </td>
                </tr>--%>

                     <tr>
                    <td>
                        Skill Set
                    </td>
                    <td>
                         <ext:MultiCombo Width="300"  LabelSeparator="" SelectionMode="All" LabelWidth="50" ID="MultiCombo1" DisplayField="Name"
                                        ValueField="SkillSetId"  runat="server">
                                        <Store>
                                            <ext:Store ID="Store1" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model1" IDProperty="SkillSetId" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="SkillSetId" />
                                                            <ext:ModelField Name="Name" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <Listeners>
                                            
                                        </Listeners>
                                    </ext:MultiCombo>
                       

                    </td>
                </tr>

                <tr>
                    <td>
                         Work Experience *
                    </td>
                    <td>
                    
                       
                         <ext:TextField ID="txtWorkExperiencePeriod"  runat="server"
                            Width="60"  >                      
                        </ext:TextField>

                        <asp:CompareValidator SetFocusOnError="true" ValueToCompare="0" Display="None" Operator="DataTypeCheck"
                                    Type="Double" ID="cvWorkExperiencePeriod" ControlToValidate="txtWorkExperiencePeriod" ValidationGroup="Save"
                                    runat="server" ErrorMessage="Invalid work experience period."></asp:CompareValidator>
                        <asp:RequiredFieldValidator Display="None" ID="rfvWorkExperience" runat="server" ValidationGroup="Save"
                                ControlToValidate="txtWorkExperiencePeriod"  ErrorMessage="Work experience is required." />  
                                
                               
                     </td>

                      <td>
                          <ext:Label ID="lblYears" Text="Years" runat="server" Width="80"></ext:Label>
                         <%--<ext:TextField ID="txtWorkExperienceType" runat="server"
                                Width="80" >
                            </ext:TextField>
                         <asp:RequiredFieldValidator Display="None" ID="rfvWorkExperienceType" runat="server" ValidationGroup="Save"
                                ControlToValidate="txtWorkExperienceType"  ErrorMessage="Work experience type is required." /> --%>
                      </td>
                </tr>
               
                <tr>
                    <td>
                        Attach Document *
                    </td>
                    <td>
                        
                    <ext:FileUploadField ID="upload1" StyleSpec="cursor:pointer" runat="server" Text="Attach File" ButtonOnly="true"
                        Cls="btnFlat" BaseCls="btnFlat" Width="120px">                                         
                    </ext:FileUploadField>
                 

                    <asp:RequiredFieldValidator Display="None" ID="rfvAttachedFile" runat="server" ValidationGroup="Save"
                                ControlToValidate="upload1" ErrorMessage="Document is required." />   
                
                    </td>
                    <td>
                         <table>
                        <tr>
                            <td>
                                <ext:Label ID="lblUploadedFile" runat="server" Hidden="true">
                                </ext:Label>
                            </td>
                            <td>
                                <ext:LinkButton Icon="Delete" runat="server" ID="lnkDeleteFile" OnDirectClick="lnkDeleteFile_Click"
                                    Text="Delete file" Hidden="true">
                                    <DirectEvents>
                                        <Click OnEvent="lnkDeleteFile_Click">
                                         <Confirmation ConfirmRequest="true" Message="Do you want to delete the file?" />
                                            <EventMask ShowMask="true" />
                                        </Click>
                                    </DirectEvents>
                                </ext:LinkButton>
                            </td>
                        </tr>
                    </table>
                    </td>
                </tr>


                <tr>
                
                    <td>
                        Upper Salary Range *
                    </td>
                    <td>
                        <ext:TextField ID="txtUpperSalaryRange" ClientMode="static" LabelSeparator="" runat="server" FieldStyle="text-align:right;"
                                Width="120">
                      
                            </ext:TextField>
                          <asp:CompareValidator SetFocusOnError="true" ValueToCompare="0" Display="None" Operator="DataTypeCheck"
                                    Type="Currency" ID="cvUpperSalaryRange" ControlToValidate="txtUpperSalaryRange" ValidationGroup="Save"
                                    runat="server" ErrorMessage="Invalid upper salary range amount."></asp:CompareValidator>
                        <asp:RequiredFieldValidator Display="None" ID="rfvUpperSalaryRange" runat="server" ValidationGroup="Save"
                                ControlToValidate="txtUpperSalaryRange"  ErrorMessage="Upper salary range is required." />
                    </td>
                </tr>
                
                <tr>

                    <td>
                        <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="btnSave" ValidationGroup="Save" Cls="btnFlat" BaseCls="btnFlat" Text="<i></i>Save" >
                                    <DirectEvents>
                                        <Click OnEvent="btnSave_Click">
                                            <EventMask ShowMask="true" />
                                        </Click>
                                    </DirectEvents>
                                    <Listeners>
                                        <Click Handler="valGroup = 'Save'; return CheckValidation();">
                                        </Click>
                                    </Listeners>
                                </ext:LinkButton>
                    </td>
                    <td>

                    


                        
                    </td>
                </tr>

               

             </table>
            </div>
        </div>


    </div>


  


</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
