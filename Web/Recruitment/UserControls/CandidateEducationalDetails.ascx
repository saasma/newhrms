﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CandidateEducationalDetails.ascx.cs" Inherits="Web.Recruitment.UserControls.CandidateEducationalDetails" %>
<ext:Hidden runat="server" ID="hdnId">
</ext:Hidden>


  <div class="widget-body">
        <table class="fieldTable firsttdskip">
            <tr>
                <td>
                    <ext:GridPanel ID="GridEducationalDetails" runat="server" Width="950">
                        <Store>
                            <ext:Store ID="StoreEducationalDetails" runat="server">
                                <Model>
                                    <ext:Model ID="Model1" runat="server" IDProperty="EducationalDetailsID">
                                        <Fields>
                                            <ext:ModelField Name="LevelName" Type="string" />
                                            <ext:ModelField Name="Institution" Type="string" />
                                            <ext:ModelField Name="CountryName" Type="string" />
                                            <ext:ModelField Name="Place" Type="string" />
                                            <ext:ModelField Name="PassYear" Type="string" />
                                            <ext:ModelField Name="Division" Type="string" />
                                            <ext:ModelField Name="Percentage" Type="string" />
                                            <ext:ModelField Name="Grade" Type="string" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:Column ID="Column1" runat="server" Text="Level" Sortable="false" MenuDisabled="true" DataIndex="LevelName"
                                    Width="150" Flex="1" />
                                <ext:Column ID="Column2" runat="server" Sortable="false" MenuDisabled="true" Text="Institution" DataIndex="Institution"
                                    Width="200" />
                                <ext:Column ID="Column3" runat="server" Sortable="false" MenuDisabled="true" Text="Country" DataIndex="CountryName"
                                    Width="90" />
                                <ext:Column ID="Column4" runat="server" Sortable="false" MenuDisabled="true" Text="Place" DataIndex="Place"
                                    Width="120" />
                                <ext:Column ID="Column5" runat="server" Sortable="false" MenuDisabled="true" Text="PassYear" DataIndex="PassYear"
                                    Width="120" />
                                <ext:Column ID="Column6" runat="server" Sortable="false" MenuDisabled="true" Text="Division" DataIndex="Division"
                                    Width="70" />
                                <ext:Column ID="Column7" runat="server" Sortable="false" MenuDisabled="true" Text="Percentage" DataIndex="Percentage"
                                    Width="100" />
                                <ext:Column ID="Column8" runat="server" Sortable="false" MenuDisabled="true" Text="Grade" DataIndex="Grade"
                                    Width="100" />

                                <ext:CommandColumn ID="CommandColumn1" runat="server" Width="70">
                                    <Commands>
                                        <ext:GridCommand Text="<i></i>" Cls="btn-action glyphicons pencil btn-success" CommandName="Edit">
                                            <ToolTip Text="Edit" />
                                        </ext:GridCommand>
                                        <ext:CommandSeparator />
                                        <ext:GridCommand Text="<i></i>" Cls="btn-action glyphicons remove_2 btn-danger" CommandName="Delete">
                                            <ToolTip Text="Delete" />
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridEducationalDetails_Command">
                                            <EventMask ShowMask="true">
                                            </EventMask>
                                            <ExtraParams>
                                                <ext:Parameter Name="ID" Value="record.data.EducationalDetailsID" Mode="Raw">
                                                </ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw">
                                                </ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <SelectionModel>
                            <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                        </SelectionModel>
                    </ext:GridPanel>
                </td>
            </tr>
        </table>
        <div class="buttonBlock">
            <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="btnAddNewLine" Cls="btnFlat"
                BaseCls="btnFlat" Text="<i></i>Add New Educational Details">
                <DirectEvents>
                    <Click OnEvent="btnAddNewLine_Click">
                        <EventMask ShowMask="true" />
                    </Click>
                </DirectEvents>
            </ext:LinkButton>
        </div>
    </div>

<ext:Window ID="CEducationalDetailWindow" runat="server" Title="Add/Edit Educational Details" Icon="Application"
    Height="350" Width="600"  BodyPadding="10"
    Hidden="true" Modal="true">
    <Content>
        <table class="fieldTable">
        <tr>
            <td>
                
                <ext:ComboBox ID="cmbLevel" runat="server" ValueField="Value" DisplayField="Text" Width="150"
                        FieldLabel="Level" LabelAlign="top" LabelSeparator="" ForceSelection="true" QueryMode="Local">
                        <Items>
                            <ext:ListItem Text="Masters" Value="1" />
                            <ext:ListItem Text="Bachelor" Value="2" />
                            <ext:ListItem Text="MPhil" Value="3" />
                            <ext:ListItem Text="Intermediate / +2" Value="4" />
                            <ext:ListItem Text="School" Value="5" />
                            <ext:ListItem Text="Professional" Value="6" />
                            <ext:ListItem Text="Vocational" Value="7" />
                        </Items>
                       
                    </ext:ComboBox>
             
                 <asp:RequiredFieldValidator Display="None" ID="rfvLevel" runat="server" ValidationGroup="SaveED"
                                ControlToValidate="cmbLevel"  ErrorMessage="Please select Level." />
            </td>

            <td>
                <ext:TextField ID="txtInstitution" runat="server" FieldLabel="Institution"  Width="250"
                    LabelAlign="top" LabelSeparator="" />
                <asp:RequiredFieldValidator Display="None" ID="rfvInstitution" runat="server" ValidationGroup="SaveED"
                                ControlToValidate="txtInstitution"  ErrorMessage="Institution is required." />
            </td>
            </tr>
            <tr>
                <td style="padding-top: 10px; padding-bottom: 10px;">
                    <ext:ComboBox ID="cmbCountry" runat="server" ValueField="CountryName" DisplayField="CountryName"
                        FieldLabel="Country" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                        QueryMode="Local">
                        <Store>
                            <ext:Store ID="Store2" runat="server">
                                <Model>
                                    <ext:Model ID="Model4" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="CountryName" />
                                            <ext:ModelField Name="CountryName" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                    <asp:RequiredFieldValidator Display="None" ID="valcmbCountry" runat="server" ValidationGroup="SaveED"
                        ControlToValidate="cmbCountry" ErrorMessage="Please select the Country." />

                </td>
                <td>
                    <ext:TextField ID="txtPlace" runat="server" FieldLabel="Place"  Width="250"
                    LabelAlign="top" LabelSeparator="" />
                    <asp:RequiredFieldValidator Display="None" ID="rfvPlace" runat="server" ValidationGroup="SaveED"
                                ControlToValidate="txtPlace"  ErrorMessage="Place is required." />
                </td>
            </tr>
            <tr>
                <td>
                   <ext:TextField ID="txtPassYear" runat="server" FieldLabel="Pass Year"  Width="150"
                    LabelAlign="top" LabelSeparator="" />
                    <asp:RequiredFieldValidator Display="None" ID="rfvPassYear" runat="server" ValidationGroup="SaveED"
                                ControlToValidate="txtPassYear"  ErrorMessage="Pass Year is required." />
                    <asp:RangeValidator runat="server" ErrorMessage="Invalid pass year." ValidationGroup="SaveED"
                                        Type="Double" ID="rvPassYear" Display="None" MinimumValue="1900" MaximumValue="2200"
                                        ControlToValidate="txtPassYear"></asp:RangeValidator>
                </td>
                <td>
                      <ext:ComboBox ID="cmbDivision" runat="server" ValueField="Value" DisplayField="Text" Width="250"
                        FieldLabel="Division" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                        QueryMode="Local">
                        <Items>
                            <ext:ListItem Text="Distinction" Value="1" />
                            <ext:ListItem Text="First" Value="2" />
                            <ext:ListItem Text="Second" Value="3" />
                            <ext:ListItem Text="Third" Value="4" />
                            <ext:ListItem Text="Pass" Value="5" />
                            <ext:ListItem Text="Not Specified" Value="6" />
                        </Items>
                       
                    </ext:ComboBox>
                    <asp:RequiredFieldValidator Display="None" ID="valcmbDivision" runat="server" ValidationGroup="SaveED"
                        ControlToValidate="cmbDivision" ErrorMessage="Please select the Division." />

                </td>
            </tr>

            <tr>
                <td>
                   <ext:TextField ID="txtPercentage" runat="server" FieldLabel="Percentage"  Width="150"
                    LabelAlign="top" LabelSeparator="" />
                    <asp:RequiredFieldValidator Display="None" ID="rfvPercentage" runat="server" ValidationGroup="SaveED"
                                ControlToValidate="txtPercentage"  ErrorMessage="Percentage is required." />
                    <asp:RangeValidator runat="server" ErrorMessage="Invalid percent." ValidationGroup="SaveED"
                                        Type="Double" ID="rvPercentage" Display="None" MinimumValue="1" MaximumValue="100"
                                        ControlToValidate="txtPercentage"></asp:RangeValidator>
                </td>
                <td>
                    <ext:TextField ID="txtGrade" runat="server" FieldLabel="Grade"  Width="250"
                    LabelAlign="top" LabelSeparator="" />
                     <asp:RequiredFieldValidator Display="None" ID="rfvGrade" runat="server" ValidationGroup="SaveED"
                                ControlToValidate="txtGrade"  ErrorMessage="Grade is required." />
                </td>
            </tr>

           
            <tr>
                <td valign="bottom" colspan="2">
                    <div class="popupButtonDiv">
                        <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="btnSave" Cls="btnFlat"
                            BaseCls="btnFlat" Text="<i></i>Save">
                            <DirectEvents>
                                <Click OnEvent="btnSave_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                            <Listeners>
                                <Click Handler="valGroup = 'SaveED'; return CheckValidation();">
                                </Click>
                            </Listeners>
                        </ext:LinkButton>
                        <div class="btnFlatOr">
                            or
                        </div>
                        <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="LinkButton2" Cls="btnFlatLeftGap"
                            Text="<i></i>Cancel">
                            <Listeners>
                                <Click Handler="#{CEducationalDetailWindow}.hide();">
                                </Click>
                            </Listeners>
                        </ext:LinkButton>
                    </div>
                </td>
            </tr>
        </table>
    </Content>
</ext:Window>
