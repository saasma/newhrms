﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using System.Xml;

namespace Web.Recruitment.UserControls
{
    public partial class CandidateEmploymentDetails : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        private void Initialise()
        {
            ClearFields();
            LoadEmploymentDetail();
        }

        private void ClearFields()
        {
            cmbState.Value = 0;
            txtOrganization.Text = "";
            txtPlace.Text = "";
            txtPosition.Text = "";
            txtMajorResponisbility.Text = "";
            calFromDate.Text = "";
            calToDate.Text = "";
            hdnId.Text = "";
        }

        private int CandidateId()
        {
            int candidateId = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["candidateId"]))
            {
                candidateId = int.Parse(Request.QueryString["candidateId"]);
            }
            return candidateId;
        }

        private void LoadEmploymentDetail()
        {
            GridEmploymentDetail.GetStore().DataSource = RecruitmentManager.GetEmploymentDetails(CandidateId());
            GridEmploymentDetail.GetStore().DataBind();
        }

        protected void btnAddNewLine_Click(object sender, DirectEventArgs e)
        {
            ClearFields();
            CEmploymentDetailWindow.Show();
        }

        protected void GridTrainingDetail_Command(object sender, DirectEventArgs e)
        {
            string commandName = e.ExtraParams["command"];
            int employmentDetailId = int.Parse(e.ExtraParams["ID"]);
            hdnId.Value = employmentDetailId.ToString();

            switch (commandName)
            {
                case "Delete":
                    this.DeleteData(employmentDetailId);
                    break;

                case "Edit":
                    this.EditEducationalDetails(employmentDetailId);
                    break;
            }

        }

        protected void DeleteData(int employmentDetailId)
        {
            Status status = RecruitmentManager.DeleteEmploymentDetails(employmentDetailId);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Employment Details deleted.");
                LoadEmploymentDetail();
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }

        }

        private void EditEducationalDetails(int employmentDetailId)
        {
            ClearFields();
            hdnId.Text = Convert.ToString(employmentDetailId);
            CEmploymentDetail entity = RecruitmentManager.GetEmploymentDetailById(employmentDetailId);
            if (entity.State.ToString() == "Current")
                cmbState.Text = "1";
            else
                cmbState.Text = "2";

            txtOrganization.Text = entity.Organization.ToString();
            txtPlace.Text = entity.Place.ToString();
            txtPosition.Text = entity.Position.ToString();
            txtMajorResponisbility.Text = entity.MajorResponisbility.ToString();

            calFromDate.Text = entity.FromDate;
            calToDate.Text = entity.ToDate;
            CEmploymentDetailWindow.Show();
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveEmpDetl");
            if (Page.IsValid)
            {
                SaveData();
            }
        }

        private void SaveData()
        {
            CEmploymentDetail entity = new CEmploymentDetail();
            entity.CandidateID = CandidateId();

            if (!string.IsNullOrEmpty(hdnId.Text.Trim()))
                entity.EmploymentDetailsID = int.Parse(hdnId.Text.Trim());

            if (!string.IsNullOrEmpty(cmbState.SelectedItem.Value))
            {
                if (cmbState.SelectedItem.Value == "1")
                    entity.State = "Current";
                else
                    entity.State = "Previous";
            }

            if (!string.IsNullOrEmpty(txtOrganization.Text.Trim()))
                entity.Organization = txtOrganization.Text.Trim();

            if (!string.IsNullOrEmpty(txtPlace.Text.Trim()))
                entity.Place = txtPlace.Text.Trim();

            if (!string.IsNullOrEmpty(txtPosition.Text.Trim()))
                entity.Position = txtPosition.Text.Trim();

            if (!string.IsNullOrEmpty(txtMajorResponisbility.Text.Trim()))
                entity.MajorResponisbility = txtMajorResponisbility.Text.Trim();


            if (!string.IsNullOrEmpty(calFromDate.Text.Trim()))
            {
                entity.FromDate = calFromDate.Text.Trim();
                entity.FromDateEng = BLL.BaseBiz.GetEngDate(entity.FromDate, IsEnglish);
            }

            if (!string.IsNullOrEmpty(calToDate.Text.Trim()))
            {
                entity.ToDate = calToDate.Text.Trim();
                entity.ToDateEng = BLL.BaseBiz.GetEngDate(entity.ToDate, IsEnglish);
            }

            Status status = RecruitmentManager.InsertUpdateEmploymentDetails(entity);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Employment Details saved.");
                LoadEmploymentDetail();
                CEmploymentDetailWindow.Hide();
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }
    }
}