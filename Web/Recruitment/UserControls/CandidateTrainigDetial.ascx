﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CandidateTrainigDetial.ascx.cs" Inherits="Web.Recruitment.UserControls.CandidateTrainigDetial" %>

<ext:Hidden runat="server" ID="hdnId">
</ext:Hidden>


  <div class="widget-body">
        <table class="fieldTable firsttdskip">
            <tr>
                <td>
                    <ext:GridPanel ID="GridTrainingDetail" runat="server" Width="600">
                        <Store>
                            <ext:Store ID="StoreTrainigDetail" runat="server">
                                <Model>
                                    <ext:Model ID="Model1" runat="server" IDProperty="TrainingDetailsID">
                                        <Fields>
                                            <ext:ModelField Name="TrainingName" Type="string" />
                                            <ext:ModelField Name="Institution" Type="string" />
                                            <ext:ModelField Name="Duration" Type="string" />
                                            <ext:ModelField Name="Year" Type="string" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:Column ID="Column1" runat="server" Sortable="false" MenuDisabled="true" Text="Training Name" DataIndex="TrainingName"
                                    Width="150" Flex="1" />
                                <ext:Column ID="Column2" runat="server" Sortable="false" MenuDisabled="true" Text="Institution" DataIndex="Institution"
                                    Width="200" />
                                <ext:Column ID="Column3" runat="server" Sortable="false" MenuDisabled="true" Text="Duration" DataIndex="Duration"
                                    Width="70" />
                                <ext:Column ID="Column4" runat="server" Sortable="false" MenuDisabled="true" Text="Year" DataIndex="Year"
                                    Width="100" />
                                

                                <ext:CommandColumn ID="CommandColumn1" runat="server" Width="70">
                                    <Commands>
                                        <ext:GridCommand Text="<i></i>" Cls="btn-action glyphicons pencil btn-success" CommandName="Edit">
                                            <ToolTip Text="Edit" />
                                        </ext:GridCommand>
                                        <ext:CommandSeparator />
                                        <ext:GridCommand Text="<i></i>" Cls="btn-action glyphicons remove_2 btn-danger" CommandName="Delete">
                                            <ToolTip Text="Delete" />
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridTrainingDetail_Command">
                                            <EventMask ShowMask="true">
                                            </EventMask>
                                            <ExtraParams>
                                                <ext:Parameter Name="ID" Value="record.data.TrainingDetailsID" Mode="Raw">
                                                </ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw">
                                                </ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <SelectionModel>
                            <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                        </SelectionModel>
                    </ext:GridPanel>
                </td>
            </tr>
        </table>
        <div class="buttonBlock">
            <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="btnAddNewLine" Cls="btnFlat"
                BaseCls="btnFlat" Text="<i></i>Add New Training Details">
                <DirectEvents>
                    <Click OnEvent="btnAddNewLine_Click">
                        <EventMask ShowMask="true" />
                    </Click>
                </DirectEvents>
            </ext:LinkButton>
        </div>
    </div>

<ext:Window ID="CTrainingDetailWindow" runat="server" Title="Add/Edit Training Details" Icon="Application"
    Height="300" Width="600"  BodyPadding="10"
    Hidden="true" Modal="true">
    <Content>
        <table class="fieldTable">
            <td>
                <ext:TextField ID="txtTrainingName" runat="server" FieldLabel="Training Name"  Width="250"
                    LabelAlign="top" LabelSeparator="" />
                <asp:RequiredFieldValidator Display="None" ID="rfvTrainingName" runat="server" ValidationGroup="SaveTD"
                                ControlToValidate="txtTrainingName"  ErrorMessage="Training Name is required." />
            </td>
            <td>
                <ext:TextField ID="txtInstitution" runat="server" FieldLabel="Institution"  Width="250"
                    LabelAlign="top" LabelSeparator="" />
                <asp:RequiredFieldValidator Display="None" ID="rfvInstitution" runat="server" ValidationGroup="SaveTD"
                                ControlToValidate="txtInstitution"  ErrorMessage="Institution is required." />
            </td>
            </tr>
            <tr>
                <td style="padding-top: 10px; padding-bottom: 10px;">
                    <ext:TextField ID="txtDuration" runat="server" FieldLabel="Duration"  Width="100"
                    LabelAlign="top" LabelSeparator="" />
                    </br>
                    
                    <ext:ComboBox ID="cmbDurationType" runat="server" ValueField="Value" DisplayField="Text" Width="100"
                        ForceSelection="true" QueryMode="Local">
                        <Items>
                            <ext:ListItem Text="Year" Value="1" />
                            <ext:ListItem Text="Month" Value="2" />
                            <ext:ListItem Text="Week" Value="3" />
                            <ext:ListItem Text="Day" Value="4" />
                        </Items>
                       
                    </ext:ComboBox>

                    <asp:RequiredFieldValidator Display="None" ID="rfvDuration" runat="server" ValidationGroup="SaveTD"
                                ControlToValidate="txtDuration"  ErrorMessage="Duration is required." />
                    <asp:RequiredFieldValidator Display="None" ID="rfvDurationType" runat="server" ValidationGroup="SaveTD"
                                ControlToValidate="cmbDurationType"  ErrorMessage="Please select Duration Type." />
                </td>
                <td>
                    <ext:TextField ID="txtYear" runat="server" FieldLabel="Year"  Width="100"
                    LabelAlign="top" LabelSeparator="" />
                    <asp:RequiredFieldValidator Display="None" ID="rfvYear" runat="server" ValidationGroup="SaveTD"
                                ControlToValidate="txtYear"  ErrorMessage="Year is required." />
                    <asp:RangeValidator runat="server" ErrorMessage="Invalid year." ValidationGroup="SaveTD"
                                        Type="Double" ID="rvYear" Display="None" MinimumValue="1900" MaximumValue="2200"
                                        ControlToValidate="txtYear"></asp:RangeValidator>
                </td>
            </tr>
            
            <tr>
                <td valign="bottom" colspan="2">
                    <div class="popupButtonDiv">
                        <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="btnSave" Cls="btnFlat"
                            BaseCls="btnFlat" Text="<i></i>Save">
                            <DirectEvents>
                                <Click OnEvent="btnSave_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                            <Listeners>
                                <Click Handler="valGroup = 'SaveTD'; return CheckValidation();">
                                </Click>
                            </Listeners>
                        </ext:LinkButton>
                        <div class="btnFlatOr">
                            or
                        </div>
                        <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="LinkButton2" Cls="btnFlatLeftGap"
                            Text="<i></i>Cancel">
                            <Listeners>
                                <Click Handler="#{CTrainingDetailWindow}.hide();">
                                </Click>
                            </Listeners>
                        </ext:LinkButton>
                    </div>
                </td>
            </tr>
        </table>
    </Content>
</ext:Window>

