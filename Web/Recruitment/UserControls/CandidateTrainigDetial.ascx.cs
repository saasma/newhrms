﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using System.Xml;

namespace Web.Recruitment.UserControls
{
    public partial class CandidateTrainigDetial : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        private void Initialise()
        {
            LoadTrainingDetails();
        }

        private void ClearFields()
        {
            txtTrainingName.Text = "";
            txtInstitution.Text = "";
            txtDuration.Text = "";
            cmbDurationType.Value = "";
            txtYear.Text = "";
            hdnId.Text = "";
        }

        private int CandidateID()
        {
            int candidateId = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["candidateId"]))
            {
                candidateId = int.Parse(Request.QueryString["candidateId"]);
            }
            return candidateId;
        }

        private void LoadTrainingDetails()
        {
            GridTrainingDetail.GetStore().DataSource = RecruitmentManager.GetTrainingDetail(CandidateID());
            GridTrainingDetail.GetStore().DataBind();
        }

        protected void btnAddNewLine_Click(object sender, DirectEventArgs e)
        {
            ClearFields();
            CTrainingDetailWindow.Show();
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveTD");
            if (Page.IsValid)
            {
                SaveData();
            }
        }

        protected void GridTrainingDetail_Command(object sender, DirectEventArgs e)
        {
            string commandName = e.ExtraParams["command"];
            int trainingDetialsId = int.Parse(e.ExtraParams["ID"]);
            hdnId.Value = trainingDetialsId.ToString();

            switch (commandName)
            {
                case "Delete":
                    this.DeleteData(trainingDetialsId);
                    break;

                case "Edit":
                    this.EditEducationalDetails(trainingDetialsId);
                    break;
            }

        }

        protected void DeleteData(int trainingDetailId)
        {
            Status status = RecruitmentManager.DeleteCTrainingDetail(trainingDetailId);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Training Details deleted.");
                LoadTrainingDetails();
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        private void EditEducationalDetails(int trainingDetialsId)
        {
            hdnId.Text = Convert.ToString(trainingDetialsId);
            CTrainingDetial entity = RecruitmentManager.GetTrainingDetailById(trainingDetialsId);
            txtTrainingName.Text = entity.TrainingName.ToString();
            txtInstitution.Text = entity.Institution.ToString();

            string duration = entity.Duration.ToString();
            string[] dur = duration.Split(' ');
            txtDuration.Text = dur[0].ToString();

            if (dur[1].ToString() == "Year")
                cmbDurationType.SetValue(Convert.ToString(1));
            else if (dur[1].ToString() == "Month")
                cmbDurationType.SetValue(Convert.ToString(2));
            else if (dur[1].ToString() == "Week")
                cmbDurationType.SetValue(Convert.ToString(3));
            else if (dur[1].ToString() == "Day")
                cmbDurationType.SetValue(Convert.ToString(4));

            txtYear.Text = entity.Year.ToString();
            CTrainingDetailWindow.Show();
        }

        private void SaveData()
        {
            CTrainingDetial entity = new CTrainingDetial();
            entity.CandidateID = CandidateID();

            if(!string.IsNullOrEmpty(hdnId.Text.Trim()))
                entity.TrainingDetailsID = int.Parse(hdnId.Text.Trim());

            if(!string.IsNullOrEmpty(txtTrainingName.Text.Trim()))
                entity.TrainingName = txtTrainingName.Text.Trim();

            if(!string.IsNullOrEmpty(txtInstitution.Text.Trim()))
                entity.Institution = txtInstitution.Text.Trim();

            if (!string.IsNullOrEmpty(txtDuration.Text.Trim()))
                entity.Duration = txtDuration.Text.Trim() + " " + cmbDurationType.SelectedItem.Text.Trim();

            if (!string.IsNullOrEmpty(txtYear.Text.Trim()))
                entity.Year = txtYear.Text.Trim();

            Status status = RecruitmentManager.InsertUpdateTrainingDetail(entity);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Training Details saved.");
                LoadTrainingDetails();
                CTrainingDetailWindow.Hide();
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }
    }
}