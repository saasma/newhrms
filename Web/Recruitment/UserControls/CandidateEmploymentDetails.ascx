﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CandidateEmploymentDetails.ascx.cs" Inherits="Web.Recruitment.UserControls.CandidateEmploymentDetails" %>

<ext:Hidden runat="server" ID="hdnId">
</ext:Hidden>

  <div class="widget-body">
        <table class="fieldTable firsttdskip">
            <tr>
                <td>
                    <ext:GridPanel ID="GridEmploymentDetail" runat="server" Width="940">
                        <Store>
                            <ext:Store ID="StoreEmploymentDetails" runat="server">
                                <Model>
                                    <ext:Model ID="Model1" runat="server" IDProperty="EmploymentDetailsID">
                                        <Fields>
                                            <ext:ModelField Name="State" Type="string" />
                                            <ext:ModelField Name="Organization" Type="string" />
                                            <ext:ModelField Name="Place" Type="string" />
                                            <ext:ModelField Name="Position" Type="string" />
                                            <ext:ModelField Name="MajorResponisbility" Type="string" />
                                            <ext:ModelField Name="FromDate" Type="string" />
                                            <ext:ModelField Name="ToDate" Type="string" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:Column ID="Column1" runat="server" Sortable="false" MenuDisabled="true" Text="State" DataIndex="State"
                                    Width="150" Flex="1" />
                                <ext:Column ID="Column2" runat="server" Sortable="false" MenuDisabled="true" Text="Organization" DataIndex="Organization"
                                    Width="200" />
                                <ext:Column ID="Column3" runat="server" Sortable="false" MenuDisabled="true" Text="Place" DataIndex="Place"
                                    Width="70" />
                                <ext:Column ID="Column4" runat="server" Sortable="false" MenuDisabled="true" Text="Position" DataIndex="Position"
                                    Width="120" />
                                <ext:Column ID="Column5" runat="server" Sortable="false" MenuDisabled="true" Text="Major Responisbility" DataIndex="MajorResponisbility"
                                    Width="150" />
                                <ext:Column ID="Column6" runat="server" Sortable="false" MenuDisabled="true" Text="From" DataIndex="FromDate"
                                    Width="100" />
                                <ext:Column ID="Column7" runat="server" Sortable="false" MenuDisabled="true" Text="To" DataIndex="ToDate"
                                    Width="100" />
                                

                                <ext:CommandColumn ID="CommandColumn1" runat="server" Width="70">
                                    <Commands>
                                        <ext:GridCommand Text="<i></i>" Cls="btn-action glyphicons pencil btn-success" CommandName="Edit">
                                            <ToolTip Text="Edit" />
                                        </ext:GridCommand>
                                        <ext:CommandSeparator />
                                        <ext:GridCommand Text="<i></i>" Cls="btn-action glyphicons remove_2 btn-danger" CommandName="Delete">
                                            <ToolTip Text="Delete" />
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridTrainingDetail_Command">
                                            <EventMask ShowMask="true">
                                            </EventMask>
                                            <ExtraParams>
                                                <ext:Parameter Name="ID" Value="record.data.EmploymentDetailsID" Mode="Raw">
                                                </ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw">
                                                </ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <SelectionModel>
                            <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                        </SelectionModel>
                    </ext:GridPanel>
                </td>
            </tr>
        </table>
        <div class="buttonBlock">
            <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="btnAddNewLine" Cls="btnFlat"
                BaseCls="btnFlat" Text="<i></i>Add New EmploymentDetails">
                <DirectEvents>
                    <Click OnEvent="btnAddNewLine_Click">
                        <EventMask ShowMask="true" />
                    </Click>
                </DirectEvents>
            </ext:LinkButton>
        </div>
    </div>

<ext:Window ID="CEmploymentDetailWindow" runat="server" Title="Add/Edit Employment Details" Icon="Application"
    Height="350" Width="600"  BodyPadding="10"
    Hidden="true" Modal="true">
    <Content>
        <table class="fieldTable">
            <td>
                <ext:ComboBox ID="cmbState" runat="server" ValueField="Id" DisplayField="Name"  Width="250"
                         FieldLabel="State" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                        QueryMode="Local">
                        <Items>
                            <ext:ListItem Text="Current" Value="1" />
                            <ext:ListItem Text="Previous" Value="2" />
                        </Items>
                              
                </ext:ComboBox>
                <asp:RequiredFieldValidator Display="None" ID="rfvState" runat="server" ValidationGroup="SaveEmpDetl"
                    ControlToValidate="cmbState"  ErrorMessage="Please select state." />

            </td>
            <td>
                <ext:TextField ID="txtOrganization" runat="server" FieldLabel="Organization"  Width="250"
                    LabelAlign="top" LabelSeparator="" />
                <asp:RequiredFieldValidator Display="None" ID="rfvOrganization" runat="server" ValidationGroup="SaveEmpDetl"
                                ControlToValidate="txtOrganization"  ErrorMessage="Organization is required." />
            </td>
            </tr>
            <tr>
                <td style="padding-top: 10px; padding-bottom: 10px;">
                    <ext:TextField ID="txtPlace" runat="server" FieldLabel="Place"  Width="250"
                    LabelAlign="top" LabelSeparator="" />
                    <asp:RequiredFieldValidator Display="None" ID="rfvDuration" runat="server" ValidationGroup="SaveEmpDetl"
                                ControlToValidate="txtPlace"  ErrorMessage="Place is required." />
                </td>
                <td>
                    <ext:TextField ID="txtPosition" runat="server" FieldLabel="Position"  Width="250"
                    LabelAlign="top" LabelSeparator="" />
                    <asp:RequiredFieldValidator Display="None" ID="rfvPosition" runat="server" ValidationGroup="SaveEmpDetl"
                                ControlToValidate="txtPosition"  ErrorMessage="Position is required." />
                </td>
            </tr>

             <tr>
                <td style="padding-top: 10px; padding-bottom: 10px;">
                    <ext:TextField ID="txtMajorResponisbility" runat="server" FieldLabel="Major Responisbility"  Width="250"
                    LabelAlign="top" LabelSeparator="" />
                    <asp:RequiredFieldValidator Display="None" ID="rfvMajorResponisbility" runat="server" ValidationGroup="SaveEmpDetl"
                                ControlToValidate="txtMajorResponisbility"  ErrorMessage="Major Responisbility is required." />
                </td>
                <td>

                    <pr:CalendarExtControl FieldLabel="From" Width="250" ID="calFromDate" runat="server" 
                                    LabelSeparator="" LabelAlign="Top" />
                                <asp:RequiredFieldValidator Display="None" ID="valcalFromDate" runat="server" ValidationGroup="SaveEmpDetl"
                                    ControlToValidate="calFromDate" ErrorMessage="From Date is required." />

                </td>
            </tr>

            <tr>
                <td>

                    <pr:CalendarExtControl FieldLabel="To" Width="250" ID="calToDate" runat="server"
                                    LabelSeparator="" LabelAlign="Top" />
                                <asp:RequiredFieldValidator Display="None" ID="rfvcalToDate" runat="server" ValidationGroup="SaveEmpDetl"
                                    ControlToValidate="calToDate" ErrorMessage="To Date is required." />
             
                </td>
                <td></td>
            </tr>
            
            <tr>
                <td valign="bottom" colspan="2">
                    <div class="popupButtonDiv">
                        <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="btnSave" Cls="btnFlat"
                            BaseCls="btnFlat" Text="<i></i>Save">
                            <DirectEvents>
                                <Click OnEvent="btnSave_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                            <Listeners>
                                <Click Handler="valGroup = 'SaveEmpDetl'; return CheckValidation();">
                                </Click>
                            </Listeners>
                        </ext:LinkButton>
                        <div class="btnFlatOr">
                            or
                        </div>
                        <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="LinkButton2" Cls="btnFlatLeftGap"
                            Text="<i></i>Cancel">
                            <Listeners>
                                <Click Handler="#{CEmploymentDetailWindow}.hide();">
                                </Click>
                            </Listeners>
                        </ext:LinkButton>
                    </div>
                </td>
            </tr>
        </table>
    </Content>
</ext:Window>
