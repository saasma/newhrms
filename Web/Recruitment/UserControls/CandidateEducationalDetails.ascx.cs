﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using System.Xml;

namespace Web.Recruitment.UserControls
{
    public partial class CandidateEducationalDetails : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        private void Initialise()
        {
            CommonManager _CommonManager = new CommonManager();
            cmbCountry.Store[0].DataSource = _CommonManager.GetAllCountries();
            cmbCountry.Store[0].DataBind();

            LoadEducationalDetails();
        }

        private void ClearFields()
        {
            cmbLevel.Value = "";
            txtInstitution.Text = "";
            cmbCountry.Value = "";
            txtPlace.Text = "";
            txtPassYear.Text = "";
            cmbDivision.Value = "";
            txtPercentage.Text = "";
            txtGrade.Text = "";
            hdnId.Text = "";
        }

        private int CandidateID()
        {
            int candidateId = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["candidateId"]))
            {
                candidateId = int.Parse(Request.QueryString["candidateId"]);
            }
            return candidateId;
        }

        private void LoadEducationalDetails()
        {
            GridEducationalDetails.GetStore().DataSource = RecruitmentManager.GetEducationalDetails(CandidateID());
            GridEducationalDetails.GetStore().DataBind();
        }

        protected void btnAddNewLine_Click(object sender, DirectEventArgs e)
        {
            ClearFields();
            CEducationalDetailWindow.Show();
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveED");
            if (Page.IsValid)
            {
                SaveData();
            }
        }

        protected void GridEducationalDetails_Command(object sender, DirectEventArgs e)
        {
            string commandName = e.ExtraParams["command"];
            int educationalDetailId = int.Parse(e.ExtraParams["ID"]);
            hdnId.Value = educationalDetailId.ToString();

            switch (commandName)
            {
                case "Delete":
                    this.DeleteData(educationalDetailId);
                    break;

                case "Edit":
                    this.EditEducationalDetails(educationalDetailId);
                    break;
            }
             
        }

        protected void DeleteData(int ID)
        {
            Status status = new Status();
            status = RecruitmentManager.DeleteCEducationalDetails(ID);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Educational Details deleted.");
                LoadEducationalDetails();
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }

        }

        private void EditEducationalDetails(int educationalDetailId)
        {
            hdnId.Text = Convert.ToString(educationalDetailId);
            CEducationalDetail cEducationalDetial = RecruitmentManager.GetCEducationalDetailById(educationalDetailId);
            cmbLevel.SetValue(cEducationalDetial.LevelTypeId.ToString());
            txtInstitution.Text = cEducationalDetial.Institution.ToString();

            string countryName = RecruitmentManager.GetCountryNameUsingId(int.Parse(cEducationalDetial.CountryID.Value.ToString()));

            cmbCountry.SetValue(countryName);
            txtPlace.Text = cEducationalDetial.Place.ToString();
            txtPassYear.Text = cEducationalDetial.PassYear.ToString();
            cmbDivision.SetValue(cEducationalDetial.DivisionID.ToString());
            txtPercentage.Text = cEducationalDetial.Percentage.ToString();
            txtGrade.Text = cEducationalDetial.Grade.ToString();
            CEducationalDetailWindow.Show();            
        }

        private void SaveData()
        {
            CEducationalDetail entity = new CEducationalDetail();
            if (!string.IsNullOrEmpty(hdnId.Text.Trim()))
                entity.EducationalDetailsID = int.Parse(hdnId.Text.Trim());

            if (!string.IsNullOrEmpty(cmbLevel.SelectedItem.Text))
            {
                entity.LevelTypeId = int.Parse(cmbLevel.SelectedItem.Value);
                entity.LevelName = cmbLevel.SelectedItem.Text;
            }

            if (!string.IsNullOrEmpty(txtInstitution.Text.Trim()))
                entity.Institution = txtInstitution.Text.Trim();

            if (!string.IsNullOrEmpty(cmbCountry.SelectedItem.Text))
            {
                CountryList obj = RecruitmentManager.GetCountryUsingName(cmbCountry.SelectedItem.Text);
                entity.CountryID = obj.CountryId;

            }

            if (!string.IsNullOrEmpty(txtPlace.Text.Trim()))
                entity.Place = txtPlace.Text.Trim();

            if (!string.IsNullOrEmpty(txtPassYear.Text.Trim()))
                entity.PassYear = txtPassYear.Text.Trim();

            if (!string.IsNullOrEmpty(cmbDivision.SelectedItem.Value))
            {
                entity.DivisionID = int.Parse(cmbDivision.SelectedItem.Value.ToString());
                entity.DivisionName = cmbDivision.SelectedItem.Text;
            }

            if (!string.IsNullOrEmpty(txtPercentage.Text.Trim()))
                entity.Percentage = Convert.ToDouble(txtPercentage.Text.Trim());

            if (!string.IsNullOrEmpty(txtGrade.Text.Trim()))
                entity.Grade = txtGrade.Text.Trim();

            entity.CandidateID = CandidateID();

            Status status = RecruitmentManager.InsertUpdateCEducationalDetails(entity);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Educational Details saved.");
                LoadEducationalDetails();
                CEducationalDetailWindow.Hide();
                
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

    }
}