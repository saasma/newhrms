﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using System.Xml;

namespace Web.Recruitment
{
    public partial class JobOpeningList : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        private void Initialise()
        {
            LoadJobOpeningList();
        }

        private void LoadJobOpeningList()
        {
            GridJobOpeningList.GetStore().DataSource = RecruitmentManager.GetRecruitmentJobOpenings();
            GridJobOpeningList.GetStore().DataBind();
        }

        protected void btnDelete_Click(object sender, DirectEventArgs e)
        {
            int openingId = int.Parse(hiddenValue.Text.Trim());

            RecruitmentOpening recruitmentOpening = RecruitmentManager.GetRecruitmentOpeningById(openingId);
            string path = recruitmentOpening.AttachedDocumentUrl;

            Status status = RecruitmentManager.DeleteJobOpeningById(openingId);
            if (status.IsSuccess)
            {
                if (File.Exists(path))
                {
                    File.Delete(path);
                }

                LoadJobOpeningList();
                NewMessage.ShowNormalMessage("Job Opening deleted.");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        protected void btnEdit_Click(object sender, DirectEventArgs e)
        {
            int openingId = int.Parse(hiddenValue.Text.Trim());
            Response.Redirect("~/Recruitment/JobOpeningDetails.aspx?jobId=" + openingId.ToString());
        }

        protected void btnAdd_Click(object sender, DirectEventArgs e)
        {
            Response.Redirect("~/Recruitment/JobOpeningDetails.aspx");
        }

    }
}