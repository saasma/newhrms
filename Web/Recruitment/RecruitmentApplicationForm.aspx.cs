﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using System.Xml;

namespace Web.Recruitment
{
    public partial class RecruitmentApplicationForm : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();

                divHide.Visible = false;

                if (Request.QueryString["candidateId"] != null)
                {
                    hdnValue.Text = Request.QueryString["candidateId"].ToString();
                    LoadCandidate();
                    divHide.Visible = true;
                }
            }
        }

        private void Initialise()
        {
            ClearFields();

            List<SkillSet> skillSets = RecruitmentManager.GetSkillTests();
            MultiCombo1.Store[0].DataSource = skillSets;
            MultiCombo1.Store[0].DataBind();
        }

        private void ClearFields()
        {
            hdnValue.Text = "";
            txtCandidateID.Text = "";
            txtFirstName.Text = "";
            txtMiddleName.Text = "";
            txtLastName.Text = "";
            txtContactAddress.Text = "";
            txtEmailID.Text = "";
            txtContactNumber.Text = "";
            txtCreateOn.Text = "";
            txtSource.Text = "";
            txtFatherName.Text = "";
            txtMotherName.Text = "";
            txtCitizenshipNo.Text = "";
            txtIssuedFrom.Text = "";
            MultiCombo1.Text = "";
            lblUploadedFile.Text = "";
            lnkDeleteFile.Hide();
            txtCurrentSalary.Text = "";
            txtExpectedSalary.Text = "";
            txtAdditionalInfo.Text = "";
        }

        private void LoadCandidate()
        {
            Candidate entity = RecruitmentManager.GetCandidateByCandidateId(int.Parse(hdnValue.Text.Trim()));
            txtFirstName.Text = entity.FirstName.ToString();
            if (!string.IsNullOrEmpty(entity.MiddleName))
                txtMiddleName.Text = entity.MiddleName.ToString();
            txtLastName.Text = entity.LastName.ToString();
            txtContactAddress.Text = entity.ContactAddress.ToString();
            txtEmailID.Text = entity.EmailID.ToString();
            txtContactNumber.Text = entity.ContactNumber.ToString();
            txtCreateOn.Text = DateTime.Parse(entity.CreatedOnDate.ToString()).ToString("yyyy/MM/dd");
            txtSource.Text = entity.Source.ToString();
            txtFatherName.Text = entity.FatherName.ToString();
            txtMotherName.Text = entity.MotherName.ToString();
            txtCitizenshipNo.Text = entity.CitizenshipNo.ToString();
            txtIssuedFrom.Text = entity.IssuedFrom.ToString();

            List<CSkillSet> list = RecruitmentManager.GetCandidateSkillSet(int.Parse(hdnValue.Text.Trim()));
            foreach (CSkillSet item in list)
            {
                MultiCombo1.SelectedItems.Add(new Ext.Net.ListItem(item.SkillSetId.ToString()));
            }

            txtCurrentSalary.Text = Convert.ToDecimal(entity.CurrentSalary.ToString()).ToString("##0.00");
            txtExpectedSalary.Text = Convert.ToDecimal(entity.ExpectedSalary.ToString()).ToString("##0.00");
            txtAdditionalInfo.Text = entity.AdditionalInfo.ToString();

            if (!string.IsNullOrEmpty(entity.AttachedResumeName))
            {
                lblUploadedFile.Text = entity.AttachedResumeName;
                lblUploadedFile.Show();
                lnkDeleteFile.Show();
            }
            else
            {
                lblUploadedFile.Text = "";
                lnkDeleteFile.Hide();
            }

            txtCandidateID.Text = entity.CandidateID.ToString();
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            if (Request.QueryString["candidateId"] != null)
            {
                if (lblUploadedFile.Text == "" && !upload1.HasFile)
                {
                    upload1.Focus();
                    NewMessage.ShowWarningMessage("Resume is required to attach.");
                    return;
                }
            }

            if (!upload1.HasFile && Request.QueryString["candidateId"] == null)
            {
                upload1.Focus();
                NewMessage.ShowWarningMessage("Resume is required to attach.");
                return;
            }

            SaveUpdate();
        }

        private void SaveUpdate()
        {
            Candidate entity = new Candidate();
            if (!string.IsNullOrEmpty(txtFirstName.Text.Trim()))
                entity.FirstName = txtFirstName.Text.Trim();

            if (!string.IsNullOrEmpty(txtMiddleName.Text.Trim()))
                entity.MiddleName = txtMiddleName.Text.Trim();

            if (!string.IsNullOrEmpty(txtLastName.Text.Trim()))
                entity.LastName = txtLastName.Text.Trim();

            if (!string.IsNullOrEmpty(txtContactAddress.Text.Trim()))
                entity.ContactAddress = txtContactAddress.Text.Trim();

            if (!string.IsNullOrEmpty(txtEmailID.Text.Trim()))
                entity.EmailID = txtEmailID.Text.Trim();

            if (!string.IsNullOrEmpty(txtContactNumber.Text.Trim()))
                entity.ContactNumber = txtContactNumber.Text.Trim();

           
            if(Request.QueryString["candidateId"] != null)
            {
                Candidate obj = RecruitmentManager.GetCandidateByCandidateId(int.Parse(hdnValue.Text.Trim()));
                entity.CreatedOnDate = obj.CreatedOnDate;
            }
            else
                entity.CreatedOnDate = System.DateTime.Now;

            if (!string.IsNullOrEmpty(txtSource.Text.Trim()))
                entity.Source = txtSource.Text.Trim();

            if (!string.IsNullOrEmpty(txtFatherName.Text.Trim()))
                entity.FatherName = txtFatherName.Text.Trim();

            if (!string.IsNullOrEmpty(txtMotherName.Text.Trim()))
                entity.MotherName = txtMotherName.Text.Trim();

            if (!string.IsNullOrEmpty(txtCitizenshipNo.Text.Trim()))
                entity.CitizenshipNo = txtCitizenshipNo.Text.Trim();

            if (!string.IsNullOrEmpty(txtIssuedFrom.Text.Trim()))
                entity.IssuedFrom = txtIssuedFrom.Text.Trim();

            if (!string.IsNullOrEmpty(txtCurrentSalary.Text.Trim()))
                entity.CurrentSalary = Convert.ToDecimal(txtCurrentSalary.Text.Trim());

            if (!string.IsNullOrEmpty(txtExpectedSalary.Text.Trim()))
                entity.ExpectedSalary = Convert.ToDecimal(txtExpectedSalary.Text.Trim());

            if (Request.QueryString["candidateId"] != null && lblUploadedFile.Text != "")
            {
                Candidate obj = RecruitmentManager.GetCandidateById(int.Parse(hdnValue.Text));
                entity.AttachedResumeName = obj.AttachedResumeName;
                entity.AttachedResumeUrl = obj.AttachedResumeUrl;
            }
            else
            {
                if (upload1.HasFile)
                {
                    entity.AttachedResumeName = upload1.PostedFile.FileName;

                    // file upload section
                    string userFileName = entity.AttachedResumeName;
                    string serverFileName = Guid.NewGuid().ToString() + Path.GetExtension(userFileName);
                    string absolutePath = Server.MapPath(@"~/Uploads/" + serverFileName);
                    upload1.PostedFile.SaveAs(absolutePath);
                    entity.AttachedResumeUrl = absolutePath;
                }
            }

            if (!string.IsNullOrEmpty(txtAdditionalInfo.Text.Trim()))
                entity.AdditionalInfo = txtAdditionalInfo.Text.Trim();

            List<CSkillSet> cSkillSetList = new List<CSkillSet>();
            foreach (Ext.Net.ListItem item in MultiCombo1.SelectedItems)
            {

                CSkillSet obj = new CSkillSet();
                obj.SkillSetId = int.Parse(item.Value.ToString());
                cSkillSetList.Add(obj); 
            }

            if (!string.IsNullOrEmpty(hdnValue.Text.Trim()))
                entity.CandidateID = int.Parse(hdnValue.Text.Trim());

            Status status = RecruitmentManager.InsertUpdateRecruitmentCandidate(entity, cSkillSetList);
            if (status.IsSuccess)
            {
                
                if (string.IsNullOrEmpty(hdnValue.Text))
                {
                    Candidate objCandidate = RecruitmentManager.GetRecruitmentCandidate(txtCitizenshipNo.Text.Trim());
                    Response.Redirect("~/Recruitment/RecruitmentApplicationForm.aspx?candidateId=" + objCandidate.CandidateID.ToString());
                }
                ClearFields();
                Response.Redirect("~/Recruitment/RecruitmentCandidateList.aspx");
                //NewMessage.ShowNormalMessage("Candidate saved successfully.");

            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }


        protected void lnkDeleteFile_Click(object sender, DirectEventArgs e)
        {
            int candidateId = int.Parse(hdnValue.Text.Trim());
            Status status = new Status();

            Candidate entity = RecruitmentManager.GetCandidateById(candidateId);
            string path = entity.AttachedResumeUrl;
            status = RecruitmentManager.DeleteApplicationFormResume(candidateId);
            if (status.IsSuccess)
            {
                if (File.Exists(path))
                {
                    File.Delete(path);
                    lblUploadedFile.Text = "";
                    lnkDeleteFile.Hide();
                    lblUploadedFile.Hide();
                    NewMessage.ShowNormalMessage("Attached Resume deleted successfully.");
                }
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }
    }
}