﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using System.Xml;
namespace Web.Recruitment
{
    public partial class JobOpeningDetails : BasePage
    {
        //protected void Page_Init(object sender, EventArgs e)
        //{

        //    List<SkillSet> skillSets = RecruitmentManager.GetSkillTests();
        //    int i = 0;
        //    foreach (SkillSet item in skillSets)
        //    {
        //        Checkbox chk = new Checkbox();
        //        chk.ID = "chk" + i.ToString();
        //        chk.FieldLabel = item.Name;
        //        chk.Value = item.SkillSetId;
        //        chk.LabelWidth.Equals(200);
        //        chkBGSkillTest.Items.Add(chk);
        //        i++;
        //    }
        //}

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();

                if (Request.QueryString["jobId"] != null)
                {
                    hdnValue.Text = Request.QueryString["jobId"].ToString();
                    LoadJobOpeningData();
                }
            }
         
        }

        public void Initialise()
        {
            cmbBranch.Store[0].DataSource = RecruitmentManager.GetBranches();
            cmbBranch.Store[0].DataBind();

            cmbDepartment.Store[0].DataSource = RecruitmentManager.GetDepartments();
            cmbDepartment.Store[0].DataBind();

            cmbDesignation.Store[0].DataSource = RecruitmentManager.GetEDesignations();
            cmbDesignation.Store[0].DataBind();

            cmbHiringManager.Store[0].DataSource = RecruitmentManager.GetRecruitmentHiringManagers();
            cmbHiringManager.Store[0].DataBind();

            cmbJobType.Store[0].DataSource = RecruitmentManager.GetRecruitmentJobTypes();
            cmbJobType.Store[0].DataBind();

            CommonManager _CommonManager = new CommonManager();
            cmbCountry.Store[0].DataSource = _CommonManager.GetAllCountries();
            cmbCountry.Store[0].DataBind();

            List<SkillSet> skillSets = RecruitmentManager.GetSkillTests();
            MultiCombo1.Store[0].DataSource = skillSets;
            MultiCombo1.Store[0].DataBind();

        }

        private void LoadJobOpeningData()
        {
            int openingId = int.Parse(hdnValue.Text);
            RecruitmentOpening recruitmentOpening = RecruitmentManager.GetRecruitmentOpeningById(openingId);
            txtJobOpeningID.Text = recruitmentOpening.OpeningID.ToString();
            cmbBranch.SelectedItem.Value = recruitmentOpening.BranchID.ToString();
            cmbDepartment.SelectedItem.Value = recruitmentOpening.DepartmentID.ToString();
            txtLocation.Text = recruitmentOpening.Location;

            cmbDesignation.SelectedItem.Value = Convert.ToString(recruitmentOpening.DesignationID);
            txtEditorDescription.Text = recruitmentOpening.JobDescription;
            cmbHiringManager.SelectedItem.Value = recruitmentOpening.HiringManagerID.ToString();
            txtAssignedRecruiter.Text = recruitmentOpening.AssingedRecruiter;

            calDateOpened.Text = recruitmentOpening.DateOpened;
            calLastDateOfSub.Text = recruitmentOpening.LastDateOfSubmission;

            txtNoOfPositions.Text = recruitmentOpening.NumberOfPositions.ToString();
            cmbJobType.SelectedItem.Value = recruitmentOpening.JobTypeID.ToString();

            string CountryName = RecruitmentManager.GetCountryNameUsingId(int.Parse(recruitmentOpening.CountryID.Value.ToString()));
            cmbCountry.SelectedItem.Value = CountryName;
            txtPostedOn.Text = recruitmentOpening.PostedOn;

            calPostedDate.Text = recruitmentOpening.PostedDate;

            txtWorkExperiencePeriod.Text = recruitmentOpening.WorkExperienceYears.ToString();

            if (!string.IsNullOrEmpty(recruitmentOpening.AttachedDocumentName))
            {
                lblUploadedFile.Text = recruitmentOpening.AttachedDocumentName;
                lblUploadedFile.Show();
                lnkDeleteFile.Show();
            }
            else
            {
                lblUploadedFile.Text = "";
                lnkDeleteFile.Hide();
            }

            txtUpperSalaryRange.Text = Convert.ToDecimal(recruitmentOpening.UpperSalaryRange.ToString()).ToString("##0.00");

            List<RecruitmentOpeningSkillSet> skillSets = RecruitmentManager.GetRecruitmentOpeningSkills(int.Parse(hdnValue.Text.Trim()));
            foreach (RecruitmentOpeningSkillSet sk in skillSets)
            {
                string a = sk.SkillSetID.ToString();
                MultiCombo1.SelectedItems.Add(new Ext.Net.ListItem (a));
            }
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            if (Request.QueryString["jobId"] != null)
            {
                if (lblUploadedFile.Text == "" && !upload1.HasFile)
                {
                    upload1.Focus();
                    NewMessage.ShowWarningMessage("Document is required to be attached.");
                    return;
                }
            }

            if (!upload1.HasFile && Request.QueryString["jobId"] == null)
            {
                upload1.Focus();
                NewMessage.ShowWarningMessage("Document is required to be attached.");
                return;
            }

            //Page.Validate("Save");
            //if (Page.IsValid)
            //{
            //    InsertData();
            //}
            InsertData();
        }

        private void InsertData()
        {
            RecruitmentOpening recruitmentOpening = new RecruitmentOpening();

            if (!string.IsNullOrEmpty(hdnValue.Text.Trim()))
                recruitmentOpening.OpeningID = int.Parse(hdnValue.Text);

            recruitmentOpening.BranchID = int.Parse(cmbBranch.SelectedItem.Value);

            if (!string.IsNullOrEmpty(cmbDepartment.SelectedItem.Value))
                recruitmentOpening.DepartmentID = int.Parse(cmbDepartment.SelectedItem.Value);

            recruitmentOpening.Location = txtLocation.Text.Trim();

            if (!string.IsNullOrEmpty(cmbDesignation.SelectedItem.Value))
                recruitmentOpening.DesignationID = int.Parse(cmbDesignation.SelectedItem.Value);

            if (!string.IsNullOrEmpty(txtEditorDescription.Text))
                recruitmentOpening.JobDescription = txtEditorDescription.Text;

            if (!string.IsNullOrEmpty(cmbHiringManager.SelectedItem.Value))
                recruitmentOpening.HiringManagerID = int.Parse(cmbHiringManager.SelectedItem.Value);

            if(!string.IsNullOrEmpty(txtAssignedRecruiter.Text))
                recruitmentOpening.AssingedRecruiter = txtAssignedRecruiter.Text.Trim();

            if (!string.IsNullOrEmpty(calDateOpened.Text.Trim()))
            {
                recruitmentOpening.DateOpened = calDateOpened.Text.Trim();
                recruitmentOpening.DateOpenedEng = BLL.BaseBiz.GetEngDate(recruitmentOpening.DateOpened, IsEnglish);
            }

            if (!string.IsNullOrEmpty(calLastDateOfSub.Text))
            {
                recruitmentOpening.LastDateOfSubmission = calLastDateOfSub.Text.Trim();
                recruitmentOpening.LastDateOfSubmissionEng = BLL.BaseBiz.GetEngDate(recruitmentOpening.LastDateOfSubmission, IsEnglish);
            }


            if (!string.IsNullOrEmpty(txtNoOfPositions.Text))
                recruitmentOpening.NumberOfPositions = int.Parse(txtNoOfPositions.Text);

            if (!string.IsNullOrEmpty(cmbJobType.SelectedItem.Value))
                recruitmentOpening.JobTypeID = int.Parse(cmbJobType.SelectedItem.Value);


            if (!string.IsNullOrEmpty(cmbCountry.SelectedItem.Text))
            {
                CountryList obj = RecruitmentManager.GetCountryUsingName(cmbCountry.SelectedItem.Text);
                recruitmentOpening.CountryID = obj.CountryId;

            }

            if (txtPostedOn.Text != "")
                recruitmentOpening.PostedOn = txtPostedOn.Text.Trim();

            if (!string.IsNullOrEmpty(calPostedDate.Text))
            {
                recruitmentOpening.PostedDate = calPostedDate.Text.Trim();
                recruitmentOpening.PostedDateEng = BLL.BaseBiz.GetEngDate(recruitmentOpening.PostedDate, IsEnglish);
            }
            
            if (txtWorkExperiencePeriod.Text.Trim() != "")
                recruitmentOpening.WorkExperienceYears = float.Parse(txtWorkExperiencePeriod.Text);

            if (txtUpperSalaryRange.Text.Trim() != "")
                recruitmentOpening.UpperSalaryRange = decimal.Parse(txtUpperSalaryRange.Text);

            if (Request.QueryString["jobId"] != null && lblUploadedFile.Text != "")
            {
                RecruitmentOpening obj = RecruitmentManager.GetRecruitmentOpeningById(int.Parse(hdnValue.Text));
                recruitmentOpening.AttachedDocumentName = obj.AttachedDocumentName;
                recruitmentOpening.AttachedDocumentUrl = obj.AttachedDocumentUrl;
            }
            else
            {
                if (upload1.HasFile)
                {
                    recruitmentOpening.AttachedDocumentName = upload1.PostedFile.FileName;

                    // file upload section
                    string userFileName = recruitmentOpening.AttachedDocumentName;
                    string serverFileName = Guid.NewGuid().ToString() + Path.GetExtension(userFileName);
                    string absolutePath = Server.MapPath(@"~/Uploads/" + serverFileName);
                    upload1.PostedFile.SaveAs(absolutePath);
                    recruitmentOpening.AttachedDocumentUrl = absolutePath;
                }
            }

            List<RecruitmentOpeningSkillSet> list = new List<RecruitmentOpeningSkillSet>();
            foreach (Ext.Net.ListItem v in MultiCombo1.SelectedItems)
            {
                RecruitmentOpeningSkillSet sk = new RecruitmentOpeningSkillSet();
                sk.SkillSetID = int.Parse(v.Value.ToString());
                list.Add(sk);
            }

            Status status = RecruitmentManager.InsertUpdateRecruitmentOpeningDetails(recruitmentOpening, list);
            if (status.IsSuccess)
            {
                ClearFields();
                NewMessage.ShowNormalMessage("Job Opening Detail Saved.");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
           

        }

        private void ClearFields()
        {
            txtJobOpeningID.Text = "";
            cmbBranch.Value = 0;
            cmbDepartment.Value = 0;
            txtLocation.Text = "";
            cmbDesignation.Value = 0;      
            txtEditorDescription.Text = "";
            cmbHiringManager.Value = 0;
            txtAssignedRecruiter.Text = "";
            calDateOpened.Text = "";
            calLastDateOfSub.Text = "";
            txtNoOfPositions.Clear();
            cmbJobType.Value = 0;
            txtPostedOn.Text = "";
            calPostedDate.Text = "";
            //chkBGSkillTest
            txtWorkExperiencePeriod.Text = "";
            //txtWorkExperienceType.Text = "";
            upload1.Clear();
            txtUpperSalaryRange.Text = "";
            MultiCombo1.Text = "";

            foreach (Ext.Net.ListItem v in MultiCombo1.SelectedItems)
            {
                v.State.Equals(false);
            }
            cmbCountry.Value = "";
            lblUploadedFile.Hide();
            lblUploadedFile.Text = "";
            lnkDeleteFile.Hide();
        }

        protected void lnkDeleteFile_Click(object sender, DirectEventArgs e)
        {
            int id = int.Parse(hdnValue.Text);
            Status status = new Status();

            RecruitmentOpening recruitmentOpening = RecruitmentManager.GetRecruitmentOpeningById(id);
            string path = recruitmentOpening.AttachedDocumentUrl;
            status = RecruitmentManager.DeleteJobOpeningAttachedFile(id);
            if (status.IsSuccess)
            {
                if (File.Exists(path))
                {
                    File.Delete(path);
                    lblUploadedFile.Text = "";
                    lnkDeleteFile.Hide();
                    lblUploadedFile.Hide();
                    NewMessage.ShowNormalMessage("File deleted successfully.");
                }
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }

        }
    }
}