﻿<%@ Page Title="Promotion Listing" Language="C#" MasterPageFile="~/Master/NewDetails.master"
    AutoEventWireup="true" CodeBehind="PromotionListReport.aspx.cs" Inherits="Web.NewHR.PromotionListReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
        #content
        {
            margin: inherit;
        }
    </style>
    <script type="text/javascript">
        function renderArrear(value) {
            if (value == true)
                return 'Yes'
            else 
                return  '';
        }

         function searchList() {
            <%=PagingToolbar1.ClientID %>.doRefresh();
        }


         var CommandHandler1 = function(command, record){
            <%= hiddenValueRow.ClientID %>.setValue(record.data.DetailId);
            
                if(command=="Edit")
                {
                       <%= btnEdit.ClientID %>.fireEvent('click');
                }
                else
                {
                
                    <%= btnDeleteLevel.ClientID %>.fireEvent('click');
                }

             }


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <ext:Hidden runat="server" ID="hdn">
    </ext:Hidden>
    <ext:Hidden runat="server" ID="hiddenValueRow" />
    <ext:Button ID="btnDeleteLevel" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnDeleteLevel_Click">
                <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the Promotion History?" />
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>
    <ext:Button ID="btnEdit" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnEdit_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Promotion List History
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel" style="padding-top: 0px">
        <ext:Label ID="lblMsg" runat="server" />
        <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
            <Proxy>
                <ext:AjaxProxy Url="../Handler/EmpSearch.ashx">
                    <ActionMethods Read="GET" />
                    <Reader>
                        <ext:JsonReader Root="plants" TotalProperty="total" />
                    </Reader>
                </ext:AjaxProxy>
            </Proxy>
            <Model>
                <ext:Model ID="Model2" IDProperty="EmployeeId" Name="ItemLineModel" runat="server">
                    <Fields>
                        <ext:ModelField Name="Name" Type="String" />
                        <ext:ModelField Name="EmployeeId" Type="String" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
        <div class="alert alert-info">
            <table class="fieldTable firsttdskip">
                <tr>
                    <td>
                        <pr:CalendarExtControl Width="100px" FieldLabel="From Date" ID="calFromDate" runat="server"
                            LabelAlign="Top" LabelSeparator="" />
                    </td>
                    <td>
                        <pr:CalendarExtControl Width="100px" FieldLabel="To Date" ID="calToDate" runat="server"
                            LabelAlign="Top" LabelSeparator="" />
                    </td>
                    <td>
                         <ext:Store ID="StoreNames" runat="server">
        <Model>
            <ext:Model ID="Model5" runat="server" IDProperty="LevelGradeChangeId">
                <Fields>
                    <ext:ModelField Name="LevelGradeChangeId" Type="String" />
                    <ext:ModelField Name="Name" />
                </Fields>
            </ext:Model>
        </Model>
    </ext:Store>
                        <ext:ComboBox ID="cmbName" LabelSeparator="" FieldLabel="Name" LabelWidth="70" LabelAlign="Top"
                            runat="server"  Width="300" TriggerAction="All"
                            ValueField="LevelGradeChangeId"
                                DisplayField="Name" StoreID="StoreNames" 
                            >
                           
                        </ext:ComboBox>
                    </td>
                    <td>
                        <ext:ComboBox ID="cmbType" LabelSeparator="" FieldLabel="Type" LabelWidth="70" LabelAlign="Top"
                            runat="server" TypeAhead="false" Width="180" TriggerAction="All" ForceSelection="true">
                            <Items>
                                <ext:ListItem Text="All" Value="All" />
                                <ext:ListItem Text="Level Change(Promotion) Only" Value="true" />
                                <ext:ListItem Text="Grade Change Only" Value="false" />
                            </Items>
                        </ext:ComboBox>
                    </td>
                    <td>
                        <ext:ComboBox ID="cmbSearch" LabelSeparator="" FieldLabel="Search Employee" LabelWidth="70"
                            LabelAlign="Top" runat="server" DisplayField="Name" ValueField="EmployeeId" StoreID="storeSearch"
                            TypeAhead="false" Width="180" PageSize="9999" HideBaseTrigger="true" MinChars="1"
                            TriggerAction="All" ForceSelection="true">
                            <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                                <ItemTpl ID="ItemTpl1" runat="server">
                                    <Html>
                                        <tpl>
                                            <div class="search-item">
                                                            <span>{Name}</span>  
                                             </div>
					                </tpl>
                                    </Html>
                                </ItemTpl>
                            </ListConfig>
                            <Triggers>
                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                            </Triggers>
                            <Listeners>
                                <Select Handler="this.getTrigger(0).show();" />
                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                            </Listeners>
                        </ext:ComboBox>
                    </td>
                    <td style="padding-top: 15px;">
                        <ext:Button runat="server" ID="btnLoad" Cls="btn btn-primary" Text="<i></i>Load"
                            runat="server">
                            <DirectEvents>
                                <Click OnEvent="btnLoad_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                    </td>
                    <td style="padding-top: 15px;">
                        <ext:Button runat="server" AutoPostBack="true" OnClick="btnExport_Click" ID="btnAdd"
                            Cls="btn btn-primary" Text="<i></i>Export" runat="server">
                        </ext:Button>
                    </td>
                     <td>
                            <asp:LinkButton ID="btnExport" runat="server" Text="History import" OnClientClick="taxImportPopup();return false;"
                                CssClass=" excel marginRight tiptip" Style="float: left;" />
                        </td>
                </tr>
            </table>
        </div>
        <div style="clear: both; margin-top: 15px;">
        </div>
        <ext:GridPanel ID="gridLevelGradeHistory" runat="server" Cls="itemgrid">
            <Store>
                <ext:Store ID="StorePreviousEmployment" runat="server" AutoLoad="true" PageSize="50">
                    <Proxy>
                        <ext:AjaxProxy Json="true" Url="../Handler/PromotionListReportHandler.ashx">
                            <ActionMethods Read="GET" />
                            <Reader>
                                <ext:JsonReader Root="data" TotalProperty="total" />
                            </Reader>
                        </ext:AjaxProxy>
                    </Proxy>
                    <AutoLoadParams>
                        <ext:Parameter Name="start" Value="0" Mode="Raw" />
                    </AutoLoadParams>
                    <Parameters>
                        <ext:StoreParameter Name="fromDate" Value="#{calFromDate}.getValue()" Mode="Raw"
                            ApplyMode="Always" />
                        <ext:StoreParameter Name="toDate" Value="#{calToDate}.getValue()" Mode="Raw" ApplyMode="Always" />
                        <ext:StoreParameter Name="empId" Value="#{cmbSearch}.getValue()" Mode="Raw" ApplyMode="Always" />
                        <ext:StoreParameter Name="type" Value="#{cmbType}.getValue()" Mode="Raw" ApplyMode="Always" />
                         <ext:StoreParameter Name="LevelGradeChangeId" Value="#{cmbName}.getValue()" Mode="Raw" ApplyMode="Always" />
                    </Parameters>
                    <Model>
                        <ext:Model ID="Model1" runat="server">
                            <Fields>
                                <ext:ModelField Name="DetailId" Type="String" />
                                <ext:ModelField Name="FromDate" Type="String" />
                                <ext:ModelField Name="FromDateEng" Type="Date" />
                                <ext:ModelField Name="STATUS" Type="String" />
                                <ext:ModelField Name="OldLevel" Type="string" />
                                <ext:ModelField Name="PrevStepGrade" Type="string" />
                                <ext:ModelField Name="NewLevel" Type="string" />
                                <ext:ModelField Name="StepGrade" Type="string" />
                                <ext:ModelField Name="ChangeType" Type="string" />
                                <ext:ModelField Name="FromDateEng" Type="Date" />
                                <ext:ModelField Name="Name" Type="String" />
                                <ext:ModelField Name="EmployeeId" Type="String" />
                                <ext:ModelField Name="IdCardNo" Type="String" />
                                <ext:ModelField Name="IsBackdatedPromotion" Type="Boolean" />
                                <ext:ModelField Name="OldDesignation" Type="String" />
                                <ext:ModelField Name="NewDesignation" Type="String" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>
            <ColumnModel>
                <Columns>
                    <ext:Column ID="Column6"  Locked="true" runat="server" Text="EIN" Width="50px" DataIndex="EmployeeId">
                    </ext:Column>
                    <ext:Column ID="Column9"  Locked="true"  runat="server" Text="I No" Width="50px" DataIndex="IdCardNo">
                    </ext:Column>
                    <ext:Column ID="Column7"  Locked="true"  runat="server" Text="Name" Width="180px" DataIndex="Name">
                    </ext:Column>
                    <ext:Column ID="Column1" runat="server" Text="From Date" DataIndex="FromDate" />
                    <ext:DateColumn runat="server" Format="yyyy/MM/dd" Text="From Date Eng" Width="120px"
                        DataIndex="FromDateEng">
                    </ext:DateColumn>
                    <ext:Column ID="Column10" runat="server" Text="Status" Width="90px" DataIndex="STATUS">
                    </ext:Column>
                    <ext:Column ID="Column2" runat="server" Text="Old Level" Width="180px" DataIndex="OldLevel">
                    </ext:Column>
                    <ext:Column ID="Column3" runat="server" Text="Old Grade" Width="70" DataIndex="PrevStepGrade">
                    </ext:Column>
                    <ext:Column ID="Column4" runat="server" Text="New Level" Width="180px" DataIndex="NewLevel">
                    </ext:Column>
                    <ext:Column ID="Column5" runat="server" Text="New Grade" DataIndex="StepGrade">
                    </ext:Column>
                    <ext:Column ID="Column11" runat="server" Text="Old Designation" Width="120px" DataIndex="OldDesignation">
                    </ext:Column>
                    <ext:Column ID="Column12" runat="server" Text="New Designation" Width="120px"  DataIndex="NewDesignation">
                    </ext:Column>
                    <ext:Column ID="Column8" runat="server" Text="Arrear" Width="60px" DataIndex="IsBackdatedPromotion">
                        <Renderer Fn="renderArrear" />
                    </ext:Column>
                    <ext:CommandColumn ID="CommandColumn1" runat="server" Text="Actions" Align="Center"
                        Width="63">
                        <Commands>
                            <ext:GridCommand Cls="editGridButton" ToolTip-Text="Edit" Text="<i class='fa fa-pencil'></i>"
                                CommandName="Edit" />
                            <ext:GridCommand Cls="editGridButton" ToolTip-Text="Delete" Text="<i class='fa fa-trash-o'></i>"
                                CommandName="Delete" />
                        </Commands>
                        <Listeners>
                            <Command Handler="CommandHandler1(command,record);" />
                        </Listeners>
                    </ext:CommandColumn>
                </Columns>
            </ColumnModel>
            <SelectionModel>
                <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
            </SelectionModel>
            <BottomBar>
                <ext:PagingToolbar ID="PagingToolbar1" runat="server" PageSize="50" DisplayInfo="true"
                    DisplayMsg="Displaying Requests {0} - {1} of {2}" EmptyMsg="No Records to display">
                    <Items>
                        <ext:ComboBox Hidden="true" runat="server" SelectedIndex="0" Width="80" ID="cmbPageSize">
                            <Items>
                                <ext:ListItem Text="20" Value="20" />
                                <ext:ListItem Text="30" Value="30" />
                                <ext:ListItem Text="50" Value="50" />
                                <ext:ListItem Text="100" Value="100" />
                            </Items>
                            <Listeners>
                                <Select Handler="#{PagingToolbar1}.pageSize = parseInt( '50'); searchList();" />
                            </Listeners>
                        </ext:ComboBox>
                    </Items>
                </ext:PagingToolbar>
            </BottomBar>
        </ext:GridPanel>
        <div style="clear: both">
        </div>
    </div>
    <ext:Window ID="window" runat="server" Title="Promotion Details" Icon="Application"
        Width="730" Height="200" BodyPadding="5" Hidden="true" Modal="true">
        <Content>
            <table class="fieldTable" style="margin-left: 20px;">
                <tr>
                    <td style="width: 120px;">
                        Employee Name
                    </td>
                    <td style="width: 160px;">
                        <ext:DisplayField ID="lblEmpName" runat="server" />
                    </td>
                    <td style="width: 120px;">
                        Date *
                    </td>
                    <td>
                        <pr:CalendarExtControl Width="180" ID="calDate" runat="server" LabelSeparator="" />
                        <asp:RequiredFieldValidator ID="rfvLetterDate" runat="server" ControlToValidate="calDate"
                            Display="None" ErrorMessage="Date is required." ValidationGroup="SaveBrTran"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td style="width: 120px;">
                        From Level
                    </td>
                    <td>
                        <ext:TextField ID="txtFromLevel" Width="180" runat="server" LabelSeparator="">
                        </ext:TextField>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFromLevel"
                            Display="None" ErrorMessage="From level is required." ValidationGroup="SaveBrTran"></asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 120px;">
                        New Level
                    </td>
                    <td>
                        <ext:TextField ID="txtNewLevel" Width="180" runat="server" LabelSeparator="">
                        </ext:TextField>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtNewLevel"
                            Display="None" ErrorMessage="To level is required." ValidationGroup="SaveBrTran"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td valign="bottom" colspan="4">
                        <div id="buttonsBar" runat="server" class="popupButtonDiv" style="float: right;">
                            <div style="width: 100px;">
                                <ext:Button runat="server" ID="btnSave" Cls="btn btn-primary" Width="90" Text="<i></i>Save">
                                    <DirectEvents>
                                        <Click OnEvent="btnSave_Click">
                                            <EventMask ShowMask="true" />
                                        </Click>
                                    </DirectEvents>
                                    <Listeners>
                                        <Click Handler="valGroup = 'SaveBrTran'; if(CheckValidation()) return this.disable(); else return false;">
                                        </Click>
                                    </Listeners>
                                </ext:Button>
                            </div>
                            <ext:Button runat="server" ID="btnCancel" Cls="btn btn-default" Text="Cancel">
                                <Listeners>
                                    <Click Handler="#{window}.hide();" />
                                </Listeners>
                            </ext:Button>
                        </div>
                    </td>
                </tr>
            </table>
        </Content>
    </ext:Window>
</asp:Content>
