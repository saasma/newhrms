﻿<%@ Page Title="Employee Home Salary" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="EmployeeHomeSalary.aspx.cs" Inherits="Web.CP.EmployeeHomeSalary" %>

<%@ MasterType VirtualPath="~/Master/NewDetails.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

    function searchList() {
              <%=PagingToolbar1.ClientID %>.doRefresh();
        }

 
   
    function SelEmpSearch()
    {
        <%=hdnEmpSearch.ClientID %>.setValue(<%=cmbEmpSearch.ClientID %>.getValue());
    }

    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <ext:Hidden ID="hdnEmployeeId" runat="server" />
    <ext:Hidden ID="hdnBranchDepartmentId" runat="server" />
    <ext:Hidden ID="hdnMsg" runat="server" />
    <ext:Hidden ID="hdnEmpSearch" runat="server" />
    <ext:Hidden ID="hdnSortBy" runat="server" />
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Employee Home Salary
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel" style="margin-top: 0px !important; padding-top: 0px !important;">
        <div class="innerLR">
            <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                <Proxy>
                    <ext:AjaxProxy Url="../Handler/EmpSearchID.ashx">
                        <ActionMethods Read="GET" />
                        <Reader>
                            <ext:JsonReader Root="plants" TotalProperty="total" />
                        </Reader>
                    </ext:AjaxProxy>
                </Proxy>
                <Model>
                    <ext:Model ID="Model5" IDProperty="EmployeeId" Name="ItemLineModel" runat="server">
                        <Fields>
                            <ext:ModelField Name="Name" Type="String" />
                            <ext:ModelField Name="EmployeeId" Type="String" />
                        </Fields>
                    </ext:Model>
                </Model>
            </ext:Store>
            <div class="alert alert-info" style="margin-top: 10px; padding-top: 0px!important;">
                <ext:Container ID="listingContainer" runat="server">
                    <Content>
                        <table>
                            <tr>
                                <td>
                                    <ext:ComboBox Width="140" MatchFieldWidth="false" MarginSpec="0 5 0 5" LabelSeparator=""
                                        LabelAlign="Top" QueryMode="Local" ID="cmbBranch" DisplayField="Name" ValueField="BranchId"
                                        runat="server" FieldLabel="Branch">
                                        <Items>
                                            <ext:ListItem Index="0" Value="-1" Text="All">
                                            </ext:ListItem>
                                        </Items>
                                        <Store>
                                            <ext:Store ID="Store5" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model7" IDProperty="BranchId" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="BranchId" />
                                                            <ext:ModelField Name="Name" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                    </ext:ComboBox>
                                </td>
                                <td>
                                    <ext:ComboBox FieldLabel="Department" MatchFieldWidth="false" ID="cmbDepartment"
                                        Width="140" MarginSpec="0 5 0 5" runat="server" ValueField="DepartmentId" DisplayField="Name"
                                        LabelAlign="Top" LabelSeparator="" QueryMode="Local">
                                        <Items>
                                            <ext:ListItem Index="0" Value="-1" Text="All">
                                            </ext:ListItem>
                                        </Items>
                                        <Store>
                                            <ext:Store ID="Store6" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model8" IDProperty="DepartmentId" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="DepartmentId" />
                                                            <ext:ModelField Name="Name" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                    </ext:ComboBox>
                                </td>
                                <td>
                                    <ext:ComboBox ID="cmbLevel" MatchFieldWidth="false" runat="server" FieldLabel="Level/Position"
                                        Width="140" MarginSpec="0 5 0 5" DisplayField="Name" ValueField="LevelId" QueryMode="Local"
                                        LabelSeparator="" LabelAlign="Top">
                                        <Items>
                                            <ext:ListItem Index="0" Value="-1" Text="All">
                                            </ext:ListItem>
                                        </Items>
                                        <Store>
                                            <ext:Store ID="storeLevel" runat="server">
                                                <Model>
                                                    <ext:Model ID="modelLevel" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="LevelId" Type="Int" />
                                                            <ext:ModelField Name="Name" Type="String" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                    </ext:ComboBox>
                                </td>
                                <td>
                                    <ext:ComboBox MatchFieldWidth="false" FieldLabel="Designation" ID="cmbDesignation"
                                        Width="140" MarginSpec="0 5 0 5" runat="server" ValueField="DesignationId" DisplayField="LevelAndDesignation"
                                        LabelAlign="Top" LabelSeparator="" QueryMode="Local">
                                        <Items>
                                            <ext:ListItem Index="0" Value="-1" Text="All">
                                            </ext:ListItem>
                                        </Items>
                                        <Store>
                                            <ext:Store ID="Store7" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model9" IDProperty="DesignationId" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="DesignationId" />
                                                            <ext:ModelField Name="LevelAndDesignation" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                    </ext:ComboBox>
                                </td>
                                <td style="width: 310px;">
                                    <ext:ComboBox LabelSeparator="" ID="cmbEmpSearch" FieldLabel="Search Employee" EmptyText="Employee Name"
                                        LabelWidth="80" LabelAlign="Top" runat="server" DisplayField="Name" ValueField="EmployeeId"
                                        StoreID="storeSearch" TypeAhead="false" Width="250" PageSize="9999" HideBaseTrigger="true"
                                        MinChars="1" TriggerAction="All" ForceSelection="false">
                                        <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                                            <ItemTpl ID="ItemTpl2" runat="server">
                                                <Html>
                                                    <tpl>
                                                        <div class="search-item">
                                                                        <span>{Name}</span>  
                                                         </div>
					                            </tpl>
                                                </Html>
                                            </ItemTpl>
                                        </ListConfig>
                                        <Triggers>
                                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                        </Triggers>
                                        <Listeners>
                                            <Select Handler="this.getTrigger(0).show(); SelEmpSearch();" />
                                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                            <TriggerClick Handler="if (index == 0) { 
                                                       this.clearValue(); 
                                                       this.getTrigger(0).hide();
                                                   }" />
                                        </Listeners>
                                    </ext:ComboBox>
                                </td>
                                <td>
                                    <ext:Button ID="btnLoad" runat="server" Text="Load" Width="100" Cls="btn btn-default"
                                        MarginSpec="25 10 10 10">
                                        <Listeners>
                                            <Click Fn="searchList">
                                            </Click>
                                        </Listeners>
                                    </ext:Button>
                                </td>
                            </tr>
                        </table>
                    </Content>
                </ext:Container>
            </div>
            <ext:KeyMap ID="KeyMap1" runat="server" Target="={#{listingContainer}.getBody()}">
                <Binding>
                    <ext:KeyBinding Handler="#{btnLoad}.fireEvent('click');">
                        <Keys>
                            <ext:Key Code="ENTER" />
                        </Keys>
                    </ext:KeyBinding>
                </Binding>
            </ext:KeyMap>
            <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridBranchTransfer" runat="server"
                Cls="itemgrid1" OnReadData="Store_ReadData"  AutoScroll="true">
                <Store>
                    <ext:Store PageSize="25" ID="storeEmpList" runat="server" AutoLoad="true" OnReadData="Store_ReadData"
                        RemotePaging="true" RemoteSort="true">
                        <Proxy>
                            <ext:PageProxy />
                        </Proxy>
                        <Model>
                            <ext:Model ID="Model1" runat="server" IDProperty="EmployeeId">
                                <Fields>
                                    <ext:ModelField Name="RowNumber" Type="String" />
                                    <ext:ModelField Name="EmployeeId" Type="String" />
                                    <ext:ModelField Name="IDCardNo" Type="String" />
                                    <ext:ModelField Name="NAME" Type="String" />
                                    <ext:ModelField Name="Branch" Type="String" />
                                    <ext:ModelField Name="Department" Type="String" />
                                    <ext:ModelField Name="Position" Type="String" />
                                    <ext:ModelField Name="Designation" Type="String" />
                                    <ext:ModelField Name="AccountNo" Type="String" />
                                    <ext:ModelField Name="AIncome1" Type="String" />
                                    <ext:ModelField Name="AIncome2" Type="String" />
                                    <ext:ModelField Name="AIncome3" Type="String" />
                                    <ext:ModelField Name="AIncome4" Type="String" />
                                    <ext:ModelField Name="AIncome5" Type="String" />
                                    <ext:ModelField Name="AIncome6" Type="String" />
                                    <ext:ModelField Name="APFIncome" Type="String" />
                                    <ext:ModelField Name="AGrossSalary" Type="String" />
                                    <ext:ModelField Name="APFDeduction" Type="String" />
                                    <ext:ModelField Name="ACIT" Type="String" />
                                    <ext:ModelField Name="ATax" Type="String" />
                                    <ext:ModelField Name="AnnualNetSalary" Type="String" />
                                    <ext:ModelField Name="Income1" Type="String" />
                                    <ext:ModelField Name="Income2" Type="String" />
                                    <ext:ModelField Name="Income3" Type="String" />
                                    <ext:ModelField Name="Income4" Type="String" />
                                    <ext:ModelField Name="Income5" Type="String" />
                                    <ext:ModelField Name="Income6" Type="String" />
                                    <ext:ModelField Name="PFIncome" Type="String" />
                                    <ext:ModelField Name="GrossSalary" Type="String" />
                                    <ext:ModelField Name="PFDeduction" Type="String" />
                                    <ext:ModelField Name="CIT" Type="String" />
                                    <ext:ModelField Name="Tax" Type="String" />
                                    <ext:ModelField Name="NetSalary" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Sorters>
                            <ext:DataSorter Property="EmployeeId" Direction="ASC" />
                        </Sorters>
                    </ext:Store>
                </Store>
                <ColumnModel ID="ColumnModel1" runat="server">
                    <Columns>
                        <ext:Column ID="colFromDate" runat="server" Text="SN" Locked="true" DataIndex="RowNumber"
                            MenuDisabled="false" Sortable="false" Align="Right" Width="50" />
                        <ext:Column ID="Column1" runat="server" Text="EIN" Locked="true" DataIndex="EmployeeId"
                            MenuDisabled="false" Sortable="true" Align="Right" Width="50" />
                        <ext:Column ID="Column2" runat="server" Text="I No" Locked="true" DataIndex="IDCardNo"
                            MenuDisabled="false" Sortable="true" Align="Right" Width="50" />
                        <ext:Column ID="Column3" runat="server" Text="Name" Locked="true" DataIndex="NAME"
                            MenuDisabled="false" Sortable="true" Align="Left" Width="150" />
                        <ext:Column ID="Column4" runat="server" Text="Branch" DataIndex="Branch" MenuDisabled="false"
                            Sortable="true" Align="Left" Width="120" />
                        <ext:Column ID="Column5" runat="server" Text="Department" DataIndex="Department"
                            MenuDisabled="false" Sortable="true" Align="Left" Width="120" />
                        <ext:Column ID="Column6" runat="server" Text="Position" DataIndex="Position" MenuDisabled="false"
                            Sortable="true" Align="Left" Width="120" />
                        <ext:Column ID="Column7" runat="server" Text="Designation" DataIndex="Designation"
                            MenuDisabled="false" Sortable="true" Align="Left" Width="120" />
                        <ext:Column ID="Column8" runat="server" Text="Account No" DataIndex="AccountNo" MenuDisabled="false"
                            Sortable="true" Align="Left" Width="120" />
                        <ext:Column ID="Column10" Text="Monthly Income Estimation" StyleSpec="background-color:#DDEBF7"
                            runat="server">
                            <Columns>
                                <ext:Column ID="ColumnIncome1" runat="server" Text="" StyleSpec="background-color:#DDEBF7"
                                    DataIndex="Income1" MenuDisabled="false" Sortable="false" Align="Center" Width="120">
                                    <Renderer Fn="getFormattedAmount" />
                                </ext:Column>
                                <ext:Column ID="ColumnIncome2" runat="server" Text="" StyleSpec="background-color:#DDEBF7"
                                    DataIndex="Income2" MenuDisabled="false" Sortable="false" Align="Right" Width="120">
                                    <Renderer Fn="getFormattedAmount" />
                                </ext:Column>
                                <ext:Column ID="ColumnIncome3" runat="server" Text="" StyleSpec="background-color:#DDEBF7"
                                    DataIndex="Income3" MenuDisabled="false" Sortable="false" Align="Right" Width="120">
                                    <Renderer Fn="getFormattedAmount" />
                                </ext:Column>
                                <ext:Column ID="ColumnIncome4" runat="server" Text="" StyleSpec="background-color:#DDEBF7"
                                    DataIndex="Income4" MenuDisabled="false" Sortable="false" Align="Right" Width="120">
                                    <Renderer Fn="getFormattedAmount" />
                                </ext:Column>
                                <ext:Column ID="ColumnIncome5" runat="server" Text="" StyleSpec="background-color:#DDEBF7"
                                    DataIndex="Income5" MenuDisabled="false" Sortable="false" Align="Right" Width="120">
                                    <Renderer Fn="getFormattedAmount" />
                                </ext:Column>
                                <ext:Column ID="ColumnIncome6" runat="server" Text="" StyleSpec="background-color:#DDEBF7"
                                    DataIndex="Income6" MenuDisabled="false" Sortable="false" Align="Right" Width="120">
                                    <Renderer Fn="getFormattedAmount" />
                                </ext:Column>
                                <ext:Column ID="Column19" runat="server" Text="PF" StyleSpec="background-color:#DDEBF7"
                                    DataIndex="PFIncome" MenuDisabled="false" Sortable="false" Align="Right" Width="120">
                                    <Renderer Fn="getFormattedAmount" />
                                </ext:Column>
                                <ext:Column ID="Column20" runat="server" StyleSpec="background-color:#DDEBF7" Text="Gross Salary"
                                    DataIndex="GrossSalary" MenuDisabled="false" Sortable="false" Align="Right" Width="120">
                                    <Renderer Fn="getFormattedAmount" />
                                </ext:Column>
                            </Columns>
                        </ext:Column>
                        <ext:Column ID="Column14" Text="Monthly Deduction Estimation" StyleSpec="background-color:#DDEBF7"
                            runat="server">
                            <Columns>
                                <ext:Column ID="Column21" runat="server" StyleSpec="background-color:#DDEBF7" Text="PF"
                                    DataIndex="PFDeduction" MenuDisabled="false" Sortable="false" Align="Right" Width="120">
                                    <Renderer Fn="getFormattedAmount" />
                                </ext:Column>
                                <%-- <ext:Column ID="Column22" runat="server"  StyleSpec="background-color:#DDEBF7"   Text="CIT" DataIndex="CIT" MenuDisabled="false"
                                    Sortable="false" Align="Right" Width="120">
                                    <Renderer Fn="getFormattedAmount" />
                                </ext:Column>--%>
                                <ext:Column ID="Column23" runat="server" StyleSpec="background-color:#DDEBF7" Text="Tax"
                                    DataIndex="Tax" MenuDisabled="false" Sortable="false" Align="Right" Width="120">
                                    <Renderer Fn="getFormattedAmount" />
                                </ext:Column>
                            </Columns>
                        </ext:Column>
                        <ext:Column ID="Column24" runat="server" StyleSpec="background-color:#DDEBF7" Text="Net Salary"
                            DataIndex="NetSalary" MenuDisabled="false" Sortable="false" Align="Right" Width="120">
                            <Renderer Fn="getFormattedAmount" />
                        </ext:Column>
                        <ext:Column Text="Annual Income Estimation" StyleSpec="background-color:#FCE4D6"
                            runat="server">
                            <Columns>
                                <ext:Column ID="ColumnAIncome1" runat="server" Text="" StyleSpec="background-color:#FCE4D6"
                                    DataIndex="AIncome1" MenuDisabled="false" Sortable="false" Align="Center" Width="120">
                                    <Renderer Fn="getFormattedAmount" />
                                </ext:Column>
                                <ext:Column ID="ColumnAIncome2" runat="server" Text="" StyleSpec="background-color:#FCE4D6"
                                    DataIndex="AIncome2" MenuDisabled="false" Sortable="false" Align="Right" Width="120">
                                    <Renderer Fn="getFormattedAmount" />
                                </ext:Column>
                                <ext:Column ID="ColumnAIncome3" runat="server" Text="" StyleSpec="background-color:#FCE4D6"
                                    DataIndex="AIncome3" MenuDisabled="false" Sortable="false" Align="Right" Width="120">
                                    <Renderer Fn="getFormattedAmount" />
                                </ext:Column>
                                <ext:Column ID="ColumnAIncome4" runat="server" Text="" StyleSpec="background-color:#FCE4D6"
                                    DataIndex="AIncome4" MenuDisabled="false" Sortable="false" Align="Right" Width="120">
                                    <Renderer Fn="getFormattedAmount" />
                                </ext:Column>
                                <ext:Column ID="ColumnAIncome5" runat="server" Text="" StyleSpec="background-color:#FCE4D6"
                                    DataIndex="AIncome5" MenuDisabled="false" Sortable="false" Align="Right" Width="120">
                                    <Renderer Fn="getFormattedAmount" />
                                </ext:Column>
                                <ext:Column ID="ColumnAIncome6" runat="server" Text="" StyleSpec="background-color:#FCE4D6"
                                    DataIndex="AIncome6" MenuDisabled="false" Sortable="false" Align="Right" Width="120">
                                    <Renderer Fn="getFormattedAmount" />
                                </ext:Column>
                                <ext:Column ID="Column13" runat="server" Text="PF" StyleSpec="background-color:#FCE4D6"
                                    DataIndex="APFIncome" MenuDisabled="false" Sortable="false" Align="Right" Width="120">
                                    <Renderer Fn="getFormattedAmount" />
                                </ext:Column>
                                <ext:Column ID="Column131" runat="server" StyleSpec="background-color:#FCE4D6" Text="Gross Salary"
                                    DataIndex="AGrossSalary" MenuDisabled="false" Sortable="false" Align="Right"
                                    Width="120">
                                    <Renderer Fn="getFormattedAmount" />
                                </ext:Column>
                            </Columns>
                        </ext:Column>
                        <ext:Column Text="Annual Deduction Estimation" StyleSpec="background-color:#FCE4D6"
                            runat="server">
                            <Columns>
                                <ext:Column ID="Column9" runat="server" Text="PF" StyleSpec="background-color:#FCE4D6"
                                    DataIndex="APFDeduction" MenuDisabled="false" Sortable="false" Align="Right"
                                    Width="120">
                                    <Renderer Fn="getFormattedAmount" />
                                </ext:Column>
                                <%--  <ext:Column ID="Column10" runat="server" Text="CIT" StyleSpec="background-color:#FCE4D6"  DataIndex="ACIT" MenuDisabled="false"
                                    Sortable="false" Align="Right" Width="120">
                                    <Renderer Fn="getFormattedAmount" />
                                </ext:Column>--%>
                                <ext:Column ID="Column11" runat="server" Text="Tax" StyleSpec="background-color:#FCE4D6"
                                    DataIndex="ATax" MenuDisabled="false" Sortable="false" Align="Right" Width="120">
                                    <Renderer Fn="getFormattedAmount" />
                                </ext:Column>
                            </Columns>
                        </ext:Column>
                        <ext:Column ID="Column12" runat="server" StyleSpec="background-color:#FCE4D6" Text="Annual Net Salary"
                            DataIndex="AnnualNetSalary" MenuDisabled="false" Sortable="false" Align="Right"
                            Width="120">
                            <Renderer Fn="getFormattedAmount" />
                        </ext:Column>
                    </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar1" runat="server" StoreID="storeEmpList" DisplayInfo="true">
                    </ext:PagingToolbar>
                </BottomBar>
            </ext:GridPanel>
        </div>
        <div class="alert alert-info" style="margin-top: 15px; height: 50px;">
            <div style="float: right;">
                <ext:Button runat="server" AutoPostBack="true" OnClick="btnExport_Click" ID="btnExport"
                    Text="<i></i>Export To Excel">
                </ext:Button>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
