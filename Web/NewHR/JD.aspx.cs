﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Entity;
using BLL;
using Utils;
using System.IO;
using Web.Helper;
using BLL.BO;
using DevExpress.Web;

using DevExpress.Web.Internal;
using DevExpress.XtraRichEdit;
using DevExpress.Utils.Internal;

using System.Reflection;
using System.Text.RegularExpressions;

namespace Web.NewHR
{
    public partial class JD : BasePage
    {
        int employeeId = 0;

        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.HumanResource;
            }
        }

        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.NIBL)
                Response.Redirect("EmployeeSummary.aspx?ID=" + Request.QueryString["ID"]);

            this.DisableViewStateForExtResourceManager();
            employeeId = int.Parse(Request.QueryString["ID"]);
            

            if (!X.IsAjaxRequest && !IsPostBack)
            {
                LoadEditData();
            }

           
                //Load Edit Window and Desc on Editor
                //DateTime EffectiveDate = DateTime.Parse(Request.QueryString["ed"]);
                //EditJD(EffectiveDate);
           
        }

        private void LoadEmpCurrentJob()
        {
            List<EmployeeCurrentJobBo> _listCurrentJob = new List<EmployeeCurrentJobBo>();
            EmployeeCurrentJobBo _obj = new EmployeeCurrentJobBo();


            int? DesignationId = EmployeeManager.GetEmployeeCurrentPositionForDate(employeeId);
            int LevelId = CommonManager.GetLevelIDForDesignation(DesignationId.Value);
            if (DesignationId != null)
            {
                EDesignation desc = CommonManager.GetDesigId(DesignationId.Value);
                _obj.Level = desc.BLevel.Name;
                _obj.Position = desc.Name;
            }

            _obj.EffectiveDate = "";
            _obj.FunctionalTitle = "";
            _listCurrentJob.Add(_obj);

            gridJobDesc.GetStore().DataSource = _listCurrentJob;
            gridJobDesc.GetStore().DataBind();
        }

        private void LoadEditData()
        {
           
            EEmployee emp = EmployeeManager.GetEmployeeById(employeeId);
            if (emp != null)
            {
                LoadBasicInfo(emp);
                LoadPersonalInfo(emp);
                LoadContactInfo(emp);
                GetCurrentJobDescripton();
                GetJobDescriptionHistory();
                if (emp.IsRetired != null && emp.IsRetired.Value)
                {
                    if (emp.EHumanResources[0].DateOfRetirementEng != null && emp.EHumanResources[0].DateOfRetirementEng < DateTime.Now)
                    {
                        spanRetiredDate.InnerHtml = "Retired : " + emp.EHumanResources[0].DateOfRetirement + " (" + emp.EHumanResources[0].DateOfRetirementEng.Value.ToLongDateString() + ")";
                    }
                }
                if (emp.IsResigned != null && emp.IsResigned.Value)
                {
                    if (emp.EHumanResources[0].DateOfResignationEng != null && emp.EHumanResources[0].DateOfResignationEng < DateTime.Now)
                    {
                        spanRetiredDate.InnerHtml = "Retired : " + emp.EHumanResources[0].DateOfResignation + " (" + emp.EHumanResources[0].DateOfResignationEng.Value.ToLongDateString() + ")";
                    }
                }
            }
        }

        protected void GetJobDescriptionHistory()
        {
            List<JobDescription> dbJobDescriptionList = NewHRManager.GetJobDescriptonHistory(employeeId);
            List<EmployeeCurrentJobBo> _listCurrentJob = new List<EmployeeCurrentJobBo>();
            foreach (JobDescription _item in dbJobDescriptionList)
            {
                EmployeeCurrentJobBo _obj = new EmployeeCurrentJobBo();
                int? DesignationId = _item.PositionId;
                int LevelId = _item.LevelId.Value;
            //    if (_item.EffectiveDateEng != null)
                   // hiddenValue.Text = _item.EffectiveDateEng.Date.ToString();
                if (DesignationId != null)
                {
                    EDesignation desc = CommonManager.GetDesigId(DesignationId.Value);
                    _obj.Level = desc.BLevel.Name;
                    _obj.Position = desc.Name;
                }

                _obj.EffectiveDate = _item.EffectiveDate;
                _obj.EffectiveDateEng = _item.EffectiveDateEng;
                _obj.FunctionalTitle = "";
                _listCurrentJob.Add(_obj);
            }
            gridJobDescHistory.GetStore().DataSource = _listCurrentJob;
            gridJobDescHistory.GetStore().DataBind();
        }


        protected void GetCurrentJobDescripton()
        {
            List<JobDescription> dbJobDescriptionList = NewHRManager.GetCurrentJobDescripton(employeeId);
            JobDescription  dbJobDescription =  new JobDescription();
            if (!dbJobDescriptionList.Any())
                return;
            else
            dbJobDescription = dbJobDescriptionList[0];

            List<EmployeeCurrentJobBo> _listCurrentJob = new List<EmployeeCurrentJobBo>();
            EmployeeCurrentJobBo _obj = new EmployeeCurrentJobBo();

            int? DesignationId = dbJobDescription.PositionId;
            int LevelId = dbJobDescription.LevelId.Value;
            if (dbJobDescription.EffectiveDateEng!=null)
            hiddenValue.Text = dbJobDescription.EffectiveDateEng.Date.ToString();
            if (DesignationId != null)
            {
                EDesignation desc = CommonManager.GetDesigId(DesignationId.Value);
                _obj.Level = desc.BLevel.Name;
                _obj.Position = desc.Name;
            }

            _obj.EffectiveDate = dbJobDescription.EffectiveDate;
            _obj.EffectiveDateEng = dbJobDescription.EffectiveDateEng;
            _obj.FunctionalTitle = "";
            _listCurrentJob.Add(_obj);


            if (dbJobDescription.JobDesc != null)
            {
                txtEditorDescription.Text = dbJobDescription.JobDesc;
            }
            gridJobDesc.GetStore().DataSource = _listCurrentJob;
            gridJobDesc.GetStore().DataBind();
        }

        protected void btnDeleteJd_Click(object sender, DirectEventArgs e)
        {

        }

        protected void btnSaveEditor_Click(object sender, DirectEventArgs e)
        {
          
                JobDescription objJobDescription = new JobDescription();
                objJobDescription.EmpId = employeeId;
                objJobDescription.EffectiveDateEng = DateTime.Parse(hiddenValue.Text.Trim());
              //  var myByteArray = ASPxRichEdit1.SaveCopy(DocumentFormat.Rtf);
                objJobDescription.JobDesc = hdnEditorDescription.Text;

               // Guid FileID = new Guid();
               // string Filepath = Server.MapPath("~\\App_Data\\WorkDirectory\\") + "jd" + FileID + ".Rtf";

               // File.WriteAllBytes(Filepath, myByteArray);
               //ASPxRichEdit1.Open(Filepath, DocumentFormat.Rtf);
               //if (File.Exists(Filepath))
               //{
               //    try
               //    {
               //        File.Delete(Filepath);
               //    }
               //    catch
               //    {

               //    }
               //}

                Status status = NewHRManager.UpdateJDEditor(objJobDescription);
                if (status.IsSuccess)
                {
                    WindowLevel.Hide();
                    NewMessage.ShowNormalMessage("Information updated successfully");
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }
            
        }

        protected void btnEditJd_Click(object sender, DirectEventArgs e)
        {
            string EffectiveDate = hiddenValue.Text.Trim();
            //Response.Redirect("jd.aspx?ID=" + employeeId + "&ed=" + EffectiveDate);
            EditJD(DateTime.Parse(EffectiveDate));
        }

        protected void EditJD(DateTime EffectiveDate)
        {
           // DateTime EffectiveDate = DateTime.Parse(hiddenValue.Text.Trim());
            JobDescription entity = NewHRManager.GetEmpJobDescriptionByEffectiveDate(employeeId, EffectiveDate);
          
            calEffectiveDate.Text = BLL.BaseBiz.GetAppropriateDate(entity.EffectiveDateEng);
            if (entity.PositionId != null)
            {
                EDesignation desc = CommonManager.GetDesigId(entity.PositionId.Value);
                //lblDesignation.Text = desc.Name;
                //lblLevel.Text = desc.BLevel.Name;
                hiddenPositionId.Text = entity.PositionId.ToString();
                hiddenLevelId.Text = entity.LevelId.ToString();
            }

            if (entity.JobDesc != null)
            {
                //txtEditorDescription.Text = entity.JobDesc;
                // string a = Server.HtmlDecode(entity.JobDesc);
                string NewValue = Regex.Replace(entity.JobDesc, @"\r\n?|\n", string.Empty);
                string JScript = "CKEDITOR.instances.ctl00_ContentPlaceHolder_Main_txtEditorDescription.setData('" + NewValue + "');";
                X.AddScript(JScript);
            }
            else
            {
                string JScript= "CKEDITOR.instances.ctl00_ContentPlaceHolder_Main_txtEditorDescription.setData('');";
                X.AddScript(JScript);
            }

            WindowLevel.Center();
            WindowLevel.Show();
        }

        protected void btnAddJd_Click(object sender, DirectEventArgs e)
        {
            Clearvalues();
            int? DesignationId = EmployeeManager.GetEmployeeCurrentPositionForDate(employeeId);
            int LevelId = CommonManager.GetLevelIDForDesignation(DesignationId.Value);
            hiddenLevelId.Text = LevelId.ToString();

            if (DesignationId != null)
            {
                EDesignation desc = CommonManager.GetDesigId(DesignationId.Value);
                //lblDesignation.Text = desc.Name;
                //lblLevel.Text = desc.BLevel.Name;
                hiddenPositionId.Text = DesignationId.ToString();
            }

            //lblFunctionalTitle.Text = "";
            hiddenFunctionalTitleId.Text = "";
            WindowLevel.Center();
            WindowLevel.Show();
        }
        protected void Clearvalues()
        {
            calEffectiveDate.Clear();
            hiddenValue.Clear();
        }

        protected void btnSaveJD_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveUpdateLevel");
            if (Page.IsValid)
            {
                JobDescription objJobDescription = new JobDescription();
                objJobDescription.EmpId = employeeId;
                objJobDescription.EffectiveDate = calEffectiveDate.Text;
                objJobDescription.EffectiveDateEng = GetEngDate(calEffectiveDate.Text);
                if (!string.IsNullOrEmpty(hiddenLevelId.Text))
                    objJobDescription.LevelId = int.Parse(hiddenLevelId.Text);

                if (!string.IsNullOrEmpty(hiddenPositionId.Text))
                    objJobDescription.PositionId = int.Parse(hiddenPositionId.Text);

                if (!string.IsNullOrEmpty(hiddenFunctionalTitleId.Text))
                    objJobDescription.FunctionalId = int.Parse(hiddenFunctionalTitleId.Text);

                Status status = NewHRManager.InsertUpdateJD(objJobDescription);
                if (status.IsSuccess)
                {
                    WindowLevel.Hide();
                    GetCurrentJobDescripton();
                    GetJobDescriptionHistory();
                    NewMessage.ShowNormalMessage("Information saved successfully");
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }
            }
        }

        
        private void LoadContactInfo(EEmployee emp)
        {
        

            
            if (emp.EAddresses.Count > 0)
            {
                // Address
                EAddress entity = emp.EAddresses[0];

                string present = "", permanent = "";
                WebHelper.GetAddressHTML(entity, ref present, ref permanent);
               


                // Contact
          
             
            }
        }
        private void LoadPersonalInfo(EEmployee emp)
        {
           // ein.InnerHtml = emp.EmployeeId.ToString();
           // gender.InnerHtml = new Gender().GetText(emp.Gender.Value);
            //dob.InnerHtml = emp.DateOfBirth;
           // maritalStatus.InnerHtml = emp.MaritalStatus;
           // fatherName.InnerHtml = emp.FatherName;

            //editPersonalInfo.HRef += emp.EmployeeId;

            //if (emp.FunctionalTitleId != null)
            //{
            //    FunctionalTitle title = ListManager.GetFunctionalTitle(emp.FunctionalTitleId.Value);
            //    if (title != null)
            //        spanFunctionalTitle.InnerHtml = title.Name;
            //}
            //else
            //    rowFunctionalTitle.Visible = false;

            
        }
        private void LoadBasicInfo(EEmployee emp)
        {

            title.InnerHtml = emp.Title + " " + emp.Name + " - " + emp.EmployeeId;
            if (!string.IsNullOrEmpty(emp.EHumanResources[0].IdCardNo))
            {
                title.InnerHtml += " (I No : " + emp.EHumanResources[0].IdCardNo + ")";
            }
            // Designation/Position
            if (emp.EDesignation != null)
                positionDesignation.InnerHtml = emp.EDesignation.Name;
            // Group/Level
            if (emp.PEmployeeIncomes.Count > 0 && emp.PEmployeeIncomes[0].LevelId != null)
            {
                BLevel level = NewPayrollManager.GetLevelById(emp.PEmployeeIncomes[0].LevelId.Value);
                BLevel actingLevel = NewPayrollManager.GetActingLevelInTodayDate(emp.EmployeeId);

                if (level != null)
                    groupLevel.InnerHtml = level.BLevelGroup.Name + " (Level - " + level.Name +
                        (actingLevel == null ? "" : ", Acting Level - " + actingLevel.Name)
                        + ")";
            }
            else
                groupLevelImg.Visible = false;

            // Branch
            GetEmployeeCurrentBranchDepartmentResult currentBranchDep = EmployeeManager.GetEmployeeCurrentBranchDepartment(emp.EmployeeId);
            if (currentBranchDep != null)
            {
                branch.InnerHtml = currentBranchDep.Department + ", " + currentBranchDep.Branch;
            }

            // Contact
            if (emp.EAddresses.Count > 0)
            {
                EAddress entity = emp.EAddresses[0];

                //if (!string.IsNullOrEmpty(entity.CIPhoneNo) || !string.IsNullOrEmpty(entity.CIMobileNo) || !string.IsNullOrEmpty(entity.Extension))
                //    contactMobile.InnerHtml += string.Format("{0} / {1} ({2})", entity.CIPhoneNo, entity.CIMobileNo, entity.Extension);


                // Mobile
                //if (EmployeeManager.IsBranchOrDepartmentOrRegionalHead(emp.EmployeeId))
                //{
                    if (!string.IsNullOrEmpty(entity.CIMobileNo))
                    {
                        contactMobile.InnerHtml = entity.CIMobileNo + " (O)";

                        if (!string.IsNullOrEmpty(entity.PersonalMobile))
                        {
                            contactMobile.InnerHtml += " " + entity.PersonalMobile + " (P)";
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(entity.PersonalMobile))
                        {
                            contactMobile.InnerHtml += entity.PersonalMobile + " (P)";
                        }
                    }
                    contactMobile.InnerHtml += "&nbsp;";
                //}
                //else
                //{
                //    contactMobile.Visible = false;
                //    imgMobile.Visible = false;
                //}

                // Phone
                if (!string.IsNullOrEmpty(entity.CIPhoneNo))
                    contactPhone.InnerHtml = entity.CIPhoneNo;
                if( !string.IsNullOrEmpty(entity.Extension))
                    contactPhone.InnerHtml += " - " + entity.Extension;
                if (contactPhone.InnerHtml != "")
                    contactPhone.InnerHtml += " (O)";

                if (!string.IsNullOrEmpty(entity.PersonalPhone))
                    contactPhone.InnerHtml += " " + entity.PersonalPhone + " (R)";

                contactPhone.InnerHtml += "&nbsp;";
            

                email.InnerHtml += string.Format("<a href='mailto:{0}'>{0}</a> ", entity.CIEmail);
            }

            // Working for since
            int years, months, days, hours;
            int minutes, seconds, milliseconds;
            ServicePeroid servicePeriod = EmployeeManager.GetServicePeroidDate(employeeId);
            if (servicePeriod != null)
            {
                NewHelper.GetElapsedTime(servicePeriod.FromDateEng, servicePeriod.ToDateEng.Value, out years, out months, out days, out hours, out minutes, out seconds, out milliseconds);
                string text = "";
                if (years != 0)
                    text = " " + years + " years";
                if(months != 0)
                    text += " " + months + " months";
                if (days != 0)
                    text += " " + days + " days";

                workingFor.InnerHtml += text;

                workingFor.InnerHtml += ", Since " +
                    (IsEnglish ? servicePeriod.FromDateEng.ToShortDateString() : servicePeriod.FromDate + " (" + servicePeriod.FromDateEng.ToShortDateString() + ")");
                
            }

            // Photo
            bool hasPhoto = false;
            if (emp.HHumanResources.Count > 0)
            {
                if (!string.IsNullOrEmpty(emp.HHumanResources[0].UrlPhotoThumbnail))
                {
                    image.ImageUrl = Page.ResolveUrl(Config.UploadLocation + "/" + emp.HHumanResources[0].UrlPhotoThumbnail);
                    if (File.Exists(Server.MapPath(image.ImageUrl)))
                        hasPhoto = true;
                }
            }
            if (!hasPhoto)
            {
                image.ImageUrl = Page.ResolveUrl(Web.Helper.WebHelper.GetEmployeeNoImage(emp.EmployeeId));
            }
        }

        private void HideButtonsAndColumns()
        {
           
        }

     

        protected void btnEditEmployee_Click(object sender, DirectEventArgs e)
        {
            if(string.IsNullOrEmpty(Request.QueryString["ID"]))
                return;
            string EmployeeId = Request.QueryString["ID"].ToString();
            Response.Redirect("~/newhr/PersonalDetail.aspx?ID="+ employeeId);
        }

       

    }
}