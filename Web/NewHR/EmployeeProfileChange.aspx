﻿<%@ Page Title="Employee Profile Change" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="EmployeeProfileChange.aspx.cs" Inherits="Web.NewHR.EmployeeProfileChange" %>

<%@ Register Src="~/NewHR/UserControls/EducationPopUp.ascx" TagName="EduPopup" TagPrefix="ucE" %>
<%@ Register Src="~/NewHR/UserControls/EmploymentPopup.ascx" TagName="EmployPopup"
    TagPrefix="ucE" %>
<%@ Register Src="~/NewHR/UserControls/TrainingPopupCtrl.ascx" TagName="TrainingPop"
    TagPrefix="ucT" %>
<%@ Register Src="~/NewHR/UserControls/SeminarPopUpCtrl.ascx" TagName="SemPoUp" TagPrefix="ucSeminar" %>
<%@ Register Src="~/NewHR/UserControls/SkillSetPopUpCtrl.ascx" TagName="SkillSetPop"
    TagPrefix="ucSkillSet" %>
<%@ Register Src="~/NewHR/UserControls/LanguagePopupCtrl.ascx" TagName="LanguageSetPop"
    TagPrefix="ucLang" %>
<%@ Register Src="~/NewHR/UserControls/PublicationPopupCtrl.ascx" TagName="PublicationPop"
    TagPrefix="ucPub" %>
<%@ Register Src="~/NewHR/UserControls/FamilyPopupCtrl.ascx" TagName="FamilyPopup"
    TagPrefix="ucF" %>
<%@ Register Src="~/NewHR/UserControls/UCitizenshipPopupCtrl.ascx" TagName="Ctnship"
    TagPrefix="ucCtzn" %>
<%@ Register Src="~/NewHR/UserControls/UDrivingLiscenceCtlPopupCtrl.ascx" TagName="DrLic"
    TagPrefix="ucDrivLc" %>
<%@ Register Src="~/NewHR/UserControls/UPassortCtrlPopup.ascx" TagName="PassCtrl"
    TagPrefix="ucPas" %>
<%@ Register Src="~/NewHR/UserControls/DocumentPopupCtrl.ascx" TagName="docPopup" TagPrefix="ucDoc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script>
     /* A header Checkbox of CheckboxSelectionModel deals with the current page only.
     This override demonstrates how to take into account all the pages.
     It works with local paging only. It is not going to work with remote paging.
     */
     Ext.selection.CheckboxModel.override({
         selectAll: function (suppressEvent) {
             var me = this,
                    selections = me.store.getAllRange(), // instead of the getRange call
                    i = 0,
                    len = selections.length,
                    start = me.getSelection().length;

             me.suspendChanges();

             for (; i < len; i++) {
                 me.doSelect(selections[i], true, suppressEvent);
             }

             me.resumeChanges();
             if (!suppressEvent) {
                 me.maybeFireSelectionChange(me.getSelection().length !== start);
             }
         },

         deselectAll: Ext.Function.createSequence(Ext.selection.CheckboxModel.prototype.deselectAll, function () {
             this.view.panel.getSelectionMemory().clearMemory();
         }),

         updateHeaderState: function () {
             var me = this,
                    store = me.store,
                    storeCount = store.getTotalCount(),
                    views = me.views,
                    hdSelectStatus = false,
                    selectedCount = 0,
                    selected, len, i;

             if (!store.buffered && storeCount > 0) {
                 selected = me.view.panel.getSelectionMemory().selectedIds;
                 hdSelectStatus = true;
                 for (s in selected) {
                     ++selectedCount;
                 }

                 hdSelectStatus = storeCount === selectedCount;
             }

             if (views && views.length) {
                 me.toggleUiHeader(hdSelectStatus);
             }
         }
     });

     Ext.grid.plugin.SelectionMemory.override({
         memoryRestoreState: function (records) {
             if (this.store !== null && !this.store.buffered && !this.grid.view.bufferedRenderer) {
                 var i = 0,
                        ind,
                        sel = [],
                        len,
                        all = true,
                        cm = this.headerCt;

                 if (!records) {
                     records = this.store.getAllRange(); // instead of getRange
                 }

                 if (!Ext.isArray(records)) {
                     records = [records];
                 }

                 if (this.selModel.isLocked()) {
                     this.wasLocked = true;
                     this.selModel.setLocked(false);
                 }

                 if (this.selModel instanceof Ext.selection.RowModel) {
                     for (ind = 0, len = records.length; ind < len; ind++) {
                         var rec = records[ind],
                                id = rec.getId();

                         if ((id || id === 0) && !Ext.isEmpty(this.selectedIds[id])) {
                             sel.push(rec);
                         } else {
                             all = false;
                         }

                         ++i;
                     }

                     if (sel.length > 0) {
                         this.surpressDeselection = true;
                         this.selModel.select(sel, false, !this.grid.selectionMemoryEvents);
                         this.surpressDeselection = false;
                     }
                 } else {
                     for (ind = 0, len = records.length; ind < len; ind++) {
                         var rec = records[ind],
                                id = rec.getId();

                         if ((id || id === 0) && !Ext.isEmpty(this.selectedIds[id])) {
                             if (this.selectedIds[id].dataIndex) {
                                 var colIndex = cm.getHeaderIndex(cm.down('gridcolumn[dataIndex=' + this.selectedIds[id].dataIndex + ']'))
                                 this.selModel.setCurrentPosition({
                                     row: i,
                                     column: colIndex
                                 });
                             }
                             return false;
                         }

                         ++i;
                     }
                 }

                 if (this.selModel instanceof Ext.selection.CheckboxModel) {
                     if (all) {
                         this.selModel.toggleUiHeader(true);
                     } else {
                         this.selModel.toggleUiHeader(false);
                     }
                 }

                 if (this.wasLocked) {
                     this.selModel.setLocked(true);
                 }
             }
         }
     });



     var CommandHandler = function (command, record) {

            
            if(command == 'View')
            { 
                 <%= hdnEmployeeId.ClientID %>.setValue(record.data.EmployeeId);
                 <%= hdnTableID.ClientID %>.setValue(record.data.Id);
                 <%= hdnTableName.ClientID %>.setValue(record.data.Tbl);
                 <%= btnView.ClientID %>.fireEvent('click');
            }
           
        };

        var cmbTypeChange = function(){
            <%= btnComboChange.ClientID %>.fireEvent('click');
        };

        var CommandDownload = function (command, record) {

            
            if(command == 'Download')
            { 
                 <%= hdnEmployeeId.ClientID %>.setValue(record.data.EmployeeId);
                 <%= hdnTableID.ClientID %>.setValue(record.data.Id);
                 <%= hdnTableName.ClientID %>.setValue(record.data.Tbl);
                 <%= btnDownload.ClientID %>.fireEvent('click');
            }
           
        };

      
         var prepareDownload = function(grid, toolbar, rowIndex, record){
                var downloadBtn = toolbar.items.get(1);
                if(record.data.UserFileName == ''){  
                    downloadBtn.setVisible(false);     
                }
            };
 

    </script>
    <style type="text/css">
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Employee Profile Change
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <ext:Hidden ID="hdnEmployeeId" runat="server" />
        <ext:Hidden ID="hdnTableID" runat="server" />
        <ext:Hidden ID="hdnTableName" runat="server" />
        <ext:LinkButton ID="btnView" runat="server" Hidden="true">
            <DirectEvents>
                <Click OnEvent="btnView_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:LinkButton>
        <ext:LinkButton ID="btnComboChange" runat="server" Hidden="true">
            <DirectEvents>
                <Click OnEvent="btnComboChange_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:LinkButton>
        <ext:LinkButton ID="btnDownload" runat="server" Hidden="true">
            <DirectEvents>
                <Click OnEvent="btnDownload_Click">
                </Click>
            </DirectEvents>
        </ext:LinkButton>
        <ext:Container ID="lblMsg" runat="server" />
        <div>
            <div class="panel-body" style="padding: 0px;">
                <div class="alert alert-info" style="padding: 0px; margin-bottom: 0px;">
                    <table>
                        <tr>
                            <td>
                                <ext:ComboBox ID="cmbType" runat="server" ValueField="ID" DisplayField="Name" Width="110"
                                    FieldLabel="Group by" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                                    QueryMode="Local">
                                    <Items>
                                        <ext:ListItem Text="Name" Value="1" />
                                        <ext:ListItem Text="Table Type" Value="2" />
                                    </Items>
                                    <Listeners>
                                        <Select Handler="cmbTypeChange();" />
                                    </Listeners>
                                </ext:ComboBox>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <br />
            <ext:GridPanel ID="GridPanel1" runat="server" Cls="itemgrid" Width="710" Scroll="None">
                <Store>
                    <ext:Store ID="Store1" runat="server" GroupField="Name">
                        <Sorters>
                            <ext:DataSorter Property="SN" Direction="ASC" />
                        </Sorters>
                        <Model>
                            <ext:Model ID="Model2" runat="server" IDProperty="SN">
                                <Fields>
                                    <ext:ModelField Name="SN" />
                                    <ext:ModelField Name="Id" />
                                    <ext:ModelField Name="EmployeeId" />
                                    <ext:ModelField Name="Name" />
                                    <ext:ModelField Name="ModifiedOn" />
                                    <ext:ModelField Name="TableType" />
                                    <ext:ModelField Name="Tbl" />
                                    <ext:ModelField Name="Value1" />
                                    <ext:ModelField Name="Value2" />
                                    <ext:ModelField Name="UserFileName" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel ID="ColumnModel2" runat="server">
                    <Columns>
                        <ext:Column ID="colName" runat="server" Text="Employee Name" Width="200" DataIndex="Name">
                        </ext:Column>
                        <ext:Column ID="colModifiedOn" runat="server" Text="Date" Width="100" DataIndex="ModifiedOn">
                        </ext:Column>
                        <ext:Column ID="colTableType" runat="server" Text="Type" Width="230" DataIndex="TableType">
                        </ext:Column>
                        <ext:Column ID="colTbl" runat="server" Text="Type" Width="10" DataIndex="Tbl" Hidden="true">
                        </ext:Column>
                        <ext:Column ID="colValue1" runat="server" Text="Value" Width="250" DataIndex="Value1">
                        </ext:Column>
                        <ext:Column ID="colValue2" runat="server" Text="Value" Width="250" DataIndex="Value2"
                            Hidden="true">
                        </ext:Column>
                        <ext:CommandColumn ID="CommandColumn4" runat="server" Width="40" Sortable="false"
                            MenuDisabled="true" Align="Center">
                            <Commands>
                                <ext:CommandSeparator />
                                <ext:GridCommand Icon="PageWhitePut" CommandName="Download" ToolTip-Text="Download attachment" />
                            </Commands>
                            <PrepareToolbar Fn="prepareDownload" />
                            <Listeners>
                                <Command Handler="CommandDownload(command,record);" />
                            </Listeners>
                        </ext:CommandColumn>
                        <ext:CommandColumn ID="CommandColumn1" runat="server" Width="70" Sortable="false"
                            MenuDisabled="true" Text="" Align="Center">
                            <Commands>
                                <ext:CommandSeparator />
                                <ext:GridCommand Text="<i></i>View" CommandName="View" ToolTip-Text="View" />
                            </Commands>
                            <Listeners>
                                <Command Handler="CommandHandler(command,record);" />
                            </Listeners>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
                <Features>
                    <ext:GroupingSummary ID="Group1" runat="server" GroupHeaderTplString="{name}" HideGroupedHeader="true"
                        EnableGroupingMenu="false" />
                </Features>
                <%--<BottomBar>
                <ext:PagingToolbar ID="PagingToolbar2" runat="server" DisplayInfo="false">
                    <Items>
                      
                    </Items>
                </ext:PagingToolbar>
            </BottomBar>--%>
                <SelectionModel>
                    <ext:CheckboxSelectionModel ID="CheckboxSelectionModel1" runat="server" Mode="Multi" />
                </SelectionModel>
            </ext:GridPanel>
            <ext:GridPanel ID="GridPanel2" runat="server" Cls="itemgrid" Width="910" Hidden="true"
                Scroll="None">
                <Store>
                    <ext:Store ID="Store2" runat="server" GroupField="Tbl">
                        <Sorters>
                            <ext:DataSorter Property="SN" Direction="ASC" />
                        </Sorters>
                        <Model>
                            <ext:Model ID="Model1" runat="server" IDProperty="SN">
                                <Fields>
                                    <ext:ModelField Name="SN" />
                                    <ext:ModelField Name="Id" />
                                    <ext:ModelField Name="EmployeeId" />
                                    <ext:ModelField Name="Name" />
                                    <ext:ModelField Name="ModifiedOn" />
                                    <ext:ModelField Name="TableType" />
                                    <ext:ModelField Name="Tbl" />
                                    <ext:ModelField Name="Value1" />
                                    <ext:ModelField Name="UserFileName" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel ID="ColumnModel1" runat="server">
                    <Columns>
                        <ext:Column ID="Column1" runat="server" Text="Employee Name" Width="200" DataIndex="Name">
                        </ext:Column>
                        <ext:Column ID="Column2" runat="server" Text="Date" Width="100" DataIndex="ModifiedOn">
                        </ext:Column>
                        <ext:Column ID="Column3" runat="server" Text="Type" Width="230" DataIndex="TableType">
                        </ext:Column>
                        <ext:Column ID="Column4" runat="server" Text="Type" Width="10" DataIndex="Tbl" Hidden="true">
                        </ext:Column>
                        <ext:Column ID="Column5" runat="server" Text="Value" Width="250" DataIndex="Value1">
                        </ext:Column>
                        <ext:Column ID="Column6" runat="server" Text="Value" Width="250" DataIndex="Value2"
                            Hidden="true">
                        </ext:Column>
                        <ext:CommandColumn ID="CommandColumn3" runat="server" Width="40" Sortable="false"
                            MenuDisabled="true" Text="" Align="Center">
                            <Commands>
                                <ext:CommandSeparator />
                                <ext:GridCommand Icon="PageWhitePut" CommandName="Download" ToolTip-Text="Download attachment" />
                            </Commands>
                            <PrepareToolbar Fn="prepareDownload" />
                            <Listeners>
                                <Command Handler="CommandDownload(command,record);" />
                            </Listeners>
                        </ext:CommandColumn>
                        <ext:CommandColumn ID="CommandColumn2" runat="server" Width="70" Sortable="false"
                            MenuDisabled="true" Text="Actions" Align="Center">
                            <Commands>
                                <ext:CommandSeparator />
                                <ext:GridCommand Text="<i></i>View" CommandName="View" ToolTip-Text="View" />
                            </Commands>
                            <Listeners>
                                <Command Handler="CommandHandler(command,record);" />
                            </Listeners>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
                <Features>
                    <ext:GroupingSummary ID="GroupingSummary1" runat="server" GroupHeaderTplString="{name}"
                        HideGroupedHeader="true" EnableGroupingMenu="false" />
                </Features>
                <%--<BottomBar>
                <ext:PagingToolbar ID="PagingToolbar2" runat="server" DisplayInfo="false">
                    <Items>
                      
                    </Items>
                </ext:PagingToolbar>
            </BottomBar>--%>
                <SelectionModel>
                    <ext:CheckboxSelectionModel ID="CheckboxSelectionModel2" runat="server" Mode="Multi" />
                </SelectionModel>
            </ext:GridPanel>
            <div class="buttonBlock" style="width: 68%;">

            <table>
                <tr>
                    <td style="width:300px;">
                        
                <ext:Button ID="btnSave" runat="server" Cls="btn btn-success" Text="Approve" Width="120">
                    <DirectEvents>
                        <Click OnEvent="Button1_Click">
                            <EventMask ShowMask="true" />
                            <Confirmation ConfirmRequest="true" Message="Are you sure, you want to approve the profile?" />
                            <ExtraParams>
                                <ext:Parameter Name="gridItems" Value="Ext.encode(#{GridPanel1}.getRowsValues({selectedOnly:true}))"
                                    Mode="Raw" />
                            </ExtraParams>
                        </Click>
                    </DirectEvents>
                    <Listeners>
                        <Click Handler="valGroup = 'Save'; return CheckValidation();">
                        </Click>
                    </Listeners>
                </ext:Button>
                <ext:Button ID="btnSave2" runat="server" Cls="btn btn-success" Text="Approve" Hidden="true"
                    Width="120">
                    <DirectEvents>
                        <Click OnEvent="Button1_Click">
                            <EventMask ShowMask="true" />
                            <Confirmation ConfirmRequest="true" Message="Are you sure, you want to approve the profile?" />
                            <ExtraParams>
                                <ext:Parameter Name="gridItems" Value="Ext.encode(#{GridPanel2}.getRowsValues({selectedOnly:true}))"
                                    Mode="Raw" />
                            </ExtraParams>
                        </Click>
                    </DirectEvents>
                    <Listeners>
                        <Click Handler="valGroup = 'Save'; return CheckValidation();">
                        </Click>
                    </Listeners>
                </ext:Button>
                    </td>

                    <td>                    
                        
                <ext:Button ID="btnDelete1" runat="server" Cls="btn btn-save" Text="Delete" Width="120">
                    <DirectEvents>
                        <Click OnEvent="btnDelete_Click">
                            <EventMask ShowMask="true" />
                            <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the profile?" />
                            <ExtraParams>
                                <ext:Parameter Name="gridItems" Value="Ext.encode(#{GridPanel1}.getRowsValues({selectedOnly:true}))"
                                    Mode="Raw" />
                            </ExtraParams>
                        </Click>
                    </DirectEvents>
                   
                </ext:Button>
                <ext:Button ID="btnDelete2" runat="server" Cls="btn btn-save" Text="Delete" Hidden="true"
                    Width="120">
                    <DirectEvents>
                        <Click OnEvent="btnDelete_Click">
                            <EventMask ShowMask="true" />
                            <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the profile?" />
                            <ExtraParams>
                                <ext:Parameter Name="gridItems" Value="Ext.encode(#{GridPanel2}.getRowsValues({selectedOnly:true}))"
                                    Mode="Raw" />
                            </ExtraParams>
                        </Click>
                    </DirectEvents>
                    <Listeners>
                        <Click Handler="valGroup = 'Save'; return CheckValidation();">
                        </Click>
                    </Listeners>
                </ext:Button>

                    </td>
                </tr>
            </table>

                <br />
            </div>
            <br />
        </div>
    </div>
    <br />
    <ext:Window ID="WFamily" runat="server" Title="Family Details" Icon="Application"
         Width="650" Height="520" BodyPadding="5" Hidden="true" Modal="true">
        <Content>
            <ucF:FamilyPopup Id="ucFamily" runat="server" />
        </Content>
    </ext:Window>
    <ext:Window ID="AEEducationWindow" runat="server" Title="Education Details" Icon="Application"
        Height="600" Width="700" BodyPadding="5" Hidden="true" Modal="true">
        <Content>
            <ucE:EduPopup Id="ucEdu" runat="server" />
        </Content>
    </ext:Window>
    <ext:Window ID="AEPreviousEmploymentWindow" runat="server" Title="Previous Employment Details"
        Icon="Application" Height="500" Width="430" BodyPadding="5" Hidden="true" Modal="true">
        <Content>
            <ucE:EmployPopup Id="ucEmploymentCtrl" runat="server" />
        </Content>
    </ext:Window>
    <ext:Window ID="AETrainingWindow" runat="server" Title="Add/Edit Training" Icon="Application"
        Height="580" Width="700" BodyPadding="5" Hidden="true" Modal="true">
        <Content>
            <ucT:TrainingPop Id="trainCtrl" runat="Server" />
        </Content>
    </ext:Window>
    <ext:Window ID="AESeminarWindow" runat="server" Title="Seminar Details" Icon="Application"
        Height="340" Width="650" BodyPadding="5" Hidden="true" Modal="true">
        <Content>
            <ucSeminar:SemPoUp Id="seminarCtrl" runat="server" />
        </Content>
    </ext:Window>
    <ext:Window ID="AESkillSetsWindow" runat="server" Title="SkillSet Details" Icon="Application"
        Height="300" Width="450" BodyPadding="5" Hidden="true" Modal="true">
        <Content>
            <ucSkillSet:SkillSetPop Id="skillSetCtrl" runat="server" />
        </Content>
    </ext:Window>
    <ext:Window ID="AELanguageSetsWindow" runat="server" Title="Language Details" Icon="Application"
        Height="245" Width="450" BodyPadding="10" Hidden="true" Modal="true">
        <Content>
            <ucLang:LanguageSetPop Id="languageSetCtrl" runat="server" />
        </Content>
    </ext:Window>
    <ext:Window ID="AEPublicationWindow" runat="server" Title="Publication Details" Icon="Application"
        Height="285" Width="450" BodyPadding="5" Hidden="true" Modal="true">
        <Content>
            <ucPub:PublicationPop Id="publicationCtrl" runat="server" />
        </Content>
    </ext:Window>

    <ext:Window ID="AECitizenshipWindow" runat="server" Title="Citizenship Details" Icon="Application"
        Height="390" Width="550" BodyPadding="5" Hidden="true" Modal="true">
        <Content>
            <ucCtzn:Ctnship ID="ucCtznCtrl" runat="server" />          
        </Content>
    </ext:Window>

    <ext:Window ID="AEDrivingLiscenceWindow" runat="server" Title="Driving Liscence Details"
        Icon="Application" Height="420" Width="500" 
        BodyPadding="5" Hidden="true" Modal="true">
        <Content>
            <ucDrivLc:DrLic ID="ucDrivLicense" runat="server" />        
        </Content>
    </ext:Window>

    <ext:Window ID="AEPassportWindow" runat="server" Title="Passport Details" Icon="Application"
        Height="350" Width="500"  BodyPadding="5"
        Hidden="true" Modal="true">
        <Content>
           <ucPas:PassCtrl ID="ucPassport" runat="server" />
        </Content>
    </ext:Window>

     <ext:Window ID="AEDocument" runat="server" Title="Document Details" Icon="Application"
                Width="500" Height="310" BodyPadding="10" Hidden="true" Modal="true">
                <Content>                    
                    <ucDoc:docPopup Id="ucDocument" runat="server" />
                </Content>
            </ext:Window>

    <ext:Window ID="AEEAddress" runat="server" Title="Address Details" Icon="Application"
        Height="700" Width="1050" BodyPadding="5" Hidden="true" Modal="true" AutoScroll="true">
        <Content>
            <div style="">
                <div class="col-sm-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                Permanent Address</h4>
                        </div>
                        <div class="panel-body">
                            <table>
                                <tr>
                                    <td colspan="2">
                                        <ext:ComboBox ID="cmbCountryPerm" FieldLabel="Country " runat="server" LabelAlign="Top"
                                            Width="180" LabelSeparator="" QueryMode="Local" DisplayField="CountryName" ValueField="CountryId">
                                            <Store>
                                                <ext:Store ID="store3" runat="server">
                                                    <Fields>
                                                        <ext:ModelField Name="CountryId" Type="String" />
                                                        <ext:ModelField Name="CountryName" />
                                                    </Fields>
                                                </ext:Store>
                                            </Store>
                                        </ext:ComboBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 200px;">
                                        <ext:ComboBox ID="cmbZonePerm" FieldLabel="Zone " runat="server" LabelAlign="Top"
                                            DisplayField="Zone" Width="180" LabelSeparator="" QueryMode="Local" ValueField="ZoneId">
                                            <Store>
                                                <ext:Store ID="Store4" runat="server">
                                                    <Fields>
                                                        <ext:ModelField Name="Zone" />
                                                        <ext:ModelField Name="ZoneId" Type="String" />
                                                    </Fields>
                                                </ext:Store>
                                            </Store>
                                        </ext:ComboBox>
                                    </td>
                                    <td>
                                        <ext:ComboBox ID="cmbDistrictPerm" FieldLabel="District " runat="server" LabelAlign="Top"
                                            Width="180" LabelSeparator="" QueryMode="Local" DisplayField="District" ValueField="DistrictId">
                                            <Store>
                                                <ext:Store ID="storeGender" runat="server">
                                                    <Fields>
                                                        <ext:ModelField Name="District" />
                                                        <ext:ModelField Name="DistrictId" Type="String" />
                                                    </Fields>
                                                </ext:Store>
                                            </Store>
                                        </ext:ComboBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 200px;">
                                        <ext:TextField ID="cmbVDCPerm" FieldLabel="VDC / Municipality " runat="server" Width="180"
                                            LabelSeparator="" LabelAlign="Top">
                                        </ext:TextField>
                                    </td>
                                    <td>
                                        <ext:TextField ID="txtStreetColonyPerm" FieldLabel="Street / Colony " runat="server"
                                            LabelAlign="Top" Width="180" LabelSeparator="">
                                        </ext:TextField>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 200px;">
                                        <ext:TextField ID="txtWardNoPerm" FieldLabel="Ward No. " runat="server" LabelAlign="Top"
                                            Width="180" LabelSeparator="">
                                        </ext:TextField>
                                    </td>
                                    <td>
                                        <ext:TextField ID="txtHouseNoPerm" FieldLabel="House No. " runat="server" LabelAlign="Top"
                                            Width="180" LabelSeparator="">
                                        </ext:TextField>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <ext:TextArea ID="txtLocalityPerm" FieldLabel="Locality " runat="server" LabelAlign="Top"
                                            LabelSeparator="" Width="380">
                                        </ext:TextArea>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 200px;">
                                        <ext:TextField ID="txtStatePerm" FieldLabel="State  " runat="server" LabelAlign="Top"
                                            Width="180" LabelSeparator="">
                                        </ext:TextField>
                                    </td>
                                    <td>
                                        <ext:TextField ID="txtZipCodePerm" FieldLabel="Zip Code " runat="server" LabelAlign="Top"
                                            Width="180" LabelSeparator="">
                                        </ext:TextField>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <!-- panel -->
                </div>
                <div class="col-sm-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                Present Address</h4>
                        </div>
                        <div class="panel-body">
                            <table>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 200px;">
                                        <ext:ComboBox ID="cmbCountryTemp" FieldLabel="Country " runat="server" LabelAlign="Top"
                                            Width="180" LabelSeparator="" QueryMode="Local" DisplayField="CountryName" ValueField="CountryId">
                                            <Store>
                                                <ext:Store ID="store5" runat="server">
                                                    <Fields>
                                                        <ext:ModelField Name="CountryId" Type="String" />
                                                        <ext:ModelField Name="CountryName" />
                                                    </Fields>
                                                </ext:Store>
                                            </Store>
                                        </ext:ComboBox>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 200px;">
                                        <ext:ComboBox ID="cmbZoneTemp" FieldLabel="Zone " runat="server" LabelAlign="Top"
                                            DisplayField="Zone" Width="180" LabelSeparator="" QueryMode="Local" ValueField="ZoneId">
                                            <Store>
                                                <ext:Store ID="Store6" runat="server">
                                                    <Fields>
                                                        <ext:ModelField Name="Zone" />
                                                        <ext:ModelField Name="ZoneId" Type="String" />
                                                    </Fields>
                                                </ext:Store>
                                            </Store>
                                        </ext:ComboBox>
                                    </td>
                                    <td>
                                        <ext:ComboBox ID="cmbDistrictTemp" FieldLabel="District " runat="server" LabelAlign="Top"
                                            Width="180" LabelSeparator="" QueryMode="Local" DisplayField="District" ValueField="DistrictId">
                                            <Store>
                                                <ext:Store ID="store7" runat="server">
                                                    <Fields>
                                                        <ext:ModelField Name="DistrictId" Type="String" />
                                                        <ext:ModelField Name="District" />
                                                    </Fields>
                                                </ext:Store>
                                            </Store>
                                        </ext:ComboBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ext:TextField ID="cmbVDCTemp" FieldLabel="VDC / Municipality " runat="server" LabelAlign="Top"
                                            Width="180" LabelSeparator="">
                                        </ext:TextField>
                                    </td>
                                    <td>
                                        <ext:TextField ID="txtStreetColonyTemp" FieldLabel="Street / Colony " runat="server"
                                            LabelAlign="Top" Width="180" LabelSeparator="">
                                        </ext:TextField>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 200px;">
                                        <ext:TextField ID="txtWardNoTemp" FieldLabel="Ward No. " runat="server" LabelAlign="Top"
                                            Width="180" LabelSeparator="">
                                        </ext:TextField>
                                    </td>
                                    <td>
                                        <ext:TextField ID="txtHouseNoTemp" FieldLabel="House No. " runat="server" LabelAlign="Top"
                                            Width="180" LabelSeparator="">
                                        </ext:TextField>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <ext:TextArea ID="txtLocalityTemp" FieldLabel="Locality " runat="server" LabelAlign="Top"
                                            Width="375" LabelSeparator="">
                                        </ext:TextArea>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 200px;">
                                        <ext:TextField ID="txtStateTemp" FieldLabel="State " runat="server" LabelAlign="Top"
                                            Width="180" LabelSeparator="">
                                        </ext:TextField>
                                    </td>
                                    <td>
                                        <ext:TextField ID="txtZipCodeTemp" FieldLabel="Zip Code " runat="server" LabelAlign="Top"
                                            Width="180" LabelSeparator="">
                                        </ext:TextField>
                                    </td>
                                </tr>
                                 <tr>
                                            <td colspan="2">
                                                 <ext:TextField ID="txtCitIssuseDist" FieldLabel="Citizenship Issue Address " runat="server" LabelAlign="Top"
                                                    Width="180" LabelSeparator="">
                                                </ext:TextField>
                                            </td>
                                        </tr>
                            </table>
                        </div>
                        <!-- panel -->
                    </div>
                </div>
                <div class="col-sm-6" style="width: 100%;">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                Present Address</h4>
                        </div>
                        <div class="panel-body">
                            <table>
                                <tr>
                                    <td>
                                        <h5 style="width: 200px;">
                                            OFFICIAL CONTACT</h5>
                                    </td>
                                    <td>
                                        <h5 style="width: 200px;">
                                            PERSONAL CONTACT</h5>
                                    </td>
                                    <td>
                                        <h5 style="width: 200px;">
                                            EMERGENCY CONTACT</h5>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ext:TextField ID="txtEmailOfficial" FieldLabel="Email Address  " runat="server"
                                            LabelAlign="Top" TabIndex='1' Width="180" LabelSeparator="">
                                        </ext:TextField>
                                    </td>
                                    <td>
                                        <ext:TextField TabIndex='5' ID="txtEmailPersonal" FieldLabel="Email Address   " runat="server"
                                            LabelAlign="Top" Width="180" LabelSeparator="">
                                        </ext:TextField>
                                    </td>
                                    <td>
                                        <ext:TextField TabIndex='8' ID="txtNameEmergency" FieldLabel="Contact Name   " runat="server"
                                            LabelAlign="Top" Width="180" LabelSeparator="">
                                        </ext:TextField>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ext:TextField TabIndex='2' ID="txtPhoneOfficial" FieldLabel="Phone Number " runat="server"
                                            LabelAlign="Top" Width="180" LabelSeparator="">
                                        </ext:TextField>
                                    </td>
                                    <td>
                                        <ext:TextField TabIndex='6' ID="txtPhonePersonal" FieldLabel="Phone Number" runat="server"
                                            LabelAlign="Top" Width="180" LabelSeparator="">
                                        </ext:TextField>
                                    </td>
                                    <td>
                                        <ext:TextField TabIndex='9' ID="txtRelationEmergency" FieldLabel="Relation" runat="server"
                                            LabelAlign="Top" Width="180" LabelSeparator="">
                                        </ext:TextField>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ext:TextField TabIndex='3' ID="txtExtentionOfficial" FieldLabel="Extention  "
                                            runat="server" LabelAlign="Top" Width="180" LabelSeparator="">
                                        </ext:TextField>
                                    </td>
                                    <td>
                                        <ext:TextField TabIndex='7' ID="txtMobilePersonal" FieldLabel="Mobile Number " runat="server"
                                            LabelAlign="Top" Width="180" LabelSeparator="">
                                        </ext:TextField>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ControlToValidate="txtMobilePersonal"
                                            ValidationGroup="InsertUpdate" runat="server">
                                        </asp:RequiredFieldValidator>
                                    </td>
                                    <td>
                                        <ext:TextField TabIndex='10' ID="txtPhoneEmergency" FieldLabel="Phone  " runat="server"
                                            LabelAlign="Top" Width="180" LabelSeparator="">
                                        </ext:TextField>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ext:TextField TabIndex='4' ID="txtMobileOfficial" FieldLabel="Mobile Number " runat="server"
                                            LabelAlign="Top" Width="180" LabelSeparator="">
                                        </ext:TextField>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                        <ext:TextField TabIndex='11' ID="txtMobileEmergency" FieldLabel="Mobile Number "
                                            runat="server" LabelAlign="Top" Width="180" LabelSeparator="">
                                        </ext:TextField>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </Content>
    </ext:Window>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
