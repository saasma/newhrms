﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using Utils.Calendar;
using BLL.Base;

namespace Web.NewHR
{
    public partial class ChangeEngJoiningDateToNepali : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            //List<TempDesig> list
            //    = BLL.BaseBiz.PayrollDataContext.TempDesigs.ToList();

            //foreach (TempDesig item in list)
            //{

            //    item.NepDate = BLL.BaseBiz.GetAppropriateDate(item.Date.Value);

            //}

            //List<TempAppoint> list1
            //    = BLL.BaseBiz.PayrollDataContext.TempAppoints.ToList();

            //foreach (TempAppoint item in list1)
            //{
            //    if (item.AppointmentDateEng != null)
            //        item.AppointmentDate = BLL.BaseBiz.GetAppropriateDate(item.AppointmentDateEng.Value);

            //}

            //List<TempJoinDate> branchDepList = BLL.BaseBiz.PayrollDataContext.TempJoinDates
            //    .ToList();
            //foreach (TempJoinDate item in branchDepList)
            //{
            //    //if (item.LetterDateEng != null)
            //    //    item.LetterDate = BLL.BaseBiz.GetAppropriateDate(item.LetterDateEng.Value);

            //    //if (!string.IsNullOrEmpty( item.FromDate))
            //    //    item.FromDateEng = BLL.BaseBiz.GetEngDate(item.FromDate,IsEnglish);

            //    if (item.JoinDateEng != null)
            //    {
            //        item.JoinDate = BLL.BaseBiz.GetAppropriateDate(item.JoinDateEng.Value);
            //    }
            //    //if (item.DepartureDateEng != null)
            //    //    item.DepartureDate = BLL.BaseBiz.GetAppropriateDate(item.DepartureDateEng.Value);
            //}

            foreach (DAL.EmpRet item in BLL.BaseBiz.PayrollDataContext.EmpRets.ToList())
            {
                DateTime date = Convert.ToDateTime(item.DateEng);

                CustomDate d = new CustomDate(date.Day, date.Month, date.Year, true);
                item.Date = d.ToString();
            }
          

            BLL.BaseBiz.PayrollDataContext.SubmitChanges();
        }

        public void GenerateEnglishDateToEachMonthEnpMonth()
        {
            
            for (int year = 2040; year <= 2100; year++)
            {
                for (int month = 1; month <= 12; month++)
                {

                    CustomDate date = new CustomDate(1, month, year, false);
                    date = CustomDate.ConvertNepToEng(date);

                    Response.Write("<br>nepToEng[" + year.ToString() + month.ToString() + "]=" +
                        "'" + date.EnglishDate.Year + "/" + date.EnglishDate.Month + "/" + date.EnglishDate.Day + "';");
                }
            }
        }

        public DateTime FirstDay(int month, int year)
        {
            var date = new DateTime(year, month, 1);
            while (true)
            {
                if (date.DayOfWeek == DayOfWeek.Sunday)
                    return date;
                date = date.AddDays(-1);
            }

        }

        public void GenerateNepaliStartMonth()
        {
            int day = 1, month = 1, year = 2012;

            int total = 200;

            DateTime date = new DateTime(year, month, day);
            for (int i = 1; i <= total; i++)
            {

                date = FirstDay(date.Month, date.Year);

                CustomDate nepDate = new CustomDate(date.Day, date.Month, date.Year, true);
                nepDate = CustomDate.ConvertEngToNep(nepDate);

                int nextMonth = nepDate.Month + 1;
                if (nextMonth > 12)
                    nextMonth = 1;

                Response.Write("<br>engToNep[" + date.Day.ToString() + date.Month.ToString() + date.Year.ToString() + "]=" +
                    "'" + nepDate.Day + "/" + DateHelper.GetTotalDaysInTheMonth(nepDate.Year,nepDate.Month,false) + "/" 
                    +
                    DateHelper.GetMonthName(nepDate.Month, false).Remove(3) + "-" + DateHelper.GetMonthName(nextMonth, false).Remove(3)
                     + " " + nepDate.Year
                    + "';");



                date = date.AddDays(45);
            }

        }
        private void NewMethod()
        {
            List<EEmployee> branchList = BLL.BaseBiz.PayrollDataContext.EEmployees.ToList();

            foreach (EEmployee item in branchList)
            {
                //if (string.IsNullOrEmpty(item.Date))
                {



                    item.SeparateCombinedName();
                }
            }

            BLL.BaseBiz.PayrollDataContext.SubmitChanges();

            Response.Write("Date conversion successfull.");
        }
    }
}