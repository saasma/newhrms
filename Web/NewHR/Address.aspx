﻿<%@ Page Title="Employee Address" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="Address.aspx.cs" Inherits="Web.NewHR.Address" %>

<%@ Register Src="~/NewHR/UserControls/EmployeeDetailsCtl.ascx" TagName="EmployeeDetailsCtl"
    TagPrefix="uc1" %>
<%@ Register Src="~/newhr/UserControls/EmployeeWizard.ascx" TagName="EmployeeWizard"
    TagPrefix="uc2" %>
<%@ Register Src="~/NewHR/UserControls/EmpDetailsLeftMenu.ascx" TagName="EmpDetailsWizard"
    TagPrefix="ucEW" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .menu
        {
            height: 50px;
        }
        
        .RowTable
        {
            margin-left: 0px;
            clear: both;
        }
        
        .menu ul
        {
            list-style: none;
            margin-left: 5px;
        }
        
        .menu li
        {
            display: inline;
            float: left;
            width: 200px;
            margin-left: -5px;
        }
        .userDetailsClass
        {
            background-color:#D8E7F3; 
            height:50px; text-align:center; 
            padding-top:10px; 
            margin-bottom:10px;
            margin-left:8px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <ext:Hidden ID="Hidden_CustomerID" runat="server">
    </ext:Hidden>
    <div class="pageheader">
        <div class="media">
            <div class="media-body" style="margin-left:145px;">
                <h4>
                    Address
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="innerLR">
            <div class="separator bottom">
            </div>
            
            <%--<uc2:EmployeeWizard Id="EmployeeWizard1" runat="server" />--%>
            
            <table>
                <tr>
                    <td valign="top">
                        <div style="float: left; margin-right: 15px;">
                            <ucEW:EmpDetailsWizard Id="EmployeeWizard1" runat="server" />
                        </div>
                    </td>
                    <td valign="top">
                        
                        <div class="userDetailsClass">
                            <uc1:EmployeeDetailsCtl Id="EmployeeDetailsCtl1" runat="server" />
                          </div>

                          <div style="padding-left:10px;">
                          <ext:Label ID="lblMsg" runat="server" /></div>
                        <div class="col-sm-6">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        Permanent Address</h4>
                                </div>
                                <div class="panel-body">
                                    <table>
                                        <tr>
                                            <td colspan="2">
                                                <ext:ComboBox ID="cmbCountryPerm" FieldLabel="Country " runat="server" LabelAlign="Top"
                                                    Width="180" LabelSeparator="" QueryMode="Local" DisplayField="CountryName" ValueField="CountryId">
                                                    <Store>
                                                        <ext:Store ID="store2" runat="server">
                                                            <Fields>
                                                                <ext:ModelField Name="CountryId" />
                                                                <ext:ModelField Name="CountryName" />
                                                            </Fields>
                                                        </ext:Store>
                                                    </Store>
                                                </ext:ComboBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 200px;">
                                                <ext:ComboBox ID="cmbZonePerm" FieldLabel="Zone " runat="server" LabelAlign="Top"
                                                    DisplayField="Zone" Width="180" LabelSeparator="" QueryMode="Local" ValueField="ZoneId">
                                                    <Store>
                                                        <ext:Store ID="Store1" runat="server">
                                                            <Fields>
                                                                <ext:ModelField Name="Zone" />
                                                                <ext:ModelField Name="ZoneId" />
                                                            </Fields>
                                                        </ext:Store>
                                                    </Store>
                                                    <DirectEvents>
                                                        <Select OnEvent="ZoneChangePerm">
                                                        </Select>
                                                    </DirectEvents>
                                                </ext:ComboBox>
                                            </td>
                                            <td>
                                                <ext:ComboBox ID="cmbDistrictPerm" FieldLabel="District " runat="server" LabelAlign="Top"
                                                    Width="180" LabelSeparator="" QueryMode="Local" DisplayField="District" ValueField="DistrictId">
                                                    <Store>
                                                        <ext:Store ID="storeGender" runat="server">
                                                            <Fields>
                                                                <ext:ModelField Name="District" />
                                                                <ext:ModelField Name="DistrictId" />
                                                            </Fields>
                                                        </ext:Store>
                                                    </Store>
                                                </ext:ComboBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 200px;">
                                                <ext:TextField ID="cmbVDCPerm" FieldLabel="VDC / Municipality " runat="server" Width="180"
                                                    LabelSeparator="" LabelAlign="Top">
                                                </ext:TextField>
                                            </td>
                                            <td>
                                                <ext:TextField ID="txtStreetColonyPerm" FieldLabel="Street / Colony " runat="server"
                                                    LabelAlign="Top" Width="180" LabelSeparator="">
                                                </ext:TextField>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 200px;">
                                                <ext:TextField ID="txtWardNoPerm" FieldLabel="Ward No. " runat="server" LabelAlign="Top"
                                                    Width="180" LabelSeparator="">
                                                </ext:TextField>
                                            </td>
                                            <td>
                                                <ext:TextField ID="txtHouseNoPerm" FieldLabel="House No. " runat="server" LabelAlign="Top"
                                                    Width="180" LabelSeparator="">
                                                </ext:TextField>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <ext:TextArea ID="txtLocalityPerm" FieldLabel="Locality " runat="server" LabelAlign="Top"
                                                    LabelSeparator="" Width="380">
                                                </ext:TextArea>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 200px;">
                                                <ext:TextField ID="txtStatePerm" FieldLabel="State  " runat="server" LabelAlign="Top"
                                                    Width="180" LabelSeparator="">
                                                </ext:TextField>
                                            </td>
                                            <td>
                                                <ext:TextField ID="txtZipCodePerm" FieldLabel="Zip Code " runat="server" LabelAlign="Top"
                                                    Width="180" LabelSeparator="">
                                                </ext:TextField>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <!-- panel -->
                        </div>
                        <div class="col-sm-6">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        Present Address</h4>
                                </div>
                                <div class="panel-body">
                                    <table>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                                <ext:LinkButton Icon="ArrowRight" runat="server" ID="btnCopyAbove" Text="Copy">
                                                    <DirectEvents>
                                                        <Click OnEvent="CopyButton_Click">
                                                            <EventMask ShowMask="true" />
                                                        </Click>
                                                    </DirectEvents>
                                                </ext:LinkButton>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 200px;">
                                                <ext:ComboBox ID="cmbCountryTemp" FieldLabel="Country " runat="server" LabelAlign="Top"
                                                    Width="180" LabelSeparator="" QueryMode="Local" DisplayField="CountryName" ValueField="CountryId">
                                                    <Store>
                                                        <ext:Store ID="store4" runat="server">
                                                            <Fields>
                                                                <ext:ModelField Name="CountryId" />
                                                                <ext:ModelField Name="CountryName" />
                                                            </Fields>
                                                        </ext:Store>
                                                    </Store>
                                                </ext:ComboBox>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 200px;">
                                                <ext:ComboBox ID="cmbZoneTemp" FieldLabel="Zone " runat="server" LabelAlign="Top"
                                                    DisplayField="Zone" Width="180" LabelSeparator="" QueryMode="Local" ValueField="ZoneId">
                                                    <Store>
                                                        <ext:Store ID="Store5" runat="server">
                                                            <Fields>
                                                                <ext:ModelField Name="Zone" />
                                                                <ext:ModelField Name="ZoneId" />
                                                            </Fields>
                                                        </ext:Store>
                                                    </Store>
                                                    <DirectEvents>
                                                        <Select OnEvent="ZoneChangeTemp">
                                                        </Select>
                                                    </DirectEvents>
                                                </ext:ComboBox>
                                            </td>
                                            <td>
                                                <ext:ComboBox ID="cmbDistrictTemp" FieldLabel="District " runat="server" LabelAlign="Top"
                                                    Width="180" LabelSeparator="" QueryMode="Local" DisplayField="District" ValueField="DistrictId">
                                                    <Store>
                                                        <ext:Store ID="store6" runat="server">
                                                            <Fields>
                                                                <ext:ModelField Name="DistrictId" />
                                                                <ext:ModelField Name="District" />
                                                            </Fields>
                                                        </ext:Store>
                                                    </Store>
                                                </ext:ComboBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <ext:TextField ID="cmbVDCTemp" FieldLabel="VDC / Municipality " runat="server" LabelAlign="Top"
                                                    Width="180" LabelSeparator="">
                                                </ext:TextField>
                                            </td>
                                            <td>
                                                <ext:TextField ID="txtStreetColonyTemp" FieldLabel="Street / Colony " runat="server"
                                                    LabelAlign="Top" Width="180" LabelSeparator="">
                                                </ext:TextField>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 200px;">
                                                <ext:TextField ID="txtWardNoTemp" FieldLabel="Ward No. " runat="server" LabelAlign="Top"
                                                    Width="180" LabelSeparator="">
                                                </ext:TextField>
                                            </td>
                                            <td>
                                                <ext:TextField ID="txtHouseNoTemp" FieldLabel="House No. " runat="server" LabelAlign="Top"
                                                    Width="180" LabelSeparator="">
                                                </ext:TextField>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <ext:TextArea ID="txtLocalityTemp" FieldLabel="Locality " runat="server" LabelAlign="Top"
                                                    Width="375" LabelSeparator="">
                                                </ext:TextArea>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 200px;">
                                                <ext:TextField ID="txtStateTemp" FieldLabel="State " runat="server" LabelAlign="Top"
                                                    Width="180" LabelSeparator="">
                                                </ext:TextField>
                                            </td>
                                            <td>
                                                <ext:TextField ID="txtZipCodeTemp" FieldLabel="Zip Code " runat="server" LabelAlign="Top"
                                                    Width="180" LabelSeparator="">
                                                </ext:TextField>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                 <ext:TextField ID="txtCitIssuseDist" FieldLabel="Citizenship Issue Address " runat="server" LabelAlign="Top"
                                                    Width="180" LabelSeparator="">
                                                </ext:TextField>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <!-- panel -->
                            </div>
                        </div>
                        <div class="col-sm-6" style="width: 100%;">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        Present Address</h4>
                                </div>
                                <div class="panel-body">
                                    <table>
                                        <tr>
                                            <td>
                                                <h5 style="width: 200px;">
                                                    OFFICIAL CONTACT</h5>
                                            </td>
                                            <td>
                                                <h5 style="width: 200px;">
                                                    PERSONAL CONTACT</h5>
                                            </td>
                                            <td>
                                                <h5 style="width: 200px;">
                                                    EMERGENCY CONTACT</h5>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <ext:TextField ID="txtEmailOfficial" FieldLabel="Email Address  " runat="server"
                                                    LabelAlign="Top" TabIndex='1' Width="180" LabelSeparator="">
                                                </ext:TextField>
                                            </td>
                                            <td>
                                                <ext:TextField TabIndex='5' ID="txtEmailPersonal" FieldLabel="Email Address   " runat="server"
                                                    LabelAlign="Top" Width="180" LabelSeparator="">
                                                </ext:TextField>
                                            </td>
                                            <td>
                                                <ext:TextField TabIndex='8' ID="txtNameEmergency" FieldLabel="Contact Name   " runat="server"
                                                    LabelAlign="Top" Width="180" LabelSeparator="">
                                                </ext:TextField>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <ext:TextField TabIndex='2' ID="txtPhoneOfficial" FieldLabel="Phone Number " runat="server"
                                                    LabelAlign="Top" Width="180" LabelSeparator="">
                                                </ext:TextField>
                                            </td>
                                            <td>
                                                <ext:TextField TabIndex='6' ID="txtPhonePersonal" FieldLabel="Phone Number" runat="server"
                                                    LabelAlign="Top" Width="180" LabelSeparator="">
                                                </ext:TextField>
                                            </td>
                                            <td>
                                                <ext:TextField TabIndex='9' ID="txtRelationEmergency" FieldLabel="Relation" runat="server"
                                                    LabelAlign="Top" Width="180" LabelSeparator="">
                                                </ext:TextField>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <ext:TextField TabIndex='3' ID="txtExtentionOfficial" FieldLabel="Extention  "
                                                    runat="server" LabelAlign="Top" Width="180" LabelSeparator="">
                                                </ext:TextField>
                                            </td>
                                            <td>
                                                <ext:TextField TabIndex='7' ID="txtMobilePersonal" FieldLabel="Mobile Number " runat="server"
                                                    LabelAlign="Top" Width="180" LabelSeparator="">
                                                </ext:TextField>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ControlToValidate="txtMobilePersonal"
                                                    ValidationGroup="InsertUpdate" runat="server">
                                                </asp:RequiredFieldValidator>
                                            </td>
                                            <td>
                                                <ext:TextField TabIndex='10' ID="txtPhoneEmergency" FieldLabel="Phone  " runat="server"
                                                    LabelAlign="Top" Width="180" LabelSeparator="">
                                                </ext:TextField>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <ext:TextField TabIndex='4' ID="txtMobileOfficial" FieldLabel="Mobile Number " runat="server"
                                                    LabelAlign="Top" Width="180" LabelSeparator="">
                                                </ext:TextField>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                                <ext:TextField TabIndex='11' ID="txtMobileEmergency" FieldLabel="Mobile Number "
                                                    runat="server" LabelAlign="Top" Width="180" LabelSeparator="">
                                                </ext:TextField>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <ext:Button ID="btnNext" runat="server" Cls="btn btn-primary" Width="100px" Text="Save"
                                                    StyleSpec="margin-top:20px;" Height="35">
                                                    <DirectEvents>
                                                        <Click OnEvent="ButtonNext_Click">
                                                            <EventMask ShowMask="true" />
                                                        </Click>
                                                    </DirectEvents>
                                                </ext:Button>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
