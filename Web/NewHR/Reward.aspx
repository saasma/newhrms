﻿<%@ Page Title="Reward" Language="C#" MasterPageFile="~/Master/HR.Master" AutoEventWireup="true"
    CodeBehind="Reward.aspx.cs" Inherits="Web.NewHR.UserControls.Reward" %>

<%@ Register Src="~/NewHR/UserControls/EmployeeDetailsCtl.ascx" TagName="EmployeeDetailsCtl"
    TagPrefix="uc1" %>
<%@ Register Src="~/NewHR/UserControls/EmploymentHistoryCtl.ascx" TagName="EmploymentHistoryCtl"
    TagPrefix="ucEmploymentHistoryCtl" %>
<%@ Register Src="UserControls/EmployeeBlockDetails.ascx" TagName="EmployeeBlockDetails"
    TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <ext:Hidden runat="server" ID="hdnID" Text="">
    </ext:Hidden>
    <div class="innerLR">
        <div class="separator bottom">
        </div>
        <h3 class="heading">
            Reward</h3>
        <ext:Store ID="storeEmployee" runat="server">
            <Model>
                <ext:Model IDProperty="EmployeeId" runat="server">
                    <Fields>
                        <ext:ModelField Name="Name" />
                        <ext:ModelField Name="EmployeeId" Type="String" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
        <ext:ComboBox QueryMode="Local" LabelWidth="120" FieldLabel="Select Employee" ID="ddlEmployee"
            Width="300px" ForceSelection="true" runat="server" StoreID="storeEmployee" Editable="true"
            DisplayField="Name" ValueField="EmployeeId" Mode="Local" EmptyText="">
            <DirectEvents>
                <Select OnEvent="ddlEmployee_Select">
                    <EventMask ShowMask="true" />
                </Select>
            </DirectEvents>
        </ext:ComboBox>
        <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator4" runat="server"
            ValidationGroup="SaveUpdate" ControlToValidate="ddlEmployee" ErrorMessage="Employee is required." />
        <div class="widget" style="margin-top: 15px">
            <div class="widget-body">
                <uc2:EmployeeBlockDetails Id="employeeBlock" runat="server" />
                <table style="margin-top: 10px;" class="fieldTable firsttdskip">
                    <tr>
                        <td>
                            <ext:TextField ID="txtLetterNo" FieldLabel="Letter No" runat="server" LabelAlign="Top"
                                Width="180" LabelSeparator="">
                            </ext:TextField>
                            <%-- <asp:RequiredFieldValidator ErrorMessage="DOB is required." Display="None" ID="RequiredFieldValidator9"
                                ControlToValidate="txtDOBEng" ValidationGroup="InsertUpdate" runat="server">
                            </asp:RequiredFieldValidator>--%>
                        </td>
                        <td>
                            <pr:CalendarExtControl Width="180px" FieldLabel="Letter Date" ID="calLetterDate"
                                runat="server" LabelAlign="Top" LabelSeparator="" />
                        </td>
                        <td>
                            <pr:CalendarExtControl Width="180px" FieldLabel="Reward Date" ID="calRewardDate"
                                runat="server" LabelAlign="Top" LabelSeparator="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ext:ComboBox QueryMode="Local" LabelAlign="Top" FieldLabel="Recommended By" ID="cmbRecommendedBy"
                                Width="180px" ForceSelection="true" runat="server" StoreID="storeEmployee" Editable="true"
                                DisplayField="Name" ValueField="EmployeeId" Mode="Local" EmptyText="">
                            </ext:ComboBox>
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <ext:ComboBox Width="74px" QueryMode="Local" ID="cmbType" ForceSelection="true" runat="server"
                                            LabelAlign="Top" FieldLabel="Type">
                                            <Items>
                                                <ext:ListItem Text="Cash" Value="1" />
                                                <ext:ListItem Text="Grade" Value="2" />
                                            </Items>
                                            <Listeners>
                                                <Select Handler="if(this.getValue()=='1') #{calEffectiveFrom}.hide(); else #{calEffectiveFrom}.show();" />
                                            </Listeners>
                                            <SelectedItems>
                                                <ext:ListItem Index="0" />
                                            </SelectedItems>
                                        </ext:ComboBox>
                                    </td>
                                    <td style="padding-left: 5px">
                                        <ext:NumberField ID="txtReward" MinValue="0" Width="100" runat="server" FieldLabel="Reward"
                                            LabelAlign="Top">
                                        </ext:NumberField>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <pr:CalendarExtControl Hidden="true" Width="180px" FieldLabel="Effective From" ID="calEffectiveFrom"
                                runat="server" LabelAlign="Top" LabelSeparator="" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <ext:TextArea ID="txtRewardGrantedFor" FieldLabel="Reward granted for " runat="server"
                                LabelAlign="Top" LabelSeparator="" Width="375">
                            </ext:TextArea>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <ext:TextArea ID="txtNotes" FieldLabel="Notes" runat="server" LabelAlign="Top" LabelSeparator=""
                                Width="375">
                            </ext:TextArea>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ext:ComboBox Disabled="true" ID="cmbCurrentLevel" Width="180px" runat="server" ValueField="LevelId"
                                DisplayField="Name" FieldLabel="Level *" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                                QueryMode="Local">
                                <Store>
                                    <ext:Store ID="Store4" runat="server">
                                        <Model>
                                            <ext:Model ID="Model4" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="LevelId" Type="String" />
                                                    <ext:ModelField Name="Name" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                            </ext:ComboBox>
                            <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator3" runat="server"
                                ValidationGroup="SaveUpdate" ControlToValidate="cmbCurrentLevel" ErrorMessage="Current level is required." />
                        </td>
                        <td>
                            <ext:ComboBox Disabled="true" ID="cmbCurrentPost" Width="180px" runat="server" ValueField="DesignationId"
                                DisplayField="Name" FieldLabel="Current Post *" LabelAlign="top" LabelSeparator=""
                                ForceSelection="true" QueryMode="Local">
                                <Store>
                                    <ext:Store ID="Store1" runat="server">
                                        <Model>
                                            <ext:Model ID="Model1" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="DesignationId" Type="String" />
                                                    <ext:ModelField Name="Name" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                            </ext:ComboBox>
                            <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator1" runat="server"
                                ValidationGroup="SaveUpdate" ControlToValidate="cmbCurrentPost" ErrorMessage="Current post is required." />
                        </td>
                    </tr>
                </table>
                <div class="buttonBlock buttonLeft">
                    <ext:LinkButton runat="server" StyleSpec="padding:0px;margin-top:20px;" ID="btnSaveUpdate"
                        Cls="btnFlat" BaseCls="btnFlat" Text="<i></i>Save" runat="server">
                        <DirectEvents>
                            <Click OnEvent="btnSaveUpdate_Click">
                                <EventMask ShowMask="true" />
                            </Click>
                        </DirectEvents>
                        <Listeners>
                            <Click Handler="valGroup = 'SaveUpdate'; return CheckValidation();">
                            </Click>
                        </Listeners>
                    </ext:LinkButton>
                    <div class="btnFlatOr">
                        or</div>
                    <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="LinkButton2" Cls="btnFlatLeftGap"
                        Text="<i></i>Cancel">
                        <Listeners>
                            <Click Handler="window.location = 'RewardList.aspx';">
                            </Click>
                        </Listeners>
                    </ext:LinkButton>
                </div>
                <div style="clear: both">
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
