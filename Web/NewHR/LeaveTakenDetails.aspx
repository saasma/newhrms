﻿<%@ Page Title="Leave Taken Details" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="LeaveTakenDetails.aspx.cs" Inherits="Web.NewHr.LeaveTakenDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">




        function SelEmpSearch() {

        }

        var renderValue = function (value) {
            if (parseFloat(value) == 0)
                return '-';
            return value;
        }

    </script>
    <style type="text/css">
        .x-grid-cell-inner
        {
            padding: 5px 5px 5px 5px;
        }
        .x-column-header-inner
        {
            padding: 7px 5px 7px 5px;
        }
        .x-column-header
        {
            font: bold 12px/15px helvetica,arial,verdana,sans-serif !important;
        }
        .x-panel-default
        {
                border-color: lightgray!important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <ext:Hidden ID="hdnEmployeeId" runat="server" />
    <ext:Hidden ID="hdnBranchDepartmentId" runat="server" />
    <ext:Hidden ID="hdnMsg" runat="server" />
    <ext:Hidden ID="hdnEmpSearch" runat="server" />
    <ext:Hidden ID="hdnSortBy" runat="server" />
  
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4 runat="server" id="lbl">
                    Leave Taken Details
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel" style="margin-top: 0px !important; padding-top: 0px !important;">
        <div class="innerLR">
            <%--<ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                <Proxy>
                    <ext:AjaxProxy Url="../Handler/EmpSearchID.ashx">
                        <ActionMethods Read="GET" />
                        <Reader>
                            <ext:JsonReader Root="plants" TotalProperty="total" />
                        </Reader>
                    </ext:AjaxProxy>
                </Proxy>
                <Model>
                    <ext:Model ID="Model5" IDProperty="EmployeeId" Name="ItemLineModel" runat="server">
                        <Fields>
                            <ext:ModelField Name="Name" Type="String" />
                            <ext:ModelField Name="EmployeeId" Type="String" />
                        </Fields>
                    </ext:Model>
                </Model>
            </ext:Store>--%>
           <%-- <div class="alert alert-info" style="margin-top: 0px; padding-top: 0px!important;">
            </div>--%>
            <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridBranchTransfer" runat="server"
                AutoScroll="true">
                <Store>
                    <ext:Store ID="storeEmpList" runat="server" AutoLoad="true">
                        <Model>
                            <ext:Model ID="Model1" runat="server">
                                <Fields>
                                    <ext:ModelField Name="EmployeeId" Type="String" />
                                    <ext:ModelField Name="Name" Type="String" />
                                    <ext:ModelField Name="LeaveType" Type="String" />
                                    <ext:ModelField Name="FromDateEng" Type="Date" />
                                    <ext:ModelField Name="ToDateEng" Type="String" />
                                    <ext:ModelField Name="DaysOrHours" Type="String" />
                                    <ext:ModelField Name="IsHalfDayLeave" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Sorters>
                            <ext:DataSorter Property="Name" Direction="ASC" />
                        </Sorters>
                    </ext:Store>
                </Store>
                <ColumnModel ID="ColumnModel1" runat="server">
                    <Columns>
                        <ext:Column ID="colFromDate" runat="server" Text="EIN" Locked="true" DataIndex="EmployeeId"
                            MenuDisabled="false" Sortable="false" Align="Center" Width="50" />
                        <ext:Column ID="Column1" runat="server" Text="Name" Locked="true" DataIndex="Name"
                            MenuDisabled="false" Sortable="true" Align="Left" Width="150" />
                        <ext:Column ID="Column2" runat="server" Text="Leave" Locked="true" DataIndex="LeaveType"
                            MenuDisabled="false" Sortable="true" Align="Left" Width="150" />
                        <ext:DateColumn ID="Column3" runat="server" Text="From Date" Locked="true" DataIndex="FromDateEng"
                            MenuDisabled="false" Sortable="true" Align="Left" Width="150" Format="yyyy/MMM/dd" />
                        <ext:DateColumn ID="Column31" runat="server" Text="To Date" Locked="true" DataIndex="ToDateEng"
                            MenuDisabled="false" Sortable="true" Align="Left" Width="150" Format="yyyy/MMM/dd" />
                        <ext:Column ID="Column4" runat="server" Text="Days" Locked="true" DataIndex="DaysOrHours"
                            MenuDisabled="false" Sortable="true" Align="Center" Width="50" />
                        <ext:Column ID="Column5" runat="server" Text="Half Day" Locked="true" DataIndex="IsHalfDayLeave"
                            MenuDisabled="false" Sortable="true" Align="Center" Width="80" />
                    </Columns>
                </ColumnModel>
            </ext:GridPanel>
        </div>
        <div class="alert alert-info" style="margin-top: 15px; height: 50px;">
          
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
