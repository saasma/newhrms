﻿<%@ Page Title="Training Type List" Language="C#" MasterPageFile="~/Master/HR.Master" AutoEventWireup="true" CodeBehind="TrainingTypeList.aspx.cs" Inherits="Web.NewHR.TrainingTypeList" %>

<%@ Register Src="~/NewHR/UserControls/ListWizard.ascx" TagName="listWizard" TagPrefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
    
    var CommandHandler = function(command, record){
            <%= hiddenValue.ClientID %>.setValue(record.data.TrainingTypeId);
            
                if(command=="Edit")
                {
                    <%= btnEdit.ClientID %>.fireEvent('click');
                }
                else
                {
                    <%= btnDelete.ClientID %>.fireEvent('click');
                }

             }

    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <ext:Hidden ID="hiddenValue" runat="server" />
    <ext:LinkButton ID="btnEdit" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnEdit_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:LinkButton ID="btnDelete" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnDelete_Click">
                <EventMask ShowMask="true" />
                <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the training type?" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <div class="separator bottom">
    </div>
    <div class="innerLR">
        
        <div class="widget">
            <div class="widget-head">
                <h4 class="heading">
                    Training Type List</h4>
            </div>
            <div class="widget-body">
                <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridTrainingType" runat="server"
                    Cls="itemgrid">
                    <Store>
                        <ext:Store ID="Store1" runat="server">
                            <Model>
                                <ext:Model ID="Model4" runat="server" IDProperty="TrainingTypeId">
                                    <Fields>
                                        <ext:ModelField Name="TrainingTypeId" Type="String" />
                                        <ext:ModelField Name="TrainingTypeName" Type="string" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                    </Store>
                    <ColumnModel>
                        <Columns>
                            <ext:Column ID="colID" Sortable="false" MenuDisabled="true" runat="server" Text="Training Type Id"
                                Align="Center" Width="120" DataIndex="TrainingTypeId" />
                            <ext:Column ID="Column5" Sortable="false" MenuDisabled="true" runat="server" Text="Training Type Name"
                                Align="Left" Width="200" DataIndex="TrainingTypeName" />
                            <ext:CommandColumn ID="CommandColumn1" runat="server" Width="75" Text="Actions" Align="Center">
                                <Commands>
                                    <ext:CommandSeparator />
                                    <ext:GridCommand Text="<i></i>" Cls="btn-action glyphicons pencil btn-success" CommandName="Edit" ToolTip-Text="Edit"/>
                                    <ext:GridCommand Text="<i></i>" Cls="btn-action glyphicons remove_2 btn-danger" CommandName="Delete" ToolTip-Text="Delete" />
                                </Commands>
                                <Listeners>
                                    <Command Handler="CommandHandler(command,record);" />
                                </Listeners>
                            </ext:CommandColumn>
                        </Columns>
                    </ColumnModel>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                    </SelectionModel>
                </ext:GridPanel>
                <div style="clear: both;" />
                <div class="buttonBlock" runat="server" id="buttonBlock">
                    <ext:LinkButton runat="server" Cls="btnFlat" BaseCls="btnFlat" StyleSpec="padding:0px;"
                        ID="btnAddNew" Text="<i></i>Add New">
                        <DirectEvents>
                            <Click OnEvent="btnAddNew_Click">
                                <EventMask ShowMask="true" />
                            </Click>
                        </DirectEvents>
                    </ext:LinkButton>
                </div>
            </div>
        </div>
        <br />
    </div>

    <ext:Window ID="WTrainingType" runat="server" Title="Add/Edit Training Type" Icon="Application"
        Width="300" Height="200" BodyPadding="5" Hidden="true" Modal="true">
        <Content>
            <table class="fieldTable">
                <tr>
                    <td>
                        <ext:TextField ID="txtName" LabelSeparator="" runat="server" FieldLabel="Name *"
                            LabelWidth="60" Width="250" LabelAlign="Left">
                        </ext:TextField>
                        <asp:RequiredFieldValidator Display="None" ID="rfvName" runat="server" ValidationGroup="SaveUpdTrainingType"
                            ControlToValidate="txtName" ErrorMessage="Name is required." />
                    </td>
                </tr>
                <tr>
                    <td valign="bottom">
                        <div class="popupButtonDiv">
                            <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="btnSave" Cls="btnFlat"
                                BaseCls="btnFlat" Text="<i></i>Save">
                                <DirectEvents>
                                    <Click OnEvent="btnSave_Click">
                                        <EventMask ShowMask="true" />
                                    </Click>
                                </DirectEvents>
                                <Listeners>
                                    <Click Handler="valGroup = 'SaveUpdTrainingType'; if(CheckValidation()) return this.disable(); else return false;">
                                    </Click>
                                </Listeners>
                            </ext:LinkButton>
                            <div class="btnFlatOr">
                                or</div>
                            <ext:LinkButton runat="server" StyleSpec="padding:0px;" Cls="btnFlatLeftGap" ID="LinkButton1"
                                Text="<i></i>Cancel">
                                <Listeners>
                                    <Click Handler="#{WTrainingType}.hide();">
                                    </Click>
                                </Listeners>
                            </ext:LinkButton>
                        </div>
                    </td>
                </tr>
            </table>
        </Content>
    </ext:Window>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
