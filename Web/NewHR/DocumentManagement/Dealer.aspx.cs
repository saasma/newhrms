﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Entity;
using BLL;
using System.IO;
using Utils;
using System.Text;
using BLL.BO;
namespace Web.NewHR.DocumentManagement
{
    public partial class Dealer : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.DocumentManagement;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }

            string eventTarget = Request.Form["__EVENTTARGET"];
            if (eventTarget != null && eventTarget.Equals("ReloadOtherDoc"))
                DownloadFile();

            if (eventTarget != null && eventTarget.Equals("ReloadfromIncomeGridOther"))
            {
                DownloadFile();                
            }
        }

        private void HideIfNoPermission()
        {
            if (!DocumentManager.CanUserAddEditDocument())
            {
                btnSave.Hide();
                btnAdd.Hide();
                btnAddIncome.Hide();
                CCRenew.Hide();
            }

            if (!DocumentManager.CanUserDeleteDocument())
            {
                CCAgreementDelete.Hide();
                CommandGridOperationsDel.Hide();
            }
        }
      
        protected void DownloadFile()
        {
            string ServerFileName = "";
            string path = "";
            string contentType = "";
            string name = "";

            if (!string.IsNullOrEmpty(hdnServerfileName.Text))
            {
                ServerFileName = hdnServerfileName.Text;
                path = Context.Server.MapPath(ServerFileName);
                contentType = hdnFileType.Text;
                name = hdnFileName.Text + "." + hdnFileFormat.Text;
            }
            else
            {

                Guid DocumentId = new Guid(hiddenValue.Text);
                DocContactAttachment obj = DocumentManager.DocContactAttachmentById(DocumentId);
                ServerFileName = obj.SavedFileName;
                path = Context.Server.MapPath(ServerFileName);
                contentType = obj.FileType;
                name = obj.UserFileName + "." + obj.FileFormat;
            }

            byte[] bytes = System.IO.File.ReadAllBytes(path);
            Context.Response.Clear();
            Context.Response.ClearHeaders();
            Context.Response.AppendHeader("Content-Disposition", "attachment; filename=\"" + name + "\"");
            Context.Response.ContentType = contentType;
            Context.Response.BinaryWrite(bytes);
            Context.Response.End();
        }

        private void BindBranchCombo()
        {
            cmbBranch.Store[0].DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            cmbBranch.Store[0].DataBind();
        }

        private void BindDealerCombo()
        {
            cmbDealer.Store[0].DataSource = DocumentManager.GetDealers();
            cmbDealer.Store[0].DataBind();
        }

        private void Initialise()
        {
            if (!string.IsNullOrEmpty(Request.QueryString["type"]))
            {
                string type = Request.QueryString["type"];

                hdnType.Text = type;

                if (type == "1")
                {
                    FormPanel1.Title = "New Dealer";
                    pageTitle.InnerText = "Dealer";

                    rfvDealer.Enabled = false;
                }
                else
                {
                    FormPanel1.Title = "New Painter";
                    pageTitle.InnerText = "Painter";

                    
                    cmbDealer.Show();
                    rfvDealer.Enabled = true;                    

                    gridAgreement.Hide();
                    btnAdd.Hide();
                }
            }

            BindDealerCombo();
            BindBranchCombo();
            BindZoneCombo();
            
            if (!string.IsNullOrEmpty(Request.QueryString["DocumentId"]))
            {
                hdnDocumentID.Text = Request.QueryString["DocumentId"];
                Guid ContactId = new Guid(hdnDocumentID.Text);

                BidContactInformation(ContactId);
                BindRenewGrid(ContactId);
                BindAttachmentGrid(ContactId);

                //if (!string.IsNullOrEmpty(Request.QueryString["IsSave"]))
                //{
                //    if (Request.QueryString["IsSave"] == "true")
                //    {
                //        DocDocument objDocDocument = DocumentManager.GetDocDocumentById(new Guid(hdnDocumentID.Text.Trim()));

                //        dfMsg.Text = string.Format("The Document {0} has been created.", objDocDocument.ID);
                //        X.Js.Call("showNewAddTd");
                //    }
                //}
            }

            
        }

        private void BindZoneCombo()
        {
            CommonManager _CommonManager = new CommonManager();
            cmbZone.Store[0].DataSource = _CommonManager.GetAllZones();
            cmbZone.Store[0].DataBind();
        }

        private void BindCategoryComboBox()
        {
            List<DocCategory> list = DocumentManager.GetDocCategories();
            list.Insert(0, (new DocCategory() { Name = "Add New Category" }));
        }

        protected string ToSizeString(double bytes)
        {

            string[] SizeSuffixes = { "bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };
            int mag = (int)Math.Log(bytes, 1024);
            decimal adjustedSize = (decimal)bytes / (1 << (mag * 10));
            return string.Format("{0:n1} {1}", adjustedSize, SizeSuffixes[mag]);
        }

        protected void btnSaveWindowDoc_Click(object sender, DirectEventArgs e)
        {
            string entryLineJson = e.ExtraParams["ImagesLine"];
            string gridDocumentJSON = e.ExtraParams["gridDocumentJSON"];
            SaveDocumentFormData(entryLineJson, gridDocumentJSON);
        }

        protected void btnSaveRenew_Click(object sender, DirectEventArgs e)
        {
           DateTime StartDate =  GetEngDate(txtStartDate.Text);
           DateTime EndDate =  GetEngDate(txtEndDate.Text);

           if (EndDate.Date<StartDate.Date)
           {
               NewMessage.ShowNormalMessage("End Date must be greater than Start Date");
               return;
           }

            string gridRenewJSON = e.ExtraParams["gridRenewJSON"];
            List<DealerRenewBO> _LineList = JSON.Deserialize<List<DealerRenewBO>>(gridRenewJSON);
            
            DealerRenewBO _objDealerRenewBO = new DealerRenewBO();
            _objDealerRenewBO.StartDate = txtStartDate.Text;
            _objDealerRenewBO.EndDate = txtEndDate.Text;
            _objDealerRenewBO.AgrementPeriod = "Agreement Period";

            if (string.IsNullOrEmpty(hdnAgreementID.Text))
                _objDealerRenewBO.AgreementId = Guid.NewGuid().ToString();
            else
                _objDealerRenewBO.AgreementId = hdnAgreementID.Text.Trim();
            
            _LineList.Add(_objDealerRenewBO);
            gridAgreement.GetStore().DataSource = _LineList;
            gridAgreement.GetStore().DataBind();
            WindowRenew.Hide();

        }

        private void SaveDocumentFormData(string entryLineJsonUploadFile, string gridDocumentJSON)
        {
            Status myStatus = new Status();
            ////Document -----

            List<DealerDocumentBO> _LineList = JSON.Deserialize<List<DealerDocumentBO>>(entryLineJsonUploadFile);
            List<DealerDocumentBO> _GridDocumentDisplayList = JSON.Deserialize<List<DealerDocumentBO>>(gridDocumentJSON);

            foreach (DealerDocumentBO _item in _LineList)
            {
                _item.Period = txtPeriod.Text;
                _item.DocumentID = Guid.NewGuid().ToString();
                _item.name = txtDocumentName.Text;
                if (_GridDocumentDisplayList.Where(x => x.name == _item.name).Any())
                {
                    NewMessage.ShowNormalMessage("Document Name already exists");
                    return;
                }
 
                _item.DateAdded = SessionManager.GetAppropriateDate(SessionManager.GetCurrentDateAndTime().Date);
            }

            _GridDocumentDisplayList.AddRange(_LineList);

            GridIncome.GetStore().DataSource = _GridDocumentDisplayList;
            GridIncome.GetStore().DataBind();
            WindowOtherSrc.Hide();
            
        }

        private void Clear()
        {
            //gridFiles.Store[0].DataSource = new List<DocAttachment>() { };
            //gridFiles.Store[0].DataBind();
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
          
            Page.Validate("SaveUpd");
            if (Page.IsValid)
            {
                bool isSave = true;
                DocPartyContact obj = new DocPartyContact();

                if (!string.IsNullOrEmpty(hdnDocumentID.Text.Trim()))
                {
                    obj.ContactId = new Guid(hdnDocumentID.Text.Trim());
                    isSave = false;
                }
                else
                    obj.ContactId = Guid.NewGuid();

                obj.Type=int.Parse(hdnType.Text.Trim());
                obj.Name=txtName.Text.Trim();
                obj.OfficePhoneNo= txtDealerNumber.Text.Trim();
                if(!string.IsNullOrEmpty(cmbZone.SelectedItem.Value))
                obj.ZoneRef_Id=int.Parse(cmbZone.SelectedItem.Value);
                if(!string.IsNullOrEmpty(cmbDistrict.SelectedItem.Value))
                obj.DistrictRef_Id=int.Parse(cmbDistrict.SelectedItem.Value);

                obj.Address=txtAddress.Text.Trim();
                obj.ContactPerson = txtContactPerson.Text.Trim();
                obj.Phone=txtPhoneNo.Text.Trim();
                obj.MobileNo=txtMobileNo.Text.Trim();
                obj.Emai=txtEmail.Text.Trim();

                if (cmbBranch.SelectedItem != null && cmbBranch.SelectedItem.Value != null)
                    obj.BranchId = int.Parse(cmbBranch.SelectedItem.Value);
                else
                {
                    NewMessage.ShowWarningMessage("Branch is required.");
                    return;
                }

                if(!string.IsNullOrEmpty(txtArea.Text.Trim()))
                    obj.Area = txtArea.Text.Trim();

                if (hdnType.Text == "2")
                {
                    if (cmbDealer.SelectedItem != null && cmbDealer.SelectedItem.Value != null)
                        obj.DealerId = new Guid(cmbDealer.SelectedItem.Value);
                    else
                    {
                        NewMessage.ShowWarningMessage("Dealer is required.");
                        return;
                    }
                }

                if (!string.IsNullOrEmpty(txtID.Text.Trim()))
                    obj.ID = txtID.Text.Trim();                

                string saveJsonDocument = e.ExtraParams["saveJsonGridIncome"];
                string saveJsongridAgreement = e.ExtraParams["saveJsongridAgreement"];

                List<DealerRenewBO> _RenewLine = JSON.Deserialize<List<DealerRenewBO>>(saveJsongridAgreement);
                List<DealerDocumentBO> _DocLine = JSON.Deserialize<List<DealerDocumentBO>>(saveJsonDocument);

                List<DocContactAttachment> _listAttachments = new List<DocContactAttachment>();
                foreach (DealerDocumentBO _item in _DocLine)
                {

                    DocContactAttachment _objDocContactAttachment = new DocContactAttachment();
                    _objDocContactAttachment.ContactRef_ID = obj.ContactId;
                    _objDocContactAttachment.DocumentID = Guid.NewGuid();
                    _objDocContactAttachment.Type = obj.Type;
                    _objDocContactAttachment.Period = _item.Period;
                    _objDocContactAttachment.DateAdded = _item.DateAdded;
                    _objDocContactAttachment.DateAddedEng = GetEngDate(_objDocContactAttachment.DateAdded);
                    _objDocContactAttachment.name = _item.name;
                    _objDocContactAttachment.FileFormat = _item.FileFormat;
                    _objDocContactAttachment.FileType = _item.FileType;
                    _objDocContactAttachment.FileLocation = @"~/uploads/Document/";
                    _objDocContactAttachment.SavedFileName = _item.SavedFileName;
                    _objDocContactAttachment.UserFileName = _item.name;
                    _objDocContactAttachment.Size = _item.size;
                    _listAttachments.Add(_objDocContactAttachment);
                }


                List<DocContactAgreementLine> _listDocContactAgreementLines = new List<DocContactAgreementLine>();
                foreach (DealerRenewBO _item in _RenewLine)
                {

                    DocContactAgreementLine _objDocContactAgreementLine = new DocContactAgreementLine();
                    _objDocContactAgreementLine.AgreementId = Guid.NewGuid();
                    _objDocContactAgreementLine.ContactRef_Id = obj.ContactId;
                    _objDocContactAgreementLine.AgrementPeriod = "Agreement Period";
                    _objDocContactAgreementLine.StartDate = _item.StartDate;
                    _objDocContactAgreementLine.StartDateEng = GetEngDate(_item.StartDate);

                    _objDocContactAgreementLine.EndDate = _item.EndDate;
                    _objDocContactAgreementLine.EndDateEng = GetEngDate(_item.EndDate);
                    _listDocContactAgreementLines.Add(_objDocContactAgreementLine);
                }

                Status status = DocumentManager.InsertUpdateDealer(obj,_listAttachments,_listDocContactAgreementLines, isSave);
                if (status.IsSuccess)
                {

                    //if (string.IsNullOrEmpty(hdnDocumentID.Text))
                    //    Response.Redirect("~/NewHr/DocumentManagement/Document.aspx?DocumentId=" + obj.DocumentID + "&IsSave=true");
                    //else

                    hdnDocumentID.Text = obj.ContactId.ToString();

                    if(isSave)

                        NewMessage.ShowNormalMessage("Information saved successfully");
                    else
                        NewMessage.ShowNormalMessage("Information updated successfully");
                }
                else
                {

                    NewMessage.ShowWarningMessage(status.ErrorMessage);

                    //if (string.IsNullOrEmpty(hdnDocumentID.Text))
                    //    NewMessage.ShowWarningMessage("Error while saving record.");
                    //else
                    //    NewMessage.ShowWarningMessage("Error while updating record.");
                }


            }
        }

        protected void btnCancel_Click(object sender, DirectEventArgs e)
        {
            Clear();
            hdnDocumentID.Clear();
            btnSave.Text = "Save";
        }

      
     

        protected void btnDownload_Click(object sender, DirectEventArgs e)
        {
            if (string.IsNullOrEmpty(hdnFileID.Text))
            {
                NewMessage.ShowWarningMessage("Attachment not found.");
                return;
            }

            Guid fileId = new Guid(hdnFileID.Text);
            DocAttachment doc = DocumentManager.GetDocAttacmentById(fileId);

            string path = Server.MapPath(@"~/uploads/Document/"  +doc.ServerFileName);
            string contentType = doc.FileType;
            string name = doc.UserFileName;

            byte[] bytes = File.ReadAllBytes(path);

            Context.Response.Clear();
            Context.Response.ClearHeaders();
            Context.Response.AppendHeader("Content-Disposition", "attachment; filename=\"" + name + "\"");
            Context.Response.ContentType = contentType;
            Context.Response.BinaryWrite(bytes);
            Context.Response.End();
        }

        protected void btnDeleteFile_Click(object sender, DirectEventArgs e)
        {
            Guid fileId = new Guid(hdnFileID.Text);

            string itemString = e.ExtraParams["itemList"];
            List<DocAttachment> list = JSON.Deserialize<List<DocAttachment>>(itemString);

            DocAttachment obj = list.SingleOrDefault(x => x.FileID == fileId);

            string path = Server.MapPath(@"~/uploads/Document/" + obj.ServerFileName); 

            list.Remove(obj);

            if (File.Exists(path))
                File.Delete(path);


            if (!string.IsNullOrEmpty(hdnDocumentID.Text))
            {
                Status status = DocumentManager.DeleteDocAttachment(fileId);
                if (status.IsSuccess)
                    NewMessage.ShowNormalMessage("Document attachment deleted successfully.");
                else
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
            }

          //  gridFiles.Store[0].DataSource = list;
           // gridFiles.Store[0].DataBind();
        }
        protected void btnAgreementRenew_Click(object sender, DirectEventArgs e)
        {
            ClearRenew();
            WindowRenew.Show();
        }
        

        protected void btnAddIncome_Click(object sender, DirectEventArgs e)
        {
            ClearAddDocField();
            WindowOtherSrc.Show();
        }

        protected void ClearAddDocField()
        {
            txtDocumentName.Clear();
            txtPeriod.Clear();
            txtDocumentName.Clear();
            btnSave.Text = "Save";
            UploadGrid.GetStore().RemoveAll();
        }

        protected void ClearRenew()
        {
            hdnAgreementID.Clear();
            txtStartDate.Clear();
            txtEndDate.Clear();
        }


        protected void UploadIncomeDocClick(object sender, DirectEventArgs e)
        {
            if (this.FileUploadIncomeSource.HasFile)
            {

                string NewId = Guid.NewGuid().ToString();
                string relativePath = @"~/uploads/Document/" + NewId + Path.GetExtension(FileUploadIncomeSource.FileName);
                this.FileUploadIncomeSource.PostedFile.SaveAs(Server.MapPath(relativePath));
                List<object> _DocumentBOList = new List<object>();

                _DocumentBOList.Add(new
                {
                    name = FileUploadIncomeSource.FileName,
                    DocumentID = "0",
                    status = "Uploaded",
                    SavedFileName = relativePath,
                    FileFormat = Path.GetExtension(FileUploadIncomeSource.FileName).Replace(".", "").Trim(),
                    FileType = "@" + FileUploadIncomeSource.PostedFile.ContentType,
                    size = FileUploadIncomeSource.PostedFile.ContentLength.ToString()
                });
            
                //storeDocument.Add(_DocumentBOList);
                storeDocument.DataSource = _DocumentBOList;
                storeDocument.DataBind();
            }
        }


        private void BidContactInformation(Guid ContactId)
        {
            DocPartyContact _dbDocPartyContact = DocumentManager.GetDocPartyContact(ContactId);
            if (_dbDocPartyContact == null)
                return;

            hdnDocumentID.Text = _dbDocPartyContact.ContactId.ToString();
            hdnType.Text = _dbDocPartyContact.Type.ToString();
            txtName.Text = _dbDocPartyContact.Name;
            txtDealerNumber.Text = _dbDocPartyContact.OfficePhoneNo;


            if (_dbDocPartyContact.Type == 1)
            {
                pageTitle.InnerText = "Dealer";
                FormPanel1.Title = "Edit Dealer";
            }
            else
            {
                pageTitle.InnerText = "Painter";
                FormPanel1.Title = "Edit Painter";

                cmbDealer.Show();

                if (_dbDocPartyContact.DealerId != null)
                    ExtControlHelper.ComboBoxSetSelected(_dbDocPartyContact.DealerId, cmbDealer);

                gridAgreement.Hide();
                btnAdd.Hide();
            }

            if (_dbDocPartyContact.ZoneRef_Id != null)
                cmbZone.SetValue(_dbDocPartyContact.ZoneRef_Id);

            if (_dbDocPartyContact.ZoneRef_Id != null)
            {
               // BindZoneCombo();
                
                cmbZone.SetValue(_dbDocPartyContact.ZoneRef_Id);
                GetDistrictBySelectedZone(_dbDocPartyContact.ZoneRef_Id.Value);
            }

            if (_dbDocPartyContact.DistrictRef_Id != null)
                //cmbDistrict.SetValue(_dbDocPartyContact.DistrictRef_Id.ToString().ToLower());
            ExtControlHelper.ComboBoxSetSelected(_dbDocPartyContact.DistrictRef_Id, cmbDistrict);

            txtAddress.Text = _dbDocPartyContact.Address;
            txtContactPerson.Text = _dbDocPartyContact.ContactPerson;
            txtPhoneNo.Text = _dbDocPartyContact.Phone;
            txtMobileNo.Text = _dbDocPartyContact.MobileNo;
            txtEmail.Text = _dbDocPartyContact.Emai;

            if (_dbDocPartyContact.BranchId != null)
                ExtControlHelper.ComboBoxSetSelected(_dbDocPartyContact.BranchId.Value, cmbBranch);

            txtArea.Text = _dbDocPartyContact.Area;
            txtID.Text = _dbDocPartyContact.ID;
        }

        private void BindRenewGrid(Guid ContactId)
        {
            gridAgreement.GetStore().DataSource = DocumentManager.GetDocContactAgreementLine(ContactId);
            gridAgreement.GetStore().DataBind();
        }

        private void BindAttachmentGrid(Guid ContactId)
        {
            GridIncome.GetStore().DataSource = DocumentManager.GetDocContactAttachment(ContactId);
            GridIncome.GetStore().DataBind();
        }

        private void BindEditData()
        {
            Guid documentId = new Guid(hdnDocumentID.Text.Trim());

            DocDocument objDocDocument = DocumentManager.GetDocDocumentById(documentId);
            if (objDocDocument != null)
            {
                hdnPartyID.Text = objDocDocument.PartyRef_ID.ToString();

                DocParty objDocParty = DocumentManager.GetDocPartyById(objDocDocument.PartyRef_ID);
              
                List<DocAssign> listDocAssign = DocumentManager.GetDocAssignByDocumentId(objDocDocument.DocumentID);
                List<TextValue> listTextValue = new List<TextValue>();
                foreach (var item in listDocAssign)
                {
                    UUser objUUser = UserManager.GetUserByUserID(item.UserID);
                    TextValue obj = new TextValue() {Text = objUUser.UserName, Value = item.UserID.ToString() };
                    listTextValue.Add(obj);
                }

                List<DocAttachment> listDocAttachment = DocumentManager.GetDocAttachmentByDocumentId(objDocDocument.DocumentID);
              //  gridFiles.Store[0].DataSource = listDocAttachment;
              //  gridFiles.Store[0].DataBind();
                if (listDocAttachment.Count > 0)
                 //   gridFiles.Show();

              //  CommandColmGFDownload.Show();

             

                btnSave.Text = "Update";

                pageTitle.InnerText = "Edit Document";
            }
        }

        protected void btnAddNew_Click(object sender, DirectEventArgs e)
        {
            Response.Redirect("~/NewHr/DocumentManagement/Document.aspx");
        }

        private void BindEmptyAdditionalInformationGrid()
        {
            List<DocAdditionalInformationBO> list = new List<DocAdditionalInformationBO>();
            List<DocAdditionalInformation> listDAI = DocumentManager.GetDocAdditionalInformations();

            if (listDAI.Count > 0)
            {
                foreach (var item in listDAI)
                {
                    DocAdditionalInformationBO obj = new DocAdditionalInformationBO()
                    {
                        Name = item.Name,
                        AdditionalInformationID = item.AdditionalInformationID.ToString()
                    };

                    list.Add(obj);
                }
            }
        }
        
        public void btnDeleteCommand_Click(object sender, DirectEventArgs e)
        {

            Guid ID = new Guid(hiddenValue.Text.Trim());

           //bool result = Finance_BikeManager.DeleteCustomerOtherDoc(ID);
           //if (result)
           //    this.LoadDocument(GetCustomerId());
        }

       
        protected void ZoneChange(object sender, DirectEventArgs e)
        {
            GetDistrictBySelectedZone(int.Parse(cmbZone.SelectedItem.Value));
        }

        private void GetDistrictBySelectedZone(int ZoneId)
        {
            CommonManager comManager = new CommonManager();
            cmbDistrict.Clear();
            cmbDistrict.Store[0].DataSource = comManager.GetAllDistricts(ZoneId);
            cmbDistrict.Store[0].DataBind();
        }

        protected void btnRenewAgreement_Click(object sender, DirectEventArgs e)
        {
            if (string.IsNullOrEmpty(hdnAgreementID.Text))
                return;

            string gridRenewJSON = e.ExtraParams["gridRenewJSON"];
            List<DealerRenewBO> _LineList = JSON.Deserialize<List<DealerRenewBO>>(gridRenewJSON);

            DealerRenewBO obj = _LineList.SingleOrDefault(x => x.AgreementId == hdnAgreementID.Text.Trim());
            if (obj != null)
            {
                txtStartDate.Text = obj.StartDate;
                txtEndDate.Text = obj.EndDate;
                WindowRenew.Center();
                WindowRenew.Show();
            }

        }


    }
}