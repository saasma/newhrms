﻿<%@ Page Title="Document Additional Information" Language="C#" MasterPageFile="~/Master/NewDetails.Master" AutoEventWireup="true" CodeBehind="DocumentAdditionalInformation.aspx.cs" Inherits="Web.NewHR.DocumentManagement.DocumentAdditionalInformation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

<style type="text/css">
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
</style>

<script type="text/javascript">

    var CommandHandler = function(command, record){
        <%= hdnAdditionalInformationID.ClientID %>.setValue(record.data.AdditionalInformationID);
            
            if(command=="Edit")
            {
                <%= btnEdit.ClientID %>.fireEvent('click');
            }
            else
            {
                <%= btnDelete.ClientID %>.fireEvent('click');
            }

        };


</script>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">

<ext:Hidden ID="hdnAdditionalInformationID" runat="server" />

<ext:LinkButton ID="btnEdit" runat="server" Hidden="true">
    <DirectEvents>
        <Click OnEvent="btnEdit_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:LinkButton>
<ext:LinkButton ID="btnDelete" runat="server" Hidden="true">
    <DirectEvents>
        <Click OnEvent="btnDelete_Click">
            <EventMask ShowMask="true" />
            <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the record?" />
        </Click>
    </DirectEvents>
</ext:LinkButton>

<div class="pageheader">
    <div class="media">
        <div class="media-body">
            <h4>
                Document Additional Information
            </h4>
        </div>
    </div>
</div>

<div class="contentpanel">

<ext:Button runat="server" Cls="btn btn-primary" Icon="Add" ID="btnAddNew" Text="<i></i>Add Additional Information">
    <DirectEvents>
        <Click OnEvent="btnAddNew_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:Button>
<br />
<ext:GridPanel ID="gridAdditionalInformation" runat="server" Width="380" Cls="itemgrid">
    <Store>
        <ext:Store ID="Store1" runat="server">
            <Model>
                <ext:Model ID="Model4" runat="server" IDProperty="AdditionalInformationID">
                    <Fields>
                        <ext:ModelField Name="AdditionalInformationID" Type="String" />
                        <ext:ModelField Name="Name" Type="string" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
    </Store>
    <ColumnModel>
        <Columns>
            <ext:Column ID="colName" Sortable="false" MenuDisabled="true" runat="server" Text="Name"
                Align="Left" Width="300" DataIndex="Name" />
            <ext:CommandColumn ID="CommandColumn1" runat="server" Width="80" Text="" Align="Center">
                <Commands>
                    <ext:CommandSeparator />
                    <ext:GridCommand Cls="editGridButton" ToolTip-Text="Edit" Text="<i class='fa fa-pencil'></i>"
                        CommandName="Edit" />
                    <ext:GridCommand Cls="editGridButton" ToolTip-Text="Delete" Text="<i class='fa fa-trash-o'></i>"
                        CommandName="Delete" />
                </Commands>
                <Listeners>
                    <Command Handler="CommandHandler(command,record);" />
                </Listeners>
            </ext:CommandColumn>
        </Columns>
    </ColumnModel>
    <SelectionModel>
        <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
    </SelectionModel>
</ext:GridPanel>

</div>


 <ext:Window ID="WAdditionalInformation" runat="server" Title="Additional Information" Icon="Application" ButtonAlign="Left"
            Width="400" Height="230" BodyPadding="5" Hidden="true" Modal="true">
            <Content>
                <table class="fieldTable">
                    <tr>
                        <td>
                            <ext:TextField ID="txtName" LabelSeparator="" runat="server" FieldLabel="Name *"
                                LabelWidth="60" Width="300" LabelAlign="Left">
                            </ext:TextField>
                            <asp:RequiredFieldValidator Display="None" ID="rfvName" runat="server" ValidationGroup="SaveUpdate"
                                ControlToValidate="txtName" ErrorMessage="Name is required." />
                        </td>
                    </tr>
                </table>
            </Content>
            <Buttons>
                <ext:Button runat="server" ID="btnSave" Text="Save" MarginSpec="0 0 0 15">
                    <DirectEvents>
                        <Click OnEvent="btnSave_Click">
                            <EventMask ShowMask="true" />
                        </Click>
                    </DirectEvents>
                    <Listeners>
                        <Click Handler="valGroup = 'SaveUpdate'; if(CheckValidation()) return ''; else return false;">
                        </Click>
                    </Listeners>
                </ext:Button>

                <ext:Button runat="server" ID="LinkButton1" Text="<i></i>Cancel" MarginSpec="0 0 0 10">
                    <Listeners>
                        <Click Handler="#{WAdditionalInformation}.hide();">
                        </Click>
                    </Listeners>
                </ext:Button>
            </Buttons>
        </ext:Window>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
