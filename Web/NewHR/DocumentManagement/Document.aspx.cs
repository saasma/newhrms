﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Entity;
using BLL;
using System.IO;
using Utils;
using System.Text;
using BLL.BO;
namespace Web.NewHR.DocumentManagement
{
    public partial class Document : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.DocumentManagement;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();

                if (!DocumentManager.CanUserDeleteDocument())
                    CommandColmnGFDelete.Hide();
            }
        }

        private void Initialise()
        {
            BindPartyComboBox();
            BindCategoryComboBox();
            BindEmptyAdditionalInformationGrid();

            FormPanel1.Title = "New Document";

            if (!string.IsNullOrEmpty(Request.QueryString["DocumentId"]))
            {
                FormPanel1.Title = "Edit Document";
                hdnDocumentID.Text = Request.QueryString["DocumentId"];
                BindEditData();

                if (!string.IsNullOrEmpty(Request.QueryString["IsSave"]))
                {
                    if (Request.QueryString["IsSave"] == "true")
                    {
                        DocDocument objDocDocument = DocumentManager.GetDocDocumentById(new Guid(hdnDocumentID.Text.Trim()));

                        dfMsg.Text = string.Format("The Document {0} has been created.", objDocDocument.ID);
                        X.Js.Call("showNewAddTd");
                    }
                }
            }

            
        }

        private void BindPartyComboBox()
        {
            List<DocParty> list = DocumentManager.GetDocPartySearch("");
            list.Insert(0, (new DocParty() { Name = "" }));
            cmbParty.Store[0].DataSource = list;
            cmbParty.Store[0].DataBind();
        }

        private void BindCategoryComboBox()
        {
            List<DocCategory> list = DocumentManager.GetDocCategories();
            list.Insert(0, (new DocCategory() { Name = "" }));
            cmbCategory.Store[0].DataSource = list;
            cmbCategory.Store[0].DataBind();
        }


        protected void cmbCategory_Select(object sender, DirectEventArgs e)
        {
            cmbDocumentSubCategory.Clear();

            if (cmbCategory.SelectedItem != null && cmbCategory.SelectedItem.Text == "Add New Category")
            {
                DocumentCategoryCtrl1.AddNewCategory("0");
                return;
            }

            if (cmbCategory.SelectedItem == null || cmbCategory.SelectedItem.Value == null)
            {
                List<DocSubCategory> listDSC = new List<DocSubCategory>();
                listDSC.Insert(0, (new DocSubCategory() { Name = "" }));

                cmbDocumentSubCategory.Store[0].DataSource = listDSC;
                cmbDocumentSubCategory.Store[0].DataBind();
                return;
            }


            Guid categoryId = new Guid(cmbCategory.SelectedItem.Value);

            List<DocSubCategory> list = DocumentManager.GetDocSubCategoryByCategoryId(categoryId);
            list.Insert(0, (new DocSubCategory() { Name = "" }));

            cmbDocumentSubCategory.Store[0].DataSource = list;
            cmbDocumentSubCategory.Store[0].DataBind();
        }

        protected void file_Added(object sender, DirectEventArgs e)
        {
            if (fileUpload1.HasFile)
            {
                string itemString = e.ExtraParams["itemList"];
                List<DocAttachment> list = JSON.Deserialize<List<DocAttachment>>(itemString);

                if (fileUpload1.HasFile)
                {
                    string UserFileName = this.fileUpload1.FileName;

                    string ext = Path.GetExtension(UserFileName);

                    if (ext.ToLower() == ".exe")
                    {
                        fileUpload1.Reset();
                        fileUpload1.Focus();
                        NewMessage.ShowWarningMessage("Please upload proper file.");
                        return;
                    }

                    string ServerFileName = Guid.NewGuid().ToString() + ext;

                    string relativePath = @"~/uploads/Document/" + ServerFileName;

                    if (this.UploadAttachment(relativePath))
                    {
                        double fileSize = fileUpload1.PostedFile.ContentLength;
                        list.Add(new DocAttachment()
                     {
                         FileID = Guid.NewGuid(),
                         FileFormat = Path.GetExtension(this.fileUpload1.FileName).Replace(".", "").Trim(),
                         FileType = this.fileUpload1.PostedFile.ContentType,
                         FileLocation = @"../Uploads/Document/",
                         ServerFileName = ServerFileName,
                         UserFileName = UserFileName,
                         Size = this.ToSizeString(fileSize)
                     });
                    }


                }

                gridFiles.Store[0].DataSource = list;
                gridFiles.Store[0].DataBind();

                gridFiles.Show();
            }
        }

        protected bool UploadAttachment(string relativePath)
        {
            if (this.fileUpload1.HasFile)
            {

                int fileSize = fileUpload1.PostedFile.ContentLength;
                this.fileUpload1.PostedFile.SaveAs(Server.MapPath(relativePath));
                return true;
            }
            else
                return false;

        }
        protected string ToSizeString(double bytes)
        {

            string[] SizeSuffixes = { "bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };
            int mag = (int)Math.Log(bytes, 1024);
            decimal adjustedSize = (decimal)bytes / (1 << (mag * 10));
            return string.Format("{0:n1} {1}", adjustedSize, SizeSuffixes[mag]);
        }

        private void Clear()
        {
            cmbParty.ClearValue();
            cmbCategory.ClearValue();
            cmbDocumentSubCategory.ClearValue();
            calDocumentDate.Clear();
            calExpirationDate.Clear();
            chkDoesNotExpire.Checked = false;
            txtDescription.Clear();
            chkIsPrivate.Checked = false;
            chkNotifyAssigneesByEmail.Checked = false;
            cmbAssignTo.ClearValue();
            txtDocumentTitle.Clear();
            gridFiles.Store[0].DataSource = new List<DocAttachment>() { };
            gridFiles.Store[0].DataBind();

            gridAssignTo.Store[0].DataSource = new List<TextValue>() { };
            gridAssignTo.Store[0].DataBind();
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveUpd");
            if (Page.IsValid)
            {
                bool isSave = true;
                DocDocument obj = new DocDocument();

                if (!string.IsNullOrEmpty(hdnDocumentID.Text.Trim()))
                {
                    obj.DocumentID = new Guid(hdnDocumentID.Text.Trim());
                    isSave = false;
                }
                else
                    obj.DocumentID = Guid.NewGuid();

                DocParty objDocParty = new DocParty();

                if (!string.IsNullOrEmpty(hdnPartyID.Text.Trim()))
                {
                    objDocParty.Name = hdnPartyName.Text.Trim();
                    objDocParty.PartyID = new Guid(hdnPartyID.Text.Trim());
                }
                else
                {
                    NewMessage.ShowWarningMessage("Name of the party is required.");
                    return;
                }

                string stringAssignTo = e.ExtraParams["itemAssignTo"];

                if (string.IsNullOrEmpty(stringAssignTo))
                {
                    NewMessage.ShowWarningMessage("Assign to is required.");
                    cmbAssignTo.Focus();
                    return;
                }

                List<TextValue> listAssignTo = JSON.Deserialize<List<TextValue>>(stringAssignTo);

                List<DocAssign> listDocAssign = new List<DocAssign>();
                foreach (TextValue item in listAssignTo)
                {
                    listDocAssign.Add(new DocAssign() { DocumentRef_ID = obj.DocumentID, UserID = new Guid(item.Value) });
                }

                string stringDocAttachments = e.ExtraParams["itemList"];
                List<DocAttachment> listDocs = JSON.Deserialize<List<DocAttachment>>(stringDocAttachments);
                List<DocAttachment> listDocAttachment = new List<DocAttachment>();
                foreach (var item in listDocs)
                {
                    listDocAttachment.Add(new DocAttachment() { 
                        FileID = item.FileID,
                        DocumentRef_ID = obj.DocumentID,
                        ServerFileName = item.ServerFileName,
                        UserFileName = item.UserFileName,
                        FileLocation = item.FileLocation,
                        FileFormat = item.FileFormat,
                        Size = item.Size,
                        FileType = item.FileType
                    });
                }                

                obj.PartyRef_ID = objDocParty.PartyID;
                obj.Title = txtDocumentTitle.Text.Trim();

                if (cmbCategory.SelectedItem != null && cmbCategory.SelectedItem.Value != null)
                {
                    if (cmbCategory.SelectedItem.Text != "Add New Category")
                        obj.CategoryID = new Guid(cmbCategory.SelectedItem.Value);
                    else
                    {
                        NewMessage.ShowWarningMessage("Document Category is required.");
                        return;
                    }
                }
                else
                {
                    NewMessage.ShowWarningMessage("Document Category is required.");
                    return;
                }

                if (cmbDocumentSubCategory.SelectedItem != null && cmbDocumentSubCategory.SelectedItem.Value != null)
                {
                    if (cmbDocumentSubCategory.SelectedItem.Text != "Add New Sub-Category")
                        obj.SubCategoryID = new Guid(cmbDocumentSubCategory.SelectedItem.Value);
                    else
                    {
                        NewMessage.ShowWarningMessage("Document Sub-Category is required.");
                        return;
                    }
                }
                else
                {
                    NewMessage.ShowWarningMessage("Document Sub-Category is required.");
                    return;
                }                
                
                obj.DocumentDate = calDocumentDate.Text.Trim();
                obj.DocumentDateEng = GetEngDate(obj.DocumentDate);

                if (chkDoesNotExpire.Checked)
                    obj.HasExpiry = false;
                else
                {
                    obj.HasExpiry = true;
                    obj.ExpiryDate = calExpirationDate.Text.Trim();
                    obj.ExpiryDateEng = GetEngDate(obj.ExpiryDate);
                }

                obj.Description = txtDescription.Text.Trim();
                if (chkIsPrivate.Checked)
                    obj.IsPrivate = true;
                else
                    obj.IsPrivate = false;

                string stringAdditionalInformation = e.ExtraParams["itemAddInformList"];
                List<DocAdditionalInformationBO> listAdditionalInformation = JSON.Deserialize<List<DocAdditionalInformationBO>>(stringAdditionalInformation);
                List<DocAdditionalInformationDetail> listDocAdditionalInformationDetail = new List<DocAdditionalInformationDetail>();
                //DocAdditionalInformationDetail
                foreach (var item in listAdditionalInformation)
                {
                    DocAdditionalInformationDetail objDocAdditionalInformationDetail = new DocAdditionalInformationDetail()
                    {
                       AdditionalInformationDetailID = Guid.NewGuid(),
                       AdditionalInformationRef_ID = new Guid(item.AdditionalInformationID),
                       Value = item.Value
                    };

                    listDocAdditionalInformationDetail.Add(objDocAdditionalInformationDetail);
                }


                Status status = DocumentManager.SaveUpdateDocument(obj, objDocParty, listDocAssign, listDocAttachment, listDocAdditionalInformationDetail, isSave);
                if (status.IsSuccess)
                {
                    if (string.IsNullOrEmpty(hdnDocumentID.Text))
                        Response.Redirect("~/NewHr/DocumentManagement/Document.aspx?DocumentId=" + obj.DocumentID + "&IsSave=true");
                    else
                        NewMessage.ShowNormalMessage("Document is updated.");                    
                }
                else
                {
                    if (string.IsNullOrEmpty(hdnDocumentID.Text))
                        NewMessage.ShowWarningMessage("Error while saving record.");
                    else
                        NewMessage.ShowWarningMessage("Error while updating record.");
                }


            }
        }

        protected void btnCancel_Click(object sender, DirectEventArgs e)
        {
            Clear();
            hdnDocumentID.Clear();
            btnSave.Text = "Save";
        }

        protected void cmbParty_Select(object sender, DirectEventArgs e)
        {
            if (cmbParty.SelectedItem == null || cmbParty.SelectedItem.Value == null)
            {
                hdnPartyID.Clear();
                hdnPartyName.Clear();
                return;
            }

            if (cmbParty.SelectedItem != null && cmbParty.SelectedItem.Value != null)
            {
                hdnPartyID.Text = cmbParty.SelectedItem.Value;
                hdnPartyName.Text = cmbParty.SelectedItem.Text;
            }

        }

        protected void cmbAssignTo_Select(object sender, DirectEventArgs e)
        {
            if (cmbAssignTo.SelectedItem == null || cmbAssignTo.SelectedItem.Value == null)
                return;

            string userName = cmbAssignTo.SelectedItem.Text;
            string userId = cmbAssignTo.SelectedItem.Value;
            cmbAssignTo.ClearValue();

            string itemString = e.ExtraParams["itemAssignTo"];
            List<TextValue> list = JSON.Deserialize<List<TextValue>>(itemString);

            TextValue obj = new TextValue(){ Text = userName, Value = userId};
            if(list.Contains(obj))
            {
                NewMessage.ShowWarningMessage("User is already selected.");
                return;
            }
            
            list.Add(obj);

            gridAssignTo.Store[0].DataSource = list;
            gridAssignTo.Store[0].DataBind();
            gridAssignTo.Show();
        }

        protected void btnDownload_Click(object sender, DirectEventArgs e)
        {
            if (string.IsNullOrEmpty(hdnFileID.Text))
            {
                NewMessage.ShowWarningMessage("Attachment not found.");
                return;
            }

            Guid fileId = new Guid(hdnFileID.Text);
            DocAttachment doc = DocumentManager.GetDocAttacmentById(fileId);

            string path = Server.MapPath(@"~/uploads/Document/"  +doc.ServerFileName);
            string contentType = doc.FileType;
            string name = doc.UserFileName;

            byte[] bytes = File.ReadAllBytes(path);

            Context.Response.Clear();
            Context.Response.ClearHeaders();
            Context.Response.AppendHeader("Content-Disposition", "attachment; filename=\"" + name + "\"");
            Context.Response.ContentType = contentType;
            Context.Response.BinaryWrite(bytes);
            Context.Response.End();
        }

        protected void btnDeleteFile_Click(object sender, DirectEventArgs e)
        {
            Guid fileId = new Guid(hdnFileID.Text);

            string itemString = e.ExtraParams["itemList"];
            List<DocAttachment> list = JSON.Deserialize<List<DocAttachment>>(itemString);

            DocAttachment obj = list.SingleOrDefault(x => x.FileID == fileId);

            string path = Server.MapPath(@"~/uploads/Document/" + obj.ServerFileName); 

            list.Remove(obj);

            if (File.Exists(path))
                File.Delete(path);


            if (!string.IsNullOrEmpty(hdnDocumentID.Text))
            {
                Status status = DocumentManager.DeleteDocAttachment(fileId);
                if (status.IsSuccess)
                    NewMessage.ShowNormalMessage("Document attachment deleted successfully.");
                else
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
            }

            gridFiles.Store[0].DataSource = list;
            gridFiles.Store[0].DataBind();
        }

        private void BindEditData()
        {
            Guid documentId = new Guid(hdnDocumentID.Text.Trim());

            DocDocument objDocDocument = DocumentManager.GetDocDocumentById(documentId);
            if (objDocDocument != null)
            {
                hdnPartyID.Text = objDocDocument.PartyRef_ID.ToString();

                DocParty objDocParty = DocumentManager.GetDocPartyById(objDocDocument.PartyRef_ID);
                cmbParty.SelectedItem.Value = objDocDocument.PartyRef_ID.ToString();
                cmbParty.ReadOnly = true;

                txtDocumentTitle.Text = objDocDocument.Title;

                txtID.Text = objDocDocument.ID;
                txtID.Show();

                cmbCategory.SelectedItem.Value = objDocDocument.CategoryID.ToString();

                cmbCategory_Select(null, null);

                cmbDocumentSubCategory.SelectedItem.Value = objDocDocument.SubCategoryID.ToString();
                
                calDocumentDate.Text = objDocDocument.DocumentDate;

                if (objDocDocument.HasExpiry)
                    calExpirationDate.Text = objDocDocument.ExpiryDate;
                else
                {
                    chkDoesNotExpire.Checked = true;
                    calExpirationDate.Disable();
                }

                txtDescription.Text = objDocDocument.Description;
                chkIsPrivate.Checked = objDocDocument.IsPrivate;

                List<DocAssign> listDocAssign = DocumentManager.GetDocAssignByDocumentId(objDocDocument.DocumentID);
                List<TextValue> listTextValue = new List<TextValue>();
                foreach (var item in listDocAssign)
                {
                    UUser objUUser = UserManager.GetUserByUserID(item.UserID);
                    TextValue obj = new TextValue() {Text = objUUser.UserName, Value = item.UserID.ToString() };
                    listTextValue.Add(obj);
                }

                gridAssignTo.Store[0].DataSource = listTextValue;
                gridAssignTo.Store[0].DataBind();

                if (listDocAssign.Count > 0)
                    gridAssignTo.Show();

                List<DocAttachment> listDocAttachment = DocumentManager.GetDocAttachmentByDocumentId(objDocDocument.DocumentID);
                gridFiles.Store[0].DataSource = listDocAttachment;
                gridFiles.Store[0].DataBind();
                if (listDocAttachment.Count > 0)
                    gridFiles.Show();

                CommandColmGFDownload.Show();

                BindAdditionalInformationGridEditData(documentId);

                btnSave.Text = "Update";

                pageTitle.InnerText = "Edit Document";
            }
        }

        protected void btnAddNew_Click(object sender, DirectEventArgs e)
        {
            Response.Redirect("~/NewHr/DocumentManagement/Document.aspx");
        }

        private void BindEmptyAdditionalInformationGrid()
        {
            List<DocAdditionalInformationBO> list = new List<DocAdditionalInformationBO>();
            List<DocAdditionalInformation> listDAI = DocumentManager.GetDocAdditionalInformations();

            if (listDAI.Count > 0)
            {
                foreach (var item in listDAI)
                {
                    DocAdditionalInformationBO obj = new DocAdditionalInformationBO()
                    {
                        Name = item.Name,
                        AdditionalInformationID = item.AdditionalInformationID.ToString()
                    };

                    list.Add(obj);
                }

                gridAdditionalInformation.Show();
                dfAdditionalInformation.Show();

                gridAdditionalInformation.Store[0].DataSource = list;
                gridAdditionalInformation.Store[0].DataBind();

            }
        }

        private void BindAdditionalInformationGridEditData(Guid documentID)
        {
            List<DocAdditionalInformationBO> list = DocumentManager.GetAdditionalInformationBOByDocumentID(documentID);
            if (list.Count > 0)
            {
                gridAdditionalInformation.Store[0].DataSource = list;
                gridAdditionalInformation.Store[0].DataBind();
            }
        }

        protected void btnCategoryComboReload_Click(object sender, DirectEventArgs e)
        {
            BindCategoryComboBox();            

            cmbCategory.SetValue(hdnCategoryID.Text);
            cmbCategory_Select(null, null);
        }

        protected void cmbDocumentSubCategory_Select(object sender, DirectEventArgs e)
        {
            if (cmbDocumentSubCategory.SelectedItem != null && cmbDocumentSubCategory.SelectedItem.Text == "Add New Sub-Category")
            {
                Guid? categoryID = null;
                if (cmbCategory.SelectedItem != null && cmbCategory.SelectedItem.Value != null)
                    categoryID = new Guid(cmbCategory.SelectedItem.Value);

                DocumentSubCategoryCtrl1.AddNewSubCategory("0", categoryID);
            }
        }

        protected void btnSubCategoryComboReload_Click(object sender, DirectEventArgs e)
        {
            cmbDocumentSubCategory.ClearValue();

            Guid categoryId = new Guid(cmbCategory.SelectedItem.Value);

            List<DocSubCategory> list = DocumentManager.GetDocSubCategoryByCategoryId(categoryId);
            list.Insert(0, (new DocSubCategory() { Name = "" }));

            cmbDocumentSubCategory.Store[0].DataSource = list;
            cmbDocumentSubCategory.Store[0].DataBind();

            cmbDocumentSubCategory.SetValue(hdnSubCategoryID.Text);
            
        }

        [DirectMethod]
        public void AddNewCategoryPopUp()
        {
            DocumentCategoryCtrl1.AddNewCategory("0");
        }

        [DirectMethod]
        public void AddNewSubCategoryPopUp()
        {
            Guid? categoryID = null;
            if (cmbCategory.SelectedItem != null && cmbCategory.SelectedItem.Value != null)
                categoryID = new Guid(cmbCategory.SelectedItem.Value);

            DocumentSubCategoryCtrl1.AddNewSubCategory("0", categoryID);
        }

        [DirectMethod]
        public void AddNewPartyPopUp()
        {
            DocPartyCtrl1.AddNewDocParty("0");
        }

        protected void btnPartyComboReload_Click(object sender, DirectEventArgs e)
        {
            BindPartyComboBox();
            cmbParty.SetValue(hdnPartyID.Text);
        }

    }
}