﻿<%@ Page Title="Document" Language="C#" MasterPageFile="~/Master/NewDetails.Master" AutoEventWireup="true" CodeBehind="Document.aspx.cs" Inherits="Web.NewHR.DocumentManagement.Document" %>

<%@ Register Src="~/newhr/UserControls/DocPartyCtrl.ascx" TagName="DocPartyCtrl"
    TagPrefix="uc3" %>
<%@ Register Src="~/newhr/UserControls/DocumentCategoryCtrl.ascx" TagName="DocumentCategoryCtrl"
    TagPrefix="uc1" %>
<%@ Register Src="~/newhr/UserControls/DocumentSubCategoryCtrl.ascx" TagName="DocumentSubCategoryCtrl"
    TagPrefix="uc2" %>


<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>
<style type="text/css">
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
        
        .x-panel-default-framed
        {
            border : 2px solid #157FCC;
        }
        
        .x-form-trigger-wrap .x-form-text {
            border-width: 0;
            height: 30px;
        }
        
        .x-form-text
        {
            height: 30px;
        }
        
        .x-form-textarea 
        {
            height:55px !important;
        }
        
        .x-panel-body 
        {
              background-color: #e6eff7;
        }
        
        .x-panel-default-framed {
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            -ms-border-radius: 4px;
            -o-border-radius: 4px;
            border-radius: 0px;
            padding: 0;
            border-width: 2px;
            border-style: solid;
          
        }
         
    </style>

    <script type="text/javascript">

        var CommandHandlerFile = function(command, record){
            <%= hdnFileID.ClientID %>.setValue(record.data.FileID);
       
            if(command=="Delete")
            {
                <%= btnDeleteFile.ClientID %>.fireEvent('click');
            }
            else if(command == "DownLoad")
            {
                <%= btnDownload.ClientID %>.fireEvent('click');
            }
        };

        var prepareToolbar = function (grid, toolbar, rowIndex, record) {

            if (record.data.FileID == '') {
                toolbar.hide();
            }
        };

        var chkDoesNotExpireChange = function () {
            var value = <%= chkDoesNotExpire.ClientID %>.getValue();

            if(value)
            {
                <%= calExpirationDate.ClientID %>.clear();
                <%= calExpirationDate.ClientID %>.disable();
            }
            else
            {
                <%= calExpirationDate.ClientID %>.enable();
            }
        
        };
    
        var RemoveItemLine = function (column, command, record, recordIndex, cellIndex) {
            var store = this.grid.store;
            store.remove(record);
        };

        function showNewAddTd()
        {
            document.getElementById("<%= tdAddNew.ClientID %>").style.display = 'block';
        }

         function ReloadCategoryCombo(catID) {
            <%= hdnCategoryID.ClientID %>.setValue(catID);
            <%= btnCategoryComboReload.ClientID %>.fireEvent('click');
         }

         function ReloadSubCategoryCombo(subCatID) {
            <%= hdnSubCategoryID.ClientID %>.setValue(subCatID);
            <%= btnSubCategoryComboReload.ClientID %>.fireEvent('click');
         }

         function addNewCategory()
         {
            Ext.net.DirectMethods.AddNewCategoryPopUp();
         }

         function addNewSubCategory()
         {
            Ext.net.DirectMethods.AddNewSubCategoryPopUp();
         }

         function addNewParty()
         {
            Ext.net.DirectMethods.AddNewPartyPopUp();
         }

         function ReloadPartyCombo(partyID)
         {
            <%= hdnPartyID.ClientID %>.setValue(partyID);
            <%= btnPartyComboReload.ClientID %>.fireEvent('click');
         }
    </script>

   

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">

<ext:Hidden ID="hdnDocumentID" runat="server" />
<ext:Hidden ID="hdnPartyID" runat="server" />
<ext:Hidden ID="hdnPartyName" runat="server" />
<ext:Hidden ID="hdnFileID" runat="server" />

<ext:Hidden ID="hdnCategoryID" runat="server" />
<ext:Hidden ID="hdnSubCategoryID" runat="server" />

<ext:Button ID="btnCat" runat="server" Hidden="true">
    <DirectEvents>
        <Click OnEvent="cmbCategory_Select">
        </Click>
    </DirectEvents>
</ext:Button>

<ext:Button ID="btnCategoryComboReload" runat="server" Hidden="true">
    <DirectEvents>
        <Click OnEvent="btnCategoryComboReload_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:Button>

<ext:Button ID="btnSubCategoryComboReload" runat="server" Hidden="true">
    <DirectEvents>
        <Click OnEvent="btnSubCategoryComboReload_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:Button>

<ext:Button ID="btnPartyComboReload" runat="server" Hidden="true">
    <DirectEvents>
        <Click OnEvent="btnPartyComboReload_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:Button>

<ext:LinkButton ID="btnDownload" runat="server" Hidden="true">
    <DirectEvents>
        <Click OnEvent="btnDownload_Click">
        </Click>
    </DirectEvents>
</ext:LinkButton>

 <ext:LinkButton ID="btnDeleteFile" runat="server" Hidden="true">
    <DirectEvents>
        <Click OnEvent="btnDeleteFile_Click">
            <ExtraParams>
                <ext:Parameter Name="itemList" Value="Ext.encode(#{gridFiles}.getRowsValues({selectedOnly : false}))"
                    Mode="Raw" />
            </ExtraParams>
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:LinkButton>

<div class="pageheader">
    <div class="media">
        <div class="media-body">
            <h4 id="pageTitle" runat="server">
                 Document
            </h4>
        </div>
    </div>
</div>

<div class="contentpanel">

 

<ext:Store runat="server" ID="storeParty" AutoLoad="false">
    <Proxy>
        <ext:AjaxProxy Url="../../Handler/PartySearch.ashx">
            <ActionMethods Read="GET" />
            <Reader>
                <ext:JsonReader Root="plants" TotalProperty="total" />
            </Reader>
        </ext:AjaxProxy>
    </Proxy>
    <Model>
        <ext:Model ID="Model3" IDProperty="Name" Name="ItemLineModel" runat="server">
            <Fields>
                <ext:ModelField Name="Name" Type="String" />
                <ext:ModelField Name="PartyID" Type="String" />
            </Fields>
        </ext:Model>
    </Model>
</ext:Store>

<ext:Store runat="server" ID="storeUserSearch" AutoLoad="false">
    <Proxy>
        <ext:AjaxProxy Url="../../Handler/UserSearch.ashx">
            <ActionMethods Read="GET" />
            <Reader>
                <ext:JsonReader Root="plants" TotalProperty="total" />
            </Reader>
        </ext:AjaxProxy>
    </Proxy>
    <Model>
        <ext:Model ID="Model5" IDProperty="Value" Name="ItemLineModel" runat="server">
            <Fields>
                <ext:ModelField Name="Text" Type="String" />
                <ext:ModelField Name="Value" Type="String" />
            </Fields>
        </ext:Model>
    </Model>
    </ext:Store>

<table>
    <tr>
        <td>
        
       
<ext:FormPanel ID="FormPanel1"
                runat="server"
                Title="New Document"
                Frame="true" PaddingSpec="0 0 0 0"
                 ButtonAlign="Left"
                Width="600" MinHeight="950"
                Border="false"
                BodyPadding="30">
<Content>
<div style="margin-left:20px;">
<table>
    <tr>
        <td>
            
                    <%--<ext:ComboBox LabelSeparator="" ID="cmbParty" runat="server" DisplayField="Name" MarginSpec="0 0 0 0"
                        FieldLabel="Name of the Party" LabelAlign="Top" ValueField="PartyID"
                        StoreID="storeParty" TypeAhead="false" Width="250" HideBaseTrigger="true" MinChars="1" 
                        TriggerAction="All" ForceSelection="false">
                        <ListConfig LoadingText="Searching..." MinWidth="250" StyleSpec="border-top:1px solid #98c0f4;">
                            <ItemTpl ID="ItemTpl1" runat="server">
                                <Html>
                                    <tpl>
                                            <div class="search-item">
                                                            <span>{Name}</span>  
                                             </div>
					                </tpl>
                                </Html>
                            </ItemTpl>
                        </ListConfig>
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                        </Triggers>
                        <Listeners>
                            <Select Handler="this.getTrigger(0).show();" />
                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                            <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                        </Listeners>
                        <DirectEvents>
                            <Select OnEvent="cmbParty_Select" />
                            <TriggerClick OnEvent="cmbParty_Select" />
                        </DirectEvents>
                    </ext:ComboBox>--%>

                    <ext:ComboBox Width="250" LabelSeparator="" LabelAlign="Top" MarginSpec="0 0 0 0"
                        QueryMode="Local" ID="cmbParty" ForceSelection="true" DisplayField="Name" ValueField="PartyID"
                        runat="server" FieldLabel="Name of the Party">
                        <ListConfig>
                            <ItemTpl ID="ItemTpl5" runat="server">
                                <Html>
                                    <tpl if="Name==''">
							                                <div class="button"><a href="javascript:void(0)" class="button add" onclick="addNewParty();">Add New Party</a></div>
						                                </tpl>
                                    <tpl if="Name!=''>
                                                            {Name}
					                                    </tpl>
                                </Html>
                            </ItemTpl>
                        </ListConfig>
                        <Store>
                            <ext:Store ID="storeDocParty" runat="server">
                                <Model>
                                    <ext:Model ID="Model8" IDProperty="PartyID" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PartyID" Type="String" />
                                            <ext:ModelField Name="Name" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>               
                        <DirectEvents>
                            <Select OnEvent="cmbParty_Select">
                                <EventMask ShowMask="true" />
                            </Select>
                        </DirectEvents>
                    </ext:ComboBox>
                    
        </td>
    </tr>
    <tr>
        <td>
            <ext:TextField ID="txtDocumentTitle" runat="server" LabelSeparator="" LabelAlign="Top"  MarginSpec="20 0 0 0"  FieldLabel="Document Title" Text="" Width="250" >
            </ext:TextField>
            
        </td>
    </tr>
    <tr>
        <td>
            <ext:TextField ID="txtID" runat="server" LabelSeparator="" LabelAlign="Top" ReadOnly="true" Hidden="true"  MarginSpec="20 0 0 0"  FieldLabel="Document ID" Text="" Width="450">
            </ext:TextField>
        </td>
    </tr>
    <tr>
        <td>
            <ext:ComboBox Width="250" LabelSeparator="" LabelAlign="Top" MarginSpec="20 0 0 0"
                QueryMode="Local" ID="cmbCategory" ForceSelection="true" DisplayField="Name" ValueField="CategoryID"
                runat="server" FieldLabel="Document Category">
                <ListConfig>
                    <ItemTpl ID="ItemTpl3" runat="server">
                        <Html>
                            <tpl if="Name==''">
							                        <div class="button"><a href="javascript:void(0)" class="button add" onclick="addNewCategory();">Add New Category</a></div>
						                        </tpl>
                            <tpl if="Name!=''>
                                                    {Name}
					                            </tpl>
                        </Html>
                    </ItemTpl>
                </ListConfig>
                <Store>
                    <ext:Store ID="storeSubCategory" runat="server">
                        <Model>
                            <ext:Model ID="Model2" IDProperty="CategoryID" runat="server">
                                <Fields>
                                    <ext:ModelField Name="CategoryID" Type="String" />
                                    <ext:ModelField Name="Name" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>               
                <DirectEvents>
                    <Select OnEvent="cmbCategory_Select">
                        <EventMask ShowMask="true" />
                    </Select>
                </DirectEvents>
            </ext:ComboBox>
            
        </td>
    </tr>
    <tr>
        <td>
            <ext:ComboBox Width="250" LabelSeparator="" LabelAlign="Top" MarginSpec="20 0 0 0"
                QueryMode="Local" ID="cmbDocumentSubCategory" ForceSelection="true" DisplayField="Name" ValueField="SubCategoryID"
                runat="server" FieldLabel="Sub-Category">
                <ListConfig>
                    <ItemTpl ID="ItemTpl4" runat="server">
                        <Html>
                            <tpl if="Name==''">
							                        <div class="button"><a href="javascript:void(0)" class="button add" onclick="addNewSubCategory();">Add New Sub-Category</a></div>
						                        </tpl>
                            <tpl if="Name!=''>
                                                    {Name}
					                            </tpl>
                        </Html>
                    </ItemTpl>
                </ListConfig>
                <Store>
                    <ext:Store ID="Store1" runat="server">
                        <Model>
                            <ext:Model ID="Model1" IDProperty="SubCategoryID" runat="server">
                                <Fields>
                                    <ext:ModelField Name="SubCategoryID" Type="String" />
                                    <ext:ModelField Name="Name" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                               
                <DirectEvents>
                    <Select OnEvent="cmbDocumentSubCategory_Select">
                        <EventMask ShowMask="true" />
                    </Select>
                </DirectEvents>
            </ext:ComboBox>
            
        </td>
    </tr>
   
    <tr>
        <td>
            <pr:CalendarExtControl ID="calDocumentDate" LabelSeparator="" runat="server" FieldLabel="Document Date" MarginSpec="20 0 0 0"
                Width="250" LabelAlign="Top">
            </pr:CalendarExtControl>
            
        </td>
    </tr>
    <tr>
        <td>
            <ext:FieldContainer runat="server">
                <Content>
                    <table>
                         <tr>
                            <td>
                                <pr:CalendarExtControl ID="calExpirationDate" LabelSeparator="" runat="server" FieldLabel="Expiration Date" MarginSpec="20 0 0 0"
                                    Width="250" LabelAlign="Top">
                                </pr:CalendarExtControl>  
                            </td>
                            <td valign="bottom">    
                                <ext:Checkbox ID="chkDoesNotExpire" runat="server" BoxLabel="Does not expire" BoxLabelAlign="After"  MarginSpec="20 0 0 20">
                                    <Listeners>
                                        <Change Fn="chkDoesNotExpireChange" />
                                    </Listeners>
                                </ext:Checkbox>
                            </td>
                        </tr>
                    </table>
                </Content>
            </ext:FieldContainer>
        </td>
    </tr>
</table>




<table>
    <tr>
        <td>
            <ext:TextArea ID="txtDescription" runat="server" TextMode="multiline" Width="450"  MarginSpec="20 0 0 0" FieldLabel="Description" LabelAlign="Top" LabelSeparator="" />
            
        </td>
    </tr>
    <tr>
        <td>
            <ext:Checkbox  ID="chkIsPrivate" runat="server" BoxLabel="Private" BoxLabelAlign="After" MarginSpec="20 0 0 0" Hidden="true" />
        </td>
    </tr>
    <tr>
        <td>
            <ext:Checkbox  ID="chkNotifyAssigneesByEmail" Hidden="true" runat="server" FieldLabel="Notify Assigness by Email"  MarginSpec="20 0 0 0" LabelAlign="Top" Width="250" LabelSeparator="" />
        </td>
    </tr>
    <tr>
        <td>
            
                    <ext:ComboBox LabelSeparator="" ID="cmbAssignTo" runat="server" DisplayField="Text" MarginSpec="20 0 0 0" Hidden="true"
                        FieldLabel="Assign to" LabelAlign="Top" ValueField="Value"
                        StoreID="storeUserSearch" TypeAhead="false" Width="250" HideBaseTrigger="true" MinChars="1" 
                        TriggerAction="All" ForceSelection="true">
                        <ListConfig LoadingText="Searching..." MinWidth="250" StyleSpec="border-top:1px solid #98c0f4;">
                            <ItemTpl ID="ItemTpl2" runat="server">
                                <Html>
                                    <tpl>
                                            <div class="search-item">
                                                            <span>{Text}</span>  
                                             </div>
					                </tpl>
                                </Html>
                            </ItemTpl>
                        </ListConfig>
                        <Listeners>
                            <Select Handler="this.getTrigger(0).show();" />
                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                        </Listeners>
                        <DirectEvents>
                            <Select OnEvent="cmbAssignTo_Select">
                                <ExtraParams>
                                    <ext:Parameter Name="itemAssignTo" Value="Ext.encode(#{gridAssignTo}.getRowsValues({selectedOnly : false}))"
                                        Mode="Raw" />
                                </ExtraParams>
                                <EventMask ShowMask="true" />
                            </Select>
                        </DirectEvents>
                    </ext:ComboBox>
               </td>
    </tr>
        <tr>
            <td>
                    <ext:GridPanel ID="gridAssignTo" runat="server" Width="240" MarginSpec="10 20 0 0"
                            Scroll="None" Cls="itemgrid" Hidden="true">
                            <Store>
                                <ext:Store ID="Store4" runat="server">
                                    <Model>
                                        <ext:Model ID="Model6" runat="server" IDProperty="Value">
                                            <Fields>
                                                <ext:ModelField Name="Value" Type="String" />
                                                <ext:ModelField Name="Text" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <ColumnModel>
                                <Columns>
                                    <ext:Column ID="Column1" Sortable="false" MenuDisabled="true" runat="server" Text="Name"
                                        Align="Left" Width="200" DataIndex="Text" />
                                    <ext:CommandColumn ID="CommandColumn1" runat="server" Sortable="false" MenuDisabled="true"
                                        Width="40" Align="Center">
                                        <Commands>
                                            <ext:GridCommand Cls="editGridButton" ToolTip-Text="Delete" Text="<i class='fa fa-trash-o'></i>"
                                                    CommandName="Delete" />
                                        </Commands>
                                        <Listeners>
                                            <Command Fn="RemoveItemLine">
                                            </Command>
                                        </Listeners>
                                    </ext:CommandColumn>
                                </Columns>
                            </ColumnModel>
                            <SelectionModel>
                                <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                            </SelectionModel>
                        </ext:GridPanel>
            </td>
        </tr>               




  <tr>
    <td>
        <ext:DisplayField ID="dfAdditionalInformation" runat="server" Text = "Additional Information" MarginSpec="20 0 0 0" Hidden="true" />
    </td>
  </tr>
    <tr>
        <td>
   
            <ext:GridPanel ID="gridAdditionalInformation" runat="server" Width="400" MarginSpec="10 20 0 0"
                    Scroll="None" Cls="itemgrid" HideHeaders="true" Hidden="true">
                <Store>
                    <ext:Store ID="Store5" runat="server">
                        <Model>
                            <ext:Model ID="Model7" runat="server" IDProperty="AdditionalInformationID">
                                <Fields>
                                    <ext:ModelField Name="AdditionalInformationID" Type="String" />
                                    <ext:ModelField Name="Name" Type="String" />
                                    <ext:ModelField Name="Value" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:Column ID="Column2" Sortable="false" MenuDisabled="true" runat="server" Text=""
                            Align="Left" Width="240" DataIndex="Name" />
                        <ext:Column ID="Column3" Sortable="false" MenuDisabled="true" runat="server" Text=""
                            Align="Right" Width="160" DataIndex="Value">
                            <Editor>
                                <ext:TextField ID="txtValue" runat="server" />
                            </Editor>    
                        </ext:Column>                                    
                    </Columns>
                </ColumnModel>
                <Plugins>
                    <ext:CellEditing ID="CellEditing1" runat="server" ClicksToEdit="1">
                    </ext:CellEditing>
                </Plugins>
                <SelectionModel>
                    <ext:RowSelectionModel ID="RowSelectionModel3" runat="server" Mode="Single" />
                </SelectionModel>
            </ext:GridPanel>
        </td>
    </tr>
</table>
<table>  
    

    <tr>
    
        <td>
                        <ext:FileUploadField ID="fileUpload1" runat="server"   MarginSpec="20 0 0 0" Width="125" FieldLabel="Attachments"
                            ButtonOnly="true" LabelSeparator="" LabelAlign="Left" >
                            <DirectEvents>
                                <Change OnEvent="file_Added">
                                    <ExtraParams>
                                        <ext:Parameter Name="itemList" Value="Ext.encode(#{gridFiles}.getRowsValues({selectedOnly : false}))"
                                            Mode="Raw" />
                                    </ExtraParams>
                                    <EventMask ShowMask="true" />
                                </Change>
                            </DirectEvents>
                        </ext:FileUploadField>
                    </td>
                    <td>
                        <ext:GridPanel ID="gridFiles" runat="server" Width="280" MarginSpec="10 20 0 0" Hidden="true"
                             Scroll="None" Cls="itemgrid">
                            <Store>
                                <ext:Store ID="Store3" runat="server">
                                    <Model>
                                        <ext:Model ID="Model4" runat="server" IDProperty="ServerFileName">
                                            <Fields>
                                                <ext:ModelField Name="DocumentRef_ID" Type="String" />
                                                <ext:ModelField Name="FileID" Type="String" />
                                                <ext:ModelField Name="ServerFileName" Type="String" />
                                                <ext:ModelField Name="UserFileName" Type="String" />
                                                <ext:ModelField Name="FileLocation" Type="String" />
                                                <ext:ModelField Name="FileFormat" Type="String" />
                                                <ext:ModelField Name="Size" Type="String" />
                                                <ext:ModelField Name="FileType" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <ColumnModel>
                                <Columns>
                                    <ext:Column ID="Column6" Sortable="false" MenuDisabled="true" runat="server" Text="File Name"
                                        Align="Left" Width="200" DataIndex="UserFileName" />
                                    <ext:CommandColumn ID="CommandColmnGFDelete" runat="server" Width="40" Text="" Align="Center" >
                                        <Commands>
                                            <ext:CommandSeparator />
                                            <ext:GridCommand Cls="editGridButton" ToolTip-Text="Delete" Text="<i class='fa fa-trash-o'></i>"
                                                    CommandName="Delete" />
                                        </Commands>
                                        <Listeners>
                                            <Command Handler="CommandHandlerFile(command,record);" />
                                        </Listeners>
                                    </ext:CommandColumn>
                                    <ext:CommandColumn ID="CommandColmGFDownload" runat="server" Width="40" Text="" Align="Center" Hidden="true">
                                        <Commands>
                                            <ext:CommandSeparator />
                                            <ext:GridCommand Icon="PageWhitePut" CommandName="DownLoad">
                                                <ToolTip Text="DownLoad" />
                                            </ext:GridCommand>
                                        </Commands>
                                        <PrepareToolbar Fn="prepareToolbar" />
                                        <Listeners>
                                            <Command Handler="CommandHandlerFile(command,record);" />
                                        </Listeners>
                                    </ext:CommandColumn>
                                    
                                </Columns>
                            </ColumnModel>
                            <SelectionModel>
                                <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                            </SelectionModel>
                        </ext:GridPanel>
                    </td>
            
        
    </tr>

</table>

<br />
</div>

</Content>
 <Buttons>
    <ext:Button runat="server" ID="btnSave" Text="Save" MarginSpec="20 0 0 20">
                    <DirectEvents>
                        <Click OnEvent="btnSave_Click">
                            <EventMask ShowMask="true" />
                            <ExtraParams>
                                <ext:Parameter Name="itemAssignTo" Value="Ext.encode(#{gridAssignTo}.getRowsValues({selectedOnly : false}))"
                                    Mode="Raw" />
                                <ext:Parameter Name="itemList" Value="Ext.encode(#{gridFiles}.getRowsValues({selectedOnly : false}))"
                                            Mode="Raw" />
                                <ext:Parameter Name="itemAddInformList" Value="Ext.encode(#{gridAdditionalInformation}.getRowsValues({selectedOnly : false}))"
                                            Mode="Raw" />
                            </ExtraParams>
                        </Click>
                    </DirectEvents>
                    <Listeners>
                        <Click Handler="valGroup = 'SaveUpd'; if(CheckValidation()) return ''; else return false;">
                        </Click>
                    </Listeners>
                </ext:Button>     
          
    <ext:Button runat="server" ID="btnCancel" Text="<i></i>Cancel" MarginSpec="20 0 0 10">
                    <DirectEvents>
                        <Click OnEvent="btnCancel_Click">
                            <EventMask ShowMask="true" />
                        </Click>
                    </DirectEvents>
                </ext:Button>
</Buttons>
</ext:FormPanel>

 </td>
 <td style="width:20px;"></td>
        <td id="tdAddNew" runat="server" valign="top" style="margin-left:20px; display:none;">
        
        
<div style="background-color:#B1FFB1;  height:40px; padding-left:20px; padding-top:10px; width:500px;">
    <ext:DisplayField ID="dfMsg" runat="server" Text="New Message" FieldStyle="color:#177ECB;" />
</div>

<br />

<ext:Button runat="server" ID="btnAddNew" Text="Add New" MarginSpec="0 0 0 0" Width="100">
    <DirectEvents>
        <Click OnEvent="btnAddNew_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>             
</ext:Button>     

</td>
    </tr>
</table>


 <uc1:DocumentCategoryCtrl Id="DocumentCategoryCtrl1" runat="server" />
 <uc2:DocumentSubCategoryCtrl Id="DocumentSubCategoryCtrl1" runat="server" />
 <uc3:DocPartyCtrl Id="DocPartyCtrl1" runat="server" />
</div>


<asp:RequiredFieldValidator Display="None" ID="rfvParty" runat="server" ValidationGroup="SaveUpd"
    ControlToValidate="cmbParty" ErrorMessage="Name of the Party is required." />
<asp:RequiredFieldValidator Display="None" ID="rfvDocumentTitle" runat="server" ValidationGroup="SaveUpd"
                        ControlToValidate="txtDocumentTitle" ErrorMessage="Document Title is required." />
<asp:RequiredFieldValidator Display="None" ID="rfvCategory" runat="server" ValidationGroup="SaveUpd"
                        ControlToValidate="cmbCategory" ErrorMessage="Document Category is required." />
<asp:RequiredFieldValidator Display="None" ID="rfvSubCategory" runat="server" ValidationGroup="SaveUpd"
                        ControlToValidate="cmbDocumentSubCategory" ErrorMessage="Document Sub-Category is required." />

<asp:RequiredFieldValidator Display="None" ID="rfvDocumentDate" runat="server" ValidationGroup="SaveUpd"
                        ControlToValidate="calDocumentDate" ErrorMessage="Document Date is required." />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
        