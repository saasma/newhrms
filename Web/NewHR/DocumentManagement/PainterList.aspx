﻿<%@ Page Title="Painters" Language="C#" MasterPageFile="~/Master/NewDetails.Master" AutoEventWireup="true" CodeBehind="PainterList.aspx.cs" Inherits="Web.NewHR.DocumentManagement.PainterList" %>

<%@ Register Src="~/newhr/UserControls/PainterListCtrl.ascx" TagName="PainterListCtrl"
    TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

<style type="text/css">
    .hideLeftBlockCssInPage
    {
        margin: 0px !important;
        padding-left: 20px !important;
    }
    #menu
    {
        display: none;
    }
         
</style>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">

<div class="pageheader">
    <div class="media">
        <div class="media-body">
            <h4 id="pageTitle" runat="server">
                Painters
            </h4>
        </div>
    </div>
</div>

<div class="contentpanel">

<uc1:PainterListCtrl Id="PainterListCtrl1" runat="server" />

</div>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
