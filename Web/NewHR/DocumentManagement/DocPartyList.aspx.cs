﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Entity;
using BLL;
using System.IO;
using Utils;
using System.Text;
using BLL.BO;

namespace Web.NewHR.DocumentManagement
{
    public partial class DocPartyList : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.DocumentManagement;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
                Initialise();
        }

        private void Initialise()
        {
            BindPartyGrid();
        }

        private void BindPartyGrid()
        {
            gridDocParty.Store[0].DataSource = DocumentManager.GetDocPartySearch("");
            gridDocParty.Store[0].DataBind();
        }

        protected void btnAddNewParty_Click(object sender, DirectEventArgs e)
        {
            DocPartyCtrl1.AddNewDocParty("1");
        }

        

        protected void btnPartyEdit_Click(object sender, DirectEventArgs e)
        {
            if (string.IsNullOrEmpty(hdnPartyID.Text))
                return;

            Guid partyID = new Guid(hdnPartyID.Text.Trim());

            DocPartyCtrl1.EditDocParty(partyID);
        }

        protected void btnPartyDelete_Click(object sender, DirectEventArgs e)
        {
            if (string.IsNullOrEmpty(hdnPartyID.Text))
                return;

            Guid partyID = new Guid(hdnPartyID.Text.Trim());

            Status status = DocumentManager.DeleteDocParty(partyID);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Party is deleted.");
                BindPartyGrid();
            }
            else
                NewMessage.ShowWarningMessage(status.ErrorMessage);
        }

        protected void btnReloadGrid_Click(object sender, DirectEventArgs e)
        {
            BindPartyGrid();
        }


    }
}