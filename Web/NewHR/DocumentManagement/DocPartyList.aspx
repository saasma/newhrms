﻿<%@ Page Title="Party List" Language="C#" MasterPageFile="~/Master/NewDetails.Master" AutoEventWireup="true" CodeBehind="DocPartyList.aspx.cs" Inherits="Web.NewHR.DocumentManagement.DocPartyList" %>

<%@ Register Src="~/newhr/UserControls/DocPartyCtrl.ascx" TagName="DocPartyCtrl"
    TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

<style type="text/css">
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }

</style>

<script type="text/javascript">
    
    var CommandHandler = function(command, record){
            <%= hdnPartyID.ClientID %>.setValue(record.data.PartyID);
            
                if(command=="Edit")
                {
                    <%= btnPartyEdit.ClientID %>.fireEvent('click');
                }
                else
                {
                    <%= btnPartyDelete.ClientID %>.fireEvent('click');
                }

             };


    function reloadPartyGrid()
    {
        <%= btnReloadGrid.ClientID %>.fireEvent('click');
    }
</script>


</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">

<ext:Hidden ID="hdnPartyID" runat="server" />

<ext:Button ID="btnReloadGrid" runat="server" Hidden="true">
    <DirectEvents>
        <Click OnEvent="btnReloadGrid_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:Button>

<ext:Button ID="btnPartyEdit" runat="server" Hidden="true">
    <DirectEvents>
        <Click OnEvent="btnPartyEdit_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:Button>

<ext:Button ID="btnPartyDelete" runat="server" Hidden="true">
    <DirectEvents>
        <Click OnEvent="btnPartyDelete_Click">
            <Confirmation Message="Are you sure, you want to delete the record?" ConfirmRequest="true" />
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:Button>


<div class="pageheader">
    <div class="media">
        <div class="media-body">
            <h4 id="pageTitle" runat="server">
                Party List
            </h4>
        </div>
    </div>
</div>

<div class="contentpanel">

<ext:Button runat="server" Cls="btn btn-primary" Icon="Add" ID="btnAddNewParty" Text="<i></i>Add Party">
    <DirectEvents>
        <Click OnEvent="btnAddNewParty_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:Button>
<br />


<ext:GridPanel ID="gridDocParty" runat="server" Width="380" Cls="itemgrid">
    <Store>
        <ext:Store ID="Store1" runat="server">
            <Model>
                <ext:Model ID="Model4" runat="server" IDProperty="PartyID">
                    <Fields>
                        <ext:ModelField Name="PartyID" Type="String" />
                        <ext:ModelField Name="Name" Type="string" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
    </Store>
    <ColumnModel>
        <Columns>
            <ext:Column ID="Column5" Sortable="false" MenuDisabled="true" runat="server" Text="Name"
                Align="Left" Width="300" DataIndex="Name" />
            <ext:CommandColumn ID="CommandColumn1" runat="server" Width="80" Text="" Align="Center">
                <Commands>
                    <ext:CommandSeparator />
                    <ext:GridCommand Cls="editGridButton" ToolTip-Text="Edit" Text="<i class='fa fa-pencil'></i>"
                        CommandName="Edit" />
                    <ext:GridCommand Cls="editGridButton" ToolTip-Text="Delete" Text="<i class='fa fa-trash-o'></i>"
                        CommandName="Delete" />
                </Commands>
                <Listeners>
                    <Command Handler="CommandHandler(command,record);" />
                </Listeners>
            </ext:CommandColumn>
        </Columns>
    </ColumnModel>
    <SelectionModel>
        <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
    </SelectionModel>
</ext:GridPanel>


<uc1:DocPartyCtrl Id="DocPartyCtrl1" runat="server" />

</div>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
