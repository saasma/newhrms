﻿<%@ Page Title="Dealer/Painter" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="Dealer.aspx.cs" Inherits="Web.NewHR.DocumentManagement.Dealer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>
    <style type="text/css">
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
        
        .x-panel-default-framed
        {
            border: 2px solid #157FCC;
        }
        
        .x-form-trigger-wrap .x-form-text
        {
            border-width: 0;
            height: 30px;
        }
        
        .x-form-text
        {
            height: 30px;
        }
        
        .x-form-textarea
        {
            height: 55px !important;
        }
        
        .x-panel-body 
        {
              background-color: #e6eff7;
        }
        
        .x-panel-default-framed {
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            -ms-border-radius: 4px;
            -o-border-radius: 4px;
            border-radius: 0px;
            padding: 0;
            border-width: 2px;
            border-style: solid;
          
        }
    </style>
    <script type="text/javascript">
    

        var RenewAgreementHandler = function(command, record){
            <%= hdnAgreementID.ClientID %>.setValue(record.data.AgreementId);
       
            if(command=="Renew")
            {
                <%= btnRenewAgreement.ClientID %>.fireEvent('click');
            }
        };

        var CommandHandlerFile = function(command, record){
            <%= hdnFileID.ClientID %>.setValue(record.data.FileID);
       
            if(command=="Delete")
            {
                <%= btnDeleteFile.ClientID %>.fireEvent('click');
            }
            else if(command == "DownLoad")
            {
                <%= btnDownload.ClientID %>.fireEvent('click');
            }
        };

        var prepareToolbar = function (grid, toolbar, rowIndex, record) {

            if (record.data.FileID == '') {
                toolbar.hide();
            }
        };

              var fnRendererSN = function (value, metaData, record, rowIndex, colIndex, store) {
              return rowIndex+1;
        }

          var RemoveRenewLine = function (column, command, record, recordIndex, cellIndex) {
            var store = <%=storeAgreement.ClientID %>;
            store.remove(record);
        };

         var RemoveDocumentLine = function (column, command, record, recordIndex, cellIndex) {
            var store = <%=StoreIncome.ClientID %>;
            store.remove(record);
        };

         var RemoveAttachDocLine = function (column, command, record, recordIndex, cellIndex) {
            var store = <%=storeDocument.ClientID %>;
            store.remove(record);
        };
        


          var statusIconRenderer = function (value) {
            switch (value) {
                default:
                    return value;
                case 'Pending':
                    return '<img src="' + Ext.net.ResourceMgr.getIconUrl("Hourglass") + '" width=16 height=16>';
                case 'Sending':
                    return '<div src="x-loading-indicator" width=16 height=16>';
                case 'Error':
                    return '<img src="' + Ext.net.ResourceMgr.getIconUrl("Decline") + '" width=16 height=16>';
                case 'Cancelled':
                case 'Aborted':
                    return '<img src="' + Ext.net.ResourceMgr.getIconUrl("Decline") + '" width=16 height=16>';
                case 'Uploaded':
                    return '<img src="' + Ext.net.ResourceMgr.getIconUrl("Tick") + '" width=16 height=16>';
            }
        };

             var DocumentCommandOtherHandler = function(command, record){
             <%= hiddenValue.ClientID %>.setValue(record.data.DocumentID);
             <%=hdnServerfileName.ClientID %>.setValue(record.data.SavedFileName);
             <%=hdnFileFormat.ClientID %>.setValue(record.data.FileFormat);
             <%=hdnFileType.ClientID %>.setValue(record.data.FileType);
             <%=hdnFileName.ClientID %>.setValue(record.data.name);

                 if(command="download")
                {
                 __doPostBack('ReloadOtherDoc', '');
                }
             };

        var removeIncDocRow = function (btn) {
            var gridrecord = <%=UploadGrid.ClientID%>.getSelectionModel().getSelected();
            var rowIndex =  <%=storeDocument.ClientID %>.getAt(gridrecord[1]);
            <%=storeDocument.ClientID %>.removeAt(rowIndex);
        };

              var CommandHandlerOtherSource = function(command, record){
        
        
         <%= hiddenValue.ClientID %>.setValue(record.data.DocumentID);
             <%=hdnServerfileName.ClientID %>.setValue(record.data.SavedFileName);
             <%=hdnFileFormat.ClientID %>.setValue(record.data.FileFormat);
             <%=hdnFileType.ClientID %>.setValue(record.data.FileType);
             <%=hdnFileName.ClientID %>.setValue(record.data.name);


                 if(command=="downloadfirstDocOther")
                {
                    __doPostBack('ReloadfromIncomeGridOther', '');
                }

                 if(command=="Delete")
                {
                    <%= btnDeleteCommand.ClientID %>.fireEvent('click');
                }
             };

              var CommandHandlerGridRenew = function(command, record){
                 if(command=="Delete")
                {
                   
                }
             };


    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <ext:Hidden ID="hdnDocumentID" runat="server" />
    <ext:Hidden ID="hdnPartyID" runat="server" />
    <ext:Hidden ID="hdnPartyName" runat="server" />
    <ext:Hidden ID="hdnFileID" runat="server" />
    <ext:Hidden ID="hdnType" runat="server" />
    <ext:Hidden ID="hdnAgreementID" runat="server" />
    
    <ext:Hidden runat="server" ID="hiddenValue" Text="" />
    <ext:Hidden runat="server" ID="hdnServerfileName" />
    <ext:Hidden runat="server" ID="hdnFileFormat">
    </ext:Hidden>
    <ext:Button ID="btnDeleteCommand" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnDeleteCommand_Click">
                <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete?" />
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>

    <ext:Button ID="btnRenewAgreement" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnRenewAgreement_Click">
                <EventMask ShowMask="true" />
                <ExtraParams>
                    <ext:Parameter Name="gridRenewJSON" Value="Ext.encode(#{gridAgreement}.getRowsValues({selectedOnly : false}))"
                        Mode="Raw" />
                </ExtraParams>
            </Click>
        </DirectEvents>
    </ext:Button>

    <ext:Hidden runat="server" ID="hdnFileType">
    </ext:Hidden>
    <ext:Hidden runat="server" ID="hdnCustomerID">
    </ext:Hidden>
    <ext:Hidden runat="server" ID="hdnFileName" />
    <ext:LinkButton ID="btnDownload" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnDownload_Click">
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:LinkButton ID="btnDeleteFile" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnDeleteFile_Click">
                <ExtraParams>
                    <ext:Parameter Name="itemList" Value="Ext.encode(#{gridFiles}.getRowsValues({selectedOnly : false}))"
                        Mode="Raw" />
                </ExtraParams>
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4 id="pageTitle" runat="server">
                    Dealer/Painter
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">


<ext:FormPanel ID="FormPanel1"
                runat="server"
                Title="Dealer"
                Frame="true" PaddingSpec="0 0 0 0"
                 ButtonAlign="Left"
                Width="870" MinHeight="1150"
                Border="false"
                BodyPadding="30" Layout="AutoLayout">
<Content>
        <div style="margin-left: 20px;">
            <table>
                <tr>
                    <td>
                        <ext:FieldContainer ID="FieldContainer1" runat="server">
                            <Content>
                            
            <table>
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td>
                                    <ext:TextField ID="txtName" runat="server" LabelSeparator="" LabelAlign="Top" FieldLabel="Name *"
                                        Text="" Width="250" MarginSpec="0 0 0 0">
                                    </ext:TextField>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ext:ComboBox ID="cmbBranch" Width="250" runat="server" ValueField="BranchId" DisplayField="Name"
                                            FieldLabel="Branch *" LabelAlign="top" LabelSeparator="" ForceSelection="true" MarginSpec="20 0 0 0"
                                            QueryMode="Local">
                                            <Store>
                                                <ext:Store ID="Store5" runat="server">
                                                    <Model>
                                                        <ext:Model ID="Model5" runat="server">
                                                            <Fields>
                                                                <ext:ModelField Name="BranchId" />
                                                                <ext:ModelField Name="Name"  />
                                                            </Fields>
                                                        </ext:Model>
                                                    </Model>
                                                </ext:Store>
                                            </Store>
                                        </ext:ComboBox>
                                </td>
                            </tr>
                             <tr>
                                <td>
                                    <ext:ComboBox ID="cmbDealer" Width="250" runat="server" ValueField="ContactId" DisplayField="Name" MarginSpec="20 0 0 0"
                                            FieldLabel="Dealer *" LabelAlign="top" LabelSeparator="" ForceSelection="true" Hidden="true"
                                            QueryMode="Local">
                                            <Store>
                                                <ext:Store ID="Store2" runat="server">
                                                    <Model>
                                                        <ext:Model ID="Model4" runat="server">
                                                            <Fields>
                                                                <ext:ModelField Name="Name" />
                                                                <ext:ModelField Name="ContactId" />
                                                            </Fields>
                                                        </ext:Model>
                                                    </Model>
                                                </ext:Store>
                                            </Store>
                                        </ext:ComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ext:TextField ID="txtArea" runat="server" LabelSeparator="" LabelAlign="Top" 
                                        MarginSpec="20 0 0 0" FieldLabel="Area" Text="" Width="250">
                                    </ext:TextField>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ext:TextField ID="txtID" runat="server" LabelSeparator="" LabelAlign="Top"
                                        MarginSpec="20 0 0 0" FieldLabel="ID" Text="" Width="250">
                                    </ext:TextField>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ext:TextField ID="txtDealerNumber" runat="server" LabelSeparator="" LabelAlign="Top"
                                        MarginSpec="20 0 0 0" FieldLabel="Phone Number *" Text="" Width="250">
                                    </ext:TextField>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ext:ComboBox ID="cmbZone" FieldLabel="Zone *" runat="server" LabelAlign="Top" DisplayField="Zone"
                                        Width="250" LabelSeparator="" QueryMode="Local" ValueField="ZoneId" MarginSpec="20 0 0 0">
                                        <Store>
                                            <ext:Store ID="Store1" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="Zone" />
                                                    <ext:ModelField Name="ZoneId" />
                                                </Fields>
                                            </ext:Store>
                                        </Store>
                                        <DirectEvents>
                                            <Select OnEvent="ZoneChange">
                                            </Select>
                                        </DirectEvents>
                                    </ext:ComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ext:ComboBox ID="cmbDistrict" FieldLabel="District *" runat="server" LabelAlign="Top" MarginSpec="20 0 0 0"
                                        Width="250" LabelSeparator="" QueryMode="Local" DisplayField="District" ValueField="DistrictId">
                                        <Store>
                                            <ext:Store ID="storeDistrict" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="District" />
                                                    <ext:ModelField Name="DistrictId" />
                                                </Fields>
                                            </ext:Store>
                                        </Store>
                                    </ext:ComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ext:TextArea ID="txtAddress" runat="server" TextMode="multiline" Width="250" MarginSpec="20 0 0 0"
                                        FieldLabel="Address" LabelAlign="Top" LabelSeparator="" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ext:TextField ID="txtContactPerson" runat="server" LabelSeparator="" LabelAlign="Top"
                                        MarginSpec="20 0 0 0" FieldLabel="Contact Person" Text="" Width="250">
                                    </ext:TextField>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ext:TextField ID="txtMobileNo" runat="server" LabelSeparator="" LabelAlign="Top"
                                        MarginSpec="20 0 0 0" FieldLabel="Mobile Number" Text="" Width="250">
                                    </ext:TextField>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ext:TextField ID="txtPhoneNo" runat="server" LabelSeparator="" LabelAlign="Top"
                                        MarginSpec="20 0 0 0" FieldLabel="Phone Number" Text="" Width="250">
                                    </ext:TextField>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ext:TextField ID="txtEmail" runat="server" LabelSeparator="" LabelAlign="Top" MarginSpec="20 0 0 0"
                                        FieldLabel="E-Mail Address" Text="" Width="250">
                                    </ext:TextField>
                                </td>
                            </tr>
                        </table>
                        
                    </td>
                    <td valign="top">
                        <table class="fieldTable firsttdskip" style="margin-left: 50px">
                            <tr>
                                <td colspan="2">
                                    <ext:GridPanel ID="gridAgreement" runat="server" Width="500" Cls="gridheight">
                                        <Store>
                                            <ext:Store ID="storeAgreement" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model3" runat="server" IDProperty="AgreementId">
                                                        <Fields>
                                                            <ext:ModelField Name="AgreementId" />
                                                            <ext:ModelField Name="StartDate" />
                                                            <ext:ModelField Name="EndDate" />
                                                            <ext:ModelField Name="AgrementPeriod" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:Column ID="Column5" runat="server" Text="" DataIndex="AgrementPeriod" Width="200" />
                                                <ext:Column ID="Column6" runat="server" Width="100" Text="Start Date" DataIndex="StartDate">
                                                </ext:Column>
                                                <ext:Column ID="Column2" runat="server" Width="100" Text="End Date" DataIndex="EndDate">
                                                </ext:Column>
                                                <ext:CommandColumn ID="CCAgreementDelete" runat="server" Width="40">
                                                    <Commands>
                                                        <ext:GridCommand Icon="Delete" CommandName="Delete">
                                                            <ToolTip Text="Delete" />
                                                        </ext:GridCommand>
                                                    </Commands>
                                                    <Listeners>
                                                        <Command Fn="RemoveRenewLine">
                                                        </Command>
                                                    </Listeners>
                                                </ext:CommandColumn>
                                                <ext:CommandColumn ID="CCRenew" runat="server" Width="60">
                                                    <Commands>
                                                        <ext:GridCommand Text="Renew" CommandName="Renew">
                                                            <ToolTip Text="Renew" />
                                                        </ext:GridCommand>
                                                    </Commands>
                                                    <Listeners>
                                                        <Command Handler="RenewAgreementHandler(command,record);" />
                                                    </Listeners>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <View>
                                            <ext:GridView ID="GridView1" runat="server" EmptyText="No Renew has been added yet." />
                                        </View>
                                        <SelectionModel>
                                            <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                                        </SelectionModel>
                                    </ext:GridPanel>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="bottomBlock btns" style='width: 625px!important'>
                                        <span class="left">
                                            <ext:Button runat="server" Width="120" Text="Add Period" ID="btnAdd">
                                                <DirectEvents>
                                                    <Click OnEvent="btnAgreementRenew_Click">
                                                        <EventMask ShowMask="true" />
                                                    </Click>
                                                </DirectEvents>
                                            </ext:Button>
                                        </span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <ext:GridPanel ID="GridIncome" runat="server" Width="500" Cls="gridheight" MarginSpec="20 0 0 0">
                                        <Store>
                                            <ext:Store ID="StoreIncome" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model1" runat="server" IDProperty="DocumentID">
                                                        <Fields>
                                                            <ext:ModelField Name="DocumentID" />
                                                            <ext:ModelField Name="Period" />
                                                            <ext:ModelField Name="name" />
                                                            <ext:ModelField Name="DateAdded" />
                                                            <ext:ModelField Name="size" />
                                                            <ext:ModelField Name="status" />
                                                            <ext:ModelField Name="progress" />
                                                            <ext:ModelField Name="SavedFileName" />
                                                            <ext:ModelField Name="FileFormat" />
                                                            <ext:ModelField Name="FileType" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:Column ID="Column1" runat="server" Text="SN" Width="40">
                                                    <Renderer Fn="fnRendererSN">
                                                    </Renderer>
                                                </ext:Column>
                                                <ext:Column ID="Column4" runat="server" Text="Document Name" DataIndex="name" Flex="120" />
                                                <ext:Column ID="Column3" runat="server" Width="110" Text="Document Period" DataIndex="Period">
                                                </ext:Column>
                                                <ext:Column ID="Coasdflumn2" runat="server" Width="100" Text="Date Added" DataIndex="DateAdded">
                                                </ext:Column>
                                                <ext:CommandColumn ID="CommandColumn1" runat="server" Width="40">
                                                    <Commands>
                                                        <ext:GridCommand Icon="Attach" CommandName="downloadfirstDocOther">
                                                            <ToolTip Text="Download" />
                                                        </ext:GridCommand>
                                                    </Commands>
                                                    <Listeners>
                                                        <Command Handler="CommandHandlerOtherSource(command,record);" />
                                                    </Listeners>
                                                </ext:CommandColumn>
                                                <ext:CommandColumn ID="CommandGridOperationsDel" runat="server" Width="40">
                                                    <Commands>
                                                        <%-- <ext:GridCommand Icon="NoteEdit" CommandName="Edit">
                                                            <ToolTip Text="Edit" />
                                                        </ext:GridCommand>--%>
                                                        <ext:GridCommand Icon="Delete" CommandName="Delete">
                                                            <ToolTip Text="Delete" />
                                                        </ext:GridCommand>
                                                    </Commands>
                                                    <Listeners>
                                                        <Command Fn="RemoveDocumentLine">
                                                        </Command>
                                                    </Listeners>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <View>
                                            <ext:GridView ID="GridView2" runat="server" EmptyText="No Documents has been added yet." />
                                        </View>
                                        <SelectionModel>
                                            <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                                        </SelectionModel>
                                    </ext:GridPanel>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table class="innerTable">
                                        <tr>
                                            <td>
                                                <div class="bottomBlock btns" style='width: 625px!important'>
                                                    <span class="left">
                                                        <ext:Button runat="server" Width="120" Text="Add Document" ID="btnAddIncome">
                                                            <DirectEvents>
                                                                <Click OnEvent="btnAddIncome_Click">
                                                                    <EventMask ShowMask="true" />
                                                                </Click>
                                                            </DirectEvents>
                                                        </ext:Button>
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>
            </table>

            </Content>
                        </ext:FieldContainer>
                    </td>
                </tr>
            </table>

     <br />
        </div>

</Content>
<Buttons>
      
                        <ext:Button runat="server" ID="btnSave" Text="Save" MarginSpec="20 0 0 20">
                            <DirectEvents>
                                <Click OnEvent="btnSave_Click">
                                    <EventMask ShowMask="true" />
                                    <ExtraParams>
                                        <ext:Parameter Name="saveJsonGridIncome" Value="Ext.encode(#{GridIncome}.getRowsValues({selectedOnly : false}))"
                                            Mode="Raw" />
                                        <ext:Parameter Name="saveJsongridAgreement" Value="Ext.encode(#{gridAgreement}.getRowsValues({selectedOnly : false}))"
                                            Mode="Raw" />
                                    </ExtraParams>
                                </Click>
                            </DirectEvents>
                            <Listeners>
                                <Click Handler="valGroup = 'saveUpdate'; if(CheckValidation()) return ''; else return false;">
                                </Click>
                            </Listeners>
                        </ext:Button>
</Buttons>
                    
           
        
    

</ext:FormPanel>
    </div>

  <ext:Window ID="WindowOtherSrc" runat="server" AutoRender="False" Collapsible="false"
        Height="310" Width="650" Modal="True" BodyPadding="10" Resizable="true" Title="Documents"
        Hidden="True" ButtonAlign="Left">
        <Content>
            <table class="fieldTable firsttdskip">
                <tr>
                    <td valign="top">
                        <ext:TextField ID="txtDocumentName" runat="Server" LabelAlign="Left" FieldLabel="Name"
                            LabelWidth="50" Width="200" LabelSeparator="">
                        </ext:TextField>
                        <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator2" runat="server"
                            ValidationGroup="SaveOtherDoc" ControlToValidate="txtDocumentName" ErrorMessage="Document Name is required." />
                    </td>
                    <td rowspan="4">
                        <ext:GridPanel ID="UploadGrid" runat="server" Width="400" Height="150" Frame="false">
                            <Store>
                                <ext:Store ID="storeDocument" runat="server">
                                    <Model>
                                        <ext:Model ID="Model2" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="DocumentID" />
                                                <ext:ModelField Name="name" />
                                                <ext:ModelField Name="size" />
                                                <ext:ModelField Name="status" />
                                                <ext:ModelField Name="progress" />
                                                <ext:ModelField Name="SavedFileName" />
                                                <ext:ModelField Name="FileFormat" />
                                                <ext:ModelField Name="FileType" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <ColumnModel>
                                <Columns>
                                    <ext:Column ID="Column8" runat="server" Text="File Name" DataIndex="name" Width="150" />
                                    <ext:Column ID="Column9" runat="server" Text="Size" DataIndex="size" Width="60">
                                        <Renderer Format="FileSize" />
                                    </ext:Column>
                                    <ext:Column ID="Column10" runat="server" Text="&nbsp;" DataIndex="status" Width="30">
                                        <Renderer Fn="statusIconRenderer" />
                                    </ext:Column>
                                    <ext:CommandColumn ID="CommandGridOpeasdfarations" runat="server" Width="60" Align="Center">
                                        <Commands>
                                            <ext:CommandSpacer Width="20">
                                            </ext:CommandSpacer>
                                            <ext:GridCommand Icon="PageWhitePut" CommandName="Download">
                                                <ToolTip Text="Download" />
                                            </ext:GridCommand>
                                        </Commands>
                                        <Listeners>
                                            <Command Handler="DocumentCommandOtherHandler(command,record);" />
                                        </Listeners>
                                    </ext:CommandColumn>
                                    <ext:CommandColumn ID="CommandColumn2" runat="server" Width="40">
                                        <Commands>
                                            <ext:GridCommand Icon="Delete" CommandName="Delete">
                                                <ToolTip Text="Delete" />
                                            </ext:GridCommand>
                                        </Commands>
                                        <Listeners>
                                            <Command Fn="RemoveAttachDocLine">
                                            </Command>
                                        </Listeners>
                                    </ext:CommandColumn>
                                </Columns>
                            </ColumnModel>
                            <TopBar>
                                <ext:Toolbar ID="Toolbar1" runat="server">
                                    <Items>
                                        <ext:FileUploadField ID="FileUploadIncomeSource" runat="server" ButtonText="Browse"
                                            Width="100" Icon="Attach" Visible="true" ButtonOffset="1" ButtonOnly="true">
                                            <DirectEvents>
                                                <Change OnEvent="UploadIncomeDocClick" />
                                            </DirectEvents>
                                        </ext:FileUploadField>
                                        <ext:ToolbarSeparator />
                                    </Items>
                                </ext:Toolbar>
                            </TopBar>
                        </ext:GridPanel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:TextField LabelAlign="Left" LabelSeparator="" FieldLabel="Period" runat="server"
                            EmptyText="Year / Month" ID="txtPeriod" LabelWidth="50" Width="200">
                        </ext:TextField>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp
                    </td>
                </tr>
            </table>
        </Content>
        <Buttons>
            <ext:Button runat="server" Text="Save" ID="Button1">
                <DirectEvents>
                    <Click OnEvent="btnSaveWindowDoc_Click">
                        <EventMask ShowMask="true" />
                        <ExtraParams>
                            <ext:Parameter Name="ImagesLine" Value="Ext.encode(#{UploadGrid}.getRowsValues({selectedOnly : false}))"
                                Mode="Raw" />
                            <ext:Parameter Name="gridDocumentJSON" Value="Ext.encode(#{GridIncome}.getRowsValues({selectedOnly : false}))"
                                Mode="Raw" />
                        </ExtraParams>
                    </Click>
                </DirectEvents>
                <Listeners>
                    <Click Handler="valGroup = 'SaveOtherDoc';return CheckValidation();">
                    </Click>
                </Listeners>
            </ext:Button>
            <ext:Button ID="Button2" runat="server" Text="Cancel" Cls="graybutton">
                <Listeners>
                    <Click Handler="#{WindowOtherSrc}.hide();" />
                </Listeners>
            </ext:Button>
        </Buttons>
    </ext:Window>
    <ext:Window ID="WindowRenew" runat="server" AutoRender="False" Collapsible="false"
        Height="180" Width="300" Modal="True" BodyPadding="10" Resizable="true" Title="Renew"
        Hidden="True" ButtonAlign="Left">
        <Content>
            <table class="fieldTable firsttdskip">
                <tr>
                    <td valign="top">
                        <pr:CalendarExtControl ID="txtStartDate" LabelSeparator="" runat="server" FieldLabel="Start Date *"
                            Width="120" LabelAlign="Top">
                        </pr:CalendarExtControl>
                        <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator1" runat="server"
                            ValidationGroup="SaveRenewDoc" ControlToValidate="txtStartDate" ErrorMessage="Start Date is required." />
                    </td>
                    <td valign="top">
                        <pr:CalendarExtControl ID="txtEndDate" LabelSeparator="" runat="server" FieldLabel="End Date *"
                            Width="120" LabelAlign="Top">
                        </pr:CalendarExtControl>
                        <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator3" runat="server"
                            ValidationGroup="SaveRenewDoc" ControlToValidate="txtEndDate" ErrorMessage="End Date is required." />
                    </td>
                </tr>
            </table>
        </Content>
        <Buttons>
            <ext:Button runat="server" Text="Save" ID="Button5">
                <DirectEvents>
                    <Click OnEvent="btnSaveRenew_Click">
                        <EventMask ShowMask="true" />
                        <ExtraParams>
                            <ext:Parameter Name="gridRenewJSON" Value="Ext.encode(#{gridAgreement}.getRowsValues({selectedOnly : false}))"
                                Mode="Raw" />
                        </ExtraParams>
                    </Click>
                </DirectEvents>
                <Listeners>
                    <Click Handler="valGroup = 'SaveRenewDoc';return CheckValidation();">
                    </Click>
                </Listeners>
            </ext:Button>
            <ext:Button ID="Button6" runat="server" Text="Cancel" Cls="graybutton">
                <Listeners>
                    <Click Handler="#{WindowRenew}.hide();" />
                </Listeners>
            </ext:Button>
        </Buttons>
    </ext:Window>

    <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator4" runat="server"
        ValidationGroup="saveUpdate" ControlToValidate="txtName" ErrorMessage="Name is required." />
    <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator6" runat="server"
        ValidationGroup="saveUpdate" ControlToValidate="txtDealerNumber" ErrorMessage="Phone Number is required." />
    <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator7" runat="server"
        ValidationGroup="saveUpdate" ControlToValidate="cmbZone" ErrorMessage="Zone is required." />
    <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator8" runat="server"
        ValidationGroup="saveUpdate" ControlToValidate="cmbDistrict" ErrorMessage="District is required." />



    <asp:RequiredFieldValidator Display="None" ID="rfvBranch" runat="server"
        ValidationGroup="saveUpdate" ControlToValidate="cmbBranch" ErrorMessage="Branch is required." />
    <asp:RequiredFieldValidator Display="None" ID="rfvDealer" runat="server"
        ValidationGroup="saveUpdate" ControlToValidate="cmbDealer" ErrorMessage="Dealer is required." />



</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
