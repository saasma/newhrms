﻿<%@ Page Title="Document View" Language="C#" MasterPageFile="~/Master/NewDetails.Master" AutoEventWireup="true" CodeBehind="DocumentView.aspx.cs" Inherits="Web.NewHR.DocumentManagement.DocumentView" %>

<%@ Register Src="~/newhr/UserControls/ViewDocumentCtrl.ascx" TagName="ViewDocumentCtrl"
    TagPrefix="uc1" %>
<%@ Register Src="~/newhr/UserControls/DocumentCommentCtrl.ascx" TagName="DocumentCommentCtrl"
    TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

<style type="text/css">
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
</style>


</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">

<div class="pageheader">
    <div class="media">
        <div class="media-body">
            <h4>
                Document Details
            </h4>
        </div>
    </div>
</div>

<div class="contentpanel">

<table>
    <tr>
        <td valign="top">
            <uc1:ViewDocumentCtrl Id="ViewDocumentCtrl1" runat="server" />
        </td>
        <td valign="top">
            <uc2:DocumentCommentCtrl Id="DocumentCommentCtrl1" runat="server" />
        </td>
    </tr>
</table>

</div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
