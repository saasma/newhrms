﻿<%@ Page Title="Document List" Language="C#" MasterPageFile="~/Master/NewDetails.Master" AutoEventWireup="true" CodeBehind="DocumentList.aspx.cs" Inherits="Web.NewHR.DocumentManagement.DocumentList" %>

<%@ Register Src="~/newhr/UserControls/DocumentListCtrl.ascx" TagName="DocumentListCtrl"
    TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

<style type="text/css">
    .hideLeftBlockCssInPage
    {
        margin: 0px !important;
        padding-left: 20px !important;
    }
    #menu
    {
        display: none;
    }
         
</style>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">

<div class="pageheader">
    <div class="media">
        <div class="media-body">
            <h4 id="pageTitle" runat="server">
                Document List
            </h4>
        </div>
    </div>
</div>

<div class="contentpanel">

<uc1:DocumentListCtrl Id="DocumentListCtrl1" runat="server" />

</div>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
