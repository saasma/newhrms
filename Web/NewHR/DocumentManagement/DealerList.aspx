﻿<%@ Page Title="Dealers" Language="C#" MasterPageFile="~/Master/NewDetails.Master" AutoEventWireup="true" CodeBehind="DealerList.aspx.cs" Inherits="Web.NewHR.DocumentManagement.DealerList" %>

<%@ Register Src="~/newhr/UserControls/DealerListCtrl.ascx" TagName="DealerListCtrl"
    TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

<style type="text/css">
    .hideLeftBlockCssInPage
    {
        margin: 0px !important;
        padding-left: 20px !important;
    }
    #menu
    {
        display: none;
    }
         
</style>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">

<div class="pageheader">
    <div class="media">
        <div class="media-body">
            <h4 id="pageTitle" runat="server">
                Dealers
            </h4>
        </div>
    </div>
</div>

<div class="contentpanel">

<uc1:DealerListCtrl Id="DealerListCtrl1" runat="server" />

</div>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
