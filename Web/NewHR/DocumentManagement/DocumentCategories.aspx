﻿<%@ Page Title="Document Categories" Language="C#" MasterPageFile="~/Master/NewDetails.Master" AutoEventWireup="true" CodeBehind="DocumentCategories.aspx.cs" Inherits="Web.NewHR.DocumentManagement.DocumentCategories" %>

<%@ Register Src="~/newhr/UserControls/DocumentCategoryCtrl.ascx" TagName="DocumentCategoryCtrl"
    TagPrefix="uc1" %>
<%@ Register Src="~/newhr/UserControls/DocumentSubCategoryCtrl.ascx" TagName="DocumentSubCategoryCtrl"
    TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

<style type="text/css">
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
</style>


<script type="text/javascript">
    
    var CommandCategoryHandler = function(command, record){
            <%= hdnDocumentCategory.ClientID %>.setValue(record.data.CategoryID);
            
                if(command=="Edit")
                {
                    <%= btnCategoryEdit.ClientID %>.fireEvent('click');
                }
                else
                {
                    <%= btnCategoryDelete.ClientID %>.fireEvent('click');
                }

             };

     var CommandHandler = function(command, record){
            <%= hiddenValue.ClientID %>.setValue(record.data.SubCategoryID);
            
                if(command=="Edit")
                {
                    <%= btnEdit.ClientID %>.fireEvent('click');
                }
                else
                {
                    <%= btnDelete.ClientID %>.fireEvent('click');
                }

             };

        function reloadCategoryGrid()
        {
            <%= btnReloadCategoryGrid.ClientID %>.fireEvent('click');
        }

        function reloadSubCategoryGrid()
        {
            <%= btnReloadSubCategoryGrid.ClientID %>.fireEvent('click');
        }

 </script>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">


<ext:LinkButton ID="btnReloadCategoryGrid" runat="server" Hidden="true">
    <DirectEvents>
        <Click OnEvent="btnReloadCategoryGrid_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:LinkButton>

<ext:LinkButton ID="btnReloadSubCategoryGrid" runat="server" Hidden="true">
    <DirectEvents>
        <Click OnEvent="btnReloadSubCategoryGrid_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:LinkButton>

<ext:Hidden ID="hdnDocumentCategory" runat="server" />
    <ext:LinkButton ID="btnCategoryEdit" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnCategoryEdit_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:LinkButton ID="btnCategoryDelete" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnCategoryDelete_Click">
                <EventMask ShowMask="true" />
                <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the record?" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>

     <ext:Hidden ID="hiddenValue" runat="server" />
    <ext:LinkButton ID="btnEdit" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnEdit_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:LinkButton ID="btnDelete" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnDelete_Click">
                <EventMask ShowMask="true" />
                <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the record?" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>

<div class="pageheader">
    <div class="media">
        <div class="media-body">
            <h4>
                Document Categories
            </h4>
        </div>
    </div>
</div>

<div class="contentpanel">
<h4>Categories</h4>
<ext:Button runat="server" Cls="btn btn-primary" Icon="Add" ID="btnAddNewCategory" Text="<i></i>Add Category">
    <DirectEvents>
        <Click OnEvent="btnAddNewCategory_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:Button>
<br />


<ext:GridPanel ID="gridDocCategory" runat="server" Width="430" Cls="itemgrid">
    <Store>
        <ext:Store ID="Store1" runat="server">
            <Model>
                <ext:Model ID="Model4" runat="server" IDProperty="CategoryID">
                    <Fields>
                        <ext:ModelField Name="CategoryID" Type="String" />
                        <ext:ModelField Name="Name" Type="string" />
                        <ext:ModelField Name="Code" Type="string" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
    </Store>
    <ColumnModel>
        <Columns>
            <ext:Column ID="Column5" Sortable="false" MenuDisabled="true" runat="server" Text="Name"
                Align="Left" Width="250" DataIndex="Name" />
            <ext:Column ID="Column1" Sortable="false" MenuDisabled="true" runat="server" Text="Code"
                Align="Left" Width="100" DataIndex="Code" />
            <ext:CommandColumn ID="CommandColumn1" runat="server" Width="80" Text="" Align="Center">
                <Commands>
                    <ext:CommandSeparator />
                    <ext:GridCommand Cls="editGridButton" ToolTip-Text="Edit" Text="<i class='fa fa-pencil'></i>"
                        CommandName="Edit" />
                    <ext:GridCommand Cls="editGridButton" ToolTip-Text="Delete" Text="<i class='fa fa-trash-o'></i>"
                        CommandName="Delete" />
                </Commands>
                <Listeners>
                    <Command Handler="CommandCategoryHandler(command,record);" />
                </Listeners>
            </ext:CommandColumn>
        </Columns>
    </ColumnModel>
    <SelectionModel>
        <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
    </SelectionModel>
</ext:GridPanel>

<br />
<h4>Sub Categories</h4>
 <ext:Button runat="server" Cls="btn btn-primary" Icon="Add" ID="btnAddNew" Text="<i></i>Add Sub-Category">
    <DirectEvents>
        <Click OnEvent="btnAddNew_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:Button>

<br />

   <ext:GridPanel ID="gridDocSubCategory" runat="server" Width="630" Cls="itemgrid">
        <Store>
            <ext:Store ID="Store2" runat="server">
                <Model>
                    <ext:Model ID="Model1" runat="server" IDProperty="SubCategoryID">
                        <Fields>
                            <ext:ModelField Name="SubCategoryID" Type="String" />
                            <ext:ModelField Name="Name" Type="String" />
                            <ext:ModelField Name="CategoryName" Type="String" />
                            <ext:ModelField Name="Code" Type="String" />
                        </Fields>
                    </ext:Model>
                </Model>
            </ext:Store>
        </Store>
        <ColumnModel>
            <Columns>
                <ext:Column ID="Column2" Sortable="false" MenuDisabled="true" runat="server" Text="Name"
                    Align="Left" Width="250" DataIndex="Name" />
                <ext:Column ID="Column3" Sortable="false" MenuDisabled="true" runat="server" Text="Code"
                    Align="Left" Width="100" DataIndex="Code" />
                <ext:Column ID="Column4" Sortable="false" MenuDisabled="true" runat="server" Text="Category"
                    Align="Left" Width="200" DataIndex="CategoryName" />                                        
                <ext:CommandColumn ID="CommandColumn2" runat="server" Width="80" Text="" Align="Center">
                    <Commands>
                        <ext:CommandSeparator />
                        <ext:GridCommand Cls="editGridButton" ToolTip-Text="Edit" Text="<i class='fa fa-pencil'></i>"
                            CommandName="Edit" />
                        <ext:GridCommand Cls="editGridButton" ToolTip-Text="Delete" Text="<i class='fa fa-trash-o'></i>"
                            CommandName="Delete" />
                    </Commands>
                    <Listeners>
                        <Command Handler="CommandHandler(command,record);" />
                    </Listeners>
                </ext:CommandColumn>
            </Columns>
        </ColumnModel>
        <SelectionModel>
            <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
        </SelectionModel>
    </ext:GridPanel>

    <br />
</div>

 <uc1:DocumentCategoryCtrl Id="DocumentCategoryCtrl1" runat="server" />
 <uc2:DocumentSubCategoryCtrl Id="DocumentSubCategoryCtrl1" runat="server" />

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
