﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;
using System.Text;
namespace Web.NewHR.DocumentManagement
{
    public partial class DocumentAdditionalInformation : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.DocumentManagement;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
                Initialise();
        }

        private void Initialise()
        {
            BindInformationGrid();
        }

        private void BindInformationGrid()
        {
            gridAdditionalInformation.Store[0].DataSource = DocumentManager.GetDocAdditionalInformations();
            gridAdditionalInformation.Store[0].DataBind();
        }

        protected void btnAddNew_Click(object sender, DirectEventArgs e)
        {
            hdnAdditionalInformationID.Clear();
            txtName.Clear();
            btnSave.Text = "Save";
            WAdditionalInformation.Center();
            WAdditionalInformation.Show();
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveUpdate");
            if (Page.IsValid)
            {
                DocAdditionalInformation obj = new DocAdditionalInformation();

                if (!string.IsNullOrEmpty(hdnAdditionalInformationID.Text.Trim()))
                    obj.AdditionalInformationID = new Guid(hdnAdditionalInformationID.Text.Trim());
                else
                    obj.AdditionalInformationID = Guid.NewGuid();

                obj.Name = txtName.Text.Trim();

                Status status = DocumentManager.SaveUpdateDocAdditionalInformation(obj);
                if (status.IsSuccess)
                {
                    WAdditionalInformation.Close();

                    if (!string.IsNullOrEmpty(hdnAdditionalInformationID.Text.Trim()))
                        NewMessage.ShowNormalMessage("Record updated succesffully.");
                    else
                        NewMessage.ShowNormalMessage("Record saved succesffully.");

                    BindInformationGrid();
                }
                else
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        protected void btnEdit_Click(object sender, DirectEventArgs e)
        {
            Guid additionalInformationID = new Guid(hdnAdditionalInformationID.Text.Trim());
            DocAdditionalInformation obj = DocumentManager.GetDocAdditionalInformationById(additionalInformationID);
            if (obj != null)
            {
                txtName.Text = obj.Name;

                btnSave.Text = "Update";
                WAdditionalInformation.Center();
                WAdditionalInformation.Show();
            }
        }

        protected void btnDelete_Click(object sender, DirectEventArgs e)
        {
            Guid additionalInformationID = new Guid(hdnAdditionalInformationID.Text.Trim());
            Status status = DocumentManager.DeleteDocAdditionalInformation(additionalInformationID);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Record deleted succesffully.");

                BindInformationGrid();
            }
            else
                NewMessage.ShowWarningMessage(status.ErrorMessage);
        }


    }
}