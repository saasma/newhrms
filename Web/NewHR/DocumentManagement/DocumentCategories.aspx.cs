﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;
using System.Text;

namespace Web.NewHR.DocumentManagement
{
    public partial class DocumentCategories : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.DocumentManagement;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        private void Initialise()
        {            
            BindDocCategoryGrid();
            BindDocSubCategories();
        }

        private void BindDocCategoryGrid()
        {
            gridDocCategory.GetStore().DataSource = DocumentManager.GetDocCategories();
            gridDocCategory.GetStore().DataBind();
        }

        private void BindDocSubCategories()
        {
            gridDocSubCategory.GetStore().DataSource = DocumentManager.GetDocSubCategories();
            gridDocSubCategory.GetStore().DataBind();
        }


        protected void btnAddNewCategory_Click(object sender, DirectEventArgs e)
        {
            DocumentCategoryCtrl1.AddNewCategory("1");
        }

        protected void btnCategoryEdit_Click(object sender, DirectEventArgs e)
        {
            Guid categoryId = new Guid(hdnDocumentCategory.Text);
            DocumentCategoryCtrl1.EditCategory(categoryId);
        }

        protected void btnCategoryDelete_Click(object sender, DirectEventArgs e)
        {
            Guid categoryId = new Guid(hdnDocumentCategory.Text);

            Status status = DocumentManager.DeleteDocCategories(categoryId);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Record deleted successfully.");
                BindDocCategoryGrid();
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }
                
        protected void btnAddNew_Click(object sender, DirectEventArgs e)
        {
            DocumentSubCategoryCtrl1.AddNewSubCategory("1", null);
        }        

        protected void btnEdit_Click(object sender, DirectEventArgs e)
        {
            Guid subCategoryId = new Guid(hiddenValue.Text);
            DocumentSubCategoryCtrl1.EditSubCategory(subCategoryId);
        }

        protected void btnDelete_Click(object sender, DirectEventArgs e)
        {
            Guid subCategoryId = new Guid(hiddenValue.Text);

            Status status = DocumentManager.DeleteDocSubCategories(subCategoryId);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Record deleted successfully.");
                BindDocSubCategories();
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        protected void btnReloadCategoryGrid_Click(object sender, DirectEventArgs e)
        {
            BindDocCategoryGrid();
        }

        protected void btnReloadSubCategoryGrid_Click(object sender, DirectEventArgs e)
        {
            BindDocSubCategories();
        }

    }
}