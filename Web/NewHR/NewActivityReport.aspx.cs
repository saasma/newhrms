﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Entity;
using BLL;
using System.IO;

namespace Web.NewHR
{
    public partial class NewActivityReport : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.HumanResource;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        private void Initialise()
        {
            BindClientCombo();
            BindSoftwareCombo();

            LoadGrids();

            cmbActivityType.SetValue("0");
            //cmbClient.SetValue("0");
        }

        private void LoadGrids()
        {
            BindClientVisitGrid();
            BindSoftwareTestingGrid();
            BindDocumentationGrid();
            BindRemoteSupportGrid();
            BindOtherGrid();
        }

        protected void btnLoad_Click(object sender, DirectEventArgs e)
        {
            LoadGrids();

            int activityTypeCombo = int.Parse(cmbActivityType.SelectedItem.Value);
            if (activityTypeCombo == 0)
            {
                gridClientVisit.Show();
                gridSoftwareTesting.Show();
                gridDocumentation.Show();
                gridRemoteSupport.Show();
                gridOther.Show();
            }
            else if (activityTypeCombo == 1)
            {
                gridClientVisit.Show();
                gridSoftwareTesting.Hide();
                gridDocumentation.Hide();
                gridRemoteSupport.Hide();
                gridOther.Hide();
            }
            else if (activityTypeCombo == 2)
            {
                gridClientVisit.Hide();
                gridSoftwareTesting.Show();
                gridDocumentation.Hide();
                gridRemoteSupport.Hide();
                gridOther.Hide();
            }
            else if (activityTypeCombo == 3)
            {
                gridClientVisit.Hide();
                gridSoftwareTesting.Hide();
                gridDocumentation.Show();
                gridRemoteSupport.Hide();
                gridOther.Hide();
            }
            else if (activityTypeCombo == 4)
            {
                gridClientVisit.Hide();
                gridSoftwareTesting.Hide();
                gridDocumentation.Hide();
                gridRemoteSupport.Show();
                gridOther.Hide();
            }
            else if (activityTypeCombo == 5)
            {
                gridClientVisit.Hide();
                gridSoftwareTesting.Hide();
                gridDocumentation.Hide();
                gridRemoteSupport.Hide();
                gridOther.Show();
            }

        }

        private List<GetNewActivityListResult> NewActivitylList(int activityType)
        {
            DateTime? startDate = null;
            DateTime? endDate = null;
            string employeeName = null;
            int clientSoftwareId = 0;
            int activityTypeCombo = 0;

            if (!string.IsNullOrEmpty(txtStartDate.Text.Trim()))
                startDate = BLL.BaseBiz.GetEngDate(txtStartDate.Text.Trim(), IsEnglish);

            if (!string.IsNullOrEmpty(txtEndDate.Text.Trim()))
                endDate = BLL.BaseBiz.GetEngDate(txtEndDate.Text.Trim(), IsEnglish);

            if (!string.IsNullOrEmpty(txtEmployeeName.Text.Trim()))
                employeeName = txtEmployeeName.Text.Trim();

            if(!string.IsNullOrEmpty(cmbActivityType.SelectedItem.Value))
                activityTypeCombo = int.Parse(cmbActivityType.SelectedItem.Value);

            if (activityTypeCombo == 0 || activityTypeCombo == 5)
                clientSoftwareId = 0;
            else if (activityTypeCombo == 1 || activityTypeCombo == 4)
                clientSoftwareId = int.Parse(cmbClient.SelectedItem.Value);
            else if (activityTypeCombo == 2 || activityTypeCombo == 3)
                clientSoftwareId = int.Parse(cmbSoftware.SelectedItem.Value);


            List<GetNewActivityListResult> list = NewHRManager.GetNewActivityListRes(startDate, endDate, employeeName, activityType, clientSoftwareId);
            return list;
            
        }

        private void BindClientCombo()
        {
            List<ClientList> list = ListManager.GetAllClients();
            ClientList obj = new ClientList() {ClientId = 0, ClientName = "All" };
            list.Insert(0, obj);
            cmbClient.Store[0].DataSource = list;
            cmbClient.Store[0].DataBind();
        }

        private void BindSoftwareCombo()
        {
            List<SoftwareList> list = ListManager.GetAllSoftware();
            SoftwareList obj = new SoftwareList() {SoftwareId = 0, SoftwareName = "All" };
            list.Insert(0, obj);
            cmbSoftware.Store[0].DataSource = list;
            cmbSoftware.Store[0].DataBind();
        }

        private void BindClientVisitGrid()
        {
            gridClientVisit.GetStore().DataSource = NewActivitylList((int)ActivityTypeEnum.ClientVisit);
            gridClientVisit.GetStore().DataBind();
        }

        private void BindSoftwareTestingGrid()
        {
            gridSoftwareTesting.GetStore().DataSource = NewActivitylList((int)ActivityTypeEnum.SoftwareTesting);
            gridSoftwareTesting.GetStore().DataBind();            
        }

        private void BindDocumentationGrid()
        {
            gridDocumentation.GetStore().DataSource = NewActivitylList((int)ActivityTypeEnum.Documentation);
            gridDocumentation.GetStore().DataBind();
        }

        private void BindRemoteSupportGrid()
        {
            gridRemoteSupport.GetStore().DataSource = NewActivitylList((int)ActivityTypeEnum.RemoteSupport);
            gridRemoteSupport.GetStore().DataBind();
        }

        private void BindOtherGrid()
        {
            gridOther.GetStore().DataSource = NewActivitylList((int)ActivityTypeEnum.Other);
            gridOther.GetStore().DataBind();
        }

        protected void btnClientNameSelect_Click(object sender, DirectEventArgs e)
        {
            int activityType = int.Parse(cmbActivityType.SelectedItem.Value.Trim());
            if (activityType == 0)
            {
                cmbClient.Show();
                cmbClient.Disabled = true;
                cmbSoftware.Hide();
                cmbSoftware.Disabled = true;
                cmbClient.ClearValue();
                cmbClient.SetValue("0");
                
            }
            else if (activityType == 1 || activityType == 4)
            {
                cmbClient.Show();
                cmbClient.Disabled = false;
                cmbSoftware.Hide();
                cmbSoftware.Disabled = true;
                cmbSoftware.ClearValue();
                cmbClient.SetValue("0");
            }
            else if (activityType == 2 || activityType == 3)
            {
                cmbClient.Hide();
                cmbClient.Disabled = true;
                cmbSoftware.Show();
                cmbSoftware.Disabled = false;
                cmbSoftware.ClearValue();
                cmbSoftware.SetValue("0");
            }
            else
            {
                cmbClient.Hide();
                cmbClient.Disabled = true;
                cmbSoftware.Hide();
                cmbSoftware.Disabled = true;
            }
        }

        protected void btnDownload_Click(object sender, DirectEventArgs e)
        {
            int activityId = int.Parse(hdnActivityId.Text);

            NewActivity obj = NewHRManager.GetNewActivityById(activityId);


            string path = Context.Server.MapPath(obj.FileLocation + obj.ServerFileName);
            string contentType = obj.FileType;
            string name = obj.UserFileName + "." + obj.FileFormat;

            byte[] bytes = File.ReadAllBytes(path);

            Context.Response.Clear();
            Context.Response.ClearHeaders();
            Context.Response.AppendHeader("Content-Disposition", "attachment; filename=\"" + name + "\"");
            Context.Response.ContentType = contentType;
            Context.Response.BinaryWrite(bytes);
            Context.Response.End();
        }

        protected void btnMarkRead_Click(object sender, DirectEventArgs e)
        {
            int activityId = int.Parse(hdnActivityId.Text);
            int activityType = int.Parse(hdnAcitivityType.Text);

            Status status = NewHRManager.MarkNewActivityAsApproved(activityId);
            if (status.IsSuccess)
            {
                if (activityType == (int)ActivityTypeEnum.ClientVisit)
                    BindClientVisitGrid();
                else if (activityType == (int)ActivityTypeEnum.SoftwareTesting)
                    BindSoftwareTestingGrid();
                else if (activityType == (int)ActivityTypeEnum.Documentation)
                    BindDocumentationGrid();
                else if (activityType == (int)ActivityTypeEnum.RemoteSupport)
                    BindRemoteSupportGrid();
                else
                    BindOtherGrid();

                NewMessage.ShowNormalMessage("Record marked as read successfully.");
            }
            else
                NewMessage.ShowWarningMessage(status.ErrorMessage);

        }

        protected void btnSaveAndSend_Click(object sender, DirectEventArgs e)
        {

            List<GetNewActivityListResult> listResult = new List<GetNewActivityListResult>();

            string gridItemsJsonCV = e.ExtraParams["gridItemsCV"];
            List<GetNewActivityListResult> listCV = JSON.Deserialize<List<GetNewActivityListResult>>(gridItemsJsonCV);
            listResult.AddRange(listCV);

            string gridItemsJsonST = e.ExtraParams["gridItemsST"];
            List<GetNewActivityListResult> listST = JSON.Deserialize<List<GetNewActivityListResult>>(gridItemsJsonST);
            listResult.AddRange(listST);

            string gridItemsJsonDoc = e.ExtraParams["gridItemsDoc"];
            List<GetNewActivityListResult> listDoc = JSON.Deserialize<List<GetNewActivityListResult>>(gridItemsJsonDoc);
            listResult.AddRange(listDoc);

            string gridItemsJsonRS = e.ExtraParams["gridItemsRS"];
            List<GetNewActivityListResult> listRS = JSON.Deserialize<List<GetNewActivityListResult>>(gridItemsJsonRS);
            listResult.AddRange(listRS);

            string gridItemsJsonOthers = e.ExtraParams["gridItemsOther"];
            List<GetNewActivityListResult> listOthers = JSON.Deserialize<List<GetNewActivityListResult>>(gridItemsJsonOthers);
            listResult.AddRange(listOthers);

            if (listResult.Count == 0)
            {
                NewMessage.ShowWarningMessage("Select the activities to save and send.");
                return;
            }

            Status status = NewHRManager.SaveAndSendNewActivities(listResult);
            if (status.IsSuccess)
            {
                CheckboxSelectionModel1.ClearSelection();
                CheckboxSelectionModel2.ClearSelection();
                CheckboxSelectionModel3.ClearSelection();
                CheckboxSelectionModel4.ClearSelection();
                CheckboxSelectionModel5.ClearSelection();               

                NewMessage.ShowNormalMessage("Activities Save and Send saved successfully.");

                LoadGrids();
            }
            else
                NewMessage.ShowWarningMessage(status.ErrorMessage);
        }

        protected void btnMarkAllAsRead_Click(object sender, DirectEventArgs e)
        {
            List<GetNewActivityListResult> listResult = new List<GetNewActivityListResult>();

            string gridItemsJsonCV = e.ExtraParams["gridItemsCV"];
            List<GetNewActivityListResult> listCV = JSON.Deserialize<List<GetNewActivityListResult>>(gridItemsJsonCV);
            listResult.AddRange(listCV);

            string gridItemsJsonST = e.ExtraParams["gridItemsST"];
            List<GetNewActivityListResult> listST = JSON.Deserialize<List<GetNewActivityListResult>>(gridItemsJsonST);
            listResult.AddRange(listST);

            string gridItemsJsonDoc = e.ExtraParams["gridItemsDoc"];
            List<GetNewActivityListResult> listDoc = JSON.Deserialize<List<GetNewActivityListResult>>(gridItemsJsonDoc);
            listResult.AddRange(listDoc);

            string gridItemsJsonRS = e.ExtraParams["gridItemsRS"];
            List<GetNewActivityListResult> listRS = JSON.Deserialize<List<GetNewActivityListResult>>(gridItemsJsonRS);
            listResult.AddRange(listRS);

            string gridItemsJsonOthers = e.ExtraParams["gridItemsOther"];
            List<GetNewActivityListResult> listOthers = JSON.Deserialize<List<GetNewActivityListResult>>(gridItemsJsonOthers);
            listResult.AddRange(listOthers);

            if (listResult.Count == 0)
            {
                NewMessage.ShowWarningMessage("Select the activities to Mark as Read.");
                return;
            }

            Status status = NewHRManager.SaveNewActivitiesAsApproved(listResult);
            if (status.IsSuccess)
            {
                CheckboxSelectionModel1.ClearSelection();
                CheckboxSelectionModel2.ClearSelection();
                CheckboxSelectionModel3.ClearSelection();
                CheckboxSelectionModel4.ClearSelection();
                CheckboxSelectionModel5.ClearSelection();

                NewMessage.ShowNormalMessage("Activities Marked as Read saved successfully.");

                LoadGrids();
            }
            else
                NewMessage.ShowWarningMessage(status.ErrorMessage);

        }

    }
}