﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using BLL;
using DAL;
using Ext.Net;
using Utils.Web;
using Utils.Helper;
using System.Text;
using Newtonsoft.Json.Linq;
using BLL.BO;

namespace Web.NewHR
{
    public partial class ChildrenAllowances : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack && !X.IsAjaxRequest)
                Initialise();
        }

        private void Initialise()
        {
            storeAllowance.DataSource = AllowanceManager.GetChildAllowanceIncomeList();
            storeAllowance.DataBind();
            storeLoanRepayment.Reload();
        }

        protected void btnLoad_Click(object sender, DirectEventArgs e)
        {
            storeLoanRepayment.Reload();
        }
        protected void btnAssignAllowance_Click(object sender, DirectEventArgs e)
        {
            ChildrenAllowanceCtl1.ClearFields();
            this.windowAllowance.Show();
        }

        protected void btnEdit_Click(object sender, DirectEventArgs e)
        {
            if (!string.IsNullOrEmpty(hdn.Text.Trim()))
            {
                windowAllowance.Show();
                ChildrenAllowanceCtl1.LoadData(int.Parse(hdn.Text.Trim()));
            }
        }

        protected void btnDelete_Click(object sender, DirectEventArgs e)
        {
            int AllowanceID = int.Parse(hdn.Text.Trim());
            Status status = AllowanceManager.DeleteChildrenAllowance(AllowanceID);
            if (status.IsSuccess)
            {
                X.Js.AddScript("searchList();");
                NewMessage.ShowNormalMessage("Children Allowance deleted successfully.");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }
        protected void btnTabChange_Click(object sender, DirectEventArgs e)
        {
            storeLoanRepayment.Reload();
        }

        public void btnExport_Click(object sender, EventArgs e)
        {
            int EmployeeID = -1;
            int AllownaceID = -1;
            DateTime? StartDate = null;
            DateTime? EndDate = null;

            if (!string.IsNullOrEmpty(cmbSearch.SelectedItem.Value))
                EmployeeID = int.Parse(cmbSearch.SelectedItem.Value);

            PayrollPeriod _PayrollPeriod = CommonManager.GetValidLastPayrollPeriod();
            if (_PayrollPeriod != null)
            {
                StartDate = _PayrollPeriod.StartDateEng;
                EndDate = _PayrollPeriod.EndDateEng;
            }

            if (!string.IsNullOrEmpty(cmbSearch.SelectedItem.Value))
                EmployeeID = int.Parse(cmbSearch.SelectedItem.Value);

            if (!string.IsNullOrEmpty(cmbAllowance.SelectedItem.Value))
                AllownaceID = int.Parse(cmbAllowance.SelectedItem.Value);

            List<GetChildrenAllowanceResult> list = AllowanceManager.GetChildAllowanceList(0, int.MaxValue, EmployeeID, null, null, AllownaceID);

            if (hdnTabStatus.Text == "active allowances")
            {
                list = list = list.Where(x =>
                        (x.StartDate >= StartDate && x.StartDate <= EndDate) ||
                        (x.EndDate >= StartDate && x.EndDate <= EndDate) ||
                        (StartDate >= x.StartDate && StartDate <= x.EndDate) ||
                        (EndDate >= x.StartDate && EndDate <= x.EndDate)
                        ).ToList();
            }

            else if (hdnTabStatus.Text == "started this month")
               list = list.Where(x => x.StartDate >= StartDate && x.StartDate <= EndDate).ToList();
            

            else if (hdnTabStatus.Text == "ending this month")
                list = list.Where(x => x.EndDate >= StartDate && x.EndDate <= EndDate).ToList();
            

            else if (hdnTabStatus.Text == "inactive allowances")
                list = list.Where(a => a.EndDate < StartDate || a.StartDate > EndDate).ToList();

            Bll.ExcelHelper.ExportToExcel("Children Allowance", list, new List<String> { "TotalRows", "RowNumber", "ChildrenAllowanceID", "EndDate", "StartDate", "Dob" },
           new List<String>() { },
            new Dictionary<string, string>() { { "StartDateEngText", "Start Date" }, { "EndDateEngText", "End Date" }, { "DOBEngText", "DOB" }, { "EmployeeName", "Employee Name" }, { "EmployeeId", "EIN" }, { "ChildrenName", "Children Name" }, { "StartDate", "StartDate" }, { "EnDate", "End Date" } },
           new List<string>() { }
           ,
           new List<string>() { "Amount"},
           new List<string>() { "StartDateEngText", "EndDateEngText", "DOBEngText"}
           , new Dictionary<string, string>() {
              {"",""}
            }
            , new List<string> { "EmployeeId", "EmployeeName", "ChildrenName", "Relation", "Income", "DOBEngText", "StartDateEngText", "EndDateEngText" });



        }


        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            int EmployeeID = -1;
            DateTime? StartDate = null;
            DateTime? EndDate = null;
            int AllownaceID = -1;

            PayrollPeriod _PayrollPeriod = CommonManager.GetValidLastPayrollPeriod();

            if (_PayrollPeriod != null)
            {
                StartDate = _PayrollPeriod.StartDateEng;
                EndDate = _PayrollPeriod.EndDateEng;
            }

            if (!string.IsNullOrEmpty(cmbSearch.SelectedItem.Value))
                EmployeeID = int.Parse(cmbSearch.SelectedItem.Value);

            if (!string.IsNullOrEmpty(cmbAllowance.SelectedItem.Value))
                AllownaceID = int.Parse(cmbAllowance.SelectedItem.Value);


            List<GetChildrenAllowanceResult> list = AllowanceManager.GetChildAllowanceList(0, int.MaxValue, EmployeeID, null, null, AllownaceID);


            if (hdnTabStatus.Text == "active allowances")
            {
                list = list.Where(x => 
                        (x.StartDate >= StartDate && x.StartDate <= EndDate) ||
                        (x.EndDate >= StartDate && x.EndDate <= EndDate) ||
                        (StartDate >= x.StartDate && StartDate <= x.EndDate) ||
                        (EndDate >= x.StartDate && EndDate <= x.EndDate)
                        ).ToList();
            }

            else if (hdnTabStatus.Text == "started this month")
                list = list.Where(x => x.StartDate >= StartDate && x.StartDate <= EndDate).ToList();
            

            else if (hdnTabStatus.Text == "ending this month")
                            list = list.Where(x => x.EndDate >= StartDate && x.EndDate <= EndDate).ToList();

            else if (hdnTabStatus.Text == "inactive allowances")
            {

                //List<int> ListCurrentPayollPeriodAllowanceID = new List<int>();
                //List<GetChildrenAllowanceResult> listActive = list.Where(x => x.EndDate >= StartDate && x.EndDate <= EndDate).ToList();

                //foreach (GetChildrenAllowanceResult _item in listActive)
                //{
                //    ListCurrentPayollPeriodAllowanceID.Add(_item.ChildrenAllowanceID);
                //}

                list = list.Where(a => a.EndDate < StartDate || a.StartDate > EndDate).ToList();
            }

            if (list.Any())
                e.Total = list.Count();
            else
                e.Total = 0;

            list = list.Skip(e.Start * int.Parse(cmbPageSize.SelectedItem.Value)).Take(int.Parse(cmbPageSize.SelectedItem.Value)).ToList();


            storeLoanRepayment.DataSource = list;
            storeLoanRepayment.DataBind();
        }
    }
}