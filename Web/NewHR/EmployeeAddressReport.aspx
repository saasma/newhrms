﻿<%@ Page Title="Employee Address Report" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="EmployeeAddressReport.aspx.cs" Inherits="Web.NewHR.EmployeeAddressReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .x-tab-bar-default {
            background-color: inherit;
        }

        .hideLeftBlockCssInPage {
            margin: 0px !important;
            padding-left: 20px !important;
        }

        #menu {
            display: none;
        }

        #content {
            margin: inherit;
        }
    </style>
    <script type="text/javascript">
        function searchList() {
            <%=gridEmp.ClientID %>.getStore().pageSize = <%=cmbPageSize.ClientID %>.getValue();
            <%=PagingToolbar1.ClientID %>.doRefresh();
        }

        function PassQueryString(btn) {
            var EmpID = <%=cmbSearch.ClientID %>.getValue();
            var BranchID=<%=cmbBranch .ClientID %>.getValue();
            var LevelID=<%=cmbLevel.ClientID %>.getValue();
            if(EmpID==null)
                EmpID="";
            if(BranchID==null)
                BranchID="";
            if(LevelID==null)
                LevelID="";
            var ret = EmpPopup("EmpID=" +EmpID+"&BranchID="+BranchID+"&LevelID="+LevelID);
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>Employee Address List
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel" style="margin-top: 0px !important; padding-top: 0px !important;">
        <div class="separator bottom">
        </div>
        <div class="innerLR">
            <ext:Store ID="storeBranch" runat="server">
                <Model>
                    <ext:Model ID="Model1" runat="server" IDProperty="BranchId">
                        <Fields>
                            <ext:ModelField Name="BranchId" Type="String" />
                            <ext:ModelField Name="Name" Type="String" />
                        </Fields>
                    </ext:Model>
                </Model>
            </ext:Store>
            <ext:Store ID="storeLevel" runat="server">
                <Model>
                    <ext:Model ID="Model2" runat="server" IDProperty="LevelId">
                        <Fields>
                            <ext:ModelField Name="LevelId" Type="String" />
                            <ext:ModelField Name="Name" Type="String" />
                        </Fields>
                    </ext:Model>
                </Model>
            </ext:Store>
            <table style="margin-top: 10px;">
                <tr>
                    <td style="width: 160px;">
                        <ext:ComboBox ID="cmbBranch" runat="server" ValueField="BranchId" DisplayField="Name" EmptyText="Branch Filter"
                            FieldLabel="" Width="150" LabelSeparator="" ForceSelection="true"
                            QueryMode="Local" StoreID="storeBranch">
                            <Triggers>
                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                            </Triggers>
                            <Listeners>
                                <Select Handler="this.getTrigger(0).show();" />
                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                <TriggerClick Handler="if (index == 0) { 
                                    this.clearValue(); 
                                    this.getTrigger(0).hide();
                                }" />
                            </Listeners>
                        </ext:ComboBox>
                    </td>
                    <td style="width: 160px;">
                        <ext:ComboBox ID="cmbLevel" runat="server" ValueField="LevelId" DisplayField="Name" EmptyText="Level Filter"
                            FieldLabel="" Width="150" LabelSeparator="" ForceSelection="true"
                            QueryMode="Local" StoreID="storeLevel">
                            <Triggers>
                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                            </Triggers>
                            <Listeners>
                                <Select Handler="this.getTrigger(0).show();" />
                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                <TriggerClick Handler="if (index == 0) { 
                                    this.clearValue(); 
                                    this.getTrigger(0).hide();
                                }" />
                            </Listeners>
                        </ext:ComboBox>
                    </td>
                    <td style="width: 210px;">
                        <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                            <Proxy>
                                <ext:AjaxProxy Url="../Handler/EmpSearchID.ashx">
                                    <ActionMethods Read="GET" />
                                    <Reader>
                                        <ext:JsonReader Root="plants" TotalProperty="total" />
                                    </Reader>
                                </ext:AjaxProxy>
                            </Proxy>
                            <Model>
                                <ext:Model ID="Model3" IDProperty="Name" Name="ItemLineModel" runat="server">
                                    <Fields>
                                        <ext:ModelField Name="Name" Type="String" />
                                        <ext:ModelField Name="EmployeeId" Type="String" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                        <ext:ComboBox LabelSeparator="" ID="cmbSearch" runat="server" DisplayField="Name"
                            ValueField="EmployeeId" EmptyText="Search Employee"
                            StoreID="storeSearch" TypeAhead="false" Width="200" HideBaseTrigger="true" MinChars="1"
                            TriggerAction="All" ForceSelection="false">
                            <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                                <ItemTpl ID="ItemTpl1" runat="server">
                                    <Html>
                                        <tpl>
                                            <div class="search-item">
                                                            <span>{Name}</span>  
                                             </div>
					                </tpl>
                                    </Html>
                                </ItemTpl>
                            </ListConfig>
                            <Triggers>
                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                            </Triggers>
                            <Listeners>
                                <Select Handler="this.getTrigger(0).show();" />
                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                            </Listeners>
                        </ext:ComboBox>
                    </td>
                    <td>
                        <ext:Button ID="Button1" runat="server" Text="Load">
                            <Listeners>
                                <Click Fn="searchList">
                                </Click>
                            </Listeners>
                        </ext:Button>
                    </td>
                    <td>
                        <ext:Button ID="Button2" runat="server" Text="Export" AutoPostBack="true" OnClick="btnExport_Click" MarginSpec="0 0 0 10">
                        </ext:Button>
                    </td>
                    <td>
                        <ext:Button ID="btnImportExcel" runat="server" OnClientClick="return PassQueryString(this)" Text="<i></i>Import from Excel" MarginSpec="0 0 0 10">
                        </ext:Button>
                    </td>

                </tr>


            </table>
        </div>
        <div style="clear: both; padding-top: 30px" runat="server" id="gridContainer">
        </div>
        <ext:GridPanel ID="gridEmp" Border="true" Header="false" runat="server" Title="Title" Cls="gridtbl">
            <Store>
                <ext:Store runat="server" ID="gridEmpStore" AutoLoad="true" OnReadData="Store_ReadData" RemotePaging="true" RemoteSort="true">
                    <Proxy>
                        <ext:PageProxy />
                    </Proxy>
                    <Model>
                        <ext:Model ID="gridEmpModel" Name="gridEmpStoreModel" runat="server">
                            <Fields>
                                <ext:ModelField Name="EMPLOYEEID" Type="Int" />
                                <ext:ModelField Name="NAME" Type="String" />
                                <ext:ModelField Name="PECOUNTRYID" Type="String" />
                                <ext:ModelField Name="PEZONEID" Type="String" />
                                <ext:ModelField Name="PEDISTRICTID" Type="String" />
                                <ext:ModelField Name="PEVDCMUNCIPALITY" Type="String" />
                                <ext:ModelField Name="PESTREET" Type="String" />
                                <ext:ModelField Name="PEWARDNO" Type="String" />
                                <ext:ModelField Name="PEHOUSENO" Type="String" />
                                <ext:ModelField Name="PELOCALITY" Type="String" />
                                <ext:ModelField Name="PESTATE" Type="String" />
                                <ext:ModelField Name="PEZIPCODE" Type="String" />

                                <ext:ModelField Name="PSCOUNTRYID" Type="String" />
                                <ext:ModelField Name="PSZONEID" Type="String" />
                                <ext:ModelField Name="PSDISTRICTID" Type="String" />
                                <ext:ModelField Name="PSVDCMUNCIPALITY" Type="String" />
                                <ext:ModelField Name="PSSTREET" Type="String" />
                                <ext:ModelField Name="PSWARDNO" Type="String" />
                                <ext:ModelField Name="PSHOUSENO" Type="String" />
                                <ext:ModelField Name="PSLOCALITY" Type="String" />
                                <ext:ModelField Name="PSSTATE" Type="String" />
                                <ext:ModelField Name="PSZIPCODE" Type="String" />
                                <ext:ModelField Name="PSCITISSDIS" Type="String" />

                                <ext:ModelField Name="CIEMAIL" Type="String" />
                                <ext:ModelField Name="CIPHONENO" Type="String" />
                                <ext:ModelField Name="EXTENSION" Type="String" />
                                <ext:ModelField Name="CIMOBILENO" Type="String" />

                                <ext:ModelField Name="PERSONALEMAIL" Type="String" />
                                <ext:ModelField Name="PERSONALPHONE" Type="String" />
                                <ext:ModelField Name="PERSONALMOBILE" Type="String" />

                                <ext:ModelField Name="EMERGENCYNAME" Type="String" />
                                <ext:ModelField Name="EMERGENCYRELATION" Type="String" />
                                <ext:ModelField Name="EMERGENCYPHONE" Type="String" />
                                <ext:ModelField Name="EMERGENCYMOBILE" Type="String" />



                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>
            <ColumnModel runat="server" ID="ColumnModel2">
                <Columns>

                    <ext:Column runat="server" Sortable="false" MenuDisabled="true" Header="EIN" Width="50" ColumnID="ColEmployeeId"
                        Locked="true" DataIndex="EMPLOYEEID">
                    </ext:Column>
                    <ext:Column runat="server" Sortable="false" MenuDisabled="true" Header="Name" Width="150" ColumnID="ColName"
                        Locked="true" DataIndex="NAME">
                    </ext:Column>

                    <ext:Column runat="server" Text="Permanent Address">
                        <Columns>
                            <ext:Column runat="server" Sortable="false" MenuDisabled="true" Header="Country" Width="80" ColumnID="ColpeCountry"
                                Locked="true" DataIndex="PECOUNTRYID">
                            </ext:Column>
                            <ext:Column runat="server" Sortable="false" MenuDisabled="true" Header="Zone" Width="80" ColumnID="ColpeZoneid"
                                Locked="true" DataIndex="PEZONEID">
                            </ext:Column>
                            <ext:Column runat="server" Sortable="false" MenuDisabled="true" Header="District" Width="80" ColumnID="ColpeDisid"
                                Locked="true" DataIndex="PEDISTRICTID">
                            </ext:Column>
                            <ext:Column runat="server" Sortable="false" MenuDisabled="true" Header="VDC/Municipality" Width="130" ColumnID="Colpevdc"
                                Locked="true" DataIndex="PEVDCMUNCIPALITY">
                            </ext:Column>
                            <ext:Column runat="server" Sortable="false" MenuDisabled="true" Header="Street/Colony" Width="100" ColumnID="ColpeSteet"
                                Locked="true" DataIndex="PESTREET">
                            </ext:Column>
                            <ext:Column runat="server" Sortable="false" MenuDisabled="true" Header="Ward No." Width="80" ColumnID="ColpewardNo"
                                Locked="true" DataIndex="PEWARDNO">
                            </ext:Column>
                            <ext:Column runat="server" Sortable="false" MenuDisabled="true" Header="House No." Width="100" ColumnID="Colpehouseno"
                                Locked="true" DataIndex="PEHOUSENO">
                            </ext:Column>
                            <ext:Column runat="server" Sortable="false" MenuDisabled="true" Header="Locality" Width="100" ColumnID="Colpelocality"
                                Locked="true" DataIndex="PELOCALITY">
                            </ext:Column>
                            <ext:Column runat="server" Sortable="false" MenuDisabled="true" Header="State" Width="100" ColumnID="Colpestate"
                                Locked="true" DataIndex="PESTATE">
                            </ext:Column>
                            <ext:Column runat="server" Sortable="false" MenuDisabled="true" Header="Zip Code" Width="100" ColumnID="Colpezipcode"
                                Locked="true" DataIndex="PEZIPCODE">
                            </ext:Column>
                        </Columns>
                    </ext:Column>

                    <ext:Column runat="server" Text="Present Address">
                        <Columns>
                            <ext:Column runat="server" Sortable="false" MenuDisabled="true" Header="Country" Width="80" ColumnID="ColpsCountry"
                                Locked="true" DataIndex="PSCOUNTRYID">
                            </ext:Column>
                            <ext:Column runat="server" Sortable="false" MenuDisabled="true" Header="Zone" Width="80" ColumnID="ColpsZoneid"
                                Locked="true" DataIndex="PSZONEID">
                            </ext:Column>
                            <ext:Column runat="server" Sortable="false" MenuDisabled="true" Header="District" Width="80" ColumnID="ColpsDisid"
                                Locked="true" DataIndex="PSDISTRICTID">
                            </ext:Column>
                            <ext:Column runat="server" Sortable="false" MenuDisabled="true" Header="VDC/Municipality" Width="130" ColumnID="Colpsvdc"
                                Locked="true" DataIndex="PSVDCMUNCIPALITY">
                            </ext:Column>
                            <ext:Column runat="server" Sortable="false" MenuDisabled="true" Header="Street/Colony" Width="100" ColumnID="ColpsSteet"
                                Locked="true" DataIndex="PSSTREET">
                            </ext:Column>
                            <ext:Column runat="server" Sortable="false" MenuDisabled="true" Header="Ward No." Width="80" ColumnID="ColpswardNo"
                                Locked="true" DataIndex="PSWARDNO">
                            </ext:Column>
                            <ext:Column runat="server" Sortable="false" MenuDisabled="true" Header="House No." Width="100" ColumnID="Colpshouseno"
                                Locked="true" DataIndex="PSHOUSENO">
                            </ext:Column>
                            <ext:Column runat="server" Sortable="false" MenuDisabled="true" Header="Locality" Width="100" ColumnID="Colpslocality"
                                Locked="true" DataIndex="PSLOCALITY">
                            </ext:Column>
                            <ext:Column runat="server" Sortable="false" MenuDisabled="true" Header="State" Width="100" ColumnID="Colpsstate"
                                Locked="true" DataIndex="PSSTATE">
                            </ext:Column>
                            <ext:Column runat="server" Sortable="false" MenuDisabled="true" Header="Zip Code" Width="100" ColumnID="Colpszipcode"
                                Locked="true" DataIndex="PSZIPCODE">
                            </ext:Column>
                            <ext:Column runat="server" Sortable="false" MenuDisabled="true" Header="Cit. issue address" Width="150" ColumnID="ColpscitIsDis"
                                Locked="true" DataIndex="PSCITISSDIS">
                            </ext:Column>
                        </Columns>
                    </ext:Column>

                    <ext:Column runat="server" Text="Official Contact">
                        <Columns>
                            <ext:Column runat="server" Sortable="false" MenuDisabled="true" Header="Email" Width="120" ColumnID="Colciemail"
                                Locked="true" DataIndex="CIEMAIL">
                            </ext:Column>
                            <ext:Column runat="server" Sortable="false" MenuDisabled="true" Header="Phone" Width="100" ColumnID="ColciPhone"
                                Locked="true" DataIndex="CIPHONENO">
                            </ext:Column>
                            <ext:Column runat="server" Sortable="false" MenuDisabled="true" Header="Extension" Width="80" ColumnID="Colextension"
                                Locked="true" DataIndex="EXTENSION">
                            </ext:Column>
                            <ext:Column runat="server" Sortable="false" MenuDisabled="true" Header="Mobile" Width="100" ColumnID="ColciMobileNo"
                                Locked="true" DataIndex="CIMOBILENO">
                            </ext:Column>
                        </Columns>
                    </ext:Column>

                    <ext:Column runat="server" Text="Personal Contact">
                        <Columns>
                            <ext:Column runat="server" Sortable="false" MenuDisabled="true" Header="Email" Width="120" ColumnID="ColperEmail"
                                Locked="true" DataIndex="PERSONALEMAIL">
                            </ext:Column>
                            <ext:Column runat="server" Sortable="false" MenuDisabled="true" Header="Phone" Width="80" ColumnID="ColperPhone"
                                Locked="true" DataIndex="PERSONALPHONE">
                            </ext:Column>
                            <ext:Column runat="server" Sortable="false" MenuDisabled="true" Header="Mobile" Width="100" ColumnID="ColperMobile"
                                Locked="true" DataIndex="PERSONALMOBILE">
                            </ext:Column>
                        </Columns>
                    </ext:Column>

                    <ext:Column runat="server" Text="Emergency Contact">
                        <Columns>
                            <ext:Column runat="server" Sortable="false" MenuDisabled="true" Header="Name" Width="120" ColumnID="ColemerName"
                                Locked="true" DataIndex="EMERGENCYNAME">
                            </ext:Column>
                            <ext:Column runat="server" Sortable="false" MenuDisabled="true" Header="Relation" Width="120" ColumnID="ColemerRelation"
                                Locked="true" DataIndex="EMERGENCYRELATION">
                            </ext:Column>
                            <ext:Column runat="server" Sortable="false" MenuDisabled="true" Header="Phone" Width="80" ColumnID="ColemerPhone"
                                Locked="true" DataIndex="EMERGENCYPHONE">
                            </ext:Column>
                            <ext:Column runat="server" Sortable="false" MenuDisabled="true" Header="Mobile" Width="100" ColumnID="ColemerMobile"
                                Locked="true" DataIndex="EMERGENCYMOBILE">
                            </ext:Column>
                        </Columns>
                    </ext:Column>


                </Columns>
            </ColumnModel>
            <SelectionModel>
                <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
            </SelectionModel>
            <BottomBar>
                <ext:PagingToolbar Height="40" ID="PagingToolbar1" runat="server" StoreID="gridEmpStore"
                    DisplayInfo="true">
                    <Items>
                        <ext:Label ID="lblPageSize" runat="server" Text="Page size:" />
                        <ext:ToolbarSpacer ID="ToolbarSpacer_listItems" runat="server" Width="10" />
                        <ext:ComboBox ID="cmbPageSize" runat="server" Width="80" SelectOnFocus="true" Selectable="true"
                            ValueField="Value" DisplayField="Text" ForceSelection="true" AllowBlank="false">
                            <Listeners>
                                <Select Handler="searchList()" />
                            </Listeners>
                            <Items>
                                <ext:ListItem Value="25" Text="25" />
                                <ext:ListItem Value="50" Text="50" />
                                <ext:ListItem Value="100" Text="100" />
                                <ext:ListItem Value="9999999" Text="All" />
                            </Items>
                            <SelectedItems>
                                <ext:ListItem Index="0">
                                </ext:ListItem>
                            </SelectedItems>
                        </ext:ComboBox>
                    </Items>
                </ext:PagingToolbar>
            </BottomBar>
        </ext:GridPanel>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
