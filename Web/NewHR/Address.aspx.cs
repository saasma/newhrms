﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Entity;
using BLL;

namespace Web.NewHR
{
    public partial class Address : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.HumanResource;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
                if (NewHRManager.GetAddressByID(int.Parse(Request.QueryString["ID"])) != null)
                {
                    LoadEditData();
                }
            }
        }

        public void Initialise()
        {
            Hidden_CustomerID.Text = Request.QueryString["ID"];

            CommonManager comManager = new CommonManager();

            cmbCountryPerm.Store[0].DataSource = comManager.GetAllCountries();
            cmbCountryPerm.Store[0].DataBind();

            cmbCountryTemp.Store[0].DataSource = comManager.GetAllCountries();
            cmbCountryTemp.Store[0].DataBind();

            cmbZonePerm.Store[0].DataSource = comManager.GetAllZones();
            cmbZonePerm.Store[0].DataBind();

            cmbZoneTemp.Store[0].DataSource = comManager.GetAllZones();
            cmbZoneTemp.Store[0].DataBind();

            if (NewHRManager.GetAddressByID(int.Parse(Request.QueryString["ID"])) != null)
                LoadEditContact();
            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.NIBL)
            txtPhoneOfficial.FieldLabel = "Direct Line";
            
        }

        private void LoadEditContact()
        {
            EAddress eAddress = new EAddress();
            eAddress = NewHRManager.GetAddressByID(int.Parse(Request.QueryString["ID"]));
            if (eAddress != null)
            {
                txtEmailOfficial.Text = eAddress.CIEmail ;
                txtPhoneOfficial.Text = eAddress.CIPhoneNo;
                txtExtentionOfficial.Text= eAddress.Extension;
                txtMobileOfficial.Text= eAddress.CIMobileNo;

                txtEmailPersonal.Text = eAddress.PersonalEmail;
                txtMobilePersonal.Text = eAddress.PersonalMobile;
                txtPhonePersonal.Text = eAddress.PersonalPhone;

                txtRelationEmergency.Text= eAddress.EmergencyRelation;
                txtNameEmergency.Text= eAddress.EmergencyName;
                txtPhoneEmergency.Text= eAddress.EmergencyPhone;
                txtMobileEmergency.Text = eAddress.EmergencyMobile;

                if (eAddress.CreatedOn != null)
                    btnNext.Text = "Update";

            }
        }
        protected void ButtonNext_Click(object sender, DirectEventArgs e)
        {
            EAddress address = new EAddress();
            int EmpID;
            if (cmbCountryPerm.SelectedItem.Index != -1)
            {
                address.PECountryId = int.Parse(cmbCountryPerm.SelectedItem.Value);
            }

            if (cmbZonePerm.SelectedItem.Index != -1)
            {
                address.PEZoneId = int.Parse(cmbZonePerm.SelectedItem.Value);
            }

            if (cmbDistrictPerm.SelectedItem.Index != -1)
            {
                address.PEDistrictId = int.Parse(cmbDistrictPerm.SelectedItem.Value);
            }

            address.PEVDCMuncipality = cmbVDCPerm.Text;
            address.PEStreet = txtStreetColonyPerm.Text;
            address.PEWardNo = txtWardNoPerm.Text;
            address.PEHouseNo = txtHouseNoPerm.Text;
            address.PEState = txtStatePerm.Text.Trim();
            address.PEZipCode = txtZipCodePerm.Text.Trim();
            address.PELocality = txtLocalityPerm.Text.Trim();


            if (cmbCountryTemp.SelectedItem.Index != -1)
            {
                address.PSCountryId = int.Parse(cmbCountryTemp.SelectedItem.Value);
            }
            if (cmbZoneTemp.SelectedItem.Index != -1)
            {
                address.PSZoneId = int.Parse(cmbZoneTemp.SelectedItem.Value);
            }
            if (cmbDistrictTemp.SelectedItem.Index != -1)
            {
                address.PSDistrictId = int.Parse(cmbDistrictTemp.SelectedItem.Value);
            }
            address.PSVDCMuncipality = cmbVDCTemp.Text;
            address.PSStreet = txtStreetColonyTemp.Text;
            address.PSWardNo = txtWardNoTemp.Text;
            address.PSHouseNo = txtHouseNoTemp.Text;
            address.PSState = txtStateTemp.Text.Trim();
            address.PSZipCode = txtZipCodeTemp.Text.Trim();
            address.PSLocality = txtLocalityTemp.Text.Trim();
            address.PSCitIssDis = txtCitIssuseDist.Text.Trim();

            EmpID = int.Parse(Request.QueryString["id"]);

            // Contact section
            address.CIEmail = txtEmailOfficial.Text;
            address.CIPhoneNo = txtPhoneOfficial.Text;
            address.Extension = txtExtentionOfficial.Text;
            address.CIMobileNo = txtMobileOfficial.Text;

            address.PersonalEmail = txtEmailPersonal.Text;
            address.PersonalMobile = txtMobilePersonal.Text;
            address.PersonalPhone = txtPhonePersonal.Text;

            address.EmergencyRelation = txtRelationEmergency.Text;
            address.EmergencyName = txtNameEmergency.Text;
            address.EmergencyPhone = txtPhoneEmergency.Text;
            address.EmergencyMobile = txtMobileEmergency.Text;


            bool isInsert;
            if (string.IsNullOrEmpty(Request.QueryString["isedit"]))
            {
                isInsert = true;
            }
            else
            {
                isInsert = false;
            }

            Status respStatus;
            respStatus = NewHRManager.InsertUpdateAddress(address, EmpID);
            if (respStatus.IsSuccess)
            {
                btnNext.Text = "Update";
                SetMessage(lblMsg, "Address information updated.");
                //Response.Redirect("Address.aspx?id=" + EmpID.ToString());
            }

        }

        protected void CopyButton_Click(object sender, DirectEventArgs e)
        {
            CommonManager comManager = new CommonManager();
            if (cmbCountryPerm.SelectedItem != null && cmbCountryPerm.SelectedItem.Value != null)
                cmbCountryTemp.SetValue(int.Parse( cmbCountryPerm.SelectedItem.Value));
                //cmbCountryTemp.SelectedItem.Value = cmbCountryPerm.SelectedItem.Value;


            if (cmbZonePerm.SelectedItem != null && cmbZonePerm.SelectedItem.Value != null)
            {
                cmbZoneTemp.SetValue(int.Parse( cmbZonePerm.SelectedItem.Value));
                //cmbZoneTemp.SelectedItem.Value = cmbZonePerm.SelectedItem.Value;
                if (cmbDistrictPerm.SelectedItem != null && cmbDistrictPerm.SelectedItem.Value != null)
                {
                    cmbDistrictTemp.Clear();
                    cmbDistrictTemp.Store[0].DataSource = comManager.GetAllDistricts(int.Parse(cmbZonePerm.SelectedItem.Value));
                    cmbDistrictTemp.Store[0].DataBind();
                    //cmbDistrictTemp.SelectedItem.Value = cmbDistrictPerm.SelectedItem.Value;
                    cmbDistrictTemp.SetValue(int.Parse(cmbDistrictPerm.SelectedItem.Value));
                }
            }
           

            cmbVDCTemp.Text = cmbVDCPerm.Text;
            txtStreetColonyTemp.Text = txtStreetColonyPerm.Text;
            txtWardNoTemp.Text = txtWardNoPerm.Text;
            txtHouseNoTemp.Text = txtHouseNoPerm.Text;
            txtStateTemp.Text = txtStatePerm.Text;
            txtZipCodeTemp.Text = txtZipCodePerm.Text;
            txtLocalityTemp.Text = txtLocalityPerm.Text;
        }

        protected void ZoneChangeTemp(object sender, DirectEventArgs e)
        {
            CommonManager comManager = new CommonManager();
            cmbDistrictTemp.Clear();
            cmbDistrictTemp.Store[0].DataSource = comManager.GetAllDistricts(int.Parse(cmbZoneTemp.SelectedItem.Value));
            cmbDistrictTemp.Store[0].DataBind();

        }
        protected void ZoneChangePerm(object sender, DirectEventArgs e)
        {
            CommonManager comManager = new CommonManager();
            cmbDistrictPerm.Clear();
            cmbDistrictPerm.Store[0].DataSource = comManager.GetAllDistricts(int.Parse(cmbZonePerm.SelectedItem.Value));
            cmbDistrictPerm.Store[0].DataBind();
        }



        public void LoadEditData()
        {
            EAddress eAddress = new EAddress();
            eAddress = NewHRManager.GetAddressByID(int.Parse(Request.QueryString["ID"]));
            if (eAddress != null)
            {
                if(eAddress.PECountryId!= null && eAddress.PECountryId != -1)
                ExtControlHelper.ComboBoxSetSelected(eAddress.PECountryId, cmbCountryPerm);
                if (eAddress.PEZoneId != null)
                {
                    ExtControlHelper.ComboBoxSetSelected(eAddress.PEZoneId, cmbZonePerm);

                    CommonManager comManager = new CommonManager();
                    cmbDistrictPerm.Clear();
                    cmbDistrictPerm.Store[0].DataSource = comManager.GetAllDistricts(eAddress.PEZoneId.Value);
                    cmbDistrictPerm.Store[0].DataBind();
                }

                if (eAddress.PEDistrictId != null)
                ExtControlHelper.ComboBoxSetSelected(eAddress.PEDistrictId, cmbDistrictPerm);

                if (eAddress.PSCountryId != null)
                    ExtControlHelper.ComboBoxSetSelected(eAddress.PSCountryId, cmbCountryTemp);
                if (eAddress.PSZoneId != null)
                {
                    ExtControlHelper.ComboBoxSetSelected(eAddress.PSZoneId, cmbZoneTemp);


                    CommonManager comManager = new CommonManager();
                    cmbDistrictTemp.Clear();
                    cmbDistrictTemp.Store[0].DataSource = comManager.GetAllDistricts(eAddress.PSZoneId.Value);
                    cmbDistrictTemp.Store[0].DataBind();
                }
                if (eAddress.PSDistrictId != null && eAddress.PSDistrictId != -1)
                    ExtControlHelper.ComboBoxSetSelected(eAddress.PSDistrictId, cmbDistrictTemp);

                cmbVDCPerm.Text = eAddress.PEVDCMuncipality;
                txtStreetColonyPerm.Text = eAddress.PEStreet;
                txtWardNoPerm.Text = eAddress.PEWardNo;
                txtHouseNoPerm.Text = eAddress.PEHouseNo;
                txtStatePerm.Text = eAddress.PEState;
                txtZipCodePerm.Text = eAddress.PEZipCode;
                txtLocalityPerm.Text = eAddress.PELocality;

                cmbVDCTemp.Text = eAddress.PSVDCMuncipality;
                txtStreetColonyTemp.Text = eAddress.PSStreet;
                txtWardNoTemp.Text = eAddress.PSWardNo;
                txtHouseNoTemp.Text = eAddress.PSHouseNo;
                txtStateTemp.Text = eAddress.PSState;
                txtZipCodeTemp.Text = eAddress.PSZipCode;
                txtLocalityTemp.Text = eAddress.PSLocality;
                txtCitIssuseDist.Text = eAddress.PSCitIssDis;

                if (eAddress.CreatedOn != null)
                    btnNext.Text = "Update";

            }
        }


    }
}