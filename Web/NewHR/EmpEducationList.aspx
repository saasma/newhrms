﻿<%@ Page Title="Employee Education Report" Language="C#" MasterPageFile="~/Master/NewDetails.Master" AutoEventWireup="true" CodeBehind="EmpEducationList.aspx.cs" Inherits="Web.NewHR.EmpEducationList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">
    
        function searchList() {
            <%=gridEducation.ClientID %>.getStore().pageSize = <%=cmbPageSize.ClientID %>.getValue();
            <%=PagingToolbar1.ClientID %>.doRefresh();
        }

        function refreshWindow()
        {
            searchList();
        }


    </script>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">

    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>Employee Education Report
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <ext:Store ID="storeBranch" runat="server">
            <Model>
                <ext:Model ID="Model1" runat="server" IDProperty="BranchId">
                    <Fields>
                        <ext:ModelField Name="BranchId" Type="String" />
                        <ext:ModelField Name="Name" Type="String" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
        <ext:Store ID="storeLevel" runat="server">
            <Model>
                <ext:Model ID="Model2" runat="server" IDProperty="LevelId">
                    <Fields>
                        <ext:ModelField Name="LevelId" Type="String" />
                        <ext:ModelField Name="Name" Type="String" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
        <table>
            <tr>
                <td style="width: 160px;">
                    <ext:ComboBox ID="cmbBranch" runat="server" ValueField="BranchId" DisplayField="Name" EmptyText="Branch Filter"
                        FieldLabel="" Width="150" LabelSeparator="" ForceSelection="true"
                        QueryMode="Local" StoreID="storeBranch">
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                        </Triggers>
                        <Listeners>
                            <Select Handler="this.getTrigger(0).show();" />
                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                            <TriggerClick Handler="if (index == 0) { 
                                    this.clearValue(); 
                                    this.getTrigger(0).hide();
                                }" />
                        </Listeners>
                    </ext:ComboBox>
                </td>
                <td style="width: 160px;">
                    <ext:ComboBox ID="cmbLevel" runat="server" ValueField="LevelId" DisplayField="Name" EmptyText="Level Filter"
                        FieldLabel="" Width="150" LabelSeparator="" ForceSelection="true"
                        QueryMode="Local" StoreID="storeLevel">
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                        </Triggers>
                        <Listeners>
                            <Select Handler="this.getTrigger(0).show();" />
                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                            <TriggerClick Handler="if (index == 0) { 
                                    this.clearValue(); 
                                    this.getTrigger(0).hide();
                                }" />
                        </Listeners>
                    </ext:ComboBox>
                </td>
                <td style="width: 160px;">
                    <ext:ComboBox ID="cmbEducationLevel" runat="server" ValueField="ID" DisplayField="Name" EmptyText="Edu Level Filter"
                        FieldLabel="" Width="150" LabelSeparator="" ForceSelection="true"
                        QueryMode="Local">
                        <Store>
                            <ext:Store ID="Store3" runat="server">
                                <Model>
                                    <ext:Model ID="Model5" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="ID" Type="String" />
                                            <ext:ModelField Name="Name" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                        </Triggers>
                        <Listeners>
                            <Select Handler="this.getTrigger(0).show();" />
                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                            <TriggerClick Handler="if (index == 0) { 
                                    this.clearValue(); 
                                    this.getTrigger(0).hide();
                                }" />
                        </Listeners>
                    </ext:ComboBox>
                </td>
                <td style="width: 160px;">
                    <ext:ComboBox ID="cmbFaculty" runat="server" ValueField="ID" DisplayField="Name" EmptyText="Faculty Filter"
                        FieldLabel="" Width="150" LabelSeparator="" ForceSelection="true"
                        QueryMode="Local">
                        <Store>
                            <ext:Store ID="Store1" runat="server">
                                <Model>
                                    <ext:Model ID="Model6" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="ID" Type="String" />
                                            <ext:ModelField Name="Name" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                        </Triggers>
                        <Listeners>
                            <Select Handler="this.getTrigger(0).show();" />
                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                            <TriggerClick Handler="if (index == 0) { 
                                    this.clearValue(); 
                                    this.getTrigger(0).hide();
                                }" />
                        </Listeners>
                    </ext:ComboBox>
                </td>
                <td style="width: 210px;">
                    <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                        <Proxy>
                            <ext:AjaxProxy Url="../Handler/EmpSearchID.ashx">
                                <ActionMethods Read="GET" />
                                <Reader>
                                    <ext:JsonReader Root="plants" TotalProperty="total" />
                                </Reader>
                            </ext:AjaxProxy>
                        </Proxy>
                        <Model>
                            <ext:Model ID="Model3" IDProperty="Name" Name="ItemLineModel" runat="server">
                                <Fields>
                                    <ext:ModelField Name="Name" Type="String" />
                                    <ext:ModelField Name="EmployeeId" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                    <ext:ComboBox LabelSeparator="" ID="cmbSearch" runat="server" DisplayField="Name"
                        ValueField="EmployeeId" EmptyText="Search Employee"
                        StoreID="storeSearch" TypeAhead="false" Width="200" HideBaseTrigger="true" MinChars="1"
                        TriggerAction="All" ForceSelection="false">
                        <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                            <ItemTpl ID="ItemTpl1" runat="server">
                                <Html>
                                    <tpl>
                                            <div class="search-item">
                                                            <span>{Name}</span>  
                                             </div>
					                </tpl>
                                </Html>
                            </ItemTpl>
                        </ListConfig>
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                        </Triggers>
                        <Listeners>
                            <Select Handler="this.getTrigger(0).show();" />
                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                            <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                        </Listeners>
                    </ext:ComboBox>
                </td>
                <td>
                    <ext:Button ID="btnLoad" runat="server" Text="Load">
                        <Listeners>
                            <Click Fn="searchList">
                            </Click>
                        </Listeners>
                    </ext:Button>
                </td>
                <td>
                    <ext:Button ID="btnExport" runat="server" Text="Export" AutoPostBack="true" OnClick="btnExport_Click" MarginSpec="0 0 0 10">
                    </ext:Button>
                </td>
                <td>
                    <ext:Button ID="btnImportExcel" runat="server" OnClientClick="EducationImport();return false;" Text="<i></i>Import from Excel" MarginSpec="0 0 0 10">
                    </ext:Button>
                </td>

            </tr>
        </table>
        <br />

        <ext:GridPanel StyleSpec="margin-top:10px;" ID="gridEducation" runat="server"
            Cls="itemgrid" OnReadData="Store_ReadData" Scroll="Horizontal">
            <Store>
                <ext:Store ID="storeEducation" runat="server" AutoLoad="true" OnReadData="Store_ReadData"
                    RemotePaging="true" RemoteSort="true">
                    <Proxy>
                        <ext:PageProxy />
                    </Proxy>
                    <Model>
                        <ext:Model ID="Model4" runat="server" IDProperty="EductionId">
                            <Fields>
                                <ext:ModelField Name="EductionId" Type="String" />
                                <ext:ModelField Name="EmployeeId" Type="String" />
                                <ext:ModelField Name="Name" Type="String" />
                                <ext:ModelField Name="Level" Type="String" />
                                <ext:ModelField Name="Faculty" Type="String" />
                                <ext:ModelField Name="Course" Type="String" />
                                <ext:ModelField Name="University" Type="String" />
                                <ext:ModelField Name="Institution" Type="String" />
                                <ext:ModelField Name="Country" Type="String" />
                                <ext:ModelField Name="Percentage" Type="String" />
                                <ext:ModelField Name="Division" Type="String" />
                                <ext:ModelField Name="MajorSubjects" Type="String" />
                                <ext:ModelField Name="PassedYear" Type="String" />
                                <ext:ModelField Name="Place" Type="String" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>
            <ColumnModel ID="ColumnModel1" runat="server">
                <Columns>
                    <ext:Column ID="colEmplooyeeName" runat="server" Text="Name" DataIndex="Name"
                        MenuDisabled="true" Sortable="false" Align="Left" Width="200" Locked="true" />
                    <ext:Column ID="colEmployeeId" runat="server" Text="EIN" DataIndex="EmployeeId" MenuDisabled="true"
                        Sortable="false" Align="Center" Width="60" Locked="true" />
                    <ext:Column ID="colLevel" runat="server" Text="Level" DataIndex="Level" MenuDisabled="true"
                        Sortable="false" Align="Left" Width="140" />
                    <ext:Column ID="colFaculty" runat="server" Text="Faculty" DataIndex="Faculty" MenuDisabled="true"
                        Sortable="false" Align="Left" Width="140" />
                    <ext:Column ID="colCourse" runat="server" Text="Course" DataIndex="Course" MenuDisabled="true"
                        Sortable="false" Align="Left" Width="140" />
                    <ext:Column ID="colUniversity" runat="server" Text="University" DataIndex="University"
                        MenuDisabled="true" Sortable="false" Align="Left" Width="140" />
                    <ext:Column ID="colInstitution" runat="server" Text="Institute" DataIndex="Institution"
                        MenuDisabled="true" Sortable="false" Align="Left" Width="140" />
                    <ext:Column ID="colCountry" runat="server" Text="Country" DataIndex="Country"
                        MenuDisabled="true" Sortable="false" Align="Left" Width="100" />
                    <ext:Column ID="colPercentage" runat="server" Text="Percentage" DataIndex="Percentage"
                        MenuDisabled="true" Sortable="false" Align="Left" Width="80" />
                    <ext:Column ID="colDivision" runat="server" Text="Division" DataIndex="Division"
                        MenuDisabled="true" Sortable="false" Align="Left" Width="100" />
                    <ext:Column ID="colMajorSubjects" runat="server" Text="Major Subjects" DataIndex="MajorSubjects"
                        MenuDisabled="true" Sortable="false" Align="Left" Width="140" />
                    <ext:Column ID="colPassedYear" runat="server" Text="Passed Year" DataIndex="PassedYear"
                        MenuDisabled="true" Sortable="false" Align="Left" Width="110" />
                    <ext:Column ID="colPlace" runat="server" Text="Place" DataIndex="Place"
                        MenuDisabled="true" Sortable="false" Align="Left" Width="140" />
                </Columns>
            </ColumnModel>
            <Plugins>
                <ext:FilterHeader ID="FilterHeader2" runat="server" Remote="true" OnCreateFilterableField="OnCreateFilterableField" />
            </Plugins>
            <BottomBar>
                <ext:PagingToolbar Height="40" ID="PagingToolbar1" runat="server" StoreID="storeEducation"
                    DisplayInfo="true">
                    <Items>
                        <ext:Label ID="lblPageSize" runat="server" Text="Page size:" />
                        <ext:ToolbarSpacer ID="ToolbarSpacer_listItems" runat="server" Width="10" />
                        <ext:ComboBox ID="cmbPageSize" runat="server" Width="80" SelectOnFocus="true" Selectable="true"
                            ValueField="Value" DisplayField="Text" ForceSelection="true" AllowBlank="false">
                            <Listeners>
                                <Select Handler="searchList()" />
                                <%--#{cmbPageSize}.pageSize = this.getValue();#{PagingToolbar1}.moveFirst();--%>
                            </Listeners>
                            <Items>
                                <ext:ListItem Value="25" Text="25" />
                                <ext:ListItem Value="100" Text="100" />
                                <ext:ListItem Value="500" Text="500" />
                                <ext:ListItem Value="100000" Text="All" />
                            </Items>
                            <SelectedItems>
                                <ext:ListItem Index="0">
                                </ext:ListItem>
                            </SelectedItems>
                        </ext:ComboBox>
                    </Items>
                </ext:PagingToolbar>
            </BottomBar>
        </ext:GridPanel>
        <br />
    </div>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
