﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;

namespace Web.NewHR
{
    public partial class OtherHRDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                EEmployee emp = new EmployeeManager().GetById(GetEmployeeID());


                //txtINo.Text = emp.EHumanResources[0].IdCardNo;
                //if (emp.EHumanResources.Count > 0 && emp.EHumanResources[0].ReportToEmployeeId != null)
                {
                    EHumanResource hr = emp.EHumanResources[0];

                    if (hr.ReportToEmployeeId != null)
                        ExtControlHelper.ComboBoxSetSelected(hr.ReportToEmployeeId.ToString(), cmbEmployee);

                    if (emp.CostCodeId != null)
                        ExtControlHelper.ComboBoxSetSelected(emp.CostCodeId.ToString(), cmbCostCode);

                    if (hr.ProgrammeId != null)
                        ExtControlHelper.ComboBoxSetSelected(hr.ProgrammeId.ToString(), cmbProgramme);

                    if (!string.IsNullOrEmpty(hr.AccountCode))
                        txtAccountCode.Text = hr.AccountCode;

                    if (hr.VoucherDepartmentID != null)
                        ExtControlHelper.ComboBoxSetSelected(hr.VoucherDepartmentID.ToString(), cmbHPLVoucherGroup);
                }

                storeEmployeeList.DataSource = EmployeeManager.GetAllActiveEmployees().Where(x => x.EmployeeId != emp.EmployeeId).OrderBy(x => x.Name).ToList();
                storeEmployeeList.DataBind();

                txtProjectCategory.Text = emp.EHumanResources[0].ProjectCategory == null ? "" :
                    emp.EHumanResources[0].ProjectCategory;

                cmbCostCode.Store[0].DataSource = CommonManager.GetAllCostCodes();
                cmbCostCode.Store[0].DataBind();


                List<EProgramme> _listProgramme = new CommonManager().GetAllProgrammes();
                cmbProgramme.Store[0].DataSource = _listProgramme; 
                cmbProgramme.Store[0].DataBind();

                if (_listProgramme.Any())
                    reqdProgram.Visible = true;
                else
                    reqdProgram.Visible = false;

                cmbHPLVoucherGroup.Store[0].DataSource = CommonManager.GetVoucherGroupsForEmp();
                cmbHPLVoucherGroup.Store[0].DataBind();

                if (CommonManager.CompanySetting.WhichCompany == WhichCompany.PSI)
                {
                    valReqdCostCode.Visible = true;
                    reqdProgram.Visible = true;
                }
                else if (CommonManager.CompanySetting.WhichCompany == WhichCompany.HPL)
                {
                    cmbHPLVoucherGroup.Visible = true;
                    reqdVoucherGroup.Visible = true;
                }

                
            }
        }


        protected void btnUpload_Click(object sender, DirectEventArgs e)
        {
            int? managerId = null, costCodeId = null, programId = null;
            int? voucherGroupID = null;

            if(cmbEmployee.SelectedItem != null && cmbEmployee.SelectedItem.Value != null)
                managerId =  int.Parse(cmbEmployee.SelectedItem.Value);

            if (cmbCostCode.SelectedItem != null && cmbCostCode.SelectedItem.Value != null)
                costCodeId = int.Parse(cmbCostCode.SelectedItem.Value);

            if (cmbProgramme.SelectedItem != null && cmbProgramme.SelectedItem.Value != null)
                programId = int.Parse(cmbProgramme.SelectedItem.Value);

            if (cmbHPLVoucherGroup.SelectedItem != null && cmbHPLVoucherGroup.SelectedItem.Value != null)
                voucherGroupID = int.Parse(cmbHPLVoucherGroup.SelectedItem.Value);
            

            Status status = EmployeeManager.UpdateOtherReportsTo
                (GetEmployeeID(), managerId, costCodeId, programId, voucherGroupID,txtAccountCode.Text.Trim());
                
                EmployeeManager.UpdateProjectCat(GetEmployeeID() , txtProjectCategory.Text.Trim());
            if (status.IsSuccess)
                NewMessage.ShowNormalPopup("Employee's Manager Updated");
            else
                NewMessage.ShowWarningMessage(status.ErrorMessage);
        }

        protected int GetEmployeeID()
        {
            //Guid? AssetID = null;
            int EmpID = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                EmpID = int.Parse(Request.QueryString["ID"]);
            }
            return EmpID;
        }

    }
}