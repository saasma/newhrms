﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using Utils.Helper;
using Utils;
using Utils.Web;

namespace Web.NewHR
{
    public partial class EmployeePayroll : BasePage
    {
        PayManager mgr = new PayManager();


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                gender.InnerHtml = string.Format(gender.InnerHtml, CommonManager.GetGenderName);

                lblHandicapped1.Text = CommonManager.GetHandicappedName;
                lblHandicapped2.Text = CommonManager.GetHandicappedName;

                Initialise();
            }
            JavascriptHelper.AttachPopUpCode(Page, "popupIncome", "../cp/IncomeList.aspx", 480, 520);
            JavascriptHelper.AttachPopUpCode(Page, "popupUpdateIncome", "../cp/AEIncome.aspx", 650, 700);

            JavascriptHelper.AttachPopUpCode(Page, "popupDeduction", "../cp/DeductionList.aspx", 470, 440);
            JavascriptHelper.AttachPopUpCode(Page, "popupUpdateDeduction", "../cp/AEDeduction.aspx", 600, 600);
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdfsf", "EmployeeId=" + GetEmployeeId() + ";", true);
            JavascriptHelper.AttachEnableDisablingJSCode(chkCITNoContribution, divNoCIT.ClientID, false);
        }

        public int GetEmployeeId()
        {
            return int.Parse(Request.QueryString["Id"]);
        }

        public void Initialise()
        {
            if (string.IsNullOrEmpty(Request.QueryString["Id"]))
            {
                Response.Write("Employee should be selected for leave assignment,please select employee first.");
                Response.End();
                return;
            }

            //if (CommonManager.CompanySetting.WhichCompany != WhichCompany.Heifer)
            //    txtAdditionalPFDeduction.Visible = false;

            if (CommonManager.Setting.EnableWeeklyLikePay != null && CommonManager.Setting.EnableWeeklyLikePay.Value)
                rowDoNotCalculateSalary.Visible = false;

            if (SessionManager.CurrentCompany.PFRFFunds.Count >= 1
                && SessionManager.CurrentCompany.PFRFFunds[0].HasOtherPFFund != null
                && SessionManager.CurrentCompany.PFRFFunds[0].HasOtherPFFund.Value)
            {
                List<KeyValue> list1 = new List<KeyValue>();
                list1.Add(new KeyValue { Value = SessionManager.CurrentCompany.PFRFAbbreviation, Key = "false" });
                list1.Add(new KeyValue { Value = SessionManager.CurrentCompany.PFRFFunds[0].OtherPFFundName, Key = "true" });
                cmbPFList.Store[0].DataSource = list1;
                cmbPFList.Store[0].DataBind();
            }
            else
                rowPFList.Visible = false;

            EEmployee emp = EmployeeManager.GetEmployeeById(GetEmployeeId());

            SetGeneralTaxInformation(emp);
            SetPFCIT(emp);

            incomeList.InnerHtml = mgr.GetHTMLEmployeeIncomeList(emp.EmployeeId);

            deductionList.InnerHtml = PayManager.GetHTMLEmployeeDeductionList(emp.EmployeeId);

            valRegExPanNo2.ValidationExpression = Config.PanNoRegExp;
            //cmbEmployeePAN.Store[0].DataSource = new PAN().GetMembers();
            //cmbEmployeePAN.Store[0].DataBind();

            cmbPFRelation.Store[0].DataSource = new Relation().GetMembers();
            cmbPFRelation.Store[0].DataBind();


            List<Bank> list = PayManager.GetBankList();
            cmbBankName.Store[0].DataSource = list;
            cmbBankName.Store[0].DataBind();

            //cmbBankBranch.Store[0].DataSource = PayManager.GetBankBranchList();
            //cmbBankBranch.Store[0].DataBind();

            ddlNoCITFromMonth.DataSource = DateManager.GetCurrentMonthList();
            ddlNoCITFromMonth.DataBind();
            ddlNoCITFromYear.DataSource = DateManager.GetYearListForPayrollPeriod();
            ddlNoCITFromYear.DataBind();

            ddlNoCITToMonth.DataSource = DateManager.GetCurrentMonthList();
            ddlNoCITToMonth.DataBind();

            ddlNoCITToYear.DataSource = DateManager.GetYearListForPayrollPeriod();
            ddlNoCITToYear.DataBind();

            PFBlock(emp);

            List<CITIncome> citIncomes = BLL.BaseBiz.PayrollDataContext.CITIncomes.ToList();
            if (citIncomes.Count == 0)
                lblIncomes.Text = new PayManager().GetBasicIncome(SessionManager.CurrentCompanyId).Title;
            else
            {
                lblIncomes.Text = "";
                foreach (CITIncome income in citIncomes)
                {
                    PIncome inc = new PayManager().GetIncomeById(income.IncomeId.Value);

                    if (inc != null)
                    {
                        if (lblIncomes.Text == "")
                            lblIncomes.Text = inc.Title;
                        else
                        {
                            lblIncomes.Text += ", " + inc.Title;
                        }

                    }
                }
            }
        }

        //protected void valCustomRFAC_ServerValidate(object source, ServerValidateEventArgs args)
        //{
        //    if (cmbPaymentMode.SelectedItem == null || cmbPaymentMode.SelectedItem.Value == null)
        //    {
        //        args.IsValid = false;
        //    }
        //    else if (ddlPayMode.SelectedValue == PaymentMode.BANK_DEPOSIT)// || ddlPayMode.SelectedValue == PaymentMode.CHEQUE)
        //    {
        //        args.IsValid = args.Value.Trim() != string.Empty;
        //    }
        //    else
        //        args.IsValid = true;
        //}

        //protected void ValidateTextBoxBank_ServerValidate(object source, ServerValidateEventArgs args)
        //{
        //    if (cmbBankName.SelectedItem == null || cmbBankName.SelectedItem.Value == null)
        //    {
        //        args.IsValid = false;
        //    }
        //    else if (cmbBankName.SelectedItem.Value != PaymentMode.BANK_DEPOSIT)// || ddlPayMode.SelectedValue == PaymentMode.CHEQUE)
        //    {
        //        args.IsValid = false;
        //    }
        //    else
        //        args.IsValid = true;
        //}

        public void cmbBankName_Change(object sender, DirectEventArgs e)
        {
            int bankId = int.Parse(cmbBankName.SelectedItem.Value);

            cmbBankBranch.Store[0].DataSource = PayManager.GetBankBranchList(bankId);
            cmbBankBranch.Store[0].DataBind();
        }

        void PFBlock(EEmployee emp)
        {
            if (emp.EFundDetails.Count > 0)
            {
                EFundDetail fund = emp.EFundDetails[0];
                if (fund.HasOtherPFFund != null && fund.HasOtherPFFund.Value)
                    ExtControlHelper.ComboBoxSetSelected("true", cmbPFList);
                    //cmbPFList.SelectedItems.Add(new Ext.Net.ListItem { Index = 1 });
                txtPFNumber.Text = fund.PFRFNo;
                txtCIDNumber.Text = fund.CID;
                txtPFNominee.Text = fund.Nominee;
                if (!string.IsNullOrEmpty(fund.Relation))
                    ExtControlHelper.ComboBoxSetSelected(fund.Relation, cmbPFRelation);
                //if (!string.IsNullOrEmpty(fund.EmployeePan))
                //    ExtControlHelper.ComboBoxSetSelected(fund.EmployeePan, cmbEmployeePAN);

                //if (fund.AdditionalPFDeduction != null)
                //    txtAdditionalPFDeduction.Text = GetCurrency(fund.AdditionalPFDeduction);

                txtPANNo.Text = fund.PANNo == null ? "" : fund.PANNo.Trim();
                txtCITNumber.Text = fund.CITNo == null ? "" : fund.CITNo.Trim();
                txtCITCode.Text = fund.CITCode == null ? "" : fund.CITCode.Trim();

                if (fund.CITIsRate == null || fund.CITIsRate.Value == false)
                {
                    txtCITAmount.Text = GetCurrency(fund.CITAmount);
                    rdbAmount.Checked = true;
                    rdbRate.Checked = false;
                }
                else if (fund.CITRate != null)
                {
                    txtCITRate.Text = fund.CITRate.ToString();
                    if (fund.CITRateAdditionalAmount != null)
                        txtCITRateAdditionalAmount.Text = GetCurrency(fund.CITRateAdditionalAmount);
                    rdbRate.Checked = true;
                    rdbAmount.Checked = false;

                    txtCITAmount.Disable(true);
                    txtCITRate.Enable(true);
                    txtCITRateAdditionalAmount.Enable(true);
                }

                if (fund.CITIsTable != null && fund.CITIsTable.Value)
                {
                    chkCITTable.Checked = true;
                    rdbAmount.Disable(true);
                    rdbRate.Disable(true);
                    txtCITAmount.Disable(true);
                    txtCITRate.Disable(true);
                    txtCITRateAdditionalAmount.Disable(true);
                }

                chkCITNoContribution.Checked = fund.NoCITContribution.Value;
                if (chkCITNoContribution.Checked)
                {
                    UIHelper.SetSelectedInDropDown(ddlNoCITFromMonth, fund.NoCITFromMonth);
                    UIHelper.SetSelectedInDropDown(ddlNoCITFromYear, fund.NoCITFromYear);
                    UIHelper.SetSelectedInDropDown(ddlNoCITToMonth, fund.NoCITToMonth);
                    UIHelper.SetSelectedInDropDown(ddlNoCITToYear, fund.NoCITToYear);
                    ddlNoCITFromMonth.Enabled = true;
                    ddlNoCITFromYear.Enabled = true;
                    ddlNoCITToMonth.Enabled = true;
                    ddlNoCITToYear.Enabled = true;
                }
                
            }

            // salary payment
            if (emp.PPays.Count > 0)
            {
                PPay pay = emp.PPays[0];
                ExtControlHelper.ComboBoxSetSelected(pay.PaymentMode, cmbPaymentMode);

                if (pay.PaymentMode == PaymentMode.BANK_DEPOSIT)// || entity.PaymentMode == PaymentMode.CHEQUE)
                {
                    cmbBankName.Enable();
                    txtBankNo.Enable();

                    
                   
                    //if (ddlBankName.SelectedItem != null && PayManager.IsBankDropDown())
                    //{
                    //    entity.BankID = int.Parse(ddlBankName.SelectedItem.Value);
                    //    entity.BankName = ddlBankName.SelectedItem.Text;
                    //}
                }

                txtBankNo.Text = pay.BankACNo == null ? "" : pay.BankACNo;
                if (pay.BankID != null)
                {
                    ExtControlHelper.ComboBoxSetSelected(pay.BankID.ToString(), cmbBankName);

                    int bankId = pay.BankID.Value;
                    cmbBankBranch.Store[0].DataSource = PayManager.GetBankBranchList(bankId);
                    cmbBankBranch.Store[0].DataBind();
                }

                if (pay.BankBranchID != null)
                    ExtControlHelper.ComboBoxSetSelected(pay.BankBranchID.ToString(), cmbBankBranch);

                txtLoanAccount.Text = pay.LoanAccount == null ? "" : pay.LoanAccount;
                txtAdvanceAccount.Text = pay.AdvanceAccount == null ? "" : pay.AdvanceAccount;

            }
        }
        private void SetPFCIT(EEmployee emp)
        {
            PayrollPeriod period = CommonManager.GetLastPayrollPeriod();
            if (period != null)
            {
                if (SessionManager.CurrentCompany.HasPFRFFund)
                {
                    GetEmployeeCurrentStatusForPayrollPeriodResult item =
                        BLL.BaseBiz.PayrollDataContext.GetEmployeeCurrentStatusForPayrollPeriod(period.PayrollPeriodId,
                        emp.EmployeeId,
                        SessionManager.CurrentCompany.PFRFFunds[0].EffectiveFrom).SingleOrDefault();
                    if (item != null)
                    {
                        lblRetirementFund.Text = SessionManager.CurrentCompany.PFRFFunds[0]
                            .EmployeeContribution + "%";
                    }
                }

                if (emp.EFundDetails.Count > 0)
                {
                    EFundDetail fund = emp.EFundDetails[0];

                    if (Convert.ToDecimal(fund.CITAmount) != 0)
                    {
                        lblCITContribution.Text = "Yes";
                    }
                    else if (Convert.ToDecimal(fund.CITRate) != 0)
                    {
                        lblCITContribution.Text = "Yes";
                    }
                    else
                    {
                        OptimumPFAndCIT optimumPFAndCIT = PayManager.GetOptimumPFAndCIT(emp.EmployeeId);
                        if (optimumPFAndCIT != null)
                        {
                            lblCITContribution.Text = "Yes";
                        }
                    }

                    if (fund.NoPF != null && fund.NoPF.Value)
                        chkNoPF.Checked = fund.NoPF.Value;

                    if (fund.EEmployee.ExcludeInSalary != null && fund.EEmployee.ExcludeInSalary.Value)
                        chkExcludeInSalary.Checked = fund.EEmployee.ExcludeInSalary.Value;

                    //if (fund.ExcludeInGratuity != null && fund.ExcludeInGratuity.Value)
                    //    chkExcludeGratuity.Checked = fund.ExcludeInGratuity.Value;
                }

                

                bool? hasInsurance = BLL.BaseBiz.PayrollDataContext.IIndividualInsurances.Any(x => x.EmployeeId == emp.EmployeeId);
                if (hasInsurance != null && hasInsurance.Value)
                {
                    lblInsurance.Text = "Yes";
                }

                if (BLL.BaseBiz.PayrollDataContext.MedicalTaxCredits.Any(x => x.EmployeeId == emp.EmployeeId))
                {
                    lblMedicalTaxCredit.Text = "Yes";
                }
            }


           

        }

        private void SetGeneralTaxInformation(EEmployee emp)
        {
            lblGender.Text = EmployeeManager.GetGenderValue(emp.Gender);
            lblMaritalStatus.Text = emp.MaritalStatus;

            if (emp.MaritalStatus != MaritalStatus.MARRIED)
            {
                btnTaxStatusEdit.Visible = false;
                chkTaxStatus.Disable(true);
            }

            if (emp.HasCoupleTaxStatus != null && emp.HasCoupleTaxStatus.Value)
                chkTaxStatus.Checked = true;
            else
                chkTaxStatus.Checked = false;

            if (emp.IsEmployeeIncomeFromOtherSource != null && emp.IsEmployeeIncomeFromOtherSource.Value)
                chkIsIncomeFromOtherSource.Checked = true;
            else
                chkIsIncomeFromOtherSource.Checked = false;

            if (emp.IsHandicapped != null && emp.IsHandicapped.Value)
                lblHandicapped.Text = "Yes";
            else
                lblHandicapped.Text = "No";

            if (emp.BranchId != null)
            {
                GetEmployeeCurrentBranchDepartmentResult currentBranchDep = EmployeeManager.GetEmployeeCurrentBranchDepartment(emp.EmployeeId);

                lblBranch.Text = currentBranchDep.Branch + " , Branch Setting";
                Branch branch = new BranchManager().GetById(currentBranchDep.BranchId.Value);
                if (branch.IsInRemoteArea != null && branch.IsInRemoteArea.Value)
                    lblRemoateArea.Text = "Yes";
            }


            if (emp.IsLocalEmployee != null && emp.IsLocalEmployee.Value)
                chkLocalEmployee.Checked = true;

            if (emp.IsNonResident != null && emp.IsNonResident.Value)
                chkNonResident.Checked = true;

            if (emp.NoTax != null && emp.NoTax.Value)
                chkNoTax.Checked = true;

        }


        protected void btnSaveUpdate_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("InsertUpdate");
            if (Page.IsValid)
            {
                EEmployee emp = new EEmployee();
                PPay pay = new PPay();
                EFundDetail fund = new EFundDetail();
                emp.EFundDetails.Add(fund);
                emp.PPays.Add(pay);
                emp.EmployeeId = GetEmployeeId();

                emp.HasCoupleTaxStatus = chkTaxStatus.Checked;
                emp.ExcludeInSalary = chkExcludeInSalary.Checked;
                emp.IsEmployeeIncomeFromOtherSource = chkIsIncomeFromOtherSource.Checked;
                emp.IsLocalEmployee = chkLocalEmployee.Checked;
                emp.IsNonResident = chkNonResident.Checked;
                emp.NoTax = chkNoTax.Checked;

                fund.NoPF = chkNoPF.Checked;
                fund.PFRFNo = txtPFNumber.Text.Trim();

                if (cmbPFList.SelectedItem != null && cmbPFList.SelectedItem.Value != null)
                {
                    if (cmbPFList.SelectedItem.Index == 0)
                    { }
                    else
                        fund.HasOtherPFFund = true;
                }

                fund.CID = txtCIDNumber.Text.Trim();
                fund.Nominee = txtPFNominee.Text.Trim();
                //fund.ExcludeInGratuity = chkExcludeGratuity.Checked;
                if (cmbPFRelation.SelectedItem != null && cmbPFRelation.SelectedItem.Value != null)
                    fund.Relation = cmbPFRelation.SelectedItem.Value;

                //if (!string.IsNullOrEmpty(txtAdditionalPFDeduction.Text.Trim()))
                //    fund.AdditionalPFDeduction = decimal.Parse(txtAdditionalPFDeduction.Text.Trim());

                // CIT Block
                fund.CITNo = txtCITNumber.Text.Trim();
                fund.CITCode = txtCITCode.Text.Trim();
                fund.HasCIT = true;
                //if (fund.HasCIT)
                //{
                //    fund.CITEffectiveFromMonth = int.Parse(ddlCITFromMonths.SelectedValue);
                //    fund.CITEffectiveFromYear = int.Parse(ddlCITFromYears.SelectedValue);
                //}

                if (rdbAmount.Checked)
                {

                    decimal amt;
                    if (decimal.TryParse(txtCITAmount.Text.Trim(), out amt))
                        fund.CITAmount = amt;
                    else
                        fund.CITAmount = null;
                }
                else if (rdbRate.Checked)
                {
                    double rate;
                    if (double.TryParse(txtCITRate.Text.Trim(), out rate))
                        fund.CITRate = rate;
                    else
                        fund.CITRate = null;

                    if (!string.IsNullOrEmpty(txtCITRateAdditionalAmount.Text))
                        fund.CITRateAdditionalAmount = Convert.ToDecimal(txtCITRateAdditionalAmount.Text.Trim());
                }
                fund.CITIsRate = rdbRate.Checked;

                fund.NoCITContribution = chkCITNoContribution.Checked;
                if (fund.NoCITContribution.Value)
                {
                    fund.NoCITFromMonth = int.Parse(ddlNoCITFromMonth.SelectedValue);
                    fund.NoCITFromYear = int.Parse(ddlNoCITFromYear.SelectedValue);
                    fund.NoCITToMonth = int.Parse(ddlNoCITToMonth.SelectedValue);
                    fund.NoCITToYear = int.Parse(ddlNoCITToYear.SelectedValue);
                }

                // Salary Payment
                pay.PaymentMode = cmbPaymentMode.SelectedItem.Value;
                if (pay.PaymentMode == PaymentMode.BANK_DEPOSIT)// || entity.PaymentMode == PaymentMode.CHEQUE)
                {
                    pay.BankACNo = txtBankNo.Text.Trim();

                    pay.BankID = int.Parse(cmbBankName.SelectedItem.Value);
                    pay.BankName = cmbBankName.SelectedItem.Text;

                    if (cmbBankBranch.SelectedItem != null && cmbBankBranch.SelectedItem.Value != null)
                        pay.BankBranchID = int.Parse(cmbBankBranch.SelectedItem.Value);
                }
                else
                {
                    // do not update bank info for Cash type
                    //if (cmbBankName.SelectedItem != null && cmbBankName.SelectedItem.Value != null)
                    //{
                    //    pay.BankID = int.Parse(cmbBankName.SelectedItem.Value);
                    //    pay.BankName = cmbBankName.SelectedItem.Text;

                    //    if (cmbBankBranch.SelectedItem != null && cmbBankBranch.SelectedItem.Value != null)
                    //        pay.BankBranchID = int.Parse(cmbBankBranch.SelectedItem.Value);
                    //}

                    //pay.BankACNo = txtBankNo.Text.Trim();
                }

                pay.LoanAccount = txtLoanAccount.Text.Trim();
                pay.AdvanceAccount = txtAdvanceAccount.Text.Trim();

                //if (cmbEmployeePAN.SelectedItem != null && cmbEmployeePAN.SelectedItem.Value != null)
                //    fund.EmployeePan = cmbEmployeePAN.SelectedItem.Value;
                fund.PANNo = txtPANNo.Text.Trim();

                
                Status status =  NewPayrollManager.UpdateFund(fund,emp,pay);

                X.Js.AddScript("window.scrollTo(0,0);");

                if (status.IsSuccess)
                    SetMessage(lblMsg, "Information is updated.");
                else
                    SetWarning(lblMsg, status.ErrorMessage);
            }

        }

        

    }

}