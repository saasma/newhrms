﻿<%@ Page Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" Title="Retirement Details" EnableEventValidation="false"
    CodeBehind="Retirement.aspx.cs" Inherits="Web.NewHR.Retirement" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/ContentHeader.ascx" TagName="ContentHeader" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .GridRowHover
        {
            cursor: pointer;
        }
        .empName
        {
            font-size: 20px;
            color: #439EC6;
            font-weight: bold;
        }
        .titleDesign
        {
            font-size: 15px;
            color: #000000;
            text-decoration: underline;
            padding-top: 10px;
        }
        
        .items span
        {
            display: block;
            padding-top: 3px;
            font-weight: bold;
        }
        .selected td
        {
            background-color: #E0EDF9 !important;
        }
        form strong
        {
            display: block !important;
            padding-bottom: 3px;
        }
        
        .total
        {
            border-top: 1px solid black;
            border-bottom: 4px double black;
            margin-top: 9px;
            padding: 5px;
            font-weight: bold;
            font-size: 15px;
        }
        
        .total .totalAmount
        {
            float: right;
        }
        .netPayTotal
        {
            width: 396px;
            margin-left: 570px;
        }
        .mb30
        {
            margin-bottom: 0px !important;
        }
    </style>
    <style type="text/css">
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
        .hideLeftBlockCssInPage
        {
            padding-left: 10px !important;
        }
        
        .panel-heading
        {
            height: 40px !important;
        }
        .dashboardNotificatioin li
        {
            padding: 5px;
        }
        .blankColorCell
        {
            background-color: #fff !important;
            border-top: #fff !important;
            border-left: #fff !important;
            border-bottom: #fff !important;
        }
        .selected
        {
            background-color: lightblue;
        }
    </style>
    <script type="text/javascript">
        function openCalculationPopup(ein) {
            if (typeof (ein) == 'undefined')
                ein = 0;
            window.open('../CP/retirementcalculation.aspx?EmployeeID=' + ein, 'Retirement Saving', 'height=600,width=1200');
        }

        function refreshPage() {
            __doPostBack('Reload', '');
        }

        function ACE_item_selected(source, eventArgs) {
            var value = eventArgs.get_value();
            document.getElementById('<%=hiddenEmpID.ClientID %>').value = value;
            //window.location = "Employee.aspx?Id=" + value; 
            //alert(value);
            //document.getElementById('<%= txtEmpSearchText.ClientID%>').value = eventArgs.get_text();


        }

        function viewAdjustment(leaveTypeId) {
            <%=hdnLeaveTypeId.ClientID %>.setValue(leaveTypeId);
            <%=btnShowAdjustment.ClientID %>.fireEvent('click');
        }

        function showDetails(empid, ctl) {

            document.getElementById('<%=txtEmpSearchText.ClientID %>').value = ctl.getAttribute('empname');
            document.getElementById('<%=hiddenEmpID.ClientID %>').value = empid;

            __doPostBack('Reload', '');
        }
    </script>
    <link href="../css/core.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4 id="header" runat="server">
                    Employee Retirement for :
                </h4>
            </div>
        </div>
    </div>
    <ext:Hidden ID="hdnLeaveTypeId" runat="server" />
    <ext:Button ID="btnShowAdjustment" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnShowAdjustment_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>
    <div class="contentpanel">
        <uc1:ContentHeader Id="ContentHeader1" runat="server" />
        <asp:HiddenField ID="hiddenEmpID" runat="server" />
        <div class="contentArea">
            <div class="attribute" runat="server" id="divSearch" style="padding: 10px">
                <div class="left" style="padding-left: 10px;">
                    <strong>Employee</strong>
                    <asp:TextBox ID="txtEmpSearchText" AutoPostBack="true" OnTextChanged="LoadEmployeeDetails"
                        Style='width: 200px; font-weight: normal;' runat="server"></asp:TextBox>
                    <cc1:TextBoxWatermarkExtender ID="TBWE2" runat="server" TargetControlID="txtEmpSearchText"
                        WatermarkText="Employee Search" WatermarkCssClass="searchBoxText" />
                    <cc1:AutoCompleteExtender EnableCaching="true" ID="AutoCompleteExtenderOrganization"
                        runat="server" MinimumPrefixLength="2" ServiceMethod="GetEmployeeNames" ServicePath="~/PayrollService.asmx"
                        OnClientItemSelected="ACE_item_selected" TargetControlID="txtEmpSearchText" CompletionSetCount="10"
                        CompletionInterval="250" CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList"
                        CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
                    </cc1:AutoCompleteExtender>
                </div>
            </div>
            <div style="clear: both" />
            <uc2:MsgCtl ID="divMsgCtl" Width="900px" EnableViewState="false" Hide="true" runat="server" />
            <uc2:WarningCtl ID="divWarningMsg" Width="900px" EnableViewState="false" Hide="true"
                runat="server" />
            <table>
                <tr>
                    <td valign="top">
                        <div class="panel panel-primary" runat="server" id="body">
                            <div class="panel-heading">
                                <!-- panel-btns -->
                                <h3 class="panel-title">
                                    Employee Details</h3>
                            </div>
                            <div class="panel-body">
                                <table style='clear: both; padding-bottom: 10px; margin-top: 10px;'>
                                    <tr>
                                        <td valign="top" style="padding-left: 5px;">
                                            <asp:Image ID="image" runat="server" ImageUrl="~/images/sample.jpg" Width="150px"
                                                Height="150px" />
                                        </td>
                                        <td valign="top" style="padding-left: 40px" class="items">
                                            <asp:Label ID="lblName" CssClass="empName" runat="server" />
                                            <asp:Label ID="lblBranch" runat="server" />
                                            <asp:Label ID="lblDepartment" runat="server" />
                                            <asp:Label ID="lblPosition" runat="server" />
                                            <asp:Label ID="lblSince" runat="server" />
                                            <asp:Label ID="lblTime" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" valign="top" style="padding-left: 0px" class="items">
                                            <table class="fieldTable firsttdskip">
                                                <tr>
                                                    <td>
                                                        <strong style="padding-bottom: 0px; margin-top: -3px;">Retirement Date</strong>
                                                        <pr:CalendarExtControl Width="150px" ID="calRetirementDate" runat="server" LabelSeparator="" />
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="calRetirementDate"
                                                            Display="None" ErrorMessage="Please choose a Retirement Date." ValidationGroup="AEEmployee"></asp:RequiredFieldValidator>
                                                    </td>
                                                    <td valign="bottom">
                                                        <strong style="padding-bottom: 0px; margin-top: -3px;">Retirement Type</strong>
                                                        <asp:DropDownList ID="ddlRetirementType" AppendDataBoundItems="true" DataTextField="Name"
                                                            DataValueField="EventID" runat="server" Width="170px">
                                                            <asp:ListItem Text="--Select Type--" Value="-1" Selected="True" />
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator InitialValue="-1" ID="reqdRetirementTypes" runat="server"
                                                            ControlToValidate="ddlRetirementType" Display="None" ErrorMessage="Retirement type is required."
                                                            ValidationGroup="AEEmployee"></asp:RequiredFieldValidator>
                                                    </td>
                                                    <td valign="bottom">
                                                        <asp:Button ID="btnSetRetirement" CssClass="save" runat="server" Width="120px" Text="Set Retirement"
                                                            OnClientClick="if(confirm('Are you sure to retire this employee?')) {valGroup='AEEmployee';return CheckValidation();} else return false;"
                                                            OnClick="btnSetRetirement_Click" />
                                                        <asp:Button ID="btnDeleteRetirement" Width="150px" CssClass="save" runat="server"
                                                            Text="Delete Retirement" OnClientClick="if(confirm('Confirm delete retirement?')) {valGroup='AEEmployee';return CheckValidation();}  else return false;"
                                                            OnClick="btnDeleteRetirement_Click" />
                                                    </td>
                                                    <td style="padding-left: 0px!important">
                                                        <asp:LinkButton ID="btnReload" OnClientClick="Ext.getBody().mask('Loading...');"
                                                            ToolTip="Reload Calculation of current employee" runat="server" Text="" OnClick="LoadEmployeeDetailsWithAtte"
                                                            Style='float: right; margin-right: 0px;'>
                                                            <asp:Image ID="Image1" Style='width: 22px; margin-top: 3px; margin-right: 6px;' ImageUrl='~/images/refresh.png'
                                                                runat="server" />
                                                        </asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                <div style="clear: both">
                                </div>
                                <div runat="server" id="bodyDetails" visible="false">
                                    <div style="margin-top: 10px; margin-bottom: 10px;">
                                        <h3>
                                            Encashable Leaves</h3>
                                        <cc2:EmptyDisplayGridView Style="clear: both; width: auto" CssClass="table table-primary mb30 table-bordered table-hover"
                                            UseAccessibleHeader="true" ShowHeaderWhenEmpty="false" ID="gridEncashableLeaves"
                                            runat="server" DataKeyNames="LeaveTypeId" AutoGenerateColumns="False" GridLines="None"
                                            AllowSorting="True" ShowFooterWhenEmpty="False">
                                            <%-- <RowStyle BackColor="#E3EAEB" />--%>
                                            <Columns>
                                                <asp:TemplateField HeaderStyle-Width="140" HeaderText="Leave" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <%# Eval("Name")%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderStyle-Width="40" ItemStyle-HorizontalAlign="Right" HeaderText="Last Year">
                                                    <ItemTemplate>
                                                        <%# (Eval("LastYearOpening", "{0:n2}"))%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderStyle-Width="40" ItemStyle-HorizontalAlign="Right" HeaderText="Current Year">
                                                    <ItemTemplate>
                                                        <%# (Eval("CurrentYearAccured","{0:n2}"))%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderStyle-Width="40" ItemStyle-HorizontalAlign="Right" HeaderText="Current Year Eligible">
                                                    <ItemTemplate>
                                                        <%# (Eval("CurrentYearEligible", "{0:n2}"))%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderStyle-Width="40" ItemStyle-HorizontalAlign="Right" HeaderText="Taken">
                                                    <ItemTemplate>
                                                        <%# (Eval("Taken"))%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderStyle-Width="40" ItemStyle-HorizontalAlign="Right" HeaderText="Adjusted">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" Text='<%# (Eval("Adjusted","{0:n2}"))%>' ToolTip='<%# Eval("AdjustmentNotes") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderStyle-Width="40" ItemStyle-HorizontalAlign="Right" HeaderText="Balance">
                                                    <ItemTemplate>
                                                        <%# (Eval("ReminingLeave"))%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderStyle-Width="40" HeaderStyle-CssClass="blankColorCell" HeaderText="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;">
                                                    <ItemTemplate>
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    </ItemTemplate>
                                                    <ItemStyle CssClass="blankColorCell" />
                                                </asp:TemplateField>
                                                <%--<asp:TemplateField HeaderStyle-Width="40" ItemStyle-HorizontalAlign="Right" HeaderText="Lapse">
                                                    <ItemTemplate>
                                                        <%# (Eval("Lapse"))%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>
                                                <asp:TemplateField HeaderStyle-Width="40" ItemStyle-HorizontalAlign="Right" HeaderText="Encash Total">
                                                    <ItemTemplate>
                                                        <%# (Eval("EncashTotal"))%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderStyle-Width="40" HeaderStyle-CssClass="blankColorCell" HeaderText="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;">
                                                    <ItemTemplate>
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    </ItemTemplate>
                                                    <ItemStyle CssClass="blankColorCell" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderStyle-Width="40" ItemStyle-HorizontalAlign="Right" HeaderText="Regular Encash">
                                                    <ItemTemplate>
                                                        <%# (Eval("Encased"))%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderStyle-Width="40" ItemStyle-HorizontalAlign="Right" HeaderText="Retirement Encash">
                                                    <ItemTemplate>
                                                        <%# (Eval("RetirementEncash"))%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                                    HeaderText="Encashment Incomes" HeaderStyle-Width="150px">
                                                    <ItemTemplate>
                                                        <%# Eval("EncashmentIncomes")%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                                    HeaderText="" HeaderStyle-Width="50px">
                                                    <ItemTemplate>
                                                        <asp:HyperLink runat="server" Text="Adjust" NavigateUrl='javascript:void(0)' onclick='<%# "viewAdjustment(" + Eval("LeaveTypeId") + ");" %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <%--  <RowStyle CssClass="odd" />
                                            <AlternatingRowStyle CssClass="even" />--%>
                                        </cc2:EmptyDisplayGridView>
                                    </div>
                                    <div style="clear: both">
                                    </div>
                                    <table style="margin-top: 20px;">
                                        <tr>
                                            <td valign="top">
                                                <h3>
                                                    Amount Payable</h3>
                                                <cc2:EmptyDisplayGridView Style="clear: both; width: auto" CssClass="table table-primary mb30 table-bordered table-hover"
                                                    UseAccessibleHeader="true" ShowHeaderWhenEmpty="True" ID="gvw" runat="server"
                                                    DataKeyNames="Type,SourceId,Amount,AmountWithOutAdjustment" AutoGenerateColumns="False"
                                                    GridLines="None" AllowSorting="True" ShowFooterWhenEmpty="False">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Pay Name" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="245px">
                                                            <ItemTemplate>
                                                                <%# Eval("HeaderName")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"
                                                            HeaderText="Salary" HeaderStyle-Width="100px">
                                                            <ItemTemplate>
                                                                <%# GetCurrency(Eval("FullAmount"))%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderText="Calculated Amount"
                                                            HeaderStyle-Width="108px">
                                                            <ItemTemplate>
                                                                <%# GetCurrency(Eval("AmountWithOutAdjustment"))%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"
                                                            HeaderText="Adjusted" HeaderStyle-Width="90px">
                                                            <ItemTemplate>
                                                                <asp:TextBox autocomplete="off" Text='<%# GetCurrency(Eval("Amount"))%>' Enabled='<%# IsEditable(Eval("Type"),Eval("SourceId")) %>'
                                                                    Width="90px" AutoCompleteType="None" CssClass='calculationInput' Style='text-align: right'
                                                                    ID="txtAmount" runat="server"></asp:TextBox>
                                                                <asp:RequiredFieldValidator SetFocusOnError="true" Display="None" ID="val1" ControlToValidate="txtAmount"
                                                                    ValidationGroup="AEEmployee" runat="server" ErrorMessage="Amount is required." />
                                                                <asp:CompareValidator SetFocusOnError="true" ValueToCompare="0" Display="None" Operator="DataTypeCheck"
                                                                    Type="Currency" ID="valOpeningBalance" ControlToValidate="txtAmount" ValidationGroup="AEEmployee"
                                                                    runat="server" ErrorMessage="Invalid amount."></asp:CompareValidator>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <RowStyle CssClass="odd" />
                                                    <AlternatingRowStyle CssClass="even" />
                                                </cc2:EmptyDisplayGridView>
                                            </td>
                                            <td style="padding-left: 10px" valign="top">
                                                <h3>
                                                    Amount Deductable</h3>
                                                <cc2:EmptyDisplayGridView Style='clear: both; width: auto' CssClass="table table-primary mb30 table-bordered table-hover"
                                                    UseAccessibleHeader="true" ShowHeaderWhenEmpty="True" ID="gvwDeductionList" runat="server"
                                                    DataKeyNames="Type,SourceId,Amount,AmountWithOutAdjustment" AutoGenerateColumns="False"
                                                    GridLines="None" AllowSorting="True" ShowFooterWhenEmpty="False">
                                                    <Columns>
                                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Pay Name" HeaderStyle-Width="150px">
                                                            <ItemTemplate>
                                                                <asp:Label ID="Label2" runat="server" Text='<%# Eval("HeaderName")%>' Width="150px" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderText="Calculated Amount"
                                                            HeaderStyle-Width="176px">
                                                            <ItemTemplate>
                                                                <%-- for cit enable--%>
                                                                <asp:DropDownList SelectedValue='<%# Eval("HasCIT").ToString().ToLower() %>' Visible='<%# (Eval("Type") + ":" + Eval("SourceId"))=="11:11" %>'
                                                                    Width="176px" ID="ddlHasCIT" runat="server">
                                                                    <asp:ListItem Text="Regular" Value="true" />
                                                                    <asp:ListItem Text="Do Not Deduct" Value="false" />
                                                                </asp:DropDownList>
                                                                <span id="Span1" runat="server" visible='<%# (Eval("Type") + ":" + Eval("SourceId"))!="11:11" %>'>
                                                                    <%# GetCurrency(Eval("AmountWithOutAdjustment"))%></span>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Right" HeaderText="Adjusted" HeaderStyle-Width="90px">
                                                            <ItemTemplate>
                                                                <asp:TextBox Text='<%# GetCurrency(Eval("Amount"))%>' Enabled='<%# IsEditable(Eval("Type"),Eval("SourceId")) %>'
                                                                    Width="100px" AutoCompleteType="None" CssClass='calculationInput' Style='text-align: right'
                                                                    ID="txtAmount1" runat="server"></asp:TextBox>
                                                                <asp:RequiredFieldValidator SetFocusOnError="true" Display="None" ID="val1" ControlToValidate="txtAmount1"
                                                                    ValidationGroup="AEEmployee" runat="server" ErrorMessage="Amount is required." />
                                                                <asp:CompareValidator SetFocusOnError="true" ValueToCompare="0" Display="None" Operator="DataTypeCheck"
                                                                    Type="Currency" ID="valOpeningBalance" ControlToValidate="txtAmount1" ValidationGroup="AEEmployee"
                                                                    runat="server" ErrorMessage="Invalid amount."></asp:CompareValidator>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <RowStyle CssClass="odd" />
                                                    <AlternatingRowStyle CssClass="even" />
                                                </cc2:EmptyDisplayGridView>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="total" style="width: 456px; margin-left: 560px; border-bottom: 0px; border-top: 0px;">
                                        <span class="totalText">Net Salary</span> <span class="totalAmount" runat="server"
                                            id="netSalary">100</span>
                                    </div>
                                    <table>
                                        <tr>
                                            <td valign="top">
                                                <cc2:EmptyDisplayGridView CssClass="table table-primary mb30 table-bordered table-hover"
                                                    ShowHeader="true" UseAccessibleHeader="true" ShowHeaderWhenEmpty="false" ID="gvwOtherIncomes"
                                                    Style="margin-top: 10px; clear: both; width: auto" runat="server" DataKeyNames="Type,SourceId,Amount,FullAmount"
                                                    AutoGenerateColumns="False" AllowSorting="True" ShowFooterWhenEmpty="False">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Other Incomes" HeaderStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <asp:Label ID="Label2" runat="server" Text='<%# Eval("HeaderName")%>' Width="100px" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"
                                                            HeaderText="Gross Amt" HeaderStyle-Width="80px">
                                                            <ItemTemplate>
                                                                <asp:Label ID="Label1" runat="server" Text='<%# GetCurrency(Eval("FullAmount"))%>'
                                                                    Width="80px" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Right" HeaderText="Adjusted Gross"
                                                            HeaderStyle-Width="90px">
                                                            <ItemTemplate>
                                                                <asp:TextBox Text='<%# GetCurrency(Eval("AdjustedAmount"))%>' Width="90px" AutoCompleteType="None"
                                                                    CssClass='calculationInput' Style='text-align: right' ID="txtAmount" runat="server"></asp:TextBox>
                                                                <asp:RequiredFieldValidator SetFocusOnError="true" Display="None" ID="val1" ControlToValidate="txtAmount"
                                                                    ValidationGroup="AEEmployee" runat="server" ErrorMessage="Amount is required." />
                                                                <asp:CompareValidator SetFocusOnError="true" ValueToCompare="0" Display="None" Operator="DataTypeCheck"
                                                                    Type="Currency" ID="valOpeningBalance" ControlToValidate="txtAmount" ValidationGroup="AEEmployee"
                                                                    runat="server" ErrorMessage="Invalid Adjusted Gross amount."></asp:CompareValidator>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"
                                                            HeaderText="TDS (15%)" HeaderStyle-Width="70px">
                                                            <ItemTemplate>
                                                                <asp:Label ID="Label3" runat="server" Text='<%# GetCurrency(Eval("TDS"))%>' Width="70px" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Right" HeaderText="TDS Adjustment"
                                                            HeaderStyle-Width="90px">
                                                            <ItemTemplate>
                                                                <asp:TextBox Text='<%# (Eval("TDSAdjustment", "{0:0.00}"))%>' Width="80px" AutoCompleteType="None"
                                                                    CssClass='calculationInput' Style='text-align: right' ID="txtTDSAdjustment" runat="server"></asp:TextBox>
                                                                <asp:CompareValidator SetFocusOnError="true" ValueToCompare="0" Display="None" Operator="DataTypeCheck"
                                                                    Type="Double" ID="valOpeningBalance1" ControlToValidate="txtTDSAdjustment" ValidationGroup="AEEmployee"
                                                                    runat="server" ErrorMessage="Invalid TDS Adjustment amount."></asp:CompareValidator>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"
                                                            HeaderText="Net Payable" HeaderStyle-Width="70px">
                                                            <ItemTemplate>
                                                                <asp:Label ID="Label31" runat="server" Text='<%# GetCurrency(Eval("NetPayable"))%>'
                                                                    Width="70px" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <RowStyle CssClass="odd" />
                                                    <AlternatingRowStyle CssClass="even" />
                                                </cc2:EmptyDisplayGridView>
                                                <span style="width: 525px!important; display: block; margin-top: 13px;">Note : (First
                                                    save the adjustment for non percent incomes & deductions other than sst/tds, and
                                                    then only change and save for tax) </span>
                                            </td>
                                            <td style="padding-left: 10px" valign="top">
                                                <cc2:EmptyDisplayGridView CssClass="table table-primary mb30 table-bordered table-hover"
                                                    ShowHeader="true" UseAccessibleHeader="true" ShowHeaderWhenEmpty="false" ID="gvwOtherDeductions"
                                                    Style="margin-top: 10px; clear: both;" runat="server" DataKeyNames="SettlementId,Type,SourceId,Amount,AmountWithOutAdjustment"
                                                    AutoGenerateColumns="False" CellPadding="4" GridLines="None" AllowSorting="True"
                                                    ShowFooterWhenEmpty="False">
                                                    <%--<RowStyle BackColor="#E3EAEB" />--%>
                                                    <Columns>
                                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Pay Name">
                                                            <ItemTemplate>
                                                                <asp:Label ID="Label2" runat="server" Text='<%# Eval("HeaderName")%>' Width="196px" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderText="Calculated Amount">
                                                            <ItemTemplate>
                                                                <asp:Label ID="Label3" runat="server" Text='<%# GetCurrency(Eval("AmountWithOutAdjustment"))%>'
                                                                    Width="130px" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Right" HeaderText="Adjusted">
                                                            <ItemTemplate>
                                                                <asp:TextBox Text='<%# GetCurrency(Eval("Amount"))%>' Width="100px" AutoCompleteType="None"
                                                                    CssClass='calculationInput' Style='text-align: right' ID="txtAmountOtherDeduction"
                                                                    runat="server"></asp:TextBox>
                                                                <asp:RequiredFieldValidator SetFocusOnError="true" Display="None" ID="val1" ControlToValidate="txtAmountOtherDeduction"
                                                                    ValidationGroup="AEEmployee" runat="server" ErrorMessage="Please enter some amount." />
                                                                <asp:CompareValidator SetFocusOnError="true" ValueToCompare="0" Display="None" Operator="DataTypeCheck"
                                                                    Type="Currency" ID="valOpeningBalance" ControlToValidate="txtAmountOtherDeduction"
                                                                    ValidationGroup="AEEmployee" runat="server" ErrorMessage="The amount you entered is not valid."></asp:CompareValidator>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Right" HeaderText="Exclude">
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkExclude" runat="server" Checked='<%# Eval("ExcludeFromRetirement")%>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <%--<RowStyle CssClass="odd" />
                                                    <AlternatingRowStyle CssClass="even" />--%>
                                                </cc2:EmptyDisplayGridView>
                                            </td>
                                        </tr>
                                    </table>
                                    <table>
                                        <tr>
                                            <td valign="top">
                                                <div class="total" style="width: 558px;">
                                                    <span class="totalText">Total Pay</span> <span class="totalAmount" runat="server"
                                                        id="spanInomeTotal"></span><span></span>
                                                </div>
                                            </td>
                                            <td style="padding-left: 10px" valign="top">
                                                <div class="total" style="width: 456px;">
                                                    <span class="totalText">Total Deduction</span> <span class="totalAmount" runat="server"
                                                        id="spanDeductionTotal"></span>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <div style="clear: both">
                                    </div>
                                    <div class="total" style="width: 456px; margin-left: 568px;">
                                        <span class="totalText">Net Pay</span> <span class="totalAmount" runat="server" id="spanNetPay">
                                        </span>
                                    </div>
                                     <div runat="server" id="divGratuity" visible="false" style="clear: both;margin-top:20px;">
                                       <span style='font-weight:bold;font-size:14px;'> Gratuity </span><br />
                                       <asp:Label  style='font-weight:bold;font-size:12px;' runat="server" ID="lblGratuity" />
                                    </div>
                                    <div style="clear: both;margin-top:20px;">
                                        Retirement Notes<br />
                                        <asp:TextBox Width="557px" Height="50" runat="server" TextMode="MultiLine" ID="txtRetirementNotes">
                                        </asp:TextBox>
                                    </div>
                                    <div class="buttonsDiv" style="text-align: left; clear: both" runat="server" id="buttonsBar">
                                        &nbsp; &nbsp;
                                        <asp:Button ID="btnSave" Width="120px" CssClass="save" runat="server" Text="Save Adjustment"
                                            OnClientClick="if(confirm('Do you really want to change the amounts?')) {valGroup='AEEmployee';return CheckValidation();}  else return false;"
                                            OnClick="btnSave_Click" />
                                        <asp:Button ID="btnSaveCalculation" Width="120px" CssClass="update" runat="server"
                                            Text="Confirm Payment" OnClientClick="" OnClick="btnSave_Click" />
                                        <asp:Button ID="btnResetAllAdjustment" Width="150px" CssClass="save" runat="server"
                                            Text="Reset All Adjustment" OnClientClick="if(confirm('All the adjustment amounts will be lost?')) {valGroup='AEEmployee';return CheckValidation();}  else return false;"
                                            OnClick="btnResetAllAdjustment_Click" />
                                    </div>
                                </div>
                                <div style="clear: both" />
                            </div>
                        </div>
                    </td>
                    <td valign="top" style="padding-left: 5px">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <!-- panel-btns -->
                                <h3 class="panel-title">
                                    Retiring This Month</h3>
                            </div>
                            <div class="panel-body">
                                <ul class="nav nav-pills nav-stacked dashboardNotificatioin">
                                    <asp:Repeater runat="server" ID="rptTiisMonth">
                                        <ItemTemplate>
                                            <li <%# Eval("Other") %>><span style="">
                                                <asp:HyperLink runat="server" onclick='<%# "showDetails(" + Eval("ID") + ",this" + ")" %>'
                                                    EmpName='<%# Eval("Text") %>' NavigateUrl='javascript:void(0)' Text='<%# Eval("Text")%>' />
                                                , </span><span style="">
                                                    <%# Eval("Date", "{0:dd MMM yyyy}").ToString().Replace("12:00AM","")%>
                                                </span></li>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ul>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <ext:Window ID="windowAdjustment" runat="server" Title="Adjustment Details" Icon="Application"
        Height="300" Width="400" BodyPadding="5" Hidden="true" Modal="true">
        <Content>
            <table class="fieldTable">
                <tr>
                    <td valign="top">
                        <ext:NumberField ID="txtRegular" Width="180px" runat="server" FieldLabel="Regular"
                            LabelAlign="top" LabelSeparator="" />
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <ext:NumberField ID="txtRetirement" Width="180px" runat="server" FieldLabel="Retirement"
                            LabelAlign="top" LabelSeparator="" />
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <ext:TextField ID="txtNotes" Width="250px" runat="server" FieldLabel="Notes" LabelAlign="top"
                            LabelSeparator="" />
                    </td>
                </tr>
                <tr>
                    <td valign="bottom" colspan="2">
                        <div class="popupButtonDiv">
                            <ext:Button runat="server" AutoPostBack="true" OnClick="btnSaveLeaveAdjustment_Click"
                                Cls="btn btn-primary" ID="Button1" Text="<i></i>Save">
                                <Listeners>
                                    <Click Handler="Ext.getBody().mask('Loading...')">
                                    </Click>
                                </Listeners>
                            </ext:Button>
                            <div class="btnFlatOr">
                                or</div>
                            <ext:Button runat="server" StyleSpec="margin-left:10px" AutoPostBack="true" OnClick="btnResetLeaveAdjustment_Click"
                                Cls="btn btn-primary" ID="btnResetLeaveAdjustment" Text="<i></i>Reset Adjustment">
                                <Listeners>
                                    <Click Handler="Ext.getBody().mask('Loading...');">
                                    </Click>
                                </Listeners>
                            </ext:Button>
                            <div class="btnFlatOr">
                                or</div>
                            <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="LinkButton2" Cls="btnFlatLeftGap"
                                Text="<i></i>Cancel">
                                <Listeners>
                                    <Click Handler="#{windowAdjustment}.hide();">
                                    </Click>
                                </Listeners>
                            </ext:LinkButton>
                        </div>
                    </td>
                </tr>
            </table>
        </Content>
    </ext:Window>
</asp:Content>
