﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using Utils.Calendar;
using Web.Helper;
using Bll;

namespace Web.NewHR
{
    public partial class EmployeeCountReportDetails : BasePage
    {
        int branchid = -1;
        int depid = -1;
        int levelid = -1;
        int desigid = -1;

        protected void Page_Load(object sender, EventArgs e)
        {
            branchid = int.Parse(Request.QueryString["branchid"]);
            depid = int.Parse(Request.QueryString["depid"]);
            levelid = int.Parse(Request.QueryString["levelid"]);
            desigid = int.Parse(Request.QueryString["desigid"]);

            if (!X.IsAjaxRequest)
            {
                Initialise();

            }
        }

      

        public void Initialise()
        {

            string type = Request.QueryString["type"];
            string value = Request.QueryString["value"];

            details.InnerHtml += type + " : " + value;

            details.InnerHtml += " for ";

            if (branchid != -1)
                details.InnerHtml += " Branch " + BranchManager.GetBranchById(branchid).Name;

            if (depid != -1)
                details.InnerHtml += " Department " + DepartmentManager.GetDepartmentById(depid).Name;

            if (levelid != -1)
                details.InnerHtml += " Position " + NewPayrollManager.GetLevelById(levelid).Name;

            if (desigid != -1)
                details.InnerHtml += " Designation " + new CommonManager().GetDesignationById(desigid).Name;

            btnLoad_Change(null, null);
            
        }



        public void btnLoad_Change(object sender, DirectEventArgs e)
        {

            string type = Request.QueryString["type"];
            string value = Request.QueryString["value"];

            gridEmployee.GetStore().DataSource =
                EmployeeManager.GetEmployeeCountDetails(type,value,branchid,depid,levelid,desigid).ToList();
            gridEmployee.GetStore().DataBind();
        }


        public void btnExport_Click(object sender, EventArgs e)
        {

            string type = Request.QueryString["type"];
            string value = Request.QueryString["value"];

            List<Report_GetEmployeeCountDetailsResult> list =
                EmployeeManager.GetEmployeeCountDetails(type, value, branchid, depid, levelid, desigid).ToList();

            string title = "";
         
            if (hdnFilterValue.Text.ToLower() == "thismonth")
            {
                title = "";
            }




            ExcelHelper.ExportToExcel("Employee Count", list,
                  new List<string>() { },
                     new List<String>() { },
                     new Dictionary<string, string>() { { "Total", "Count" } },
                     new List<string>() { }
                     , new Dictionary<string, string>() { }
                     , new List<string> {  });
        }
     
    }
}