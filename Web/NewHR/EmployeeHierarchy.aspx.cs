﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Base;
using BLL;


namespace Web.NewHR
{
    public partial class EmployeeHierarchy : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.HumanResource;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest && !IsPostBack)
            {
                Initialize();            
            }
        }

        private void Initialize()
        {
            BindBranchCombo();
            BindDepartmentCombo();

            LoadEmployeeHierarchy();
        }

        private void BindBranchCombo()
        {
            List<Branch> listBranch = ListManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            Branch obj = new Branch() { BranchId = -1, Name = "All" };
            listBranch.Insert(0, obj);
            cmbBranch.Store[0].DataSource = listBranch;
            cmbBranch.Store[0].DataBind();
        }

        private void BindDepartmentCombo()
        {
            List<Department> list = new DepartmentManager().GetDepartmentsByCompany(SessionManager.CurrentCompanyId);
            Department obj = new Department() { DepartmentId = -1, Name = "All" };
            list.Insert(0, obj);
            cmbDepartment.Store[0].DataSource = list;
            cmbDepartment.Store[0].DataBind();
        }

        private NodeCollection LoadEmployeeHierarchy()
        {          

            Ext.Net.NodeCollection mainTreeNode = treeEmployeeHierarchy.Root;
            
            Ext.Net.Node rootTreeNode = new Ext.Net.Node();

            rootTreeNode.Text = "Root";
            rootTreeNode.Expanded = true;
            rootTreeNode.Leaf = false;
            rootTreeNode.NodeID = Guid.Empty.ToString();        

            int branchId = -1, departmentId = -1;

            if (!string.IsNullOrEmpty(cmbBranch.SelectedItem.Text))
                branchId = int.Parse(cmbBranch.SelectedItem.Value);

            if (!string.IsNullOrEmpty(cmbDepartment.SelectedItem.Text))
                departmentId = int.Parse(cmbDepartment.SelectedItem.Value);

            List<GetEmployeeHierarchyResult> list = NewHRManager.GetEmployeeHierarchyList(branchId, departmentId, cmbSortBy.Text.Trim());


            if (list.Count > 0)
            {
                List<int> groupIdList = (from groupLevel in list
                                         select groupLevel.LevelGroupId.Value).Distinct().ToList();


                foreach (var itemGroup in groupIdList)
                {
                    Node groupNode = new Node();
                    groupNode.NodeID = "G" + itemGroup.ToString();
                    groupNode.Text = NewHRManager.GetBLevelGroupById(itemGroup).Name;
                    groupNode.Expanded = false;

                    List<GetEmployeeHierarchyResult> listEmployeeCountInGroup = list.Where(x => x.LevelGroupId == itemGroup).ToList();
                    groupNode.CustomAttributes.Add(new ConfigItem { Name = "Branch", Value = listEmployeeCountInGroup.Count.ToString() });
                    groupNode.CustomAttributes.Add(new ConfigItem { Name = "CssClass", Value = "BlueCls" });

                    List<int> listBLevelId = (from blevel in list
                                              where blevel.LevelGroupId == itemGroup
                                              select blevel.LevelId.Value).Distinct().ToList();

                    foreach (var itemBLevel in listBLevelId)
                    {
                        Node blevelNode = new Node();
                        blevelNode.NodeID = "BL" + itemBLevel.ToString();
                        blevelNode.Text = NewHRManager.GetBLevelById(itemBLevel).Name;
                        blevelNode.Expanded = false;
                        blevelNode.Leaf = false;

                        List<GetEmployeeHierarchyResult> listEmployeeCountInBlevel = list.Where(x => x.LevelGroupId == itemGroup && x.LevelId == itemBLevel).ToList();
                        blevelNode.CustomAttributes.Add(new ConfigItem { Name = "Branch", Value = listEmployeeCountInBlevel.Count.ToString() });
                        blevelNode.CustomAttributes.Add(new ConfigItem { Name = "CssClass", Value = "MediumBlueCls" });

                        List<int> listEDesignationId = (from edesignation in list
                                                        where edesignation.LevelGroupId == itemGroup && edesignation.LevelId == itemBLevel
                                                        select edesignation.DesignationId.Value).Distinct().ToList();




                        foreach (var itemEDesignation in listEDesignationId)
                        {
                            Node eDesignationNode = new Node();
                            eDesignationNode.NodeID = "ED" + itemEDesignation.ToString();
                            eDesignationNode.Text = NewHRManager.GetDesignationById(itemEDesignation).Name;
                            eDesignationNode.Expanded = false;
                            eDesignationNode.Leaf = false;

                            List<GetEmployeeHierarchyResult> listEmployeeHierarchy = (from employeelist in list
                                                                                      where employeelist.LevelGroupId == itemGroup && employeelist.LevelId == itemBLevel && employeelist.DesignationId == itemEDesignation
                                                                                      select employeelist).ToList();
                            eDesignationNode.CustomAttributes.Add(new ConfigItem { Name = "Branch", Value = listEmployeeHierarchy.Count.ToString() });
                            eDesignationNode.CustomAttributes.Add(new ConfigItem { Name = "CssClass", Value = "LightBlue" });
                            

                            foreach (var itemEmployee in listEmployeeHierarchy)
                            {
                                Node empNode = new Node();


                                empNode.NodeID = "Emp" + itemEmployee.EmployeeId.ToString();
                                empNode.Text = itemEmployee.Name;

                                empNode.CustomAttributes.Add(new ConfigItem { Name = "Branch", Value = itemEmployee.Branch });
                                empNode.CustomAttributes.Add(new ConfigItem { Name = "Department", Value = itemEmployee.Department });
                                empNode.CustomAttributes.Add(new ConfigItem { Name = "JoinDate", Value = itemEmployee.JoinDate });
                                empNode.CustomAttributes.Add(new ConfigItem { Name = "RetirementEligibleDate", Value = itemEmployee.RetirementEligibleDate });

                                empNode.Leaf = true;
                                eDesignationNode.Children.Add(empNode);
                            }

                            blevelNode.Children.Add(eDesignationNode);

                        }

                        groupNode.Children.Add(blevelNode);
                    }
                    rootTreeNode.Children.Add(groupNode);
                }
                mainTreeNode.Add(rootTreeNode);

             
            }
            return mainTreeNode;
        }

        protected void cmbBranch_Change(object sender, DirectEventArgs e)
        {
            LoadEmployeeHierarchy();
            treeEmployeeHierarchy.Render();
            treeEmployeeHierarchy.AddCls("treepanelCls");
          
        }

        protected void cmbDepartment_Change(object sender, DirectEventArgs e)
        {
            LoadEmployeeHierarchy();
            treeEmployeeHierarchy.Render();
            treeEmployeeHierarchy.AddCls("treepanelCls");
        }

        protected void cmbSortBy_Change(object sender, DirectEventArgs e)
        {
            LoadEmployeeHierarchy();
            treeEmployeeHierarchy.Render();
            treeEmployeeHierarchy.AddCls("treepanelCls");
        }

    }
}