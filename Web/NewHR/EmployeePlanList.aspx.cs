﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Entity;
using BLL;
using System.IO;
using Utils;

namespace Web.NewHR
{
    public partial class EmployeePlanList : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.HumanResource;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        private void Clear(int employeeId)
        {
            txtDate.Text = "";

            EEmployee emp = EmployeeManager.GetEmployeeById(employeeId);
            bool hasPhoto = false;
            if (emp.HHumanResources.Count > 0)
            {
                if (!string.IsNullOrEmpty(emp.HHumanResources[0].UrlPhotoThumbnail))
                {
                    img.ImageUrl = Page.ResolveUrl(Config.UploadLocation + "/" + emp.HHumanResources[0].UrlPhotoThumbnail);
                    if (File.Exists(Server.MapPath(img.ImageUrl)))
                        hasPhoto = true;
                }
            }
            if (!hasPhoto)
            {
                img.ImageUrl = Page.ResolveUrl(Web.Helper.WebHelper.GetEmployeeNoImage(employeeId));

            }

            dfName.Text = emp.Name;
        }

        protected void btnView_Click(object sender, DirectEventArgs e)
        {
            int planId = int.Parse(hdnPlanId.Text);

            EmployeePlan obj = NewHRManager.GetEmployeePlanById(planId);
            if (obj != null)
            {
                Clear(obj.EmployeeId);

                txtDate.SelectedDate = obj.DateEng.Value;

                gridPlan.Store[0].DataSource = obj.EmployeePlanDetails;
                gridPlan.Store[0].DataBind();

                wPlan.Center();
                wPlan.Show();
            }
        }

        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            int totalRecords = 0;
            DateTime? startDate = null, endDate = null;

            int type = 0, employeeId = -1;
            bool all = false;

            type = TabPanel1.ActiveTabIndex + 1;

            if (TabPanel1.ActiveTabIndex == 5)
            {
                type = -1;
                all = true;

                if (txtStartDate.SelectedDate != new DateTime())
                {
                    startDate = txtStartDate.SelectedDate;

                    if (txtEndDate.SelectedDate == new DateTime())
                    {
                        NewMessage.ShowWarningMessage("To Date is required for date filter.");
                        txtEndDate.Focus();
                        return;
                    }
                }

                if (txtEndDate.SelectedDate != new DateTime())
                {
                    endDate = txtEndDate.SelectedDate;

                    if (txtStartDate.SelectedDate == new DateTime())
                    {
                        NewMessage.ShowWarningMessage("From Date is required for date filter.");
                        txtStartDate.Focus();
                        return;
                    }
                }
            }

            if (cmbSearch.SelectedItem != null && cmbSearch.SelectedItem.Value != null)
                employeeId = int.Parse(cmbSearch.SelectedItem.Value);

            int pageSize = int.Parse(cmbPageSize.SelectedItem.Value);
            List<GetEmpPlanListForAdminResult> list = NewHRManager.GetEmpPlanForAdmin(e.Start / pageSize, pageSize, employeeId, type, startDate, endDate, all);

            if (list.Count > 0)
                totalRecords = list[0].TotalRows.Value;

            e.Total = totalRecords;

            storePlan.DataSource = list;
            storePlan.DataBind();

        }

        [DirectMethod]
        public static string GetGrid(Dictionary<string, string> parameters)
        {
            int planId = int.Parse(parameters["id"]);

            EmployeePlan obj = NewHRManager.GetEmployeePlanById(planId);
            if (obj != null)
            {
                List<object> data = new List<object>();

                foreach (var item in obj.EmployeePlanDetails)
                {
                    data.Add(new { SN = item.SN, Description = item.Description, TotalTimeDate = item.TotalTime });
                }

                int count = obj.EmployeePlanDetails.Count();
                int height = 32 + 32 * count;

                GridPanel grid = new GridPanel
                {
                    Height = height,
                    EnableColumnHide = false,
                    Store = 
                { 
                    new Store 
                    { 
                        Model = {
                            new Model {
                                IDProperty = "SN",
                                Fields = 
                                {
                                    new ModelField("SN"),
                                    new ModelField("Description"),
                                    new ModelField("TotalTimeDate")
                                }
                            }
                        },
                        DataSource = data
                    }
                },
                    ColumnModel =
                    {
                        Columns = 
                    { 
                        new Column { Text = "SN", DataIndex = "SN", Width=140, Sortable = false, MenuDisabled = true },
                        new Column { Text = "Description", DataIndex = "Description", Width=600, Sortable = false, MenuDisabled = true, Wrap = true  },
                        new Column { Text = "Time", DataIndex = "TotalTimeDate", Width=200, Sortable = false, MenuDisabled = true  }
                    }
                    }
                };

                return ComponentLoader.ToConfig(grid);
            }
            else
                return "";
        }



    }
}