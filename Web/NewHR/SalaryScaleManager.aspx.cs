﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using Utils.Calendar;

namespace Web.NewHR
{
    public partial class SalaryScaleManager : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.AdministratorTools;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }

            
        }

        public void Initialise()
        {
            //cmbGroup.Store[0].DataSource = NewPayrollManager.GetLevelGroup();
            //cmbGroup.Store[0].DataBind();

            List<BLevelDate> dateList = NewPayrollManager.GetSalaryChangeDateList();
            cmbSalaryChangeDate.Store[0].DataSource = dateList;
            cmbSalaryChangeDate.Store[0].DataBind();

            if (dateList.Count > 0)
            {
                ExtControlHelper.ComboBoxSetSelected(dateList[dateList.Count - 1].DateID.ToString(), cmbSalaryChangeDate);
                btnGenerateScale_Click(null, null);
            }

            
        }
        protected void btnChangeScale_Click(object sender, DirectEventArgs e)
        {
            string selectedChildItems = e.ExtraParams["ScaleList"];
            List<SalaryScale> selectedItems = JSON.Deserialize<List<SalaryScale>>(selectedChildItems);

            BLevelDate date = new BLevelDate();
            bool isInsert = true;
            if (!string.IsNullOrEmpty(hdn.Text))
            {
                isInsert = false;
                date = NewPayrollManager.GetLevelDateById(int.Parse(hdn.Text));

                BLevelDate lastDate = NewPayrollManager.GetLastLevelDate();
                if (lastDate.DateID != date.DateID)
                {
                    NewMessage.ShowWarningMessage("Only last salary change is editable.");
                    return;
                }

                if (date.EffectiveFromEng != null)
                {
                    if (string.IsNullOrEmpty(calEffectiveFrom.Text.Trim()))
                    {
                        NewMessage.ShowWarningMessage("Effective date must be provided.");
                        return;
                    }
                }

                date.Name = txtName.Text.Trim();
                if (date.EffectiveFromEng != null)
                {
                    date.EffectiveFrom = calEffectiveFrom.Text.Trim();
                    date.EffectiveFromEng = GetEngDate(date.EffectiveFrom);
                }
            }
            else
            {
                if (string.IsNullOrEmpty(calEffectiveFrom.Text.Trim()))
                {
                    NewMessage.ShowWarningMessage("Effective date must be provided.");
                    return;
                }

                date.Name = txtName.Text.Trim();
                date.EffectiveFrom = calEffectiveFrom.Text.Trim();
                date.EffectiveFromEng = GetEngDate(date.EffectiveFrom);

            }

            if (date.EffectiveFromEng != null)
            {
                CustomDate dd = CustomDate.GetCustomDateFromString(date.EffectiveFrom, IsEnglish);
                if (dd.Day != 1)
                {
                    NewMessage.ShowWarningMessage("Effective date must be the first day of the month.");
                    return;
                }

                BLevelDate prevDate = NewPayrollManager.GetPrevLevelDate(date.DateID);
                if (prevDate != null && prevDate.EffectiveFromEng >= date.EffectiveFromEng)
                {
                    NewMessage.ShowWarningMessage("Effective date must be greater than previous salary change effective date.");
                    return;
                }
            }


            NewPayrollManager.ChangeSalaryScale(selectedItems, date, isInsert);
            NewMessage.ShowNormalMessage("Salary scale saved.", "forcePostBack");
            WindowLevel.Hide();

            btnGenerateScale_Click(null, null);

            //X.Js.Call("forcePostBack");
        }
       
        protected void cmbGroup_Change(object sender, DirectEventArgs e)
        {
            //cmbLevel.Store[0].DataSource = NewPayrollManager.GetLevels(int.Parse(cmbGroup.SelectedItem.Value));
            //cmbLevel.Store[0].DataBind();

        }

        void ClearFields()
        {

        }
        protected void btnSaveUpdate_Click(object sender, DirectEventArgs e)
        {
        //    Page.Validate("SaveUpdate");

        //    if (Page.IsValid)
        //    {

        //        BLevelRate level = new BLevelRate();
        //        level.LevelID = int.Parse(cmbLevel.SelectedItem.Value);
        //        level.PayScale = (decimal)txtPayScale.Number;
        //        ;
        //        level.NoOfStepsGrade = (int)txtNoOfSteps.Number;
        //        level.StepGradeRate = (decimal)txtStepRate.Number; ;


        //        NewPayrollManager.UpdateLevelScale(level);

        //        Response.Redirect("SalaryScaleManager.aspx");
            //}
        }
        protected void btnChangeSalaryScale_Click(object sender, DirectEventArgs e)
        {
            ClearFields();
            WindowLevel.Show();

            btnChangeScale.Text = "Update";
            BLevelDate date = NewPayrollManager.GetLastSalaryScaleDate();
            hdn.Text = cmbSalaryChangeDate.SelectedItem.Value;
            txtName.Text = date.Name;

            if (date.EffectiveFromEng == null)
                calEffectiveFrom.Hide();
            else
            {
                calEffectiveFrom.Show();
                calEffectiveFrom.Text = date.EffectiveFrom;
            }
            List<SalaryScale> list = NewPayrollManager.GetSalaryScaleList(int.Parse(hdn.Text));

            gridSalaryScale.Store[0].DataSource = list;
            gridSalaryScale.Store[0].DataBind();
        }

        protected void btnAddSalaryScale_Click(object sender, DirectEventArgs e)
        {
            ClearFields();
            WindowLevel.Show();
            btnChangeScale.Text = "Save";
            hdn.Text = "";
            BLevelDate date = NewPayrollManager.GetLastSalaryScaleDate();

            txtName.Text = "";
            calEffectiveFrom.Text = "";
            calEffectiveFrom.Show();

            List<SalaryScale> list = NewPayrollManager.GetSalaryScaleList(date.DateID);

            gridSalaryScale.Store[0].DataSource = list;
            gridSalaryScale.Store[0].DataBind();
        }
        protected void cmbLevel_Change(object sender, DirectEventArgs e)
        {
            //BLevel level = NewPayrollManager.GetLevelById(int.Parse(cmbLevel.SelectedItem.Value));

            //lblCode.Text = "Code : " + level.Code;
            //txtPayScale.Number =(double) level.PayScale;
            //txtNoOfSteps.Number = level.NoOfSteps;
            //txtStepRate.Number = (double)level.StepRate;

            //if (level.IncomeId == 0)
            //    btnSaveUpdate.Text = "Save";
            //else
            //    btnSaveUpdate.Text = "Update";
        }

        protected void btnGenerateScale_Click(object sender, DirectEventArgs e)
        {
            List<SalaryScale> list = NewPayrollManager.GetSalaryScaleList(int.Parse(cmbSalaryChangeDate.SelectedItem.Value));

            int columnWidht = 100;
            // Create Columns for the Site 
            Model SiteModel = (this.GridPayScale.Store[0].Model[0] as Model);
            string columnId = "Item_{0}";

            int? maxNoOfSteps = NewPayrollManager.GetMaxGradeStepForAllDateForGrid();
            if (maxNoOfSteps == null)
                maxNoOfSteps = 1;


            bool allowDecimalInGrade = NewPayrollManager.AllowDecimalInGrade();

            //foreach (Branch s in siteList)
            int k = 1;
            for (int i = 1; i <= maxNoOfSteps; i++)
            {
                string gridIndexId = string.Format(columnId, k);
                ModelField field = new ModelField(gridIndexId, ModelFieldType.String);
                field.UseNull = true;
                SiteModel.Fields.Add(field);
                GridPayScale.ColumnModel.Columns.Add(new Column { DataIndex = gridIndexId, Align = Alignment.Center, Text = i.ToString(), Width = columnWidht });

                if (allowDecimalInGrade)
                {
                    k++;
                    string gridIndexId1 = string.Format(columnId, k);
                    ModelField field1 = new ModelField(gridIndexId1, ModelFieldType.String);
                    field1.UseNull = true;
                    SiteModel.Fields.Add(field1);
                    GridPayScale.ColumnModel.Columns.Add(new Column { DataIndex = gridIndexId1, Align = Alignment.Center, Text = (i + 0.5).ToString(), Width = columnWidht });
                }
                k++;
            }



            object[] data = new object[list.Count];
            int row = 0;
            foreach (SalaryScale entity in list)
            {
                //object[] rowData = new object[maxNoOfSteps.Value + 5]; //except site there are other more three column(item name,total,unit of measure)

                object[] rowData;

                if (allowDecimalInGrade)
                    rowData = new object[(maxNoOfSteps.Value * 2)  + 5];
                else
                    rowData = new object[maxNoOfSteps.Value + 5];

                rowData[0] = entity.GroupLevel;
                rowData[1] = NewPayrollManager.GetLevelById(entity.LevelId).Name;
                rowData[2] = entity.LevelCode;
                rowData[3] = entity.PayScale;
                rowData[4] =entity.StepRate;

                int index = 5;

                //Value of Item in each Step

                double increment = 0;
                double noOfSteps = 0;

                if (allowDecimalInGrade)
                {
                    increment = .5;
                    noOfSteps = entity.NoOfSteps + 0.5;
                }
                else
                {
                    increment = 1;
                    noOfSteps = entity.NoOfSteps;
                }

                for (double i = 1; i <= noOfSteps; i = i + increment)
                {

                    decimal? amount = entity.GetAmountForDecimialInGrade(entity.LevelGroupId, entity.LevelId, i);              
               
                    if (amount != null)
                        rowData[index++] = GetCurrency(amount);
                    else
                        rowData[index++] = "";

                }
                

                //Add a Row
                data[row++] = rowData;
            }

            GridPayScale.Store[0].DataSource = data;
            GridPayScale.Store[0].DataBind();
        }

    }
}