﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Entity;
using BLL;
using System.IO;
using Utils;
using System.Text;
using Utils.Helper;
using Ionic;
using Ionic.Zip;

namespace Web.NewHR
{
    public partial class LogList : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.AdministratorTools;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            List<LogFiles> list = new List<LogFiles>();

            int i = 0, totalLogs = 0;

            foreach (string file in Directory.GetFiles(CommonManager.GetLogFileLocation()))
            {
                LogFiles obj = new LogFiles();
                FileInfo objFileInfo = new FileInfo(file);
                
                obj.FileName = objFileInfo.Name;
                if (obj.FileName == "blank.txt")
                    continue;

                obj.CreatedDate = objFileInfo.CreationTime;
                obj.CreatedDateString = obj.CreatedDate.ToString("MMM dd yyyy hh:mm:ss tt");
                obj.IsChecked = false;
                obj.Id = ++i;

                list.Add(obj);
            }

            totalLogs = list.Count;
            int pageSize = int.Parse(cmbPageSize.SelectedItem.Value);

            list = list.OrderByDescending(x => x.CreatedDate).ToList();
            list = list.Skip(e.Start).Take(pageSize).ToList();

            gridLog.Store[0].DataSource = list;
            gridLog.Store[0].DataBind();
            e.Total = totalLogs;

        }

        protected void btnView_Click(object sender, DirectEventArgs e)
        {
            string filename = hdnFileName.Text;
            
            txtLogDetail.Text = FileHelper.GetAbsPath("~/App_Data/logs/" + filename);
            WLogDetail.Center();
            WLogDetail.Show();
        }

        protected void btnDownload_Click(object sender, DirectEventArgs e)
        {
            hdnFileNames.Text = "";
            string gridItemsJson = e.ExtraParams["gridItems"];
            List<LogFiles> list = JSON.Deserialize<List<LogFiles>>(gridItemsJson);
            list = list.Where(x => x.IsChecked == true).ToList();

            foreach (var item in list)
            {
                hdnFileNames.Text += item.FileName + ",";
            }

            hdnFileNames.Text = hdnFileNames.Text.TrimEnd(',');

            X.Js.Call("DownloadFn");
        }

        protected void btnDownloadHelper_Click(object sender, EventArgs e)
        {
            Response.Clear();
            Response.ContentType = "application/zip";
            Response.AddHeader("content-disposition", "filename=" + "LogFiles.zip");

            string[] split = hdnFileNames.Text.Split(',');

            using (ZipFile zip = new ZipFile())
            {
                foreach (var fileName in split)
                {
                    zip.AddFile(HttpContext.Current.Server.MapPath(@"~/App_Data/logs/") + fileName, "");
                }
                zip.Save(Response.OutputStream);
            }


        }

    }

    public class LogFiles
    {
        public int Id { get; set; }
        public string FileName { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedDateString { get; set; }
        public bool IsChecked { get; set; }
    }
}