﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Entity;
using BLL;
using BLL.BO;
using Utils.Helper;
using Utils.Calendar;
using System.Drawing;

namespace Web.NewHR
{
    public partial class PersonalDetail : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.HumanResource;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                cmbGender.FieldLabel = CommonManager.GetGenderName + " *";
                Initialise();

            }
        }
        public int GetEmployeeId()
        {
            return int.Parse(Request.QueryString["Id"]);
        }

        private void SetForNextPreviousEmployee()
        {
            int currentEmpId = UrlHelper.GetIdFromQueryString("ID");
            if (currentEmpId != 0)
            {
                int nextEmpId = 0, previousEmpId = 0, firstEmpId = 0, lastEmpId = 0;
                string nextEmpName = "", previousEmpName = "";

                EmployeeManager.SetNextPreviousEmployeeId(currentEmpId, ref previousEmpId, ref nextEmpName, ref  nextEmpId, ref previousEmpName, ref firstEmpId, ref lastEmpId);

                if (previousEmpId == 0)
                    btnPrevious.ForeColor = Color.LightGray;
                else
                {
                    btnPrevious.NavigateUrl = "PersonalDetail.aspx?Id=" + previousEmpId;
                    btnPrevious.ToolTip = string.Format("{0}", previousEmpName);
                }

                if (nextEmpId == 0)
                    btnNextEmployee.ForeColor = Color.LightGray;
                else
                {
                    btnNextEmployee.NavigateUrl = "PersonalDetail.aspx?Id=" + nextEmpId;
                    btnNextEmployee.ToolTip = string.Format("{0}", nextEmpName);
                }

                if (firstEmpId == 0 || currentEmpId == firstEmpId)
                    btnFirst.ForeColor = Color.LightGray;
                else
                {
                    btnFirst.NavigateUrl = "PersonalDetail.aspx?Id=" + firstEmpId;
                }

                if (lastEmpId == 0 || currentEmpId == lastEmpId)
                    btnLast.ForeColor = Color.LightGray;
                else
                    btnLast.NavigateUrl = "PersonalDetail.aspx?Id=" + lastEmpId;
            }
            else
            {
                //pnlNavigation.Visible = false;
            }
        }

        public void Initialise()
        {
            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.Care)
            {
                trExtendProbation1.Visible = true;
                trExtendProbation2.Visible = true;
            }

            if (CommonManager.CompanySetting.WhichCompany != WhichCompany.NIBL)
                reqdRef.Enabled = false;

            if(CommonManager.IsServiceHistoryEnabled)
                btnPromote.Visible=false;

            if (CommonManager.IsUnitEnabled)
            {
                cmbUnit.Show();
                cmbUnit.Store[0].DataSource = CommonManager.GetAllUnitList();
                cmbUnit.Store[0].DataBind();
            }

            JavascriptHelper.AttachPopUpCode(Page, "changeProbation", "../CP/ChangeEmployeeProbation.aspx", 480, 350);

            cmbLevel.FieldLabel = CommonManager.GetLevelName + " *";
            valLevel.ErrorMessage = "Please select employee " + CommonManager.GetLevelName + ".";

            JavascriptHelper.AttachPopUpCode(Page, "promote", "../CP/ChangeEmployeeStatus.aspx", 480, 400);

            cmbGroup.Store[0].DataSource = NewPayrollManager.GetLevelGroup();
            cmbGroup.Store[0].DataBind();

            if (CommonManager.CompanySetting.HasLevelGradeSalary == false)
            {
                tdGroup.Visible = false;
                tdGroup.Style["display"] = "none";
                tdStep.Style["display"] = "none";

                cmbLevel.Store[0].DataSource = NewPayrollManager.GetAllParentLevels();
                cmbLevel.Store[0].DataBind();

                chkExcludeNonMatrixSalary.Hide();
            }

            cmbBranch.Store[0].DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            cmbBranch.Store[0].DataBind();

            List<FunctionalTitle> functionTitles = ListManager.GetFunctionalTitleType();
            if (functionTitles.Count <= 0)
                cmbFunctionTitle.Hide();
            else
            {
                cmbFunctionTitle.Store[0].DataSource = functionTitles;
                cmbFunctionTitle.Store[0].DataBind();
            }
            //cmbTypes.Store[0].DataSource = CommonManager.GetServieEventTypes();
            //cmbTypes.Store[0].DataBind();

            //cmbHireMethod.Store[0].DataSource = ExtControlHelper.GetTextValues(typeof(HireMethodEnum));
            //cmbHireMethod.Store[0].DataBind();

            List<KeyValue> list = new JobStatus().GetMembers();
            list.RemoveAt(0);
            cmbStatus.Store[0].DataSource = list;
            cmbStatus.Store[0].DataBind();

            cmbStep.Hidden = !CommonManager.CompanySetting.HasLevelGradeSalary;
           

            cmbTitle.Store[0].DataSource = new Title().GetMembers();
            cmbTitle.Store[0].DataBind();

            //cmbGender.Store[0].DataSource = new Gender().GetMembers();
            //cmbGender.Store[0].DataBind();

            cmbMarried.Store[0].DataSource = new MaritalStatus().GetMembers();
            cmbMarried.Store[0].DataBind();

            if (CommonManager.Setting.EnableWeeklyLikePay != null && CommonManager.Setting.EnableWeeklyLikePay.Value)
            {
                List<PayGroup> payList = PeriodicPayManager.GetPayGroup(); ;
                payList.Insert(0, new PayGroup { PayGroupID = 0, Name = "Regular Pay" });
                cmbPayGroup.Store[0].DataSource = payList;
                cmbPayGroup.Store[0].DataBind();
                cmbPayGroup.Show();
            }

            //cmbReligion.Store[0].DataSource = new Religion().GetMembers();
            //cmbReligion.Store[0].DataBind();

            cmbReligion.Store[0].DataSource = CommonManager.GetReligionList();
            cmbReligion.Store[0].DataBind();
            
            CommonManager comManager = new CommonManager();
            cmbEthnicity.Store[0].DataSource = comManager.GetAllCastes();
            cmbEthnicity.Store[0].DataBind();

            List<HolidayGroup> groupList = CommonManager.GetAllHolidayGroups();
            cmbHolidayGroup.Store[0].DataSource = groupList;
            cmbHolidayGroup.Store[0].DataBind();
            if (groupList.Count <= 0)
                cmbHolidayGroup.Hide();

            cmbELocation.Store[0].DataSource = ListManager.GetELocationList();
            cmbELocation.Store[0].DataBind();

            cmbGrade.Store[0].DataSource = new CommonManager().GetAllGrades();
            cmbGrade.Store[0].DataBind();

            cmbSubDepartment.Store[0].DataSource = new DepartmentManager().GetSubDepartmentsByCompany(SessionManager.CurrentCompanyId);
            cmbSubDepartment.Store[0].DataBind();

            cmbHireMethod.Store[0].DataSource = CommonManager.GetHireMethodList();
            cmbHireMethod.Store[0].DataBind();

            if (CommonManager.Setting.ShowGradeStep != null && CommonManager.Setting.ShowGradeStep == true)
            {
                cmbGrade.Hidden = false;
                
            }
            else
            {
                cmbGrade.Hidden = true;
                
            }

            RegisterEmployeeSavedMsg();

            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                navigation.Style["display"] = "";

                SetForNextPreviousEmployee();

                LoadEditData();
                LoadEmploymentDetails();

                //RetirementDisplayValidation();

                EmployeeManager empMgr = new EmployeeManager();

                empProbationList.InnerHtml = empMgr.GetProbationList(GetEmployeeId());

                empStatusList.InnerHtml = empMgr.GetCurrentStatus(GetEmployeeId());
                X.Js.AddScript(string.Format("EmployeeId=" + GetEmployeeId() + ";"));
                ECurrentStatus lastStatus = empMgr.GetCurrentLastStatus(GetEmployeeId());

                CustomDate date = CustomDate.GetCustomDateFromString(lastStatus.FromDate,
                                                                     SessionManager.CurrentCompany.IsEnglishDate);

                date = date.IncrementByOneDay();

                bool isFirstStatusOnly = (empMgr.GetStatusCount(GetEmployeeId()) == 1);

                Page.ClientScript.RegisterClientScriptBlock(this.GetType(),
                                                            "empStatus",
                                                            "var isFirst = " + (isFirstStatusOnly ? "true" : "false") + ";",

                                                            true);
            }
            else
            {
                //tableRetirement2.Style["display"] = "none";
                if (CommonManager.Setting.IsEINEditable != null && CommonManager.Setting.IsEINEditable.Value)
                {
                    txtEIN.Show();
                    txtEIN.Disabled = false;

                    if (CommonManager.CompanySetting.WhichCompany != WhichCompany.Care)
                        txtEIN.Text = EmployeeManager.GetNextEIN().ToString();
                }
                divTitle.Style["margin-left"] = "25px";
                EmployeeWizard1.Visible = false;
                divEmployeeDetails.Visible = false;
            }
        }


        public void cmbStatus_Change(object sender, DirectEventArgs e)
        {
            // if no level/grade in the company then don't change assigned level
            if (CommonManager.CompanySetting.HasLevelGradeSalary == false)
                return;

           
            cmbStep.ClearValue();


            int status = int.Parse(cmbStatus.SelectedItem.Value);
            if (EmployeeManager.HasLevelGradeForStatus(status,chkExcludeNonMatrixSalary.Checked)==false)
            {
                             
                cmbStep.Hide();

                cmbStep.Store[0].DataSource = new List<TextValue> { new TextValue { Value = "0", Text = "None" } };
                cmbStep.Store[0].DataBind();
                cmbStep.SetValue("0");


            }
            else
            {
                cmbStep.Show();
            }


        }

        /// <summary>
        /// Displayes emp saved msg, as the page is redirected to itself after save
        /// </summary>
        void RegisterEmployeeSavedMsg()
        {
            if (SessionManager.EmployeeSaved)
            {
                //JavascriptHelper.DisplayClientMsgWithTimeout("Employee information saved.", Page);
                SetMessage(lblMsg, Resources.Messages.EmployeeInformationSaved);
                SessionManager.EmployeeSaved = false;
            }
        }

        protected void txtDOBEng_Change(object sender, DirectEventArgs e)
        {
            if (txtDOB.SelectedDate == DateTime.MinValue)
            {
                txtAge.Text = "";
                txtDOBNep.Text = "";
                return;
            }

            if (IsEnglish == false)
            {
                if (!string.IsNullOrEmpty(txtDOB.Text.Trim()))
                {
                    CustomDate cd = CustomDate.GetCustomDateFromString(txtDOB.SelectedDate.ToString("yyyy/MM/dd"), true);
                    txtDOBNep.Text = CustomDate.ConvertEngToNep(cd).ToString();
                }
            }

            if (!string.IsNullOrEmpty(txtDOB.Text.Trim()))
                txtAge.Text = NewHelper.GetElapsedTime(txtDOB.SelectedDate, DateTime.Now);
        }


        protected void txtDOBNep_Change(object sender, DirectEventArgs e)
        {
            if (IsEnglish == false)
            {
                if (!string.IsNullOrEmpty(txtDOBNep.Text.Trim()))
                {
                    DateTime DBOEng = GetEngDate(txtDOBNep.Text);
                    txtDOB.Text = DBOEng.ToString();

                }
            }
            if (!string.IsNullOrEmpty(txtDOB.Text.Trim()))
                txtAge.Text = NewHelper.GetElapsedTime(txtDOB.SelectedDate, DateTime.Now);
        }


        //protected void RetirementDisplayValidation()
        //{

        //    PayrollPeriod lastPayrollDate = CommonManager.RetiredValidPayrollPeriod(GetEmployeeId());

        //    if (EmployeeManager.IsRetiredOrResignedIncludedInCalculation(GetEmployeeId()))
        //    {
        //        chkRetirement.Disable();
        //        calRetirementDate.Disable();
        //        cmbTypes.Disable();
        //    }
        //    else if (lastPayrollDate == null)
        //    {
        //        chkRetirement.Disable();
        //        calRetirementDate.Disable();
        //        cmbTypes.Disable();
        //        //NewMessage.ShowNormalPopup("Please define a payroll period first, partial saved payroll period not exists.");
        //    }
        //    else if (CalculationManager.IsCalculationSavedForEmployee(lastPayrollDate.PayrollPeriodId, GetEmployeeId()))
        //    {
        //        chkRetirement.Disable();
        //        calRetirementDate.Disable();
        //        cmbTypes.Disable();
        //        NewMessage.ShowNormalPopup(Resources.Messages.NotEdiableDuringPayrollGeneration);
        //    }
           
        //}

        //protected bool RetirementSaveValidation()
        //{

        //    PayrollPeriod lastPayrollDate = CommonManager.RetiredValidPayrollPeriod(GetEmployeeId());

        //    if (EmployeeManager.IsRetiredOrResignedIncludedInCalculation(GetEmployeeId()))
        //    {
        //        if(chkRetirement.Checked==false ||
        //            string.IsNullOrEmpty(calRetirementDate.Text.Trim()) ||
        //            GetEngDate(calRetirementDate.Text.Trim()) != EmployeeManager.GetEmployeeById(GetEmployeeId()).EHumanResources[0].DateOfRetirementEng)
        //        {
                    
        //            NewMessage.ShowWarningMessage("Retirement can not be changed or removed after the effect of retirement in salary.");
        //            return false;
        //        }
        //    }
           
        //    else if (lastPayrollDate != null && CalculationManager.IsCalculationSavedForEmployee(lastPayrollDate.PayrollPeriodId, GetEmployeeId()))
        //    {
        //        //DisableRetiredResigned();
        //        //chkIsResigned.ToolTip = calDateOfResignation.ToolTip = Resources.Messages.NotEdiableDuringPayrollGeneration;
        //        //chkIsRetired.ToolTip = calDateOfRetirement.ToolTip = Resources.Messages.NotEdiableDuringPayrollGeneration;
        //    }
        //    else if(chkRetirement.Checked)
        //    {
        //        CustomDate date = CustomDate.GetCustomDateFromString(lastPayrollDate.StartDate, IsEnglish);

        //        string jsCode = "";



        //        if (IsEnglish
        //            || SessionManager.User.IsEnglishPreferred == null
        //            || (SessionManager.User.IsEnglishPreferred != null &&
        //                SessionManager.User.IsEnglishPreferred.Value == SessionManager.CurrentCompany.IsEnglishDate)
        //            )
        //        {
        //            date = date.DecrementByOneDay();
        //        }
        //        // Nepali calendar but english date
        //        else
        //        {
        //            date = CustomDate.GetCustomDateFromString(lastPayrollDate.StartDate, false);
        //            date = CustomDate.ConvertNepToEng(date);
        //            date = date.DecrementByOneDay();
        //        }


        //        DateTime retDate = GetEngDate(calRetirementDate.Text.Trim()).Date;

        //        //if(retDate < date.EnglishDate)
        //        //{
        //        //    NewMessage.ShowWarningMessage("Retirement can not exceed " + date.ToString() + " date.");
        //        //    return false;
        //        //}
                


        //    }



        //    //if (chkRetirement.Checked)
        //    //{
        //    //    if (string.IsNullOrEmpty(calRetirementDate.Text.Trim()))
        //    //    {
        //    //        NewMessage.ShowWarningMessage("Retirement date is required.");
        //    //        return false;
        //    //    }
        //    //    if (cmbTypes.SelectedItem == null || cmbTypes.SelectedItem.Value == null)
        //    //    {
        //    //        NewMessage.ShowWarningMessage("Retirement type is required.");
        //    //        return false;
        //    //    }


              
        //    //}



        //    return true;
        //}

        protected void ButtonNext_Click(object sender, DirectEventArgs e)
        {
            EEmployee eEmployee = new EEmployee();

            //if (!string.IsNullOrEmpty(Request.QueryString["ID"]) && RetirementSaveValidation() == false)
            //    return;

            eEmployee = GetEmployeePersonalDetails();

            bool isInsert;



            if (CommonManager.IsUnitEnabled && eEmployee.UnitId == null)
            {
                NewMessage.ShowWarningMessage("Unit is required.");
                return;
            }

            if (string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                isInsert = true;

                if (CommonManager.Setting.IsEINEditable != null && CommonManager.Setting.IsEINEditable.Value
                    && string.IsNullOrEmpty(txtEIN.Text.Trim()))
                {
                    NewMessage.ShowWarningMessage("EIN is required.");
                    return;
                }

                if (CommonManager.Setting.IsEINEditable != null && CommonManager.Setting.IsEINEditable.Value)
                {
                    eEmployee.EmployeeId = int.Parse(txtEIN.Text.Trim());
                }
            }
            else
            {
                isInsert = false;
                eEmployee.EmployeeId = int.Parse(Request.QueryString["ID"]);
            }

            // Validations
            if (cmbTitle.SelectedItem == null || cmbTitle.SelectedItem.Value == null)
            {
                NewMessage.ShowWarningMessage("Title is required.");
                return;
            }

            Status respStatus;

            respStatus = AddEmployeementDetails(eEmployee, isInsert);

            if (respStatus.IsSuccess == false)
            {
                SetWarning(lblMsg, respStatus.ErrorMessage);
                X.Js.AddScript("window.scrollTo(0,0);");
                return;
            }

            respStatus = NewHRManager.InsertUpdateNewEmployee(eEmployee, isInsert);
            if (respStatus.IsSuccess)
            {
                if (isInsert)
                {
                    SessionManager.EmployeeSaved = true;
                    Response.Redirect("PersonalDetail.aspx?id=" + eEmployee.EmployeeId);

                }
                else
                {
                    SetMessage(lblMsg, Resources.Messages.EmployeeInformationUpdatedMsg);
                    //NewMessage.ShowNormalPopup(Resources.Messages.EmployeeInformationUpdatedMsg);

                }
            }
            else
            {
                SetWarning(lblMsg, respStatus.ErrorMessage);
                //NewMessage.ShowWarningPopup(respStatus.ErrorMessage);
            }
            X.Js.AddScript("window.scrollTo(0,0);");
        }

        private EEmployee GetEmployeePersonalDetails()
        {
            EEmployee eEmployee = new EEmployee();
            HHumanResource hHR = new HHumanResource();
            EHumanResource hr = new EHumanResource();

            if (cmbTitle.SelectedItem != null)
                eEmployee.Title = cmbTitle.SelectedItem.Value;

            eEmployee.Gender = int.Parse(cmbGender.SelectedItem.Value);
            eEmployee.FirstName = txtFirstNameEng.Text.Trim();
            eEmployee.MiddleName = txtMiddleNameEng.Text.Trim();
            eEmployee.LastName = txtLastNameEng.Text.Trim();
            eEmployee.Name =
                EEmployee.GetCombinedName(eEmployee.FirstName, eEmployee.MiddleName, eEmployee.LastName).Trim();
            eEmployee.NameNepali = txtNameNep.Text.Trim();
            eEmployee.FatherName = txtFatherName.Text.Trim();
            eEmployee.MotherName = txtMotherName.Text.Trim();

            eEmployee.MotherTongue = txtMotherTongue.Text.Trim();
            if (!string.IsNullOrEmpty(txtChildren.Text))
                eEmployee.Children = int.Parse(txtChildren.Text.Trim());

            if (cmbMarried.SelectedItem != null)
                eEmployee.MaritalStatus = cmbMarried.SelectedItem.Value;

            if (cmbUnit.SelectedItem != null && cmbUnit.SelectedItem.Value != null)
                eEmployee.UnitId = int.Parse(cmbUnit.SelectedItem.Value);

            if (!string.IsNullOrEmpty(txtDOB.Text))
            {
                eEmployee.DateOfBirthEng = txtDOB.SelectedDate;
                eEmployee.DateOfBirth = BLL.BaseBiz.GetAppropriateDate(eEmployee.DateOfBirthEng.Value);
                eEmployee.IsEnglishDOB = IsEnglish;    


            }
            eEmployee.PlaceOfBirth = txtPOB.Text;

            if (cmbReligion.SelectedItem != null && cmbReligion.SelectedItem.Value != null)
                hHR.Religion = cmbReligion.SelectedItem.Value;

            if (cmbFunctionTitle.SelectedItem != null && cmbFunctionTitle.SelectedItem.Value != null)
                eEmployee.FunctionalTitleId = int.Parse(cmbFunctionTitle.SelectedItem.Value);


            if (cmbEthnicity.SelectedItem != null && cmbEthnicity.SelectedItem.Value != null)
                hHR.CasteId = int.Parse(cmbEthnicity.SelectedItem.Value);

            if (cmbHolidayGroup.SelectedItem != null && cmbHolidayGroup.SelectedItem.Value != null)
                hHR.HolidayGroupId = int.Parse(cmbHolidayGroup.SelectedItem.Value);

            if (!string.IsNullOrEmpty(txtHobby.Text.Trim()))
                eEmployee.Hobby = txtHobby.Text.Trim();

            if (txtMarriageAnniversary.SelectedValue!= null)
            {
                eEmployee.MarriageAniversaryEng = DateTime.Parse(txtMarriageAnniversary.Text.Trim());
                eEmployee.MarriageAniversary = BLL.BaseBiz.GetAppropriateDate(eEmployee.MarriageAniversaryEng.Value);
            }

            //if (chkRetirement.Checked)
            //{
            //    hr.DateOfRetirement = calRetirementDate.Text.Trim();
            //    hr.DateOfRetirementEng = GetEngDate(hr.DateOfRetirement);
            //    eEmployee.IsRetired = true;                
            //    hr.RetirementType = int.Parse(cmbTypes.SelectedItem.Value);
            //}

            eEmployee.HHumanResources.Add(hHR);
            eEmployee.EHumanResources.Add(hr);

            return eEmployee;
        }

        public void LoadEditData()
        {
            EEmployee eEmp = new EEmployee();
            eEmp = NewHRManager.GetEmployeeByID(int.Parse(Request.QueryString["ID"]));
            if (eEmp != null)
            {
                if (eEmp.IsRetired != null && eEmp.IsRetired.Value)
                {
                    if (eEmp.EHumanResources[0].DateOfRetirementEng != null && eEmp.EHumanResources[0].DateOfRetirementEng < DateTime.Now)
                    {
                        spanRetiredDate.InnerHtml = "Retired : " + eEmp.EHumanResources[0].DateOfRetirement + " (" + eEmp.EHumanResources[0].DateOfRetirementEng.Value.ToLongDateString() + ")";
                    }
                    else //if (eEmp.EHumanResources[0].DateOfRetirementEng != null && eEmp.EHumanResources[0].DateOfRetirementEng >= DateTime.Now)
                    {
                        spanRetiredDate.InnerHtml = "Retiring in : " + eEmp.EHumanResources[0].DateOfRetirement + " (" + eEmp.EHumanResources[0].DateOfRetirementEng.Value.ToLongDateString() + ")";
                    }
                }
                if (eEmp.IsResigned != null && eEmp.IsResigned.Value)
                {
                    if (eEmp.EHumanResources[0].DateOfResignationEng != null && eEmp.EHumanResources[0].DateOfResignationEng < DateTime.Now)
                    {
                        spanRetiredDate.InnerHtml = "Retired : " + eEmp.EHumanResources[0].DateOfResignation + " (" + eEmp.EHumanResources[0].DateOfResignationEng.Value.ToLongDateString() + ")";
                    }
                    else
                        spanRetiredDate.InnerHtml = "Retiring in : " + eEmp.EHumanResources[0].DateOfResignation + " (" + eEmp.EHumanResources[0].DateOfResignationEng.Value.ToLongDateString() + ")";
                }
                btnNext.Text = "Update";

                txtEIN.Show();
                txtEIN.Text = eEmp.EmployeeId.ToString();

                cmbTitle.SelectedItem.Text = eEmp.Title;

                if (eEmp.UnitId != null)
                    cmbUnit.SelectedItem.Value = eEmp.UnitId.ToString();
                //cmbGender.SelectedItem.Value = eEmp.Gender.Value.ToString();
                //if((eEmp.Gender.Value == 1))
                //ExtControlHelper.ComboBoxSetSelected("Male", cmbGender);
                //else if ((eEmp.Gender.Value == 2))
                //    ExtControlHelper.ComboBoxSetSelected("Female", cmbGender);
                //else 
                ExtControlHelper.ComboBoxSetSelected(eEmp.Gender.ToString(), cmbGender);

                txtFirstNameEng.Text = eEmp.FirstName;
                txtMiddleNameEng.Text = eEmp.MiddleName;
                txtLastNameEng.Text = eEmp.LastName;
                txtFatherName.Text = eEmp.FatherName;
                txtMotherName.Text = eEmp.MotherName;

                txtMotherTongue.Text = eEmp.MotherTongue;
                if (eEmp.Children != null)
                    txtChildren.Text = eEmp.Children.ToString();

                if (eEmp.FunctionalTitleId != null)
                    ExtControlHelper.ComboBoxSetSelected(eEmp.FunctionalTitleId.ToString(), cmbFunctionTitle);
                
                txtNameNep.Text = eEmp.NameNepali;
                cmbMarried.SelectedItem.Text = eEmp.MaritalStatus;
                if (!string.IsNullOrEmpty(eEmp.PlaceOfBirth))
                    txtPOB.Text = eEmp.PlaceOfBirth;

                if (eEmp.DateOfBirthEng != null)
                {
                    txtDOB.SelectedDate = eEmp.DateOfBirthEng.Value;
                    CustomDate cd = CustomDate.GetCustomDateFromString(eEmp.DateOfBirthEng.Value.ToString("yyyy/MM/dd"), true);
                    try
                    {
                        
                        txtDOBNep.Text = CustomDate.ConvertEngToNep(cd).ToString();
                    }
                        // if date range exceeded do nothing as DOB not so imp
                    catch { }
                }         

                if (eEmp.HHumanResources.Count > 0)
                {
                    HHumanResource entity = eEmp.HHumanResources[0];
                    if (!string.IsNullOrEmpty(entity.Religion))
                        cmbReligion.SelectedItem.Text = entity.Religion;
                    if (entity.CasteId != null)
                        cmbEthnicity.SelectedItem.Value = entity.CasteId.ToString();

                    if (entity.HolidayGroupId != null)
                        cmbHolidayGroup.SelectedItem.Value = entity.HolidayGroupId.ToString();

                }

                if (eEmp.Hobby != null)
                    txtHobby.Text = eEmp.Hobby;

                if (eEmp.MarriageAniversaryEng != null)
                    txtMarriageAnniversary.Text = eEmp.MarriageAniversaryEng.Value.ToString();

                if (eEmp.PayGroupRef_ID != null)
                    ExtControlHelper.ComboBoxSetSelected(eEmp.PayGroupRef_ID.ToString(), cmbPayGroup);
           

                if (IsEnglish)
                {
                    if (eEmp.DateOfBirthEng != null)
                        txtAge.Text = NewHelper.GetElapsedTime(eEmp.DateOfBirthEng.Value, DateTime.Now);
                }
                else
                {
                    if (eEmp.DateOfBirth != null)
                        txtAge.Text = NewHelper.GetElapsedTime(BLL.BaseBiz.GetEngDate(eEmp.DateOfBirth, false), DateTime.Now);
                }
            }
        }


        protected void cmbGroup_Change(object sender, DirectEventArgs e)
        {
            cmbLevel.Store[0].DataSource = NewPayrollManager.GetParentLevels(int.Parse(cmbGroup.SelectedItem.Value));
            cmbLevel.Store[0].DataBind();

            cmbStep.Store[0].DataSource = new List<TextValue>();
            cmbStep.Store[0].DataBind();

            cmbPosition.Store[0].DataSource = new List<EDesignation>();
            cmbPosition.Store[0].DataBind();

        }
        protected void cmbBranch_Change(object sender, DirectEventArgs e)
        {
            cmbDepartment.Store[0].DataSource = DepartmentManager.GetAllDepartmentsByBranch(int.Parse(cmbBranch.SelectedItem.Value));
            cmbDepartment.Store[0].DataBind();
        }

        protected void cmbLevel_Change(object sender, DirectEventArgs e)
        {
            bool allowDecimalInGrade = NewPayrollManager.AllowDecimalInGrade();

            BLevel level = NewPayrollManager.GetLevelById(int.Parse(cmbLevel.SelectedItem.Value));
            List<TextValue> list = new List<TextValue>();
            BLevelRate rate = NewPayrollManager.GetLevelRate(level.LevelId, DateTime.Now);
            if (rate != null)
            {
                for (int i = 0; i <= rate.NoOfStepsGrade; i++)
                {
                    list.Add(new TextValue { Text = i.ToString(), Value = i.ToString() });

                    if (allowDecimalInGrade && i != 0)
                        list.Add(new TextValue { Text = (i + 0.5).ToString(), Value = (i + 0.5).ToString() });
                }

            }
            cmbStep.Store[0].DataSource = list;
            cmbStep.Store[0].DataBind();

            cmbPosition.Store[0].DataSource = NewPayrollManager.GetDesignationByLevel(int.Parse(cmbLevel.SelectedItem.Value));
            cmbPosition.Store[0].DataBind();

            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.RBB)
            {
                cmbStep.SetValue("0");
            }

        }

        //void SetRetirementOptions(EEmployee employee)
        //{
        //    try
        //    {
        //        if (employee.DateOfBirthEng != null)
        //        {
        //            //DateTime dob = employee.DateOfBirthEng.Value;
        //            //dob = dob.AddYears(58);
        //            //dob = dob.AddDays(-1);


        //            lblRetirementByAgeDate.Text = DateManager.AddYearToNepaliDate(employee.DateOfBirth,58);  //BLL.BaseBiz.GetAppropriateDate(dob);
        //            lblRetirementByAgeBasis.Text = string.Format("Date of Birth ({0})", employee.DateOfBirth);
        //        }
        //    }
        //    catch { }
        //}

        //public void calHireDate_Blur(object sender, DirectEventArgs e)
        //{
        //    try
        //    {
        //        if (!string.IsNullOrEmpty(calHireDate.Text.Trim()))
        //        {
        //            //DateTime dob = GetEngDate(calHireDate.Text.Trim());
        //            //dob = dob.AddYears(30);
        //            //dob = dob.AddDays(-1);

        //            lblRetirementByServiceDate.Text = DateManager.AddYearToNepaliDate(calHireDate.Text, 30);
        //            lblRetirementByServiceBasis.Text = string.Format("Hire date ({0})", calHireDate.Text);
        //        }
        //    }
        //    catch { }
        //}

        void LoadEmploymentDetails()
        {
            EEmployee employee = EmployeeManager.GetEmployeeById(GetEmployeeId());

            LoadProbationHistory(employee);


            if (employee.BranchId != null)
            {
                btnNext.Text = "Update";

                ExtControlHelper.ComboBoxSetSelected(employee.BranchId.Value, cmbBranch);

                if (NewHRManager.IsBranchEditable(employee.EmployeeId) == false)
                {
                    cmbDepartment.Disable();
                    cmbBranch.Disable();
                }
                cmbDepartment.Store[0].DataSource = DepartmentManager.GetAllDepartmentsByBranch(employee.BranchId.Value);
                cmbDepartment.Store[0].DataBind();

                ExtControlHelper.ComboBoxSetSelected(employee.DepartmentId, cmbDepartment);
            }

            if (employee.EHumanResources.Count > 0)
            {
                if (employee.EHumanResources[0].HireMethod != null)
                    ExtControlHelper.ComboBoxSetSelected(employee.EHumanResources[0].HireMethod.ToString(), cmbHireMethod);

               

                EHumanResource humanresource = employee.EHumanResources[0];
                txtAppointmentLetterNo.Text = humanresource.AppointmentLetterNo;

                if (IsEnglish == false && SessionManager.User.IsEnglishPreferred != null && SessionManager.User.IsEnglishPreferred)
                {
                    if (humanresource.AppointmentLetterDateEng != null)
                        calAppointLetterDate.Text = humanresource.AppointmentLetterDateEng.Value.ToString("yyyy/MM/dd");
                }
                else
                {
                    calAppointLetterDate.Text = humanresource.AppointmentLetterDate;
                }

                txtHireReference.Text = humanresource.HireReference;
                if (humanresource.LocationId != null)
                {
                    ExtControlHelper.ComboBoxSetSelected(humanresource.LocationId.ToString(), cmbELocation);

                    if (ListManager.LocationHistoryCountByEmployeeId(employee.EmployeeId) > 0)
                        cmbELocation.Disabled = true;
                }
            }

            if (employee.GradeId != null)
            {
                ExtControlHelper.ComboBoxSetSelected(employee.GradeId.ToString(), cmbGrade);
                if(ListManager.GradeHistoryCountByEmployeeId(employee.EmployeeId) > 0)
                    cmbGrade.Disabled = true;
            }

            if (employee.SubDepartmentId != null)
                ExtControlHelper.ComboBoxSetSelected(employee.SubDepartmentId.ToString(), cmbSubDepartment);

            ECurrentStatus status = null;
            if (employee.ECurrentStatus.Count >= 1)
            {
                // status
                status = employee.ECurrentStatus.OrderBy(x => x.FromDateEng).First();

                if (IsEnglish == false && SessionManager.User.IsEnglishPreferred != null && SessionManager.User.IsEnglishPreferred)
                    calHireDate.Text = status.FromDateEng.ToString("yyyy/MM/dd");
                else
                    calHireDate.Text = status.FromDate;


                if (status.DefineToDate != null && status.DefineToDate.Value)
                {
                    //chkDOJToDefined.Checked = status.DefineToDate.Value;
                    //calDOJTo.SetSelectedDate(status.ToDate, isEnglishDate);
                    chkContractEndDate.Checked = status.DefineToDate.Value;


                    if (IsEnglish == false && SessionManager.User.IsEnglishPreferred != null && SessionManager.User.IsEnglishPreferred)
                    {

                        if (status.FromDateEng != null)
                            calContractEndDate.Text = status.ToDateEng.Value.ToString("yyyy/MM/dd");
                    }
                    else
                    {
                        calContractEndDate.Text = status.ToDate;
                    }
                    
                    calContractEndDate.Disabled = false;
                    //calDOJTo.Enabled = true;
                }
                else
                {
                    //calDOJTo.Enabled = false;
                }
                //calDOJTo.SetSelectedDate(status.ToDate, isEnglishDate);
                if (status.CurrentStatus != 0)
                    ExtControlHelper.ComboBoxSetSelected(status.CurrentStatus.ToString(), cmbStatus);
                //UIHelper.SetSelectedInDropDown(ddlJoiningStatus, status.CurrentStatus);

                if ((new EmployeeManager().GetStatusCount(employee.EmployeeId) != 1))
                {
                    cmbStatus.Enabled = false;
                    if (OvertimeManager.GetSetting().AllowToChangeJoiningDate != null
                        && OvertimeManager.GetSetting().AllowToChangeJoiningDate.Value) { }
                    else
                        calHireDate.Enabled = false;
                    //calDOJTo.Enabled = false;
                    //chkDOJToDefined.Enabled = false;
                    //chkDOJToDefined.Enabled = false;
                    //cmbStatus.ToolTip = calDOJFrom.ToolTip
                    //     = Resources.Messages.NotEditableAsAlreadyContainsMoreStatus;
                }
                //check if the emp has multiple grade or location or work shift 
                // then disable this drop down as this date should always be the same as that of 
                // the first from date of grade or location or work shift, done to prevent first from date can be greater then second from date
                //if (calHireDate.Enabled == true)
                //{
                //    if (employee.EGradeHistories.Count >= 2 || employee.EWorkShiftHistories.Count >= 2
                //        || employee.ELocationHistories.Count >= 2)
                //    {
                //        calDOJFrom.Enabled = false;
                //        calDOJFrom.ToolTip = Resources.Messages.NotEditableAsContainsMultipleGradeOrWorkShiftOrLocation;
                //    }
                //}



                //chkSalaryCalFrom.Checked = true;
                // Disable to un check in edit mode
                //
                chkSalaryCalculateDate.Checked = true;

                if (IsEnglish == false && SessionManager.User.IsEnglishPreferred != null && SessionManager.User.IsEnglishPreferred)
                {
                    calSalaryCalculationDate.Text = employee.EHumanResources[0].SalaryCalculateFromEng.ToString("yyyy/MM/dd");
                }
                else
                {
                    calSalaryCalculationDate.Text = employee.EHumanResources[0].SalaryCalculateFrom;
                }

                //if (employee.ExcludeInSalary != null && employee.ExcludeInSalary.Value)
                //    ExtControlHelper.ComboBoxSetSelected(employee.ExcludeInSalary.Value.ToString().ToLower(), cmbExcludeInSalary);

                //calSalaryCalculationDate.Disabled = false;
                //calHireDate_Blur(null, null);
            }

            bool isCalculationSaved = CalculationManager.IsCalculatedSavedForEmployee(employee.EmployeeId);
            if (isCalculationSaved)
            {
                //chkSalaryCalculateDate.Disable();
                calSalaryCalculationDate.ReadOnly = true;
                if (OvertimeManager.GetSetting().AllowToChangeJoiningDate != null
                       && OvertimeManager.GetSetting().AllowToChangeJoiningDate.Value)
                {
                    calSalaryCalculationDate.ReadOnly = false;
                    calHireDate.Enable(true);

                }
                else
                {
                    calSalaryCalculationDate.ReadOnly = true;
                    calHireDate.Disable();
                }
            }
            else
                calSalaryCalculationDate.ReadOnly = false;

            if (employee.ECurrentStatus.Count > 1)
            {
                
                cmbStatus.Disable();
            }

            if (employee.SkipLevelGradeMatrix != null && employee.SkipLevelGradeMatrix.Value)
                chkExcludeNonMatrixSalary.Checked = employee.SkipLevelGradeMatrix.Value;

            if (EmployeeManager.HasLevelGradeForStatus(employee.ECurrentStatus.OrderByDescending(x => x.FromDateEng).First().CurrentStatus, employee.SkipLevelGradeMatrix))
            {
                // First Joined level/step
                if (employee.PEmployeeIncomes.Count >= 1)
                {
                    PEmployeeIncome income = employee.PEmployeeIncomes.FirstOrDefault(x => x.PIncome.IsBasicIncome == true);
                    if (income.LevelId != null)
                    {
                        bool isLevelEditable = true;
                        int groupID = 0, levelID = 0;
                        double stepGrade = 0;
                        NewPayrollManager.GetEmployeeFirstLevelGrade(employee.EmployeeId, ref isLevelEditable, ref groupID, ref levelID, ref stepGrade);
                        
                        if (isLevelEditable == false )
                        {
                            cmbGroup.Disable();
                            cmbLevel.Disable();
                            cmbStep.Disable();
                            cmbPosition.Disable();
                            cmbStatus.Disable();
                        }


                        BLevel currentLevel = NewPayrollManager.GetLevelById(income.LevelId.Value);

                        ExtControlHelper.ComboBoxSetSelected(currentLevel.LevelGroupId, cmbGroup);
                        cmbLevel.Store[0].DataSource = NewPayrollManager.GetParentLevels(currentLevel.LevelGroupId);
                        cmbLevel.Store[0].DataBind();

                        ExtControlHelper.ComboBoxSetSelected(income.LevelId, cmbLevel);
                        cmbPosition.Store[0].DataSource = NewPayrollManager.GetDesignationByLevel(income.LevelId.Value);
                        cmbPosition.Store[0].DataBind();


                        // First Joined Designation
                        bool isDesignationeditable = NewPayrollManager.IsDesignationEditable(employee.EmployeeId);
                        ExtControlHelper.ComboBoxSetSelected(employee.DesignationId, cmbPosition);
                        if (isDesignationeditable == false)
                            cmbPosition.Disable();


                        bool allowDecimalInGrade = NewPayrollManager.AllowDecimalInGrade();

                        List<TextValue> list = new List<TextValue>();
                        BLevelRate stepGrades = NewPayrollManager.GetLevelRate(income.LevelId.Value, status.FromDateEng);
                        if (stepGrades != null)
                        {
                            for (int i = 0; i <= stepGrades.NoOfStepsGrade; i++)
                            {
                                list.Add(new TextValue { Text = i.ToString(), Value = i.ToString() });

                                if(allowDecimalInGrade && i != 0)
                                    list.Add(new TextValue { Text = (i + 0.5).ToString(), Value =  (i + 0.5).ToString() });
                            }

                        }

                        cmbStep.Store[0].DataSource = list;
                        cmbStep.Store[0].DataBind();

                        if (income.Step != null)
                            ExtControlHelper.ComboBoxSetSelected(income.Step.Value, cmbStep);
                        else
                            ExtControlHelper.ComboBoxSetSelected(0, cmbStep);
                    }
                }
            }
            else
            {
               
                cmbStatus_Change(null, null);

                

                // First Joined Designation
                bool isDesignationeditable = NewPayrollManager.IsDesignationEditable(employee.EmployeeId);
                EDesignation designation = new CommonManager().GetDesignationById(employee.DesignationId.Value);

                if (isDesignationeditable == false)
                {
                    cmbGroup.Disable();
                    cmbPosition.Disable();
                    cmbLevel.Disable();
                }

                
                if (designation.LevelId != null)
                {
                    cmbLevel.Store[0].DataSource = NewPayrollManager.GetAllParentLevels();
                    cmbLevel.Store[0].DataBind();

                    //cmbLevel.Store[0].DataSource = NewPayrollManager.GetParentLevels(designation.BLevel.LevelGroupId);
                    //cmbLevel.Store[0].DataBind();

                    // if no Level/Grade display case
                    cmbPosition.Store[0].DataSource = NewPayrollManager.GetDesignationByLevel(designation.LevelId.Value);
                    cmbPosition.Store[0].DataBind();

                    ExtControlHelper.ComboBoxSetSelected(designation.LevelId.ToString(), cmbLevel);



                    cmbGroup.Store[0].DataSource = NewPayrollManager.GetLevelGroup();
                    cmbGroup.Store[0].DataBind();
                    ExtControlHelper.ComboBoxSetSelected(designation.BLevel.LevelGroupId.ToString(), cmbGroup);
                }
                cmbStep.Store[0].DataSource = new List<TextValue> { new TextValue { Value = "0", Text = "None" } };
                cmbStep.Store[0].DataBind();
                ExtControlHelper.ComboBoxSetSelected("0", cmbStep);



                ExtControlHelper.ComboBoxSetSelected(employee.DesignationId, cmbPosition);
            }

            //if (employee.IsRetired != null && employee.IsRetired.Value)
            //{
            //    chkRetirement.Checked = employee.IsRetired.Value;
            //    calRetirementDate.Text = employee.EHumanResources[0].DateOfRetirement;
            //    calRetirementDate.Enable();
            //    cmbTypes.Enable();
            //    if (employee.EHumanResources[0].RetirementType != null)
            //    {
            //        ExtControlHelper.ComboBoxSetSelected(employee.EHumanResources[0].RetirementType.ToString(),
            //            cmbTypes);
            //    }
            //}
            //if (employee.IsResigned != null && employee.IsResigned.Value)
            //{
            //    chkRetirement.Checked = employee.IsResigned.Value;
            //    calRetirementDate.Text = employee.EHumanResources[0].DateOfResignation;
            //    calRetirementDate.Enable();
            //    cmbTypes.Enable();
            //    if (employee.EHumanResources[0].RetirementType != null)
            //    {
            //        ExtControlHelper.ComboBoxSetSelected(employee.EHumanResources[0].RetirementType.ToString(),
            //            cmbTypes);
            //    }
            //}


            // If service history enabled and there is other row other than Appoitment then disable Status,
            // Branch,Dep,Designation fields
            if (CommonManager.IsServiceHistoryEnabled &&
                BLL.BaseBiz.PayrollDataContext.EmployeeServiceHistories.Any
                (x => x.EmployeeId == employee.EmployeeId && x.EventID != (int)ServiceEventType.Appointment))
            {
                calHireDate.Disable();
                calSalaryCalculationDate.ReadOnly = true;
                calContractEndDate.Disable();
                cmbStatus.Disable();
                chkContractEndDate.Disable();

                cmbLevel.Disable();
                cmbPosition.Disable();

                cmbBranch.Disable();
                cmbDepartment.Disable();
            }
        }



        protected Status AddEmployeementDetails(EEmployee employee, bool isInsert)
        {
            Status rstatus = new Status();
            EHumanResource empHR = employee.EHumanResources[0];
            PEmployeeIncome income = new PEmployeeIncome();
            ECurrentStatus status = null;
            status = new ECurrentStatus();

            status.CurrentStatus = int.Parse(cmbStatus.SelectedItem.Value);


            if (IsEnglish == false && SessionManager.User.IsEnglishPreferred != null && SessionManager.User.IsEnglishPreferred)
            {
                status.FromDateEng = BLL.BaseBiz.GetEngDate(calHireDate.Text.Trim(), true);
                try
                {
                    status.FromDate = BLL.BaseBiz.GetAppropriateDate(status.FromDateEng);
                }
                catch (Exception exp)
                {

                    rstatus.ErrorMessage = "Invalid english date value " + calHireDate.Text + " in appointment/hire date field, please re-check the date.<br>"
                        +exp.Message;
                    return rstatus;
                    
                }
            }
            else
            {
                status.FromDate = calHireDate.Text.Trim();
                status.FromDateEng = GetEngDate(status.FromDate);
            }

            bool hasLevelGrade = false;

            if (isInsert)
            {
                hasLevelGrade = EmployeeManager.HasLevelGradeForStatus(status.CurrentStatus, chkExcludeNonMatrixSalary.Checked);
            }
            else
            {
                if (chkExcludeNonMatrixSalary.Checked)
                    hasLevelGrade = false;
                else
                    hasLevelGrade = NewHRManager.IsEmployeeLastStatusForLevelGradeEligible(employee.EmployeeId);
            }

            if (hasLevelGrade)
            {
                //income.GroupId = int.Parse(cmbGroup.SelectedItem.Value);
                income.LevelId = int.Parse(cmbLevel.SelectedItem.Value);
                if (cmbStep.SelectedItem != null && cmbStep.SelectedItem.Value != null)
                    income.Step = double.Parse(cmbStep.SelectedItem.Value);
            }

            employee.BranchId = int.Parse(cmbBranch.SelectedItem.Value);
            employee.DepartmentId = int.Parse(cmbDepartment.SelectedItem.Value);
            employee.DesignationId = int.Parse(cmbPosition.SelectedItem.Value);

            if (cmbGrade.SelectedItem != null && cmbGrade.SelectedItem.Value != null)
                employee.GradeId = int.Parse(cmbGrade.SelectedItem.Value);

            if (cmbSubDepartment.SelectedItem != null && cmbSubDepartment.SelectedItem.Value != null)
                employee.SubDepartmentId = int.Parse(cmbSubDepartment.SelectedItem.Value);

            employee.SkipLevelGradeMatrix = chkExcludeNonMatrixSalary.Checked;
            if (chkContractEndDate.Checked)
            {

                if (string.IsNullOrEmpty(calContractEndDate.Text))
                {
                    rstatus.ErrorMessage = "Contract end date is required.";
                    return rstatus;
                }

                status.DefineToDate = true;


                if (IsEnglish == false && SessionManager.User.IsEnglishPreferred != null && SessionManager.User.IsEnglishPreferred)
                {
                    status.ToDateEng = BLL.BaseBiz.GetEngDate(calContractEndDate.Text.Trim(), true);
                    status.ToDate = BLL.BaseBiz.GetAppropriateDate(status.ToDateEng.Value);
                }
                else
                {
                    status.ToDate = calContractEndDate.Text.Trim();
                    status.ToDateEng = GetEngDate(status.ToDate);
                }
            }
            else
                status.DefineToDate = false;

            if (CommonManager.Setting.EnableWeeklyLikePay != null && CommonManager.Setting.EnableWeeklyLikePay.Value)
            {
                if (cmbPayGroup.SelectedItem != null && cmbPayGroup.SelectedItem.Value != null)
                    employee.PayGroupRef_ID = int.Parse(cmbPayGroup.SelectedItem.Value);

                if (employee.PayGroupRef_ID == 0)
                    employee.PayGroupRef_ID = null;
            }
            //status.ToDate = calDOJTo.SelectedDate.ToString();
            //status.ToDateEng = BLL.BaseBiz.GetEngDate(status.ToDate, IsEnglish);
            // If unselected then same as DOJ only may happen for save mode as checkbox are not editable in
            // update mode
            //if (string.IsNullOrEmpty(calSalaryCalculationDate.Text.Trim()))
            //if (cmbExcludeInSalary.SelectedItem != null && cmbExcludeInSalary.SelectedItem.Value != null)
            //{
            //    employee.ExcludeInSalary = bool.Parse(cmbExcludeInSalary.SelectedItem.Value);
            //}

            if (chkSalaryCalculateDate.Checked == false)
            {
                empHR.SalaryCalculateFrom = status.FromDate;
                empHR.SalaryCalculateFromEng = GetEngDate(empHR.SalaryCalculateFrom);
            }
            else
            {
                if (IsEnglish == false && SessionManager.User.IsEnglishPreferred != null && SessionManager.User.IsEnglishPreferred)
                {
                    empHR.SalaryCalculateFromEng = BLL.BaseBiz.GetEngDate(calSalaryCalculationDate.Text.Trim(), true);
                    empHR.SalaryCalculateFrom = BLL.BaseBiz.GetAppropriateDate(empHR.SalaryCalculateFromEng);
                }
                else
                {
                    empHR.SalaryCalculateFrom = calSalaryCalculationDate.Text.Trim();
                    empHR.SalaryCalculateFromEng = GetEngDate(empHR.SalaryCalculateFrom);
                }
            }
           
            empHR.HireReference = txtHireReference.Text.Trim();

            //if (CommonManager.CompanySetting.WhichCompany == WhichCompany.RBB)
            //{

            //    if (empHR.RetirementOptionType == (int)RetirementOptionType.ServiceYear)
            //    {

            //        employee.RetirementEligibleDate = DateManager.AddYearToNepaliDate(calHireDate.Text, 58);  //BLL.BaseBiz.GetAppropriateDate(dob);
            //        employee.RetirementEligibleDateEng = GetEngDate(employee.RetirementEligibleDate);

            //    }
            //    else
            //    {
            //        employee.RetirementEligibleDate = DateManager.AddYearToNepaliDate(calHireDate.Text, 30);  //BLL.BaseBiz.GetAppropriateDate(dob);
            //        employee.RetirementEligibleDateEng = GetEngDate(employee.RetirementEligibleDate);
            //    }

            //}


            if (cmbHireMethod.SelectedItem != null && cmbHireMethod.SelectedItem.Value != null)
                empHR.HireMethod = int.Parse(cmbHireMethod.SelectedItem.Value);



            if (!string.IsNullOrEmpty(calAppointLetterDate.Text.Trim()))
            {
                if (IsEnglish == false && SessionManager.User.IsEnglishPreferred != null && SessionManager.User.IsEnglishPreferred)
                {
                    empHR.AppointmentLetterDateEng = BLL.BaseBiz.GetEngDate(calAppointLetterDate.Text.Trim(), true);
                    empHR.AppointmentLetterDate = BLL.BaseBiz.GetAppropriateDate(empHR.AppointmentLetterDateEng.Value);
                }
                else
                {
                    empHR.AppointmentLetterDate = calAppointLetterDate.Text.Trim();
                    empHR.AppointmentLetterDateEng = GetEngDate(empHR.AppointmentLetterDate);
                }
            }

            empHR.AppointmentLetterNo = txtAppointmentLetterNo.Text.Trim();

            if (GetEngDate(empHR.SalaryCalculateFrom) < GetEngDate(status.FromDate))
            {
                rstatus.ErrorMessage = "Salary calculation date must be greater equal to hire date.";
                return rstatus;

            }

            if (cmbELocation.SelectedItem != null && cmbELocation.SelectedItem.Value != null)
                empHR.LocationId = int.Parse(cmbELocation.SelectedItem.Value);

            //save eng date for checkin in active or not
            //if (employee.IsRetired.Value)
            //{
            //    empHR.DateOfRetirement = calDateOfRetirement.SelectedDate.ToString();
            //}
            ////emp.IsResigned = chkIsResigned.Checked;
            //if (employee.IsResigned.Value)
            //{
            //    empHR.DateOfResignation = calDateOfResignation.SelectedDate.ToString();
            //}
            //empHR.NoPFOnRetirementResignation = chkNOPFOnRetResg.Checked;

            //Probation History
            ProbationHistory probation = null;
            if (chkHasProbation.Checked)
            {
                probation = new ProbationHistory();
                probation.CreatedBy = SessionManager.CurrentLoggedInUserID;
                probation.CreatedOn = DateTime.Now;
                if (string.IsNullOrEmpty(calProbationStart.Text))
                {
                    rstatus.ErrorMessage = "Probation start date is required.";
                    return rstatus;
                }
                if (string.IsNullOrEmpty(calProbationEnd.Text))
                {
                    rstatus.ErrorMessage = "Probation start date is required.";
                    return rstatus;
                }

                probation.FromDate = calProbationStart.Text.Trim();
                probation.FromDateEng = GetEngDate(probation.FromDate);
                probation.ToDate = calProbationEnd.Text.Trim();
                probation.ToDateEng = GetEngDate(probation.ToDate);

                employee.ProbationHistories.Add(probation);
            }

            employee.PEmployeeIncomes.Add(income);
            //employee.EHumanResources.Add(empHR);
            employee.ECurrentStatus.Add(status);

            // Retirement
            //if (chkRetirement.Checked)
            //{
            //    employee.IsRetired = true;
            //    employee.EHumanResources[0].DateOfRetirement = calRetirementDate.Text.Trim();
            //    employee.EHumanResources[0].DateOfRetirementEng = GetEngDate(employee.EHumanResources
            //        [0].DateOfRetirement);
            //    employee.EHumanResources[0].reg

            //}

            return rstatus;

        }

        private void LoadProbationHistory(EEmployee employee)
        {
            ProbationHistory probation = employee.ProbationHistories.OrderBy(x => x.ProbationId).FirstOrDefault();
            if (probation != null)
            {
                chkHasProbation.Checked = true;
                calProbationStart.Enable();
                calProbationEnd.Enable();

                calProbationStart.Text = probation.FromDate;
                calProbationEnd.Text = probation.ToDate;

                if (employee.ProbationHistories.Count > 1)
                {
                    chkHasProbation.Disable();
                    calProbationEnd.Disable();
                    calProbationStart.Disable();
                }
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            bool isEng = false;
            if (UserManager.IsEnglishDatePreferredInNepaliDateSys())
                isEng = true;

            UserControl ctrl = Master.ContentHeaderCtrl();
            ResourceManager rm = ctrl.FindControl("ResourceManager1") as ResourceManager;

            rm.RegisterClientInitScript("Date11111", string.Format("isEnglish = {0};", isEng.ToString().ToLower()));

            rm.RegisterClientInitScript("Date22222", string.Format("todayDate = '{0}';function calendarOnClick(event, sourceElement, e3){{}};",
                                                                      CustomDate.GetTodayDate(isEng).ToString()));
        }

    }
}