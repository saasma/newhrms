﻿<%@ Page Title="Employee Listing" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="EmployeeListing.aspx.cs" Inherits="Web.NewHR.EmployeeListing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .x-grid-cell-inner
        {
            padding: 2px 5px 2px 5px !important;
        }
        
        .x-column-header-inner
        {
            padding: 7px 5px 7px 5px;
        }
    </style>
    <script type="text/javascript">

      var CommandHandler = function(command, record){
            <%= hiddenValue.ClientID %>.setValue(record.data.EmployeeId);
                if(command=="Edit")
                {
                    window.location.href = "personaldetail.aspx?id="+record.data.EmployeeId;
                    //window.open('personaldetail.aspx?id='record.data.EmployeeId+,'_blank');
     
                }
                else
                {
                    <%= btnDelete.ClientID %>.fireEvent('click');
                }

             }


             function searchList() {
             <%=GridLevels.ClientID %>.getStore().pageSize = <%=cmbPageSize.ClientID %>.getValue();
            <%=PagingToolbar1.ClientID %>.doRefresh();
        }


        function EmpImageFunc(e,e1,e2,record) {
            //console.log(e);
            if(e2==0)
            {
                var EmpImage = <%=EmpImage.ClientID %>;
                EmpImage.setImageUrl(record.data.UrlPhoto);
            }
           
        };

     

        function RetiredRenderer(e1,e2,record)
        {
            var isActive =  record.data.IsRetiredOrResigned;
            if(isActive.trim()=="0")
            {
                return '<img src="../images/yes.png"/>';
            }
            else
            {
                return '<img src="../images/no.png"/>';
            }
            //return '<a href="EmployeeAppraisalHRPage.aspx?efid='+ record.data.AppraisalEmployeeFormID + '&fid='+ record.data.AppraisalFormRef_ID +'">View</a>';
        }

        function EmpPicRenderer(e1,e2,record)
        {
            var UrlPhoto =  record.data.UrlPhoto;
            var JS = ""+UrlPhoto+")";
            return '<img class="media-object img-circle" style="" src="'+UrlPhoto+'" height="38" width="38"/>';
            
            //return '<a href="EmployeeAppraisalHRPage.aspx?efid='+ record.data.AppraisalEmployeeFormID + '&fid='+ record.data.AppraisalFormRef_ID +'">View</a>';
        }


        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <ext:Hidden runat="server" ID="Hidden_SelectedEmpPic">
    </ext:Hidden>
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Employee List
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel" style="margin-top: 0px !important; padding-top: 0px !important;">
        <ext:Hidden runat="server" ID="hiddenValue" />
        <ext:Button ID="btnDelete" runat="server" Hidden="true">
            <DirectEvents>
                <Click OnEvent="btnDelete_Click">
                    <Confirmation ConfirmRequest="true" Message="'Do you want to confirm delete the employee,all data will be lost?" />
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:Button>
        <div class="separator bottom">
        </div>
        <div class="innerLR">
            <div class="alert alert-info" style="margin-top: 25px;">
                <ext:Container ID="listingContainer" runat="server">
                    <Content>
                        <table>
                            <tr>
                                <td>
                                    <ext:ComboBox Width="140" MatchFieldWidth="false" MarginSpec="0 5 0 5" LabelSeparator=""
                                        LabelAlign="Top" QueryMode="Local" ID="cmbBranch" DisplayField="Name" ValueField="BranchId"
                                        runat="server" FieldLabel="Branch">
                                        <Items>
                                            <ext:ListItem Index="0" Value="-1" Text="All">
                                            </ext:ListItem>
                                        </Items>
                                        <Store>
                                            <ext:Store ID="Store1" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model1" IDProperty="BranchId" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="BranchId" />
                                                            <ext:ModelField Name="Name" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                    </ext:ComboBox>
                                </td>
                                <td>
                                    <ext:ComboBox FieldLabel="Department" MatchFieldWidth="false" ID="cmbDepartment"
                                        Width="140" MarginSpec="0 5 0 5" runat="server" ValueField="DepartmentId" DisplayField="Name"
                                        LabelAlign="Top" LabelSeparator="" QueryMode="Local">
                                        <Items>
                                            <ext:ListItem Index="0" Value="-1" Text="All">
                                            </ext:ListItem>
                                        </Items>
                                        <Store>
                                            <ext:Store ID="Store2" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model2" IDProperty="DepartmentId" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="DepartmentId" />
                                                            <ext:ModelField Name="Name" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                    </ext:ComboBox>
                                </td>
                                <td>
                                    <ext:ComboBox ID="cmbLevel" MatchFieldWidth="false" runat="server" FieldLabel="Level"
                                        Width="140" MarginSpec="0 5 0 5" DisplayField="Name" ValueField="LevelId" QueryMode="Local"
                                        LabelSeparator="" LabelAlign="Top">
                                        <Items>
                                            <ext:ListItem Index="0" Value="-1" Text="All">
                                            </ext:ListItem>
                                        </Items>
                                        <Store>
                                            <ext:Store ID="storeLevel" runat="server">
                                                <Model>
                                                    <ext:Model ID="modelLevel" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="LevelId" Type="Int" />
                                                            <ext:ModelField Name="Name" Type="String" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                    </ext:ComboBox>
                                </td>
                                <td>
                                    <ext:ComboBox MatchFieldWidth="false" FieldLabel="Designation" ID="cmbDesignation"
                                        Width="140" MarginSpec="0 5 0 5" runat="server" ValueField="DesignationId" DisplayField="Name"
                                        LabelAlign="Top" LabelSeparator="" QueryMode="Local">
                                        <Items>
                                            <ext:ListItem Index="0" Value="-1" Text="All">
                                            </ext:ListItem>
                                        </Items>
                                        <Store>
                                            <ext:Store ID="Store4" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model5" IDProperty="DesignationId" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="DesignationId" />
                                                            <ext:ModelField Name="Name" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                    </ext:ComboBox>
                                </td>
                                
                                <td>
                                    <ext:TextField ID="txtINO" EmptyText="Search" Width="90" MarginSpec="0 5 0 5" FieldLabel="I No"
                                        runat="server" LabelAlign="Top" LabelSeparator="">
                                    </ext:TextField>
                                </td>
                                <td>
                                    <ext:ComboBox ID="cmbEmployee" EmptyText="Search" runat="server" Width="135" FieldLabel="Employee"
                                        LabelAlign="Top" HideBaseTrigger="true" LabelSeparator="" QueryMode="Local" DisplayField="Name"
                                        ValueField="EmployeeId">
                                        <Store>
                                            <ext:Store ID="storeEmployeeList" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="EmployeeId" Type="String" />
                                                    <ext:ModelField Name="Name" />
                                                </Fields>
                                            </ext:Store>
                                        </Store>
                                    </ext:ComboBox>
                                </td>
                                <td>
                                    <ext:Button ID="btnLoad" runat="server" Text="Load" Width="100" Cls="btn btn-default" MarginSpec="25 10 10 10">
                                        <Listeners>
                                            <Click Fn="searchList">
                                            </Click>
                                        </Listeners>
                                    </ext:Button>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="9">
                                    <div style="float: right;">
                                        <ext:ComboBox ID="cmbRet" LabelAlign="Left" FieldLabel="Retired Filter" LabelSeparator=""
                                            runat="server" ForceSelection="true" AllowBlank="false">
                                            <Items>
                                                <ext:ListItem Index="0" Text="Active Only" Value="false">
                                                </ext:ListItem>
                                                <ext:ListItem Index="1" Text="All" Value="">
                                                </ext:ListItem>
                                                <ext:ListItem Index="2" Text="Retired Only" Value="true">
                                                </ext:ListItem>
                                            </Items>
                                            <Listeners>
                                                <Change Fn="searchList">
                                                </Change>
                                            </Listeners>
                                        </ext:ComboBox>
                                    </div>
                                </td>
                                <td>
                                    <ext:Checkbox ID="chkShowRetired" LabelAlign="Left" FieldLabel="Show Retired" Hidden="true"
                                        Visible="false" LabelSeparator="" runat="server">
                                        <Listeners>
                                            <Change Fn="searchList">
                                            </Change>
                                        </Listeners>
                                    </ext:Checkbox>
                                </td>
                            </tr>
                        </table>
                    </Content>
                </ext:Container>
            </div>
            <ext:KeyMap ID="KeyMap1" runat="server" Target="={#{listingContainer}.getBody()}">
                <Binding>
                    <ext:KeyBinding Handler="#{btnLoad}.fireEvent('click');">
                        <Keys>
                            <ext:Key Code="ENTER" />
                        </Keys>
                    </ext:KeyBinding>
                </Binding>
            </ext:KeyMap>
            <ext:GridPanel StyleSpec="margin-top:15px;" ID="GridLevels" runat="server" Cls="itemgrid"
                OnReadData="Store_ReadData">
                <Store>
                    <ext:Store ID="storeEmpList" runat="server" AutoLoad="true" OnReadData="Store_ReadData"
                        RemotePaging="true" RemoteSort="true">
                        <Proxy>
                            <ext:PageProxy />
                        </Proxy>
                        <Model>
                            <ext:Model ID="Model4" runat="server" IDProperty="EmployeeId">
                                <Fields>
                                    <ext:ModelField Name="EmployeeId" Type="String" />
                                    <ext:ModelField Name="Name" Type="string" />
                                    <ext:ModelField Name="Branch" Type="string" />
                                    <ext:ModelField Name="Department" Type="string" />
                                    <ext:ModelField Name="Level" Type="string" />
                                    <ext:ModelField Name="Designation" Type="string" />
                                    <ext:ModelField Name="StatusText" Type="string" />
                                    <ext:ModelField Name="IdCardNo" Type="string" />
                                    <ext:ModelField Name="IsRetiredOrResigned" Type="string" />
                                    <ext:ModelField Name="UrlPhoto" Type="string" />
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Sorters>
                            <ext:DataSorter Property="EmployeeId" Direction="ASC" />
                        </Sorters>
                    </ext:Store>
                </Store>
                <Listeners>
                    <CellClick Fn="EmpImageFunc">
                    </CellClick>
                </Listeners>
                <Callouts>
                    <ext:Callout ID="Callout2" runat="server" Alignment="TopLeft" Trigger="Click" BodyStyle="padding:2px 0px;"
                        Delegate=".x-grid-cell-first">
                        <BodyWidget>
                            <ext:Image Cls="media-object img-circle img-online" ID="EmpImage" Width="200" Height="200"
                                runat="server">
                            </ext:Image>
                        </BodyWidget>
                    </ext:Callout>
                </Callouts>
                <ColumnModel>
                    <Columns>
                        <ext:Column ID="UrlPhoto" Sortable="false" MenuDisabled="true" runat="server" Text=""
                            Align="Left" DataIndex="UrlPhoto" Width="50">
                            <Renderer Fn="EmpPicRenderer" />
                        </ext:Column>
                        <ext:Column ID="Column5" Sortable="true" runat="server" Text="EIN" Align="Left" DataIndex="EmployeeId"
                            Width="50" />
                        <ext:Column ID="Column7" Sortable="true" runat="server" Text="I No" Align="Left"
                            DataIndex="IdCardNo" Width="60">
                        </ext:Column>
                        <ext:TemplateColumn ID="TemplateColumn1" runat="server" Text="Name" Width="175" DataIndex="Name"
                            TemplateString='<a href="EmployeeDetails.aspx?id={EmployeeId}"> {Name}</a>' />
                        <ext:Column ID="Column1" Sortable="true" runat="server" Text="Branch" Align="Left"
                            DataIndex="Branch" Width="150">
                        </ext:Column>
                        <ext:Column ID="Column8" Sortable="true" runat="server" Text="Department" Align="Left"
                            DataIndex="Department" Width="150">
                        </ext:Column>
                        <ext:Column ID="Column2" Sortable="true" runat="server" Text="Level" Align="Left"
                            DataIndex="Level" Width="150">
                        </ext:Column>
                        <ext:Column ID="Column3" Sortable="true" runat="server" Text="Designation" Align="Left"
                            DataIndex="Designation" Width="150">
                        </ext:Column>
                        <ext:Column ID="Column4" Sortable="false" runat="server" Text="Status" Align="Left"
                            DataIndex="StatusText" Width="90">
                        </ext:Column>
                        <ext:Column ID="Column9" Sortable="false" runat="server" Text="Active" Align="Left"
                            DataIndex="IsRetiredOrResigned" Width="50">
                            <Renderer Fn="RetiredRenderer" />
                        </ext:Column>
                        <ext:CommandColumn runat="server" Text="" Width="45">
                            <Commands>
                                <ext:GridCommand Cls="editGridButton" Text="<i class='fa fa-pencil'></i>" CommandName="Edit" />
                            </Commands>
                            <Listeners>
                                <Command Handler="CommandHandler(command,record);" />
                            </Listeners>
                        </ext:CommandColumn>
                        <ext:CommandColumn ID="CommandColumn1" runat="server" Text="" Width="45">
                            <Commands>
                                <ext:GridCommand Cls="editGridButton" Text="<i class='fa fa-trash-o'></i>" CommandName="Delete" />
                            </Commands>
                            <Listeners>
                                <Command Handler="CommandHandler(command,record);" />
                            </Listeners>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
                <SelectionModel>
                    <ext:CellSelectionModel ID="CellSelectionModel1" runat="server" Mode="Single" />
                </SelectionModel>
                <BottomBar>
                    <%--<ext:PagingToolbar ID="PagingToolbar1" runat="server" PageSize="50" DisplayInfo="true"
                        DisplayMsg="Displaying employees {0} - {1} of {2}" EmptyMsg="No Employee to display">
                        <Items>
                            <ext:ComboBox runat="server" SelectedIndex="0" Width="80" ID="cmbPageSize">
                                <Items>
                                    <ext:ListItem Text="50" Value="5" />
                                    <ext:ListItem Text="75" Value="7" />
                                    <ext:ListItem Text="100" Value="10" />
                                </Items>
                                <Listeners>
                                    <Select Handler="#{PagingToolbar1}.pageSize = parseInt( this.getValue()); searchList();" />
                                </Listeners>
                            </ext:ComboBox>
                        </Items>
                    </ext:PagingToolbar>--%>
                    <ext:PagingToolbar ID="PagingToolbar1" runat="server" StoreID="storeEmpList" DisplayInfo="true">
                        <Items>
                            <ext:Label ID="lblPageSize" runat="server" Text="Page size:" />
                            <ext:ToolbarSpacer ID="ToolbarSpacer_listItems" runat="server" Width="10" />
                            <ext:ComboBox ID="cmbPageSize" runat="server" Width="80" SelectOnFocus="true" Selectable="true"
                                ValueField="Value" DisplayField="Text" ForceSelection="true" AllowBlank="false">
                                <Listeners>
                                    <Select Handler="searchList()" />
                                    <%--#{cmbPageSize}.pageSize = this.getValue();#{PagingToolbar1}.moveFirst();--%>
                                </Listeners>
                                <Items>
                                    <ext:ListItem Value="20" Text="20" />
                                    <ext:ListItem Value="50" Text="50" />
                                    <ext:ListItem Value="300" Text="300" />
                                </Items>
                                <SelectedItems>
                                    <ext:ListItem Index="2">
                                    </ext:ListItem>
                                </SelectedItems>
                            </ext:ComboBox>
                        </Items>
                    </ext:PagingToolbar>
                </BottomBar>
            </ext:GridPanel>
            <div class="alert alert-info" style="margin-top: 15px;">
                <table>
                    <tr>
                        <td>
                            <asp:LinkButton ID="btnExportPopup" runat="server" Text="Import Employee" OnClientClick="employeePopup();return false;"
                                CssClass=" excel marginRight tiptip" />
                        </td>
                        <td style="padding-left: 20px">
                            <asp:LinkButton ID="btnExportImportAccounts" runat="server" Text="Import Accounts"
                                OnClientClick="otherPopup();return false;" />
                        </td>
                        <td style="padding-left: 20px">
                            <asp:LinkButton ID="btnExport" runat="server" Text="Export to Excel" OnClick="btnExport_Click"
                                CssClass=" excel marginRight" />
                        </td>
                    </tr>
                </table>
            </div>
            <ext:Window ID="WindowLevel" runat="server" Title="Add/Edit Document" Icon="Application"
                Width="450" Height="280" BodyPadding="5" Hidden="true" Modal="true">
                <Content>
                    <div class="bevel">
                        <div class="fields">
                            <table>
                                <tr id="rowDesc" runat="server">
                                    <td style='width: 60px!important; vertical-align: top'>
                                        Description
                                    </td>
                                    <td>
                                        <%--<asp:TextBox Width="200px"  Rows="3" ID="txtDesc" runat="server" TextMode="MultiLine" />--%>
                                        <ext:TextArea Width="300" Height="100" ID="txtDesc" runat="server">
                                        </ext:TextArea>
                                    </td>
                                </tr>
                                <tr style="height: 50px;">
                                    <td style='width: 100px!important'>
                                        Select File
                                    </td>
                                    <td>
                                        <%--<asp:FileUpload ID="fup" Width="300px" CssClass="file" runat="server" />--%>
                                        <ext:FileUploadField ID="fup" runat="server" Width="300" Icon="Attach" />
                                        <asp:RequiredFieldValidator ValidationGroup="File" ControlToValidate="fup" Display="None"
                                            Text="Please select a file." ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please select a file."></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr style="height: 30px;">
                                    <td style='width: 100px!important'>
                                        <%--Type--%>
                                    </td>
                                    <td>
                                        <%--<ext:ComboBox Width="300"  ID ="cmbType" runat="server"></ext:ComboBox>--%>
                                    </td>
                                </tr>
                                <tr style="width: 300px;">
                                    <td style="width: 150px;">
                                        <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="LinkButton1" Cls="btn btn-default btn-icon glyphicons share"
                                            Text="<i></i>Cancel" runat="server">
                                            <Listeners>
                                                <Click Handler="#{WindowLevel}.hide();">
                                                </Click>
                                            </Listeners>
                                        </ext:LinkButton>
                                    </td>
                                    <td style='text-align: right; width: 150px;'>
                                        <%--<asp:Button ID="btnUpload"   OnClientClick="valGroup='File';return CheckValidation();"   CssClass="update marginleft" runat="server" OnClick="btnUpload_Click"
                                        Text="Upload" />--%>
                                        <ext:LinkButton runat="server" StyleSpec="padding:0px; margin-left:10px;" ID="btnUpload"
                                            Cls="btn btn-primary btn-icon glyphicons ok_2" Text="<i></i>Upload" runat="server">
                                            <DirectEvents>
                                                <Click OnEvent="btnLevelSaveUpdate_Click">
                                                    <EventMask ShowMask="true" />
                                                </Click>
                                            </DirectEvents>
                                            <Listeners>
                                                <Click Handler="valGroup = 'SaveUpdateLevel'; return CheckValidation();">
                                                </Click>
                                            </Listeners>
                                        </ext:LinkButton>
                                    </td>
                                </tr>
                            </table>
                            <%--<asp:ValidationSummary runat="server" ShowSummary="false" ShowMessageBox="true" />--%>
                        </div>
                    </div>
                </Content>
            </ext:Window>
        </div>
    </div>
    <br />
</asp:Content>
