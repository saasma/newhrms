﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using Utils.Helper;
using System.IO;
using Utils;
using Utils.Security;

namespace Web.NewHR
{
    public partial class EmployeeCatalogueList : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.HumanResource;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest && !IsPostBack)
            {
                Initialise();
            }
        }

        public void Initialise()
        {
           
            // Disable all emp export/import once salary is saved           

            List<Branch> branchList = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            cmbBranch.Store[0].DataSource = branchList;
            cmbBranch.Store[0].DataBind();

            cmbDepartment.Store[0].DataSource = DepartmentManager.GetAllDepartments().OrderBy(x => x.Name).ToList();
            cmbDepartment.Store[0].DataBind();

            cmbDesignation.Store[0].DataSource = new CommonManager().GetAllDesignations().OrderBy(x => x.Name).ToList();
            cmbDesignation.Store[0].DataBind();

            this.storeLevel.DataSource = ActivityManager.GetLevels();
            this.storeLevel.DataBind();

            JobStatus statues = new JobStatus();
            storeEmpStatus.DataSource = statues.GetMembersForHirarchyView();

            storeEmployeeList.DataSource = EmployeeManager.GetAllEmployeesForEmployeeImportExport().OrderBy(x => x.Name).ToList();
            storeEmployeeList.DataBind();

            cmbBranch.SelectedItem.Index = 0;
            cmbDepartment.SelectedItem.Index = 0;
            cmbDesignation.SelectedItem.Index = 0;
            cmbEmpStatus.SelectedItem.Index = 0;
            cmbLevel.SelectedItem.Index = 0;

            JavascriptHelper.AttachNonDialogPopUpCode(Page, "employeePopup", "../ExcelWindow/EmployeeExcel.aspx", 450, 500);
            JavascriptHelper.AttachNonDialogPopUpCode(Page, "otherPopup", "../ExcelWindow/OtherEmployeeImport.aspx", 450, 500);

            btnEmpCatDetl_Click(null, null);
        }

        protected void btnEmpCatDetl_Click(object sender, DirectEventArgs e)
        {
            Hidden hdnBranchId = (Hidden)ucEmpCat.FindControl("hdnBranchId");
            if (cmbBranch.SelectedItem.Value != null && cmbBranch.SelectedItem.Index != -1 && !string.IsNullOrEmpty(cmbBranch.SelectedItem.Value.Trim()))
                hdnBranchId.Text = cmbBranch.SelectedItem.Value;
            else
                hdnBranchId.Text = "-1";

            Hidden hdnDepartmentId = (Hidden)ucEmpCat.FindControl("hdnDepartmentId");
            if (cmbDepartment.SelectedItem.Value != null && cmbDepartment.SelectedItem.Index != -1 && !string.IsNullOrEmpty(cmbDepartment.SelectedItem.Value.Trim()))
                hdnDepartmentId.Text = cmbDepartment.SelectedItem.Value;
            else
                hdnDepartmentId.Text = "-1";

            Hidden hdnLevelId = (Hidden)ucEmpCat.FindControl("hdnLevelId");
            if (cmbLevel.SelectedItem.Value != null && cmbLevel.SelectedItem.Index != -1 && !string.IsNullOrEmpty(cmbLevel.SelectedItem.Value.Trim()))
                hdnLevelId.Text = cmbLevel.SelectedItem.Value;
            else
                hdnLevelId.Text = "-1";

            Hidden hdnDesignationId = (Hidden)ucEmpCat.FindControl("hdnDesignationId");
            if (cmbDesignation.SelectedItem.Value != null && cmbDesignation.SelectedItem.Index != -1 && !string.IsNullOrEmpty(cmbDesignation.SelectedItem.Value.Trim()))
                hdnDesignationId.Text = cmbDesignation.SelectedItem.Value;
            else
                hdnDesignationId.Text = "-1";

            Hidden hdnStatusId = (Hidden)ucEmpCat.FindControl("hdnStatusId");
            Hidden hdnMinStatusOnly = (Hidden)ucEmpCat.FindControl("hdnMinStatusOnly");
            if (cmbEmpStatus.SelectedItem.Value != null && cmbEmpStatus.SelectedItem.Index != -1 && !string.IsNullOrEmpty(cmbEmpStatus.SelectedItem.Value.Trim()))
            {
                hdnStatusId.Text = cmbEmpStatus.SelectedItem.Value;
                if (cmbEmpStatus.SelectedItem.Text.ToLower().Contains("only"))
                    hdnMinStatusOnly.Text = "true";
                else
                    hdnMinStatusOnly.Text = "false";
            }
            else
            {
                hdnStatusId.Text = "-1";
                hdnMinStatusOnly.Text = "false";
            }

            Hidden hdnEmpSearch = (Hidden)ucEmpCat.FindControl("hdnEmpSearch");
            if (!string.IsNullOrEmpty(cmbEmployee.SelectedItem.Text))
                hdnEmpSearch.Text = Convert.ToString(cmbEmployee.SelectedItem.Text).Trim();  

            Hidden hdnINo = (Hidden)ucEmpCat.FindControl("hdnINo");
            if (!string.IsNullOrEmpty(txtINO.Text))
                hdnINo.Text = Convert.ToString(txtINO.Text).Trim();


            X.Js.Call("searchList");
            
        }

    }
}