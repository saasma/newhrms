﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using Utils.Helper;
using System.IO;
using Utils;
using Utils.Security;

namespace Web.NewHR
{
    public partial class LeaveSpecificFlow : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialize();
            }

            JavascriptHelper.AttachPopUpCode(Page, "leaveSpecImport", "../ExcelWindow/EmployeeSpecificSupervisorExcel.aspx", 450, 500);
        }

        private void Initialize()
        {            
            //LoadBranchCombos();
            //LoadDepartmentFilterCombo();
            BindEmployeeCombos();
        }

        private void Clear()
        {
            cmbEmployee.Clear();
            cmbEmployeeRecommender1.Clear();
            cmbEmployeeRecommender2.Clear();
            cmbEmployeeApproval1.Clear();
            cmbEmployeeApproval2.Clear();
            storeEmployeeAdd.ClearFilter();
        }

        //private void LoadBranchCombos()
        //{
        //    cmbBranchFilter.GetStore().DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
        //    cmbBranchFilter.GetStore().DataBind();

        //    cmbBranch.GetStore().DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
        //    cmbBranch.GetStore().DataBind();
        //}

        //private void LoadDepartmentFilterCombo()
        //{
        //    cmbDepartmentFilter.GetStore().DataSource = DepartmentManager.GetAllDepartments();
        //    cmbDepartmentFilter.GetStore().DataBind();
        //}

        private void BindEmployeeCombos()
        {
            storeEmployee.DataSource = EmployeeManager.GetAllActiveEmployees();
            storeEmployee.DataBind();
        }

        protected void Branch_Select(object sender, DirectEventArgs e)
        {
            //cmbDepartment.ClearValue();
            //Clear();
           //// LoadDepartment(int.Parse(cmbBranch.SelectedItem.Value));
        }       

        private void LoadDepartment(int branchId)
        {
           // Clear();
           // cmbDepartment.GetStore().DataSource = DepartmentManager.GetAllDepartmentsByBranch(branchId);
         //   cmbDepartment.GetStore().DataBind();
        }

        protected void Department_Select(object sender, DirectEventArgs e)
        {
           // LoadEmployeesByBranchDepartment(int.Parse(cmbBranch.SelectedItem.Value), int.Parse(cmbDepartment.SelectedItem.Value));
        }

        private void LoadEmployeesByBranchDepartment(int branchId, int departmentId)
        {
            storeEmployeeAdd.DataSource = EmployeeManager.GetAllActiveEmployees();
            storeEmployeeAdd.DataBind();
        }

        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            int totalRecords = 0;

            int employeeId = -1, branchId = -1, departmentId = -1, recommenderId = -1, approvalId = -1, type = 0;

            int.TryParse(cmbEmpSearch.SelectedItem.Value, out employeeId);
           // int.TryParse(cmbBranchFilter.SelectedItem.Value, out branchId);

           // int.TryParse(cmbDepartmentFilter.SelectedItem.Value, out departmentId);
            int.TryParse(cmbRecommenderFilter.SelectedItem.Value, out recommenderId);
            int.TryParse(cmbApprovalFilter.SelectedItem.Value, out approvalId);
            int.TryParse(cmbTypeFilter.SelectedItem.Value, out type);

            if (type == 0)
            {
                NewMessage.ShowWarningMessage("Type is required.");
                cmbTypeFilter.Focus();
                return;
            }

            if (employeeId == 0)
                employeeId = -1;

            if (branchId == 0)
                branchId = -1;

            if (departmentId == 0)
                departmentId = -1;

            if (recommenderId == 0)
                recommenderId = -1;

            if (approvalId == 0)
                approvalId = -1;

            List<GetEmpSpecificLeaveDefListResult> list = NewHRManager.GetEmpSpecLeaveDefList(e.Start, int.Parse(cmbPageSize.SelectedItem.Value), type, employeeId, branchId, departmentId,
                recommenderId, approvalId, ref totalRecords);

            e.Total = totalRecords;

            if (list.Any())
                storeLeaveSpecEmpList.DataSource = list;
            storeLeaveSpecEmpList.DataBind();

        }

        protected void btnAddNew_Click(object sender, DirectEventArgs e)
        {
            if (cmbTypeFilter.SelectedItem == null || string.IsNullOrEmpty(cmbTypeFilter.SelectedItem.Value))
            {
                NewMessage.ShowWarningMessage("Type is required.");
                cmbTypeFilter.Focus();
                return;
            }
             Clear();
            LoadEmployeesByBranchDepartment(0,0);
            btnSave.Enable();
            hdnEmployeeId.Text = "";
            dispType.Text = cmbTypeFilter.SelectedItem.Text;
            winLeaveSpec.Show();
        }

        protected void btnEdit_Click(object sender, DirectEventArgs e)
        {
            Clear();
            btnSave.Enable();
            int employeeId = int.Parse(hdnEmployeeId.Text);

            LeaveSpecificEmployee obj = NewHRManager.GetLeaveSpecificEmpById(employeeId, int.Parse(cmbTypeFilter.SelectedItem.Value));
            if (obj != null)
            {
                EEmployee objEEmployee = EmployeeManager.GetEmployeeById(obj.EmployeeId);
                //cmbBranch.SetValue(objEEmployee.BranchId.Value.ToString());

                LoadDepartment(objEEmployee.BranchId.Value);

               // cmbDepartment.SetValue(objEEmployee.DepartmentId.Value.ToString());

                LoadEmployeesByBranchDepartment(objEEmployee.BranchId.Value, objEEmployee.DepartmentId.Value);

                cmbEmployee.SetValue(obj.EmployeeId.ToString());
                if (obj.Recommender1 != null)
                    cmbEmployeeRecommender1.SetValue(obj.Recommender1.Value.ToString());

                if (obj.Recommender2 != null)
                    cmbEmployeeRecommender2.SetValue(obj.Recommender2.Value.ToString());

                if (obj.Approval1 != null)
                    cmbEmployeeApproval1.SetValue(obj.Approval1.Value.ToString());

                if (obj.Approval2 != null)
                    cmbEmployeeApproval2.SetValue(obj.Approval2.Value.ToString());


            }
            else
            {
                EEmployee objEEmployee = EmployeeManager.GetEmployeeById(employeeId);
               // cmbBranch.SetValue(objEEmployee.BranchId.Value.ToString());

                LoadDepartment(objEEmployee.BranchId.Value);

               // cmbDepartment.SetValue(objEEmployee.DepartmentId.Value.ToString());

                LoadEmployeesByBranchDepartment(objEEmployee.BranchId.Value, objEEmployee.DepartmentId.Value);

                cmbEmployee.SetValue(objEEmployee.EmployeeId.ToString());
            }

            dispType.Text = cmbTypeFilter.SelectedItem.Text;

            winLeaveSpec.Show();
        }

        protected void btnDelete_Click(object sender, DirectEventArgs e)
        {
            int employeeId = int.Parse(hdnEmployeeId.Text);

            Status status = NewHRManager.DeleteLeaveSpecificEmployee(employeeId, int.Parse(cmbTypeFilter.SelectedItem.Value));
            if (status.IsSuccess)
            {
                if (cmbTypeFilter.SelectedItem.Value == "1")
                    NewMessage.ShowNormalMessage("Employee leave request definition deleted successfully.");
                else
                    NewMessage.ShowNormalMessage("Employee overtime request definition deleted successfully.");

                X.Js.Call("searchList");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveLeaveSp");
            if (Page.IsValid)
            {
                LeaveSpecificEmployee obj = new LeaveSpecificEmployee();

                obj.EmployeeId = int.Parse(cmbEmployee.SelectedItem.Value);
                obj.Type = int.Parse(cmbTypeFilter.SelectedItem.Value);

                int recommender1 = -1, recommender2 = -1, approval1 = -1, approval2 = -1;

                int.TryParse(cmbEmployeeRecommender1.SelectedItem.Value, out recommender1);
                int.TryParse(cmbEmployeeRecommender2.SelectedItem.Value, out recommender2);
                int.TryParse(cmbEmployeeApproval1.SelectedItem.Value, out approval1);
                int.TryParse(cmbEmployeeApproval2.SelectedItem.Value, out approval2);

                if (recommender1 == 0 && recommender2 == 0)
                {
                    NewMessage.ShowWarningMessage("Recommender is required.");
                    cmbEmployeeRecommender1.Focus();
                    return;
                }

                if (approval1 == 0 && approval2 == 0)
                {
                    NewMessage.ShowWarningMessage("Approval is required.");
                    cmbEmployeeApproval1.Focus();
                    return;
                }

                if (recommender1 != 0)
                    obj.Recommender1 = recommender1;
                if (recommender2 != 0)
                    obj.Recommender2 = recommender2;
                if (approval1 != 0)
                    obj.Approval1 = approval1;
                if (approval2 != 0)
                    obj.Approval2 = approval2;

                Status status = NewHRManager.SaveUpdateLeaveSpecificEmployee(obj);
                if (status.IsSuccess)
                {
                    if(cmbTypeFilter.SelectedItem.Value == "1")
                        NewMessage.ShowNormalMessage("Employee leave request definition saved successfully.");
                    else
                        NewMessage.ShowNormalMessage("Employee overtime request definition saved successfully.");

                    winLeaveSpec.Close();

                    X.Js.Call("searchList");
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }

            }
        }

    }
}