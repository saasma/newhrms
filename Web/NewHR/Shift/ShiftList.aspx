﻿<%@ Page Title="Shift List" Language="C#" MasterPageFile="~/Master/NewDetails.Master "
    AutoEventWireup="true" CodeBehind="ShiftList.aspx.cs" Inherits="Web.NewHR.ShiftList" %>

<asp:Content ID="content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        var winShift = null;
        var storeShift = null;
        var gridShift = null;
        var btnDelete = null;

        var afterEdit = function (e1, e2) {
            e2.record.commit();
        }
        var commandHandler = function (command, record) {
            <%= hiddenValueWorkShiftID.ClientID %>.setValue(record.data.WorkShiftId);
            if (command == "delete") {
                Ext.Msg.confirm('Confirm','Are you sure you want to delete this shift?', function(btn){
                    if(btn=='yes'){
                        <%= btnDeleteShift.ClientID %>.fireEvent('click');
                    }
                });
            }
            else { 
                <%= btnEditShift.ClientID %>.fireEvent('click');
            }
        };

        var RemoveShift = function(workShiftId){
          var storeShift = <%=storeShift.ClientID %>;
          var shiftIndex = storeShift.indexOfId(workShiftId);
          var shift = storeShift.getAt(shiftIndex);

          storeShift.remove(shift);
        };

        var renderTotHour= function(start, end, type){
            
            start = start.split(":");
            end = end.split(":");
            var startDate = new Date(0, 0, 0, start[0], start[1], 0);            
            var endDate = new Date(0, 0, 0, end[0], end[1], 0);

            if(type=="2"){
                endDate = new Date(0,0,1, end[0], end[1],0)
            }
            
            var diff = endDate.getTime() - startDate.getTime();
            var hours = Math.floor(diff / 1000 / 60 / 60);
            diff -= hours * 1000 * 60 * 60;
            var minutes = Math.floor(diff / 1000 / 60);
            
            return (hours <= 9 ? "0" : "") + hours + ":" + (minutes <= 9 ? "0" : "") + minutes;
        };

        
        var CalculateEffectiveHour = function(){
            Ext.net.DirectMethods.CalculateEffeciveHours();
        };
    </script>
</asp:Content>
<asp:Content ID="content2" runat="server" ContentPlaceHolderID="ContentPlaceHolder_Main">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Manage Shifts
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="innerLR">
         <div runat="server" style="color: red;" id="divMessage">
            One of the shift must be defined as Default for late processing and other leave reports </div>
            <ext:Store ID="storeShift" runat="server">
                <Model>
                    <ext:Model ID="modelShift" runat="server" Name="shiftItem" IDProperty="WorkShiftId">
                        <Fields>
                            <ext:ModelField Name="WorkShiftId" Type="Int" />
                            <ext:ModelField Name="CompanyId" Type="Int" />
                            <ext:ModelField Name="Name" Type="String" />
                            <ext:ModelField Name="ShortName" Type="String" />
                            <ext:ModelField Name="Type" Type="Int" />
                            <ext:ModelField Name="ShiftStart" />
                            <ext:ModelField Name="LunchOut" />
                            <ext:ModelField Name="LunchBack" />
                            <ext:ModelField Name="ShiftEnd" />
                            <ext:ModelField Name="IsDefault"  Type="Boolean"/>
                        </Fields>
                    </ext:Model>
                </Model>
            </ext:Store>
            <ext:LinkButton ID="btnEditShift" runat="server" Hidden="true">
                <DirectEvents>
                    <Click OnEvent="btnEditShift_Click">
                        <EventMask ShowMask="true" />
                    </Click>
                </DirectEvents>
            </ext:LinkButton>
            <ext:LinkButton ID="btnDeleteShift" runat="server" Hidden="true">
                <DirectEvents>
                    <Click OnEvent="btnDeleteShift_Click">
                        <EventMask ShowMask="true" />
                    </Click>
                </DirectEvents>
            </ext:LinkButton>
            <ext:Hidden ID="hiddenValueWorkShiftID" runat="server" />
            <ext:GridPanel ID="gridShift" runat="server" Header="false" Width="960" Title="Shifts"
                StoreID="storeShift" MinHeight="0" AutoScroll="true">
                <ColumnModel>
                    <Columns>
                        <ext:Column ID="colShiftName" runat="server" Flex="1" Text="Shift Name" DataIndex="Name">
                        </ext:Column>
                        <ext:Column ID="colShortName" runat="server" Flex="1" Text="Short Name" DataIndex="ShortName">
                        </ext:Column>
                        <ext:Column ID="colType" runat="server" Flex="1" Text="Type" DataIndex="Type">
                            <Renderer Handler="if(value=='1'){
                                            return 'Regular';
                                       } else {
                                            return 'Overnight';
                                       }" />
                        </ext:Column>
                          <ext:Column ID="Column1" runat="server" Flex="1" Text="Default" DataIndex="IsDefault">
                            <Renderer Handler="if(value==true){
                                            return 'Yes';
                                       } else {
                                            return '';
                                       }" />
                        </ext:Column>
                        <ext:Column ID="colShiftStart" runat="server" Flex="1" Text="Shift Starts" DataIndex="ShiftStart">
                        </ext:Column>
                        <ext:Column ID="colLaunchOut" runat="server" Flex="1" Text="Lunch Out" DataIndex="LunchOut">
                        </ext:Column>
                        <ext:Column ID="colLaunchBack" runat="server" Flex="1" Text="Lunch Back" DataIndex="LunchBack">
                        </ext:Column>
                        <ext:Column ID="colShiftEnd" runat="server" Flex="1" Text="Shift Ends" DataIndex="ShiftEnd">
                        </ext:Column>
                        <ext:Column Hidden="true" ID="colTotHour" runat="server" Flex="1" Text="Total Hour">
                            <Renderer Handler="return renderTotHour(record.data.ShiftStart, record.data.ShiftEnd, record.data.Type)" />
                        </ext:Column>
                        <ext:CommandColumn  TdCls="centerButtons"  ID="cmdCol1" runat="server" Width="75">
                            <Commands>
                                <ext:GridCommand Cls="editGridButton" ToolTip-Text="Edit"  Text="<i class='fa fa-pencil'></i>" CommandName="edit" />
                            <ext:GridCommand Cls="editGridButton" ToolTip-Text="Delete"  Text="<i class='fa fa-trash-o'></i>"
                                CommandName="delete" />
                            </Commands>
                            <Listeners>
                                <Command Handler="commandHandler(command, record);" />
                            </Listeners>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
                <SelectionModel>
                    <ext:CellSelectionModel ID="cellSelection1" runat="server">
                    </ext:CellSelectionModel>
                </SelectionModel>
                <Plugins>
                    <ext:CellEditing ID="cellEditing1" runat="server" ClicksToEdit="1">
                        <Listeners>
                            <Edit Fn="afterEdit" />
                        </Listeners>
                    </ext:CellEditing>
                </Plugins>
            </ext:GridPanel>
            <ext:Button ID="btnAddShift" Cls="btn btn-primary" runat="server" Width="120" StyleSpec="margin-top:10px"
                Height="30" Text="Add Shift" >
                <DirectEvents>
                    <Click OnEvent="btnAddShift_Click">
                        <EventMask ShowMask="true" />
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Window ID="winShift" runat="server" Title="Create/Update Shifts" Hidden="true"
                Width="600" Height="300">
                <Content>
                    <table class="fieldTable firsttdskip" style="margin: 10px">
                        <tr>
                            <td>
                                <ext:TextField ID="txtShiftName" runat="server" FieldLabel="Shift Name" LabelSeparator=""
                                    LabelAlign="Top" Width="220">
                                </ext:TextField>
                                <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator6" runat="server"
                                    ValidationGroup="SaveUpdate" ControlToValidate="txtShiftName" ErrorMessage="Shift name is required." />
                            </td>
                            <td>
                                <ext:TextField ID="txtShortName" runat="server" FieldLabel="Short Name" LabelAlign="Top"
                                    LabelSeparator="" Width="220">
                                </ext:TextField>
                                <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator1" runat="server"
                                    ValidationGroup="SaveUpdate" ControlToValidate="txtShortName" ErrorMessage="Shift short name is required." />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <ext:ComboBox ID="cmbShiftType" runat="server" FieldLabel="Shift Type" LabelAlign="Top"
                                    LabelSeparator="" Width="220" ForceSelection="true">
                                    <Items>
                                        <ext:ListItem Text="Regular" Value="1">
                                        </ext:ListItem>
                                        <ext:ListItem Text="Overnight" Value="2">
                                        </ext:ListItem>
                                    </Items>
                                    <SelectedItems>
                                        <ext:ListItem Value="1" />
                                    </SelectedItems>
                                </ext:ComboBox>
                                <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator2" runat="server"
                                    ValidationGroup="SaveUpdate" ControlToValidate="cmbShiftType" ErrorMessage="Shift type is required." />
                            </td>
                            <td>
                                <ext:ComboBox ID="cmbColor" runat="server" DisplayField="ColorName" ValueField="ColorName"
                                    QueryMode="Local" FieldLabel="Color" LabelAlign="Top" LabelSeparator="" ForceSelection="true"
                                    Width="220">
                                    <Store>
                                        <ext:Store ID="storeColor" runat="server">
                                            <Model>
                                                <ext:Model ID="modelColor" runat="server" Name="colorItem">
                                                    <Fields>
                                                        <ext:ModelField Name="ColorCode" Type="String" />
                                                        <ext:ModelField Name="ColorName" Type="String" />
                                                    </Fields>
                                                </ext:Model>
                                            </Model>
                                        </ext:Store>
                                    </Store>
                                    <ListConfig>
                                        <ItemTpl ID="itemTplColor" runat="server">
                                            <Html>
                                                <table>
                                            <tr>
                                                <td style="width:150px;">{ColorName}</td>
                                                <td style="width:30px; background-color:{ColorCode}">&nbsp;</td>
                                            </tr>
                                        </table>
                                            </Html>
                                        </ItemTpl>
                                    </ListConfig>
                                    <Listeners>
                                        <Change Handler="if(this.valueModels.length>0) { this.setIconCls(this.valueModels[0].get('iconCls'));}" />
                                    </Listeners>
                                </ext:ComboBox>
                                <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator3" runat="server"
                                    ValidationGroup="SaveUpdate" ControlToValidate="cmbColor" ErrorMessage="Color is required." />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <ext:Checkbox ID="chkIsDefault" runat="server" FieldLabel="Is Default" LabelAlign="Top">
                                </ext:Checkbox>
                            </td>
                        </tr>
                        <%--<tr>
                    <td>
                        <ext:TimeField ID="timeFieldStart" runat="server" MinTime="0:00" MaxTime="0:00" Increment="30"
                            SelectedTime="6:00" Format="hh:mm tt" Width="220" LabelAlign="Top" FieldLabel="Shift Start"
                            LabelSeparator="">
                            <Listeners>
                                <Change Fn="CalculateEffectiveHour" />
                            </Listeners>
                        </ext:TimeField>
                    </td>
                    <td>
                        <ext:TimeField ID="timeFieldLunchOut" runat="server" MinTime="0:00" MaxTime="0:00"
                            Increment="30" SelectedTime="10:00" Format="hh:mm tt" Width="220" LabelAlign="Top"
                            FieldLabel="Lunch Out" LabelSeparator="">
                            <Listeners>
                                <Change Fn="CalculateEffectiveHour" />
                            </Listeners>
                        </ext:TimeField>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:TimeField ID="timefieldEnd" runat="server" MinTime="0:00" MaxTime="0:00" SelectedTime="0:00"
                            Increment="30" Format="hh:mm tt" Width="220" LabelAlign="Top" FieldLabel="Shift End"
                            LabelSeparator="">
                            <Listeners>
                                <Change Fn="CalculateEffectiveHour" />
                            </Listeners>
                        </ext:TimeField>
                    </td>
                    <td>
                        <ext:TimeField ID="timeFieldLunchBack" runat="server" MinTime="0:00" MaxTime="0:00"
                            Increment="30" SelectedTime="10:00" Format="hh:mm tt" Width="220" LabelAlign="Top"
                            FieldLabel="Lunch Back" LabelSeparator="">
                            <Listeners>
                                <Change Fn="CalculateEffectiveHour" />
                            </Listeners>
                        </ext:TimeField>
                    </td>
                </tr>
                <tr>
                    <td>
                        Effective work hours: <ext:Label ID="lblTotal" runat="server" Text="0 hr 0 min"></ext:Label>
                    </td>
                </tr>--%>
                        <tr>
                            <td colspan="2">
                                <div class="popupButtonDiv">
                                    <ext:Button ID="btnSave1" Cls="btn btn-primary" 
                                        runat="server" Text="Save">
                                        <DirectEvents>
                                            <Click OnEvent="btnSave_Click">
                                                <EventMask ShowMask="true" />
                                            </Click>
                                        </DirectEvents>
                                        <Listeners>
                                            <Click Handler="valGroup = 'SaveUpdate'; return CheckValidation();">
                                            </Click>
                                        </Listeners>
                                    </ext:Button>
                                    <div class="btnFlatOr">
                                        or</div>
                                    <ext:LinkButton ID="btnCancel1" StyleSpec="padding:0px;" runat="server" Text="Cancel"
                                        Cls="btnFlatLeftGap">
                                        <Listeners>
                                            <Click Handler="#{winShift}.hide();" />
                                        </Listeners>
                                    </ext:LinkButton>
                                </div>
                            </td>
                        </tr>
                    </table>
                </Content>
            </ext:Window>
        </div>
    </div>
</asp:Content>
