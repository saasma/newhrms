﻿<%@ Page Title="Shift Group" MasterPageFile="~/Master/HR.Master" Language="C#" AutoEventWireup="true" CodeBehind="ShiftGroup.aspx.cs" Inherits="Web.NewHR.ShiftGroup" %>

<asp:Content ID="content1" runat="server" ContentPlaceHolderID="head">
    
</asp:Content>

<asp:Content ID="content2" runat="server" ContentPlaceHolderID="ContentPlaceHolder_Main">
    <ext:Store ID="storeShiftGroup" runat="server">
        <Model>
            <ext:Model ID="modelShiftGroup" runat="server">
                <Fields>
                    <ext:ModelField Name="ShiftGroupID" Type="Int" />
                    <ext:ModelField Name="Name" Type="String" />
                    <ext:ModelField Name="EmpNo" Type="String" />
                </Fields>
            </ext:Model>
        </Model>
    </ext:Store>

    <ext:GridPanel ID="gridShiftGroup" runat="server" Width="700" StoreID="storeShiftGroup">
        <ColumnModel>
            <Columns>
                <ext:Column ID="colGroupName" runat="server" Text="Group Name" Flex="1" DataIndex="Name"></ext:Column>
                <ext:Column ID="colEmpNo" runat="server" Text="Employees in the Group" Flex="1" DataIndex="EmpNo"></ext:Column>
                <ext:CommandColumn ID="cmdCol1" runat="server" Width="75">
                    <Commands>
                        <ext:GridCommand CommandName="edit" Icon="PageEdit" ToolTip-Text="Edit"></ext:GridCommand>
                        <ext:GridCommand CommandName="delete" Icon="PageDelete" ToolTip-Text="Delete"></ext:GridCommand>
                    </Commands>
                </ext:CommandColumn>
            </Columns>
        </ColumnModel>
    </ext:GridPanel>
</asp:Content>
