﻿<%@ Page Title="Change Shift Type" MasterPageFile="~/Master/NewDetails.Master" Language="C#"
    AutoEventWireup="true" CodeBehind="ChangeShiftType.aspx.cs" Inherits="Web.NewHR.ChangeShiftType" %>

<%@ Register Src="~/NewHR/UserControls/ChangeEmpShiftCtrl.ascx" TagName="ChangeShiftTypeCtrl" TagPrefix="uc1" %>

<asp:Content ID="content1" runat="server" ContentPlaceHolderID="head">

</asp:Content>
<asp:Content ID="content2" runat="server" ContentPlaceHolderID="ContentPlaceHolder_Main">
<div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Change Employee Shift Schedule
                </h4>
            </div>
        </div>
    </div>
     <uc1:ChangeShiftTypeCtrl Id="ChangeShiftCtrl" runat="server" />
 
</asp:Content>
