﻿<%@ Page Title="Shift-Employee Relation" MasterPageFile="~/Master/NewDetails.Master"
    Language="C#" AutoEventWireup="true" CodeBehind="ShiftEmployeeRelation.aspx.cs"
    Inherits="Web.NewHR.ShiftEmployeeRelation" %>

<asp:Content ID="content1" runat="server" ContentPlaceHolderID="head">
    <script type="text/javascript">

        var storeShift = null;
        var gridEmpShift = null;
        var storeEmpShift = null;

        var afterEdit = function (e1, e2) {
            e2.record.commit();
        };

        var setShiftType = function (value, rowIndex) {
            storeEmpShift = <%= storeEmpShift.ClientID %>;
            storeShift = <%= storeShift.ClientID %>;

            var currentRow = storeEmpShift.data.items[rowIndex];
            var row = storeShift.getById(value.toString().toLowerCase());
            if (row != null && typeof (row) != 'undefined')
                currentRow.data.WorkShiftId = row.data.WorkShiftId.toString();   

            currentRow.commit();
        };
        
        var shiftTypeRenderer = function(value, metaData, record, rowIndex, colIndex, store){
            var r = <%= storeShift.ClientID %>.getById(value.toString().toLowerCase());
            if(Ext.isEmpty(r)){
                    return "";
                };
            return r.data.Name;
        };

        var renderShiftStart = function(workShiftId) {
            storeShift = <%= storeShift.ClientID %>;
            var row = storeShift.getById(workShiftId.toString());
            return row.data.ShiftStart.toString();

        };
          
        var renderShiftEnd = function(workShiftId) {
            storeShift = <%= storeShift.ClientID %>;
            var row = storeShift.getById(workShiftId.toString());
            return row.data.ShiftEnd.toString();
        };

        var renderTotHour= function(workShiftId){
            
            storeShift = <%= storeShift.ClientID %>;
            var row = storeShift.getById(workShiftId.toString());
            var type = row.data.Type.toString();
            var start = row.data.ShiftStart.toString();
            var end = row.data.ShiftEnd.toString();



            start = start.split(":");
            end = end.split(":");
            var startDate = new Date(0, 0, 0, start[0], start[1], 0);            
            var endDate = new Date(0, 0, 0, end[0], end[1], 0);

            if(type=="2"){
                endDate = new Date(0,0,1, end[0], end[1],0)
            }
            
            var diff = endDate.getTime() - startDate.getTime();
            var hours = Math.floor(diff / 1000 / 60 / 60);
            diff -= hours * 1000 * 60 * 60;
            var minutes = Math.floor(diff / 1000 / 60);
            
            return (hours <= 9 ? "0" : "") + hours + ":" + (minutes <= 9 ? "0" : "") + minutes;
        };

        var onShiftFocus= function(rowIndex){
            storeEmpShift =<%= storeEmpShift.ClientID %>;
            var currentRow = storeEmpShift.data.items[rowIndex];

            <%= cmbShift.ClientID %>.setValue(currentRow.data.WorkShiftId.toString());
        };

        var onScheduleFocus = function(rowIndex){
            storeEmpShift = <%= storeEmpShift.ClientID %>;
            var currentRow = storeEmpShift.data.items[rowIndex];

            <%= cmbSchedule.ClientID %>.setValue(currentRow.data.Schedule.toString());
        };
    </script>
</asp:Content>
<asp:Content ID="content2" runat="server" ContentPlaceHolderID="ContentPlaceHolder_Main">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Employee Shift
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="separator bottom">
        </div>
        <div class="innerLR">
            <ext:Store ID="storeEmpShift" runat="server">
                <Model>
                    <ext:Model ID="modelEmpShift" runat="server">
                        <Fields>
                            <ext:ModelField Name="BranchName" Type="String" />
                            <ext:ModelField Name="DepartmentName" Type="String" />
                            <ext:ModelField Name="EmployeeId" Type="Int" />
                            <ext:ModelField Name="Name" Type="String" />
                            <ext:ModelField Name="WorkShiftId" Type="Int" />
                            <ext:ModelField Name="Schedule" Type="Int" />
                            <ext:ModelField Name="WorkShiftHistoryId" Type="Int" />
                            <ext:ModelField Name="DefaultWorkShiftId" Type="Int" />
                        </Fields>
                    </ext:Model>
                </Model>
            </ext:Store>
            <ext:Store ID="storeShift" runat="server">
                <Model>
                    <ext:Model ID="modelShift" runat="server" IDProperty="WorkShiftId">
                        <Fields>
                            <ext:ModelField Name="WorkShiftId" Type="String" />
                            <ext:ModelField Name="ShiftStart" />
                            <ext:ModelField Name="ShiftEnd" />
                            <ext:ModelField Name="Type" Type="Int" />
                            <ext:ModelField Name="Name" Type="String" />
                        </Fields>
                    </ext:Model>
                </Model>
            </ext:Store>
            <table>
                <tr>
                    <td style='width: 160px'>
                        <ext:DateField ID="dfFromDate" runat="server" FieldLabel="From Date" Width="150"
                            LabelAlign="Top" LabelSeparator="" LabelWidth="80">
                        </ext:DateField>
                    </td>
                    <td style="padding-left: 10px">
                        <ext:ComboBox ID="cmbBranch" runat="server" ValueField="BranchId" DisplayField="Name"
                            LabelAlign="Top" LabelSeparator="" FieldLabel="Branch" ForceSelection="true"
                            LabelWidth="50" QueryMode="Local">
                            <DirectEvents>
                                <Select OnEvent="cmbBranch_Change">
                                    <EventMask ShowMask="true" />
                                </Select>
                            </DirectEvents>
                            <Store>
                                <ext:Store ID="Store5" runat="server">
                                    <Model>
                                        <ext:Model ID="Model5" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="BranchId" />
                                                <ext:ModelField Name="Name" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator15" runat="server"
                            ValidationGroup="InsertUpdate" ControlToValidate="cmbBranch" ErrorMessage="Please select a Branch for the employee." />
                    </td>
                    <td style="padding-left: 10px">
                        <ext:ComboBox ForceSelection="true" QueryMode="Local" ID="cmbDepartment" FieldLabel="Department"
                            runat="server" LabelAlign="Top" DisplayField="Name" Width="180" LabelSeparator=""
                            ValueField="DepartmentId">
                            <Store>
                                <ext:Store ID="store1" runat="server">
                                    <Fields>
                                        <ext:ModelField Name="DepartmentId" Type="String" />
                                        <ext:ModelField Name="Name" />
                                    </Fields>
                                </ext:Store>
                            </Store>
                            <DirectEvents>
                                <Select OnEvent="cmbDepartment_Change">
                                    <EventMask ShowMask="true" />
                                </Select>
                            </DirectEvents>
                        </ext:ComboBox>
                    </td>
                    <td style="padding-left: 10px">
                        <ext:ComboBox ID="cmbSubDepartment" Width="180px" runat="server" ValueField="SubDepartmentId"
                            DisplayField="Name" FieldLabel="Sub Department" LabelAlign="top" LabelSeparator=""
                            ForceSelection="true" QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store11" runat="server">
                                    <Model>
                                        <ext:Model ID="Model11" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="SubDepartmentId" />
                                                <ext:ModelField Name="Name" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <DirectEvents>
                                <Select OnEvent="cmbSubDepartment_Change">
                                    <EventMask ShowMask="true" />
                                </Select>
                            </DirectEvents>
                        </ext:ComboBox>
                    </td>
                    <td style="padding-left: 10px">
                        <ext:ComboBox ForceSelection="true" QueryMode="Local" ID="cmbLevel" FieldLabel="Level"
                            runat="server" LabelAlign="Top" DisplayField="Name" Width="180" LabelSeparator=""
                            ValueField="LevelId">
                            <Store>
                                <ext:Store ID="storeLevel" runat="server">
                                    <Fields>
                                        <ext:ModelField Name="LevelId" Type="String" />
                                        <ext:ModelField Name="Name" />
                                    </Fields>
                                </ext:Store>
                            </Store>
                            <DirectEvents>
                                <Select OnEvent="cmbLevel_Change">
                                    <EventMask ShowMask="true" />
                                </Select>
                            </DirectEvents>
                        </ext:ComboBox>
                    </td>
                </tr>
            </table>
            <ext:GridPanel ID="gridEmpShift" StyleSpec="margin-top:10px;" runat="server" Width="960"
                StoreID="storeEmpShift" MinHeight="0">
                <ColumnModel>
                    <Columns>
                        <ext:Column ID="colBranch" runat="server" Flex="1" Text="Branch" DataIndex="BranchName">
                        </ext:Column>
                        <ext:Column ID="colDepartment" runat="server" Flex="1" Text="Department" DataIndex="DepartmentName">
                        </ext:Column>
                        <ext:Column ID="colEmployee" runat="server" Flex="1" Text="Employee" DataIndex="Name">
                        </ext:Column>
                        <ext:Column ID="colSchedule" runat="server" Hidden="true" Text="Schedule" Flex="1"
                            DataIndex="Schedule">
                            <%--<Renderer Handler="if(value=='1')
                                                return 'Fixed';
                                            else
                                                return 'Changing';" />--%>
                            <Editor>
                                <ext:ComboBox ID="cmbSchedule" runat="server" ForceSelection="true" TypeAhead="true"
                                    SelectOnTab="true">
                                    <Items>
                                        <ext:ListItem Text="Fixed" Value="1" />
                                        <ext:ListItem Text="Changing" Value="2" />
                                    </Items>
                                    <%-- <Listeners>
                                    <Focus Handler="onScheduleFocus(#{gridEmpShift}.getSelectionModel().getCurrentPosition().row);" />
                                </Listeners>--%>
                                </ext:ComboBox>
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="colShift" runat="server" Text="Shift" Flex="1" DataIndex="WorkShiftId">
                            <Renderer Fn="shiftTypeRenderer" />
                            <Editor>
                                <ext:ComboBox ID="cmbShift" runat="server" ValueField="WorkShiftId" DisplayField="Name"
                                    StoreID="storeShift" QueryMode="Local" ForceSelection="true">
                                    <Listeners>
                                        <Select Handler="setShiftType(this.getValue(), #{gridEmpShift}.getSelectionModel().getCurrentPosition().row);" />
                                        <BeforeSelect Handler="return record.data.isEmpty !=true;">
                                        </BeforeSelect>
                                        <Focus Handler="onShiftFocus(#{gridEmpShift}.getSelectionModel().getCurrentPosition().row);" />
                                    </Listeners>
                                </ext:ComboBox>
                            </Editor>
                        </ext:Column>
                        <%-- <ext:Column ID="colInTime" runat="server" Text="In Time" Flex="1" DataIndex="InTime">
                        <Renderer Handler="return renderShiftStart(record.data.WorkShiftId);" />
                    </ext:Column>
                    <ext:Column ID="colOutTime" runat="server" Text="Out Time" Flex="1" DataIndex="OutTime">
                        <Renderer Handler="return renderShiftEnd(record.data.WorkShiftId);" />
                    </ext:Column>
                    <ext:Column ID="colWorkHours" runat="server" Text="Work Hours" Flex="1">
                     <Renderer Handler="return renderTotHour(record.data.WorkShiftId);" />
                    </ext:Column>--%>
                    </Columns>
                </ColumnModel>
                <Plugins>
                    <ext:CellEditing ID="cellEditing1" runat="server" ClicksToEdit="1">
                        <%--<Listeners>
                        <Edit Fn="afterEdit" />
                    </Listeners>--%>
                    </ext:CellEditing>
                </Plugins>
            </ext:GridPanel>
            <ext:Button ID="btnSave" Cls="btn btn-primary btn-sect" Style="margin-top: 10px"
                runat="server" Text="Save Changes">
                <DirectEvents>
                    <Click OnEvent="btnSave_Click">
                        <ExtraParams>
                            <ext:Parameter Name="EmpShiftExtraParam" Value="Ext.encode(#{gridEmpShift}.getRowsValues({selectedOnly : false}))"
                                Mode="Raw" />
                        </ExtraParams>
                        <EventMask ShowMask="true" />
                    </Click>
                </DirectEvents>
            </ext:Button>
        </div>
    </div>
</asp:Content>
