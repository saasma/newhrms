﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using BLL.Entity;
using Utils.Helper;
using Utils.Calendar;

namespace Web
{

   

    //public class MyEventCollection : List<MyEvent>
    //{
    //    public MyEventCollection() { }
    //}
    //public class MyEvent : Event
    //{
    //    public bool IsHalfDay { get; set; }
    //}

    /// <summary>
    /// Summary description for LeaveRequestService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
     [System.Web.Script.Services.ScriptService]
    public class ShiftService : System.Web.Services.WebService
    {

       

        [WebMethod]
        public EventModelCollection GetShiftList(DateTime? start, DateTime? end,DateTime? StartDate,DateTime? EndDate,  string Employee
            ,int page,int limit)
        {
            //start = StartDate;
            //end = EndDate;

            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
                return new EventModelCollection();

            List<DAL.GetEmployeeShiftListResult> list =
                ShiftManager.GetEmployeeShiftList(
                start.Value, end.Value, Employee);

            EventModelCollection events = new EventModelCollection();
            foreach (DAL.GetEmployeeShiftListResult request in list)
            {
                EventModel entity = new EventModel();
                entity.EventId = request.WorkShiftHistoryId;


                entity.CalendarId = request.WorkShiftId;

                entity.Notes = request.Name + " - " + request.WorkShift;



                entity.Title = request.WorkShift;



                entity.StartDate = request.FromDateEngWithShiftTime.Value;

                if (request.ToDateEngWithShiftTime == null)
                    entity.EndDate = entity.StartDate;
                else
                    entity.EndDate = request.ToDateEngWithShiftTime.Value;
                entity.IsAllDay = false;
                //entity.IsNew = false;



                events.Add(entity);
            }
            return events;
        }

      

    }
}
