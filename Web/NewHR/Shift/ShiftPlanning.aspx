﻿<%@ Page Title="Shift Planning" MasterPageFile="~/Master/NewDetails.Master" Language="C#"
    AutoEventWireup="true" CodeBehind="ShiftPlanning.aspx.cs" Inherits="Web.NewHR.ShiftPlanning" %>

<%@ Register Src="~/Controls/ShiftLegendCtl.ascx" TagName="AttendanceLegendCtl" TagPrefix="uc1" %>
<%@ Register Assembly="DayPilot" Namespace="DayPilot.Web.Ui" TagPrefix="DayPilot" %>
<asp:Content ID="content1" runat="server" ContentPlaceHolderID="head">
    <style type="text/css">
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
        .innerLR
        {
            padding: 0px;
        }
        /*hide DEMO of DayPilot*/
        .scheduler_default_corner
        {
            visibility: hidden;
        }
        .itemList
        {
            margin: 0px;
        }
        .itemList li
        {
            float: left;
            list-style-type: none;
        }
    </style>
    <script type="text/javascript">


        /* Fast filter helpers */

        function clear() {
            var filterBox = document.getElementById("TextBoxFilter");
            filterBox.value = '';
            filter();
        }
        function filter() {
            var filterBox = document.getElementById("TextBoxFilter");
            var filterText = filterBox.value;

            dps1.clientState = { "filter": filterText };
            dps1.commandCallBack("filter");
        }

        function key(e) {
            var keynum = (window.event) ? event.keyCode : e.keyCode;

            if (keynum === 13) {
                filter();
                return false;
            }

            return true;
        }

        function afterRenderMessage(data) {
            if (data) {
                Ext.net.Notification.show({
                    iconCls: 'icon-information',
                    pinEvent: 'click',
                    html: data,
                    title: 'Updated'
                });
            }
        }

        /* Event editing helpers - modal dialog */
        function dialog() {

            ctl00_ContentPlaceHolder_Main_windowShift.show();
            //            var modal = new DayPilot.Modal();
            //            modal.top = 60;
            //            modal.width = 300;
            //            modal.opacity = 70;
            //            modal.border = "10px solid #d0d0d0";
            //            modal.closed = function () {
            //                if (this.result == "OK") {
            //                    dps1.commandCallBack('refresh');
            //                }
            //                dps1.clearSelection();
            //            };

            //            modal.height = 250;
            //            modal.zIndex = 100;
            //            return modal;
        }

        function timeRangeSelected(start, end, resourceEmployeeId) {
            ctl00_ContentPlaceHolder_Main_cmbEmployee.setValue(resourceEmployeeId);
            ctl00_ContentPlaceHolder_Main_txtStartDate.setValue(new Date(start));

            var d = new Date(end); // today!
            d.setDate(d.getDate() - 1);

            ctl00_ContentPlaceHolder_Main_txtEndDate.setValue(d);
            ctl00_ContentPlaceHolder_Main_txtEndDate.enable(true);
            ctl00_ContentPlaceHolder_Main_cmbShift.clearValue();
            ctl00_ContentPlaceHolder_Main_cmbEmployee.enable(true);
            ctl00_ContentPlaceHolder_Main_chkHasToDate.setValue(false);
            ctl00_ContentPlaceHolder_Main_hdnWorkshitHistoryId.setValue("");
            ctl00_ContentPlaceHolder_Main_windowShift.setTitle('Add Shift');
            var modal = dialog();

        }

        function eventClick(e) {
            var workShiftHistoryId = e.data.id;
            ctl00_ContentPlaceHolder_Main_hdnWorkshitHistoryId.setValue(workShiftHistoryId);
            ctl00_ContentPlaceHolder_Main_btnEditShift.fireEvent('click');
            //var modal = dialog();

        }

        function refreshEvents() {
            dps1.commandCallBack('refresh');
        }
    </script>
    <script type="text/javascript">
        var txtStartDate = null;
        var txtEndDate = null;
        var windowShift = null;
        var gridEmployeeList = null;
        var cmbEmployee = null;
        var cmbShift = null;

        var setStartDate = function (picker, date) {

            this.getCalendar().setStartDate(date);
            //CompanyX.hdFromDateEng.value = date.toDateString();

            //reloadGrids();
        }

        var rangeSelect = function (cal, dates, callback) {

            if (gridEmployeeList.getSelectionModel().selected.items.length <= 0) {
                alert("Please select the employee on left side first.");
                return;
            }

            var employeeId = gridEmployeeList.getSelectionModel().selected.items[0].data.EmployeeId;
            var name = gridEmployeeList.getSelectionModel().selected.items[0].data.Name;

            var store = cmbEmployee.getStore();
            var myArray = new Employee();
            myArray.data.EmployeeId = employeeId;
            myArray.data.Name = name;
            store.removeAll();
            store.add(myArray);

            cmbEmployee.setValue(employeeId);

            txtStartDate.setValue(dates.StartDate);
            txtEndDate.setValue(dates.EndDate);
            cmbShift.clearValue();
            windowShift.show();
            //this.record.show(cal, dates);
            //this.getWindow().on('hide', callback, cal, { single: true });
        }

        var showWindow = function (employeeId) {

        }
        var setCurrentDate = function (type) {
            var date = new Date(dps1.startDate);
            if (type == "next") {
                date.setMonth(date.getMonth() + 1);
            }
            else if (type == "previous") {
                date.setMonth(date.getMonth() - 1);
            }
            else if (type == "this") {
                var date = new Date();
                date = new Date(date.getFullYear(), date.getMonth(), 1);
            }
            ctl00_ContentPlaceHolder_Main_hdnStartDate.value = (date.getMonth() + 1) + "/1/" + date.getFullYear();
        }
    </script>
</asp:Content>
<asp:Content ID="content2" runat="server" ContentPlaceHolderID="ContentPlaceHolder_Main">
<div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Shift Planning
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
    <div class="separator bottom">
    </div>
    <ext:Hidden ID="hdnWorkshitHistoryId" runat="server" Text="" />
    <asp:HiddenField runat="server" ID="hdnStartDate" />
    <ext:LinkButton ID="btnEditShift" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnEditShift_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <div class="innerLR">
        
        <div style="padding-bottom: 15px">
            <table>
                <tr>
                    <td>
                        <strong>Branch</strong>
                    </td>
                    <td style="width: 10px">
                    </td>
                    <td>
                        <strong>Department</strong>
                    </td>
                    <td rowspan="2">
                        <asp:Button ID="ButtonExport" Style="width: 80px; height: 30px; margin-top: 13px;
                            margin-left: 12px;" runat="server" OnClick="ButtonExport_Click" Text="Export" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:DropDownList AutoPostBack="true" OnSelectedIndexChanged="ddlBranch_Event" AppendDataBoundItems="true"
                            ID="ddlBranch" runat="server" Width="180px" DataTextField="Name" DataValueField="BranchId">
                            <asp:ListItem Text="--Select Branch" Value="-1" />
                        </asp:DropDownList>
                    </td>
                    <td style="width: 10px">
                    </td>
                    <td>
                        <asp:DropDownList AutoPostBack="true" OnSelectedIndexChanged="ddlBranch_Event" AppendDataBoundItems="true"
                            ID="ddlDepartment" runat="server" Width="180px" DataTextField="Name" DataValueField="DepartmentId">
                            <asp:ListItem Text="--Select Department" Value="-1" />
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
        </div>
        <div id="toolbar">
            <a style="font-size: 18px" title="Previous" href="javascript:setCurrentDate('previous');dps1.commandCallBack('previous');">
                &#x25c4;</a> <a style="font-size: 18px" href="javascript:setCurrentDate('next');dps1.commandCallBack('next');">
                    &#x25ba;</a> <a title="Next" href="javascript:setCurrentDate('this');dps1.commandCallBack('this');">
                        This Month</a>
            <%-- <div class="right">
                Fast filter:
                <input type="text" id="TextBoxFilter" onkeypress="return key(event);" />&nbsp;<a
                    href="javascript:filter();" style="font-weight: bold">Apply</a>&nbsp;<a href="javascript:clear();">Clear</a>
            </div>--%>
        </div>
        <DayPilot:DayPilotScheduler ID="DayPilotScheduler1" runat="server" DataTagFields="EmployeeId,Name,Branch"
            Width="100%" RowHeaderWidth="120" CellDuration="1440" CellGroupBy="Month" CellWidth="40"
            TimeRangeSelectedHandling="JavaScript" TimeRangeSelectedJavaScript="timeRangeSelected(start, end, resource)"
            ClientObjectName="dps1" EventMoveHandling="CallBack" OnEventMove="DayPilotScheduler1_EventMove"
            EventMoveJavaScript="dps1.eventMoveCallBack(e, newStart, newEnd, newResource);"
            EventResizeHandling="Disabled" OnTimeRangeSelected="DayPilotScheduler1_TimeRangeSelected"
            EventClickHandling="JavaScript" EventClickJavaScript="eventClick(e);" EventEditHandling="CallBack"
            OnBeforeEventRender="DayPilotScheduler1_BeforeEventRender" OnEventMenuClick="DayPilotScheduler1_EventMenuClick"
            xContextMenuID="DayPilotMenu2" ContextMenuResourceID="DayPilotMenuRes" BusinessBeginsHour="5"
            BusinessEndsHour="24" OnBeforeResHeaderRender="DayPilotScheduler1_BeforeResHeaderRender"
            EnableViewState="false" ScrollLabelsVisible="false" BubbleID="DayPilotBubble1"
            ShowToolTip="false" HeightSpec="Max" Height="500" OnEventClick="DayPilotScheduler1_EventClick"
            OnTimeRangeMenuClick="DayPilotScheduler1_TimeRangeMenuClick" OnBeforeCellRender="DayPilotScheduler1_BeforeCellRender"
            EventDoubleClickHandling="Disabled" ResourceBubbleID="DayPilotBubble1" OnResourceHeaderMenuClick="DayPilotScheduler1_ResourceHeaderMenuClick"
            ResourceHeaderClickHandling="JavaScript" OnResourceHeaderClick="DayPilotScheduler1_ResourceHeaderClick"
            OnBeforeTimeHeaderRender="DayPilotScheduler1_BeforeTimeHeaderRender" ContextMenuSelectionID="DayPilotMenuSelection"
            SyncResourceTree="true" DragOutAllowed="true" TimeRangeDoubleClickHandling="JavaScript"
            TimeRangeDoubleClickJavaScript="" DataStartField="FromDateEng" DataEndField="ToDateEng"
            DataTextField="WorkShift" DataValueField="WorkShiftHistoryId" DataResourceField="EmployeeId"
            AfterRenderJavaScript="afterRenderMessage(data);" OnCommand="DayPilotScheduler1_Command">
            <HeaderColumns>
                <DayPilot:RowHeaderColumn Title="Name" Width="100" />
                <DayPilot:RowHeaderColumn Title="Branch" Width="80" />
            </HeaderColumns>
            <Resources>
                <%--<DayPilot:Resource Name="Room A" Value="A" Expanded="True">
                    <Children>
                        <DayPilot:Resource Name="Room A.1" Value="A.1" Expanded="False">
                            <Children>
                                <DayPilot:Resource Name="Room A.1.1" Value="A.1.1" Expanded="False" />
                                <DayPilot:Resource Name="Room A.1.2" Value="A.1.2" Expanded="False" />
                            </Children>
                        </DayPilot:Resource>
                        <DayPilot:Resource Name="Room A.2" Value="A.2" Expanded="False" />
                    </Children>
                </DayPilot:Resource>
                <DayPilot:Resource Name="Room B" Value="B" Expanded="False" />
                <DayPilot:Resource Name="Room C" Value="C" ToolTip="Test" Expanded="False" />
                <DayPilot:Resource Name="Room D" Value="D" Expanded="False" />
                <DayPilot:Resource Name="Room E" Value="E" Expanded="False" />
                <DayPilot:Resource Name="Room F" Value="F" Expanded="False" />
                <DayPilot:Resource Name="Room G" Value="G" Expanded="False" />
                <DayPilot:Resource Name="Room H" Value="H" Expanded="False" />
                <DayPilot:Resource Name="Room I" Value="I" Expanded="False" />
                <DayPilot:Resource Name="Room J" Value="J" Expanded="False" />
                <DayPilot:Resource Name="Room K" Value="K" Expanded="False" />
                <DayPilot:Resource Name="Room L" Value="L" Expanded="False" />
                <DayPilot:Resource Name="Room M" Value="M" Expanded="False" />
                <DayPilot:Resource Name="Room N" Value="N" Expanded="False" />
                <DayPilot:Resource Name="Room O" Value="O" Expanded="False" />
                <DayPilot:Resource Name="Room P" Value="P" Expanded="False" />
                <DayPilot:Resource Name="Room Q" Value="Q" Expanded="False" />
                <DayPilot:Resource Name="Room R" Value="R" Expanded="False" />
                <DayPilot:Resource Name="Room S" Value="S" Expanded="False" />
                <DayPilot:Resource Name="Room T" Value="T" Expanded="False" />
                <DayPilot:Resource Name="Room U" Value="U" Expanded="False" />
                <DayPilot:Resource Name="Room V" Value="V" Expanded="False" />
                <DayPilot:Resource Name="Room W" Value="W" Expanded="False" />
                <DayPilot:Resource Name="Room X" Value="X" Expanded="False" />
                <DayPilot:Resource Name="Room Y" Value="Y" Expanded="False" />
                <DayPilot:Resource Name="Room Z" Value="Z" Expanded="False" />--%>
            </Resources>
        </DayPilot:DayPilotScheduler>
        <%--<DayPilot:DayPilotMenu ID="DayPilotMenu2" runat="server" UseShadow="false">
            <DayPilot:MenuItem Text="Open" Action="JavaScript" JavaScript="alert('Opening event (id ' + e.value() + ')');">
            </DayPilot:MenuItem>
            <DayPilot:MenuItem Text="Send" Action="JavaScript" JavaScript="alert('Sending event (id ' + e.value() + ')');">
            </DayPilot:MenuItem>
            <DayPilot:MenuItem Text="Refresh" Action="JavaScript" JavaScript="dps1.commandCallBack('refresh');">
            </DayPilot:MenuItem>
            <DayPilot:MenuItem Text="-" Action="NavigateUrl">
            </DayPilot:MenuItem>
            <DayPilot:MenuItem Text="Delete (CallBack)" Action="Callback" Command="Delete">
            </DayPilot:MenuItem>
            <DayPilot:MenuItem Action="PostBack" Command="Delete" Text="Delete (PostBack)" />
            <DayPilot:MenuItem Action="JavaScript" JavaScript="alert('x:' + dps1.eventOffset.x + ' y:' + dps1.eventOffset.y + ' resource:' + e.row());"
                Text="Mouse offset (relative to event)" />
            <DayPilot:MenuItem Action="NavigateUrl" NavigateUrl="javascript:alert('Going somewhere else (id {0})');"
                Text="NavigateUrl test" />
        </DayPilot:DayPilotMenu>--%>
        <%--<DayPilot:DayPilotMenu ID="DayPilotMenuSpecial" runat="server" ClientObjectName="cmSpecial"
            UseShadow="false">
            <DayPilot:MenuItem Text="Open" Action="JavaScript" JavaScript="alert('Opening event (id ' + e.value() + ')');">
            </DayPilot:MenuItem>
            <DayPilot:MenuItem Text="Send" Action="JavaScript" JavaScript="alert('Sending event (id ' + e.value() + ')');">
            </DayPilot:MenuItem>
            <DayPilot:MenuItem Text="Delete (JavaScript using callback)" Action="JavaScript"
                Command='Delete' JavaScript="if (confirm('Do you really want to delete event ' + e.text() + ' ?')) dps1.eventMenuClickCallBack(e, command);">
            </DayPilot:MenuItem>
        </DayPilot:DayPilotMenu>--%>
        <DayPilot:DayPilotMenu ID="DayPilotMenuSelection" runat="server">
            <%-- <DayPilot:MenuItem Action="JavaScript" JavaScript="dps1.timeRangeSelectedCallBack(e.start, e.end, e.resource);"
                Text="Create new event (JavaScript)" />
            <DayPilot:MenuItem Action="PostBack" Command="Insert" Text="Create new event (PostBack)" />
            <DayPilot:MenuItem Action="CallBack" Command="Insert" Text="Create new event (CallBack)" />
            <DayPilot:MenuItem Text="-" Action="JavaScript">
            </DayPilot:MenuItem>
            <DayPilot:MenuItem Action="JavaScript" JavaScript="alert('Start: ' + e.start + '\nEnd: ' + e.end + '\nResource id: ' + e.resource);"
                Text="Show selection details" />--%>
            <DayPilot:MenuItem Action="JavaScript" JavaScript="dps1.clearSelection();" Text="Clean selection" />
        </DayPilot:DayPilotMenu>
        <DayPilot:DayPilotMenu ID="DayPilotMenuRes" runat="server">
            <DayPilot:MenuItem Action="CallBack" Command="Insert" Text="Add child" />
            <DayPilot:MenuItem Text="-" Action="JavaScript">
            </DayPilot:MenuItem>
            <DayPilot:MenuItem Action="CallBack" Command="Delete" Text="Delete" />
            <DayPilot:MenuItem Action="CallBack" Command="DeleteChildren" Text="Delete children" />
            <DayPilot:MenuItem Text="-" Action="JavaScript">
            </DayPilot:MenuItem>
            <DayPilot:MenuItem Action="JavaScript" JavaScript="alert(e.name + '\n' + e.value);"
                Text="Show resource details" />
        </DayPilot:DayPilotMenu>
        <DayPilot:DayPilotBubble ID="DayPilotBubble1" runat="server" OnRenderContent="DayPilotBubble1_RenderContent"
            ClientObjectName="bubble" OnRenderEventBubble="DayPilotBubble1_RenderEventBubble"
            Width="250" Corners="Rounded" Position="EventTop">
        </DayPilot:DayPilotBubble>
        <%-- <table style="display:none">
            <tr>
                <td valign="top">
                    <ext:DatePicker ID="DatePicker1" runat="server" StyleSpec='StyleSpec="border:1px solid #A3BAD9;'
                        Width="215">
                       

                    </ext:DatePicker>
                    <ext:Store ID="storeEmployeeList" runat="server">
                        <Model>
                            <ext:Model ID="modelEmpShift" runat="server" IDProperty="EmployeeId">
                                <Fields>
                                    <ext:ModelField Name="Branch" Type="String" />
                                    <ext:ModelField Name="Department" Type="String" />
                                    <ext:ModelField Name="EmployeeId" Type="Int" />
                                    <ext:ModelField Name="Name" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                    <ext:GridPanel ID="gridEmployeeList" runat="server" StoreID="storeEmployeeList" MinHeight="0">
                        <ColumnModel>
                            <Columns>
                                <ext:Column ID="colBranch" Width="90" runat="server" Text="Branch" DataIndex="Branch">
                                </ext:Column>
                                <ext:Column ID="colEmployee" Width="200" runat="server" Text="Employee" DataIndex="Name">
                                </ext:Column>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>
                </td>
                <td style="width: 10px">
                </td>
                <td valign="top">
                    <ext:CalendarPanel ShowNavBar="false" ID="CalendarPanel1" Width="972" HeaderAsText="true"
                        Header="true" Height="600" runat="server" ActiveIndex="1" Border="true" Margins="0">
                        <CalendarStore ID="GroupStore1" runat="server">
                        </CalendarStore>
                        <EventStore ID="EventStore1" runat="server" NoMappings="true">
                            <Proxy>
                                <ext:AjaxProxy Url="ShiftService.asmx/GetShiftList" Json="true">
                                    <ActionMethods Read="POST" />
                                    <Reader>
                                        <ext:JsonReader Root="d" />
                                    </Reader>
                                </ext:AjaxProxy>
                            </Proxy>
                            <Mappings>
                                <ext:ModelField Name="StartDate" Type="Date" DateFormat="M$" />
                                <ext:ModelField Name="EndDate" Type="Date" DateFormat="M$" />
                            </Mappings>
                            <Parameters>
                                <ext:StoreParameter Name="StartDate" Mode="Value" Value="1/1/2000" />
                                <ext:StoreParameter Name="EndDate" Mode="Value" Value="1/1/2015" />
                                <ext:StoreParameter Name="Employee" Value="0" Mode="Raw" />
                            </Parameters>
                            <Listeners>
                                <BeforeLoad Handler="Ext.net.Mask.show();" />
                                <Load Handler="Ext.net.Mask.hide();" />
                            </Listeners>
                        </EventStore>
                        <WeekView runat="server" ShowTime="false" />
                        <MonthView ID="MonthView1" StyleSpec="height:200px!important" runat="server" ShowHeader="true"
                            ShowWeekLinks="true" ShowWeekNumbers="true" />
                        <Listeners>
                            <RangeSelect Fn="rangeSelect" />
                           

                        </Listeners>
                    </ext:CalendarPanel>
                </td>
            </tr>
        </table>--%>
        <div style='width: 100%;' class="colRight">
            <uc1:AttendanceLegendCtl ID="AttendanceLegendCtl1" runat="server" />
        </div>
    </div>
</div>
    <ext:Window ID="windowShift" runat="server" Title="Add Shift" Icon="Application"
        Height="350" Width="550" BodyPadding="5" Hidden="true" Modal="false">
        <Content>
            <table class="fieldTable">
                <tr>
                    <td>
                        <ext:ComboBox LabelSeparator="" TypeAhead="true" ForceSelection="true" QueryMode="Local"
                            FieldLabel="Employee" ValueField="EmployeeId" DisplayField="Name" runat="server"
                            ID="cmbEmployee" LabelWidth="75" Width="315">
                            <Store>
                                <ext:Store ID="storeLeaveType" runat="server">
                                    <Model>
                                        <ext:Model ID="Model1" runat="server" Name="Employee" IDProperty="EmployeeId">
                                            <Fields>
                                                <ext:ModelField Name="EmployeeId" Type="String" />
                                                <ext:ModelField Name="Name" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:ComboBox LabelSeparator="" TypeAhead="true" ForceSelection="true" QueryMode="Local"
                            FieldLabel="Shift" ValueField="WorkShiftId" DisplayField="Name" runat="server"
                            ID="cmbShift" LabelWidth="75" Width="315">
                            <Store>
                                <ext:Store ID="store1" runat="server">
                                    <Model>
                                        <ext:Model ID="Model2" runat="server" IDProperty="WorkShiftId">
                                            <Fields>
                                                <ext:ModelField Name="WorkShiftId" Type="Int" />
                                                <ext:ModelField Name="Name" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator6" runat="server"
                            ValidationGroup="SaveUpdate" ControlToValidate="cmbShift" ErrorMessage="Shift is required." />
                    </td>
                </tr>
                <tr>
                    <td style="padding-left: 246px">
                        <ext:Checkbox ID="chkHasToDate" runat="server" BoxLabel="No To Date">
                            <Listeners>
                                <Change Handler="if(this.checked==true) #{txtEndDate}.disable(true); else #{txtEndDate}.enable(true);" />
                            </Listeners>
                        </ext:Checkbox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ul class="itemList">
                            <li>
                                <ext:DateField LabelSeparator="" StyleSpec="200" FieldLabel="When" runat="server"
                                    ID="txtStartDate" LabelWidth="75" Width="180">
                                </ext:DateField>
                                <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator1" runat="server"
                                    ValidationGroup="SaveUpdate" ControlToValidate="txtStartDate" ErrorMessage="From date is required." />
                            </li>
                            <li>
                                <ext:DateField FieldLabel="to" StyleSpec="margin-left:10px" LabelWidth="15" LabelSeparator=""
                                    runat="server" ID="txtEndDate" Width="125">
                                </ext:DateField>
                            </li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td valign="bottom" colspan="2">
                        <div class="popupButtonDiv">
                            <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="btnSave" Cls="btnFlat"
                                BaseCls="btnFlat" Text="<i></i>Save">
                                <DirectEvents>
                                    <Click OnEvent="btnSave_Click">
                                        <EventMask ShowMask="true" />
                                    </Click>
                                </DirectEvents>
                                <Listeners>
                                    <Click Handler="valGroup = 'SaveUpdate'; return CheckValidation();">
                                    </Click>
                                </Listeners>
                            </ext:LinkButton>
                            <div class="btnFlatOr">
                                or</div>
                            <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="LinkButton2" Cls="btnFlatLeftGap"
                                Text="<i></i>Cancel">
                                <Listeners>
                                    <Click Handler="#{windowShift}.hide();">
                                    </Click>
                                </Listeners>
                            </ext:LinkButton>
                        </div>
                    </td>
                </tr>
            </table>
        </Content>
    </ext:Window>
    <script type="text/javascript">

        function reloadCalendar() {
            ctl00_ContentPlaceHolder_Main_EventStore1.reload();
        }
        Ext.onReady(
            function () {

                document.getElementsByClassName('scheduler_default_columnheader')[0].nextSibling.style.display = "none";
                document.getElementsByClassName('scheduler_default_corner')[0].style.visibility = "inherit";
            }
        );
    </script>
    <style type="text/css">
        
    </style>
</asp:Content>
