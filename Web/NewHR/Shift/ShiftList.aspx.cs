﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using BLL;
using DAL;
using BLL.Manager;
using BLL.Base;

namespace Web.NewHR
{
    public partial class ShiftList : BasePage
    {
        private static bool isInsert;
        
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.AdministratorTools;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialize();
            }
        }

        protected void btnEditShift_Click(object s, DirectEventArgs e)
        {
            isInsert = true;
            BindWindowData();
            winShift.Title = "Update shift";
            winShift.Show();
        }

        protected void btnAddShift_Click(object s, DirectEventArgs e)
        {
            InitializeVariables();
            ResetFields();
            winShift.Title = "Create shift";
            winShift.Show();
        }

        protected void btnDeleteShift_Click(object s, DirectEventArgs e)
        {
            int workShiftId = Convert.ToInt32(hiddenValueWorkShiftID.Text.Trim());

            Status status = ShiftManager.DeleteShiftById(workShiftId);

            if (status.IsSuccess)
            {
                X.Js.Call("RemoveShift", workShiftId);
            }
            else
            {
                NewMessage.ShowWarningMessage("Sorry! couldn't delete. Try again.");
            }
        }

        protected void btnSave_Click(object s, DirectEventArgs e)
        {
            EWorkShift entity = new EWorkShift();

            entity.Name = txtShiftName.Text.Trim();
            entity.ShortName = txtShortName.Text.Trim();
            entity.Type = Convert.ToInt32(cmbShiftType.Value);
            //entity.ShiftStart = timeFieldStart.SelectedTime;
            //entity.ShiftEnd = timefieldEnd.SelectedTime;
            //entity.LunchOut = timeFieldLunchOut.SelectedTime;
            //entity.LunchBack = timeFieldLunchBack.SelectedTime;
            entity.Color = cmbColor.SelectedItem.Text.Trim() + " ";
            entity.IsDefault = chkIsDefault.Checked;

            
            if (!string.IsNullOrEmpty(hiddenValueWorkShiftID.Text.Trim()))
            {
                isInsert = false;
                entity.WorkShiftId = Convert.ToInt32(hiddenValueWorkShiftID.Text.Trim());
            }

            Status status = ShiftManager.InsertUpdateShift(entity, isInsert);

            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Successfully Saved!!!");
                BindShiftGrid();
                winShift.Close();
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        #region Methods...

        private void Initialize()
        {
            InitializeVariables();
            ResetFields();
            BindShiftGrid();

            List<ColorList> lstColors = ShiftManager.GetAllColors();

            if (lstColors.Any())
            {
                this.storeColor.DataSource = lstColors;
                this.storeColor.DataBind();
            }
        }

        private void InitializeVariables()
        {
            hiddenValueWorkShiftID.Value = null;
            isInsert = true;
        }

        private void ResetFields()
        {
            txtShiftName.Text = string.Empty;
            txtShortName.Text = string.Empty;
            cmbShiftType.SetValue("1");
            cmbColor.SelectedItem.Index = 0;
            chkIsDefault.Checked = false;
        }

        private void BindShiftGrid()
        {
            List<EWorkShift> list = ShiftManager.GetAllShifts();
            this.storeShift.DataSource = list;
            this.storeShift.DataBind();

            if (list.Any(x => x.IsDefault != null && x.IsDefault.Value))
                divMessage.Style["display"] = "none";
        }

        private List<EWorkShift> GetEmptyShiftGrid()
        {
            List<EWorkShift> lstEmptyShifts = new List<EWorkShift>();

            for (int i = 0; i < 5; i++)
            {
                lstEmptyShifts.Add(new EWorkShift());
            }

            return lstEmptyShifts;
        }

        private void BindWindowData()
        {
            int workShiftId = Convert.ToInt32(hiddenValueWorkShiftID.Text.Trim());

            EWorkShift entity = ShiftManager.GetWorkShiftById(workShiftId);

            txtShiftName.Text = entity.Name;
            txtShortName.Text = entity.ShortName;
            cmbShiftType.SetValue(entity.Type.ToString());
            //cmbColor.SetValue(entity.Color.Trim());
            if(entity.Color!=null)
            ExtControlHelper.ComboBoxSetSelected(entity.Color, cmbColor);

            if (entity.IsDefault == true)
            {
                chkIsDefault.Checked = true;
            }
            else
            {
                chkIsDefault.Checked = false;
            }

            
            //timeFieldStart.SelectedTime = entity.ShiftStart.Value;
            //timefieldEnd.SelectedTime = entity.ShiftEnd.Value;
            //timeFieldLunchOut.SelectedTime = entity.LunchOut.Value;
            //timeFieldLunchBack.SelectedTime = entity.LunchBack.Value;

            double totWorkingMinutes;
            double totLunchMinutes;
            double totEffecitveMinutes;

            // calculate total effective hours
            //if (cmbShiftType.Value.ToString()=="1")// if regular
            //{
            //    totWorkingMinutes = entity.ShiftEnd.Value.TotalMinutes - entity.ShiftStart.Value.TotalMinutes;
            //    totLunchMinutes = entity.LunchBack.Value.TotalMinutes - entity.LunchOut.Value.TotalMinutes;
            //}
            //else // if overnight
            //{
            //    totWorkingMinutes = (24*60 - entity.ShiftStart.Value.TotalMinutes) + entity.ShiftEnd.Value.TotalMinutes;

            //    if ((entity.LunchBack.Value.TotalMinutes - entity.LunchOut.Value.TotalMinutes) >= 0)
            //    {
            //        totLunchMinutes = entity.LunchBack.Value.TotalMinutes - entity.LunchOut.Value.TotalMinutes;
            //    }
            //    else
            //    {
            //        totLunchMinutes = (24*60 - entity.LunchOut.Value.TotalMinutes) + entity.LunchBack.Value.TotalMinutes;
            //    }
                
            //}

            //totEffecitveMinutes = totWorkingMinutes - totLunchMinutes;
            //int hr = Convert.ToInt32(totEffecitveMinutes / 60);
            //int min = Convert.ToInt32(totEffecitveMinutes % 60);

            //lblTotal.Text = hr + "hrs " + min + "mins";
        }

        [DirectMethod]
        public void CalculateEffeciveHours()
        {
            //double shiftEndTotMin = timefieldEnd.SelectedTime.TotalMinutes;
            //double shiftStartTotMin = timeFieldStart.SelectedTime.TotalMinutes;
            //double lunchOutTotMin = timeFieldLunchOut.SelectedTime.TotalMinutes;
            //double lunchBackTotMin = timeFieldLunchBack.SelectedTime.TotalMinutes;
            
            double totWorkingMinutes;
            double totLunchMinutes;
            double totEffecitveMinutes;

            // calculate total effective hours
            //if (cmbShiftType.Value.ToString() == "1")// if regular
            //{
            //    totWorkingMinutes = shiftEndTotMin - shiftStartTotMin;
            //    totLunchMinutes = lunchBackTotMin - lunchOutTotMin;
            //}
            //else // if overnight
            //{
            //    totWorkingMinutes = (24 * 60 - shiftStartTotMin) + shiftEndTotMin;

            //    if ((lunchBackTotMin - lunchOutTotMin) >= 0)
            //    {
            //        totLunchMinutes = lunchBackTotMin - lunchOutTotMin;
            //    }
            //    else
            //    {
            //        totLunchMinutes = (24 * 60 - lunchOutTotMin) + lunchBackTotMin;
            //    }

            //}

            //totEffecitveMinutes = totWorkingMinutes - totLunchMinutes;
            //int hr = Convert.ToInt32(totEffecitveMinutes / 60);
            //int min = Convert.ToInt32(totEffecitveMinutes % 60);

            //lblTotal.Text = hr + "hrs " + min + "mins";
        }

        #endregion
    }
}