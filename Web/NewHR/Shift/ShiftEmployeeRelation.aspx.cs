﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using BLL.Manager;
using BLL;
using DAL;
using BLL.Base;

namespace Web.NewHR
{
    public partial class ShiftEmployeeRelation : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.AdministratorTools;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialize();
            }
        }

        protected void btnSave_Click(object s, DirectEventArgs e)
        {
            Status status = new Status();

            if (string.IsNullOrEmpty(dfFromDate.RawText))
            {
                NewMessage.ShowWarningMessage("Date is required.");
                return;
            }

            List<EWorkShiftHistory> empShiftLines = new List<EWorkShiftHistory>();

            string jsonEmpShift = e.ExtraParams["EmpShiftExtraParam"];

            if (string.IsNullOrEmpty(jsonEmpShift))
                return;

            empShiftLines = JSON.Deserialize<List<EWorkShiftHistory>>(jsonEmpShift);

            // find out whether a line needs to be inserted or updated
            List<EWorkShiftHistory> linesToInsert = new List<EWorkShiftHistory>();
            List<EWorkShiftHistory> linestoUpdate = new List<EWorkShiftHistory>();


            status = ShiftManager.SaveUpdateShiftSchedule(empShiftLines
                .Where(x=>x.DefaultWorkShiftId != x.WorkShiftId)
                .ToList(),dfFromDate.SelectedDate);

            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Successfully saved.");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        #region Methods...

        private void Initialize()
        {

            List<Branch> _listBranch = new List<Branch>();
            Branch _objBranch = new Branch();
            _objBranch.Name = "All";
            _objBranch.BranchId = -1;
            _listBranch.Add(_objBranch);
            _listBranch.AddRange(BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId));

            cmbBranch.Store[0].DataSource = _listBranch;
            cmbBranch.Store[0].DataBind();
            LodLevel();
            //BindGrid();
        }

        private void LodLevel()
        {
            this.storeLevel.DataSource = ActivityManager.GetLevels();
            this.storeLevel.DataBind();

            this.cmbLevel.Items.Insert(0, new Ext.Net.ListItem("All", -1));
            this.cmbLevel.SelectedItem.Index = 0;
        }

        protected void cmbBranch_Change(object sender, DirectEventArgs e)
        {

            cmbDepartment.ClearValue();
            LoadDepartment(int.Parse(cmbBranch.SelectedItem.Value));
            LoadSubDepartment();
            BindGrid();
        }


        protected void cmbDepartment_Change(object sender, DirectEventArgs e)
        {
            BindGrid();
        }


        protected void cmbSubDepartment_Change(object sender, DirectEventArgs e)
        {
            BindGrid();
        }

        protected void cmbLevel_Change(object sender, DirectEventArgs e)
        {
            BindGrid();
        }

        private void LoadDepartment(int branchId)
        {
            List<GetDepartmentListResult> _list = DepartmentManager.GetAllDepartmentsByBranch(branchId);
            GetDepartmentListResult _obj =  new GetDepartmentListResult();

            _obj.Name = "All";
            _obj.DepartmentId = -1;
            _list.Insert(0, _obj);

            cmbDepartment.GetStore().DataSource = _list;
            cmbDepartment.GetStore().DataBind();
           
        }

        private void LoadSubDepartment()
        {

            List<SubDepartment> _list = new DepartmentManager().GetSubDepartmentsByCompany(SessionManager.CurrentCompanyId);
            SubDepartment _obj = new SubDepartment();

            _obj.Name = "All";
            _obj.DepartmentId = -1;
            _list.Insert(0, _obj);

            cmbSubDepartment.Store[0].DataSource = _list;
            cmbSubDepartment.Store[0].DataBind();
        }

        private void BindGrid()
        {
            int? Branch = -1;
            if (!string.IsNullOrEmpty(cmbBranch.SelectedItem.Value))
                Branch = int.Parse(cmbBranch.SelectedItem.Value);

            int? Department = -1;
            if (!string.IsNullOrEmpty(cmbDepartment.SelectedItem.Value))
                Department = int.Parse(cmbDepartment.SelectedItem.Value);

            int? LevelID = -1;
            if (!string.IsNullOrEmpty(cmbLevel.SelectedItem.Value))
                LevelID = int.Parse(cmbLevel.SelectedItem.Value);


            this.storeEmpShift.DataSource = ShiftManager.GetEmployee_Shifts(Branch, Department, -1, null, -1, LevelID);
            this.storeEmpShift.DataBind();

            this.storeShift.DataSource = ShiftManager.GetAllShifts();
            this.storeShift.DataBind();
        }
        #endregion
    }
}