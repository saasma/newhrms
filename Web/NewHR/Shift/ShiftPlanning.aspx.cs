﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using System.Data;


using DayPilot.Json;
using DayPilot.Utils;
using DayPilot.Web.Ui;
using DayPilot.Web.Ui.Data;
using DayPilot.Web.Ui.Enums;
using DayPilot.Web.Ui.Events;
using DayPilot.Web.Ui.Events.Bubble;
using DayPilot.Web.Ui.Events.Scheduler;
using System.Text;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;

namespace Web.NewHR
{
    public partial class ShiftPlanning : BasePage
    {
        private DataTable table;
        DateTime date = DateTime.Now;
        DateTime endDate = DateTime.Now;

        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.AdministratorTools;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //DayPilot.Web.Ui.DayPilotScheduler
            
            int days = 0;
            SetStart(ref date, ref days, ref endDate);

            if (!X.IsAjaxRequest && !IsPostBack)
            {
                DayPilotScheduler1.StartDate = date;
                DayPilotScheduler1.Days = days;

                Initialize();
                BindShiftEmployeeList(date, endDate);
            }

            string eventTarget = Request.Form["__EVENTTARGET"];
            //employeeId = UrlHelper.GetIdFromQueryString("EId");
            if (
                (eventTarget != null && eventTarget.Equals("Reload"))
                || !string.IsNullOrEmpty( Request.Params["ctl00$ContentPlaceHolder_Main$ButtonExport"])
                )
            {
                DayPilotScheduler1.StartDate = date;
                DayPilotScheduler1.Days = days;

                Initialize();
                BindShiftEmployeeList(date, endDate);
            }

           



            //initData();


            //DayPilotScheduler1.Separators.Add(DateTime.Today, ColorTranslator.FromHtml("#aaaaaa"), SeparatorLayer.BelowEvents, 60, 60);

            //DayPilotScheduler1.CornerHtml = String.Format("<div style='padding:5px; font-weight: bold; font-size:22px; text-align:center'>{0}</div>", DayPilotScheduler1.StartDate.Year);



            if (!IsPostBack)
            {
                //DayPilotScheduler1.SetScrollX(DateTime.Today.AddDays(-1));
                //DayPilotScheduler1.ScrollY = 10;
                //setDataSourceAndBind();
                //DayPilotScheduler1.UpdateWithMessage("Welcome!");
            }
        }

        public void SetStart(ref DateTime startDate,ref int uptoDays,ref DateTime endDate)
        {
            if (!IsPostBack && !X.IsAjaxRequest)
            {
                startDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                hdnStartDate.Value = startDate.ToShortDateString();

                uptoDays = DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month);
                endDate = startDate.AddDays(uptoDays);
            }
            else
            {
                startDate = DateTime.Parse(hdnStartDate.Value.Trim());
                uptoDays = DateTime.DaysInMonth(startDate.Year, startDate.Month); 
                endDate = startDate.AddDays(uptoDays);

            }
        }

        #region Methods...

        private void Initialize()
        {
            BindEmployeeList();
        }

        [DirectMethod]
        public void ShowWindow(string workShiftId,string employeeId)
        {
            bool isInsert = string.IsNullOrEmpty(workShiftId);

        }

        protected void DayPilotScheduler1_Command(object sender, DayPilot.Web.Ui.Events.CommandEventArgs e)
        {
            switch (e.Command)
            {
                case "next":
                    DayPilotScheduler1.StartDate = DayPilotScheduler1.StartDate.AddMonths(1);
                    DayPilotScheduler1.Days = DateTime.DaysInMonth(DayPilotScheduler1.StartDate.Year,
                        DayPilotScheduler1.StartDate.Month);

                    date = DayPilotScheduler1.StartDate;
                    endDate = DayPilotScheduler1.StartDate.AddDays(DayPilotScheduler1.Days);

                    DayPilotScheduler1.ScrollY = 20;
                   
                    break;
                case "previous":
                    DayPilotScheduler1.StartDate = DayPilotScheduler1.StartDate.AddMonths(-1);
                    DayPilotScheduler1.Days = DateTime.DaysInMonth(DayPilotScheduler1.StartDate.Year,
                        DayPilotScheduler1.StartDate.Month);

                    date = DayPilotScheduler1.StartDate;
                    endDate = DayPilotScheduler1.StartDate.AddDays(DayPilotScheduler1.Days);
                    break;
                case "this":
                    DayPilotScheduler1.StartDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                    DayPilotScheduler1.Days = DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month);

                    date = DayPilotScheduler1.StartDate;
                    endDate = DayPilotScheduler1.StartDate.AddDays(DayPilotScheduler1.Days);
                //endDate = startDate.AddDays(uptoDays);

                    //DayPilotScheduler1.StartDate = new DateTime(DateTime.Today.Year, 1, 1);
                    //DayPilotScheduler1.Days = Year.Days(DayPilotScheduler1.StartDate.Year);
                    break;
                case "test":
                    JsonData d = e.Data;
                    break;
                case "refresh":
                case "filter":
                    // refresh is always done, see setDataSourceAndBind()
                    break;
                default:
                    throw new Exception("Unknown command.");

            }

            setDataSourceAndBind();
            DayPilotScheduler1.Update();
        }

        protected void ddlBranch_Event(object sender, EventArgs e)
        {
            int branchId = int.Parse(ddlBranch.SelectedValue);
            List<GetEmployeesByBranchForShiftPlanningResult> list = new EmployeeManager().GetEmployeesByBranchForShiftPlanning(branchId
                ,int.Parse(ddlDepartment.SelectedValue))
                .OrderBy(x => x.Branch).ThenBy(x => x.Name).ToList();

            DayPilotScheduler1.Resources.Clear();
            foreach (GetEmployeesByBranchForShiftPlanningResult item in list)
            {
                //DayPilotScheduler1.Resources.Add(item.Name, item.EmployeeId.ToString());

                DayPilot.Web.Ui.Resource res = new DayPilot.Web.Ui.Resource(item.Name, item.EmployeeId.ToString());
                res.DataItem = item;
                res.Columns.Add(new ResourceColumn(item.Branch));


                DayPilotScheduler1.Resources.Add(res);
            }

            //BindEmployeeList();
            //BindShiftEmployeeList(date, endDate);
            //DayPilotScheduler1.Update();
            DayPilotScheduler1.StartDate = date;
            DayPilotScheduler1.Days = DateTime.DaysInMonth(date.Year, date.Month);

            Initialize();
            BindShiftEmployeeList(date, endDate);
        }
        private void BindEmployeeList()
        {

            if (!IsPostBack && !X.IsAjaxRequest)
            {
                List<Branch> branchList = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
                ddlBranch.DataSource = branchList;
                ddlBranch.DataBind();

                ddlDepartment.DataSource = DepartmentManager.GetAllDepartments();
                ddlDepartment.DataBind();
            }

            cmbShift.Store[0].DataSource = ShiftManager.GetAllShifts();
            cmbShift.Store[0].DataBind();

            int branchId = int.Parse(ddlBranch.SelectedValue);
            List<GetEmployeesByBranchForShiftPlanningResult> list = new EmployeeManager().GetEmployeesByBranchForShiftPlanning(branchId, -1)
                .OrderBy(x => x.Branch).ThenBy(x => x.Name).ToList();

            cmbEmployee.Store[0].DataSource = list;
            cmbEmployee.Store[0].DataBind();

            if (!IsPostBack && !X.IsAjaxRequest)
            {
                foreach (GetEmployeesByBranchForShiftPlanningResult item in list)
                {
                    //DayPilotScheduler1.Resources.Add(item.Name, item.EmployeeId.ToString());

                    DayPilot.Web.Ui.Resource res = new DayPilot.Web.Ui.Resource(item.Name, item.EmployeeId.ToString());
                    res.DataItem = item;
                    res.Columns.Add(new ResourceColumn(item.Branch));


                    DayPilotScheduler1.Resources.Add(res);
                }

               
            }
            

        }

        public void BindShiftEmployeeList(DateTime start,DateTime end)
        {
            List<DAL.GetEmployeeShiftListResult> list =
               ShiftManager.GetEmployeeShiftList(
               start, end, "");

            foreach (DAL.GetEmployeeShiftListResult item in list)
            {
                if (item.ToDateEng == null)
                    item.ToDateEng = end;
                else
                    //as one day as due to 0 time will show by 1 day previous
                    item.ToDateEng = item.ToDateEngWithShiftTime;
            }

            DayPilotScheduler1.DataSource = list;
            DayPilotScheduler1.DataBind();
        }

        protected void btnEditShift_Click(object s, DirectEventArgs e)
        {

            int workShifthistoryId = int.Parse(hdnWorkshitHistoryId.Text.Trim());

            EWorkShiftHistory history = CommonManager.GetWorkShiftHistoryById(workShifthistoryId);

            cmbEmployee.ClearValue();
            cmbEmployee.SetValue(history.EmployeeId.ToString());
            cmbEmployee.Disable(true);
            cmbShift.ClearValue();
            cmbShift.SetValue(history.WorkShiftId);
            //cmbShift.Disable(true);
            //X.AddScript("#{cmbShift}.enable(true);");
            //cmbShift.Enable(true);

            txtStartDate.SelectedDate = history.FromDateEng.Value;
            if (history.ToDateEng != null)
                txtEndDate.SelectedDate = history.ToDateEng.Value;
            else
            {
                chkHasToDate.Checked = true;
                txtEndDate.Text = "";
                txtEndDate.Disable(true);
            }
            windowShift.Title = "Update Shift";
            windowShift.Show();
        }

        protected void ButtonExport_Click(object sender, EventArgs e)
        {
            setDataSourceAndBind();

            Response.Clear();
            Response.ContentType = "image/png";
            Response.AddHeader("content-disposition", "attachment;filename=print.png");
            MemoryStream img = DayPilotScheduler1.Export(ImageFormat.Png);
            img.WriteTo(Response.OutputStream);
            Response.End();
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {

            EWorkShiftHistory workShift = new EWorkShiftHistory();

            bool isInsert = string.IsNullOrEmpty(hdnWorkshitHistoryId.Text.Trim());
            if (isInsert == false)
                workShift.WorkShiftHistoryId = int.Parse(hdnWorkshitHistoryId.Text.Trim());
            workShift.EmployeeId = int.Parse(cmbEmployee.SelectedItem.Value);
            workShift.WorkShiftId = int.Parse(cmbShift.SelectedItem.Value);
            workShift.FromDateEng = txtStartDate.SelectedDate;

            AttendanceInOutTime time = ShiftManager.GetShiftTime(workShift.WorkShiftId.Value,workShift.FromDateEng.Value);
            EWorkShift shift = ShiftManager.GetShiftById(workShift.WorkShiftId.Value);

            if(time == null)
            {
                NewMessage.ShowWarningMessage("Shift time not defined.");
                return;
            }



            workShift.FromDateEngWithShiftTime = new DateTime(
                workShift.FromDateEng.Value.Year, workShift.FromDateEng.Value.Month, workShift.FromDateEng.Value.Day,
                time.OfficeInTime.Value.Hours, time.OfficeInTime.Value.Minutes, 0);

            if (chkHasToDate.Checked==false)
            {
                workShift.ToDateEng = txtEndDate.SelectedDate;
                workShift.FromDate = BLL.BaseBiz.GetAppropriateDate(workShift.FromDateEng.Value);
                workShift.ToDate = BLL.BaseBiz.GetAppropriateDate(workShift.ToDateEng.Value);

                int hour, min;

                // for overnight set mid night as date could not touch other day
                if (shift.Type == 2)
                {
                    hour = 23;
                    min = 59;
                }
                else
                {
                    hour = time.OfficeOutTime.Value.Hours;
                    min = time.OfficeOutTime.Value.Minutes;
                }

                workShift.ToDateEngWithShiftTime = new DateTime(
                    workShift.ToDateEng.Value.Year, workShift.ToDateEng.Value.Month, workShift.ToDateEng.Value.Day,
                    hour, min, 0);
            }



            Status status = ShiftManager.InsertUpdateShift(isInsert, workShift);


            if (status.IsSuccess)
            {
                X.AddScript("refreshEvents();");
                windowShift.Hide();
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }



            //return status;
        }


        #endregion

        #region "Day Pilot"
        private void setDataSourceAndBind()
        {
            // ensure that filter is loaded
            string filter = (string)DayPilotScheduler1.ClientState["filter"];
            BindShiftEmployeeList(date,endDate);
        }

        protected void DayPilotScheduler1_EventMove(object sender, DayPilot.Web.Ui.Events.EventMoveEventArgs e)
        {

            ShiftManager.MoveResizeShift(int.Parse(e.Value), int.Parse(e.NewResource), e.NewStart, e.NewEnd);


            setDataSourceAndBind();
            DayPilotScheduler1.Update("Shift updated.");
        }
        protected void DayPilotScheduler1_EventResize(object sender, DayPilot.Web.Ui.Events.EventResizeEventArgs e)
        {
            #region Simulation of database update

            //DataRow dr = table.Rows.Find(e.Value);
            //if (dr != null)
            //{
            //    dr["start"] = e.NewStart;
            //    dr["end"] = e.NewEnd;
            //    table.AcceptChanges();
            //}
            EWorkShiftHistory history = CommonManager.GetWorkShiftHistoryById(int.Parse(e.Value));

            // when item resized & moved then end date not correct so solve that bug & only enable resize

            ShiftManager.MoveResizeShift(int.Parse(e.Value), history.EmployeeId.Value, e.NewStart, e.NewEnd.AddDays(-1));

            #endregion

            setDataSourceAndBind();
            DayPilotScheduler1.Update("Shift updated.");
            
            //NewMessage.ShowNormalMessage("Updated.");
        }
        protected void DayPilotScheduler1_TimeRangeSelected(object sender, DayPilot.Web.Ui.Events.TimeRangeSelectedEventArgs e)
        {
            #region Simulation of database update

            DataRow dr = table.NewRow();
            dr["start"] = e.Start;
            dr["end"] = e.End;
            dr["id"] = Guid.NewGuid().ToString();
            dr["name"] = "New event";
            dr["column"] = e.Resource;

            table.Rows.Add(dr);
            table.AcceptChanges();
            #endregion

            setDataSourceAndBind();
            DayPilotScheduler1.UpdateWithMessage("New event created.");
        }
        //protected void DayPilotScheduler1_EventEdit(object sender, DayPilot.Web.Ui.Events.EventEditEventArgs e)
        //{
        //    #region Simulation of database update

        //    DataRow dr = table.Rows.Find(e.Value);
        //    if (dr != null)
        //    {
        //        dr["name"] = e.NewText;
        //        table.AcceptChanges();
        //    }

        //    #endregion

        //    setDataSourceAndBind();
        //    DayPilotScheduler1.Update();
        //}

        protected void DayPilotScheduler1_BeforeEventRender(object sender, BeforeEventRenderEventArgs e)
        {
            //e.Areas.Add(new Area().Width(17).Height(17).Right(19).Top(6).CssClass("event_action_delete").JavaScript("dps1.commandCallBack('delete', {id:e.value() });"));
            //e.Areas.Add(new Area().Width(17).Height(17).Right(2).Top(6).CssClass("event_action_menu").ContextMenu("cmSpecial"));

            // Sailendra code
            string color = e.DataItem["color"] as string;
            if (!String.IsNullOrEmpty(color))
            {
                e.DurationBarColor = color;
            }

            //e.Areas.Add(new Area().Width(20).Left(9).Height(20).Top(3).Html("X").Visibility(AreaVisibility.Visible));
        }

        protected void DayPilotScheduler1_EventMenuClick(object sender, DayPilot.Web.Ui.Events.EventMenuClickEventArgs e)
        {
            //switch (e.Command)
            //{
            //    case "Delete":
            //        #region Simulation of database update
            //        DataRow dr = table.Rows.Find(e.Value);

            //        if (dr != null)
            //        {
            //            table.Rows.Remove(dr);
            //            table.AcceptChanges();
            //        }
            //        #endregion
            //        break;
            //}

            //setDataSourceAndBind();
            //DayPilotScheduler1.Update();
        }

        protected void DayPilotScheduler1_BeforeResHeaderRender(object sender, DayPilot.Web.Ui.Events.Scheduler.BeforeResHeaderRenderEventArgs e)
        {
            //if (e.Value == "A")
            //{
            //    e.ContextMenuClientName = "cmSpecial";
            //}
            //e.Areas.Add(new Area().Width(17).Bottom(0).Right(0).Top(0).CssClass("resource_action_menu").Html("<div><div></div></div>").JavaScript("alert(e.Value);"));
        }

        protected void DayPilotBubble1_RenderContent(object sender, RenderEventArgs e)
        {
            //if (e is RenderResourceBubbleEventArgs)
            //{
            //    RenderResourceBubbleEventArgs re = e as RenderResourceBubbleEventArgs;
            //    e.InnerHTML = "<b>Resource header details</b><br />Value: " + re.ResourceId;
            //}
            //else if (e is RenderCellBubbleEventArgs)
            //{
            //    RenderCellBubbleEventArgs re = e as RenderCellBubbleEventArgs;
            //    e.InnerHTML = "<b>Cell details</b><br />Resource:" + re.ResourceId + "<br />From:" + re.Start + "<br />To: " + re.End;
            //}
        }

        //protected void DayPilotScheduler1_Command(object sender, DayPilot.Web.Ui.Events.CommandEventArgs e)
        //{
        //    switch (e.Command)
        //    {
        //        case "next":
        //            DayPilotScheduler1.StartDate = DayPilotScheduler1.StartDate.AddYears(1);
        //            DayPilotScheduler1.Days = Year.Days(DayPilotScheduler1.StartDate.Year);
        //            DayPilotScheduler1.ScrollY = 20;
        //            break;
        //        case "previous":
        //            DayPilotScheduler1.StartDate = DayPilotScheduler1.StartDate.AddYears(-1);
        //            DayPilotScheduler1.Days = Year.Days(DayPilotScheduler1.StartDate.Year);
        //            break;
        //        case "this":
        //            DayPilotScheduler1.StartDate = new DateTime(DateTime.Today.Year, 1, 1);
        //            DayPilotScheduler1.Days = Year.Days(DayPilotScheduler1.StartDate.Year);
        //            break;
        //        case "delete":
        //            string id = (string)e.Data["id"];
        //            #region Simulation of database update
        //            DataRow dr = table.Rows.Find(id);

        //            if (dr != null)
        //            {
        //                table.Rows.Remove(dr);
        //                table.AcceptChanges();
        //            }
        //            #endregion

        //            break;
        //        case "test":
        //            JsonData d = e.Data;
        //            break;
        //        case "refresh":
        //        case "filter":
        //            // refresh is always done, see setDataSourceAndBind()
        //            break;
        //        default:
        //            throw new Exception("Unknown command.");

        //    }

        //    setDataSourceAndBind();
        //    DayPilotScheduler1.Update();
        //}

        protected void DayPilotScheduler1_EventClick(object sender, DayPilot.Web.Ui.Events.EventClickEventArgs e)
        {
            setDataSourceAndBind();
            DayPilotScheduler1.UpdateWithMessage(String.Format("Event {0} clicked.", e.Value));
            //throw new Exception("ScrollX: " + DayPilotScheduler1.ScrollX);
        }

        protected void DayPilotScheduler1_TimeRangeMenuClick(object sender, DayPilot.Web.Ui.Events.TimeRangeMenuClickEventArgs e)
        {
            //if (e.Command == "Insert")
            //{
            //    #region Simulation of database update

            //    DataRow dr = table.NewRow();
            //    dr["start"] = e.Start;
            //    dr["end"] = e.End;
            //    dr["id"] = Guid.NewGuid().ToString();
            //    dr["name"] = "New event";
            //    dr["column"] = e.ResourceId;

            //    table.Rows.Add(dr);
            //    table.AcceptChanges();
            //    #endregion

            //    setDataSourceAndBind();
            //    DayPilotScheduler1.Update();
            //}
        }


        /// <summary>
        /// Make sure a copy of the data is in the Session so users can try changes on their own copy.
        /// </summary>
        private void initData()
        {

            //if (Session[PageHash] == null)
            //{
            //    //Session[PageHash] = DataGeneratorScheduler.GetData();
            //}
            //table = (DataTable)Session[PageHash];
        }

        protected string PageHash
        {
            get
            {
                return "sddfdfs";
            }
        }

        /// <summary>
        /// This method should normally load the data from the database.
        /// We will load our copy from a Session, just simulating a database.
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        private DataTable getData(DateTime start, DateTime end, string filter)
        {
            return new DataTable();
        }

        protected void DayPilotScheduler1_BeforeCellRender(object sender, DayPilot.Web.Ui.Events.BeforeCellRenderEventArgs e)
        {
        }

        protected void DayPilotScheduler1_Refresh(object sender, DayPilot.Web.Ui.Events.RefreshEventArgs e)
        {
            //DayPilotScheduler1.StartDate = e.StartDate;
            //setDataSourceAndBind();
            //DayPilotScheduler1.Update(CallBackUpdateType.Full);
        }

        protected void DayPilotBubble1_RenderEventBubble(object sender, RenderEventBubbleEventArgs e)
        {
            //StringBuilder sb = new StringBuilder();
            //sb.AppendFormat("<b>{0}</b><br />", e.Text);
            //sb.AppendFormat("Start: {0}<br />", e.Start);
            //sb.AppendFormat("End: {0}<br />", e.End);
            //sb.AppendFormat("<br />");

            //e.InnerHTML = sb.ToString();

        }
        protected void DayPilotScheduler1_ResourceHeaderMenuClick(object sender, DayPilot.Web.Ui.Events.Scheduler.ResourceHeaderMenuClickEventArgs e)
        {
            //switch (e.Command)
            //{
            //    case "Insert":
            //        e.Resource.Children.Add("Testing Child", Guid.NewGuid().ToString());
            //        e.Resource.Expanded = true;
            //        break;
            //    case "DeleteChildren":
            //        e.Resource.Children.Clear();
            //        break;
            //    case "Delete":
            //        DayPilotScheduler1.Resources.RemoveFromTree(e.Resource);
            //        break;
            //}
            //setDataSourceAndBind();
            //DayPilotScheduler1.Update();
        }
        protected void DayPilotScheduler1_ResourceHeaderClick(object sender, DayPilot.Web.Ui.Events.Scheduler.ResourceHeaderClickEventArgs e)
        {
            //DayPilotScheduler1.Resources.RemoveFromTree(e.Resource);
            //setDataSourceAndBind();
            //DayPilotScheduler1.Update();
        }
        protected void DayPilotScheduler1_BeforeTimeHeaderRender(object sender, DayPilot.Web.Ui.Events.BeforeTimeHeaderRenderEventArgs e)
        {

        }

        protected void DayPilotScheduler1_Notify(object sender, DayPilot.Web.Ui.Events.Scheduler.NotifyEventArgs e)
        {
            //foreach (DayPilotEventArgs ea in e.Queue)
            //{
            //    if (ea is EventMoveEventArgs)
            //    {
            //        EventMoveEventArgs em = (EventMoveEventArgs)ea;

            //        #region Simulation of database update

            //        DataRow dr = table.Rows.Find(em.Value);
            //        if (dr != null)
            //        {
            //            dr["start"] = em.NewStart;
            //            dr["end"] = em.NewEnd;
            //            dr["column"] = em.NewResource;
            //            table.AcceptChanges();
            //        }
            //        else
            //        {
            //            throw new Exception("Event id not found.");
            //        }
            //        #endregion
            //    }
            //    else if (ea is EventUpdateEventArgs)
            //    {
            //        DayPilotScheduler1_EventUpdate(sender, (EventUpdateEventArgs)ea);
            //    }
            //}
        }
        protected void DayPilotScheduler1_EventUpdate(object sender, EventUpdateEventArgs e)
        {
            //#region Simulation of database update

            //DataRow dr = table.Rows.Find(e.Event.Value);
            //if (dr != null)
            //{
            //    dr["start"] = e.New.Start;
            //    dr["end"] = e.New.End;
            //    dr["column"] = e.New.Resource;
            //    dr["name"] = e.New.Text;
            //    table.AcceptChanges();
            //}
            //else
            //{
            //    throw new Exception("Event id not found.");
            //}
            //#endregion


        }

        protected void DayPilotScheduler1_OnEventRightClick(object sender, EventRightClickEventArgs e)
        {

        }
        #endregion
    }
}