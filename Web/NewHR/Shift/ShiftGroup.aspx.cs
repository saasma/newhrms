﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using BLL;
using BLL.Manager;
using DAL;

namespace Web.NewHR
{
    public partial class ShiftGroup : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialize();            
            }
        }

        #region Methods...

        private void Initialize()
        {
            BindGrids();
        }

        private void BindGrids()
        {
            this.storeShiftGroup.DataSource = ShiftManager.GetShiftGroups();
            this.storeShiftGroup.DataBind();
        }

        #endregion
    }
}