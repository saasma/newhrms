﻿<%@ Page Title="Shift Planning" Language="C#" MasterPageFile="~/Master/NewDetails.Master" AutoEventWireup="true" CodeBehind="AssignShiftHr.aspx.cs" Inherits="Web.NewHR.Shift.AssignShiftHr" %>
<%@ Register Src="~/NewHR/UserControls/ShiftPlanningCtrl.ascx" TagName="ShiftPlanCtrl" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">

<uc1:ShiftPlanCtrl Id="ShiftPlanCtrol" runat="server" />

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
