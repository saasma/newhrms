<%@ Page MaintainScrollPositionOnPostback="true" Title="Defined Salary Import" Language="C#"
    MasterPageFile="~/Master/HRold.Master" AutoEventWireup="true" CodeBehind="DefinedSalaryImport.aspx.cs"
    Inherits="Web.CP.DefinedSalaryImport" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<%@ Register Src="UserControls/DefinedTypeImportCtl.ascx" TagName="VariableImportCtl"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <ext:ResourceManager ID="ResourceManager1" DisableViewState="false" runat="server"
        ShowWarningOnAjaxFailure="false" ScriptMode="Release">
    </ext:ResourceManager>
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Import Tabular (defined) Salary Structure
                </h4>
            </div>
        </div>
    
    </div>
    <div class="contentpanel">    Note : If name has .(dot) it will be replaced by #
        <uc1:VariableImportCtl ID="VariableImportCtl1" runat="server" />
    </div>
</asp:Content>
