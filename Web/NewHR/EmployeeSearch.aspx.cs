﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using Utils.Helper;
using System.IO;
using Utils;
using Utils.Security;

namespace Web.NewHR
{
    public partial class EmployeeSearch : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.HumanResource;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest && !IsPostBack)
            {
                Initialise();
            }
        }

        public void Initialise()
        {
           
            // Disable all emp export/import once salary is saved           

            List<Branch> branchList = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            cmbBranch.Store[0].DataSource = branchList;
            cmbBranch.Store[0].DataBind();

            cmbDepartment.Store[0].DataSource = DepartmentManager.GetAllDepartments().OrderBy(x => x.Name).ToList();
            cmbDepartment.Store[0].DataBind();

            cmbDesignation.Store[0].DataSource = new CommonManager().GetAllDesignations().OrderBy(x => x.Name).ToList();
            cmbDesignation.Store[0].DataBind();

            this.storeLevel.DataSource = ActivityManager.GetLevels().OrderBy(x => x.Name).ToList();
            this.storeLevel.DataBind();

            JobStatus statues = new JobStatus();
            storeEmpStatus.DataSource = statues.GetMembersForHirarchyView();

            storeEmployeeList.DataSource = EmployeeManager.GetAllEmployeesForEmployeeImportExport().OrderBy(x => x.Name).ToList();
            storeEmployeeList.DataBind();

            cmbBranch.SelectedItem.Index = 0;
            cmbDepartment.SelectedItem.Index = 0;
            cmbDesignation.SelectedItem.Index = 0;
            cmbEmpStatus.SelectedItem.Index = 0;
            cmbLevel.SelectedItem.Index = 0;

            JavascriptHelper.AttachNonDialogPopUpCode(Page, "employeePopup", "../ExcelWindow/EmployeeExcel.aspx", 450, 500);
            JavascriptHelper.AttachNonDialogPopUpCode(Page, "otherPopup", "../ExcelWindow/OtherEmployeeImport.aspx", 450, 500);

            //btnEmpCatDetl_Click(null, null);
        }

        protected void btnEmpCatDetl_Click(object sender, DirectEventArgs e)
        {
          
            X.Js.Call("searchList");
            
        }

    }
}