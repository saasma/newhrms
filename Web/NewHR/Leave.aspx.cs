﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Base;

namespace Web.NewHR
{
    public partial class Leave : System.Web.UI.Page
    {
        public void btnUpdate_Click(object sender, DirectEventArgs e)
        {
            int teamId = int.Parse(hdnTeam.Text.Trim());
            string list = hdnList.Text.Trim();

            LeaveTreeManager.Update(teamId, list);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialize();
            }
        }

        #region Methods...

        private void Initialize()
        {
            LoadLeaveTeamsTreePanel();
            treePanelLeaveTeams.ExpandAll();

            LoadEmpStructTreePanel();
            treePanelEmpStruct.ExpandAll();
        }

        /// <summary>
        /// Right tree of team structure
        /// </summary>
        /// <returns></returns>
        private NodeCollection LoadLeaveTeamsTreePanel()
        {
            Ext.Net.NodeCollection mainTreeNode = treePanelLeaveTeams.Root;

            List<Branch> branchList = LeaveTreeManager.GetAllBranches().OrderBy(x => x.BranchId).ToList();

            List<LeaveTreeJoinProjectBranchResult> leaveProjectList = LeaveTreeManager.GetAllJoinedLeaveProjects().OrderBy(x => x.LeaveProjectId).ToList();

            List<LeaveTreeJoinProjectEmployeeBranchResult> leaveProjectEmployeeList = LeaveTreeManager.GetAllJoinedLeaveProjectEmployees().OrderBy(x => x.EmployeeName).ToList();



            Ext.Net.Node rootTreeNode = new Ext.Net.Node();

            rootTreeNode.Text = "Root";
            rootTreeNode.Expanded = true;
            rootTreeNode.Leaf = false;
            rootTreeNode.NodeID = Guid.Empty.ToString();
            mainTreeNode.Add(rootTreeNode);

            // Load Department List
            foreach (Branch branch in branchList)
            {
                Node node = new Node();
                node.NodeID = branch.BranchId.ToString();
                node.Text = branch.Name;

                if (leaveProjectList.Where(t => t.BranchId == branch.BranchId).ToList().Count > 0)
                {
                    // Load LeaveProject Lists
                    foreach (LeaveTreeJoinProjectBranchResult leaveProject in leaveProjectList.Where(x => x.BranchId == branch.BranchId).ToList())
                    {
                        Node projectNode = new Node();
                        projectNode.NodeID = leaveProject.LeaveProjectId.ToString();
                        projectNode.Text = leaveProject.Name;
                        node.Children.Add(projectNode);

                        if (leaveProjectEmployeeList.Where(t => t.LeaveProjectId == leaveProject.LeaveProjectId).ToList().Count > 0)
                        {
                            // Load Employee of each Project of Department
                            foreach (LeaveTreeJoinProjectEmployeeBranchResult employee in leaveProjectEmployeeList.Where(t => t.LeaveProjectId == leaveProject.LeaveProjectId).ToList())
                            {
                                Node employeeNode = new Node();
                                employeeNode.NodeID = employee.LeaveProjectEmployeeId.ToString();
                                employeeNode.Text = employee.EmployeeName;
                                employeeNode.Leaf = true;

                                projectNode.Children.Add(employeeNode);
                            }
                            projectNode.Leaf = false;
                        }
                        else
                        {
                            projectNode.Leaf = true;
                        }
                    }
                    node.Leaf = false;
                }

                else
                    node.Leaf = true;


                rootTreeNode.Children.Add(node);
            }

            return mainTreeNode;
        }

        /// <summary>
        /// Left Tree of Employee structure
        /// </summary>
        /// <returns></returns>
        private NodeCollection LoadEmpStructTreePanel()
        {
            Ext.Net.NodeCollection mainTreeNode = treePanelEmpStruct.Root;

            List<Branch> branchList = LeaveTreeManager.GetAllBranches().OrderBy(x => x.BranchId).Where(x => x.BranchId == 225).ToList();
            List<LeaveTreeJoinBranchDepartmentResult> departmentList = LeaveTreeManager.GetAllJoinedDepartments();
            List<EEmployee> employeeList = LeaveTreeManager.GetAllCurrentEmployees().OrderBy(x => x.Name).ToList();

            Ext.Net.Node rootTreeNode = new Ext.Net.Node();

            rootTreeNode.Text = "Root";
            rootTreeNode.Expanded = true;
            rootTreeNode.Leaf = false;
            rootTreeNode.NodeID = Guid.Empty.ToString();
            mainTreeNode.Add(rootTreeNode);


            // Load Branch list
            foreach (Branch branch in branchList)
            {
                Node branchNode = new Node();
                branchNode.NodeID = branch.BranchId.ToString();
                branchNode.Text = branch.Name;


                if (departmentList.Where(x => x.BranchID == branch.BranchId).Count() > 0)
                {
                    // Load Department List
                    foreach (LeaveTreeJoinBranchDepartmentResult department in departmentList.Where(x => x.BranchID == branch.BranchId))
                    {
                        Node dptNode = new Node();
                        dptNode.NodeID = branch.BranchId.ToString() + "_" + department.DepartmentId.ToString();
                        dptNode.Text = department.Name;
                        dptNode.Expanded = false;

                        if (employeeList.Where(e => e.DepartmentId == department.DepartmentId && e.BranchId == branch.BranchId).Count() > 0)
                        {
                            // Load Employee list
                            foreach (EEmployee employee in employeeList.Where(e => e.DepartmentId == department.DepartmentId && e.BranchId == branch.BranchId))
                            {
                                Node empNode = new Node();
                                dptNode.Children.Add(empNode);

                                empNode.NodeID = employee.EmployeeId.ToString();
                                empNode.Text = employee.Name;
                                //empNode.Checked = false;


                                // creating configItem
                                ConfigItem testConfigItem = new ConfigItem();
                                testConfigItem.Name = "projectName";
                                string projectName = LeaveTreeManager.GetProjectNameofEmployee(employee.EmployeeId);
                                testConfigItem.Value = projectName;
                                empNode.CustomConfig.Add(testConfigItem);




                                empNode.Leaf = true;
                            }
                            dptNode.Leaf = false;
                        }
                        else
                        {
                            dptNode.Leaf = true;
                        }
                        branchNode.Children.Add(dptNode);
                    }
                    branchNode.Leaf = false;
                }
                else
                {
                    branchNode.Leaf = true;
                }


                rootTreeNode.Children.Add(branchNode);
            }

            return mainTreeNode;
        }

        #endregion





    }

}