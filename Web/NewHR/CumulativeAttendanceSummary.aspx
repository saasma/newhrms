﻿<%@ Page Title="Cumulative Summary Report" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="CumulativeAttendanceSummary.aspx.cs" Inherits="Web.CP.CumulativeAttendanceSummary" %>

<%@ MasterType VirtualPath="~/Master/NewDetails.Master" %>
<%@ Register Src="~/newhr/UserControls/CumulativeAttendanceSummaryCtl.ascx" TagName="AttendanceDeductionReportCtl"
    TagPrefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Cumulative Summary Report
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel" style="margin-top: 0px !important; padding-top: 0px !important;">
        <div class="innerLR">
            <uc:AttendanceDeductionReportCtl Id="AttendanceDeductionReportCtl1" PageView="Admin" runat="server" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
