﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using Utils.Helper;

namespace Web.NewHR
{
    public partial class PositionGradeManager : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.AdministratorTools;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }

            JavascriptHelper.AttachNonDialogPopUpCode(Page, "taxImportPopup", "../ExcelWindow/NewDesignationImportExcel.aspx", 450, 500);


        }

        public void Initialise()
        {
            LoadLevels();

            LoadPositions();

            cmbGroup.Store[0].DataSource = NewPayrollManager.GetLevelGroup();
            cmbGroup.Store[0].DataBind();

            cmbLevel2.Store[0].DataSource = NewPayrollManager.GetAllParentLevelList();
            cmbLevel2.Store[0].DataBind();

            cmbPositionCategory.Store[0].DataSource = ExtControlHelper.GetTextValues(typeof(DesignationCategory));
            cmbPositionGroup.Store[0].DataSource = ExtControlHelper.GetTextValues(typeof(DesignationGroup));

            if (CommonManager.CompanySetting.WhichCompany != WhichCompany.RBB)
            {
                ///divSubLevel.Style["display"] = "none";               
            }
        }
        
        private void LoadLevels()
        {
            storeLevel.DataSource = NewPayrollManager.GetAllParentLevelList();
            storeLevel.DataBind();

            //GridSubLevels.Store[0].DataSource = NewPayrollManager.GetAllSubLevelList();
            //GridSubLevels.Store[0].DataBind();
        }
        private void LoadPositionLevels()
        {
            cmbLevel.Store[0].DataSource = NewPayrollManager.GetAllParentLevelList();
            cmbLevel.Store[0].DataBind();

            cmbLevel2.Store[0].DataSource = NewPayrollManager.GetAllParentLevelList();
            cmbLevel2.Store[0].DataBind();
        }

        public void btnLoadDesignations_Click(object sender, DirectEventArgs e)
        {
            LoadPositions();
        }
        private void LoadPositions()
        {
            storePosition.DataSource = NewPayrollManager.GetAllPositionOrDesignationsList();
            storePosition.DataBind();
        }
        public void ClearLevelFields()
        {
            hiddenValue.Text = "";
            cmbGroup.ClearValue();
            txtLevelCode.Text = "";
            txtLevelName.Text = "";
            txtLevelOrder.Number = 0;
            cmbClericalType.ClearValue();
        }
        public void ClearSubLevelFields()
        {
            hiddenValue.Text = "";
            cmbLevel2.Enable(true);
            txtLevelCode2.Text = "";
            txtLevelName2.Text = "";
            txtLevelOrder2.Number = 0;
            cmbLevel2.ClearValue();
        }
        public void ClearPositionFields()
        {
            hiddenValue.Text = "";
            cmbGroup.ClearValue();
            txtPositionCode.Text = "";
            txtPositionName.Text = "";
            //txtPositionOrder.Number = 0;
            LoadPositionLevels();
            cmbLevel.ClearValue();
            cmbLevel.Enable();

        }
        protected void btnAddPosition_Click(object sender, DirectEventArgs e)
        {
            ClearPositionFields();
            WindowPosition.Center();
            WindowPosition.Show();
        }
        protected void btnAddLevel_Click(object sender, DirectEventArgs e)
        {
            ClearLevelFields();
            WindowLevel.Center();
            WindowLevel.Show();
        }
        protected void btnAddSubLevel_Click(object sender, DirectEventArgs e)
        {
            ClearSubLevelFields();
            WindowSubLevel.Show();
        }


        protected void btnDeleteLevel_Click(object sender, DirectEventArgs e)
        {
            int levelId = int.Parse(hiddenValue.Text.Trim());
            Status status = NewPayrollManager.DeleteLevel(levelId);
            if (status.IsSuccess)
            {
                LoadLevels();
                NewMessage.ShowNormalMessage("Level deleted.");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }

        }

        protected void btnDeletePosition_Click(object sender, DirectEventArgs e)
        {
            int id = int.Parse(hiddenValue.Text.Trim());
            EDesignation entity = new CommonManager().GetDesignationById(id);
            bool status = new CommonManager().Delete(entity);
            if (status)
            {
                LoadPositions();
                NewMessage.ShowNormalMessage("Position deleted.");
            }
            else
            {
                NewMessage.ShowWarningMessage("Position is in use.");
            }

        }

        protected void btnEditPosition_Click(object sender, DirectEventArgs e)
        {
            LoadPositionLevels();

            int id = int.Parse(hiddenValue.Text.Trim());
            EDesignation entity = new CommonManager().GetDesignationById(id);
            
            //cmbLevel.Disable();
            WindowPosition.Center();
            WindowPosition.Show();

            

            //cmbLevel.SetValue(entity.LevelId.ToString()); 
            //X.Js.AddScript("#{cmbLevel}.lastQuery = '';");
            
            txtPositionCode.Text = entity.Code;
            txtPositionName.Text = entity.Name;
            //if (entity.Order != null)
            //    txtPositionOrder.Number = (double)entity.Order;
            //else
            //    txtPositionOrder.Text = "";
            
            if (entity.LevelId != null)
            {
                cmbLevel.Store[0].ClearFilter();
                cmbLevel.SetValue(entity.LevelId.ToString());
            }
            else
                cmbLevel.ClearValue();

            cmbPositionCategory.Store[0].ClearFilter();
            if (entity.CategoryId != null)
            {
                cmbPositionCategory.SetValue(entity.CategoryId.ToString());
            }
            if (entity.GroupId != null)
            {
                cmbPositionGroup.SetValue(entity.GroupId.ToString());
            }
        }

        protected void btnEditLevel_Click(object sender, DirectEventArgs e)
        {
            int levelId = int.Parse(hiddenValue.Text.Trim());
            BLevel entity = NewPayrollManager.GetLevelById(levelId);
            if (entity.ParentLevelId == null)
            {
                WindowLevel.Center();
                WindowLevel.Show();

                cmbGroup.Store[0].ClearFilter();
                cmbGroup.SetValue(entity.LevelGroupId.ToString());
                txtLevelCode.Text = entity.Code;
                txtLevelName.Text = entity.Name;
                txtLevelOrder.Number = entity.Order;

                if (entity.ClericalType != null)
                    cmbClericalType.SetValue(entity.ClericalType.Value.ToString());
                else
                    cmbClericalType.ClearValue();
            }
            else
            {
                WindowSubLevel.Show();

                cmbLevel2.Disable(true);
                cmbLevel2.Store[0].ClearFilter();
                cmbLevel2.SetValue(entity.ParentLevelId.ToString());
                txtLevelCode2.Text = entity.Code;
                txtLevelName2.Text = entity.Name;
                txtLevelOrder2.Number = entity.Order;
            }
          
        }

        protected void btnPositionSaveUpdate_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("InsertUpdatePosition");
            if (Page.IsValid)
            {

                EDesignation entity = new EDesignation();
                bool isInsert = true;
                if (!string.IsNullOrEmpty(hiddenValue.Text))
                {
                    isInsert = false;
                    entity.DesignationId = int.Parse(hiddenValue.Text.Trim());
                }

                if (cmbLevel.SelectedItem != null && cmbLevel.SelectedItem.Value != null)
                    entity.LevelId = int.Parse(cmbLevel.SelectedItem.Value);

                entity.LevelId = int.Parse(cmbLevel.SelectedItem.Value);
                entity.Name = txtPositionName.Text.Trim();
                entity.Code = txtPositionCode.Text.Trim();
                //entity.Order = (int)txtPositionOrder.Number;
                entity.CompanyId = SessionManager.CurrentCompanyId;

                if (cmbPositionCategory.SelectedItem != null && cmbPositionCategory.SelectedItem.Value != null)
                    entity.CategoryId = int.Parse(cmbPositionCategory.SelectedItem.Value);

                if (cmbPositionGroup.SelectedItem != null && cmbPositionGroup.SelectedItem.Value != null)
                    entity.GroupId = int.Parse(cmbPositionGroup.SelectedItem.Value);

                //if (entity.Order != null)
                //{
                //    bool IsOrderExits = NewPayrollManager.IsPositionOrderExists(entity.DesignationId, entity.Order.Value);
                //    if (IsOrderExits)
                //    {
                //        NewMessage.ShowNormalMessage("Order already exists.");
                //        return;
                //    }
                //}

                bool IsPositionCodeExits = NewPayrollManager.IsDesignationNameExists(entity.LevelId.Value,entity.DesignationId,entity.Name);
                
                if (IsPositionCodeExits)
                {
                    NewMessage.ShowNormalMessage("Name already exists for this level/position.");
                    return;
                }


                bool status;
                if (isInsert)
                    status = new CommonManager().Save(entity);
                else
                    status = new CommonManager().Update(entity);

                if (status)
                {
                    MyCache.Reset();
                    WindowPosition.Hide();
                    LoadPositions();
                    NewMessage.ShowNormalMessage("Position saved.");
                }
                else
                {
                    NewMessage.ShowWarningMessage("Position name already exists.");
                }

            }
        }

        protected void btnChangeDesignationOrder_Click(object sender, DirectEventArgs e)
        {
            int sourceId = int.Parse(hdnSource.Text.Trim());
            int destId = int.Parse(hdnDest.Text.Trim());
            string mode = hdnDropMode.Text.Trim();

            NewPayrollManager.ChangeDesignationOrder(sourceId, destId, mode);

            LoadPositions();
        }

        protected void btnSubLevelSaveUpdate_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("InsertUpdateSubLevel");
            if (Page.IsValid)
            {

                BLevel entity = new BLevel();
                bool isInsert = true;
                if (!string.IsNullOrEmpty(hiddenValue.Text))
                {
                    isInsert = false;
                    entity.LevelId = int.Parse(hiddenValue.Text.Trim());
                }
                entity.ParentLevelId = int.Parse(cmbLevel2.SelectedItem.Value);
                BLevel parentLevel = NewPayrollManager.GetLevelById(entity.ParentLevelId.Value);
                entity.LevelGroupId = parentLevel.LevelGroupId;
                entity.Name = txtLevelName2.Text.Trim();
                entity.Code = txtLevelCode2.Text.Trim();
                entity.Order = (int)txtLevelOrder2.Number;
                entity.IsAM = true;
                

                Status status = NewPayrollManager.InsertUpdateLevel(entity, isInsert);

                if (status.IsSuccess)
                {
                    WindowSubLevel.Hide();
                    LoadLevels();

                    if (isInsert)
                        NewMessage.ShowNormalMessage("Sub-Level saved, please resave \"Change Salary Scale\" to add salary scale for this level.");
                    else
                        NewMessage.ShowNormalMessage("Sub-Level updated.");
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }
                
            }
        }
        protected void btnLevelSaveUpdate_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("InsertUpdateLevel");
            if (Page.IsValid)
            {

                BLevel entity = new BLevel();
                bool isInsert = true;
                if (!string.IsNullOrEmpty(hiddenValue.Text))
                {
                    isInsert = false;
                    entity.LevelId = int.Parse(hiddenValue.Text.Trim());
                }
                entity.LevelGroupId = int.Parse(cmbGroup.SelectedItem.Value);
                entity.Name = txtLevelName.Text.Trim();
                entity.Code = txtLevelCode.Text.Trim();
                entity.Order = (int)txtLevelOrder.Number;
                entity.ClericalType = int.Parse(cmbClericalType.SelectedItem.Value);
                //bool IsOrderExits = NewPayrollManager.IsLevelOrderExists(entity.LevelId, entity.Order);
                //if (IsOrderExits)
                //{
                //    NewMessage.ShowNormalMessage("Order already exists.");
                //    return;
                //}

                bool IsLeveNameAlreadyExistsForThisGroup = NewPayrollManager.IsLevelNameExists(entity.LevelId, entity.LevelGroupId, entity.Name);
                if (IsLeveNameAlreadyExistsForThisGroup)
                {
                    NewMessage.ShowNormalMessage("Level name already exists for this group.");
                    return;
                }


                

                Status status = NewPayrollManager.InsertUpdateLevel(entity, isInsert);

                if (status.IsSuccess)
                {
                    WindowLevel.Hide();
                    LoadLevels();

                    if (isInsert)
                        NewMessage.ShowNormalMessage("Level saved, please resave \"Change Salary Scale\" to add salary scale for this level.");
                    else
                        NewMessage.ShowNormalMessage("Level updated.");
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }
                
            }
        }

        public void btnExport_Click(object sender, EventArgs e)
        {
            List<BLevel> list = NewPayrollManager.GetAllParentLevelList();

            List<string> hiddenList = new List<string>();

            hiddenList = new List<string>{ "LevelId","LevelGroupId","CreatedBy","CreatedOn","ModifiedBy","ModifiedOn","IsActive",
                    "IncomeId","IsAM","ParentLevelId", "ParentName", "LoanAmount", "GroupLevel", "EDesignations", "AppraisalRollouts", "ClericalType"};

            Bll.ExcelHelper.ExportToExcel("Levels And Positions", list,
                hiddenList,
            new List<String>() { },
            new Dictionary<string, string>() { { "GroupName", "Group" }, { "ClericalTypeName", "Clerical Type" } },
            new List<string>() { }
            , new Dictionary<string, string>() {{"Levels And Positions",""} }
            , new List<string> { "GroupName", "Name", "Code", "Order", "ClericalTypeName" });
        }

        protected void btnExportDesignation_Click(object sender, EventArgs e)
        {
            List<EDesignation> list = NewPayrollManager.GetAllPositionOrDesignationsList();

            List<string> hiddenList = new List<string>();

            hiddenList = new List<string> { "DesignationId", "CompanyId", "LevelId", "CategoryId", "GroupId", "CodeAndName", "LevelAndDesignation", "AppraisalCompetencyPositions", "EEmployees" };

            Bll.ExcelHelper.ExportToExcel("Desginations", list,
                hiddenList,
            new List<String>() { },
            new Dictionary<string, string>() { { "Name", "Designation" }, { "LevelName", "Level/Position" } },
            new List<string>() { }
            , new Dictionary<string, string>() { { "Desginations", "" } }
            , new List<string> { "Name", "Category", "Group", "Code", "Order", "LevelName" });
        }

        protected void LevelData_Refresh(object sender, StoreReadDataEventArgs e)
        {
            LoadLevels();
        }

        protected void PositionData_Refresh(object sender, StoreReadDataEventArgs e)
        {
            LoadPositions();
        }

    }
}