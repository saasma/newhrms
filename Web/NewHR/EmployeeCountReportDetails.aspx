﻿<%@ Page Title="Employee Count Details" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="EmployeeCountReportDetails.aspx.cs" Inherits="Web.NewHR.EmployeeCountReportDetails" %>

<%@ Register Src="~/newhr/UserControls/EmployeeContractTabStrip.ascx" TagName="EmployeeContractTabStripCtl"
    TagPrefix="ucEmployeeContractTabStrip" %>
<%@ Register Src="~/NewHR/UserControls/ActingListCtl.ascx" TagName="FamilyCtl" TagPrefix="uc1" %>
<%@ Register Src="~/NewHR/UserControls/EmployeeDetailsCtl.ascx" TagName="EmployeeDetailsCtl"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function focusEvent(e1, tab) {

            <%=hdnFilterValue.ClientID %>.setValue(tab.title.toString());
            <%=btnLoad.ClientID %>.fireEvent('click');

        };

        var rendername = function(e1,e2,e3)
        {
            return "<a href='EmployeeCountReportDetails.aspx?type=" + <%=hdnFilterValue.ClientID %>.getValue()+ "&value=" + e1 
            + "'>" + e1 + "</a>";
        }
    </script>
    <style type="text/css">
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <ext:Hidden ID="hdnFilterValue" runat="server" Text="Position">
    </ext:Hidden>
    <ext:Button ID="btnLoad" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnLoad_Change">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>
    <div class="separator bottom">
    </div>
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4 runat="server" id="details">
                    Employee Count Details of 
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel" style="padding-top: 10px">
        <div class="widget">
            <div class="widget-body">
                <div>
                </div>
                <div class="right">
                    <asp:LinkButton ID="btnExport" runat="server" Text="Export to Excel" OnClick="btnExport_Click"
                        CssClass=" excel marginRight" Style="float: left;" />
                </div>
                <div style="clear: both">
                </div>
                <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridEmployee" runat="server" Cls="itemgrid">
                    <Store>
                        <ext:Store ID="Store3" runat="server">
                            <Model>
                                <ext:Model ID="Model4" runat="server">
                                    <Fields>
                                    <ext:ModelField Name="SN" Type="string" />
                                        <ext:ModelField Name="EmployeeId" Type="string" />
                                        <ext:ModelField Name="IDCardNo" Type="string" />
                                        <ext:ModelField Name="Name" Type="string" />
                                        <ext:ModelField Name="Branch" Type="string" />
                                        <ext:ModelField Name="Department" Type="string" />
                                        <ext:ModelField Name="LevelPosition" Type="string" />
                                        <ext:ModelField Name="Designation" Type="string" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                    </Store>
                    <ColumnModel>
                        <Columns>
                            <ext:Column ID="Column6" Sortable="false" MenuDisabled="true" runat="server" Text="SN"
                                Align="Right" Width="50" DataIndex="SN" />
                            <ext:Column ID="Column9" Sortable="true" MenuDisabled="false" runat="server" Text="EIN"
                                Align="Right" Width="50" DataIndex="EmployeeId">
                            </ext:Column>
                            <ext:Column ID="Column8" Sortable="true" MenuDisabled="false" runat="server" Text="I No"
                                Align="Right" Width="50" DataIndex="IDCardNo" />
                            <ext:Column ID="Column1" Sortable="true" MenuDisabled="false" runat="server" Text="Name"
                                Align="Left" Width="150" DataIndex="Name" />
                            <ext:Column ID="Column2" Sortable="true" MenuDisabled="false" runat="server" Text="Branch"
                                Align="Left" Width="150" DataIndex="Branch" />
                            <ext:Column ID="Column3" Sortable="true" MenuDisabled="false" runat="server" Text="Department"
                                Align="Left" Width="150" DataIndex="Department" />
                            <ext:Column ID="Column4" Sortable="true" MenuDisabled="false" runat="server" Text="Position"
                                Align="Left" Width="150" DataIndex="LevelPosition" />
                            <ext:Column ID="Column5" Sortable="true" MenuDisabled="false" runat="server" Text="Designation"
                                Align="Left" Width="150" DataIndex="Designation" />
                        </Columns>
                    </ColumnModel>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel2111" runat="server" Mode="Single" />
                    </SelectionModel>
                </ext:GridPanel>
                <div style="clear: both">
                </div>
            </div>
        </div>
    </div>
    <br />
</asp:Content>
