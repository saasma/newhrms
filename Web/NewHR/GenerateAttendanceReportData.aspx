﻿<%@ Page Title="Attendance Report Date" EnableEventValidation="false" ValidateRequest="false"
    Language="C#" MasterPageFile="~/Master/NewDetails.Master" AutoEventWireup="true"
    CodeBehind="GenerateAttendanceReportData.aspx.cs" Inherits="Web.NewHR.GenerateAttendanceReportData" %>

<%@ Register Src="~/Employee/UserControls/AssignLeave.ascx" TagName="AssingLeave"
    TagPrefix="uc5" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
        
        .RequestRow td
        {
            background-color: #FFF3C3 !important;
        }
        .RecommendedRow td
        {
            background-color: #B5D2FF !important;
        }
        .ApprovedRow td
        {
            background-color: #C6E0B4 !important;
        }
        .innerLR
        {
            padding-top: -20px !important;
        }
    </style>
    <script type="text/javascript">


    </script>
    <script type="text/javascript" src="<%= ResolveUrl("~/Employee/common.js?v=") + Web.Helper.WebHelper.Version %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Update Attendance Report
                </h4>
            </div>
        </div>
    </div>
    <div class="separator bottom">
        <ext:Hidden ID="hdnFromDate" runat="server" />
        <ext:Hidden ID="hdnToDate" runat="server" />
        <ext:Hidden ID="HiddenLeaveRequestId" runat="server" Text="" />
    </div>
    <div class="contentpanel">
        <div class="innerLR">
            <ext:Label ID="lblMsg" runat="server" />
            <div class="alert alert-info" style="margin-top: -15px;">
                <table class="fieldTable firsttdskip" style="margin-top: 00px !important;" runat="server"
                    id="tbl">
                    <tr>
                        <%-- <td>
                            <ext:DisplayField ID="dispLastDate" runat="server" FieldLabel="Last Processed Date"
                                LabelAlign="Top" />
                        </td>--%>
                        <td>
                            <ext:DateField runat="server" Width="120" ID="txtFromDate" LabelAlign="Top" FieldLabel="From">
                                <Plugins>
                                    <ext:GenericPlugin ID="GenericPlugin1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                                </Plugins>
                            </ext:DateField>
                        </td>
                        <td>
                            <ext:DateField runat="server" Width="120" ID="txtToDate" LabelAlign="Top" FieldLabel="To">
                                <Plugins>
                                    <ext:GenericPlugin ID="GenericPlugin2" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                                </Plugins>
                            </ext:DateField>
                        </td>
                        <td>
                            <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                                <Proxy>
                                    <ext:AjaxProxy Url="../Handler/EmpSearch.ashx">
                                        <ActionMethods Read="GET" />
                                        <Reader>
                                            <ext:JsonReader Root="plants" TotalProperty="total" />
                                        </Reader>
                                    </ext:AjaxProxy>
                                </Proxy>
                                <Model>
                                    <ext:Model ID="Model2" IDProperty="Name" Name="ItemLineModel" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Name" Type="String" />
                                            <ext:ModelField Name="EmployeeId" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                            <ext:ComboBox ID="cmbSearch" FieldLabel="Search Employee" LabelWidth="70" LabelAlign="Top"
                                runat="server" DisplayField="Name" ValueField="EmployeeId" StoreID="storeSearch"
                                TypeAhead="false" Width="220" PageSize="9999" HideBaseTrigger="true" MinChars="1"
                                TriggerAction="All" ForceSelection="true">
                                <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                                    <ItemTpl ID="ItemTpl1" runat="server">
                                        <Html>
                                            <tpl>
                                            <div class="search-item">
                                                            <span>{Name}</span>  
                                             </div>
					                </tpl>
                                        </Html>
                                    </ItemTpl>
                                </ListConfig>
                                <Triggers>
                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                </Triggers>
                                <Listeners>
                                    <Select Handler="this.getTrigger(0).show();" />
                                    <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                    <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                                </Listeners>
                            </ext:ComboBox>
                        </td>
                        <td style="padding-top: 20px">
                            <ext:Button ID="btnGenerate" runat="server" Cls="btn btn-default" Text="Generate Report">
                                <DirectEvents>
                                    <Click OnEvent="btnGenerate_Click" Timeout="999999999">
                                        <EventMask ShowMask="true" />
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </td>
                    </tr>
                </table>
            </div>
            <ext:GridPanel StyleSpec="margin-top:15px;" ID="grid" runat="server" Cls="itemgrid">
                <Store>
                    <ext:Store PageSize="1000" ID="storeGrid" runat="server" AutoLoad="true" RemotePaging="false"
                        RemoteSort="false">
                        <Model>
                            <ext:Model ID="Model1" runat="server">
                                <Fields>
                                    <ext:ModelField Name="ID" Type="String" />
                                    <ext:ModelField Name="LastUptoDate" Type="Date" />
                                    <ext:ModelField Name="Date" Type="Date" />
                                    <ext:ModelField Name="LastDate" Type="Date" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel ID="ColumnModel1" runat="server">
                    <Columns>
                        <ext:DateColumn ID="DateColumn1" runat="server" Text="Processed Date" DataIndex="LastUptoDate"
                             MenuDisabled="false" Sortable="true" Align="Left" Width="200" />
                        <ext:DateColumn ID="DateColumn2" runat="server" Text="From" DataIndex="Date" Format="dd-MMM-yyyy"
                            MenuDisabled="false" Sortable="true" Align="Left" Width="100" />
                        <ext:DateColumn ID="DateColumn3" runat="server" Text="To" DataIndex="LastDate" Format="dd-MMM-yyyy"
                            MenuDisabled="false" Sortable="true" Align="Left" Width="100" />
                    </Columns>
                </ColumnModel>
            </ext:GridPanel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
