﻿<%@ Page Title="Loan Repayment" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="LoanRepayment.aspx.cs" Inherits="Web.NewHR.LoanRepayment" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
    var blockMultipleFormSubmit = true;
    function refreshWindow() {
       // alert('h');
        <%=btnLoad.ClientID %>.fireEvent('click');
    }

    function isPayrollSelected(btn) {
        if(<%=cmbLoan.ClientID %>.value == null)
        {
            return;
        }
        var ret = shiftPopup("DeductionId=" + <%=cmbLoan.ClientID %>.value);
        return false;
    }

    var columnRenderer = function (value, meta, record) {
        if (value == null)
            return "";

        if (value == "")
            return "";
        return value.toFixed(2);
    }

    var afterEdit = function (e) {
    };

    var amountRenderer = function (value, meta, record) {

        if (typeof (value) == 'undefined' || value == null)
            return "";

        var value = parseFloat(value);

        if (value < 0) {

            meta.attr = 'style="border: 1px solid red;"';
            meta.attr += ('title="Negative value."');
        }

        return value.toFixed(2);
    };
      function searchList() {
             <%=gridLoanRepayment.ClientID %>.getStore().pageSize = <%=cmbPageSize.ClientID %>.getValue();
            <%=PagingToolbar1.ClientID %>.doRefresh();
        }

    </script>
    <style type="text/css">
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4 runat="server" id="title">
                    Loan Repayment</h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="attribute" style="padding: 10px">
            <table>
                <tr>
                    <td style=''>
                        <ext:ComboBox ValueField="DeductionId" FieldLabel="Loan" Width="200" LabelWidth="50"
                            LabelAlign="Left" DisplayField="Title" ID="cmbLoan" runat="server">
                            <Store>
                                <ext:Store ID="Store1" runat="server">
                                    <Model>
                                        <ext:Model ID="modelLoan" runat="server">
                                            <Fields>
                                                <ext:ModelField runat="server" Name="DeductionId" Type="Int" />
                                                <ext:ModelField runat="server" Name="Title" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <DirectEvents>
                                <Change OnEvent="btnLoad_Click">
                                    <EventMask ShowMask="true" />
                                </Change>
                            </DirectEvents>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator4" runat="server"
                            ValidationGroup="SaveUpdate" ControlToValidate="cmbLoan" ErrorMessage="Loan is required." />
                    </td>
                    <td style="padding-left: 20px">
                        <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                            <Proxy>
                                <ext:AjaxProxy Url="../Handler/EmpSearchID.ashx">
                                    <ActionMethods Read="GET" />
                                    <Reader>
                                        <ext:JsonReader Root="plants" TotalProperty="total" />
                                    </Reader>
                                </ext:AjaxProxy>
                            </Proxy>
                            <Model>
                                <ext:Model ID="Model5" IDProperty="Name" Name="ItemLineModel" runat="server">
                                    <Fields>
                                        <ext:ModelField Name="Name" Type="String" />
                                        <ext:ModelField Name="EmployeeId" Type="String" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                        <ext:ComboBox LabelSeparator="" ID="cmbEmpSearch" FieldLabel="Search Employee" EmptyText="Employee Name"
                            LabelWidth="120" LabelAlign="Left" runat="server" DisplayField="Name" ValueField="EmployeeId"
                            StoreID="storeSearch" TypeAhead="false" Width="350" PageSize="9999" HideBaseTrigger="true"
                            MinChars="1" TriggerAction="All" ForceSelection="false">
                            <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                                <ItemTpl ID="ItemTpl2" runat="server">
                                    <Html>
                                        <tpl>
                                                        <div class="search-item">
                                                                        <span>{Name}</span>  
                                                         </div>
					                            </tpl>
                                    </Html>
                                </ItemTpl>
                            </ListConfig>
                            <Triggers>
                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                            </Triggers>
                            <Listeners>
                                <Select Handler="this.getTrigger(0).show(); SelEmpSearch();" />
                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                <TriggerClick Handler="if (index == 0) { 
                                                       this.clearValue(); 
                                                       this.getTrigger(0).hide();
                                                   }" />
                            </Listeners>
                        </ext:ComboBox>
                    </td>
                    <td style="padding-left: 20px">
                        <ext:Button ID="btnLoad" runat="server" Cls="btn btn-default btn-sect btn-sm" Width="100"
                            Height="30" Text="Show">
                            <DirectEvents>
                                <Click OnEvent="btnLoad_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                            <Listeners>
                                <Click Handler="">
                                </Click>
                            </Listeners>
                        </ext:Button>
                    </td>
                    <td style='padding-left: 10px'>
                        <asp:LinkButton ID="btnExport" runat="server" Text="Import from Excel" OnClientClick="valGroup = 'SaveUpdate'; if(CheckValidation()) return isPayrollSelected(this); else return false;"
                            CssClass=" excel marginRight tiptip" Style="float: left;" />
                    </td>
                </tr>
            </table>
        </div>
        <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
        <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
        <div style="clear: both" runat="server" id="gridContainer">
        </div>
        <br />
        <ext:GridPanel ID="gridLoanRepayment" ClicksToEdit="1" Border="true" StripeRows="true"
            Header="false" runat="server" Title="Title" Cls="gridtbl">
            <Store>
                <ext:Store ID="storeLoanRepayment" runat="server" AutoLoad="false" OnReadData="Store_ReadData"
                    RemotePaging="true" RemoteSort="true">
                    <Proxy>
                        <ext:PageProxy />
                    </Proxy>
                    <Model>
                        <ext:Model ID="Model1" runat="server">
                            <Fields>
                                <ext:ModelField Name="EmployeeDeductionId" Type="Int" />
                                <ext:ModelField Name="EmployeeId" Type="Int" />
                                <ext:ModelField Name="EmployeeName" Type="String" />
                                <ext:ModelField Name="Amount" Type="Float" />
                                <ext:ModelField Name="InstallmentNo" Type="String" />
                                <ext:ModelField Name="DateFrom" Type="String" />
                                <ext:ModelField Name="DateTo" Type="String" />
                                 <ext:ModelField Name="TakenOnEng" Type="Date" />
                                <ext:ModelField Name="StartingFromEng" Type="Date" />
                                <ext:ModelField Name="LastPaymentEng" Type="Date" />
                                <ext:ModelField Name="NoOfInstallments" Type="String" />
                                <ext:ModelField Name="LoanAccount" Type="String" />
                                <ext:ModelField Name="INo" Type="String" />
                                <ext:ModelField Name="AdvanceLoanAmount" Type="Float" />
                                <ext:ModelField Name="TotalInstallment" Type="String" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>
            <Listeners>
            </Listeners>
            <SelectionModel>
                <ext:RowSelectionModel ID="RowSelectionModel11" runat="server">
                </ext:RowSelectionModel>
            </SelectionModel>
            <ColumnModel runat="server" ID="columnModel">
                <Columns>
                    <ext:Column ID="Column1" Locked="true" runat="server" Sortable="false" MenuDisabled="true"
                        Header="EIN" Width="50" DataIndex="EmployeeId">
                    </ext:Column>
                    <ext:Column ID="Column4" Locked="true" runat="server" Sortable="false" MenuDisabled="true"
                        Header="I No" Width="50" DataIndex="INo">
                    </ext:Column>
                    <ext:Column runat="server" Locked="true" Sortable="false" MenuDisabled="true" Header="Name"
                        Width="150" ColumnID="EmployeeName" DataIndex="EmployeeName">
                    </ext:Column>
                    <ext:Column ID="Column6" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                        Header="Total Installment" Width="120" DataIndex="TotalInstallment">
                    </ext:Column>
                    <ext:NumberColumn ID="colAmount" runat="server" Sortable="false" Align="Right" MenuDisabled="true"
                        Header="Monthly Repayment" Width="130" ColumnID="Amount" DataIndex="Amount">
                    </ext:NumberColumn>
                    <ext:NumberColumn ID="NumberColumn1" runat="server" Sortable="false" Align="Right"
                        MenuDisabled="true" Header="Loan Amount" Width="100" ColumnID="AdvanceLoanAmount"
                        DataIndex="AdvanceLoanAmount">
                    </ext:NumberColumn>
                    <ext:Column ID="Column5" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                        Header="Installment No" Width="110" DataIndex="InstallmentNo">
                    </ext:Column>
                      <ext:DateColumn Format="yyyy/MM/dd" ID="DateColumn2" runat="server" Sortable="false"
                        Align="Left" MenuDisabled="true" Header="Taken On" Width="90" DataIndex="TakenOnEng">
                    </ext:DateColumn>
                    <ext:Column ID="colStartingFrom" runat="server" Sortable="false" Align="Left" MenuDisabled="true"
                        Header="Start Year/Month" Width="120" DataIndex="DateFrom">
                    </ext:Column>
                    <ext:DateColumn Format="yyyy/MM/dd" ID="Column7" runat="server" Sortable="false"
                        Align="Left" MenuDisabled="true" Header="Start Date" Width="90" DataIndex="StartingFromEng">
                    </ext:DateColumn>
                    <ext:Column ID="Column2" runat="server" Sortable="false" Align="Left" MenuDisabled="true"
                        Header="End Year/Month" Width="120" DataIndex="DateTo">
                    </ext:Column>
                    <ext:DateColumn Format="yyyy/MM/dd" ID="DateColumn1" runat="server" Sortable="false"
                        Align="Left" MenuDisabled="true" Header="End Date" Width="90" DataIndex="LastPaymentEng">
                    </ext:DateColumn>
                    <ext:Column ID="Column_Percent" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                        Header="No of Deductions" Width="110" ColumnID="NoOfInstallments" DataIndex="NoOfInstallments">
                    </ext:Column>
                    <ext:Column ID="Column3" runat="server" Sortable="false" Align="Left" MenuDisabled="true"
                        Header="Loan Account" Width="120" ColumnID="LoanAccount" DataIndex="LoanAccount">
                    </ext:Column>
                </Columns>
            </ColumnModel>
            <BottomBar>
                <ext:PagingToolbar ID="PagingToolbar1" runat="server" StoreID="storeLoanRepayment"
                    DisplayInfo="true">
                    <Items>
                        <ext:Label ID="lblPageSize" runat="server" Text="Page size:" />
                        <ext:ToolbarSpacer ID="ToolbarSpacer_listItems" runat="server" Width="10" />
                        <ext:ComboBox ID="cmbPageSize" runat="server" Width="80" SelectOnFocus="true" Selectable="true"
                            ValueField="Value" DisplayField="Text" ForceSelection="true" AllowBlank="false">
                            <Listeners>
                                <Select Handler="searchList()" />
                                <%--#{cmbPageSize}.pageSize = this.getValue();#{PagingToolbar1}.moveFirst();--%>
                            </Listeners>
                            <Items>
                                <ext:ListItem Value="20" Text="20" />
                                <ext:ListItem Value="50" Text="50" />
                                <ext:ListItem Value="100" Text="100" />
                            </Items>
                            <SelectedItems>
                                <ext:ListItem Index="0">
                                </ext:ListItem>
                            </SelectedItems>
                        </ext:ComboBox>
                    </Items>
                </ext:PagingToolbar>
            </BottomBar>
        </ext:GridPanel>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
