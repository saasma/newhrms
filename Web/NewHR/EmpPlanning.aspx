﻿<%@ Page Title="Employee Plan" Language="C#" MasterPageFile="~/Master/NewDetails.Master" AutoEventWireup="true" CodeBehind="EmpPlanning.aspx.cs" Inherits="Web.NewHR.EmpPlanning" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

<style type="text/css">

   .treepanelCls
   {
       margin-left:80px;
       margin-top:50px;
   }
   .BlueCls .x-grid-cell
   {
       background-color:#59afee !important;
       color:White;
   }
   .LightBlue .x-grid-cell
   {
       background-color:#DFEAF2 !important;
       color:black !important;
   }
   
   .alignTextCenter
   {
       text-align : center;
   }
   
  
</style>

<script type="text/javascript">

    var template = '<span style="color:{0};">{1}</span>';

    var change1 = function (value) {
        if (value == 0) {
            return Ext.String.format(template, "black", value);
        }
        else if (value < 0) {
            return Ext.String.format(template, "red", value);
        }
        else {
            return Ext.String.format(template, "green", value);
        }
    };


    var applyRowBackground = function (record, rowIndex, rowParams, store) {
        return record.data.CssClass;
    };


    var PlannedEmpChange = function () {
        var plannedEmp = <%= txtPlannedEmployeeNo.ClientID %>.getValue();
        var existEmp = <%= txtExistingEmployeeNo.ClientID %>.getValue();

        if(plannedEmp == null)
            return;

        var res = plannedEmp - existEmp;
        var txtShortExcessNoEl = <%= txtShortExcessNo.ClientID %>;
        txtShortExcessNoEl.setValue(Math.abs(res).toString());
        if(res > 0)
        {
            txtShortExcessNoEl.setFieldStyle('background-color: green;');
           
        }
        else if(res < 0 )
        {   
            txtShortExcessNoEl.setFieldStyle('background-color: red;');
        }
        else
        {
            txtShortExcessNoEl.setFieldStyle('background-color: white;');
        }
    };
   

</script>



</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">


<div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Employee Plan
                </h4>
            </div>
        </div>
    </div>

    <div class="contentpanel">
        
            <div class="separator bottom">
            </div>

            <table>
            <tr>
                <td>
                   
                </td>
                <td style="width:200px;">
                    <ext:ComboBox ID="cmbBranch" runat="server" ValueField="BranchId" DisplayField="Name" FieldLabel="Branch Filter" Width="180"
                        LabelAlign="top" LabelSeparator="" ForceSelection="true" QueryMode="Local">
                        <Store>
                            <ext:Store ID="storeBranch" runat="server">
                                <Model>
                                    <ext:Model ID="Model8" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="BranchId" Type="String" />
                                            <ext:ModelField Name="Name" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <DirectEvents>
                             <Change OnEvent="cmbBranch_Change">
                                <EventMask ShowMask="true" />
                            </Change>
                        </DirectEvents>
                                      
                    </ext:ComboBox>
                </td>

                <td style="width:200px;">                  

                    <ext:ComboBox ID="cmbDepartment" runat="server" ValueField="DepartmentId" DisplayField="Name" FieldLabel="Department Filter" Width="180" 
                            LabelAlign="top" LabelSeparator="" ForceSelection="true" QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store7" runat="server">
                                    <Model>
                                        <ext:Model ID="Model7" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="DepartmentId" Type="String" />
                                                <ext:ModelField Name="Name" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <DirectEvents>
                                <Change OnEvent="cmbDepartment_Change">
                                    <EventMask ShowMask="true" />
                                </Change>     
                            </DirectEvents>  
                        </ext:ComboBox>

                </td>

                <td>
                    <ext:ComboBox ID="cmbSortBy" runat="server" FieldLabel="Sort By" Width="180"
                            LabelAlign="top" LabelSeparator="" ForceSelection="true" QueryMode="Local">
                            <Items>
                                <ext:ListItem Text="Branch" Value="Branch" />
                                <ext:ListItem Text="Department" Value="Department" />
                            </Items>  
                            <SelectedItems>
                                <ext:ListItem Text="Branch" Value="Branch" />
                            </SelectedItems> 
                             <DirectEvents>
                                <Change OnEvent="cmbSortBy_Change">
                                    <EventMask ShowMask="true" />
                                </Change>     
                            </DirectEvents>  
                        </ext:ComboBox>
                </td>
                
            </tr>
         
            
        </table>

       <br />

       <ext:Button runat="server" ID="btnAdd" Cls="btn btn-primary btn-sm btn-sect" Text="<i></i>Add New Plan">
                <DirectEvents>
                    <Click OnEvent="btnAdd_Click">
                        <EventMask ShowMask="true" />
                    </Click>
                </DirectEvents>
            </ext:Button>

            <br />
       

            <ext:TreePanel MultiSelect="true" Icon="Blank" ID="treeEmployeeHierarchy" RootVisible="false" Layout="AbsoluteLayout" Width="1050"
                    AutoScroll="true" Animate="true" Mode="Local" runat="server" Collapsible="false"
                    UseArrows="true" Title="Employee Plan" OnSubmit="SubmitNodes">
                    <Fields>
                        <ext:ModelField Name="Name" />
                        <ext:ModelField Name="LevelGroupId" />
                        <ext:ModelField Name="LevelId" />
                        <ext:ModelField Name="DesignationId" />
                        <ext:ModelField Name="BranchId" />
                        <ext:ModelField Name="Branch" Type="String" />
                        <ext:ModelField Name="DepartmentId" />
                        <ext:ModelField Name="Department" Type="String" />
                        <ext:ModelField Name="ExistingEmployees" />
                        <ext:ModelField Name="PlannedEmployees" />
                        <ext:ModelField Name="AvailableVacancies" Type="String" />
                        <ext:ModelField Name="IsPage" Type="String" />
                        <ext:ModelField Name="CssClass" Type="String" />
                    </Fields>
                    <ColumnModel>

                        <Columns>
                            <ext:TreeColumn ID="TreeColumn1" Width="300" runat="server" Text="" DataIndex="text" MenuDisabled="true" Sortable="false"
                                Flex="1">
                            </ext:TreeColumn>

                            <ext:Column ID="colBranch" Width="150" runat="server" Flex="1" MenuDisabled="true" Sortable="false" Text="Branch"
                                DataIndex="Branch" />
                            <ext:Column ID="colDepartment" Width="150" runat="server" Flex="1" MenuDisabled="true" Sortable="false" Text="Department"
                                DataIndex="Department" />

                            <ext:Column ID="colExistingEmployee" Width="150" runat="server" Align="Center" Text="Existing Employees" Flex="1" MenuDisabled="true" Sortable="false"
                                DataIndex="ExistingEmployees" />

                            <ext:Column ID="Column1" runat="server" DataIndex="PlannedEmployees" Align="Center" Text="Planned Employees" Width="150" MenuDisabled="true" Sortable="false">
                                <Editor>
                                    <ext:NumberField ID="NumberField1" runat="server" MinValue="0" >                                       
                                    </ext:NumberField>                                                                      
                                </Editor>                           
                            </ext:Column>                       
                            <ext:Column ID="colJD" Width="150" runat="server" Align="Center" Text="Available Vacancies" Flex="1" MenuDisabled="true" Sortable="false"
                                DataIndex="AvailableVacancies">
                                    <Renderer Fn="change1" />
                                </ext:Column>

                        </Columns>
                    </ColumnModel>                    
                    <Listeners>
                    </Listeners>
                    <View>
                        <ext:TreeView ID="TreeView1" runat="server">                            
                            <GetRowClass Fn="applyRowBackground" />                           
                        </ext:TreeView>                       
                      
                    </View>
                    
                    <Plugins>                
                        <ext:CellEditing ID="CellEditing1" runat="server" ClicksToEdit="1" >
                        <Listeners>
                            <BeforeEdit Handler="if (
                                             !(e.record.get('IsPage') === 'Yes')) {                                        
                                                 return false; //to cancel editing          
                                             }"/>
                        </Listeners>                          
                        </ext:CellEditing>
                    </Plugins>
                     <Buttons>
                        <ext:Button ID="Button1" runat="server" Text="Save">
                            <Listeners>
                                <Click Handler="#{treeEmployeeHierarchy}.submitNodes();" />
                            </Listeners>
                        </ext:Button>
                    </Buttons>
                </ext:TreePanel>

                <br />

    </div>


          <ext:Window ID="WEmployeePlan" runat="server" Title="Add New Plan" Icon="Application"
            Width="500" Height="450" BodyPadding="5" Hidden="true" Modal="true">
            <Content>
                <table class="fieldTable">
                    <tr>
                        <td>
                           <ext:ComboBox ID="cmbGroup" runat="server" ValueField="LevelGroupId" DisplayField="Name" FieldLabel="Group *" Width="200" 
                                LabelAlign="top" LabelSeparator="" ForceSelection="true" QueryMode="Local">
                                <Store>
                                    <ext:Store ID="Store1" runat="server">
                                        <Model>
                                            <ext:Model ID="Model1" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="LevelGroupId" Type="String" />
                                                    <ext:ModelField Name="Name" Type="String" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>
                                    <Select OnEvent="cmbGroupName_Change">
                                        <EventMask ShowMask="true" />
                                    </Select>
                                </DirectEvents>  
                            </ext:ComboBox>
                            <asp:RequiredFieldValidator Display="None" ID="rfvGroup" runat="server"
                                ValidationGroup="SaveEmployeePlan" ControlToValidate="cmbGroup" ErrorMessage="Please select a Group." />
                        </td>
                        <td>
                            <ext:TextField ID="txtExistingEmployeeNo" LabelSeparator="" Disabled="true" runat="server" FieldLabel="Existing Employees" FieldCls="alignTextCenter"
                                Width="200" LabelAlign="Top">                              
                            </ext:TextField>                            
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ext:ComboBox ID="cmbLevel" runat="server" ValueField="LevelId" DisplayField="Name" FieldLabel="Group *" Width="200" 
                                LabelAlign="top" LabelSeparator="" ForceSelection="true" QueryMode="Local">
                                <Store>
                                    <ext:Store ID="Store2" runat="server">
                                        <Model>
                                            <ext:Model ID="Model2" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="LevelId" Type="String" />
                                                    <ext:ModelField Name="Name" Type="String" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>
                                    <Select OnEvent="cmbLevel_Change">
                                        <EventMask ShowMask="true" />
                                    </Select>
                                </DirectEvents>  
                            </ext:ComboBox>  
                            <asp:RequiredFieldValidator Display="None" ID="rfvLevel" runat="server"
                                ValidationGroup="SaveEmployeePlan" ControlToValidate="cmbLevel" ErrorMessage="Please select a Level." />                          
                        </td>
                         <td>
                            <ext:TextField ID="txtPlannedEmployeeNo" LabelSeparator="" MaskRe="/[\d]+/" runat="server" FieldLabel="Planned Employees" FieldCls="alignTextCenter"
                                Width="200" LabelAlign="Top">   
                                <Listeners>
                                    <Change Fn="PlannedEmpChange" />
                                </Listeners>                             
                            </ext:TextField> 
                            <asp:RequiredFieldValidator Display="None" ID="rfvPlannedEmployeeNo" runat="server"
                                ValidationGroup="SaveEmployeePlan" ControlToValidate="txtPlannedEmployeeNo" ErrorMessage="Please enter planned employees number." />       
                        </td>
                    </tr>

                       <tr>
                        <td>
                            <ext:ComboBox ID="cmbDesignation" runat="server" ValueField="DesignationId" DisplayField="Name" FieldLabel="Designation *" Width="200" 
                                LabelAlign="top" LabelSeparator="" ForceSelection="true" QueryMode="Local">
                                <Store>
                                    <ext:Store ID="Store3" runat="server">
                                        <Model>
                                            <ext:Model ID="Model3" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="DesignationId" Type="String" />
                                                    <ext:ModelField Name="Name" Type="String" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                            </ext:ComboBox>       
                            <asp:RequiredFieldValidator Display="None" ID="rfvDesignation" runat="server"
                                ValidationGroup="SaveEmployeePlan" ControlToValidate="cmbDesignation" ErrorMessage="Please select Desgination." />                     
                        </td>
                         <td>
                            <ext:TextField ID="txtShortExcessNo" LabelSeparator="" runat="server" Disabled="true" FieldLabel="Short(Excess)" FieldCls="alignTextCenter"
                                Width="200" LabelAlign="Top">
                            </ext:TextField> 
                        </td>
                    </tr>

                       <tr>
                        <td>
                            <ext:ComboBox ID="cmbBranchAdd" runat="server" ValueField="BranchId" DisplayField="Name" FieldLabel="Branch *" Width="200" 
                                LabelAlign="top" LabelSeparator="" ForceSelection="true" QueryMode="Local">
                                <Store>
                                    <ext:Store ID="Store4" runat="server">
                                        <Model>
                                            <ext:Model ID="Model4" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="BranchId" Type="String" />
                                                    <ext:ModelField Name="Name" Type="String" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                            </ext:ComboBox>  
                            <asp:RequiredFieldValidator Display="None" ID="rfvBranch" runat="server"
                                ValidationGroup="SaveEmployeePlan" ControlToValidate="cmbBranchAdd" ErrorMessage="Please select Branch." />                              
                        </td>
                         <td>
                          
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ext:ComboBox ID="cmbDepartmentAdd" runat="server" ValueField="DepartmentId" DisplayField="Name" FieldLabel="Department *" Width="200" 
                                LabelAlign="top" LabelSeparator="" ForceSelection="true" QueryMode="Local">
                                <Store>
                                    <ext:Store ID="Store5" runat="server">
                                        <Model>
                                            <ext:Model ID="Model5" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="DepartmentId" Type="String" />
                                                    <ext:ModelField Name="Name" Type="String" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>
                                    <Select OnEvent="cmbDepartmentAdd_Change">
                                        <EventMask ShowMask="true" />
                                    </Select>
                                </DirectEvents>  
                            </ext:ComboBox>   
                            <asp:RequiredFieldValidator Display="None" ID="rfvDepartment" runat="server"
                                ValidationGroup="SaveEmployeePlan" ControlToValidate="cmbDepartmentAdd" ErrorMessage="Please select Department." />     
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td valign="bottom" colspan="2">
                            <div class="popupButtonDiv">
                                <ext:Button runat="server" ID="btnSaveEmpPlan" Cls="btn btn-primary" Text="<i></i>Save">
                                    <DirectEvents>
                                        <Click OnEvent="btnSaveEmpPlan_Click">
                                            <EventMask ShowMask="true" />
                                        </Click>
                                    </DirectEvents>
                                    <Listeners>
                                        <Click Handler="valGroup = 'SaveEmployeePlan'; return CheckValidation();">
                                        </Click>
                                    </Listeners>
                                </ext:Button>
                                <div class="btnFlatOr">
                                    </div>

                                <ext:Button runat="server" ID="Button2" Cls="btn btn-primary" Text="<i></i>Cancel">
                                    <Listeners>
                                        <Click Handler="#{WEmployeePlan}.hide();">
                                        </Click>
                                    </Listeners>
                                </ext:Button>
                            </div>
                        </td>
                    </tr>
                </table>
            </Content>
        </ext:Window>


</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
