﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Entity;
using BLL;
using System.IO;
using Utils;
using Utils.Calendar;
using BLL.BO;

namespace Web.NewHR
{
    public partial class HPLDutyPeriodList : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
                Initialise();
        }

        private void Initialise()
        {
            List<TextValue> list = new List<TextValue>();
            int year = DateTime.Now.Year;
            list.Add(new TextValue() { Text = (year - 2).ToString(), Value = (year - 2).ToString() });
            list.Add(new TextValue() { Text = (year - 1).ToString(), Value = (year - 1).ToString() });
            list.Add(new TextValue() { Text = (year).ToString(), Value = (year).ToString() });
            list.Add(new TextValue() { Text = (year + 1).ToString(), Value = (year + 1).ToString() });
            list.Add(new TextValue() { Text = (year + 2).ToString(), Value = (year + 2).ToString() });
            list.Add(new TextValue() { Text = (year + 3).ToString(), Value = (year + 3).ToString() });

            storeYear.DataSource = list;
            storeYear.DataBind();
            
        }

        protected void btnGenerate_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveUpdate");
            if (Page.IsValid)
            {
                List<HPLDutyPeriod> list = new List<HPLDutyPeriod>();

                int k = 0;

                if (cmbYear.SelectedItem == null || cmbYear.SelectedItem.Value == null)
                {
                    NewMessage.ShowWarningMessage("Please select year.");
                    cmbYear.Focus();
                    return;
                }

                int year = int.Parse(cmbYear.SelectedItem.Value);
                DateTime startDate = new DateTime(year, 1, 1);


                for (int i = 1; i <= 52; i++)
                {
                    HPLDutyPeriod obj = new HPLDutyPeriod();

                    obj.Year = year;
                    obj.StartDate = startDate.AddDays(k);
                    obj.EndDate = obj.StartDate.Value.AddDays(6);
                    obj.PeriodName = string.Format("{0}-{1}", obj.StartDate.Value.Day, obj.EndDate.Value.Day);

                    if (obj.StartDate.Value.Month == obj.EndDate.Value.Month)
                        obj.MonthName = DateHelper.GetMonthShortName(obj.StartDate.Value.Month, true);
                    else
                        obj.MonthName = string.Format("{0}-{1}", DateHelper.GetMonthShortName(obj.StartDate.Value.Month, true), DateHelper.GetMonthShortName(obj.EndDate.Value.Month, true));

                    obj.WeekNo = i;

                    k = 1;
                    startDate = obj.EndDate.Value;
                    list.Add(obj);

                }

                HPLDutyPeriod lastObj = new HPLDutyPeriod();
                lastObj.Year = year;
                lastObj.StartDate = startDate.AddDays(1);

                if (DateTime.IsLeapYear(startDate.Year))
                {
                    lastObj.EndDate = lastObj.StartDate.Value.AddDays(1);
                    lastObj.PeriodName = string.Format("{0}-{1}", lastObj.StartDate.Value.Day, lastObj.EndDate.Value.Day);
                }
                else
                {
                    lastObj.EndDate = lastObj.StartDate.Value.AddDays(0);
                    lastObj.PeriodName = lastObj.StartDate.Value.Day.ToString();
                }

                lastObj.MonthName = DateHelper.GetMonthShortName(lastObj.StartDate.Value.Month, true);

                lastObj.WeekNo = 53;

                k = 1;
                list.Add(lastObj);

                Status status = NewHRManager.SaveHPLDutyPeriod(list);
                if (status.IsSuccess)
                {
                    NewMessage.ShowNormalMessage("Duty periods generated successfully.");
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }

                gridSchedule.Store[0].DataSource = NewHRManager.GetHPLDutyPeriods(year);
                gridSchedule.Store[0].DataBind();

            }

        }


    }
}