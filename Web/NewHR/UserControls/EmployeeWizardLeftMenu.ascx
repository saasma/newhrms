﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployeeWizardLeftMenu.ascx.cs"
    Inherits="Web.NewHR.UserControls.EmployeeWizardLeftMenu" %>
<ul class="nav nav-pills nav-stacked nav-msg leftNav" id="rootwizard" runat="server">
    <li id="Li7" runat="server"><a id="A5" runat="server" href="~/newhr/PersonalDetail.aspx">
        <i class="glyphicon glyphicon-star"></i>Main_Info </a></li>
    <li id="Li2" runat="server"><a id="A6" runat="server" href="~/newhr/address.aspx"><i
        class="glyphicon glyphicon-star"></i>Address</a></li>
    <li id="Li3" runat="server"><a id="A7" runat="server" href="~/newhr/Health.aspx"><i
        class="glyphicon glyphicon-star"></i>Health</a></li>
    <li id="Li4" runat="server"><a id="A8" runat="server" href="~/newhr/DocumentListing.aspx">
        <i class="glyphicon glyphicon-star"></i>Documents</a></li>
    <li id="Li5" runat="server"><a id="A9" runat="server" href="~/newhr/Family.aspx"><i
        class="glyphicon glyphicon-star"></i>Family</a></li>
    <li id="Li6" runat="server"><a id="A10" runat="server" href="~/newhr/Identification.aspx">
        <i class="glyphicon glyphicon-star"></i>Identification</a></li>
    <li id="Li8" runat="server"><a id="A2" runat="server" href="~/newhr/PreviousEmployment.aspx">
        <i class="glyphicon glyphicon-star"></i>Work_Exp</a></li>
    <li id="Li9" runat="server"><a id="A3" runat="server" href="~/newhr/EducationTraining.aspx">
        <i class="glyphicon glyphicon-star"></i>Education</a></li>
    <li id="Li10" runat="server"><a id="A1" runat="server" href="~/newhr/EmployeeLeave.aspx">
        <i class="glyphicon glyphicon-star"></i>Leave</a></li>
    <li id="Li1" runat="server"><a id="A11" runat="server" href="~/newhr/EmployeePayroll.aspx">
        <i class="glyphicon glyphicon-star"></i>Payroll</a></li>
    <li id="LiReward" title="Reward or Addition Income" runat="server"><a id="A4" runat="server"
        href="~/newhr/EmployeeGradeReward.aspx"><i class="glyphicon glyphicon-star"></i>Addition</a></li>
    <li id="liAdditionalHR" runat="server"><a id="A12" runat="server" href="~/newhr/OtherHRDetails.aspx">
        <i class="glyphicon glyphicon-star"></i>Other HR</a></li>
</ul>
