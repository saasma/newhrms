﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;
using BLL.Base;
using Ext.Net;


namespace Web.NewHR.UserControls
{
    public partial class NomineeCtrl : BaseUserControl
    {
        public bool ShowEmpty = false;

        private bool _isDisplayMode = false;
        public bool IsDisplayMode
        {
            set
            {
                if (value == true)
                {
                    _isDisplayMode = value;
                    btnAddNewLine.Visible = false;
                    WNominee.Visible = false;
                    CommandColumn21.Visible = false;
                    CommandColumn2.Visible = false;
                    gridNominee.Width = 920;
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        private void HideButtonBlock()
        {
            buttonBlock.Visible = false;
        }

        private void Initialise()
        {
            ClearFields();
            BindRelatoinCombo();
            BindGrid();
        }

        private void ClearFields()
        {
            txtNomineeName.Text = "";
            cmbRelation.ClearValue();
            txtEffectiveDate.Text = "";
            txtEffectiveDate.ReadOnly = false;
            txtRemarks.Text = "";
            btnSave.Text = Resources.Messages.Save;
        }

        private void BindRelatoinCombo()
        {
            cmbRelation.Store[0].DataSource = ListManager.GetFamilyRelations();
            cmbRelation.Store[0].DataBind();
        }

        private void BindGrid()
        {
            int EmployeeID = GetEmployeeID();
            List<HrNominee> list = NewHRManager.GetHrNomineeByEmployeeId(EmployeeID);
            if (list.Count > 0)
            {
                gridNominee.Store[0].DataSource = list;
                gridNominee.Store[0].DataBind();
                gridNominee.Show();
            }
            else
                gridNominee.Hide();
        }

        protected void btnAddNewLine_Click(object sender, DirectEventArgs e)
        {
            ClearFields();
            hdnNomineeId.Text = "";    
            WNominee.Show();
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveUpdateNominee");
            if (Page.IsValid)
            {
                HrNominee obj = new HrNominee();
                if (!string.IsNullOrEmpty(hdnNomineeId.Text))
                    obj.NomineeId = int.Parse(hdnNomineeId.Text);

                obj.EmployeeId = GetEmployeeID();
                obj.NomineeName = txtNomineeName.Text.Trim();
                obj.RelationId = int.Parse(cmbRelation.SelectedItem.Value);
                obj.EffectiveDate = txtEffectiveDate.Text.Trim();
                obj.EffectiveDateEng = BLL.BaseBiz.GetEngDate(obj.EffectiveDate, IsEnglish);

                if (!string.IsNullOrEmpty(txtRemarks.Text.Trim()))
                    obj.Remarks = txtRemarks.Text.Trim();

                Status status = NewHRManager.SaveUpdateHrNominee(obj);
                if (status.IsSuccess)
                {
                    WNominee.Close();

                    if (string.IsNullOrEmpty(hdnNomineeId.Text))
                        NewMessage.ShowNormalMessage("Record saved successfully.");
                    else
                        NewMessage.ShowNormalMessage("Record updated successfully.");

                    BindGrid();
                }
                else
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
            }

        }

        protected int GetEmployeeID()
        {
            int EmpID = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                EmpID = int.Parse(Request.QueryString["ID"]);
            }
            else
            {
                EmpID = SessionManager.CurrentLoggedInEmployeeId;
            }
            return EmpID;
        }


        protected void gridNominee_Command(object sender, DirectEventArgs e)
        {
            string commandName = e.ExtraParams["command"];
            int nomineeId = int.Parse(e.ExtraParams["ID"]);

            switch (commandName)
            {
                case "Delete":
                    this.DeleteHrNominee(nomineeId);
                    break;
                case "Edit":
                    this.EditHrNominee(nomineeId);
                    break;
            }

        }

        private void DeleteHrNominee(int nomineeId)
        {
            Status status = NewHRManager.DeleteHrNominee(nomineeId);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Record deleted successfully.");
                BindGrid();
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        private void EditHrNominee(int nomineeId)
        {
            HrNominee obj = NewHRManager.GetHrNomineeByNomineeId(nomineeId);
            if (obj != null)
            {
                txtNomineeName.Text = obj.NomineeName;

                if (obj.RelationId != null)
                    cmbRelation.SetValue(obj.RelationId.ToString());
                else
                    cmbRelation.ClearValue();

                txtEffectiveDate.Text = obj.EffectiveDate;
                txtEffectiveDate.ReadOnly = true;
                if (obj.Remarks != null)
                    txtRemarks.Text = obj.Remarks;

                hdnNomineeId.Text = obj.NomineeId.ToString();
                btnSave.Text = Resources.Messages.Update;
                WNominee.Show();
            }

        }

    }
}