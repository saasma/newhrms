﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShiftPlanningCtrl.ascx.cs"
    Inherits="Web.NewHR.UserControls.ShiftPlanningCtrl" %>
<%@ Register Src="~/Controls/ShiftLegendCtl.ascx" TagName="AttendanceLegendCtl" TagPrefix="uc1" %>
<%@ Register Assembly="DayPilot" Namespace="DayPilot.Web.Ui" TagPrefix="DayPilot" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<style type="text/css">
    /*hide DEMO of DayPilot*/
    .scheduler_default_corner
    {
        visibility: hidden;
    }
    .itemList
    {
        margin: 0px;
    }
    .itemList li
    {
        float: left;
        list-style-type: none;
    }
    .pnlPaging
    {
        background-color: whitesmoke;
    }
</style>
<script type="text/javascript">


     /* Fast filter helpers */

     function clear() {
         var filterBox = document.getElementById("TextBoxFilter");
         filterBox.value = '';
         filter();
     }
     function filter() {
         var filterBox = document.getElementById("TextBoxFilter");
         var filterText = filterBox.value;

         dps1.clientState = { "filter": filterText };
         dps1.commandCallBack("filter");
     }

     function key(e) {
         var keynum = (window.event) ? event.keyCode : e.keyCode;

         if (keynum === 13) {
             filter();
             return false;
         }

         return true;
     }

     function afterRenderMessage(data) {
         if (data) {
             Ext.net.Notification.show({
                 iconCls: 'icon-information',
                 pinEvent: 'click',
                 html: data,
                 title: 'Updated'
             });
         }
     }

     /* Event editing helpers - modal dialog */
     function dialog() {
        
         <%= windowShift.ClientID %>.show();
         //            var modal = new DayPilot.Modal();
         //            modal.top = 60;
         //            modal.width = 300;
         //            modal.opacity = 70;
         //            modal.border = "10px solid #d0d0d0";
         //            modal.closed = function () {
         //                if (this.result == "OK") {
         //                    dps1.commandCallBack('refresh');
         //                }
         //                dps1.clearSelection();
         //            };

         //            modal.height = 250;
         //            modal.zIndex = 100;
         //            return modal;
     }

     function timeRangeSelected(start, end, resourceEmployeeId) {
         <%= cmbEmployee.ClientID %>.setValue(resourceEmployeeId);
       
            <%= txtStartDate.ClientID %>.setValue(new Date(start.d));
      

         
             var d = new Date(end.d); // today!
             d.setDate(d.getDate() - 1);        

             <%= txtEndDate.ClientID %>.setValue(d);
         

         <%= txtEndDate.ClientID %>.enable(true);
         <%= cmbShift.ClientID %>.clearValue();
          <%= cmbEmployee.ClientID %>.enable(true);
          <%= chkHasToDate.ClientID %>.setValue(false);
         <%= hdnWorkshitHistoryId.ClientID %>.setValue("");
         <%= windowShift.ClientID %>.setTitle('Add Shift');
         <%= windowShift.ClientID %>.center();
         <%= windowShift.ClientID %>.show();
         //var modal = dialog();

     }

     function eventClick(e) {
         var workShiftHistoryId = e.data.id;
         <%= hdnWorkshitHistoryId.ClientID %>.setValue(workShiftHistoryId);
         <%= btnEditShift.ClientID %>.fireEvent('click');
         //var modal = dialog();

     }

     function refreshEvents() {
         dps1.commandCallBack('refresh');
     }
</script>
<script type="text/javascript">
        var txtStartDate = null;
        var txtEndDate = null;
        var windowShift = null;
        var gridEmployeeList = null;
        var cmbEmployee = null;
        var cmbShift = null;

        var setStartDate = function (picker, date) {

            this.getCalendar().setStartDate(date);
            //CompanyX.hdFromDateEng.value = date.toDateString();

            //reloadGrids();
        }

        var rangeSelect = function (cal, dates, callback) {

            if (gridEmployeeList.getSelectionModel().selected.items.length <= 0) {
                alert("Please select the employee on left side first.");
                return;
            }

            var employeeId = gridEmployeeList.getSelectionModel().selected.items[0].data.EmployeeId;
            var name = gridEmployeeList.getSelectionModel().selected.items[0].data.Name;

            var store = cmbEmployee.getStore();
            var myArray = new Employee();
            myArray.data.EmployeeId = employeeId;
            myArray.data.Name = name;
            store.removeAll();
            store.add(myArray);

            cmbEmployee.setValue(employeeId);

            txtStartDate.setValue(dates.StartDate);
            txtEndDate.setValue(dates.EndDate);
            cmbShift.clearValue();
            windowShift.center();
            windowShift.show();
            //this.record.show(cal, dates);
            //this.getWindow().on('hide', callback, cal, { single: true });
        }

        var showWindow = function (employeeId) {

        }
        var setCurrentDate = function (type) {
            var date = new Date(dps1.startDate);
            if (type == "next") {
                date.setMonth(date.getMonth() + 1);
            }
            else if (type == "previous") {
                date.setMonth(date.getMonth() - 1);
            }
            else if (type == "this") {
                var date = new Date();
                date = new Date(date.getFullYear(), date.getMonth(), 1);
            }
             
            document.getElementById('<%= hdnStartDate.ClientID %>').value = (date.getMonth() + 1) + "/1/" + date.getFullYear();
        }

        function ACE_item_selected(source, eventArgs) {
            var value = eventArgs.get_value();
            selEmpId = value;
            <%= hdnEmployeeId.ClientID %>.setValue(selEmpId);
        }
        function clearEmp()
        {
            <%= hdnEmployeeId.ClientID %>.setValue('');
        }

</script>
<div class="contentpanel" style="padding-left: 0px; padding-top: 0px;">
    <%-- <div class="separator bottom">
    </div>--%>
    <ext:Hidden ID="hdnWorkshitHistoryId" runat="server" Text="" />
    <asp:HiddenField runat="server" ID="hdnStartDate" />
    <ext:Hidden ID="hdnEmployeeId" runat="server" />
    <ext:LinkButton ID="btnEditShift" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnEditShift_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <h4>
        Shift Planning
    </h4>
    <div class="innerLR">
        <div style="padding-bottom: 15px" class="attribute1">
            <table style="position: relative" class="fieldTable">
                <tr>
                    <td id="branchRow" runat="server">
                        <strong>Branch</strong>
                    </td>
                    <td id="deptRow" runat="server">
                        <strong>Department</strong>
                    </td>
                    <td id="empRow" runat="server">
                        <strong>Employee</strong>
                    </td>
                    <td style="padding-left: 20px">
                        <strong>From Date</strong>
                    </td>
                    <td style="padding-left: 20px">
                        <strong>To Date</strong>
                    </td>
                    <td rowspan="2" style="padding-left: 20px;">
                        <div style="float: left">
                            <asp:Button ID="btnLoad" Style="width: 80px; height: 30px; margin-top: 13px; margin-left: 12px;"
                                runat="server" OnClick="btnLoad_Click" Text="Load" />
                        </div>
                        <div style="float: right">
                            <ext:Button runat="server" OnClientClick="EmpAssignShift();return false;" Cls="btn btn-primary btn-sect"
                                StyleSpec=" margin-top: 13px; margin-left: 12px;" ID="btnChangeShift" Text="<i></i>Import">
                            </ext:Button>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td id="brRow2" runat="server">
                        <asp:DropDownList AppendDataBoundItems="true" ID="ddlBranch" runat="server" Width="180px"
                            DataTextField="Name" DataValueField="BranchId">
                            <asp:ListItem Text="--Select Branch" Value="-1" />
                        </asp:DropDownList>
                    </td>
                    <td id="deptRow2" runat="server">
                        <asp:DropDownList AppendDataBoundItems="true" ID="ddlDepartment" runat="server" Width="180px"
                            DataTextField="Name" DataValueField="DepartmentId">
                            <asp:ListItem Text="--Select Department" Value="-1" />
                        </asp:DropDownList>
                    </td>
                    <td id="empRow2" runat="server">
                        <asp:TextBox ID="txtEmpSearch" onchange="if(this.value=='') clearEmp();" runat="server"
                            placeholder="Search Employee" />
                        <cc1:AutoCompleteExtender EnableCaching="true" ID="AutoCompleteExtenderOrganization"
                            runat="server" MinimumPrefixLength="2" ServiceMethod="GetEmployeeNamesWithID"
                            ServicePath="~/PayrollService.asmx" TargetControlID="txtEmpSearch" OnClientItemSelected="ACE_item_selected"
                            CompletionSetCount="10" CompletionInterval="250" CompletionListCssClass="AutoExtender"
                            CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
                        </cc1:AutoCompleteExtender>
                    </td>
                    <td>
                        <ext:DateField LabelSeparator="" LabelAlign="Left" LabelWidth="10" runat="server"
                            ID="dfFromDate" FieldLabel="&nbsp;" Width="180">
                        </ext:DateField>
                    </td>
                    <td style="padding-left: 10px">
                        <ext:DateField LabelSeparator="" LabelAlign="Left" LabelWidth="10" runat="server"
                            ID="dfToDate" FieldLabel="&nbsp;" Width="180">
                            <Listeners>
                                <Render Handler="this.hide();this.show();" />
                            </Listeners>
                        </ext:DateField>
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
        </div>
        <div id="toolbar" style="">
            <a style="font-size: 18px" title="Previous" href="javascript:setCurrentDate('previous');dps1.commandCallBack('previous');">
                &#x25c4;</a> <a style="font-size: 18px" href="javascript:setCurrentDate('next');dps1.commandCallBack('next');">
                    &#x25ba;</a> <a title="Next" href="javascript:setCurrentDate('this');dps1.commandCallBack('this');">
                        This Month</a>
            <%-- <div class="right">
                Fast filter:
                <input type="text" id="TextBoxFilter" onkeypress="return key(event);" />&nbsp;<a
                    href="javascript:filter();" style="font-weight: bold">Apply</a>&nbsp;<a href="javascript:clear();">Clear</a>
            </div>--%>
        </div>
        <DayPilot:DayPilotScheduler ID="DayPilotScheduler1" runat="server" DataTagFields="EmployeeId,Name,Branch"
            Width="100%" RowHeaderWidth="120" CellDuration="1440" CellGroupBy="Month" CellWidth="70"
            TimeRangeSelectedHandling="JavaScript" TimeRangeSelectedJavaScript="timeRangeSelected(start, end, resource)"
            ClientObjectName="dps1" EventResizeHandling="Disabled" OnTimeRangeSelected="DayPilotScheduler1_TimeRangeSelected"
            EventClickHandling="JavaScript" EventClickJavaScript="eventClick(e);" EventEditHandling="CallBack"
            OnEventMenuClick="DayPilotScheduler1_EventMenuClick" xContextMenuID="DayPilotMenu2"
            ContextMenuResourceID="DayPilotMenuRes" BusinessBeginsHour="5" BusinessEndsHour="24"
            OnBeforeResHeaderRender="DayPilotScheduler1_BeforeResHeaderRender" EnableViewState="false"
            ScrollLabelsVisible="false" BubbleID="DayPilotBubble1" ShowToolTip="false" HeightSpec="Max"
            Height="600" OnEventClick="DayPilotScheduler1_EventClick" OnTimeRangeMenuClick="DayPilotScheduler1_TimeRangeMenuClick"
            OnBeforeCellRender="DayPilotScheduler1_BeforeCellRender" EventDoubleClickHandling="Disabled"
            ResourceBubbleID="DayPilotBubble1" OnResourceHeaderMenuClick="DayPilotScheduler1_ResourceHeaderMenuClick"
            OnBeforeTimeHeaderRender="DayPilotScheduler1_BeforeTimeHeaderRender" ContextMenuSelectionID="DayPilotMenuSelection"
            SyncResourceTree="true" DragOutAllowed="true" TimeRangeDoubleClickHandling="JavaScript"
            TimeRangeDoubleClickJavaScript="" DataStartField="FromDateEng" DataEndField="ToDateEng"
            DataTextField="WorkShift" DataValueField="WorkShiftHistoryId" DataResourceField="EmployeeId"
            AfterRenderJavaScript="afterRenderMessage(data);" OnCommand="DayPilotScheduler1_Command">
            <HeaderColumns>
                <DayPilot:RowHeaderColumn Title="Name" Width="100" />
                <DayPilot:RowHeaderColumn Title="Branch" Width="80" />
            </HeaderColumns>
            <Resources>
                <%--<DayPilot:Resource Name="Room A" Value="A" Expanded="True">
                    <Children>
                        <DayPilot:Resource Name="Room A.1" Value="A.1" Expanded="False">
                            <Children>
                                <DayPilot:Resource Name="Room A.1.1" Value="A.1.1" Expanded="False" />
                                <DayPilot:Resource Name="Room A.1.2" Value="A.1.2" Expanded="False" />
                            </Children>
                        </DayPilot:Resource>
                        <DayPilot:Resource Name="Room A.2" Value="A.2" Expanded="False" />
                    </Children>
                </DayPilot:Resource>
                <DayPilot:Resource Name="Room B" Value="B" Expanded="False" />
                <DayPilot:Resource Name="Room C" Value="C" ToolTip="Test" Expanded="False" />
                <DayPilot:Resource Name="Room D" Value="D" Expanded="False" />
                <DayPilot:Resource Name="Room E" Value="E" Expanded="False" />
                <DayPilot:Resource Name="Room F" Value="F" Expanded="False" />
                <DayPilot:Resource Name="Room G" Value="G" Expanded="False" />
                <DayPilot:Resource Name="Room H" Value="H" Expanded="False" />
                <DayPilot:Resource Name="Room I" Value="I" Expanded="False" />
                <DayPilot:Resource Name="Room J" Value="J" Expanded="False" />
                <DayPilot:Resource Name="Room K" Value="K" Expanded="False" />
                <DayPilot:Resource Name="Room L" Value="L" Expanded="False" />
                <DayPilot:Resource Name="Room M" Value="M" Expanded="False" />
                <DayPilot:Resource Name="Room N" Value="N" Expanded="False" />
                <DayPilot:Resource Name="Room O" Value="O" Expanded="False" />
                <DayPilot:Resource Name="Room P" Value="P" Expanded="False" />
                <DayPilot:Resource Name="Room Q" Value="Q" Expanded="False" />
                <DayPilot:Resource Name="Room R" Value="R" Expanded="False" />
                <DayPilot:Resource Name="Room S" Value="S" Expanded="False" />
                <DayPilot:Resource Name="Room T" Value="T" Expanded="False" />
                <DayPilot:Resource Name="Room U" Value="U" Expanded="False" />
                <DayPilot:Resource Name="Room V" Value="V" Expanded="False" />
                <DayPilot:Resource Name="Room W" Value="W" Expanded="False" />
                <DayPilot:Resource Name="Room X" Value="X" Expanded="False" />
                <DayPilot:Resource Name="Room Y" Value="Y" Expanded="False" />
                <DayPilot:Resource Name="Room Z" Value="Z" Expanded="False" />--%>
            </Resources>
        </DayPilot:DayPilotScheduler>
        <%--<DayPilot:DayPilotMenu ID="DayPilotMenu2" runat="server" UseShadow="false">
            <DayPilot:MenuItem Text="Open" Action="JavaScript" JavaScript="alert('Opening event (id ' + e.value() + ')');">
            </DayPilot:MenuItem>
            <DayPilot:MenuItem Text="Send" Action="JavaScript" JavaScript="alert('Sending event (id ' + e.value() + ')');">
            </DayPilot:MenuItem>
            <DayPilot:MenuItem Text="Refresh" Action="JavaScript" JavaScript="dps1.commandCallBack('refresh');">
            </DayPilot:MenuItem>
            <DayPilot:MenuItem Text="-" Action="NavigateUrl">
            </DayPilot:MenuItem>
            <DayPilot:MenuItem Text="Delete (CallBack)" Action="Callback" Command="Delete">
            </DayPilot:MenuItem>
            <DayPilot:MenuItem Action="PostBack" Command="Delete" Text="Delete (PostBack)" />
            <DayPilot:MenuItem Action="JavaScript" JavaScript="alert('x:' + dps1.eventOffset.x + ' y:' + dps1.eventOffset.y + ' resource:' + e.row());"
                Text="Mouse offset (relative to event)" />
            <DayPilot:MenuItem Action="NavigateUrl" NavigateUrl="javascript:alert('Going somewhere else (id {0})');"
                Text="NavigateUrl test" />
        </DayPilot:DayPilotMenu>--%>
        <%--<DayPilot:DayPilotMenu ID="DayPilotMenuSpecial" runat="server" ClientObjectName="cmSpecial"
            UseShadow="false">
            <DayPilot:MenuItem Text="Open" Action="JavaScript" JavaScript="alert('Opening event (id ' + e.value() + ')');">
            </DayPilot:MenuItem>
            <DayPilot:MenuItem Text="Send" Action="JavaScript" JavaScript="alert('Sending event (id ' + e.value() + ')');">
            </DayPilot:MenuItem>
            <DayPilot:MenuItem Text="Delete (JavaScript using callback)" Action="JavaScript"
                Command='Delete' JavaScript="if (confirm('Do you really want to delete event ' + e.text() + ' ?')) dps1.eventMenuClickCallBack(e, command);">
            </DayPilot:MenuItem>
        </DayPilot:DayPilotMenu>--%>
        <DayPilot:DayPilotMenu ID="DayPilotMenuSelection" runat="server">
            <%-- <DayPilot:MenuItem Action="JavaScript" JavaScript="dps1.timeRangeSelectedCallBack(e.start, e.end, e.resource);"
                Text="Create new event (JavaScript)" />
            <DayPilot:MenuItem Action="PostBack" Command="Insert" Text="Create new event (PostBack)" />
            <DayPilot:MenuItem Action="CallBack" Command="Insert" Text="Create new event (CallBack)" />
            <DayPilot:MenuItem Text="-" Action="JavaScript">
            </DayPilot:MenuItem>
            <DayPilot:MenuItem Action="JavaScript" JavaScript="alert('Start: ' + e.start + '\nEnd: ' + e.end + '\nResource id: ' + e.resource);"
                Text="Show selection details" />--%>
            <DayPilot:MenuItem Action="JavaScript" JavaScript="dps1.clearSelection();" Text="Clean selection" />
        </DayPilot:DayPilotMenu>
        <%-- <DayPilot:DayPilotMenu ID="DayPilotMenuRes" runat="server">
            <DayPilot:MenuItem Action="CallBack" Command="Insert" Text="Add child" />
            <DayPilot:MenuItem Text="-" Action="JavaScript">
            </DayPilot:MenuItem>
            <DayPilot:MenuItem Action="CallBack" Command="Delete" Text="Delete" />
            <DayPilot:MenuItem Action="CallBack" Command="DeleteChildren" Text="Delete children" />
            <DayPilot:MenuItem Text="-" Action="JavaScript">
            </DayPilot:MenuItem>
            <DayPilot:MenuItem Action="JavaScript" JavaScript="alert(e.name + '\n' + e.value);"
                Text="Show resource details" />
        </DayPilot:DayPilotMenu>--%>
        <DayPilot:DayPilotBubble ID="DayPilotBubble1" runat="server" OnRenderContent="DayPilotBubble1_RenderContent"
            ClientObjectName="bubble" OnRenderEventBubble="DayPilotBubble1_RenderEventBubble"
            Width="250" Corners="Rounded" Position="EventTop">
        </DayPilot:DayPilotBubble>
        <div class="pnlPaging">
            <uc1:PagingCtl ID="pagingCtl1" ShowDropDown="true" OnNextRecord="btnNext_Click" OnChangePage="ChangePageNumber"
                OnPrevRecord="btnPrevious_Click" OnDropDownShowRecordsChanged="ddlRecords_SelectedIndexChanged"
                runat="server" />
        </div>
        <div style='width: 100%;' class="colRight">
            <uc1:AttendanceLegendCtl ID="AttendanceLegendCtl1" runat="server" />
        </div>
    </div>
</div>
<ext:Window ID="windowShift" runat="server" Title="Add Shift" Hidden="false" X="-1000"
    Y="-1000" Icon="Application" Height="350" Width="500" BodyPadding="5" Modal="false">
    <Content>
        <table class="fieldTable" style="margin-left: 20px; margin-top: 20px;">
            <tr>
                <td>
                    <ext:ComboBox LabelSeparator="" TypeAhead="true" ForceSelection="true" QueryMode="Local"
                        FieldLabel="Employee" ValueField="EmployeeId" DisplayField="Name" runat="server"
                        ID="cmbEmployee" LabelWidth="75" Width="315">
                        <Store>
                            <ext:Store ID="storeLeaveType" runat="server">
                                <Model>
                                    <ext:Model ID="Model1" runat="server" Name="Employee" IDProperty="EmployeeId">
                                        <Fields>
                                            <ext:ModelField Name="EmployeeId" Type="String" />
                                            <ext:ModelField Name="Name" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                </td>
            </tr>
            <tr>
                <td>
                    <ext:ComboBox LabelSeparator="" TypeAhead="true" ForceSelection="true" QueryMode="Local"
                        FieldLabel="Shift" ValueField="WorkShiftId" DisplayField="Name" runat="server"
                        ID="cmbShift" LabelWidth="75" Width="315">
                        <Store>
                            <ext:Store ID="store1" runat="server">
                                <Model>
                                    <ext:Model ID="Model2" runat="server" IDProperty="WorkShiftId">
                                        <Fields>
                                            <ext:ModelField Name="WorkShiftId" Type="String" />
                                            <ext:ModelField Name="Name" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                    <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator6" runat="server"
                        ValidationGroup="SaveUpdate" ControlToValidate="cmbShift" ErrorMessage="Shift is required." />
                </td>
            </tr>
            <tr>
                <td style="padding-left: 246px">
                    <ext:Checkbox ID="chkHasToDate" runat="server" BoxLabel="No To Date">
                        <Listeners>
                            <Change Handler="if(this.checked==true) #{txtEndDate}.disable(true); else #{txtEndDate}.enable(true);" />
                        </Listeners>
                    </ext:Checkbox>
                </td>
            </tr>
            <tr>
                <td>
                    <ul class="itemList" style="margin-left: -40px;">
                        <li>
                            <ext:DateField LabelSeparator="" StyleSpec="200" FieldLabel="When" runat="server"
                                ID="txtStartDate" LabelWidth="75" Width="180">
                            </ext:DateField>
                            <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator1" runat="server"
                                ValidationGroup="SaveUpdate" ControlToValidate="txtStartDate" ErrorMessage="From date is required." />
                        </li>
                        <li>
                            <ext:DateField FieldLabel="to" StyleSpec="margin-left:10px" LabelWidth="15" LabelSeparator=""
                                runat="server" ID="txtEndDate" Width="125">
                            </ext:DateField>
                        </li>
                    </ul>
                </td>
            </tr>
            <tr>
                <td valign="bottom" colspan="2">
                    <div class="popupButtonDiv">
                        <ext:Button runat="server" ID="Button1" Cls="btn btn-primary" Text="<i></i>Save">
                            <DirectEvents>
                                <Click OnEvent="btnSave_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                            <Listeners>
                                <Click Handler="valGroup = 'SaveUpdate'; return CheckValidation();">
                                </Click>
                            </Listeners>
                        </ext:Button>
                        <div class="btnFlatOr">
                            or</div>
                        <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="LinkButton2" Cls="btnFlatLeftGap"
                            Text="<i></i>Cancel">
                            <Listeners>
                                <Click Handler="#{windowShift}.hide();">
                                </Click>
                            </Listeners>
                        </ext:LinkButton>

                         <ext:LinkButton runat="server" ID="btnDelete"  Text="<i></i>Delete" StyleSpec="margin-left:20px;margin-top:5px" >
                            <DirectEvents>
                                <Click OnEvent="btnDelete_Click">
                                <Confirmation ConfirmRequest="true" Message="Are you sure,you want to delete?" />
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                           
                        </ext:LinkButton>

                    </div>
                </td>
            </tr>
        </table>
    </Content>
</ext:Window>
<script type="text/javascript">

    //    function reloadCalendar() {
    //        ctl00_ContentPlaceHolder_Main_EventStore1.reload();
    //    }
    //    Ext.onReady(
    //            function () {

    //                document.getElementsByClassName('scheduler_default_columnheader')[0].nextSibling.style.display = "none";
    //                document.getElementsByClassName('scheduler_default_corner')[0].style.visibility = "inherit";
    //            }
    //        );
</script>
