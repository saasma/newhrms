﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FamilyCtl.ascx.cs" Inherits="Web.NewHR.UserControls.FamilyCtl" %>
<script type="text/javascript">

      var CommandHandler = function(command, record){
            <%= hiddenValue.ClientID %>.setValue(record.data.FamilyId);
                if(command=="Edit")
                {
                    <%= btnEditLevel.ClientID %>.fireEvent('click');
                }
                else
                {
                    <%= btnDeleteLevel.ClientID %>.fireEvent('click');
                }

             }
        var dependentRenderer = function(value)
        {
            if(value=="true")
                return "Yes";
            return "No";
        }

    

        var prepare = function (grid, toolbar, rowIndex, record) {

            var EditBtn = toolbar.items.get(0);
            var deleteBtn = toolbar.items.get(2);

            if (record.data.IsEditable == 0) {
                EditBtn.setVisible(false);
                deleteBtn.setVisible(false);
            }

        }
    
</script>
<ext:Hidden runat="server" ID="hiddenValue" />
<ext:LinkButton runat="server" Hidden="true" ID="btnEditLevel" runat="server">
    <DirectEvents>
        <Click OnEvent="btnEditLevel_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:LinkButton>
<ext:Button ID="btnDeleteLevel" runat="server" Hidden="true">
    <DirectEvents>
        <Click OnEvent="btnDeleteLevel_Click">
            <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the family member?" />
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:Button>
<ext:GridPanel StyleSpec="margin-top:15px;" ID="GridLevels" runat="server" Cls="itemgrid">
    <Store>
        <ext:Store ID="Store3" runat="server">
            <Model>
                <ext:Model ID="Model4" runat="server" IDProperty="FamilyId">
                    <Fields>
                        <ext:ModelField Name="FamilyId" Type="String" />
                        <ext:ModelField Name="Name" Type="string" />
                        <ext:ModelField Name="Relation" Type="string" />
                        <ext:ModelField Name="DateOfBirth" Type="string" />
                        <ext:ModelField Name="HasDependent" Type="string" />
                        <ext:ModelField Name="Remarks" Type="string" />
                        <ext:ModelField Name="SpecifiedDate" Type="string" />
                        <ext:ModelField Name="AgeOnSpecifiedSPDate" Type="string" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
    </Store>
    <ColumnModel>
        <Columns>
            <ext:Column ID="Column8" Sortable="false" MenuDisabled="true" runat="server" Text="Relation"
                Align="Left" Width="150" DataIndex="Relation" />
            <ext:Column ID="Column5" Sortable="false" MenuDisabled="true" runat="server" Text="Name"
                Align="Left" Width="300" DataIndex="Name" />
            <ext:Column ID="Column6" Sortable="false" MenuDisabled="true" runat="server" Text="DOB"
                Align="Center" DataIndex="DateOfBirth">
            </ext:Column>
            <ext:Column ID="Column1" Sortable="false" MenuDisabled="true" runat="server" Text="Dependent"
                Align="Center" DataIndex="HasDependent">
                <Renderer Fn="dependentRenderer" />
            </ext:Column>
            <ext:Column ID="Column7" Sortable="false" MenuDisabled="true" runat="server" Text="Remarks"
                Align="Left" Width="300" DataIndex="Remarks">
            </ext:Column>
            <ext:CommandColumn ID="CommandColumn1" runat="server" Width="95" Text="" Align="Center">
                <Commands>
                    <ext:CommandSeparator />
                    <ext:GridCommand Text="<i></i>" Cls="btn-action glyphicons pencil btn-success" CommandName="Edit" />
                    <ext:GridCommand Text="<i></i>" Cls="btn-action glyphicons remove_2 btn-danger" CommandName="Delete" />
                </Commands>
                <Listeners>
                    <Command Handler="CommandHandler(command,record);" />
                </Listeners>
                <PrepareToolbar Fn="prepare" />
            </ext:CommandColumn>
        </Columns>
    </ColumnModel>
    <SelectionModel>
        <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
    </SelectionModel>
</ext:GridPanel>
<div class="buttonBlockSection">
    <ext:LinkButton runat="server" ID="btnAddLevel" Cls="btn btn-primary btn-sect"
        Text="<i></i>Add Family Member" runat="server">
        <DirectEvents>
            <Click OnEvent="btnAddLevel_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
</div>
<ext:Window ID="WindowLevel" runat="server" Title="Add/Edit Family" Icon="Application"
    Width="500" Height="450" BodyPadding="5" Hidden="true" Modal="true">
    <Content>
        <table class="fieldTable">
            <tr>
                <td>
                    <ext:TextField ID="txtName" LabelSeparator="" runat="server" FieldLabel="Name *"
                        Width="200" LabelAlign="Top">
                    </ext:TextField>
                    <asp:RequiredFieldValidator Display="None" ID="valcmbFaculty" runat="server" ValidationGroup="SaveUpdateLevel"
                        ControlToValidate="txtName" ErrorMessage="Name is required." />
                </td>
                <td>
                    <%--<ext:TextField ID="txtRelation" LabelSeparator="" runat="server" FieldLabel="Relation *"
                        Width="200" LabelAlign="Top">
                    </ext:TextField>--%>
                    <ext:ComboBox ID="cmbRelation" runat="server" ValueField="ID" DisplayField="Name" FieldLabel="Relation *"
                        LabelAlign="top" LabelSeparator="" ForceSelection="true" QueryMode="Local">
                        <Store>
                            <ext:Store ID="Store1" runat="server">
                                <Model>
                                    <ext:Model ID="Model3" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="ID" Type="String" />
                                            <ext:ModelField Name="Name" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                    <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator1" runat="server"
                        ValidationGroup="SaveUpdateLevel" ControlToValidate="cmbRelation" ErrorMessage="Relation is required." />
                </td>
            </tr>
            <tr>
                <td>
                    <pr:CalendarExtControl Width="180px" FieldLabel="Date Of Birth" ID="txtDOB" runat="server"
                        LabelAlign="Top" LabelSeparator="">
                    </pr:CalendarExtControl>
                </td>
                <td>
                    <ext:Checkbox Width="180px" BoxLabelAlign="After" BoxLabel="Dependent" ID="chkHasDependent"
                        runat="server" LabelSeparator="" />
                </td>
            </tr>
            <tr style="display: none;">
                <td>
                    <pr:CalendarExtControl Width="180px" FieldLabel="Specified Date *" ID="txtSPDate"
                        runat="server" LabelAlign="Top" LabelSeparator="" />
                    <%--<asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator4" runat="server"
                        ValidationGroup="SaveUpdateLevel" ControlToValidate="txtSPDate" ErrorMessage="Specified Date is required." />--%>
                </td>
                <td>
                    <ext:TextField ID="txtAgeOnSPDate" LabelSeparator="" runat="server" FieldLabel="Age on Specified Date"
                        Width="200" LabelAlign="Top">
                    </ext:TextField>
                    <%-- <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator5" runat="server"
                                ValidationGroup="SaveUpdateLevel" ControlToValidate="txtAgeOnSPDate" ErrorMessage="Age on Specified Date is required." />--%>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <ext:TextArea ID="txtRemarks" Width="420" Rows="4" LabelSeparator="" runat="server"
                        FieldLabel="Note " LabelAlign="Top">
                    </ext:TextArea>
                </td>
            </tr>
            <tr>
                <td valign="bottom" colspan="2">
                    <div class="popupButtonDiv">
                        <ext:LinkButton runat="server" StyleSpec="padding:0px;
                                " ID="btnLevelSaveUpdate" Cls="btnFlat" BaseCls="btnFlat" Text="<i></i>Save"
                            runat="server">
                            <DirectEvents>
                                <Click OnEvent="btnLevelSaveUpdate_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                            <Listeners>
                                <Click Handler="valGroup = 'SaveUpdateLevel'; if(CheckValidation()) return this.disable(); else return false;">
                                </Click>
                            </Listeners>
                        </ext:LinkButton>
                        <div class="btnFlatOr">
                            or</div>
                        <ext:LinkButton runat="server" StyleSpec="padding:0px;" Cls="btnFlatLeftGap" ID="LinkButton1"
                            Text="<i></i>Cancel" runat="server">
                            <Listeners>
                                <Click Handler="#{WindowLevel}.hide();">
                                </Click>
                            </Listeners>
                        </ext:LinkButton>
                    </div>
                </td>
            </tr>
        </table>
    </Content>
</ext:Window>
