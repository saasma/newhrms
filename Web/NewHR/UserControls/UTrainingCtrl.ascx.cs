﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;

namespace Web.NewHR.UserControls
{
    public partial class UTrainingCtrl : BaseUserControl
    {
        private bool _isDisplayMode = false;
        public bool IsDisplayMode
        {
            set
            {
                if (value == true)
                {
                    _isDisplayMode = value;
                    btnAddNewLine.Visible = false;
                    AETrainingWindow.Visible = false;
                    CommandColumn2.Visible = false;
                    CommandColumn3.Visible = false;
                    GridTraining.Width = 920;
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
                HideButtonsAndGridColumn();
            }
        }

        protected void Initialise()
        {
            int EmployeeID = GetEmployeeID();
            this.LoadTrainingGrid(EmployeeID);          
        }
     
        protected void TrainingDownLoad(int ID)
        {

            //string contentType = "";
            HTraining doc = NewHRManager.GetTrainingDetailsById(ID);

            string path = Server.MapPath(@"~/uploads/" + doc.FileLocation + doc.ServerFileName);
            string contentType = doc.FileType;
            string name = doc.UserFileName;

            byte[] bytes = File.ReadAllBytes(path);

            Context.Response.Clear();
            Context.Response.ClearHeaders();
            Context.Response.AppendHeader("Content-Disposition", "attachment; filename=\"" + name + "\"");
            Context.Response.ContentType = contentType;
            Context.Response.BinaryWrite(bytes);
            Context.Response.End();
            // Response.Redirect("~/DocumentHandler.ashx?ID=" + ID);
        }

        
        protected void LoadTrainingGrid(int EmployeeID)
        {
            List<HTraining> _HTraining = NewHRManager.GetTrainingByEmployeeID(EmployeeID);

            if (SessionManager.CurrentLoggedInEmployeeId == 0)
                _HTraining = _HTraining.Where(x => x.Status == (int)HRStatusEnum.Approved).ToList();

            if (Request.QueryString["ID"] != null)
            {
                if (int.Parse(Request.QueryString["ID"].ToString()) != SessionManager.CurrentLoggedInEmployeeId)
                    _HTraining = _HTraining.Where(x => x.Status == (int)HRStatusEnum.Approved).ToList();
            }

            this.StoreTraining.DataSource = _HTraining;
            this.StoreTraining.DataBind();

            if (_isDisplayMode && _HTraining.Count <= 0)
                GridTraining.Hide();
            else
                GridTraining.Show();

        }


        protected int GetEmployeeID()
        {
            //Guid? AssetID = null;
            int EmpID = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                EmpID = int.Parse(Request.QueryString["ID"]);
            }
            else
            {
                EmpID = SessionManager.CurrentLoggedInEmployeeId;
            }
            return EmpID;
        }

        protected void btnAddNewLine_Click(object sender, DirectEventArgs e)
        {
            trainCtrl.ClearFields();

            Ext.Net.Button btnSave = (Ext.Net.Button)trainCtrl.FindControl("btnSave");
            btnSave.Disabled = false;
            AETrainingWindow.Center();
            this.AETrainingWindow.Show();
        }

    

        private object[] TrainingFillData
        {
            get
            {
                return new object[]
            {
                new object[] { "", "", "",""},
               
            };
            }
        }

        protected void GridTraining_Command(object sender, DirectEventArgs e)
        {
            string commandName = e.ExtraParams["command"];
            int TrainingID = int.Parse(e.ExtraParams["ID"]);
            HTraining _HTraining = NewHRManager.GetTrainingDetailsById(int.Parse(e.ExtraParams["ID"]));
            string Path = Server.MapPath(_HTraining.FileLocation) + _HTraining.ServerFileName;
            switch (commandName)
            {
                case "Delete":
                    this.DeleteTraining(TrainingID, Path);
                    break;
                case "DownLoad":
                    this.TrainingDownLoad(TrainingID);
                    break;
                case "Edit":
                    {
                        Ext.Net.Button btnSave = (Ext.Net.Button)trainCtrl.FindControl("btnSave");
                        btnSave.Disabled = false;

                        this.editTraining(TrainingID);
                        break;
                    }
            }

        }

        protected void DeleteTraining(int ID, string path)
        {

            bool result = NewHRManager.DeleteTrainingByID(ID);
            if (result)
            {
                if (File.Exists(path))
                    File.Delete(path);
                //refresh grid

                NewMessage.ShowNormalMessage("Record deleted successfully.");
                this.LoadTrainingGrid(this.GetEmployeeID());
            }

        }
        
        public void editTraining(int TrainingID)
        {
            AETrainingWindow.Show();
            trainCtrl.editTraining(TrainingID);            
        }

        protected void btnLoadGrid_Click(object sender, DirectEventArgs e)
        {
            int employeeId = 0;
            if (Request.QueryString["ID"] != null)
                employeeId = int.Parse(Request.QueryString["ID"].ToString());
            else
                employeeId = SessionManager.CurrentLoggedInEmployeeId;

            LoadTrainingGrid(employeeId);           
        }

        private void HideButtonsAndGridColumn()
        {
            if (Request.QueryString["ID"] != null && SessionManager.CurrentLoggedInEmployeeId != 0)
            {
                if (SessionManager.CurrentLoggedInEmployeeId != int.Parse(Request.QueryString["ID"].ToString()))
                {
                    IsDisplayMode = true;
                }
            }
        }
    }
}