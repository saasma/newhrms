﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using Utils.Helper;
namespace Web.NewHR.UserControls
{
    public partial class ActingCtrlView : BaseUserControl
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        public void Initialise()
        {
            LoadActingGrid();
        }

        private void LoadActingGrid()
        {
            int total = 0;
            DateTime? start = null, end = null;

            int employeeId = UrlHelper.GetIdFromQueryString("ID");

            List<ActingEmployee> list = NewPayrollManager.GetActingEmployeeList(
                0, int.MaxValue, ref total, "", employeeId, start, end);
            GridLevels.Store[0].DataSource = list;
            GridLevels.Store[0].DataBind();

            if (list.Count <= 0)
                GridLevels.Hide();
        }


    }
}