﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Entity;
using BLL;
using BLL.BO;

namespace Web.NewHR.UserControls
{
    public partial class StatusChangeHistoryCtrl : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                BindGrid();
            }
        }

        private void BindGrid()
        {
            int employeeId = 0;
            if (Request.QueryString["ID"] != null)
                employeeId = int.Parse(Request.QueryString["ID"].ToString());
            else
                employeeId = SessionManager.CurrentLoggedInEmployeeId;

            gridChangeStatusHist.GetStore().DataSource = EmployeeManager.GetCurrentStatusHist(employeeId);
            gridChangeStatusHist.GetStore().DataBind();
        }
    }
}