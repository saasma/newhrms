﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using BLL.Manager;
using BLL;
using DAL;
using BLL.BO;
using BLL.Base;
using Utils.Helper;

namespace Web.NewHR.UserControls
{
    public partial class ChangeEmpShiftCtrl : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialize();

                if (SessionManager.CurrentLoggedInEmployeeId == 0)
                {
                    cmbSearch.Show();
                    cmbBranch.Show();
                    cmbDepartment.Show();
                    btnChangeShift.Style.Remove("display");
                }

            }

            if (Request.Form["__EVENTTARGET"] != null && Request.Form["__EVENTTARGET"].ToLower().Contains("refresh"))
                BindGridReload();

            JavascriptHelper.AttachNonDialogPopUpCode(Page, "changeEmpShift", "../../ExcelWindow/ChangeShiftExcel.aspx", 450, 500);
        }

        protected void btnSave_Click(object s, DirectEventArgs e)
        {
            Status status = new Status();

            List<GetEmployee_ShiftsResult> empShiftLines = new List<GetEmployee_ShiftsResult>();

            string jsonEmpShift = e.ExtraParams["EmpShiftExtraParam"];

            if (string.IsNullOrEmpty(jsonEmpShift))
                return;

            empShiftLines = JSON.Deserialize<List<GetEmployee_ShiftsResult>>(jsonEmpShift);



            status = ShiftManager.UpdateShiftSchedule(empShiftLines);

            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Successfully saved.");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        #region Methods...

        private void Initialize()
        {
            //BindGrid();
            X.Js.Call("searchListEL");
            BindComboBoxes();
        }

        public void btnLoad_Click(object sender, DirectEventArgs e)
        {
            int branchId = -1;
            if (cmbBranch.SelectedItem != null && cmbBranch.SelectedItem.Value != null)
                branchId = int.Parse(cmbBranch.SelectedItem.Value);


            int depId = -1;
            if (cmbDepartment.SelectedItem != null && cmbDepartment.SelectedItem.Value != null)
                depId = int.Parse(cmbDepartment.SelectedItem.Value);


            int empId = -1;
            if (cmbSearch.SelectedItem != null && cmbSearch.SelectedItem.Value != null)
                empId = int.Parse(cmbSearch.SelectedItem.Value);

            int shiftId = -1;
            if (cmbShiftList.SelectedItem != null && cmbShiftList.SelectedItem.Value != null)
                shiftId = int.Parse(cmbShiftList.SelectedItem.Value);

            bool? type = null;

            if (cmbShiftType.SelectedItem.Index == 1)
                type = false;
            else if (cmbShiftType.SelectedItem.Index == 2)
                type = true;

            if (SessionManager.CurrentLoggedInEmployeeId == 0)
            {
                this.storeEmpShift.DataSource = ShiftManager.GetEmployee_Shifts(branchId, depId, empId, type, shiftId,-1);
                this.storeEmpShift.DataBind();
            }
            else
            {
                this.storeEmpShift.DataSource = ShiftManager.GetEmployee_ShiftsForManager(branchId, depId, SessionManager.CurrentLoggedInEmployeeId, type, shiftId);
                this.storeEmpShift.DataBind();
            }
        }

        private void BindGridReload()
        {
            cmbSearch.Clear();
            cmbBranch.Clear();
            cmbDepartment.Clear();
            cmbShiftList.Clear();
            cmbShiftType.Clear();
            X.Js.Call("searchList");
        }

        private void BindComboBoxes()
        {
            this.storeShift.DataSource = ShiftManager.GetAllShifts();
            this.storeShift.DataBind();

            StoreBranch.DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            storeSearch.DataBind();

            StoreDepartment.DataSource = DepartmentManager.GetAllDepartments();
            StoreDepartment.DataBind();

            List<TextValue> type = new List<TextValue>();
            type.Add(new TextValue { Text = "Fixed", Value = "false" });
            type.Add(new TextValue { Text = "Changable", Value = "true" });
            storeShiftType.DataSource = type;
            storeShiftType.DataBind();
        }

        private void BindGrid()
        {
            if (SessionManager.CurrentLoggedInEmployeeId == 0)
            {
                this.storeEmpShift.DataSource = ShiftManager.GetEmployee_Shifts(-1, -1, -1, null, -1,-1);
                this.storeEmpShift.DataBind();
            }
            else
            {
                this.storeEmpShift.DataSource = ShiftManager.GetEmployee_ShiftsForManager(-1, -1, SessionManager.CurrentLoggedInEmployeeId, null, -1);
                this.storeEmpShift.DataBind();
            }
            
        }
        #endregion


        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            int branchId = -1;
            if (cmbBranch.SelectedItem != null && cmbBranch.SelectedItem.Value != null)
                branchId = int.Parse(cmbBranch.SelectedItem.Value);


            int depId = -1;
            if (cmbDepartment.SelectedItem != null && cmbDepartment.SelectedItem.Value != null)
                depId = int.Parse(cmbDepartment.SelectedItem.Value);


            int empId = -1;
            if (cmbSearch.SelectedItem != null && cmbSearch.SelectedItem.Value != null)
                empId = int.Parse(cmbSearch.SelectedItem.Value);

            int shiftId = -1;
            if (cmbShiftList.SelectedItem != null && cmbShiftList.SelectedItem.Value != null)
                shiftId = int.Parse(cmbShiftList.SelectedItem.Value);

            bool? type = null;

            if (cmbShiftType.SelectedItem.Index == 1)
                type = false;
            else if (cmbShiftType.SelectedItem.Index == 2)
                type = true;

            int totalRecords = 0;

            if (SessionManager.CurrentLoggedInEmployeeId == 0)
            {
                List<GetEmployee_ShiftsResult> list = ShiftManager.GetEmployee_Shifts(branchId, depId, empId, type, shiftId,-1);
                totalRecords = list.Count;

                list = list.Skip(e.Start).Take(int.Parse(cmbPageSize.SelectedItem.Value)).ToList();

                this.storeEmpShift.DataSource = list;
                this.storeEmpShift.DataBind();
            }
            else
            {
                List<GetEmployee_ShiftsForManagerResult> list = ShiftManager.GetEmployee_ShiftsForManager(branchId, depId, SessionManager.CurrentLoggedInEmployeeId, type, shiftId);
                totalRecords = list.Count;

                list = list.Skip(e.Start).Take(int.Parse(cmbPageSize.SelectedItem.Value)).ToList();

                this.storeEmpShift.DataSource = list;
                this.storeEmpShift.DataBind();
            }


            e.Total = totalRecords;

        }

    }
}