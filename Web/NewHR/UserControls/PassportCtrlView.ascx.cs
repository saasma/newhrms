﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;

namespace Web.NewHR.UserControls
{
    public partial class PassportCtrlView : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        protected void Initialise()
        {
            int EmployeeID = GetEmployeeID();
            this.LoadPassportGrid(EmployeeID);

        }

        protected void PassportDownLoad(int ID)
        {
            HPassport doc = NewHRManager.GetPassportDetailsById(ID);

            string path = Context.Server.MapPath(@"~/uploads/" + doc.FileLocation + doc.ServerFileName);
            string contentType = doc.FileType;
            string name = doc.UserFileName;

            byte[] bytes = File.ReadAllBytes(path);

            Context.Response.Clear();
            Context.Response.ClearHeaders();
            Context.Response.AppendHeader("Content-Disposition", "attachment; filename=\"" + name + "\"");
            Context.Response.ContentType = contentType;
            Context.Response.BinaryWrite(bytes);
            Context.Response.End();
        }


        protected void LoadPassportGrid(int EmployeeID)
        {
            List<HPassport> _HPassport = NewHRManager.GetPassportByEmployeeID(EmployeeID);
            if (_HPassport != null)
            {
                this.StorePassport.DataSource = _HPassport;
                this.StorePassport.DataBind();
            }
            else
            {
                this.StorePassport.DataSource = this.PassportFillData;
                this.StorePassport.DataBind();
            }

            if (_HPassport.Count <= 0)
                GridPassport.Hide();
        }


        protected int GetEmployeeID()
        {
            int EmpID = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                EmpID = int.Parse(Request.QueryString["ID"]);
            }
            else
                EmpID = SessionManager.CurrentLoggedInEmployeeId;

            return EmpID;
        }

        private object[] PassportFillData
        {
            get
            {
                return new object[]
            {
                new object[] { "", "", "",""},
               
            };
            }
        }

        protected void btnDownloadPassFile_Click(object sender, EventArgs e)
        {
            int passportId = int.Parse(hdnPassportIdValue.Text);
            PassportDownLoad(passportId);
        }

    }
}