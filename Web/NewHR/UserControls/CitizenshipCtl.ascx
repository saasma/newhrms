﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CitizenshipCtl.ascx.cs"
    Inherits="Web.NewHR.UserControls.CitizenshipCtl" %>
<ext:Hidden runat="server" ID="hdnCitizenshipID">
</ext:Hidden>
<ext:Hidden runat="server" ID="hdnIsEmployee" />
<div style="width:823px; margin-left:-19px">
    <div>
        <!-- panel-btns -->
        <h4 class="sectionHeading">
            <i></i>Citizenship
        </h4>
    </div>
    <!-- panel-heading -->
    <div class="panel-body">
        <table class="fieldTable firsttdskip">
            <tr>
                <td>
                    <ext:GridPanel ID="GridCitizenship" runat="server" Width="823" Cls="itemgrid">
                        <Store>
                            <ext:Store ID="StoreCitizenship" runat="server">
                                <Model>
                                    <ext:Model ID="Model1" runat="server" IDProperty="CitizenshipId">
                                        <Fields>
                                            <ext:ModelField Name="Nationality" Type="string" />
                                            <ext:ModelField Name="CitizenshipNo" Type="String" />
                                            <ext:ModelField Name="IssueDate" Type="string" />
                                            <ext:ModelField Name="Place" Type="string" />
                                            <ext:ModelField Name="Status" Type="Int" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:Column ID="Column1" runat="server" Text="Nationality" DataIndex="Nationality"
                                    Width="100" Flex="1" />
                                <ext:Column ID="Column2" runat="server" Text="Citizenship Number" DataIndex="CitizenshipNo"
                                    Width="150">
                                </ext:Column>
                                <ext:Column ID="Column3" runat="server" Text="Issue Date" DataIndex="IssueDate" Width="100">
                                </ext:Column>
                                <ext:Column ID="Column4" runat="server" Text="Place" DataIndex="Place" Width="130">
                                </ext:Column>
                                <ext:CommandColumn ID="CommandColumn1" runat="server" Width="40">
                                    <Commands>
                                        <ext:CommandSeparator />                                        
                                        <ext:GridCommand Icon="PageWhitePut" CommandName="DownLoad">
                                            <ToolTip Text="DownLoad" />
                                            </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridCitizenship_Command">
                                            <ExtraParams>
                                                <ext:Parameter Name="ID" Value="record.data.CitizenshipId" Mode="Raw">
                                                </ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw">
                                                </ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>

                                <ext:CommandColumn ID="CommandColumn3" runat="server" Width="40">
                                    <Commands>
                                        <ext:CommandSeparator />
                                        <ext:GridCommand Cls="editGridButton" ToolTip-Text="Edit" Text="<i class='fa fa-pencil'></i>"
                                            CommandName="Edit" />
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridCitizenship_Command">
                                            <ExtraParams>
                                                <ext:Parameter Name="ID" Value="record.data.CitizenshipId" Mode="Raw">
                                                </ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw">
                                                </ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                    <PrepareToolbar Fn="prepareCitizenship" />
                                </ext:CommandColumn>

                                <ext:CommandColumn ID="CommandColumn2" runat="server" Width="40">
                                    <Commands>
                                        <ext:CommandSeparator />
                                        <ext:GridCommand Cls="editGridButton" ToolTip-Text="Delete" Text="<i class='fa fa-trash-o'></i>"
                                            CommandName="Delete" />
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridCitizenship_Command">
                                            <ExtraParams>
                                                <ext:Parameter Name="ID" Value="record.data.CitizenshipId" Mode="Raw">
                                                </ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw">
                                                </ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the record?" />
                                        </Command>
                                    </DirectEvents>
                                    <PrepareToolbar Fn="prepareCitizenship" />
                                </ext:CommandColumn>

                            </Columns>
                        </ColumnModel>
                        <SelectionModel>
                            <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                        </SelectionModel>
                    </ext:GridPanel>
                </td>
            </tr>
        </table>
        <div class="buttonBlockSection">
            <ext:Button runat="server" ID="LinkButton1" Cls="btn btn-primary btn-sect" Width="120" StyleSpec="margin-top:10px"
                Height="30" Text="<i></i>Add New Line">
                <DirectEvents>
                    <Click OnEvent="btnAddNewLine_Click">
                        <EventMask ShowMask="true" />
                    </Click>
                </DirectEvents>
            </ext:Button>
        </div>
    </div>
</div>
<ext:Window ID="AECitizenshipWindow" runat="server" Title="Citizenship Details" Icon="Application"
    Height="390" Width="550" BodyPadding="5" Hidden="true" Modal="true">
    <Content>
        <ext:Label ID="lblMsg" runat="server" />
        <table class="fieldTable">
            <tr>
                <td>
                    <ext:ComboBox ID="cmbNationality" Width="180px" runat="server" ValueField="Nationality"
                        DisplayField="Nationality" FieldLabel="Nationality" LabelAlign="top" LabelSeparator=""
                        ForceSelection="true" QueryMode="Local">
                        <%--<Items>
                            <ext:ListItem Text="Nepali" Value="Nepali" />
                            <ext:ListItem Text="Indian" Value="Indian" />
                        </Items>--%>
                        <Store>
                            <ext:Store ID="Store2" runat="server">
                                <Model>
                                    <ext:Model ID="Model4" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Nationality" Type="String" />
                                            <ext:ModelField Name="Nationality" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                    <asp:RequiredFieldValidator Display="None" ID="valcmbNationality" runat="server"
                        ValidationGroup="CitizenshipSaveUpdate" ControlToValidate="cmbNationality" ErrorMessage="Please choose a Nationality." />
                </td>
            </tr>
            <tr>
                <td valign="top">
                    <ext:TextField ID="txtCitizenshipNumber" Width="180px" runat="server" FieldLabel="Citizenship Number"
                        LabelAlign="top" LabelSeparator="" />
                    <asp:RequiredFieldValidator Display="None" ID="valtxtCitizenshipNumber" runat="server"
                        ValidationGroup="CitizenshipSaveUpdate" ControlToValidate="txtCitizenshipNumber"
                        ErrorMessage="Citizenship Number is required." />
                </td>
            </tr>
            <tr>
                <td>
                    <%--  <ext:DateField ID="calIssueDate" runat="server" Width="150" LabelAlign="Top" FieldLabel="Issue Date"
                                                LabelSeparator="">
                                            </ext:DateField>--%>
                    <pr:CalendarExtControl FieldLabel="Issue Date" Width="180px" ID="calIssueDate" runat="server"
                        LabelSeparator="" LabelAlign="Top" />
                    <asp:RequiredFieldValidator Display="None" ID="valcalIssueDate" runat="server" ValidationGroup="CitizenshipSaveUpdate"
                        ControlToValidate="calIssueDate" ErrorMessage="Please enter citizenship Issue Date." />
                </td>
                <td style="padding-top: 5px">
                    <ext:TextField ID="txtPlace" Width="180px" runat="server" FieldLabel="Place" LabelAlign="top"
                        LabelSeparator="" />
                    <asp:RequiredFieldValidator Display="None" ID="valtxtPlace" runat="server" ValidationGroup="CitizenshipSaveUpdate"
                        ControlToValidate="txtPlace" ErrorMessage="Please type the Place." />
                </td>
            </tr>
            <tr>
                <td style="padding-top: 5px;" valign="top">
                    <ext:FileUploadField ID="FileCitizenshipDocumentUpload" runat="server" Width="180px"
                        Icon="Attach" FieldLabel="&nbsp;" LabelAlign="Top" LabelSeparator="" />
                    <%-- <span style="color: #ACACAC; padding-top: 20px;">*(only doc,pdf,txt,docx,rar)</span>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationExpression="^.*\.(doc|DOC|pdf|PDF|txt|TXT|docx|DOCX|rar|RAR)$"
                        runat="server" ValidationGroup="CitizenshipSaveUpdate" Display="None" ControlToValidate="FileCitizenshipDocumentUpload"
                        ErrorMessage="Invalid file format."></asp:RegularExpressionValidator>--%>
                </td>
                <td>
                    <table style="margin-top: 15px">
                        <tr>
                            <td>
                                <ext:Label ID="lblUploadedFile" runat="server" Hidden="true">
                                </ext:Label>
                            </td>
                            <td>
                                <ext:LinkButton Icon="Delete" runat="server" ID="lnkDeleteFile" OnDirectClick="lnkDeleteFile_Click"
                                    Text="Delete file" Hidden="true">
                                    <DirectEvents>
                                        <Click OnEvent="lnkDeleteFile_Click">
                                            <EventMask ShowMask="true" />
                                        </Click>
                                    </DirectEvents>
                                </ext:LinkButton>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td valign="bottom" colspan="2">
                    <div class="popupButtonDiv">
                        <ext:Button runat="server" Cls="btn btn-primary" ID="btnSave" Text="<i></i>Save">
                            <DirectEvents>
                                <Click OnEvent="btnSave_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                            <Listeners>
                                <Click Handler="valGroup = 'CitizenshipSaveUpdate'; return CheckValidation();">
                                </Click>
                            </Listeners>
                        </ext:Button>
                        <div class="btnFlatOr">
                            or</div>
                        <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="LinkButton2" Cls="btnFlatLeftGap"
                            Text="<i></i>Cancel">
                            <Listeners>
                                <Click Handler="#{AECitizenshipWindow}.hide();">
                                </Click>
                            </Listeners>
                        </ext:LinkButton>
                    </div>
                </td>
            </tr>
        </table>
    </Content>
</ext:Window>

<script type="text/javascript">

    var prepareCitizenship = function (grid, toolbar, rowIndex, record) {
        var delBtn = toolbar.items.get(1);
        var IsEmployee = <%= hdnIsEmployee.ClientID %>.getValue();

        if (record.data.Status == 1 && IsEmployee == "1" ) {
            delBtn.setVisible(false);
        }

    } 

</script>
