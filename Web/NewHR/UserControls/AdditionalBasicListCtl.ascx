﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdditionalBasicListCtl.ascx.cs"
    Inherits="Web.NewHR.UserControls.AdditionalBasicListCtl" %>
<%@ Register Src="~/newhr/UserControls/EmployeeWizard.ascx" TagName="EmployeeWizard"
    TagPrefix="uc2" %>
<%@ Register Src="~/NewHR/UserControls/EmployeeDetailsCtl.ascx" TagName="EmployeeDetailsCtl"
    TagPrefix="uc1" %>
<script type="text/javascript">


        function searchList() {
           <%=PagingToolbar1.ClientID %>.doRefresh();
        }
        
        
        var CommandHandler2 = function(command, record){
            <%= hdn.ClientID %>.setValue(record.data.GradeAdditionalBasicID);
                if(command=="Edit")
                {
                    <%= btnEditAdditionalBasic.ClientID %>.fireEvent('click');
                }
                


        }
        var CommandHandler = function(command, record){
        <%= hdn.ClientID %>.setValue(record.data.RewardID);
           

            }
        function refreshWindow() {
            searchList();
        }

        function isPayrollSelected() {
            var cmb = <%=cmbIncome.ClientID %>;
            var incomeId = cmb.getValue() ==  null ? -1 : cmb.getValue();

            var ret = employeePopup("ID=" + incomeId);
            return false;
        }
</script>
<ext:LinkButton runat="server" Hidden="true" ID="btnEditAdditionalBasic" runat="server">
    <DirectEvents>
        <Click OnEvent="btnEditAdditionalBasic_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:LinkButton>
<ext:Button ID="btnDelete" runat="server" Hidden="true">
    <DirectEvents>
        <Click OnEvent="btnDeleteLevel_Click">
            <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the additional basic?" />
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:Button>
<ext:Hidden runat="server" ID="hdn">
</ext:Hidden>
<ext:Store ID="StoreLevel" runat="server">
    <Model>
        <ext:Model ID="Model1" runat="server" IDProperty="LevelId">
            <Fields>
                <ext:ModelField Name="LevelId" Type="String" />
                <ext:ModelField Name="GroupLevel" />
            </Fields>
        </ext:Model>
    </Model>
</ext:Store>
<ext:Store ID="StoreBranch" runat="server">
    <Model>
        <ext:Model ID="Model3" runat="server" IDProperty="BranchId">
            <Fields>
                <ext:ModelField Name="BranchId" Type="String" />
                <ext:ModelField Name="Name" />
            </Fields>
        </ext:Model>
    </Model>
</ext:Store>
<ext:Store ID="StoreIncome" runat="server">
    <Model>
        <ext:Model ID="Model4" runat="server" IDProperty="IncomeId">
            <Fields>
                <ext:ModelField Name="IncomeId" Type="String" />
                <ext:ModelField Name="Title" />
            </Fields>
        </ext:Model>
    </Model>
</ext:Store>
<div class="pageheader">
    <div class="media">
        <div class="media-body">
            <h4>
                Addition Income List
            </h4>
        </div>
    </div>
</div>
<div class="contentpanel" style="padding-top: 0px">
    <div class="innerLR">
        <ext:Label ID="lblMsg" runat="server" />
        <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
            <Proxy>
                <ext:AjaxProxy Url="../../Handler/EmpSearch.ashx">
                    <ActionMethods Read="GET" />
                    <Reader>
                        <ext:JsonReader Root="plants" TotalProperty="total" />
                    </Reader>
                </ext:AjaxProxy>
            </Proxy>
            <Model>
                <ext:Model ID="Model2" IDProperty="Name" Name="ItemLineModel" runat="server">
                    <Fields>
                        <ext:ModelField Name="Name" Type="String" />
                        <ext:ModelField Name="EmployeeId" Type="String" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
        <table class="fieldTable firsttdskip" runat="server" id="tbl">
            <tr>
                <td>
                    <ext:ComboBox ID="cmbSearch" FieldLabel="Search Employee" LabelWidth="70" LabelAlign="Top"
                        runat="server" DisplayField="Name" ValueField="EmployeeId" StoreID="storeSearch"
                        TypeAhead="false" Width="180" PageSize="9999" HideBaseTrigger="true" MinChars="1"
                        TriggerAction="All" ForceSelection="true">
                        <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                            <ItemTpl ID="ItemTpl1" runat="server">
                                <Html>
                                    <tpl>
                                            <div class="search-item">
                                                            <span>{Name}</span>  
                                             </div>
					                </tpl>
                                </Html>
                            </ItemTpl>
                        </ListConfig>
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                        </Triggers>
                        <Listeners>
                            <Select Handler="this.getTrigger(0).show();" />
                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                            <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                        </Listeners>
                    </ext:ComboBox>
                </td>
                <td>
                    <ext:ComboBox FieldLabel="Level" ID="cmbLevelList" Width="180px" runat="server" ValueField="LevelId"
                        DisplayField="GroupLevel" StoreID="StoreLevel" LabelAlign="Top" LabelSeparator=""
                        ForceSelection="true" QueryMode="Local">
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                        </Triggers>
                        <Listeners>
                            <Select Handler="this.getTrigger(0).show();" />
                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                            <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                        </Listeners>
                    </ext:ComboBox>
                </td>
                <td>
                    <ext:ComboBox FieldLabel="Branch" ID="cmbBranch" Width="180px" runat="server" ValueField="BranchId"
                        DisplayField="Name" StoreID="StoreBranch" LabelAlign="Top" LabelSeparator=""
                        ForceSelection="true" QueryMode="Local">
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                        </Triggers>
                        <Listeners>
                            <Select Handler="this.getTrigger(0).show();" />
                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                            <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                        </Listeners>
                    </ext:ComboBox>
                </td>
                <td>
                    <ext:ComboBox FieldLabel="Income" ID="cmbIncome" Width="180px" runat="server" ValueField="IncomeId"
                        DisplayField="Title" StoreID="StoreIncome" LabelAlign="Top" LabelSeparator=""
                        ForceSelection="true" QueryMode="Local">
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                        </Triggers>
                        <Listeners>
                            <Select Handler="this.getTrigger(0).show();" />
                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                            <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                        </Listeners>
                    </ext:ComboBox>
                    <asp:RequiredFieldValidator Display="None" ID="valtxtInspectedBy" runat="server"
                        ValidationGroup="IncomeValidation" ControlToValidate="cmbIncome" ErrorMessage="Income is required." />
                </td>
                <td style="padding-top: 20px">
                    <ext:Button runat="server" Width="100px" ID="btnLoad" Cls="btn btn-primary" Text="<i></i>Load"
                        runat="server">
                        <Listeners>
                            <Click Handler="searchList();" />
                        </Listeners>
                    </ext:Button>
                </td>
                <td style="padding-top: 15px">
                    <ext:Checkbox ID="chkShowLevelHistory" runat="server" BoxLabel="Show Past Level Also" />
                </td>
                <td>
                    <div class="left">
                        <ext:Button ID="btnExportPopup1" runat="server" Text="Import" OnClientClick="isPayrollSelected();return false;"
                            StyleSpec="float: left;" />
                    </div>
                </td>
                <td>
                    <div class="left">
                        <ext:Button ID="btnExportDetails" runat="server" Text="Export Details" AutoPostBack="true"
                            OnClick="btnExportDetails_Click">
                            <Listeners>
                                <Click Handler="valGroup = 'IncomeValidation';  return CheckValidation();">
                                </Click>
                            </Listeners>
                        </ext:Button>
                    </div>
                </td>
            </tr>
        </table>
        <div style="clear: both; padding-top: 10px;">
        </div>
        <ext:GridPanel Border="true" StyleSpec="margin-top:5px;" Width="1000" ID="gridRewardList"
            runat="server" Cls="itemgrid">
            <Store>
                <ext:Store ID="gridRewardStore" PageSize="50" runat="server">
                    <Proxy>
                        <ext:AjaxProxy Url="~/Handler/AdditionaBasicListHandler.ashx">
                            <ActionMethods Read="GET" />
                            <Reader>
                                <ext:JsonReader Root="data" TotalProperty="total" />
                            </Reader>
                        </ext:AjaxProxy>
                    </Proxy>
                    <AutoLoadParams>
                        <%-- <ext:Parameter Name="UserName" Mode="Raw" Value="#{hdUserName}.getValue()" />--%>
                        <ext:Parameter Name="start" Value="={0}" />
                        <ext:Parameter Name="limit" Value="={50}" />
                    </AutoLoadParams>
                    <Parameters>
                        <ext:StoreParameter Name="EmployeeId" Value="#{cmbSearch}.getValue() == null ? '-1' : #{cmbSearch}.getValue()"
                            Mode="Raw" />
                        <ext:StoreParameter Name="ShowHistory" Value="#{chkShowLevelHistory}.getValue() == null ? 'false' : #{chkShowLevelHistory}.getValue()"
                            Mode="Raw" />
                        <ext:StoreParameter Name="LevelId" Mode="Raw" Value="#{cmbLevelList}.getValue() == null ? '-1' : #{cmbLevelList}.getValue()" />
                        <ext:StoreParameter Name="BranchId" Mode="Raw" Value="#{cmbBranch}.getValue() == null ? '-1' : #{cmbBranch}.getValue()" />
                        <ext:StoreParameter Name="IncomeId" Mode="Raw" Value="#{cmbIncome}.getValue() == null ? '-1' : #{cmbIncome}.getValue()" />
                    </Parameters>
                    <Model>
                        <ext:Model runat="server" Root="data" IDProperty="GradeAdditionalBasicID">
                            <Fields>
                                <ext:ModelField Name="GradeAdditionalBasicID" Type="string" />
                                <ext:ModelField Name="Name" Type="string" />
                                <ext:ModelField Name="Income" Type="string" />
                                <ext:ModelField Name="Amount" Type="string" />
                                <ext:ModelField Name="LevelId" Type="string" />
                                <ext:ModelField Name="EmployeeId" Type="string" />
                                <ext:ModelField Name="Level" Type="string" />
                                <ext:ModelField Name="Branch" Type="string" />
                                <ext:ModelField Name="Notes" Type="string" />
                                <ext:ModelField Name="CurrentDesignation" Type="string" />
                                <ext:ModelField Name="CurrentLevel" Type="string" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>
            <ColumnModel>
                <Columns>
                    <ext:Column ID="Column6" Width="50" Sortable="false" MenuDisabled="true" runat="server"
                        Text="EIN" Align="Left" DataIndex="EmployeeId">
                    </ext:Column>
                    <ext:Column ID="Column_Employee" Sortable="true" runat="server" Text="Employee" Align="Left"
                        Width="200" DataIndex="Name" Hideable="false" />
                    <ext:Column ID="Column_Level" Width="200" Sortable="false" MenuDisabled="true" runat="server"
                        Text="Income Level" Align="Left" DataIndex="Level">
                    </ext:Column>
                    <ext:Column ID="Column4" Width="120" Sortable="false" MenuDisabled="true" runat="server"
                        Text="Current Level" Align="Left" DataIndex="CurrentLevel">
                    </ext:Column>
                    <ext:Column ID="Column5" Width="120" Sortable="false" MenuDisabled="true" runat="server"
                        Text="Current Designation" Align="Left" DataIndex="CurrentDesignation">
                    </ext:Column>
                    <ext:Column ID="Column2" Width="140" Sortable="false" MenuDisabled="true" runat="server"
                        Text="Income" Align="Left" DataIndex="Income">
                    </ext:Column>
                    <ext:Column ID="Column3" Width="100" Sortable="false" MenuDisabled="true" runat="server"
                        Text="Amount" Align="Right" DataIndex="Amount">
                        <Renderer Fn="getFormattedAmount" />
                    </ext:Column>
                    <ext:Column ID="Column1" Width="250" Sortable="false" MenuDisabled="true" runat="server"
                        Text="Notes" Align="Left" DataIndex="Notes">
                    </ext:Column>
                    <ext:CommandColumn ID="CommandColumn1" runat="server" Width="75" Text="Actions" Align="Center">
                        <%--<Commands>
                        <ext:CommandSeparator />
                        <ext:GridCommand Text="<i></i>" Cls="btn-action glyphicons pencil btn-success" CommandName="Edit" />
                        <ext:GridCommand Text="<i></i>" Cls="btn-action glyphicons remove_2 btn-danger" CommandName="Delete" />
                    </Commands>--%>
                        <Listeners>
                            <Command Handler="CommandHandler(command,record);" />
                        </Listeners>
                    </ext:CommandColumn>
                </Columns>
            </ColumnModel>
            <View>
                <ext:GridView ID="GridView1" runat="server">
                    <%--<GetRowClass Fn="getRowClass" />--%>
                </ext:GridView>
            </View>
            <SelectionModel>
                <ext:RowSelectionModel ID="RowSelectionModel2111" runat="server" Mode="Single" />
            </SelectionModel>
            <BottomBar>
                <ext:PagingToolbar PageSize="50" ID="PagingToolbar1" DisplayInfo="true" DisplayMsg="Displaying record {0} - {1} of {2}"
                    EmptyMsg="No record to display" runat="server">
                    <%-- <Plugins>
                    <ext:GroupPaging ID="GroupPaging1" runat="server" />
                </Plugins>--%>
                </ext:PagingToolbar>
            </BottomBar>
        </ext:GridPanel>
        <div class="buttonBlock">
            <div class="left" style="display: none">
                <ext:LinkButton runat="server" StyleSpec="padding:0px;margin-top:20px;" ID="btnAdd"
                    Cls="btnFlat" BaseCls="btnFlat" Text="<i></i>Add" runat="server">
                    <DirectEvents>
                        <Click OnEvent="btnAdd_Click">
                            <EventMask ShowMask="true" />
                        </Click>
                    </DirectEvents>
                </ext:LinkButton>
            </div>
        </div>
        <div style="clear: both">
        </div>
    </div>
    <ext:Window ID="window" runat="server" Title="Addition Amount" Icon="Application"
        Height="420" Width="500" BodyPadding="5" Hidden="true" Modal="true">
        <Content>
            <table class="fieldTable">
                <tr>
                    <td runat="server" id="employeeTD">
                        <ext:ComboBox ID="cmbGradeEmployee" LabelSeparator="" FieldLabel="Employee" LabelWidth="70"
                            LabelAlign="Top" runat="server" DisplayField="Name" ValueField="EmployeeId" StoreID="storeSearch"
                            TypeAhead="false" Width="180" PageSize="9999" HideBaseTrigger="true" MinChars="1"
                            TriggerAction="All" ForceSelection="true">
                            <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                                <ItemTpl ID="ItemTpl2" runat="server">
                                    <Html>
                                        <tpl>
                                            <div class="search-item">
                                                            <span>{Name}</span>  
                                             </div>
					                </tpl>
                                    </Html>
                                </ItemTpl>
                            </ListConfig>
                            <%-- <DirectEvents>
                            <Select OnEvent="cmbGradeEmployee_Select">
                                <EventMask ShowMask="true" />
                            </Select>
                        </DirectEvents>--%>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator3" runat="server"
                            ValidationGroup="SaveUpdate" ControlToValidate="cmbGradeEmployee" ErrorMessage="Employee is required." />
                    </td>
                    <td>
                        <ext:DisplayField ID="dispLevel" LabelSeparator="" LabelAlign="Top" runat="server"
                            FieldLabel="Level">
                        </ext:DisplayField>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:TextField FieldLabel="Amount" Width="180px" ID="txtRate" runat="server" LabelSeparator=""
                            LabelAlign="Top" />
                        <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator1" runat="server"
                            ValidationGroup="SaveUpdate" ControlToValidate="txtRate" ErrorMessage="Rate is required." />
                        <asp:CompareValidator Display="None" ID="RequiredFieldValidator2" runat="server"
                            Type="Currency" Operator="GreaterThan" ValueToCompare="0" ValidationGroup="SaveUpdate"
                            ControlToValidate="txtRate" ErrorMessage="Invalid rate." />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <ext:TextArea ID="txtNotes" runat="server" FieldLabel="Notes" LabelSeparator="" LabelAlign="Top"
                            Rows="3" Cols="55" />
                    </td>
                </tr>
                <tr>
                    <td valign="bottom" colspan="2">
                        <div class="popupButtonDiv">
                            <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="btnSave" Cls="btnFlat"
                                BaseCls="btnFlat" Text="<i></i>Save">
                                <DirectEvents>
                                    <Click OnEvent="btnSaveAdditionalBasic_Click">
                                        <EventMask ShowMask="true" />
                                    </Click>
                                </DirectEvents>
                                <Listeners>
                                    <Click Handler="valGroup = 'SaveUpdate'; return CheckValidation();">
                                    </Click>
                                </Listeners>
                            </ext:LinkButton>
                            <div class="btnFlatOr">
                                or</div>
                            <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="LinkButton2" Cls="btnFlatLeftGap"
                                Text="<i></i>Cancel">
                                <Listeners>
                                    <Click Handler="#{window}.hide();">
                                    </Click>
                                </Listeners>
                            </ext:LinkButton>
                        </div>
                    </td>
                </tr>
            </table>
        </Content>
    </ext:Window>
</div>
