﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;
using BLL.Base;
using Ext.Net;

namespace Web.NewHR.UserControls
{
    public partial class NomineeCtrlView : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        private void Initialise()
        {
            BindGrid();
        }

        protected int GetEmployeeID()
        {
            int EmpID = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                EmpID = int.Parse(Request.QueryString["ID"]);
            }
            else
            {
                EmpID = SessionManager.CurrentLoggedInEmployeeId;
            }
            return EmpID;
        }

        private void BindGrid()
        {
            int EmployeeID = GetEmployeeID();
            List<HrNominee> list = NewHRManager.GetHrNomineeByEmployeeId(EmployeeID);
            
            gridNominee.Store[0].DataSource = list;
            gridNominee.Store[0].DataBind();

            if(list.Count <= 0)
                gridNominee.Hide();
        }

    }
}