﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;


namespace Web.NewHR.UserControls
{
    public partial class EducationCtrlView : BaseUserControl
    {      

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise(0);
            }
        }

        public void Initialise(int employeeId)
        {
            int EmployeeID = GetEmployeeID();

            if (employeeId != 0)
                EmployeeID = employeeId;


            this.LoadEducationGrid(EmployeeID);


        }



        protected void EducationDownLoad(int ID)
        {
            HEducation doc = NewHRManager.GetEducationDetailsById(ID);

            string path = Server.MapPath(@"~/uploads/" + doc.FileLocation + doc.ServerFileName);
            string contentType = doc.FileType;
            string name = doc.UserFileName;

            byte[] bytes = File.ReadAllBytes(path);

            Context.Response.Clear();
            Context.Response.ClearHeaders();
            Context.Response.AppendHeader("Content-Disposition", "attachment; filename=\"" + name + "\"");
            Context.Response.ContentType = contentType;
            Context.Response.BinaryWrite(bytes);
            Context.Response.End();
        }

        protected void LoadEducationGrid(int EmployeeID)
        {
            List<HEducation> _HEducation = NewHRManager.GetEducationByEmployeeID(EmployeeID);

            if (SessionManager.CurrentLoggedInEmployeeId == 0)
                _HEducation = _HEducation.Where(x => x.Status == (int)HRStatusEnum.Approved).ToList();

            if (Request.QueryString["ID"] != null)
            {
                if (int.Parse(Request.QueryString["ID"].ToString()) != SessionManager.CurrentLoggedInEmployeeId)
                    _HEducation = _HEducation.Where(x => x.Status == (int)HRStatusEnum.Approved).ToList();
            }

            this.StoreEducation.DataSource = _HEducation;
            this.StoreEducation.DataBind();

            if (_HEducation.Count <= 0)
                GridEducation.Hide();

        }
        protected int GetEmployeeID()
        {
            int EmpID = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                EmpID = int.Parse(Request.QueryString["ID"]);
            }
            else
            {
                EmpID = SessionManager.CurrentLoggedInEmployeeId;
            }
            return EmpID;
        }


        public void btnDownloadEduFile_Click(object sender, EventArgs e)
        {
            int educationId = int.Parse(hdnEducationIdValue.Text);
            this.EducationDownLoad(educationId);
        }

    }
}