﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PublicationCtl.ascx.cs"
    Inherits="Web.NewHR.UserControls.PublicationCtl" %>

    <script type="text/javascript">

        var prepare = function (grid, toolbar, rowIndex, record) {
            var editBtn = toolbar.items.get(0);
            var deleteBtn = toolbar.items.get(2);

            if (record.data.IsEditable == 0) {
                editBtn.setVisible(false);
                deleteBtn.setVisible(false);
            }
        }

    </script>

<ext:Hidden runat="server" ID="hdnPublicationID">
</ext:Hidden>

    <div class="widget-body">
        <table class="fieldTable firsttdskip">
            <tr>
                <td>
                    <ext:GridPanel ID="GridPublication" runat="server" Width="900">
                        <Store>
                            <ext:Store ID="StorePublication" runat="server">
                                <Model>
                                    <ext:Model ID="Model1" runat="server" IDProperty="PublicationId">
                                        <Fields>
                                            <ext:ModelField Name="PublicationName" Type="string" />
                                            <ext:ModelField Name="PublicationTypeName" Type="String" />
                                            <ext:ModelField Name="Publisher" Type="string" />
                                            <ext:ModelField Name="Country" Type="string" />
                                            <ext:ModelField Name="Year" Type="string" />
                                            <ext:ModelField Name="IsEditable" Type="Int" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:Column ID="Column1" runat="server" Text="Publication Name" DataIndex="PublicationName"
                                    Flex="1" Width="200" />
                                <ext:Column ID="Column2" runat="server" Text="Publication Type" DataIndex="PublicationTypeName"
                                    Width="150">
                                </ext:Column>
                                <ext:Column ID="Column3" runat="server" Text="Publisher" DataIndex="Publisher" Width="200">
                                </ext:Column>
                                <ext:Column ID="Column4" runat="server" Text="Country" DataIndex="Country" Flex="1">
                                </ext:Column>
                                <ext:Column ID="Column6" runat="server" Text="Year" DataIndex="Year">
                                </ext:Column>
                                <ext:CommandColumn ID="CommandColumn1" runat="server" Width="70">
                                    <Commands>
                                        <ext:GridCommand Text="<i></i>" Cls="btn-action glyphicons pencil btn-success" CommandName="Edit">
                                            <ToolTip Text="Edit" />
                                        </ext:GridCommand>
                                        <ext:CommandSeparator />
                                        <ext:GridCommand Text="<i></i>" Cls="btn-action glyphicons remove_2 btn-danger" CommandName="Delete">
                                            <ToolTip Text="Delete" />
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridPublication_Command">
                                            <EventMask ShowMask="true">
                                            </EventMask>
                                            <ExtraParams>
                                                <ext:Parameter Name="ID" Value="record.data.PublicationId" Mode="Raw">
                                                </ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw">
                                                </ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                    <PrepareToolbar Fn="prepare" />
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <SelectionModel>
                            <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                        </SelectionModel>
                    </ext:GridPanel>
                </td>
            </tr>
        </table>
        <div class="buttonBlock">
            <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="LinkButton1" Cls="btnFlat"
                BaseCls="btnFlat" Text="<i></i>Add New Line">
                <DirectEvents>
                    <Click OnEvent="btnAddNewLine_Click">
                        <EventMask ShowMask="true" />
                    </Click>
                </DirectEvents>
            </ext:LinkButton>
        </div>
    </div>

<ext:Window ID="AEPublicationWindow" runat="server" Title="Add/Edit Publication"
    Icon="Application" Height="360" Width="450" 
    BodyPadding="5" Hidden="true" Modal="true">
    <Content>
        <table class="fieldTable">
            <tr>
                <td colspan="2">
                   <ext:ComboBox ID="cmbPublicationType" runat="server" ValueField="PublicationTypeID" DisplayField="PublicationTypeName"
                        Width="180" FieldLabel="Publication Type" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                        QueryMode="Local">
                        <%--<Items>
                            <ext:ListItem Text="Book" Value="1" />
                            <ext:ListItem Text="Research" Value="2" />
                            <ext:ListItem Text="Article" Value="3" />
                            <ext:ListItem Text="Report" Value="4" />
                        </Items>--%>
                         <Store>
                            <ext:Store ID="Store1" runat="server">
                                <Model>
                                    <ext:Model ID="Model2" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PublicationTypeID" Type="String" />
                                            <ext:ModelField Name="PublicationTypeName" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                    <asp:RequiredFieldValidator Display="None" ID="valcmbPublicationType" runat="server"
                        ValidationGroup="PublicationSaveUpdate" ControlToValidate="cmbPublicationType"
                        ErrorMessage="Publication Type is required." />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <ext:TextField ID="txtPublicationName" runat="server" FieldLabel="Publication Name"
                        LabelAlign="top" LabelSeparator="" Width="400" />
                    <asp:RequiredFieldValidator Display="None" ID="valtxtPublicationName" runat="server"
                        ValidationGroup="PublicationSaveUpdate" ControlToValidate="txtPublicationName"
                        ErrorMessage="Publication Name is required." />
                </td>
            </tr>
            <tr>
                <td>
                    <ext:TextField ID="txtPublisher" Width="180" runat="server" FieldLabel="Publisher"
                        LabelAlign="top" LabelSeparator="" />
                    <asp:RequiredFieldValidator Display="None" ID="valtxtPublisher" runat="server" ValidationGroup="PublicationSaveUpdate"
                        ControlToValidate="txtPublisher" ErrorMessage="Publihser is required." />
                </td>
                <td>
                    <ext:ComboBox ID="cmbCountry" Width="180" runat="server" ValueField="CountryName"
                        DisplayField="CountryName" FieldLabel="Country" LabelAlign="top" LabelSeparator=""
                        ForceSelection="true" QueryMode="Local">
                        <Store>
                            <ext:Store ID="Store2" runat="server">
                                <Model>
                                    <ext:Model ID="Model4" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="CountryName" />
                                            <ext:ModelField Name="CountryName" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                    <asp:RequiredFieldValidator Display="None" ID="valcmbCountry" runat="server" ValidationGroup="PublicationSaveUpdate"
                        ControlToValidate="cmbCountry" ErrorMessage="Country is required." />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <ext:TextField ID="txtYear" Width="180" runat="server" FieldLabel="Year" LabelAlign="top"
                        LabelSeparator="" MaskRe="[0-9]|\.|%" />
                    <asp:RequiredFieldValidator Display="None" ID="valtxtYear" runat="server" ValidationGroup="PublicationSaveUpdate"
                        ControlToValidate="txtYear" ErrorMessage="Year is required." />
                </td>
            </tr>
            <tr>
                <td valign="bottom" colspan="2">
                    <div class="popupButtonDiv">
                        <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="btnSave" Cls="btnFlat"
                            BaseCls="btnFlat" Text="<i></i>Save">
                            <DirectEvents>
                                <Click OnEvent="btnSave_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                            <Listeners>
                                <Click Handler="valGroup = 'PublicationSaveUpdate'; if(CheckValidation()) return this.disable(); else return false;">
                                </Click>
                            </Listeners>
                        </ext:LinkButton>
                        <div class="btnFlatOr">
                            or
                        </div>
                        <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="LinkButton2" Cls="btnFlatLeftGap"
                            Text="<i></i>Cancel">
                            <Listeners>
                                <Click Handler="#{AEPublicationWindow}.hide();">
                                </Click>
                            </Listeners>
                        </ext:LinkButton>
                    </div>
                </td>
            </tr>
        </table>
    </Content>
</ext:Window>
