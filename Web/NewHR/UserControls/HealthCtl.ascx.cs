﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;

using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;
using Utils;

namespace Web.NewHR.UserControls
{
    public partial class HealthCtl : BaseUserControl
    {
        private bool _isDisplayMode = false;
        public bool IsDisplayMode
        {
            set
            {
                if (value == true)
                {
                    _isDisplayMode = value;
                    btnEditHandicapped.Visible = false;
                    btnSaveBirthMark.Visible = false;
                    txtBirthMark.Visible = false;
                    LinkButton1.Visible = false;
                    td1.Visible = false;
                    td2.Visible = false;
                    CommandColumn1.Visible = false;
                    CommandColumn2.Visible = false;
                }
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                lblHandicapped1.Text = CommonManager.GetHandicappedName;
                Initialise();
            }
        }
        protected void Initialise()
        {
            int EmployeeID = GetEmployeeID();

            cmbContition.Store[0].DataSource = CommonManager.GetHealthConditionList();
            cmbContition.Store[0].DataBind();

            cmbBoodGroup.Store[0].DataSource = CommonManager.GetAllBloodGroup();
            cmbBoodGroup.Store[0].DataBind();

            this.LoadHealthGrid(EmployeeID);
            this.LoadHandicappedDetails(EmployeeID);

            EEmployee emp = EmployeeManager.GetEmployeeById(EmployeeID);

            if (emp != null && emp.Birthmark != null)
                txtBirthMark.Text = emp.Birthmark;

            HHumanResource hRes = EmployeeManager.GethHumanResourceById(EmployeeID);
            if (hRes != null && hRes.BloodGroup != null && hRes.BloodGroup != "")
            {
                    ExtControlHelper.ComboBoxSetSelected(hRes.BloodGroup, cmbBoodGroup);
                //cmbBoodGroup.SelectedItem.Value = hRes.BloodGroup;
            }

            valRegPhone.ValidationExpression = Config.PhoneRegExp;
            varRegEmail.ValidationExpression = Config.EmailAddressRegEx;

        }
        protected void btnSave_Click(object sender, DirectEventArgs e)
        {

            this.SaveFormData();

        }
        private void SaveFormData()
        {

            Status myStatus = new Status();
            HHealth _HHealth = new HHealth();
            _HHealth.EmployeeId = this.GetEmployeeID();
            bool isSave = bool.Parse(this.hdnHealthID.Text == "" ? "true" : "false");
            if (!isSave)
                _HHealth.HealthId = int.Parse(this.hdnHealthID.Text);

            _HHealth.HealthStatus = txtHealthStatus.Text.Trim();
            if (!string.IsNullOrEmpty(cmbContition.SelectedItem.Text))
            {
                _HHealth.ContitionTypeName = cmbContition.SelectedItem.Text;
                _HHealth.ContitionTypeID = int.Parse(cmbContition.SelectedItem.Value);
            }

            if (!string.IsNullOrEmpty(cmbDiagnosed.SelectedItem.Text))
            {
                _HHealth.DiagnosedTypeName = cmbDiagnosed.SelectedItem.Text;
                _HHealth.DiagnosedTypeID = int.Parse(cmbDiagnosed.SelectedItem.Value);
            }

            if (!string.IsNullOrEmpty(cmbOngoingTreatment.SelectedItem.Text))
            {
                _HHealth.OngoingTreatment = bool.Parse(cmbOngoingTreatment.SelectedItem.Value);

            }

            if (!string.IsNullOrEmpty(calDiagnosed.Text.Trim()))
            {
                _HHealth.DiagnosedDate = calDiagnosed.Text.Trim();
                _HHealth.DiagnosedDateEng = BLL.BaseBiz.GetEngDate(_HHealth.DiagnosedDate, IsEnglish);
            }
            if (!string.IsNullOrEmpty(txtHospitalClinic.Text.Trim()))
            {
                _HHealth.HospitalClinicName = txtHospitalClinic.Text.Trim();

            }
            _HHealth.Details = txtDetails.Text;

            if (!string.IsNullOrEmpty(txtHospitalAddress.Text.Trim()))
                _HHealth.HospitalAddress = txtHospitalAddress.Text.Trim();

            if (!string.IsNullOrEmpty(txtDoctorName.Text.Trim()))
                _HHealth.DoctorName = txtDoctorName.Text.Trim();

            if (!string.IsNullOrEmpty(txtPhoneNo.Text.Trim()))
                _HHealth.PhoneNumber = txtPhoneNo.Text.Trim();

            if (!string.IsNullOrEmpty(txtHeight.Text.Trim()))
                _HHealth.Height = txtHeight.Text.Trim();

            if (!string.IsNullOrEmpty(txtDoctorEmail.Text.Trim()))
                _HHealth.DoctorEmail = txtDoctorEmail.Text.Trim();

            myStatus = NewHRManager.Instance.InsertUpdateHealth(_HHealth, isSave);
            if (myStatus.IsSuccess)
            {
                if (isSave)
                    NewMessage.ShowNormalMessage("Record Save successfully");
                else
                    NewMessage.ShowNormalMessage("Record Updated successfully");

                //  this.ClearFields();
                this.AEHealthWindow.Close();
                this.LoadHealthGrid(this.GetEmployeeID());

            }
            else
                NewMessage.ShowWarningMessage(myStatus.ErrorMessage);
        }
        protected void LoadHandicappedDetails(int EmployeeID)
        {
            EEmployee _EEmployee = NewHRManager.Instance.GetHandicappedDetailsByEmployee(EmployeeID);
            if (_EEmployee != null)
            {

                txtHandicappedDetail.Text = _EEmployee.HandicappedDetails;
                lblreftxtHandicappedDetail.Text = _EEmployee.HandicappedDetails;
                if (_EEmployee.IsHandicapped != null)
                {
                    cmbHandicapped.SetValue(_EEmployee.IsHandicapped.ToString());
                    if (bool.Parse(_EEmployee.IsHandicapped.ToString()))
                        lblRefcmbHandicapped.Text = "Yes";
                    else
                        lblRefcmbHandicapped.Text = "No";
                }
                else
                {
                    lblRefcmbHandicapped.Text = "No";
                    cmbHandicapped.SetValue("False");

                }


            }

        }
        protected void LoadHealthGrid(int EmployeeID)
        {
            List<HHealth> _HHealth = NewHRManager.GetHealthByEmployeeID(EmployeeID);
            this.StoreHealth.DataSource = _HHealth;
            this.StoreHealth.DataBind();

            if (_isDisplayMode && _HHealth.Count <= 0)
                GridHealth.Hide();

        }
        protected int GetEmployeeID()
        {
            //Guid? AssetID = null;
            int EmpID = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                EmpID = int.Parse(Request.QueryString["ID"]);
            }
            return EmpID;
        }
        protected void btnAddNewLine_Click(object sender, DirectEventArgs e)
        {
            this.ClearFields();
            this.AEHealthWindow.Show();

        }
        protected void btnEditHandicapped_Click(object sender, DirectEventArgs e)
        {

            if (cmbHandicapped.SelectedItem.Text == "Yes")
            {
                txtHandicappedDetail.Show();
            }
            else
                txtHandicappedDetail.Hide();
            lblRefcmbHandicapped.Hide();
            lblreftxtHandicappedDetail.Hide();
            cmbHandicapped.Show();
            btnEditHandicapped.Hide();
            btnUpdateHandicapped.Show();

        }
        protected void btnUpdateHandicapped_Click(object sender, DirectEventArgs e)
        {

            Status myStatus = new Status();
            EEmployee _EEmployee = new EEmployee();
            _EEmployee.EmployeeId = GetEmployeeID();
            _EEmployee.IsHandicapped = bool.Parse(cmbHandicapped.SelectedItem.Value);
            _EEmployee.HandicappedDetails = txtHandicappedDetail.Text.Trim();
            txtDetails.Disabled = false;
            myStatus = NewHRManager.Instance.UpdateHandicappedDetail(_EEmployee);
            if (myStatus.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Record Updated successfully.");
                lblRefcmbHandicapped.Show();
                lblRefcmbHandicapped.Text = cmbHandicapped.SelectedItem.Text;
                cmbHandicapped.Hide();

                btnUpdateHandicapped.Hide();
                btnEditHandicapped.Show();

                lblreftxtHandicappedDetail.Show();
                txtHandicappedDetail.Hide();
                lblreftxtHandicappedDetail.Text = txtHandicappedDetail.Text;



            }
        }


        protected void btnSaveBirthMark_Click(object sender, DirectEventArgs e)
        {

            Status myStatus = new Status();
            EEmployee _EEmployee = new EEmployee();
            _EEmployee.EmployeeId = GetEmployeeID();
            _EEmployee.Birthmark = txtBirthMark.Text.Trim();

            myStatus = NewHRManager.Instance.UpdateBrithMark(_EEmployee);

            HHumanResource _HHumanResource = new HHumanResource();
            _HHumanResource.EmployeeId = GetEmployeeID();
            _HHumanResource.BloodGroup = cmbBoodGroup.SelectedItem.Value;
            myStatus = NewHRManager.Instance.UpdateBloodGroup(_HHumanResource);
            if (myStatus.IsSuccess)
                NewMessage.ShowNormalMessage("Brithmark and blood group updated successfully.");

        }


        protected void GridHealth_Command(object sender, DirectEventArgs e)
        {
            string commandName = e.ExtraParams["command"];
            int HealthID = int.Parse(e.ExtraParams["ID"]);
            HHealth _HHealth = NewHRManager.GetHealthDetailsById(int.Parse(e.ExtraParams["ID"]));
            switch (commandName)
            {
                case "Delete":
                    this.DeleteData(HealthID);
                    break;

                case "Edit":
                    this.editHealth(HealthID);
                    break;
            }

        }
        protected void DeleteData(int ID)
        {

            bool result = NewHRManager.DeleteHealthByID(ID);
            if (result)
            {
                NewMessage.ShowNormalMessage("Record deleted successfully.");
                this.LoadHealthGrid(this.GetEmployeeID());
            }

        }
        protected void ClearFields()
        {
            this.hdnHealthID.Text = "";
            txtHealthStatus.Text = "";
            cmbContition.Value = "";
            cmbDiagnosed.Value = "";
            txtHospitalClinic.Text = "";
            calDiagnosed.Text = "";
            cmbOngoingTreatment.Value = "";
            txtDetails.Text = "";
            txtBirthMark.Clear();
            txtDoctorName.Text = "";
            txtHospitalAddress.Text = "";
            txtPhoneNo.Text = "";
            txtHospitalClinic.Hidden = false;
            txtHospitalAddress.Hidden = false;
            txtDoctorName.Hidden = false;
            txtPhoneNo.Hidden = false;
            txtHeight.Text = "";
            txtDoctorEmail.Text = "";
            txtDoctorEmail.Hide();
            btnSave.Text = Resources.Messages.Save;
        }
        public void editHealth(int HealthID)
        {

            HHealth _HHealth = NewHRManager.GetHealthDetailsById(HealthID);
            this.hdnHealthID.Text = HealthID.ToString();
            txtHealthStatus.Text = _HHealth.HealthStatus;

            //cmbContition.Store[0].ClearFilter();
            if (_HHealth.ContitionTypeID != null)
                cmbContition.SetValue(_HHealth.ContitionTypeID.ToString());
            else
                cmbContition.ClearValue();

            //cmbContition.SetValue(_HHealth.ContitionTypeID.ToString());

            cmbDiagnosed.SetValue(_HHealth.DiagnosedTypeID.ToString());
            calDiagnosed.Text = _HHealth.DiagnosedDate;
            txtHospitalClinic.Text = _HHealth.HospitalClinicName;
            cmbOngoingTreatment.SetValue(_HHealth.OngoingTreatment.ToString());
            txtDetails.Text = _HHealth.Details;

            txtHospitalAddress.Text = _HHealth.HospitalAddress;
            txtDoctorName.Text = _HHealth.DoctorName;
            txtPhoneNo.Text = _HHealth.PhoneNumber;

            if (_HHealth.Height != null)
                txtHeight.Text = _HHealth.Height;

            if (_HHealth.DoctorEmail != null)
                txtDoctorEmail.Text = _HHealth.DoctorEmail;

            if (_HHealth.OngoingTreatment.ToString() == "True")
            {
                txtHospitalClinic.Show();
                txtHospitalAddress.Show();
                txtDoctorName.Show();
                txtPhoneNo.Show();
                txtDoctorEmail.Show();
            }
            else
            {
                txtHospitalClinic.Hide();
                txtHospitalAddress.Hide();
                txtDoctorName.Hide();
                txtPhoneNo.Hide();
                txtDoctorEmail.Hide();
            }

            btnSave.Text = Resources.Messages.Update;
            this.AEHealthWindow.Show();
        }
        public void cmbOngoingTreatment_Change(object sender, DirectEventArgs e)
        {
            //if (cmbOngoingTreatment.SelectedItem.Text == "Yes")
            //    txtHospitalClinic.Show();
            //else
            //    txtHospitalClinic.Hide();
            if (cmbOngoingTreatment.SelectedItem.Text == "Yes")
            {
                txtHospitalClinic.Hidden = false;
                txtHospitalAddress.Hidden = false;
                txtDoctorName.Hidden = false;
                txtPhoneNo.Hidden = false;
            }
            else
            {
                txtHospitalClinic.Hidden = true;
                txtHospitalAddress.Hidden = true;
                txtDoctorName.Hidden = true;
                txtPhoneNo.Hidden = true;
            }
        }
        public void cmbHandicapped_Change(object sender, DirectEventArgs e)
        {
            if (cmbHandicapped.SelectedItem.Text == "Yes")
            {
                txtHandicappedDetail.Show();
                lblreftxtHandicappedDetail.Hide();
            }

            else
            {
                txtHandicappedDetail.Hide();
                txtHandicappedDetail.Text = "";
                lblreftxtHandicappedDetail.Text = "";
                lblreftxtHandicappedDetail.Show();
            }
        }







    }
}