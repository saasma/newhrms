﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HobbyCtrl.ascx.cs" Inherits="Web.NewHR.UserControls.HobbyCtrl" %>


<script type="text/javascript">

    var prepareHobby = function (grid, toolbar, rowIndex, record) {
        var editDelBtn = toolbar.items.get(1);
        if (record.data.IsEditable == 0) {
            editDelBtn.setVisible(false);
        }

    };
    
</script>

<ext:Hidden runat="server" ID="hdnHobbyId">
</ext:Hidden>

<div class="widget-body">
    <table class="fieldTable firsttdskip">
        <tr>
            <td>
                <ext:GridPanel ID="gridHobbies" runat="server" Width='830px' Cls="itemgrid">
                    <Store>
                        <ext:Store ID="storeHobbies" runat="server">
                            <Model>
                                <ext:Model ID="Model1" runat="server" IDProperty="HobbyId">
                                    <Fields>
                                        <ext:ModelField Name="HobbyId" Type="String" />
                                        <ext:ModelField Name="HobbyTypeId" Type="String" />
                                        <ext:ModelField Name="HobbyName" Type="string" />
                                        <ext:ModelField Name="InvolvedFrom" Type="string" />
                                        <ext:ModelField Name="Description" Type="string" />
                                        <ext:ModelField Name="IsEditable" Type="Int" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                    </Store>
                    <ColumnModel>
                        <Columns>                            
                            <ext:Column ID="Column1" Width="200" runat="server" Text="Hobby Name" DataIndex="HobbyName" Wrap="true" />
                            <ext:Column ID="Column2" Width="150" runat="server" Text="Involved From" DataIndex="InvolvedFrom">
                            </ext:Column>
                            <ext:Column ID="Column3" Width="400" runat="server" Text="Description" DataIndex="Description" Wrap="true">
                            </ext:Column>
                          
                       <ext:CommandColumn ID="CommandColumn21" runat="server" Width="40">
                                <Commands>
                                    <ext:CommandSeparator />
                                    <ext:GridCommand Cls="editGridButton" ToolTip-Text="Edit"  Text="<i class='fa fa-pencil'></i>" CommandName="Edit" />
                                    
                                </Commands>
                                <DirectEvents>
                                    <Command OnEvent="gridHobbies_Command">
                                        <EventMask ShowMask="true">
                                        </EventMask>
                                        <ExtraParams>
                                            <ext:Parameter Name="ID" Value="record.data.HobbyId" Mode="Raw">
                                            </ext:Parameter>
                                            <ext:Parameter Name="command" Value="command" Mode="Raw">
                                            </ext:Parameter>
                                        </ExtraParams>
                                    </Command>
                                </DirectEvents>
                                <PrepareToolbar Fn="prepareHobby" />
                            </ext:CommandColumn>
                            <ext:CommandColumn ID="CommandColumn2" runat="server" Width="40">
                                <Commands>
                                    <ext:CommandSeparator />
                                    <ext:GridCommand Cls="editGridButton" ToolTip-Text="Delete"  Text="<i class='fa fa-trash-o'></i>"
                                        CommandName="Delete" />
                                </Commands>
                                <DirectEvents>
                                    <Command OnEvent="gridHobbies_Command">
                                        <EventMask ShowMask="true">
                                        </EventMask>
                                        <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the record?" />
                                        <ExtraParams>
                                            <ext:Parameter Name="ID" Value="record.data.HobbyId" Mode="Raw">
                                            </ext:Parameter>
                                            <ext:Parameter Name="command" Value="command" Mode="Raw">
                                            </ext:Parameter>
                                        </ExtraParams>
                                    </Command>
                                </DirectEvents>
                                <PrepareToolbar Fn="prepareHobby" />
                            </ext:CommandColumn>
                        </Columns>
                    </ColumnModel>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                    </SelectionModel>
                </ext:GridPanel>
            </td>
        </tr>
    </table>
    <div class="buttonBlock" runat="server" id="buttonBlock">
       <ext:Button runat="server" Cls="btn btn-primary" Width="120" StyleSpec="margin-top:10px" Height="30"
            ID="btnAddNewLine" Text="<i></i>Add New Line">
            <DirectEvents>
                <Click OnEvent="btnAddNewLine_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:Button>
    </div>
</div>

<ext:Window ID="WHobby" runat="server" Title="Add/Edit Hobby" Icon="Application"
    Height="320" Width="430" BodyPadding="5" Hidden="true" Modal="true">
    <Content>
        <table class="fieldTable">
            <tr>
                <td>
                    <ext:ComboBox ID="cmbHobby" runat="server" ValueField="HobbyTypeId" DisplayField="HobbyName" FieldLabel="Hobby *"
                        LabelAlign="top" LabelSeparator="" ForceSelection="true" QueryMode="Local">
                        <Store>
                            <ext:Store ID="Store1" runat="server">
                                <Model>
                                    <ext:Model ID="Model3" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="HobbyTypeId" Type="String" />
                                            <ext:ModelField Name="HobbyName" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                    <asp:RequiredFieldValidator Display="None" ID="rfvHobby" runat="server"
                        ValidationGroup="SaveUpdateHobby" ControlToValidate="cmbHobby" ErrorMessage="Hobby is required." />
                </td>
               
           
                <td>
                    <pr:CalendarExtControl ID="txtInvolvedFrom" LabelSeparator="" runat="server" FieldLabel="Involved From Date"
                        Width="160" LabelAlign="Top">
                    </pr:CalendarExtControl>   
                </td>
            </tr>
          
             <td colspan="2">
                    <ext:TextArea ID="txtDescription" Width="330" runat="server" FieldLabel="Description" LabelAlign="Top"
                        LabelSeparator="" />
                </td>

    
            <tr>
                <td valign="bottom" colspan="2">
                    <div class="popupButtonDiv">
                        <ext:Button runat="server" Cls="btn btn-primary" ID="btnSave" Text="<i></i>Save">
                            <DirectEvents>
                                <Click OnEvent="btnSave_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                            <Listeners>
                                <Click Handler="valGroup = 'SaveUpdateHobby';  if(CheckValidation()) return this.disable(); else return false;">
                                </Click>
                            </Listeners>
                        </ext:Button>
                        <div class="btnFlatOr">
                            or</div>
                        <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="LinkButton2" Cls="btnFlatLeftGap"
                            Text="<i></i>Cancel">
                            <Listeners>
                                <Click Handler="#{WHobby}.hide();">
                                </Click>
                            </Listeners>
                        </ext:LinkButton>
                    </div>
                </td>
            </tr>
        </table>
    </Content>
</ext:Window>