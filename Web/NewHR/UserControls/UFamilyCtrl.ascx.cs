﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;

namespace Web.NewHR.UserControls
{
    public partial class UFamilyCtrl : BaseUserControl
    {
        private bool _isDisplayMode = false;
        public bool IsDisplayMode
        {
            set
            {
                if (value == true)
                {
                    _isDisplayMode = value;
                    btnAddLevel.Visible = false;
                    CommandColumn1.Visible = false;
                    GridLevels.Width = 920;
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                HideButtonsAndGridColumns();
                CheckPermission();
                Initialise();               
            }
        }

        public void Initialise()
        {
            LoadLevels();
        }


        public int GetEmployeeId()
        {
            int employeeId = 0;

            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                employeeId = int.Parse(Request.QueryString["ID"]);
            }
            else
            {
                employeeId = SessionManager.CurrentLoggedInEmployeeId;
            }

            return employeeId;
        }

        private void LoadLevels()
        {
            List<DAL.HFamily> list = NewPayrollManager.GetAllFamilyList(GetEmployeeId());

            if (SessionManager.CurrentLoggedInEmployeeId == 0)
                list = list.Where(x => x.Status == (int)HRStatusEnum.Approved).ToList();

            if (Request.QueryString["ID"] != null)
            {
                if (int.Parse(Request.QueryString["ID"].ToString()) != SessionManager.CurrentLoggedInEmployeeId)
                    list = list.Where(x => x.Status == (int)HRStatusEnum.Approved).ToList();
            }

            GridLevels.Store[0].DataSource = list;
            GridLevels.Store[0].DataBind();

            if (_isDisplayMode && list.Count <= 0)
                GridLevels.Hide();
            else
                GridLevels.Show();
        }

        protected void btnAddLevel_Click(object sender, DirectEventArgs e)
        {
            ucFamily.ClearLevelFields();
            Ext.Net.Button btnSave = (Ext.Net.Button)ucFamily.FindControl("btnLevelSaveUpdate");
            btnSave.Disabled = false;
            WFamily.Show();
        }


        protected void btnDeleteLevel_Click(object sender, DirectEventArgs e)
        {
            int levelId = int.Parse(hiddenValue.Text.Trim());
            Status status = NewPayrollManager.DeleteFamilyMember(levelId);
            if (status.IsSuccess)
            {
                LoadLevels();
                NewMessage.ShowNormalMessage("Person deleted.");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }

        }


        protected void btnEditLevel_Click(object sender, DirectEventArgs e)
        {
            int familyId = int.Parse(hiddenValue.Text);
            ucFamily.EditFamily(familyId);
            Ext.Net.Button btnSave = (Ext.Net.Button)ucFamily.FindControl("btnLevelSaveUpdate"); 
            btnSave.Disabled = false;
            WFamily.Show();           
        }

        protected void btnReloadGrid_Click(object sender, DirectEventArgs e)
        {
            LoadLevels();
        }

        private void HideButtonsAndGridColumns()
        {
            if (Request.QueryString["ID"] != null && SessionManager.CurrentLoggedInEmployeeId != 0)
            {
                if (SessionManager.CurrentLoggedInEmployeeId != int.Parse(Request.QueryString["ID"].ToString()))
                {
                    IsDisplayMode = true;
                }
            }
        }

        private void CheckPermission()
        {
            int employeeId = 0;
            if (Request.QueryString["ID"] != null && SessionManager.CurrentLoggedInEmployeeId != 0)
            {
                if (SessionManager.CurrentLoggedInEmployeeId != int.Parse(Request.QueryString["ID"].ToString()))
                {
                    employeeId = int.Parse(Request.QueryString["ID"]);
                    List<GetOtherEmployeeListForBranchDepartmentHeadResult> list = NewHRManager.GetOtherEmployeeListByEmpId(SessionManager.CurrentLoggedInEmployeeId);
                    bool found = false;
                    foreach (GetOtherEmployeeListForBranchDepartmentHeadResult obj in list)
                    {
                        if (obj.EmployeeId == employeeId)
                        {
                            found = true;
                            break;
                        }
                    }

                    if (found == false)
                    {
                        Response.Redirect("~/Employee/EmpNoPermissionPage.aspx");
                    }
                }
            }

        }
       
    }
}