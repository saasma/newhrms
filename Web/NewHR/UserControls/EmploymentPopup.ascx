﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmploymentPopup.ascx.cs"
    Inherits="Web.NewHR.UserControls.EmploymentPopup" %>
<ext:Hidden runat="server" ID="hdnPreviousEmploymentID">
</ext:Hidden>
<table class="fieldTable">
    <tr>
        <td>
            <ext:ComboBox ID="cmbExpCategory" runat="server" ValueField="CategoryID" DisplayField="Name"
                FieldLabel="Experience Category" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                QueryMode="Local">
                <Store>
                    <ext:Store ID="Store1" runat="server">
                        <Model>
                            <ext:Model ID="Model2" runat="server">
                                <Fields>
                                    <ext:ModelField Name="CategoryID" Type="String" />
                                    <ext:ModelField Name="Name" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
            </ext:ComboBox>
        </td>
        <td style="padding-top: 5px">
        </td>
    </tr>
    <tr>
        <td>
            <ext:TextField ID="txtOrganization" Width="180px" runat="server" FieldLabel="Organization *"
                LabelAlign="top" LabelSeparator="" />
            <asp:RequiredFieldValidator Display="None" ID="valtxtOrganization" runat="server"
                ValidationGroup="EmploymentHistorySaveUpdate" ControlToValidate="txtOrganization"
                ErrorMessage="Please enter the Organization." />
        </td>
        <td style="padding-top: 5px">
            <ext:TextField ID="txtPlace" Width="180px" runat="server" FieldLabel="Place *" LabelAlign="top"
                LabelSeparator="" />
            <asp:RequiredFieldValidator Display="None" ID="valtxtPlace" runat="server" ValidationGroup="EmploymentHistorySaveUpdate"
                ControlToValidate="txtPlace" ErrorMessage="Please type a Place." />
        </td>
    </tr>
    <tr>
        <td style="padding-top: 5px">
            <ext:TextField ID="txtPosition" Width="180px" runat="server" FieldLabel="Position *"
                LabelAlign="top" LabelSeparator="" />
            <asp:RequiredFieldValidator Display="None" ID="valtxtPosition" runat="server" ValidationGroup="EmploymentHistorySaveUpdate"
                ControlToValidate="txtPosition" ErrorMessage="Please enter Position." />
        </td>
        <td style="padding-top: 5px">
            <ext:TextField ID="txtJobResponsibility" Width="180px" runat="server" FieldLabel="Job Responsibility *"
                LabelAlign="top" LabelSeparator="" />
            <asp:RequiredFieldValidator Display="None" ID="valtxtJobResponsibility" runat="server"
                ValidationGroup="EmploymentHistorySaveUpdate" ControlToValidate="txtJobResponsibility"
                ErrorMessage="Please type the Job Responsibility." />
        </td>
    </tr>

        <tr>
        <td colspan="2">
        <ext:Checkbox ID="chkISYear" runat="server" BoxLabel="Type in Year" BoxLabelAlign="After" OnDirectCheck="ChkIsYear_Change"></ext:Checkbox>
        </td>
    </tr>
    <tr>
        <td>
            <ext:TextField ID="txtFromYear" runat="server" FieldLabel="From Year" Width="180" Hidden="true"
                LabelAlign="top" LabelSeparator="" />
        </td>
        <td>
            <ext:TextField ID="txtToYear" runat="server" FieldLabel="To Year" Width="180" LabelAlign="top"  Hidden="true"
                LabelSeparator="" />
        </td>
    </tr>

    <tr>
        <td>
            <pr:CalendarExtControl FieldLabel="From " Width="180px" ID="calFrom" runat="server"
                LabelSeparator="" LabelAlign="Top" />
            <%--<asp:RequiredFieldValidator Display="None" ID="valcalFrom" runat="server" ValidationGroup="EmploymentHistorySaveUpdate"
                        ControlToValidate="calFrom" ErrorMessage="Please enter From Date." />--%>
        </td>
        <td>
            <pr:CalendarExtControl FieldLabel="To" Width="180px" ID="calTo" runat="server" LabelSeparator=""
                LabelAlign="Top" />
            <%--<asp:RequiredFieldValidator Display="None" ID="valcalTo" runat="server" ValidationGroup="EmploymentHistorySaveUpdate"
                        ControlToValidate="calTo" ErrorMessage="Please enter To Date." />--%>
        </td>
    </tr>

    <tr>
        <td colspan="2">
            <ext:TextField ID="txtReasonforLeaving" runat="server" FieldLabel="Reason for Leaving *"
                Width="375" LabelAlign="top" LabelSeparator="" />
            <asp:RequiredFieldValidator Display="None" ID="valtxtReasonforLeaving" runat="server"
                ValidationGroup="EmploymentHistorySaveUpdate" ControlToValidate="txtReasonforLeaving"
                ErrorMessage="Please type Reason for Leaving." />
    </tr>
    <tr>
        <td colspan="2">
            <ext:TextArea ID="txtNote" runat="server" FieldStyle="height:50px!important" Width="375px"
                FieldLabel="Note" LabelAlign="Top" LabelSeparator="">
            </ext:TextArea>
    </tr>
    <tr>
        <td valign="bottom" colspan="2">
            <div class="popupButtonDiv">
                <ext:Button runat="server" ID="btnSave" Cls="btn btn-primary" Text="<i></i>Save">
                    <DirectEvents>
                        <Click OnEvent="btnSave_Click">
                            <EventMask ShowMask="true" />
                        </Click>
                    </DirectEvents>
                    <Listeners>
                        <Click Handler="valGroup = 'EmploymentHistorySaveUpdate'; if(CheckValidation()) return this.disable(); else return false;">
                        </Click>
                    </Listeners>
                </ext:Button>
                <div class="btnFlatOr">
                    or</div>
                <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="LinkButton2" Cls="btnFlatLeftGap"
                    Text="<i></i>Cancel">
                    <Listeners>
                        <Click Handler="#{AEPreviousEmploymentWindow}.hide();">
                        </Click>
                    </Listeners>
                </ext:LinkButton>
            </div>
        </td>
    </tr>
</table>
