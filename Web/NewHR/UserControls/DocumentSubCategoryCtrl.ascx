﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DocumentSubCategoryCtrl.ascx.cs" Inherits="Web.NewHR.UserControls.DocumentSubCategoryCtrl" %>

<ext:Hidden ID="hdnIsSubCategoryPage" runat="server" />
<ext:Hidden ID="hdnSubCategoryID" runat="server" />
 
<ext:Window ID="WDocumentSubCategory" runat="server" Title="Document Sub Category" Icon="Application" ButtonAlign="Left"
            Width="400" Height="280" BodyPadding="5" Hidden="true" Modal="true">
            <Content>
                <table class="fieldTable">
                    <tr>
                        <td>
                            <ext:ComboBox Width="300" LabelSeparator="" LabelAlign="Left" LabelWidth="80" MarginSpec="10 0 0 0"
                                QueryMode="Local" ID="cmbCategory" ForceSelection="true" DisplayField="Name" ValueField="CategoryID"
                                runat="server" FieldLabel="Create Under">
                                <Store>
                                    <ext:Store ID="Store3" runat="server">
                                        <Model>
                                            <ext:Model ID="Model2" IDProperty="CategoryID" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="CategoryID" Type="String" />
                                                    <ext:ModelField Name="Name" Type="String" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                            </ext:ComboBox>
                            <asp:RequiredFieldValidator Display="None" ID="rfvCategory" runat="server" ValidationGroup="SaveUpdSubCat"
                                ControlToValidate="cmbCategory" ErrorMessage="Category is required." />
                        </td>  
                    </tr> 
                    <tr>
                        <td>
                            <ext:TextField ID="txtName" LabelSeparator="" runat="server" FieldLabel="Name *"
                                LabelWidth="80" Width="300" LabelAlign="Left">
                            </ext:TextField>
                            <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator1" runat="server" ValidationGroup="SaveUpdSubCat"
                                ControlToValidate="txtName" ErrorMessage="Name is required." />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ext:TextField ID="txtCode" LabelSeparator="" runat="server" FieldLabel="Code *" MaxLength="4"
                                LabelWidth="80" Width="150" LabelAlign="Left" EnforceMaxLength="true">
                            </ext:TextField>
                            <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator2" runat="server" ValidationGroup="SaveUpdSubCat"
                                ControlToValidate="txtCode" ErrorMessage="Code is required." />
                        </td>
                    </tr>
                
                    
                   
                </table>
            </Content>
            <Buttons>
                <ext:Button runat="server" ID="btnSaveSubCat" Text="Save" MarginSpec="0 0 0 15">
                    <DirectEvents>
                        <Click OnEvent="btnSaveSubCat_Click">
                            <EventMask ShowMask="true" />
                        </Click>
                    </DirectEvents>
                    <Listeners>
                        <Click Handler="valGroup = 'SaveUpdSubCat'; if(CheckValidation()) return ''; else return false;">
                        </Click>
                    </Listeners>
                </ext:Button>

                <ext:Button runat="server" ID="Button1" Text="<i></i>Cancel" MarginSpec="0 0 0 10">
                    <Listeners>
                        <Click Handler="#{WDocumentSubCategory}.hide();">
                        </Click>
                    </Listeners>
                </ext:Button>
            </Buttons>
        </ext:Window>