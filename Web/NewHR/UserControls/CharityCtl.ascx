﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CharityCtl.ascx.cs"
    Inherits="Web.NewHR.UserControls.CharityCtl" %>
<%@ Register Src="~/newhr/UserControls/EmployeeWizard.ascx" TagName="EmployeeWizard"
    TagPrefix="uc2" %>
<%@ Register Src="~/NewHR/UserControls/EmployeeDetailsCtl.ascx" TagName="EmployeeDetailsCtl"
    TagPrefix="uc1" %>
<script type="text/javascript">


   
     

        var CommandHandler = function(command, record){
        <%= hdn.ClientID %>.setValue(record.data.CharityID);
            if(command=="Edit")
            {
                <%= btnEdit.ClientID %>.fireEvent('click');
            }
            else
            {
                <%= btnDelete.ClientID %>.fireEvent('click');
            }

            }
       
      

</script>
<ext:LinkButton runat="server" Hidden="true" ID="btnEdit" runat="server">
    <DirectEvents>
        <Click OnEvent="btnEditLevel_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:LinkButton>
<ext:Button ID="btnDelete" runat="server" Hidden="true">
    <DirectEvents>
        <Click OnEvent="btnDeleteLevel_Click">
            <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the Charity?" />
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:Button>
<ext:Hidden runat="server" ID="hdn">
</ext:Hidden>
<div class="pageheader">
    <div class="media">
        <div class="media-body">
            <h4>
                Employee Charity List
            </h4>
        </div>
    </div>
</div>
<div class="contentpanel" style="padding-top:0px">
    <ext:Label ID="lblMsg" runat="server" />
    <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
        <Proxy>
            <ext:AjaxProxy Url="../../Handler/EmpSearch.ashx">
                <ActionMethods Read="GET" />
                <Reader>
                    <ext:JsonReader Root="plants" TotalProperty="total" />
                </Reader>
            </ext:AjaxProxy>
        </Proxy>
        <Model>
            <ext:Model ID="Model2" IDProperty="Name" Name="ItemLineModel" runat="server">
                <Fields>
                    <ext:ModelField Name="Name" Type="String" />
                    <ext:ModelField Name="EmployeeId" Type="String" />
                </Fields>
            </ext:Model>
        </Model>
    </ext:Store>
    <table class="fieldTable firsttdskip" runat="server" id="tbl">
        <tr>
            <td>
                <ext:ComboBox ID="cmbSearch" FieldLabel="Search Employee" LabelWidth="70" LabelAlign="Top"
                    runat="server" DisplayField="Name" ValueField="EmployeeId" StoreID="storeSearch"
                    TypeAhead="false" Width="180" PageSize="9999" HideBaseTrigger="true" MinChars="1"
                    TriggerAction="All" ForceSelection="true">
                    <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                        <ItemTpl ID="ItemTpl1" runat="server">
                            <Html>
                                <tpl>
                                            <div class="search-item">
                                                            <span>{Name}</span>  
                                             </div>
					                </tpl>
                            </Html>
                        </ItemTpl>
                    </ListConfig>
                    <Triggers>
                        <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                    </Triggers>
                    <Listeners>
                        <Select Handler="this.getTrigger(0).show();" />
                        <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                        <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                    </Listeners>
                </ext:ComboBox>
            </td>
            <td style="padding-top: 20px">
                <ext:Button runat="server" ID="btnLoad" Cls="btn btn-primary" Text="<i></i>Load"
                    runat="server">
                    <Listeners>
                        <Click Handler="searchList();" />
                    </Listeners>
                </ext:Button>
            </td>
        </tr>
    </table>
   
    <ext:GridPanel StyleSpec="margin-top:5px;" ID="gridRewardList" runat="server" Cls="itemgrid">
        <Store>
            <ext:Store ID="gridRewardStore" PageSize="50" runat="server">
                <Model>
                    <ext:Model Root="data" IDProperty="CharityID">
                        <Fields>
                            <ext:ModelField Name="CharityID" Type="string" />
                            <ext:ModelField Name="Name" Type="string" />
                            <ext:ModelField Name="CharityDate" Type="string" />
                            <ext:ModelField Name="Notes" Type="string" />
                            <ext:ModelField Name="Amount" Type="string" />
                            <ext:ModelField Name="Institution" Type="string" />
                        </Fields>
                    </ext:Model>
                </Model>
            </ext:Store>
        </Store>
        <ColumnModel>
            <Columns>
                <ext:Column ID="Column_Employee" Sortable="true" runat="server" Text="Employee" Align="Left"
                    Width="150" DataIndex="Name" Hideable="false" />
                <ext:Column ID="Column8" Sortable="false" MenuDisabled="true" runat="server" Text="Date"
                    Align="Left" Width="90" DataIndex="CharityDate" />
                <ext:Column ID="Column2" Width="200" Sortable="false" MenuDisabled="true" runat="server"
                    Text="Institution" Align="Left" DataIndex="Institution">
                </ext:Column>
                <ext:Column ID="Column6" Width="200" Sortable="false" MenuDisabled="true" runat="server"
                    Text="Notes" Align="Left" DataIndex="Notes">
                </ext:Column>
                <ext:Column ID="Column3" Width="100" Sortable="false" MenuDisabled="true" runat="server"
                    Text="Amount" Align="Right" DataIndex="Amount">
                    <Renderer Fn="getFormattedAmount" />
                </ext:Column>
                <ext:CommandColumn ID="CommandColumn1" runat="server" Width="75" Text="Actions" Align="Center">
                    <Commands>
                        <ext:CommandSeparator />
                        <ext:GridCommand Text="<i></i>" Cls="btn-action glyphicons pencil btn-success" CommandName="Edit" />
                        <%-- <ext:GridCommand Text="<i></i>" Cls="btn-action glyphicons remove_2 btn-danger" CommandName="Delete" />--%>
                    </Commands>
                    <Listeners>
                        <Command Handler="CommandHandler(command,record);" />
                    </Listeners>
                </ext:CommandColumn>
            </Columns>
        </ColumnModel>
        <View>
            <ext:GridView ID="GridView1" runat="server">
                <%--<GetRowClass Fn="getRowClass" />--%>
            </ext:GridView>
        </View>
        <SelectionModel>
            <ext:RowSelectionModel ID="RowSelectionModel2111" runat="server" Mode="Single" />
        </SelectionModel>
    </ext:GridPanel>
    <div class="buttonBlock">
        <div class="left">
            <ext:LinkButton runat="server" StyleSpec="padding:0px;margin-top:20px;" ID="btnAdd"
                Cls="btnFlat" BaseCls="btnFlat" Text="<i></i>Add Charity" runat="server">
                <DirectEvents>
                    <Click OnEvent="btnAdd_Click">
                        <EventMask ShowMask="true" />
                    </Click>
                </DirectEvents>
            </ext:LinkButton>
        </div>
    </div>
    <div style="clear: both">
    </div>
</div>
<ext:Window ID="window" runat="server" Title="Add Charity" Icon="Application" Height="450"
    Width="500" BodyPadding="5" Hidden="true" Modal="true">
    <Content>
        <table class="fieldTable">
            <tr>
                <td runat="server" id="employeeTD">
                    <ext:ComboBox ID="cmbGradeEmployee" LabelSeparator="" FieldLabel="Employee" LabelWidth="70"
                        LabelAlign="Top" runat="server" DisplayField="Name" ValueField="EmployeeId" StoreID="storeSearch"
                        TypeAhead="false" Width="180" PageSize="9999" HideBaseTrigger="true" MinChars="1"
                        TriggerAction="All" ForceSelection="true">
                        <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                            <ItemTpl ID="ItemTpl2" runat="server">
                                <Html>
                                    <tpl>
                                            <div class="search-item">
                                                            <span>{Name}</span>  
                                             </div>
					                </tpl>
                                </Html>
                            </ItemTpl>
                        </ListConfig>
                    </ext:ComboBox>
                    <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator3" runat="server"
                        ValidationGroup="SaveUpdate" ControlToValidate="cmbGradeEmployee" ErrorMessage="Employee is required." />
                </td>
            </tr>
            <tr>
                <td>
                    <ext:DateField ID="date" runat="server" FieldLabel="Date" LabelAlign="Top" LabelSeparator=""
                        Width="180px">
                    </ext:DateField>
                    <asp:RequiredFieldValidator Display="None" ID="valcalStartDate" runat="server" ValidationGroup="SaveUpdate"
                        ControlToValidate="date" ErrorMessage="Date is required." />
                </td>
            </tr>
            <tr>
                <td>
                    <ext:TextField FieldLabel="Amount" Width="180px" ID="txtAmount" runat="server" LabelSeparator=""
                        LabelAlign="Top" />
                    <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator1" runat="server"
                        ValidationGroup="SaveUpdate" ControlToValidate="txtAmount" ErrorMessage="Amount is required." />
                    <asp:CompareValidator Display="None" ID="RequiredFieldValidator2" runat="server"
                        Type="Currency" Operator="GreaterThan" ValueToCompare="0" ValidationGroup="SaveUpdate"
                        ControlToValidate="txtAmount" ErrorMessage="Invalid Amount." />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <ext:TextField ID="txtInstitution" runat="server" FieldLabel="Institution" LabelSeparator=""
                        LabelAlign="Top" Width="300px" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <ext:TextArea ID="txtNotes" runat="server" FieldLabel="Notes" LabelSeparator="" LabelAlign="Top"
                        Rows="3" Cols="55" />
                </td>
            </tr>
            <tr>
                <td valign="bottom" colspan="2">
                    <div class="popupButtonDiv">
                        <ext:Button  runat="server" ID="btnSave" Cls="btn btn-primary" Text="<i></i>Save">
                            <DirectEvents>
                                <Click OnEvent="btnSave_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                            <Listeners>
                                <Click Handler="valGroup = 'SaveUpdate'; return CheckValidation();">
                                </Click>
                            </Listeners>
                        </ext:Button>
                        <div class="btnFlatOr">
                            or</div>
                        <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="LinkButton2" Cls="btnFlatLeftGap"
                            Text="<i></i>Cancel">
                            <Listeners>
                                <Click Handler="#{window}.hide();">
                                </Click>
                            </Listeners>
                        </ext:LinkButton>
                    </div>
                </td>
            </tr>
        </table>
    </Content>
</ext:Window>
