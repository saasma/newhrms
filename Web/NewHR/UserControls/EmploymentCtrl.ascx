﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmploymentCtrl.ascx.cs"
    Inherits="Web.NewHR.UserControls.EmploymentCtrl" %>
<%@ Register Src="~/NewHR/UserControls/EmploymentPopup.ascx" TagName="EmplmentPopUp"
    TagPrefix="uc" %>
<script type="text/javascript">

     var prepareEmpHistory = function (grid, toolbar, rowIndex, record) {

         var btnEditDelete = toolbar.items.get(0);       

         if (record.data.IsEditable == 0) {
             btnEditDelete.setVisible(false);
         }

     }

     function reloadEmpGrid() {
        <%= btnReloadGrid.ClientID %>.fireEvent('click');
     }

      



</script>

<ext:Hidden runat="server" ID="hdnPreviousEmploymentID">
</ext:Hidden>
<ext:LinkButton ID="btnReloadGrid" runat="server" Hidden="true">
    <DirectEvents>
        <Click OnEvent="btnReloadGrid_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:LinkButton>
<table class="fieldTable firsttdskip">
    <tr>
        <td>
            <ext:GridPanel ID="GridPreviousEmployment" runat="server" Width="1000" Cls="itemgrid" TrackMouseOver="true" AutoExpandColumn="Organization"
                Scroll="None">
                <Store>
                    <ext:Store ID="StorePreviousEmployment" runat="server">
                        <Model>
                            <ext:Model ID="Model1" runat="server" IDProperty="PrevEmploymentId">
                                <Fields>
                                    <ext:ModelField Name="Category" Type="String" />
                                    <ext:ModelField Name="Organization" Type="String" />
                                    <ext:ModelField Name="Place" Type="string" />
                                    <ext:ModelField Name="Position" Type="string" />
                                    <ext:ModelField Name="JobResponsibility" Type="string" />
                                    <ext:ModelField Name="IsEditable" Type="Int" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:Column ID="Column5" Width="160" runat="server" Text="Experience Category" Wrap="true"
                            DataIndex="Category" />
                        <ext:Column ID="Column1" Width="180" runat="server" Text="Organization" DataIndex="Organization" Wrap="true" />
                        <ext:Column ID="Column2" runat="server" Width="160" Text="Place" DataIndex="Place" Wrap="true">
                        </ext:Column>
                        <ext:Column ID="Column3" runat="server" Width="160" Text="Position" DataIndex="Position" Wrap="true">
                        </ext:Column>
                        <ext:Column ID="Column4" runat="server" Text="Job Responsibility" DataIndex="JobResponsibility" Wrap="true"
                            Width="260">
                        </ext:Column>
                        <ext:CommandColumn ID="CommandColumn2" runat="server" Width="40">
                            <Commands>                            
                                <ext:GridCommand Cls="editGridButton" ToolTip-Text="Edit"  Text="<i class='fa fa-pencil'></i>" CommandName="Edit" />
                            </Commands>
                            <DirectEvents>
                                <Command OnEvent="GridPreviousEmployment_Command">
                                    <EventMask ShowMask="true">
                                    </EventMask>
                                    <ExtraParams>
                                        <ext:Parameter Name="ID" Value="record.data.PrevEmploymentId" Mode="Raw">
                                        </ext:Parameter>
                                        <ext:Parameter Name="command" Value="command" Mode="Raw">
                                        </ext:Parameter>
                                    </ExtraParams>
                                </Command>
                            </DirectEvents>
                            <PrepareToolbar Fn="prepareEmpHistory" />
                        </ext:CommandColumn>
                        <ext:CommandColumn ID="CommandColumn1" runat="server" Width="40">
                            <Commands>
                                <ext:GridCommand Cls="editGridButton" ToolTip-Text="Delete"  Text="<i class='fa fa-trash-o'></i>"
                                CommandName="Delete" />                             
                            </Commands>
                            <DirectEvents>
                                <Command OnEvent="GridPreviousEmployment_Command">
                                    <EventMask ShowMask="true">
                                    </EventMask>
                                    <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the record?" />
                                    <ExtraParams>
                                        <ext:Parameter Name="ID" Value="record.data.PrevEmploymentId" Mode="Raw">
                                        </ext:Parameter>
                                        <ext:Parameter Name="command" Value="command" Mode="Raw">
                                        </ext:Parameter>
                                    </ExtraParams>
                                </Command>
                            </DirectEvents>
                            <PrepareToolbar Fn="prepareEmpHistory" />
                        </ext:CommandColumn>
                        
                    </Columns>
                </ColumnModel>
                <SelectionModel>
                    <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                </SelectionModel>
            </ext:GridPanel>
            <ext:ToolTip 
            ID="RowTip" 
            runat="server" 
            Target="#{GridPreviousEmployment.getView().mainBody}"
            Delegate=".x-grid3-cell"
            TrackMouse="true">
            <Listeners>
                <Show Fn="showTip" />
            </Listeners>
        </ext:ToolTip>
        </td>
    </tr>
</table>
<div class="buttonBlockSection">
    <ext:Button runat="server" ID="LinkButton1" Cls="btn btn-primary btn-sect" runat="server" Width="120" StyleSpec="margin-top:10px" Height="30"
         Text="<i></i>Add New Line">
        <DirectEvents>
            <Click OnEvent="btnAddNewLine_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>
</div>
<ext:Window ID="AEPreviousEmploymentWindow" runat="server" Title="Previous Employment Details"
    Icon="Application" Height="550" Width="460" BodyPadding="5" Hidden="false" Modal="false" X="-1000" Y="-1000">
    <Content>
        <uc:EmplmentPopUp Id="EmplymntCtrl" runat="server" />
    </Content>
</ext:Window>


<script type="text/javascript">

    var showTip = function () {
        var rowIndex = GridPreviousEmployment.view.findRowIndex(this.triggerElement),
                cellIndex = GridPreviousEmployment.view.findCellIndex(this.triggerElement),
                record = StorePreviousEmployment.getAt(rowIndex),
                fieldName = GridPreviousEmployment.getColumnModel().getDataIndex(cellIndex),
                data = record.get(fieldName);

        this.body.dom.innerHTML = data;
    };

</script>
