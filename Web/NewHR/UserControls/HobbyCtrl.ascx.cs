﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;
using BLL.Base;
using Ext.Net;

namespace Web.NewHR.UserControls
{
    public partial class HobbyCtrl : BaseUserControl
    {
        public bool ShowEmpty = false;

        private bool _isDisplayMode = false;
        public bool IsDisplayMode
        {
            set
            {
                if (value == true)
                {
                    _isDisplayMode = value;
                    btnAddNewLine.Visible = false;
                    WHobby.Visible = false;
                    CommandColumn21.Visible = false;
                    CommandColumn2.Visible = false;
                    gridHobbies.Width = 920;
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        private void HideButtonBlock()
        {
            buttonBlock.Visible = false;
        }

        private void BindHobbyCombo()
        {
            cmbHobby.Store[0].DataSource = ListManager.GetHobbyTpyeList();
            cmbHobby.Store[0].DataBind();
        }

        private void Initialise()
        {
            ClearFields();
            BindHobbyCombo();
            BindGrid();
        }

        private void ClearFields()
        {
            cmbHobby.Text = "";
            txtInvolvedFrom.Text = "";
            txtDescription.Text = "";
            btnSave.Text = Resources.Messages.Save;
        }

        private void BindGrid()
        {
            int EmployeeID = GetEmployeeID();
            List<EHobby> list = NewHRManager.GetEHobbyByEmployeeId(EmployeeID);

            if (list.Count > 0)
            {
                gridHobbies.Store[0].DataSource = list;
                gridHobbies.Store[0].DataBind();
                gridHobbies.Show();
            }
            else
                gridHobbies.Hide();
        }

        protected void btnAddNewLine_Click(object sender, DirectEventArgs e)
        {
            ClearFields();
            hdnHobbyId.Text = "";
            WHobby.Show();
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveUpdateHobby");
            if (Page.IsValid)
            {
                EHobby obj = new EHobby();

                if (!string.IsNullOrEmpty(hdnHobbyId.Text))
                    obj.HobbyId = int.Parse(hdnHobbyId.Text);

                obj.EmployeeId = GetEmployeeID();
                obj.HobbyTypeId = int.Parse(cmbHobby.SelectedItem.Value);

                if (!string.IsNullOrEmpty(txtInvolvedFrom.Text.Trim()))
                {
                    obj.InvolvedFrom = txtInvolvedFrom.Text.Trim();
                    obj.InvolvedFromEng = BLL.BaseBiz.GetEngDate(obj.InvolvedFrom, IsEnglish);
                }

                if (!string.IsNullOrEmpty(txtDescription.Text.Trim()))
                    obj.Description = txtDescription.Text.Trim();

                Status status = NewHRManager.SaveUpdateEHobby(obj);
                if (status.IsSuccess)
                {
                    WHobby.Close();

                    if (string.IsNullOrEmpty(hdnHobbyId.Text))
                        NewMessage.ShowNormalMessage("Record saved successfully.");
                    else
                        NewMessage.ShowNormalMessage("Record updated successfully.");

                    BindGrid();
                }
                else
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
            }

        }

        protected int GetEmployeeID()
        {
            int EmpID = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                EmpID = int.Parse(Request.QueryString["ID"]);
            }
            else
            {
                EmpID = SessionManager.CurrentLoggedInEmployeeId;
            }
            return EmpID;
        }


        protected void gridHobbies_Command(object sender, DirectEventArgs e)
        {
            string commandName = e.ExtraParams["command"];
            int hobbyId = int.Parse(e.ExtraParams["ID"]);

            switch (commandName)
            {
                case "Delete":
                    this.DeleteHobby(hobbyId);
                    break;
                case "Edit":
                    this.EditEHobby(hobbyId);
                    break;
            }

        }

        private void DeleteHobby(int hobbyId)
        {
            Status status = NewHRManager.DeleteEHobby(hobbyId);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Record deleted successfully.");
                BindGrid();
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        private void EditEHobby(int hobbyId)
        {
            EHobby obj = NewHRManager.GetEHobbyById(hobbyId);
            if (obj != null)
            {
                cmbHobby.SetValue(obj.HobbyTypeId.ToString());

                if (!string.IsNullOrEmpty(obj.InvolvedFrom))
                    txtInvolvedFrom.Text = obj.InvolvedFrom;

                if (obj.Description != null)
                    txtDescription.Text = obj.Description;

                hdnHobbyId.Text = obj.HobbyId.ToString();
                btnSave.Text = Resources.Messages.Update;
                WHobby.Show();
            }

        }


    }
}