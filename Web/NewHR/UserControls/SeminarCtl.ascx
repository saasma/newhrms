﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SeminarCtl.ascx.cs"
    Inherits="Web.NewHR.UserControls.SeminarCtl" %>
<script type="text/javascript">
    var renderDuration = function (value, meta, record) {

        return record.data.Duration + " (" + record.data.DurationTypeName + ")";
    }

    var prepare = function (grid, toolbar, rowIndex, record) {

        btnEdit = toolbar.items.get(0);
        btnDelete = toolbar.items.get(2);

        if (record.data.IsEditable == 0) {
            btnEdit.setVisible(false);
            btnDelete.setVisible(false);
        }
    }



</script>
<ext:Hidden runat="server" ID="hdnSeminarID">
</ext:Hidden>

    <div class="widget-body">
        <table class="fieldTable firsttdskip">
            <tr>
                <td>
                    <ext:GridPanel ID="GridSeminar" runat="server" Width="900" Cls="itemgrid">
                        <Store>
                            <ext:Store ID="StoreSeminar" runat="server">
                                <Model>
                                    <ext:Model ID="Model1" runat="server" IDProperty="SeminarId">
                                        <Fields>
                                            <ext:ModelField Name="Organizer" Type="string" />
                                            <ext:ModelField Name="Country" Type="String" />
                                            <ext:ModelField Name="Place" Type="string" />
                                            <ext:ModelField Name="Duration" Type="string" />
                                            <ext:ModelField Name="DurationTypeName" Type="string" />
                                            <ext:ModelField Name="StartDate" Type="string" />
                                            <ext:ModelField Name="EndDate" Type="string" />
                                            <ext:ModelField Name="IsEditable" Type="Int" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:Column ID="Column1" runat="server" Text="Organizer" DataIndex="Organizer" Flex="1" />
                                <ext:Column ID="Column2" runat="server" Text="Country" DataIndex="Country">
                                </ext:Column>
                                <ext:Column ID="Column3" runat="server" Text="Place" DataIndex="Place">
                                </ext:Column>
                                <ext:Column ID="Column4" runat="server" Text="Duration" DataIndex="Duration">
                                    <Renderer Fn="renderDuration" />
                                </ext:Column>
                                <ext:Column ID="Column6" runat="server" Text="Start Date" DataIndex="StartDate">
                                </ext:Column>
                                <ext:Column ID="Column5" runat="server" Text="End Date" DataIndex="EndDate">
                                </ext:Column>
                                <ext:CommandColumn ID="CommandColumn1" runat="server" Width="110">
                                    <Commands>
                                        <ext:GridCommand Text="<i></i>" Cls="btn-action glyphicons pencil btn-success" CommandName="Edit">
                                            <ToolTip Text="Edit" />
                                        </ext:GridCommand>
                                        <ext:CommandSeparator />
                                        <ext:GridCommand Text="<i></i>" Cls="btn-action glyphicons remove_2 btn-danger" CommandName="Delete">
                                            <ToolTip Text="Delete" />
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridSeminar_Command">
                                            <EventMask ShowMask="true">
                                            </EventMask>
                                            <ExtraParams>
                                                <ext:Parameter Name="ID" Value="record.data.SeminarId" Mode="Raw">
                                                </ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw">
                                                </ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                     <PrepareToolbar Fn="prepare" />
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <SelectionModel>
                            <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                        </SelectionModel>
                    </ext:GridPanel>
                </td>
            </tr>
        </table>
        <div class="buttonBlock">
            <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="btnAddNewLine" Cls="btnFlat"
                BaseCls="btnFlat" Text="<i></i>Add New Line">
                <DirectEvents>
                    <Click OnEvent="btnAddNewLine_Click">
                        <EventMask ShowMask="true" />
                    </Click>
                </DirectEvents>
            </ext:LinkButton>
        </div>
    </div>

<ext:Window ID="AESeminarWindow" runat="server" Title="Add/Edit Seminar" Icon="Application"
    Height="450" Width="650"  BodyPadding="5"
    Hidden="true" Modal="true">
    <Content>
        <table class="fieldTable">
            <tr>
                <td>
                    <ext:TextField ID="txtOrganizer" Width="180" runat="server" FieldLabel="Organizer" LabelAlign="top"
                        LabelSeparator="" />
                    <asp:RequiredFieldValidator Display="None" ID="valtxtOrganizer" runat="server" ValidationGroup="SeminarSaveUpdate"
                        ControlToValidate="txtOrganizer" ErrorMessage="Organizer is required." />
                </td>
            </tr>
            <tr>
                <td>
                    <table class="fieldTable firsttdskip">
                        <tr>
                            <td>
                                <ext:ComboBox ID="cmbCountry"  Width="180" runat="server" ValueField="CountryName" DisplayField="CountryName"
                                    FieldLabel="Country" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                                    QueryMode="Local">
                                    <Store>
                                        <ext:Store ID="Store2" runat="server">
                                            <Model>
                                                <ext:Model ID="Model4" runat="server">
                                                    <Fields>
                                                        <ext:ModelField Name="CountryName" />
                                                        <ext:ModelField Name="CountryName" />
                                                    </Fields>
                                                </ext:Model>
                                            </Model>
                                        </ext:Store>
                                    </Store>
                                </ext:ComboBox>
                                <asp:RequiredFieldValidator Display="None" ID="valcmbCountry" runat="server" ValidationGroup="SeminarSaveUpdate"
                                    ControlToValidate="cmbCountry" ErrorMessage="Country is required." />
                            </td>
                            <td>
                                <ext:TextField ID="txtPlace"  Width="180" runat="server" FieldLabel="Place" LabelAlign="top" LabelSeparator="" />
                                <asp:RequiredFieldValidator Display="None" ID="valtxtPlace" runat="server" ValidationGroup="SeminarSaveUpdate"
                                    ControlToValidate="txtPlace" ErrorMessage="Place is required." />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <table class="fieldTable firsttdskip">
                        <tr>
                            <td>
                                <ext:TextField ID="txtDuration" runat="server" FieldLabel="Duration" LabelAlign="top"
                                    Width="85" LabelSeparator="" MaskRe="[0-9]|\.|%" />
                                <asp:RequiredFieldValidator Display="None" ID="valtxtDuration" runat="server" ValidationGroup="SeminarSaveUpdate"
                                    ControlToValidate="txtDuration" ErrorMessage="Duration  is required." />
                            </td>
                            <td style="padding-top: 5px">
                                <ext:ComboBox ID="cmbDurationType" runat="server" ValueField="Value" DisplayField="Text"
                                    Width="80" FieldLabel="&nbsp" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                                    QueryMode="Local">
                                    <Items>
                                        <ext:ListItem Text="Year" Value="1" />
                                        <ext:ListItem Text="Month" Value="2" />
                                        <ext:ListItem Text="Week" Value="3" />
                                        <ext:ListItem Text="Day" Value="4" />
                                    </Items>
                                    <%-- <Store>
                                            <ext:Store ID="Store1" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model2" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="Value" />
                                                            <ext:ModelField Name="Text" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>--%>
                                </ext:ComboBox>
                                <asp:RequiredFieldValidator Display="None" ID="valcmbDurationType" runat="server"
                                    ValidationGroup="SeminarSaveUpdate" ControlToValidate="cmbDurationType" ErrorMessage="Duration Type is required." />
                            </td>
                            <td>
                                <pr:CalendarExtControl FieldLabel="Start Date" Width="180px" ID="calStartDate" runat="server"
                                    LabelSeparator="" LabelAlign="Top" />
                                <asp:RequiredFieldValidator Display="None" ID="valcalStartDate" runat="server" ValidationGroup="SeminarSaveUpdate"
                                    ControlToValidate="calStartDate" ErrorMessage="Start Date is required." />
                            </td>
                            <td>
                                <pr:CalendarExtControl FieldLabel="End Date" Width="180px" ID="calEndDate" runat="server"
                                    LabelSeparator="" LabelAlign="Top" />
                                <asp:RequiredFieldValidator Display="None" ID="valcalEndDate" runat="server" ValidationGroup="SeminarSaveUpdate"
                                    ControlToValidate="calEndDate" ErrorMessage="End Date is required." />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <ext:TextArea ID="txtNote" runat="server" FieldLabel="Notes" LabelSeparator="" LabelAlign="Top"
                        Rows="4" Cols="55" />
                </td>
            </tr>
            <tr>
                <td valign="bottom" colspan="2">
                    <div class="popupButtonDiv">
                        <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="btnSave"
                            Cls="btnFlat" BaseCls="btnFlat" Text="<i></i>Save">
                            <DirectEvents>
                                <Click OnEvent="btnSave_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                            <Listeners>
                                <Click Handler="valGroup = 'SeminarSaveUpdate'; if(CheckValidation()) return this.disable(); else return false;">
                                </Click>
                            </Listeners>
                        </ext:LinkButton>
                        <div class="btnFlatOr">
                            or</div>
                        <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="LinkButton2" Cls="btnFlatLeftGap"
                            Text="<i></i>Cancel">
                            <Listeners>
                                <Click Handler="#{AESeminarWindow}.hide();">
                                </Click>
                            </Listeners>
                        </ext:LinkButton>
                    </div>
                </td>
            </tr>
        </table>
    </Content>
</ext:Window>
