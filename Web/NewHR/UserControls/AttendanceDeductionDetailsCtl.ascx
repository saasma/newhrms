﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AttendanceDeductionDetailsCtl.ascx.cs"
    Inherits="Web.NewHR.UserControls.AttendanceDeductionDetailsCtl" %>
<script type="text/javascript">

    function searchList() {
              <%=PagingToolbar1.ClientID %>.doRefresh();
        }

 
   
    function SelEmpSearch()
    {
        <%=hdnEmpSearch.ClientID %>.setValue(<%=cmbEmpSearch.ClientID %>.getValue());
    }

</script>
<script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>
<script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>
<ext:Hidden ID="hdnEmployeeId" runat="server" />
<ext:Hidden ID="hdnBranchDepartmentId" runat="server" />
<ext:Hidden ID="hdnMsg" runat="server" />
<ext:Hidden ID="hdnEmpSearch" runat="server" />
<ext:Hidden ID="hdnSortBy" runat="server" />
<ext:Store runat="server" ID="storeSearch" AutoLoad="false">
    <Proxy>
        <ext:AjaxProxy Url="../../Handler/EmpSearchID.ashx">
            <ActionMethods Read="GET" />
            <Reader>
                <ext:JsonReader Root="plants" TotalProperty="total" />
            </Reader>
        </ext:AjaxProxy>
    </Proxy>
    <Model>
        <ext:Model ID="Model5" IDProperty="EmployeeId" Name="ItemLineModel" runat="server">
            <Fields>
                <ext:ModelField Name="Name" Type="String" />
                <ext:ModelField Name="EmployeeId" Type="String" />
            </Fields>
        </ext:Model>
    </Model>
</ext:Store>
<div class="alert alert-info" style="margin-top: 10px; padding-top: 0px!important;">
    <ext:Container ID="listingContainer" runat="server">
        <Content>
            <table>
                <tr>
                    <td>
                        <ext:ComboBox Width="100" MarginSpec="0 5 0 5" LabelSeparator="" LabelAlign="Top"
                            QueryMode="Local" ID="cmbYear" ForceSelection="true" DisplayField="Year" ValueField="Year"
                            runat="server" FieldLabel="Year">
                            <Store>
                                <ext:Store ID="Store1" runat="server">
                                    <Model>
                                        <ext:Model ID="Model2" IDProperty="Year" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="Year" Type="String" />
                                                <ext:ModelField Name="Year" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                    </td>
                    <td>
                        <ext:ComboBox Width="100" MarginSpec="0 5 0 5" LabelSeparator="" LabelAlign="Top"
                            QueryMode="Local" ID="cmbMonth" ForceSelection="true" DisplayField="Value" ValueField="Key"
                            runat="server" FieldLabel="Month">
                            <Store>
                                <ext:Store ID="Store2" runat="server">
                                    <Model>
                                        <ext:Model ID="Model3" IDProperty="Key" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="Key" Type="String" />
                                                <ext:ModelField Name="Value" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                    </td>
                    <td>
                        <ext:ComboBox Width="140" MatchFieldWidth="false" MarginSpec="0 5 0 5" LabelSeparator=""
                            LabelAlign="Top" QueryMode="Local" ID="cmbBranch" DisplayField="Name" ValueField="BranchId"
                            runat="server" FieldLabel="Branch">
                            <Items>
                                <ext:ListItem Index="0" Value="-1" Text="All">
                                </ext:ListItem>
                            </Items>
                            <Store>
                                <ext:Store ID="Store5" runat="server">
                                    <Model>
                                        <ext:Model ID="Model7" IDProperty="BranchId" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="BranchId" />
                                                <ext:ModelField Name="Name" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                    </td>
                    <td>
                        <ext:ComboBox FieldLabel="Department" MatchFieldWidth="false" ID="cmbDepartment"
                            Width="140" MarginSpec="0 5 0 5" runat="server" ValueField="DepartmentId" DisplayField="Name"
                            LabelAlign="Top" LabelSeparator="" QueryMode="Local">
                            <Items>
                                <ext:ListItem Index="0" Value="-1" Text="All">
                                </ext:ListItem>
                            </Items>
                            <Store>
                                <ext:Store ID="Store6" runat="server">
                                    <Model>
                                        <ext:Model ID="Model8" IDProperty="DepartmentId" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="DepartmentId" />
                                                <ext:ModelField Name="Name" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                    </td>
                    <td>
                        <ext:ComboBox ID="cmbLevel" MatchFieldWidth="false" runat="server" FieldLabel="Level/Position"
                            Width="140" MarginSpec="0 5 0 5" DisplayField="Name" ValueField="LevelId" QueryMode="Local"
                            LabelSeparator="" LabelAlign="Top">
                            <Items>
                                <ext:ListItem Index="0" Value="-1" Text="All">
                                </ext:ListItem>
                            </Items>
                            <Store>
                                <ext:Store ID="storeLevel" runat="server">
                                    <Model>
                                        <ext:Model ID="modelLevel" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="LevelId" Type="Int" />
                                                <ext:ModelField Name="Name" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                    </td>
                    <td>
                        <ext:ComboBox MatchFieldWidth="false" FieldLabel="Designation" ID="cmbDesignation"
                            Width="140" MarginSpec="0 5 0 5" runat="server" ValueField="DesignationId" DisplayField="LevelAndDesignation"
                            LabelAlign="Top" LabelSeparator="" QueryMode="Local">
                            <Items>
                                <ext:ListItem Index="0" Value="-1" Text="All">
                                </ext:ListItem>
                            </Items>
                            <Store>
                                <ext:Store ID="Store7" runat="server">
                                    <Model>
                                        <ext:Model ID="Model9" IDProperty="DesignationId" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="DesignationId" />
                                                <ext:ModelField Name="LevelAndDesignation" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                    </td>
                    <td>
                        <ext:ComboBox LabelSeparator="" ID="cmbEmpSearch" FieldLabel="Search Employee" EmptyText="Employee Name"
                            LabelWidth="80" LabelAlign="Top" runat="server" DisplayField="Name" ValueField="EmployeeId"
                            StoreID="storeSearch" TypeAhead="false" Width="200" PageSize="9999" HideBaseTrigger="true"
                            MinChars="1" TriggerAction="All" ForceSelection="false">
                            <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                                <ItemTpl ID="ItemTpl2" runat="server">
                                    <Html>
                                        <tpl>
                                                        <div class="search-item">
                                                                        <span>{Name}</span>  
                                                         </div>
					                            </tpl>
                                    </Html>
                                </ItemTpl>
                            </ListConfig>
                            <Triggers>
                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                            </Triggers>
                            <Listeners>
                                <Select Handler="this.getTrigger(0).show(); SelEmpSearch();" />
                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                <TriggerClick Handler="if (index == 0) { 
                                                       this.clearValue(); 
                                                       this.getTrigger(0).hide();
                                                   }" />
                            </Listeners>
                        </ext:ComboBox>
                    </td>
                    <td style="padding-left: 10px">
                        <ext:Button ID="btnLoad" runat="server" Text="Load" Width="100" Cls="btn btn-default"
                            MarginSpec="25 10 10 10">
                            <Listeners>
                                <Click Fn="searchList">
                                </Click>
                            </Listeners>
                        </ext:Button>
                    </td>
                    <td style="padding-left: 10px">
                        <ext:Button runat="server" MarginSpec="25 10 10 10" AutoPostBack="true" OnClick="btnExport_Click"
                            ID="btnExport" Text="<i></i>Export To Excel">
                        </ext:Button>
                    </td>
                </tr>
            </table>
        </Content>
    </ext:Container>
</div>
<ext:KeyMap ID="KeyMap1" runat="server" Target="={#{listingContainer}.getBody()}">
    <Binding>
        <ext:KeyBinding Handler="#{btnLoad}.fireEvent('click');">
            <Keys>
                <ext:Key Code="ENTER" />
            </Keys>
        </ext:KeyBinding>
    </Binding>
</ext:KeyMap>
<ext:GridPanel StyleSpec="margin-top:15px;" ID="gridBranchTransfer" runat="server"
    OnReadData="Store_ReadData" AutoScroll="true">
    <Store>
        <ext:Store PageSize="25" ID="storeEmpList" runat="server" AutoLoad="false" OnReadData="Store_ReadData"
            RemotePaging="true" RemoteSort="true">
            <Proxy>
                <ext:PageProxy />
            </Proxy>
            <Model>
                <ext:Model ID="Model1" runat="server" IDProperty="EmployeeId">
                    <Fields>
                        <ext:ModelField Name="SN" Type="String" />
                        <ext:ModelField Name="EIN" Type="String" />
                        <ext:ModelField Name="AccountNo" Type="String" />
                        <ext:ModelField Name="Name" Type="String" />
                        <ext:ModelField Name="Level" Type="String" />
                        <ext:ModelField Name="Designation" Type="String" />
                        <ext:ModelField Name="Branch" Type="String" />
                        <ext:ModelField Name="Department" Type="String" />
                        <ext:ModelField Name="AbsentDays" Type="String" />
                        <ext:ModelField Name="AbsentDetails" Type="String" />
                        <ext:ModelField Name="LWPDays" Type="String" />
                        <ext:ModelField Name="LWPDetails" Type="String" />
                        <ext:ModelField Name="LateDays" Type="String" />
                        <ext:ModelField Name="LateDetails" Type="String" />
                        <ext:ModelField Name="TotalSalaryDeduction" Type="String" />
                    </Fields>
                </ext:Model>
            </Model>
            <Sorters>
                <ext:DataSorter Property="EmployeeId" Direction="ASC" />
            </Sorters>
        </ext:Store>
    </Store>
    <ColumnModel ID="ColumnModel1" runat="server">
        <Columns>
            <ext:Column ID="colFromDate" runat="server" Text="SN" Locked="true" DataIndex="SN"
                MenuDisabled="false" Sortable="false" Align="Right" Width="50" />
            <ext:Column ID="Column1" runat="server" Text="EIN" Locked="true" DataIndex="EIN"
                MenuDisabled="false" Sortable="true" Align="Right" Width="50" />
            <ext:Column ID="ColumnAccountNo" runat="server" Text="AccountNo" Locked="true" DataIndex="AccountNo"
                MenuDisabled="false" Sortable="true" Align="Right" Width="50" />
            <ext:Column ID="Column3" runat="server" Text="Name" Locked="true" DataIndex="Name"
                MenuDisabled="false" Sortable="true" Align="Left" Width="150" />
            <ext:Column ID="ColumnLevel" runat="server" Text="Level" DataIndex="Level" MenuDisabled="false"
                Sortable="true" Align="Left" Width="120" />
            <ext:Column ID="ColumnDesignation" runat="server" Text="Designation" DataIndex="Designation"
                MenuDisabled="false" Sortable="true" Align="Left" Width="120" />
            <ext:Column ID="ColumnBranch" runat="server" Text="Branch" DataIndex="Branch" MenuDisabled="false"
                Sortable="true" Align="Left" Width="120" />
            <ext:Column ID="ColumnDepartment" runat="server" Text="Department" DataIndex="Department"
                MenuDisabled="false" Sortable="true" Align="Left" Width="120" />
            <ext:Column ID="income1" runat="server" Text="Absent Days" DataIndex="AbsentDays"
                MenuDisabled="false" Sortable="true" Align="Center" Width="100">
            </ext:Column>
            <ext:Column ID="Column8" runat="server" Text="Absent Details" DataIndex="AbsentDetails"
                MenuDisabled="false" Sortable="true" Align="Left" Width="150">
            </ext:Column>
            <ext:Column ID="Column9" runat="server" Text="LWP Days" DataIndex="LWPDays" MenuDisabled="false"
                Sortable="true" Align="Center" Width="100">
            </ext:Column>
            <ext:Column ID="Column10" runat="server" Text="LWP Details" DataIndex="LWPDetails"
                MenuDisabled="false" Sortable="true" Align="Left" Width="150">
            </ext:Column>
            <ext:Column ID="Column11" runat="server" Text="Late Days" DataIndex="LateDays" MenuDisabled="false"
                Sortable="true" Align="Center" Width="100">
            </ext:Column>
            <ext:Column ID="Column12" runat="server" Text="Late Details" DataIndex="LateDetails"
                MenuDisabled="false" Sortable="true" Align="Left" Width="150">
            </ext:Column>
            <ext:Column ID="Column13" runat="server" Text="Total Deduction Days" DataIndex="TotalSalaryDeduction"
                MenuDisabled="false" Sortable="true" Align="Center" Width="150">
            </ext:Column>
        </Columns>
    </ColumnModel>
    <BottomBar>
        <ext:PagingToolbar ID="PagingToolbar1" runat="server" StoreID="storeEmpList" DisplayInfo="true">
        </ext:PagingToolbar>
    </BottomBar>
</ext:GridPanel>
