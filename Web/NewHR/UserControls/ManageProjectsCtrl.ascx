﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ManageProjectsCtrl.ascx.cs" Inherits="Web.NewHR.UserControls.ManageProjectsCtrl" %>


<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningMsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="InfoMsgCtl" TagPrefix="uc2" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>


 <script type="text/javascript">


     function filterCallback(chk) {
         if (chk.checked) {


             disableElement('<%= calFilterTo.ClientID %>' + '_date');

         }
         else {

             enableElement('<%= calFilterTo.ClientID %>' + '_date');
         }

     }

     function addProjectPopup() {
         var ret = popupProject();

         if (typeof (ret) != 'undefined') {
             if (ret == 'Reload') {
                 __doPostBack('', 'Reload');
             }
         }
         return false;
     }

     function editProjectPopup(projectId) {
         var ret = popupProject('Id=' + projectId);

         if (typeof (ret) != 'undefined') {
             if (ret == 'Reload') {
                 __doPostBack('', 'Reload');
             }
         }
         return false;
     }

     function reloadProject(childWindow) {
         childWindow.close();
         __doPostBack('Reload', '');
     }
  
    </script>



  <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Manage Projects
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="contentArea">
            <uc2:InfoMsgCtl ID="msgInfo" EnableViewState="false" Hide="true" runat="server" />
            <uc2:WarningMsgCtl ID="msgWarning" EnableViewState="false" Hide="true" runat="server" />
            <div class="attribute"  style="padding:10px">
                Filter From &nbsp;
                <My:Calendar Id="calFilterFrom" runat="server" />
                To &nbsp;
                <My:Calendar Id="calFilterTo" runat="server" />
                &nbsp;
                <asp:CheckBox ID="chkAllProjects" runat="server" Text="All Projects" />
                &nbsp;
                <asp:TextBox ID="txtSearch" runat="server" />
                <asp:LinkButton ID="btnLoad" runat="server" OnClick="btnLoad_Click" Text="Load" CssClass="btn btn-default btn-sm"
                    Style='float: right; padding-right: 30px' />
            </div>
            <div style="clear: both;" />
            <div style="text-align: left">
                <asp:Button ID="btnAdd" Width="120px" Style="margin-left: 0px;" CausesValidation="false"
                    CssClass="btn btn-warning btn-sm" runat="server" Text="Add Project" OnClientClick="return addProjectPopup();" />
            </div>
            <br />
            <cc2:EmptyDisplayGridView Width="100%" Style='clear: both; margin-bottom:0px' PagerStyle-HorizontalAlign="Center"
                PagerStyle-CssClass="defaultPagingBar" CssClass="table table-primary mb30 table-bordered table-hover" UseAccessibleHeader="true"
                ShowHeaderWhenEmpty="True" ID="gvwProjects" runat="server" DataKeyNames="ProjectId"
                AutoGenerateColumns="False" CellPadding="4" GridLines="None" ShowFooterWhenEmpty="False"
                OnPageIndexChanged="gvwRoles_PageIndexChanged" OnPageIndexChanging="gvwRoles_PageIndexChanging"
                OnRowDeleting="gvwRoles_RowDeleting">
                <Columns>
                    <asp:TemplateField HeaderText="Name" FooterStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Left"
                        HeaderStyle-Width="300px">
                        <ItemTemplate>
                            <%# Eval("Name")%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Project Code" FooterStyle-HorizontalAlign="Right"
                        HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="150px">
                        <ItemTemplate>
                            <%# Eval("Code")%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Start Date" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="100px">
                        <ItemTemplate>
                            <%# Eval("StartDate")%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="End Date" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="100px">
                        <ItemTemplate>
                            <%# Eval("EndDate")%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Donor" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="150px">
                        <ItemTemplate>
                            <%# Eval("Donor")%></ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Project Analysis Code" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="150px">
                        <ItemTemplate>
                            <%# Eval("ProjectAnalysisCode")%></ItemTemplate>
                    </asp:TemplateField>

                  <%--  <asp:TemplateField HeaderText="Fund Code" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="100px">
                        <ItemTemplate>
                            <%# Eval("FundCode")%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="PID" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="100px">
                        <ItemTemplate>
                            <%# Eval("PID")%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="DepartmentID" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="100px">
                        <ItemTemplate>
                            <%# Eval("DepartmentID")%></ItemTemplate>
                    </asp:TemplateField>--%>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left"></asp:TemplateField>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderStyle-Width="100px">
                        <ItemTemplate>
                            <%--<asp:ImageButton ID="ImageButton1" runat="server" CommandName="Select" ImageUrl="~/images/edit.gif" />--%>
                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/images/edit.gif" OnClientClick='<%# "return editProjectPopup(" +  Eval("ProjectId") + ");" %>' />
                            &nbsp;
                            <asp:ImageButton ID="ImageButton2" OnClientClick="return confirm('Do you really want to delete the project?')"
                                runat="server" CommandName="Delete" ImageUrl="~/images/delet.png" />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                    </asp:TemplateField>
                </Columns>
                <PagerSettings Mode="NumericFirstLast" NextPageText="Next »" PreviousPageText="« Prev"
                    FirstPageText="First" LastPageText="Last" />
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                <PagerStyle CssClass="defaultPagingBar" />
                <SelectedRowStyle CssClass="selected" />
                <EmptyDataTemplate>
                    <b>No projects. </b>
                </EmptyDataTemplate>
            </cc2:EmptyDisplayGridView>
            <uc1:PagingCtl ID="pagintCtl" OnNextRecord="btnNext_Click" OnPrevRecord="btnPrevious_Click"
                OnDropDownShowRecordsChanged="ddlRecords_SelectedIndexChanged" runat="server" />
        </div>
        <div style='margin-top: 15px'>
        </div>
    </div>

