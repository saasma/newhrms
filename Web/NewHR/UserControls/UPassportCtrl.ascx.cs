﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;

namespace Web.NewHR.UserControls
{
    public partial class UPassportCtrl : BaseUserControl
    {
        private bool _isDisplayMode = false;
        public bool IsDisplayMode
        {
            set
            {
                if (value == true)
                {
                    _isDisplayMode = value;
                    CommandColumn3.Visible = false;
                    CommandColumn2.Visible = false;
                    LinkButton1.Visible = false;
                    GridPassport.Width = 920;
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();

                if (SessionManager.CurrentLoggedInEmployeeId != 0)
                    hdnIsEmployee.Text = "1";
                else
                    hdnIsEmployee.Text = "0";
            }
        }

        protected void Initialise()
        {
            int EmployeeID = GetEmployeeID();
            this.LoadPassportGrid(EmployeeID);

        }       
        
        protected void PassportDownLoad(int ID)
        {

            //string contentType = "";
            HPassport doc = NewHRManager.GetPassportDetailsById(ID);

            string path = Context.Server.MapPath(@"~/uploads/" + doc.FileLocation + doc.ServerFileName);
            string contentType = doc.FileType;
            string name = doc.UserFileName;

            byte[] bytes = File.ReadAllBytes(path);

            Context.Response.Clear();
            Context.Response.ClearHeaders();
            Context.Response.AppendHeader("Content-Disposition", "attachment; filename=\"" + name + "\"");
            Context.Response.ContentType = contentType;
            Context.Response.BinaryWrite(bytes);
            Context.Response.End();
            // Response.Redirect("~/DocumentHandler.ashx?ID=" + ID);
        }

        
        protected void LoadPassportGrid(int EmployeeID)
        {
            List<HPassport> _HPassport = NewHRManager.GetPassportByEmployeeID(EmployeeID);
            if (_HPassport != null)
            {
                this.StorePassport.DataSource = _HPassport;
                this.StorePassport.DataBind();
            }
            else
            {
                this.StorePassport.DataSource = this.PassportFillData;
                this.StorePassport.DataBind();
            }
        }


        protected int GetEmployeeID()
        {
            //Guid? AssetID = null;
            int EmpID = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                EmpID = int.Parse(Request.QueryString["ID"]);
            }
            else
                EmpID = SessionManager.CurrentLoggedInEmployeeId;

            return EmpID;
        }

        protected void btnAddNewLine_Click(object sender, DirectEventArgs e)
        {
            ucPassport.ClearFields();
            Ext.Net.Button btnSave = (Ext.Net.Button)ucPassport.FindControl("btnSave");
            btnSave.Disabled = false;
            AEPassportWindow.Center();
            this.AEPassportWindow.Show();
        }

        private object[] PassportFillData
        {
            get
            {
                return new object[]
            {
                new object[] { "", "", "",""},
               
            };
            }
        }

        protected void GridPassport_Command(object sender, DirectEventArgs e)
        {
            string commandName = e.ExtraParams["command"];
            int PassportID = int.Parse(e.ExtraParams["ID"]);
            HPassport _HPassport = NewHRManager.GetPassportDetailsById(int.Parse(e.ExtraParams["ID"]));
            string Path = Server.MapPath(@"~/uploads/" + _HPassport.FileLocation + _HPassport.ServerFileName);
            switch (commandName)
            {
                case "Delete":
                    this.DeletePassport(PassportID, Path);
                    break;
                case "DownLoad":
                    this.PassportDownLoad(PassportID);
                    break;
                case "Edit":
                    this.editPassport(PassportID);
                    break;
            }

        }

        protected void DeletePassport(int ID, string path)
        {

            bool result = NewHRManager.DeletePassportByID(ID);
            if (result)
            {
                if (File.Exists(path))
                    File.Delete(path);
                //refresh grid
                NewMessage.ShowNormalMessage("Record deleted successfully.");
                this.LoadPassportGrid(this.GetEmployeeID());
            }

        }

        public void editPassport(int PassportID)
        {
            ucPassport.editPassport(PassportID);
            this.AEPassportWindow.Show();
        }

        protected void btnLoadPassportGrid_Click(object sender, DirectEventArgs e)
        {
            int employeeId = 0;
            if (Request.QueryString["ID"] != null)
                employeeId = int.Parse(Request.QueryString["ID"].ToString());
            else
                employeeId = SessionManager.CurrentLoggedInEmployeeId;

            LoadPassportGrid(employeeId);
        }

    }
}