﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using Utils.Web;
using Utils.Helper;

namespace Web.NewHR.UserControls
{
    public partial class ChildrenAllowanceCtl : BaseUserControl
    {
      
        public int GetEmployeeId()
        {
            return int.Parse(Request.QueryString["Id"]);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMsg.Text = "";
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        public void Initialise()
        {
            storeAllowance.DataSource = AllowanceManager.GetChildAllowanceIncomeList();
            storeAllowance.DataBind();
        }


        protected void AddDuration_Change(object sender, DirectEventArgs e)
        {
            if (!string.IsNullOrEmpty(cmbYearMonth.SelectedItem.Value) && !string.IsNullOrEmpty(txtAddYearMonth.Text.Trim()))
            {

                if (CalAllownceStartDate.SelectedDate != CalAllownceStartDate.EmptyDate)
                    if (cmbYearMonth.SelectedItem.Text.ToLower() == "month")
                        calAllowanceEndDate.Text =((CalAllownceStartDate.SelectedDate).AddMonths(int.Parse(txtAddYearMonth.Text))).ToString();
                    else
                        calAllowanceEndDate.Text = ((CalAllownceStartDate.SelectedDate).AddYears(int.Parse(txtAddYearMonth.Text))).ToString();
            }
            else
                calAllowanceEndDate.Text = "";
        }

        protected void CalAllownceStartDate_Change(object sender, DirectEventArgs e)
        {

            DateTime startDate = CalAllownceStartDate.SelectedDate;

            if(cmbYearMonth.SelectedItem != null && cmbYearMonth.SelectedItem.Value != null)
            {
                int period = 0;

                if(int.TryParse(txtAddYearMonth.Text,out period))
                {
                    // for year
                    if (cmbYearMonth.SelectedItem.Value == "1")
                    {
                        startDate = startDate.AddYears(period);
                    }
                    else
                        startDate = startDate.AddMonths(period);
                }
            }

            calAllowanceEndDate.SelectedDate = startDate;
        }

        protected void Change_EmployeeSearch(object sender, DirectEventArgs e)
        {
            if (!string.IsNullOrEmpty(cmbSearch.SelectedItem.Value))
            {

                int EmployeeId = int.Parse(cmbSearch.SelectedItem.Value);
                hdnEmployee.Text = cmbSearch.SelectedItem.Value;
                storeChildren.DataSource = AllowanceManager.GetChildernAllowanceFamilyMemberOfEmployee(EmployeeId);
                storeChildren.DataBind();
                
            }
        }

        protected void cmbChildren_Change(object sender, DirectEventArgs e)
        {
            if (!string.IsNullOrEmpty(cmbChildren.SelectedItem.Value))
            {

                int FamilyID = int.Parse(cmbChildren.SelectedItem.Value);
                hdnEmployee.Text = cmbSearch.SelectedItem.Value;
                DateTime? DateOfBirthEng = AllowanceManager.GetDateOfBirthOfFamilyMember(FamilyID);
                if (DateOfBirthEng != null)
                    calDateOfBrith.Text = DateOfBirthEng.Value.ToString();
                else
                    calDateOfBrith.Text = "";
            }
        }
        

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveUpdateChildrenAllowance");
            if (Page.IsValid)
            {
                ChildrenAllowance _ChildrenAllowance = new ChildrenAllowance();

                // entity = Process(null);
                bool isInsert = string.IsNullOrEmpty(hdn.Text);
                if (isInsert == false)
                    _ChildrenAllowance.ChildrenAllowanceID = int.Parse(hdn.Text);

                if (!string.IsNullOrEmpty(cmbAllowance.SelectedItem.Value))
                    _ChildrenAllowance.IncomeId = int.Parse(cmbAllowance.SelectedItem.Value);

                if (!string.IsNullOrEmpty(cmbChildren.SelectedItem.Value))
                    _ChildrenAllowance.FamilyId = int.Parse(cmbChildren.SelectedItem.Value);

                if (calDateOfBrith.SelectedDate != calDateOfBrith.EmptyDate)
                    _ChildrenAllowance.DOB = calDateOfBrith.SelectedDate;

                if (!string.IsNullOrEmpty(txtNote.Text.Trim()))
                    _ChildrenAllowance.Note = txtNote.Text.Trim();

                if (!string.IsNullOrEmpty(txtAddYearMonth.Text.Trim()))
                    _ChildrenAllowance.Duration = int.Parse(txtAddYearMonth.Text.Trim());

                if (!string.IsNullOrEmpty(cmbYearMonth.SelectedItem.Value))
                    _ChildrenAllowance.MonthYearID = int.Parse(cmbYearMonth.SelectedItem.Value);

                if (!string.IsNullOrEmpty(txtAmountPerMonth.Text))
                    _ChildrenAllowance.Amount = decimal.Parse(txtAmountPerMonth.Text);

                if (CalAllownceStartDate.SelectedDate!=calAllowanceEndDate.EmptyDate)
                    _ChildrenAllowance.StartDate = CalAllownceStartDate.SelectedDate;

                if (calAllowanceEndDate.SelectedDate != calAllowanceEndDate.EmptyDate)
                    _ChildrenAllowance.EndDate = calAllowanceEndDate.SelectedDate;

                if (!string.IsNullOrEmpty(hdnEmployee.Text.Trim()))
                    _ChildrenAllowance.EmployeeId = int.Parse(hdnEmployee.Text.Trim());

                Status status = AllowanceManager.InsertUpdateChildrenAllowance(_ChildrenAllowance, isInsert);
                if (status.IsSuccess)
                {
                    ClearFields();
                    if(isInsert)
                    NewMessage.ShowNormalMessage("Record Saved Successfully", "refreshWindow()");
                    else
                        NewMessage.ShowNormalMessage("Record Updated Successfully", "refreshWindow()");
                }

                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }
            }

        }

        public void LoadData(int AllowanceID)
        {
            ClearFields();
            hdn.Text = AllowanceID.ToString();

            ChildrenAllowance _ChildrenAllowance = AllowanceManager.GetChildrenAllowance(AllowanceID);
            if (_ChildrenAllowance != null)
            {

                string EmployeeName = EmployeeManager.GetEmployeeById(_ChildrenAllowance.EmployeeId).Name;
                cmbSearch.SetValue(_ChildrenAllowance.EmployeeId.ToString());
                cmbSearch.SetRawValue(EmployeeName);
                hdnEmployee.Text = _ChildrenAllowance.EmployeeId.ToString();

                storeChildren.DataSource = AllowanceManager.GetChildernAllowanceFamilyMemberOfEmployee(_ChildrenAllowance.EmployeeId);
                storeChildren.DataBind();

                if (_ChildrenAllowance.MonthYearID != null)
                {   
                    //cmbYearMonth.SuspendEvents();
                    cmbYearMonth.SetValue(_ChildrenAllowance.MonthYearID.ToString());
                   // cmbYearMonth.ResumeEvents();
                }

                if (_ChildrenAllowance.Duration != null)
                {
                    txtAddYearMonth.SuspendEvents();
                    txtAddYearMonth.Text = _ChildrenAllowance.Duration.ToString();
                    txtAddYearMonth.ResumeEvents();
                }


                if (_ChildrenAllowance.Note != null)
                    txtNote.Text = _ChildrenAllowance.Note;

                cmbAllowance.SetValue(_ChildrenAllowance.IncomeId.ToString());
                cmbChildren.SetValue(_ChildrenAllowance.FamilyId.ToString());

                if (_ChildrenAllowance.DOB != null)
                    calDateOfBrith.Text = _ChildrenAllowance.DOB.ToString();

                txtAmountPerMonth.Text = _ChildrenAllowance.Amount.ToString("0.00");

                if (_ChildrenAllowance.StartDate != null)
                    CalAllownceStartDate.Text = _ChildrenAllowance.StartDate.ToString();

                if (_ChildrenAllowance.EndDate != null)
                    calAllowanceEndDate.Text = _ChildrenAllowance.EndDate.ToString();
            }
        }

        public void ClearFields()
        {   hdn.Text = "";
            cmbSearch.Clear();
            txtNote.Clear();
            cmbAllowance.Clear();
            cmbChildren.Clear();
            calDateOfBrith.Clear();
            txtAmountPerMonth.Clear();
            CalAllownceStartDate.Clear();
            calAllowanceEndDate.Clear();
            txtAddYearMonth.Clear();
            cmbYearMonth.Clear();
        }

    }
}