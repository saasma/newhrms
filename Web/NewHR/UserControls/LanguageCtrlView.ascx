﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LanguageCtrlView.ascx.cs"
    Inherits="Web.NewHR.UserControls.LanguageCtrlView" %>

<ext:GridPanel ID="GridLanguageSets" runat="server" Width="920" Scroll="None">
    <Store>
        <ext:Store ID="StoreLanguageSets" runat="server">
            <Model>
                <ext:Model ID="Model1" runat="server" IDProperty="LanguageSetId">
                    <Fields>
                        <ext:ModelField Name="LanguageSetName" Type="string" />
                        <ext:ModelField Name="FluencySpeakName" Type="String" />
                        <ext:ModelField Name="FluencyWriteName" Type="String" />
                        <ext:ModelField Name="IsEditable" Type="Int" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
    </Store>
    <ColumnModel>
        <Columns>
            <ext:Column ID="Column1" runat="server" Text="Language" DataIndex="LanguageSetName"
                Wrap="true" Width="200" Flex="1" />
            <ext:Column ID="Column2" runat="server" Text="Fluency : Speaking" DataIndex="FluencySpeakName"
                Wrap="true" Width="150" />
            <ext:Column ID="Column3" runat="server" Text="Fluency : Writing" DataIndex="FluencyWriteName"
                Wrap="true" Width="150" />
            <ext:Column ID="Column4" runat="server" Text="" Width="420" />
        </Columns>
    </ColumnModel>
    <SelectionModel>
        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
    </SelectionModel>
</ext:GridPanel>
