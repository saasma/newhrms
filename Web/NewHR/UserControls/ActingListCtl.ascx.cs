﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using Utils.Helper;

namespace Web.NewHR.UserControls
{
    public partial class ActingListCtl : BaseUserControl
    {
        private bool _isDisplayMode = false;
        public bool IsDisplayMode
        {
            set
            {
                if (value == true)
                {
                    _isDisplayMode = value;
                    btnAddLevel.Visible = false;
                    CommandColumn1.Visible = false;

                    tblFilter.Visible = false;
                }
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        public void Initialise()
        {
            LoadLevels();
        }

        public void btnLoad_Click(object sender, DirectEventArgs e)
        {
            LoadLevels();
        }

        private void LoadLevels()
        {
            int total = 0;
            DateTime? start = null, end = null;

            if (!string.IsNullOrEmpty(calFromDate.Text))
                start = GetEngDate(calFromDate.Text);

            if (!string.IsNullOrEmpty(calToDate.Text))
                end = GetEngDate(calToDate.Text);

            int employeeId = UrlHelper.GetIdFromQueryString("ID");
            List<ActingEmployee> list = NewPayrollManager.GetActingEmployeeList(
                0, int.MaxValue, ref total, cmbSearch.Text, employeeId,start,end);
            GridLevels.Store[0].DataSource = list;
            GridLevels.Store[0].DataBind();

            if (_isDisplayMode && list.Count <= 0)
                GridLevels.Hide();
        }





        public void btnExport_Click(object sender, EventArgs e)
        {
            int total = 0;
            DateTime? start = null, end = null;

            if (!string.IsNullOrEmpty(calFromDate.Text))
                start = GetEngDate(calFromDate.Text);

            if (!string.IsNullOrEmpty(calToDate.Text))
                end = GetEngDate(calToDate.Text);

            int employeeId = UrlHelper.GetIdFromQueryString("ID");
            List<ActingEmployee> list = NewPayrollManager.GetActingEmployeeList(
                0, int.MaxValue, ref total, cmbSearch.Text, employeeId, start, end);


            List<string> hiddenList = new List<string>();
            hiddenList.Add("ActingID");
            hiddenList.Add("BranchDepartmentId");
            hiddenList.Add("LetterDateEng");
            hiddenList.Add("LetterDateEng");
            hiddenList.Add("ApplicableTillEng");
            hiddenList.Add("ApplicableFromEng");
            hiddenList.Add("CreatedBy");
            hiddenList.Add("CreatedOn");
            hiddenList.Add("ModifiedBy");
            hiddenList.Add("ModifiedOn");
            hiddenList.Add("FromLevelId");
            hiddenList.Add("ToLevelId");
            hiddenList.Add("DesignationId");
            hiddenList.Add("Amount");
            hiddenList.Add("ActingEmployee2");
            hiddenList.Add("ActingEmployee1");

            Dictionary<string, string> renameList = new Dictionary<string, string>();


            Bll.ExcelHelper.ExportToExcel("Acting List", list,
                hiddenList,
            new List<String>() { },
            renameList,
            new List<string>() { }
            , new Dictionary<string, string>() { { "Acting List", "" } }
            , new List<string> { "EmployeeId", "INo", "EmployeeName", "LetterNumber", "LetterDate", "ApplicableFrom", "ApplicableTill", "FromLevel", "ToLevel" });


        }



        protected void btnDeleteLevel_Click(object sender, DirectEventArgs e)
        {
            int levelId = int.Parse(hiddenValue.Text.Trim());
            Status status = NewPayrollManager.DeleteActing(levelId);
            if (status.IsSuccess)
            {
                LoadLevels();
                NewMessage.ShowNormalMessage("Acting deleted.");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }

        }



        protected void btnEditLevel_Click(object sender, DirectEventArgs e)
        {
            int levelId = int.Parse(hiddenValue.Text.Trim());
            HFamily entity = NewPayrollManager.GetFamilyMemberById(levelId);
            


            //cmbGroup.SetValue(entity.LevelGroupId.ToString());
            //txtLevelCode.Text = entity.Code;
            //txtLevelName.Text = entity.Name;
            //txtLevelOrder.Number = entity.Order;

        }

        

    }
}