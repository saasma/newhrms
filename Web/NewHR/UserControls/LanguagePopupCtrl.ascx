﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LanguagePopupCtrl.ascx.cs" Inherits="Web.NewHR.UserControls.LanguagePopupCtrl" %>

<ext:Hidden runat="server" ID="hdnLanguageSetsID">
</ext:Hidden>

 <table class="fieldTable">
    <tr>
            <td>
                <ext:ComboBox ID="cmbLanguage" runat="server" ValueField="LanguageSetId" DisplayField="Name"
                    FieldLabel="Select Language" LabelAlign="top" LabelSeparator="" QueryMode="Local">
                    <Store>
                        <ext:Store ID="Store2" runat="server">
                            <Model>
                                <ext:Model ID="Model4" runat="server">
                                    <Fields>
                                        <ext:ModelField Name="LanguageSetId" />
                                        <ext:ModelField Name="Name" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                    </Store>
                </ext:ComboBox>
            </td>
            <td>
                <ext:TextField ID="txtNewLanguage" runat="server" FieldLabel="Add a new Language"
                    LabelAlign="top" LabelSeparator="" />
            </td>
            </tr>
            <tr>
                <td style="padding-top: 10px; padding-bottom: 10px;">
                    <ext:Checkbox ID="chkIsNativeLanguage" runat="server" FieldLabel="Native Language"
                        LabelAlign="Right" LabelSeparator="">
                    </ext:Checkbox>
                </td>
                <td>
                    <ext:Checkbox ID="chkAddtoLanguagePool" runat="server" FieldLabel="Add to Languages"
                        Width="200" LabelAlign="Right" LabelWidth="105" LabelSeparator="">
                    </ext:Checkbox>
                </td>
            </tr>
            <tr>
                <td>
                    <ext:ComboBox ID="cmbFluencySpeak" runat="server" ValueField="LevelID" DisplayField="Name"
                        Width="150" FieldLabel="Fluency: Speaking" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                        QueryMode="Local">
                        <Items>
                            <ext:ListItem Text="Fluent" Value="1" />
                            <ext:ListItem Text="Excellent" Value="2" />
                            <ext:ListItem Text="Good" Value="3" />
                            <ext:ListItem Text="Understand" Value="4" />
                        </Items>
                        <%-- <Store>
                                                    <ext:Store ID="Store1" runat="server">
                                                        <Model>
                                                            <ext:Model ID="Model2" runat="server">
                                                                <Fields>
                                                                    <ext:ModelField Name="LevelID" />
                                                                    <ext:ModelField Name="Name" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                    </ext:Store>
                                                </Store>--%>
                    </ext:ComboBox>
                    <asp:RequiredFieldValidator Display="None" ID="valcmbFluencySpeak" runat="server"
                        ValidationGroup="LanguageSetsSaveUpdate" ControlToValidate="cmbFluencySpeak"
                        ErrorMessage="Please check fluency in speaking." />
                </td>
                <td>
                    <ext:ComboBox ID="cmbFluencyWrite" runat="server" ValueField="LevelID" DisplayField="Name"
                        Width="150" FieldLabel="Fluency: Writing" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                        QueryMode="Local">
                        <Items>
                            <ext:ListItem Text="Excellent" Value="1" />
                            <ext:ListItem Text="Very Good" Value="2" />
                            <ext:ListItem Text="Good" Value="3" />
                            <ext:ListItem Text="Average" Value="4" />
                            <ext:ListItem Text="None" Value="5" />
                        </Items>
                        <%-- <Store>
                                                    <ext:Store ID="Store1" runat="server">
                                                        <Model
                                                            <ext:Model ID="Model2" runat="server">
                                                                <Fields>
                                                                    <ext:ModelField Name="LevelID" />
                                                                    <ext:ModelField Name="Name" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                    </ext:Store>
                                                </Store>--%>
                    </ext:ComboBox>
                    <asp:RequiredFieldValidator Display="None" ID="valcmbFluencyWrite" runat="server"
                        ValidationGroup="LanguageSetsSaveUpdate" ControlToValidate="cmbFluencyWrite"
                        ErrorMessage="Please check fluency in writing." />
                </td>
            </tr>
            <tr>
                <td valign="bottom" colspan="2">
                    <div class="popupButtonDiv">
                        <ext:Button runat="server" Cls="btn btn-primary" ID="btnSave" Text="<i></i>Save">
                            <DirectEvents>
                                <Click OnEvent="btnSave_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                            <Listeners>
                                <Click Handler="valGroup = 'LanguageSetsSaveUpdate'; return CheckValidation();">
                                </Click>
                            </Listeners>
                        </ext:Button>
                        <div class="btnFlatOr">
                            or
                        </div>
                        <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="LinkButton2" Cls="btnFlatLeftGap"
                            Text="<i></i>Cancel">
                            <Listeners>
                                <Click Handler="#{AELanguageSetsWindow}.hide();">
                                </Click>
                            </Listeners>
                        </ext:LinkButton>
                    </div>
                </td>
            </tr>
        </table>