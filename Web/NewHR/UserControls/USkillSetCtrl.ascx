﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="USkillSetCtrl.ascx.cs" Inherits="Web.NewHR.UserControls.USkillSetCtrl" %>

<%@ Register Src="~/NewHR/UserControls/SkillSetPopUpCtrl.ascx" TagName="SkillSetPop" TagPrefix="ucS" %>


 <script type="text/javascript">

     var prepareSkillSet = function (grid, toolbar, rowIndex, record) {

         var btnEditDel = toolbar.items.get(0);

         if (record.data.IsEditable == 0) {
             btnEditDel.setVisible(false);
         }

     }

       function reloadSkillSetGrid() {  
        <%= btnLoadGrid.ClientID %>.fireEvent('click');
    }

    
    </script>

<ext:Hidden runat="server" ID="hdnSkillSetsID">
</ext:Hidden>

<ext:LinkButton ID="btnLoadGrid" runat="server" Hidden="true">
    <DirectEvents>
        <Click OnEvent="btnLoadGrid_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:LinkButton>

<div class="widget-body">
        <table class="fieldTable firsttdskip">
            <tr>
                <td>
                    <ext:GridPanel ID="GridSkillSets" runat="server" Width="1000" Cls="itemgrid" Scroll="None">
                        <Store>
                            <ext:Store ID="StoreSkillSets" runat="server">
                                <Model>
                                    <ext:Model ID="Model1" runat="server" IDProperty="SkillSetId">
                                        <Fields>
                                            <ext:ModelField Name="SkillSetName" Type="string" />
                                            <ext:ModelField Name="LevelOfExpertise" Type="String" />
                                            <ext:ModelField Name="IsEditable" Type="Int" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:Column ID="Column1" runat="server" Text="Skill" DataIndex="SkillSetName" Width="200" Wrap="true" />
                                <ext:Column ID="Column2" runat="server" Text="Expertise Level" DataIndex="LevelOfExpertise" Wrap="true"
                                    Width="150">
                                </ext:Column>
                                <ext:CommandColumn ID="CommandColumn1" runat="server" Width="40">
                                    <Commands>
                                        <ext:GridCommand Cls="editGridButton" ToolTip-Text="Edit"  Text="<i class='fa fa-pencil'></i>" CommandName="Edit" />                                        
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridSkillSets_Command">
                                            <EventMask ShowMask="true">
                                            </EventMask>
                                            <ExtraParams>
                                                <ext:Parameter Name="ID" Value="record.data.SkillSetId" Mode="Raw">
                                                </ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw">
                                                </ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                    <PrepareToolbar fn="prepareSkillSet" />
                                </ext:CommandColumn>
                                <ext:CommandColumn ID="CommandColumn2" runat="server" Width="40">
                                    <Commands>                                      
                                        <ext:GridCommand Cls="editGridButton" ToolTip-Text="Delete"  Text="<i class='fa fa-trash-o'></i>"
                                            CommandName="Delete" />
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridSkillSets_Command">
                                            <EventMask ShowMask="true">
                                            </EventMask>
                                            <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the record?" />
                                            <ExtraParams>
                                                <ext:Parameter Name="ID" Value="record.data.SkillSetId" Mode="Raw">
                                                </ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw">
                                                </ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                    <PrepareToolbar fn="prepareSkillSet" />
                                </ext:CommandColumn>
                                <ext:Column ID="Column3" runat="server" Width="570"/>
                            </Columns>
                        </ColumnModel>
                        <SelectionModel>
                            <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                        </SelectionModel>
                    </ext:GridPanel>
                </td>
            </tr>
        </table>
        <div class="buttonBlockSection">
            <ext:Button runat="server" ID="LinkButton1" Cls="btn btn-primary btn-sect" runat="server" Width="120" StyleSpec="margin-top:0px" Height="30"
            Text="<i></i>Add New Skill">
                <DirectEvents>
                    <Click OnEvent="btnAddNewLine_Click">
                        <EventMask ShowMask="true" />
                    </Click>
                </DirectEvents>
            </ext:Button>
        </div>
    </div>



<ext:Window ID="AESkillSetsWindow" runat="server" Title="Add/Edit SkillSets" Icon="Application"
    Height="400" Width="600"  BodyPadding="5"
    Hidden="true" Modal="true">
    <Content>
        <ucS:SkillSetPop ID="ucSkillSet" runat="server" />
    </Content>
</ext:Window>