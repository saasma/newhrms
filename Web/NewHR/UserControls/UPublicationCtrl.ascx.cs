﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;


namespace Web.NewHR.UserControls
{
    public partial class UPublicationCtrl : BaseUserControl
    {
        private bool _isDisplayMode = false;
        public bool IsDisplayMode
        {
            set
            {
                if (value == true)
                {
                    _isDisplayMode = value;
                    LinkButton1.Visible = false;
                    AEPublicationWindow.Visible = false;
                    CommandColumn1.Visible = false;
                    CommandColumn2.Visible = false;
                    GridPublication.Width = 920;
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
                HideButtonAndGridColumn();
            }
        }

        protected void Initialise()
        {
            int EmployeeID = GetEmployeeID();
            this.LoadPublicationGrid(EmployeeID);
        }

        protected void LoadPublicationGrid(int EmployeeID)
        {
            List<HPublication> _HPublication = NewHRManager.GetPublicationByEmployeeID(EmployeeID);

            if (SessionManager.CurrentLoggedInEmployeeId == 0)
                _HPublication = _HPublication.Where(x => x.Status == (int)HRStatusEnum.Approved).ToList();

            if (Request.QueryString["ID"] != null)
            {
                if (int.Parse(Request.QueryString["ID"].ToString()) != SessionManager.CurrentLoggedInEmployeeId)
                    _HPublication = _HPublication.Where(x => x.Status == (int)HRStatusEnum.Approved).ToList();
            }

            this.StorePublication.DataSource = _HPublication;
            this.StorePublication.DataBind();

            if (_isDisplayMode && _HPublication.Count <= 0)
                GridPublication.Hide();
            else
                GridPublication.Show();
        }


        protected int GetEmployeeID()
        {
            //Guid? AssetID = null;
            int EmpID = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                EmpID = int.Parse(Request.QueryString["ID"]);
            }
            else
            {
                EmpID = SessionManager.CurrentLoggedInEmployeeId;
            }
            return EmpID;
        }

        protected void btnAddNewLine_Click(object sender, DirectEventArgs e)
        {
            ucPublication.ClearFields();

            Ext.Net.Button btnSave = (Ext.Net.Button)ucPublication.FindControl("btnSave");
            btnSave.Disabled = false;

            this.AEPublicationWindow.Show();            
        }

        private object[] PublicationFillData
        {
            get
            {
                return new object[]
            {
                new object[] { "", "", "",""},
               
            };
            }
        }

        protected void GridPublication_Command(object sender, DirectEventArgs e)
        {
            string commandName = e.ExtraParams["command"];
            int PublicationID = int.Parse(e.ExtraParams["ID"]);
            HPublication _HPublication = NewHRManager.GetPublicationDetailsById(int.Parse(e.ExtraParams["ID"]));
            switch (commandName)
            {
                case "Delete":
                    this.DeleteData(PublicationID);
                    break;

                case "Edit":
                    {
                        Ext.Net.Button btnSave = (Ext.Net.Button)ucPublication.FindControl("btnSave");
                        btnSave.Disabled = false;
                        this.editPublication(PublicationID);
                        break;
                    }
            }

        }

        protected void DeleteData(int ID)
        {

            bool result = NewHRManager.DeletePublicationByID(ID);
            if (result)
            {
                NewMessage.ShowNormalMessage("Record deleted successfully.");
                this.LoadPublicationGrid(this.GetEmployeeID());
            }

        }
      
        public void editPublication(int PublicationID)
        {
            this.AEPublicationWindow.Show();
            ucPublication.editPublication(PublicationID);
        }

        protected void btnReloadGrid_Click(object sender, DirectEventArgs e)
        {
            int employeeId = 0;
            if (Request.QueryString["ID"] != null)
                employeeId = int.Parse(Request.QueryString["ID"].ToString());
            else
                employeeId = SessionManager.CurrentLoggedInEmployeeId;
            this.LoadPublicationGrid(employeeId);
        }

        private void HideButtonAndGridColumn()
        {
            if (Request.QueryString["ID"] != null && SessionManager.CurrentLoggedInEmployeeId != 0)
            {
                if (SessionManager.CurrentLoggedInEmployeeId != int.Parse(Request.QueryString["ID"].ToString()))
                {
                    IsDisplayMode = true;
                }
            }
        }
    }
}