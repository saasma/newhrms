﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BLL.Base;
using Ext.Net;
using BLL.Manager;
namespace Web.NewHR.UserControls
{
    public partial class EmpDetailsLeftMenu : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack && !X.IsAjaxRequest)
                SetSelection();
        }

        private void SetSelection()
        {
            string url = CurrentPageUrl.ToLower();

            LiReward.Visible = CommonManager.CompanySetting.HasLevelGradeSalary;

            if (string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                rootwizard.Visible = false;
            }
            else
            {
                string id = Request.QueryString["ID"];

                foreach (Control item in list.Controls)
                {
                    HtmlGenericControl ctl = item as HtmlGenericControl;
                    if (ctl != null && ctl.TagName == "li")
                    {
                        foreach (HtmlAnchor anchor in ctl.Controls)
                        {
                            if (anchor != null)
                            {
                                anchor.HRef += "?ID=" + id;

                                if (anchor.HRef.ToLower().Contains(url))
                                {


                                    HtmlGenericControl parent = anchor.Parent as HtmlGenericControl;
                                    parent.Attributes["class"] = "active";


                                }
                            }
                        }
                    }
                }
            }
        }
    }
}