﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmpPayInsuranceInfoCtl.ascx.cs"
    Inherits="Web.NewHR.UserControls.EmpPayInsuranceInfoCtl" %>
<ext:Hidden runat="server" ID="hdnCitizenshipID">
</ext:Hidden>
<ext:Hidden runat="server" ID="hdnIsEmployee" />

    <!-- panel-heading -->
    <div class="widget-body">
        <table class="fieldTable firsttdskip">
            <tr>
                <td>
                    <ext:GridPanel ID="gridPayInsurance" runat="server" Width="823" Cls="itemgrid">
                        <Store>
                            <ext:Store ID="storePayInsurance" runat="server">
                                <Model>
                                    <ext:Model ID="Model1" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="InsuranceCompany" Type="string" />
                                            <ext:ModelField Name="EmployerPaysPercent" Type="Float" />
                                            <ext:ModelField Name="PolicyAmount" Type="Float" />
                                            <ext:ModelField Name="Premium" Type="Float" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:Column ID="Column1" runat="server" Text="Insurance Company" DataIndex="InsuranceCompany"
                                    Width="100" Flex="1" />
                                <ext:TemplateColumn Text="Paid By" runat="server">
                                    <Template ID="Template1" runat="server">
                                        <Html>
                                            <tpl if="EmployerPaysPercent!=0">
						 EmployerPaysPercent
					</tpl>
                                            <tpl if="EmployerPaysPercent== 0">
						  Employee
					</tpl>
                                        </Html>
                                    </Template>
                                </ext:TemplateColumn>
                                <ext:Column ID="Column3" runat="server" Text="Policy Amount" DataIndex="PolicyAmount"
                                    Width="100">
                                </ext:Column>
                                <ext:Column ID="Column4" runat="server" Text="Premium" DataIndex="Premium" Width="130">
                                </ext:Column>
                            </Columns>
                        </ColumnModel>
                        <SelectionModel>
                            <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                        </SelectionModel>
                    </ext:GridPanel>
                </td>
            </tr>
        </table>
    </div>

