﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using BLL;
using DAL;
using Ext.Net;
using Utils.Web;
using Utils.Helper;
using System.Text;
using Newtonsoft.Json.Linq;
using BLL.BO;
using Utils.Calendar;

namespace Web.CP.Extension.UserControls
{
    public partial class DefinedTypeImportCtl : BaseUserControl
    {
        int columnWidth = 140;

        CommonManager commonMgr = new CommonManager();
        public bool isNsetDashainAmountImport = true;
        public bool IsDeduction
        {
            get
            {
                return isDeduction;
            }
            set
            {
                isDeduction = value;
            }
        }
        public bool isDeduction = false;


        protected void btnChangeScale_Click(object sender, DirectEventArgs e)
        {

            PIncomeDate date = new PIncomeDate();
            bool isInsert = true;
            //if (!string.IsNullOrEmpty(hdn.Text))
            //{
            //    isInsert = false;
            //    date = NewPayrollManager.GetLevelDateById(int.Parse(hdn.Text));

            //    BLevelDate lastDate = NewPayrollManager.GetLastLevelDate();
            //    if (lastDate.DateID != date.DateID)
            //    {
            //        NewMessage.ShowWarningMessage("Only last salary change is editable.");
            //        return;
            //    }

            //    if (date.EffectiveFromEng != null)
            //    {
            //        if (string.IsNullOrEmpty(calEffectiveFrom.Text.Trim()))
            //        {
            //            NewMessage.ShowWarningMessage("Effective date must be provided.");
            //            return;
            //        }
            //    }

            //    date.Name = txtName.Text.Trim();
            //    if (date.EffectiveFromEng != null)
            //    {
            //        date.EffectiveFrom = calEffectiveFrom.Text.Trim();
            //        date.EffectiveFromEng = GetEngDate(date.EffectiveFrom);
            //    }
            //}
            //else

            string incomeId = cmbIncomes.SelectedValue;

            if (string.IsNullOrEmpty(incomeId))
            {
                NewMessage.ShowWarningMessage("Income must be selected.");
                return;
            }

            {
                if (string.IsNullOrEmpty(calEffectiveFrom.Text.Trim()))
                {
                    NewMessage.ShowWarningMessage("Effective date must be provided.");
                    return;
                }
                date.IncomeId = int.Parse(incomeId);
                date.Name = txtName.Text.Trim();
                date.EffectiveFrom = calEffectiveFrom.Text.Trim();
                date.EffectiveFromEng = GetEngDate(date.EffectiveFrom);

            }

            if (date.EffectiveFromEng != null)
            {
                CustomDate dd = CustomDate.GetCustomDateFromString(date.EffectiveFrom, IsEnglish);
                if (dd.Day != 1)
                {
                    NewMessage.ShowWarningMessage("Effective date must be the first day of the month.");
                    return;
                }

                PIncomeDate prevDate = NewPayrollManager.GetPrevIncomeDate(date.DateID,date.IncomeId);
                if (prevDate!= null && prevDate.EffectiveFromEng >= date.EffectiveFromEng)
                {
                    NewMessage.ShowWarningMessage("Effective date must be greater than change effective date.");
                    return;
                }
            }


            NewPayrollManager.ChangeDefinedDate(date, isInsert);
            NewMessage.ShowNormalMessage("Scale saved, now please select the income & import the new scale amounts.", "forcePostBack");
            WindowAdd.Hide();

            //btnGenerateScale_Click(null, null);

            //X.Js.Call("forcePostBack");
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack && !X.IsAjaxRequest)
            {


                cmbIncomes.DataSource = NewPayrollManager.GetDefinedTypeIncomes();
                cmbIncomes.DataBind();
            }

            if (!IsPostBack && !X.IsAjaxRequest)
                Initialise();

            JavascriptHelper.AttachNonDialogPopUpCode(Page, "shiftPopup", "DefinedSalaryExcel.aspx", 450, 500);

            string code = string.Format("var totalHoursOrDayInAMonth = {0}; ",
                CommonManager.CompanySetting.IsProjectInputType ? ProjectManager.TotalHourOrDaysInAMonth : 100);
            code = string.Format("var isDeduction = {0}; ",isDeduction.ToString().ToLower());

            Page.ClientScript.RegisterStartupScript(this.GetType(), "sdffd", code, true);
            // programatic client-side calls to __doPostBack() can bypass this
            Page.ClientScript.RegisterOnSubmitStatement(GetType(), "ServerForm",
                                                    "if (this.submitted) return false; this.submitted = true; return true;");

            Page.ClientScript.RegisterClientScriptBlock(GetType(), "sffdsf", string.Format("var IsProjectInputType = {0};", CommonManager.CompanySetting.IsProjectInputType ? "true" : "false"), true);

        }

        public void Initialise()
        {
           


           
            //BindList();
        }

        public void ddlFrom_Select(object sender, EventArgs e)
        {
            
            BindList();
        }

        public void cmbIncomes_Select(object sender, EventArgs e)
        {
            List<PIncomeDate> list = NewPayrollManager.GetIncomeDateList(int.Parse(cmbIncomes.SelectedValue));
            ddlFrom.DataSource = list;
            ddlFrom.DataBind();
            if (list.Count > 0 && ddlFrom.Items.Count > 0)
            {
                ddlFrom.SelectedIndex = list.Count-1;
            }


            if (cmbIncomes.Items.Count > 0 && cmbIncomes.Items[0].Value == "")
                cmbIncomes.Items.RemoveAt(0);

            BindList();
        }

        Column GetExtGridPanelColumn(string indexId, string headerName)
        {
            Column column = new Column();
            column.DataIndex = indexId;
            column.DataIndex = indexId;
            column.Text = headerName;
            column.Align = Alignment.Center;
            column.ToolTip = headerName;
            column.MenuDisabled = true;
            column.Sortable = false;
            column.Width = new Unit(140);
            column.Renderer.Fn = "columnRenderer";

            //if (headerName == "Total")
            column.Renderer.Fn = "getFormattedAmount";


            return column;
        }

        public void BindForSingle(PIncome income)
        {

            try
            {

                ColumnFirst.Text = ((IncomeDefinedTypeEnum)income.XType).ToString();

                List<TextValue> xlist = NewPayrollManager.GetDefinedList(income.IncomeId, true);

                int? dateId = null;
                lblFrom.Text = "";



                if (!string.IsNullOrEmpty(ddlFrom.SelectedValue))
                {
                    dateId = int.Parse(ddlFrom.SelectedValue);
                    PIncomeDate date = NewPayrollManager.GetIncomeDate(income.IncomeId, dateId.Value);
                    lblFrom.Text = "From date : " + (date.EffectiveFrom == null ? "" : date.EffectiveFrom);
                }

                List<PIncomeDefinedAmount> values = NewPayrollManager.GetDefinedAmounts(income.IncomeId, dateId);

                ArrayReader reader = (storeProjects.Reader[0] as ArrayReader);
                //ColumnModel columnModelProjects = gridProjects.ColumnModel;

                string columnId = "P_{0}";
                //foreach (var project in xlist)
                {
                    string gridIndexId = string.Format(columnId, ColumnFirst.Text);
                    ModelField field = new ModelField(gridIndexId, ModelFieldType.Float);
                    field.UseNull = true;
                    //field.AllowBlank = true;
                    ModelProject.Fields.Add(gridIndexId, ModelFieldType.Float);
                    gridProjects.ColumnModel.Columns.Add(GetExtGridPanelColumn(gridIndexId, "Amount"));
                }


                // Generate data array & bind to Store
                // Add each  id as key
                Dictionary<string, double?> selection = new Dictionary<string, double?>();
                foreach (var item in values)
                {
                    selection.Add(item.XType.ToString(), item.Amount == null ? 0 : (double)item.Amount);
                }

                object[] data = new object[xlist.Count];

                // Iterate for each Project & Skip first as it represents All Project
                for (int i = 0; i < xlist.Count; i++)
                {
                    //first one for EIN, second for Name & last for Total
                    object[] rowData = new object[2];

                    int columnIndex = 0;

                    if (xlist.Count > 0)
                        rowData[columnIndex++] = xlist[i].Text;


                    if (selection.ContainsKey(xlist[i].Value))
                    {
                        rowData[columnIndex++] = selection[xlist[i].Value];
                    }
                    else
                    {
                        rowData[columnIndex++] = null;
                    }



                    data[i] = rowData;
                }

                storeProjects.DataSource = data;
                storeProjects.DataBind();

            }
            catch (Exception exp)
            {
                // when matrix setting is displayed having data this error will come so show this message
                NewMessage.ShowWarningMessage("Define type could not be dispalyed as the row/columns has been changed, so please reimport for new settings.");

            }
        }
        public void BindList()
        {
           
            
            int incomeId = int.Parse(cmbIncomes.SelectedItem.Value);
            int? dateId = null;
            lblFrom.Text = "";

            if (!string.IsNullOrEmpty(ddlFrom.SelectedValue))
            {
                dateId = int.Parse(ddlFrom.SelectedValue);
                PIncomeDate date = NewPayrollManager.GetIncomeDate(incomeId, dateId.Value);


                lblFrom.Text = "From date : " + (date.EffectiveFrom == null ? "" : date.EffectiveFrom);
            }

            PIncome income = new PayManager().GetIncomeById(incomeId);

            // If single then call to show in Row wise

            if (income.XType != null && income.XType != 0 && (income.YType == null || income.YType == 0))
            {
                BindForSingle(income);
                return;
            }

            ColumnFirst.Text = ((IncomeDefinedTypeEnum)income.XType).ToString();
            if (income.YType != null && income.YType != 0)
                ColumnFirst.Text += "/" + ((IncomeDefinedTypeEnum)income.YType).ToString();

            List<TextValue> xlist = NewPayrollManager.GetDefinedList(incomeId, true);
            List<TextValue> ylist = NewPayrollManager.GetDefinedList(incomeId, false);

            List<PIncomeDefinedAmount> values = NewPayrollManager.GetDefinedAmounts(incomeId, dateId);

            //List<PIncome> incomes = PayManager.GetVariableIncomes(ids,isDeduction);
            //List<GetEmployeesForVariableIncomesResult> employees = PayManager.GetEmployeeForVariableIncomes(strIncomes,isDeduction);
            //List<GetEmployeesIncomeForVariableIncomesResult> incomeList = PayManager.GetEmployeesIncomeForVariableIncomes(strIncomes,isDeduction);


            ArrayReader reader = (storeProjects.Reader[0] as ArrayReader);
            //ColumnModel columnModelProjects = gridProjects.ColumnModel;

            string columnId = "P_{0}";
            foreach (var project in xlist)
            {
                string gridIndexId = string.Format(columnId, project.Value);
                ModelField field = new ModelField(gridIndexId, ModelFieldType.Float);
                field.UseNull = true;
                //field.AllowBlank = true;
                ModelProject.Fields.Add(gridIndexId, ModelFieldType.Float);
                gridProjects.ColumnModel.Columns.Add(GetExtGridPanelColumn(gridIndexId, project.Text));
            }


            // Generate data array & bind to Store
            // Add each  id as key
            Dictionary<string, double?> selection = new Dictionary<string, double?>();
            foreach (var item in values)
            {
                selection.Add(item.XType + "_" + item.YType, item.Amount == null ? 0 : (double)item.Amount);
            }

            object[] data = new object[ylist.Count];

            // Iterate for each Project & Skip first as it represents All Project
            for (int i = 0; i < ylist.Count; i++)
            {
                //first one for EIN, second for Name & last for Total
                object[] rowData = new object[xlist.Count + 1];

                int columnIndex = 0;

                if (xlist.Count > 0)
                    rowData[columnIndex++] = ylist[i].Text;

                // Iterate for each Project
                for (int j = 0; j < xlist.Count; j++)
                {
                    if (selection.ContainsKey(xlist[j].Value + "_" + ylist[i].Value))
                    {
                        rowData[columnIndex++] = selection[xlist[j].Value + "_" + ylist[i].Value];
                    }
                    else
                    {
                        rowData[columnIndex++] = null;
                    }
                }

                data[i] = rowData;
            }

            storeProjects.DataSource = data;
            storeProjects.DataBind();
        }

        public bool IsEnable(object ein)
        {
            if (IsPayrollSaved)
                return false;
            return !LeaveAttendanceManager.IsAttendanceSavedForEmployee(payrollPeriodId, Convert.ToInt32(ein));
        }


        public bool IsPayrollSaved = false;
        public int payrollPeriodId;
        public void BindOvertimes()
        {
            BindList();

            if (IsPayrollSaved)
            {
                btnExport.Visible = false;
                divWarningMsg.InnerHtml = Resources.ResourceTooltip.HPLAllowanceSalaryAlreadyGenerated;
                divWarningMsg.Hide = false;
            }
            else
            {
                btnExport.Visible = true;
            }

        }

        protected void ddlPayrollPeriods_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindOvertimes();
        }

        protected void gvwOvertimes_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            BindOvertimes();
        }


        protected void btnSave_Click1(object sender, EventArgs e)
        {
        }

        public void btnSave_DirectClick(object sender, DirectEventArgs e)
        {
            BindList();
        }

        protected void chkVariableIncomes_SelectedIndexChanged(object sender, EventArgs e)
        {
            //string strIncomes = "";
            //foreach (System.Web.UI.WebControls.ListItem item in chkVariableIncomes.Items)
            //{
            //    if (item.Selected)
            //    {
            //        if (strIncomes == "")
            //            strIncomes = item.Value;
            //        else
            //            strIncomes += "," + item.Value;
            //    }
            //}

            //X.AddScript(string.Format("strIncomes ='{0}';", strIncomes));
            BindList();
        }
    }
}