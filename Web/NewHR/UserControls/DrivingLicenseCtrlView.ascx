﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DrivingLicenseCtrlView.ascx.cs"
    Inherits="Web.NewHR.UserControls.DrivingLicenseCtrlView" %>

<script type="text/javascript">

    var prepareDrivingLicenseDownload = function (grid, toolbar, rowIndex, record) {
        var downloadBtn = toolbar.items.get(1);
        if (record.data.ServerFileName == null || record.data.ServerFileName == '') {
            downloadBtn.setVisible(false);
        }
    }

    var CommandHandlerDLiFD = function (command, record) {
        <%= hdnDrivingLicenceIdValue.ClientID %>.setValue(record.data.DrivingLicenceId);

          <%= btnDownloadDLiFile.ClientID %>.fireEvent('click');
      };

</script>

<ext:Hidden ID="hdnDrivingLicenceIdValue" runat="server" />
<ext:Button runat="server" AutoPostBack="true" OnClick="btnDownloadDLiFile_Click"
    Hidden="true" ID="btnDownloadDLiFile" Text="<i></i>Download">
</ext:Button>

<ext:GridPanel StyleSpec="margin-top:15px;" Scroll="None" ID="GridDrivingLiscence"
    runat="server" Width="920" Cls="itemgrid">
    <Store>
        <ext:Store ID="StoreDrivingLiscence" runat="server">
            <Model>
                <ext:Model ID="Model1" runat="server" IDProperty="DrivingLicenceId">
                    <Fields>
                        <ext:ModelField Name="LiscenceTypeName" />
                        <ext:ModelField Name="DrivingLicenceNo" />
                        <ext:ModelField Name="DrivingLicenceIssueDate" />
                        <ext:ModelField Name="IssuingCountry" />
                        <ext:ModelField Name="Status" Type="Int" />
                        <ext:ModelField Name="ServerFileName" Type="String" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
    </Store>
    <ColumnModel>
        <Columns>
            <ext:Column ID="Column1" runat="server" Text="Type" DataIndex="LiscenceTypeName"
                Wrap="true" Width="150" Flex="1" />
            <ext:Column ID="Column2" runat="server" Text="Liscence Number" DataIndex="DrivingLicenceNo"
                Wrap="true" Width="150">
            </ext:Column>
            <ext:Column ID="colDrivingLicenceIssueDate" runat="server" Text="Issue Date" DataIndex="DrivingLicenceIssueDate" Wrap="true" Width="100">
            </ext:Column>
            <ext:Column ID="Column3" runat="server" Text="Issuing Country" DataIndex="IssuingCountry"
                Wrap="true" Width="200">
            </ext:Column>
            <ext:CommandColumn ID="CommandColumn1" runat="server" Width="40">
                <Commands>
                    <ext:CommandSeparator />
                    <ext:GridCommand Icon="PageWhitePut" CommandName="DownLoad">
                        <ToolTip Text="DownLoad" />
                    </ext:GridCommand>
                </Commands>
                <Listeners>
                    <Command Handler="CommandHandlerDLiFD(command,record);" />
                </Listeners>
                <PrepareToolbar Fn="prepareDrivingLicenseDownload" />
            </ext:CommandColumn>

            <ext:Column ID="Column4" runat="server" Text="" Width="280" />
        </Columns>
    </ColumnModel>
    <SelectionModel>
        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
    </SelectionModel>
</ext:GridPanel>
