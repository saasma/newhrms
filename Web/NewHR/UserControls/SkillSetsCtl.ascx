﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SkillSetsCtl.ascx.cs"
    Inherits="Web.NewHR.UserControls.SkillSetsCtl" %>

    <script type="text/javascript">

        var prepare = function (grid, toolbar, rowIndex, record) {

            var EditBtn = toolbar.items.get(0);
            var deleteBtn = toolbar.items.get(2);

            if (record.data.IsEditable == 0) {
                EditBtn.setVisible(false);
                deleteBtn.setVisible(false);
            }

        }
    
    </script>

<ext:Hidden runat="server" ID="hdnSkillSetsID">
</ext:Hidden>

    <div class="widget-body">
        <table class="fieldTable firsttdskip">
            <tr>
                <td>
                    <ext:GridPanel ID="GridSkillSets" runat="server" Width="460" Cls="itemgrid">
                        <Store>
                            <ext:Store ID="StoreSkillSets" runat="server">
                                <Model>
                                    <ext:Model ID="Model1" runat="server" IDProperty="SkillSetId">
                                        <Fields>
                                            <ext:ModelField Name="SkillSetName" Type="string" />
                                            <ext:ModelField Name="LevelOfExpertise" Type="String" />
                                            <ext:ModelField Name="IsEditable" Type="Int" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:Column ID="Column1" runat="server" Text="Skill" DataIndex="SkillSetName" Width="200" />
                                <ext:Column ID="Column2" runat="server" Text="Expertise Level" DataIndex="LevelOfExpertise"
                                    Width="150">
                                </ext:Column>
                                <ext:CommandColumn ID="CommandColumn1" runat="server" Width="110">
                                    <Commands>
                                        <ext:GridCommand Text="<i></i>" Cls="btn-action glyphicons pencil btn-success" CommandName="Edit">
                                            <ToolTip Text="Edit" />
                                        </ext:GridCommand>
                                        <ext:CommandSeparator />
                                        <ext:GridCommand Text="<i></i>" Cls="btn-action glyphicons remove_2 btn-danger" CommandName="Delete">
                                            <ToolTip Text="Delete" />
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridSkillSets_Command">
                                            <EventMask ShowMask="true">
                                            </EventMask>
                                            <ExtraParams>
                                                <ext:Parameter Name="ID" Value="record.data.SkillSetId" Mode="Raw">
                                                </ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw">
                                                </ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                    <PrepareToolbar fn="prepare" />
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <SelectionModel>
                            <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                        </SelectionModel>
                    </ext:GridPanel>
                </td>
            </tr>
        </table>
        <div class="buttonBlockSection">
            <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="LinkButton1" Cls="btn btn-primary btn-sect"
                BaseCls="btnFlat" Text="<i></i>Add New Skill">
                <DirectEvents>
                    <Click OnEvent="btnAddNewLine_Click">
                        <EventMask ShowMask="true" />
                    </Click>
                </DirectEvents>
            </ext:LinkButton>
        </div>
    </div>

<ext:Window ID="AESkillSetsWindow" runat="server" Title="Add/Edit SkillSets" Icon="Application"
    Height="400" Width="600"  BodyPadding="5"
    Hidden="true" Modal="true">
    <Content>
        <table class="fieldTable">
            <tr>
                <td>
                    <ext:ComboBox ID="cmbSkill" runat="server" ValueField="SkillSetId" DisplayField="Name"
                        FieldLabel="Skill" LabelAlign="top" LabelSeparator="" QueryMode="Local">
                        <Store>
                            <ext:Store ID="Store2" runat="server">
                                <Model>
                                    <ext:Model ID="Model4" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="SkillSetId" />
                                            <ext:ModelField Name="Name" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                </td>
                <td>
                    <ext:TextField ID="txtNewSkill" runat="server" FieldLabel="Add a new skill" LabelAlign="top"
                        LabelSeparator="" />
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <ext:Checkbox ID="chkAddtoSkillPool" runat="server" FieldLabel="Add to Skill Pool"
                        LabelSeparator="">
                    </ext:Checkbox>
                </td>
            </tr>
            <tr>
                <td>
                    <table class="fieldTable firsttdskip">
                        <tr>
                            <td>
                                <ext:ComboBox ID="cmbLevelOfExpertise" runat="server" ValueField="LevelID" DisplayField="Name"
                                    Width="150" FieldLabel="Level of Expertise" LabelAlign="top" LabelSeparator=""
                                    ForceSelection="true" QueryMode="Local">
                                    <Store>
                                        <ext:Store ID="Store1" runat="server">
                                            <Model>
                                                <ext:Model ID="Model2" runat="server">
                                                    <Fields>
                                                        <ext:ModelField Name="LevelID" />
                                                        <ext:ModelField Name="Name" />
                                                    </Fields>
                                                </ext:Model>
                                            </Model>
                                        </ext:Store>
                                    </Store>
                                </ext:ComboBox>
                                <%-- <asp:RequiredFieldValidator Display="None" ID="valcmbLevelOfExpertise" runat="server"
                                    ValidationGroup="SkillSetsSaveUpdate" ControlToValidate="cmbLevelOfExpertise"
                                    ErrorMessage="Level of Expertise is required." />--%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <ext:TextArea ID="txtNote" runat="server" FieldLabel="Notes" LabelSeparator="" LabelAlign="Top"
                        Rows="3" Cols="50" />
                </td>
            </tr>
            <tr>
                <td valign="bottom" colspan="2">
                    <div class="popupButtonDiv">
                        <ext:LinkButton runat="server" Cls="btnFlat" BaseCls="btnFlat" StyleSpec="padding:0px;"
                            ID="btnSave" Text="<i></i>Save">
                            <DirectEvents>
                                <Click OnEvent="btnSave_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                            <Listeners>
                                <Click Handler="valGroup = 'SkillSetsSaveUpdate'; if(CheckValidation()) return this.disable(); else return false;">
                                </Click>
                            </Listeners>
                        </ext:LinkButton>
                        <div class="btnFlatOr">
                            or</div>
                        <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="LinkButton2" Cls="btnFlatLeftGap"
                            Text="<i></i>Cancel">
                            <Listeners>
                                <Click Handler="#{AESkillSetsWindow}.hide();">
                                </Click>
                            </Listeners>
                        </ext:LinkButton>
                    </div>
                </td>
            </tr>
        </table>
    </Content>
</ext:Window>
