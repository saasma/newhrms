﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Entity;
using BLL;
using System.IO;
using Utils;
using System.Text;
using BLL.BO;
using Web.Helper;

namespace Web.NewHR.UserControls
{
    public partial class DocumentCommentCtrl : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
                Initialise();
        }

        private void Initialise()
        {
            if (!string.IsNullOrEmpty(Request.QueryString["DocumentId"]))
            {
                hdnCommentDocumentID.Text = Request.QueryString["DocumentId"];
                BindComments();
            }

            if (!DocumentManager.CanUserAddEditDocument())
            {
                btnSaveComment.Hide();
                txtComment.Hide();
            }
        }

        private void BindComments()
        {
          

            string html = " <ul class='media-list msg-list'>";
            if (!string.IsNullOrEmpty(hdnCommentDocumentID.Text.Trim()))
            {
                Guid documentId = new Guid(hdnCommentDocumentID.Text.Trim());

                int gender = 2;


                List<DocComment> listDocComments = BLL.BaseBiz.PayrollDataContext.DocComments.Where(x => x.DocumentRef_ID == documentId).OrderByDescending(x => x.CommentedDate).ToList();

                foreach (var item in listDocComments)
                {


                    item.TimeDetail = WebHelper.TimeAgoWithMinSecond(item.CommentedDate);

                    UUser objUUser = UserManager.GetUserByUserID(item.CommentedBy);
                    if (objUUser.EmployeeId != null)
                    {
                        EEmployee emp = EmployeeManager.GetEmployeeById(objUUser.EmployeeId.Value);
                        gender = (emp.Gender == null ? 2 : emp.Gender.Value);
                        if (!string.IsNullOrEmpty(emp.HHumanResources[0].UrlPhotoThumbnail))
                            item.imageUrl = Page.ResolveUrl(Config.UploadLocation + "/" + emp.HHumanResources[0].UrlPhotoThumbnail);
                        else
                            item.imageUrl = Page.ResolveUrl(Web.Helper.WebHelper.GetEmployeeNoImage(emp.EmployeeId));
                    }
                    else
                        item.imageUrl = Page.ResolveUrl(Web.Helper.WebHelper.GetEmployeeNoImage(0));


                    html += string.Format(@" <li class='media'>   
                                                <div class='pull-left'>
                                                    <img class='media-object img-circle img-online' src='{0}' />
                                                </div>         
                                                <div class='media-body'>
                                                    <div class='pull-right media-option'>
                                                        <span>{1}</span>
                                                    </div> 
                                                    <h4 class='sender'><span>{2}</span></h4>
                                                    <p>
                                                        <span>{3}</span>
                                                    </p>
                                                </div>
                                            </li> ",
                                                   item.imageUrl,
                                                   item.TimeDetail,
                                                   objUUser.UserName,
                                                   item.Comments);
                }

                html += "</ul>";

                dispComments.Html = html;
            }
        }

        protected void btnSaveComment_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveComment");
            if (Page.IsValid)
            {
                DocComment obj = new DocComment();
                obj.CommentID = Guid.NewGuid();
                obj.DocumentRef_ID = new Guid(Request.QueryString["DocumentId"]);
                obj.CommentedBy = SessionManager.CurrentLoggedInUserID;
                obj.CommentedDate = BLL.BaseBiz.GetCurrentDateAndTime();
                obj.Comments = txtComment.Text.Trim();

                UUser objUUser = UserManager.GetUserByUserID(SessionManager.CurrentLoggedInUserID);
                if (objUUser.EmployeeId != null)
                    obj.CommenterName = EmployeeManager.GetEmployeeById(objUUser.EmployeeId.Value).Name;
                else
                    obj.CommenterName = objUUser.UserName;

                Status status = DocumentManager.SaveDocumentComment(obj);
                if (status.IsSuccess)
                {
                    txtComment.Clear();
                    BindComments();
                }
                else
                    NewMessage.ShowWarningMessage("Error while saving comment.");

            }

        }

    }
}