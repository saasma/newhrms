﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;
using Utils;

namespace Web.NewHR.UserControls
{
    public partial class HealthCtrlView : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                lblHandicapped.Text = CommonManager.GetHandicappedName;
                Initialise();
            }
        }

        protected void Initialise()
        {
            int employeeId = GetEmployeeID();

            LoadHandicapppedDetail(employeeId);
            this.LoadHealthGrid(employeeId);
        }

        protected int GetEmployeeID()
        {
            int EmpID = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                EmpID = int.Parse(Request.QueryString["ID"]);
            }
            return EmpID;
        }

        private void LoadHandicapppedDetail(int employeeId)
        {
            EEmployee _EEmployee = NewHRManager.Instance.GetHandicappedDetailsByEmployee(employeeId);
            if (_EEmployee != null && _EEmployee.IsHandicapped != null && _EEmployee.IsHandicapped.Value)
                lblHandicappedValue.Text = "Yes";
        }

        protected void LoadHealthGrid(int employeeId)
        {
            List<HHealth> _HHealth = NewHRManager.GetHealthByEmployeeID(employeeId);
            this.StoreHealth.DataSource = _HHealth;
            this.StoreHealth.DataBind();

            if ( _HHealth.Count <= 0)
                GridHealth.Hide();

        }

    }
}