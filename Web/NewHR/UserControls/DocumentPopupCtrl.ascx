﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DocumentPopupCtrl.ascx.cs" Inherits="Web.NewHR.UserControls.DocumentPopupCtrl" %>

<ext:Hidden runat="server" ID="hiddenValue" />

<div class="bevel">
                        <div class="fields">
                            <table>

                                   <tr style="height: 30px;">
                                    <td style='width: 100px!important'>
                                        Type
                                    </td>
                                    <td>
                                        <ext:ComboBox ForceSelection="true" DisplayField="DocumentTypeName" ValueField="DocumentTypeId" QueryMode="Local"
                                            Width="300" ID="cmbType" runat="server">
                                            <Store>
                                                <ext:Store ID="Store1" runat="server">
                                                    <Model>
                                                        <ext:Model ID="Model1" runat="server">
                                                            <Fields>
                                                                <ext:ModelField Name="DocumentTypeId" Type="String" />
                                                                <ext:ModelField Name="DocumentTypeName" Type="string" />
                                                            </Fields>
                                                        </ext:Model>
                                                    </Model>
                                                </ext:Store>
                                            </Store>
                                            <SelectedItems>
                                                <ext:ListItem Index="0" />
                                            </SelectedItems>
                                        </ext:ComboBox>
                                    </td>
                                </tr>

                                <tr id="rowDesc" runat="server">
                                    <td style='width: 60px!important; vertical-align: top'>
                                        Description
                                    </td>
                                    <td>
                                        <%--<asp:TextBox Width="200px"  Rows="3" ID="txtDesc" runat="server" TextMode="MultiLine" />--%>
                                        <ext:TextArea Width="300" Height="50" ID="txtDesc" runat="server">
                                        </ext:TextArea>
                                    </td>
                                </tr>
                                <tr style="height: 50px;">
                                    <td style='width: 100px!important'>
                                        Select File
                                    </td>
                                    <td>
                                        <%--<asp:FileUpload ID="fup" Width="300px" CssClass="file" runat="server" />--%>
                                        <ext:FileUploadField ID="fup" runat="server" Width="300" Icon="Attach" />
                                        <asp:RequiredFieldValidator ValidationGroup="SaveDocument" ControlToValidate="fup"
                                            Display="None" ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please select a file."></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                             
                                <tr style="width: 300px;">
                                    <td valign="bottom" colspan="2">
                                        <div class="popupButtonDiv">
                                            <ext:Button runat="server" ID="btnPositionSaveUpdate" Cls="btn btn-primary" Text="<i></i>Save"
                                                runat="server">
                                                <DirectEvents>
                                                    <Click OnEvent="btnLevelSaveUpdate_Click">
                                                        <EventMask ShowMask="true" />
                                                    </Click>
                                                </DirectEvents>
                                                <Listeners>
                                                    <Click Handler="valGroup = 'SaveDocument';  if(CheckValidation()) return this.disable(); else return false;">
                                                    </Click>
                                                </Listeners>
                                            </ext:Button>
                                            <div class="btnFlatOr">
                                                or
                                            </div>
                                            <ext:LinkButton runat="server" Cls="btnFlatLeftGap" StyleSpec="padding:0px;" ID="LinkButton4"
                                                Text="<i></i>Cancel" runat="server">
                                                <Listeners>
                                                    <Click Handler="#{WDocument}.hide();">
                                                    </Click>
                                                </Listeners>
                                            </ext:LinkButton>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <%--<asp:ValidationSummary runat="server" ShowSummary="false" ShowMessageBox="true" />--%>
                        </div>
                    </div>