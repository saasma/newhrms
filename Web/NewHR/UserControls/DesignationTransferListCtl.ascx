﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DesignationTransferListCtl.ascx.cs"
    Inherits="Web.NewHR.UserControls.DesignationTransferListCtl" %>
<ext:GridPanel ID="GridLevels" runat="server">
    <Store>
        <ext:Store ID="Store3" runat="server">
            <Model>
                <ext:Model ID="Model4" runat="server">
                    <Fields>
                        <ext:ModelField Name="EmployeeName" Type="string" />
                        <ext:ModelField Name="FromDate" Type="string" />
                        <ext:ModelField Name="FromDesignation" Type="string" />
                        <ext:ModelField Name="ToDesignation" Type="string" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
    </Store>
    <ColumnModel>
        <Columns>
            <ext:Column ID="Column4" Width="100" Sortable="false" MenuDisabled="true" runat="server"
                Text="From Date" Align="Left" DataIndex="FromDate">
            </ext:Column>
            <ext:Column ID="Column2" Width="150" Sortable="false" MenuDisabled="true" runat="server"
                Text="From Designation" Align="Left" DataIndex="FromDesignation">
            </ext:Column>
            <ext:Column ID="Column3" Width="150" Sortable="false" MenuDisabled="true" runat="server"
                Text="To Designation" Align="Left" DataIndex="ToDesignation">
            </ext:Column>
        </Columns>
    </ColumnModel>
    <SelectionModel>
        <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
    </SelectionModel>
</ext:GridPanel>
