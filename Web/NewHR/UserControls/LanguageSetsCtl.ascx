﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LanguageSetsCtl.ascx.cs"
    Inherits="Web.NewHR.UserControls.LanguageSetsCtl" %>

    <script type="text/javascript">

        var prepare = function (grid, toolbar, rowIndex, record) {

            var editBtn = toolbar.items.get(0);
            var deleteBtn = toolbar.items.get(2);

            if (record.data.IsEditable == 0) {
                editBtn.setVisible(false);
                deleteBtn.setVisible(false);
            }

        }
    
    </script>


<ext:Hidden runat="server" ID="hdnLanguageSetsID">
</ext:Hidden>

    <div class="widget-body">
        <table class="fieldTable firsttdskip">
            <tr>
                <td>
                    <ext:GridPanel ID="GridLanguageSets" runat="server" Width="570">
                        <Store>
                            <ext:Store ID="StoreLanguageSets" runat="server">
                                <Model>
                                    <ext:Model ID="Model1" runat="server" IDProperty="LanguageSetId">
                                        <Fields>
                                            <ext:ModelField Name="LanguageSetName" Type="string" />
                                            <ext:ModelField Name="FluencySpeakName" Type="String" />
                                            <ext:ModelField Name="FluencyWriteName" Type="String" />
                                            <ext:ModelField Name="IsEditable" Type="Int" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:Column ID="Column1" runat="server" Text="Language" DataIndex="LanguageSetName"
                                    Width="200" Flex="1" />
                                <ext:Column ID="Column2" runat="server" Text="Fluency : Speaking" DataIndex="FluencySpeakName"
                                    Width="150" />
                                <ext:Column ID="Column3" runat="server" Text="Fluency : Writing" DataIndex="FluencyWriteName"
                                    Width="150" />
                                <ext:CommandColumn ID="CommandColumn1" runat="server" Width="70">
                                    <Commands>
                                        <ext:GridCommand Text="<i></i>" Cls="btn-action glyphicons pencil btn-success" CommandName="Edit">
                                            <ToolTip Text="Edit" />
                                        </ext:GridCommand>
                                        <ext:CommandSeparator />
                                        <ext:GridCommand Text="<i></i>" Cls="btn-action glyphicons remove_2 btn-danger" CommandName="Delete">
                                            <ToolTip Text="Delete" />
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridLanguageSets_Command">
                                            <EventMask ShowMask="true">
                                            </EventMask>
                                            <ExtraParams>
                                                <ext:Parameter Name="ID" Value="record.data.LanguageSetId" Mode="Raw">
                                                </ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw">
                                                </ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                    <PrepareToolbar Fn="prepare" />
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <SelectionModel>
                            <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                        </SelectionModel>
                    </ext:GridPanel>
                </td>
            </tr>
        </table>
        <div class="buttonBlock">
            <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="btnAddNewLine" Cls="btnFlat"
                BaseCls="btnFlat" Text="<i></i>Add New Line">
                <DirectEvents>
                    <Click OnEvent="btnAddNewLine_Click">
                        <EventMask ShowMask="true" />
                    </Click>
                </DirectEvents>
            </ext:LinkButton>
        </div>
    </div>

<ext:Window ID="AELanguageSetsWindow" runat="server" Title="Add/Edit Language" Icon="Application"
    Height="350" Width="450"  BodyPadding="10"
    Hidden="true" Modal="true">
    <Content>
        <table class="fieldTable">
            <td>
                <ext:ComboBox ID="cmbLanguage" runat="server" ValueField="LanguageSetId" DisplayField="Name"
                    FieldLabel="Select Language" LabelAlign="top" LabelSeparator="" QueryMode="Local">
                    <Store>
                        <ext:Store ID="Store2" runat="server">
                            <Model>
                                <ext:Model ID="Model4" runat="server">
                                    <Fields>
                                        <ext:ModelField Name="LanguageSetId" />
                                        <ext:ModelField Name="Name" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                    </Store>
                </ext:ComboBox>
            </td>
            <td>
                <ext:TextField ID="txtNewLanguage" runat="server" FieldLabel="Add a new Language"
                    LabelAlign="top" LabelSeparator="" />
            </td>
            </tr>
            <tr>
                <td style="padding-top: 10px; padding-bottom: 10px;">
                    <ext:Checkbox ID="chkIsNativeLanguage" runat="server" FieldLabel="Native Language"
                        LabelAlign="Right" LabelSeparator="">
                    </ext:Checkbox>
                </td>
                <td>
                    <ext:Checkbox ID="chkAddtoLanguagePool" runat="server" FieldLabel="Add to Languages"
                        Width="200" LabelAlign="Right" LabelWidth="105" LabelSeparator="">
                    </ext:Checkbox>
                </td>
            </tr>
            <tr>
                <td>
                    <ext:ComboBox ID="cmbFluencySpeak" runat="server" ValueField="LevelID" DisplayField="Name"
                        Width="150" FieldLabel="Fluency: Speaking" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                        QueryMode="Local">
                        <Items>
                            <ext:ListItem Text="Fluent" Value="1" />
                            <ext:ListItem Text="Excellent" Value="2" />
                            <ext:ListItem Text="Good" Value="3" />
                            <ext:ListItem Text="Understand" Value="4" />
                        </Items>
                        <%-- <Store>
                                                    <ext:Store ID="Store1" runat="server">
                                                        <Model>
                                                            <ext:Model ID="Model2" runat="server">
                                                                <Fields>
                                                                    <ext:ModelField Name="LevelID" />
                                                                    <ext:ModelField Name="Name" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                    </ext:Store>
                                                </Store>--%>
                    </ext:ComboBox>
                    <asp:RequiredFieldValidator Display="None" ID="valcmbFluencySpeak" runat="server"
                        ValidationGroup="LanguageSetsSaveUpdate" ControlToValidate="cmbFluencySpeak"
                        ErrorMessage="Please check fluency in speaking." />
                </td>
                <td>
                    <ext:ComboBox ID="cmbFluencyWrite" runat="server" ValueField="LevelID" DisplayField="Name"
                        Width="150" FieldLabel="Fluency: Writing" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                        QueryMode="Local">
                        <Items>
                            <ext:ListItem Text="Excellent" Value="1" />
                            <ext:ListItem Text="Very Good" Value="2" />
                            <ext:ListItem Text="Good" Value="3" />
                            <ext:ListItem Text="Average" Value="4" />
                            <ext:ListItem Text="None" Value="5" />
                        </Items>
                        <%-- <Store>
                                                    <ext:Store ID="Store1" runat="server">
                                                        <Model
                                                            <ext:Model ID="Model2" runat="server">
                                                                <Fields>
                                                                    <ext:ModelField Name="LevelID" />
                                                                    <ext:ModelField Name="Name" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                    </ext:Store>
                                                </Store>--%>
                    </ext:ComboBox>
                    <asp:RequiredFieldValidator Display="None" ID="valcmbFluencyWrite" runat="server"
                        ValidationGroup="LanguageSetsSaveUpdate" ControlToValidate="cmbFluencyWrite"
                        ErrorMessage="Please check fluency in writing." />
                </td>
            </tr>
            <tr>
                <td valign="bottom" colspan="2">
                    <div class="popupButtonDiv">
                        <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="btnSave" Cls="btnFlat"
                            BaseCls="btnFlat" Text="<i></i>Save">
                            <DirectEvents>
                                <Click OnEvent="btnSave_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                            <Listeners>
                                <Click Handler="valGroup = 'LanguageSetsSaveUpdate'; return CheckValidation();">
                                </Click>
                            </Listeners>
                        </ext:LinkButton>
                        <div class="btnFlatOr">
                            or
                        </div>
                        <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="LinkButton2" Cls="btnFlatLeftGap"
                            Text="<i></i>Cancel">
                            <Listeners>
                                <Click Handler="#{AELanguageSetsWindow}.hide();">
                                </Click>
                            </Listeners>
                        </ext:LinkButton>
                    </div>
                </td>
            </tr>
        </table>
    </Content>
</ext:Window>
