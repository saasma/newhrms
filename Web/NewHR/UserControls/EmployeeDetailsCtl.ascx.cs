﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;

using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;

namespace Web.NewHR.UserControls
{
    public partial class EmployeeDetailsCtl : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();

            }
        }

        protected void Initialise()
        {
            if (string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                return;
            }

            int EmployeeID = GetEmployeeID();


            EEmployee emp = EmployeeManager.GetEmployeeById(EmployeeID);

            //empID.Text = emp.EmployeeId.ToString();
            name.InnerHtml = emp.EmployeeId + " - " + emp.Name;

            if (!string.IsNullOrEmpty(emp.EHumanResources[0].IdCardNo))
            {
                name.InnerHtml += " (I No : " + emp.EHumanResources[0].IdCardNo + ")";
            }

            branchDepartment.InnerHtml = emp.Branch.Name + ", " + emp.Department.Name;
            position.InnerHtml = emp.EDesignation.Name;
            //if (emp.BranchId != null)
            //    empBranch.Text = emp.Branch.Name;
            //if (emp.Department != null)
            //    empDep.Text = emp.Department.Name;

            if (emp.PEmployeeIncomes.Count > 0 && emp.PEmployeeIncomes[0].LevelId != null)
            {
                level.InnerHtml += "Level : " + NewPayrollManager.GetLevelById(emp.PEmployeeIncomes[0].LevelId.Value).Name;
                BLevel actingLevel = NewPayrollManager.GetActingLevelInTodayDate(emp.EmployeeId);
                if(actingLevel != null)
                {
                    level.InnerHtml += ", Acting Level : " + actingLevel.Name;
                }
            }
            //else
            //{
            //    empLevel.Hide();
            //}
        }

      


        protected int GetEmployeeID()
        {
            //Guid? AssetID = null;
            int EmpID = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                EmpID = int.Parse(Request.QueryString["ID"]);
            }
            return EmpID;
        }

      


    }
}