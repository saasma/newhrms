﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ActingEmployeeListCtl.ascx.cs"
    Inherits="Web.NewHR.UserControls.ActingEmployeeListCtl" %>
<ext:Hidden runat="server" ID="hiddenValue" />
<ext:GridPanel StyleSpec="margin-top:15px;" ID="GridLevels" runat="server" Cls="itemgrid" Width="1000">
    <Store>
        <ext:Store ID="Store3" runat="server">
            <Model>
                <ext:Model ID="Model4" runat="server" IDProperty="ActingID">
                    <Fields>
                        <ext:ModelField Name="ActingID" Type="String" />
                        <ext:ModelField Name="ApplicableFrom" Type="string" />
                        <ext:ModelField Name="ApplicableTill" Type="string" />
                        <ext:ModelField Name="FromLevel" Type="string" />
                        <ext:ModelField Name="ToLevel" Type="string" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
    </Store>
    <ColumnModel>
        <Columns>
            <ext:Column ID="Column2" Width="120" Sortable="false" MenuDisabled="true" runat="server"
                Text="Applicable From" Align="Left" DataIndex="ApplicableFrom">
            </ext:Column>
            <ext:Column ID="Column3" Width="120" Sortable="false" MenuDisabled="true" runat="server"
                Text="Applicable Till" Align="Left" DataIndex="ApplicableTill">
            </ext:Column>
            <ext:Column ID="Column1" Width="130" Sortable="false" MenuDisabled="true" runat="server"
                Text="From Level" Align="Left" DataIndex="FromLevel">
            </ext:Column>
            <ext:Column ID="Column4" Width="130" Sortable="false" MenuDisabled="true" runat="server"
                Text="To Level" Align="Left" DataIndex="ToLevel">
            </ext:Column>
            <ext:Column ID="Column5" Width="500" Sortable="false" MenuDisabled="true" runat="server"
                Text="" Align="Left">
            </ext:Column>
        </Columns>
    </ColumnModel>
    <SelectionModel>
        <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
    </SelectionModel>
</ext:GridPanel>
