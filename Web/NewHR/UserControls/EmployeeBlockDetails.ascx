﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployeeBlockDetails.ascx.cs"
    Inherits="Web.NewHR.UserControls.EmployeeBlockDetails" %>
<table style='clear: both; padding: 10px; margin-top: 10px;'>
    <tr>
        <td valign="top" style="padding-left: 10px;">
            <ext:Image ID="image" runat="server" ImageUrl="~/images/sample.jpg" Width="150px"
                Height="150px" />
        </td>
        <td valign="top" style="padding-left: 50px" class="items">
            <ext:DisplayField ID="lblName" StyleSpec="font-weight:bold" Cls="empName" runat="server" />
            <ext:DisplayField ID="lblINo" runat="server" />
            <ext:DisplayField ID="lblBranch" runat="server" />
            <ext:DisplayField ID="lblDepartment" runat="server" />
            <ext:DisplayField ID="lblSince" runat="server" />
            <ext:DisplayField ID="lblTime" runat="server" />
        </td>
    </tr>
</table>
