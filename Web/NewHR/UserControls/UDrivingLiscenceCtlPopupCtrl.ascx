﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UDrivingLiscenceCtlPopupCtrl.ascx.cs" Inherits="Web.NewHR.UserControls.UDrivingLiscenceCtlPopupCtrl" %>

<ext:Hidden runat="server" ID="hdnDrivingLiscenceID">
</ext:Hidden>

<table class="fieldTable" border="0">
    <tr>
        <td>
            <span style="padding-bottom: 50px; font-weight: bold">Driving Licence Type</span>
            <ext:Checkbox ID="chkTwoWheeler" runat="server" FieldLabel="Two Wheeler" LabelAlign="Left"
                LabelSeparator="">
            </ext:Checkbox>
            <ext:Checkbox ID="chkFourWheeler" runat="server" FieldLabel="Four Wheeler" LabelAlign="Left"
                LabelSeparator="">
            </ext:Checkbox>
            <ext:Checkbox ID="chkHeavy" runat="server" FieldLabel="Heavy Vehicle" LabelAlign="Left" LabelSeparator="">
            </ext:Checkbox>
            <%--  <ext:MultiCombo  runat="server" Width="260" ID="cmbLiscenceType" LabelAlign="Top" LabelSeparator="" FieldLabel="Type">
                                    <Items>
                                        <ext:ListItem Text="Two Wheeler" Value="1" />
                                        <ext:ListItem Text="Four Wheeler" Value="2" />
                                        <ext:ListItem Text="Heavy" Value="3" />
                                    </Items>
                                </ext:MultiCombo>--%>
        </td>
    </tr>
    <tr>
        <td>
            <ext:TextField ID="txtDrivingLiscenceNumber" runat="server" FieldLabel="Driving Licence Number"
                Width="180px" LabelAlign="top" LabelSeparator="" />
            <asp:RequiredFieldValidator Display="None" ID="valtxtDrivingLiscenceNumber" runat="server"
                ValidationGroup="DrivingLiscenceSaveUpdate" ControlToValidate="txtDrivingLiscenceNumber"
                ErrorMessage="Please enter Driving Liscence Number." />
        </td>
    </tr>
    <tr>
        <td style="padding-top: 5px;">
            <ext:ComboBox ID="cmbIssuingCountry" Width="180px" runat="server" ValueField="CountryName"
                DisplayField="CountryName" FieldLabel="Issuing Country" LabelAlign="top" LabelSeparator=""
                ForceSelection="true" QueryMode="Local">
                <Store>
                    <ext:Store ID="Store2" runat="server">
                        <Model>
                            <ext:Model ID="Model4" runat="server">
                                <Fields>
                                    <ext:ModelField Name="CountryName" />
                                    <ext:ModelField Name="CountryName" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
            </ext:ComboBox>
            <asp:RequiredFieldValidator Display="None" ID="valcmbIssuingCountry" runat="server"
                ValidationGroup="DrivingLiscenceSaveUpdate" ControlToValidate="cmbIssuingCountry"
                ErrorMessage="Issuing Country is required." />
        </td>
        <td>
            <ext:TextField runat="server" ID="txtLissueDate" FieldLabel="Licence Issue Date"
                Width="180px" LabelAlign="top" LabelSeparator="">
            </ext:TextField>
            <asp:RequiredFieldValidator Display="None" ID="rfvtxtLissueDate" runat="server"
                ValidationGroup="DrivingLiscenceSaveUpdate" ControlToValidate="txtLissueDate"
                ErrorMessage="Please Enter Driving Liscence Issue Date." />
        </td>
    </tr>
    <tr>
        <td>
            <ext:FileUploadField ID="FileDrivingLiscenceDocumentUpload" runat="server" Width="200"
                Icon="Attach" FieldLabel="&nbsp;" LabelAlign="Top" LabelSeparator="" />
            <%--<span style="color: #ACACAC; padding-top: 20px;">*(only doc,pdf,txt,docx,rar)</span>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationExpression="^.*\.(doc|DOC|pdf|PDF|txt|TXT|docx|DOCX|rar|RAR)$"
                        runat="server" ValidationGroup="DrivingLiscenceSaveUpdate" Display="None" ControlToValidate="FileDrivingLiscenceDocumentUpload"
                        ErrorMessage="Invalid file format."></asp:RegularExpressionValidator>--%>
        </td>
        <td valign="bottom">
            <table>
                <tr>
                    <td>
                        <ext:Label ID="lblUploadedFile" runat="server" Hidden="true">
                        </ext:Label>
                    </td>
                    <td>
                        <ext:LinkButton Icon="Delete" runat="server" ID="lnkDeleteFile" OnDirectClick="lnkDeleteFile_Click"
                            Text="Delete file" Hidden="true">
                            <DirectEvents>
                                <Click OnEvent="lnkDeleteFile_Click">
                                    <Confirmation ConfirmRequest="true" Message="Do you want to delete the file?" />
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                        </ext:LinkButton>
                    </td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td valign="bottom" colspan="2">
            <div class="popupButtonDiv">
                <ext:Button runat="server" ID="btnSave" Cls="btn btn-primary" Text="<i></i>Save">
                    <DirectEvents>
                        <Click OnEvent="btnSave_Click">
                            <EventMask ShowMask="true" />
                        </Click>
                    </DirectEvents>
                    <Listeners>
                        <Click Handler="valGroup = 'DrivingLiscenceSaveUpdate'; return CheckValidation();">
                        </Click>
                    </Listeners>
                </ext:Button>
                <div class="btnFlatOr">
                    or
                </div>

                <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="LinkButton1" Cls="btnFlatLeftGap"
                    Text="<i></i>Cancel">
                    <Listeners>
                        <Click Handler="#{AEDrivingLiscenceWindow}.hide();">
                        </Click>
                    </Listeners>
                </ext:LinkButton>

            </div>
        </td>
    </tr>
</table>
