﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using DAL;
using BLL;
using Utils.Helper;

namespace Web.NewHR.UserControls
{
    public partial class ManageProjectsCtrl : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialise();
            }

            if (Request.Form["__EVENTTARGET"] != null && Request.Form["__EVENTTARGET"].Equals("Reload"))
                BindUsers();
           
            JavascriptHelper.AttachPopUpCode(Page, "popupProject", "ProjectDetailsPopup.aspx", 620, 500);
        }

        private void Initialise()
        {
            calFilterFrom.IsEnglishCalendar = IsEnglish;
            calFilterTo.IsEnglishCalendar = IsEnglish;

            JavascriptHelper.AttachEnableDisablingJSCode(chkAllProjects, calFilterFrom.ClientID + "_date", true, "filterCallback");

            var currentYear = SessionManager.CurrentCompanyFinancialDate;
            calFilterFrom.SetSelectedDate(currentYear.StartingDate, IsEnglish);
            calFilterTo.SetSelectedDate(currentYear.EndingDate, IsEnglish);

            BindUsers();
        }

        private void BindUsers()
        {
            var totalRecords = 0;
            var list =
                ProjectManager.GetProjectList(
                GetEngDate(calFilterFrom.SelectedDate.ToString()), GetEngDate(calFilterTo.SelectedDate.ToString()),
                chkAllProjects.Checked, txtSearch.Text.Trim(), pagintCtl.CurrentPage, int.Parse(pagintCtl.DDLRecords.SelectedValue), ref totalRecords);

            gvwProjects.DataSource = list;
            gvwProjects.DataBind();

            pagintCtl.UpdatePagingBar(totalRecords);
        }

        protected void gvwRoles_PageIndexChanged(object sender, EventArgs e)
        {
        }

        protected void btnLoad_Click(object sender, EventArgs e)
        {
            pagintCtl.CurrentPage = 1;
            BindUsers();
        }


        protected void btnPrevious_Click()
        {
            pagintCtl.CurrentPage -= 1;
            BindUsers();
        }

        protected void btnNext_Click()
        {
            pagintCtl.CurrentPage += 1;
            BindUsers();
        }


        protected void ddlRecords_SelectedIndexChanged()
        {
            pagintCtl.CurrentPage = 1;
            BindUsers();
        }

        public bool CanChange(object value)
        {
            return Convert.ToBoolean(value);
        }


        protected void gvwRoles_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            BindUsers();
        }
        protected void ddlUserType_SelectedIndex(object sender, EventArgs e)
        {
        }

        protected void gvwRoles_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            var projectId = (int)gvwProjects.DataKeys[e.RowIndex]["ProjectId"];

            if (ProjectManager.DeleteProject(projectId))
            {
                msgInfo.InnerHtml = "Project deleted.";
                msgInfo.Hide = false;

                gvwProjects.SelectedIndex = -1;
                BindUsers();
            }
            else
            {
                msgWarning.InnerHtml = "Project is in use, can't be deleted.";
                msgWarning.Hide = false;
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (chkAllProjects.Checked)
            {
                calFilterFrom.Enabled = false;
                calFilterTo.Enabled = false;
            }
            else
            {
                calFilterFrom.Enabled = true;
                calFilterTo.Enabled = true;
            }
        }
    }
}