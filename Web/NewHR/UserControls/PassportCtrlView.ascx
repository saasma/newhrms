﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PassportCtrlView.ascx.cs"
    Inherits="Web.NewHR.UserControls.PassportCtrlView" %>

<script type="text/javascript">

    var prepareDownloadPassport = function (grid, toolbar, rowIndex, record) {
        var downloadBtn = toolbar.items.get(1);
        if (record.data.ServerFileName == null || record.data.ServerFileName == '') {
            downloadBtn.setVisible(false);
        }
    }


      var CommandHandlerPassFD = function (command, record) {
            <%= hdnPassportIdValue.ClientID %>.setValue(record.data.PassportId);

            <%= btnDownloadPassFile.ClientID %>.fireEvent('click');
        };
   

</script>

<ext:Hidden ID="hdnPassportIdValue" runat="server" />
<ext:Button runat="server" AutoPostBack="true" OnClick="btnDownloadPassFile_Click"
    Hidden="true" ID="btnDownloadPassFile" Text="<i></i>Download">
</ext:Button>

<ext:GridPanel StyleSpec="margin-top:15px;" Scroll="None" ID="GridPassport" runat="server"
    Width="920" Cls="itemgrid">
    <Store>
        <ext:Store ID="StorePassport" runat="server">
            <Model>
                <ext:Model ID="Model1" runat="server" IDProperty="PassportId">
                    <Fields>
                        <ext:ModelField Name="PassportNo" Type="String" />
                        <ext:ModelField Name="IssuingDate" Type="string" />
                        <ext:ModelField Name="ValidUpto" Type="string" />
                        <ext:ModelField Name="Status" Type="Int" />
                        <ext:ModelField Name="ServerFileName" Type="String" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
    </Store>
    <ColumnModel>
        <Columns>
            <ext:Column ID="Column1" runat="server" Text="Passport Number" DataIndex="PassportNo"
                Wrap="true" Width="200" Flex="1" />
            <ext:Column ID="Column2" runat="server" Text="Issuing Date" DataIndex="IssuingDate"
                Wrap="true" Width="200">
            </ext:Column>
            <ext:Column ID="Column3" runat="server" Text="Valid Upto" DataIndex="ValidUpto" Width="100"
                Wrap="true">
            </ext:Column>
            <ext:CommandColumn ID="CommandColumn1" runat="server" Width="40">
                <Commands>
                    <ext:CommandSeparator />
                    <ext:GridCommand Icon="PageWhitePut" CommandName="DownLoad">
                        <ToolTip Text="DownLoad" />
                    </ext:GridCommand>
                </Commands>
                <Listeners>
                    <Command Handler="CommandHandlerPassFD(command,record);" />
                </Listeners>
                <PrepareToolbar Fn="prepareDownloadPassport" />
            </ext:CommandColumn>
            <ext:Column ID="Column4" runat="server" Text="" Width="380">
            </ext:Column>
        </Columns>
    </ColumnModel>
    <SelectionModel>
        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
    </SelectionModel>
</ext:GridPanel>
