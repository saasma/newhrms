﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HobbyCtrlView.ascx.cs"
    Inherits="Web.NewHR.UserControls.HobbyCtrlView" %>

<ext:GridPanel ID="gridHobbies" runat="server" Width='920px' Cls="itemgrid">
    <Store>
        <ext:Store ID="storeHobbies" runat="server">
            <Model>
                <ext:Model ID="Model1" runat="server" IDProperty="HobbyId">
                    <Fields>
                        <ext:ModelField Name="HobbyId" Type="String" />
                        <ext:ModelField Name="HobbyTypeId" Type="String" />
                        <ext:ModelField Name="HobbyName" Type="string" />
                        <ext:ModelField Name="InvolvedFrom" Type="string" />
                        <ext:ModelField Name="Description" Type="string" />
                        <ext:ModelField Name="IsEditable" Type="Int" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
    </Store>
    <ColumnModel>
        <Columns>
            <ext:Column ID="Column1" Width="200" runat="server" Text="Hobby Name" DataIndex="HobbyName"
                Wrap="true" />
            <ext:Column ID="Column2" Width="150" runat="server" Text="Involved From" DataIndex="InvolvedFrom">
            </ext:Column>
            <ext:Column ID="Column3" Width="400" runat="server" Text="Description" DataIndex="Description"
                Wrap="true">
            </ext:Column>
            <ext:Column ID="colBlank" Width="170" runat="server" Text="" />
        </Columns>
    </ColumnModel>
    <SelectionModel>
        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
    </SelectionModel>
</ext:GridPanel>
