﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CitizenshipCtrlView.ascx.cs" Inherits="Web.NewHR.UserControls.CitizenshipCtrlView" %>


<script type="text/javascript">

    var prepareDownloadCtzn = function (grid, toolbar, rowIndex, record) {
        var downloadBtn = toolbar.items.get(1);
        if (record.data.ServerFileName == null || record.data.ServerFileName == '') {
            downloadBtn.setVisible(false);
        }
    }

      var CommandHandlerCtznFD = function (command, record) {
            <%= hdnCitizenshipIdValue.ClientID %>.setValue(record.data.CitizenshipId);

            <%= btnDownloadCtznFile.ClientID %>.fireEvent('click');
        };

</script>

<ext:Hidden ID="hdnCitizenshipIdValue" runat="server" />
<ext:Button runat="server" AutoPostBack="true" OnClick="btnDownloadCtznFile_Click"
    Hidden="true" ID="btnDownloadCtznFile" Text="<i></i>Download">
</ext:Button>

<ext:GridPanel StyleSpec="margin-top:15px;" Scroll="None" ID="GridCitizenship" runat="server"
    Width="920" Cls="itemgrid">
    <Store>
        <ext:Store ID="StoreCitizenship" runat="server">
            <Model>
                <ext:Model ID="Model1" runat="server" IDProperty="CitizenshipId">
                    <Fields>
                        <ext:ModelField Name="Nationality" Type="string" />
                        <ext:ModelField Name="CitizenshipNo" Type="String" />
                        <ext:ModelField Name="IssueDate" Type="string" />
                        <ext:ModelField Name="Place" Type="string" />
                        <ext:ModelField Name="Status" Type="Int" />
                        <ext:ModelField Name="ServerFileName" Type="String" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
    </Store>
    <ColumnModel>
        <Columns>
            <ext:Column ID="Column1" runat="server" Text="Nationality" DataIndex="Nationality" Wrap="true"
                Width="150" Flex="1" />
            <ext:Column ID="Column2" runat="server" Text="Citizenship Number" DataIndex="CitizenshipNo" Wrap="true"
                Width="150">
            </ext:Column>
            <ext:Column ID="Column3" runat="server" Text="Issue Date" DataIndex="IssueDate" Width="100" Wrap="true">
            </ext:Column>
            <ext:Column ID="Column4" runat="server" Text="Place" DataIndex="Place" Width="200" Wrap="true">
            </ext:Column>
            <ext:CommandColumn ID="CommandColumn1" runat="server" Width="40">
                <Commands>
                    <ext:CommandSeparator />
                    <ext:GridCommand Icon="PageWhitePut" CommandName="DownLoad">
                        <ToolTip Text="DownLoad" />
                    </ext:GridCommand>
                </Commands>
                <Listeners>
                    <Command Handler="CommandHandlerCtznFD(command,record);" />
                </Listeners>
                <PrepareToolbar Fn="prepareDownloadCtzn" />
            </ext:CommandColumn>
            <ext:Column ID="Column5" runat="server" Text="" Width="280">
            </ext:Column>
        </Columns>
    </ColumnModel>
    <SelectionModel>
        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
    </SelectionModel>
</ext:GridPanel>


