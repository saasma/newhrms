﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserProfileCtrl.ascx.cs"
    Inherits="Web.NewHR.UserControls.UserProfileCtrl" %>
<style type="text/css">
    .gridTopPanel
    {
        background-color:  rgba(239, 246, 251, 1);
        padding: 2px;
        border-bottom:none;
        padding-left:8px;
        
        
    }
    .panel-title
    {
        font-size: 12px !important;
        text-transform: uppercase;
    }
    
    .panel-body
    {
        padding-top:0px!important;
        padding-bottom:0px!important;
    }
    .nav-tabs > li > a
    {
        font-size: 11.5px !important;
        padding-left: 1px !important;
        padding-right: 15px !important;
        color: #428bca;
        font-weight: bold;
    }
    
    .panel-default
    {
        width: 892px !important;
    }
    .gridTopPanel > a
    {
        color: inherit;
    }
    .firsttdskip tr td:first-child
    {
        padding-left: 8px !important;
    }
    
    .table tbody > tr > td
    {
         border-color:rgba(206, 222, 236, 1)!important;
         padding-bottom:0px!important;
         padding-top:0px!important;
    }


   .table tbody>tr:not(:first-child):hover {
    background-color: #ddd!important;
}


.table tbody>tr:last-child
{
   border-bottom: 1px solid rgba(206, 222, 236, 1)!important;
}

.table tbody>tr>th
{
   font-weight:normal!important;
}




</style>
<div class="widget-body">
    <table class="fieldTable firsttdskip">
        <tr>
            <td>
                <div class="panel panel-default">
                    <%--<div style="margin-top: 20px; margin-left: 20px">
                        <ul class="nav nav-tabs nav-line">
                            <li><a href="#Status">Status Change</a></li>
                            <li><a href="#History">Level/Grade History</a></li>
                            <li><a href="#Transfer">Branch Transfer</a></li>
                            <li><a href="#Education">Eductaion</a></li>
                            <li><a href="#Training">Training</a></li>
                            <li><a href="#Employment">Employment</a></li>
                            <li><a href="#Skills">Skills</a></li>
                            <li><a href="#Languages">Languages</a></li>
                            <li><a href="#Publication">Publication</a></li>
                            <li><a href="#Health">Health</a></li>
                            <li><a href="#Family">Family</a></li>
                            <li><a href="#ExtraActivity">Extra Activity</a></li>
                            <li><a href="#Nominees">Nominees</a></li>
                        </ul>
                    </div>--%>
                    <!-- panel-heading -->
                    <div id="divStatusChange" runat="server" class="panel-body" style="margin-top:20px">
                        <div class="gridTopPanel">
                            <a name="Status">
                                <h4 class="panel-title">
                                    <i></i>Status Change</h4>
                            </a>
                        </div>
                        <div class="table-responsive">
                            <asp:GridView ID="gridChangeStatusHist" runat="server" GridLines="None" CssClass="table mb30 " HeaderStyle-BackColor="#428BCA" HeaderStyle-ForeColor="White"
                                ShowHeader="true" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:BoundField DataField="StatusName" HeaderText="Status" />
                                    <asp:BoundField DataField="FromDate" HeaderText="From Date" />
                                    <asp:BoundField DataField="ToDate" HeaderText="To Date" />
                                    <asp:BoundField DataField="TimeElapsed" HeaderText="Duration" />
                                    <asp:BoundField DataField="Note" HeaderText="Note" />
                                </Columns>
                                
                                  <EmptyDataTemplate>
                                    No record found.
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </div>
                    <!-- panel-heading -->
                    <div class="panel-body" runat="server" id="levelGradeChangeHistory">
                        <div class="gridTopPanel">
                            <a name="History">
                                <h4 class="panel-title">
                                    <i></i>Level Grade History</h4>
                            </a>
                        </div>
                        <div class="table-responsive">
                            <div id="divLevelGradeHistory" runat="server">
                                <asp:GridView ID="gridLevelGradeHistory" runat="server" GridLines="None" CssClass="table mb30 " HeaderStyle-BackColor="#428BCA" HeaderStyle-ForeColor="White"
                                ShowHeader="true" AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:BoundField DataField="FromDate" HeaderText="From Date" />
                                        <asp:BoundField DataField="OldLevel" HeaderText="Old Level" />
                                        <asp:BoundField DataField="PrevStepGrade" HeaderText="Old Grade" />
                                        <asp:BoundField DataField="NewLevel" HeaderText="New Level" />
                                        <asp:BoundField DataField="StepGrade" HeaderText="New Grade" />
                                        <asp:BoundField DataField="ChangeType" HeaderText="Change Type" />
                                    </Columns>
                                    <EmptyDataTemplate>
                                    No record found.
                                </EmptyDataTemplate>
                                </asp:GridView>
                                
                            </div>
                        </div>
                    </div>
                    <!-- panel-heading -->
                    <div id="divBranchTransfer" runat="server" class="panel-body">
                        <div class="gridTopPanel">
                            <a name="Transfer">
                                <h4 class="panel-title">
                                    <i></i>Transfer</h4>
                            </a>
                        </div>
                        <div class="table-responsive">
                            <asp:GridView ID="GridLevels" runat="server" GridLines="None" CssClass="table mb30 " HeaderStyle-BackColor="#428BCA" HeaderStyle-ForeColor="White"
                                ShowHeader="true" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:BoundField HeaderText="From Date" DataField="FromDate" />
                                    <asp:BoundField HeaderText="Letter Date" DataField="LetterDate" />
                                    <asp:BoundField HeaderText="From Branch" DataField="FromBranch" />
                                    <asp:BoundField HeaderText="To Branch" DataField="ToBranch" />
                                    <asp:BoundField HeaderText="To Department" DataField="ToDepartment" />
                                    <asp:BoundField HeaderText="Duration" DataField="TimeElapsed" />
                                </Columns>
                                <EmptyDataTemplate>
                                    No record found.
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </div>
                    <!-- panel-heading -->
                    <div id="divEducation" runat="server" class="panel-body">
                        <div class="gridTopPanel">
                            <a name="Education">
                                <h4 class="panel-title">
                                    <i></i>Education</h4>
                            </a>
                        </div>
                        <div class="table-responsive">
                            <asp:GridView ID="GridEducation" runat="server" GridLines="None" CssClass="table mb30 " HeaderStyle-BackColor="#428BCA" HeaderStyle-ForeColor="White"
                                ShowHeader="true" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:BoundField HeaderText="Course" DataField="Course" />
                                    <asp:BoundField HeaderText="Level" DataField="LevelName" />
                                    <asp:BoundField HeaderText="University" DataField="University" />
                                    <asp:BoundField HeaderText="Passed Year" DataField="PassedYear" />
                                    <asp:BoundField HeaderText="% / Grade" DataField="Percentage" />
                                </Columns>
                                <EmptyDataTemplate>
                                    No record found.
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </div>
                    <!-- panel-heading -->
                    <div id="divTraining" runat="server" class="panel-body">
                        <div class="gridTopPanel">
                            <a name="Training">
                                <h4 class="panel-title">
                                    <i></i>Training</h4>
                            </a>
                        </div>
                        <div class="table-responsive">
                            <asp:GridView ID="GridTraining" runat="server" GridLines="None" CssClass="table mb30 " HeaderStyle-BackColor="#428BCA" HeaderStyle-ForeColor="White"
                                ShowHeader="true" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:BoundField HeaderText="Training Name" DataField="TrainingName" />
                                    <asp:BoundField HeaderText="Training Type" DataField="TrainingTypeName" />
                                    <asp:BoundField HeaderText="Institute" DataField="InstitutionName" />
                                    <asp:BoundField HeaderText="Duration" DataField="Duration" />
                                    <asp:BoundField HeaderText="Start Date" DataField="TrainingFrom" />
                                    <asp:BoundField HeaderText="End Date" DataField="TrainingTo" />
                                </Columns>
                                 <EmptyDataTemplate>
                                    No record found.
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </div>
                    <!-- panel-heading -->
                    <div id="divSeminar" runat="server" class="panel-body">
                        <div class="gridTopPanel">
                            <a name="Seminar">
                                <h4 class="panel-title">
                                    <i></i>Seminar</h4>
                            </a>
                        </div>
                        <div class="table-responsive">
                            <asp:GridView ID="GridSeminar" runat="server" GridLines="None" CssClass="table mb30 " HeaderStyle-BackColor="#428BCA" HeaderStyle-ForeColor="White"
                                ShowHeader="true" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:BoundField HeaderText="Organizer" DataField="Organizer" />
                                    <asp:BoundField HeaderText="Country" DataField="Country" />
                                    <asp:BoundField HeaderText="Place" DataField="Place" />
                                    <asp:BoundField HeaderText="Duration" DataField="Duration" />
                                    <asp:BoundField HeaderText="Start Date" DataField="StartDate" />
                                    <asp:BoundField HeaderText="End Date" DataField="EndDate" />
                                </Columns>
                                 <EmptyDataTemplate>
                                    No record found.
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </div>
                    <!-- panel-heading -->
                    <div id="divPrevEmploy" runat="server" class="panel-body">
                        <div class="gridTopPanel">
                            <a name="Employment">
                                <h4 class="panel-title">
                                    <i></i>Previous Employment</h4>
                            </a>
                        </div>
                        <div class="table-responsive">
                            <asp:GridView ID="GridPreviousEmployment" runat="server" GridLines="None" CssClass="table mb30 " HeaderStyle-BackColor="#428BCA" HeaderStyle-ForeColor="White"
                                ShowHeader="true" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:BoundField HeaderText="Experience Category" DataField="Category" />
                                    <asp:BoundField HeaderText="Organization" DataField="Organization" />
                                    <asp:BoundField HeaderText="Place" DataField="Place" />
                                    <asp:BoundField HeaderText="Position" DataField="Position" />
                                    <asp:BoundField HeaderText="Job Responsibility" DataField="JobResponsibility" />
                                </Columns>
                                <EmptyDataTemplate>
                                    No record found.
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </div>
                    <!-- panel-heading -->
                    <div id="divSkillSet" runat="server" class="panel-body">
                        <div class="gridTopPanel">
                            <a name="Skills">
                                <h4 class="panel-title">
                                    <i></i>Skill Set</h4>
                            </a>
                        </div>
                        <div class="table-responsive">
                            <asp:GridView ID="GridSkillSets" runat="server" GridLines="None" CssClass="table mb30 " HeaderStyle-BackColor="#428BCA" HeaderStyle-ForeColor="White"
                                ShowHeader="true" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:BoundField HeaderText="Skill" DataField="SkillSetName" />
                                    <asp:BoundField HeaderText="Expertise Level" DataField="LevelOfExpertise" />
                                </Columns>
                                 <EmptyDataTemplate>
                                    No record found.
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </div>
                    <!-- panel-heading -->
                    <div id="divLanguages" runat="server" class="panel-body">
                        <div class="gridTopPanel">
                            <a name="Languages">
                                <h4 class="panel-title">
                                    <i></i>Languages</h4>
                            </a>
                        </div>
                        <div class="table-responsive">
                            <asp:GridView ID="GridLanguageSets" runat="server" GridLines="None" CssClass="table mb30 " HeaderStyle-BackColor="#428BCA" HeaderStyle-ForeColor="White"
                                ShowHeader="true" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:BoundField HeaderText="Language" DataField="LanguageSetName" />
                                    <asp:BoundField HeaderText="Fluency : Speaking" DataField="FluencySpeakName" />
                                    <asp:BoundField HeaderText="Fluency : Writing" DataField="FluencyWriteName" />
                                </Columns>
                                 <EmptyDataTemplate>
                                    No record found.
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </div>
                    <!-- panel-heading -->
                    <div id="divPublication" runat="server" class="panel-body">
                        <div class="gridTopPanel">
                            <a name="Publication">
                                <h4 class="panel-title">
                                    <i></i>Publication</h4>
                            </a>
                        </div>
                        <div class="table-responsive">
                            <asp:GridView ID="GridPublication" runat="server" GridLines="None" CssClass="table mb30 " HeaderStyle-BackColor="#428BCA" HeaderStyle-ForeColor="White"
                                ShowHeader="true" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:BoundField HeaderText="Publication Name" DataField="PublicationName" />
                                    <asp:BoundField HeaderText="Publication Type" DataField="PublicationTypeName" />
                                    <asp:BoundField HeaderText="Publisher" DataField="Publisher" />
                                    <asp:BoundField HeaderText="Country" DataField="Country" />
                                    <asp:BoundField HeaderText="Year" DataField="Year" />
                                </Columns>
                                 <EmptyDataTemplate>
                                    No record found.
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </div>
                    <!-- panel-heading -->
                    <div id="divHealthStatus" runat="server" class="panel-body">
                        <table id="tblHandicapped">
                            <tr>
                                <td style="padding: 5px; width: 100px; display: none">
                                    <asp:Label runat="server" ID="lblHandicapped1" /> :
                                </td>
                                <td>
                                    <asp:Label runat="server" ID="lblRefcmbHandicapped" Text="No" Visible="false"></asp:Label>
                                </td>
                            </tr>
                        </table>
                        <div class="gridTopPanel">
                            <a name="Health">
                                <h4 class="panel-title">
                                    <i></i>Health Status</h4>
                            </a>
                        </div>
                        <div class="table-responsive">
                            <asp:GridView ID="GridHealth" runat="server" GridLines="None" CssClass="table mb30 " HeaderStyle-BackColor="#428BCA" HeaderStyle-ForeColor="White"
                                ShowHeader="true" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:BoundField HeaderText="Health Status" DataField="HealthStatus" />
                                    <asp:BoundField HeaderText="Contition" DataField="ContitionTypeName" />
                                    <asp:BoundField HeaderText="Diagnosed Type" DataField="DiagnosedTypeName" />
                                    <asp:BoundField HeaderText="Diagnosed Date" DataField="DiagnosedDate" />
                                </Columns>
                                 <EmptyDataTemplate>
                                   No record found.
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </div>
                    <!-- panel-heading -->
                    <div id="divFamily" runat="server" class="panel-body">
                        <div class="gridTopPanel">
                            <a name="Family">
                                <h4 class="panel-title">
                                    <i></i>Family</h4>
                            </a>
                        </div>
                        <div class="table-responsive">
                            <asp:GridView ID="GridFamily" runat="server" GridLines="None" CssClass="table mb30 " HeaderStyle-BackColor="#428BCA" HeaderStyle-ForeColor="White"
                                ShowHeader="true" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:BoundField HeaderText="Relation" DataField="Relation" />
                                    <asp:BoundField HeaderText="Name" DataField="Name" />
                                    <asp:BoundField HeaderText="DOB" DataField="DateOfBirth" />
                                    <asp:BoundField HeaderText="Dependent" DataField="HasDependent" />
                                    <asp:BoundField HeaderText="Remarks" DataField="Remarks" />
                                </Columns>
                                 <EmptyDataTemplate>
                                    No record found.
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </div>

                      <!-- panel-heading -->
                    <div id="divExtraActivity" runat="server" class="panel-body">
                        <div class="gridTopPanel">
                            <a name="ExtraActivity">
                                <h4 class="panel-title">
                                    <i></i>Extra Activity</h4>
                            </a>
                        </div>
                        <div class="table-responsive">
                            <asp:GridView ID="GridExtraActivity" runat="server" GridLines="None" CssClass="table mb30 " HeaderStyle-BackColor="#428BCA" HeaderStyle-ForeColor="White"
                                ShowHeader="true" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:BoundField HeaderText="Acitivity Name" DataField="AcitivityName" />
                                    <asp:BoundField HeaderText="Proficiency" DataField="Proficiency" />
                                    <asp:BoundField HeaderText="Award" DataField="Award" />
                                    <asp:BoundField HeaderText="Year" DataField="Year" />
                                </Columns>
                                 <EmptyDataTemplate>
                                    No record found.
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </div>

                       <!-- panel-heading -->
                    <div id="divNominees" runat="server" class="panel-body">
                        <div class="gridTopPanel">
                            <a name="Nominees">
                                <h4 class="panel-title">
                                    <i></i>Nominees</h4>
                            </a>
                        </div>
                        <div class="table-responsive">
                            <asp:GridView ID="GridNominee" runat="server" GridLines="None" CssClass="table mb30 " HeaderStyle-BackColor="#428BCA" HeaderStyle-ForeColor="White"
                                ShowHeader="true" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:BoundField HeaderText="Nominee Name" DataField="NomineeName" />
                                    <asp:BoundField HeaderText="Relation" DataField="Relation" />
                                    <asp:BoundField HeaderText="EffectiveDate" DataField="EffectiveDate" />
                                </Columns>
                                 <EmptyDataTemplate>
                                    No record found.
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </div>

                </div>
            </td>
        </tr>
    </table>
</div>
