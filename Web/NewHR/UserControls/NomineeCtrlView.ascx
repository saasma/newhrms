﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NomineeCtrlView.ascx.cs"
    Inherits="Web.NewHR.UserControls.NomineeCtrlView" %>

<ext:GridPanel ID="gridNominee" runat="server" Width="920" Cls="itemgrid">
    <Store>
        <ext:Store ID="storeNominee" runat="server">
            <Model>
                <ext:Model ID="Model1" runat="server" IDProperty="NomineeId">
                    <Fields>
                        <ext:ModelField Name="NomineeId" Type="String" />
                        <ext:ModelField Name="SN" Type="string" />
                        <ext:ModelField Name="NomineeName" Type="string" />
                        <ext:ModelField Name="Relation" Type="string" />
                        <ext:ModelField Name="EffectiveDate" Type="string" />
                        <ext:ModelField Name="IsEditable" Type="Int" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
    </Store>
    <ColumnModel>
        <Columns>
            <ext:Column ID="Column5" Width="100" runat="server" Text="Nominee No" DataIndex="SN" />
            <ext:Column ID="Column1" Width="200" runat="server" Text="Nominee Name" DataIndex="NomineeName"
                Wrap="true" />
            <ext:Column ID="Column2" Width="150" runat="server" Text="Relation" DataIndex="Relation"
                Wrap="true">
            </ext:Column>
            <ext:Column ID="Column6" Width="100" runat="server" Text="Effective Date" DataIndex="EffectiveDate">
            </ext:Column>
            <ext:Column ID="Column4" Width="370" runat="server" Text="">
            </ext:Column>
        </Columns>
    </ColumnModel>
    <SelectionModel>
        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
    </SelectionModel>
</ext:GridPanel>
