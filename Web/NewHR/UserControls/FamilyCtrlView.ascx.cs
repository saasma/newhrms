﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
namespace Web.NewHR.UserControls
{
    public partial class FamilyCtrlView : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
                Initialise();
        }

        public void Initialise()
        {
            LoadFamilyGrid();
        }

        public int GetEmployeeId()
        {
            int employeeId = 0;

            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                employeeId = int.Parse(Request.QueryString["ID"]);
            }
            else
            {
                employeeId = SessionManager.CurrentLoggedInEmployeeId;
            }

            return employeeId;
        }

        private void LoadFamilyGrid()
        {
            List<DAL.HFamily> list = NewPayrollManager.GetAllFamilyList(GetEmployeeId());

            if (SessionManager.CurrentLoggedInEmployeeId == 0)
                list = list.Where(x => x.Status == (int)HRStatusEnum.Approved).ToList();

            if (Request.QueryString["ID"] != null)
            {
                if (int.Parse(Request.QueryString["ID"].ToString()) != SessionManager.CurrentLoggedInEmployeeId)
                    list = list.Where(x => x.Status == (int)HRStatusEnum.Approved).ToList();
            }

            GridLevels.Store[0].DataSource = list;
            GridLevels.Store[0].DataBind();

            if (list.Count <= 0)
                GridLevels.Hide();
        }


    }
}