﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Entity;
using BLL;
using System.IO;
using Utils;
using System.Text;
using BLL.BO;
using Utils.Calendar;
namespace Web.NewHR.UserControls
{
    public partial class DocumentListCtrl : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
                Initialise();
        }

        private void Initialise()
        {
            BindPartyGrid("");
            BindCategoryComboBox();
            BindSubCategoryComboBox();

            if (!DocumentManager.CanUserAddEditDocument())
                ComColEdit.Hide();

            if (!DocumentManager.CanUserDeleteDocument())
                ComColDel.Hide();

            if (!string.IsNullOrEmpty(Request.QueryString["PartyID"]))
                hdnPartyID.Text = Request.QueryString["PartyID"];
        }

        private void BindPartyGrid(string searchName)
        {
            List<GetDocPartySearchListResult> list = BLL.BaseBiz.PayrollDataContext.GetDocPartySearchList(searchName, SessionManager.CurrentLoggedInUserID).ToList();

            int allCount = 0;
            if (list.Count > 0)
                allCount = list.Sum(x => x.cnt).Value;

            list.Insert(0, new GetDocPartySearchListResult { Name = string.Format("All({0})", allCount), PartyID = Guid.Empty });
            
            gridParty.Store[0].DataSource = list;
            gridParty.Store[0].DataBind();
        }

        private void BindCategoryComboBox()
        {
            cmbCategory.Store[0].DataSource = DocumentManager.GetDocCategories();
            cmbCategory.Store[0].DataBind();
        }

        private void BindSubCategoryComboBox()
        {
            cmbSubCategory.Store[0].DataSource = DocumentManager.GetDocSubCategories();
            cmbSubCategory.Store[0].DataBind();
        }

        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            int totalRows = 0, pageSize = 0;

            DateTime? dateAddedFrom = null, dateAddedTo = null, expiryDateFrom = null, expiryDateTo = null;
            Guid? partyID = null, categoryID = null, subCategoryID = null, documentID = null;
            string documentNameSearch = "";

           
            pageSize = int.Parse(cmbPageSize.SelectedItem.Value);

            if (!string.IsNullOrEmpty(hdnPartyID.Text.Trim()))
                partyID = new Guid(hdnPartyID.Text.Trim());

            if (partyID == Guid.Empty)
                partyID = null;

            if (cmbCategory.SelectedItem != null && cmbCategory.SelectedItem.Value != null)
                categoryID = new Guid(cmbCategory.SelectedItem.Value);

            if (cmbSubCategory.SelectedItem != null && cmbSubCategory.SelectedItem.Value != null)
                subCategoryID = new Guid(cmbSubCategory.SelectedItem.Value);

            if (cmbDocument.SelectedItem != null && cmbDocument.SelectedItem.Value != null)
            {
                try
                {
                    documentID = new Guid(cmbDocument.SelectedItem.Value);
                }
                catch (Exception ex)
                {
                    documentID = null;
                }
            }
           
            documentNameSearch = cmbDocument.Text;

            if (documentID != null)
                documentNameSearch = "";

            int thisMonth = 0, daysInMonth = 0, thisYear = 0;
            CustomDate cdToday = CustomDate.GetTodayDate(false);
            string monthStartDateString = "", monthEndDateString = "";
            CustomDate cdFromDate = null, cdToDate = null;

            thisMonth = cdToday.Month;
            thisYear = cdToday.Year;

            if (cmbDateAdded.SelectedItem != null && cmbDateAdded.SelectedItem.Value != null)
            {
                int value = int.Parse(cmbDateAdded.SelectedItem.Value);
                if (value == 1)//today
                {
                    dateAddedFrom = DateTime.Today.Date;
                    dateAddedTo = DateTime.Today.Date;
                }
                else if (value == 2)//this week
                {
                    dateAddedFrom = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek).Date;
                    dateAddedTo = dateAddedFrom.Value.AddDays(7).AddSeconds(-1).Date;
                }
                else if (value == 3)//last week
                {
                    DateTime weekStartDate = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek).Date;
                    dateAddedFrom = weekStartDate.AddDays(-7);
                    dateAddedTo = weekStartDate.AddDays(-1);
                }
                else if (value == 4)//this month
                {
                    monthStartDateString = string.Format("{0}/{1}/{2}", thisYear, thisMonth, 1);

                    daysInMonth = DateHelper.GetTotalDaysInTheMonth(thisYear, thisMonth, false);
                    monthEndDateString = string.Format("{0}/{1}/{2}", thisYear, thisMonth, daysInMonth);

                    cdFromDate = CustomDate.GetCustomDateFromString(monthStartDateString, false);
                    cdToDate = CustomDate.GetCustomDateFromString(monthEndDateString, false);

                    dateAddedFrom = cdFromDate.EnglishDate.Date;
                    dateAddedTo = cdToDate.EnglishDate.Date;
                }
                else if (value == 5)//last month
                {
                    int lastYear = 0;
                    int lastMonth = thisMonth - 1;
                    if (lastMonth == 0)
                    {
                        lastMonth = 12;
                        lastYear = thisYear - 1;
                    }
                    else
                        lastYear = thisYear;

                    monthStartDateString = string.Format("{0}/{1}/{2}", lastYear, lastMonth, 1);

                    daysInMonth = DateHelper.GetTotalDaysInTheMonth(lastYear, lastMonth, false);
                    monthEndDateString = string.Format("{0}/{1}/{2}", lastYear, lastMonth, daysInMonth);

                    cdFromDate = CustomDate.GetCustomDateFromString(monthStartDateString, false);
                    cdToDate = CustomDate.GetCustomDateFromString(monthEndDateString, false);

                    dateAddedFrom = cdFromDate.EnglishDate.Date;
                    dateAddedTo = cdToDate.EnglishDate.Date;

                }
                else if (value == 6)//custom date
                {
                    if(!string.IsNullOrEmpty(calDateAddedFrom.Text.Trim()))
                        dateAddedFrom = GetEngDate(calDateAddedFrom.Text.Trim());

                    if(!string.IsNullOrEmpty(calDateAddedTo.Text.Trim()))
                        dateAddedTo = GetEngDate(calDateAddedTo.Text.Trim());
                }
                
            }

            if (cmbExpiring.SelectedItem != null && cmbExpiring.SelectedItem.Value != null)
            {
                int value = int.Parse(cmbExpiring.SelectedItem.Value);
                if (value == 1)//today
                {
                    expiryDateFrom  = DateTime.Today.Date;
                    expiryDateTo = DateTime.Today.Date;
                }
                else if (value == 2)//this week
                {
                    expiryDateFrom = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek).Date;
                    expiryDateTo = expiryDateFrom.Value.AddDays(7).AddSeconds(-1).Date;
                }
                else if (value == 3)//last week
                {
                    DateTime weekStartDate = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek).Date;
                    expiryDateFrom = weekStartDate.AddDays(-7);
                    expiryDateTo = weekStartDate.AddDays(-1);
                }
                else if (value == 4)//this month
                {
                    monthStartDateString = string.Format("{0}/{1}/{2}", thisYear, thisMonth, 1);

                    daysInMonth = DateHelper.GetTotalDaysInTheMonth(thisYear, thisMonth, false);
                    monthEndDateString = string.Format("{0}/{1}/{2}", thisYear, thisMonth, daysInMonth);

                    cdFromDate = CustomDate.GetCustomDateFromString(monthStartDateString, false);
                    cdToDate = CustomDate.GetCustomDateFromString(monthEndDateString, false);

                    expiryDateFrom = cdFromDate.EnglishDate.Date;
                    expiryDateTo = cdToDate.EnglishDate.Date;
                }
                else if (value == 5)//last month
                {
                    int lastYear = 0;
                    int lastMonth = thisMonth - 1;
                    if (lastMonth == 0)
                    {
                        lastMonth = 12;
                        lastYear = thisYear - 1;
                    }
                    else
                        lastYear = thisYear;

                    monthStartDateString = string.Format("{0}/{1}/{2}", lastYear, lastMonth, 1);

                    daysInMonth = DateHelper.GetTotalDaysInTheMonth(lastYear, lastMonth, false);
                    monthEndDateString = string.Format("{0}/{1}/{2}", lastYear, lastMonth, daysInMonth);

                    cdFromDate = CustomDate.GetCustomDateFromString(monthStartDateString, false);
                    cdToDate = CustomDate.GetCustomDateFromString(monthEndDateString, false);

                    expiryDateFrom = cdFromDate.EnglishDate.Date;
                    expiryDateTo = cdToDate.EnglishDate.Date;

                }
                else if (value == 6)//custom date
                {
                    if (!string.IsNullOrEmpty(calExpiringDateFrom.Text.Trim()))
                        expiryDateFrom = GetEngDate(calExpiringDateFrom.Text.Trim());

                    if (!string.IsNullOrEmpty(calExpiringDateTo.Text.Trim()))
                        expiryDateTo = GetEngDate(calExpiringDateTo.Text.Trim());
                }
                
            }

            List<GetDocumentListResult> list = DocumentManager.GetDocumentList(e.Start / pageSize, pageSize, partyID, categoryID, subCategoryID, documentID,
                documentNameSearch, dateAddedFrom, dateAddedTo, expiryDateFrom, expiryDateTo);

            if (list.Count > 0)
                totalRows = list[0].TotalRows.Value;

            storeDocumentList.DataSource = list;
            storeDocumentList.DataBind();
            e.Total = totalRows;


        }

        protected void btnDownload_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(hdnFileID.Text))
            {
                NewMessage.ShowWarningMessage("Attachment not found.");
                return;
            }

            Guid fileId = new Guid(hdnFileID.Text);
            DocAttachment doc = DocumentManager.GetDocAttacmentById(fileId);

            string path = Server.MapPath(@"~/uploads/Document/" + doc.ServerFileName);
            string contentType = doc.FileType;
            string name = doc.UserFileName;

            byte[] bytes = File.ReadAllBytes(path);

            Context.Response.Clear();
            Context.Response.ClearHeaders();
            Context.Response.AppendHeader("Content-Disposition", "attachment; filename=\"" + name + "\"");
            Context.Response.ContentType = contentType;
            Context.Response.BinaryWrite(bytes);
            Context.Response.End();
        }

        protected void txtSearchParty_Change(object sender, DirectEventArgs e)
        {
            string partyNameSearch = txtSearchParty.Text.Trim();
            BindPartyGrid(partyNameSearch);
        }

        protected void btnDelete_Click(object sender, DirectEventArgs e)
        {
            if (string.IsNullOrEmpty(hdnDocumentID.Text.Trim()))
                return;

            Guid documentID = new Guid(hdnDocumentID.Text.Trim());
            Status status = DocumentManager.DeleteDocDocument(documentID);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Document is deleted.");
                txtSearchParty_Change(null, null);
                X.Js.Call("searchList");
            }
            else
                NewMessage.ShowWarningMessage(status.ErrorMessage);
        }

        private void SetDivHeight()
        {
            bool maxHeight = false;

            if (cmbDateAdded.SelectedItem != null && cmbDateAdded.SelectedItem.Value != null && cmbDateAdded.SelectedItem.Value == "6")
                maxHeight = true;

            if (cmbExpiring.SelectedItem != null && cmbExpiring.SelectedItem.Value != null && cmbExpiring.SelectedItem.Value == "6")
                maxHeight = true;

            if (maxHeight)
                X.Js.Call("SetMaxHeight");
            else
                X.Js.Call("SetMinHeight");
        }

        protected void cmbDateAdded_Click(object sender, DirectEventArgs e)
        {
            calDateAddedFrom.Hide();
            calDateAddedTo.Hide();

            if (cmbDateAdded.SelectedItem != null && cmbDateAdded.SelectedItem.Value != null && cmbDateAdded.SelectedItem.Value == "6")
            {
                calDateAddedFrom.Show();
                calDateAddedTo.Show();
            }
            
            SetDivHeight();
        }

        protected void cmbExpiring_Select(object sender, DirectEventArgs e)
        {
            calExpiringDateFrom.Hide();
            calExpiringDateTo.Hide();

            if (cmbExpiring.SelectedItem != null && cmbExpiring.SelectedItem.Value != null && cmbExpiring.SelectedItem.Value == "6")
            {
                calExpiringDateFrom.Show();
                calExpiringDateTo.Show();
            }

            SetDivHeight();
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            int totalRows = 0;

            DateTime? dateAddedFrom = null, dateAddedTo = null, expiryDateFrom = null, expiryDateTo = null;
            Guid? partyID = null, categoryID = null, subCategoryID = null, documentID = null;
            string documentNameSearch = "";

            if (!string.IsNullOrEmpty(hdnPartyID.Text.Trim()))
                partyID = new Guid(hdnPartyID.Text.Trim());

            if (partyID == Guid.Empty)
                partyID = null;

            if (cmbCategory.SelectedItem != null && cmbCategory.SelectedItem.Value != null)
                categoryID = new Guid(cmbCategory.SelectedItem.Value);

            if (cmbSubCategory.SelectedItem != null && cmbSubCategory.SelectedItem.Value != null)
                subCategoryID = new Guid(cmbSubCategory.SelectedItem.Value);

            if (cmbDocument.SelectedItem != null && cmbDocument.SelectedItem.Value != null)
            {
                try
                {
                    documentID = new Guid(cmbDocument.SelectedItem.Value);
                }
                catch (Exception ex)
                {
                    documentID = null;
                }
            }

            documentNameSearch = cmbDocument.Text;

            if (documentID != null)
                documentNameSearch = "";

            int thisMonth = 0, daysInMonth = 0, thisYear = 0;
            CustomDate cdToday = CustomDate.GetTodayDate(false);
            string monthStartDateString = "", monthEndDateString = "";
            CustomDate cdFromDate = null, cdToDate = null;

            thisMonth = cdToday.Month;
            thisYear = cdToday.Year;

            if (cmbDateAdded.SelectedItem != null && cmbDateAdded.SelectedItem.Value != null)
            {
                int value = int.Parse(cmbDateAdded.SelectedItem.Value);
                if (value == 1)//today
                {
                    dateAddedFrom = DateTime.Today.Date;
                    dateAddedTo = DateTime.Today.Date;
                }
                else if (value == 2)//this week
                {
                    dateAddedFrom = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek).Date;
                    dateAddedTo = dateAddedFrom.Value.AddDays(7).AddSeconds(-1).Date;
                }
                else if (value == 3)//last week
                {
                    DateTime weekStartDate = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek).Date;
                    dateAddedFrom = weekStartDate.AddDays(-7);
                    dateAddedTo = weekStartDate.AddDays(-1);
                }
                else if (value == 4)//this month
                {
                    monthStartDateString = string.Format("{0}/{1}/{2}", thisYear, thisMonth, 1);

                    daysInMonth = DateHelper.GetTotalDaysInTheMonth(thisYear, thisMonth, false);
                    monthEndDateString = string.Format("{0}/{1}/{2}", thisYear, thisMonth, daysInMonth);

                    cdFromDate = CustomDate.GetCustomDateFromString(monthStartDateString, false);
                    cdToDate = CustomDate.GetCustomDateFromString(monthEndDateString, false);

                    dateAddedFrom = cdFromDate.EnglishDate.Date;
                    dateAddedTo = cdToDate.EnglishDate.Date;
                }
                else if (value == 5)//last month
                {
                    int lastYear = 0;
                    int lastMonth = thisMonth - 1;
                    if (lastMonth == 0)
                    {
                        lastMonth = 12;
                        lastYear = thisYear - 1;
                    }
                    else
                        lastYear = thisYear;

                    monthStartDateString = string.Format("{0}/{1}/{2}", lastYear, lastMonth, 1);

                    daysInMonth = DateHelper.GetTotalDaysInTheMonth(lastYear, lastMonth, false);
                    monthEndDateString = string.Format("{0}/{1}/{2}", lastYear, lastMonth, daysInMonth);

                    cdFromDate = CustomDate.GetCustomDateFromString(monthStartDateString, false);
                    cdToDate = CustomDate.GetCustomDateFromString(monthEndDateString, false);

                    dateAddedFrom = cdFromDate.EnglishDate.Date;
                    dateAddedTo = cdToDate.EnglishDate.Date;

                }
                else if (value == 6)//custom date
                {
                    if (!string.IsNullOrEmpty(calDateAddedFrom.Text.Trim()))
                        dateAddedFrom = GetEngDate(calDateAddedFrom.Text.Trim());

                    if (!string.IsNullOrEmpty(calDateAddedTo.Text.Trim()))
                        dateAddedTo = GetEngDate(calDateAddedTo.Text.Trim());
                }
            }

            if (cmbExpiring.SelectedItem != null && cmbExpiring.SelectedItem.Value != null)
            {
                int value = int.Parse(cmbExpiring.SelectedItem.Value);
                if (value == 1)//today
                {
                    expiryDateFrom = DateTime.Today.Date;
                    expiryDateTo = DateTime.Today.Date;
                }
                else if (value == 2)//this week
                {
                    expiryDateFrom = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek).Date;
                    expiryDateTo = expiryDateFrom.Value.AddDays(7).AddSeconds(-1).Date;
                }
                else if (value == 3)//last week
                {
                    DateTime weekStartDate = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek).Date;
                    expiryDateFrom = weekStartDate.AddDays(-7);
                    expiryDateTo = weekStartDate.AddDays(-1);
                }
                else if (value == 4)//this month
                {
                    monthStartDateString = string.Format("{0}/{1}/{2}", thisYear, thisMonth, 1);

                    daysInMonth = DateHelper.GetTotalDaysInTheMonth(thisYear, thisMonth, false);
                    monthEndDateString = string.Format("{0}/{1}/{2}", thisYear, thisMonth, daysInMonth);

                    cdFromDate = CustomDate.GetCustomDateFromString(monthStartDateString, false);
                    cdToDate = CustomDate.GetCustomDateFromString(monthEndDateString, false);

                    expiryDateFrom = cdFromDate.EnglishDate.Date;
                    expiryDateTo = cdToDate.EnglishDate.Date;
                }
                else if (value == 5)//last month
                {
                    int lastYear = 0;
                    int lastMonth = thisMonth - 1;
                    if (lastMonth == 0)
                    {
                        lastMonth = 12;
                        lastYear = thisYear - 1;
                    }
                    else
                        lastYear = thisYear;

                    monthStartDateString = string.Format("{0}/{1}/{2}", lastYear, lastMonth, 1);

                    daysInMonth = DateHelper.GetTotalDaysInTheMonth(lastYear, lastMonth, false);
                    monthEndDateString = string.Format("{0}/{1}/{2}", lastYear, lastMonth, daysInMonth);

                    cdFromDate = CustomDate.GetCustomDateFromString(monthStartDateString, false);
                    cdToDate = CustomDate.GetCustomDateFromString(monthEndDateString, false);

                    expiryDateFrom = cdFromDate.EnglishDate.Date;
                    expiryDateTo = cdToDate.EnglishDate.Date;

                }
                else if (value == 6)//custom date
                {
                    if (!string.IsNullOrEmpty(calExpiringDateFrom.Text.Trim()))
                        expiryDateFrom = GetEngDate(calExpiringDateFrom.Text.Trim());

                    if (!string.IsNullOrEmpty(calExpiringDateTo.Text.Trim()))
                        expiryDateTo = GetEngDate(calExpiringDateTo.Text.Trim());
                }
            }

            List<GetDocumentListResult> list = DocumentManager.GetDocumentList(0, 999999, partyID, categoryID, subCategoryID, documentID,
                documentNameSearch, dateAddedFrom, dateAddedTo, expiryDateFrom, expiryDateTo);

            Bll.ExcelHelper.ExportToExcel("Document List", list,
              new List<string> { "DocumentID", "IsDeleted", "DocumentDateEng", "FileID", "TotalRows", "RowNumber" },
          new List<String>() { },
          new Dictionary<string, string>() { { "Title", "Document Name" },
            { "ParyName", "Pary" }, { "CategorySubCategory", "Category:Sub-Category" }, { "DocumentDate", "Document Date"}, {"ExpiryDate", "Expiry Date"}, 
          {"CreatedOn", "Date Added"}, {"UserFileName", "Attachment"}},
          new List<string>() { }
          , new List<string> { }
          , new List<string> { }
           , new Dictionary<string, string>() {
              {"Document List",""}
            }
           , new List<string> { "ID", "Title", "ParyName", "CategorySubCategory", "DocumentDate", "Description", "ExpiryDate", "CreatedOn", "UserFileName" });
        }


    }
}