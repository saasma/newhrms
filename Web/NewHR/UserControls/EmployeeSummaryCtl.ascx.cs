﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using Utils.Web;
using Utils.Helper;

namespace Web.NewHR.UserControls
{
    public partial class EmployeeSummaryCtl : BaseUserControl
    {
      


    
      
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest && !IsPostBack)
            {
                if (SessionManager.CurrentLoggedInEmployeeId == 0)
                    btnEditAmounts.Visible = true;
            }
          
        }

        protected void btnChangeHistoryAmount_Click(object sender, EventArgs e)
        {
            int eventSourceId = int.Parse(hdnSourceID.Text.Trim());

            decimal income1 = 0, income2 = 0, income3 = 0, income4 = 0;

            if (!string.IsNullOrEmpty(txtIncome1.Text.Trim()))
                income1 = decimal.Parse(txtIncome1.Text.Trim());
            if (!string.IsNullOrEmpty(txtIncome2.Text.Trim()))
                income2 = decimal.Parse(txtIncome2.Text.Trim());
            if (!string.IsNullOrEmpty(txtIncome3.Text.Trim()))
                income3 = decimal.Parse(txtIncome3.Text.Trim());
            if (!string.IsNullOrEmpty(txtIncome4.Text.Trim()))
                income4 = decimal.Parse(txtIncome4.Text.Trim());

            EmployeeManager.UpdateServiceEventAmountForHistory(eventSourceId, income1, income2, income3, income4);

            NewMessage.ShowNormalPopup("Amount changed.");
            windowAmounts.Hide();

            Initialise(eventSourceId, 0);
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            //int empid = int.Parse(hdnEIN.Text);
            //int? type = null;
            int? sourceId = 0;
            int sn = 0;

            if (!string.IsNullOrEmpty(hdnType.Text))
                sn = int.Parse(hdnType.Text);

            if (!string.IsNullOrEmpty(hdnSourceID.Text))
                sourceId = int.Parse(hdnSourceID.Text);

            Initialise(sourceId.Value, sn);
        }

        protected void btn_Click(object sender, EventArgs e)
        {

            if (CommonManager.Setting.EnableServiceHistory != null && CommonManager.Setting.EnableServiceHistory.Value)
            {


                //int empid = int.Parse(hdnEIN.Text);
                //int? type = null;
                int? sourceId = 0;
                int sn = 0;

                if (!string.IsNullOrEmpty(hdnType.Text))
                    sn = int.Parse(hdnType.Text);

                if (!string.IsNullOrEmpty(hdnSourceID.Text))
                    sourceId = int.Parse(hdnSourceID.Text);

                Initialise(sourceId.Value, sn);
            }
            else
            {
                int empid = int.Parse(hdnEIN.Text);
                int? type = null;
                int? sourceId = null;

                if (!string.IsNullOrEmpty(hdnType.Text))
                    type = int.Parse(hdnType.Text);

                if (!string.IsNullOrEmpty(hdnSourceID.Text))
                    sourceId = int.Parse(hdnSourceID.Text);

                InitialiseOld(empid, type, sourceId);
            }
        }

        class Data
        {
            public string Income { get; set; }
            public string Amount { get; set; }
        }

        public void Initialise(int eventSourceID,int sn)
        {
            int total = 0;

            EmployeeServiceHistory item1 = BLL.BaseBiz.PayrollDataContext.EmployeeServiceHistories.FirstOrDefault(x => x.EventSourceID == eventSourceID);

            if (item1 == null)
                return;

            List<GetServiceHistoryNewResult> list = BranchManager.GetNewServiceHistory
              (0, 99999, ref total, "", item1.EmployeeId, null, null, null);



            foreach (GetServiceHistoryNewResult item in list)
            {
                if (item.EventSourceID == eventSourceID)
                {
                    window.Title = "Event Details " + item.RowNumber;



                    eventName.Text = item.EventName;


                    branch.Text = item.Branch;


                    department.Text = item.Department;

                    dispEffectiveDate.Text = item.Date;
                    dispEffectiveDateEng.Text = item.DateEng.ToString("dd-MMM-yyyy");
                    dispLetterDateEng.Text = item1.LetterDateEng == null ? "" : item1.LetterDateEng.Value.ToString("dd-MMM-yyyy");

                    int? desigId = BLL.BaseBiz.PayrollDataContext.GetEmployeeCurrentPositionForDate(item1.DateEng, item1.EmployeeId);
                    if (desigId != null)
                    {
                        EDesignation desig = new CommonManager().GetDesignationById(desigId.Value);
                        if (desig != null)
                        {
                            dispLevel.Text = desig.BLevel.Name;
                            dispDesignation.Text = desig.Name;
                        }
                    }
                    dispNotes.Text = item1.Notes;


                    if (item.StatusId == null)
                        item.StatusId = BLL.BaseBiz.PayrollDataContext.GetEmployeeStatusForDate(item1.DateEng, item1.EmployeeId);
                    Grid.Store[0].DataSource = PayManager.GetEmployeeIncomesNew(item1.EmployeeId, item1.DateEng, item1.EventSourceID, item.StatusId);
                    Grid.Store[0].DataBind();

                }
            }
            window.Center();
            window.Show();
        }

        public void InitialiseOld(int empid, int? type, int? sourceID)
        {
            List<GetServiceHistoryListResult> list = EmployeeManager.GetServiceHistoryList(empid, null, null, 0, 9999,null);
            GetServiceHistoryListResult item = list
                .FirstOrDefault(x => x.Type == type && x.SourceID == sourceID);

            if (item != null)
            {
                window.Title = "Event Details : " + item.SN;



                eventName.Text = item.EVENT;
                branch.Text = item.Branch;
                department.Text = item.Department;

                dispEffectiveDate.Text = item.FromDate;
                dispEffectiveDateEng.Text = item.FromDateEng.Value.ToString("dd-MMM-yyyy");
                dispLetterDateEng.Text = item.LetterDateEng == null ? "" : item.LetterDateEng.Value.ToString("dd-MMM-yyyy");

                dispLevel.Text = item.Level;
                dispDesignation.Text = item.Designation;

                dispNotes.Text = item.Note;



                Grid.Store[0].DataSource = PayManager.GetEmployeeIncomes(empid, item.FromDateEng.Value, item.CreatedOn, item.EVENT,
                    item.Type, item.SourceID, item.ServiceStatus, list.Count, item.EVENT);
                Grid.Store[0].DataBind();
            }

            window.Center();
            window.Show();
        }
    }
}