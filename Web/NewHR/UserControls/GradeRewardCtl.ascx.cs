﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using Utils.Web;
using Utils.Helper;

namespace Web.NewHR.UserControls
{
    public partial class GradeRewardCtl : BaseUserControl
    {
        private bool isEmployee = false;
        public bool IsEmployee
        {
            set
            {
                isEmployee = true;

                tbl.Style["display"] = "none";
                gridRewardStore.GroupField = "";
                employeeTD.Style["display"] = "none";

                EmployeeDetailsCtl1.Visible = true;
                EmpDetailsWizard1.Visible = true;

                EEmployee emp = EmployeeManager.GetEmployeeById(GetEmployeeId());
                BLevel currentLevel = NewHRManager.GetEmployeeLastestLevel(emp.EmployeeId);
                if (currentLevel != null)
                {
                    dispLevel.Text = currentLevel.Name;
                }
                gridRewardList.Features.Clear();

              
                
                cmbGradeEmployee.SetRawValue(GetEmployeeId().ToString());
                StoreParameter param = (StoreParameter)gridRewardStore.Parameters[0];
                param.Value = GetEmployeeId().ToString();


               
            }
            get
            {
                return isEmployee;
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (IsEmployee)
            {
                Column_Employee.Hide();
                Column_Level.Text = "Level";

                // Additional Basic
                upperDiv.Style["display"] = "";
                gridAdditionalBasic.Show();
                headerAdditionalBasic.Style["display"] = "";
            }
        }

        public int GetEmployeeId()
        {
            return int.Parse(Request.QueryString["Id"]);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMsg.Text = "";
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }

            JavascriptHelper.AttachNonDialogPopUpCode(Page, "employeePopup", "../ExcelWindow/GradeRewardExcel.aspx", 450, 500);
        }

        [DirectMethod]
        public string GetNewGrade(int levelId, int gradeStep, int newLevelId)
        {
            return NewPayrollManager.GetNewGrade(levelId, gradeStep, newLevelId,new BasePage().GetCurrentDateForSalary(), LevelGradeChangeType.Promotion).ToString();
        }

        public void Initialise()
        {
            StoreLevel.DataSource = NewPayrollManager.GetAllParentLevels();
            StoreLevel.DataBind();

            StoreIncome.DataSource = PayManager.GetFixedIncomeListByCompany(SessionManager.CurrentCompanyId);
            StoreIncome.DataBind();

            StoreBranch.DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            storeSearch.DataBind();

            if (string.IsNullOrEmpty(Request.QueryString["ID"]) == false)
            {
                gridAdditionalBasic.Store[0].DataSource
                        = NewPayrollManager.GetEmployeeAdditionalBasic(GetEmployeeId());
                gridAdditionalBasic.Store[0].DataBind();
            }
        }

        public GradeRewardEmployee Process(GradeRewardEmployee entity)
        {
            if (entity == null)
            {
                entity = new GradeRewardEmployee();
                if (string.IsNullOrEmpty(hdn.Text))
                {
                    if (isEmployee == false)
                        entity.EmployeeId = int.Parse(cmbGradeEmployee.SelectedItem.Value);
                    else
                        entity.EmployeeId = GetEmployeeId();
                }
                else
                    entity.EmployeeId = NewPayrollManager.GetGradeReward(int.Parse(hdn.Text)).EmployeeId;

                entity.Notes = txtNotes.Text.Trim();
                entity.LevelId = NewHRManager.GetEmployeeLastestLevel(entity.EmployeeId).LevelId;
                entity.RewardDate = calDate.Text.Trim();
                entity.RewardDateEng = GetEngDate(entity.RewardDate);
                entity.RewardPoints = txtPoints.Number;
                entity.RewardRate = Convert.ToDecimal(txtRate.Text);
                entity.Amount = (decimal)entity.RewardPoints * entity.RewardRate;
                entity.IsActive = true;
                return entity;
            }
            else
            {
                EEmployee emp = new EmployeeManager().GetById(entity.EmployeeId);

                storeSearch.DataSource = new List<EEmployee> { new EEmployee{ 
                        EmployeeId = emp.EmployeeId,
                Name = emp.Name} 
                
                }
                ;
                storeSearch.DataBind();


                cmbGradeEmployee.Items.Add(new Ext.Net.ListItem { Value = emp.EmployeeId.ToString(), Text = emp.Name });
                cmbGradeEmployee.SelectedItems.Add(new Ext.Net.ListItem { Index = 0 });
                calDate.Text = entity.RewardDate;
                cmbGradeEmployee.SetValue(emp.EmployeeId.ToString());
                cmbGradeEmployee.SetRawValue(emp.Name.ToString());
                dispLevel.Text = NewPayrollManager.GetLevelById(entity.LevelId.Value).Name;
                txtPoints.Number = entity.RewardPoints.Value;
                txtRate.Text = GetCurrency(entity.RewardRate);
                txtNotes.Text = entity.Notes;

                cmbGradeEmployee.Disable(true);
            }
            return null;
        }
        public GradeAdditionlBasic ProcessAdditionalBasic(GradeAdditionlBasic entity)
        {
            if (entity == null)
            {
                entity = new GradeAdditionlBasic();
                if (string.IsNullOrEmpty(hdn.Text))
                {

                    entity.EmployeeId = GetEmployeeId();
                }
                else
                    entity.EmployeeId = NewPayrollManager.GetAdditionalBasic(int.Parse(hdn.Text)).EmployeeId;

                entity.Notes = txtNotes.Text.Trim();
                entity.LevelId = NewHRManager.GetEmployeeLastestLevel(entity.EmployeeId).LevelId;
                entity.IncomeId = int.Parse(cmbIncomes.SelectedItem.Value);

                entity.Amount = decimal.Parse(txtAdditionalBasicAmount.Text.Trim());
                return entity;
            }
            else
            {
                



                dispAdditionalBasicLevel.Text = NewPayrollManager.GetLevelById(entity.LevelId.Value).Name;

                PIncome basicIncome = new PayManager().GetBasicIncome(SessionManager.CurrentCompanyId);

                if (entity.IncomeId != null)
                {
                    cmbIncomes.SetValue(entity.IncomeId.ToString());
                    cmbIncomes.Disable(true);
                }
                else
                    cmbIncomes.SetValue(basicIncome.IncomeId.ToString());

                txtAdditionalBasicAmount.Text = GetCurrency(entity.Amount);
                txtNotesAddiotionalBasic.Text = entity.Notes;

            }
            return null;
        }
        protected void btnDeleteLevel_Click(object sender, DirectEventArgs e)
        {
            int levelId = int.Parse(hdn.Text.Trim());
            Status status = NewPayrollManager.DeleteGradeReward(levelId);
            if (status.IsSuccess)
            {
                X.Js.AddScript("searchList();");
                NewMessage.ShowNormalMessage("Reward deleted.");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }

        }



        protected void btnEditLevel_Click(object sender, DirectEventArgs e)
        {
            int levelId = int.Parse(hdn.Text.Trim());
            GradeRewardEmployee entity = NewPayrollManager.GetGradeReward(levelId);
            Process(entity);
            window.Center();
            window.Show();



            //cmbGroup.SetValue(entity.LevelGroupId.ToString());
            //txtLevelCode.Text = entity.Code;
            //txtLevelName.Text = entity.Name;
            //txtLevelOrder.Number = entity.Order;

        }

        protected void btnEditAdditionalBasic_Click(object sender, DirectEventArgs e)
        {
            int levelId = int.Parse(hdn.Text.Trim());
            GradeAdditionlBasic entity = NewPayrollManager.GetAdditionalBasic(levelId);
            ProcessAdditionalBasic(entity);
            windowAdditionalDate.Show();



            //cmbGroup.SetValue(entity.LevelGroupId.ToString());
            //txtLevelCode.Text = entity.Code;
            //txtLevelName.Text = entity.Name;
            //txtLevelOrder.Number = entity.Order;

        }
        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("InsertUpdate");
            if (Page.IsValid)
            {


                GradeRewardEmployee entity = Process(null);
                bool isInsert = string.IsNullOrEmpty(hdn.Text);
                if (isInsert == false)
                    entity.RewardID = int.Parse(hdn.Text);
                Status status = NewPayrollManager.InsertUpdateGradeReward(entity, isInsert);
                if (status.IsSuccess)
                {
                    NewMessage.ShowNormalMessage("Record saved successfully.");
                    X.Js.AddScript("searchList();");
                    window.Hide();
                    
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }
            }

        }
        protected void btnSaveAdditionalBasic_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveUpdateAdditionalBasic");
            if (Page.IsValid)
            {


                GradeAdditionlBasic entity = ProcessAdditionalBasic(null);
                bool isInsert = string.IsNullOrEmpty(hdn.Text);
                if (isInsert == false)
                    entity.GradeAdditionalBasicID = int.Parse(hdn.Text);
                Status status = NewPayrollManager.InsertUpdateAdditionalBasic(entity, isInsert);
                if (status.IsSuccess)
                {
                    NewMessage.ShowNormalMessage("Record saved successfully.");

                   gridAdditionalBasic.Store[0].DataSource
                         = NewPayrollManager.GetEmployeeAdditionalBasic(GetEmployeeId());
                    gridAdditionalBasic.Store[0].DataBind();

                    windowAdditionalDate.Hide();
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }
            }

        }
        public void cmbGradeEmployee_Select(object sender, DirectEventArgs e)
        {

            int employeeId = int.Parse(cmbGradeEmployee.SelectedItem.Value);


            EEmployee emp = EmployeeManager.GetEmployeeById(employeeId);
            BLevel currentLevel = NewHRManager.GetEmployeeLastestLevel(emp.EmployeeId);
            if (currentLevel == null)
            {
                NewMessage.ShowWarningMessage("Reward can be given to level assigned employee only.");
                return;
            }
            BLevelDate date = NewPayrollManager.GetLastLevelDate();
            if (date == null)
            {
                NewMessage.ShowWarningMessage("Salary structure not defined.");
                return;
            }

            BLevelRate rate = NewPayrollManager.GetLevelRate(currentLevel.LevelId, date.DateID);

            if (rate != null)
                txtRate.Text = GetCurrency(rate.StepGradeRate);
            else
                txtRate.Text = "";

            dispLevel.Text = currentLevel.Name;
        }

        public void ClearFields()
        {
            hdn.Text = "";
            calDate.Clear();
            txtPoints.Clear();
            txtRate.Clear();
            txtNotes.Clear();

            if (isEmployee == false)
            {
                cmbGradeEmployee.ClearValue();
                cmbGradeEmployee.Enable(true);
                dispLevel.Text = "";
            }
        }

        public void ClearFieldsAdditionalBasic()
        {
            hdn.Text = "";
            txtAdditionalBasicAmount.Text = "";
            txtNotesAddiotionalBasic.Text = "";
            cmbIncomes.Enable(true);
            dispAdditionalBasicLevel.Text = "";
        }
        public void btnAddAdditionalBasic_Click(object sender, DirectEventArgs e)
        {

            ClearFieldsAdditionalBasic();
            windowAdditionalDate.Show();
        }

        public void btnAdd_Click(object sender, DirectEventArgs e)
        {

            ClearFields();
            window.Show();
        }

    }
}