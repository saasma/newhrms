﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GenerateAttendanceRptCtl.ascx.cs"
    Inherits="Web.NewHR.UserControls.GenerateAttendanceRptCtl" %>
<script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>
<script type="text/javascript">
    var RefreshProceedDate = function (daterange) {
    var dispfield = <%=dispLastDate.ClientID %>;
        dispfield.textContent = daterange;
    }
</script>

<div class="separator bottom">
    <ext:Hidden ID="hdnFromDate" runat="server" />
    <ext:Hidden ID="hdnToDate" runat="server" />
    <ext:Hidden ID="HiddenLeaveRequestId" runat="server" Text="" />
</div>
<div style="padding-left: 20px">
    <div class="innerLR">
        <ext:Label ID="lblMsg" runat="server" />
        <table class="fieldTable firsttdskip" style="margin-top: 0px !important;" runat="server"
            id="tbl">
            <tr>
                <td>
                    <span>Last Proceed for </span>
                </td>
                <td>
                    <span id="dispLastDate" runat="server" style="color: #428bca"></span>
                    
                </td>
            </tr>
            <tr>
                <td>
                    <ext:DateField runat="server" Width="120" ID="txtFromDate" LabelAlign="Top" FieldLabel="From">
                        <Plugins>
                            <ext:GenericPlugin ID="GenericPlugin1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                        </Plugins>
                    </ext:DateField>
                </td>
                <td>
                    <ext:DateField runat="server" Width="120" ID="txtToDate" LabelAlign="Top" FieldLabel="To">
                        <Plugins>
                            <ext:GenericPlugin ID="GenericPlugin2" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                        </Plugins>
                    </ext:DateField>
                </td>
            </tr>
            <tr>
                <td>
                    <ext:Checkbox StyleSpec="margin-top:2px" ID="chkUpdateOnlyEmployee" runat="server"
                        BoxLabel="Update only for" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                        <Proxy>
                            <ext:AjaxProxy Url="../../Handler/EmpSearch.ashx">
                                <ActionMethods Read="GET" />
                                <Reader>
                                    <ext:JsonReader Root="plants" TotalProperty="total" />
                                </Reader>
                            </ext:AjaxProxy>
                        </Proxy>
                        <Model>
                            <ext:Model ID="Model2" IDProperty="Name" Name="ItemLineModel" runat="server">
                                <Fields>
                                    <ext:ModelField Name="Name" Type="String" />
                                    <ext:ModelField Name="EmployeeId" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                    <ext:ComboBox ID="cmbSearch" EmptyText="Search Employee" LabelWidth="70" LabelAlign="Top"
                        runat="server" DisplayField="Name" ValueField="EmployeeId" StoreID="storeSearch"
                        TypeAhead="false" Width="268" PageSize="9999" HideBaseTrigger="true" MinChars="1"
                        TriggerAction="All" ForceSelection="true">
                        <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                            <ItemTpl ID="ItemTpl1" runat="server">
                                <Html>
                                    <tpl>
                                            <div class="search-item">
                                                            <span>{Name}</span>  
                                             </div>
					                </tpl>
                                </Html>
                            </ItemTpl>
                        </ListConfig>
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" />
                        </Triggers>
                        <Listeners>
                            <Select Handler="this.getTrigger(0).show();" />
                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                            <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                        </Listeners>
                    </ext:ComboBox>
                </td>
            </tr>
            <tr>
                <td style="padding-top: 20px">
                    <ext:Button ID="btnGenerate" runat="server" Cls="btn btn-default" Text="Generate Report">
                        <DirectEvents>
                            <Click OnEvent="btnGenerate_Click" Timeout="999999999">
                                <EventMask ShowMask="true" />
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </td>
            </tr>
        </table>
    </div>
</div>
