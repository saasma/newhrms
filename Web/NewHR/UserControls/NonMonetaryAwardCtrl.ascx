﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NonMonetaryAwardCtrl.ascx.cs" Inherits="Web.NewHR.UserControls.NonMonetaryAwardCtrl" %>


<script type="text/javascript">

    var prepateNMAward = function (grid, toolbar, rowIndex, record) {
        var editDelBtn = toolbar.items.get(1);
        if (record.data.IsEditable == 0) {
            editDelBtn.setVisible(false);
        }

    };
    
</script>

<ext:Hidden runat="server" ID="hdnAwardId">
</ext:Hidden>

<div class="widget-body">
    <table class="fieldTable firsttdskip">
        <tr>
            <td>
                <ext:GridPanel ID="gridNMAward" runat="server" Width="1000" Cls="itemgrid">
                    <Store>
                        <ext:Store ID="storeNMAward" runat="server">
                            <Model>
                                <ext:Model ID="Model1" runat="server" IDProperty="NomineeId">
                                    <Fields>
                                        <ext:ModelField Name="AwardId" Type="String" />
                                        <ext:ModelField Name="AwardName" Type="string" />
                                        <ext:ModelField Name="AwardedBy" Type="string" />
                                        <ext:ModelField Name="AwardDate" Type="string" />
                                        <ext:ModelField Name="Description" Type="string" />
                                        <ext:ModelField Name="IsEditable" Type="Int" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                    </Store>
                    <ColumnModel>
                        <Columns>                            
                            <ext:Column ID="Column1" Width="200" runat="server" Text="Award Name" DataIndex="AwardName" Wrap="true" />
                            <ext:Column ID="Column2" Width="200" runat="server" Text="Awarded By" DataIndex="AwardedBy" Wrap="true">
                            </ext:Column>
                            <ext:Column ID="Column6" Width="100" runat="server" Text="Award Date" DataIndex="AwardDate">
                            </ext:Column>
                            <ext:Column ID="Column3" Width="250" runat="server" Text="Description" DataIndex="Description" Wrap="true">
                            </ext:Column>
                          
                       <ext:CommandColumn ID="CommandColumn21" runat="server" Width="40">
                                <Commands>
                                    <ext:CommandSeparator />
                                    <ext:GridCommand Cls="editGridButton" ToolTip-Text="Edit"  Text="<i class='fa fa-pencil'></i>" CommandName="Edit" />
                                    
                                </Commands>
                                <DirectEvents>
                                    <Command OnEvent="gridNMAward_Command">
                                        <EventMask ShowMask="true">
                                        </EventMask>
                                        <ExtraParams>
                                            <ext:Parameter Name="ID" Value="record.data.AwardId" Mode="Raw">
                                            </ext:Parameter>
                                            <ext:Parameter Name="command" Value="command" Mode="Raw">
                                            </ext:Parameter>
                                        </ExtraParams>
                                    </Command>
                                </DirectEvents>
                                <PrepareToolbar Fn="prepateNMAward" />
                            </ext:CommandColumn>
                            <ext:CommandColumn ID="CommandColumn2" runat="server" Width="40">
                                <Commands>
                                    <ext:CommandSeparator />
                                    <ext:GridCommand Cls="editGridButton" ToolTip-Text="Delete"  Text="<i class='fa fa-trash-o'></i>"
                                        CommandName="Delete" />
                                </Commands>
                                <DirectEvents>
                                    <Command OnEvent="gridNMAward_Command">
                                        <EventMask ShowMask="true">
                                        </EventMask>
                                        <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the record?" />
                                        <ExtraParams>
                                            <ext:Parameter Name="ID" Value="record.data.AwardId" Mode="Raw">
                                            </ext:Parameter>
                                            <ext:Parameter Name="command" Value="command" Mode="Raw">
                                            </ext:Parameter>
                                        </ExtraParams>
                                    </Command>
                                </DirectEvents>
                                <PrepareToolbar Fn="prepateNMAward" />
                            </ext:CommandColumn>
                            <ext:Column ID="Column4" Width="170" runat="server" Text="">
                            </ext:Column>
                        </Columns>
                    </ColumnModel>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                    </SelectionModel>
                </ext:GridPanel>
            </td>
        </tr>
    </table>
    <div class="buttonBlock" runat="server" id="buttonBlock">
       <ext:Button runat="server" Cls="btn btn-primary" Width="120" StyleSpec="margin-top:10px" Height="30"
            ID="btnAddNewLine" Text="<i></i>Add New Line">
            <DirectEvents>
                <Click OnEvent="btnAddNewLine_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:Button>
    </div>
</div>

<ext:Window ID="WNMAward" runat="server" Title="Add/Edit Non Monetary Award" Icon="Application"
    Height="400" Width="430" BodyPadding="5" Hidden="true" Modal="true">
    <Content>
        <table class="fieldTable">
            <tr>
                <td colspan="2">
                    <ext:TextField Width="330" ID="txtAwardName" runat="server" FieldLabel="Award Name *"
                        LabelAlign="top" LabelSeparator="" />
                    <asp:RequiredFieldValidator Display="None" ID="rfvAwardName" runat="server"
                        ValidationGroup="SaveUpdateNMAward" ControlToValidate="txtAwardName" ErrorMessage="Award Name is required." />
                </td>
               
            </tr>
            <tr>
                <td>
                   <ext:TextField Width="330" ID="txtAwardedBy" runat="server" FieldLabel="Awarded By"
                        LabelAlign="top" LabelSeparator="" />
                </td>
            </tr>
            <tr>
                <td>
                    <pr:CalendarExtControl ID="txtAwardedDate" LabelSeparator="" runat="server" FieldLabel="Awarded Date"
                        Width="160" LabelAlign="Top">
                    </pr:CalendarExtControl>    
                </td>
            </tr>
          
             <td colspan="2">
                    <ext:TextArea ID="txtDescription" Width="330" runat="server" FieldLabel="Description" LabelAlign="Top"
                        LabelSeparator="" />
                </td>

    
            <tr>
                <td valign="bottom" colspan="2">
                    <div class="popupButtonDiv">
                        <ext:Button runat="server" Cls="btn btn-primary" ID="btnSave" Text="<i></i>Save">
                            <DirectEvents>
                                <Click OnEvent="btnSave_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                            <Listeners>
                                <Click Handler="valGroup = 'SaveUpdateNMAward';  if(CheckValidation()) return this.disable(); else return false;">
                                </Click>
                            </Listeners>
                        </ext:Button>
                        <div class="btnFlatOr">
                            or</div>
                        <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="LinkButton2" Cls="btnFlatLeftGap"
                            Text="<i></i>Cancel">
                            <Listeners>
                                <Click Handler="#{WNMAward}.hide();">
                                </Click>
                            </Listeners>
                        </ext:LinkButton>
                    </div>
                </td>
            </tr>
        </table>
    </Content>
</ext:Window>