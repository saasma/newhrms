﻿using System;
using System.Collections.Generic;
using System.Linq;
using BLL.Base;
using Ext.Net;
using BLL.Manager;
using BLL;
using Utils.Helper;

namespace Web.NewHR.UserControls
{
    public partial class RewardListCtl : BaseUserControl
    {
        public bool IsDisplayMode
        {
            set
            {
                if (value == true)
                {
                    btnAddLevel.Visible = false;
                    CommandColumn1.Visible = false;
                }
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        public void Initialise()
        {
            LoadLevels();
        }

        private void LoadLevels()
        {
            var total = 0;

            var employeeId = UrlHelper.GetIdFromQueryString("ID");

            var list = NewPayrollManager.GetRewardEmployeeList(
                0, int.MaxValue, ref total, string.Empty, employeeId);
            GridLevels.Store[0].DataSource = list;
            GridLevels.Store[0].DataBind();

            if (list.Count <= 0)
            {
                GridLevels.Hide();
            }
        }




        protected void btnDeleteLevel_Click(object sender, DirectEventArgs e)
        {
            var levelId = int.Parse(hiddenValue.Text.Trim());
            var status = NewPayrollManager.DeleteReward(levelId);
            if (status.IsSuccess)
            {
                LoadLevels();
                NewMessage.ShowNormalMessage("Reward deleted.");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }
    }
}
