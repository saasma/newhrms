﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployeeDetailsCtl.ascx.cs"
    Inherits="Web.NewHR.UserControls.EmployeeDetailsCtl" %>
<ext:Hidden runat="server" ID="hdnPassportID">
</ext:Hidden>
<table>
    <tr>
        <td style="padding-left:10px">
            <span runat="server" style="color: #355D81; font-size: 20px; font-weight: bold" id="name">
            </span>
        </td>
        <td>
            <span runat="server" style="color: #355D81; font-size: 16px; padding-left: 30px;"
                id="position"></span>
        </td>
        <td>
            <span runat="server" style="color: #355D81; font-size: 16px; padding-left: 30px;"
                id="branchDepartment"></span>
        </td>
        <td>
            <span runat="server" style="color: #355D81; font-size: 16px; padding-left: 30px;"
                id="level"></span>
        </td>
    </tr>
</table>
<div style="clear: both;margin-bottom:10px;">
</div>
