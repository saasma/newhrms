﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PhotographCtl.ascx.cs"
    Inherits="Web.NewHR.UserControls.PhotographCtl" %>
<script type="text/javascript">
    function myFunction() {
        //some code here
        $("#popupdiv").dialog({
            modal: true,
            draggable: false,
            resizable: false,
            width: 'auto',
            height: 'auto',
            position: ['center', 'center'],
            show: 'blind',
            hide: 'blind',
            dialogClass: 'ui-dialog-osx',
            buttons: {
                "Close": function () {
                    $(this).dialog("close");
                }
            }
        });
    }
</script>
<style type="text/css">
    .ui-dialog-titlebar-close
    {
        visibility: hidden;
    }
</style>
<div style="margin-left: 20px;">
    <h4>
        Photograph</h4>
</div>
<table>
    <tr>
        <td style="padding-left: 20px; padding-top: 10px;">
            <div style="width: 180px; height: 180px;">
                <ext:Image ID="ImgEmployee" runat="Server" Width="180" Height="180">
                </ext:Image>
            </div>
        </td>
    </tr>
    <tr>
        <td style="padding-left: 10px; padding-top: 10px;">
            <ext:FileUploadField ID="fup" runat="server" ButtonText="Browse" Icon="Add" Width="300"
                Visible="true" ButtonOffset="1">
                 <DirectEvents>
                    <Change OnEvent="btnUpload_Click" />
                </DirectEvents>
            </ext:FileUploadField>
        </td>
       
    </tr>
</table>
<br />
<div style="margin-left: 20px;">
    <input type="button" id="btnclick" value="Crop Photograph" style="background-color: #428BCA;
        color: White; visibility: hidden;" />
</div>
<div style="margin-left: 20px;">
    <ext:Image ID="ImgEmployeeThumbprint" runat="Server" Width="90" Height="90">
    </ext:Image>
</div>
<div id="popupdiv" title="" style="display: none; background-color: Silver;">
    <asp:Image ID="Image1" runat="server" />
    <br />
    <ext:Button runat="server" Text="Crop" ID="btnCrop" Cls="bluebutton left">
        <DirectEvents>
            <Click OnEvent="btnCropImage_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
        <Listeners>
        </Listeners>
    </ext:Button>
</div>
<input type="hidden" runat="server" id="_xField" />
<input type="hidden" runat="server" id="_yField" />
<input type="hidden" runat="server" id="_widthField" />
<input type="hidden" runat="server" id="_heightField" />
<script type="text/javascript">
    $(function () {
        $('#btnclick').click(function () {
            $("#popupdiv").dialog({
                modal: true,
                draggable: false,
                resizable: false,
                width: 'auto',
                height: 'auto',
                position: ['center', 'center'],
                show: 'blind',
                hide: 'blind',
                dialogClass: 'ui-dialog-osx',
                buttons: {
                    "Close": function () {
                        $(this).dialog("close");
                    }
                }
            });

        });


    })

      

</script>
<script type="text/javascript">

    function hideDiv() {
        document.getElementById('popupdiv').style.display = "none";
    }

    var editorID = '<%= Image1.ClientID %>';

    jQuery(function () {
        jQuery('#' + editorID).Jcrop({
            onChange: showCoords,
            onSelect: showCoords,
            aspectRatio: 1
        });
    });

    function showCoords(c) {

        var xField = document.getElementById('<%= _xField.ClientID %>');
        var yField = document.getElementById('<%= _yField.ClientID %>');
        var widthField = document.getElementById('<%= _widthField.ClientID %>');
        var heightField = document.getElementById('<%= _heightField.ClientID %>');

        xField.value = c.x;
        yField.value = c.y;
        widthField.value = c.w;
        heightField.value = c.h;
    }

    
    
</script>
