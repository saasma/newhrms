﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;

namespace Web.NewHR.UserControls
{
    public partial class TrainingPopupCtrl : BaseUserControl
    {
      
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        protected void Initialise()
        {
            int EmployeeID = GetEmployeeID();       
            CommonManager _CommonManager = new CommonManager();
            cmbCountry.Store[0].DataSource = _CommonManager.GetAllCountries();
            cmbCountry.Store[0].DataBind();

            cmbTrainingType.Store[0].DataSource = CommonManager.GetTrainingTypeList();
            cmbTrainingType.Store[0].DataBind();
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            this.SaveFormData();
        }

        private void SaveFormData()
        {

            Status myStatus = new Status();
            HTraining _HTraining = new HTraining();
            _HTraining.EmployeeId = this.GetEmployeeID();
            bool isSave = bool.Parse(this.hdnTrainingID.Text == "" ? "true" : "false");
            if (!isSave)
                _HTraining.TrainingId = int.Parse(this.hdnTrainingID.Text);
            if (!string.IsNullOrEmpty(cmbTrainingType.SelectedItem.Text))
            {
                _HTraining.TrainingTypeID = int.Parse(cmbTrainingType.SelectedItem.Value);
                _HTraining.TrainingTypeName = cmbTrainingType.SelectedItem.Text;
            }
            _HTraining.TrainingName = txtTrainingName.Text.Trim();
            _HTraining.InstitutionName = txtInstitute.Text.Trim();
            _HTraining.ResourcePerson = txtResourcePerson.Text.Trim();
            _HTraining.Venue = txtVenue.Text.Trim();
            if (!string.IsNullOrEmpty(cmbCountry.SelectedItem.Text))
            {
                _HTraining.Country = cmbCountry.SelectedItem.Text;
            }


            if (!string.IsNullOrEmpty(txtDuration.Text.Trim()))
                _HTraining.Duration = int.Parse(txtDuration.Text.Trim());

            if (!string.IsNullOrEmpty(cmbDurationType.SelectedItem.Text))
            {
                _HTraining.DurationTypeID = int.Parse(cmbDurationType.SelectedItem.Value);
                _HTraining.DurationTypeName = cmbDurationType.SelectedItem.Text;
            }

            if (!string.IsNullOrEmpty(calStartDate.Text.Trim()))
            {
                _HTraining.TrainingFrom = calStartDate.Text.Trim();
                _HTraining.TrainingFromEngDate = BLL.BaseBiz.GetEngDate(_HTraining.TrainingFrom, IsEnglish);
            }
            if (!string.IsNullOrEmpty(calEndDate.Text.Trim()))
            {
                _HTraining.TrainingTo = calEndDate.Text.Trim();
                _HTraining.TrainingToEngDate = BLL.BaseBiz.GetEngDate(_HTraining.TrainingTo, IsEnglish);
            }
            if (!string.IsNullOrEmpty(txtCostOfTraining.Text.Trim()))
                _HTraining.CostOfTraining = decimal.Parse(txtCostOfTraining.Text.Trim());

            if (!string.IsNullOrEmpty(txtTotalCost.Text.Trim()))
                _HTraining.TotalCost = decimal.Parse(txtTotalCost.Text.Trim());

            if (!string.IsNullOrEmpty(cmbBond.SelectedItem.Text))
            {
                _HTraining.Bond = bool.Parse(cmbBond.SelectedItem.Value);
            }
            if (!string.IsNullOrEmpty(cmbBondType.SelectedItem.Text))
            {
                _HTraining.BondTypeID = int.Parse(cmbBondType.SelectedItem.Value);
                _HTraining.BondTypeName = cmbBondType.SelectedItem.Text;
                if (!string.IsNullOrEmpty(txtBondPeriod.Text.Trim()))
                    _HTraining.BondPeriod = int.Parse(txtBondPeriod.Text.Trim());
            }

            if (!string.IsNullOrEmpty(txtNote.Text.Trim()))
                _HTraining.Note = txtNote.Text.Trim();


            //file upload section
            string UserFileName = this.FileTrainingDocumentUpload.FileName;

            string ext = Path.GetExtension(UserFileName);

            if (ext.ToLower() == ".exe")
            {
                FileTrainingDocumentUpload.Reset();
                FileTrainingDocumentUpload.Focus();
                NewMessage.ShowWarningMessage("Please upload proper file.");
                return;
            }

            string ServerFileName = Guid.NewGuid().ToString() + ext;

            string relativePath = @"~/uploads/" + ServerFileName;

            if (this.UploadFileTraining(relativePath))
            {
                double fileSize = FileTrainingDocumentUpload.PostedFile.ContentLength;

                _HTraining.FileFormat = Path.GetExtension(this.FileTrainingDocumentUpload.FileName).Replace(".", "").Trim();
                _HTraining.FileType = this.FileTrainingDocumentUpload.PostedFile.ContentType;
                _HTraining.FileLocation = @"../Uploads/";
                _HTraining.ServerFileName = ServerFileName;
                _HTraining.UserFileName = UserFileName;
                _HTraining.Size = this.ToSizeString(fileSize);

            }

            myStatus = NewHRManager.Instance.InsertUpdateTraining(_HTraining, isSave);

            if (myStatus.IsSuccess)
            {
                Window win = (Window)this.Parent.FindControl("AETrainingWindow");
                win.Close();
                if (isSave)
                    NewMessage.ShowNormalMessage("Record Saved Successfully.", "reloadTrainingGrid()");
                else
                    NewMessage.ShowNormalMessage("Record Updated Successfully.", "reloadTrainingGrid()");    
            }
            else
                NewMessage.ShowWarningMessage(myStatus.ErrorMessage);
        }

        protected string ToSizeString(double bytes)
        {

            string[] SizeSuffixes = { "bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };
            int mag = (int)Math.Log(bytes, 1024);
            decimal adjustedSize = (decimal)bytes / (1 << (mag * 10));
            return string.Format("{0:n1} {1}", adjustedSize, SizeSuffixes[mag]);
        }     

        protected bool UploadFileTraining(string relativePath)
        {

            if (this.FileTrainingDocumentUpload.HasFile)
            {

                int fileSize = FileTrainingDocumentUpload.PostedFile.ContentLength;
                this.FileTrainingDocumentUpload.PostedFile.SaveAs(Server.MapPath(relativePath));
                return true;
            }
            else
                return false;

        }


        protected int GetEmployeeID()
        {
            //Guid? AssetID = null;
            int EmpID = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                EmpID = int.Parse(Request.QueryString["ID"]);
            }
            else
            {
                EmpID = SessionManager.CurrentLoggedInEmployeeId;
            }
            return EmpID;
        }

        protected void btnAddNewLine_Click(object sender, DirectEventArgs e)
        {
            this.ClearFields();
         

        }

       
        private object[] TrainingFillData
        {
            get
            {
                return new object[]
            {
                new object[] { "", "", "",""},
               
            };
            }
        }      

        protected void DeleteTraining(int ID, string path)
        {

            bool result = NewHRManager.DeleteTrainingByID(ID);
            if (result)
            {
                if (File.Exists(path))
                    File.Delete(path);
                //refresh grid
                Response.Redirect("");
            }

        }

        public void ClearFields()
        {
            this.hdnTrainingID.Text = "";
            cmbTrainingType.Value = "";
            txtTrainingName.Text = "";
            txtInstitute.Text = "";
            cmbCountry.Value = "";
            txtDuration.Text = "";
            cmbDurationType.Value = "";
            calStartDate.Text = "";
            calEndDate.Text = "";
            txtCostOfTraining.Text = "";
            txtTotalCost.Text = "";
            txtResourcePerson.Text = "";
            txtVenue.Text = "";
            txtBondPeriod.Text = "";
            cmbBond.Value = "";
            cmbBondType.Value = "";
            lnkFileName.Hide();
            lnkFileName.Text = "";
            lnkDeleteFile.Hide();
            txtBondPeriod.Show();
            cmbBondType.Show();
            txtNote.Text = "";
            btnSave.Text = Resources.Messages.Save;
            FileTrainingDocumentUpload.Reset();
        }

        public void editTraining(int TrainingID)
        {

            HTraining _HTraining = NewHRManager.GetTrainingDetailsById(TrainingID);
            this.hdnTrainingID.Text = TrainingID.ToString();

            //cmbTrainingType.Store[0].ClearFilter();
            if (_HTraining.TrainingTypeID != null)
                cmbTrainingType.SetValue(_HTraining.TrainingTypeID.ToString());
            else
                cmbTrainingType.ClearValue();

            //cmbTrainingType.SetValue(_HTraining.TrainingTypeID.ToString());

            txtTrainingName.Text = _HTraining.TrainingName;
            txtInstitute.Text = _HTraining.InstitutionName;
            cmbCountry.SetValue(_HTraining.Country);
            txtResourcePerson.Text = _HTraining.ResourcePerson == null ? "" : _HTraining.ResourcePerson;
            txtVenue.Text = _HTraining.Venue == null ? "" : _HTraining.Venue;

            if (_HTraining.Duration != null)
                txtDuration.Text = _HTraining.Duration.ToString();
            else
                txtDuration.Text = "";

            if (_HTraining.DurationTypeID != null)
                cmbDurationType.SetValue(_HTraining.DurationTypeID.ToString());
            else
                cmbDurationType.ClearValue();

            calStartDate.Text = _HTraining.TrainingFrom;
            calEndDate.Text = _HTraining.TrainingTo;
            if (_HTraining.CostOfTraining != null)
            {
                txtCostOfTraining.Text = decimal.Parse(_HTraining.CostOfTraining.ToString()).ToString("0.00");
            }
            else
                txtCostOfTraining.Text = "";
            if (_HTraining.TotalCost != null)
            {
                txtTotalCost.Text = decimal.Parse(_HTraining.TotalCost.ToString()).ToString("0.00");
            }
            else
                txtTotalCost.Text = "";

            txtBondPeriod.Text = _HTraining.BondPeriod.ToString();
            cmbBond.SetValue(_HTraining.Bond.ToString());
            cmbBondType.SetValue(_HTraining.BondTypeID.ToString());
            txtNote.Text = _HTraining.Note;
            if (_HTraining.Bond.ToString() == "True")
            {
                cmbBondType.Show();
                txtBondPeriod.Show();
            }
            else
            {
                cmbBondType.Hide();
                txtBondPeriod.Hide();
            }
            txtNote.Text = _HTraining.Note;
            if (!string.IsNullOrEmpty(_HTraining.UserFileName))
            {
                lnkFileName.Show();
                lnkFileName.Text = _HTraining.UserFileName;
                lnkDeleteFile.Show();
            }
            else
            {
                lnkFileName.Hide();
                lnkFileName.Text = "";
                lnkDeleteFile.Hide();
            }
            btnSave.Text = Resources.Messages.Update;           
        }

        public void cmbBond_Change(object sender, DirectEventArgs e)
        {
            if (cmbBond.SelectedItem.Text == "Yes")
            {
                cmbBondType.Show();
                txtBondPeriod.Show();

            }
            else
            {
                cmbBondType.Hide();
                txtBondPeriod.Hide();

            }
        }

        public void HideButtons()
        {
            btnSave.Hidden = true;
            divOr.Visible = false;
            LinkButton2.Hidden = true;
        }

        protected void lnkDeleteFile_Click(object sender, DirectEventArgs e)
        {
            int TrainingID = int.Parse(hdnTrainingID.Text);
            Status myStatus = new Status();

            HTraining _HTraining = NewHRManager.GetTrainingDetailsById(TrainingID);
            string path = Server.MapPath(@"~/uploads/" + _HTraining.ServerFileName); 
            myStatus = NewHRManager.Instance.DeleteTrainingFile(TrainingID);
            if (myStatus.IsSuccess)
            {
                if (File.Exists(path))
                {
                    File.Delete(path);
                    lnkDeleteFile.Hide();
                    lnkFileName.Hide();
                    NewMessage.ShowNormalMessage("file deleted successfully");
                    //refresh grid

                }
            }

        }

        protected void lnkFileName_Click(object sender, DirectEventArgs e)
        {
            int trainingId = int.Parse(hdnTrainingID.Text);
            HTraining doc = NewHRManager.GetTrainingDetailsById(trainingId);

            string path = Server.MapPath(@"~/uploads/" + doc.FileLocation + doc.ServerFileName);
            string contentType = doc.FileType;
            string name = doc.UserFileName + "." + doc.FileFormat;

            byte[] bytes = File.ReadAllBytes(path);

            Context.Response.Clear();
            Context.Response.ClearHeaders();
            Context.Response.AppendHeader("Content-Disposition", "attachment; filename=\"" + name + "\"");
            Context.Response.ContentType = contentType;
            Context.Response.BinaryWrite(bytes);
            Context.Response.End();
        }
    }
}