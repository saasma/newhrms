﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CumulativeAttendanceSummaryCtl.ascx.cs"
    Inherits="Web.NewHR.UserControls.CumulativeAttendanceSummaryCtl" %>
<script type="text/javascript">

    function refreshWindow() {
       // alert('h');
        <%=btnLoad.ClientID %>.fireEvent('click');
    }

    function isPayrollSelected(btn) {
        if(typeof(periodId)=='undefined')
        {
            alert("Please load the list for the period first before importing.");
            return false;
        }
        var ret = shiftPopup("PayrollPeriodId=" + periodId);
        return false;
    }

    function searchList() {
              <%=PagingToolbar1.ClientID %>.doRefresh();
        }

 
   
    function SelEmpSearch()
    {
        <%=hdnEmpSearch.ClientID %>.setValue(<%=cmbEmpSearch.ClientID %>.getValue());
    }

     var CommandHandler = function(command, record){
            <%= hdnEmployeeId.ClientID %>.setValue(record.data.EIN);
            
            if(command=="Edit")
            {
                <%= btnEdit.ClientID %>.fireEvent('click');
            }
        };

    function selectEmployee()
    {
        <%= hdnEmployeeId.ClientID %>.setValue(<%= cmbEmployee.ClientID %>.getValue());
    }


</script>
<script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>
<script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>
<ext:Hidden ID="hdnEmployeeId" runat="server" />
<ext:Hidden ID="hdnBranchDepartmentId" runat="server" />
<ext:Hidden ID="hdnMsg" runat="server" />
<ext:Hidden ID="hdnEmpSearch" runat="server" />
<ext:Hidden ID="hdnSortBy" runat="server" />

<ext:Button ID="btnEdit" runat="server" Hidden="true">
    <DirectEvents>
        <Click OnEvent="btnEdit_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:Button>

<ext:Store runat="server" ID="storeSearch" AutoLoad="false">
    <Proxy>
        <ext:AjaxProxy Url="../../Handler/EmpSearchID.ashx">
            <ActionMethods Read="GET" />
            <Reader>
                <ext:JsonReader Root="plants" TotalProperty="total" />
            </Reader>
        </ext:AjaxProxy>
    </Proxy>
    <Model>
        <ext:Model ID="Model5" IDProperty="EmployeeId" Name="ItemLineModel" runat="server">
            <Fields>
                <ext:ModelField Name="Name" Type="String" />
                <ext:ModelField Name="EmployeeId" Type="String" />
            </Fields>
        </ext:Model>
    </Model>
</ext:Store>
<div class="alert alert-info" style="margin-top: 10px; padding-top: 0px!important;">
    <ext:Container ID="listingContainer" runat="server">
        <Content>
            <table>
                <tr>
                    <td>
                        <ext:ComboBox Width="100" MarginSpec="0 5 0 5" LabelSeparator="" LabelAlign="Top"
                            QueryMode="Local" ID="cmbYear" ForceSelection="true" DisplayField="Year" ValueField="Year"
                            runat="server" FieldLabel="Year">
                            <Store>
                                <ext:Store ID="Store1" runat="server">
                                    <Model>
                                        <ext:Model ID="Model2" IDProperty="Year" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="Year" Type="String" />
                                                <ext:ModelField Name="Year" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                    </td>
                    <td>
                        <ext:ComboBox Width="100" MarginSpec="0 5 0 5" LabelSeparator="" LabelAlign="Top"
                            QueryMode="Local" ID="cmbMonth" ForceSelection="true" DisplayField="Value" ValueField="Key"
                            runat="server" FieldLabel="Month">
                            <Store>
                                <ext:Store ID="Store2" runat="server">
                                    <Model>
                                        <ext:Model ID="Model3" IDProperty="Key" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="Key" Type="String" />
                                                <ext:ModelField Name="Value" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                    </td>
                    <td>
                        <ext:ComboBox Width="140" MatchFieldWidth="false" MarginSpec="0 5 0 5" LabelSeparator=""
                            LabelAlign="Top" QueryMode="Local" ID="cmbBranch" DisplayField="Name" ValueField="BranchId"
                            runat="server" FieldLabel="Branch">
                            <Items>
                                <ext:ListItem Index="0" Value="-1" Text="All">
                                </ext:ListItem>
                            </Items>
                            <Store>
                                <ext:Store ID="Store5" runat="server">
                                    <Model>
                                        <ext:Model ID="Model7" IDProperty="BranchId" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="BranchId" />
                                                <ext:ModelField Name="Name" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                    </td>
                    <td style='display:none'>
                        <ext:ComboBox FieldLabel="Department" MatchFieldWidth="false" ID="cmbDepartment"
                            Width="140" MarginSpec="0 5 0 5" runat="server" ValueField="DepartmentId" DisplayField="Name"
                            LabelAlign="Top" LabelSeparator="" QueryMode="Local">
                            <Items>
                                <ext:ListItem Index="0" Value="-1" Text="All">
                                </ext:ListItem>
                            </Items>
                            <Store>
                                <ext:Store ID="Store6" runat="server">
                                    <Model>
                                        <ext:Model ID="Model8" IDProperty="DepartmentId" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="DepartmentId" />
                                                <ext:ModelField Name="Name" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                    </td>
                    <td>
                        <ext:ComboBox ID="cmbLevel" MatchFieldWidth="false" runat="server" FieldLabel="Level/Position"
                            Width="140" MarginSpec="0 5 0 5" DisplayField="Name" ValueField="LevelId" QueryMode="Local"
                            LabelSeparator="" LabelAlign="Top">
                            <Items>
                                <ext:ListItem Index="0" Value="-1" Text="All">
                                </ext:ListItem>
                            </Items>
                            <Store>
                                <ext:Store ID="storeLevel" runat="server">
                                    <Model>
                                        <ext:Model ID="modelLevel" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="LevelId" Type="Int" />
                                                <ext:ModelField Name="Name" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                    </td>
                    <td>
                        <ext:ComboBox MatchFieldWidth="false" FieldLabel="Designation" ID="cmbDesignation"
                            Width="140" MarginSpec="0 5 0 5" runat="server" ValueField="DesignationId" DisplayField="LevelAndDesignation"
                            LabelAlign="Top" LabelSeparator="" QueryMode="Local">
                            <Items>
                                <ext:ListItem Index="0" Value="-1" Text="All">
                                </ext:ListItem>
                            </Items>
                            <Store>
                                <ext:Store ID="Store7" runat="server">
                                    <Model>
                                        <ext:Model ID="Model9" IDProperty="DesignationId" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="DesignationId" />
                                                <ext:ModelField Name="LevelAndDesignation" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                    </td>
                    <td>
                        <ext:ComboBox LabelSeparator="" ID="cmbEmpSearch" FieldLabel="Search Employee" EmptyText="Employee Name"
                            LabelWidth="80" LabelAlign="Top" runat="server" DisplayField="Name" ValueField="EmployeeId"
                            StoreID="storeSearch" TypeAhead="false" Width="200" PageSize="9999" HideBaseTrigger="true"
                            MinChars="1" TriggerAction="All" ForceSelection="false">
                            <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                                <ItemTpl ID="ItemTpl2" runat="server">
                                    <Html>
                                        <tpl>
                                                        <div class="search-item">
                                                                        <span>{Name}</span>  
                                                         </div>
					                            </tpl>
                                    </Html>
                                </ItemTpl>
                            </ListConfig>
                            <Triggers>
                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                            </Triggers>
                            <Listeners>
                                <Select Handler="this.getTrigger(0).show(); SelEmpSearch();" />
                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                <TriggerClick Handler="if (index == 0) { 
                                                       this.clearValue(); 
                                                       this.getTrigger(0).hide();
                                                   }" />
                            </Listeners>
                        </ext:ComboBox>
                    </td>
                    <td style="padding-left: 10px">
                        <ext:Button ID="btnLoad" runat="server" Text="Load" Width="100" Cls="btn btn-default"
                            MarginSpec="25 10 10 10">
                            <Listeners>
                                <Click Fn="searchList">
                                </Click>
                            </Listeners>
                        </ext:Button>
                    </td>
                    <td style="padding-left: 10px"  valign="bottom">
                        <ext:Button runat="server"  AutoPostBack="true" OnClick="btnExport_Click"
                            ID="btnExport" Text="<i></i>Export To Excel">
                        </ext:Button>

                          <asp:LinkButton ID="LinkButton1" runat="server"  Text="Import from Excel" OnClientClick="valGroup = 'SaveUpdate'; if(CheckValidation()) return isPayrollSelected(this); else return false;"
                            CssClass=" excel marginRight tiptip" Style="float: left;margin-top:8px;" />
                    </td>
                    <td style="padding-left: 10px"  valign="bottom">
                        <ext:Button runat="server" ID="btnAddNew" Text="<i></i>Add New">
                            <DirectEvents>
                                <Click OnEvent="btnAddNew_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                    </td>
                </tr>
            </table>
        </Content>
    </ext:Container>
</div>
<ext:KeyMap ID="KeyMap1" runat="server" Target="={#{listingContainer}.getBody()}">
    <Binding>
        <ext:KeyBinding Handler="#{btnLoad}.fireEvent('click');">
            <Keys>
                <ext:Key Code="ENTER" />
            </Keys>
        </ext:KeyBinding>
    </Binding>
</ext:KeyMap>
<ext:GridPanel StyleSpec="margin-top:15px;" ID="gridBranchTransfer" runat="server"
    OnReadData="Store_ReadData" AutoScroll="true">
    <Store>
        <ext:Store PageSize="25" ID="storeEmpList" runat="server" AutoLoad="false" OnReadData="Store_ReadData"
            RemotePaging="true" RemoteSort="true">
            <Proxy>
                <ext:PageProxy />
            </Proxy>
            <Model>
                <ext:Model ID="Model1" runat="server" IDProperty="EIN">
                    <Fields>
                        <ext:ModelField Name="EIN" Type="Int" />
                        <ext:ModelField Name="SN" Type="String" />
                        <ext:ModelField Name="EIN" Type="String" />
                        <ext:ModelField Name="Name" Type="String" />
                        <ext:ModelField Name="Level" Type="String" />
                        <ext:ModelField Name="Designation" Type="String" />
                        <ext:ModelField Name="Branch" Type="String" />
                        <ext:ModelField Name="Department" Type="String" />
                        <ext:ModelField Name="LateDays" Type="String" />
                        <ext:ModelField Name="LeaveDays" Type="String" />
                        <ext:ModelField Name="AbsentDays" Type="String" />
                        <ext:ModelField Name="LWPDays" Type="String" />
                        <ext:ModelField Name="HalfDayLeaveDaysCount" Type="String" />
                    </Fields>
                </ext:Model>
            </Model>
            <Sorters>
                <ext:DataSorter Property="EmployeeId" Direction="ASC" />
            </Sorters>
        </ext:Store>
    </Store>
    <ColumnModel ID="ColumnModel1" runat="server">
        <Columns>
            <ext:Column ID="colFromDate" runat="server" Text="SN" Locked="true" DataIndex="SN"
                MenuDisabled="false" Sortable="false" Align="Right" Width="50" />
            <ext:Column ID="Column1" runat="server" Text="EIN" Locked="true" DataIndex="EIN"
                MenuDisabled="false" Sortable="true" Align="Right" Width="50" />
            <ext:Column ID="Column3" runat="server" Text="Name" Locked="true" DataIndex="Name"
                MenuDisabled="false" Sortable="true" Align="Left" Width="150" />
            <ext:Column ID="ColumnLevel" runat="server" Text="Level" DataIndex="Level" MenuDisabled="false"
                Sortable="true" Align="Left" Width="120" />
            <ext:Column ID="ColumnDesignation" runat="server" Text="Designation" DataIndex="Designation"
                MenuDisabled="false" Sortable="true" Align="Left" Width="120" />
            <ext:Column ID="ColumnBranch" runat="server" Text="Branch" DataIndex="Branch" MenuDisabled="false"
                Sortable="true" Align="Left" Width="120" />
            <ext:Column ID="ColumnDepartment" runat="server" Text="Department" DataIndex="Department"
                MenuDisabled="false" Sortable="true" Align="Left" Width="120" />
            <ext:Column ID="income1" runat="server" Text="Late Days" DataIndex="LateDays" MenuDisabled="false"
                Sortable="true" Align="Center" Width="100">
            </ext:Column>
            <ext:Column ID="Column8" runat="server" Text="Leave Days" DataIndex="LeaveDays" MenuDisabled="false"
                Sortable="true" Align="Center" Width="120">
            </ext:Column>
              <ext:Column ID="Column5" runat="server" Text="Half Leave Count" DataIndex="HalfDayLeaveDaysCount" MenuDisabled="false"
                Sortable="true" Align="Center" Width="120">
            </ext:Column>
            <ext:Column ID="Column2" runat="server" Text="Absent Days" DataIndex="AbsentDays" MenuDisabled="false"
                Sortable="true" Align="Center" Width="100">
            </ext:Column>
            <ext:Column ID="Column4" runat="server" Text="LWP Days" DataIndex="LWPDays" MenuDisabled="false"
                Sortable="true" Align="Center" Width="120">
            </ext:Column>
            <ext:CommandColumn ID="CommandColumn1" runat="server" Width="40" Text="" Align="Center">
                <Commands>
                    <ext:CommandSeparator />
                    <ext:GridCommand Cls="editGridButton" ToolTip-Text="Edit" Text="<i class='fa fa-pencil'></i>"
                        CommandName="Edit" />
                </Commands>
                <Listeners>
                    <Command Handler="CommandHandler(command,record);" />
                </Listeners>
            </ext:CommandColumn>
        </Columns>
    </ColumnModel>
    <BottomBar>
        <ext:PagingToolbar ID="PagingToolbar1" runat="server" StoreID="storeEmpList" DisplayInfo="true">
        </ext:PagingToolbar>
    </BottomBar>
</ext:GridPanel>




<ext:Window ID="WCumulativeSummary" runat="server" Title="Cumulative Summary" Icon="Application" ButtonAlign="Left"
        Width="400" Height="250" BodyPadding="5" Hidden="true" Modal="true">
        <Content>
            <table class="fieldTable">
                <tr>
                    <td>
                        <ext:Store runat="server" ID="store3" AutoLoad="false">
                        <Proxy>
                            <ext:AjaxProxy Url="../Handler/EmpSearchID.ashx">
                                <ActionMethods Read="GET" />
                                <Reader>
                                    <ext:JsonReader Root="plants" TotalProperty="total" />
                                </Reader>
                            </ext:AjaxProxy>
                        </Proxy>
                        <Model>
                            <ext:Model ID="Model4" IDProperty="Name" Name="ItemLineModel" runat="server">
                                <Fields>
                                    <ext:ModelField Name="Name" Type="String" />
                                    <ext:ModelField Name="EmployeeId" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                    <ext:ComboBox LabelSeparator="" ID="cmbEmployee" runat="server" DisplayField="Name" LabelWidth="110"
                        FieldLabel="Employee" LabelAlign="Left" ValueField="EmployeeId" EmptyText="Search Employee"
                        StoreID="storeSearch" TypeAhead="false" Width="350" HideBaseTrigger="true" MinChars="1"
                        TriggerAction="All" ForceSelection="false">
                        <ListConfig LoadingText="Searching..." MinWidth="350" StyleSpec="border-top:1px solid #98c0f4;">
                            <ItemTpl ID="ItemTpl1" runat="server">
                                <Html>
                                    <tpl>
                                            <div class="search-item">
                                                            <span>{Name}</span>  
                                             </div>
					                </tpl>
                                </Html>
                            </ItemTpl>
                        </ListConfig>
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                        </Triggers>
                        <Listeners>
                            <Select Handler="this.getTrigger(0).show(); selectEmployee();" />
                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                            <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide(); selectEmployee();
                                       }" />
                        </Listeners>
                    </ext:ComboBox>
                        
                        <asp:RequiredFieldValidator Display="None" ID="rfvName" runat="server" ValidationGroup="SaveUpdCAS"
                            ControlToValidate="cmbEmployee" ErrorMessage="Name is required." />
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:NumberField ID="txtLateDays" AllowDecimals="true" Width="200" runat="server"  LabelWidth="110" LabelSeparator="" MinValue="0"
                            FieldLabel="Late Days" LabelAlign="Left">
                        </ext:NumberField>
                        <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator1" runat="server" ValidationGroup="SaveUpdCAS"
                            ControlToValidate="txtLateDays" ErrorMessage="Late Days is required." />
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:NumberField ID="txtLeaveDays" AllowDecimals="true" Width="200" runat="server"  LabelWidth="110" LabelSeparator="" MinValue="0"
                            FieldLabel="Leave Days" LabelAlign="Left">
                        </ext:NumberField>
                        <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator2" runat="server" ValidationGroup="SaveUpdCAS"
                            ControlToValidate="txtLeaveDays" ErrorMessage="Leave Days is required." />
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:NumberField ID="txtHalfLeaveCount" AllowDecimals="true" Width="200" runat="server"  LabelWidth="110" LabelSeparator="" MinValue="0"
                            FieldLabel="Half Leave Count" LabelAlign="Left">
                        </ext:NumberField>
                    </td>
                </tr>
            </table>
        </Content>
        <Buttons>
            <ext:Button runat="server" ID="btnSave" Text="Save">
                    <DirectEvents>
                        <Click OnEvent="btnSave_Click">
                            <EventMask ShowMask="true" />
                        </Click>
                    </DirectEvents>
                    <Listeners>
                        <Click Handler="valGroup = 'SaveUpdCAS'; if(CheckValidation()) return this.disable(); else return false;">
                        </Click>
                    </Listeners>
                </ext:Button>

                <ext:Button runat="server" ID="LinkButton2" MarginSpec="0 0 0 20"
                    Text="Cancel">
                    <Listeners>
                        <Click Handler="#{WCumulativeSummary}.hide();">
                        </Click>
                    </Listeners>
                </ext:Button>
        </Buttons>
    </ext:Window>