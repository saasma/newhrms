﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;

namespace Web.NewHR.UserControls
{
    public partial class CitizenshipCtrlView : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }
        protected void Initialise()
        {
            int EmployeeID = GetEmployeeID();

            this.LoadCitizenshipGrid(EmployeeID);

        }

        protected void CitizenshipDownLoad(int ID)
        {
            HCitizenship doc = NewHRManager.GetCitizenshipDetailsById(ID);

            string path = Context.Server.MapPath(@"~/uploads/" + doc.FileLocation + doc.ServerFileName);
            string contentType = doc.FileType;
            string name = doc.UserFileName;

            byte[] bytes = File.ReadAllBytes(path);

            Context.Response.Clear();
            Context.Response.ClearHeaders();
            Context.Response.AppendHeader("Content-Disposition", "attachment; filename=\"" + name + "\"");
            Context.Response.ContentType = contentType;
            Context.Response.BinaryWrite(bytes);
            Context.Response.End();
        }

        protected void LoadCitizenshipGrid(int EmployeeID)
        {
            List<HCitizenship> _HCitizenship = NewHRManager.GetCitizenshipByEmployeeID(EmployeeID);
            if (_HCitizenship != null)
            {
                this.StoreCitizenship.DataSource = _HCitizenship;
                this.StoreCitizenship.DataBind();
            }
            else
            {
                this.StoreCitizenship.DataSource = this.CitizenshipFillData;
                this.StoreCitizenship.DataBind();
            }

            if (_HCitizenship.Count <= 0)
                GridCitizenship.Hide();
        }
        protected int GetEmployeeID()
        {
            int EmpID = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                EmpID = int.Parse(Request.QueryString["ID"]);
            }
            else
                EmpID = SessionManager.CurrentLoggedInEmployeeId;
            return EmpID;
        }

        private object[] CitizenshipFillData
        {
            get
            {
                return new object[]
            {
                new object[] { "", "", "",""},
               
            };
            }
        }

        public void btnDownloadCtznFile_Click(object sender, EventArgs e)
        {
            int citizshipId = int.Parse(hdnCitizenshipIdValue.Text);
            this.CitizenshipDownLoad(citizshipId);
        }
    }
}