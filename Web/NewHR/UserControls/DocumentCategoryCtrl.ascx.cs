﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;
using System.Text;


namespace Web.NewHR.UserControls
{
    public partial class DocumentCategoryCtrl : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void AddNewCategory(string IsCategoryPage)
        {
            btnCategorySave.Text = "Save";
            txtCategoryName.Text = "";
            txtCategoryCode.Clear();
            hdnDocumentCategory.Text = "";
            hdnIsCategoryPage.Text = IsCategoryPage;

            WDocumentCategory.Center();
            WDocumentCategory.Show();
        }

        protected void btnCategorySave_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveUpdCategory");
            if (Page.IsValid)
            {
                bool isSave = true;

                DocCategory obj = new DocCategory();

                obj.Name = txtCategoryName.Text.Trim();
                obj.Code = txtCategoryCode.Text.Trim();

                if (!string.IsNullOrEmpty(hdnDocumentCategory.Text))
                {
                    obj.CategoryID = new Guid(hdnDocumentCategory.Text);
                    isSave = false;
                }
                else
                    obj.CategoryID = Guid.NewGuid();

                Status status = DocumentManager.SaveUpdateDocCategory(obj, isSave);
                if (status.IsSuccess)
                {
                    if (hdnIsCategoryPage.Text == "1")
                    {
                        if (!string.IsNullOrEmpty(hdnDocumentCategory.Text))
                            NewMessage.ShowNormalMessage("Record updated successfully.", "reloadCategoryGrid");
                        else
                            NewMessage.ShowNormalMessage("Record saved successfully.", "reloadCategoryGrid");
                    }
                    else
                    {
                        X.Js.Call("ReloadCategoryCombo", obj.CategoryID.ToString());
                    }

                    WDocumentCategory.Close();
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }

            }
        }

        public void EditCategory(Guid categoryID)
        {
            DocCategory objDocCategory = DocumentManager.GetDocCategoryById(categoryID);
            if (objDocCategory != null)
            {
                hdnIsCategoryPage.Text = "1";
                hdnDocumentCategory.Text = categoryID.ToString();
                txtCategoryName.Text = objDocCategory.Name;
                txtCategoryCode.Text = objDocCategory.Code;
                btnCategorySave.Text = "Update";
                WDocumentCategory.Center();
                WDocumentCategory.Show();
            }
        }

    }
}