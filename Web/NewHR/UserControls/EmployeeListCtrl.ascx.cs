﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using Utils.Helper;
using System.IO;
using Utils;
using Utils.Security;
namespace Web.NewHR.UserControls
{
    public partial class EmployeeListCtrl : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPagePermission();

            if (!X.IsAjaxRequest && !IsPostBack)
            {
                Initialise();

                ProcessSectionLevelPermission();
            }
        }

        private void Initialise()
        {
            if (BLL.BaseBiz.PayrollDataContext.CCalculations.Any(
               x => x.IsFinalSaved == true))
            {
                btnExportPopup.Visible = false;
            }

            

        }

        public void SetPagePermission()
        {
            if (SessionManager.IsCustomRole && SessionManager.CustomRole.IsReadOnly != null && SessionManager.CustomRole.IsReadOnly.Value)
            {
                //btnAddNew.Visible = false;
                btnExportPopup.Visible = false;
                btnExportImportAccounts.Visible = false;
            }
        }

        public void ProcessSectionLevelPermission()
        {
            if (SessionManager.IsCustomRole)
            {
                UUser user = UserManager.GetUserByUserName(Page.User.Identity.Name);
                int currentPageId = UserManager.GetPageIdByPageUrl(CurrentPageUrl);

                btnExportPopup.Visible = false;

                List<UPermissionSection> sections = UserManager.GetAccessibleSectionsForPage(user.RoleId.Value, currentPageId);

                foreach (UPermissionSection section in sections)
                {
                    if (section.PageSectionId == 2)
                    {
                        btnExportPopup.Visible = true;
                    }
                }

            }
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            int totalRecords = 0;

            string deparmentList = null;
            if (SessionManager.IsCustomRole)
                deparmentList = SessionManager.CustomRoleDeparmentIDList;

            bool? showRetiredOnly = null; //All = 1
            //if (cmbRet.SelectedItem.Text != "All")
            //    showRetiredOnly = bool.Parse(cmbRet.SelectedItem.Value);

            EmployeeManager empMgr = new EmployeeManager();
            //List<EEmployee> list = empMgr.GetAllEmployees(1, 9999, ref totalRecords, "",
            //    SessionManager.CurrentCompanyId, cmbEmployee.SelectedItem.Text == null ? "" : cmbEmployee.SelectedItem.Text,
            //    txtINO.Text.Trim(), showRetiredOnly,
            //     int.Parse(cmbBranch.SelectedItem.Value), int.Parse(cmbDepartment.SelectedItem.Value),
            //     int.Parse(cmbDesignation.SelectedItem.Value), int.Parse(cmbLevel.SelectedItem.Value), deparmentList,
            //     int.Parse(cmbEmpStatus.SelectedItem.Value), cmbEmpStatus.SelectedItem.Text.ToString().ToLower().Contains("only"));


            int start = 0;
            int pagesize = 50;
            int BranchID = -1;
            int DepartmentID = -1;
            int LevelID = -1;
            int DesignationID = -1;
            //int StatusID = -1;
            int CurrentPage = 1;

            bool? ShowRetired = null; //All
            string EmpID = "";
            string INO = "";
            string StatusOnly = "";

            int? total = 0;
            //bool statusOnly = false;


            ComboBox cmbBranch = (ComboBox)this.Parent.FindControl("cmbBranch");
            BranchID = int.Parse(cmbBranch.SelectedItem.Value);

            ComboBox cmbDepartment = (ComboBox)this.Parent.FindControl("cmbDepartment");
            DepartmentID = int.Parse(cmbDepartment.SelectedItem.Value);

            ComboBox cmbLevel = (ComboBox)this.Parent.FindControl("cmbLevel");
            LevelID = int.Parse(cmbLevel.SelectedItem.Value);

            ComboBox cmbDesignation = (ComboBox)this.Parent.FindControl("cmbDesignation");
            DesignationID = int.Parse(cmbDesignation.SelectedItem.Value);

           

            TextField txtINO = (TextField)this.Parent.FindControl("txtINO");
            INO = txtINO.Text;

            ComboBox cmbEmployee = (ComboBox)this.Parent.FindControl("cmbEmployee");
            EmpID = cmbEmployee.SelectedItem.Text;      

            List<EEmployee> list = empMgr.GetAllEmployees(1, 9999, ref totalRecords, "",
               SessionManager.CurrentCompanyId, EmpID,
                INO, showRetiredOnly,
                BranchID, DepartmentID,
                DesignationID, LevelID, deparmentList,
                "",-1,0,0,"",-1);


            ExcelGenerator.WriteExcel(ExcelGenerator.GetExcelForEmployeeList(list));

        }

        private void LoadLevels()
        {
            EmployeeManager empMgr = new EmployeeManager();
            int totalRecords = 0;

            string customRoleDeparmentList = null;
            if (SessionManager.IsCustomRole)
                customRoleDeparmentList = SessionManager.CustomRoleDeparmentIDList;


            PagingToolbar1.DoRefresh();
        }

        protected void btnDelete_Click(object sender, DirectEventArgs e)
        {
            int empId = int.Parse(hiddenValue.Text.Trim());
            EmployeeManager empMgr = new EmployeeManager();

            if (empMgr.Delete(empId))
            {
                X.Js.AddScript("searchList();");
                NewMessage.ShowNormalMessage(Resources.Messages.EmployeeDeletedMsg);

            }
            else
            {
                NewMessage.ShowWarningMessage("Employee could not be deleted.");

            }
        }

        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            int start = 0;
            int pagesize = 50;
            int BranchID = -1;
            int DepartmentID = -1;
            int LevelID = -1;
            int DesignationID = -1;
            int StatusID = -1;
            int CurrentPage = 1;

            bool? ShowRetired = null; //All
            string EmpID = "";
            string INO = "";
            string StatusOnly = "";

            int? total = 0;
            bool statusOnly = false;
            string customRoleDeparmentList = null;

            ComboBox cmbBranch = (ComboBox)this.Parent.FindControl("cmbBranch");
            if (cmbBranch.SelectedItem.Value != null && cmbBranch.SelectedItem.Index != -1 && !string.IsNullOrEmpty(cmbBranch.SelectedItem.Value.Trim()))
                BranchID = Convert.ToInt32(cmbBranch.SelectedItem.Value);

            ComboBox cmbDepartment = (ComboBox)this.Parent.FindControl("cmbDepartment");
            if (cmbDepartment.SelectedItem.Value != null && cmbDepartment.SelectedItem.Index != -1 && !string.IsNullOrEmpty(cmbDepartment.SelectedItem.Value.Trim()))
                DepartmentID = Convert.ToInt32(cmbDepartment.SelectedItem.Value);

            ComboBox cmbLevel = (ComboBox)this.Parent.FindControl("cmbLevel");
            if (cmbLevel.SelectedItem.Value != null && cmbLevel.SelectedItem.Index != -1 && !string.IsNullOrEmpty(cmbLevel.SelectedItem.Value.Trim()))
                LevelID = Convert.ToInt32(cmbLevel.SelectedItem.Value);

            ComboBox cmbDesignation = (ComboBox)this.Parent.FindControl("cmbDesignation");
            if (cmbDesignation.SelectedItem.Value != null && cmbDesignation.SelectedItem.Index != -1 && !string.IsNullOrEmpty(cmbDesignation.SelectedItem.Value.Trim()))
                DesignationID = Convert.ToInt32(cmbDesignation.SelectedItem.Value);

            ComboBox cmbEmpStatus = (ComboBox)this.Parent.FindControl("cmbEmpStatus");
            if (cmbEmpStatus.SelectedItem.Value != null && cmbEmpStatus.SelectedItem.Index != -1 && !string.IsNullOrEmpty(cmbEmpStatus.SelectedItem.Value.Trim()))
            {
                StatusID = Convert.ToInt32(cmbEmpStatus.SelectedItem.Value);
                statusOnly = cmbEmpStatus.SelectedItem.Text.ToLower().Contains("only");
            }

            TextField txtINO = (TextField)this.Parent.FindControl("txtINO");
            if (!string.IsNullOrEmpty(txtINO.Text))
                INO = Convert.ToString(txtINO.Text).Trim();

            ComboBox cmbEmployee = (ComboBox)this.Parent.FindControl("cmbEmployee");
            if (!string.IsNullOrEmpty(cmbEmployee.SelectedItem.Text))
                EmpID = Convert.ToString(cmbEmployee.SelectedItem.Text).Trim();      


            if (SessionManager.IsCustomRole)
                customRoleDeparmentList = SessionManager.CustomRoleDeparmentIDList;


            EmployeeManager employeeManager = new EmployeeManager();

            List<GetAllEmployeesNewResult> resultSet = employeeManager.GetAllEmployeesNew(e.Start, int.Parse(cmbPageSize.SelectedItem.Value),
                 ref total, (e.Sort[0].Property + " " + e.Sort[0].Direction).ToLower(), SessionManager.CurrentCompanyId,
                   EmpID, INO, ShowRetired, BranchID, DepartmentID, DesignationID, LevelID, customRoleDeparmentList, StatusID, statusOnly);


            e.Total = total.Value;

            if (resultSet.Any())
                storeEmpList.DataSource = resultSet;
            storeEmpList.DataBind();
        }



    }
}