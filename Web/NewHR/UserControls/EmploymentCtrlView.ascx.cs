﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;

namespace Web.NewHR.UserControls
{
    public partial class EmploymentCtrlView : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }
        protected void Initialise()
        {
            int EmployeeID = GetEmployeeID();
            this.LoadPreviousEmploymentGrid(EmployeeID);
        }

        protected void LoadPreviousEmploymentGrid(int EmployeeID)
        {
            List<HPreviousEmployment> _HPreviousEmployment = NewHRManager.GetPreviousEmploymentByEmployeeID(EmployeeID);

            if (SessionManager.CurrentLoggedInEmployeeId == 0)
                _HPreviousEmployment = _HPreviousEmployment.Where(x => x.Status == (int)HRStatusEnum.Approved).ToList();

            if (Request.QueryString["ID"] != null)
            {
                if (int.Parse(Request.QueryString["ID"].ToString()) != SessionManager.CurrentLoggedInEmployeeId)
                    _HPreviousEmployment = _HPreviousEmployment.Where(x => x.Status == (int)HRStatusEnum.Approved).ToList();
            }

            this.StorePreviousEmployment.DataSource = _HPreviousEmployment;
            this.StorePreviousEmployment.DataBind();

            if (_HPreviousEmployment.Count <= 0)
                GridPreviousEmployment.Hide();
        }

        protected int GetEmployeeID()
        {
            int EmpID = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                EmpID = int.Parse(Request.QueryString["ID"]);
            }
            else
            {
                EmpID = SessionManager.CurrentLoggedInEmployeeId;
            }
            return EmpID;
        }

    }
}