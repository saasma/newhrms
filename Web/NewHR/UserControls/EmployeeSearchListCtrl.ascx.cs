﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using Utils.Helper;
using System.IO;
using Utils;
using Utils.Security;
using Web.Helper;
namespace Web.NewHR.UserControls
{
    public partial class EmployeeSearchListCtrl : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {


            if (!X.IsAjaxRequest && !IsPostBack)
            {
                Initialise();
            }
        }

        private void Initialise()
        {
            
        }

    

    

        protected void btnExport_Click(object sender, EventArgs e)
        {
            int start = 0;
            int pagesize = 50;
            int BranchID = -1;
            int DepartmentID = -1;
            int LevelID = -1;
            int DesignationID = -1;
            int StatusID = -1;
            int CurrentPage = 1;

            bool? ShowRetired = null; //All
            string EmpID = "";
            string INO = "";
            string StatusOnly = "";

            int? total = 0;
            bool statusOnly = false;
            string customRoleDeparmentList = null;

            ComboBox cmbBranch = (ComboBox)this.Parent.FindControl("cmbBranch");
            if (cmbBranch.SelectedItem.Value != null && cmbBranch.SelectedItem.Index != -1 && !string.IsNullOrEmpty(cmbBranch.SelectedItem.Value.Trim()))
                BranchID = Convert.ToInt32(cmbBranch.SelectedItem.Value);

            ComboBox cmbDepartment = (ComboBox)this.Parent.FindControl("cmbDepartment");
            if (cmbDepartment.SelectedItem.Value != null && cmbDepartment.SelectedItem.Index != -1 && !string.IsNullOrEmpty(cmbDepartment.SelectedItem.Value.Trim()))
                DepartmentID = Convert.ToInt32(cmbDepartment.SelectedItem.Value);

            ComboBox cmbLevel = (ComboBox)this.Parent.FindControl("cmbLevel");
            if (cmbLevel.SelectedItem.Value != null && cmbLevel.SelectedItem.Index != -1 && !string.IsNullOrEmpty(cmbLevel.SelectedItem.Value.Trim()))
                LevelID = Convert.ToInt32(cmbLevel.SelectedItem.Value);

            ComboBox cmbDesignation = (ComboBox)this.Parent.FindControl("cmbDesignation");
            if (cmbDesignation.SelectedItem.Value != null && cmbDesignation.SelectedItem.Index != -1 && !string.IsNullOrEmpty(cmbDesignation.SelectedItem.Value.Trim()))
                DesignationID = Convert.ToInt32(cmbDesignation.SelectedItem.Value);

            ComboBox cmbEmpStatus = (ComboBox)this.Parent.FindControl("cmbEmpStatus");
            if (cmbEmpStatus.SelectedItem.Value != null && cmbEmpStatus.SelectedItem.Index != -1 && !string.IsNullOrEmpty(cmbEmpStatus.SelectedItem.Value.Trim()))
            {
                StatusID = Convert.ToInt32(cmbEmpStatus.SelectedItem.Value);
                statusOnly = cmbEmpStatus.SelectedItem.Text.ToLower().Contains("only");
            }

            TextField txtINO = (TextField)this.Parent.FindControl("txtINO");
            if (!string.IsNullOrEmpty(txtINO.Text))
                INO = Convert.ToString(txtINO.Text).Trim();

            ComboBox cmbEmployee = (ComboBox)this.Parent.FindControl("cmbEmployee");
            if (!string.IsNullOrEmpty(cmbEmployee.SelectedItem.Text))
                EmpID = Convert.ToString(cmbEmployee.SelectedItem.Text).Trim();


            if (SessionManager.IsCustomRole)
                customRoleDeparmentList = SessionManager.CustomRoleDeparmentIDList;


            EmployeeManager employeeManager = new EmployeeManager();

            List<GetAllEmployeeSearchNewResult> resultSet = employeeManager.GetAllEmployeesSearchNew(0, 99000,
              ref total, hiddenSort.Text, SessionManager.CurrentCompanyId,
                EmpID, INO, ShowRetired, BranchID, DepartmentID, DesignationID, LevelID, customRoleDeparmentList, StatusID, statusOnly);

            foreach (GetAllEmployeeSearchNewResult item in resultSet)
                item.JoinDateEngText = WebHelper.FormatDateForExcel(item.JoinDateEng);


            List<string> _hideColumnList = new List<string>();
            string[] _hideColumn = { "JoinDateEng","Title", "IdCardNo", "UrlPhoto", "BranchId", "StatusText", "Department", "IsRetiredOrResigned", "JoinDateNepali", "TotalRows", "EmployeeId", "StatusID" };
            _hideColumnList.AddRange(_hideColumn.ToList());
            //if (IsEnglish)
            //    _hideColumnList.Add("JoinDateNepali");

            Bll.ExcelHelper.ExportToExcel("EmployeeSearchResult", resultSet,
            _hideColumnList,
            new List<String>() { },
            new Dictionary<string, string>() { {"RowNumber", "SN" },{"JoinDateEngText","Join Date(A.D)"}, { "StatusText", "Status" }, { "Level", "Level/Position" }, { "Name", "Employee Name" }, { "EmployeeId", "EIN" }, { "JoinDateEng", "Join Date(A.D)" }, 
            { "JoinDateNepali", "Join Date(B.S)" }, { "ServicePeriodTillNow", "Service Period" } },
            new List<string>() { }
            , new List<string> { }
            , new List<string> {"JoinDateEngText" }
            , new Dictionary<string, string>() { {"Employee List",""}}
            , new List<string> { "RowNumber", "Branch", "Name", "Level", "Designation", "JoinDateEngText", "ServicePeriodTillNow" });



           // ExcelGenerator.WriteExcel(ExcelGenerator.GetExcelForEmployeeList(list));




        }

        private void LoadLevels()
        {
            EmployeeManager empMgr = new EmployeeManager();
            int totalRecords = 0;

            string customRoleDeparmentList = null;
            if (SessionManager.IsCustomRole)
                customRoleDeparmentList = SessionManager.CustomRoleDeparmentIDList;


            PagingToolbar1.DoRefresh();
        }

        protected void btnDelete_Click(object sender, DirectEventArgs e)
        {
            int empId = int.Parse(hiddenValue.Text.Trim());
            EmployeeManager empMgr = new EmployeeManager();

            if (empMgr.Delete(empId))
            {
                X.Js.AddScript("searchList();");
                NewMessage.ShowNormalMessage(Resources.Messages.EmployeeDeletedMsg);

            }
            else
            {
                NewMessage.ShowWarningMessage("Employee could not be deleted.");

            }
        }

        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            int start = 0;
            int pagesize = 50;
            int BranchID = -1;
            int DepartmentID = -1;
            int LevelID = -1;
            int DesignationID = -1;
            int StatusID = -1;
            int CurrentPage = 1;

            bool? ShowRetired = null; //All
            string EmpID = "";
            string INO = "";
            string StatusOnly = "";

            int? total = 0;
            bool statusOnly = false;
            string customRoleDeparmentList = null;

            ComboBox cmbBranch = (ComboBox)this.Parent.FindControl("cmbBranch");
            if (cmbBranch.SelectedItem.Value != null && cmbBranch.SelectedItem.Index != -1 && !string.IsNullOrEmpty(cmbBranch.SelectedItem.Value.Trim()))
                BranchID = Convert.ToInt32(cmbBranch.SelectedItem.Value);

            ComboBox cmbDepartment = (ComboBox)this.Parent.FindControl("cmbDepartment");
            if (cmbDepartment.SelectedItem.Value != null && cmbDepartment.SelectedItem.Index != -1 && !string.IsNullOrEmpty(cmbDepartment.SelectedItem.Value.Trim()))
                DepartmentID = Convert.ToInt32(cmbDepartment.SelectedItem.Value);

            ComboBox cmbLevel = (ComboBox)this.Parent.FindControl("cmbLevel");
            if (cmbLevel.SelectedItem.Value != null && cmbLevel.SelectedItem.Index != -1 && !string.IsNullOrEmpty(cmbLevel.SelectedItem.Value.Trim()))
                LevelID = Convert.ToInt32(cmbLevel.SelectedItem.Value);

            ComboBox cmbDesignation = (ComboBox)this.Parent.FindControl("cmbDesignation");
            if (cmbDesignation.SelectedItem.Value != null && cmbDesignation.SelectedItem.Index != -1 && !string.IsNullOrEmpty(cmbDesignation.SelectedItem.Value.Trim()))
                DesignationID = Convert.ToInt32(cmbDesignation.SelectedItem.Value);

            ComboBox cmbEmpStatus = (ComboBox)this.Parent.FindControl("cmbEmpStatus");
            if (cmbEmpStatus.SelectedItem.Value != null && cmbEmpStatus.SelectedItem.Index != -1 && !string.IsNullOrEmpty(cmbEmpStatus.SelectedItem.Value.Trim()))
            {
                StatusID = Convert.ToInt32(cmbEmpStatus.SelectedItem.Value);
                statusOnly = cmbEmpStatus.SelectedItem.Text.ToLower().Contains("only");
            }

            TextField txtINO = (TextField)this.Parent.FindControl("txtINO");
            if (!string.IsNullOrEmpty(txtINO.Text))
                INO = Convert.ToString(txtINO.Text).Trim();

            ComboBox cmbEmployee = (ComboBox)this.Parent.FindControl("cmbEmployee");
            if (!string.IsNullOrEmpty(cmbEmployee.SelectedItem.Text))
                EmpID = Convert.ToString(cmbEmployee.SelectedItem.Text).Trim();      


            if (SessionManager.IsCustomRole)
                customRoleDeparmentList = SessionManager.CustomRoleDeparmentIDList;

            hiddenSort.Text = (e.Sort[0].Property + " " + e.Sort[0].Direction).ToLower();

            EmployeeManager employeeManager = new EmployeeManager();

            List<GetAllEmployeeSearchNewResult> resultSet = employeeManager.GetAllEmployeesSearchNew(e.Start, int.Parse(cmbPageSize.SelectedItem.Value),
                 ref total, (e.Sort[0].Property + " " + e.Sort[0].Direction).ToLower(), SessionManager.CurrentCompanyId,
                   EmpID, INO, false, BranchID, DepartmentID, DesignationID, LevelID, customRoleDeparmentList, StatusID, statusOnly);


            e.Total = total.Value;

            if (resultSet.Any())
                storeEmpList.DataSource = resultSet;
            storeEmpList.DataBind();
        }



    }
}