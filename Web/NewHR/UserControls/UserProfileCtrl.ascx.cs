﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Entity;
using BLL;
using BLL.BO;

namespace Web.NewHR.UserControls
{
    public partial class UserProfileCtrl : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                lblHandicapped1.Text = CommonManager.GetHandicappedName;
                BindGrid();
            }
        }

        private void BindGrid()
        {
            levelGradeChangeHistory.Visible = CommonManager.CompanySetting.HasLevelGradeSalary;

            int employeeId = 0;
            if (Request.QueryString["ID"] != null)
                employeeId = int.Parse(Request.QueryString["ID"].ToString());
            else
                employeeId = SessionManager.CurrentLoggedInEmployeeId;

            LoadChangeHistoryStatus(employeeId);
            LoadBranchTransfer(employeeId);
            LoadLevelGradeHistory(employeeId); 
            LoadEducationGrid(employeeId);
            LoadTrainingGrid(employeeId);
            LoadSeminarGrid(employeeId);
            LoadPreviousEmploymentGrid(employeeId);
            LoadSkillSetsGrid(employeeId);
            LoadLanguageSetsGrid(employeeId);
            LoadPublicationGrid(employeeId);
            LoadHandicappedDetails(employeeId);
            LoadFamily(employeeId);
            LoadHealthGrid(employeeId);
            LoadExtraActivityGrid(employeeId);
            LoadNomineeGrid(employeeId);
        }

        protected void LoadBranchTransfer(int EmployeeId)
        {
            int total = 0;
            List<BranchDepartmentHistory> list = BranchManager.GetBranchTransferList(false, 0, int.MaxValue, ref total, "", EmployeeId);
            GridLevels.DataSource = list;
            GridLevels.DataBind();

            if (list.Count <= 0)
                divBranchTransfer.Visible = false;
        }

        protected void LoadLevelGradeHistory(int employeeId)
        {

            int? levelId = null; 
            double grade = 0;
            NewHRManager.SetEmployeeJoiningLevelGrade(ref levelId, ref grade, employeeId);

            if (levelId != null)
            {

                gridLevelGradeHistory.DataSource = NewHRManager.GetLevelGradeHistory(employeeId);
                gridLevelGradeHistory.DataBind();
            }
            else
            {
                gridLevelGradeHistory.EmptyDataText = "No record found.";
                gridLevelGradeHistory.DataBind();
            }

            if (NewHRManager.GetLevelGradeHistory(employeeId).Count <= 0)
                levelGradeChangeHistory.Visible = false;
        }

        protected void LoadChangeHistoryStatus(int EmployeeID)
        {
            gridChangeStatusHist.DataSource = EmployeeManager.GetCurrentStatusHist(EmployeeID);
            gridChangeStatusHist.DataBind();

            if (EmployeeManager.GetCurrentStatusHist(EmployeeID).Count <= 0)
                divStatusChange.Visible = false;
        }

        
        protected void LoadHandicappedDetails(int EmployeeID)
        {
            EEmployee _EEmployee = NewHRManager.Instance.GetHandicappedDetailsByEmployee(EmployeeID);
            if (_EEmployee != null)
            {
                if (_EEmployee.IsHandicapped != null)
                {
                    if (bool.Parse(_EEmployee.IsHandicapped.ToString()))
                        lblRefcmbHandicapped.Text = "Yes";
                    else
                        lblRefcmbHandicapped.Text = "No";
                }
                else
                    lblRefcmbHandicapped.Text = "No";
                
            }

        }

        protected void LoadSkillSetsGrid(int EmployeeID)
        {

            List<NewHREmployeeSkillSetBO> _EmployeeSkillSetBO = NewHRManager.GetEmployeesSkillSet(EmployeeID);

            this.GridSkillSets.DataSource = _EmployeeSkillSetBO;
            this.GridSkillSets.DataBind();

            if (_EmployeeSkillSetBO.Count <= 0)
                divSkillSet.Visible = false;
        }



        protected void LoadTrainingGrid(int EmployeeID)
        {
            List<HTraining> _HTraining = NewHRManager.GetTrainingByEmployeeID(EmployeeID);

            if (SessionManager.CurrentLoggedInEmployeeId == 0)
                _HTraining = _HTraining.Where(x => x.Status == (int)HRStatusEnum.Approved).ToList();

            this.GridTraining.DataSource = _HTraining;
            this.GridTraining.DataBind();

            if (_HTraining.Count <= 0)
                divTraining.Visible = false;

        }


        protected void LoadEducationGrid(int EmployeeID)
        {
            List<HEducation> _HEducation = NewHRManager.GetEducationByEmployeeID(EmployeeID);

            if (SessionManager.CurrentLoggedInEmployeeId == 0)
                _HEducation = _HEducation.Where(x => x.Status == (int)HRStatusEnum.Approved).ToList();

           GridEducation.DataSource = _HEducation;
           GridEducation.DataBind();

           if (_HEducation.Count <= 0)
               divEducation.Visible = false;

            //if (ShowEmpty == false)
            //{
            //    if (_isDisplayMode && _HEducation.Count <= 0)
            //        GridEducation.Hide();
            //}
            //else
            //    GridEducation.Show();
        }

        protected void LoadSeminarGrid(int EmployeeID)
        {
            List<HSeminar> _HSeminar = NewHRManager.GetSeminarByEmployeeID(EmployeeID);

            if (SessionManager.CurrentLoggedInEmployeeId == 0)
                _HSeminar = _HSeminar.Where(x => x.Status == (int)HRStatusEnum.Approved).ToList();

            this.GridSeminar.DataSource = _HSeminar;
            this.GridSeminar.DataBind();

            if (_HSeminar.Count <= 0)
                divSeminar.Visible = false;

        }

        protected void LoadPreviousEmploymentGrid(int EmployeeID)
        {
            List<HPreviousEmployment> _HPreviousEmployment = NewHRManager.GetPreviousEmploymentByEmployeeID(EmployeeID);

            if (SessionManager.CurrentLoggedInEmployeeId == 0)
                _HPreviousEmployment = _HPreviousEmployment.Where(x => x.Status == (int)HRStatusEnum.Approved).ToList();

            this.GridPreviousEmployment.DataSource = _HPreviousEmployment;
            this.GridPreviousEmployment.DataBind();

            if (_HPreviousEmployment.Count <= 0)
                divPrevEmploy.Visible = false;

        }

        protected void LoadLanguageSetsGrid(int EmployeeID)
        {

            List<NewHREmployeeLanguageSetBO> _EmployeeLanguageSetBO = NewHRManager.GetEmployeesLanguageSet(EmployeeID);
            //List<LanguageSetEmployee> _LanguageSetEmployee = NewHRManager.GetEmployeesLanguageSet(EmployeeID);

            this.GridLanguageSets.DataSource = _EmployeeLanguageSetBO;
            this.GridLanguageSets.DataBind();

            if (_EmployeeLanguageSetBO.Count <= 0)
                divLanguages.Visible = false;
        }

        protected void LoadPublicationGrid(int EmployeeID)
        {
            List<HPublication> _HPublication = NewHRManager.GetPublicationByEmployeeID(EmployeeID);

            if (SessionManager.CurrentLoggedInEmployeeId == 0)
                _HPublication = _HPublication.Where(x => x.Status == (int)HRStatusEnum.Approved).ToList();

            this.GridPublication.DataSource = _HPublication;
            this.GridPublication.DataBind();

            if (_HPublication.Count <= 0)
                divPublication.Visible = false;
        }


        private void LoadFamily(int EmployeeID)
        {
            List<DAL.HFamily> list = NewPayrollManager.GetAllFamilyList(EmployeeID);

            if (SessionManager.CurrentLoggedInEmployeeId == 0)
                list = list.Where(x => x.Status == (int)HRStatusEnum.Approved).ToList();
            GridFamily.DataSource = list;
            GridFamily.DataBind();

            if (list.Count <= 0)
                divFamily.Visible = false;
        }


        protected void LoadHealthGrid(int EmployeeID)
        {
            List<HHealth> _HHealth = NewHRManager.GetHealthByEmployeeID(EmployeeID);
            this.GridHealth.DataSource = _HHealth;
            this.GridHealth.DataBind();

            if (_HHealth.Count <= 0)
                divHealthStatus.Visible = false;
        }

        private void LoadExtraActivityGrid(int EmployeeId)
        {
            List<HExtraActivity> list = NewHRManager.GetExtraActivityByEmployeeId(EmployeeId);
            if (SessionManager.CurrentLoggedInEmployeeId == 0)
                list = list.Where(x => x.Status == (int)HRStatusEnum.Approved).ToList();
            GridExtraActivity.DataSource = list;
            GridExtraActivity.DataBind();

            if (list.Count <= 0)
                divExtraActivity.Visible = false;
        }

        private void LoadNomineeGrid(int EmployeeId)
        {
            List<HrNominee> list = NewHRManager.GetHrNomineeByEmployeeId(EmployeeId);
            if (SessionManager.CurrentLoggedInEmployeeId == 0)
                list = list.Where(x => x.Status == (int)HRStatusEnum.Approved).ToList();
            GridNominee.DataSource = list;
            GridNominee.DataBind();

            if (list.Count <= 0)
                divNominees.Visible = false;
        }
    }
}