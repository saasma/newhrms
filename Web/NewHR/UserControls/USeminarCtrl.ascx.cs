﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;

namespace Web.NewHR.UserControls
{
    public partial class USeminarCtrl : BaseUserControl
    {
        private bool _isDisplayMode = false;
        public bool IsDisplayMode
        {
            set
            {
                if (value == true)
                {
                    _isDisplayMode = value;
                    btnAddNewLine.Visible = false;
                    AESeminarWindow.Visible = false;
                    CommandColumn1.Visible = false;
                    CommandColumn2.Visible = false;
                    GridSeminar.Width = 920;
                }
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
                HideButtonsAndGridColumns();
            }
        }

        protected void Initialise()
        {
            int EmployeeID = GetEmployeeID();
            this.LoadSeminarGrid(EmployeeID);
        }
       
        protected void LoadSeminarGrid(int EmployeeID)
        {
            List<HSeminar> _HSeminar = NewHRManager.GetSeminarByEmployeeID(EmployeeID);

            if (SessionManager.CurrentLoggedInEmployeeId == 0)
                _HSeminar = _HSeminar.Where(x => x.Status == (int)HRStatusEnum.Approved).ToList();

            if (Request.QueryString["ID"] != null)
            {
                if (int.Parse(Request.QueryString["ID"].ToString()) != SessionManager.CurrentLoggedInEmployeeId)
                    _HSeminar = _HSeminar.Where(x => x.Status == (int)HRStatusEnum.Approved).ToList();
            }

            this.StoreSeminar.DataSource = _HSeminar;
            this.StoreSeminar.DataBind();

            if (_isDisplayMode && _HSeminar.Count <= 0)
                GridSeminar.Hide();
            else
                GridSeminar.Show();
        }


        protected int GetEmployeeID()
        {
            //Guid? AssetID = null;
            int EmpID = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                EmpID = int.Parse(Request.QueryString["ID"]);
            }
            else
            {
                EmpID = SessionManager.CurrentLoggedInEmployeeId;
            }
            return EmpID;
        }

        protected void btnAddNewLine_Click(object sender, DirectEventArgs e)
        {
            ucSeminar.ClearFields();
            Ext.Net.Button btnSave = (Ext.Net.Button)ucSeminar.FindControl("btnSave");
            btnSave.Disabled = false;
            this.AESeminarWindow.Show();
        }

        private object[] SeminarFillData
        {
            get
            {
                return new object[]
            {
                new object[] { "", "", "",""},
               
            };
            }
        }

        protected void GridSeminar_Command(object sender, DirectEventArgs e)
        {
            string commandName = e.ExtraParams["command"];
            int SeminarID = int.Parse(e.ExtraParams["ID"]);
            HSeminar _HSeminar = NewHRManager.GetSeminarDetailsById(int.Parse(e.ExtraParams["ID"]));
            switch (commandName)
            {
                case "Delete":
                    this.DeleteData(SeminarID);
                    break;

                case "Edit":
                    {
                        Ext.Net.Button btnSave = (Ext.Net.Button)ucSeminar.FindControl("btnSave");
                        btnSave.Disabled = false;

                        this.editSeminar(SeminarID);
                        break;
                    }
            }

        }

        protected void DeleteData(int ID)
        {

            bool result = NewHRManager.DeleteSeminarByID(ID);
            if (result)
            {
                NewMessage.ShowNormalMessage("Record deleted successfully.");
                this.LoadSeminarGrid(this.GetEmployeeID());
            }
        }


        public void editSeminar(int SeminarID)
        {
            ucSeminar.editSeminar(SeminarID);
            this.AESeminarWindow.Show();
        }

        protected void btnLoadGrid_Click(object sender, DirectEventArgs e)
        {
            int employeeId = 0;
            if (Request.QueryString["ID"] != null)
                employeeId = int.Parse(Request.QueryString["ID"].ToString());
            else
                employeeId = SessionManager.CurrentLoggedInEmployeeId;

            LoadSeminarGrid(employeeId);
        }

        private void HideButtonsAndGridColumns()
        {
            if (Request.QueryString["ID"] != null && SessionManager.CurrentLoggedInEmployeeId != 0)
            {
                if (SessionManager.CurrentLoggedInEmployeeId != int.Parse(Request.QueryString["ID"].ToString()))
                {
                    IsDisplayMode = true;
                }
            }
        }
    }
}