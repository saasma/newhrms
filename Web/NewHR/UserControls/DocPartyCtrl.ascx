﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DocPartyCtrl.ascx.cs" Inherits="Web.NewHR.UserControls.DocPartyCtrl" %>

<ext:Hidden ID="hdnPartyID" runat="server" />
<ext:Hidden ID="hdnIsDocPartyPage" runat="server" />

<ext:Window ID="WDocParty" runat="server" Title="Party" Icon="Application" ButtonAlign="Left"
            Width="400" Height="200" BodyPadding="5" Hidden="true" Modal="true">
            <Content>
                <table class="fieldTable">
                    <tr>
                        <td>
                            <ext:TextField ID="txtName" LabelSeparator="" runat="server" FieldLabel="Name *"
                                LabelWidth="60" Width="300" LabelAlign="Left">
                            </ext:TextField>
                            <asp:RequiredFieldValidator Display="None" ID="rfvName" runat="server" ValidationGroup="SaveUpdateParty"
                                ControlToValidate="txtName" ErrorMessage="Name is required." />
                        </td>
                    </tr>
                </table>
            </Content>
            <Buttons>
                <ext:Button runat="server" ID="btnPartySave" Text="Save" MarginSpec="0 0 0 15">
                    <DirectEvents>
                        <Click OnEvent="btnPartySave_Click">
                            <EventMask ShowMask="true" />
                        </Click>
                    </DirectEvents>
                    <Listeners>
                        <Click Handler="valGroup = 'SaveUpdateParty'; if(CheckValidation()) return ''; else return false;">
                        </Click>
                    </Listeners>
                </ext:Button>

                <ext:Button runat="server" ID="LinkButton1" Text="<i></i>Cancel" MarginSpec="0 0 0 10">
                    <Listeners>
                        <Click Handler="#{WDocParty}.hide();">
                        </Click>
                    </Listeners>
                </ext:Button>
            </Buttons>
        </ext:Window>