﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ChangeEmpShiftCtrl.ascx.cs"
    Inherits="Web.NewHR.UserControls.ChangeEmpShiftCtrl" %>
<script type="text/javascript">

        var storeShift = null;
        var gridEmpShift = null;
        var storeEmpShift = null;

        var afterEdit = function (e1, e2) {
            e2.record.commit();
        };

        var setShiftType = function (value, rowIndex) {
            storeEmpShift = <%= storeEmpShift.ClientID %>;
            storeShift = <%= storeShift.ClientID %>;

            var currentRow = storeEmpShift.data.items[rowIndex];
            var row = storeShift.getById(value.toString().toLowerCase());
            if (row != null && typeof (row) != 'undefined')
                currentRow.data.WorkShiftId = row.data.WorkShiftId.toString();   

            currentRow.commit();
        };
        
        var shiftTypeFixedOrChangableRenderer = function(value)
        {
            if(value==true)
                return 'Changable';
            return 'Fixed';
        }
        var shiftTypeRenderer = function(value, metaData, record, rowIndex, colIndex, store){
            var r = <%= storeShift.ClientID %>.getById(value.toString().toLowerCase());
            if(Ext.isEmpty(r)){
                    return "";
                };
            return r.data.Name;
        };

        function refreshWindow() {

            __doPostBack('Refresh', 0);

        }

        function searchListEL() {

             <%=gridEmpShift.ClientID %>.getStore().pageSize = <%=cmbPageSize.ClientID %>.getValue();
            <%=PagingToolbar1.ClientID %>.doRefresh();
        }

</script>
<ext:Hidden ID="hdnEmployeeId" runat="server" />
<div class="contentpanel1">
    <div class="separator bottom">
    </div>
    <div class="innerLR">
        <ext:Store ID="storeEmpShift" runat="server" AutoLoad="false" OnReadData="Store_ReadData"
            RemotePaging="true" RemoteSort="true">
            <Proxy>
                <ext:PageProxy />
            </Proxy>
            <Model>
                <ext:Model ID="modelEmpShift" runat="server">
                    <Fields>
                        <ext:ModelField Name="BranchName" Type="String" />
                        <ext:ModelField Name="DepartmentName" Type="String" />
                        <ext:ModelField Name="EmployeeId" Type="Int" />
                        <ext:ModelField Name="Name" Type="String" />
                        <ext:ModelField Name="WorkShiftId" Type="Int" />
                        <ext:ModelField Name="Schedule" Type="Int" />
                        <ext:ModelField Name="WorkShiftHistoryId" Type="Int" />
                        <ext:ModelField Name="DefaultWorkShiftId" Type="Int" />
                        <ext:ModelField Name="ShiftType" Type="Boolean" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
        <ext:Store ID="storeShift" runat="server">
            <Model>
                <ext:Model ID="modelShift" runat="server" IDProperty="WorkShiftId">
                    <Fields>
                        <ext:ModelField Name="WorkShiftId" Type="String" />
                        <ext:ModelField Name="ShiftStart" />
                        <ext:ModelField Name="ShiftEnd" />
                        <ext:ModelField Name="Type" Type="Int" />
                        <ext:ModelField Name="Name" Type="String" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
        <ext:Store ID="storeShiftType" runat="server">
            <Model>
                <ext:Model ID="model1" runat="server">
                    <Fields>
                        <ext:ModelField Name="Text" Type="String" />
                        <ext:ModelField Name="Value" Type="Boolean" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
        <table class="fieldTable">
            <tr>
                <td style="padding-left:0px;">
                    <ext:ComboBox FieldLabel="Schedule Type" ID="cmbShiftType" Width="180px" runat="server"
                        LabelAlign="Top" LabelSeparator="" ForceSelection="true" QueryMode="Local">
                        <Items>
                            <ext:ListItem Text="All" Value="-1">
                            </ext:ListItem>
                            <ext:ListItem Text="Fixed" Value="false">
                            </ext:ListItem>
                            <ext:ListItem Text="Changable" Value="true">
                            </ext:ListItem>
                        </Items>
                        <SelectedItems>
                            <ext:ListItem Index="0" />
                        </SelectedItems>
                    </ext:ComboBox>
                </td>
                <td>
                    <ext:ComboBox FieldLabel="Shift" ID="cmbShiftList" Width="180px" runat="server" ValueField="WorkShiftId"
                        DisplayField="Name" StoreID="storeShift" LabelAlign="Top" LabelSeparator="" ForceSelection="true"
                        QueryMode="Local">
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                        </Triggers>
                        <Listeners>
                            <Select Handler="this.getTrigger(0).show();" />
                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                            <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                        </Listeners>
                    </ext:ComboBox>
                </td>
                <td>
                    <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                        <Proxy>
                            <ext:AjaxProxy Url="../../Handler/EmpSearchID.ashx">
                                <ActionMethods Read="GET" />
                                <Reader>
                                    <ext:JsonReader Root="plants" TotalProperty="total" />
                                </Reader>
                            </ext:AjaxProxy>
                        </Proxy>
                        <Model>
                            <ext:Model ID="Model2" IDProperty="Name" Name="ItemLineModel" runat="server">
                                <Fields>
                                    <ext:ModelField Name="Name" Type="String" />
                                    <ext:ModelField Name="EmployeeId" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                    <ext:ComboBox LabelSeparator="" ID="cmbSearch" FieldLabel="Search Employee" LabelWidth="70"
                        LabelAlign="Top" runat="server" DisplayField="Name" ValueField="EmployeeId" StoreID="storeSearch"
                        TypeAhead="false" Width="180" PageSize="9999" HideBaseTrigger="true" MinChars="1"
                        TriggerAction="All" ForceSelection="true" Hidden="true">
                        <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                            <ItemTpl ID="ItemTpl1" runat="server">
                                <Html>
                                    <tpl>
                                            <div class="search-item">
                                                            <span>{Name}</span>  
                                             </div>
					                </tpl>
                                </Html>
                            </ItemTpl>
                        </ListConfig>
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                        </Triggers>
                        <Listeners>
                            <Select Handler="this.getTrigger(0).show();" />
                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                            <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                        </Listeners>
                    </ext:ComboBox>
                </td>
                <td>
                    <ext:Store ID="StoreBranch" runat="server">
                        <Model>
                            <ext:Model ID="Model5" runat="server" IDProperty="BranchId">
                                <Fields>
                                    <ext:ModelField Name="BranchId" Type="String" />
                                    <ext:ModelField Name="Name" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                    <ext:ComboBox FieldLabel="Branch" ID="cmbBranch" Width="180px" runat="server" ValueField="BranchId"
                        DisplayField="Name" StoreID="StoreBranch" LabelAlign="Top" LabelSeparator=""
                        ForceSelection="true" QueryMode="Local" Hidden="true">
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                        </Triggers>
                        <Listeners>
                            <Select Handler="this.getTrigger(0).show();" />
                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                            <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                        </Listeners>
                    </ext:ComboBox>
                </td>
                <td>
                    <ext:Store ID="StoreDepartment" runat="server">
                        <Model>
                            <ext:Model ID="Model3" runat="server" IDProperty="DepartmentId">
                                <Fields>
                                    <ext:ModelField Name="DepartmentId" Type="String" />
                                    <ext:ModelField Name="Name" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                    <ext:ComboBox FieldLabel="Department" ID="cmbDepartment" Width="180px" runat="server"
                        ValueField="DepartmentId" DisplayField="Name" StoreID="StoreDepartment" LabelAlign="Top"
                        LabelSeparator="" ForceSelection="true" QueryMode="Local" Hidden="true">
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                        </Triggers>
                        <Listeners>
                            <Select Handler="this.getTrigger(0).show();" />
                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                            <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                        </Listeners>
                    </ext:ComboBox>
                </td>
                <td style="padding-top: 20px">
                    <ext:Button runat="server" Cls="btn btn-default btn-sect btn-sm" ID="btnLoad" Text="<i></i>Show">
                        <%--<DirectEvents>
                                <Click OnEvent="btnLoad_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>--%>
                        <Listeners>
                            <Click Fn="searchListEL" />
                        </Listeners>
                    </ext:Button>
                </td>
                <td style="padding-top:20px">

                <ext:Button runat="server"  OnClientClick="changeEmpShift();return false;" Cls="btn btn-primary btn-sect"
                            ID="btnChangeShift"  Text="<i></i>Change Shift Import">
                        </ext:Button>
                        
                <%--    <asp:LinkButton ID="btnChangeShift" runat="server" Text="Change Shift Import" OnClientClick="changeEmpShift();return false;" 
                        CssClass=" excel marginRight tiptip" Style="float: left; display: none;"></asp:LinkButton>--%>
                </td>
            </tr>
        </table>
        <ext:GridPanel ID="gridEmpShift" StyleSpec="margin-top:10px;" runat="server" Width="960"
            OnReadData="Store_ReadData" StoreID="storeEmpShift" MinHeight="0">
            <ColumnModel>
                <Columns>
                    <ext:Column ID="colBranch" runat="server" Flex="1" Text="Branch" DataIndex="BranchName">
                    </ext:Column>
                    <ext:Column ID="colDepartment" runat="server" Flex="1" Text="Department" DataIndex="DepartmentName">
                    </ext:Column>
                    <ext:Column ID="colEmployee" runat="server" Flex="1" Text="Employee" DataIndex="Name">
                    </ext:Column>
                    <ext:Column ID="colSchedule" runat="server" Hidden="true" Text="Schedule" Flex="1"
                        DataIndex="Schedule">
                        <%--<Renderer Handler="if(value=='1')
                                                return 'Fixed';
                                            else
                                                return 'Changing';" />--%>
                        <Editor>
                            <ext:ComboBox ID="cmbSchedule" runat="server" ForceSelection="true" TypeAhead="true"
                                SelectOnTab="true">
                                <Items>
                                    <ext:ListItem Text="Fixed" Value="1" />
                                    <ext:ListItem Text="Changing" Value="2" />
                                </Items>
                                <%-- <Listeners>
                                    <Focus Handler="onScheduleFocus(#{gridEmpShift}.getSelectionModel().getCurrentPosition().row);" />
                                </Listeners>--%>
                            </ext:ComboBox>
                        </Editor>
                    </ext:Column>
                    <ext:Column ID="colShift" runat="server" Text="Shift" Flex="1" DataIndex="WorkShiftId">
                        <Renderer Fn="shiftTypeRenderer" />
                        <%--<Editor>
                            <ext:ComboBox ID="cmbShift" runat="server" ValueField="WorkShiftId" DisplayField="Name"
                                StoreID="storeShift" QueryMode="Local" ForceSelection="true">
                                <Listeners>
                                    <Select Handler="setShiftType(this.getValue(), #{gridEmpShift}.getSelectionModel().getCurrentPosition().row);" />
                                    <BeforeSelect Handler="return record.data.isEmpty !=true;">
                                    </BeforeSelect>
                                    <Focus Handler="onShiftFocus(#{gridEmpShift}.getSelectionModel().getCurrentPosition().row);" />
                                </Listeners>
                            </ext:ComboBox>
                        </Editor>--%>
                    </ext:Column>
                    <ext:Column ID="Column1" runat="server" Text="Type" Flex="1" DataIndex="ShiftType">
                        <Renderer Fn="shiftTypeFixedOrChangableRenderer" />
                        <Editor>
                            <ext:ComboBox DisplayField="Text" ValueField="Value" ID="cmbShiftType1" StoreID="storeShiftType"
                                runat="server" QueryMode="Local" ForceSelection="true">
                                <Listeners>
                                    <%--  <Select Handler="setShiftType(this.getValue(), #{gridEmpShift}.getSelectionModel().getCurrentPosition().row);" />
                                    <BeforeSelect Handler="return record.data.isEmpty !=true;">
                                    </BeforeSelect>
                                    <Focus Handler="onShiftFocus(#{gridEmpShift}.getSelectionModel().getCurrentPosition().row);" />--%>
                                </Listeners>
                            </ext:ComboBox>
                        </Editor>
                    </ext:Column>
                </Columns>
            </ColumnModel>
            <SelectionModel>
                <ext:CheckboxSelectionModel ID="CheckboxSelectionModel1" runat="server" Mode="Multi" />
            </SelectionModel>
            <Plugins>
                <ext:CellEditing ID="cellEditing1" runat="server" ClicksToEdit="1">
                    <%--<Listeners>
                        <Edit Fn="afterEdit" />
                    </Listeners>--%>
                </ext:CellEditing>
            </Plugins>
            <BottomBar>
                <ext:PagingToolbar ID="PagingToolbar1" runat="server" StoreID="storeEmpList" DisplayInfo="true">
                    <Items>
                        <ext:Label ID="lblPageSize" runat="server" Text="Page size:" />
                        <ext:ToolbarSpacer ID="ToolbarSpacer_listItems" runat="server" Width="10" />
                        <ext:ComboBox ID="cmbPageSize" runat="server" Width="80" SelectOnFocus="true" Selectable="true"
                            ValueField="Value" DisplayField="Text" ForceSelection="true" AllowBlank="false">
                            <Listeners>
                                <Select Handler="searchListEL()" />
                                <%--#{cmbPageSize}.pageSize = this.getValue();#{PagingToolbar1}.moveFirst();--%>
                            </Listeners>
                            <Items>
                                <ext:ListItem Value="50" Text="50" />
                                <ext:ListItem Value="100" Text="100" />
                                <ext:ListItem Value="200" Text="200" />
                            </Items>
                            <SelectedItems>
                                <ext:ListItem Index="0">
                                </ext:ListItem>
                            </SelectedItems>
                        </ext:ComboBox>
                    </Items>
                </ext:PagingToolbar>
            </BottomBar>
        </ext:GridPanel>
        <ext:Button ID="btnSave" Cls="btn btn-primary btn-sect" runat="server" StyleSpec='margin-top:10px;'
            Text="Save Changes">
            <DirectEvents>
                <Click OnEvent="btnSave_Click">
                    <ExtraParams>
                        <ext:Parameter Name="EmpShiftExtraParam" Value="Ext.encode(#{gridEmpShift}.getRowsValues({selectedOnly : false}))"
                            Mode="Raw" />
                    </ExtraParams>
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:Button>
    </div>
</div>
