﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;

using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;

namespace Web.NewHR.UserControls
{
    public partial class EmploymentHistoryCtl : BaseUserControl
    {
        private bool _isDisplayMode = false;
        public bool IsDisplayMode
        {
            set
            {
                if (value == true)
                {
                    _isDisplayMode = value;
                    LinkButton1.Visible = false;
                    AEPreviousEmploymentWindow.Visible = false;
                    //td1.Visible = false;
                    //td2.Visible = false;
                    CommandColumn1.Visible = false;
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();

            }
        }
        protected void Initialise()
        {
            cmbExpCategory.Store[0].DataSource = NewHRManager.GetAllExperienceCategory();
            cmbExpCategory.Store[0].DataBind();

            int EmployeeID = GetEmployeeID();
            this.LoadPreviousEmploymentGrid(EmployeeID);

        }
        protected void btnSave_Click(object sender, DirectEventArgs e)
        {

            this.SaveFormData();

        }
        private void SaveFormData()
        {

            Status myStatus = new Status();
            HPreviousEmployment _HPreviousEmployment = new HPreviousEmployment();
            _HPreviousEmployment.EmployeeId = this.GetEmployeeID();
            bool isSave = bool.Parse(this.hdnPreviousEmploymentID.Text == "" ? "true" : "false");
            if (!isSave)
                _HPreviousEmployment.PrevEmploymentId = int.Parse(this.hdnPreviousEmploymentID.Text);

            if (cmbExpCategory.SelectedItem != null && cmbExpCategory.SelectedItem.Value != null)
                _HPreviousEmployment.CategoryID = int.Parse(cmbExpCategory.SelectedItem.Value);

            _HPreviousEmployment.Organization = txtOrganization.Text.Trim();
            _HPreviousEmployment.Place = txtPlace.Text.Trim();
            _HPreviousEmployment.Position = txtPosition.Text.Trim();
            _HPreviousEmployment.JobResponsibility = txtJobResponsibility.Text.Trim();
            if (!string.IsNullOrEmpty(calFrom.Text.Trim()))
            {
                _HPreviousEmployment.JoinDate = calFrom.Text.Trim();
                _HPreviousEmployment.JoinDateEng = BLL.BaseBiz.GetEngDate(_HPreviousEmployment.JoinDate, IsEnglish);
            }
            if (!string.IsNullOrEmpty(calTo.Text.Trim()))
            {
                _HPreviousEmployment.ResignDate = calTo.Text.Trim();
                _HPreviousEmployment.ResignDateEng = BLL.BaseBiz.GetEngDate(_HPreviousEmployment.ResignDate, IsEnglish);
            }
            _HPreviousEmployment.ReasonForLeaving = txtReasonforLeaving.Text.Trim();
            _HPreviousEmployment.Notes = txtNote.Text.Trim();

            myStatus = NewHRManager.Instance.InsertUpdatePreviousEmployment(_HPreviousEmployment, isSave);

            if (myStatus.IsSuccess)
            {
                if (isSave)
                    NewMessage.ShowNormalMessage("Record Save successfully");
                else
                    NewMessage.ShowNormalMessage("Record Updated successfully");

                //  this.ClearFields();
                this.AEPreviousEmploymentWindow.Close();
                this.LoadPreviousEmploymentGrid(this.GetEmployeeID());

            }
            else
                NewMessage.ShowWarningMessage(myStatus.ErrorMessage);
        }
        protected void LoadPreviousEmploymentGrid(int EmployeeID)
        {
            List<HPreviousEmployment> _HPreviousEmployment = NewHRManager.GetPreviousEmploymentByEmployeeID(EmployeeID);


            this.StorePreviousEmployment.DataSource = _HPreviousEmployment;
            this.StorePreviousEmployment.DataBind();

            if (_isDisplayMode && _HPreviousEmployment.Count <= 0)
                GridPreviousEmployment.Hide();
        }
        protected int GetEmployeeID()
        {
            int EmpID = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                EmpID = int.Parse(Request.QueryString["ID"]);
            }
            else
            {
                EmpID = SessionManager.CurrentLoggedInEmployeeId;
            }
            return EmpID;
        }
        protected void btnAddNewLine_Click(object sender, DirectEventArgs e)
        {
            this.ClearFields();
            this.AEPreviousEmploymentWindow.Show();

        }
        private object[] PreviousEmploymentFillData
        {
            get
            {
                return new object[]
            {
                new object[] { "", "", "",""},
               
            };
            }
        }
        protected void GridPreviousEmployment_Command(object sender, DirectEventArgs e)
        {
            string commandName = e.ExtraParams["command"];
            int PreviousEmploymentID = int.Parse(e.ExtraParams["ID"]);
            HPreviousEmployment _HPreviousEmployment = NewHRManager.GetPreviousEmploymentDetailsById(int.Parse(e.ExtraParams["ID"]));
          
            switch (commandName)
            {
                case "Delete":
                    this.DeletePreviousEmployment(PreviousEmploymentID);
                    break;
              
                case "Edit":
                    this.editPreviousEmployment(PreviousEmploymentID);
                    break;
            }

        }
        protected void DeletePreviousEmployment(int ID)
        {

            bool result = NewHRManager.DeletePreviousEmploymentByID(ID);
            if (result)
            {
                //refresh grid
                this.LoadPreviousEmploymentGrid(this.GetEmployeeID());
            }

        }
        protected void ClearFields()
        {
            this.hdnPreviousEmploymentID.Text = "";
            txtOrganization.Text = "";
            txtPlace.Text = "";
            txtPosition.Text = "";
            txtJobResponsibility.Text = "";
            calFrom.Text = "";
            calTo.Text = "";
            txtReasonforLeaving.Text = "";
            txtNote.Text = "";
            btnSave.Text = Resources.Messages.Save;
        }
        public void editPreviousEmployment(int PreviousEmploymentID)
        {
            HPreviousEmployment _HPreviousEmployment = NewHRManager.GetPreviousEmploymentDetailsById(PreviousEmploymentID);
            this.hdnPreviousEmploymentID.Text = PreviousEmploymentID.ToString();

            if (_HPreviousEmployment.CategoryID != null)
                cmbExpCategory.SetValue(_HPreviousEmployment.CategoryID.ToString());

            txtOrganization.Text = _HPreviousEmployment.Organization;
            txtPlace.Text = _HPreviousEmployment.Place;
            txtPosition.Text = _HPreviousEmployment.Position;
            txtJobResponsibility.Text = _HPreviousEmployment.JobResponsibility;
            calFrom.Text = _HPreviousEmployment.JoinDate;
            calTo.Text  =_HPreviousEmployment.ResignDate;
            txtReasonforLeaving.Text = _HPreviousEmployment.ReasonForLeaving;
            txtNote.Text = _HPreviousEmployment.Notes;
            btnSave.Text = Resources.Messages.Update;
            this.AEPreviousEmploymentWindow.Show();
        }

    }
}