﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;

namespace Web.NewHR.UserControls
{
    public partial class FamilyCtl : BaseUserControl
    {
        private bool _isDisplayMode = false;
        public bool IsDisplayMode
        {
            set
            {
                if (value == true)
                {
                    _isDisplayMode = value;
                    btnAddLevel.Visible = false;
                    LinkButton1.Visible = false;
                    CommandColumn1.Visible = false;
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        public void Initialise()
        {
            LoadLevels();

            cmbRelation.Store[0].DataSource = CommonManager.GetRelationList();
            cmbRelation.Store[0].DataBind();
        }

        private void LoadLevels()
        {
            int EmpID = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                EmpID = int.Parse(Request.QueryString["ID"]);
            }
            else
            {
                EmpID = SessionManager.CurrentLoggedInEmployeeId;
            }

            List<DAL.HFamily> list = NewPayrollManager.GetAllFamilyList(EmpID);
            GridLevels.Store[0].DataSource = list;
            GridLevels.Store[0].DataBind();

            if (_isDisplayMode && list.Count <= 0)
                GridLevels.Hide();
        }


        //public void txtDOB_Blur(object sender, DirectEventArgs e)
        //{
        //    if (!string.IsNullOrEmpty(txtDOB.Text.Trim()))
        //    {
        //        int years, months, days, hours, minutes, seconds, milliseconds;
        //        NewHelper.GetElapsedTime(GetEngDate(txtDOB.Text.Trim()),
        //            BLL.BaseBiz.GetCurrentDateAndTime(), out years, out months, out days, out hours, out minutes, out seconds, out milliseconds);

        //        txtAgeOnSPDate.Text = string.Format(" {0} years {1} months {2} days",
        //            years, months, days);
        //    }
        //}
        public void ClearLevelFields()
        {
            hiddenValue.Text = "";
            txtName.Text = "";
            cmbRelation.ClearValue();
            txtRemarks.Text = "";
            txtDOB.Text = "";
            chkHasDependent.Checked = false;
            txtSPDate.Text = "";
            txtAgeOnSPDate.Text = "";

        }



        protected void btnAddLevel_Click(object sender, DirectEventArgs e)
        {
            ClearLevelFields();
            WindowLevel.Center();
            WindowLevel.Show();
        }


        protected void btnDeleteLevel_Click(object sender, DirectEventArgs e)
        {
            int levelId = int.Parse(hiddenValue.Text.Trim());
            Status status = NewPayrollManager.DeleteFamilyMember(levelId);
            if (status.IsSuccess)
            {
                LoadLevels();
                NewMessage.ShowNormalMessage("Person deleted.");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }

        }



        protected void btnEditLevel_Click(object sender, DirectEventArgs e)
        {
            int levelId = int.Parse(hiddenValue.Text.Trim());
            HFamily entity = NewPayrollManager.GetFamilyMemberById(levelId);
            WindowLevel.Center();
            WindowLevel.Show();

            txtName.Text = entity.Name;
            if (entity.RelationID != null)
                cmbRelation.SetValue(entity.RelationID.ToString());
            else
                cmbRelation.ClearValue();
            txtRemarks.Text = entity.Remarks;
            txtDOB.Text = entity.DateOfBirth;
            if (entity.HasDependent != null)
                chkHasDependent.Checked = entity.HasDependent.Value;
            else
                chkHasDependent.Checked = false;
            txtSPDate.Text = entity.SpecifiedDate;
            txtAgeOnSPDate.Text = entity.AgeOnSpecifiedSPDate;


            //cmbGroup.SetValue(entity.LevelGroupId.ToString());
            //txtLevelCode.Text = entity.Code;
            //txtLevelName.Text = entity.Name;
            //txtLevelOrder.Number = entity.Order;

        }

        protected void btnLevelSaveUpdate_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("InsertUpdateLevel");
            if (Page.IsValid)
            {
                HFamily hFamily = new HFamily();
                int EmpID;
                int FamilyID;
                bool isInsert = true;
                hFamily.Name = txtName.Text;
                hFamily.Relation = cmbRelation.SelectedItem.Text;
                hFamily.RelationID = int.Parse(cmbRelation.SelectedItem.Value);
                hFamily.Remarks = txtRemarks.Text;
                hFamily.DateOfBirth = txtDOB.Text;
                if (!string.IsNullOrEmpty(hFamily.DateOfBirth))
                    hFamily.DateOfBirthEng = GetEngDate(hFamily.DateOfBirth);
                hFamily.HasDependent = false;
                if (chkHasDependent.Checked)
                    hFamily.HasDependent = true;
                hFamily.SpecifiedDate = txtSPDate.Text;
                hFamily.AgeOnSpecifiedSPDate = txtAgeOnSPDate.Text;

                if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
                {
                    EmpID = int.Parse(Request.QueryString["ID"]);
                }
                else
                {
                    EmpID = SessionManager.CurrentLoggedInEmployeeId;
                }
                hFamily.EmployeeId = EmpID;
                if (!string.IsNullOrEmpty(hiddenValue.Text))
                {
                    hFamily.FamilyId = int.Parse(hiddenValue.Text.Trim());
                    isInsert = false;
                }


                Status status = NewHRManager.InsertUpdateFamily(hFamily, isInsert);
                if (status.IsSuccess)
                {
                    WindowLevel.Hide();
                    LoadLevels();
                    NewMessage.ShowNormalMessage("Family Member saved.");
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }

            }
        }


   
    }
}