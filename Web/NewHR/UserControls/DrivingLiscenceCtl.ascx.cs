﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;

using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;

namespace Web.NewHR.UserControls
{
    public partial class DrivingLiscenceCtl : BaseUserControl
    {
        private bool _isDisplayMode = false;
        public bool IsDisplayMode
        {
            set
            {
                if (value == true)
                {
                    _isDisplayMode = value;
                    CommandColumn3.Visible = false;
                    CommandColumn2.Visible = false;
                    LinkButton1.Visible = false;
                    GridDrivingLiscence.Width = 680;
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();

                if (SessionManager.CurrentLoggedInEmployeeId != 0)
                    hdnIsEmployee.Text = "1";
                else
                    hdnIsEmployee.Text = "0";

            }
        }
        protected void Initialise()
        {
            int EmployeeID = GetEmployeeID();
            this.LoadDrivingLiscenceGrid(EmployeeID);
            CommonManager _CommonManager = new CommonManager();
            cmbIssuingCountry.Store[0].DataSource = _CommonManager.GetAllCountries();
            cmbIssuingCountry.Store[0].DataBind();

        }
        protected void btnSave_Click(object sender, DirectEventArgs e)
        {

            this.SaveFormData();

        }
        private void SaveFormData()
        {

            Status myStatus = new Status();
            HDrivingLicence _HDrivingLiscence = new HDrivingLicence();
            _HDrivingLiscence.EmployeeId = this.GetEmployeeID();
            bool isSave = bool.Parse(this.hdnDrivingLiscenceID.Text == "" ? "true" : "false");
            if (!isSave)
                _HDrivingLiscence.DrivingLicenceId = int.Parse(this.hdnDrivingLiscenceID.Text);
           
            var LiscenceTypeName = "";

            if (chkTwoWheeler.Checked)
            {

                if (LiscenceTypeName == "")
                    LiscenceTypeName += "Two Wheeler";
                else
                    LiscenceTypeName += "," + "Two Wheeler";
            }

            if (chkFourWheeler.Checked)
            {

                if (LiscenceTypeName == "")
                    LiscenceTypeName += "Four Wheeler";
                else
                    LiscenceTypeName += "," + "Four Wheeler";
            }

            if (chkHeavy.Checked)
            {

                if (LiscenceTypeName == "")
                    LiscenceTypeName += "Heavy";
                else
                    LiscenceTypeName += "," + "Heavy";
            }

            if (string.IsNullOrEmpty(LiscenceTypeName))
            {
                NewMessage.ShowWarningMessage("Select Driving Licence Type.");
                return;
            }



            //foreach (Ext.Net.ListItem _Items in this.cmbLiscenceType.SelectedItems.ToList())
            //{
            //    if (LiscenceTypevalues == "")
            //        LiscenceTypevalues += _Items.Value;
            //    else
            //        LiscenceTypevalues += "," + _Items.Value ;

            //    if (LiscenceTypeName == "")
            //        LiscenceTypeName += _Items.Text;
            //    else
            //        LiscenceTypeName += "," + _Items.Text ;
            //}

            if (!string.IsNullOrEmpty(LiscenceTypeName))
            {
              //  _HDrivingLiscence.LiscenceTypeID = LiscenceTypevalues;
                _HDrivingLiscence.LiscenceTypeName = LiscenceTypeName;
            }

            _HDrivingLiscence.DrivingLicenceNo = txtDrivingLiscenceNumber.Text.Trim();
            _HDrivingLiscence.IssuingCountry = cmbIssuingCountry.SelectedItem.Text;
         
            //file upload section
            string UserFileName = this.FileDrivingLiscenceDocumentUpload.FileName;
            string ServerFileName = Guid.NewGuid().ToString() + Path.GetExtension(UserFileName);
            string relativePath = @"../uploads/" + ServerFileName;
            if (this.UploadFileDrivingLiscence(relativePath))
            {
                double fileSize = FileDrivingLiscenceDocumentUpload.PostedFile.ContentLength;

                _HDrivingLiscence.FileFormat = Path.GetExtension(this.FileDrivingLiscenceDocumentUpload.FileName).Replace(".", "").Trim();
                _HDrivingLiscence.FileType = this.FileDrivingLiscenceDocumentUpload.PostedFile.ContentType;
                _HDrivingLiscence.FileLocation = @"../Uploads/";
                _HDrivingLiscence.ServerFileName = ServerFileName;
                _HDrivingLiscence.UserFileName = UserFileName;
                _HDrivingLiscence.Size = this.ToSizeString(fileSize);

            }

            if (SessionManager.CurrentLoggedInEmployeeId == 0)
                _HDrivingLiscence.Status = 1;
            else
                _HDrivingLiscence.Status = 0;

            myStatus = NewHRManager.Instance.InsertUpdateDrivingLicence(_HDrivingLiscence, isSave);

            if (myStatus.IsSuccess)
            {
                if (isSave)
                    NewMessage.ShowNormalMessage("Record Save successfully");
                else
                    NewMessage.ShowNormalMessage("Record Updated successfully");

                //  this.ClearFields();
                this.AEDrivingLiscenceWindow.Close();
                this.LoadDrivingLiscenceGrid(this.GetEmployeeID());

            }
            else
                NewMessage.ShowWarningMessage(myStatus.ErrorMessage);
        }
        protected string ToSizeString(double bytes)
        {

            string[] SizeSuffixes = { "bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };
            int mag = (int)Math.Log(bytes, 1024);
            decimal adjustedSize = (decimal)bytes / (1 << (mag * 10));
            return string.Format("{0:n1} {1}", adjustedSize, SizeSuffixes[mag]);
        }
        protected void DrivingLiscenceDownLoad(int ID)
        {

            //string contentType = "";
            HDrivingLicence doc = NewHRManager.GetDrivingLicenceDetailsById(ID);

            string path = Context.Server.MapPath(doc.FileLocation + doc.ServerFileName);
            string contentType = doc.FileType;
            string name = doc.UserFileName + "." + doc.FileFormat;

            byte[] bytes = File.ReadAllBytes(path);

            Context.Response.Clear();
            Context.Response.ClearHeaders();
            Context.Response.AppendHeader("Content-Disposition", "attachment; filename=\"" + name + "\"");
            Context.Response.ContentType = contentType;
            Context.Response.BinaryWrite(bytes);
            Context.Response.End();
            // Response.Redirect("~/DocumentHandler.ashx?ID=" + ID);
        }
        protected bool UploadFileDrivingLiscence(string relativePath)
        {

            if (this.FileDrivingLiscenceDocumentUpload.HasFile)
            {

                int fileSize = FileDrivingLiscenceDocumentUpload.PostedFile.ContentLength;
                this.FileDrivingLiscenceDocumentUpload.PostedFile.SaveAs(Server.MapPath(relativePath));
                return true;
            }
            else
                return false;

        }
        protected void LoadDrivingLiscenceGrid(int EmployeeID)
        {
            List<HDrivingLicence> _HDrivingLiscence = NewHRManager.GetDrivingLicenceByEmployeeID(EmployeeID);
            if (_HDrivingLiscence != null)
            {
                this.StoreDrivingLiscence.DataSource = _HDrivingLiscence;
                this.StoreDrivingLiscence.DataBind();
            }
            else
            {
                this.StoreDrivingLiscence.DataSource = this.DrivingLiscenceFillData;
                this.StoreDrivingLiscence.DataBind();
            }
        }
        protected int GetEmployeeID()
        {
            //Guid? AssetID = null;
            int EmpID = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                EmpID = int.Parse(Request.QueryString["ID"]);
            }
            else
                EmpID = SessionManager.CurrentLoggedInEmployeeId;

            return EmpID;
        }
        protected void btnAddNewLine_Click(object sender, DirectEventArgs e)
        {
            this.ClearFields();
            this.AEDrivingLiscenceWindow.Show();

        }
        protected void lnkDeleteFile_Click(object sender, DirectEventArgs e)
        {
            int DrivingLiscenceID = int.Parse(hdnDrivingLiscenceID.Text);
            Status myStatus = new Status();

            HDrivingLicence _HDrivingLiscence = NewHRManager.GetDrivingLicenceDetailsById(DrivingLiscenceID);
            string path = Context.Server.MapPath(_HDrivingLiscence.FileLocation + _HDrivingLiscence.ServerFileName);
            myStatus = NewHRManager.Instance.DeleteDrivingLicenceFile(DrivingLiscenceID);
            if (myStatus.IsSuccess)
            {
                if (File.Exists(path))
                {
                    File.Delete(path);
                    lnkDeleteFile.Hide();
                    lblUploadedFile.Hide();
                    NewMessage.ShowNormalMessage("file deleted successfully");
                    //refresh grid
                    this.LoadDrivingLiscenceGrid(this.GetEmployeeID());

                }
            }



        }
        private object[] DrivingLiscenceFillData
        {
            get
            {
                return new object[]
            {
                new object[] { "", "", "",""},
               
            };
            }
        }
        protected void GridDrivingLiscence_Command(object sender, DirectEventArgs e)
        {
            string commandName = e.ExtraParams["command"];
            int DrivingLiscenceID = int.Parse(e.ExtraParams["ID"]);
            HDrivingLicence _HDrivingLiscence = NewHRManager.GetDrivingLicenceDetailsById(int.Parse(e.ExtraParams["ID"]));
            string Path = Server.MapPath(_HDrivingLiscence.FileLocation) + _HDrivingLiscence.ServerFileName;
            switch (commandName)
            {
                case "Delete":
                    this.DeleteDrivingLiscence(DrivingLiscenceID, Path);
                    break;
                case "DownLoad":
                    this.DrivingLiscenceDownLoad(DrivingLiscenceID);
                    break;
                case "Edit":
                    this.editDrivingLiscence(DrivingLiscenceID);
                    break;
            }

        }
        protected void DeleteDrivingLiscence(int ID, string path)
        {

            bool result = NewHRManager.DeleteDrivingLicenceByID(ID);
            if (result)
            {
                if (File.Exists(path))
                    File.Delete(path);
                //refresh grid
                NewMessage.ShowNormalMessage("Record deleted successfully.");
                this.LoadDrivingLiscenceGrid(this.GetEmployeeID());
            }

        }
        protected void ClearFields()
        {
            this.hdnDrivingLiscenceID.Text = "";
            txtDrivingLiscenceNumber.Text = "";
            cmbIssuingCountry.Value = "";
            chkTwoWheeler.Checked = false;
            chkFourWheeler.Checked = false;
            chkHeavy.Checked = false;
            lblUploadedFile.Hide();
            lblUploadedFile.Text = "";
            lnkDeleteFile.Hide();
            btnSave.Text = Resources.Messages.Save;
            FileDrivingLiscenceDocumentUpload.Reset();
        }
        public void editDrivingLiscence(int DrivingLiscenceID)
        {
            HDrivingLicence _HDrivingLiscence = NewHRManager.GetDrivingLicenceDetailsById(DrivingLiscenceID);
            this.hdnDrivingLiscenceID.Text = DrivingLiscenceID.ToString();
            txtDrivingLiscenceNumber.Text = _HDrivingLiscence.DrivingLicenceNo;
            cmbIssuingCountry.SetValue(_HDrivingLiscence.IssuingCountry.ToString());
            if (_HDrivingLiscence.LiscenceTypeName.Contains("Two Wheeler") == true)
                chkTwoWheeler.Checked = true;
            else
                chkTwoWheeler.Checked = false;

            if (_HDrivingLiscence.LiscenceTypeName.Contains("Four Wheeler") == true)
                chkFourWheeler.Checked = true;
            else
                chkFourWheeler.Checked = false;

            if (_HDrivingLiscence.LiscenceTypeName.Contains("Heavy") == true)
                chkHeavy.Checked = true;
            else
                chkHeavy.Checked = false;
          
            if (!string.IsNullOrEmpty(_HDrivingLiscence.UserFileName))
            {
                lblUploadedFile.Show();
                lblUploadedFile.Text = _HDrivingLiscence.UserFileName;
                lnkDeleteFile.Show();
            }
            else
            {
                lblUploadedFile.Hide();
                lblUploadedFile.Text = "";
                lnkDeleteFile.Hide();
            }
            btnSave.Text = Resources.Messages.Update;
            this.AEDrivingLiscenceWindow.Show();
        }
        public string GetComboSelection(Ext.Net.ComboBox cmb)
        {
            if (cmb.SelectedItem == null || cmb.SelectedItem.Value == null)
                return null;

            return cmb.SelectedItem.Value;

        }
        public string GetComboSelectionText(Ext.Net.ComboBox cmb)
        {
            if (cmb.SelectedItem == null || cmb.SelectedItem.Value == null)
                return null;

            return cmb.SelectedItem.Text;
        }

    }
}