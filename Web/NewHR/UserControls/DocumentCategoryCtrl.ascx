﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DocumentCategoryCtrl.ascx.cs" Inherits="Web.NewHR.UserControls.DocumentCategoryCtrl" %>

<ext:Hidden ID="hdnDocumentCategory" runat="server" />
<ext:Hidden ID="hdnIsCategoryPage" runat="server" />

<ext:Window ID="WDocumentCategory" runat="server" Title="Document Category" Icon="Application" ButtonAlign="Left"
            Width="400" Height="230" BodyPadding="5" Hidden="true" Modal="true">
            <Content>
                <table class="fieldTable">
                    <tr>
                        <td>
                            <ext:TextField ID="txtCategoryName" LabelSeparator="" runat="server" FieldLabel="Name *"
                                LabelWidth="60" Width="300" LabelAlign="Left">
                            </ext:TextField>
                            <asp:RequiredFieldValidator Display="None" ID="rfvName" runat="server" ValidationGroup="SaveUpdCategory"
                                ControlToValidate="txtCategoryName" ErrorMessage="Name is required." />
                        </td>
                    </tr>
                   <tr>
                        <td>
                            <ext:TextField ID="txtCategoryCode" LabelSeparator="" runat="server" FieldLabel="Code *"
                                LabelWidth="60" Width="150" LabelAlign="Left" MaxLength="4" EnforceMaxLength="true">
                            </ext:TextField>
                            <asp:RequiredFieldValidator Display="None" ID="rfvCode" runat="server" ValidationGroup="SaveUpdCategory"
                                ControlToValidate="txtCategoryCode" ErrorMessage="Code is required." />
                        </td>
                    </tr>
                </table>
            </Content>
            <Buttons>
                <ext:Button runat="server" ID="btnCategorySave" Text="Save" MarginSpec="0 0 0 15">
                    <DirectEvents>
                        <Click OnEvent="btnCategorySave_Click">
                            <EventMask ShowMask="true" />
                        </Click>
                    </DirectEvents>
                    <Listeners>
                        <Click Handler="valGroup = 'SaveUpdCategory'; if(CheckValidation()) return ''; else return false;">
                        </Click>
                    </Listeners>
                </ext:Button>

                <ext:Button runat="server" ID="LinkButton1" Text="<i></i>Cancel" MarginSpec="0 0 0 10">
                    <Listeners>
                        <Click Handler="#{WDocumentCategory}.hide();">
                        </Click>
                    </Listeners>
                </ext:Button>
            </Buttons>
        </ext:Window>