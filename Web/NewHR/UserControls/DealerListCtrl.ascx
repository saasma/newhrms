﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DealerListCtrl.ascx.cs"
    Inherits="Web.NewHR.UserControls.DealerListCtrl" %>
<ext:Hidden ID="hdnPartyID" runat="server" />
<ext:Hidden ID="hdnFileID" runat="server" />
<ext:Hidden ID="hdnDocumentID" runat="server" />
<ext:Button runat="server" AutoPostBack="true" OnClick="btnDownload_Click" Hidden="true"
    ID="btnDownloadFile" Text="<i></i>Download">
</ext:Button>
<ext:Button ID="btnDelete" runat="server" Hidden="true">
    <DirectEvents>
        <Click OnEvent="btnDelete_Click">
            <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete?" />
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:Button>
<table>
    <tr>
        <td valign="top">
            <div id="divList" runat="server" style="background-color: #ddebf7; height: 80px;
                padding-top: 10px; margin-left: 0px; padding-right: 10px;">
                <table>
                    <tr>
                        <td valign="bottom">
                            <ext:Store runat="server" ID="storeDocument" AutoLoad="false">
                                <Proxy>
                                    <ext:AjaxProxy Url="../../Handler/DocDealerNameHandler.ashx">
                                        <ActionMethods Read="GET" />
                                        <Reader>
                                            <ext:JsonReader Root="plants" TotalProperty="total" />
                                        </Reader>
                                    </ext:AjaxProxy>
                                </Proxy>
                                <Model>
                                    <ext:Model ID="Model3" IDProperty="Value" Name="ItemLineModel" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Text" Type="String" />
                                            <ext:ModelField Name="Value" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Parameters>
                                    <ext:StoreParameter Name="Type" Value="1" Mode="Raw"
                                    ApplyMode="Always" />
                                    </Parameters>
                            </ext:Store>
                            <ext:ComboBox LabelSeparator="" ID="cmbDealerName" runat="server" DisplayField="Text"
                                MarginSpec="20 0 0 10" FieldLabel="" LabelAlign="Top" ValueField="Value" EmptyText="Search Dealer"
                                StoreID="storeDocument" TypeAhead="false" Width="200" HideBaseTrigger="true"
                                MinChars="1" TriggerAction="All" ForceSelection="false">
                                <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                                    <ItemTpl ID="ItemTpl1" runat="server">
                                        <Html>
                                            <tpl>
                                            <div class="search-item">
                                                            <span>{Text}</span>  
                                             </div>
					                </tpl>
                                        </Html>
                                    </ItemTpl>
                                </ListConfig>
                                <Triggers>
                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                </Triggers>
                                <Listeners>
                                    <Select Handler="this.getTrigger(0).show();" />
                                    <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                    <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                                </Listeners>
                            </ext:ComboBox>
                        </td>
                        <td>
                            <ext:ComboBox ID="cmbBranch" Width="150" runat="server" ValueField="BranchId" DisplayField="Name"
                                    FieldLabel="Branch" LabelAlign="top" LabelSeparator="" ForceSelection="true" MarginSpec="0 0 0 10"
                                    QueryMode="Local">
                                    <Store>
                                        <ext:Store ID="Store5" runat="server">
                                            <Model>
                                                <ext:Model ID="Model5" runat="server">
                                                    <Fields>
                                                        <ext:ModelField Name="BranchId" />
                                                        <ext:ModelField Name="Name"  />
                                                    </Fields>
                                                </ext:Model>
                                            </Model>
                                        </ext:Store>
                                    </Store>
                                    <Triggers>
                                        <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                    </Triggers>
                                    <Listeners>
                                        <Select Handler="this.getTrigger(0).show();" />
                                        <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                        <TriggerClick Handler="if (index == 0) {
                                               this.clearValue();
                                               this.getTrigger(0).hide();
                                           }" />
                                    </Listeners>
                                </ext:ComboBox>
                        </td>
                        <td>
                            <ext:ComboBox ID="cmbZone" FieldLabel="Zone" runat="server" LabelAlign="Top" DisplayField="Zone"
                                TriggerAction="All" MarginSpec="0 0 0 10" Width="150" LabelSeparator="" QueryMode="Local"
                                ValueField="ZoneId">
                                <Store>
                                    <ext:Store ID="Store1" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Zone" />
                                            <ext:ModelField Name="ZoneId" />
                                        </Fields>
                                    </ext:Store>
                                </Store>
                                <Triggers>
                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                </Triggers>
                                <Listeners>
                                    <Select Handler="this.getTrigger(0).show();" />
                                    <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                    <TriggerClick Handler="if (index == 0) {
                                           this.clearValue();
                                           this.getTrigger(0).hide();
                                       }" />
                                </Listeners>
                            </ext:ComboBox>
                        </td>
                        <td>
                            <ext:ComboBox ID="cmbDistrict" FieldLabel="District" runat="server" LabelAlign="Top"
                                MarginSpec="0 0 0 10" Width="150" LabelSeparator="" QueryMode="Local" DisplayField="District"
                                ValueField="DistrictId">
                                <Store>
                                    <ext:Store ID="storeDistrict" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="District" />
                                            <ext:ModelField Name="DistrictId" />
                                        </Fields>
                                    </ext:Store>
                                </Store>
                                <Triggers>
                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                </Triggers>
                                <Listeners>
                                    <Select Handler="this.getTrigger(0).show();" />
                                    <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                    <TriggerClick Handler="if (index == 0) {
                                           this.clearValue();
                                           this.getTrigger(0).hide();
                                       }" />
                                </Listeners>
                            </ext:ComboBox>
                        </td>
                        <td>
                            <ext:ComboBox Width="110" LabelSeparator="" LabelAlign="Top" MarginSpec="0 0 0 10"
                                QueryMode="Local" ID="cmbExpiring" ForceSelection="true" DisplayField="Text"
                                ValueField="Value" runat="server" FieldLabel="Expiring">
                                <Items>
                                    <ext:ListItem Text="Today" Value="1" />
                                    <ext:ListItem Text="This Week" Value="2" />
                                    <ext:ListItem Text="Last Week" Value="3" />
                                    <ext:ListItem Text="This Month" Value="4" />
                                    <ext:ListItem Text="Last Month" Value="5" />
                                    <ext:ListItem Text="Custom Date" Value="6" />
                                </Items>
                                <Triggers>
                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                </Triggers>
                                <Listeners>
                                    <Select Handler="this.getTrigger(0).show();" />
                                    <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                    <TriggerClick Handler="if (index == 0) { 
                            this.clearValue(); 
                            this.getTrigger(0).hide();
                        }" />
                                </Listeners>
                                <DirectEvents>
                                    <Select OnEvent="cmbExpiring_Select" />
                                    <TriggerClick OnEvent="cmbExpiring_Select" />
                                </DirectEvents>
                            </ext:ComboBox>
                        </td>
                        <td style="padding-top: 18px;">
                            <ext:Button ID="btnLoad" runat="server" Cls="btn btn-save" Text="Show" Width="100"
                                MarginSpec="0 0 0 10">
                                <Listeners>
                                    <Click Fn="searchList">
                                    </Click>
                                </Listeners>
                            </ext:Button>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <pr:CalendarExtControl ID="calDateAddedFrom" LabelSeparator="" runat="server" FieldLabel="Date Added From"
                                MarginSpec="10 0 0 10" Hidden="true" Width="120" LabelAlign="Top">
                            </pr:CalendarExtControl>
                        </td>
                        <td>
                            <pr:CalendarExtControl ID="calDateAddedTo" LabelSeparator="" runat="server" FieldLabel="Date Added To"
                                MarginSpec="10 0 0 10" Hidden="true" Width="120" LabelAlign="Top">
                            </pr:CalendarExtControl>
                        </td>
                        <td>
                            <pr:CalendarExtControl ID="calExpiringDateFrom" LabelSeparator="" runat="server"
                                FieldLabel="Expiring Date From" MarginSpec="10 0 0 10" Hidden="true" Width="120"
                                LabelAlign="Top">
                            </pr:CalendarExtControl>
                        </td>
                        <td>
                            <pr:CalendarExtControl ID="calExpiringDateTo" LabelSeparator="" runat="server" FieldLabel="Expiring Date To"
                                MarginSpec="10 0 0 10" Hidden="true" Width="120" LabelAlign="Top">
                            </pr:CalendarExtControl>
                        </td>
                    </tr>
                </table>
            </div>
            <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridDocumentList" Width="1050" runat="server"
                Cls="itemgrid" MinHeight="400" Scroll="Horizontal">
                <Store>
                    <ext:Store ID="storeDocumentList" runat="server" AutoLoad="true" OnReadData="Store_ReadData"
                        RemotePaging="true" RemoteSort="true">
                        <Proxy>
                            <ext:PageProxy />
                        </Proxy>
                        <Model>
                            <ext:Model ID="Model6" runat="server" IDProperty="ContactId">
                                <Fields>
                                    <ext:ModelField Name="ContactId" Type="String" />
                                    <ext:ModelField Name="Name" Type="String" />
                                    <ext:ModelField Name="ID" Type="String" />
                                    <ext:ModelField Name="Branch" Type="String" />
                                    <ext:ModelField Name="Zone" Type="String" />
                                    <ext:ModelField Name="District" Type="String" />
                                    <ext:ModelField Name="ContactPerson" Type="String" />
                                    <ext:ModelField Name="OfficePhoneNo" Type="String" />
                                    <ext:ModelField Name="MobileNo" Type="String" />
                                    <ext:ModelField Name="ExpiryDate" Type="String" />
                                    <ext:ModelField Name="DaysTillExpiry" Type="String" />
                                    <ext:ModelField Name="UserFileName" Type="String" />
                                    <ext:ModelField Name="FileID" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:Column ID="colParyName" Sortable="false" MenuDisabled="true" runat="server"
                            Text="Dealer Name" Width="190" Align="Left" DataIndex="Name" Locked="true">
                        </ext:Column>
                        <ext:Column ID="Column1" Sortable="false" MenuDisabled="true" runat="server" Text="Dealer Id"
                            Width="100" Align="Left" DataIndex="ID">
                        </ext:Column>
                        <ext:Column ID="Column9" Sortable="false" MenuDisabled="true" runat="server" Text="Branch"
                            Width="150" Align="Left" DataIndex="Branch">
                        </ext:Column>
                        <ext:Column ID="Column2" Sortable="false" MenuDisabled="true" runat="server" Text="Zone"
                            Width="100" Align="Left" DataIndex="Zone">
                        </ext:Column>
                        <ext:Column ID="Column4" Sortable="false" MenuDisabled="true" runat="server" Text="District"
                            Width="100" Align="Left" DataIndex="District">
                        </ext:Column>
                        <ext:Column ID="Column6" Sortable="false" MenuDisabled="true" runat="server" Text="Contact Person"
                            Width="120" Align="Left" DataIndex="ContactPerson">
                        </ext:Column>
                        <ext:Column ID="Column7" Sortable="false" MenuDisabled="true" runat="server" Text="Phone No"
                            Width="100" Align="Left" DataIndex="OfficePhoneNo">
                        </ext:Column>
                        <ext:Column ID="Column8" Sortable="false" MenuDisabled="true" runat="server" Text="Mobile No"
                            Width="100" Align="Left" DataIndex="MobileNo">
                        </ext:Column>
                        <ext:Column ID="Column3" Sortable="false" MenuDisabled="true" runat="server" Text="Agreement Expiry"
                            Width="120" Align="Left" DataIndex="ExpiryDate">
                        </ext:Column>
                        <ext:Column ID="colDocumentDate" Sortable="false" MenuDisabled="true" runat="server"
                            Text="Expiry" Width="100" Align="Left" DataIndex="DaysTillExpiry">
                        </ext:Column>
                         <ext:Column ID="Column5" Sortable="false" MenuDisabled="true" runat="server"
                            Text="Document Name" Width="180" Align="Left" DataIndex="UserFileName" Hidden="true">
                        </ext:Column>
                         <ext:CommandColumn ID="CommandColumn3" runat="server" Width="40" Text="" Align="Center" MenuDisabled="true" Sortable="false" Hidden="true">
                            <Commands>
                                <ext:CommandSeparator />
                                <ext:GridCommand Icon="PageWhitePut" CommandName="DownLoad">
                                    <ToolTip Text="DownLoad" />
                                </ext:GridCommand>
                            </Commands>
                            <PrepareToolbar Fn="prepareToolbar" />
                            <Listeners>
                                <Command Handler="CommandHandlerFile(command,record);" />
                            </Listeners>
                        </ext:CommandColumn>

                        <ext:CommandColumn ID="ComColEdit" runat="server" Width="40" Text="" Align="Center" MenuDisabled="true" Sortable="false">
                            <Commands>
                                <ext:CommandSeparator />
                                <ext:GridCommand Cls="editGridButton" ToolTip-Text="Edit" Text="<i class='fa fa-pencil'></i>"
                                    CommandName="Edit">
                                    <ToolTip Text="Edit" />    
                                </ext:GridCommand>
                            </Commands>
                            <Listeners>
                                <Command Handler="CommandHandler(command,record);" />
                            </Listeners>
                        </ext:CommandColumn>
                        <ext:CommandColumn ID="ComColDel" runat="server" Width="40" Text="" Align="Center" MenuDisabled="true" Sortable="false">
                            <Commands>
                                <ext:CommandSeparator />
                                <ext:GridCommand Cls="editGridButton" ToolTip-Text="Delete" Text="<i class='fa fa-trash-o'></i>"
                                CommandName="Delete">
                                    <ToolTip Text="Delete" />
                                </ext:GridCommand>
                            </Commands>
                            <Listeners>
                                <Command Handler="CommandHandler(command,record);" />
                            </Listeners>
                        </ext:CommandColumn>     
                    </Columns>
                </ColumnModel>
                <View>
                    <ext:GridView ID="GridView1" runat="server">
                        <GetRowClass Fn="getRowClass" />
                    </ext:GridView>
                </View>
                <BottomBar>
                    <ext:PagingToolbar Height="40" ID="PagingToolbar1" runat="server" StoreID="storeDocumentList"
                        DisplayInfo="true">
                        <Items>
                            <ext:Label ID="lblPageSize" runat="server" Text="Page size:" />
                            <ext:ToolbarSpacer ID="ToolbarSpacer_listItems" runat="server" Width="10" />
                            <ext:ComboBox ID="cmbPageSize" runat="server" Width="80" SelectOnFocus="true" Selectable="true"
                                ValueField="Value" DisplayField="Text" ForceSelection="true" AllowBlank="false">
                                <Listeners>
                                    <Select Handler="searchList()" />
                                </Listeners>
                                <Items>
                                    <ext:ListItem Value="20" Text="20" />
                                    <ext:ListItem Value="50" Text="50" />
                                    <ext:ListItem Value="100" Text="100" />
                                </Items>
                                <SelectedItems>
                                    <ext:ListItem Index="0">
                                    </ext:ListItem>
                                </SelectedItems>
                            </ext:ComboBox>

                            <ext:Button ID="btnExport" runat="server" Text="To Excel" AutoPostBack="true" OnClick="btnExport_Click"  Icon="PageExcel">
                            </ext:Button>
                        </Items>
                    </ext:PagingToolbar>
                </BottomBar>
            </ext:GridPanel>
        </td>
    </tr>
</table>
<script type="text/javascript">

    var getRowClass = function (record) {

    };

    function searchList() {
        <%=gridDocumentList.ClientID %>.getStore().pageSize = <%=cmbPageSize.ClientID %>.getValue();
        <%=PagingToolbar1.ClientID %>.doRefresh();
    }

    function selectParty(dataview, record, item, index, e) 
    {
        <%= hdnPartyID.ClientID %>.setValue(record.data.PartyID);
        searchList();  
    }

    var CommandHandler = function(command, record){
        var documentID = record.data.ContactId;
          
        if(command=="View")
        {
            window.location = 'Dealer.aspx?DocumentId=' + documentID;
        }
        else if(command=="Edit")
        {
            window.location = 'Dealer.aspx?DocumentId=' + documentID;
        }
        else
        {
            <%= hdnDocumentID.ClientID %>.setValue(documentID);
            <%= btnDelete.ClientID %>.fireEvent('click');
        }

    };

     var CommandHandlerFile = function(command, record){
            <%= hdnFileID.ClientID %>.setValue(record.data.FileID);
       
         if(command == "DownLoad")
            {
                <%= btnDownloadFile.ClientID %>.fireEvent('click');
            }
        };

    var prepareToolbar = function (grid, toolbar, rowIndex, record) {

        if (record.data.FileID == '') {
            toolbar.hide();
        }
    };

    function SetMinHeight()
    {
        document.getElementById("<%= divList.ClientID %>").style.height = "80px";
    }

    function SetMaxHeight()
    {
        document.getElementById("<%= divList.ClientID %>").style.height = "140px";
    }


</script>
