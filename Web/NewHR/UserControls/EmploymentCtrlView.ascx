﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmploymentCtrlView.ascx.cs"
    Inherits="Web.NewHR.UserControls.EmploymentCtrlView" %>

<ext:GridPanel ID="GridPreviousEmployment" runat="server" Width="920" Cls="itemgrid"
    TrackMouseOver="true" AutoExpandColumn="Organization" Scroll="None">
    <Store>
        <ext:Store ID="StorePreviousEmployment" runat="server">
            <Model>
                <ext:Model ID="Model1" runat="server" IDProperty="PrevEmploymentId">
                    <Fields>
                        <ext:ModelField Name="Category" Type="String" />
                        <ext:ModelField Name="Organization" Type="String" />
                        <ext:ModelField Name="Place" Type="string" />
                        <ext:ModelField Name="Position" Type="string" />
                        <ext:ModelField Name="JobResponsibility" Type="string" />
                        <ext:ModelField Name="IsEditable" Type="Int" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
    </Store>
    <ColumnModel>
        <Columns>
            <ext:Column ID="Column5" Width="160" runat="server" Text="Experience Category" Wrap="true"
                DataIndex="Category" />
            <ext:Column ID="Column1" Width="180" runat="server" Text="Organization" DataIndex="Organization"
                Wrap="true" />
            <ext:Column ID="Column2" runat="server" Width="160" Text="Place" DataIndex="Place"
                Wrap="true">
            </ext:Column>
            <ext:Column ID="Column3" runat="server" Width="160" Text="Position" DataIndex="Position"
                Wrap="true">
            </ext:Column>
            <ext:Column ID="Column4" runat="server" Text="Job Responsibility" DataIndex="JobResponsibility"
                Wrap="true" Width="260">
            </ext:Column>
        </Columns>
    </ColumnModel>
    <SelectionModel>
        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
    </SelectionModel>
</ext:GridPanel>
<ext:ToolTip ID="RowTip" runat="server" Target="#{GridPreviousEmployment.getView().mainBody}"
    Delegate=".x-grid3-cell" TrackMouse="true">
    <Listeners>
        <Show Fn="showTip" />
    </Listeners>
</ext:ToolTip>

<script type="text/javascript">

    var showTip = function () {
        var rowIndex = GridPreviousEmployment.view.findRowIndex(this.triggerElement),
                cellIndex = GridPreviousEmployment.view.findCellIndex(this.triggerElement),
                record = StorePreviousEmployment.getAt(rowIndex),
                fieldName = GridPreviousEmployment.getColumnModel().getDataIndex(cellIndex),
                data = record.get(fieldName);

        this.body.dom.innerHTML = data;
    };

</script>
