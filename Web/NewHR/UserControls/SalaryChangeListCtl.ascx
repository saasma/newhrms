﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SalaryChangeListCtl.ascx.cs"
    Inherits="Web.NewHR.UserControls.SalaryChangeListCtl" %>
<script type="text/javascript">
    var arrearRender = function (value) {
        if (value)
            return 'Yes';
        return '';
    }
</script>
<ext:GridPanel ID="GridLevels" runat="server">
    <Store>
        <ext:Store ID="Store3" runat="server">
            <Model>
                <ext:Model ID="Model4" runat="server">
                    <Fields>
                        <ext:ModelField Name="Income" Type="string" />
                        <ext:ModelField Name="FromDate" Type="string" />
                        <ext:ModelField Name="PreviousAmountRate" Type="string" />
                        <ext:ModelField Name="NewAmountRate" Type="string" />
                        <ext:ModelField Name="IsReterospect" Type="Boolean" />
                        <ext:ModelField Name="Period" Type="string" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
    </Store>
    <ColumnModel>
        <Columns>
            <ext:Column ID="Column4" Width="100" Sortable="false" MenuDisabled="true" runat="server"
                Text="Income" Align="Left" DataIndex="Income">
            </ext:Column>
            <ext:Column ID="Column1" Width="100" Sortable="false" MenuDisabled="true" runat="server"
                Text="From Date" Align="Left" DataIndex="FromDate">
            </ext:Column>
            <ext:Column ID="Column5" Width="120" Sortable="false" MenuDisabled="true" runat="server"
                Text="Period" Align="Left" DataIndex="Period">
            </ext:Column>
             <ext:Column ID="Column6" Width="100" Sortable="false" MenuDisabled="true" runat="server"
                Text="Arrear" Align="Left" DataIndex="IsReterospect">
                <Renderer Fn="arrearRender" />
            </ext:Column>
            <ext:Column ID="Column2" Width="150" Sortable="false" MenuDisabled="true" runat="server"
                Text="Old Amount/Rate" Align="Right" DataIndex="PreviousAmountRate">
                <Renderer Fn="getFormattedAmount" />
            </ext:Column>
            <ext:Column ID="Column3" Width="150" Sortable="false" MenuDisabled="true" runat="server"
                Text="New Amount/Rate" Align="Right" DataIndex="NewAmountRate">
                <Renderer Fn="getFormattedAmount" />
            </ext:Column>
        </Columns>
    </ColumnModel>
    <SelectionModel>
        <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
    </SelectionModel>
</ext:GridPanel>
