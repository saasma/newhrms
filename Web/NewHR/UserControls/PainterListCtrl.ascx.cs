﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Entity;
using BLL;
using System.IO;
using Utils;
using System.Text;
using BLL.BO;
using Utils.Calendar;
namespace Web.NewHR.UserControls
{
    public partial class PainterListCtrl : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
                Initialise();
        }

        private void Initialise()
        {
            BindBranchCombo();
            BindDealerCombo();
            BindZoneCombo();
            BindDistrictCombo();

            if (!DocumentManager.CanUserAddEditDocument())
                ComColEdit.Hide();

            if (!DocumentManager.CanUserDeleteDocument())
                ComColDel.Hide();
        }

        private void BindBranchCombo()
        {
            cmbBranch.Store[0].DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            cmbBranch.Store[0].DataBind();
        }

        private void BindDealerCombo()
        {
            cmbDealer.Store[0].DataSource = DocumentManager.GetDealers();
            cmbDealer.Store[0].DataBind();
        }

        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            int totalRows = 0, pageSize = 0;

            DateTime? expiryDateFrom = null, expiryDateTo = null;
            Guid? partyID = null;


            pageSize = int.Parse(cmbPageSize.SelectedItem.Value);


            int? ZoneId = null;
            int? DistrictId = null;

            if (!string.IsNullOrEmpty(cmbZone.SelectedItem.Value))
                ZoneId = int.Parse(cmbZone.SelectedItem.Value);

            if (!string.IsNullOrEmpty(cmbDistrict.SelectedItem.Value))
                DistrictId = int.Parse(cmbDistrict.SelectedItem.Value);

            string DealerName = "";
            if (!string.IsNullOrEmpty(cmbDealerName.SelectedItem.Text))
                DealerName = cmbDealerName.SelectedItem.Text;
            else
                DealerName = cmbDealerName.Text.Trim();

            int branchId = -1;
            Guid? dealerId = null;

            if (cmbBranch.SelectedItem != null && cmbBranch.SelectedItem.Value != null)
                branchId = int.Parse(cmbBranch.SelectedItem.Value);

            if (cmbDealer.SelectedItem != null && cmbDealer.SelectedItem.Value != null)
                dealerId = new Guid(cmbDealer.SelectedItem.Value);

            List<GetDocPainterListResult> list = DocumentManager.GetGetDocPainterListResult(e.Start / pageSize, pageSize, DealerName, ZoneId, DistrictId, branchId, dealerId);

            if (list.Count > 0)
                totalRows = list[0].TotalRows.Value;

            storeDocumentList.DataSource = list;
            storeDocumentList.DataBind();
            e.Total = totalRows;

        }

        private void BindZoneCombo()
        {
            CommonManager _CommonManager = new CommonManager();
            cmbZone.Store[0].DataSource = _CommonManager.GetAllZones();
            cmbZone.Store[0].DataBind();
        }

        private void BindDistrictCombo()
        {
            cmbDistrict.Store[0].DataSource = BLL.BaseBiz.PayrollDataContext.DistrictLists.OrderBy(x => x.District);
            cmbDistrict.Store[0].DataBind();
        }

        protected void btnDownload_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(hdnFileID.Text))
            {
                NewMessage.ShowWarningMessage("Document not found.");
                return;
            }

            Guid fileId = new Guid(hdnFileID.Text);
            DocContactAttachment doc = DocumentManager.GetDocContactAttachmentsByID(fileId);

            string path = Context.Server.MapPath(doc.SavedFileName);
            string contentType = doc.FileType;
            string name = doc.name + "." + doc.FileFormat;

            byte[] bytes = File.ReadAllBytes(path);

            Context.Response.Clear();
            Context.Response.ClearHeaders();
            Context.Response.AppendHeader("Content-Disposition", "attachment; filename=\"" + name + "\"");
            Context.Response.ContentType = contentType;
            Context.Response.BinaryWrite(bytes);
            Context.Response.End();
        }



        protected void btnDelete_Click(object sender, DirectEventArgs e)
        {
            if (string.IsNullOrEmpty(hdnDocumentID.Text.Trim()))
                return;

            Guid documentID = new Guid(hdnDocumentID.Text.Trim());
            Status status = DocumentManager.DeleteDocDealerPainter(documentID);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Painter is deleted.");

                X.Js.Call("searchList");
            }
            else
                NewMessage.ShowWarningMessage(status.ErrorMessage);
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            int totalRows = 0;

            DateTime? expiryDateFrom = null, expiryDateTo = null;
            Guid? partyID = null;

            int? ZoneId = null;
            int? DistrictId = null;

            if (!string.IsNullOrEmpty(cmbZone.SelectedItem.Value))
                ZoneId = int.Parse(cmbZone.SelectedItem.Value);

            if (!string.IsNullOrEmpty(cmbDistrict.SelectedItem.Value))
                DistrictId = int.Parse(cmbDistrict.SelectedItem.Value);

            string DealerName = "";
            if (!string.IsNullOrEmpty(cmbDealerName.SelectedItem.Text))
                DealerName = cmbDealerName.SelectedItem.Text;
            else
                DealerName = cmbDealerName.Text.Trim();

            int branchId = -1;
            Guid? dealerId = null;

            if (cmbBranch.SelectedItem != null && cmbBranch.SelectedItem.Value != null)
                branchId = int.Parse(cmbBranch.SelectedItem.Value);

            if (cmbDealer.SelectedItem != null && cmbDealer.SelectedItem.Value != null)
                dealerId = new Guid(cmbDealer.SelectedItem.Value);

            List<GetDocPainterListResult> list = DocumentManager.GetGetDocPainterListResult(0, 999999, DealerName, ZoneId, DistrictId, branchId, dealerId);

            Bll.ExcelHelper.ExportToExcel("Painter List", list,
             new List<string> { "ContactId", "ContactPerson", "ExpiryDate", "DaysTillExpiry", "TotalRows", "RowNumber", "DealerId" },
         new List<String>() { },
         new Dictionary<string, string>() { { "Name", "Painter Name" }, { "OfficePhoneNo", "Phone No"}, {"MobileNo", "Mobile No"}},
         new List<string>() { }
         , new List<string> { }
         , new List<string> { }
          , new Dictionary<string, string>() {
              {"Painter List",""}
            }
          , new List<string> { "Name", "Zone", "District", "OfficePhoneNo", "MobileNo"});

        }


    }
}