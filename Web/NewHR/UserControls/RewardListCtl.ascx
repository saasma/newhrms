﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RewardListCtl.ascx.cs"
    Inherits="Web.NewHR.UserControls.RewardListCtl" %>
<script type="text/javascript">

      var CommandHandler = function(command, record){
            <%= hiddenValue.ClientID %>.setValue(record.data.RewardID);
                if(command=="Edit")
                {
                    window.location = 'Reward.aspx?RewardID=' + record.data.RewardID;
                }
                else
                {
                    <%= btnDeleteLevel.ClientID %>.fireEvent('click');
                }

             }
        var typeRenderer = function(value)
        {
            if(value=="1")
                return "Cash";
            return "Grade";
        }
        
</script>
<ext:Hidden runat="server" ID="hiddenValue" />
<ext:Button ID="btnDeleteLevel" runat="server" Hidden="true">
    <DirectEvents>
        <Click OnEvent="btnDeleteLevel_Click">
            <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the Reward?" />
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:Button>
<ext:GridPanel StyleSpec="margin-top:15px;" ID="GridLevels" runat="server" Cls="itemgrid">
    <Store>
        <ext:Store ID="Store3" runat="server">
            <Model>
                <ext:Model ID="Model4" runat="server" IDProperty="RewardID">
                    <Fields>
                        <ext:ModelField Name="RewardID" Type="String" />
                        <ext:ModelField Name="EmployeeName" Type="string" />
                        <ext:ModelField Name="LetterNumber" Type="string" />
                        <ext:ModelField Name="LetterDate" Type="string" />
                        <ext:ModelField Name="RewardDate" Type="string" />
                        <ext:ModelField Name="RecommendedBy" Type="string" />
                        <ext:ModelField Name="Type" Type="string" />
                        <ext:ModelField Name="RewardCashOrGrade" Type="string" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
    </Store>
    <ColumnModel>
        <Columns>
            <ext:Column ID="Column8" Sortable="false" MenuDisabled="true" runat="server" Text="Employee"
                Align="Left" Width="180" DataIndex="EmployeeName" />
            <ext:Column ID="Column5" Sortable="false" MenuDisabled="true" runat="server" Text="Letter No"
                Align="Left" Width="100" DataIndex="LetterNumber" />
            <ext:Column ID="Column6" Width="100" Sortable="false" MenuDisabled="true" runat="server"
                Text="LetterDate" Align="Left" DataIndex="LetterDate">
            </ext:Column>
            <ext:Column ID="Column2" Width="120" Sortable="false" MenuDisabled="true" runat="server"
                Text="Reward Date" Align="Left" DataIndex="RewardDate">
            </ext:Column>
            <ext:Column ID="Column3" Width="140" Sortable="false" MenuDisabled="true" runat="server"
                Text="Recommended By" Align="Left" DataIndex="RecommendedBy">
            </ext:Column>
            <ext:Column ID="Column1" Width="100" Sortable="false" MenuDisabled="true" runat="server"
                Text="Type" Align="Left" DataIndex="Type">
                <Renderer Fn="typeRenderer" />
            </ext:Column>
            <ext:Column ID="Column4" Width="80" Sortable="false" MenuDisabled="true" runat="server"
                Text="Reward" Align="Left" DataIndex="RewardCashOrGrade">
            </ext:Column>
            <ext:CommandColumn ID="CommandColumn1" runat="server" Width="75" Text="Actions" Align="Center">
                <Commands>
                    <ext:CommandSeparator />
                    <ext:GridCommand Text="<i></i>" Cls="btn-action glyphicons pencil btn-success" CommandName="Edit" />
                    <ext:GridCommand Text="<i></i>" Cls="btn-action glyphicons remove_2 btn-danger" CommandName="Delete" />
                </Commands>
                <Listeners>
                    <Command Handler="CommandHandler(command,record);" />
                </Listeners>
            </ext:CommandColumn>
        </Columns>
    </ColumnModel>
    <SelectionModel>
        <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
    </SelectionModel>
</ext:GridPanel>
<div class="buttonBlock">
    <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="btnAddLevel" Cls="btnFlat"
        BaseCls="btnFlat" Text="<i></i>Add Reward" runat="server">
        <Listeners>
            <Click Handler="window.location='Reward.aspx';" />
        </Listeners>
    </ext:LinkButton>
</div>
