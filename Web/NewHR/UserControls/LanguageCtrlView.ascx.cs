﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;


namespace Web.NewHR.UserControls
{
    public partial class LanguageCtrlView : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
                Initialise();
        }

        protected void Initialise()
        {
            int EmployeeID = GetEmployeeID();
            this.LoadLanguageSetsGrid(EmployeeID);
        }

        protected void LoadLanguageSetsGrid(int EmployeeID)
        {
            List<NewHREmployeeLanguageSetBO> _EmployeeLanguageSetBO = NewHRManager.GetEmployeesLanguageSet(EmployeeID);

            this.StoreLanguageSets.DataSource = _EmployeeLanguageSetBO;
            this.StoreLanguageSets.DataBind();
            if (_EmployeeLanguageSetBO.Count <= 0)
                GridLanguageSets.Hide();
        }

        protected int GetEmployeeID()
        {
            int EmpID = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                EmpID = int.Parse(Request.QueryString["ID"]);
            }
            else
            {
                EmpID = SessionManager.CurrentLoggedInEmployeeId;
            }
            return EmpID;
        }

    }
}