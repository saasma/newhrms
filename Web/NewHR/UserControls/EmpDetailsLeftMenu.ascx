﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmpDetailsLeftMenu.ascx.cs"
    Inherits="Web.NewHR.UserControls.EmpDetailsLeftMenu" %>
<style type="text/css">
    .contentpanel:before
    {
        content: '';
        height: 100%;
        position: fixed;
        top: 0;
        left: 0;
        width: 210px;
        border-right: 1px solid #e7e7e7;
        z-index: -1;
    }
    .wizardClass ul li
    {
        width: 98px;
    }
</style>
<div class="col-sm-3 col-md-3 col-lg-2 wizardClass" id="rootwizard" runat="server">
    <ul class="nav nav-pills nav-stacked nav-msg" id="list" runat="server">
        <li id="Li7" runat="server"><a id="A13" runat="server" href="~/newhr/EmployeeDetails.aspx">
            Summary </a></li>
        <li id="Li1" runat="server"><a id="A5" runat="server" href="~/newhr/PersonalDetail.aspx">
            Main </a></li>
        <li id="Li2" runat="server"><a id="A6" runat="server" href="~/newhr/address.aspx">Address</a></li>
        <li id="Li11" runat="server"><a id="A14" runat="server" href="~/newhr/jd.aspx">JD</a></li>

        <li id="Li3" runat="server"><a id="A7" runat="server" href="~/newhr/Health.aspx">Health</a></li>
        <li id="Li4" runat="server"><a id="A8" runat="server" href="~/newhr/DocumentListing.aspx">
            Documents</a></li>
        <li id="Li5" runat="server"><a id="A9" runat="server" href="~/newhr/Family.aspx">Family</a></li>
        <li id="Li6" runat="server"><a id="A10" runat="server" href="~/newhr/Identification.aspx">
            Identification</a></li>
        <li id="Li8" runat="server"><a id="A2" runat="server" href="~/newhr/PreviousEmployment.aspx"
            title="Work Experience">Experience</a></li>
        <li id="Li9" runat="server"><a id="A3" runat="server" href="~/newhr/EducationTraining.aspx">
            Education</a></li>
        <li id="Li10" runat="server"><a id="A1" runat="server" href="~/newhr/EmployeeLeave.aspx">
            Leaves</a></li>
        <li id="LiPayroll" runat="server"><a id="A11" runat="server" href="~/newhr/EmployeePayroll.aspx">
            Payroll</a></li>
        <li id="LiReward" title="Reward or Addition Income" runat="server"><a id="A4" runat="server"
            href="~/newhr/EmployeeGradeReward.aspx">Addition</a></li>
        <li id="liAdditionalHR" runat="server"><a id="A12" runat="server" href="~/newhr/OtherHRDetails.aspx">
            Other</a></li>
    </ul>
    <br>
</div>
<script type="text/javascript">

    $(document).ready(function () {

        $('#hrmasterPageCollapsed').click(function () {
            var className = $('#masterPageCollapsed').attr('class');

            if (className == 'headerwrapper') {
                $('.contentpanel').append('<style>.contentpanel:before{width:375px !important;}</style>');
            }
            else {
                $('.contentpanel').append('<style>.contentpanel:before{width:210px !important;}</style>');
            }
        });
    });

</script>
