﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UPublicationCtrl.ascx.cs" Inherits="Web.NewHR.UserControls.UPublicationCtrl" %>

<%@ Register Src="~/NewHR/UserControls/PublicationPopupCtrl.ascx" TagName="PublicationPopup" TagPrefix="uc" %>

 <script type="text/javascript">

     var preparePub = function (grid, toolbar, rowIndex, record) {
         var btnEditDel = toolbar.items.get(0);

         if (record.data.IsEditable == 0) {
             btnEditDel.setVisible(false);
         }
     }

     function reloadPublicGrid() {
        <%= btnReloadGrid.ClientID %>.fireEvent('click');
     }

    </script>

<ext:Hidden runat="server" ID="hdnPublicationID">
</ext:Hidden>

<ext:LinkButton ID="btnReloadGrid" runat="server" Hidden="true">
    <DirectEvents>
        <Click OnEvent="btnReloadGrid_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:LinkButton>

<div class="widget-body">
        <table class="fieldTable firsttdskip">
            <tr>
                <td>
                    <ext:GridPanel ID="GridPublication" runat="server" Width="1000" Scroll="None">
                        <Store>
                            <ext:Store ID="StorePublication" runat="server">
                                <Model>
                                    <ext:Model ID="Model1" runat="server" IDProperty="PublicationId">
                                        <Fields>
                                            <ext:ModelField Name="PublicationName" Type="string" />
                                            <ext:ModelField Name="PublicationTypeName" Type="String" />
                                            <ext:ModelField Name="Publisher" Type="string" />
                                            <ext:ModelField Name="Country" Type="string" />
                                            <ext:ModelField Name="Year" Type="string" />
                                            <ext:ModelField Name="IsEditable" Type="Int" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:Column ID="Column1" runat="server" Text="Publication Name" DataIndex="PublicationName" Wrap="true"
                                    Flex="1" Width="200" />
                                <ext:Column ID="Column2" runat="server" Text="Publication Type" DataIndex="PublicationTypeName" Wrap="true"
                                    Width="150">
                                </ext:Column>
                                <ext:Column ID="Column3" runat="server" Text="Publisher" DataIndex="Publisher" Width="180" Wrap="true">
                                </ext:Column>
                                <ext:Column ID="Column4" runat="server" Text="Country" DataIndex="Country" Flex="1" Width="120" Wrap="true">
                                </ext:Column>
                                <ext:Column ID="Column6" runat="server" Text="Year" DataIndex="Year"  Width="70" Wrap="true">
                                </ext:Column>
                                <ext:CommandColumn ID="CommandColumn1" runat="server" Width="40">
                                    <Commands>
                                        <ext:GridCommand Cls="editGridButton" ToolTip-Text="Edit"  Text="<i class='fa fa-pencil'></i>" CommandName="Edit" />                                       
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridPublication_Command">
                                            <EventMask ShowMask="true">
                                            </EventMask>
                                            <ExtraParams>
                                                <ext:Parameter Name="ID" Value="record.data.PublicationId" Mode="Raw">
                                                </ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw">
                                                </ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                    <PrepareToolbar Fn="preparePub" />
                                </ext:CommandColumn>
                                <ext:CommandColumn ID="CommandColumn2" runat="server" Width="40">
                                    <Commands>
                                        <ext:GridCommand Cls="editGridButton" ToolTip-Text="Delete"  Text="<i class='fa fa-trash-o'></i>"
                                            CommandName="Delete" />
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridPublication_Command">
                                            <EventMask ShowMask="true">
                                            </EventMask>
                                            <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the record?" />
                                            <ExtraParams>
                                                <ext:Parameter Name="ID" Value="record.data.PublicationId" Mode="Raw">
                                                </ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw">
                                                </ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                    <PrepareToolbar Fn="preparePub" />
                                </ext:CommandColumn>
                                 <ext:Column ID="Column5" runat="server" Text=""  Width="200">
                                </ext:Column>
                            </Columns>
                        </ColumnModel>
                        <SelectionModel>
                            <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                        </SelectionModel>
                    </ext:GridPanel>
                </td>
            </tr>
        </table>
        <div class="buttonBlockSection">
            <ext:Button runat="server"  ID="LinkButton1" Cls="btn btn-primary btn-sect" runat="server" Width="120" StyleSpec="margin-top:0px" Height="30"
             Text="<i></i>Add Publication">
                <DirectEvents>
                    <Click OnEvent="btnAddNewLine_Click">
                        <EventMask ShowMask="true" />
                    </Click>
                </DirectEvents>
            </ext:Button>
        </div>
    </div>

<ext:Window ID="AEPublicationWindow" runat="server" Title="Add/Edit Publication"
    Icon="Application" Height="390" Width="450" 
    BodyPadding="5" Hidden="true" Modal="true">
    <Content>
        <uc:PublicationPopup Id="ucPublication" runat="server" />
    </Content>
</ext:Window>