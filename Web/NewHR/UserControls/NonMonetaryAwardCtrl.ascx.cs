﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;
using BLL.Base;
using Ext.Net;

namespace Web.NewHR.UserControls
{
    public partial class NonMonetaryAwardCtrl : BaseUserControl
    {
        public bool ShowEmpty = false;

        private bool _isDisplayMode = false;
        public bool IsDisplayMode
        {
            set
            {
                if (value == true)
                {
                    _isDisplayMode = value;
                    btnAddNewLine.Visible = false;
                    WNMAward.Visible = false;
                    CommandColumn21.Visible = false;
                    CommandColumn2.Visible = false;
                    gridNMAward.Width = 920;
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        private void HideButtonBlock()
        {
            buttonBlock.Visible = false;
        }

        private void Initialise()
        {
            ClearFields();
            BindGrid();
        }

        private void ClearFields()
        {
            txtAwardName.Text = "";
            txtAwardedBy.Text = "";
            txtAwardedDate.Text = "";
            txtDescription.Text = "";
            btnSave.Text = Resources.Messages.Save;
        }

        private void BindGrid()
        {
            int EmployeeID = GetEmployeeID();
            List<NonMonetaryAward> list = NewHRManager.GetNonMonetaryAwardByEmployeeId(EmployeeID);

            if (list.Count > 0)
            {
                gridNMAward.Store[0].DataSource = list;
                gridNMAward.Store[0].DataBind();
                gridNMAward.Show();
            }
            else
                gridNMAward.Hide();
        }

        protected void btnAddNewLine_Click(object sender, DirectEventArgs e)
        {
            ClearFields();
            hdnAwardId.Text = "";
            WNMAward.Show();
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveUpdateNMAward");
            if (Page.IsValid)
            {
                NonMonetaryAward obj = new NonMonetaryAward();
                if (!string.IsNullOrEmpty(hdnAwardId.Text))
                    obj.AwardId = int.Parse(hdnAwardId.Text);

                obj.EmployeeId = GetEmployeeID();
                obj.AwardName = txtAwardName.Text.Trim();

                if(!string.IsNullOrEmpty(txtAwardedBy.Text.Trim()))
                    obj.AwardedBy = txtAwardedBy.Text.Trim();

                if (!string.IsNullOrEmpty(txtAwardedDate.Text.Trim()))
                {
                    obj.AwardDate = txtAwardedDate.Text.Trim();
                    obj.AwardDateEng = BLL.BaseBiz.GetEngDate(obj.AwardDate, IsEnglish);
                }

                if (!string.IsNullOrEmpty(txtDescription.Text.Trim()))
                    obj.Description = txtDescription.Text.Trim();

                Status status = NewHRManager.SaveUpdateNMAward(obj);
                if (status.IsSuccess)
                {
                    WNMAward.Close();

                    if (string.IsNullOrEmpty(hdnAwardId.Text))
                        NewMessage.ShowNormalMessage("Record saved successfully.");
                    else
                        NewMessage.ShowNormalMessage("Record updated successfully.");

                    BindGrid();
                }
                else
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
            }

        }

        protected int GetEmployeeID()
        {
            int EmpID = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                EmpID = int.Parse(Request.QueryString["ID"]);
            }
            else
            {
                EmpID = SessionManager.CurrentLoggedInEmployeeId;
            }
            return EmpID;
        }


        protected void gridNMAward_Command(object sender, DirectEventArgs e)
        {
            string commandName = e.ExtraParams["command"];
            int awardId = int.Parse(e.ExtraParams["ID"]);

            switch (commandName)
            {
                case "Delete":
                    this.DeleteAward(awardId);
                    break;
                case "Edit":
                    this.EditNMAward(awardId);
                    break;
            }

        }

        private void DeleteAward(int awardId)
        {
            Status status = NewHRManager.DeleteNMAward(awardId);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Record deleted successfully.");
                BindGrid();
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        private void EditNMAward(int awardId)
        {
            NonMonetaryAward obj = NewHRManager.GetNonMonetaryAwardById(awardId);
            if (obj != null)
            {
                txtAwardName.Text = obj.AwardName;

                if (!string.IsNullOrEmpty(obj.AwardedBy))
                    txtAwardedBy.Text = obj.AwardedBy;

                if (!string.IsNullOrEmpty(obj.AwardDate))
                    txtAwardedDate.Text = obj.AwardDate;

                if (obj.Description != null)
                    txtDescription.Text = obj.Description;

                hdnAwardId.Text = obj.AwardId.ToString();
                btnSave.Text = Resources.Messages.Update;
                WNMAward.Show();
            }

        }
    }
}