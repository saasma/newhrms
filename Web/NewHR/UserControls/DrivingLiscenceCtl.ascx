﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DrivingLiscenceCtl.ascx.cs"
    Inherits="Web.NewHR.UserControls.DrivingLiscenceCtl" %>
<ext:Hidden runat="server" ID="hdnDrivingLiscenceID">
</ext:Hidden>
<ext:Hidden runat="server" ID="hdnIsEmployee" />

<div style="width:823px; margin-left:-19px">
    <div>
        <!-- panel-btns -->
        <h4 class="panel-title sectionHeading">
            <i></i>Driving Licence
        </h4>
    </div>
    <!-- panel-heading -->
    <div class="panel-body">
        <table class="fieldTable firsttdskip">
            <tr>
                <td>
                    <ext:GridPanel ID="GridDrivingLiscence" runat="server" Width="760" Cls="itemgrid">
                        <Store>
                            <ext:Store ID="StoreDrivingLiscence" runat="server">
                                <Model>
                                    <ext:Model ID="Model1" runat="server" IDProperty="DrivingLicenceId">
                                        <Fields>
                                            <ext:ModelField Name="LiscenceTypeName" />
                                            <ext:ModelField Name="DrivingLicenceNo" />
                                            <ext:ModelField Name="IssuingCountry" />
                                            <ext:ModelField Name="Status" Type="Int" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:Column ID="Column1" runat="server" Text="Type" DataIndex="LiscenceTypeName"
                                    Width="150" Flex="1" />
                                <ext:Column ID="Column2" runat="server" Text="Liscence Number" DataIndex="DrivingLicenceNo"
                                    Width="150">
                                </ext:Column>
                                <ext:Column ID="Column3" runat="server" Text="Issuing Country" DataIndex="IssuingCountry"
                                    Width="200">
                                </ext:Column>
                                <ext:CommandColumn ID="CommandColumn1" runat="server" Width="40">
                                    <Commands>                                
                                        <ext:CommandSeparator />
                                        <ext:GridCommand Icon="PageWhitePut" CommandName="DownLoad">
                                            <ToolTip Text="DownLoad" />
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridDrivingLiscence_Command">
                                            <ExtraParams>
                                                <ext:Parameter Name="ID" Value="record.data.DrivingLicenceId" Mode="Raw">
                                                </ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw">
                                                </ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>

                                <ext:CommandColumn ID="CommandColumn3" runat="server" Width="40">
                                    <Commands>
                                        <ext:CommandSeparator />
                                        <ext:GridCommand Cls="editGridButton" ToolTip-Text="Edit" Text="<i class='fa fa-pencil'></i>"
                                            CommandName="Edit" />  
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridDrivingLiscence_Command">
                                            <ExtraParams>
                                                <ext:Parameter Name="ID" Value="record.data.DrivingLicenceId" Mode="Raw">
                                                </ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw">
                                                </ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                    <PrepareToolbar Fn="prepareDrivingLic" />
                                </ext:CommandColumn>

                                <ext:CommandColumn ID="CommandColumn2" runat="server" Width="40">
                                    <Commands>
                                        <ext:CommandSeparator />
                                        <ext:GridCommand Cls="editGridButton" ToolTip-Text="Delete" Text="<i class='fa fa-trash-o'></i>"
                                            CommandName="Delete" />
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridDrivingLiscence_Command">
                                            <ExtraParams>
                                                <ext:Parameter Name="ID" Value="record.data.DrivingLicenceId" Mode="Raw">
                                                </ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw">
                                                </ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the record?" />
                                        </Command>
                                    </DirectEvents>
                                    <PrepareToolbar Fn="prepareDrivingLic" />
                                </ext:CommandColumn>

                            </Columns>
                        </ColumnModel>
                        <SelectionModel>
                            <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                        </SelectionModel>
                    </ext:GridPanel>
                </td>
            </tr>
        </table>
        <div class="buttonBlockSection">
            <ext:Button runat="server"  ID="LinkButton1" Cls="btn btn-primary btn-sect" Width="120" StyleSpec="margin-top:10px"
                Height="30" Text="<i></i>Add New Line">
                <DirectEvents>
                    <Click OnEvent="btnAddNewLine_Click">
                        <EventMask ShowMask="true" />
                    </Click>
                </DirectEvents>
            </ext:Button>
        </div>
    </div>
</div>


<ext:Window ID="AEDrivingLiscenceWindow" runat="server" Title="Driving Liscence Details"
    Icon="Application" Height="420" Width="500" 
    BodyPadding="5" Hidden="true" Modal="true">
    <Content>
        <ext:Label ID="lblMsg" runat="server" />
        <table class="fieldTable">
            <tr>
                <td>
                    <span style="padding-bottom: 50px; font-weight: bold">Driving Licence Type</span>
                    <ext:Checkbox ID="chkTwoWheeler" runat="server" FieldLabel="Two Wheeler" LabelAlign="Left"
                        LabelSeparator="">
                    </ext:Checkbox>
                    <ext:Checkbox ID="chkFourWheeler" runat="server" FieldLabel="Four Wheeler" LabelAlign="Left"
                        LabelSeparator="">
                    </ext:Checkbox>
                    <ext:Checkbox ID="chkHeavy" runat="server" FieldLabel="Heavy Vehicle" LabelAlign="Left" LabelSeparator="">
                    </ext:Checkbox>
                    <%--  <ext:MultiCombo  runat="server" Width="260" ID="cmbLiscenceType" LabelAlign="Top" LabelSeparator="" FieldLabel="Type">
                                    <Items>
                                        <ext:ListItem Text="Two Wheeler" Value="1" />
                                        <ext:ListItem Text="Four Wheeler" Value="2" />
                                        <ext:ListItem Text="Heavy" Value="3" />
                                    </Items>
                                </ext:MultiCombo>--%>
                </td>
            </tr>
            <tr>
                <td>
                    <ext:TextField ID="txtDrivingLiscenceNumber" runat="server" FieldLabel="Driving Licence Number"
                        Width="180px" LabelAlign="top" LabelSeparator="" />
                    <asp:RequiredFieldValidator Display="None" ID="valtxtDrivingLiscenceNumber" runat="server"
                        ValidationGroup="DrivingLiscenceSaveUpdate" ControlToValidate="txtDrivingLiscenceNumber"
                        ErrorMessage="Please enter Driving Liscence Number." />
                </td>
            </tr>
            <tr>
                <td style="padding-top: 5px;">
                    <ext:ComboBox ID="cmbIssuingCountry" Width="180px" runat="server" ValueField="CountryName"
                        DisplayField="CountryName" FieldLabel="Issuing Country" LabelAlign="top" LabelSeparator=""
                        ForceSelection="true" QueryMode="Local">
                        <Store>
                            <ext:Store ID="Store2" runat="server">
                                <Model>
                                    <ext:Model ID="Model4" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="CountryName" />
                                            <ext:ModelField Name="CountryName" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                    <asp:RequiredFieldValidator Display="None" ID="valcmbIssuingCountry" runat="server"
                        ValidationGroup="DrivingLiscenceSaveUpdate" ControlToValidate="cmbIssuingCountry"
                        ErrorMessage="Issuing Country is required." />
                </td>
            </tr>
            <tr>
                <td>
                    <ext:FileUploadField ID="FileDrivingLiscenceDocumentUpload" runat="server" Width="200"
                        Icon="Attach" FieldLabel="&nbsp;" LabelAlign="Top" LabelSeparator="" />
                    <%--<span style="color: #ACACAC; padding-top: 20px;">*(only doc,pdf,txt,docx,rar)</span>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationExpression="^.*\.(doc|DOC|pdf|PDF|txt|TXT|docx|DOCX|rar|RAR)$"
                        runat="server" ValidationGroup="DrivingLiscenceSaveUpdate" Display="None" ControlToValidate="FileDrivingLiscenceDocumentUpload"
                        ErrorMessage="Invalid file format."></asp:RegularExpressionValidator>--%>
                </td>
                <td valign="bottom">
                    <table>
                        <tr>
                            <td>
                                <ext:Label ID="lblUploadedFile" runat="server" Hidden="true">
                                </ext:Label>
                            </td>
                            <td>
                                <ext:LinkButton Icon="Delete" runat="server" ID="lnkDeleteFile" OnDirectClick="lnkDeleteFile_Click"
                                    Text="Delete file" Hidden="true">
                                    <DirectEvents>
                                        <Click OnEvent="lnkDeleteFile_Click">
                                            <Confirmation ConfirmRequest="true" Message="Do you want to delete the file?" />
                                            <EventMask ShowMask="true" />
                                        </Click>
                                    </DirectEvents>
                                </ext:LinkButton>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td valign="bottom" colspan="2">
                    <div class="popupButtonDiv">
                        <ext:Button runat="server"  ID="btnSave" Cls="btn btn-primary" Text="<i></i>Save">
                            <DirectEvents>
                                <Click OnEvent="btnSave_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                            <Listeners>
                                <Click Handler="valGroup = 'DrivingLiscenceSaveUpdate'; return CheckValidation();">
                                </Click>
                            </Listeners>
                        </ext:Button>
                        <div class="btnFlatOr">
                            or</div>
                        <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="LinkButton2" Cls="btnFlatLeftGap"
                            Text="<i></i>Cancel">
                            <Listeners>
                                <Click Handler="#{AEDrivingLiscenceWindow}.hide();">
                                </Click>
                            </Listeners>
                        </ext:LinkButton>
                    </div>
                </td>
            </tr>
        </table>
    </Content>
</ext:Window>


<script type="text/javascript">

    var prepareDrivingLic= function (grid, toolbar, rowIndex, record) {
        var delBtn = toolbar.items.get(1);
        var IsEmployee = <%= hdnIsEmployee.ClientID %>.getValue();

        if (record.data.Status == 1 && IsEmployee == "1" ) {
            delBtn.setVisible(false);
        }

    } 

</script>