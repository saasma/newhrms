﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployeeContractTabStrip.ascx.cs"
    Inherits="Web.NewHR.UserControls.EmployeeContractTabStrip" %>
<style type="text/css">
    .excel
    {
        width: 146px;
        height: 28px;
        font: bold 11px/28px Arial;
        background: url("../css/images/excel.png") no-repeat left center;
        color: #0D83DD;
        text-align: center;
        border: 0px;
        cursor: pointer;
        margin-left: 10px;
        padding-left: 3px;
    }
</style>
<div id="rootwizard" class="wizard" style="margin-bottom: 15px" runat="server">
    <div class="wizard-head">
        <ul class="nav nav-justified nav-wizard nav-wizard-success" runat="server" id="list">
            <li runat="server"><a runat="server" href="~/newhr/EmployeeContractExpiring.aspx?ID=1&type=thismonth">
                Contract Expiring</a></li>
            <li runat="server"><a runat="server" href="~/newhr/EmployeeRetiring.aspx?ID=1&type=thismonth">
                Retiring</a></li>
            <%-- <li runat="server"><a runat="server" href="~/newhr/ContactInformation.aspx" >
                Contact</a></li>--%>
            <li runat="server"><a runat="server" href="~/newhr/EmployeePeriodSixMonthComplete.aspx?ID=1&type=thismonth">
                6 Month Complete</a></li>
            <li runat="server"><a runat="server" href="~/newhr/EmployeePeriodYearComplete.aspx?ID=1&type=thismonth">
                1 Year Complete</a></li>
            <li runat="server"><a runat="server" href="~/newhr/EmployeeNewJoinees.aspx?ID=1&type=thismonth">
                New Joinees</a></li>
            <%--<li runat="server"><a id="A1" runat="server" href="~/newhr/Photograph.aspx" >
                Photo</a></li>--%>
        </ul>
    </div>
</div>
