﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Entity;
using BLL;
using System.IO;
using Utils;
using System.Text;
using BLL.BO;

namespace Web.NewHR.UserControls
{
    public partial class DocPartyCtrl : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void AddNewDocParty(string IsDocPartyListPage)
        {
            btnPartySave.Text = "Save";
            txtName.Text = "";
            hdnPartyID.Text = "";
            hdnIsDocPartyPage.Text = IsDocPartyListPage;

            WDocParty.Center();
            WDocParty.Show();
        }

        protected void btnPartySave_Click(object sender, DirectEventArgs e)
        {
            this.Page.Validate("SaveUpdateParty");
            if (this.Page.IsValid)
            {
                DocParty objDocParty = new DocParty();
                bool IsSave = true;

                if (!string.IsNullOrEmpty(hdnPartyID.Text))
                {
                    objDocParty.PartyID = new Guid(hdnPartyID.Text.Trim());
                    IsSave = false;
                }
                else
                    objDocParty.PartyID = Guid.NewGuid();

                objDocParty.Name = txtName.Text.Trim();

                Status status = DocumentManager.SaveUpdateDocParty(objDocParty, IsSave);
                if (status.IsSuccess)
                {
                    if (hdnIsDocPartyPage.Text == "1")
                    {
                        if (!string.IsNullOrEmpty(hdnPartyID.Text))
                            NewMessage.ShowNormalMessage("Record updated successfully.", "reloadPartyGrid");
                        else
                            NewMessage.ShowNormalMessage("Record saved successfully.", "reloadPartyGrid");
                    }
                    else
                    {
                        X.Js.Call("ReloadPartyCombo", objDocParty.PartyID.ToString());
                    }

                    WDocParty.Close();
                }
                else
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
            }    
        }

        public void EditDocParty(Guid partyID)
        {
            DocParty objDocParty = DocumentManager.GetDocPartyById(partyID);
            if (objDocParty != null)
            {
                hdnPartyID.Text = partyID.ToString();
                hdnIsDocPartyPage.Text = "1";
                txtName.Text = objDocParty.Name;

                btnPartySave.Text = "Update";
                WDocParty.Center();
                WDocParty.Show();
            }
        }



    }
}