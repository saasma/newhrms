﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;

using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;

namespace Web.NewHR.UserControls
{
    public partial class LanguageSetsCtl : BaseUserControl
    {
        private bool _isDisplayMode = false;
        public bool IsDisplayMode
        {
            set
            {
                if (value == true)
                {
                    _isDisplayMode = value;
                    btnAddNewLine.Visible = false;
                    AELanguageSetsWindow.Visible = false;
                    CommandColumn1.Visible = false;
                }
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();

            }
        }
        protected void Initialise()
        {
            int EmployeeID = GetEmployeeID();
            this.LoadLanguageSetsGrid(EmployeeID);




            LoadLanguage();


        }

        private void LoadLanguage()
        {
            cmbLanguage.Store[0].DataSource = NewHRManager.Instance.GetAllLanguageSet();
            cmbLanguage.Store[0].DataBind();
        }
        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            //validation 
            if (!string.IsNullOrEmpty(cmbLanguage.SelectedItem.Text) && txtNewLanguage.Text.Trim() != "")
            {
                NewMessage.ShowWarningMessage("fill either Language or new Language !");
                return;
            }
            else if((chkAddtoLanguagePool.Checked==true) && (txtNewLanguage.Text.Trim()==""))
            {
                NewMessage.ShowWarningMessage("please enter new Language !");
                return;
            }
            else if (txtNewLanguage.Text.Trim()!= ""  && (chkAddtoLanguagePool.Checked==false))
            {
                NewMessage.ShowWarningMessage("plese check mark for Add to Language Pool.");
                return;
            }
            else if (!string.IsNullOrEmpty(cmbLanguage.SelectedItem.Value))
            {
                if (NewHRManager.IsDuplicateLanguageByEmpID(int.Parse(cmbLanguage.SelectedItem.Value), GetEmployeeID()))
                {
                    NewMessage.ShowWarningMessage("This Language already exists for this employee.");
                    return;
                }
            }
            else if (!string.IsNullOrEmpty(txtNewLanguage.Text.Trim()))
            {
                if (NewHRManager.IsDuplicateLanguageInPool(txtNewLanguage.Text.Trim()))
                {
                    NewMessage.ShowWarningMessage("This Language already exists in skill pool.");
                    return;
                }
            }


            //-------------
            this.SaveFormData();

        }
        private void SaveFormData()
        {

            Status myStatus = new Status();
            LanguageSetEmployee _LanguageSetEmployee = new LanguageSetEmployee();
            _LanguageSetEmployee.EmployeeId = this.GetEmployeeID();
            bool SaveToPool = false;
            string NewLanguagePool = string.Empty;
         
            bool isSave = bool.Parse(this.hdnLanguageSetsID.Text == "" ? "true" : "false");
            if (!isSave)
                _LanguageSetEmployee.LanguageSetId = int.Parse(this.hdnLanguageSetsID.Text);

            if (!string.IsNullOrEmpty(cmbLanguage.SelectedItem.Text))
            {
                _LanguageSetEmployee.LanguageSetId = int.Parse(cmbLanguage.SelectedItem.Value);
            }
            else
            {
                if (chkAddtoLanguagePool.Checked == true)
                {
                    SaveToPool = true;
                    NewLanguagePool = txtNewLanguage.Text.Trim();
                }
            }
            if (!string.IsNullOrEmpty(cmbFluencySpeak.SelectedItem.Text))
            {
                _LanguageSetEmployee.FluencySpeakID = int.Parse(cmbFluencySpeak.SelectedItem.Value);
                _LanguageSetEmployee.FluencySpeakName = cmbFluencySpeak.SelectedItem.Text;
            }
            if (!string.IsNullOrEmpty(cmbFluencyWrite.SelectedItem.Text))
            {
                _LanguageSetEmployee.FluencyWriteID = int.Parse(cmbFluencyWrite.SelectedItem.Value);
                _LanguageSetEmployee.FluencyWriteName = cmbFluencyWrite.SelectedItem.Text;
            }

            _LanguageSetEmployee.IsNativeLanguage = chkIsNativeLanguage.Checked;

            myStatus = NewHRManager.Instance.InsertUpdateLanguageSets(_LanguageSetEmployee, isSave, SaveToPool, NewLanguagePool, _LanguageSetEmployee.EmployeeId);

            if (chkAddtoLanguagePool.Checked)
                LoadLanguage();

            if (myStatus.IsSuccess)
            {
                if (isSave)
                    NewMessage.ShowNormalMessage("Record Save successfully");
                else
                    NewMessage.ShowNormalMessage("Record Updated successfully");

                //  this.ClearFields();
                this.AELanguageSetsWindow.Close();
                this.LoadLanguageSetsGrid(this.GetEmployeeID());

            }
            else
                NewMessage.ShowWarningMessage(myStatus.ErrorMessage);
        }
        protected void LoadLanguageSetsGrid(int EmployeeID)
        {

            List<NewHREmployeeLanguageSetBO> _EmployeeLanguageSetBO = NewHRManager.GetEmployeesLanguageSet(EmployeeID);
            //List<LanguageSetEmployee> _LanguageSetEmployee = NewHRManager.GetEmployeesLanguageSet(EmployeeID);



            this.StoreLanguageSets.DataSource = _EmployeeLanguageSetBO;
            this.StoreLanguageSets.DataBind();
            if (_isDisplayMode && _EmployeeLanguageSetBO.Count <= 0)
                GridLanguageSets.Hide();

        }
        protected int GetEmployeeID()
        {
            //Guid? AssetID = null;
            int EmpID = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                EmpID = int.Parse(Request.QueryString["ID"]);
            }
            else
            {
                EmpID = SessionManager.CurrentLoggedInEmployeeId;
            }
            return EmpID;
        }
        protected void btnAddNewLine_Click(object sender, DirectEventArgs e)
        {
            this.ClearFields();
            this.AELanguageSetsWindow.Show();

        }
        private object[] LanguageSetsFillData
        {
            get
            {
                return new object[]
            {
                new object[] { "", "", "",""},
               
            };
            }
        }
        protected void GridLanguageSets_Command(object sender, DirectEventArgs e)
        {
            string commandName = e.ExtraParams["command"];
            int LanguageSetsID = int.Parse(e.ExtraParams["ID"]);
            LanguageSetEmployee _LanguageSetEmployee = NewHRManager.GetLanguageSetsDetailsById(int.Parse(e.ExtraParams["ID"]), GetEmployeeID());
            switch (commandName)
            {
                case "Delete":
                    this.DeleteData(LanguageSetsID);
                    break;
             
                case "Edit":
                    this.editLanguageSets(LanguageSetsID);
                    break;
            }

        }
        protected void DeleteData(int ID)
        {

            bool result = NewHRManager.DeleteLanguageSetsByID(ID,GetEmployeeID());
            this.LoadLanguageSetsGrid(this.GetEmployeeID());
          
        }
        protected void ClearFields()
        {
            this.hdnLanguageSetsID.Text = "";
            txtNewLanguage.Text = "";
            cmbLanguage.Value = "";
            cmbFluencyWrite.Value = "";
            cmbFluencySpeak.Value = "";
            chkAddtoLanguagePool.Checked = false;
            chkIsNativeLanguage.Checked = false;
            cmbLanguage.Disabled = false;
            txtNewLanguage.Disabled = false;
            chkAddtoLanguagePool.Disabled = false;
            btnSave.Text = Resources.Messages.Save;
        }
        public void editLanguageSets(int LanguageSetsID)
        {
            LanguageSetEmployee _LanguageSetEmployee = NewHRManager.GetLanguageSetsDetailsById(LanguageSetsID, GetEmployeeID());
            this.hdnLanguageSetsID.Text = LanguageSetsID.ToString();
            cmbLanguage.Value=_LanguageSetEmployee.LanguageSetId;
            cmbFluencySpeak.SetValue(_LanguageSetEmployee.FluencySpeakID.ToString());
            cmbFluencyWrite.SetValue(_LanguageSetEmployee.FluencyWriteID.ToString());
            if (_LanguageSetEmployee.IsNativeLanguage.Value)
                chkIsNativeLanguage.Checked = true;
            else
                chkIsNativeLanguage.Checked = false;
            btnSave.Text = Resources.Messages.Update;
            cmbLanguage.Disabled = true;
            txtNewLanguage.Disabled = true;
            chkAddtoLanguagePool.Disabled = true;
            this.AELanguageSetsWindow.Show();
        }

    }
}