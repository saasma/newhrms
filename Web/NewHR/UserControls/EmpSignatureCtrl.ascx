﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmpSignatureCtrl.ascx.cs" Inherits="Web.NewHR.UserControls.EmpSignatureCtrl" %>


 <script type="text/javascript">
     function myFunction1() {
         //some code here
         $("#popupdiv1").dialog({
             modal: true,
             draggable: false,
             resizable: false,
             width: 'auto',
             height: 'auto',
             position: ['center', 'center'],
             show: 'blind',
             hide: 'blind',
             dialogClass: 'ui-dialog-osx',
             buttons: {
                 "Close": function () {
                     $(this).dialog("close");
                 }
             }
         });
     }
    </script>


<div style="margin-left:20px;">
    <h4>Signature</h4>
</div>

<table>
    <tr>
        <td style="padding-left: 10px; padding-top: 10px;">
            <div style="width: 180px; height: 180px;">
                <ext:Image ID="ImgEmployee" runat="Server" Width="180" Height="180">
                </ext:Image>
            </div>
        </td>
    </tr>
    <tr>
        <td style="padding-left: 10px; padding-top: 10px;">
            <ext:FileUploadField ID="fupSignature" runat="server" ButtonText="Browse" Icon="Add"
                Width="300" Visible="true" ButtonOffset="1">
                <DirectEvents>
                    <Change OnEvent="btnUpload_Click" />
                </DirectEvents>
            </ext:FileUploadField>
        </td>
    </tr>
</table>

<br />
<br />


<div style="margin-left:20px;">
<ext:Image ID="ImgSignatureThumbprint" runat="Server" Width="90" Height="90">
                </ext:Image>
           </div>


<div id="popupdiv1" title="" style="display: none; background-color:Silver;">
<asp:Image ID="Image11" runat="server" />
    <br />

    <ext:Button runat="server" Text="Crop" ID="btnCropImage1" Cls="bluebutton left">
                        <DirectEvents>
                            <Click OnEvent="btnCropImage1_Click">
                                <EventMask ShowMask="true" />
                            </Click>
                        </DirectEvents>
                        <Listeners>
                            
                        </Listeners>
                    </ext:Button>
</div>


<input type="hidden" runat="server" id="_xField1" />
    <input type="hidden" runat="server" id="_yField1" />
    <input type="hidden" runat="server" id="_widthField1" />
    <input type="hidden" runat="server" id="_heightField1" />


    <script type="text/javascript">

        function hideDiv() {
            document.getElementById('popupdiv1').style.display = "none";
        }

        var editorID1 = '<%= Image11.ClientID %>';

        jQuery(function () {
            jQuery('#' + editorID1).Jcrop({
                onChange: showCoords1,
                onSelect: showCoords1,
                aspectRatio: 1
            });
        });

        function showCoords1(c) {

            var xField1 = document.getElementById('<%= _xField1.ClientID %>');
            var yField1 = document.getElementById('<%= _yField1.ClientID %>');
            var widthField1 = document.getElementById('<%= _widthField1.ClientID %>');
            var heightField1 = document.getElementById('<%= _heightField1.ClientID %>');

            xField1.value = c.x;
            yField1.value = c.y;
            widthField1.value = c.w;
            heightField1.value = c.h;
        }


  
    
    </script>
