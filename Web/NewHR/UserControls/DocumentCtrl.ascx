﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DocumentCtrl.ascx.cs"
    Inherits="Web.NewHR.UserControls.DocumentCtrl" %>
<%@ Register Src="~/NewHR/UserControls/DocumentPopupCtrl.ascx" TagName="docPopup"
    TagPrefix="ucDoc" %>
<ext:Hidden runat="server" ID="hiddenValue" />
<ext:Hidden runat="server" ID="hdnIsEmployee" />
<ext:LinkButton ID="btnReloadDoc" runat="server" Hidden="true">
    <DirectEvents>
        <Click OnEvent="btnReloadDoc_Click">
        </Click>
    </DirectEvents>
</ext:LinkButton>
<ext:Button ID="btnDeleteLevel" runat="server" Hidden="true">
    <DirectEvents>
        <Click OnEvent="btnDeleteLevel_Click">
            <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the document?" />
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:Button>
<ext:LinkButton ID="btnDownload" runat="server" Hidden="true">
    <DirectEvents>
        <Click OnEvent="btnDownload_Click">
        </Click>
    </DirectEvents>
</ext:LinkButton>
<div class="" style="width: 940px">
    <div style="margin-left: 20px;">
        <!-- panel-btns -->
        <ext:GridPanel ID="GridLevels" runat="server" Cls="itemgrid" Width="1000">
            <Store>
                <ext:Store ID="Store3" runat="server">
                    <Model>
                        <ext:Model ID="Model4" runat="server" IDProperty="DocumentId">
                            <Fields>
                                <ext:ModelField Name="DocumentId" Type="String" />
                                <ext:ModelField Name="EmployeeId" Type="string" />
                                <ext:ModelField Name="Description" Type="string" />
                                <ext:ModelField Name="Name" Type="string" />
                                <ext:ModelField Name="Url" Type="string" />
                                <ext:ModelField Name="ContentType" Type="string" />
                                <ext:ModelField Name="Size" Type="string" />
                                <ext:ModelField Name="TypeName" Type="string" />
                                <ext:ModelField Name="Status" Type="Int" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>
            <ColumnModel>
                <Columns>
                    <ext:Column ID="Column8" Sortable="false" MenuDisabled="true" runat="server" Text="File Name"
                        Wrap="true" Align="Left" DataIndex="Name" Width="250">
                    </ext:Column>
                    <ext:Column ID="Column5" Sortable="false" MenuDisabled="true" runat="server" Text="File Description"
                        Wrap="true" Align="Left" DataIndex="Description" Width="250" />
                    <ext:Column ID="Column2" Sortable="false" MenuDisabled="true" runat="server" Text="Document Type"
                        Wrap="true" Align="Left" DataIndex="TypeName" Width="180">
                    </ext:Column>
                    <ext:Column ID="Column1" Sortable="false" MenuDisabled="true" runat="server" Text="Size"
                        Wrap="true" Align="Center" DataIndex="Size" Width="80">
                    </ext:Column>
                    <ext:CommandColumn ID="CommandColumn1" runat="server" Text="Link">
                        <Commands>
                            <ext:GridCommand Text="<i></i>Download" CommandName="DownLoad" />
                        </Commands>
                        <Listeners>
                            <Command Handler="DownloadHandler(command,record);" />
                        </Listeners>
                    </ext:CommandColumn>
                    <ext:CommandColumn ID="CommandColumn2" runat="server" Text="" Width="40">
                        <Commands>
                            <ext:CommandSeparator />
                            <ext:GridCommand Cls="editGridButton" ToolTip-Text="Delete" Text="<i class='fa fa-trash-o'></i>"
                                CommandName="Delete" />
                        </Commands>
                        <Listeners>
                            <Command Handler="CommandHandler(command,record);" />
                        </Listeners>
                        <PrepareToolbar Fn="prepareDocument" />
                    </ext:CommandColumn>
                    <ext:Column ID="Column3" Sortable="false" MenuDisabled="true" runat="server" Text=""
                        Align="Center" Width="200">
                    </ext:Column>
                </Columns>
            </ColumnModel>
            <SelectionModel>
                <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
            </SelectionModel>
        </ext:GridPanel>
        <div class="buttonBlockSection">
            <ext:Button ID="btnAddDoc" Cls="btn btn-primary btn-sect" runat="server" Text="Add New Document">
                <DirectEvents>
                    <Click OnEvent="btnAddLevel_Click">
                        <EventMask ShowMask="true" />
                    </Click>
                </DirectEvents>
            </ext:Button>
        </div>
    </div>
</div>
<ext:Window ID="WDocument" runat="server" Title="Add Document" Icon="Application"
    Width="450" Height="310" BodyPadding="10" Hidden="true" Modal="true">
    <Content>
        <ucDoc:docPopup Id="ucDocument" runat="server" />
    </Content>
</ext:Window>
<script type="text/javascript">

        var CommandHandler = function(command, record){
            <%= hiddenValue.ClientID %>.setValue(record.data.DocumentId);
           <%= btnDeleteLevel.ClientID %>.fireEvent('click');

        };

      var DownloadHandler = function(command, record){
          <%= hiddenValue.ClientID %>.setValue(record.data.DocumentId);
                if(command == "DownLoad"){
                    <%= btnDownload.ClientID %>.fireEvent('click');
               
             }
            };

    var prepareDocument = function (grid, toolbar, rowIndex, record) {
        var delBtn = toolbar.items.get(1);
        var IsEmployee = <%= hdnIsEmployee.ClientID %>.getValue();

        if (record.data.Status == 1 && IsEmployee == "1" ) {
            delBtn.setVisible(false);
        }

    };

     function reloadDocumentGrid()
   {
        <%= btnReloadDoc.ClientID %>.fireEvent('click');
   }
</script>
