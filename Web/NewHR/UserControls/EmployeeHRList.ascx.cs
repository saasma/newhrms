﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using Utils.Web;
using System.Web.UI.MobileControls;
using DAL;
using Utils.Helper;
using BLL.Base;

namespace Web.NewHR.UserControls
{
    public partial class EmployeeHRList : BaseUserControl
    {
        EmployeeManager empMgr = new EmployeeManager();

        public bool IsUserReadonly = false;
        
        #region "Control state related"
        private string _sortBy = "";
        private SortDirection _sortDirection = SortDirection.Ascending;

        protected override void OnInit(EventArgs e)
        {
            Page.RegisterRequiresControlState(this);
            base.OnInit(e);
        }
        protected override void LoadControlState(object savedState)
        {
            object[] rgState = (object[])savedState;
            base.LoadControlState(rgState[0]);
            _sortBy = rgState[1].ToString();
            _sortDirection = (SortDirection)rgState[2];
            //_tempCurrentPage = (int)rgState[2];

        }
        protected override object SaveControlState()
        {
            object[] rgState = new object[3];
            rgState[0] = base.SaveControlState();
            rgState[1] = _sortBy;
            rgState[2] = _sortDirection;
           // rgState[2] = _tempCurrentPage;
            return rgState;
        }
        #endregion


      
        
      
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPagePermission();
          
            if (!IsPostBack)
            {
                if (BLL.BaseBiz.PayrollDataContext.SubDepartments.Any() == false)
                    gvwEmployees.Columns[5].Visible = false;
                if (BLL.BaseBiz.PayrollDataContext.FunctionalTitles.Any() == false)
                    gvwEmployees.Columns[8].Visible = false;
                if (SessionManager.IsCustomRole && SessionManager.User.URole.IsReadOnly != null && SessionManager.User.URole.IsReadOnly.Value)
                {
                    gvwEmployees.Columns[gvwEmployees.Columns.Count - 1].Visible = false;
                }

                filterCtl.ShowDesignationName = true;
                filterCtl.SubDepartment = true;
                filterCtl.Level = true;
                //gvwEmployees.Columns[6].HeaderText = CommonManager.GetLevelName;

                // Disable all emp export/import once salary is saved
                if (SessionManager.User.URole.RoleId == (int)Role.Administrator)
                    btnExportPopup.Visible = true;
                else
                {
                    btnExportPopup.Visible = false;
                }

                if (SessionManager.EmployeeDisplayRecodsPerPage != 0)
                {
                    UIHelper.SetSelectedInDropDown(pagintCtl.DDLRecords, SessionManager.EmployeeDisplayRecodsPerPage.ToString());
                }
                //_tempCurrentPage = 1;
                
                BindEmployees();
                ProcessSectionLevelPermission();

                //if (SessionManager.User.EnablePayrollSignOff != null && SessionManager.User.EnablePayrollSignOff.Value)
                //    liSignOff.Visible = true;

            }

            JavascriptHelper.AttachNonDialogPopUpCode(Page, "employeePopup", "../ExcelWindow/EmployeeExcel.aspx", 450, 500);
            JavascriptHelper.AttachNonDialogPopUpCode(Page, "otherPopup", "../ExcelWindow/OtherEmployeeImport.aspx", 450, 500);
        }

        public void ProcessSectionLevelPermission()
        {
            if (SessionManager.IsCustomRole)
            {
                UUser user = UserManager.GetUserByUserName(Page.User.Identity.Name);
                int currentPageId = UserManager.GetPageIdByPageUrl(CurrentPageUrl);

                btnExportPopup.Visible = false;

                List<UPermissionSection> sections = UserManager.GetAccessibleSectionsForPage(user.RoleId.Value, currentPageId);

                foreach (UPermissionSection section in sections)
                {
                    if (section.PageSectionId == 2)
                    {
                        btnExportPopup.Visible = true;
                    }
                }

            }
        }

        /// <summary>
        /// Set for Custom Role if is in Readonly Mode or Not
        /// </summary>
        public void SetPagePermission()
        {
            if (SessionManager.IsCustomRole && SessionManager.CustomRole.IsReadOnly != null && SessionManager.CustomRole.IsReadOnly.Value)
            {
                //btnAddNew.Visible = false;
                btnExportPopup.Visible = false;
                IsUserReadonly = true;

               
            }

            if (SessionManager.User.URole.RoleId == (int)Role.Administrator)
                btnExportImportAccounts.Visible = true;
            else
                btnExportImportAccounts.Visible = false;
        }

        //public bool IsDeleteButtonVisible(object empId)
        //{
        //    int employeeId = (int)empId;
        //    if (IsUserReadonly)
        //        return false;

        //    return !EmployeeManager.HasEmployeeIncludedInCalculation(employeeId);
        //}

        public void ReBindEmployees()
        {
            pagintCtl.CurrentPage = 1;
            BindEmployees();
        }

        public void BindEmployees()
        {
          

            int totalRecords = 0;
            
            string customRoleDeparmentList = null;
            if (SessionManager.IsCustomRole)
                customRoleDeparmentList = SessionManager.CustomRoleDeparmentIDList;
            bool? showRetiredOnly = null;
            if (ddlRetirementType.SelectedValue != "All")
                showRetiredOnly = bool.Parse(ddlRetirementType.SelectedValue);

            string sort = (_sortBy + " " + _sortDirection).ToLower();

            if (string.IsNullOrEmpty(_sortBy))
                sort = "";

            gvwEmployees.DataSource
                = empMgr.GetAllEmployees(pagintCtl.CurrentPage, int.Parse(pagintCtl.DDLRecords.SelectedValue), ref totalRecords,
                 sort, SessionManager.CurrentCompanyId, filterCtl.EmployeeNameValue.Trim(),
                 filterCtl.EmployeeIDCardNoValue.Trim(), showRetiredOnly,
                filterCtl.BranchId, filterCtl.DepartmentId, filterCtl.DesignationId, filterCtl.LevelId, customRoleDeparmentList
                ,filterCtl.DesignationName,
                filterCtl.SubDepartmentId,filterCtl.PayGroupID,filterCtl.LevelGroupID,filterCtl.StatusIDList
                ,filterCtl.UnitId);

            gvwEmployees.DataBind();

            pagintCtl.UpdatePagingBar(totalRecords);

            //if (_tempCount <= 0)
            //{
            //    pagintCtl.Visible = false;
            //}
            //else
            //{
            //    pagintCtl.Visible = true;
            //}
            //int totalPages = (int)CalculateTotalPages(_tempCount);
            //pagintCtl.LabelCurrentPage.Text = _tempCurrentPage.ToString();
            //pagintCtl.LabelTotalPage.Text = (totalPages).ToString(); 

            //if (_tempCurrentPage <= 1)
            //{
            //    pagintCtl.EnableDisablePreviousButton(false);
            //}
            //else
            //{
            //    pagintCtl.EnableDisablePreviousButton(true);
            //}

            //if (_tempCurrentPage < totalPages)
            //    pagintCtl.EnableDisableNextButton(true);
            //    //pagintCtl.ButtonNext.Enabled = true;
            //else
            // pagintCtl.EnableDisableNextButton(false);
            //  // pagintCtl.EnableDisableNextButton(true);  pagintCtl.ButtonNext.Enabled = false;

        }

        //private int CalculateTotalPages(double totalRows)
        //{
        //    int totalPages = (int)Math.Ceiling(totalRows / int.Parse(pagintCtl.DDLRecords.SelectedValue));

        //    return totalPages;
        //}

        protected void gvwEmployees_Sorting(object sender, GridViewSortEventArgs e)
        {

            if (e.SortExpression != _sortBy)
            {
                e.SortDirection = SortDirection.Ascending;
                _sortDirection = e.SortDirection;
            }
            else if (_sortDirection == SortDirection.Ascending)
            {
                e.SortDirection = SortDirection.Descending;
            }
            else if (_sortDirection == SortDirection.Descending)
            {
                e.SortDirection = SortDirection.Ascending;
            }

            _sortDirection = e.SortDirection;

            _sortBy = e.SortExpression;

         
            e.Cancel = true;


            pagintCtl.CurrentPage = 1;
            //_tempCurrentPage = 1;
            BindEmployees();
        }

        protected void btnPrevious_Click()
        {
            pagintCtl.CurrentPage -= 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage -= 1;
            BindEmployees();
        }

        protected void ChangePageNumber()
        {
            BindEmployees();
        }

        protected void btnNext_Click()
        {
            pagintCtl.CurrentPage += 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage += 1;
            BindEmployees();
        }

        protected void ddlRecords_SelectedIndexChanged()
        {
            SessionManager.EmployeeDisplayRecodsPerPage = int.Parse(pagintCtl.DDLRecords.SelectedValue);
            //_tempCurrentPage = 1;
            pagintCtl.CurrentPage = 1;
            BindEmployees();
        }

        protected void txtEmployeeName_TextChanged(object sender, EventArgs e)
        {
            pagintCtl.CurrentPage = 1;
            //_tempCurrentPage = 1;
            BindEmployees();
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            int totalRecords = 0;

            string deparmentList = null;
            if (SessionManager.IsCustomRole)
                deparmentList = SessionManager.CustomRoleDeparmentIDList;

            bool? showRetiredOnly = null;
            if (ddlRetirementType.SelectedValue != "All")
                showRetiredOnly = bool.Parse(ddlRetirementType.SelectedValue);

            string sort = (_sortBy + " " + _sortDirection).ToLower();
            if (string.IsNullOrEmpty(_sortBy))
                sort = "";

            List<EEmployee> list = empMgr.GetAllEmployees(1, 9999, ref totalRecords, sort, SessionManager.CurrentCompanyId, filterCtl.EmployeeNameValue.Trim(), filterCtl.EmployeeIDCardNoValue.Trim(), showRetiredOnly,
                 filterCtl.BranchId, filterCtl.DepartmentId, filterCtl.DesignationId, filterCtl.LevelId, deparmentList,
                  filterCtl.DesignationName, filterCtl.SubDepartmentId, filterCtl.PayGroupID, filterCtl.LevelGroupID, filterCtl.StatusIDList, filterCtl.UnitId);

            ExcelGenerator.WriteExcel(ExcelGenerator.GetExcelForEmployeeList(list));
            
        }

        // 2) Adding a method which will return the Index of the column selected
        protected int GetSortColumnIndex()
        {


            foreach (DataControlField field in gvwEmployees.Columns)
            {
                if (field.SortExpression != "" && field.SortExpression == _sortBy)
                    return gvwEmployees.Columns.IndexOf(field);
                //else
                //    return -1;
            }
            return 0;
        }

        protected void gvwEmployees_RowCreated(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover",
                "this.originalClass=this.className;this.className='selected'");

                e.Row.Attributes.Add("onmouseout",
                "this.className=this.originalClass;");

                
            }

            int sortColumnIndex = 0;

            if (e.Row.RowType == DataControlRowType.Header)
            {
                sortColumnIndex = GetSortColumnIndex();
            }

            if (sortColumnIndex != 0)
            {
                AddSortImage(sortColumnIndex, e.Row);
            }


          
        }


        protected void AddSortImage(int columnIndex, GridViewRow HeaderRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();

            //if (showImage) // this is a boolean variable which should be false 
            {            //  on page load so that image wont show up initially.

                if (_sortDirection.ToString() == "Ascending")
                {
                    sortImage.ImageUrl = "~/Images/up.png";
                   
                }

                else
                {
                    sortImage.ImageUrl = "~/Images/down.png";
                }

                HeaderRow.Cells[columnIndex].Controls.Add(sortImage);
            }

        }
	

        protected void gvwEmployees_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int id =(int) gvwEmployees.DataKeys[e.RowIndex][0];

            if (EmployeeManager.HasEmployeeIncludedInCalculation(id) == false)
            {

                if (empMgr.Delete(id))
                {
                    BindEmployees();

                    divMsgCtl.InnerHtml = Resources.Messages.EmployeeDeletedMsg;
                    divMsgCtl.Hide = false;
                }
                else
                {
                    divWarningMsg.InnerHtml = "Employee could not be deleted.";
                    divWarningMsg.Hide = false;
                }
            }
            else
            {
                divWarningMsg.InnerHtml = "Salary saved employee can not be deleted.";
                divWarningMsg.Hide = false;
            }
        }

        protected void chkIsRetiredOrResignedAlso_CheckedChanged(object sender, EventArgs e)
        {
              
            BindEmployees();
        }
    }
}