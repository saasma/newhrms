﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="YearlyLeaveReportCtl.ascx.cs"
    Inherits="Web.NewHR.UserControls.YearlyLeaveReportCtl" %>
<script type="text/javascript">

    function searchList() {
              <%=PagingToolbar1.ClientID %>.doRefresh();
        }

 
   
    function SelEmpSearch()
    {
        <%=hdnEmpSearch.ClientID %>.setValue(<%=cmbEmpSearch.ClientID %>.getValue());
    }

    var renderValue = function(value)
    {
        if(parseFloat(value)==0)
            return '-';
        return value;
    }

      var GetGridRow = function (value, meta, record) 
    {
        cmbYear = <%=cmbYear.ClientID %>;

        if(record == null || typeof(record) == 'undefined')
            return "";
          
        var year = parseFloat(cmbYear.getValue());

        if(year == 0)
        {
            year = cmbYear.getRawValue();
            return "<a href='LeaveTakenDetails.aspx?id=" + record.data.EmployeeId + "&year=" + year +"'>" + value + "<a/>";
        }
        return "<a href='LeaveTakenDetails.aspx?id=" + record.data.EmployeeId + "&yearid=" + year +"'>" + value + "<a/>";
    };  

</script>
<style type="text/css">
    .x-grid-cell-inner
    {
        padding: 5px 5px 5px 5px;
    }
    .x-column-header-inner
    {
        padding: 7px 5px 7px 5px;
    }
</style>
<ext:Hidden ID="hdnEmployeeId" runat="server" />
<ext:Hidden ID="hdnBranchDepartmentId" runat="server" />
<ext:Hidden ID="hdnMsg" runat="server" />
<ext:Hidden ID="hdnEmpSearch" runat="server" />
<ext:Hidden ID="hdnSortBy" runat="server" />
<ext:Store runat="server" ID="storeSearch" AutoLoad="false">
    <Proxy>
        <ext:AjaxProxy Url="../../Handler/EmpSearchID.ashx">
            <ActionMethods Read="GET" />
            <Reader>
                <ext:JsonReader Root="plants" TotalProperty="total" />
            </Reader>
            <ExtraParams>
                <ext:Parameter Name="RetiredAlso" Value="true" />
            </ExtraParams>
        </ext:AjaxProxy>
    </Proxy>
    <Model>
        <ext:Model ID="Model5" IDProperty="EmployeeId" Name="ItemLineModel" runat="server">
            <Fields>
                <ext:ModelField Name="Name" Type="String" />
                <ext:ModelField Name="EmployeeId" Type="String" />
            </Fields>
        </ext:Model>
    </Model>
</ext:Store>
<div class="alert alert-info" style="margin-top: 10px; padding-top: 0px!important;">
    <ext:Container ID="listingContainer" runat="server">
        <Content>
            <table>
                <tr>
                    <td>
                        <ext:ComboBox Width="100" MatchFieldWidth="false" MarginSpec="0 5 0 5" LabelSeparator=""
                            LabelAlign="Top" QueryMode="Local" ID="cmbYear" DisplayField="Name" ValueField="FinancialDateId"
                            runat="server" FieldLabel="Year">
                            <%-- <SelectedItems>
                                            <ext:ListItem Index="0">
                                            </ext:ListItem>
                                        </SelectedItems>--%>
                            <Store>
                                <ext:Store ID="Store1" runat="server">
                                    <Model>
                                        <ext:Model ID="Model2" IDProperty="FinancialDateId" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="FinancialDateId" />
                                                <ext:ModelField Name="Name" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                    </td>
                    <td>
                        <ext:ComboBox Width="140" MatchFieldWidth="false" MarginSpec="0 5 0 5" LabelSeparator=""
                            LabelAlign="Top" QueryMode="Local" ID="cmbBranch" DisplayField="Name" ValueField="BranchId"
                            runat="server" FieldLabel="Branch">
                            <Items>
                                <ext:ListItem Index="0" Value="-1" Text="All">
                                </ext:ListItem>
                            </Items>
                            <Store>
                                <ext:Store ID="Store5" runat="server">
                                    <Model>
                                        <ext:Model ID="Model7" IDProperty="BranchId" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="BranchId" />
                                                <ext:ModelField Name="Name" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                    </td>
                    <td>
                        <ext:ComboBox FieldLabel="Department" MatchFieldWidth="false" ID="cmbDepartment"
                            Width="140" MarginSpec="0 5 0 5" runat="server" ValueField="DepartmentId" DisplayField="Name"
                            LabelAlign="Top" LabelSeparator="" QueryMode="Local">
                            <Items>
                                <ext:ListItem Index="0" Value="-1" Text="All">
                                </ext:ListItem>
                            </Items>
                            <Store>
                                <ext:Store ID="Store6" runat="server">
                                    <Model>
                                        <ext:Model ID="Model8" IDProperty="DepartmentId" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="DepartmentId" />
                                                <ext:ModelField Name="Name" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                    </td>
                    <td>
                        <ext:ComboBox LabelSeparator="" ID="cmbEmpSearch" FieldLabel="Search Employee" EmptyText="Employee Name"
                            LabelWidth="80" LabelAlign="Top" runat="server" DisplayField="Name" ValueField="EmployeeId"
                            StoreID="storeSearch" TypeAhead="false" Width="250" PageSize="9999" HideBaseTrigger="true"
                            MinChars="1" TriggerAction="All" ForceSelection="false">
                            <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                                <ItemTpl ID="ItemTpl2" runat="server">
                                    <Html>
                                        <tpl>
                                                        <div class="search-item">
                                                                        <span>{Name}</span>  
                                                         </div>
					                            </tpl>
                                    </Html>
                                </ItemTpl>
                            </ListConfig>
                            <Triggers>
                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                            </Triggers>
                            <Listeners>
                                <Select Handler="this.getTrigger(0).show(); SelEmpSearch();" />
                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                <TriggerClick Handler="if (index == 0) { 
                                                       this.clearValue(); 
                                                       this.getTrigger(0).hide();
                                                   }" />
                            </Listeners>
                        </ext:ComboBox>
                    </td>
                    <td>
                        <ext:Button ID="btnLoad" runat="server" Text="Load" Width="100" Cls="btn btn-default"
                            MarginSpec="25 10 10 10">
                            <Listeners>
                                <Click Fn="searchList">
                                </Click>
                            </Listeners>
                        </ext:Button>
                    </td>
                    <td>
                        <ext:Button runat="server" MarginSpec="25 10 10 10" AutoPostBack="true" OnClick="btnExport_Click"
                            ID="btnExport" Text="<i></i>Export To Excel">
                        </ext:Button>
                    </td>
                </tr>
            </table>
        </Content>
    </ext:Container>
</div>
<ext:KeyMap ID="KeyMap1" runat="server" Target="={#{listingContainer}.getBody()}">
    <Binding>
        <ext:KeyBinding Handler="#{btnLoad}.fireEvent('click');">
            <Keys>
                <ext:Key Code="ENTER" />
            </Keys>
        </ext:KeyBinding>
    </Binding>
</ext:KeyMap>
<ext:GridPanel StyleSpec="margin-top:15px;" ID="gridBranchTransfer" runat="server"
    OnReadData="Store_ReadData" AutoScroll="true">
    <Store>
        <ext:Store PageSize="25" ID="storeEmpList" AutoLoad="true" runat="server" OnReadData="Store_ReadData"
            RemotePaging="true" RemoteSort="true">
            <Proxy>
                <ext:PageProxy />
            </Proxy>
            <Model>
                <ext:Model ID="Model1" runat="server" IDProperty="EmployeeId">
                    <Fields>
                        <ext:ModelField Name="RowNumber" Type="String" />
                        <ext:ModelField Name="EmployeeId" Type="String" />
                        <ext:ModelField Name="IDCardNo" Type="String" />
                        <ext:ModelField Name="Name" Type="String" />
                        <ext:ModelField Name="L1LastYear" Type="String" />
                        <ext:ModelField Name="L1CurrentYear" Type="String" />
                        <ext:ModelField Name="L1Taken" Type="String" />
                        <ext:ModelField Name="L1Adjustment" Type="String" />
                        <ext:ModelField Name="L1Lapsed" Type="String" />
                        <ext:ModelField Name="L1Encashed" Type="String" />
                        <ext:ModelField Name="L1Balance" Type="String" />
                        <ext:ModelField Name="L2LastYear" Type="String" />
                        <ext:ModelField Name="L2CurrentYear" Type="String" />
                        <ext:ModelField Name="L2Taken" Type="String" />
                        <ext:ModelField Name="L2Adjustment" Type="String" />
                        <ext:ModelField Name="L2Lapsed" Type="String" />
                        <ext:ModelField Name="L2Encashed" Type="String" />
                        <ext:ModelField Name="L2Balance" Type="String" />
                        <ext:ModelField Name="L3LastYear" Type="String" />
                        <ext:ModelField Name="L3CurrentYear" Type="String" />
                        <ext:ModelField Name="L3Taken" Type="String" />
                        <ext:ModelField Name="L3Adjustment" Type="String" />
                        <ext:ModelField Name="L3Lapsed" Type="String" />
                        <ext:ModelField Name="L3Encashed" Type="String" />
                        <ext:ModelField Name="L3Balance" Type="String" />
                        <ext:ModelField Name="L4LastYear" Type="String" />
                        <ext:ModelField Name="L4CurrentYear" Type="String" />
                        <ext:ModelField Name="L4Taken" Type="String" />
                        <ext:ModelField Name="L4Adjustment" Type="String" />
                        <ext:ModelField Name="L4Lapsed" Type="String" />
                        <ext:ModelField Name="L4Encashed" Type="String" />
                        <ext:ModelField Name="L4Balance" Type="String" />
                        <ext:ModelField Name="L5LastYear" Type="String" />
                        <ext:ModelField Name="L5CurrentYear" Type="String" />
                        <ext:ModelField Name="L5Taken" Type="String" />
                       <ext:ModelField Name="L5Adjustment" Type="String" />
                        <ext:ModelField Name="L5Lapsed" Type="String" />
                        <ext:ModelField Name="L5Encashed" Type="String" />
                        <ext:ModelField Name="L5Balance" Type="String" />
                        <ext:ModelField Name="L6LastYear" Type="String" />
                        <ext:ModelField Name="L6CurrentYear" Type="String" />
                        <ext:ModelField Name="L6Taken" Type="String" />
                        <ext:ModelField Name="L6Adjustment" Type="String" />
                        <ext:ModelField Name="L6Lapsed" Type="String" />
                        <ext:ModelField Name="L6Encashed" Type="String" />
                        <ext:ModelField Name="L6Balance" Type="String" />
                        <ext:ModelField Name="L7LastYear" Type="String" />
                        <ext:ModelField Name="L7CurrentYear" Type="String" />
                        <ext:ModelField Name="L7Taken" Type="String" />
                        <ext:ModelField Name="L7Adjustment" Type="String" />
                        <ext:ModelField Name="L7Lapsed" Type="String" />
                        <ext:ModelField Name="L7Encashed" Type="String" />
                        <ext:ModelField Name="L7Balance" Type="String" />
                        <ext:ModelField Name="L8LastYear" Type="String" />
                        <ext:ModelField Name="L8CurrentYear" Type="String" />
                        <ext:ModelField Name="L8Taken" Type="String" />
                        <ext:ModelField Name="L8Adjustment" Type="String" />
                        <ext:ModelField Name="L8Lapsed" Type="String" />
                        <ext:ModelField Name="L8Encashed" Type="String" />
                        <ext:ModelField Name="L8Balance" Type="String" />
                        <ext:ModelField Name="L9LastYear" Type="String" />
                        <ext:ModelField Name="L9CurrentYear" Type="String" />
                        <ext:ModelField Name="L9Taken" Type="String" />
                       <ext:ModelField Name="L9Adjustment" Type="String" />
                        <ext:ModelField Name="L9Lapsed" Type="String" />
                        <ext:ModelField Name="L9Encashed" Type="String" />
                        <ext:ModelField Name="L9Balance" Type="String" />
                        <ext:ModelField Name="L10LastYear" Type="String" />
                        <ext:ModelField Name="L10CurrentYear" Type="String" />
                        <ext:ModelField Name="L10Taken" Type="String" />
                       <ext:ModelField Name="L10Adjustment" Type="String" />
                        <ext:ModelField Name="L10Lapsed" Type="String" />
                        <ext:ModelField Name="L10Encashed" Type="String" />
                        <ext:ModelField Name="L10Balance" Type="String" />
                        <ext:ModelField Name="L11LastYear" Type="String" />
                        <ext:ModelField Name="L11CurrentYear" Type="String" />
                        <ext:ModelField Name="L11Taken" Type="String" />
                        <ext:ModelField Name="L11Adjustment" Type="String" />
                        <ext:ModelField Name="L11Lapsed" Type="String" />
                        <ext:ModelField Name="L11Encashed" Type="String" />
                        <ext:ModelField Name="L11Balance" Type="String" />
                    </Fields>
                </ext:Model>
            </Model>
            <Sorters>
                <ext:DataSorter Property="EmployeeId" Direction="ASC" />
            </Sorters>
        </ext:Store>
    </Store>
    <ColumnModel ID="ColumnModel1" runat="server">
        <Columns>
            <ext:Column ID="colFromDate" runat="server" Text="SN" Locked="true" DataIndex="RowNumber"
                MenuDisabled="false" Sortable="false" Align="Right" Width="50" />
            <ext:Column ID="Column1" runat="server" Text="EIN" Locked="true" DataIndex="EmployeeId"
                MenuDisabled="false" Sortable="true" Align="Right" Width="50" />
            <ext:Column ID="Column2" runat="server" Text="I No" Locked="true" DataIndex="IDCardNo"
                MenuDisabled="false" Sortable="true" Align="Right" Width="50" />
            <ext:Column ID="Column3" runat="server" Text="Name" Locked="true" DataIndex="Name"
                MenuDisabled="false" Sortable="true" Align="Left" Width="150">
                <Renderer Fn="GetGridRow" />
            </ext:Column>
            <ext:Column ID="ColumnLeave1" Visible="false" Text="Leave1" StyleSpec="background-color:#DDEBF7"
                runat="server">
                <Columns>
                    <ext:Column ID="ColumnIncome1" runat="server" Text="Last Year" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L1LastYear" MenuDisabled="false" Sortable="false" Align="Center" Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                    <ext:Column ID="ColumnIncome2" runat="server" Text="Curr Year" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L1CurrentYear" MenuDisabled="false" Sortable="false" Align="Center"
                        Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                    <ext:Column ID="ColumnIncome3" runat="server" Text="Taken" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L1Taken" MenuDisabled="false" Sortable="false" Align="Center" Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                    <ext:Column ID="ColumnIncome4" runat="server" Text="Adjust" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L1Adjustment" MenuDisabled="false" Sortable="false" Align="Center"
                        Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                    <ext:Column ID="Column5" runat="server" Text="Lapsed" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L1Lapsed" MenuDisabled="false" Sortable="false" Align="Center"
                        Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                    <ext:Column ID="Column34" runat="server" Text="Encash" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L1Encashed" MenuDisabled="false" Sortable="false" Align="Center"
                        Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                    <ext:Column ID="ColumnIncome5" runat="server" Text="Balance" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L1Balance" MenuDisabled="false" Sortable="false" Align="Center" Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                </Columns>
            </ext:Column>
            <ext:Column Text="Leave 2" Visible="false" ID="ColumnLeave2" StyleSpec="background-color:#FCE4D6"
                runat="server">
                <Columns>
                    <ext:Column ID="Column4" runat="server" Text="Last Year" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L2LastYear" MenuDisabled="false" Sortable="false" Align="Center" Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                    <ext:Column ID="Column511" runat="server" Text="Curr Year" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L2CurrentYear" MenuDisabled="false" Sortable="false" Align="Center"
                        Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                    <ext:Column ID="Column6" runat="server" Text="Taken" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L2Taken" MenuDisabled="false" Sortable="false" Align="Center" Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                    <ext:Column ID="Column7" runat="server" Text="Adjust" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L2Adjustment" MenuDisabled="false" Sortable="false" Align="Center"
                        Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                    <ext:Column ID="Column40" runat="server" Text="Lapsed" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L2Lapsed" MenuDisabled="false" Sortable="false" Align="Center"
                        Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                    <ext:Column ID="Column46" runat="server" Text="Encash" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L2Encashed" MenuDisabled="false" Sortable="false" Align="Center"
                        Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                    <ext:Column ID="Column8" runat="server" Text="Balance" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L2Balance" MenuDisabled="false" Sortable="false" Align="Center" Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                </Columns>
            </ext:Column>
            <ext:Column ID="ColumnLeave3" Visible="false" Text="Leave 3" StyleSpec="background-color:#DDEBF7"
                runat="server">
                <Columns>
                    <ext:Column ID="Column10" runat="server" Text="Last Year" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L3LastYear" MenuDisabled="false" Sortable="false" Align="Center" Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                    <ext:Column ID="Column11" runat="server" Text="Curr Year" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L3CurrentYear" MenuDisabled="false" Sortable="false" Align="Center"
                        Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                    <ext:Column ID="Column12" runat="server" Text="Taken" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L3Taken" MenuDisabled="false" Sortable="false" Align="Center" Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                   <ext:Column ID="Column13" runat="server" Text="Adjust" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L3Adjustment" MenuDisabled="false" Sortable="false" Align="Center"
                        Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                    <ext:Column ID="Column52" runat="server" Text="Lapsed" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L3Lapsed" MenuDisabled="false" Sortable="false" Align="Center"
                        Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                    <ext:Column ID="Column58" runat="server" Text="Encash" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L3Encashed" MenuDisabled="false" Sortable="false" Align="Center"
                        Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                    <ext:Column ID="Column14" runat="server" Text="Balance" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L3Balance" MenuDisabled="false" Sortable="false" Align="Center" Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                </Columns>
            </ext:Column>
            <ext:Column Text="Leave 4" Visible="false" ID="ColumnLeave4" StyleSpec="background-color:#FCE4D6"
                runat="server">
                <Columns>
                    <ext:Column ID="Column16" runat="server" Text="Last Year" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L4LastYear" MenuDisabled="false" Sortable="false" Align="Center" Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                    <ext:Column ID="Column17" runat="server" Text="Curr Year" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L4CurrentYear" MenuDisabled="false" Sortable="false" Align="Center"
                        Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                    <ext:Column ID="Column18" runat="server" Text="Taken" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L4Taken" MenuDisabled="false" Sortable="false" Align="Center" Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                    <ext:Column ID="Column19" runat="server" Text="Adjust" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L4Adjustment" MenuDisabled="false" Sortable="false" Align="Center"
                        Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                    <ext:Column ID="Column59" runat="server" Text="Lapsed" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L4Lapsed" MenuDisabled="false" Sortable="false" Align="Center"
                        Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                    <ext:Column ID="Column60" runat="server" Text="Encash" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L4Encashed" MenuDisabled="false" Sortable="false" Align="Center"
                        Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                    <ext:Column ID="Column20" runat="server" Text="Balance" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L4Balance" MenuDisabled="false" Sortable="false" Align="Center" Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                </Columns>
            </ext:Column>
            <ext:Column ID="ColumnLeave5" Visible="false" Text="Leave 5" StyleSpec="background-color:#DDEBF7"
                runat="server">
                <Columns>
                    <ext:Column ID="Column15" runat="server" Text="Last Year" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L5LastYear" MenuDisabled="false" Sortable="false" Align="Center" Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                    <ext:Column ID="Column21" runat="server" Text="Curr Year" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L5CurrentYear" MenuDisabled="false" Sortable="false" Align="Center"
                        Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                    <ext:Column ID="Column22" runat="server" Text="Taken" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L5Taken" MenuDisabled="false" Sortable="false" Align="Center" Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                    <ext:Column ID="Column23" runat="server" Text="Adjust" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L5Adjustment" MenuDisabled="false" Sortable="false" Align="Center"
                        Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                    <ext:Column ID="Column61" runat="server" Text="Lapsed" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L5Lapsed" MenuDisabled="false" Sortable="false" Align="Center"
                        Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                    <ext:Column ID="Column62" runat="server" Text="Encash" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L5Encashed" MenuDisabled="false" Sortable="false" Align="Center"
                        Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                    <ext:Column ID="Column24" runat="server" Text="Balance" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L5Balance" MenuDisabled="false" Sortable="false" Align="Center" Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                </Columns>
            </ext:Column>
            <ext:Column Text="Leave 6" Visible="false" ID="ColumnLeave6" StyleSpec="background-color:#FCE4D6"
                runat="server">
                <Columns>
                    <ext:Column ID="Column26" runat="server" Text="Last Year" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L6LastYear" MenuDisabled="false" Sortable="false" Align="Center" Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                    <ext:Column ID="Column27" runat="server" Text="Curr Year" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L6CurrentYear" MenuDisabled="false" Sortable="false" Align="Center"
                        Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                    <ext:Column ID="Column28" runat="server" Text="Taken" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L6Taken" MenuDisabled="false" Sortable="false" Align="Center" Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                    <ext:Column ID="Column29" runat="server" Text="Adjust" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L6Adjustment" MenuDisabled="false" Sortable="false" Align="Center"
                        Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                    <ext:Column ID="Column63" runat="server" Text="Lapsed" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L6Lapsed" MenuDisabled="false" Sortable="false" Align="Center"
                        Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                    <ext:Column ID="Column64" runat="server" Text="Encash" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L6Encashed" MenuDisabled="false" Sortable="false" Align="Center"
                        Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                    <ext:Column ID="Column30" runat="server" Text="Balance" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L6Balance" MenuDisabled="false" Sortable="false" Align="Center" Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                </Columns>
            </ext:Column>
            <ext:Column ID="ColumnLeave7" Visible="false" Text="Leave7" StyleSpec="background-color:#DDEBF7"
                runat="server">
                <Columns>
                    <ext:Column ID="Column9" runat="server" Text="Last Year" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L7LastYear" MenuDisabled="false" Sortable="false" Align="Center" Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                    <ext:Column ID="Column25" runat="server" Text="Curr Year" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L7CurrentYear" MenuDisabled="false" Sortable="false" Align="Center"
                        Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                    <ext:Column ID="Column31" runat="server" Text="Taken" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L7Taken" MenuDisabled="false" Sortable="false" Align="Center" Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                   <ext:Column ID="Column32" runat="server" Text="Adjust" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L7Adjustment" MenuDisabled="false" Sortable="false" Align="Center"
                        Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                    <ext:Column ID="Column65" runat="server" Text="Lapsed" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L7Lapsed" MenuDisabled="false" Sortable="false" Align="Center"
                        Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                    <ext:Column ID="Column66" runat="server" Text="Encash" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L7Encashed" MenuDisabled="false" Sortable="false" Align="Center"
                        Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                    <ext:Column ID="Column33" runat="server" Text="Balance" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L7Balance" MenuDisabled="false" Sortable="false" Align="Center" Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                </Columns>
            </ext:Column>
            <ext:Column Text="Leave 8" Visible="false" ID="ColumnLeave8" StyleSpec="background-color:#FCE4D6"
                runat="server">
                <Columns>
                    <ext:Column ID="Column35" runat="server" Text="Last Year" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L8LastYear" MenuDisabled="false" Sortable="false" Align="Center" Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                    <ext:Column ID="Column36" runat="server" Text="Curr Year" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L8CurrentYear" MenuDisabled="false" Sortable="false" Align="Center"
                        Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                    <ext:Column ID="Column37" runat="server" Text="Taken" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L8Taken" MenuDisabled="false" Sortable="false" Align="Center" Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                    <ext:Column ID="Column38" runat="server" Text="Adjust" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L8Adjustment" MenuDisabled="false" Sortable="false" Align="Center"
                        Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                    <ext:Column ID="Column67" runat="server" Text="Lapsed" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L8Lapsed" MenuDisabled="false" Sortable="false" Align="Center"
                        Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                    <ext:Column ID="Column68" runat="server" Text="Encash" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L8Encashed" MenuDisabled="false" Sortable="false" Align="Center"
                        Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                    <ext:Column ID="Column39" runat="server" Text="Balance" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L8Balance" MenuDisabled="false" Sortable="false" Align="Center" Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                </Columns>
            </ext:Column>
            <ext:Column ID="ColumnLeave9" Visible="false" Text="Leave 9" StyleSpec="background-color:#DDEBF7"
                runat="server">
                <Columns>
                    <ext:Column ID="Column41" runat="server" Text="Last Year" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L9LastYear" MenuDisabled="false" Sortable="false" Align="Center" Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                    <ext:Column ID="Column42" runat="server" Text="Curr Year" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L9CurrentYear" MenuDisabled="false" Sortable="false" Align="Center"
                        Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                    <ext:Column ID="Column43" runat="server" Text="Taken" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L9Taken" MenuDisabled="false" Sortable="false" Align="Center" Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                   <ext:Column ID="Column44" runat="server" Text="Adjust" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L9Adjustment" MenuDisabled="false" Sortable="false" Align="Center"
                        Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                    <ext:Column ID="Column69" runat="server" Text="Lapsed" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L9Lapsed" MenuDisabled="false" Sortable="false" Align="Center"
                        Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                    <ext:Column ID="Column70" runat="server" Text="Encash" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L9Encashed" MenuDisabled="false" Sortable="false" Align="Center"
                        Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                    <ext:Column ID="Column45" runat="server" Text="Balance" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L9Balance" MenuDisabled="false" Sortable="false" Align="Center" Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                </Columns>
            </ext:Column>
            <ext:Column Text="Leave 10" Visible="false" ID="ColumnLeave10" StyleSpec="background-color:#FCE4D6"
                runat="server">
                <Columns>
                    <ext:Column ID="Column47" runat="server" Text="Last Year" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L10LastYear" MenuDisabled="false" Sortable="false" Align="Center"
                        Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                    <ext:Column ID="Column48" runat="server" Text="Curr Year" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L10CurrentYear" MenuDisabled="false" Sortable="false" Align="Center"
                        Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                    <ext:Column ID="Column49" runat="server" Text="Taken" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L10Taken" MenuDisabled="false" Sortable="false" Align="Center" Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                   <ext:Column ID="Column50" runat="server" Text="Adjust" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L10Adjustment" MenuDisabled="false" Sortable="false" Align="Center"
                        Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                    <ext:Column ID="Column71" runat="server" Text="Lapsed" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L10Lapsed" MenuDisabled="false" Sortable="false" Align="Center"
                        Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                    <ext:Column ID="Column72" runat="server" Text="Encash" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L10Encashed" MenuDisabled="false" Sortable="false" Align="Center"
                        Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                    <ext:Column ID="Column51" runat="server" Text="Balance" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L10Balance" MenuDisabled="false" Sortable="false" Align="Center" Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                </Columns>
            </ext:Column>
            <ext:Column ID="ColumnLeave11" Visible="false" Text="Leave 11" StyleSpec="background-color:#DDEBF7"
                runat="server">
                <Columns>
                    <ext:Column ID="Column53" runat="server" Text="Last Year" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L11LastYear" MenuDisabled="false" Sortable="false" Align="Center"
                        Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                    <ext:Column ID="Column54" runat="server" Text="Curr Year" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L11CurrentYear" MenuDisabled="false" Sortable="false" Align="Center"
                        Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                    <ext:Column ID="Column55" runat="server" Text="Taken" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L11Taken" MenuDisabled="false" Sortable="false" Align="Center" Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                  <ext:Column ID="Column56" runat="server" Text="Adjust" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L11Adjustment" MenuDisabled="false" Sortable="false" Align="Center"
                        Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                    <ext:Column ID="Column73" runat="server" Text="Lapsed" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L11Lapsed" MenuDisabled="false" Sortable="false" Align="Center"
                        Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                    <ext:Column ID="Column74" runat="server" Text="Encash" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L11Encashed" MenuDisabled="false" Sortable="false" Align="Center"
                        Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                    <ext:Column ID="Column57" runat="server" Text="Balance" StyleSpec="background-color:#DDEBF7"
                        DataIndex="L11Balance" MenuDisabled="false" Sortable="false" Align="Center" Width="70">
                        <Renderer Fn="renderValue" />
                    </ext:Column>
                </Columns>
            </ext:Column>
        </Columns>
    </ColumnModel>
    <BottomBar>
        <ext:PagingToolbar ID="PagingToolbar1" runat="server" StoreID="storeEmpList" DisplayInfo="true">
        </ext:PagingToolbar>
    </BottomBar>
</ext:GridPanel>
