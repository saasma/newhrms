﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HealthCtrlView.ascx.cs"
    Inherits="Web.NewHR.UserControls.HealthCtrlView" %>

<div style="background: #D8E7F3; border: 1px solid #C1DDF1; padding: 5px; margin-bottom: 10px;">
    <div style="float: left; margin-right: 10px;">
        <ext:Label runat="server" ID="lblHandicapped">
        </ext:Label>
    </div>
    <div>
        <ext:Label runat="server" ID="lblHandicappedValue" Text="No">
        </ext:Label>
    </div>
</div>

<ext:GridPanel ID="GridHealth" runat="server" Width="920" Cls="itemgrid">
    <Store>
        <ext:Store ID="StoreHealth" runat="server">
            <Model>
                <ext:Model ID="Model1" runat="server" IDProperty="HealthId">
                    <Fields>
                        <ext:ModelField Name="HealthStatus" Type="string" />
                        <ext:ModelField Name="ContitionTypeName" Type="String" />
                        <ext:ModelField Name="DiagnosedTypeName" Type="string" />
                        <ext:ModelField Name="DiagnosedDate" Type="string" />
                        <ext:ModelField Name="Height" Type="string" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
    </Store>
    <ColumnModel>
        <Columns>
            <ext:Column ID="Column1" runat="server" Text="Health Status" DataIndex="HealthStatus"
                Wrap="true" Width="200" Flex="1" />
            <ext:Column ID="Column2" runat="server" Text="Contition" DataIndex="ContitionTypeName"
                Wrap="true" Width="150">
            </ext:Column>
            <ext:Column ID="Column3" runat="server" Text="Diagnosed Type" DataIndex="DiagnosedTypeName"
                Wrap="true" Width="150">
            </ext:Column>
            <ext:Column ID="Column4" runat="server" Text="Diagnosed Date" DataIndex="DiagnosedDate"
                Wrap="true" Width="130">
            </ext:Column>
            <ext:Column ID="Column5" runat="server" Text="Height" DataIndex="Height" Wrap="true"
                Width="100">
            </ext:Column>
            <ext:Column ID="Column6" runat="server" Text="" Width="190">
            </ext:Column>
        </Columns>
    </ColumnModel>
    <SelectionModel>
        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
    </SelectionModel>
</ext:GridPanel>
