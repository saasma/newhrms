﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using Utils.Helper;
using BLL.BO;
using Web.CP.Report.Templates.HR;
using Utils.Calendar;
using DevExpress.XtraReports.UI;
using System.IO;

namespace Web.NewHR.UserControls
{
    public partial class YearlyLeaveReportCtl : BaseUserControl
    {
        private PageViewType _PageViewType = PageViewType.Admin;
        public PageViewType PageView
        {
            get{return _PageViewType;}
            set{_PageViewType = value;}
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (this.PageView == PageViewType.ManagerEmployee)
            {
                cmbBranch.Visible = false;
                cmbDepartment.Visible = false;
                cmbEmpSearch.Visible = false;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest && !IsPostBack)
            {
                Initialize();

            }
        }

        private void Initialize()
        {
            //DateHelper
            if (SessionManager.CurrentCompany.LeaveStartMonth == 1)
            {
                List<YearBO> list = CommonManager.GetBaisakhJanuaryLeaveYearList();
                cmbYear.Store[0].DataSource = list;
                cmbYear.Store[0].DataBind();

                ExtControlHelper.ComboBoxSetSelected(list[list.Count - 1].FinancialDateId.ToString(), cmbYear);
            }
            else if (SessionManager.CurrentCompany.LeaveStartMonth == (int)EnglishMonthEnum.April && SessionManager.CurrentCompany.IsEnglishDate)
            {
                List<YearBO> list = CommonManager.GetAprilToMarchLeaveYearList();
                cmbYear.Store[0].DataSource = list;
                cmbYear.Store[0].DataBind();

                cmbYear.SelectedItems.Add(new Ext.Net.ListItem { Index = list.Count - 1 });
                //ExtControlHelper.ComboBoxSetSelected(list[list.Count - 1].FinancialDateId.ToString(), cmbYear);
            }
            else
            {
                List<FinancialDate> financialYearList = new CommonManager().GetAllFinancialDates();
                foreach (FinancialDate date in financialYearList)
                {
                    date.SetName(IsEnglish);
                }

                cmbYear.Store[0].DataSource = financialYearList;
                cmbYear.Store[0].DataBind();

                ExtControlHelper.ComboBoxSetSelected(financialYearList[financialYearList.Count - 1].FinancialDateId.ToString(), cmbYear);
            }



            List<Branch> branchList = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            cmbBranch.Store[0].DataSource = branchList;
            cmbBranch.Store[0].DataBind();

            cmbDepartment.Store[0].DataSource = DepartmentManager.GetAllDepartments().OrderBy(x => x.Name).ToList();
            cmbDepartment.Store[0].DataBind();

            //cmbDesignation.Store[0].DataSource = new CommonManager().GetAllDesignations().OrderBy(x => x.LevelAndDesignation).ToList();
            //cmbDesignation.Store[0].DataBind();

            //this.storeLevel.DataSource = ActivityManager.GetLevels().OrderBy(x => x.Name);
            //this.storeLevel.DataBind();


            List<LLeaveType> leaveList = LeaveAttendanceManager.GetLeaveListByCompany(SessionManager.CurrentCompanyId)
                .OrderBy(x => x.Order).ToList();

            leaveList.Add(new LLeaveType { LeaveTypeId = 0, Title = "Unpaid Leave" });

            int index = 0;
            foreach (LLeaveType item in leaveList)
            {
                index += 1;

                Column col = gridBranchTransfer.ColumnModel.FindControl("ColumnLeave" + index) as Column;
                if (col != null)
                {
                    col.Text = item.Title;
                    col.Visible = true;
                }
            }


        }

        private void Clear()
        {

        }


        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            int totalRecords = 0;
            int branchId = -1, depId = -1, levelId = -1, designationId = -1;

            if (cmbBranch.SelectedItem != null && cmbBranch.SelectedItem.Value != null)
                branchId = int.Parse(cmbBranch.SelectedItem.Value);
            if (cmbDepartment.SelectedItem != null && cmbDepartment.SelectedItem.Value != null)
                depId = int.Parse(cmbDepartment.SelectedItem.Value);
            //if (cmbLevel.SelectedItem != null && cmbLevel.SelectedItem.Value != null)
            //    levelId = int.Parse(cmbLevel.SelectedItem.Value);
            //if (cmbDesignation.SelectedItem != null && cmbDesignation.SelectedItem.Value != null)
            //    designationId = int.Parse(cmbDesignation.SelectedItem.Value);

            int employeeId = -1;

            int.TryParse(cmbEmpSearch.SelectedItem.Value, out employeeId);

            if (employeeId == 0)
                employeeId = -1;

            hdnSortBy.Text = (e.Sort[0].Property + " " + e.Sort[0].Direction).ToLower();

            int yearId = int.Parse(cmbYear.SelectedItem.Value);
            int? startid = null; int? endId = null;
            int? lastYearOrYearOpeningPayrollPeriodId = null;
            DateTime? yearStart = null, yearEnd = null;
            DateTime? startDateLeaveTaken = null; DateTime? endDateLeaveTaken = null; int? currentLeavePeriodIdForAccural = null;

            if (SessionManager.CurrentCompany.LeaveStartMonth == (int)EnglishMonthEnum.April && SessionManager.CurrentCompany.IsEnglishDate)
            {
                // get 2013/2014 year first part as year
                //string year = cmbYear.SelectedItem.Value.ToString();
                yearId = int.Parse(cmbYear.SelectedItem.Value.ToString());

                CommonManager.SetPeriodForAprilToMarchYearlyLeavePeriod(yearId, ref startid, ref endId, ref startDateLeaveTaken,
                     ref endDateLeaveTaken, ref currentLeavePeriodIdForAccural, ref lastYearOrYearOpeningPayrollPeriodId, ref yearStart, ref yearEnd, employeeId);
            }
            else if (yearId >= 1)
            {
                CommonManager.SetPeriodForMiddleSharwanJulyYearlyLeavePeriod(yearId, ref startid, ref endId, ref startDateLeaveTaken,
                     ref endDateLeaveTaken, ref currentLeavePeriodIdForAccural, ref lastYearOrYearOpeningPayrollPeriodId, ref yearStart, ref yearEnd,employeeId);
            }
            else
            {
                CommonManager.SetPeriodForBaisakhJanuaaryYearlyLeavePeriod(int.Parse(cmbYear.SelectedItem.Text), ref startid, ref endId, ref startDateLeaveTaken,
                   ref endDateLeaveTaken, ref currentLeavePeriodIdForAccural, ref lastYearOrYearOpeningPayrollPeriodId, ref yearStart, ref yearEnd,employeeId);
            }

            List<Report_HR_YearlyLeaveBalanceResult> list1 = EmployeeManager.GetYearlyLeaveReport
                (branchId, depId, levelId, designationId, employeeId, e.Page - 1, e.Limit, (e.Sort[0].Property + " " + e.Sort[0].Direction).ToLower(), ref totalRecords
                , startid, endId, currentLeavePeriodIdForAccural, startDateLeaveTaken, endDateLeaveTaken, "", lastYearOrYearOpeningPayrollPeriodId, yearStart.Value, yearEnd.Value
                 ,(int)this.PageView, SessionManager.CurrentLoggedInEmployeeId);

            e.Total = totalRecords;

            if (list1.Any())
                storeEmpList.DataSource = list1;
            storeEmpList.DataBind();

        }


        public void btnExport_Click(object sender, EventArgs e)
        {
            int totalRecords = 0;
            int branchId = -1, depId = -1, levelId = -1, designationId = -1;

            if (cmbBranch.SelectedItem != null && cmbBranch.SelectedItem.Value != null)
                branchId = int.Parse(cmbBranch.SelectedItem.Value);
            if (cmbDepartment.SelectedItem != null && cmbDepartment.SelectedItem.Value != null)
                depId = int.Parse(cmbDepartment.SelectedItem.Value);
            //if (cmbLevel.SelectedItem != null && cmbLevel.SelectedItem.Value != null)
            //    levelId = int.Parse(cmbLevel.SelectedItem.Value);
            //if (cmbDesignation.SelectedItem != null && cmbDesignation.SelectedItem.Value != null)
            //    designationId = int.Parse(cmbDesignation.SelectedItem.Value);

            int employeeId = -1;

            int.TryParse(cmbEmpSearch.SelectedItem.Value, out employeeId);

            if (employeeId == 0)
                employeeId = -1;


            int yearId = int.Parse(cmbYear.SelectedItem.Value);
            int? startid = null; int? endId = null;
            int? lastYearOrYearOpeningPayrollPeriodId = null;
            DateTime? yearStart = null, yearEnd = null;
            DateTime? startDateLeaveTaken = null; DateTime? endDateLeaveTaken = null; int? currentLeavePeriodIdForAccural = null;

            if (SessionManager.CurrentCompany.LeaveStartMonth == (int)EnglishMonthEnum.April && SessionManager.CurrentCompany.IsEnglishDate)
            {
                // get 2013/2014 year first part as year
             //   string year = cmbYear.SelectedItem.Value.ToString();
                yearId = int.Parse(cmbYear.SelectedItem.Value.ToString());

                CommonManager.SetPeriodForAprilToMarchYearlyLeavePeriod(yearId, ref startid, ref endId, ref startDateLeaveTaken,
                     ref endDateLeaveTaken, ref currentLeavePeriodIdForAccural, ref lastYearOrYearOpeningPayrollPeriodId, ref yearStart, ref yearEnd, employeeId);
            }
            else if (yearId >= 1)
            {
                CommonManager.SetPeriodForMiddleSharwanJulyYearlyLeavePeriod(yearId, ref startid, ref endId, ref startDateLeaveTaken,
                     ref endDateLeaveTaken, ref currentLeavePeriodIdForAccural, ref lastYearOrYearOpeningPayrollPeriodId, ref yearStart, ref yearEnd, employeeId);
            }
            else
            {
                CommonManager.SetPeriodForBaisakhJanuaaryYearlyLeavePeriod(int.Parse(cmbYear.SelectedItem.Text), ref startid, ref endId, ref startDateLeaveTaken,
                   ref endDateLeaveTaken, ref currentLeavePeriodIdForAccural, ref lastYearOrYearOpeningPayrollPeriodId, ref yearStart, ref yearEnd, employeeId);
            }

            List<Report_HR_YearlyLeaveBalanceResult> list1 = EmployeeManager.GetYearlyLeaveReport
                (branchId, depId, levelId, designationId, employeeId, 0, 999999, "", ref totalRecords
                , startid, endId, currentLeavePeriodIdForAccural, startDateLeaveTaken, endDateLeaveTaken, "", lastYearOrYearOpeningPayrollPeriodId, yearStart.Value, yearEnd.Value
                , (int)this.PageView, SessionManager.CurrentLoggedInEmployeeId);




            ReportDataSet.YearlyReportTableDataTable table = new ReportDataSet.YearlyReportTableDataTable();

            foreach (Report_HR_YearlyLeaveBalanceResult item in list1)
            {
                ReportDataSet.YearlyReportTableRow row = table.NewYearlyReportTableRow();
                row.EIN = item.EmployeeId.ToString();
                row.Name = item.Name;


                row.L1LastYear = item.L1LastYear;
                if (item.L1LastYear == 0)
                    row.SetL1LastYearNull();
                row.L1CurrentYear = item.L1CurrentYear;
                if (item.L1CurrentYear == 0)
                    row.SetL1CurrentYearNull();
                row.L1Taken = item.L1Taken;
                if (item.L1Taken == 0)
                    row.SetL1TakenNull();
                row.L1Adjustment = item.L1Adjustment;
                if (item.L1Adjustment == 0)
                    row.SetL1AdjustmentNull();
                row.L1Lapsed = item.L1Lapsed;
                if (item.L1Lapsed == 0)
                    row.SetL1LapsedNull();
                row.L1Encashed = item.L1Encashed;
                if (item.L1Encashed == 0)
                    row.SetL1EncashedNull();
                row.L1Balance = item.L1Balance;
                if (item.L1Balance == 0)
                    row.SetL1BalanceNull();

                row.L2LastYear = item.L2LastYear;
                if (item.L2LastYear == 0)
                    row.SetL2LastYearNull();
                row.L2CurrentYear = item.L2CurrentYear;
                if (item.L2CurrentYear == 0)
                    row.SetL2CurrentYearNull();
                row.L2Taken = item.L2Taken;
                if (item.L2Taken == 0)
                    row.SetL2TakenNull();
                row.L2Adjustment = item.L2Adjustment;
                if (item.L2Adjustment == 0)
                    row.SetL2AdjustmentNull();
                row.L2Lapsed = item.L2Lapsed;
                if (item.L2Lapsed == 0)
                    row.SetL2LapsedNull();
                row.L2Encashed = item.L2Encashed;
                if (item.L2Encashed == 0)
                    row.SetL2EncashedNull();
                row.L2Balance = item.L2Balance;
                if (item.L2Balance == 0)
                    row.SetL2BalanceNull();

                row.L3LastYear = item.L3LastYear;
                if (item.L3LastYear == 0)
                    row.SetL3LastYearNull();
                row.L3CurrentYear = item.L3CurrentYear;
                if (item.L3CurrentYear == 0)
                    row.SetL3CurrentYearNull();
                row.L3Taken = item.L3Taken;
                if (item.L3Taken == 0)
                    row.SetL3TakenNull();
                row.L3Adjustment = item.L3Adjustment;
                if (item.L3Adjustment == 0)
                    row.SetL3AdjustmentNull();
                row.L3Lapsed = item.L3Lapsed;
                if (item.L3Lapsed == 0)
                    row.SetL3LapsedNull();
                row.L3Encashed = item.L3Encashed;
                if (item.L3Encashed == 0)
                    row.SetL3EncashedNull();
                row.L3Balance = item.L3Balance;
                if (item.L3Balance == 0)
                    row.SetL3BalanceNull();

                row.L4LastYear = item.L4LastYear;
                if (item.L4LastYear == 0)
                    row.SetL4LastYearNull();
                row.L4CurrentYear = item.L4CurrentYear;
                if (item.L4CurrentYear == 0)
                    row.SetL4CurrentYearNull();
                row.L4Taken = item.L4Taken;
                if (item.L4Taken == 0)
                    row.SetL4TakenNull();
                row.L4Adjustment = item.L4Adjustment;
                if (item.L4Adjustment == 0)
                    row.SetL4AdjustmentNull();
                row.L4Lapsed = item.L4Lapsed;
                if (item.L4Lapsed == 0)
                    row.SetL4LapsedNull();
                row.L4Encashed = item.L4Encashed;
                if (item.L4Encashed == 0)
                    row.SetL4EncashedNull();
                row.L4Balance = item.L4Balance;
                if (item.L4Balance == 0)
                    row.SetL4BalanceNull();

                row.L5LastYear = item.L5LastYear;
                if (item.L5LastYear == 0)
                    row.SetL5LastYearNull();
                row.L5CurrentYear = item.L5CurrentYear;
                if (item.L5CurrentYear == 0)
                    row.SetL5CurrentYearNull();
                row.L5Taken = item.L5Taken;
                if (item.L5Taken == 0)
                    row.SetL5TakenNull();
                row.L5Adjustment = item.L5Adjustment;
                if (item.L5Adjustment == 0)
                    row.SetL5AdjustmentNull();
                row.L5Lapsed = item.L5Lapsed;
                if (item.L5Lapsed == 0)
                    row.SetL5LapsedNull();
                row.L5Encashed = item.L5Encashed;
                if (item.L5Encashed == 0)
                    row.SetL5EncashedNull();
                row.L5Balance = item.L5Balance;
                if (item.L5Balance == 0)
                    row.SetL5BalanceNull();

                row.L6LastYear = item.L6LastYear;
                if (item.L6LastYear == 0)
                    row.SetL6LastYearNull();
                row.L6CurrentYear = item.L6CurrentYear;
                if (item.L6CurrentYear == 0)
                    row.SetL6CurrentYearNull();
                row.L6Taken = item.L6Taken;
                if (item.L6Taken == 0)
                    row.SetL6TakenNull();
                row.L6Adjustment = item.L6Adjustment;
                if (item.L6Adjustment == 0)
                    row.SetL6AdjustmentNull();
                row.L6Lapsed = item.L6Lapsed;
                if (item.L6Lapsed == 0)
                    row.SetL6LapsedNull();
                row.L6Encashed = item.L6Encashed;
                if (item.L6Encashed == 0)
                    row.SetL6EncashedNull();
                row.L6Balance = item.L6Balance;
                if (item.L6Balance == 0)
                    row.SetL6BalanceNull();

                row.L7LastYear = item.L7LastYear;
                if (item.L7LastYear == 0)
                    row.SetL7LastYearNull();
                row.L7CurrentYear = item.L7CurrentYear;
                if (item.L7CurrentYear == 0)
                    row.SetL7CurrentYearNull();
                row.L7Taken = item.L7Taken;
                if (item.L7Taken == 0)
                    row.SetL7TakenNull();
                row.L7Adjustment = item.L7Adjustment;
                if (item.L7Adjustment == 0)
                    row.SetL7AdjustmentNull();
                row.L7Lapsed = item.L7Lapsed;
                if (item.L7Lapsed == 0)
                    row.SetL7LapsedNull();
                row.L7Encashed = item.L7Encashed;
                if (item.L7Encashed == 0)
                    row.SetL7EncashedNull();
                row.L7Balance = item.L7Balance;
                if (item.L7Balance == 0)
                    row.SetL7BalanceNull();

                row.L8LastYear = item.L8LastYear;
                if (item.L8LastYear == 0)
                    row.SetL8LastYearNull();
                row.L8CurrentYear = item.L8CurrentYear;
                if (item.L8CurrentYear == 0)
                    row.SetL8CurrentYearNull();
                row.L8Taken = item.L8Taken;
                if (item.L8Taken == 0)
                    row.SetL8TakenNull();
                row.L8Adjustment = item.L8Adjustment;
                if (item.L8Adjustment == 0)
                    row.SetL8AdjustmentNull();
                row.L8Lapsed = item.L8Lapsed;
                if (item.L8Lapsed == 0)
                    row.SetL8LapsedNull();
                row.L8Encashed = item.L8Encashed;
                if (item.L8Encashed == 0)
                    row.SetL8EncashedNull();
                row.L8Balance = item.L8Balance;
                if (item.L8Balance == 0)
                    row.SetL8BalanceNull();

                row.L9LastYear = item.L9LastYear;
                if (item.L9LastYear == 0)
                    row.SetL9LastYearNull();
                row.L9CurrentYear = item.L9CurrentYear;
                if (item.L9CurrentYear == 0)
                    row.SetL9CurrentYearNull();
                row.L9Taken = item.L9Taken;
                if (item.L9Taken == 0)
                    row.SetL9TakenNull();
                row.L9Adjustment = item.L9Adjustment;
                if (item.L9Adjustment == 0)
                    row.SetL9AdjustmentNull();
                row.L9Lapsed = item.L9Lapsed;
                if (item.L9Lapsed == 0)
                    row.SetL9LapsedNull();
                row.L9Encashed = item.L9Encashed;
                if (item.L9Encashed == 0)
                    row.SetL9EncashedNull();
                row.L9Balance = item.L9Balance;
                if (item.L9Balance == 0)
                    row.SetL9BalanceNull();

                row.L10LastYear = item.L10LastYear;
                if (item.L10LastYear == 0)
                    row.SetL10LastYearNull();
                row.L10CurrentYear = item.L10CurrentYear;
                if (item.L10CurrentYear == 0)
                    row.SetL10CurrentYearNull();
                row.L10Taken = item.L10Taken;
                if (item.L10Taken == 0)
                    row.SetL10TakenNull();
                row.L10Adjustment = item.L10Adjustment;
                if (item.L10Adjustment == 0)
                    row.SetL10AdjustmentNull();
                row.L10Lapsed = item.L10Lapsed;
                if (item.L10Lapsed == 0)
                    row.SetL10LapsedNull();
                row.L10Encashed = item.L10Encashed;
                if (item.L10Encashed == 0)
                    row.SetL10EncashedNull();
                row.L10Balance = item.L10Balance;
                if (item.L10Balance == 0)
                    row.SetL10BalanceNull();

                row.L11LastYear = item.L11LastYear;
                if (item.L11LastYear == 0)
                    row.SetL11LastYearNull();
                row.L11CurrentYear = item.L11CurrentYear;
                if (item.L11CurrentYear == 0)
                    row.SetL11CurrentYearNull();
                row.L11Taken = item.L11Taken;
                if (item.L11Taken == 0)
                    row.SetL11TakenNull();
                row.L11Adjustment = item.L11Adjustment;
                if (item.L11Adjustment == 0)
                    row.SetL11AdjustmentNull();
                row.L11Lapsed = item.L11Lapsed;
                if (item.L11Lapsed == 0)
                    row.SetL11LapsedNull();
                row.L11Encashed = item.L11Encashed;
                if (item.L11Encashed == 0)
                    row.SetL11EncashedNull();
                row.L11Balance = item.L11Balance;
                if (item.L11Balance == 0)
                    row.SetL11BalanceNull();


                table.Rows.Add(row);
            }




            ReportYearlyLeaveBalance report1 = new ReportYearlyLeaveBalance();

            report1.DataSource = table;
            report1.DataMember = "Report";

            if (IsEnglish == false)
            {
                CustomDate year1 = CustomDate.GetCustomDateFromString(BLL.BaseBiz.GetAppropriateDate(yearStart.Value), false);
                CustomDate year2 = CustomDate.GetCustomDateFromString(BLL.BaseBiz.GetAppropriateDate(yearEnd.Value), false);

                if (year1.Year == year2.Year)
                    report1.labelTitle.Text += " for " + year1.Year;
                else
                    report1.labelTitle.Text += " for " + year1.Year + "/" + year2.Year;
            }


            List<LLeaveType> leaveList = LeaveAttendanceManager.GetLeaveListByCompany(SessionManager.CurrentCompanyId)
              .OrderBy(x => x.Order).ToList();
            leaveList.Add(new LLeaveType { LeaveTypeId = 0, Title = "Unpaid Leave" });
            int index = 0;
            foreach (LLeaveType leave in leaveList)
            {
                index += 1;

                XRTableCell leaveTitle = report1.FindControl("leave" + index, true) as XRTableCell;
                if (leaveTitle != null)
                {
                    leaveTitle.Text = leave.Title;

                    //(report1.Bands[BandKind.PageHeader] .FindControl("panel" + index, true) as XRPanel).Visible = true;
                    (report1.Bands[BandKind.Detail].FindControl("paneldata" + index, true) as XRPanel).Visible = true;
                }
            }

            using (MemoryStream stream = new MemoryStream())
            {
                report1.ExportToXls(stream);
                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                HttpContext.Current.Response.AppendHeader("Content-Type", "application/vnd.ms-excel");
                HttpContext.Current.Response.AddHeader("Content-Length", stream.ToArray().Length.ToString());
                HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=YearlyLeaveBalance" + ".xls");
                HttpContext.Current.Response.OutputStream.Write(stream.ToArray(), 0, stream.ToArray().Length);
                HttpContext.Current.Response.End();
            }

        }


    }
}