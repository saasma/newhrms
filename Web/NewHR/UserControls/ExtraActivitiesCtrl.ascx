﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ExtraActivitiesCtrl.ascx.cs" Inherits="Web.NewHR.UserControls.ExtraActivitiesCtrl" %>

<script type="text/javascript">

    var prepareEActivities = function (grid, toolbar, rowIndex, record) {
        var editDelBtn = toolbar.items.get(1);
        if (record.data.IsEditable == 0) {
            editDelBtn.setVisible(false);
        }

    };
    
</script>


<ext:Hidden runat="server" ID="hdnCurricularId">
</ext:Hidden>
<div class="widget-body">
    <table class="fieldTable firsttdskip">
        <tr>
            <td>
                <ext:GridPanel ID="gridExtraActivity" runat="server" Width="1000" Cls="itemgrid">
                    <Store>
                        <ext:Store ID="storeExtraActivity" runat="server">
                            <Model>
                                <ext:Model ID="Model1" runat="server" IDProperty="CurricularId">
                                    <Fields>
                                        <ext:ModelField Name="CurricularId" Type="String" />
                                        <ext:ModelField Name="AcitivityName" Type="string" />
                                        <ext:ModelField Name="Proficiency" Type="string" />
                                        <ext:ModelField Name="Award" Type="string" />
                                        <ext:ModelField Name="Year" Type="string" />
                                        <ext:ModelField Name="IsEditable" Type="Int" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                    </Store>
                    <ColumnModel>
                        <Columns>
                            <ext:Column ID="Column1" Width="200" runat="server" Text="Activity Name" DataIndex="AcitivityName" Wrap="true" />
                            <ext:Column ID="Column2" Width="100" runat="server" Text="Proficiency" DataIndex="Proficiency" Wrap="true">
                            </ext:Column>
                            <ext:Column ID="Column6" Width="200" runat="server" Text="Award" DataIndex="Award" Wrap="true">
                            </ext:Column>
                            <ext:Column ID="Column81" Width="100" runat="server" Text="Year" DataIndex="Year" Wrap="true">
                            </ext:Column>                          
                           
                       <ext:CommandColumn ID="CommandColumn21" runat="server" Width="40">
                                <Commands>
                                    <ext:CommandSeparator />
                                    <ext:GridCommand Cls="editGridButton" ToolTip-Text="Edit"  Text="<i class='fa fa-pencil'></i>" CommandName="Edit" />
                                    
                                </Commands>
                                <DirectEvents>
                                    <Command OnEvent="gridExtraActivity_Command">
                                        <EventMask ShowMask="true">
                                        </EventMask>
                                        <ExtraParams>
                                            <ext:Parameter Name="ID" Value="record.data.CurricularId" Mode="Raw">
                                            </ext:Parameter>
                                            <ext:Parameter Name="command" Value="command" Mode="Raw">
                                            </ext:Parameter>
                                        </ExtraParams>
                                    </Command>
                                </DirectEvents>
                                <PrepareToolbar Fn="prepareEActivities" />
                            </ext:CommandColumn>
                            <ext:CommandColumn ID="CommandColumn2" runat="server" Width="40">
                                <Commands>
                                    <ext:CommandSeparator />
                                    <ext:GridCommand Cls="editGridButton" ToolTip-Text="Delete"  Text="<i class='fa fa-trash-o'></i>"
                                        CommandName="Delete" />
                                </Commands>
                                <DirectEvents>
                                    <Command OnEvent="gridExtraActivity_Command">
                                        <EventMask ShowMask="true">
                                        </EventMask>
                                        <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the record?" />
                                        <ExtraParams>
                                            <ext:Parameter Name="ID" Value="record.data.CurricularId" Mode="Raw">
                                            </ext:Parameter>
                                            <ext:Parameter Name="command" Value="command" Mode="Raw">
                                            </ext:Parameter>
                                        </ExtraParams>
                                    </Command>
                                </DirectEvents>
                                <PrepareToolbar Fn="prepareEActivities" />
                            </ext:CommandColumn>
                            <ext:Column ID="Column3" Width="420" runat="server" Text="">
                            </ext:Column>
                        </Columns>
                    </ColumnModel>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                    </SelectionModel>
                </ext:GridPanel>
            </td>
        </tr>
    </table>
    <div class="buttonBlock" runat="server" id="buttonBlock">
       <ext:Button runat="server" Cls="btn btn-primary" Width="120" StyleSpec="margin-top:10px" Height="30"
            ID="btnAddNewLine" Text="<i></i>Add New Line">
            <DirectEvents>
                <Click OnEvent="btnAddNewLine_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:Button>
    </div>
</div>
<ext:Window ID="WExtraActivities" runat="server" Title="Extra Activities" Icon="Application"
    Height="420" Width="430" BodyPadding="5" Hidden="true" Modal="true">
    <Content>
        <table class="fieldTable">
            <tr>
                <td colspan="2">
                    <ext:TextField Width="330" ID="txtActivityName" runat="server" FieldLabel="Activity Name *"
                        LabelAlign="top" LabelSeparator="" />
                    <asp:RequiredFieldValidator Display="None" ID="rfvActivityName" runat="server"
                        ValidationGroup="SaveUpdateActivity" ControlToValidate="txtActivityName" ErrorMessage="Activity Name is required." />
                </td>
               
            </tr>
            <tr>
                <td colspan="2">
                    <ext:TextField Width="330" ID="txtAward" runat="server" FieldLabel="Award"
                        LabelAlign="top" LabelSeparator="" />
                </td>
           
            </tr>
            <tr>
                <td>
                    <ext:TextField ID="txtYear" Width="160" runat="server" FieldLabel="Year *" MaskRe="/[0-9]/"
                        LabelAlign="top" LabelSeparator="" />
                    <asp:RequiredFieldValidator Display="None" ID="rfvYear" runat="server"
                        ValidationGroup="SaveUpdateActivity" ControlToValidate="txtYear"
                        ErrorMessage="Year is required." />
                 
                </td>
                <td>
                     <ext:TextField Width="160" ID="txtProficiency" runat="server" FieldLabel="Proficiency"
                        LabelAlign="top" LabelSeparator="" />     
                </td>
            </tr>
             <td colspan="2">
                    <ext:TextArea ID="txtRemarks" Width="330" runat="server" FieldLabel="Remarks" LabelAlign="Top"
                        LabelSeparator="" />
                </td>

    
            <tr>
                <td valign="bottom" colspan="2">
                    <div class="popupButtonDiv">
                        <ext:Button runat="server" Cls="btn btn-primary" ID="btnSave" Text="<i></i>Save">
                            <DirectEvents>
                                <Click OnEvent="btnSave_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                            <Listeners>
                                <Click Handler="valGroup = 'SaveUpdateActivity';  if(CheckValidation()) return this.disable(); else return false;">
                                </Click>
                            </Listeners>
                        </ext:Button>
                        <div class="btnFlatOr">
                            or</div>
                        <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="LinkButton2" Cls="btnFlatLeftGap"
                            Text="<i></i>Cancel">
                            <Listeners>
                                <Click Handler="#{WExtraActivities}.hide();">
                                </Click>
                            </Listeners>
                        </ext:LinkButton>
                    </div>
                </td>
            </tr>
        </table>
    </Content>
</ext:Window>