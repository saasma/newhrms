﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SkillSetPopUpCtrl.ascx.cs" Inherits="Web.NewHR.UserControls.SkillSetPopUpCtrl" %>

<ext:Hidden runat="server" ID="hdnSkillSetsID">
</ext:Hidden>

 <table class="fieldTable">
            <tr>
                <td>
                    <ext:ComboBox ID="cmbSkill" runat="server" ValueField="SkillSetId" DisplayField="Name"
                        FieldLabel="Skill" LabelAlign="top" LabelSeparator="" QueryMode="Local">
                        <Store>
                            <ext:Store ID="Store2" runat="server">
                                <Model>
                                    <ext:Model ID="Model4" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="SkillSetId" />
                                            <ext:ModelField Name="Name" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                </td>
                <td>
                    <ext:TextField ID="txtNewSkill" runat="server" FieldLabel="Add a new skill" LabelAlign="top"
                        LabelSeparator="" />
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <ext:Checkbox ID="chkAddtoSkillPool" runat="server" FieldLabel="Add to Skill Pool"
                        LabelSeparator="">
                    </ext:Checkbox>
                </td>
            </tr>
            <tr>
                <td>
                    <table class="fieldTable firsttdskip">
                        <tr>
                            <td>
                                <ext:ComboBox ID="cmbLevelOfExpertise" runat="server" ValueField="LevelID" DisplayField="Name"
                                    Width="150" FieldLabel="Level of Expertise" LabelAlign="top" LabelSeparator=""
                                    ForceSelection="true" QueryMode="Local">
                                    <Store>
                                        <ext:Store ID="Store1" runat="server">
                                            <Model>
                                                <ext:Model ID="Model2" runat="server">
                                                    <Fields>
                                                        <ext:ModelField Name="LevelID" />
                                                        <ext:ModelField Name="Name" />
                                                    </Fields>
                                                </ext:Model>
                                            </Model>
                                        </ext:Store>
                                    </Store>
                                </ext:ComboBox>
                                <%-- <asp:RequiredFieldValidator Display="None" ID="valcmbLevelOfExpertise" runat="server"
                                    ValidationGroup="SkillSetsSaveUpdate" ControlToValidate="cmbLevelOfExpertise"
                                    ErrorMessage="Level of Expertise is required." />--%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <ext:TextArea ID="txtNote" runat="server" FieldLabel="Notes" LabelSeparator="" LabelAlign="Top"
                        Rows="3" Cols="50" />
                </td>
            </tr>
            <tr>
                <td valign="bottom" colspan="2">
                    <div class="popupButtonDiv">
                        <ext:Button runat="server" Cls="btn btn-primary"
                            ID="btnSave" Text="<i></i>Save">
                            <DirectEvents>
                                <Click OnEvent="btnSave_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                            <Listeners>
                                <Click Handler="valGroup = 'SkillSetsSaveUpdate'; if(CheckValidation()) return this.disable(); else return false;">
                                </Click>
                            </Listeners>
                        </ext:Button>
                        <div class="btnFlatOr">
                            or</div>
                        <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="LinkButton2" Cls="btnFlatLeftGap"
                            Text="<i></i>Cancel">
                            <Listeners>
                                <Click Handler="#{AESkillSetsWindow}.hide();">
                                </Click>
                            </Listeners>
                        </ext:LinkButton>
                    </div>
                </td>
            </tr>
        </table>