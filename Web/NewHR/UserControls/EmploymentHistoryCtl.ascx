﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmploymentHistoryCtl.ascx.cs"
    Inherits="Web.NewHR.UserControls.EmploymentHistoryCtl" %>
<ext:Hidden runat="server" ID="hdnPreviousEmploymentID">
</ext:Hidden>
<table class="fieldTable firsttdskip">
    <tr>
        <td>
            <ext:GridPanel ID="GridPreviousEmployment" runat="server" Width="950" Cls="itemgrid">
                <Store>
                    <ext:Store ID="StorePreviousEmployment" runat="server">
                        <Model>
                            <ext:Model ID="Model1" runat="server" IDProperty="PrevEmploymentId">
                                <Fields>
                                    <ext:ModelField Name="Category" Type="String" />
                                    <ext:ModelField Name="Organization" Type="String" />
                                    <ext:ModelField Name="Place" Type="string" />
                                    <ext:ModelField Name="Position" Type="string" />
                                    <ext:ModelField Name="JobResponsibility" Type="string" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:Column ID="Column5"  Width="150px"  runat="server" Text="Experience Category" DataIndex="Category" />
                        <ext:Column ID="Column1"  Width="300px" runat="server" Text="Organization" DataIndex="Organization" />
                        <ext:Column ID="Column2"  Width="150px" runat="server" Text="Place" DataIndex="Place">
                        </ext:Column>
                        <ext:Column ID="Column3"  Width="200px" runat="server" Text="Position" DataIndex="Position">
                        </ext:Column>
                        <ext:Column ID="Column4" Width="200" runat="server" Text="Job Responsibility" DataIndex="JobResponsibility"
                            >
                        </ext:Column>
                        <ext:CommandColumn ID="CommandColumn1" runat="server" Width="80">
                            <Commands>
                                <ext:GridCommand Icon="Delete" CommandName="Delete">
                                    <ToolTip Text="Delete" />
                                </ext:GridCommand>
                                <ext:CommandSeparator />
                                <ext:GridCommand Icon="NoteEdit" CommandName="Edit">
                                    <ToolTip Text="Edit" />
                                </ext:GridCommand>
                            </Commands>
                            <DirectEvents>
                                <Command OnEvent="GridPreviousEmployment_Command">
                                    <EventMask ShowMask="true">
                                    </EventMask>
                                    <ExtraParams>
                                        <ext:Parameter Name="ID" Value="record.data.PrevEmploymentId" Mode="Raw">
                                        </ext:Parameter>
                                        <ext:Parameter Name="command" Value="command" Mode="Raw">
                                        </ext:Parameter>
                                    </ExtraParams>
                                </Command>
                            </DirectEvents>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
                <SelectionModel>
                    <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                </SelectionModel>
            </ext:GridPanel>
        </td>
    </tr>
</table>
<div class="buttonBlock">
    <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="LinkButton1" Cls="btnFlat"
        BaseCls="btnFlat" Text="<i></i>Add New Line">
        <DirectEvents>
            <Click OnEvent="btnAddNewLine_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
</div>
<ext:Window ID="AEPreviousEmploymentWindow" runat="server" Title="Previous Employment Details"
    Icon="Application" Height="500" Width="460" BodyPadding="5" Hidden="true" Modal="true">
    <Content>
        <table class="fieldTable">
            <tr>
                <td>
                    <ext:ComboBox ID="cmbExpCategory" runat="server" ValueField="CategoryID" DisplayField="Name"
                        FieldLabel="Experience Category" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                        QueryMode="Local">                     
                        <Store>
                            <ext:Store ID="Store1" runat="server">
                                <Model>
                                    <ext:Model ID="Model2" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="CategoryID" Type="String" />
                                            <ext:ModelField Name="Name" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                </td>
                <td style="padding-top: 5px">
                </td>
            </tr>
            <tr>
                <td>
                    <ext:TextField ID="txtOrganization" Width="180px" runat="server" FieldLabel="Organization *"
                        LabelAlign="top" LabelSeparator="" />
                    <asp:RequiredFieldValidator Display="None" ID="valtxtOrganization" runat="server"
                        ValidationGroup="EmploymentHistorySaveUpdate" ControlToValidate="txtOrganization"
                        ErrorMessage="Please enter the Organization." />
                </td>
                <td style="padding-top: 5px">
                    <ext:TextField ID="txtPlace" Width="180px" runat="server" FieldLabel="Place *" LabelAlign="top"
                        LabelSeparator="" />
                    <asp:RequiredFieldValidator Display="None" ID="valtxtPlace" runat="server" ValidationGroup="EmploymentHistorySaveUpdate"
                        ControlToValidate="txtPlace" ErrorMessage="Please type a Place." />
                </td>
            </tr>
            <tr>
                <td style="padding-top: 5px">
                    <ext:TextField ID="txtPosition" Width="180px" runat="server" FieldLabel="Position *"
                        LabelAlign="top" LabelSeparator="" />
                    <asp:RequiredFieldValidator Display="None" ID="valtxtPosition" runat="server" ValidationGroup="EmploymentHistorySaveUpdate"
                        ControlToValidate="txtPosition" ErrorMessage="Please enter Position." />
                </td>
                <td style="padding-top: 5px">
                    <ext:TextField ID="txtJobResponsibility" Width="180px" runat="server" FieldLabel="Job Responsibility *"
                        LabelAlign="top" LabelSeparator="" />
                    <asp:RequiredFieldValidator Display="None" ID="valtxtJobResponsibility" runat="server"
                        ValidationGroup="EmploymentHistorySaveUpdate" ControlToValidate="txtJobResponsibility"
                        ErrorMessage="Please type the Job Responsibility." />
                </td>
            </tr>
            <tr>
                <td>
                    <pr:CalendarExtControl FieldLabel="From " Width="180px" ID="calFrom" runat="server"
                        LabelSeparator="" LabelAlign="Top" />
                    <%--<asp:RequiredFieldValidator Display="None" ID="valcalFrom" runat="server" ValidationGroup="EmploymentHistorySaveUpdate"
                        ControlToValidate="calFrom" ErrorMessage="Please enter From Date." />--%>
                </td>
                <td>
                    <pr:CalendarExtControl FieldLabel="To" Width="180px" ID="calTo" runat="server" LabelSeparator=""
                        LabelAlign="Top" />
                    <%--<asp:RequiredFieldValidator Display="None" ID="valcalTo" runat="server" ValidationGroup="EmploymentHistorySaveUpdate"
                        ControlToValidate="calTo" ErrorMessage="Please enter To Date." />--%>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <ext:TextField ID="txtReasonforLeaving" runat="server" FieldLabel="Reason for Leaving *"
                        Width="375" LabelAlign="top" LabelSeparator="" />
                    <asp:RequiredFieldValidator Display="None" ID="valtxtReasonforLeaving" runat="server"
                        ValidationGroup="EmploymentHistorySaveUpdate" ControlToValidate="txtReasonforLeaving"
                        ErrorMessage="Please type Reason for Leaving." />
            </tr>
            <tr>
                <td colspan="2">
                    <ext:TextArea ID="txtNote" runat="server" FieldStyle="height:50px!important" Width="375px"
                        FieldLabel="Note" LabelAlign="Top" LabelSeparator="">
                    </ext:TextArea>
            </tr>
            <tr>
                <td valign="bottom" colspan="2">
                    <div class="popupButtonDiv">
                        <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="btnSave" Cls="btnFlat"
                            BaseCls="btnFlat" Text="<i></i>Save">
                            <DirectEvents>
                                <Click OnEvent="btnSave_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                            <Listeners>
                                <Click Handler="valGroup = 'EmploymentHistorySaveUpdate'; if(CheckValidation()) return this.disable(); else return false;">
                                </Click>
                            </Listeners>
                        </ext:LinkButton>
                        <div class="btnFlatOr">
                            or</div>
                        <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="LinkButton2" Cls="btnFlatLeftGap"
                            Text="<i></i>Cancel">
                            <Listeners>
                                <Click Handler="#{AEPreviousEmploymentWindow}.hide();">
                                </Click>
                            </Listeners>
                        </ext:LinkButton>
                    </div>
                </td>
            </tr>
        </table>
    </Content>
</ext:Window>
