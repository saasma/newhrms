﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="USeminarCtrl.ascx.cs" Inherits="Web.NewHR.UserControls.USeminarCtrl" %>

<%@ Register Src="~/NewHR/UserControls/SeminarPopUpCtrl.ascx" TagName="SeminarPopUp" TagPrefix="ucS" %>


<script type="text/javascript">
    var renderDuration = function (value, meta, record) {

        return record.data.Duration + " (" + record.data.DurationTypeName + ")";
    }

    var prepareSeminar = function (grid, toolbar, rowIndex, record) {

        btnEditDel = toolbar.items.get(0);

        if (record.data.IsEditable == 0) {
            btnEditDel.setVisible(false);
        }
    }

    function reloadSeminarGrid() {  
        <%= btnLoadGrid.ClientID %>.fireEvent('click');
    }


</script>

<ext:Hidden runat="server" ID="hdnSeminarID">
</ext:Hidden>

<ext:LinkButton ID="btnLoadGrid" runat="server" Hidden="true">
    <DirectEvents>
        <Click OnEvent="btnLoadGrid_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:LinkButton>

    <div class="widget-body">
        <table class="fieldTable firsttdskip">
            <tr>
                <td>
                    <ext:GridPanel ID="GridSeminar" runat="server" Width="1000" Cls="itemgrid" Scroll="None">
                        <Store>
                            <ext:Store ID="StoreSeminar" runat="server">
                                <Model>
                                    <ext:Model ID="Model1" runat="server" IDProperty="SeminarId">
                                        <Fields>
                                            <ext:ModelField Name="Organizer" Type="string" />
                                            <ext:ModelField Name="Country" Type="String" />
                                            <ext:ModelField Name="Place" Type="string" />
                                            <ext:ModelField Name="Duration" Type="string" />
                                            <ext:ModelField Name="DurationTypeName" Type="string" />
                                            <ext:ModelField Name="StartDate" Type="string" />
                                            <ext:ModelField Name="EndDate" Type="string" />
                                            <ext:ModelField Name="IsEditable" Type="Int" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:Column ID="Column1" runat="server" Text="Organizer" DataIndex="Organizer" Flex="1" Width="200" Wrap="true"/>
                                <ext:Column ID="Column2" runat="server" Text="Country" DataIndex="Country" Width="160" Wrap="true">
                                </ext:Column>
                                <ext:Column ID="Column3" runat="server" Text="Place" DataIndex="Place" Width="160" Wrap="true">
                                </ext:Column>
                                <ext:Column ID="Column4" runat="server" Text="Duration" DataIndex="Duration" Width="140" Wrap="true">
                                    <Renderer Fn="renderDuration" />
                                </ext:Column>
                                <ext:Column ID="Column6" runat="server" Text="Start Date" DataIndex="StartDate" Width="140">
                                </ext:Column>
                                <ext:Column ID="Column5" runat="server" Text="End Date" DataIndex="EndDate" Width="140">
                                </ext:Column>
                                <ext:CommandColumn ID="CommandColumn1" runat="server" Width="40">
                                    <Commands>
                                        <ext:GridCommand Cls="editGridButton" ToolTip-Text="Edit"  Text="<i class='fa fa-pencil'></i>" CommandName="Edit" />                                      
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridSeminar_Command">
                                            <EventMask ShowMask="true">
                                            </EventMask>
                                            <ExtraParams>
                                                <ext:Parameter Name="ID" Value="record.data.SeminarId" Mode="Raw">
                                                </ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw">
                                                </ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                     <PrepareToolbar Fn="prepareSeminar" />
                                </ext:CommandColumn>
                                <ext:CommandColumn ID="CommandColumn2" runat="server" Width="40">
                                    <Commands>                                      
                                        <ext:GridCommand Cls="editGridButton" ToolTip-Text="Delete"  Text="<i class='fa fa-trash-o'></i>"
                                            CommandName="Delete" />
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridSeminar_Command">
                                            <EventMask ShowMask="true">
                                            </EventMask>
                                            <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the record?" />
                                            <ExtraParams>
                                                <ext:Parameter Name="ID" Value="record.data.SeminarId" Mode="Raw">
                                                </ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw">
                                                </ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                     <PrepareToolbar Fn="prepareSeminar" />
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <SelectionModel>
                            <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                        </SelectionModel>
                    </ext:GridPanel>
                </td>
            </tr>
        </table>
        <div class="buttonBlockSection">
            <ext:Button runat="server" ID="btnAddNewLine" Cls="btn btn-primary btn-sect" runat="server" Width="120" StyleSpec="margin-top:0px" Height="30" Text="<i></i>Add Seminar">
                <DirectEvents>
                    <Click OnEvent="btnAddNewLine_Click">
                        <EventMask ShowMask="true" />
                    </Click>
                </DirectEvents>
            </ext:Button>
        </div>
    </div>


<ext:Window ID="AESeminarWindow" runat="server" Title="Add or Edit Seminar" Icon="Application"
    Height="450" Width="650"  BodyPadding="5"
    Hidden="true" Modal="true">
    <Content>

        <ucS:SeminarPopUp ID="ucSeminar" runat="server" />

    </Content>
</ext:Window>