﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FamilyPopupCtrl.ascx.cs"
    Inherits="Web.NewHR.UserControls.FamilyPopupCtrl" %>
<ext:Hidden runat="server" ID="hiddenValue" />
<script type="text/javascript">
    function titleChanged(title, cmbGender) {
        //if (cmbGender.getValue() == null) 
        {
            if (title == "Mr")
                cmbGender.setValue("1");
            else if (title == "Mrs" || title == "Miss")
                cmbGender.setValue("0");
        }
    }
</script>
<table class="fieldTable" border="0">
    <tr>
        <td>
            <ext:ComboBox ForceSelection="true" QueryMode="Local" ID="cmbTitle" FieldLabel="Title *"
                runat="server" LabelAlign="Top" DisplayField="Key" LabelSeparator=""
                ValueField="Value">
                <Store>
                    <ext:Store ID="Store2" runat="server">
                        <Fields>
                            <ext:ModelField Name="Key" />
                            <ext:ModelField Name="Value" />
                        </Fields>
                    </ext:Store>
                </Store>
                <Listeners>
                    <Select Handler="titleChanged(this.getValue(),#{cmbGender});">
                    </Select>
                </Listeners>
            </ext:ComboBox>
            <asp:RequiredFieldValidator ID="rfvcmbTitle" ErrorMessage="Please select employee title."
                Display="None" ControlToValidate="cmbTitle" ValidationGroup="SaveUpdateLevel" runat="server">
            </asp:RequiredFieldValidator>
        </td>
        <td>
            <ext:TextField ID="txtName" LabelSeparator="" runat="server" FieldLabel="Name *"
                Width="200" LabelAlign="Top">
            </ext:TextField>
            <asp:RequiredFieldValidator Display="None" ID="valcmbFaculty" runat="server" ValidationGroup="SaveUpdateLevel"
                ControlToValidate="txtName" ErrorMessage="Name is required." />
        </td>
        <td>
            <ext:ComboBox ID="cmbRelation" runat="server" ValueField="ID" DisplayField="Name"
                FieldLabel="Relation *" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                QueryMode="Local">
                <Store>
                    <ext:Store ID="Store1" runat="server">
                        <Model>
                            <ext:Model ID="Model3" runat="server">
                                <Fields>
                                    <ext:ModelField Name="ID" Type="String" />
                                    <ext:ModelField Name="Name" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
            </ext:ComboBox>
            <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator1" runat="server"
                ValidationGroup="SaveUpdateLevel" ControlToValidate="cmbRelation" ErrorMessage="Relation is required." />
        </td>
    </tr>
    <tr>
        <td>
            <pr:CalendarExtControl FieldLabel="Date Of Birth" ID="txtDOB" runat="server"
                LabelAlign="Top" LabelSeparator="">
                <DirectEvents>
                    <Change OnEvent="txtDOB_Blur" />
                    <Blur OnEvent="txtDOB_Blur" />
                </DirectEvents>
            </pr:CalendarExtControl>
            <%--   <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator2" runat="server"
                        ValidationGroup="SaveUpdateLevel" ControlToValidate="txtDOB" ErrorMessage="DOB is required." />--%>
        </td>
        <td>
            <ext:Checkbox BoxLabelAlign="After" BoxLabel="Dependent" ID="chkHasDependent"
                runat="server" LabelSeparator="" />
        </td>
        <td> 
            <ext:Checkbox runat="server" BoxLabelAlign="After" BoxLabel="For Insurance" ID="chkForIsurance" LabelSeparator="" />
        </td>
    </tr>
    <tr>
        <td>
            <ext:TextField ID="txtAgeOnSPDate" Disabled="true" LabelSeparator="" runat="server"
                FieldLabel="Age" Width="180" LabelAlign="Top">
            </ext:TextField>
        </td>
        <td>
            <ext:ComboBox ForceSelection="true" QueryMode="Local" ID="cmbGender" FieldLabel="Gender *"
                runat="server" LabelAlign="Top" LabelSeparator="">
                <Items>
                    <ext:ListItem Text="Male" Value="1" />
                    <ext:ListItem Text="Female" Value="0" />
                    <ext:ListItem Text="Third Gender" Value="2" />
                </Items>
            </ext:ComboBox>
            <asp:RequiredFieldValidator ErrorMessage="Please select a Gender." Display="None"
                ID="rfvcmbGender" ControlToValidate="cmbGender" ValidationGroup="SaveUpdateLevel"
                runat="server">
            </asp:RequiredFieldValidator>
        </td>
        <td>
            <ext:ComboBox ID="cmbBoodGroup" FieldLabel="Blood Group" runat="server" LabelAlign="Top"
                LabelSeparator="" QueryMode="Local" DisplayField="BloodGroupName" ValueField="Id">
                <Store>
                    <ext:Store ID="store3" runat="server">
                        <Fields>
                            <ext:ModelField Name="Id" Type="String" />
                            <ext:ModelField Name="BloodGroupName" />
                        </Fields>
                    </ext:Store>
                </Store>
            </ext:ComboBox>
            <%--<asp:RequiredFieldValidator ID="rfvcmbBoodGroup" ControlToValidate="cmbBoodGroup" Display="None"
                ErrorMessage="Blood group is required." runat="server" ValidationGroup="SaveUpdateLevel" />--%>
        </td>
    </tr>
    <tr>
        <td>
            <ext:ComboBox ID="cmbOccupation" runat="server" ValueField="OccupationId" DisplayField="Occupation"
                FieldLabel="Occupation" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                QueryMode="Local">
                <Store>
                    <ext:Store ID="storeOccupation" runat="server">
                        <Model>
                            <ext:Model ID="Model1" runat="server">
                                <Fields>
                                    <ext:ModelField Name="OccupationId" Type="String" />
                                    <ext:ModelField Name="Occupation" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
            </ext:ComboBox>
        </td>
        <td>
            <ext:TextField ID="txtContactNumber" FieldLabel="Contact Number " runat="server"
                LabelAlign="Top" LabelSeparator="">
            </ext:TextField>
            <asp:RegularExpressionValidator ID="valRegPhone" runat="server" ControlToValidate="txtContactNumber"
                ErrorMessage="Contact number is invalid." ValidationGroup="SaveUpdateLevel" Display="None"></asp:RegularExpressionValidator>
        </td>
        <td>
            <ext:ComboBox ID="cmbNationality" FieldLabel="Nationality *" runat="server" LabelAlign="Top"
                LabelSeparator="" QueryMode="Local" DisplayField="Nationality" ValueField="NationalityId">
                <Store>
                    <ext:Store ID="store4" runat="server">
                        <Fields>
                            <ext:ModelField Name="NationalityId" />
                            <ext:ModelField Name="Nationality" />
                        </Fields>
                    </ext:Store>
                </Store>
            </ext:ComboBox>
        </td>
    </tr>
    <tr>
        <td>
            <ext:ComboBox ID="cmbDocumentType" runat="server" ValueField="DocumentTypeId" DisplayField="DocumentTypeName"
                FieldLabel="Document Type" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                QueryMode="Local">
                <Store>
                    <ext:Store ID="store5" runat="server">
                        <Model>
                            <ext:Model ID="Model2" runat="server">
                                <Fields>
                                    <ext:ModelField Name="DocumentTypeId" Type="String" />
                                    <ext:ModelField Name="DocumentTypeName" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
            </ext:ComboBox>
        </td>
        <td>
            <pr:CalendarExtControl FieldLabel="Issue Date" ID="calDocumentIssueDate" runat="server"
                LabelAlign="Top" LabelSeparator="">
            </pr:CalendarExtControl>
        </td>
        <td>
            <ext:TextField ID="txtDocumentIssuePlace" FieldLabel="Issue Place" runat="server"
                LabelAlign="Top" LabelSeparator="">
            </ext:TextField>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <ext:TextArea ID="txtRemarks" Width="540" Rows="4" LabelSeparator="" runat="server"
                FieldLabel="Note " LabelAlign="Top">
            </ext:TextArea>
        </td>
    </tr>
    <tr>
        <td valign="bottom" colspan="2">
            <div class="popupButtonDiv">
                <ext:Button runat="server" ID="btnLevelSaveUpdate" Cls="btn btn-primary" Text="<i></i>Save">
                    <DirectEvents>
                        <Click OnEvent="btnLevelSaveUpdate_Click">
                            <EventMask ShowMask="true" />
                        </Click>
                    </DirectEvents>
                    <Listeners>
                        <Click Handler="valGroup = 'SaveUpdateLevel'; if(CheckValidation()) return this.disable(); else return false;">
                        </Click>
                    </Listeners>
                </ext:Button>
                <div class="btnFlatOr">
                    or
                </div>
                <ext:LinkButton ID="LinkButton1" StyleSpec="padding:0px;" runat="server" Text="Cancel"
                    Cls="btnFlatLeftGap">
                    <Listeners>
                        <Click Handler="#{WFamily}.hide();" />
                    </Listeners>
                </ext:LinkButton>
            </div>
        </td>
    </tr>
</table>
