﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GradeRewardCtl.ascx.cs"
    Inherits="Web.NewHR.UserControls.GradeRewardCtl" %>
<%@ Register Src="~/newhr/UserControls/EmployeeWizard.ascx" TagName="EmployeeWizard"
    TagPrefix="uc2" %>
<%@ Register Src="~/NewHR/UserControls/EmployeeDetailsCtl.ascx" TagName="EmployeeDetailsCtl"
    TagPrefix="uc1" %>
<%@ Register Src="~/NewHR/UserControls/EmpDetailsLeftMenu.ascx" TagName="EmpDetailsWizard"
    TagPrefix="ucEW" %>
<script type="text/javascript">


        function searchList() {
           <%=PagingToolbar1.ClientID %>.doRefresh();
        }
        
        
        var CommandHandler2 = function(command, record){
            <%= hdn.ClientID %>.setValue(record.data.GradeAdditionalBasicID);
                if(command=="Edit")
                {
                    <%= btnEditAdditionalBasic.ClientID %>.fireEvent('click');
                }
                


        }
        var CommandHandler = function(command, record){
        <%= hdn.ClientID %>.setValue(record.data.RewardID);
            if(command=="Edit")
            {
                <%= btnEdit.ClientID %>.fireEvent('click');
            }
            else
            {
                <%= btnDelete.ClientID %>.fireEvent('click');
            }

            }
        function refreshWindow() {
            searchList();
        }

        function expandAllCollapseAllHandler(btn) {
           
            if (btn.getText() == "expand") {
                btn.setText("collapse");

                <%= Group1.ClientID %>.expandAll();

            }
            else {
                btn.setText("expand");


                <%= Group1.ClientID %>.collapseAll();
            }

            searchList();
        }
</script>

<style type="text/css">
    .userDetailsClass
        {
            background-color:#D8E7F3; 
            height:50px; text-align:center; 
            padding-top:10px; 
        }
</style>

<ext:LinkButton runat="server" Hidden="true" ID="btnEdit" runat="server">
    <DirectEvents>
        <Click OnEvent="btnEditLevel_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:LinkButton>
<ext:LinkButton runat="server" Hidden="true" ID="btnEditAdditionalBasic" runat="server">
    <DirectEvents>
        <Click OnEvent="btnEditAdditionalBasic_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:LinkButton>
<ext:Button ID="btnDelete" runat="server" Hidden="true">
    <DirectEvents>
        <Click OnEvent="btnDeleteLevel_Click">
            <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the Reward?" />
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:Button>
<ext:Hidden runat="server" ID="hdn">
</ext:Hidden>
<ext:Store ID="StoreLevel" runat="server">
    <Model>
        <ext:Model ID="Model1" runat="server" IDProperty="LevelId">
            <Fields>
                <ext:ModelField Name="LevelId" Type="String" />
                <ext:ModelField Name="GroupLevel" />
            </Fields>
        </ext:Model>
    </Model>
</ext:Store>
<ext:Store ID="StoreIncome" runat="server">
    <Model>
        <ext:Model ID="Model4" runat="server" IDProperty="IncomeId">
            <Fields>
                <ext:ModelField Name="IncomeId" Type="String" />
                <ext:ModelField Name="Title" />
            </Fields>
        </ext:Model>
    </Model>
</ext:Store>
<ext:Store ID="StoreBranch" runat="server">
    <Model>
        <ext:Model ID="Model3" runat="server" IDProperty="BranchId">
            <Fields>
                <ext:ModelField Name="BranchId" Type="String" />
                <ext:ModelField Name="Name" />
            </Fields>
        </ext:Model>
    </Model>
</ext:Store>
<div class="pageheader">
    <div class="media">
        <div class="media-body" style="margin-left:145px;">
            <h4 class="heading" runat="server" id="headerAdditionalBasic">
                Additional Incomes and Rewards</h4>
        </div>
    </div>
</div>
<div class="contentpanel">
  
    <%-- <uc2:EmployeeWizard Id="EmployeeWizard1" Visible="false" runat="server" />--%>
    <ext:Label ID="lblMsg" runat="server" />
    <table>
        <tr>
            <td valign="top">
                <div style="float: left; margin-right: 25px;">
                    <ucEW:EmpDetailsWizard Id="EmpDetailsWizard1" runat="server" />
                </div>
            </td>
            <td valign="top" style="width:100%;">
                  <div class="userDetailsClass">
                            <uc1:EmployeeDetailsCtl Id="EmployeeDetailsCtl1" runat="server" />
                          </div>

                <div style="width: 950px">
                  

                    <div style="margin-bottom: 0px; width: 1001px; margin-left: 0px; margin-bottom:25px;">
                        <h4 class="sectionHeading" style="margin-left:0px;">
                            Aditional Incomes</h4>
                    </div>
                    <ext:GridPanel StyleSpec="margin-top:25px;" ID="gridAdditionalBasic" Hidden="true"
                        runat="server" Cls="itemgrid">
                        <Store>
                            <ext:Store ID="Store1" PageSize="50" runat="server" GroupField="Name">
                                <Model>
                                    <ext:Model ID="Model5" runat="server" IDProperty="GradeAdditionalBasicID">
                                        <Fields>
                                            <ext:ModelField Name="GradeAdditionalBasicID" Type="string" />
                                            <ext:ModelField Name="Level" Type="string" />
                                            <ext:ModelField Name="Income" Type="string" />
                                            <ext:ModelField Name="Amount" Type="string" />
                                            <ext:ModelField Name="Notes" Type="string" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:Column ID="Column7" Width="200" Sortable="false" MenuDisabled="true" runat="server" Wrap="true"
                                    Text="Level Name" Align="Left" DataIndex="Level">
                                </ext:Column>
                                <ext:Column ID="Column4" Width="150" Sortable="false" MenuDisabled="true" runat="server" Wrap="true"
                                    Text="Income" Align="Left" DataIndex="Income">
                                </ext:Column>
                                <ext:Column ID="Column12" Width="100" Sortable="false" MenuDisabled="true" runat="server" Wrap="true"
                                    Text="Amount" Align="Right" DataIndex="Amount">
                                    <Renderer Fn="getFormattedAmount" />
                                </ext:Column>
                                <ext:Column ID="Column13" Width="470" Sortable="false" MenuDisabled="true" runat="server" Wrap="true"
                                    Text="Notes" Align="Left" DataIndex="Notes">
                                </ext:Column>
                                <ext:CommandColumn ID="CommandColumn2" runat="server" Width="80" Text="" Align="Center">
                                    <Commands>
                                        <ext:GridCommand Cls="editGridButton" ToolTip-Text="Edit" Text="<i class='fa fa-pencil'></i>"
                                            CommandName="Edit" />
                                        <ext:GridCommand Hidden="true" Cls="editGridButton" ToolTip-Text="Delete" Text="<i class='fa fa-trash-o'></i>"
                                            CommandName="Delete" />
                                    </Commands>
                                    <Listeners>
                                        <Command Handler="CommandHandler2(command,record);" />
                                    </Listeners>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>
                </div>
                <div class="buttonBlockSection" runat="server" id="upperDiv" style="display: none;
                    margin-bottom: 10px;">
                    <div class="left">
                        <ext:Button Cls="btn btn-primary btn-sect" runat="server" ID="btnAddAdditionalBasic"
                            Text="<i></i>Add Additional Income" runat="server">
                            <DirectEvents>
                                <Click OnEvent="btnAddAdditionalBasic_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                    </div>
                </div>
                <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                    <Proxy>
                        <ext:AjaxProxy Url="../../Handler/EmpSearch.ashx">
                            <ActionMethods Read="GET" />
                            <Reader>
                                <ext:JsonReader Root="plants" TotalProperty="total" />
                            </Reader>
                        </ext:AjaxProxy>
                    </Proxy>
                    <Model>
                        <ext:Model ID="Model2" IDProperty="Name" Name="ItemLineModel" runat="server">
                            <Fields>
                                <ext:ModelField Name="Name" Type="String" />
                                <ext:ModelField Name="EmployeeId" Type="String" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
                <table class="fieldTable firsttdskip" runat="server" id="tbl">
                    <tr>
                        <td>
                            <ext:ComboBox ID="cmbSearch" FieldLabel="Search Employee" LabelWidth="70" LabelAlign="Top"
                                runat="server" DisplayField="Name" ValueField="EmployeeId" StoreID="storeSearch"
                                TypeAhead="false" Width="180" PageSize="9999" HideBaseTrigger="true" MinChars="1"
                                TriggerAction="All" ForceSelection="true">
                                <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                                    <ItemTpl ID="ItemTpl1" runat="server">
                                        <Html>
                                            <tpl>
                                            <div class="search-item">
                                                            <span>{Name}</span>  
                                             </div>
					                </tpl>
                                        </Html>
                                    </ItemTpl>
                                </ListConfig>
                                <Triggers>
                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                </Triggers>
                                <Listeners>
                                    <Select Handler="this.getTrigger(0).show();" />
                                    <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                    <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                                </Listeners>
                            </ext:ComboBox>
                        </td>
                        <td>
                            <ext:ComboBox FieldLabel="Level" ID="cmbLevelList" Width="180px" runat="server" ValueField="LevelId"
                                DisplayField="GroupLevel" StoreID="StoreLevel" LabelAlign="Top" LabelSeparator=""
                                ForceSelection="true" QueryMode="Local">
                                <Triggers>
                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                </Triggers>
                                <Listeners>
                                    <Select Handler="this.getTrigger(0).show();" />
                                    <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                    <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                                </Listeners>
                            </ext:ComboBox>
                        </td>
                        <td>
                            <ext:ComboBox FieldLabel="Branch" ID="cmbBranch" Width="180px" runat="server" ValueField="BranchId"
                                DisplayField="Name" StoreID="StoreBranch" LabelAlign="Top" LabelSeparator=""
                                ForceSelection="true" QueryMode="Local">
                                <Triggers>
                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                </Triggers>
                                <Listeners>
                                    <Select Handler="this.getTrigger(0).show();" />
                                    <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                    <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                                </Listeners>
                            </ext:ComboBox>
                        </td>
                        <td style="padding-top: 20px">
                            <ext:Button runat="server" ID="btnLoad" Cls="btn btn-primary" Text="<i></i>Load"
                                runat="server">
                                <Listeners>
                                    <Click Handler="searchList();" />
                                </Listeners>
                            </ext:Button>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ext:LinkButton runat="server" ID="expandAllCollapseAll" Text="expand">
                                <Listeners>
                                    <Click Handler="expandAllCollapseAllHandler(this);" />
                                </Listeners>
                            </ext:LinkButton>
                        </td>
                    </tr>
                </table>
                <div style="clear: both; padding-top: 10px;">
                </div>
                <div style="width: 1001px">
                    <h4 class="sectionHeading" style="margin-left: 0px;">
                        Reward List</h4>
                    <ext:GridPanel StyleSpec="margin-top:25px; width:1000px;" ID="gridRewardList" runat="server"
                        Cls="itemgrid">
                        <Store>
                            <ext:Store ID="gridRewardStore" PageSize="50" runat="server" GroupField="Name">
                                <Proxy>
                                    <ext:AjaxProxy Url="~/Handler/RewardListHandler.ashx">
                                        <ActionMethods Read="GET" />
                                        <Reader>
                                            <ext:JsonReader Root="data" TotalProperty="total" />
                                        </Reader>
                                    </ext:AjaxProxy>
                                </Proxy>
                                <AutoLoadParams>
                                    <%-- <ext:Parameter Name="UserName" Mode="Raw" Value="#{hdUserName}.getValue()" />--%>
                                    <ext:Parameter Name="start" Value="={0}" />
                                    <ext:Parameter Name="limit" Value="={50}" />
                                </AutoLoadParams>
                                <Parameters>
                                    <ext:StoreParameter Name="EmployeeId" Value="#{cmbSearch}.getValue() == null ? '-1' : #{cmbSearch}.getValue()"
                                        Mode="Raw" />
                                    <ext:StoreParameter Name="LevelId" Mode="Raw" Value="#{cmbLevelList}.getValue() == null ? '-1' : #{cmbLevelList}.getValue()" />
                                    <ext:StoreParameter Name="BranchId" Mode="Raw" Value="#{cmbBranch}.getValue() == null ? '-1' : #{cmbBranch}.getValue()" />
                                </Parameters>
                                <Model>
                                    <ext:Model Root="data" IDProperty="RewardID">
                                        <Fields>
                                            <ext:ModelField Name="RewardID" Type="string" />
                                            <ext:ModelField Name="Name" Type="string" />
                                            <%-- <ext:ModelField Name="Branch" Type="string" />--%>
                                            <ext:ModelField Name="RewardDate" Type="string" />
                                            <ext:ModelField Name="RewardPoints" Type="string" />
                                            <ext:ModelField Name="RewardRate" Type="string" />
                                            <ext:ModelField Name="Amount" Type="string" />
                                            <ext:ModelField Name="LevelId" Type="string" />
                                            <ext:ModelField Name="Level" Type="string" />
                                            <ext:ModelField Name="Branch" Type="string" />
                                            <ext:ModelField Name="Notes" Type="string" />
                                            <ext:ModelField Name="StatusText" Type="string" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:Column ID="Column_Employee" Sortable="true" runat="server" Text="Employee" Align="Left" Wrap="true"
                                    Width="150" DataIndex="Name" Flex="1" Hideable="false" />
                                <ext:Column ID="Column_Level" Width="200" Sortable="false" MenuDisabled="true" runat="server" Wrap="true"
                                    Text="Employee Level" Align="Left" DataIndex="Level">
                                </ext:Column>
                                <ext:Column ID="Column8" Sortable="false" MenuDisabled="true" runat="server" Text="Date" Wrap="true"
                                    Align="Left" Width="70" DataIndex="RewardDate" />
                                <ext:Column ID="Column6" Width="70" Sortable="false" MenuDisabled="true" runat="server" Wrap="true"
                                    Text="Points" Align="Center" DataIndex="RewardPoints">
                                </ext:Column>
                                <ext:Column ID="Column2" Width="70" Sortable="false" MenuDisabled="true" runat="server" Wrap="true"
                                    Text="Rate" Align="Right" DataIndex="RewardRate">
                                    <Renderer Fn="getFormattedAmount" />
                                </ext:Column>
                                <ext:Column ID="Column3" Width="100" Sortable="false" MenuDisabled="true" runat="server" Wrap="true"
                                    Text="Amount" Align="Right" DataIndex="Amount">
                                    <Renderer Fn="getFormattedAmount" />
                                </ext:Column>
                                <ext:Column ID="Column1" Width="310" Sortable="false" MenuDisabled="true" runat="server" Wrap="true"
                                    Text="Notes" Align="Left" DataIndex="Notes">
                                </ext:Column>
                                <ext:Column ID="Column5" Width="100" Sortable="false" MenuDisabled="true" runat="server" Wrap="true"
                                    Text="Active" Align="Left" DataIndex="StatusText">
                                </ext:Column>
                                <ext:CommandColumn ID="CommandColumn1" runat="server" Width="80" Text="" Align="Center">
                                    <Commands>
                                        <ext:CommandSeparator />
                                        <ext:GridCommand Cls="editGridButton" ToolTip-Text="Edit" Text="<i class='fa fa-pencil'></i>"
                                            CommandName="Edit" />
                                        <ext:GridCommand Cls="editGridButton" ToolTip-Text="Delete" Text="<i class='fa fa-trash-o'></i>"
                                            CommandName="Delete" />
                                    </Commands>
                                    <Listeners>
                                        <Command Handler="CommandHandler(command,record);" />
                                    </Listeners>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <Features>
                            <ext:Grouping StartCollapsed="true" ID="Group1" runat="server" GroupHeaderTplString="{name}"
                                HideGroupedHeader="true" EnableGroupingMenu="false" />
                        </Features>
                        <View>
                            <ext:GridView ID="GridView1" runat="server">
                                <%--<GetRowClass Fn="getRowClass" />--%>
                            </ext:GridView>
                        </View>
                        <SelectionModel>
                            <ext:RowSelectionModel ID="RowSelectionModel2111" runat="server" Mode="Single" />
                        </SelectionModel>
                        <BottomBar>
                            <ext:PagingToolbar PageSize="50" ID="PagingToolbar1" DisplayInfo="true" DisplayMsg="Displaying record {0} - {1} of {2}"
                                EmptyMsg="No record to display" runat="server">
                                <%-- <Plugins>
                    <ext:GroupPaging ID="GroupPaging1" runat="server" />
                </Plugins>--%>
                            </ext:PagingToolbar>
                        </BottomBar>
                    </ext:GridPanel>
                </div>
                <div class="buttonBlockSection">
                    <div class="left">
                        <ext:LinkButton runat="server" StyleSpec="padding:0px;margin-top:0px;" ID="btnAdd"
                            Cls="btn btn-primary btn-sect btn-sm" BaseCls="" Text="<i></i>Add Reward" runat="server">
                            <DirectEvents>
                                <Click OnEvent="btnAdd_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                        </ext:LinkButton>
                    </div>
                    <div class="right">
                        <ext:Button ID="btnExportPopup1" runat="server" Text="Import" OnClientClick="employeePopup();return false;"
                            StyleSpec="float: left;" />
                    </div>
                </div>
                <div style="clear: both">
                </div>
            </td>
        </tr>
    </table>
</div>
<ext:Window ID="window" runat="server" Title="Add Reward" Icon="Application" Height="420"
    Width="500" BodyPadding="5" Hidden="true" Modal="true">
    <Content>
        <table class="fieldTable">
            <tr>
                <td runat="server" id="employeeTD">
                    <ext:ComboBox ID="cmbGradeEmployee" LabelSeparator="" FieldLabel="Employee" LabelWidth="70"
                        LabelAlign="Top" runat="server" DisplayField="Name" ValueField="EmployeeId" StoreID="storeSearch"
                        TypeAhead="false" Width="180" PageSize="9999" HideBaseTrigger="true" MinChars="1"
                        TriggerAction="All" ForceSelection="true">
                        <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                            <ItemTpl ID="ItemTpl2" runat="server">
                                <Html>
                                    <tpl>
                                            <div class="search-item">
                                                            <span>{Name}</span>  
                                             </div>
					                </tpl>
                                </Html>
                            </ItemTpl>
                        </ListConfig>
                        <DirectEvents>
                            <Select OnEvent="cmbGradeEmployee_Select">
                                <EventMask ShowMask="true" />
                            </Select>
                        </DirectEvents>
                    </ext:ComboBox>
                    <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator3" runat="server"
                        ValidationGroup="SaveUpdate" ControlToValidate="cmbGradeEmployee" ErrorMessage="Employee is required." />
                </td>
                <td>
                    <ext:DisplayField ID="dispLevel" LabelSeparator="" LabelAlign="Top" runat="server"
                        FieldLabel="Level">
                    </ext:DisplayField>
                </td>
            </tr>
            <tr>
                <td>
                    <pr:CalendarExtControl FieldLabel="Date" Width="180px" ID="calDate" runat="server"
                        LabelSeparator="" LabelAlign="Top" />
                    <asp:RequiredFieldValidator Display="None" ID="valcalStartDate" runat="server" ValidationGroup="SaveUpdate"
                        ControlToValidate="calDate" ErrorMessage="Date is required." />
                </td>
            </tr>
            <tr>
                <td>
                    <ext:NumberField FieldLabel="Points" Width="180px" ID="txtPoints" runat="server"
                        LabelSeparator="" LabelAlign="Top" MinValue="0" />
                </td>
                <td>
                    <ext:TextField FieldLabel="Rate" Width="180px" ID="txtRate" runat="server" LabelSeparator=""
                        LabelAlign="Top" />
                    <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator1" runat="server"
                        ValidationGroup="SaveUpdate" ControlToValidate="txtRate" ErrorMessage="Rate is required." />
                    <asp:CompareValidator Display="None" ID="RequiredFieldValidator2" runat="server"
                        Type="Currency" Operator="GreaterThan" ValueToCompare="0" ValidationGroup="SaveUpdate"
                        ControlToValidate="txtRate" ErrorMessage="Invalid rate." />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <ext:TextArea ID="txtNotes" runat="server" FieldLabel="Notes" LabelSeparator="" LabelAlign="Top"
                        Rows="3" Cols="55" />
                </td>
            </tr>
            <tr>
                <td valign="bottom" colspan="2">
                    <div class="popupButtonDiv">
                        <ext:Button runat="server" ID="btnSave" Cls="btn btn-primary" Text="<i></i>Save">
                            <DirectEvents>
                                <Click OnEvent="btnSave_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                            <Listeners>
                                <Click Handler="valGroup = 'SaveUpdate'; if(CheckValidation()) return this.disable(); else return false;">
                                </Click>
                            </Listeners>
                        </ext:Button>
                        <div class="btnFlatOr">
                            or</div>
                        <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="LinkButton2" Cls="btnFlatLeftGap"
                            Text="<i></i>Cancel">
                            <Listeners>
                                <Click Handler="#{window}.hide();">
                                </Click>
                            </Listeners>
                        </ext:LinkButton>
                    </div>
                </td>
            </tr>
        </table>
    </Content>
</ext:Window>
<ext:Window ID="windowAdditionalDate" runat="server" Title="Add Additional Income"
    Icon="Application" Height="420" Width="500" BodyPadding="5" Hidden="true" Modal="true">
    <Content>
        <table class="fieldTable">
            <tr>
                <td>
                    <ext:DisplayField ID="dispAdditionalBasicLevel" LabelSeparator="" LabelAlign="Top"
                        runat="server" FieldLabel="Level">
                    </ext:DisplayField>
                </td>
            </tr>
            <tr>
                <td>
                    <ext:ComboBox FieldLabel="Income" ID="cmbIncomes" Width="180px" runat="server" ValueField="IncomeId"
                        DisplayField="Title" StoreID="StoreIncome" LabelAlign="Top" LabelSeparator=""
                        ForceSelection="true" QueryMode="Local">
                        <%--  <Triggers>
                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                        </Triggers>
                        <Listeners>
                            <Select Handler="this.getTrigger(0).show();" />
                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                            <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                        </Listeners>--%>
                    </ext:ComboBox>
                    <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator4" runat="server"
                        ValidationGroup="SaveUpdateAdditionalBasic" ControlToValidate="cmbIncomes" ErrorMessage="Income type is required." />
                </td>
            </tr>
            <tr>
                <td>
                    <ext:TextField FieldLabel="Amount" Width="180px" ID="txtAdditionalBasicAmount" runat="server"
                        LabelSeparator="" LabelAlign="Top" />
                    <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator6" runat="server"
                        ValidationGroup="SaveUpdateAdditionalBasic" ControlToValidate="txtAdditionalBasicAmount"
                        ErrorMessage="Amount is required." />
                    <asp:CompareValidator Display="None" ID="CompareValidator1" runat="server" Type="Currency"
                        Operator="GreaterThan" ValueToCompare="0" ValidationGroup="SaveUpdate" ControlToValidate="txtAdditionalBasicAmount"
                        ErrorMessage="Invalid amount." />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <ext:TextArea ID="txtNotesAddiotionalBasic" runat="server" FieldLabel="Notes" LabelSeparator=""
                        LabelAlign="Top" Rows="3" Cols="55" />
                </td>
            </tr>
            <tr>
                <td valign="bottom" colspan="2">
                    <div class="popupButtonDiv">
                        <ext:Button runat="server" Cls="btn btn-primary" ID="LinkButton1" Text="<i></i>Save">
                            <DirectEvents>
                                <Click OnEvent="btnSaveAdditionalBasic_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                            <Listeners>
                                <Click Handler="valGroup = 'SaveUpdateAdditionalBasic'; if(CheckValidation()) return this.disable(); else return false;">
                                </Click>
                            </Listeners>
                        </ext:Button>
                        <div class="btnFlatOr">
                            or</div>
                        <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="LinkButton3" Cls="btnFlatLeftGap"
                            Text="<i></i>Cancel">
                            <Listeners>
                                <Click Handler="#{windowAdditionalDate}.hide();">
                                </Click>
                            </Listeners>
                        </ext:LinkButton>
                    </div>
                </td>
            </tr>
        </table>
    </Content>
</ext:Window>
