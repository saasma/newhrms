﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using Utils.Helper;
using System.IO;
using Utils;
using Utils.Security;
using System.Drawing;

namespace Web.NewHR.UserControls
{
    public partial class DocumentPopupCtrl : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        public void Initialise()
        {
            cmbType.Store[0].DataSource = CommonManager.GetDocumentDocumentTypeList();
            cmbType.Store[0].DataBind();
        }

        public void ClearLevelFields()
        {
            hiddenValue.Text = "";
            txtDesc.Text = "";
            fup.Reset();
        }


        protected void btnLevelSaveUpdate_Click(object sender, DirectEventArgs e)
        {
            if (fup.HasFile)
            {
                string ext = Path.GetExtension(fup.FileName);

                if (ext.ToLower() == ".exe")
                {
                    fup.Reset();
                    fup.Focus();
                    NewMessage.ShowWarningMessage("Please upload proper document.");
                    return;
                }

                string guidFileName = Guid.NewGuid().ToString() + ext;
                string saveLocation = Path.Combine(Server.MapPath(Config.UploadLocation), guidFileName);


                string code = "closeWindow(\"{0}\");";
                HRManager mgr = new HRManager();
                
                try
                {
                    guidFileName = guidFileName.Replace(Path.GetExtension(guidFileName), ".ashx");
                    saveLocation = saveLocation.Replace(Path.GetExtension(saveLocation), ".ashx");

                    EncryptorDecryptor enc = new EncryptorDecryptor();
                    enc.Encrypt(fup.FileContent, saveLocation);

                    HDocument doc = new HDocument();

                    if (SessionManager.CurrentLoggedInEmployeeId == 0)
                        doc.EmployeeId = int.Parse(Request.QueryString["id"]);
                    else
                        doc.EmployeeId = SessionManager.CurrentLoggedInEmployeeId;

                    doc.Name = Path.GetFileName(fup.FileName);

                    if (!string.IsNullOrEmpty(txtDesc.Text.Trim()))
                    {
                        doc.Description = txtDesc.Text.Trim();
                    }
                    else
                        doc.Description = Path.GetFileNameWithoutExtension(fup.FileName);

                    doc.ContentType = fup.PostedFile.ContentType;
                    doc.Url = guidFileName;
                    doc.Size = GetSize(fup.PostedFile.ContentLength);
                    doc.DocumentType = int.Parse(cmbType.SelectedItem.Value);
                    doc.CreatedBy = SessionManager.User.UserID;
                    doc.CreatedOn = DateTime.Now;
                    doc.Modifiedby = SessionManager.User.UserID;
                    doc.ModifiedOn = DateTime.Now;

                    if (SessionManager.CurrentLoggedInEmployeeId == 0)
                    {
                        doc.Status = (int)HRStatusEnum.Approved;
                        doc.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                        doc.ApprovedOn = DateTime.Now;
                    }
                    else
                    {
                        doc.Status = (int)HRStatusEnum.Saved;
                    }

                    if (!string.IsNullOrEmpty(hiddenValue.Text))
                    {
                        doc.Modifiedby = SessionManager.CurrentLoggedInUserID;
                        doc.ModifiedOn = DateTime.Now;
                    }

                    HRManager objHRManager = new HRManager();
                    bool status = objHRManager.SaveDocument(doc);
                    if (status)
                    {
                        Window win = (Window)this.Parent.FindControl("WDocument");
                        win.Close();
                        NewMessage.ShowNormalMessage("Record Saved Successfully.", "reloadDocumentGrid()");
                    }
                    else
                        NewMessage.ShowWarningMessage("Error while saving document.");


                }
                catch (Exception exp1)
                {
                    JavascriptHelper.DisplayClientMsg(exp1.Message, Page);
                }

            }
        }


        public static bool IsImage(string pExtension)
        {
            if ((pExtension.ToLower() == ".jpg" | pExtension.ToLower() == ".gif" | pExtension.ToLower() == ".bmp" | pExtension.ToLower() == ".jpeg" | pExtension.ToLower() == ".png"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public string GetSize(int byteValue)
        {
            if (byteValue < 1024)
                return "1 kb";

            double kb = ConvertBytesIntoKB(byteValue);
            if (kb > 1024)
            {
                return ConvertKBIntoMB(kb) + " mb";
            }
            else
            {
                return kb + " kb";
            }
        }

        public static int ConvertBytesIntoKB(int byteValue)
        {
            double mb = (byteValue / 1024);
            if (mb.ToString("#") == "")
                return 0;
            return Convert.ToInt32(mb.ToString("#"));
        }
        public static string ConvertKBIntoMB(object value)
        {
            long kb = Convert.ToInt64(value);
            double mb = (kb / 1024.0);
            return mb.ToString("#.#");
        }

        public void HideButtons()
        {
            btnPositionSaveUpdate.Hide();
            LinkButton4.Hide();
        }

        public void EditDoc(int documentId)
        {
            HDocument doc = new NewHRManager().GetDocument(documentId);
            if (doc != null)
            {
                cmbType.SetValue(doc.DocumentType.Value.ToString());
                txtDesc.Text = doc.Description;
                fup.Text = doc.Name;
            }

        }

    }
}