﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmpPayInfoCtl.ascx.cs"
    Inherits="Web.NewHR.UserControls.EmpPayInfoCtl" %>

    <!-- panel-heading -->
    
    <div class="widget-body">
        
            <table class="fieldTable firsttdskip">
                <tr>
                    <td style="width: 130px;">
                        Pan No
                    </td>
                    <td>
                        <span runat="server" id="spnPanNo"></span>
                        <ext:DisplayField ID="dispPanNo" runat="server">
                        </ext:DisplayField>
                    </td>
                </tr>
                <tr>
                    <td>
                        CIT No
                    </td>
                    <td>
                        <ext:DisplayField ID="disCITNo" runat="server">
                        </ext:DisplayField>
                    </td>
                </tr>
                <tr>
                    <td>
                        PF No
                    </td>
                    <td>
                        <ext:DisplayField ID="dispPFNo" runat="server">
                        </ext:DisplayField>
                    </td>
                </tr>
                <tr>
                    <td>
                        Account No
                    </td>
                    <td>
                        <ext:DisplayField ID="dispAccountNo" runat="server">
                        </ext:DisplayField>
                    </td>
                </tr>
                <tr>
                    <td>
                        PF Start Date
                    </td>
                    <td>
                        <ext:DisplayField ID="dispPFStartDate" runat="server">
                        </ext:DisplayField>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:DisplayField ID="dispLabelGratuityStartDate" runat="server" Hidden="true" Text="Gratuity Start Date">
                        </ext:DisplayField>
                    </td>
                    <td>
                        <ext:DisplayField ID="dispGratuityStartDate" runat="server" Hidden="true">
                        </ext:DisplayField>
                    </td>
                </tr>
            </table>
        
    
</div>
