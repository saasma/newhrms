﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ActingCtrlView.ascx.cs" Inherits="Web.NewHR.UserControls.ActingCtrlView" %>

<ext:GridPanel StyleSpec="margin-top:15px;" ID="GridLevels" runat="server" Cls="itemgrid">
    <Store>
        <ext:Store ID="Store3" runat="server">
            <Model>
                <ext:Model ID="Model4" runat="server" IDProperty="ActingID">
                    <Fields>
                        <ext:ModelField Name="ActingID" Type="String" />
                        <ext:ModelField Name="EmployeeId" Type="String" />
                        <ext:ModelField Name="INo" Type="String" />
                        <ext:ModelField Name="EmployeeName" Type="string" />
                        <ext:ModelField Name="LetterNumber" Type="string" />
                        <ext:ModelField Name="LetterDate" Type="string" />
                        <ext:ModelField Name="ApplicableFrom" Type="string" />
                        <ext:ModelField Name="ApplicableTill" Type="string" />
                        <ext:ModelField Name="FromLevel" Type="string" />
                        <ext:ModelField Name="ToLevel" Type="string" />
                        <ext:ModelField Name="Amount" Type="string" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
    </Store>
    <ColumnModel>
        <Columns>
            <ext:Column ID="Column9" Sortable="false" MenuDisabled="true" runat="server" Text="EIN"
                Align="Left" Width="50" DataIndex="EmployeeId" />
            <ext:Column ID="Column10" Sortable="false" MenuDisabled="true" runat="server" Text="I No"
                Align="Left" Width="50" DataIndex="INo" />
            <ext:Column ID="Column8" Sortable="false" MenuDisabled="true" runat="server" Text="Employee"
                Align="Left" Width="180" DataIndex="EmployeeName" />
            <ext:Column ID="Column5" Sortable="false" MenuDisabled="true" runat="server" Text="Letter No"
                Align="Left" Width="100" DataIndex="LetterNumber" />
            <ext:Column ID="Column6" Width="100" Sortable="false" MenuDisabled="true" runat="server"
                Text="LetterDate" Align="Left" DataIndex="LetterDate">
            </ext:Column>
            <ext:Column ID="Column2" Width="120" Sortable="false" MenuDisabled="true" runat="server"
                Text="Applicable From" Align="Left" DataIndex="ApplicableFrom">
            </ext:Column>
            <ext:Column ID="Column3" Width="120" Sortable="false" MenuDisabled="true" runat="server"
                Text="Applicable Till" Align="Left" DataIndex="ApplicableTill">
            </ext:Column>
            <ext:Column ID="Column1" Width="130" Sortable="false" MenuDisabled="true" runat="server"
                Text="From Level" Align="Left" DataIndex="FromLevel">
            </ext:Column>
            <ext:Column ID="Column4" Width="130" Sortable="false" MenuDisabled="true" runat="server"
                Text="To Level" Align="Left" DataIndex="ToLevel">
            </ext:Column>
            <ext:Column ID="Column7" Width="100" Hidden="true" Sortable="false" MenuDisabled="true"
                runat="server" Text="Amount" Align="Left" DataIndex="Amount">
                <Renderer Fn="getFormattedAmount" />
            </ext:Column>
        </Columns>
    </ColumnModel>
    <SelectionModel>
        <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
    </SelectionModel>
</ext:GridPanel>
