﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;

using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;

namespace Web.NewHR.UserControls
{
    public partial class DrivingLicenseCtrlView : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }
        protected void Initialise()
        {
            int EmployeeID = GetEmployeeID();
            this.LoadDrivingLiscenceGrid(EmployeeID);
        }

        protected void DrivingLiscenceDownLoad(int ID)
        {
            HDrivingLicence doc = NewHRManager.GetDrivingLicenceDetailsById(ID);

            string path = Context.Server.MapPath(@"~/uploads/" + doc.FileLocation + doc.ServerFileName);
            string contentType = doc.FileType;
            string name = doc.UserFileName;

            byte[] bytes = File.ReadAllBytes(path);

            Context.Response.Clear();
            Context.Response.ClearHeaders();
            Context.Response.AppendHeader("Content-Disposition", "attachment; filename=\"" + name + "\"");
            Context.Response.ContentType = contentType;
            Context.Response.BinaryWrite(bytes);
            Context.Response.End();
        }

        protected void LoadDrivingLiscenceGrid(int EmployeeID)
        {
            List<HDrivingLicence> _HDrivingLiscence = NewHRManager.GetDrivingLicenceByEmployeeID(EmployeeID);
            if (_HDrivingLiscence != null)
            {
                this.StoreDrivingLiscence.DataSource = _HDrivingLiscence;
                this.StoreDrivingLiscence.DataBind();
            }
            else
            {
                this.StoreDrivingLiscence.DataSource = this.DrivingLiscenceFillData;
                this.StoreDrivingLiscence.DataBind();
            }

            if (_HDrivingLiscence.Count <= 0)
                GridDrivingLiscence.Hide();
        }

        protected int GetEmployeeID()
        {
            int EmpID = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                EmpID = int.Parse(Request.QueryString["ID"]);
            }
            else
                EmpID = SessionManager.CurrentLoggedInEmployeeId;

            return EmpID;
        }

        private object[] DrivingLiscenceFillData
        {
            get
            {
                return new object[]
            {
                new object[] { "", "", "",""},
               
            };
            }
        }

        protected void btnDownloadDLiFile_Click(object sender, EventArgs e)
        {
            int drivingLicenseId = int.Parse(hdnDrivingLicenceIdValue.Text);
            DrivingLiscenceDownLoad(drivingLicenseId);
        }

    }
}