﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmpPayIncomeInfoCtl.ascx.cs"
    Inherits="Web.NewHR.UserControls.EmpPayIncomeInfoCtl" %>
<ext:Hidden runat="server" ID="hdnCitizenshipID">
</ext:Hidden>
<ext:Hidden runat="server" ID="hdnIsEmployee" />
<!-- panel-heading -->
<div class="widget-body">
    <table class="fieldTable firsttdskip">
        <tr>
            <td>
                <ext:GridPanel ID="gridIncomeList" runat="server" Width="400" Cls="itemgrid">
                    <Store>
                        <ext:Store ID="storeIncomeList" runat="server">
                            <Model>
                                <ext:Model ID="Model1" runat="server">
                                    <Fields>
                                        <ext:ModelField Name="Title" Type="string" />
                                        <ext:ModelField Name="Amount" Type="Float" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                    </Store>
                    <ColumnModel>
                        <Columns>
                            <ext:Column ID="Column1" runat="server" Text="Account" DataIndex="Title" Width="100"
                                Flex="1" />
                            <ext:Column ID="Column3" runat="server" Text="Amount" DataIndex="Amount" Align="Right"
                                Width="100">
                                <Renderer Fn="getFormattedAmount" />
                            </ext:Column>
                        </Columns>
                    </ColumnModel>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                    </SelectionModel>
                </ext:GridPanel>
            </td>
        </tr>
    </table>
</div>
