﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SeminarCtrlView.ascx.cs"
    Inherits="Web.NewHR.UserControls.SeminarCtrlView" %>

<ext:GridPanel ID="GridSeminar" runat="server" Width="920" Cls="itemgrid" Scroll="None">
    <Store>
        <ext:Store ID="StoreSeminar" runat="server">
            <Model>
                <ext:Model ID="Model1" runat="server" IDProperty="SeminarId">
                    <Fields>
                        <ext:ModelField Name="Organizer" Type="string" />
                        <ext:ModelField Name="Country" Type="String" />
                        <ext:ModelField Name="Place" Type="string" />
                        <ext:ModelField Name="Duration" Type="string" />
                        <ext:ModelField Name="DurationTypeName" Type="string" />
                        <ext:ModelField Name="StartDate" Type="string" />
                        <ext:ModelField Name="EndDate" Type="string" />
                        <ext:ModelField Name="IsEditable" Type="Int" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
    </Store>
    <ColumnModel>
        <Columns>
            <ext:Column ID="Column1" runat="server" Text="Organizer" DataIndex="Organizer" Flex="1"
                Width="200" Wrap="true" />
            <ext:Column ID="Column2" runat="server" Text="Country" DataIndex="Country" Width="160"
                Wrap="true">
            </ext:Column>
            <ext:Column ID="Column3" runat="server" Text="Place" DataIndex="Place" Width="160"
                Wrap="true">
            </ext:Column>
            <ext:Column ID="Column4" runat="server" Text="Duration" DataIndex="Duration" Width="140"
                Wrap="true">
                <Renderer Fn="renderDuration" />
            </ext:Column>
            <ext:Column ID="Column6" runat="server" Text="Start Date" DataIndex="StartDate" Width="140">
            </ext:Column>
            <ext:Column ID="Column5" runat="server" Text="End Date" DataIndex="EndDate" Width="140">
            </ext:Column>
        </Columns>
    </ColumnModel>
    <SelectionModel>
        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
    </SelectionModel>
</ext:GridPanel>


<script type="text/javascript">
    var renderDuration = function (value, meta, record) {

        return record.data.Duration + " (" + record.data.DurationTypeName + ")";
    };
    
 </script>