﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;
using BLL.Base;
using Ext.Net;

namespace Web.NewHR.UserControls
{
    public partial class ExtraActivitiesCtrl : BaseUserControl
    {
        public bool ShowEmpty = false;

        private bool _isDisplayMode = false;
        public bool IsDisplayMode
        {
            set
            {
                if (value == true)
                {
                    _isDisplayMode = value;
                    btnAddNewLine.Visible = false;
                    WExtraActivities.Visible = false;
                    CommandColumn21.Visible = false;
                    CommandColumn2.Visible = false;
                    gridExtraActivity.Width = 920;
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();

            }
        }

        private void HideButtonBlock()
        {
            buttonBlock.Visible = false;
        }

        private void Initialise()
        {
            BindGrid();           
        }

        private void ClearFields()
        {
            txtActivityName.Text = "";
            txtProficiency.Text = "";
            txtAward.Text = "";
            txtYear.Text = "";
            txtRemarks.Text = "";
        }

        private void BindGrid()
        {
            int EmployeeID = GetEmployeeID();

            List<HExtraActivity> list = NewHRManager.GetExtraActivityByEmployeeId(EmployeeID);
            if (list.Count > 0)
            {
                gridExtraActivity.Store[0].DataSource = list;
                gridExtraActivity.Store[0].DataBind();
                gridExtraActivity.Show();
            }
            else
                gridExtraActivity.Hide();
            
        }

        protected void btnAddNewLine_Click(object sender, DirectEventArgs e)
        {
            ClearFields();
            hdnCurricularId.Text = "";
            WExtraActivities.Show();
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveUpdateActivity");
            if (Page.IsValid)
            {
                HExtraActivity obj = new HExtraActivity();

                if (!string.IsNullOrEmpty(hdnCurricularId.Text))
                    obj.CurricularId = int.Parse(hdnCurricularId.Text);

                obj.EmployeeId = GetEmployeeID();

                obj.AcitivityName = txtActivityName.Text;

                if (!string.IsNullOrEmpty(txtProficiency.Text.Trim()))
                    obj.Proficiency = txtProficiency.Text.Trim();

                if (!string.IsNullOrEmpty(txtAward.Text.Trim()))
                    obj.Award = txtAward.Text.Trim();

                if (!string.IsNullOrEmpty(txtYear.Text.Trim()))
                    obj.Year = txtYear.Text.Trim();

                if (!string.IsNullOrEmpty(txtRemarks.Text.Trim()))
                    obj.Remarks = txtYear.Text.Trim();

                Status status = NewHRManager.SaveUpdateExtraActivity(obj);
                if (status.IsSuccess)
                {
                    WExtraActivities.Close();

                    if (!string.IsNullOrEmpty(hdnCurricularId.Text))
                        NewMessage.ShowNormalMessage("Record updated successfully.");
                    else
                        NewMessage.ShowNormalMessage("Record saved successfully.");
                    
                    BindGrid();
                }
                else
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
            }

        }

        protected int GetEmployeeID()
        {
            int EmpID = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                EmpID = int.Parse(Request.QueryString["ID"]);
            }
            else
            {
                EmpID = SessionManager.CurrentLoggedInEmployeeId;
            }
            return EmpID;
        }


        protected void gridExtraActivity_Command(object sender, DirectEventArgs e)
        {
            string commandName = e.ExtraParams["command"];
            int curricularId = int.Parse(e.ExtraParams["ID"]);



            switch (commandName)
            {
                case "Delete":
                    this.DeleteExtraActivity(curricularId);
                    break;               
                case "Edit":
                    this.EditExtraActivity(curricularId);
                    break;
            }

        }

        private void DeleteExtraActivity(int curricularId)
        {
            Status status = NewHRManager.DeleteExtraActivity(curricularId);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Record deleted successfully.");
                BindGrid();
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        private void EditExtraActivity(int curricularId)
        {
            hdnCurricularId.Text = curricularId.ToString();
            HExtraActivity obj = NewHRManager.GetExtraActivityById(curricularId);
            if (obj != null)
            {
                txtActivityName.Text = obj.AcitivityName;
                if (obj.Proficiency != null)
                    txtProficiency.Text = obj.Proficiency;

                if (obj.Award != null)
                    txtAward.Text = obj.Award;

                if (obj.Year != null)
                    txtYear.Text = obj.Year;

                if (obj.Remarks != null)
                    txtRemarks.Text = obj.Remarks;

                btnSave.Text = Resources.Messages.Update;

                WExtraActivities.Show();
            }
        }

    }
}