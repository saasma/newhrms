﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using Utils.Web;
using Utils.Helper;

namespace Web.NewHR.UserControls
{
    public partial class CharityCtl : BaseUserControl
    {
      


        protected void Page_PreRender(object sender, EventArgs e)
        {
           

        }

        public int GetEmployeeId()
        {
            return int.Parse(Request.QueryString["Id"]);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMsg.Text = "";
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }

            //JavascriptHelper.AttachNonDialogPopUpCode(Page, "employeePopup", "../ExcelWindow/GradeRewardExcel.aspx", 450, 500);
        }

     

        public void Initialise()
        {
           

            
            {
                gridRewardList.Store[0].DataSource
                        = NewPayrollManager.GetCharityList();
                gridRewardList.Store[0].DataBind();
            }
        }

        public Charity Process(Charity entity)
        {
            if (entity == null)
            {
                entity = new Charity();
                if (string.IsNullOrEmpty(hdn.Text))
                {
                    entity.EmployeeId = int.Parse(cmbGradeEmployee.SelectedItem.Value);
                }
                else
                    entity.EmployeeId = NewPayrollManager.GetCharity(int.Parse(hdn.Text)).EmployeeId;

               
                entity.Notes = txtNotes.Text.Trim();
                entity.Institution = txtInstitution.Text.Trim();
                
                entity.CharityDate = date.SelectedDate.ToShortDateString();
                entity.CharityDateEng = date.SelectedDate;
                
                entity.Amount = decimal.Parse(txtAmount.Text.Trim());

                return entity;
            }
            else
            {
                EEmployee emp = new EmployeeManager().GetById(entity.EmployeeId);

                storeSearch.DataSource = new List<EEmployee> { new EEmployee{ 
                        EmployeeId = emp.EmployeeId,
                Name = emp.Name} 
                
                }
                ;
                storeSearch.DataBind();

                cmbGradeEmployee.Disable(true);

                cmbGradeEmployee.Items.Add(new Ext.Net.ListItem { Value = emp.EmployeeId.ToString(), Text = emp.Name });
                cmbGradeEmployee.SelectedItems.Add(new Ext.Net.ListItem { Index = 0 });
               
                date.Text = entity.CharityDate;
                cmbGradeEmployee.SetValue(emp.EmployeeId.ToString());
                cmbGradeEmployee.SetRawValue(emp.Name.ToString());

                txtInstitution.Text = entity.Institution;
                txtAmount.Text = GetCurrency(entity.Amount);
                txtNotes.Text = entity.Notes;

                cmbGradeEmployee.Disable(true);
            }
            return null;
        }
       

        protected void btnDeleteLevel_Click(object sender, DirectEventArgs e)
        {
            int levelId = int.Parse(hdn.Text.Trim());
            Status status = NewPayrollManager.DeleteGradeReward(levelId);
            if (status.IsSuccess)
            {
                X.Js.AddScript("searchList();");
                NewMessage.ShowNormalMessage("Reward deleted.");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }

        }



        protected void btnEditLevel_Click(object sender, DirectEventArgs e)
        {
            int levelId = int.Parse(hdn.Text.Trim());
            Charity entity = NewPayrollManager.GetCharity(levelId);
            Process(entity);
            window.Center();
            window.Show();



            //cmbGroup.SetValue(entity.LevelGroupId.ToString());
            //txtLevelCode.Text = entity.Code;
            //txtLevelName.Text = entity.Name;
            //txtLevelOrder.Number = entity.Order;

        }

       

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("InsertUpdate");
            if (Page.IsValid)
            {


                Charity entity = Process(null);
                bool isInsert = string.IsNullOrEmpty(hdn.Text);
                if (isInsert == false)
                    entity.CharityID = int.Parse(hdn.Text);
                Status status = NewPayrollManager.InsertUpdateCharity(entity, isInsert);
                if (status.IsSuccess)
                {
                    Initialise();
                    window.Hide();
                    
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }
            }

        }
      



        public void ClearFields()
        {
            cmbGradeEmployee.Enable(true);
            hdn.Text = "";
            date.Clear();
            txtInstitution.Clear();
            txtAmount.Clear();
            txtNotes.Clear();
            cmbGradeEmployee.ClearValue();
        }

      


        public void btnAdd_Click(object sender, DirectEventArgs e)
        {

            ClearFields();
            window.Show();
        }

    }
}