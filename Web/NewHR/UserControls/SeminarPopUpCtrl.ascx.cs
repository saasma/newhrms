﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;

namespace Web.NewHR.UserControls
{
    public partial class SeminarPopUpCtrl : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();                
            }
        }

        protected void Initialise()
        {
            int EmployeeID = GetEmployeeID();           

            //Load Data
            if (EmployeeID > 0)
            {
                CommonManager _CommonManager = new CommonManager();
                cmbCountry.Store[0].DataSource = _CommonManager.GetAllCountries();
                cmbCountry.Store[0].DataBind();
            }
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            this.SaveFormData();
        }

        private void SaveFormData()
        {

            Status myStatus = new Status();
            HSeminar _HSeminar = new HSeminar();
            _HSeminar.EmployeeId = this.GetEmployeeID();
            bool isSave = bool.Parse(this.hdnSeminarID.Text == "" ? "true" : "false");
            if (!isSave)
                _HSeminar.SeminarId = int.Parse(this.hdnSeminarID.Text);

            _HSeminar.Organizer = txtOrganizer.Text.Trim();
            _HSeminar.Place = txtPlace.Text.Trim();
            if (!string.IsNullOrEmpty(cmbCountry.SelectedItem.Text))
            {
                _HSeminar.Country = cmbCountry.SelectedItem.Text;
            }
            _HSeminar.Duration = int.Parse(txtDuration.Text.Trim());
            if (!string.IsNullOrEmpty(cmbDurationType.SelectedItem.Text))
            {

                _HSeminar.DurationTypeID = int.Parse(cmbDurationType.SelectedItem.Value);
                _HSeminar.DurationTypeName = cmbDurationType.SelectedItem.Text;
            }

            if (!string.IsNullOrEmpty(calStartDate.Text.Trim()))
            {
                _HSeminar.StartDate = calStartDate.Text.Trim();
                _HSeminar.StartDateEng = BLL.BaseBiz.GetEngDate(_HSeminar.StartDate, IsEnglish);
            }
            if (!string.IsNullOrEmpty(calEndDate.Text.Trim()))
            {
                _HSeminar.EndDate = calEndDate.Text.Trim();
                _HSeminar.EndDateEng = BLL.BaseBiz.GetEngDate(_HSeminar.EndDate, IsEnglish);
            }

            _HSeminar.Note = txtNote.Text;

            myStatus = NewHRManager.Instance.InsertUpdateSeminar(_HSeminar, isSave);

            if (myStatus.IsSuccess)
            {
                Window win = (Window)this.Parent.FindControl("AESeminarWindow");
                win.Close();
                if (isSave)
                    NewMessage.ShowNormalMessage("Record Saved Successfully.", "reloadSeminarGrid()");
                else
                    NewMessage.ShowNormalMessage("Record Updated Successfully.", "reloadSeminarGrid()");    
            }
            else
                NewMessage.ShowWarningMessage(myStatus.ErrorMessage);
        }

        protected int GetEmployeeID()
        {
            //Guid? AssetID = null;
            int EmpID = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                EmpID = int.Parse(Request.QueryString["ID"]);
            }
            else
            {
                EmpID = SessionManager.CurrentLoggedInEmployeeId;
            }
            return EmpID;
        }

        private object[] SeminarFillData
        {
            get
            {
                return new object[]
            {
                new object[] { "", "", "",""},
               
            };
            }
        }       

        public void ClearFields()
        {
            this.hdnSeminarID.Text = "";
            txtOrganizer.Text = "";
            txtPlace.Text = "";
            cmbCountry.Value = "";
            txtDuration.Text = "";
            cmbDurationType.Value = "";
            calStartDate.Text = "";
            calEndDate.Text = "";
            txtNote.Text = "";
            btnSave.Text = Resources.Messages.Save;
        }

        public void editSeminar(int SeminarID)
        {

            HSeminar _HSeminar = NewHRManager.GetSeminarDetailsById(SeminarID);
            this.hdnSeminarID.Text = SeminarID.ToString();

            txtOrganizer.Text = _HSeminar.Organizer;
            txtPlace.Text = _HSeminar.Place;
            cmbCountry.SetValue(_HSeminar.Country);
            txtDuration.Text = _HSeminar.Duration.ToString();
            cmbDurationType.SetValue(_HSeminar.DurationTypeID.ToString());
            calStartDate.Text = _HSeminar.StartDate;
            calEndDate.Text = _HSeminar.EndDate;
            txtNote.Text = _HSeminar.Note;
            btnSave.Text = Resources.Messages.Update;
        }

        public void HideButtons()
        {
            btnSave.Hidden = true;
            LinkButton2.Hidden = true;
        }
    }
}