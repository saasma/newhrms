﻿<%@ Control Language="C#" EnableViewState="true" AutoEventWireup="true" CodeBehind="EmployeeHRList.ascx.cs"
    Inherits="Web.NewHR.UserControls.EmployeeHRList" %>
<%@ Import Namespace="BLL.Manager" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/FilterCtl.ascx" TagName="FilterCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<script type="text/javascript">

    function refreshWindow() {
        window.location = '../newhr/EmployeeList.aspx';
    }
    function redirectToOverview(id) {
        //Web.PayrollService.SetSelEmployee(id, onSuccess);
        window.location = 'EmployeeDetails.aspx?Id=' + id;
    }

    //    function onSuccess(result) {
    //        if (result) {
    //            window.location = 'Employee.aspx';
    //        }
    //    }    

</script>
<style type="text/css">
    .attribute /*{
        margin-bottom: 7px;
    }*/ .tableLightColor a:hover
    {
        text-decoration: none;
    }
    .attribute td
    {
        padding-left: 6px;
    }
    .sortLink a
    {
        color: #333333;
    }
    .PaginationBar
    {
        clear: both;
    }
</style>
<style type="text/css">
    .hideLeftBlockCssInPage
    {
        margin: 0px !important;
        padding-left: 20px !important;
    }
    #menu
    {
        display: none;
    }
    
    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th
    {
        padding: 4px 5px 4px 5px;
        height: 35px !important;
        font-weight: bold;
    }
</style>
<%--<div class="attribute">--%>
<uc2:FilterCtl SearchEmployeeTextboxAutoPostback="false" Id="filterCtl" EmployeeIDCardNo="true"
    Designation="true" SubDepartment="true" OnFilterChanged="ReBindEmployees" EmployeeName="true"
    Status="true" runat="server" />
<%--<label style="float: left; padding: 3px 5px 0pt 0pt; width: 50px;">
        Search:</label><asp:TextBox Width="160px" ID="txtEmployeeName" AutoPostBack="true"
            runat="server" OnTextChanged="txtEmployeeName_TextChanged"></asp:TextBox>
    <cc1:TextBoxWatermarkExtender ID="TBWE2" runat="server" TargetControlID="txtEmployeeName"
        WatermarkText="Employee name" WatermarkCssClass="searchBoxText " />--%>
<%--    <asp:CheckBox AutoPostBack="true" ID="chkShowInactiveEmpAlso" Visible="false" Style="display: none;"
        runat="server" Text="Show inactive employees" />--%>
<div style="float:left">
    <asp:HyperLink ID="btnLoads" Target="_blank" NavigateUrl="~/newhr/PersonalDetail.aspx"
        runat="server" Text="Add New Employee"></asp:HyperLink>
</div>
<div style='float:right; text-align: right; margin-bottom: 5px; '>
    Show
    <asp:DropDownList runat="server" ID="ddlRetirementType" Width="100px">
        <asp:ListItem Text="Active only" Value="false" />
        <asp:ListItem Text="All" Value="All" />
        <asp:ListItem Text="Retired only" Value="true" />
    </asp:DropDownList>
    <%--  <asp:CheckBox ID="chkIsRetiredOrResignedAlso" Checked="false" Text="Show Retired"
        runat="server" CssClass="chkbox" OnCheckedChanged="chkIsRetiredOrResignedAlso_CheckedChanged" />--%>
</div>
<%--</div>--%>
<div style="clear:both">
</div>
<uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
<uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
<div style="display: inline-block">
    <cc2:EmptyDisplayGridView CssClass="table table-primary mb30 table-bordered table-hover"
        Style="margin-bottom: -3px;" UseAccessibleHeader="true" ShowHeaderWhenEmpty="True"
        ID="gvwEmployees" runat="server" DataKeyNames="EmployeeId" AutoGenerateColumns="False"
        AllowSorting="True" OnSorting="gvwEmployees_Sorting" OnRowCreated="gvwEmployees_RowCreated"
        OnRowDeleting="gvwEmployees_RowDeleting" ShowFooterWhenEmpty="False" Width="100%"
        GridLines="None">
        <Columns>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Image class="media-object img-circle" Style="" ImageUrl='<%# Eval("UrlPhoto") %>'
                        Height="38" Width="38" runat="server" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField SortExpression="EmployeeId" HeaderStyle-Width="30px" DataFormatString="{0:000}"
                HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Left" DataField="EmployeeId"
                HeaderText="EIN" HeaderStyle-CssClass="sortLink">
                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                <ItemStyle HorizontalAlign="Left"></ItemStyle>
            </asp:BoundField>
            <asp:TemplateField HeaderText="Name" HeaderStyle-CssClass="sortLink" SortExpression="Name"
                HeaderStyle-Width="280px">
                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                <ItemTemplate>
                    <asp:HyperLink CssClass="employeeClass" Target="_blank" Width="150px" ID="Label2"
                        runat="server" NavigateUrl='<%# "~/newhr/EmployeeDetails.aspx?Id=" +  Eval("EmployeeId") %>'
                        Text='<%# "&nbsp;" + Eval("Name") %>'></asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Branch" HeaderStyle-CssClass="sortLink" SortExpression="Branch"
                HeaderStyle-Width="150px">
                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                <ItemTemplate>
                    <%#  Eval("BranchValue")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Department" HeaderStyle-CssClass="sortLink" SortExpression="Department"
                HeaderStyle-Width="300px">
                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                <ItemTemplate>
                    <%# Eval("DepartmentValue")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Sub Department" HeaderStyle-CssClass="sortLink" SortExpression="SubDepartment"
                HeaderStyle-Width="250px">
                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                <ItemTemplate>
                    <%# Eval("SubDepartmentValue")%>
                </ItemTemplate>
            </asp:TemplateField>
         
          
            <asp:TemplateField HeaderText="Level" HeaderStyle-CssClass="sortLink" SortExpression="Level"
                HeaderStyle-Width="200px">
                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                <ItemTemplate>
                    <%#  Eval("LevelValue")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Designation" HeaderStyle-CssClass="sortLink" SortExpression="Designation"
                HeaderStyle-Width="250px">
                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                <ItemTemplate>
                    <%#  Eval("DesignationValue")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Functional Title" HeaderStyle-CssClass="sortLink" SortExpression="FunctionalTitle"
                HeaderStyle-Width="150px">
                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                <ItemTemplate>
                    <%#  Eval("FunctionalTitle")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Status" HeaderStyle-CssClass="sortLink" SortExpression="Status"
                HeaderStyle-Width="150px">
                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                <ItemTemplate>
                    <%#  Eval("StatusText")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="I No" HeaderStyle-CssClass="sortLink" SortExpression="IdCardNo"
                HeaderStyle-Width="80px">
                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                <ItemTemplate>
                    <%#  Eval("IdCardNoValue")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Active" HeaderStyle-CssClass="sortLink" SortExpression="IsRetiredOrResigned"
                HeaderStyle-Width="40px" ItemStyle-HorizontalAlign="Center">
                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                <ItemTemplate>
                    <asp:Image ID="Image1" runat="server" ImageUrl='<%# Utils.Web.UIHelper.GetYesNoImage( !Convert.ToBoolean(Eval("IsRetiredOrResignedValue"))) %>' />
                    <%--  <%# Convert.ToBoolean(Eval("IsRetiredOrResignedValue"))?"True":""%>--%>
                </ItemTemplate>
            </asp:TemplateField>
          
            <asp:TemplateField HeaderText="Edit" HeaderStyle-Width="20px" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <asp:HyperLink ID="linkEdit" ImageUrl="~/images/edit.gif" NavigateUrl='<%# "~/newhr/EmployeeDetails.aspx?Id=" + Eval("EmployeeId") %>'
                        runat="server">                       
                    </asp:HyperLink>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center"></ItemStyle>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Delete" HeaderStyle-Width="20px" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <asp:ImageButton  ID="ImageButton2"
                        OnClientClick="return confirm('Do you want to confirm delete the employee,\nall data will be lost?')"
                        runat="server" CommandName="Delete" ImageUrl="~/images/delete.gif" />
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center"></ItemStyle>
            </asp:TemplateField>
        </Columns>
        <RowStyle CssClass="odd" />
        <AlternatingRowStyle CssClass="even" />
        <EmptyDataTemplate>
            <b>No employee records found. </b>
        </EmptyDataTemplate>
    </cc2:EmptyDisplayGridView>
</div>
<uc1:PagingCtl ID="pagintCtl" ShowDropDown="true" OnNextRecord="btnNext_Click" OnChangePage="ChangePageNumber"
    OnPrevRecord="btnPrevious_Click" OnDropDownShowRecordsChanged="ddlRecords_SelectedIndexChanged"
    runat="server">
</uc1:PagingCtl>
<div class="buttonsDiv">
    <%-- <asp:LinkButton ID="btnAddNew" CssClass="addemployee" Style="float: left; margin-right: 10px;"
        OnClientClick="window.location ='Employee.aspx';return false;" runat="server"
        Text="Add New Employee" />--%>
    <asp:LinkButton ID="btnExportPopup" runat="server" Text="Import Employee" OnClientClick="employeePopup();return false;"
        CssClass=" excel marginRight tiptip" Style="float: left;" />
    <asp:LinkButton ID="btnExportImportAccounts" runat="server" Text="Import Accounts"
        OnClientClick="otherPopup();return false;" CssClass=" excel marginRight tiptip"
        Style="float: left;" />
    <asp:LinkButton ID="btnExport" runat="server" Text="Export to Excel" OnClick="btnExport_Click"
        CssClass=" excel marginRight" Style="float: left;" />
</div>
