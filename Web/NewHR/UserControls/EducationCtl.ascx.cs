﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;

using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;

namespace Web.NewHR.UserControls
{
    public partial class EducationCtl : BaseUserControl
    {
        public bool ShowEmpty = false;

        private bool _isDisplayMode = false;
        public bool IsDisplayMode
        {
            set
            {
                if (value == true)
                {
                    _isDisplayMode = value;
                    btnAddNewLine.Visible = false;
                    AEEducationWindow.Visible = false;
                    CommandColumn1.Visible = false;
                    CommandColumn21.Visible = false;
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise(0);

            }
        }
        public void HideButtonBlock()
        {
            buttonBlock.Visible = false;
        }
        public void Initialise(int employeeId)
        {
            int EmployeeID = GetEmployeeID();

            if (employeeId != 0)
                EmployeeID = employeeId;

            cmbLevel.Store[0].DataSource = CommonManager.GetEducationLevelList();
            cmbLevel.Store[0].DataBind();

            cmbFaculty.Store[0].DataSource = CommonManager.GetEducationFacultyList();
            cmbFaculty.Store[0].DataBind();

            this.LoadEducationGrid(EmployeeID);


            //Load Data
            if (EmployeeID > 0)
            {
                CommonManager _CommonManager = new CommonManager();
                cmbCountry.Store[0].DataSource = _CommonManager.GetAllCountries();
                cmbCountry.Store[0].DataBind();
            }



        }
        protected void btnSave_Click(object sender, DirectEventArgs e)
        {

            this.SaveFormData();

        }
        private void SaveFormData()
        {

            Status myStatus = new Status();
            HEducation _HEducation = new HEducation();
            _HEducation.EmployeeId = this.GetEmployeeID();
            bool isSave = bool.Parse(this.hdnEduID.Text == "" ? "true" : "false");
            if (!isSave)
                _HEducation.EductionId = int.Parse(this.hdnEduID.Text);
            _HEducation.Course = txtCourseName.Text.Trim();
            if (cmbLevel.SelectedItem != null && cmbLevel.SelectedItem.Value != null)
            {
                _HEducation.LevelName = cmbLevel.SelectedItem.Text;
                _HEducation.LevelTypeID = int.Parse(cmbLevel.SelectedItem.Value);
            }
            if (cmbFaculty.SelectedItem != null && cmbFaculty.SelectedItem.Value != null)
            {
                _HEducation.FacultyName = cmbFaculty.SelectedItem.Text;
                _HEducation.FacultyID = int.Parse(cmbFaculty.SelectedItem.Value);
            }
            _HEducation.College = txtInstitute.Text.Trim();
            _HEducation.University = txtUniversity.Text.Trim();
            if (cmbCountry.SelectedItem != null && cmbCountry.SelectedItem.Value != null)
            {
                _HEducation.Country = cmbCountry.SelectedItem.Text;
            }
            _HEducation.Percentage = txtPercentageGrade.Text.Trim();
            if (cmbDivision.SelectedItem != null && cmbDivision.SelectedItem.Value != null)
            {
                _HEducation.DivisionID = int.Parse(cmbDivision.SelectedItem.Value);
                _HEducation.DivisionName = cmbDivision.SelectedItem.Text;
            }
            int year = 0;
            if (int.TryParse(txtPassedYear.Text, out year))
                _HEducation.PassedYear = int.Parse(txtPassedYear.Text.Trim());
            _HEducation.PassedYearNep = txtPassedYearNep.Text.Trim();
            _HEducation.MajorSubjects = txtMajorSubjects.Text;

            //file upload section
            string UserFileName = this.FileEduDocumentUpload.FileName;
            string ServerFileName = Guid.NewGuid().ToString() + Path.GetExtension(UserFileName);
            string relativePath = @"../uploads/" + ServerFileName;
            if (this.UploadFileEducation(relativePath))
            {
                double fileSize = FileEduDocumentUpload.PostedFile.ContentLength;

                _HEducation.FileFormat = Path.GetExtension(this.FileEduDocumentUpload.FileName).Replace(".", "").Trim();
                _HEducation.FileType = this.FileEduDocumentUpload.PostedFile.ContentType;
                _HEducation.FileLocation = @"../Uploads/";
                _HEducation.ServerFileName = ServerFileName;
                _HEducation.UserFileName = UserFileName;
                _HEducation.Size = this.ToSizeString(fileSize);

            }

            myStatus = NewHRManager.Instance.InsertUpdateEducation(_HEducation, isSave);

            if (myStatus.IsSuccess)
            {
                if (isSave)
                    NewMessage.ShowNormalMessage("Record Save successfully");
                else
                    NewMessage.ShowNormalMessage("Record Updated successfully");

                //  this.ClearFields();
                this.AEEducationWindow.Close();
                this.LoadEducationGrid(this.GetEmployeeID());

            }
            else
                NewMessage.ShowWarningMessage(myStatus.ErrorMessage);
        }
        protected string ToSizeString(double bytes)
        {

            string[] SizeSuffixes = { "bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };
            int mag = (int)Math.Log(bytes, 1024);
            decimal adjustedSize = (decimal)bytes / (1 << (mag * 10));
            return string.Format("{0:n1} {1}", adjustedSize, SizeSuffixes[mag]);
        }
        protected void EducationDownLoad(int ID)
        {

            //string contentType = "";
            HEducation doc = NewHRManager.GetEducationDetailsById(ID);

            string path = Context.Server.MapPath(doc.FileLocation + doc.ServerFileName);
            string contentType = doc.FileType;
            string name = doc.UserFileName + "." + doc.FileFormat;

            byte[] bytes = File.ReadAllBytes(path);

            Context.Response.Clear();
            Context.Response.ClearHeaders();
            Context.Response.AppendHeader("Content-Disposition", "attachment; filename=\"" + name + "\"");
            Context.Response.ContentType = contentType;
            Context.Response.BinaryWrite(bytes);
            Context.Response.End();
            // Response.Redirect("~/DocumentHandler.ashx?ID=" + ID);
        }
        protected bool UploadFileEducation(string relativePath)
        {

            if (this.FileEduDocumentUpload.HasFile)
            {

                int fileSize = FileEduDocumentUpload.PostedFile.ContentLength;
                this.FileEduDocumentUpload.PostedFile.SaveAs(Server.MapPath(relativePath));
                return true;
            }
            else
                return false;

        }
        protected void LoadEducationGrid(int EmployeeID)
        {
            List<HEducation> _HEducation = NewHRManager.GetEducationByEmployeeID(EmployeeID);


            this.StoreEducation.DataSource = _HEducation;
            this.StoreEducation.DataBind();

            if (ShowEmpty == false)
            {
                if (_isDisplayMode && _HEducation.Count <= 0)
                    GridEducation.Hide();
            }
            else
                GridEducation.Show();
        }
        protected int GetEmployeeID()
        {
            //Guid? AssetID = null;
            int EmpID = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                EmpID = int.Parse(Request.QueryString["ID"]);
            }
            else
            {
                EmpID = SessionManager.CurrentLoggedInEmployeeId;
            }
            return EmpID;
        }
        protected void btnAddNewLine_Click(object sender, DirectEventArgs e)
        {
            this.EducationClearFields();
            this.AEEducationWindow.Show();

        }
        protected void lnkDeleteFile_Click(object sender, DirectEventArgs e)
        {
            int EducationID = int.Parse(hdnEduID.Text);
            Status myStatus = new Status();

            HEducation _HEducation = NewHRManager.GetEducationDetailsById(EducationID);
            string path = Context.Server.MapPath(_HEducation.FileLocation + _HEducation.ServerFileName);
            myStatus = NewHRManager.Instance.DeleteEducationFile(EducationID);
            if (myStatus.IsSuccess)
            {
                if (File.Exists(path))
                {
                    File.Delete(path);
                    lnkDeleteFile.Hide();
                    lblUploadedFile.Hide();
                    NewMessage.ShowNormalMessage("file deleted successfully");
                    //refresh grid
                    this.LoadEducationGrid(this.GetEmployeeID());

                }
            }



        }
        private object[] EducationFillData
        {
            get
            {
                return new object[]
            {
                new object[] { "", "", "",""},
               
            };
            }
        }
        protected void GridEducation_Command(object sender, DirectEventArgs e)
        {
            string commandName = e.ExtraParams["command"];
            int EducationID = int.Parse(e.ExtraParams["ID"]);
            HEducation _HEducation = NewHRManager.GetEducationDetailsById(int.Parse(e.ExtraParams["ID"]));
            string Path = Server.MapPath(_HEducation.FileLocation) + _HEducation.ServerFileName;
            switch (commandName)
            {
                case "Delete":
                    this.DeleteEducationDocument(EducationID, Path);
                    break;
                case "DownLoad":
                    this.EducationDownLoad(EducationID);
                    break;
                case "Edit":
                    this.editEducation(EducationID);
                    break;
            }

        }
        protected void DeleteEducationDocument(int DocumentID, string path)
        {

            bool result = NewHRManager.DeleteEducationByID(DocumentID);
            if (result)
            {
                if (File.Exists(path))
                    File.Delete(path);
                //refresh grid
                this.LoadEducationGrid(this.GetEmployeeID());
            }

        }
        protected void EducationClearFields()
        {
            this.hdnEduID.Text = "";
            txtCourseName.Text = "";
            cmbLevel.Value = "";
            cmbFaculty.Value = "";
            txtInstitute.Text = "";
            txtUniversity.Text = "";
            cmbCountry.Value = "";
            txtPercentageGrade.Text = "";
            cmbDivision.Value = "";
            txtPassedYear.Text = "";
            txtPassedYearNep.Text = "";
            txtMajorSubjects.Text = "";
            lblUploadedFile.Hide();
            lblUploadedFile.Text = "";
            lnkDeleteFile.Hide();
            btnSave.Text = Resources.Messages.Save;
        }
        public void editEducation(int EducationID)
        {

            HEducation _HEducation = NewHRManager.GetEducationDetailsById(EducationID);
            this.hdnEduID.Text = EducationID.ToString();
            txtCourseName.Text = _HEducation.Course;

            cmbLevel.Store[0].ClearFilter();
            if (_HEducation.LevelTypeID != null)
                cmbLevel.SetValue(_HEducation.LevelTypeID.ToString());
            else
                cmbLevel.ClearValue();

            cmbFaculty.Store[0].ClearFilter();
            if (_HEducation.FacultyID != null)
                cmbFaculty.SetValue(_HEducation.FacultyID.ToString());
            else
                cmbFaculty.ClearValue();


            txtInstitute.Text = _HEducation.College;
            txtUniversity.Text = _HEducation.University;
            cmbCountry.SetValue(_HEducation.Country);
            txtPercentageGrade.Text = _HEducation.Percentage;
            cmbDivision.SetValue(_HEducation.DivisionID.ToString());
            txtPassedYear.Text = _HEducation.PassedYear.ToString();
            txtPassedYearNep.Text = _HEducation.PassedYearNep;
            txtMajorSubjects.Text = _HEducation.MajorSubjects;
            if (!string.IsNullOrEmpty(_HEducation.UserFileName))
            {
                lblUploadedFile.Show();
                lblUploadedFile.Text = _HEducation.UserFileName;
                lnkDeleteFile.Show();
            }
            else
            {
                lblUploadedFile.Hide();
                lblUploadedFile.Text = "";
                lnkDeleteFile.Hide();
            }
            btnSave.Text = Resources.Messages.Update;
            this.AEEducationWindow.Show();

            
        }

    }
}