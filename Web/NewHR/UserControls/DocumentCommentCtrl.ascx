﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DocumentCommentCtrl.ascx.cs" Inherits="Web.NewHR.UserControls.DocumentCommentCtrl" %>


<div style="background-color:#f2f2f2; margin-left:20px; height:40px; padding-left:20px; padding-top:10px; width:450px;">
   Comments
</div>

<br />

<ext:Hidden ID="hdnCommentDocumentID" runat="server" />

 

<ext:DisplayField runat="server" ID="dispComments" MarginSpec="0 0 0 20" Width="450"/>



<br />

<ext:TextArea ID="txtComment" runat="server" TextMode="multiline" Width="450" MarginSpec="0 0 0 20"
    FieldLabel=""  LabelSeparator=""  />
<asp:RequiredFieldValidator Display="None" ID="rfvComment" runat="server" ValidationGroup="SaveComment"
                        ControlToValidate="txtComment" ErrorMessage="Comment is required." />

<ext:Button runat="server" ID="btnSaveComment" Text="Save Comment" MarginSpec="10 0 0 20" Width="120" Cls="btn btn-primary">
    <DirectEvents>
        <Click OnEvent="btnSaveComment_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
    <Listeners>
        <Click Handler="valGroup = 'SaveComment'; if(CheckValidation()) return ''; else return false;">
        </Click>
    </Listeners>
</ext:Button>  
