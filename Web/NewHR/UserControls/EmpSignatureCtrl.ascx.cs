﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;
using Utils.Helper;
using Utils.Security;
using Utils;
using Web.Helper;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace Web.NewHR.UserControls
{
    public partial class EmpSignatureCtrl : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                NewHRManager _NewHRManager = new NewHRManager();
                HHumanResource _HHumanResource = _NewHRManager.GetHumanResource(GetEmployeeID());
                if (_HHumanResource != null && !string.IsNullOrEmpty(_HHumanResource.UrlSignature))
                {
                    string filename = "../Uploads/" + _HHumanResource.UrlSignature;
                    ImgEmployee.ImageUrl = filename;

                    Image11.ImageUrl = filename;
                }

                if (_HHumanResource != null && !string.IsNullOrEmpty(_HHumanResource.UrlSignatureThumbnail))
                {
                    string filename = "../Uploads/" + _HHumanResource.UrlSignatureThumbnail;
                    ImgSignatureThumbprint.ImageUrl = filename;
                }

                if (Session["Redirect1"] != null && Session["Redirect1"].ToString() == "1")
                {
                    Session["Redirect1"] = "0";
                    X.Js.Call("myFunction1");
                }
            }
        }

        bool IsAbsoluteUrl(string url)
        {
            Uri result;
            return Uri.TryCreate(url, UriKind.Absolute, out result);
        }

        protected void btnUpload_Click(object sender, DirectEventArgs e)
        {
            int employeeId = GetEmployeeID();
            if (fupSignature.HasFile)
            {
                string ext = Path.GetExtension(fupSignature.FileName);
                string guidFileName = Guid.NewGuid().ToString() + ext;
                string saveLocation = Path.Combine(Server.MapPath(Config.UploadLocation), guidFileName);
                string thumbnailLocation = Path.Combine(Server.MapPath(Config.UploadLocation), Guid.NewGuid().ToString() + ext);

                UploadType type = (UploadType)2;
                string code = "closeWindow(\"{0}\");";
                NewHRManager mgr = new NewHRManager();

                if (IsImage(ext) == false)
                {
                    // JavascriptHelper.DisplayClientMsg("Invalid image type.", Page);
                    NewMessage.ShowWarningMessage("Invalid image type.");
                    return;
                }
                fupSignature.PostedFile.SaveAs(saveLocation);
                string filename = "";

                try
                {

                    WebHelper.CreateThumbnail(saveLocation, 200, 200, thumbnailLocation);
                    thumbnailLocation = Path.GetFileName(thumbnailLocation);
                    if (IsAbsoluteUrl(thumbnailLocation) || IsAbsoluteUrl(guidFileName))
                    {
                        NewMessage.ShowWarningMessage("Unable to upload. Try again...");
                        return;
                    }
                }
                catch { }

                try
                {
                    mgr.SaveOrUpdateImage(type, guidFileName, employeeId, thumbnailLocation);
                    filename = "../Uploads/" + guidFileName;
                    code = string.Format(code, filename);

                    Session["Redirect1"] = "1";

                    Response.Redirect("~/newhr/Identification.aspx?ID=" + GetEmployeeID().ToString());
                    
                    //NewMessage.ShowNormalMessage("Signature added.");
                    //ImgEmployee.ImageUrl = filename;
                    //ImgSignatureThumbprint.ImageUrl = "../Uploads/" + thumbnailLocation;

                }
                catch (Exception exp1)
                {
                    JavascriptHelper.DisplayClientMsg(exp1.Message, Page);
                }
                Page.ClientScript.RegisterClientScriptBlock(GetType(), "sdfdsf2333",
                   string.Format("opener.process('{0}');", filename), true);


            }
        }

        protected int GetEmployeeID()
        {

            int EmpID = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                EmpID = int.Parse(Request.QueryString["ID"]);
            }
            return EmpID;
        }

        /// <summary>
        /// Checks if the file has the valid image extension or not
        /// </summary>
        /// <param name="pExtension">Extension type</param>
        /// <returns>Is valid image or not</returns>
        public static bool IsImage(string pExtension)
        {
            if ((pExtension.ToLower() == ".jpg" | pExtension.ToLower() == ".gif" | pExtension.ToLower() == ".bmp" | pExtension.ToLower() == ".jpeg" | pExtension.ToLower() == ".png"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Helper method to convert Bytes in KiloBytes
        /// </summary>
        /// <param name="byteValue"></param>
        /// <returns></returns>
        public static int ConvertBytesIntoKB(int byteValue)
        {
            double mb = (byteValue / 1024);
            if (mb.ToString("#") == "")
                return 0;
            return Convert.ToInt32(mb.ToString("#"));
        }
        public static string ConvertKBIntoMB(object value)
        {
            long kb = Convert.ToInt64(value);
            double mb = (kb / 1024.0);
            return mb.ToString("#.#");
        }

        public string GetSize(int byteValue)
        {
            if (byteValue < 1024)
                return "1 kb";

            double kb = ConvertBytesIntoKB(byteValue);
            if (kb > 1024)
            {
                return ConvertKBIntoMB(kb) + " mb";
            }
            else
            {
                return kb + " kb";
            }
        }


        protected void btnCropImage1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(_xField1.Value) || string.IsNullOrEmpty(_yField1.Value) || string.IsNullOrEmpty(_widthField1.Value) || string.IsNullOrEmpty(_heightField1.Value))
                return;
            var x = int.Parse(_xField1.Value);
            var y = int.Parse(_yField1.Value);
            var width = int.Parse(_widthField1.Value);
            var height = int.Parse(_heightField1.Value);

            if (height == 0 || width == 0)
                return;

            string filename = "";
            string newFileName = "";

            NewHRManager _NewHRManager = new NewHRManager();

            string saveLocation = "";
            string guidFileName = "";
            string thumbnailLocation = "";

            HHumanResource _HHumanResource = _NewHRManager.GetHumanResource(GetEmployeeID());
            if (_HHumanResource != null && !string.IsNullOrEmpty(_HHumanResource.UrlSignature))
            {
                filename = "../Uploads/" + _HHumanResource.UrlSignature;


                string[] splFileName = _HHumanResource.UrlSignature.Split('.');
                guidFileName = Guid.NewGuid().ToString() + "." + splFileName[1].ToString();

                saveLocation = Path.Combine(Server.MapPath(Config.UploadLocation), guidFileName);

                newFileName = "../Uploads/" + guidFileName;
                thumbnailLocation = Path.Combine(Server.MapPath(Config.UploadLocation), Guid.NewGuid().ToString() + "." + splFileName[1].ToString());

            }

            using (var photo =
                  System.Drawing.Image.FromFile(Server.MapPath(filename)))
            {
                using (var result =
                      new Bitmap(width, height, photo.PixelFormat))
                {
                    //result.SetResolution(
                    //        photo.HorizontalResolution,
                    //        photo.VerticalResolution);

                    result.SetResolution(200, 200);

                    Bitmap bmpNew = new Bitmap(result);
                    result.Dispose();


                    using (var g = Graphics.FromImage(bmpNew))
                    {
                        g.InterpolationMode =
                             InterpolationMode.HighQualityBicubic;
                        g.DrawImage(photo,
                             new Rectangle(0, 0, width, height),
                             new Rectangle(x, y, width, height),
                             GraphicsUnit.Pixel);


                        photo.Dispose();
                        g.Dispose();

                        bmpNew.Height.Equals(height);
                        bmpNew.Width.Equals(width);

                        bmpNew.Save(Server.MapPath(newFileName));



                        NewHRManager mgr = new NewHRManager();
                        UploadType type = (UploadType)2;

                        try
                        {

                            WebHelper.CreateThumbnail(saveLocation, 200, 200, thumbnailLocation);
                            thumbnailLocation = Path.GetFileName(thumbnailLocation);
                        }
                        catch { }


                        if (IsAbsoluteUrl(thumbnailLocation) || IsAbsoluteUrl(guidFileName))
                        {
                            NewMessage.ShowWarningMessage("Unable to upload. Try again...");
                            return;
                        }
                        mgr.SaveOrUpdateImage(type, guidFileName, GetEmployeeID(), thumbnailLocation);

                    }
                }
            }


            Response.Redirect("~/newhr/Identification.aspx?ID=" + GetEmployeeID().ToString());
        }



    }
}