﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ListWizard.ascx.cs"
    Inherits="Web.NewHR.UserControls.ListWizard" %>
<style type="text/css">
    .wizardClass ul li {
        width: 160px;
    }
</style>
<div class="col-sm-3 col-md-3 col-lg-2 wizardClass" id="divTest" runat="server">
    <ul class="nav nav-pills nav-stacked nav-msg" id="list" runat="server">
        <li><a id="A21" runat="server" href="~/CP/ManageSettings/ActivityTypes.aspx">Activity
            Types</a></li>
        <li><a id="A6" runat="server" href="~/CP/ManageSettings/HBloodGroupList.aspx">Blood
            Groups</a></li>
        <li><a id="A14" runat="server" href="~/CP/ManageSettings/CasteEthnicityList.aspx">Caste/Ethnicity</a></li>
        <li><a id="A9" runat="server" href="~/CP/ManageSettings/CitizenNationalityList.aspx">Citizen Nationality</a></li>
        <li><a id="A19" runat="server" href="~/CP/NewActivitySetting/ClientLists.aspx">Client
            List</a></li>
        <li><a id="A7" runat="server" href="~/CP/ManageSettings/DocumentTypeList.aspx">Document
            Type</a></li>
        <li><a id="A5" runat="server" href="~/CP/ManageSettings/ExperienceCategoryList.aspx">Exp. Categories</a></li>
        <li id="li1" runat="server"><a id="A1" runat="server" href="~/CP/ManageSettings/EducationFaculty.aspx">Education Faculties</a></li>
        <li id="li2"><a id="A2" runat="server" href="~/CP/ManageSettings/EducationLevel.aspx">Education Levels</a></li>
        <li><a id="A3" runat="server" href="~/CP/ManageSettings/FamilyRelationList.aspx">Family
            Relations</a></li>
        <li><a id="A22" runat="server" href="~/CP/ManageSettings/FunctionaTitleList.aspx">Functional Title</a></li>
        <li><a id="A15" runat="server" href="~/CP/ManageSettings/GradeStepList.aspx">Grades</a></li>
        <li><a id="A8" runat="server" href="~/CP/ManageSettings/HealthConditionList.aspx">Health
            Condition</a></li>
        <li><a id="A12" runat="server" href="~/CP/ManageSettings/HireMethodList.aspx">Hire Methods</a></li>
        <li><a id="A18" runat="server" href="~/CP/ManageSettings/HobbyTypeList.aspx">Hobby Types</a></li>
        <li><a id="A23" runat="server" href="~/CP/ManageSettings/HolidayGroupList.aspx">Holiday Groups</a></li>
        <li><a id="A4" runat="server" href="~/CP/ManageSettings/ELocationList.aspx">Locations</a></li>
        <li><a id="A11" runat="server" href="~/CP/ManageSettings/PublicationTypeList.aspx">Publication
            Types</a></li>
        <li><a id="A13" runat="server" href="~/CP/ManageSettings/ReligionList.aspx">Religions</a></li>
        <li><a id="A17" runat="server" href="~/CP/ManageSettings/RetirementTypes.aspx">Service
            Event Types</a></li>
        <li><a id="A20" runat="server" href="~/CP/NewActivitySetting/SoftwareLists.aspx">Software
            List</a></li>
        <li><a id="A16" runat="server" href="~/CP/ManageSettings/SubDepartmentList.aspx">Sub
            Departments</a></li>
        <li class="clsHeight"><a id="A10" runat="server" href="~/CP/ManageSettings/TrainingTypeList.aspx">Training Types</a></li>
        <li class="clsHeight"><a id="A25" runat="server" href="~/cp/TrainingRatingSetting.aspx">Training Feedback Rating</a></li>

        <li class="clsHeight"><a id="A24" runat="server" href="~/CP/ManageSettings/UnitListing.aspx">Units</a></li>







    </ul>
</div>
<script type="text/javascript">


    $(window).load(function () {
        LinkUpdate();

    });


    function LinkUpdate() {

        var ab = window.location.pathname.substring(location.pathname.lastIndexOf("/") + 1);

        $(".nav li a").each(function () {
            var abc = $(this).attr("href").substring($(this).attr("href").lastIndexOf("/") + 1);;
            if (ab.toLowerCase() == abc.toLowerCase()) {
                $(this).parent('li').addClass('active');
            }
        });

    }


</script>
