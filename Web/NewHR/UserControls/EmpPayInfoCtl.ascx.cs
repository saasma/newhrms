﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;

using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;

namespace Web.NewHR.UserControls
{
    public partial class EmpPayInfoCtl : BaseUserControl
    {
        
        public void LoadInfo()
        {
                int EmpID = 0;
                if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
                {
                    EmpID = int.Parse(Request.QueryString["ID"]);
                }
                else
                    EmpID = SessionManager.CurrentLoggedInEmployeeId;


                EEmployee emp = EmployeeManager.GetEmployeeById(EmpID);

                // PF start date
                Company company = SessionManager.CurrentCompany;
                ECurrentStatus pfStatusDate = EmployeeManager.GetStatusStart(EmpID, company.PFRFFunds[0].EffectiveFrom);
                if (pfStatusDate != null)
                {
                    dispPFStartDate.Text = pfStatusDate.FromDate;
                    if (IsEnglish == false)
                        dispPFStartDate.Text += " (" + pfStatusDate.FromDateEng.ToString("yyyy/MM/dd") + " AD)";
                }

                if (!string.IsNullOrEmpty(emp.EFundDetails[0].PFRFNo))
                    dispPFNo.Text = emp.EFundDetails[0].PFRFNo;

                if (!string.IsNullOrEmpty(emp.EFundDetails[0].CITNo))
                    disCITNo.Text = emp.EFundDetails[0].CITNo;

                if (!string.IsNullOrEmpty(emp.EFundDetails[0].PANNo))
                    dispPanNo.Text = emp.EFundDetails[0].PANNo;

                if (!string.IsNullOrEmpty(emp.PPays[0].BankACNo))
                    dispAccountNo.Text = emp.PPays[0].BankACNo;

                if (CommonManager.CompanySetting.WhichCompany==WhichCompany.NIBL)
                {
                    dispLabelGratuityStartDate.Show();
                    dispGratuityStartDate.Show();
                    PayrollPeriod _PayrollPeriod = CommonManager.GetLastPayrollPeriod();
                    DateTime GratuityStartDateForNIBL = PayManager.GetGratuityStartDateForNIBL(EmpID, _PayrollPeriod.EndDateEng.Value);
                }

        }
      
        
    }
}