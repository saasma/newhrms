﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Entity;
using BLL;
using System.IO;
using Utils;
using System.Text;
using BLL.BO;
using Web.Helper;
using Utils.Helper;
namespace Web.NewHR.UserControls
{
    public partial class ViewDocumentCtrl : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest && !IsPostBack)
                Initialise();
        }

        private void Initialise()
        {
            if (!DocumentManager.CanUserAddEditDocument())
            {
                editLink.Hide();                
                CommandColumnEmail.Hide();
            }

            if (!DocumentManager.CanUserDeleteDocument())
            {
                CommandColumnDelete.Hide();
            }

            if (!string.IsNullOrEmpty(Request.QueryString["DocumentId"]))
            {
                Guid documentId = new Guid(Request.QueryString["DocumentId"]);

                editLink.NavigateUrl = "~/NewHr/DocumentManagement/Document.aspx?DocumentId=" + documentId;

                DocDocument objDocDocument = DocumentManager.GetDocDocumentById(documentId);
                if (objDocDocument != null)
                {
                    dfTitle.Text = objDocDocument.Title;

                    spanNameOfTheParty.InnerText = DocumentManager.GetDocPartyById(objDocDocument.PartyRef_ID).Name;
                    spanID.InnerText = objDocDocument.ID;
                    spanDocumentCategory.InnerText = DocumentManager.GetDocCategoryById(objDocDocument.CategoryID).Name;
                    spanDocumentSubCategory.InnerText = DocumentManager.GetDocSubCategoryById(objDocDocument.SubCategoryID).Name;
                    spanDocumentDate.InnerText = objDocDocument.DocumentDateEng.ToLongDateString();

                    spanDocumentDateDetails.InnerHtml = WebHelper.TimeAgoOrInWithDays(objDocDocument.DocumentDateEng);

                    if (objDocDocument.ExpiryDateEng != null)
                    {
                        spanExpirationDate.InnerText = objDocDocument.ExpiryDateEng.Value.ToLongDateString();

                        spanExpirationDateDetails.InnerText = WebHelper.TimeAgoOrInWithDays(objDocDocument.ExpiryDateEng.Value);

                    }

                    spanDescription.InnerText = objDocDocument.Description;

                    //spanPrivate.InnerText = (objDocDocument.IsPrivate == true ? "Yes" : "No");

                    //List<DocAssign> listDocAssign = DocumentManager.GetDocAssignByDocumentId(objDocDocument.DocumentID);
                    //List<string> listAssignee = new List<string>();
                    //foreach (var item in listDocAssign)
                    //{
                    //    UUser objUUser = UserManager.GetUserByUserID(item.UserID);
                    //    listAssignee.Add(objUUser.UserName);
                    //}

                    //spanAssignees.InnerText = string.Join(",", listAssignee.ToArray());

                    gridFiles.Store[0].DataSource = DocumentManager.GetDocAttachmentByDocumentId(objDocDocument.DocumentID);
                    gridFiles.Store[0].DataBind();

                    BindDocumentHistory(documentId);

                    BindAdditionalInformationGridEditData(documentId);
                }

            }
        }

        private void BindDocumentHistory(Guid documentID)
        {
            gridDocumentHistory.Store[0].DataSource = DocumentManager.GetDocumentHistoryByDocumentID(documentID);
            gridDocumentHistory.Store[0].DataBind();
        }

        protected void btnDownload_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(hdnFileID.Text))
            {
                NewMessage.ShowWarningMessage("Attachment not found.");
                return;
            }

            Guid fileId = new Guid(hdnFileID.Text);
            DocAttachment doc = DocumentManager.GetDocAttacmentById(fileId);

            string path = Server.MapPath(@"~/uploads/Document/" + doc.ServerFileName);
            string contentType = doc.FileType;
            string name = doc.UserFileName;

            byte[] bytes = File.ReadAllBytes(path);

            Context.Response.Clear();
            Context.Response.ClearHeaders();
            Context.Response.AppendHeader("Content-Disposition", "attachment; filename=\"" + name + "\"");
            Context.Response.ContentType = contentType;
            Context.Response.BinaryWrite(bytes);
            Context.Response.End();
        }

        protected void btnEmail_Click(object sender, DirectEventArgs e)
        {
            UUser objUUser = UserManager.GetUserByUserID(SessionManager.CurrentLoggedInUserID);
            if (objUUser != null && !string.IsNullOrEmpty(objUUser.Email))
                chkSendMeACopy.BoxLabel = string.Format("Send me ({0}) a copy", objUUser.Email);
            else
                chkSendMeACopy.Hide();

            windowSendEmail.Center();
            windowSendEmail.Show();
        }

        private void ClearMailControls()
        {
            txtToEmail.Clear();
            txtSubject.Clear();
            txtCC.Clear();
            txtMessage.Clear();
            chkSendMeACopy.Clear();
            chkExcludeAttachment.Clear();
        }

        public void btnSendEmail_Click(object sender, DirectEventArgs e)
        {
            Guid fileId = new Guid(hdnFileID.Text);
            DocAttachment doc = DocumentManager.GetDocAttacmentById(fileId);

            UUser objUUser = UserManager.GetUserByUserID(SessionManager.CurrentLoggedInUserID);
            string senderName = "";
            string senderEmail = "";

            if (objUUser != null)
            {
                if (objUUser.UserMappedEmployeeId != null)
                    senderName = EmployeeManager.GetEmployeeById(objUUser.UserMappedEmployeeId.Value).Name;
                else
                    senderName = objUUser.UserName;

                if (!string.IsNullOrEmpty(objUUser.Email))
                    senderEmail = objUUser.Email;
            }

            string cc  = "";
            cc = txtCC.Text.Trim();

            if(chkSendMeACopy.Checked)
            {
                if(string.IsNullOrEmpty(cc))
                    cc = senderEmail;
                else
                    cc += "," + senderEmail;
            }

            string serverFileName = Server.MapPath(@"~/uploads/Document/" + doc.ServerFileName);
            string userFileName = doc.UserFileName;

            bool isMailSent = Utils.Helper.SMTPHelper.SendMailWithAttachment(txtToEmail.Text.Trim(), txtMessage.Text.Trim(), txtSubject.Text.Trim(), txtCC.Text.Trim(), senderName, senderEmail, userFileName, serverFileName);
            if (isMailSent)
            {
                NewMessage.ShowNormalMessage("Email sent successfully.");
                windowSendEmail.Close();
            }
            else
            {
                NewMessage.ShowWarningMessage("Error while sending email.");
            }
        }

        protected void btnDeleteFile_Click(object sender, DirectEventArgs e)
        {
            Guid fileId = new Guid(hdnFileID.Text);

            string itemString = e.ExtraParams["itemList"];
            List<DocAttachment> list = JSON.Deserialize<List<DocAttachment>>(itemString);

            DocAttachment obj = list.SingleOrDefault(x => x.FileID == fileId);

            string path = Server.MapPath(@"~/uploads/Document/" + obj.ServerFileName);

            list.Remove(obj);

            if (File.Exists(path))
                File.Delete(path);

            Status status = DocumentManager.DeleteDocAttachment(fileId);
            if (status.IsSuccess)
                NewMessage.ShowNormalMessage("Document attachment deleted successfully.");
            else
                NewMessage.ShowWarningMessage(status.ErrorMessage);

            gridFiles.Store[0].DataSource = list;
            gridFiles.Store[0].DataBind();
        }

        private void BindAdditionalInformationGridEditData(Guid documentID)
        {
            List<DocAdditionalInformationBO> list = DocumentManager.GetAdditionalInformationBOByDocumentID(documentID);
            if (list.Count > 0)
            {
                gridAdditionalInformation.Store[0].DataSource = list;
                gridAdditionalInformation.Store[0].DataBind();

                trAddInformation.Visible = true;
            }
        }

    }
}