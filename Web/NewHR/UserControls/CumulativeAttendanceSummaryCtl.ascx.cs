﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using Utils.Helper;
using BLL.BO;

namespace Web.NewHR.UserControls
{
    public partial class CumulativeAttendanceSummaryCtl : BaseUserControl
    {
        private PageViewType _PageViewType = PageViewType.Admin;
        public PageViewType PageView
        {
            get{return _PageViewType;}
            set{_PageViewType = value;}
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialize();



            }

            JavascriptHelper.AttachNonDialogPopUpCode(Page, "shiftPopup", "../ExcelWindow/CumulativeSummaryExcel.aspx", 450, 500);



        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (this.PageView == PageViewType.ManagerEmployee)
            {
                cmbBranch.Hide();
                cmbDepartment.Hide();
                cmbDepartment.Hide();
                cmbLevel.Hide();
                cmbDesignation.Hide();
                cmbEmpSearch.Hide();


                ColumnLevel.Visible = false;
                ColumnDesignation.Visible = false;
                ColumnBranch.Visible = false;
                ColumnDepartment.Visible = false;
            }
        }

        private void Initialize()
        {
            PayrollPeriod period = CommonManager.GetLastPayrollPeriod();

            //calDate.Text = CustomDate.GetTodayDate(IsEnglish).ToString();
            cmbYear.Store[0].DataSource = CommonManager.GetYearList();
            cmbYear.Store[0].DataBind();

            ExtControlHelper.ComboBoxSetSelected(period.Year.ToString(), cmbYear);
            ExtControlHelper.ComboBoxSetSelected(period.Month.ToString(), cmbMonth);

            cmbMonth.Store[0].DataSource = DateManager.GetCurrentMonthList();
            cmbMonth.Store[0].DataBind();

            List<Branch> branchList = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            cmbBranch.Store[0].DataSource = branchList;
            cmbBranch.Store[0].DataBind();

            cmbDepartment.Store[0].DataSource = DepartmentManager.GetAllDepartments().OrderBy(x => x.Name).ToList();
            cmbDepartment.Store[0].DataBind();

            cmbDesignation.Store[0].DataSource = new CommonManager().GetAllDesignations().OrderBy(x => x.LevelAndDesignation).ToList();
            cmbDesignation.Store[0].DataBind();

            this.storeLevel.DataSource = ActivityManager.GetLevels().OrderBy(x => x.Name);
            this.storeLevel.DataBind();


        }

        private void Clear()
        {

        }


        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {

            

            int year = int.Parse(cmbYear.SelectedItem.Value);
            int month = int.Parse(cmbMonth.SelectedItem.Value);

            PayrollPeriod period = CommonManager.GetPayrollPeriod(month, year);

            if (period == null)
            {
                NewMessage.ShowWarningMessage("Period does not exists for this date.");
            }

            X.Js.AddScript("periodId=" + period.PayrollPeriodId + ";");

            int totalRecords = 0;
            int branchId = -1, depId = -1, levelId = -1, designationId = -1;

            if (cmbBranch.SelectedItem != null && cmbBranch.SelectedItem.Value != null)
                branchId = int.Parse(cmbBranch.SelectedItem.Value);
            if (cmbDepartment.SelectedItem != null && cmbDepartment.SelectedItem.Value != null)
                depId = int.Parse(cmbDepartment.SelectedItem.Value);
            if (cmbLevel.SelectedItem != null && cmbLevel.SelectedItem.Value != null)
                levelId = int.Parse(cmbLevel.SelectedItem.Value);
            if (cmbDesignation.SelectedItem != null && cmbDesignation.SelectedItem.Value != null)
                designationId = int.Parse(cmbDesignation.SelectedItem.Value);

            int employeeId = -1;

            int.TryParse(cmbEmpSearch.SelectedItem.Value, out employeeId);

            if (employeeId == 0)
                employeeId = -1;

            if (this.PageView == PageViewType.ManagerEmployee)
                employeeId = SessionManager.CurrentLoggedInEmployeeId;

            hdnSortBy.Text = (e.Sort[0].Property + " " + e.Sort[0].Direction).ToLower();

            List<Report_GetCumulativeSummaryResult> list = EmployeeManager.GetCumulativeSummaryReport
                (branchId, depId, levelId, designationId, employeeId, e.Page - 1, e.Limit, (e.Sort[0].Property + " " + e.Sort[0].Direction).ToLower(), ref totalRecords
                , period.PayrollPeriodId, (int)this.PageView, SessionManager.CurrentLoggedInEmployeeId);


            e.Total = totalRecords;

            //if (list.Any())
            storeEmpList.DataSource = list;
            storeEmpList.DataBind();

        }


        public void btnExport_Click(object sender, EventArgs e)
        {
            int year = int.Parse(cmbYear.SelectedItem.Value);
            int month = int.Parse(cmbMonth.SelectedItem.Value);

            PayrollPeriod period = CommonManager.GetPayrollPeriod(month, year);

            if (period == null)
            {
                //NewMessage.ShowWarningMessage("Period does not exists for this date.");
                Bll.ExcelHelper.ExportToExcel("Attendance Deduction Report", new List<TextValue> { }, new List<string> { }, new List<string> { });
                return;
            }

            int totalRecords = 0;
            int branchId = -1, depId = -1, levelId = -1, designationId = -1;

            if (cmbBranch.SelectedItem != null && cmbBranch.SelectedItem.Value != null)
                branchId = int.Parse(cmbBranch.SelectedItem.Value);
            if (cmbDepartment.SelectedItem != null && cmbDepartment.SelectedItem.Value != null)
                depId = int.Parse(cmbDepartment.SelectedItem.Value);
            if (cmbLevel.SelectedItem != null && cmbLevel.SelectedItem.Value != null)
                levelId = int.Parse(cmbLevel.SelectedItem.Value);
            if (cmbDesignation.SelectedItem != null && cmbDesignation.SelectedItem.Value != null)
                designationId = int.Parse(cmbDesignation.SelectedItem.Value);

            int employeeId = -1;

            int.TryParse(cmbEmpSearch.SelectedItem.Value, out employeeId);

            if (employeeId == 0)
                employeeId = -1;

            if (this.PageView == PageViewType.ManagerEmployee)
                employeeId = SessionManager.CurrentLoggedInEmployeeId;

            List<Report_GetCumulativeSummaryResult> list = EmployeeManager.GetCumulativeSummaryReport
                (branchId, depId, levelId, designationId, employeeId, 0, 9999999, (hdnSortBy.Text).ToLower(), ref totalRecords
                , period.PayrollPeriodId, (int)this.PageView, SessionManager.CurrentLoggedInEmployeeId);



            Bll.ExcelHelper.ExportToExcel("Cumulative Summary Report", list,
                new List<string> { "TotalRows" },
            new List<String>() { },
            new Dictionary<string, string>() { { "AbsentDays", "Absent Days" },
            { "LWPDays", "LWP Days" }, { "LateDays", "Late Days" } ,
            { "LeaveDays", "Leave Days" }},
            new List<string>() {  }
            , new List<string> { }
            , new List<string> { "JoinDateText" }
            , new Dictionary<string, string>() { { "Cumulative Summary Report for ", period.Name } }
            , new List<string> { });


        }

        private void ClearFields()
        {
            cmbEmployee.Clear();
            txtLateDays.Clear();
            txtLeaveDays.Clear();
            txtHalfLeaveCount.Clear();
        }

        protected void btnAddNew_Click(object sender, DirectEventArgs e)
        {
            ClearFields();
            cmbEmployee.Enable();
            hdnEmployeeId.Text = "";
            WCumulativeSummary.Center();
            WCumulativeSummary.Show();
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveUpdCAS");
            if (Page.IsValid)
            {
                List<CumulativeSummary> list = new List<CumulativeSummary>();

                CumulativeSummary obj = new CumulativeSummary();
                obj.EmployeeId = int.Parse(hdnEmployeeId.Text);

                int year = int.Parse(cmbYear.SelectedItem.Value);
                int month = int.Parse(cmbMonth.SelectedItem.Value);

                PayrollPeriod period = CommonManager.GetPayrollPeriod(month, year);
                if (period == null)
                {
                    NewMessage.ShowWarningMessage("Payroll period does not exists for the selected year and month.");
                    return;
                }

                obj.PayrollPeriodId = period.PayrollPeriodId;

                obj.LateDays = double.Parse(txtLateDays.Text.Trim());
                obj.LeaveDays = double.Parse(txtLeaveDays.Text.Trim());

                if(!string.IsNullOrEmpty(txtHalfLeaveCount.Text))
                    obj.HalfDayLeaveDaysCount = double.Parse(txtHalfLeaveCount.Text.Trim());

                list.Add(obj);

                Status status = AttendanceManager.SaveUpdateCumulativeSummary(list, obj.PayrollPeriodId);
                if (status.IsSuccess)
                {
                    NewMessage.ShowNormalMessage("Record saved successfully.");
                    WCumulativeSummary.Close();
                    X.Js.Call("searchList");
                }
                else
                    NewMessage.ShowWarningMessage(status.ErrorMessage);                
            }
        }

        protected void btnEdit_Click(object sender, DirectEventArgs e)
        {
            ClearFields();

            int employeeId = int.Parse(hdnEmployeeId.Text);

            cmbEmployee.SetRawValue(employeeId.ToString() + " - " + EmployeeManager.GetEmployeeById(employeeId).Name);
            cmbEmployee.Disable();

            int year = int.Parse(cmbYear.SelectedItem.Value);
            int month = int.Parse(cmbMonth.SelectedItem.Value);

            PayrollPeriod period = CommonManager.GetPayrollPeriod(month, year);
            if (period == null)
            {
                NewMessage.ShowWarningMessage("Payroll period does not exists for the selected year and month.");
                return;
            }

            CumulativeSummary obj = AttendanceManager.GetCumulativeSummaryById(employeeId, period.PayrollPeriodId);
            if (obj != null)
            {
                txtLateDays.Text = obj.LateDays.ToString();
                txtLeaveDays.Text = obj.LeaveDays.ToString();

                if (obj.HalfDayLeaveDaysCount != null)
                    txtHalfLeaveCount.Text = obj.HalfDayLeaveDaysCount.Value.ToString();
            }

            WCumulativeSummary.Center();
            WCumulativeSummary.Show();
        }
        

    }
}