﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NonMonetaryAwardCtrlView.ascx.cs"
    Inherits="Web.NewHR.UserControls.NonMonetaryAwardCtrlView" %>

<ext:GridPanel ID="gridNMAward" runat="server" Width="920" Cls="itemgrid">
    <Store>
        <ext:Store ID="storeNMAward" runat="server">
            <Model>
                <ext:Model ID="Model1" runat="server" IDProperty="NomineeId">
                    <Fields>
                        <ext:ModelField Name="AwardId" Type="String" />
                        <ext:ModelField Name="AwardName" Type="string" />
                        <ext:ModelField Name="AwardedBy" Type="string" />
                        <ext:ModelField Name="AwardDate" Type="string" />
                        <ext:ModelField Name="Description" Type="string" />
                        <ext:ModelField Name="IsEditable" Type="Int" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
    </Store>
    <ColumnModel>
        <Columns>
            <ext:Column ID="Column1" Width="200" runat="server" Text="Award Name" DataIndex="AwardName"
                Wrap="true" />
            <ext:Column ID="Column2" Width="200" runat="server" Text="Awarded By" DataIndex="AwardedBy"
                Wrap="true">
            </ext:Column>
            <ext:Column ID="Column6" Width="100" runat="server" Text="Award Date" DataIndex="AwardDate">
            </ext:Column>
            <ext:Column ID="Column3" Width="250" runat="server" Text="Description" DataIndex="Description"
                Wrap="true">
            </ext:Column>
            <ext:Column ID="Column4" Width="170" runat="server" Text="">
            </ext:Column>
        </Columns>
    </ColumnModel>
    <SelectionModel>
        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
    </SelectionModel>
</ext:GridPanel>
