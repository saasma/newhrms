﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;

using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;

namespace Web.NewHR.UserControls
{
    public partial class PublicationCtl : BaseUserControl
    {
        private bool _isDisplayMode = false;
        public bool IsDisplayMode
        {
            set
            {
                if (value == true)
                {
                    _isDisplayMode = value;
                    LinkButton1.Visible = false;
                    AEPublicationWindow.Visible = false;
                    CommandColumn1.Visible = false;
                }
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();

            }
        }

        protected void Initialise()
        {
            int EmployeeID = GetEmployeeID();
            this.LoadPublicationGrid(EmployeeID);


            //Load Data
            if (EmployeeID > 0)
            {
                CommonManager _CommonManager = new CommonManager();
                cmbCountry.Store[0].DataSource = _CommonManager.GetAllCountries();
                cmbCountry.Store[0].DataBind();

                cmbPublicationType.Store[0].DataSource = CommonManager.GetPublicationTypeList();
                cmbPublicationType.Store[0].DataBind();
            }
            
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {

            this.SaveFormData();

        }

        private void SaveFormData()
        {

            Status myStatus = new Status();
            HPublication _HPublication = new HPublication();
            _HPublication.EmployeeId = this.GetEmployeeID();
            bool isSave = bool.Parse(this.hdnPublicationID.Text == "" ? "true" : "false");
            if (!isSave)
                _HPublication.PublicationId = int.Parse(this.hdnPublicationID.Text);
        
            _HPublication.PublicationName = txtPublicationName.Text.Trim();
            
            if (!string.IsNullOrEmpty(cmbPublicationType.SelectedItem.Text))
            {
                _HPublication.PublicationTypeName = cmbPublicationType.SelectedItem.Text;
                _HPublication.PublicationTypeID = int.Parse(cmbPublicationType.SelectedItem.Value);
            }
            
            if (!string.IsNullOrEmpty(cmbCountry.SelectedItem.Text))
            {

                _HPublication.Country = cmbCountry.SelectedItem.Text;
               
            }
      
            _HPublication.Publisher = txtPublisher.Text.Trim();
            if(!string.IsNullOrEmpty(txtYear.Text.Trim()))
            _HPublication.Year = int.Parse(txtYear.Text.Trim());

           
            myStatus = NewHRManager.Instance.InsertUpdatePublication(_HPublication, isSave);

            if (myStatus.IsSuccess)
            {
                if (isSave)
                    NewMessage.ShowNormalMessage("Record Save successfully");
                else
                    NewMessage.ShowNormalMessage("Record Updated successfully");

                //  this.ClearFields();
                this.AEPublicationWindow.Close();
                this.LoadPublicationGrid(this.GetEmployeeID());

            }
            else
                NewMessage.ShowWarningMessage(myStatus.ErrorMessage);
        }


        protected void LoadPublicationGrid(int EmployeeID)
        {
            List<HPublication> _HPublication = NewHRManager.GetPublicationByEmployeeID(EmployeeID);


            this.StorePublication.DataSource = _HPublication;
            this.StorePublication.DataBind();

            if (_isDisplayMode && _HPublication.Count <= 0)
                GridPublication.Hide();
        }


        protected int GetEmployeeID()
        {
            //Guid? AssetID = null;
            int EmpID = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                EmpID = int.Parse(Request.QueryString["ID"]);
            }
            else
            {
                EmpID = SessionManager.CurrentLoggedInEmployeeId;
            }
            return EmpID;
        }

        protected void btnAddNewLine_Click(object sender, DirectEventArgs e)
        {
            this.ClearFields();
            this.AEPublicationWindow.Show();

        }

        private object[] PublicationFillData
        {
            get
            {
                return new object[]
            {
                new object[] { "", "", "",""},
               
            };
            }
        }

        protected void GridPublication_Command(object sender, DirectEventArgs e)
        {
            string commandName = e.ExtraParams["command"];
            int PublicationID = int.Parse(e.ExtraParams["ID"]);
            HPublication _HPublication = NewHRManager.GetPublicationDetailsById(int.Parse(e.ExtraParams["ID"]));
            switch (commandName)
            {
                case "Delete":
                    this.DeleteData(PublicationID);
                    break;
             
                case "Edit":
                    this.editPublication(PublicationID);
                    break;
            }

        }

        protected void DeleteData(int ID)
        {

            bool result = NewHRManager.DeletePublicationByID(ID);
            this.LoadPublicationGrid(this.GetEmployeeID());
          
        }

        protected void ClearFields()
        {
            this.hdnPublicationID.Text = "";
            txtPublicationName.Text = "";
            txtPublisher.Text = "";
            cmbCountry.Value = "";
            txtYear.Text = "";
            cmbPublicationType.Value = "";
            btnSave.Text = Resources.Messages.Save;
        }

        public void editPublication(int PublicationID)
        {

            HPublication _HPublication = NewHRManager.GetPublicationDetailsById(PublicationID);
            this.hdnPublicationID.Text = PublicationID.ToString();
            txtPublicationName.Text = _HPublication.PublicationName;

            cmbPublicationType.Store[0].ClearFilter();
            if (_HPublication.PublicationTypeID != null)
                cmbPublicationType.SetValue(_HPublication.PublicationTypeID.ToString());
            else
                cmbPublicationType.ClearValue();

            //cmbPublicationType.SetValue(_HPublication.PublicationTypeID.ToString());

            txtPublisher.Text = _HPublication.Publisher;
            cmbCountry.SetValue(_HPublication.Country);
            txtYear.Text = _HPublication.Year.ToString();
            btnSave.Text = Resources.Messages.Update;
            this.AEPublicationWindow.Show();
        }

       

    }
}