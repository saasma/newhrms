﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EducationPopUp.ascx.cs" Inherits="Web.NewHR.UserControls.EducationPopUp" %>



<ext:Hidden runat="server" ID="hdnEduID">
</ext:Hidden>

        <table class="fieldTable">
            <tr>
                <td colspan="2">
                    <ext:TextField Width="330px" ID="txtCourseName" runat="server" FieldLabel="Course Name *"
                        LabelAlign="top" LabelSeparator="" />
                    <asp:RequiredFieldValidator Display="None" ID="valtxtInspectedBy" runat="server"
                        ValidationGroup="EducationSaveUpdate" ControlToValidate="txtCourseName" ErrorMessage="Course Name is required." />
                </td>
                
            </tr>
            <tr>
                <td>
                    <ext:ComboBox ID="cmbLevel" runat="server" ValueField="ID" DisplayField="Name" 
                        FieldLabel="Level" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                        QueryMode="Local">
                      <%--  <Items>
                            <ext:ListItem Text="Master Degree" Value="1" />
                            <ext:ListItem Text="Graduate" Value="2" />
                            <ext:ListItem Text="MPhil" Value="3" />
                            <ext:ListItem Text="Intermediate / +2" Value="4" />
                            <ext:ListItem Text="School" Value="5" />
                            <ext:ListItem Text="Professional" Value="6" />
                            <ext:ListItem Text="Vocational" Value="7" />
                        </Items>--%>
                          <Store>
                            <ext:Store ID="Store3" runat="server">
                                <Model>
                                    <ext:Model ID="Model3" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="ID" Type="String" />
                                            <ext:ModelField Name="Name" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>                         
                    </ext:ComboBox>
                  <%--  <asp:RequiredFieldValidator Display="None" ID="valcmbLevel" runat="server" ValidationGroup="EducationSaveUpdate"
                        ControlToValidate="cmbLevel" ErrorMessage="Please enter the Level." />--%>
                </td>
                <td>
                    <ext:ComboBox ID="cmbFaculty" runat="server" ValueField="ID" DisplayField="Name"
                        FieldLabel="Faculty" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                        QueryMode="Local">
                        <%--<Items>
                            <ext:ListItem Text="Management" Value="1" />
                            <ext:ListItem Text="Administration" Value="2" />
                            <ext:ListItem Text="Engineering" Value="3" />
                            <ext:ListItem Text="Humanities" Value="4" />
                            <ext:ListItem Text="Science" Value="5" />
                        </Items>--%>
                        <Store>
                            <ext:Store ID="Store1" runat="server">
                                <Model>
                                    <ext:Model ID="Model2" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="ID" Type="String" />
                                            <ext:ModelField Name="Name" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                   <%-- <asp:RequiredFieldValidator Display="None" ID="valcmbFaculty" runat="server" ValidationGroup="EducationSaveUpdate"
                        ControlToValidate="cmbFaculty" ErrorMessage="Please enter Faculty." />--%>
                </td>
                <td valign="bottom" style="padding-left:40px;">
                    <ext:FileUploadField ID="FileEduDocumentUpload" runat="server" Width="300" Icon="Attach" ButtonOnly="true" LabelStyle="color:red; font-weight:bold;"
                        FieldLabel="Attach Certificate" LabelAlign="Top" LabelSeparator="" /><br />

                    <ext:LinkButton runat="server" ID="lnkFileName" OnDirectClick="lnkFileName_Click"
                        Text="" Hidden="true">
                        <DirectEvents>
                            <Click OnEvent="lnkFileName_Click">                                
                            </Click>
                        </DirectEvents>
                    </ext:LinkButton>

                    <ext:LinkButton Icon="Delete" runat="server" ID="lnkDeleteFile" OnDirectClick="lnkDeleteFile_Click"
                        Text="Delete file" Hidden="true">
                        <DirectEvents>
                            <Click OnEvent="lnkDeleteFile_Click">
                                <EventMask ShowMask="true" />
                                <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the file?" />
                            </Click>
                        </DirectEvents>
                    </ext:LinkButton>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <ext:TextField ID="txtInstitute" Width="330px" runat="server" FieldLabel="Institute"
                        LabelAlign="top" LabelSeparator="" />
                  <%--  <asp:RequiredFieldValidator Display="None" ID="valtxtInstitute" runat="server" ValidationGroup="EducationSaveUpdate"
                        ControlToValidate="txtInstitute" ErrorMessage="Please enter the Institute." />--%>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <ext:TextField ID="txtUniversity" Width="330px" runat="server" FieldLabel="University"
                        LabelAlign="top" LabelSeparator="" />
                    <%--  <asp:RequiredFieldValidator Display="None" ID="valtxtUniversity" runat="server" ValidationGroup="EducationSaveUpdate"
                        ControlToValidate="txtUniversity" ErrorMessage="Please enter the University." />--%>
                </td>
            </tr>
            <tr>
                <td>
                    <ext:ComboBox ID="cmbCountry" runat="server" ValueField="CountryName" DisplayField="CountryName"
                        FieldLabel="Country" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                        QueryMode="Local">
                        <Store>
                            <ext:Store ID="Store2" runat="server">
                                <Model>
                                    <ext:Model ID="Model4" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="CountryName" />
                                            <ext:ModelField Name="CountryName" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                    <%--<asp:RequiredFieldValidator Display="None" ID="valcmbCountry" runat="server" ValidationGroup="EducationSaveUpdate"
                        ControlToValidate="cmbCountry" ErrorMessage="Please select the Country." />--%>
                </td>
            </tr>
            <tr>
                <td>
                    <ext:TextField ID="txtPercentageGrade" runat="server" FieldLabel="Percentage or Grade"
                        LabelAlign="top" LabelSeparator="" />
                  <%--  <asp:RequiredFieldValidator Display="None" ID="valtxtPercentageGrade" runat="server"
                        ValidationGroup="EducationSaveUpdate" ControlToValidate="txtPercentageGrade"
                        ErrorMessage="Please enter a Percentage or Grade." />--%>
                </td>
                <td>
                    <ext:ComboBox ID="cmbDivision" runat="server" ValueField="DivisionId" DisplayField="Division"
                        FieldLabel="Division" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                        QueryMode="Local">
                       <%-- <Items>
                            <ext:ListItem Text="Distinction" Value="1" />
                            <ext:ListItem Text="First" Value="2" />
                            <ext:ListItem Text="Second" Value="3" />
                            <ext:ListItem Text="Third" Value="4" />
                            <ext:ListItem Text="Pass" Value="5" />
                            <ext:ListItem Text="Not Specified" Value="6" />
                        </Items>--%>
                         <Store>
                            <ext:Store ID="Store4" runat="server">
                                <Model>
                                    <ext:Model ID="Model5" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="DivisionId" Type="String"/>
                                            <ext:ModelField Name="Division" Type="String"/>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                    <%-- <asp:RequiredFieldValidator Display="None" ID="valcmbDivision" runat="server" ValidationGroup="EducationSaveUpdate"
                        ControlToValidate="cmbDivision" ErrorMessage="Please enter the Division." />--%>
                </td>
            </tr>
            <tr>
                <td>
                    <ext:TextField ID="txtPassedYear" runat="server" FieldLabel="Passed Year" LabelAlign="top"
                        LabelSeparator="" MaskRe="[0-9]|\.|%" />
                   <%-- <asp:RequiredFieldValidator Display="None" ID="valtxtPassedYear" runat="server" ValidationGroup="EducationSaveUpdate"
                        ControlToValidate="txtPassedYear" ErrorMessage="Please enter the Passed Year." />--%>
                </td>
                <td style="padding-top: 5px">
                    <ext:TextField ID="txtPassedYearNep" runat="server" FieldLabel="पुरा गरेको बर्ष"
                        LabelAlign="top" LabelSeparator="" MaskRe="[0-9]|\.|%" />
                    <%-- <asp:RequiredFieldValidator Display="None" ID="valtxtPassedYearNep" runat="server"
                        ValidationGroup="EducationSaveUpdate" ControlToValidate="txtPassedYearNep" ErrorMessage="Passed Year is required." />--%>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <ext:TextArea ID="txtMajorSubjects" runat="server" FieldLabel="Major Subjects" LabelSeparator=""
                        LabelAlign="Top" Rows="3" Width='330px' Cols="50" />
                </td>
            </tr>
            <tr id="trBtn" runat="server">
                <td valign="bottom" colspan="2">
                    <div class="popupButtonDiv">
                        <ext:Button runat="server" Cls="btn btn-primary" ID="btnSave" Text="<i></i>Save">
                            <DirectEvents>
                                <Click OnEvent="btnSave_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                            <Listeners>
                                <Click Handler="valGroup = 'EducationSaveUpdate';  if(CheckValidation()) return this.disable(); else return false;">
                                </Click>
                            </Listeners>
                        </ext:Button>
                        <div class="btnFlatOr">
                            or</div>
                        <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="LinkButton2" Cls="btnFlatLeftGap"
                            Text="<i></i>Cancel">
                            <Listeners>
                                <Click Handler="#{AEEducationWindow}.hide();">
                                </Click>
                            </Listeners>
                        </ext:LinkButton>
                    </div>
                </td>
            </tr>
        </table>
