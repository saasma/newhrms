﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PublicationCtrlView.ascx.cs"
    Inherits="Web.NewHR.UserControls.PublicationCtrlView" %>

<ext:GridPanel ID="GridPublication" runat="server" Width="920" Scroll="None">
    <Store>
        <ext:Store ID="StorePublication" runat="server">
            <Model>
                <ext:Model ID="Model1" runat="server" IDProperty="PublicationId">
                    <Fields>
                        <ext:ModelField Name="PublicationName" Type="string" />
                        <ext:ModelField Name="PublicationTypeName" Type="String" />
                        <ext:ModelField Name="Publisher" Type="string" />
                        <ext:ModelField Name="Country" Type="string" />
                        <ext:ModelField Name="Year" Type="string" />
                        <ext:ModelField Name="IsEditable" Type="Int" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
    </Store>
    <ColumnModel>
        <Columns>
            <ext:Column ID="Column1" runat="server" Text="Publication Name" DataIndex="PublicationName"
                Wrap="true" Flex="1" Width="200" />
            <ext:Column ID="Column2" runat="server" Text="Publication Type" DataIndex="PublicationTypeName"
                Wrap="true" Width="150">
            </ext:Column>
            <ext:Column ID="Column3" runat="server" Text="Publisher" DataIndex="Publisher" Width="180"
                Wrap="true">
            </ext:Column>
            <ext:Column ID="Column4" runat="server" Text="Country" DataIndex="Country" Flex="1"
                Width="120" Wrap="true">
            </ext:Column>
            <ext:Column ID="Column6" runat="server" Text="Year" DataIndex="Year" Width="70" Wrap="true">
            </ext:Column>
            <ext:Column ID="Column5" runat="server" Text="" Width="200">
            </ext:Column>
        </Columns>
    </ColumnModel>
    <SelectionModel>
        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
    </SelectionModel>
</ext:GridPanel>
