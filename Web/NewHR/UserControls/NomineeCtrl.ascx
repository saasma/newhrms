﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NomineeCtrl.ascx.cs" Inherits="Web.NewHR.UserControls.NomineeCtrl" %>

<script type="text/javascript">

    var prepareHRNominee = function (grid, toolbar, rowIndex, record) {
        var editDelBtn = toolbar.items.get(1);
        if (record.data.IsEditable == 0) {
            editDelBtn.setVisible(false);
        }

    };
    
</script>

<ext:Hidden runat="server" ID="hdnNomineeId">
</ext:Hidden>

<div class="widget-body">
    <table class="fieldTable firsttdskip">
        <tr>
            <td>
                <ext:GridPanel ID="gridNominee" runat="server" Width="1000" Cls="itemgrid">
                    <Store>
                        <ext:Store ID="storeNominee" runat="server">
                            <Model>
                                <ext:Model ID="Model1" runat="server" IDProperty="NomineeId">
                                    <Fields>
                                        <ext:ModelField Name="NomineeId" Type="String" />
                                        <ext:ModelField Name="SN" Type="string" />
                                        <ext:ModelField Name="NomineeName" Type="string" />
                                        <ext:ModelField Name="Relation" Type="string" />
                                        <ext:ModelField Name="EffectiveDate" Type="string" />
                                        <ext:ModelField Name="IsEditable" Type="Int" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                    </Store>
                    <ColumnModel>
                        <Columns>
                            <ext:Column ID="Column5" Width="100" runat="server" Text="Nominee No" DataIndex="SN"/>
                            <ext:Column ID="Column1" Width="200" runat="server" Text="Nominee Name" DataIndex="NomineeName" Wrap="true" />
                            <ext:Column ID="Column2" Width="150" runat="server" Text="Relation" DataIndex="Relation" Wrap="true">
                            </ext:Column>
                            <ext:Column ID="Column6" Width="100" runat="server" Text="Effective Date" DataIndex="EffectiveDate">
                            </ext:Column>
                          
                       <ext:CommandColumn ID="CommandColumn21" runat="server" Width="40">
                                <Commands>
                                    <ext:CommandSeparator />
                                    <ext:GridCommand Cls="editGridButton" ToolTip-Text="Edit"  Text="<i class='fa fa-pencil'></i>" CommandName="Edit" />
                                    
                                </Commands>
                                <DirectEvents>
                                    <Command OnEvent="gridNominee_Command">
                                        <EventMask ShowMask="true">
                                        </EventMask>
                                        <ExtraParams>
                                            <ext:Parameter Name="ID" Value="record.data.NomineeId" Mode="Raw">
                                            </ext:Parameter>
                                            <ext:Parameter Name="command" Value="command" Mode="Raw">
                                            </ext:Parameter>
                                        </ExtraParams>
                                    </Command>
                                </DirectEvents>
                                <PrepareToolbar Fn="prepareHRNominee" />
                            </ext:CommandColumn>
                            <ext:CommandColumn ID="CommandColumn2" runat="server" Width="40">
                                <Commands>
                                    <ext:CommandSeparator />
                                    <ext:GridCommand Cls="editGridButton" ToolTip-Text="Delete"  Text="<i class='fa fa-trash-o'></i>"
                                        CommandName="Delete" />
                                </Commands>
                                <DirectEvents>
                                    <Command OnEvent="gridNominee_Command">
                                        <EventMask ShowMask="true">
                                        </EventMask>
                                        <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the record?" />
                                        <ExtraParams>
                                            <ext:Parameter Name="ID" Value="record.data.NomineeId" Mode="Raw">
                                            </ext:Parameter>
                                            <ext:Parameter Name="command" Value="command" Mode="Raw">
                                            </ext:Parameter>
                                        </ExtraParams>
                                    </Command>
                                </DirectEvents>
                                <PrepareToolbar Fn="prepareHRNominee" />
                            </ext:CommandColumn>
                             <ext:Column ID="Column4" Width="370" runat="server" Text="">
                            </ext:Column>
                        </Columns>
                    </ColumnModel>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                    </SelectionModel>
                </ext:GridPanel>
            </td>
        </tr>
    </table>
    <div class="buttonBlock" runat="server" id="buttonBlock">
       <ext:Button runat="server" Cls="btn btn-primary" Width="120" StyleSpec="margin-top:10px" Height="30"
            ID="btnAddNewLine" Text="<i></i>Add New Line">
            <DirectEvents>
                <Click OnEvent="btnAddNewLine_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:Button>
    </div>
</div>
<ext:Window ID="WNominee" runat="server" Title="Add/Edit Nominee" Icon="Application"
    Height="380" Width="430" BodyPadding="5" Hidden="true" Modal="true">
    <Content>
        <table class="fieldTable">
            <tr>
                <td colspan="2">
                    <ext:TextField Width="330" ID="txtNomineeName" runat="server" FieldLabel="Nominee Name *"
                        LabelAlign="top" LabelSeparator="" />
                    <asp:RequiredFieldValidator Display="None" ID="rfvNomineeName" runat="server"
                        ValidationGroup="SaveUpdateNominee" ControlToValidate="txtNomineeName" ErrorMessage="Nominee Name is required." />
                </td>
               
            </tr>
            <tr>
                <td>
                    <ext:ComboBox ID="cmbRelation" runat="server" ValueField="ID" DisplayField="Name" FieldLabel="Relation *" Width="160"
                        LabelAlign="top" LabelSeparator="" ForceSelection="true" QueryMode="Local">
                        <Store>
                            <ext:Store ID="Store1" runat="server">
                                <Model>
                                    <ext:Model ID="Model3" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="ID" Type="String" />
                                            <ext:ModelField Name="Name" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>                   
                    <asp:RequiredFieldValidator Display="None" ID="rfvRelation" runat="server"
                        ValidationGroup="SaveUpdateNominee" ControlToValidate="cmbRelation" ErrorMessage="Relation is required." />
                </td>
                <td>
                    <pr:CalendarExtControl ID="txtEffectiveDate" LabelSeparator="" runat="server" FieldLabel="Effective Date *"
                        Width="160" LabelAlign="Top">
                    </pr:CalendarExtControl>    
                     <asp:RequiredFieldValidator Display="None" ID="rfvEffectiveDate" runat="server"
                        ValidationGroup="SaveUpdateNominee" ControlToValidate="txtEffectiveDate" ErrorMessage="Effective Date is required." />               
                </td>
            </tr>
          
             <td colspan="2">
                    <ext:TextArea ID="txtRemarks" Width="330" runat="server" FieldLabel="Remarks" LabelAlign="Top"
                        LabelSeparator="" />
                </td>

    
            <tr>
                <td valign="bottom" colspan="2">
                    <div class="popupButtonDiv">
                        <ext:Button runat="server" Cls="btn btn-primary" ID="btnSave" Text="<i></i>Save">
                            <DirectEvents>
                                <Click OnEvent="btnSave_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                            <Listeners>
                                <Click Handler="valGroup = 'SaveUpdateNominee';  if(CheckValidation()) return this.disable(); else return false;">
                                </Click>
                            </Listeners>
                        </ext:Button>
                        <div class="btnFlatOr">
                            or</div>
                        <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="LinkButton2" Cls="btnFlatLeftGap"
                            Text="<i></i>Cancel">
                            <Listeners>
                                <Click Handler="#{WNominee}.hide();">
                                </Click>
                            </Listeners>
                        </ext:LinkButton>
                    </div>
                </td>
            </tr>
        </table>
    </Content>
</ext:Window>
