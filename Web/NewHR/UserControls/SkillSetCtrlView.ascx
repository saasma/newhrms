﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SkillSetCtrlView.ascx.cs"
    Inherits="Web.NewHR.UserControls.SkillSetCtrlView" %>

<ext:GridPanel ID="GridSkillSets" runat="server" Width="920" Cls="itemgrid" Scroll="None">
    <Store>
        <ext:Store ID="StoreSkillSets" runat="server">
            <Model>
                <ext:Model ID="Model1" runat="server" IDProperty="SkillSetId">
                    <Fields>
                        <ext:ModelField Name="SkillSetName" Type="string" />
                        <ext:ModelField Name="LevelOfExpertise" Type="String" />
                        <ext:ModelField Name="IsEditable" Type="Int" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
    </Store>
    <ColumnModel>
        <Columns>
            <ext:Column ID="Column1" runat="server" Text="Skill" DataIndex="SkillSetName" Width="200"
                Wrap="true" />
            <ext:Column ID="Column2" runat="server" Text="Expertise Level" DataIndex="LevelOfExpertise"
                Wrap="true" Width="150">
            </ext:Column>
            <ext:Column ID="Column3" runat="server" Width="570" />
        </Columns>
    </ColumnModel>
    <SelectionModel>
        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
    </SelectionModel>
</ext:GridPanel>
