﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;

namespace Web.NewHR.UserControls
{
    public partial class EmploymentPopup : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        protected void Initialise()
        {
            cmbExpCategory.Store[0].DataSource = NewHRManager.GetAllExperienceCategory();
            cmbExpCategory.Store[0].DataBind();

            int EmployeeID = GetEmployeeID();   
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {

            this.SaveFormData();

        }

        private void SaveFormData()
        {

            Status myStatus = new Status();
            HPreviousEmployment _HPreviousEmployment = new HPreviousEmployment();
            _HPreviousEmployment.EmployeeId = this.GetEmployeeID();
            bool isSave = bool.Parse(this.hdnPreviousEmploymentID.Text == "" ? "true" : "false");
            if (!isSave)
                _HPreviousEmployment.PrevEmploymentId = int.Parse(this.hdnPreviousEmploymentID.Text);

            if (cmbExpCategory.SelectedItem != null && cmbExpCategory.SelectedItem.Value != null)
                _HPreviousEmployment.CategoryID = int.Parse(cmbExpCategory.SelectedItem.Value);

            _HPreviousEmployment.Organization = txtOrganization.Text.Trim();
            _HPreviousEmployment.Place = txtPlace.Text.Trim();
            _HPreviousEmployment.Position = txtPosition.Text.Trim();
            _HPreviousEmployment.JobResponsibility = txtJobResponsibility.Text.Trim();
            if (!string.IsNullOrEmpty(calFrom.Text.Trim()))
            {
                _HPreviousEmployment.JoinDate = calFrom.Text.Trim();
                _HPreviousEmployment.JoinDateEng = BLL.BaseBiz.GetEngDate(_HPreviousEmployment.JoinDate, IsEnglish);
            }

            if (!string.IsNullOrEmpty(calTo.Text.Trim()))
            {
                _HPreviousEmployment.ResignDate = calTo.Text.Trim();
                _HPreviousEmployment.ResignDateEng = BLL.BaseBiz.GetEngDate(_HPreviousEmployment.ResignDate, IsEnglish);
            }

            if (!string.IsNullOrEmpty(txtToYear.Text.Trim()))
                _HPreviousEmployment.TOYEAR = txtToYear.Text.Trim();

            if (!string.IsNullOrEmpty(txtFromYear.Text.Trim()))
                _HPreviousEmployment.FROMYEAR = txtFromYear.Text.Trim();

            _HPreviousEmployment.ReasonForLeaving = txtReasonforLeaving.Text.Trim();
            _HPreviousEmployment.Notes = txtNote.Text.Trim();

            myStatus = NewHRManager.Instance.InsertUpdatePreviousEmployment(_HPreviousEmployment, isSave);

            if (myStatus.IsSuccess)
            {
                Window win = (Window)this.Parent.FindControl("AEPreviousEmploymentWindow");
                win.Close();
                if (isSave)
                    NewMessage.ShowNormalMessage("Record Saved Successfully.", "reloadEmpGrid()");
                else
                    NewMessage.ShowNormalMessage("Record Updated Successfully.", "reloadEmpGrid()");              
            }
            else
                NewMessage.ShowWarningMessage(myStatus.ErrorMessage);
        }

        protected void ChkIsYear_Change(object sender, DirectEventArgs e)
        {
            if (chkISYear.Checked == true)
            {
                txtFromYear.Show();
                txtToYear.Show();
                calFrom.Hide();
                calTo.Hide();
                calFrom.Clear();
                calTo.Clear();
            }
            else
            {
                txtFromYear.Hide();
                txtToYear.Hide();
                txtFromYear.Clear();
                txtToYear.Clear();
                calFrom.Show();
                calTo.Show();
            }
        }

        protected int GetEmployeeID()
        {
            int EmpID = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                EmpID = int.Parse(Request.QueryString["ID"]);
            }
            else
            {
                EmpID = SessionManager.CurrentLoggedInEmployeeId;
            }
            return EmpID;
        }

        public void ClearFields()
        {
            this.hdnPreviousEmploymentID.Text = "";
            cmbExpCategory.Text = "";
            txtOrganization.Text = "";
            txtPlace.Text = "";
            txtPosition.Text = "";
            txtJobResponsibility.Text = "";
            calFrom.Text = "";
            calTo.Text = "";
            txtReasonforLeaving.Text = "";
            txtNote.Text = "";
            txtToYear.Clear();
            txtFromYear.Clear();
            btnSave.Text = Resources.Messages.Save;
        }

        public void editPreviousEmployment(int PreviousEmploymentID)
        {

            ClearFields();
            HPreviousEmployment _HPreviousEmployment = NewHRManager.GetPreviousEmploymentDetailsById(PreviousEmploymentID);
            this.hdnPreviousEmploymentID.Text = PreviousEmploymentID.ToString();

            txtOrganization.Text = _HPreviousEmployment.Organization;
            txtPlace.Text = _HPreviousEmployment.Place;
            txtPosition.Text = _HPreviousEmployment.Position;
            txtJobResponsibility.Text = _HPreviousEmployment.JobResponsibility;
            calFrom.Text = _HPreviousEmployment.JoinDate;
            calTo.Text = _HPreviousEmployment.ResignDate;
            txtReasonforLeaving.Text = _HPreviousEmployment.ReasonForLeaving;
            if (_HPreviousEmployment.CategoryID != null)
                cmbExpCategory.SetValue(_HPreviousEmployment.CategoryID.ToString());

            if (!string.IsNullOrEmpty(_HPreviousEmployment.FROMYEAR) && !string.IsNullOrEmpty(_HPreviousEmployment.TOYEAR))
            {
                txtFromYear.Text = _HPreviousEmployment.FROMYEAR;
                txtToYear.Text = _HPreviousEmployment.TOYEAR;
                chkISYear.Checked = true;
                ChkIsYear_Change(null, null);
            }
          
            txtNote.Text = _HPreviousEmployment.Notes;
            btnSave.Text = Resources.Messages.Update;            
        }

        public void HideButtons()
        {
            btnSave.Hidden = true;
            LinkButton2.Hidden = true;
        }
    }
}