﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BranchTransferListCtl.ascx.cs"
    Inherits="Web.NewHR.UserControls.BranchTransferListCtl" %>
<ext:GridPanel StyleSpec="margin-top:10px;" ID="GridLevels" runat="server" Cls="itemgrid"
    Width="1000">
    <Store>
        <ext:Store ID="Store3" runat="server">
            <Model>
                <ext:Model ID="Model4" runat="server" IDProperty="BranchDepartmentId">
                    <Fields>
                        <ext:ModelField Name="BranchDepartmentId" Type="String" />
                        <ext:ModelField Name="EmployeeName" Type="string" />
                        <ext:ModelField Name="FromBranch" Type="string" />
                        <ext:ModelField Name="ToBranch" Type="string" />
                        <ext:ModelField Name="FromDepartment" Type="string" />
                        <ext:ModelField Name="ToDepartment" Type="string" />
                        <ext:ModelField Name="LetterNumber" Type="string" />
                        <ext:ModelField Name="LetterDate" Type="string" />
                        <ext:ModelField Name="DepartureDate" Type="string" />
                        <ext:ModelField Name="FromDate" Type="string" />
                        <ext:ModelField Name="TimeElapsed" Type="string" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
    </Store>
    <ColumnModel>
        <Columns>
            <ext:Column ID="Column4" Width="100" Sortable="false" MenuDisabled="true" runat="server"
                Text="From Date" Align="Left" DataIndex="FromDate">
            </ext:Column>
            <ext:Column ID="Column5" Sortable="false" MenuDisabled="true" runat="server" Text="Letter No"
                Align="Left" Width="70" DataIndex="LetterNumber" />
            <ext:Column ID="Column6" Width="90" Sortable="false" MenuDisabled="true" runat="server"
                Text="LetterDate" Align="Left" DataIndex="LetterDate">
            </ext:Column>
            <ext:Column ID="Column2" Width="120" Sortable="false" MenuDisabled="true" runat="server"
                Text="From Branch" Align="Left" DataIndex="FromBranch">
            </ext:Column>
            <ext:Column ID="Column8" Width="120" Sortable="false" MenuDisabled="true" runat="server"
                Text="From Dep" Align="Left" DataIndex="FromDepartment">
            </ext:Column>
            <ext:Column ID="Column3" Width="120" Sortable="false" MenuDisabled="true" runat="server"
                Text="To Branch" Align="Left" DataIndex="ToBranch">
            </ext:Column>
              <ext:Column ID="Column9" Width="120" Sortable="false" MenuDisabled="true" runat="server"
                Text="To Dep" Align="Left" DataIndex="ToDepartment">
            </ext:Column>
            <ext:Column ID="Column1" Width="110" Sortable="false" MenuDisabled="true" runat="server"
                Text="Departure Date" Align="Left" DataIndex="DepartureDate">
            </ext:Column>
            <ext:Column ID="Column7" Width="150" Sortable="false" MenuDisabled="true" runat="server"
                Text="Duration" Align="Left" DataIndex="TimeElapsed">
            </ext:Column>
        </Columns>
    </ColumnModel>
    <SelectionModel>
        <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
    </SelectionModel>
</ext:GridPanel>
