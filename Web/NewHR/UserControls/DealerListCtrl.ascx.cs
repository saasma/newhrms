﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Entity;
using BLL;
using System.IO;
using Utils;
using System.Text;
using BLL.BO;
using Utils.Calendar;
namespace Web.NewHR.UserControls
{
    public partial class DealerListCtrl : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
                Initialise();
        }

        private void Initialise()
        {
            BindBranchCombo();
            BindZoneCombo();
            BindDistrictCombo();

            if (!DocumentManager.CanUserAddEditDocument())
                ComColEdit.Hide();

            if (!DocumentManager.CanUserDeleteDocument())
                ComColDel.Hide();
        }

        private void BindBranchCombo()
        {
            cmbBranch.Store[0].DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            cmbBranch.Store[0].DataBind();
        }

        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            int totalRows = 0, pageSize = 0;

            DateTime? expiryDateFrom = null, expiryDateTo = null;
            Guid? partyID = null;


            pageSize = int.Parse(cmbPageSize.SelectedItem.Value);

            if (!string.IsNullOrEmpty(hdnPartyID.Text.Trim()))
                partyID = new Guid(hdnPartyID.Text.Trim());

            if (partyID == Guid.Empty)
                partyID = null;


            int thisMonth = 0, daysInMonth = 0, thisYear = 0;
            CustomDate cdToday = CustomDate.GetTodayDate(false);
            string monthStartDateString = "", monthEndDateString = "";
            CustomDate cdFromDate = null, cdToDate = null;

            thisMonth = cdToday.Month;
            thisYear = cdToday.Year;


            if (cmbExpiring.SelectedItem != null && cmbExpiring.SelectedItem.Value != null)
            {
                int value = int.Parse(cmbExpiring.SelectedItem.Value);
                if (value == 1)//today
                {
                    expiryDateFrom = DateTime.Today.Date;
                    expiryDateTo = DateTime.Today.Date;
                }
                else if (value == 2)//this week
                {
                    expiryDateFrom = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek).Date;
                    expiryDateTo = expiryDateFrom.Value.AddDays(7).AddSeconds(-1).Date;
                }
                else if (value == 3)//last week
                {
                    DateTime weekStartDate = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek).Date;
                    expiryDateFrom = weekStartDate.AddDays(-7);
                    expiryDateTo = weekStartDate.AddDays(-1);
                }
                else if (value == 4)//this month
                {
                    monthStartDateString = string.Format("{0}/{1}/{2}", thisYear, thisMonth, 1);

                    daysInMonth = DateHelper.GetTotalDaysInTheMonth(thisYear, thisMonth, false);
                    monthEndDateString = string.Format("{0}/{1}/{2}", thisYear, thisMonth, daysInMonth);

                    cdFromDate = CustomDate.GetCustomDateFromString(monthStartDateString, false);
                    cdToDate = CustomDate.GetCustomDateFromString(monthEndDateString, false);

                    expiryDateFrom = cdFromDate.EnglishDate.Date;
                    expiryDateTo = cdToDate.EnglishDate.Date;
                }
                else if (value == 5)//last month
                {
                    int lastYear = 0;
                    int lastMonth = thisMonth - 1;
                    if (lastMonth == 0)
                    {
                        lastMonth = 12;
                        lastYear = thisYear - 1;
                    }
                    else
                        lastYear = thisYear;

                    monthStartDateString = string.Format("{0}/{1}/{2}", lastYear, lastMonth, 1);

                    daysInMonth = DateHelper.GetTotalDaysInTheMonth(lastYear, lastMonth, false);
                    monthEndDateString = string.Format("{0}/{1}/{2}", lastYear, lastMonth, daysInMonth);

                    cdFromDate = CustomDate.GetCustomDateFromString(monthStartDateString, false);
                    cdToDate = CustomDate.GetCustomDateFromString(monthEndDateString, false);

                    expiryDateFrom = cdFromDate.EnglishDate.Date;
                    expiryDateTo = cdToDate.EnglishDate.Date;

                }
                else if (value == 6)//custom date
                {
                    if (!string.IsNullOrEmpty(calExpiringDateFrom.Text.Trim()))
                        expiryDateFrom = GetEngDate(calExpiringDateFrom.Text.Trim());

                    if (!string.IsNullOrEmpty(calExpiringDateTo.Text.Trim()))
                        expiryDateTo = GetEngDate(calExpiringDateTo.Text.Trim());
                }

            }

            int? ZoneId = null;
            int? DistrictId = null;

            if (!string.IsNullOrEmpty(cmbZone.SelectedItem.Value))
                ZoneId = int.Parse(cmbZone.SelectedItem.Value);

            if (!string.IsNullOrEmpty(cmbDistrict.SelectedItem.Value))
                DistrictId = int.Parse(cmbDistrict.SelectedItem.Value);

            string DealerName = "";
            if (!string.IsNullOrEmpty(cmbDealerName.SelectedItem.Text))
                DealerName = cmbDealerName.SelectedItem.Text;
            else
                DealerName = cmbDealerName.Text.Trim();

            int branchId = -1;

            if (cmbBranch.SelectedItem != null && cmbBranch.SelectedItem.Value != null)
                branchId = int.Parse(cmbBranch.SelectedItem.Value);

            List<GetDocDealerListResult> list = DocumentManager.GetGetDocDealerListResult(e.Start / pageSize, pageSize, DealerName, ZoneId, DistrictId,
                expiryDateFrom, expiryDateTo, branchId);

            if (list.Count > 0)
                totalRows = list[0].TotalRows.Value;

            storeDocumentList.DataSource = list;
            storeDocumentList.DataBind();
            e.Total = totalRows;

        }

        private void BindZoneCombo()
        {
            CommonManager _CommonManager = new CommonManager();
            cmbZone.Store[0].DataSource = _CommonManager.GetAllZones();
            cmbZone.Store[0].DataBind();
        }

        private void BindDistrictCombo()
        {
            cmbDistrict.Store[0].DataSource = BLL.BaseBiz.PayrollDataContext.DistrictLists.OrderBy(x => x.District);
            cmbDistrict.Store[0].DataBind();
        }

        protected void btnDownload_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(hdnFileID.Text))
            {
                NewMessage.ShowWarningMessage("Document not found.");
                return;
            }

            Guid fileId = new Guid(hdnFileID.Text);
            DocContactAttachment doc = DocumentManager.GetDocContactAttachmentsByID(fileId);

            string path = Context.Server.MapPath(doc.SavedFileName);
            string contentType = doc.FileType;
            string name = doc.name + "." + doc.FileFormat;

            byte[] bytes = File.ReadAllBytes(path);

            Context.Response.Clear();
            Context.Response.ClearHeaders();
            Context.Response.AppendHeader("Content-Disposition", "attachment; filename=\"" + name + "\"");
            Context.Response.ContentType = contentType;
            Context.Response.BinaryWrite(bytes);
            Context.Response.End();
        }

       

        protected void btnDelete_Click(object sender, DirectEventArgs e)
        {
            if (string.IsNullOrEmpty(hdnDocumentID.Text.Trim()))
                return;

            Guid documentID = new Guid(hdnDocumentID.Text.Trim());
            Status status = DocumentManager.DeleteDocDealerPainter(documentID);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Dealer is deleted.");
            
                X.Js.Call("searchList");
            }
            else
                NewMessage.ShowWarningMessage(status.ErrorMessage);
        }

        private void SetDivHeight()
        {
            bool maxHeight = false;

           
            if (cmbExpiring.SelectedItem != null && cmbExpiring.SelectedItem.Value != null && cmbExpiring.SelectedItem.Value == "6")
                maxHeight = true;

            if (maxHeight)
                X.Js.Call("SetMaxHeight");
            else
                X.Js.Call("SetMinHeight");
        }

     

        protected void cmbExpiring_Select(object sender, DirectEventArgs e)
        {
            calExpiringDateFrom.Hide();
            calExpiringDateTo.Hide();

            if (cmbExpiring.SelectedItem != null && cmbExpiring.SelectedItem.Value != null && cmbExpiring.SelectedItem.Value == "6")
            {
                calExpiringDateFrom.Show();
                calExpiringDateTo.Show();
            }

            SetDivHeight();
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            //int totalRows = 0;

            DateTime? expiryDateFrom = null, expiryDateTo = null;
            Guid? partyID = null;

            if (!string.IsNullOrEmpty(hdnPartyID.Text.Trim()))
                partyID = new Guid(hdnPartyID.Text.Trim());

            if (partyID == Guid.Empty)
                partyID = null;

            int thisMonth = 0, daysInMonth = 0, thisYear = 0;
            CustomDate cdToday = CustomDate.GetTodayDate(false);
            string monthStartDateString = "", monthEndDateString = "";
            CustomDate cdFromDate = null, cdToDate = null;

            thisMonth = cdToday.Month;
            thisYear = cdToday.Year;

            if (cmbExpiring.SelectedItem != null && cmbExpiring.SelectedItem.Value != null)
            {
                int value = int.Parse(cmbExpiring.SelectedItem.Value);
                if (value == 1)//today
                {
                    expiryDateFrom = DateTime.Today.Date;
                    expiryDateTo = DateTime.Today.Date;
                }
                else if (value == 2)//this week
                {
                    expiryDateFrom = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek).Date;
                    expiryDateTo = expiryDateFrom.Value.AddDays(7).AddSeconds(-1).Date;
                }
                else if (value == 3)//last week
                {
                    DateTime weekStartDate = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek).Date;
                    expiryDateFrom = weekStartDate.AddDays(-7);
                    expiryDateTo = weekStartDate.AddDays(-1);
                }
                else if (value == 4)//this month
                {
                    monthStartDateString = string.Format("{0}/{1}/{2}", thisYear, thisMonth, 1);

                    daysInMonth = DateHelper.GetTotalDaysInTheMonth(thisYear, thisMonth, false);
                    monthEndDateString = string.Format("{0}/{1}/{2}", thisYear, thisMonth, daysInMonth);

                    cdFromDate = CustomDate.GetCustomDateFromString(monthStartDateString, false);
                    cdToDate = CustomDate.GetCustomDateFromString(monthEndDateString, false);

                    expiryDateFrom = cdFromDate.EnglishDate.Date;
                    expiryDateTo = cdToDate.EnglishDate.Date;
                }
                else if (value == 5)//last month
                {
                    int lastYear = 0;
                    int lastMonth = thisMonth - 1;
                    if (lastMonth == 0)
                    {
                        lastMonth = 12;
                        lastYear = thisYear - 1;
                    }
                    else
                        lastYear = thisYear;

                    monthStartDateString = string.Format("{0}/{1}/{2}", lastYear, lastMonth, 1);

                    daysInMonth = DateHelper.GetTotalDaysInTheMonth(lastYear, lastMonth, false);
                    monthEndDateString = string.Format("{0}/{1}/{2}", lastYear, lastMonth, daysInMonth);

                    cdFromDate = CustomDate.GetCustomDateFromString(monthStartDateString, false);
                    cdToDate = CustomDate.GetCustomDateFromString(monthEndDateString, false);

                    expiryDateFrom = cdFromDate.EnglishDate.Date;
                    expiryDateTo = cdToDate.EnglishDate.Date;

                }
                else if (value == 6)//custom date
                {
                    if (!string.IsNullOrEmpty(calExpiringDateFrom.Text.Trim()))
                        expiryDateFrom = GetEngDate(calExpiringDateFrom.Text.Trim());

                    if (!string.IsNullOrEmpty(calExpiringDateTo.Text.Trim()))
                        expiryDateTo = GetEngDate(calExpiringDateTo.Text.Trim());
                }
            }

            int? ZoneId = null;
            int? DistrictId = null;

            if (!string.IsNullOrEmpty(cmbZone.SelectedItem.Value))
                ZoneId = int.Parse(cmbZone.SelectedItem.Value);

            if (!string.IsNullOrEmpty(cmbDistrict.SelectedItem.Value))
                DistrictId = int.Parse(cmbDistrict.SelectedItem.Value);

            string DealerName = "";
            if (!string.IsNullOrEmpty(cmbDealerName.SelectedItem.Text))
                DealerName = cmbDealerName.SelectedItem.Text;
            else
                DealerName = cmbDealerName.Text.Trim();

            int branchId = -1;

            if (cmbBranch.SelectedItem != null && cmbBranch.SelectedItem.Value != null)
                branchId = int.Parse(cmbBranch.SelectedItem.Value);

            List<GetDocDealerListResult> list = DocumentManager.GetGetDocDealerListResult(0, 999999, DealerName, ZoneId, DistrictId,
                expiryDateFrom, expiryDateTo, branchId);

            Bll.ExcelHelper.ExportToExcel("Dealer List", list,
              new List<string> { "ContactId", "TotalRows", "RowNumber", "FileID", "DealerId" },
          new List<String>() { },
          new Dictionary<string, string>() { { "Name", "Dealer Name" }, { "ContactPerson", "Contact Person" }, 
          { "OfficePhoneNo", "Phone No"}, {"MobileNo", "Mobile No"}, 
          {"ExpiryDate", "Agreement Expiry"}, {"DaysTillExpiry", "Expiry"}, {"UserFileName", "Document Name"}},
          new List<string>() { }
          , new List<string> { }
          , new List<string> { }
           , new Dictionary<string, string>() {
              {"Dealer List",""}
            }
           , new List<string> { "Name", "Zone", "District", "ContactPerson", "OfficePhoneNo", "MobileNo", "ExpiryDate", "DaysTillExpiry", "UserFileName" });

        }

    }
}