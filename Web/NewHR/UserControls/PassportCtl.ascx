﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PassportCtl.ascx.cs"
    Inherits="Web.NewHR.UserControls.PassportCtl" %>
<ext:Hidden runat="server" ID="hdnPassportID">
</ext:Hidden>
<ext:Hidden runat="server" ID="hdnIsEmployee" />

<div style="width:823px; margin-left:-19px;">
    <div>
        <!-- panel-btns -->
        <h4 class="panel-title sectionHeading">
            <i></i>Passport
        </h4>
    </div>
    <!-- panel-heading -->
    <div class="panel-body">
        <table class="fieldTable firsttdskip">
            <tr>
                <td>
                    <ext:GridPanel ID="GridPassport" runat="server" Width="600" Cls="itemgrid">
                        <Store>
                            <ext:Store ID="StorePassport" runat="server">
                                <Model>
                                    <ext:Model ID="Model1" runat="server" IDProperty="PassportId">
                                        <Fields>
                                            <ext:ModelField Name="PassportNo" Type="String" />
                                            <ext:ModelField Name="IssuingDate" Type="string" />
                                            <ext:ModelField Name="ValidUpto" Type="string" />
                                            <ext:ModelField Name="Status" Type="Int" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:Column ID="Column1" runat="server" Text="Passport Number" DataIndex="PassportNo"
                                    Width="100" Flex="1" />
                                <ext:Column ID="Column2" runat="server" Text="Issuing Date" DataIndex="IssuingDate"
                                    Width="100">
                                </ext:Column>
                                <ext:Column ID="Column3" runat="server" Text="Valid Upto" DataIndex="ValidUpto" Width="100">
                                </ext:Column>

                                <ext:CommandColumn ID="CommandColumn1" runat="server" Width="40">
                                    <Commands>
                                        <ext:CommandSeparator />
                                        <ext:GridCommand Icon="PageWhitePut" CommandName="DownLoad">
                                            <ToolTip Text="DownLoad" />
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridPassport_Command">
                                            <ExtraParams>
                                                <ext:Parameter Name="ID" Value="record.data.PassportId" Mode="Raw">
                                                </ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw">
                                                </ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>

                                <ext:CommandColumn ID="CommandColumn3" runat="server" Width="40">
                                    <Commands>
                                        <ext:CommandSeparator />
                                        <ext:GridCommand Cls="editGridButton" ToolTip-Text="Edit" Text="<i class='fa fa-pencil'></i>"
                                            CommandName="Edit" />
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridPassport_Command">
                                            <ExtraParams>
                                                <ext:Parameter Name="ID" Value="record.data.PassportId" Mode="Raw">
                                                </ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw">
                                                </ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                    <PrepareToolbar Fn="preparePassport" />
                                </ext:CommandColumn>

                                <ext:CommandColumn ID="CommandColumn2" runat="server" Width="40">
                                    <Commands>
                                        <ext:CommandSeparator />
                                        <ext:GridCommand Cls="editGridButton" ToolTip-Text="Delete" Text="<i class='fa fa-trash-o'></i>"
                                            CommandName="Delete" />
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridPassport_Command">
                                            <ExtraParams>
                                                <ext:Parameter Name="ID" Value="record.data.PassportId" Mode="Raw">
                                                </ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw">
                                                </ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the record?" />
                                        </Command>
                                    </DirectEvents>
                                    <PrepareToolbar Fn="preparePassport" />
                                </ext:CommandColumn>

                            </Columns>
                        </ColumnModel>
                        <SelectionModel>
                            <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                        </SelectionModel>
                    </ext:GridPanel>
                </td>
            </tr>
        </table>
        <div class="buttonBlockSection">
            <ext:Button runat="server" Cls="btn btn-primary btn-sect" Width="120" StyleSpec="margin-top:10px"
                Height="30" ID="LinkButton1" Text="<i></i>Add New Line">
                <DirectEvents>
                    <Click OnEvent="btnAddNewLine_Click">
                        <EventMask ShowMask="true" />
                    </Click>
                </DirectEvents>
            </ext:Button>
        </div>

    </div>
</div>


<ext:Window ID="AEPassportWindow" runat="server" Title="Passport Details" Icon="Application"
    Height="350" Width="500"  BodyPadding="5"
    Hidden="true" Modal="true">
    <Content>
        <ext:Label ID="lblMsg" runat="server" />
        <table class="fieldTable">
            <tr>
                <td>
                    <ext:TextField ID="txtPassportNumber" Width="180px" runat="server" FieldLabel="Passport Number"
                        LabelAlign="top" LabelSeparator="" />
                    <asp:RequiredFieldValidator Display="None" ID="valtxtPassportNumber" runat="server"
                        ValidationGroup="PassportSaveUpdate" ControlToValidate="txtPassportNumber" ErrorMessage="Please enter Passport Number." />
                </td>
            </tr>
            <tr>
                <td>
                    <pr:CalendarExtControl FieldLabel="Issue Date" Width="180px" ID="calIssueDate" runat="server"
                        LabelSeparator="" LabelAlign="Top" />
                    <asp:RequiredFieldValidator Display="None" ID="valcalIssueDate" runat="server" ValidationGroup="PassportSaveUpdate"
                        ControlToValidate="calIssueDate" ErrorMessage="Please enter passport Issue Date." />
                </td>
                <td style="padding-top: 5px">
                    <pr:CalendarExtControl FieldLabel="Valid Upto" Width="180px" ID="calValidUpto" runat="server"
                        LabelSeparator="" LabelAlign="Top" />
                    <asp:RequiredFieldValidator Display="None" ID="valcalValidUpto" runat="server" ValidationGroup="PassportSaveUpdate"
                        ControlToValidate="calValidUpto" ErrorMessage="Please check Valid Upto." />
                </td>
            </tr>
            <tr>
                <td>
                    <ext:FileUploadField ID="FilePassportDocumentUpload" runat="server" Width="200" Icon="Attach"
                        FieldLabel="&nbsp;" LabelAlign="Top" LabelSeparator="" />
                </td>
                <td valign="bottom">
                    <table>
                        <tr>
                            <td>
                                <ext:Label ID="lblUploadedFile" runat="server" Hidden="true">
                                </ext:Label>
                            </td>
                            <td>
                                <ext:LinkButton Icon="Delete" runat="server" ID="lnkDeleteFile" OnDirectClick="lnkDeleteFile_Click"
                                    Text="Delete file" Hidden="true">
                                    <DirectEvents>
                                        <Click OnEvent="lnkDeleteFile_Click">
                                         <Confirmation ConfirmRequest="true" Message="Do you want to delete the file?" />
                                            <EventMask ShowMask="true" />
                                        </Click>
                                    </DirectEvents>
                                </ext:LinkButton>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td valign="bottom" colspan="2">
                    <div class="popupButtonDiv">
                        <ext:Button runat="server" Cls="btn btn-primary" ID="btnSave"
                            Text="<i></i>Save">
                            <DirectEvents>
                                <Click OnEvent="btnSave_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                            <Listeners>
                                <Click Handler="valGroup = 'PassportSaveUpdate'; return CheckValidation();">
                                </Click>
                            </Listeners>
                        </ext:Button>
                        <div class="btnFlatOr">
                            or</div>
                        <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="LinkButton2" Cls="btnFlatLeftGap"
                            Text="<i></i>Cancel">
                            <Listeners>
                                <Click Handler="#{AEPassportWindow}.hide();">
                                </Click>
                            </Listeners>
                        </ext:LinkButton>
                    </div>
                </td>
            </tr>
        </table>
    </Content>
</ext:Window>


<script type="text/javascript">

    var preparePassport = function (grid, toolbar, rowIndex, record) {
        var delBtn = toolbar.items.get(1);
        var IsEmployee = <%= hdnIsEmployee.ClientID %>.getValue();

        if (record.data.Status == 1 && IsEmployee == "1" ) {
            delBtn.setVisible(false);
        }

    } 

</script>
