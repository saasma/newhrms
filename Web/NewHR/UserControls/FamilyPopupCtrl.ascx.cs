﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using System.IO;
using Utils;
using Web.Helper;

namespace Web.NewHR.UserControls
{
    public partial class FamilyPopupCtrl : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        public void Initialise()
        {
            LoadLevels();
            valRegPhone.ValidationExpression = Config.PhoneRegExp;
            cmbRelation.Store[0].DataSource = CommonManager.GetRelationList();
            cmbRelation.Store[0].DataBind();

            cmbOccupation.Store[0].DataSource = ListManager.GetAllOccuations();
            cmbOccupation.Store[0].DataBind();

            cmbTitle.Store[0].DataSource = new Title().GetMembers();
            cmbTitle.Store[0].DataBind();

            cmbBoodGroup.Store[0].DataSource = CommonManager.GetAllBloodGroup();
            cmbBoodGroup.Store[0].DataBind();

            cmbNationality.Store[0].DataSource = CommonManager.GetCitizenshipNationaliyList();
            cmbNationality.Store[0].DataBind();

            cmbDocumentType.Store[0].DataSource = CommonManager.GetDocumentDocumentTypeList();
            cmbDocumentType.Store[0].DataBind();

        }

        private void LoadLevels()
        {
            int employeeId = 0;

            if (Request.QueryString["id"] == null)
            {
                employeeId = SessionManager.CurrentLoggedInEmployeeId;
            }
            else
            {
                employeeId = int.Parse(Request.QueryString["id"]);
            }

        }


        public void txtDOB_Blur(object sender, DirectEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtDOB.Text.Trim()))
            {
                int years, months, days, hours, minutes, seconds, milliseconds;
                NewHelper.GetElapsedTime(GetEngDate(txtDOB.Text.Trim()),
                    BLL.BaseBiz.GetCurrentDateAndTime(), out years, out months, out days, out hours, out minutes, out seconds, out milliseconds);

                txtAgeOnSPDate.Text = string.Format(" {0} years {1} months {2} days",
                    years, months, days);
            }
        }

        public void ClearLevelFields()
        {
            hiddenValue.Text = "";
            txtName.Text = "";
            cmbRelation.ClearValue();
            txtRemarks.Text = "";
            txtDOB.Text = "";
            chkHasDependent.Checked = false;
            chkForIsurance.Checked = false;
            txtAgeOnSPDate.Text = "";
            cmbTitle.ClearValue();
            cmbGender.ClearValue();
            cmbOccupation.ClearValue();
            cmbBoodGroup.ClearValue();
            cmbNationality.ClearValue();
            cmbDocumentType.ClearValue();
            txtDocumentIssuePlace.Text = "";
            calDocumentIssueDate.Text = "";
            txtContactNumber.Text = "";
            txtRemarks.Text = "";


        }

        public void EditFamily(int familyId)
        {
            ClearLevelFields();

            HFamily entity = NewPayrollManager.GetFamilyMemberById(familyId);

            hiddenValue.Text = familyId.ToString();

            txtName.Text = entity.Name;
            if (entity.RelationID != null)
                cmbRelation.SetValue(entity.RelationID.ToString());
            else
                cmbRelation.ClearValue();

            txtRemarks.Text = entity.Remarks;
            txtDOB.Text = entity.DateOfBirth;
            txtDOB_Blur(null, null);
            if (entity.HasDependent != null)
                chkHasDependent.Checked = entity.HasDependent.Value;
            else
                chkHasDependent.Checked = false;
            //txtSPDate.Text = entity.SpecifiedDate;
            //txtAgeOnSPDate.Text = entity.AgeOnSpecifiedSPDate;

            if (entity.ForInsurance != null)
                chkForIsurance.Checked = entity.ForInsurance.Value;
            else
                chkForIsurance.Checked = false;

            cmbOccupation.Store[0].ClearFilter();
            if (entity.OccupationId != null)
                cmbOccupation.SetValue(entity.OccupationId.ToString());
            else
                cmbOccupation.ClearValue();

            if (entity.ContactNumber != null)
                txtContactNumber.Text = entity.ContactNumber;

            if (entity.Title != null)
                cmbTitle.SetValue(entity.Title.ToString());

            if (entity.BloodGroup != null)
                cmbBoodGroup.SetValue(entity.BloodGroup.ToString());
            //cmbBoodGroup.SelectedItem.Value = entity.BloodGroup.ToString();


            if (entity.Nationality != null)
                cmbNationality.SetValue(entity.Nationality.ToString());

            if (entity.DocumentIdType != null)
                cmbDocumentType.SetValue(entity.DocumentIdType.ToString());
            if (entity.Gender != null)
                cmbGender.SetValue(entity.Gender.ToString());

            calDocumentIssueDate.Text = entity.DocumentIssueDate;
            txtDocumentIssuePlace.Text = entity.DocumentIssuePlace;



        }


        protected void btnLevelSaveUpdate_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("InsertUpdateLevel");
            if (Page.IsValid)
            {
                HFamily hFamily = new HFamily();
                int EmpID;
                int FamilyID;
                bool isInsert = true;

                hFamily.Name = txtName.Text;
                hFamily.Relation = cmbRelation.SelectedItem.Text;
                hFamily.RelationID = int.Parse(cmbRelation.SelectedItem.Value);
                hFamily.Remarks = txtRemarks.Text;
                hFamily.DateOfBirth = txtDOB.Text;
                if (!string.IsNullOrEmpty(hFamily.DateOfBirth))
                    hFamily.DateOfBirthEng = GetEngDate(hFamily.DateOfBirth);
                hFamily.HasDependent = false;
                if (chkHasDependent.Checked)
                    hFamily.HasDependent = true;
                //hFamily.SpecifiedDate = txtSPDate.Text;
                hFamily.ForInsurance = false;
                if (chkForIsurance.Checked)
                    hFamily.ForInsurance = true;
                hFamily.AgeOnSpecifiedSPDate = txtAgeOnSPDate.Text;

                if (cmbOccupation.SelectedItem != null && cmbOccupation.SelectedItem.Value != null)
                    hFamily.OccupationId = int.Parse(cmbOccupation.SelectedItem.Value);

                if (!string.IsNullOrEmpty(txtContactNumber.Text.Trim()))
                    hFamily.ContactNumber = txtContactNumber.Text.Trim();

                //Added By Namaraj  Date: 2017/03/21
                if (cmbTitle.SelectedItem != null && cmbTitle.SelectedItem.Value != null)
                    hFamily.Title = cmbTitle.SelectedItem.Value;

                if (cmbGender.SelectedItem != null && cmbGender.SelectedItem.Value != null)
                    hFamily.Gender = int.Parse(cmbGender.SelectedItem.Value);

                if (cmbBoodGroup.SelectedItem != null && cmbBoodGroup.SelectedItem.Value != null)
                    hFamily.BloodGroup = int.Parse(cmbBoodGroup.SelectedItem.Value);

                if (cmbNationality.SelectedItem != null && cmbNationality.SelectedItem.Text != null)
                    hFamily.Nationality = cmbNationality.SelectedItem.Text;

                if (cmbDocumentType.SelectedItem != null && cmbDocumentType.SelectedItem.Value != null)
                    hFamily.DocumentIdType = int.Parse(cmbDocumentType.SelectedItem.Value);

                if (cmbDocumentType.SelectedItem != null && cmbDocumentType.SelectedItem.Value != null)
                    hFamily.DocumentIdType = int.Parse(cmbDocumentType.SelectedItem.Value);

                hFamily.DocumentIssueDate = calDocumentIssueDate.Text;
                if (!string.IsNullOrEmpty(hFamily.DocumentIssueDate))
                    hFamily.DocumentIssueDateEng = GetEngDate(hFamily.DocumentIssueDate);

                hFamily.DocumentIssuePlace = txtDocumentIssuePlace.Text;
                //End


                if (Request.QueryString["id"] == null)
                    EmpID = SessionManager.CurrentLoggedInEmployeeId;
                else
                    EmpID = int.Parse(Request.QueryString["id"]);


                hFamily.EmployeeId = EmpID;

                if (!string.IsNullOrEmpty(hiddenValue.Text))
                {
                    hFamily.FamilyId = int.Parse(hiddenValue.Text.Trim());
                    isInsert = false;
                }


                Status status = NewHRManager.InsertUpdateFamily(hFamily, isInsert);
                if (status.IsSuccess)
                {
                    Window win = (Window)this.Parent.FindControl("WFamily");
                    win.Close();
                    if (isInsert)
                        NewMessage.ShowNormalMessage("Record Saved Successfully.", "reloadFamilyGrid()");
                    else
                        NewMessage.ShowNormalMessage("Record Updated Successfully.", "reloadFamilyGrid()");

                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }

            }
        }

        public void HideButtons()
        {
            btnLevelSaveUpdate.Hidden = true;
            LinkButton1.Hidden = true;
        }

    }
}