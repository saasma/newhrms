﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ExtraActivitiesCtrlView.ascx.cs"
    Inherits="Web.NewHR.UserControls.ExtraActivitiesCtrlView" %>

<ext:GridPanel ID="gridExtraActivity" runat="server" Width="920" Cls="itemgrid">
    <Store>
        <ext:Store ID="storeExtraActivity" runat="server">
            <Model>
                <ext:Model ID="Model1" runat="server" IDProperty="CurricularId">
                    <Fields>
                        <ext:ModelField Name="CurricularId" Type="String" />
                        <ext:ModelField Name="AcitivityName" Type="string" />
                        <ext:ModelField Name="Proficiency" Type="string" />
                        <ext:ModelField Name="Award" Type="string" />
                        <ext:ModelField Name="Year" Type="string" />
                        <ext:ModelField Name="IsEditable" Type="Int" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
    </Store>
    <ColumnModel>
        <Columns>
            <ext:Column ID="Column1" Width="200" runat="server" Text="Activity Name" DataIndex="AcitivityName"
                Wrap="true" />
            <ext:Column ID="Column2" Width="100" runat="server" Text="Proficiency" DataIndex="Proficiency"
                Wrap="true">
            </ext:Column>
            <ext:Column ID="Column6" Width="200" runat="server" Text="Award" DataIndex="Award"
                Wrap="true">
            </ext:Column>
            <ext:Column ID="Column81" Width="100" runat="server" Text="Year" DataIndex="Year"
                Wrap="true">
            </ext:Column>
            <ext:Column ID="Column3" Width="420" runat="server" Text="">
            </ext:Column>
        </Columns>
    </ColumnModel>
    <SelectionModel>
        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
    </SelectionModel>
</ext:GridPanel>
