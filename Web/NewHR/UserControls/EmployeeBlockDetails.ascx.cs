﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using System.IO;
using Utils;
using BLL.Manager;
using BLL;
using BLL.Base;

namespace Web.NewHR.UserControls
{
    public partial class EmployeeBlockDetails : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void SetEmployeeDetailsWithPhoto(EEmployee emp)
        {
            // Photo
            bool hasPhoto = false;
            if (emp.HHumanResources.Count > 0)
            {
                if (!string.IsNullOrEmpty(emp.HHumanResources[0].UrlPhotoThumbnail))
                {
                    image.ImageUrl = Page.ResolveUrl(Config.UploadLocation + "/" + emp.HHumanResources[0].UrlPhotoThumbnail);
                    if (File.Exists(Server.MapPath(image.ImageUrl)))
                        hasPhoto = true;
                }
            }
            if (!hasPhoto)
            {
                image.ImageUrl = Page.ResolveUrl(Web.Helper.WebHelper.GetEmployeeNoImage(emp.EmployeeId));
            }

            lblINo.Text = "I No : " + emp.EHumanResources[0].IdCardNo;
            lblName.Text = emp.Title + " " + emp.Name;
            lblBranch.Text = emp.Branch.Name;
            lblDepartment.Text = emp.Department.Name;

            int years, months, days, hours;
            int minutes, seconds, milliseconds;
            ServicePeroid firstStatus = EmployeeManager.GetServicePeroidDate(emp.EmployeeId);
            NewHelper.GetElapsedTime(firstStatus.FromDateEng, firstStatus.ToDateEng.Value, out years, out months, out days, out hours, out minutes, out seconds, out milliseconds);

            lblSince.Text = "Since " +
                (IsEnglish ? firstStatus.FromDateEng.ToShortDateString() : firstStatus.FromDate + " (" + firstStatus.FromDateEng.ToShortDateString() + ")");
            lblTime.Text = string.Format("For {0} years {1} months {2} days",
                years, months, days);
        }
    }
}