﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ChildrenAllowanceCtl.ascx.cs"
    Inherits="Web.NewHR.UserControls.ChildrenAllowanceCtl" %>
<%@ Register Src="~/newhr/UserControls/EmployeeWizard.ascx" TagName="EmployeeWizard"
    TagPrefix="uc2" %>
<%@ Register Src="~/NewHR/UserControls/EmployeeDetailsCtl.ascx" TagName="EmployeeDetailsCtl"
    TagPrefix="uc1" %>
<ext:Hidden runat="server" ID="hdn">
</ext:Hidden>
<ext:Hidden runat="server" ID="hdnEmployee">
</ext:Hidden>
<ext:Store runat="server" ID="storeSearch" AutoLoad="false">
    <Proxy>
        <ext:AjaxProxy Url="../../Handler/EmpSearch.ashx">
            <ActionMethods Read="GET" />
            <Reader>
                <ext:JsonReader Root="plants" TotalProperty="total" />
            </Reader>
        </ext:AjaxProxy>
    </Proxy>
    <Model>
        <ext:Model ID="Model2" IDProperty="EmployeeId" Name="ItemLineModel" runat="server">
            <Fields>
                <ext:ModelField Name="Name" Type="String" />
                <ext:ModelField Name="EmployeeId" Type="String" />
            </Fields>
        </ext:Model>
    </Model>
</ext:Store>
<div class="contentpanel" style="padding-top: 0px">
    <ext:Label ID="lblMsg" runat="server" />
    <table class="fieldTable firsttdskip">
        <tr>
            <td colspan="4">
                <ext:ComboBox ID="cmbSearch" FieldLabel="Search Employee *" LabelWidth="70" LabelAlign="Top"
                    runat="server" DisplayField="Name" ValueField="EmployeeId" StoreID="storeSearch"
                    LabelSeparator="" TypeAhead="false" Width="180" PageSize="9999" HideBaseTrigger="true"
                    MinChars="1" TriggerAction="All" ForceSelection="true">
                    <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                        <ItemTpl ID="ItemTpl1" runat="server">
                            <Html>
                                <tpl>
                                            <div class="search-item">
                                                            <span>{Name}</span>  
                                             </div>
					                </tpl>
                            </Html>
                        </ItemTpl>
                    </ListConfig>
                    <Triggers>
                        <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                    </Triggers>
                    <Listeners>
                        <Select Handler="this.getTrigger(0).show();" />
                        <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                        <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue();
                                           this.getTrigger(0).hide();
                                       }" />
                    </Listeners>
                    <DirectEvents>
                        <Select OnEvent="Change_EmployeeSearch">
                            <EventMask ShowMask="true" />
                        </Select>
                    </DirectEvents>
                </ext:ComboBox>
                <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator4" runat="server"
                    ValidationGroup="SaveUpdateChildrenAllowance" ControlToValidate="cmbSearch" ErrorMessage="Employee is required." />
            </td>
        </tr>
        <tr>
            <td style="padding-top: 5px">
                <ext:ComboBox ID="cmbChildren" runat="server" ValueField="FamilyId" DisplayField="Name"
                    FieldLabel="Children *" Width="180" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                    QueryMode="Local">
                    <Store>
                        <ext:Store ID="storeChildren" runat="server">
                            <Model>
                                <ext:Model ID="Model4" runat="server">
                                    <Fields>
                                        <ext:ModelField Name="FamilyId" Type="String" />
                                        <ext:ModelField Name="Name" Type="String" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                    </Store>
                    <DirectEvents>
                        <Select OnEvent="cmbChildren_Change">
                            <EventMask ShowMask="true" />
                        </Select>
                    </DirectEvents>
                </ext:ComboBox>
                <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator2" runat="server"
                    ValidationGroup="SaveUpdateChildrenAllowance" ControlToValidate="cmbChildren"
                    ErrorMessage="Children is required." />
            </td>
            <td colspan="3">
                <ext:DateField FieldLabel="Date Of Birth *" Width="180px" ID="calDateOfBrith" runat="server"
                    Format="yyyy/MM/dd" LabelSeparator="" LabelAlign="Top" />
                <asp:RequiredFieldValidator Display="None" ID="valcalStartDate" runat="server" ValidationGroup="SaveUpdateChildrenAllowance"
                    ControlToValidate="calDateOfBrith" ErrorMessage="Date of birth is required." />
            </td>
        </tr>
        <tr>
            <td>
                <ext:ComboBox ID="cmbAllowance" runat="server" ValueField="IncomeId" DisplayField="Title"
                    FieldLabel="Allowance *" Width="180" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                    QueryMode="Local">
                    <Store>
                        <ext:Store ID="storeAllowance" runat="server">
                            <Model>
                                <ext:Model ID="modelall" runat="server">
                                    <Fields>
                                        <ext:ModelField Name="IncomeId" Type="String" />
                                        <ext:ModelField Name="Title" Type="String" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                    </Store>
                </ext:ComboBox>
            </td>
            <td colspan="3">
                <ext:TextField ID="txtAmountPerMonth" runat="server" FieldLabel="Amount Per Month *"
                    LabelAlign="top" Width="180px" LabelSeparator="" MaskRe="[0-9]|\." />
                <asp:RequiredFieldValidator Display="None" ID="valtxtDuration" runat="server" ValidationGroup="SaveUpdateChildrenAllowance"
                    ControlToValidate="txtAmountPerMonth" ErrorMessage="Amount Per Month  is required." />
            </td>
            <asp:RequiredFieldValidator Display="None" ID="valcmbDurationType" runat="server"
                ValidationGroup="SaveUpdateChildrenAllowance" ControlToValidate="cmbAllowance"
                ErrorMessage="Duration Type is required." />
        </tr>
        <tr>
            <td>
                <ext:DateField FieldLabel="Allowance Start Date *" Width="180px" ID="CalAllownceStartDate"
                    runat="server" LabelSeparator="" LabelAlign="Top" >

                     <Plugins>
                                    <ext:GenericPlugin ID="HighlightDates1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                                </Plugins>
                      <DirectEvents>
                          <Select OnEvent="CalAllownceStartDate_Change">
                              <EventMask ShowMask="true" />
                          </Select>
                      </DirectEvents>
                </ext:DateField>
                <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator1" runat="server"
                    ValidationGroup="SaveUpdateChildrenAllowance" ControlToValidate="CalAllownceStartDate"
                    ErrorMessage="Allowance Start Date is required." />
            </td>
            <td>
                <ext:TextField ID="txtAddYearMonth" runat="server" FieldLabel="for" LabelAlign="top"
                    Width="85" LabelSeparator="" MaskRe="[0-9]|\.">
                    <DirectEvents>
                        <Change OnEvent="AddDuration_Change">
                            <EventMask ShowMask="true" />
                        </Change>
                    </DirectEvents>
                </ext:TextField>
            </td>
            <td>
                <ext:ComboBox ID="cmbYearMonth" runat="server" ValueField="Value" DisplayField="Text"
                    Width="80" FieldLabel="Period" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                    QueryMode="Local">
                    <Items>
                        <ext:ListItem Text="Year" Value="1" />
                        <ext:ListItem Text="Month" Value="2" />
                    </Items>
                    <DirectEvents>
                        <Select OnEvent="AddDuration_Change">
                            <EventMask ShowMask="true" />
                        </Select>
                    </DirectEvents>
                </ext:ComboBox>
            </td>
            <td>
                <ext:DateField FieldLabel="Allowance End Date *" Width="140px" ID="calAllowanceEndDate"
                    runat="server" LabelSeparator="" LabelAlign="Top">

                     <Plugins>
                                    <ext:GenericPlugin ID="GenericPlugin1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                                </Plugins>
                </ext:DateField>
                <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator3" runat="server"
                    ValidationGroup="SaveUpdateChildrenAllowance" ControlToValidate="calAllowanceEndDate"
                    ErrorMessage="Allowance End Date is required." />
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <ext:TextArea runat="server" ID="txtNote" Width="510" LabelAlign="Top" FieldLabel="Note"
                    LabelSeparator="">
                </ext:TextArea>
            </td>
        </tr>
        <tr>
            <td valign="bottom" colspan="2">
                <div class="popupButtonDiv">
                    <ext:Button runat="server" StyleSpec="padding:0px;" ID="btnSave" Cls="btn btn-primary"
                        Height="30" Text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;">
                        <DirectEvents>
                            <Click OnEvent="btnSave_Click">
                                <EventMask ShowMask="true" />
                            </Click>
                        </DirectEvents>
                        <Listeners>
                            <Click Handler="valGroup = 'SaveUpdateChildrenAllowance'; if(CheckValidation()) return this.disable(); else return false;">
                            </Click>
                        </Listeners>
                    </ext:Button>
                    <div class="btnFlatOr">
                        or</div>
                    <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="LinkButton2" Cls="btnFlatLeftGap"
                        Text="<i></i>Cancel">
                        <Listeners>
                            <Click Handler="refreshWindow();">
                            </Click>
                        </Listeners>
                    </ext:LinkButton>
                </div>
            </td>
        </tr>
    </table>
</div>
