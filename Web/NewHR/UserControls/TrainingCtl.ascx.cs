﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;

using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;

namespace Web.NewHR.UserControls
{
    public partial class TrainingCtl : BaseUserControl
    {
        private bool _isDisplayMode = false;
        public bool IsDisplayMode
        {
            set
            {
                if (value == true)
                {
                    _isDisplayMode = value;
                    btnAddNewLine.Visible = false;
                    AETrainingWindow.Visible = false;
                    CommandColumn1.Visible = false;
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();

            }
        }

        protected void Initialise()
        {
            int EmployeeID = GetEmployeeID();
            this.LoadTrainingGrid(EmployeeID);
            CommonManager _CommonManager = new CommonManager();
            cmbCountry.Store[0].DataSource = _CommonManager.GetAllCountries();
            cmbCountry.Store[0].DataBind();

            cmbTrainingType.Store[0].DataSource = CommonManager.GetTrainingTypeList();
            cmbTrainingType.Store[0].DataBind();
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {

            this.SaveFormData();

        }

        private void SaveFormData()
        {

            Status myStatus = new Status();
            HTraining _HTraining = new HTraining();
            _HTraining.EmployeeId = this.GetEmployeeID();
            bool isSave = bool.Parse(this.hdnTrainingID.Text == "" ? "true" : "false");
            if (!isSave)
                _HTraining.TrainingId = int.Parse(this.hdnTrainingID.Text);
            if (!string.IsNullOrEmpty(cmbTrainingType.SelectedItem.Text))
            {
                _HTraining.TrainingTypeID = int.Parse(cmbTrainingType.SelectedItem.Value);
                _HTraining.TrainingTypeName = cmbTrainingType.SelectedItem.Text;
            }
            _HTraining.TrainingName = txtTrainingName.Text.Trim();
            _HTraining.InstitutionName = txtInstitute.Text.Trim();
            _HTraining.ResourcePerson = txtResourcePerson.Text.Trim();
            if (!string.IsNullOrEmpty(cmbCountry.SelectedItem.Text))
            {
                _HTraining.Country = cmbCountry.SelectedItem.Text;
            }


            if (!string.IsNullOrEmpty(txtDuration.Text.Trim()))
                _HTraining.Duration = int.Parse(txtDuration.Text.Trim());

            if (!string.IsNullOrEmpty(cmbDurationType.SelectedItem.Text))
            {
                _HTraining.DurationTypeID = int.Parse(cmbDurationType.SelectedItem.Value);
                _HTraining.DurationTypeName = cmbDurationType.SelectedItem.Text;
            }

            if(!string.IsNullOrEmpty(calStartDate.Text.Trim()))
            {
            _HTraining.TrainingFrom = calStartDate.Text.Trim();
            _HTraining.TrainingFromEngDate = BLL.BaseBiz.GetEngDate(_HTraining.TrainingFrom, IsEnglish);
            }
            if (!string.IsNullOrEmpty(calEndDate.Text.Trim()))
            {
                _HTraining.TrainingTo = calEndDate.Text.Trim();
                _HTraining.TrainingToEngDate = BLL.BaseBiz.GetEngDate(_HTraining.TrainingTo, IsEnglish);
            }
            if (!string.IsNullOrEmpty(txtCostOfTraining.Text.Trim()))
                _HTraining.CostOfTraining = decimal.Parse(txtCostOfTraining.Text.Trim());

            if (!string.IsNullOrEmpty(txtTotalCost.Text.Trim()))
                _HTraining.TotalCost = decimal.Parse(txtTotalCost.Text.Trim());

            if (!string.IsNullOrEmpty(cmbBond.SelectedItem.Text))
            {
                _HTraining.Bond = bool.Parse(cmbBond.SelectedItem.Value);
            }
            if (!string.IsNullOrEmpty(cmbBondType.SelectedItem.Text))
            {
                _HTraining.BondTypeID = int.Parse(cmbBondType.SelectedItem.Value);
                _HTraining.BondTypeName = cmbBondType.SelectedItem.Text;
                if (!string.IsNullOrEmpty(txtBondPeriod.Text.Trim()))
                    _HTraining.BondPeriod = int.Parse(txtBondPeriod.Text.Trim());
            }
           
            if (!string.IsNullOrEmpty(txtNote.Text.Trim()))
                _HTraining.Note = txtNote.Text.Trim();


            //file upload section
            string UserFileName = this.FileTrainingDocumentUpload.FileName;
            string ServerFileName = Guid.NewGuid().ToString() + Path.GetExtension(UserFileName);
            string relativePath = @"../uploads/" + ServerFileName;
            if (this.UploadFileTraining(relativePath))
            {
                double fileSize = FileTrainingDocumentUpload.PostedFile.ContentLength;

                _HTraining.FileFormat = Path.GetExtension(this.FileTrainingDocumentUpload.FileName).Replace(".", "").Trim();
                _HTraining.FileType = this.FileTrainingDocumentUpload.PostedFile.ContentType;
                _HTraining.FileLocation = @"../Uploads/";
                _HTraining.ServerFileName = ServerFileName;
                _HTraining.UserFileName = UserFileName;
                _HTraining.Size = this.ToSizeString(fileSize);

            }

            myStatus = NewHRManager.Instance.InsertUpdateTraining(_HTraining, isSave);

            if (myStatus.IsSuccess)
            {
                if (isSave)
                    NewMessage.ShowNormalMessage("Record Save successfully");
                else
                    NewMessage.ShowNormalMessage("Record Updated successfully");

                //  this.ClearFields();
                this.AETrainingWindow.Close();
                this.LoadTrainingGrid(this.GetEmployeeID());

            }
            else
                NewMessage.ShowWarningMessage(myStatus.ErrorMessage);
        }

        protected string ToSizeString(double bytes)
        {

            string[] SizeSuffixes = { "bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };
            int mag = (int)Math.Log(bytes, 1024);
            decimal adjustedSize = (decimal)bytes / (1 << (mag * 10));
            return string.Format("{0:n1} {1}", adjustedSize, SizeSuffixes[mag]);
        }

        protected void TrainingDownLoad(int ID)
        {

            //string contentType = "";
            HTraining doc = NewHRManager.GetTrainingDetailsById(ID);

            string path = Context.Server.MapPath(doc.FileLocation + doc.ServerFileName);
            string contentType = doc.FileType;
            string name = doc.UserFileName + "." + doc.FileFormat;

            byte[] bytes = File.ReadAllBytes(path);

            Context.Response.Clear();
            Context.Response.ClearHeaders();
            Context.Response.AppendHeader("Content-Disposition", "attachment; filename=\"" + name + "\"");
            Context.Response.ContentType = contentType;
            Context.Response.BinaryWrite(bytes);
            Context.Response.End();
            // Response.Redirect("~/DocumentHandler.ashx?ID=" + ID);
        }

        protected bool UploadFileTraining(string relativePath)
        {

            if (this.FileTrainingDocumentUpload.HasFile)
            {

                int fileSize = FileTrainingDocumentUpload.PostedFile.ContentLength;
                this.FileTrainingDocumentUpload.PostedFile.SaveAs(Server.MapPath(relativePath));
                return true;
            }
            else
                return false;

        }


        protected void LoadTrainingGrid(int EmployeeID)
        {
            List<HTraining> _HTraining = NewHRManager.GetTrainingByEmployeeID(EmployeeID);

           
            this.StoreTraining.DataSource = _HTraining;
            this.StoreTraining.DataBind();

            if (_isDisplayMode && _HTraining.Count <= 0)
                GridTraining.Hide();
            else
                GridTraining.Show();

        }


        protected int GetEmployeeID()
        {
            //Guid? AssetID = null;
            int EmpID = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                EmpID = int.Parse(Request.QueryString["ID"]);
            }
            else
            {
                EmpID = SessionManager.CurrentLoggedInEmployeeId;
            }
            return EmpID;
        }

        protected void btnAddNewLine_Click(object sender, DirectEventArgs e)
        {
            this.ClearFields();
            this.AETrainingWindow.Show();

        }

        protected void lnkDeleteFile_Click(object sender, DirectEventArgs e)
        {
            int TrainingID = int.Parse(hdnTrainingID.Text);
            Status myStatus = new Status();

            HTraining _HTraining = NewHRManager.GetTrainingDetailsById(TrainingID);
            string path = Context.Server.MapPath(_HTraining.FileLocation + _HTraining.ServerFileName);
            myStatus = NewHRManager.Instance.DeleteTrainingFile(TrainingID);
            if (myStatus.IsSuccess)
            {
                if (File.Exists(path))
                {
                    File.Delete(path);
                    lnkDeleteFile.Hide();
                    lblUploadedFile.Hide();
                    NewMessage.ShowNormalMessage("file deleted successfully");
                    //refresh grid
                    this.LoadTrainingGrid(this.GetEmployeeID());

                }
            }



        }

        private object[] TrainingFillData
        {
            get
            {
                return new object[]
            {
                new object[] { "", "", "",""},
               
            };
            }
        }

        protected void GridTraining_Command(object sender, DirectEventArgs e)
        {
            string commandName = e.ExtraParams["command"];
            int TrainingID = int.Parse(e.ExtraParams["ID"]);
            HTraining _HTraining = NewHRManager.GetTrainingDetailsById(int.Parse(e.ExtraParams["ID"]));
            string Path = Server.MapPath(_HTraining.FileLocation) + _HTraining.ServerFileName;
            switch (commandName)
            {
                case "Delete":
                    this.DeleteTraining(TrainingID, Path);
                    break;
                case "DownLoad":
                    this.TrainingDownLoad(TrainingID);
                    break;
                case "Edit":
                    this.editTraining(TrainingID);
                    break;
            }

        }

        protected void DeleteTraining(int ID, string path)
        {

            bool result = NewHRManager.DeleteTrainingByID(ID);
            if (result)
            {
                if (File.Exists(path))
                    File.Delete(path);
                //refresh grid
                this.LoadTrainingGrid(this.GetEmployeeID());
            }

        }

        protected void ClearFields()
        {
            this.hdnTrainingID.Text = "";
            cmbTrainingType.Value = "";
            txtTrainingName.Text = "";
            txtInstitute.Text = "";
            cmbCountry.Value = "";
            txtDuration.Text = "";
            cmbDurationType.Value = "";
            calStartDate.Text = "";
            calEndDate.Text = "";
            txtCostOfTraining.Text = "";
            txtTotalCost.Text = "";
            txtResourcePerson.Text = "";
            txtBondPeriod.Text = "";
            cmbBond.Value = "";
            cmbBondType.Value = "";
            lblUploadedFile.Hide();
            lblUploadedFile.Text = "";
            lnkDeleteFile.Hide();
            txtBondPeriod.Show();
            cmbBondType.Show();
            txtNote.Text = "";
            btnSave.Text = Resources.Messages.Save;
        }

        public void editTraining(int TrainingID)
        {

            HTraining _HTraining = NewHRManager.GetTrainingDetailsById(TrainingID);
            this.hdnTrainingID.Text = TrainingID.ToString();

            cmbTrainingType.Store[0].ClearFilter();
            if (_HTraining.TrainingTypeID != null)
                cmbTrainingType.SetValue(_HTraining.TrainingTypeID.ToString());
            else
                cmbTrainingType.ClearValue();

            //cmbTrainingType.SetValue(_HTraining.TrainingTypeID.ToString());

            txtTrainingName.Text =_HTraining.TrainingName;
            txtInstitute.Text = _HTraining.InstitutionName;
            cmbCountry.SetValue(_HTraining.Country);
            txtResourcePerson.Text = _HTraining.ResourcePerson == null ? "" : _HTraining.ResourcePerson;
            
            if (_HTraining.Duration != null)
                txtDuration.Text = _HTraining.Duration.ToString();
            else
                txtDuration.Text = "";

            if (_HTraining.DurationTypeID != null)
                cmbDurationType.SetValue(_HTraining.DurationTypeID.ToString());
            else
                cmbDurationType.ClearValue();

            calStartDate.Text = _HTraining.TrainingFrom;
            calEndDate.Text = _HTraining.TrainingTo;
            if (_HTraining.CostOfTraining != null)
            {
                txtCostOfTraining.Text = decimal.Parse(_HTraining.CostOfTraining.ToString()).ToString("0.00");
            }
            else
                txtCostOfTraining.Text = "";
            if (_HTraining.TotalCost != null)
            {
                txtTotalCost.Text = decimal.Parse(_HTraining.TotalCost.ToString()).ToString("0.00");
            }
            else
                txtTotalCost.Text = "";

            txtBondPeriod.Text = _HTraining.BondPeriod.ToString();
            cmbBond.SetValue(_HTraining.Bond.ToString());
            cmbBondType.SetValue(_HTraining.BondTypeID.ToString());
            txtNote.Text = _HTraining.Note;
            if (_HTraining.Bond.ToString() == "True")
            {
                cmbBondType.Show();
                txtBondPeriod.Show();
            }
            else
            {
                cmbBondType.Hide();
                txtBondPeriod.Hide();
            }
            txtNote.Text = _HTraining.Note;
            if (!string.IsNullOrEmpty(_HTraining.UserFileName))
            {
                lblUploadedFile.Show();
                lblUploadedFile.Text = _HTraining.UserFileName;
                lnkDeleteFile.Show();
            }
            else
            {
                lblUploadedFile.Hide();
                lblUploadedFile.Text = "";
                lnkDeleteFile.Hide();
            }
            btnSave.Text = Resources.Messages.Update;
            this.AETrainingWindow.Show();
        }

        public void cmbBond_Change(object sender, DirectEventArgs e)
        {
            if (cmbBond.SelectedItem.Text == "Yes")
            {
                cmbBondType.Show();
                txtBondPeriod.Show();

            }
            else
            {
                cmbBondType.Hide();
                txtBondPeriod.Hide();
                
            }
        }

    }
}