﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HealthCtl.ascx.cs" Inherits="Web.NewHR.UserControls.HealthCtl" %>
<ext:Hidden runat="server" ID="hdnHealthID">
</ext:Hidden>
<style type="text/css">
    #tblHandicapped td {
        /*border: 1px solid #428BCA;*/
    }
</style>
<br />
<div style="background: #D8E7F3; border: 1px solid #C1DDF1; padding: 5px; margin-bottom: 10px;">
    <table id="tblHandicapped">
        <tr>
            <td style="padding: 0px; width: 100px;">
                <asp:Label runat="server" ID="lblHandicapped1" />
            </td>
            <td style="padding-top: 5px;">
                <ext:Label runat="server" ID="lblRefcmbHandicapped" Text="No">
                </ext:Label>
                <ext:ComboBox ID="cmbHandicapped" runat="server" ValueField="Value" DisplayField="Text"
                    Hidden="true" Width="100" QueryMode="Local">
                    <Items>
                        <ext:ListItem Text="Yes" Value="True" />
                        <ext:ListItem Text="No" Value="False" />
                    </Items>
                    <DirectEvents>
                        <Select OnEvent="cmbHandicapped_Change">
                            <EventMask ShowMask="true" />
                        </Select>
                    </DirectEvents>
                </ext:ComboBox>
            </td>
            <td style="padding: 0px;" runat="server" id="td1">
                <div style="float: left">
                    <ext:Label runat="server" ID="Label1" Text="Details" StyleSpec="Width:300; padding-left:15px; padding-right:10px; padding-top:5px;">
                    </ext:Label>
                </div>
                <div style="float: right">
                    <ext:Label runat="server" ID="lblreftxtHandicappedDetail" Text="" Width="300">
                    </ext:Label>
                    <ext:TextField ID="txtHandicappedDetail" runat="server" Hidden="true" Width="300" />
                </div>
            </td>
            <td style="padding: 5px;" runat="server" id="td2">
                <ext:Button runat="server" Text="Edit" ID="btnEditHandicapped" Cls="bluebutton left">
                    <DirectEvents>
                        <Click OnEvent="btnEditHandicapped_Click">
                            <EventMask ShowMask="true" />
                        </Click>
                    </DirectEvents>
                </ext:Button>
                <ext:Button runat="server" Text="Update" ID="btnUpdateHandicapped" Cls="bluebutton left"
                    Hidden="true">
                    <DirectEvents>
                        <Click OnEvent="btnUpdateHandicapped_Click">
                            <EventMask ShowMask="true" />
                        </Click>
                    </DirectEvents>
                </ext:Button>
            </td>
        </tr>
    </table>
</div>
<table>
    <tr>
        <td>
            <ext:TextField ID="txtBirthMark" LabelAlign="Top" FieldLabel="Birthmark" LabelSeparator=""
                runat="server" Width="200px" />
            <asp:RequiredFieldValidator ID="req1" ControlToValidate="txtBirthMark" Display="None"
                ErrorMessage="Birthmark is required." runat="server" ValidationGroup="saveBithMark" />
        </td>

        <td style="padding-left: 10px;">
            <ext:ComboBox ID="cmbBoodGroup" FieldLabel="Blood Group" runat="server" LabelAlign="Top"
                Width="180" LabelSeparator="" QueryMode="Local" DisplayField="BloodGroupName" ValueField="Id">
                <Store>
                    <ext:Store ID="store2" runat="server">
                        <Fields>
                            <ext:ModelField Name="Id" />
                            <ext:ModelField Name="BloodGroupName" />
                        </Fields>
                    </ext:Store>
                </Store>
            </ext:ComboBox>
            <asp:RequiredFieldValidator ID="rfvcmbBoodGroup" ControlToValidate="cmbBoodGroup" Display="None"
                ErrorMessage="Blood group is required." runat="server" ValidationGroup="saveBithMark" />
        </td>
    </tr>
    <tr>
        <td style="padding-top: 10px;">
            <ext:Button runat="server" Text="Save" ID="btnSaveBirthMark" Cls="btn btn-primary"
                StyleSpec="border-radius:0px" Height="30">
                <DirectEvents>
                    <Click OnEvent="btnSaveBirthMark_Click">
                        <EventMask ShowMask="true" />
                    </Click>
                </DirectEvents>
                <Listeners>
                    <Click Handler="valGroup= 'saveBithMark'; return CheckValidation();">
                    </Click>
                </Listeners>
            </ext:Button>
        </td>
    </tr>
</table>
<br />
<table class="fieldTable firsttdskip">
    <tr>
        <td>
            <ext:GridPanel ID="GridHealth" runat="server" Width="1000" Cls="itemgrid">
                <Store>
                    <ext:Store ID="StoreHealth" runat="server">
                        <Model>
                            <ext:Model ID="Model1" runat="server" IDProperty="HealthId">
                                <Fields>
                                    <ext:ModelField Name="HealthStatus" Type="string" />
                                    <ext:ModelField Name="ContitionTypeName" Type="String" />
                                    <ext:ModelField Name="DiagnosedTypeName" Type="string" />
                                    <ext:ModelField Name="DiagnosedDate" Type="string" />
                                    <ext:ModelField Name="Height" Type="string" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:Column ID="Column1" runat="server" Text="Health Status" DataIndex="HealthStatus"
                            Wrap="true" Width="200" Flex="1" />
                        <ext:Column ID="Column2" runat="server" Text="Contition" DataIndex="ContitionTypeName"
                            Wrap="true" Width="150">
                        </ext:Column>
                        <ext:Column ID="Column3" runat="server" Text="Diagnosed Type" DataIndex="DiagnosedTypeName"
                            Wrap="true" Width="150">
                        </ext:Column>
                        <ext:Column ID="Column4" runat="server" Text="Diagnosed Date" DataIndex="DiagnosedDate"
                            Wrap="true" Width="130">
                        </ext:Column>
                        <ext:Column ID="Column5" runat="server" Text="Height" DataIndex="Height" Wrap="true"
                            Width="100">
                        </ext:Column>
                        <ext:CommandColumn ID="CommandColumn1" runat="server" Width="40">
                            <Commands>
                                <ext:CommandSeparator />
                                <ext:GridCommand Cls="editGridButton" ToolTip-Text="Edit" Text="<i class='fa fa-pencil'></i>"
                                    CommandName="Edit" />
                            </Commands>
                            <DirectEvents>
                                <Command OnEvent="GridHealth_Command">
                                    <EventMask ShowMask="true">
                                    </EventMask>
                                    <ExtraParams>
                                        <ext:Parameter Name="ID" Value="record.data.HealthId" Mode="Raw">
                                        </ext:Parameter>
                                        <ext:Parameter Name="command" Value="command" Mode="Raw">
                                        </ext:Parameter>
                                    </ExtraParams>
                                </Command>
                            </DirectEvents>
                        </ext:CommandColumn>
                        <ext:CommandColumn ID="CommandColumn2" runat="server" Width="40">
                            <Commands>
                                <ext:CommandSeparator />
                                <ext:GridCommand Cls="editGridButton" ToolTip-Text="Delete" Text="<i class='fa fa-trash-o'></i>"
                                    CommandName="Delete" />
                            </Commands>
                            <DirectEvents>
                                <Command OnEvent="GridHealth_Command">
                                    <EventMask ShowMask="true">
                                    </EventMask>
                                    <Confirmation Message="Are you sure, you want to delete the record?" ConfirmRequest="true" />
                                    <ExtraParams>
                                        <ext:Parameter Name="ID" Value="record.data.HealthId" Mode="Raw">
                                        </ext:Parameter>
                                        <ext:Parameter Name="command" Value="command" Mode="Raw">
                                        </ext:Parameter>
                                    </ExtraParams>
                                </Command>
                            </DirectEvents>
                        </ext:CommandColumn>
                        <ext:Column ID="Column6" runat="server" Text="" Width="190">
                        </ext:Column>
                    </Columns>
                </ColumnModel>
                <SelectionModel>
                    <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                </SelectionModel>
            </ext:GridPanel>
        </td>
    </tr>
</table>
<div class="buttonBlockSection">
    <ext:Button runat="server" ID="LinkButton1" Cls="btn btn-primary btn-sect" runat="server"
        Width="130" StyleSpec="margin-top:10px" Height="30" Text="<i></i>Add Health">
        <DirectEvents>
            <Click OnEvent="btnAddNewLine_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>
</div>
<ext:Window ID="AEHealthWindow" runat="server" ButtonAlign="Left" Title="Add/Edit Health" Icon="Application"
    Height="450" Width="800" BodyPadding="5" Hidden="true" Modal="true">
    <Content>
        <div style="float: left; width: 450px;">
            <table class="fieldTable">
                <tr>
                    <td>
                        <ext:TextField ID="txtHealthStatus" runat="server" FieldLabel="Health Status" LabelAlign="top"
                            LabelSeparator="" />
                        <asp:RequiredFieldValidator Display="None" ID="valtxtHealthStatus" runat="server"
                            ValidationGroup="HealthSaveUpdate" ControlToValidate="txtHealthStatus" ErrorMessage="Health Status is required." />
                    </td>
                    <td>
                        <ext:ComboBox ID="cmbContition" runat="server" ValueField="ConditionTypeId" DisplayField="ConditionTypeName"
                            FieldLabel="Condition" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                            QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store1" runat="server">
                                    <Model>
                                        <ext:Model ID="Model2" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="ConditionTypeId" Type="String" />
                                                <ext:ModelField Name="ConditionTypeName" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator Display="None" ID="valcmbContition" runat="server" ValidationGroup="HealthSaveUpdate"
                            ControlToValidate="cmbContition" ErrorMessage="Contition is required." />
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:ComboBox ID="cmbDiagnosed" runat="server" ValueField="Value" DisplayField="Text"
                            FieldLabel="Diagnosed" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                            QueryMode="Local">
                            <Items>
                                <ext:ListItem Text="Before Joining" Value="1" />
                                <ext:ListItem Text="After Joining" Value="2" />
                            </Items>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator Display="None" ID="valcmbDiagnosed" runat="server" ValidationGroup="HealthSaveUpdate"
                            ControlToValidate="cmbDiagnosed" ErrorMessage="Diagnosed is required." />
                    </td>
                    <td>
                        <pr:CalendarExtControl FieldLabel="Diagnosed Date" ID="calDiagnosed" runat="server"
                            LabelSeparator="" LabelAlign="Top" />
                        <asp:RequiredFieldValidator Display="None" ID="valcalDiagnosed" runat="server" ValidationGroup="HealthSaveUpdate"
                            ControlToValidate="calDiagnosed" ErrorMessage="Diagnosed Date is required." />
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:ComboBox ID="cmbOngoingTreatment" runat="server" ValueField="Value" DisplayField="Text"
                            FieldLabel="Ongoing Treatment" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                            QueryMode="Local">
                            <Items>
                                <ext:ListItem Text="Yes" Value="True" />
                                <ext:ListItem Text="No" Value="False" />
                            </Items>
                            <DirectEvents>
                                <Select OnEvent="cmbOngoingTreatment_Change">
                                    <EventMask ShowMask="true" />
                                </Select>
                            </DirectEvents>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator Display="None" ID="valcmbOngoingTreatment" runat="server"
                            ValidationGroup="HealthSaveUpdate" ControlToValidate="cmbOngoingTreatment" ErrorMessage="Ongoing Treatment is required." />
                    </td>
                    <td>
                        <ext:TextField ID="txtHeight" runat="server" FieldLabel="Height" LabelAlign="top"
                            LabelSeparator="" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <ext:TextArea ID="txtDetails" runat="server" FieldLabel="Details" LabelSeparator=""
                            LabelAlign="Top" Rows="5" Cols="50" />
                    </td>
                </tr>
                <%-- <tr>
                    <td valign="bottom" colspan="2">
                        <div class="popupButtonDiv">
                            <div class="btnFlatOr">
                                or</div>
                        </div>
                    </td>
                </tr>--%>
            </table>
        </div>
        <div style="margin-left: 450px; vertical-align: top;">
            <table class="fieldTable">
                <tr>
                    <td>
                        <ext:TextField ID="txtHospitalClinic" runat="server" FieldLabel="Hospital/Clinic"
                            LabelAlign="top" Width="300" LabelSeparator="" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:TextArea ID="txtHospitalAddress" runat="server" FieldLabel="Address" LabelAlign="Top"
                            Width="300" LabelSeparator="" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:TextField ID="txtDoctorName" runat="server" FieldLabel="Doctor's Name" LabelAlign="top"
                            Width="300" LabelSeparator="" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:TextField ID="txtPhoneNo" runat="server" FieldLabel="Phone Number" LabelAlign="top"
                            Width="300" LabelSeparator="" />
                        <asp:RegularExpressionValidator ID="valRegPhone" runat="server" ControlToValidate="txtPhoneNo"
                            ErrorMessage="Contact number is invalid." ValidationGroup="HealthSaveUpdate"
                            Display="None"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:TextField ID="txtDoctorEmail" runat="server" FieldLabel="Doctor Email" LabelAlign="top"
                            Width="300" LabelSeparator="" />
                        <asp:RegularExpressionValidator ID="varRegEmail" runat="server" ControlToValidate="txtDoctorEmail"
                            ErrorMessage="Email id is invalid." ValidationGroup="HealthSaveUpdate" Display="None"></asp:RegularExpressionValidator>
                    </td>
                </tr>
            </table>
        </div>
    </Content>
    <Buttons>
        <ext:Button runat="server" ID="btnSave" StyleSpec="margin-left:10px;" Cls="btn btn-primary" Text="<i></i>Save">
            <DirectEvents>
                <Click OnEvent="btnSave_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
            <Listeners>
                <Click Handler="valGroup = 'HealthSaveUpdate';  if(CheckValidation()) return this.disable(); else return false;">
                </Click>
            </Listeners>
        </ext:Button>
        <ext:Button ID="btnCancel1" StyleSpec="margin-left:10px;" Cls="btn btn-default" runat="server" Text="Cancel">
            <Listeners>
                <Click Handler="#{AEHealthWindow}.hide();" />
            </Listeners>
        </ext:Button>
    </Buttons>
</ext:Window>
