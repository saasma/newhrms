﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;

using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;

namespace Web.NewHR.UserControls
{
    public partial class PassportCtl : BaseUserControl
    {
        private bool _isDisplayMode = false;
        public bool IsDisplayMode
        {
            set
            {
                if (value == true)
                {
                    _isDisplayMode = value;
                    CommandColumn3.Visible = false;
                    CommandColumn2.Visible = false;
                    LinkButton1.Visible = false;
                    GridPassport.Width = 520;
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();

                if (SessionManager.CurrentLoggedInEmployeeId != 0)
                    hdnIsEmployee.Text = "1";
                else
                    hdnIsEmployee.Text = "0";
            }
        }

        protected void Initialise()
        {
            int EmployeeID = GetEmployeeID();
            this.LoadPassportGrid(EmployeeID);

        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {

            this.SaveFormData();

        }

        private void SaveFormData()
        {

            Status myStatus = new Status();
            HPassport _HPassport = new HPassport();
            _HPassport.EmployeeId = this.GetEmployeeID();
            bool isSave = bool.Parse(this.hdnPassportID.Text == "" ? "true" : "false");
            if (!isSave)
                _HPassport.PassportId = int.Parse(this.hdnPassportID.Text);

            _HPassport.PassportNo = txtPassportNumber.Text.Trim();
            if (!string.IsNullOrEmpty(calIssueDate.Text.Trim()))
            {
                _HPassport.IssuingDate = calIssueDate.Text.Trim();
                _HPassport.IssuingDateEng = BLL.BaseBiz.GetEngDate(_HPassport.IssuingDate, IsEnglish);
            }
            if (!string.IsNullOrEmpty(calValidUpto.Text.Trim()))
            {
                _HPassport.ValidUpto = calValidUpto.Text.Trim();
                _HPassport.ValidUptoEng = BLL.BaseBiz.GetEngDate(_HPassport.ValidUpto, IsEnglish);
            }

            //file upload section
            string UserFileName = this.FilePassportDocumentUpload.FileName;
            string ServerFileName = Guid.NewGuid().ToString() + Path.GetExtension(UserFileName);
            string relativePath = @"../uploads/" + ServerFileName;
            if (this.UploadFilePassport(relativePath))
            {
                double fileSize = FilePassportDocumentUpload.PostedFile.ContentLength;

                _HPassport.FileFormat = Path.GetExtension(this.FilePassportDocumentUpload.FileName).Replace(".", "").Trim();
                _HPassport.FileType = this.FilePassportDocumentUpload.PostedFile.ContentType;
                _HPassport.FileLocation = @"../Uploads/";
                _HPassport.ServerFileName = ServerFileName;
                _HPassport.UserFileName = UserFileName;
                _HPassport.Size = this.ToSizeString(fileSize);

            }

            if (SessionManager.CurrentLoggedInEmployeeId == 0)
                _HPassport.Status = 1;
            else
                _HPassport.Status = 0;

            myStatus = NewHRManager.Instance.InsertUpdatePassport(_HPassport, isSave);

            if (myStatus.IsSuccess)
            {
                if (isSave)
                    NewMessage.ShowNormalMessage("Record Save successfully");
                else
                    NewMessage.ShowNormalMessage("Record Updated successfully");

                //  this.ClearFields();
                this.AEPassportWindow.Close();
                this.LoadPassportGrid(this.GetEmployeeID());

            }
            else
                NewMessage.ShowWarningMessage(myStatus.ErrorMessage);
        }

        protected string ToSizeString(double bytes)
        {

            string[] SizeSuffixes = { "bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };
            int mag = (int)Math.Log(bytes, 1024);
            decimal adjustedSize = (decimal)bytes / (1 << (mag * 10));
            return string.Format("{0:n1} {1}", adjustedSize, SizeSuffixes[mag]);
        }

        protected void PassportDownLoad(int ID)
        {

            //string contentType = "";
            HPassport doc = NewHRManager.GetPassportDetailsById(ID);

            string path = Context.Server.MapPath(doc.FileLocation + doc.ServerFileName);
            string contentType = doc.FileType;
            string name = doc.UserFileName + "." + doc.FileFormat;

            byte[] bytes = File.ReadAllBytes(path);

            Context.Response.Clear();
            Context.Response.ClearHeaders();
            Context.Response.AppendHeader("Content-Disposition", "attachment; filename=\"" + name + "\"");
            Context.Response.ContentType = contentType;
            Context.Response.BinaryWrite(bytes);
            Context.Response.End();
            // Response.Redirect("~/DocumentHandler.ashx?ID=" + ID);
        }

        protected bool UploadFilePassport(string relativePath)
        {

            if (this.FilePassportDocumentUpload.HasFile)
            {

                int fileSize = FilePassportDocumentUpload.PostedFile.ContentLength;
                this.FilePassportDocumentUpload.PostedFile.SaveAs(Server.MapPath(relativePath));
                return true;
            }
            else
                return false;

        }


        protected void LoadPassportGrid(int EmployeeID)
        {
            List<HPassport> _HPassport = NewHRManager.GetPassportByEmployeeID(EmployeeID);
            if (_HPassport != null)
            {
                this.StorePassport.DataSource = _HPassport;
                this.StorePassport.DataBind();
            }
            else
            {
                this.StorePassport.DataSource = this.PassportFillData;
                this.StorePassport.DataBind();
            }
        }


        protected int GetEmployeeID()
        {
            //Guid? AssetID = null;
            int EmpID = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                EmpID = int.Parse(Request.QueryString["ID"]);
            }
            else
                EmpID = SessionManager.CurrentLoggedInEmployeeId;

            return EmpID;
        }

        protected void btnAddNewLine_Click(object sender, DirectEventArgs e)
        {
            this.ClearFields();
            this.AEPassportWindow.Show();

        }

        protected void lnkDeleteFile_Click(object sender, DirectEventArgs e)
        {
            int PassportID = int.Parse(hdnPassportID.Text);
            Status myStatus = new Status();

            HPassport _HPassport = NewHRManager.GetPassportDetailsById(PassportID);
            string path = Context.Server.MapPath(_HPassport.FileLocation + _HPassport.ServerFileName);
            myStatus = NewHRManager.Instance.DeletePassportFile(PassportID);
            if (myStatus.IsSuccess)
            {
                if (File.Exists(path))
                {
                    File.Delete(path);
                    lnkDeleteFile.Hide();
                    lblUploadedFile.Hide();
                    NewMessage.ShowNormalMessage("file deleted successfully");
                    //refresh grid
                    this.LoadPassportGrid(this.GetEmployeeID());

                }
            }



        }

        private object[] PassportFillData
        {
            get
            {
                return new object[]
            {
                new object[] { "", "", "",""},
               
            };
            }
        }

        protected void GridPassport_Command(object sender, DirectEventArgs e)
        {
            string commandName = e.ExtraParams["command"];
            int PassportID = int.Parse(e.ExtraParams["ID"]);
            HPassport _HPassport = NewHRManager.GetPassportDetailsById(int.Parse(e.ExtraParams["ID"]));
            string Path = Server.MapPath(_HPassport.FileLocation) + _HPassport.ServerFileName;
            switch (commandName)
            {
                case "Delete":
                    this.DeletePassport(PassportID, Path);
                    break;
                case "DownLoad":
                    this.PassportDownLoad(PassportID);
                    break;
                case "Edit":
                    this.editPassport(PassportID);
                    break;
            }

        }

        protected void DeletePassport(int ID, string path)
        {

            bool result = NewHRManager.DeletePassportByID(ID);
            if (result)
            {
                if (File.Exists(path))
                    File.Delete(path);
                //refresh grid
                NewMessage.ShowNormalMessage("Record deleted successfully.");
                this.LoadPassportGrid(this.GetEmployeeID());
            }

        }

        protected void ClearFields()
        {
            this.hdnPassportID.Text = "";
            txtPassportNumber.Text = "";
            calValidUpto.Text = "";
            calIssueDate.Text = "";
            lblUploadedFile.Hide();
            lblUploadedFile.Text = "";
            lnkDeleteFile.Hide();
            btnSave.Text = Resources.Messages.Save;
            FilePassportDocumentUpload.Reset();
        }

        public void editPassport(int PassportID)
        {
            HPassport _HPassport = NewHRManager.GetPassportDetailsById(PassportID);
            this.hdnPassportID.Text = PassportID.ToString();
            txtPassportNumber.Text =_HPassport.PassportNo;
            calIssueDate.Text = _HPassport.IssuingDate;
            calValidUpto.Text = _HPassport.ValidUpto;
         
            if (!string.IsNullOrEmpty(_HPassport.UserFileName))
            {
                lblUploadedFile.Show();
                lblUploadedFile.Text = _HPassport.UserFileName;
                lnkDeleteFile.Show();
            }
            else
            {
                lblUploadedFile.Hide();
                lblUploadedFile.Text = "";
                lnkDeleteFile.Hide();
            }
            btnSave.Text = Resources.Messages.Update;
            this.AEPassportWindow.Show();
        }

      

    }
}