﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;

using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;

namespace Web.NewHR.UserControls
{
    public partial class SkillSetsCtl : BaseUserControl
    {
        private bool _isDisplayMode = false;
        public bool IsDisplayMode
        {
            set
            {
                if (value == true)
                {
                    _isDisplayMode = value;
                    LinkButton1.Visible = false;
                    AESkillSetsWindow.Visible = false;
                    CommandColumn1.Visible = false;
                }
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();

            }
        }

        protected void Initialise()
        {
            int EmployeeID = GetEmployeeID();
            this.LoadSkillSetsGrid(EmployeeID);


            LoadSkillsets();





        }

        private void LoadSkillsets()
        {
            CommonManager _CommonManager = new CommonManager();
            cmbLevelOfExpertise.Store[0].DataSource = CommonManager.GetCompetencyLevels();
            cmbLevelOfExpertise.Store[0].DataBind();

            cmbSkill.Store[0].DataSource = _CommonManager.GetAllSkillSet();
            cmbSkill.Store[0].DataBind();
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            //validation 
            if (!string.IsNullOrEmpty(cmbSkill.SelectedItem.Text) && txtNewSkill.Text.Trim() != "")
            {
                NewMessage.ShowWarningMessage("fill either skill or new skill !");
                return;
            }
            else if((chkAddtoSkillPool.Checked==true) && (txtNewSkill.Text.Trim()==""))
            {
                NewMessage.ShowWarningMessage("please enter new skill !");
                return;
            }
            else if (txtNewSkill.Text.Trim()!= ""  && (chkAddtoSkillPool.Checked==false))
            {
                NewMessage.ShowWarningMessage("plese check mark for Add to Skill Pool.");
                return;
            }
            else if (!string.IsNullOrEmpty(cmbSkill.SelectedItem.Value))
            {
                if (NewHRManager.IsDuplicateSkillByEmpID(int.Parse(cmbSkill.SelectedItem.Value), GetEmployeeID()))
                {
                    NewMessage.ShowWarningMessage("This skill already exists for this employee.");
                    return;
                }
            }
            else if (!string.IsNullOrEmpty(txtNewSkill.Text.Trim()))
            {
                if (NewHRManager.IsDuplicateSkillInPool(txtNewSkill.Text.Trim()))
                {
                    NewMessage.ShowWarningMessage("This skill already exists in skill pool.");
                    return;
                }
            }



            


            //-------------
            this.SaveFormData();

        }

        private void SaveFormData()
        {

            Status myStatus = new Status();
            SkillSetEmployee _SkillSetEmployee = new SkillSetEmployee();
            _SkillSetEmployee.EmployeeId = this.GetEmployeeID();
            bool SaveToPool = false;
            string NewSkillPool = string.Empty;
         
            bool isSave = bool.Parse(this.hdnSkillSetsID.Text == "" ? "true" : "false");
            if (!isSave)
                _SkillSetEmployee.SkillSetId = int.Parse(this.hdnSkillSetsID.Text);

            if (!string.IsNullOrEmpty(cmbSkill.SelectedItem.Text))
            {
                _SkillSetEmployee.SkillSetId = int.Parse(cmbSkill.SelectedItem.Value);
            }
            else
            {
                if (chkAddtoSkillPool.Checked == true)
                {
                    SaveToPool = true;
                    NewSkillPool = txtNewSkill.Text.Trim();
                }
            }

            if (cmbLevelOfExpertise.SelectedItem != null && cmbLevelOfExpertise.SelectedItem.Value != null)
                _SkillSetEmployee.LevelID = int.Parse(cmbLevelOfExpertise.SelectedItem.Value);
            _SkillSetEmployee.Note = txtNote.Text;



            myStatus = NewHRManager.Instance.InsertUpdateSkillSets(_SkillSetEmployee, isSave, SaveToPool, NewSkillPool, GetEmployeeID());

            if (chkAddtoSkillPool.Checked)
                LoadSkillsets();

            if (myStatus.IsSuccess)
            {
                if (isSave)
                    NewMessage.ShowNormalMessage("Record Save successfully");
                else
                    NewMessage.ShowNormalMessage("Record Updated successfully");

                //  this.ClearFields();
                this.AESkillSetsWindow.Close();
                this.LoadSkillSetsGrid(this.GetEmployeeID());

            }
            else
                NewMessage.ShowWarningMessage(myStatus.ErrorMessage);
        }


        protected void LoadSkillSetsGrid(int EmployeeID)
        {

            List<NewHREmployeeSkillSetBO> _EmployeeSkillSetBO = NewHRManager.GetEmployeesSkillSet(EmployeeID);


            this.StoreSkillSets.DataSource = _EmployeeSkillSetBO;
            this.StoreSkillSets.DataBind();

            if (_isDisplayMode && _EmployeeSkillSetBO.Count <= 0)
                GridSkillSets.Hide();
        }


        protected int GetEmployeeID()
        {
            //Guid? AssetID = null;
            int EmpID = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                EmpID = int.Parse(Request.QueryString["ID"]);
            }
            else
            {
                EmpID = SessionManager.CurrentLoggedInEmployeeId;
            }
            return EmpID;
        }

        protected void btnAddNewLine_Click(object sender, DirectEventArgs e)
        {
            this.ClearFields();
            this.AESkillSetsWindow.Show();

        }

        private object[] SkillSetsFillData
        {
            get
            {
                return new object[]
            {
                new object[] { "", "", "",""},
               
            };
            }
        }

        protected void GridSkillSets_Command(object sender, DirectEventArgs e)
        {
            string commandName = e.ExtraParams["command"];
            int SkillSetsID = int.Parse(e.ExtraParams["ID"]);
            SkillSetEmployee _SkillSetEmployee = NewHRManager.GetSkillSetsDetailsById(int.Parse(e.ExtraParams["ID"]), GetEmployeeID());
            switch (commandName)
            {
                case "Delete":
                    this.DeleteData(SkillSetsID);
                    break;
             
                case "Edit":
                    this.editSkillSets(SkillSetsID);
                    break;
            }

        }

        protected void DeleteData(int ID)
        {

            bool result = NewHRManager.DeleteSkillSetsByID(ID,GetEmployeeID());
            this.LoadSkillSetsGrid(this.GetEmployeeID());
          
        }

        protected void ClearFields()
        {
            this.hdnSkillSetsID.Text = "";
            txtNote.Text = "";
            txtNewSkill.Text = "";
            cmbSkill.Value = "";
            cmbLevelOfExpertise.Value = "";
            chkAddtoSkillPool.Checked = false;
            cmbSkill.Disabled = false;
            txtNewSkill.Disabled = false;
            chkAddtoSkillPool.Disabled = false;
            btnSave.Text = Resources.Messages.Save;
        }

        public void editSkillSets(int SkillSetsID)
        {

            SkillSetEmployee _SkillSetEmployee = NewHRManager.GetSkillSetsDetailsById(SkillSetsID, GetEmployeeID());
            this.hdnSkillSetsID.Text = SkillSetsID.ToString();
            cmbSkill.Value = _SkillSetEmployee.SkillSetId;
            if (_SkillSetEmployee.LevelID != null)
                cmbLevelOfExpertise.Value = _SkillSetEmployee.LevelID;
            txtNote.Text = _SkillSetEmployee.Note;
            btnSave.Text = Resources.Messages.Update;
            cmbSkill.Disabled = true;
            txtNewSkill.Disabled = true;
            chkAddtoSkillPool.Disabled = true;
            this.AESkillSetsWindow.Show();
        }

    }
}