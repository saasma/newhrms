﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EducationCtrlView.ascx.cs"
    Inherits="Web.NewHR.UserControls.EducationCtrlView" %>
<script type="text/javascript">

     var prepareDownloadEdu = function(grid, toolbar, rowIndex, record){
        var downloadBtn = toolbar.items.get(0);
        if(record.data.ContainsFile == 0){  
            downloadBtn.setVisible(false);     
        }
    }
 
  var CommandHandlerEduFD = function (command, record) {
            <%= hdnEducationIdValue.ClientID %>.setValue(record.data.EductionId);

            <%= btnDownloadEduFile.ClientID %>.fireEvent('click');
        };
   

</script>
<ext:Hidden ID="hdnEducationIdValue" runat="server" />
<ext:Button runat="server" AutoPostBack="true" OnClick="btnDownloadEduFile_Click"
    Hidden="true" ID="btnDownloadEduFile" Text="<i></i>Download">
</ext:Button>

    <table class="fieldTable firsttdskip">
        <tr>
            <td>
                <ext:GridPanel ID="GridEducation" runat="server" Width="1000" Cls="itemgrid" Scroll="None">
                    <Store>
                        <ext:Store ID="StoreEducation" runat="server">
                            <Model>
                                <ext:Model ID="Model1" runat="server" IDProperty="EductionId">
                                    <Fields>
                                        <ext:ModelField Name="Course" Type="String" />
                                        <ext:ModelField Name="LevelName" Type="string" />
                                        <ext:ModelField Name="Order" Type="string" />
                                        <ext:ModelField Name="College" Type="string" />
                                        <ext:ModelField Name="Institution" Type="string" />
                                        <ext:ModelField Name="Country" Type="string" />
                                        <ext:ModelField Name="Percentage" Type="string" />
                                        <ext:ModelField Name="PassedYear" Type="string" />
                                        <ext:ModelField Name="IsEditable" Type="Int" />
                                        <ext:ModelField Name="ContainsFile" Type="Int" />
                                        <ext:ModelField Name="DivisionName" Type="String" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                    </Store>
                    <ColumnModel>
                        <Columns>
                            <ext:Column ID="Column3" Width="70" runat="server" Text="Order" DataIndex="SN" Align="Center">
                            </ext:Column>
                            <ext:Column ID="Column1" Width="180" runat="server" Text="Course Name" DataIndex="Course"
                                Wrap="true" />
                            <ext:Column ID="Column2" Width="150" runat="server" Text="Level" DataIndex="LevelName"
                                Wrap="true">
                            </ext:Column>
                            <ext:Column ID="Column6" Width="180" runat="server" Text="Institution" DataIndex="College"
                                Wrap="true">
                            </ext:Column>
                            <ext:Column ID="Column81" runat="server" Width="100" Text="Passed Year" DataIndex="PassedYear"
                                Wrap="true">
                            </ext:Column>
                            <ext:Column ID="Column7" runat="server" Width="100" Text="% / Grade" DataIndex="Percentage"
                                Wrap="true">
                            </ext:Column>
                            <ext:Column ID="Column4" runat="server" Width="100" Text="Division" DataIndex="DivisionName"
                                Wrap="true">
                            </ext:Column>
                            <ext:CommandColumn ID="CommandColumn1" runat="server" Width="40">
                                <Commands>
                                    <ext:GridCommand Icon="PageWhitePut" CommandName="DownLoad">
                                        <ToolTip Text="DownLoad" />
                                    </ext:GridCommand>
                                </Commands>
                                <Listeners>
                                    <Command Handler="CommandHandlerEduFD(command,record);" />
                                </Listeners>
                                <PrepareToolbar Fn="prepareDownloadEdu" />
                            </ext:CommandColumn>
                        </Columns>
                    </ColumnModel>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                    </SelectionModel>
                </ext:GridPanel>
            </td>
        </tr>
    </table>
