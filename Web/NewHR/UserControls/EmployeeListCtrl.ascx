﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployeeListCtrl.ascx.cs"
    Inherits="Web.NewHR.UserControls.EmployeeListCtrl" %>
<style type="text/css">
    .x-grid-cell-inner
    {
        padding: 2px 5px 2px 5px !important;
    }
    
    .x-column-header-inner
    {
        padding: 7px 5px 7px 5px;
    }
</style>
<script type="text/javascript">

      var CommandHandler = function(command, record){
            <%= hiddenValue.ClientID %>.setValue(record.data.EmployeeId);
                if(command=="Edit")
                {
                    window.location.href = "personaldetail.aspx?id="+record.data.EmployeeId;
                    //window.open('personaldetail.aspx?id='record.data.EmployeeId+,'_blank');
     
                }
                else
                {
                    <%= btnDelete.ClientID %>.fireEvent('click');
                }

             }


             function searchListEL() {



             <%=GridLevels.ClientID %>.getStore().pageSize = <%=cmbPageSize.ClientID %>.getValue();
            <%=PagingToolbar1.ClientID %>.doRefresh();
        }


        function EmpImageFunc(e,e1,e2,record) {
            //console.log(e);
            if(e2==0)
            {
                var EmpImage = <%=EmpImage.ClientID %>;
                EmpImage.setImageUrl(record.data.UrlPhoto);
            }
           
        };

     

        function RetiredRenderer(e1,e2,record)
        {
            var isActive =  record.data.IsRetiredOrResigned;
            if(isActive.trim()=="0")
            {
                return '<img src="../images/yes.png"/>';
            }
            else
            {
                return '<img src="../images/no.png"/>';
            }
            //return '<a href="EmployeeAppraisalHRPage.aspx?efid='+ record.data.AppraisalEmployeeFormID + '&fid='+ record.data.AppraisalFormRef_ID +'">View</a>';
        }

        function EmpPicRenderer(e1,e2,record)
        {
            var UrlPhoto =  record.data.UrlPhoto;
            var JS = ""+UrlPhoto+")";
            return '<img class="media-object img-circle" style="" src="'+UrlPhoto+'" height="38" width="38"/>';
            
            //return '<a href="EmployeeAppraisalHRPage.aspx?efid='+ record.data.AppraisalEmployeeFormID + '&fid='+ record.data.AppraisalFormRef_ID +'">View</a>';
        }


        
</script>
<ext:Hidden runat="server" ID="hiddenValue" />
<ext:Button ID="btnDelete" runat="server" Hidden="true">
    <DirectEvents>
        <Click OnEvent="btnDelete_Click">
            <Confirmation ConfirmRequest="true" Message="'Do you want to confirm delete the employee,all data will be lost?" />
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:Button>
<ext:GridPanel StyleSpec="margin-top:15px;" MinHeight="600" ID="GridLevels" runat="server"
    Cls="itemgrid" OnReadData="Store_ReadData">
    <Store>
        <ext:Store ID="storeEmpList" runat="server" AutoLoad="false" OnReadData="Store_ReadData"
            RemotePaging="true" RemoteSort="true">
            <Proxy>
                <ext:PageProxy />
            </Proxy>
            <Model>
                <ext:Model ID="Model4" runat="server" IDProperty="EmployeeId">
                    <Fields>
                        <ext:ModelField Name="EmployeeId" Type="String" />
                        <ext:ModelField Name="Name" Type="string" />
                        <ext:ModelField Name="Branch" Type="string" />
                        <ext:ModelField Name="Department" Type="string" />
                        <ext:ModelField Name="Level" Type="string" />
                        <ext:ModelField Name="Designation" Type="string" />
                        <ext:ModelField Name="StatusText" Type="string" />
                        <ext:ModelField Name="IdCardNo" Type="string" />
                        <ext:ModelField Name="IsRetiredOrResigned" Type="string" />
                        <ext:ModelField Name="UrlPhoto" Type="string" />
                    </Fields>
                </ext:Model>
            </Model>
            <Sorters>
                <ext:DataSorter Property="EmployeeId" Direction="ASC" />
            </Sorters>
        </ext:Store>
    </Store>
    <Listeners>
        <CellClick Fn="EmpImageFunc">
        </CellClick>
    </Listeners>
    <Callouts>
        <ext:Callout ID="Callout2" runat="server" Alignment="TopLeft" Trigger="Click" BodyStyle="padding:2px 0px;"
            Delegate=".x-grid-cell-first">
            <BodyWidget>
                <ext:Image Cls="media-object img-circle img-online" ID="EmpImage" Width="200" Height="200"
                    runat="server">
                </ext:Image>
            </BodyWidget>
        </ext:Callout>
    </Callouts>
    <ColumnModel>
        <Columns>
            <ext:Column ID="UrlPhoto" Sortable="false" MenuDisabled="true" runat="server" Text=""
                Align="Left" DataIndex="UrlPhoto" Width="50">
                <Renderer Fn="EmpPicRenderer" />
            </ext:Column>
            <ext:Column ID="Column5" Sortable="true" runat="server" Text="EIN" Align="Left" DataIndex="EmployeeId"
                Width="50" />
            <ext:Column ID="Column7" Sortable="true" runat="server" Text="I No" Align="Left"
                DataIndex="IdCardNo" Width="60">
            </ext:Column>
            <ext:TemplateColumn ID="TemplateColumn1" runat="server" Text="Name" Width="175" DataIndex="Name"
                TemplateString='<a href="EmployeeDetails.aspx?id={EmployeeId}"> {Name}</a>' />
            <ext:Column ID="Column1" Sortable="true" Wrap="true" runat="server" Text="Branch"
                Align="Left" DataIndex="Branch" Width="150">
            </ext:Column>
            <ext:Column ID="Column8" Sortable="true" Wrap="true" runat="server" Text="Department"
                Align="Left" DataIndex="Department" Width="150">
            </ext:Column>
            <ext:Column ID="Column2" Sortable="true" Wrap="true" runat="server" Text="Level/Position"
                Align="Left" DataIndex="Level" Width="150">
            </ext:Column>
            <ext:Column ID="Column3" Sortable="true" Wrap="true" runat="server" Text="Designation"
                Align="Left" DataIndex="Designation" Width="150">
            </ext:Column>
            <ext:Column ID="Column4" Sortable="true" runat="server" Text="Status" Align="Left"
                DataIndex="StatusText" Width="90">
            </ext:Column>
            <ext:Column ID="Column9" Sortable="false" runat="server" Text="Active" Align="Left"
                DataIndex="IsRetiredOrResigned" Width="50">
                <Renderer Fn="RetiredRenderer" />
            </ext:Column>
            <ext:CommandColumn ID="CommandColumn1" runat="server" Text="" Width="45">
                <Commands>
                    <ext:GridCommand Cls="editGridButton" Text="<i class='fa fa-pencil'></i>" CommandName="Edit" />
                </Commands>
                <Listeners>
                    <Command Handler="CommandHandler(command,record);" />
                </Listeners>
            </ext:CommandColumn>
            <ext:CommandColumn ID="CommandColumn2" runat="server" Text="" Width="45">
                <Commands>
                    <ext:GridCommand Cls="editGridButton" Text="<i class='fa fa-trash-o'></i>" CommandName="Delete" />
                </Commands>
                <Listeners>
                    <Command Handler="CommandHandler(command,record);" />
                </Listeners>
            </ext:CommandColumn>
        </Columns>
    </ColumnModel>
    <SelectionModel>
        <ext:CellSelectionModel ID="CellSelectionModel1" runat="server" Mode="Single" />
    </SelectionModel>
    <BottomBar>
        <%--<ext:PagingToolbar ID="PagingToolbar1" runat="server" PageSize="50" DisplayInfo="true"
                        DisplayMsg="Displaying employees {0} - {1} of {2}" EmptyMsg="No Employee to display">
                        <Items>
                            <ext:ComboBox runat="server" SelectedIndex="0" Width="80" ID="cmbPageSize">
                                <Items>
                                    <ext:ListItem Text="50" Value="5" />
                                    <ext:ListItem Text="75" Value="7" />
                                    <ext:ListItem Text="100" Value="10" />
                                </Items>
                                <Listeners>
                                    <Select Handler="#{PagingToolbar1}.pageSize = parseInt( this.getValue()); searchListEL();" />
                                </Listeners>
                            </ext:ComboBox>
                        </Items>
                    </ext:PagingToolbar>--%>
        <ext:PagingToolbar ID="PagingToolbar1" runat="server" StoreID="storeEmpList" DisplayInfo="true">
            <Items>
                <ext:Label ID="lblPageSize" runat="server" Text="Page size:" />
                <ext:ToolbarSpacer ID="ToolbarSpacer_listItems" runat="server" Width="10" />
                <ext:ComboBox ID="cmbPageSize" runat="server" Width="80" SelectOnFocus="true" Selectable="true"
                    ValueField="Value" DisplayField="Text" ForceSelection="true" AllowBlank="false">
                    <Listeners>
                        <Select Handler="searchListEL()" />
                        <%--#{cmbPageSize}.pageSize = this.getValue();#{PagingToolbar1}.moveFirst();--%>
                    </Listeners>
                    <Items>
                        <ext:ListItem Value="50" Text="50" />
                        <ext:ListItem Value="100" Text="100" />
                        <ext:ListItem Value="200" Text="200" />
                    </Items>
                    <SelectedItems>
                        <ext:ListItem Index="0">
                        </ext:ListItem>
                    </SelectedItems>
                </ext:ComboBox>
            </Items>
        </ext:PagingToolbar>
    </BottomBar>
</ext:GridPanel>
<div class="alert alert-info" style="margin-top: 15px;">
    <table>
        <tr>
            <td>
                <asp:LinkButton ID="btnExportPopup" runat="server" Text="Import Employee" OnClientClick="employeePopup();return false;"
                    CssClass="excel marginRight tiptip" Style="float: left;" />
            </td>
            <td style="padding-left: 20px">
                <asp:LinkButton ID="btnExportImportAccounts" runat="server" Text="Import Accounts"
                    OnClientClick="otherPopup();return false;" />
            </td>
            <td style="padding-left: 20px">
                <asp:LinkButton ID="btnExport" runat="server" Text="Export to Excel" OnClick="btnExport_Click"
                    CssClass="excel marginRight" />
            </td>
        </tr>
    </table>
</div>
