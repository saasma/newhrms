﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ActingListCtl.ascx.cs"
    Inherits="Web.NewHR.UserControls.ActingListCtl" %>
<script type="text/javascript">

      var CommandHandler = function(command, record){
            <%= hiddenValue.ClientID %>.setValue(record.data.ActingID);
                if(command=="Edit")
                {
                    window.location = 'Acting.aspx?ActingID=' + record.data.ActingID;
                }
                else
                {
                    <%= btnDeleteLevel.ClientID %>.fireEvent('click');
                }

             }
        var dependentRenderer = function(value)
        {
            if(value=="true")
                return "Yes";
            return "No";
        }
        
</script>
<ext:Hidden runat="server" ID="hiddenValue" />
<ext:LinkButton runat="server" Hidden="true" ID="btnEditLevel" runat="server">
    <DirectEvents>
        <Click OnEvent="btnEditLevel_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:LinkButton>
<ext:Button ID="btnDeleteLevel" runat="server" Hidden="true">
    <DirectEvents>
        <Click OnEvent="btnDeleteLevel_Click">
            <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the Acting?" />
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:Button>
<table class="attribute" runat="server" id="tblFilter">
    <tr>
        <td style="display: none">
            <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                <Proxy>
                    <ext:AjaxProxy Url="../../Handler/EmpSearch.ashx">
                        <ActionMethods Read="GET" />
                        <Reader>
                            <ext:JsonReader Root="plants" TotalProperty="total" />
                        </Reader>
                    </ext:AjaxProxy>
                </Proxy>
                <Model>
                    <ext:Model ID="Model2" IDProperty="EmployeeId" Name="ItemLineModel" runat="server">
                        <Fields>
                            <ext:ModelField Name="Name" Type="String" />
                            <ext:ModelField Name="EmployeeId" Type="String" />
                        </Fields>
                    </ext:Model>
                </Model>
            </ext:Store>
            <ext:ComboBox LabelSeparator="" ID="cmbSearch" FieldLabel="Search Employee" LabelWidth="70"
                LabelAlign="Top" runat="server" DisplayField="Name" ValueField="EmployeeId" StoreID="storeSearch"
                TypeAhead="false" Width="180" PageSize="9999" HideBaseTrigger="true" MinChars="1"
                TriggerAction="All" ForceSelection="true">
                <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                    <ItemTpl ID="ItemTpl1" runat="server">
                        <Html>
                            <tpl>
                                            <div class="search-item">
                                                            <span>{Name}</span>  
                                             </div>
					                </tpl>
                        </Html>
                    </ItemTpl>
                </ListConfig>
                <Triggers>
                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                </Triggers>
                <Listeners>
                    <Select Handler="this.getTrigger(0).show();" />
                    <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                    <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                </Listeners>
            </ext:ComboBox>
        </td>
        <td style="padding-left: 10px;">
            <pr:CalendarExtControl Width="150px" FieldLabel="From Date" ID="calFromDate" runat="server"
                LabelAlign="Top" LabelSeparator="" />
        </td>
        <td style="padding-left: 10px;">
            <pr:CalendarExtControl Width="150px" FieldLabel="To Date" ID="calToDate" runat="server"
                LabelAlign="Top" LabelSeparator="" />
        </td>
        <td style="padding-left: 10px;">
            <ext:Button runat="server" ID="btnLoad" Cls="btn btn-primary" Width="120" StyleSpec="margin-top:18px"
                Height="30" Text="<i></i>Load">
                <DirectEvents>
                    <Click OnEvent="btnLoad_Click">
                        <EventMask ShowMask="true" />
                    </Click>
                </DirectEvents>
            </ext:Button>
        </td>
        <td style="padding-left: 10px;">
            <ext:Button runat="server" ID="btnAddLevel" Cls="btn btn-success" Width="120" StyleSpec="margin-top:18px"
                Height="30" Text="<i></i>Add Acting">
                <Listeners>
                    <Click Handler="window.location='Acting.aspx';" />
                </Listeners>
            </ext:Button>
        </td>
       <td style="padding-left: 10px;">
         <ext:Button runat="server" AutoPostBack="true" OnClick="btnExport_Click" StyleSpec="margin-top:18px" ID="btnExport"
                    Text="<i></i>Export To Excel">
                </ext:Button>
        </td>
    </tr>
</table>
<ext:GridPanel StyleSpec="margin-top:15px;" ID="GridLevels" runat="server" Cls="itemgrid">
    <Store>
        <ext:Store ID="Store3" runat="server">
            <Model>
                <ext:Model ID="Model4" runat="server" IDProperty="ActingID">
                    <Fields>
                        <ext:ModelField Name="ActingID" Type="String" />
                        <ext:ModelField Name="EmployeeId" Type="String" />
                        <ext:ModelField Name="INo" Type="String" />
                        <ext:ModelField Name="EmployeeName" Type="string" />
                        <ext:ModelField Name="LetterNumber" Type="string" />
                        <ext:ModelField Name="LetterDate" Type="string" />
                        <ext:ModelField Name="ApplicableFrom" Type="string" />
                        <ext:ModelField Name="ApplicableTill" Type="string" />
                        <ext:ModelField Name="FromLevel" Type="string" />
                        <ext:ModelField Name="ToLevel" Type="string" />
                        <ext:ModelField Name="Amount" Type="string" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
    </Store>
    <ColumnModel>
        <Columns>
            <ext:Column ID="Column9" Sortable="false" MenuDisabled="true" runat="server" Text="EIN"
                Align="Left" Width="50" DataIndex="EmployeeId" />
            <ext:Column ID="Column10" Sortable="false" MenuDisabled="true" runat="server" Text="I No"
                Align="Left" Width="50" DataIndex="INo" />
            <ext:Column ID="Column8" Sortable="false" MenuDisabled="true" runat="server" Text="Employee"
                Align="Left" Width="180" DataIndex="EmployeeName" />
            <ext:Column ID="Column5" Sortable="false" MenuDisabled="true" runat="server" Text="Letter No"
                Align="Left" Width="100" DataIndex="LetterNumber" />
            <ext:Column ID="Column6" Width="100" Sortable="false" MenuDisabled="true" runat="server"
                Text="LetterDate" Align="Left" DataIndex="LetterDate">
            </ext:Column>
            <ext:Column ID="Column2" Width="120" Sortable="false" MenuDisabled="true" runat="server"
                Text="Applicable From" Align="Left" DataIndex="ApplicableFrom">
            </ext:Column>
            <ext:Column ID="Column3" Width="120" Sortable="false" MenuDisabled="true" runat="server"
                Text="Applicable Till" Align="Left" DataIndex="ApplicableTill">
            </ext:Column>
            <ext:Column ID="Column1" Width="130" Sortable="false" MenuDisabled="true" runat="server"
                Text="From Level" Align="Left" DataIndex="FromLevel">
            </ext:Column>
            <ext:Column ID="Column4" Width="130" Sortable="false" MenuDisabled="true" runat="server"
                Text="To Level" Align="Left" DataIndex="ToLevel">
            </ext:Column>
            <ext:Column ID="Column7" Width="100" Hidden="true" Sortable="false" MenuDisabled="true"
                runat="server" Text="Amount" Align="Left" DataIndex="Amount">
                <Renderer Fn="getFormattedAmount" />
            </ext:Column>
            <ext:CommandColumn ID="CommandColumn1" runat="server" Width="75" Text="Actions" Align="Center">
                <Commands>
                    <ext:CommandSeparator />
                    <ext:GridCommand Cls="editGridButton" ToolTip-Text="Edit" Text="<i class='fa fa-pencil'></i>"
                        CommandName="Edit" />
                    <ext:GridCommand Cls="editGridButton" ToolTip-Text="Delete" Text="<i class='fa fa-trash-o'></i>"
                        CommandName="Delete" />
                </Commands>
                <Listeners>
                    <Command Handler="CommandHandler(command,record);" />
                </Listeners>
            </ext:CommandColumn>
        </Columns>
    </ColumnModel>
    <SelectionModel>
        <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
    </SelectionModel>
</ext:GridPanel>
