﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using Utils.Web;
using Utils.Helper;
using Bll;

namespace Web.NewHR.UserControls
{
    public partial class AdditionalBasicListCtl : BaseUserControl
    {
        



       


        protected void Page_Load(object sender, EventArgs e)
        {
            lblMsg.Text = "";
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }

            JavascriptHelper.AttachNonDialogPopUpCode(Page, "employeePopup", "../ExcelWindow/AdditionalBasicExcel.aspx", 450, 500);
        }

       

        public void Initialise()
        {
            StoreLevel.DataSource = NewPayrollManager.GetAllParentLevels();
            StoreLevel.DataBind();


            StoreBranch.DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            storeSearch.DataBind();

            StoreIncome.DataSource = PayManager.GetFixedIncomeListByCompany(SessionManager.CurrentCompanyId);
            StoreIncome.DataBind();

        }

        //public GradeRewardEmployee Process(GradeRewardEmployee entity)
        //{
        //    if (entity == null)
        //    {
        //        entity = new GradeRewardEmployee();
        //        if (string.IsNullOrEmpty(hdn.Text))
        //        {
        //            if (isEmployee == false)
        //                entity.EmployeeId = int.Parse(cmbGradeEmployee.SelectedItem.Value);
        //            else
        //                entity.EmployeeId = GetEmployeeId();
        //        }
        //        else
        //            entity.EmployeeId = NewPayrollManager.GetGradeReward(int.Parse(hdn.Text)).EmployeeId;

        //        entity.Notes = txtNotes.Text.Trim();
        //        entity.LevelId = NewHRManager.GetEmployeeCurrentLevel(entity.EmployeeId).LevelId;
        //        entity.RewardDate = calDate.Text.Trim();
        //        entity.RewardDateEng = GetEngDate(entity.RewardDate);
        //        entity.RewardPoints = txtPoints.Number;
        //        entity.RewardRate = Convert.ToDecimal(txtRate.Text);
        //        entity.Amount = (decimal)entity.RewardPoints * entity.RewardRate;
        //        entity.IsActive = true;
        //        return entity;
        //    }
        //    else
        //    {
        //        EEmployee emp = new EmployeeManager().GetById(entity.EmployeeId);

        //        storeSearch.DataSource = new List<EEmployee> { new EEmployee{ 
        //                EmployeeId = emp.EmployeeId,
        //        Name = emp.Name} 
                
        //        }
        //        ;
        //        storeSearch.DataBind();


        //        cmbGradeEmployee.Items.Add(new Ext.Net.ListItem { Value = emp.EmployeeId.ToString(), Text = emp.Name });
        //        cmbGradeEmployee.SelectedItems.Add(new Ext.Net.ListItem { Index = 0 });
        //        calDate.Text = entity.RewardDate;
        //        cmbGradeEmployee.SetValue(emp.EmployeeId.ToString());
        //        cmbGradeEmployee.SetRawValue(emp.Name.ToString());
        //        dispLevel.Text = NewPayrollManager.GetLevelById(entity.LevelId.Value).Name;
        //        txtPoints.Number = entity.RewardPoints.Value;
        //        txtRate.Text = GetCurrency(entity.RewardRate);
        //        txtNotes.Text = entity.Notes;

        //        cmbGradeEmployee.Disable(true);
        //    }
        //    return null;
        //}
        //public GradeAdditionlBasic ProcessAdditionalBasic(GradeAdditionlBasic entity)
        //{
        //    if (entity == null)
        //    {
        //        entity = new GradeAdditionlBasic();
        //        if (string.IsNullOrEmpty(hdn.Text))
        //        {

        //            entity.EmployeeId = GetEmployeeId();
        //        }
        //        else
        //            entity.EmployeeId = NewPayrollManager.GetAdditionalBasic(int.Parse(hdn.Text)).EmployeeId;

        //        entity.Notes = txtNotes.Text.Trim();
        //        entity.LevelId = NewHRManager.GetEmployeeCurrentLevel(entity.EmployeeId).LevelId;

        //        entity.Amount = decimal.Parse(txtAdditionalBasicAmount.Text.Trim());
        //        return entity;
        //    }
        //    else
        //    {
                



        //        dispAdditionalBasicLevel.Text = NewPayrollManager.GetLevelById(entity.LevelId.Value).Name;
              

        //        txtAdditionalBasicAmount.Text = GetCurrency(entity.Amount);
        //        txtNotesAddiotionalBasic.Text = entity.Notes;

        //    }
        //    return null;
        //}
        protected void btnDeleteLevel_Click(object sender, DirectEventArgs e)
        {
            int levelId = int.Parse(hdn.Text.Trim());
            Status status = NewPayrollManager.DeleteGradeReward(levelId);
            if (status.IsSuccess)
            {
                X.Js.AddScript("searchList();");
                NewMessage.ShowNormalMessage("Reward deleted.");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }

        }


        public void btnExportDetails_Click(object sender, EventArgs e)
        {
            int incomeId = int.Parse(cmbIncome.SelectedItem.Value);


            List<GetListForAdditionaBasicListResult> list =
                BLL.BaseBiz.PayrollDataContext.GetListForAdditionaBasicList(incomeId).ToList();



            Dictionary<string, string> title = new Dictionary<string, string>();
            title["Additional Adjustment Income List"] = "";



            ExcelHelper.ExportToExcel(
                           "Additional List",
                           list,
                           new List<string> { }, new List<string> { },
                           new Dictionary<string, string> { { "CurrentDesignation", "Designation" }, { "CurrentLevel", "Level" }
                           ,{"DefaultAmount","Default Amount"},{"AdditionAmount","Addition Amount"}}
                           , new List<string> { "DefaultAmount", "AdditionAmount" }, title
                           );

        }


        protected void btnEditLevel_Click(object sender, DirectEventArgs e)
        {
            int levelId = int.Parse(hdn.Text.Trim());
            GradeRewardEmployee entity = NewPayrollManager.GetGradeReward(levelId);
            //Process(entity);
            window.Center();
            window.Show();



            //cmbGroup.SetValue(entity.LevelGroupId.ToString());
            //txtLevelCode.Text = entity.Code;
            //txtLevelName.Text = entity.Name;
            //txtLevelOrder.Number = entity.Order;

        }

        protected void btnEditAdditionalBasic_Click(object sender, DirectEventArgs e)
        {
            int levelId = int.Parse(hdn.Text.Trim());
            GradeAdditionlBasic entity = NewPayrollManager.GetAdditionalBasic(levelId);
            //ProcessAdditionalBasic(entity);
            //windowAdditionalDate.Show();



            //cmbGroup.SetValue(entity.LevelGroupId.ToString());
            //txtLevelCode.Text = entity.Code;
            //txtLevelName.Text = entity.Name;
            //txtLevelOrder.Number = entity.Order;

        }
        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("InsertUpdate");
            if (Page.IsValid)
            {


                GradeRewardEmployee entity = null;// Process(null);
                bool isInsert = string.IsNullOrEmpty(hdn.Text);
                if (isInsert == false)
                    entity.RewardID = int.Parse(hdn.Text);
                Status status = NewPayrollManager.InsertUpdateGradeReward(entity, isInsert);
                if (status.IsSuccess)
                {
                    X.Js.AddScript("searchList();");
                    window.Hide();
                    
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }
            }

        }
        public GradeAdditionlBasic ProcessAdditionalBasic(GradeAdditionlBasic entity)
        {
            if (entity == null)
            {
                entity = new GradeAdditionlBasic();
                if (string.IsNullOrEmpty(hdn.Text))
                {

                    entity.EmployeeId = int.Parse(cmbGradeEmployee.SelectedItem.Value);
                }
                else
                    entity.EmployeeId = NewPayrollManager.GetAdditionalBasic(int.Parse(hdn.Text)).EmployeeId;

                entity.Notes = txtNotes.Text.Trim();
                entity.LevelId = NewHRManager.GetEmployeeLastestLevel(entity.EmployeeId).LevelId;

                entity.Amount = decimal.Parse(txtRate.Text.Trim());
                return entity;
            }
            else
            {




                dispLevel.Text = NewPayrollManager.GetLevelById(entity.LevelId.Value).Name;


                txtRate.Text = GetCurrency(entity.Amount);
                txtNotes.Text = entity.Notes;

            }
            return null;
        }
        protected void btnSaveAdditionalBasic_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveUpdate");
            if (Page.IsValid)
            {

                GradeAdditionlBasic entity = ProcessAdditionalBasic(null);
                bool isInsert = string.IsNullOrEmpty(hdn.Text);
                if (isInsert == false)
                    entity.GradeAdditionalBasicID = int.Parse(hdn.Text);
                Status status = NewPayrollManager.InsertUpdateAdditionalBasic(entity, isInsert);
                if (status.IsSuccess)
                {
                    X.AddScript("refreshWindow();");

                    window.Hide();
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }
            }

        }
        //public void cmbGradeEmployee_Select(object sender, DirectEventArgs e)
        //{

        //    int employeeId = int.Parse(cmbGradeEmployee.SelectedItem.Value);


        //    EEmployee emp = EmployeeManager.GetEmployeeById(employeeId);
        //    BLevel currentLevel = NewHRManager.GetEmployeeCurrentLevel(emp.EmployeeId);
        //    if (currentLevel == null)
        //    {
        //        NewMessage.ShowWarningMessage("Reward can be given to level assigned employee only.");
        //        return;
        //    }
        //    BLevelDate date = NewPayrollManager.GetLastLevelDate();
        //    if (date == null)
        //    {
        //        NewMessage.ShowWarningMessage("Salary structure not defined.");
        //        return;
        //    }

        //    BLevelRate rate = NewPayrollManager.GetLevelRate(currentLevel.LevelId, date.DateID);

        //    if (rate != null)
        //        txtRate.Text = GetCurrency(rate.StepGradeRate);
        //    else
        //        txtRate.Text = "";

        //    dispLevel.Text = currentLevel.Name;
        //}

        public void ClearFields()
        {
            hdn.Text = "";
           
            txtRate.Clear();
            txtNotes.Clear();

        }

        public void ClearFieldsAdditionalBasic()
        {
            hdn.Text = "";
           
        }
        public void btnAddAdditionalBasic_Click(object sender, DirectEventArgs e)
        {

            ClearFieldsAdditionalBasic();
           // windowAdditionalDate.Show();
        }

        public void btnAdd_Click(object sender, DirectEventArgs e)
        {

            ClearFields();
            window.Show();
        }

    }
}