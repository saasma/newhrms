﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCitizenshipCtl.ascx.cs"
    Inherits="Web.NewHR.UserControls.UCitizenshipCtl" %>
<%@ Register Src="~/NewHR/UserControls/UCitizenshipPopupCtrl.ascx" TagName="Ctnship"
    TagPrefix="ucCtzn" %>

<ext:Hidden runat="server" ID="hdnCitizenshipID">
</ext:Hidden>
<ext:Hidden runat="server" ID="hdnIsEmployee" />
<ext:LinkButton ID="btnLoadCtznGrid" runat="server" Hidden="true">
    <DirectEvents>
        <Click OnEvent="btnLoadCtznGrid_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:LinkButton>
<ext:GridPanel StyleSpec="margin-top:15px;" Scroll="None" ID="GridCitizenship" runat="server"
    Width="1000" Cls="itemgrid">
    <Store>
        <ext:Store ID="StoreCitizenship" runat="server">
            <Model>
                <ext:Model ID="Model1" runat="server" IDProperty="CitizenshipId">
                    <Fields>
                        <ext:ModelField Name="Nationality" Type="string" />
                        <ext:ModelField Name="CitizenshipNo" Type="String" />
                        <ext:ModelField Name="IssueDate" Type="string" />
                        <ext:ModelField Name="Place" Type="string" />
                        <ext:ModelField Name="Status" Type="Int" />
                        <ext:ModelField Name="ServerFileName" Type="String" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
    </Store>
    <ColumnModel>
        <Columns>
            <ext:Column ID="Column1" runat="server" Text="Nationality" DataIndex="Nationality"
                Wrap="true" Width="150" Flex="1" />
            <ext:Column ID="Column2" runat="server" Text="Citizenship Number" DataIndex="CitizenshipNo"
                Wrap="true" Width="150">
            </ext:Column>
            <ext:Column ID="Column3" runat="server" Text="Issue Date" DataIndex="IssueDate" Width="100"
                Wrap="true">
            </ext:Column>
            <ext:Column ID="Column4" runat="server" Text="Place" DataIndex="Place" Width="200"
                Wrap="true">
            </ext:Column>
            <ext:CommandColumn ID="CommandColumn1" runat="server" Width="40">
                <Commands>
                    <ext:CommandSeparator />
                    <ext:GridCommand Icon="PageWhitePut" CommandName="DownLoad">
                        <ToolTip Text="DownLoad" />
                    </ext:GridCommand>
                </Commands>
                <DirectEvents>
                    <Command OnEvent="GridCitizenship_Command">
                        <ExtraParams>
                            <ext:Parameter Name="ID" Value="record.data.CitizenshipId" Mode="Raw">
                            </ext:Parameter>
                            <ext:Parameter Name="command" Value="command" Mode="Raw">
                            </ext:Parameter>
                        </ExtraParams>
                    </Command>
                </DirectEvents>
                <PrepareToolbar Fn="prepareDownloadCtzn" />
            </ext:CommandColumn>
            <ext:CommandColumn ID="CommandColumn3" runat="server" Width="40">
                <Commands>
                    <ext:CommandSeparator />
                    <ext:GridCommand Cls="editGridButton" ToolTip-Text="Edit" Text="<i class='fa fa-pencil'></i>"
                        CommandName="Edit" />
                </Commands>
                <DirectEvents>
                    <Command OnEvent="GridCitizenship_Command">
                        <ExtraParams>
                            <ext:Parameter Name="ID" Value="record.data.CitizenshipId" Mode="Raw">
                            </ext:Parameter>
                            <ext:Parameter Name="command" Value="command" Mode="Raw">
                            </ext:Parameter>
                        </ExtraParams>
                    </Command>
                </DirectEvents>
                <PrepareToolbar Fn="prepareCitizenship" />
            </ext:CommandColumn>
            <ext:CommandColumn ID="CommandColumn2" runat="server" Width="40">
                <Commands>
                    <ext:CommandSeparator />
                    <ext:GridCommand Cls="editGridButton" ToolTip-Text="Delete" Text="<i class='fa fa-trash-o'></i>"
                        CommandName="Delete" />
                </Commands>
                <DirectEvents>
                    <Command OnEvent="GridCitizenship_Command">
                        <ExtraParams>
                            <ext:Parameter Name="ID" Value="record.data.CitizenshipId" Mode="Raw">
                            </ext:Parameter>
                            <ext:Parameter Name="command" Value="command" Mode="Raw">
                            </ext:Parameter>
                        </ExtraParams>
                        <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the record?" />
                    </Command>
                </DirectEvents>
                <PrepareToolbar Fn="prepareCitizenship" />
            </ext:CommandColumn>
            <ext:Column ID="Column5" runat="server" Text="" Width="280">
            </ext:Column>
        </Columns>
    </ColumnModel>
    <SelectionModel>
        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
    </SelectionModel>
</ext:GridPanel>
<div class="buttonBlockSection">
    <ext:Button runat="server" ID="LinkButton1" Cls="btn btn-primary btn-sect" Width="120"
        StyleSpec="margin-top:10px" Height="30" Text="<i></i>Add New Line">
        <DirectEvents>
            <Click OnEvent="btnAddNewLine_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>
</div>
<ext:Window ID="AECitizenshipWindow" runat="server" Title="Citizenship Details" Icon="Application"
    Height="390" Width="550" BodyPadding="5" Hidden="true" Modal="true">
    <Content>
        <ucCtzn:Ctnship Id="ucCtznCtrl" runat="server" />
    </Content>
</ext:Window>
<script type="text/javascript">

    var prepareCitizenship = function (grid, toolbar, rowIndex, record) {
        var delBtn = toolbar.items.get(1);
        var IsEmployee = <%= hdnIsEmployee.ClientID %>.getValue();

        if (record.data.Status == 1 && IsEmployee == "1" ) {
            delBtn.setVisible(false);
        }

    } 

    var prepareDownloadCtzn = function(grid, toolbar, rowIndex, record){
        var downloadBtn = toolbar.items.get(1);
        if(record.data.ServerFileName == null || record.data.ServerFileName == ''){
            downloadBtn.setVisible(false);     
        }
    }

    function reloadCtznGrid() {  
        <%= btnLoadCtznGrid.ClientID %>.fireEvent('click');
    }

</script>
