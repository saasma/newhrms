﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployeeSummaryCtl.ascx.cs"
    Inherits="Web.NewHR.UserControls.EmployeeSummaryCtl" %>
<script type="text/javascript">

    function showDetails(eventSourceID,sn) {
        <%=hdnType.ClientID %>.setValue(sn);
        <%=hdnSourceID.ClientID %>.setValue(eventSourceID);

        <%=btn.ClientID %>.fireEvent('click');
    }
    function showDetailsNew(eventSourceID,sn) {
        <%=hdnType.ClientID %>.setValue(sn);
        <%=hdnSourceID.ClientID %>.setValue(eventSourceID);

        <%=btnNew.ClientID %>.fireEvent('click');
    }
	  function showDetailsOld(ein, type, sourceId) {
        <%=hdnEIN.ClientID %>.setValue(ein);
        <%=hdnType.ClientID %>.setValue(type);
        <%=hdnSourceID.ClientID %>.setValue(sourceId);

        <%=btn.ClientID %>.fireEvent('click');
    }

</script>
<ext:Hidden runat="server" ID="hdnEIN" />
<ext:Hidden runat="server" ID="hdnType" />
<ext:Hidden runat="server" ID="hdnSourceID" />
<ext:Button runat="server" Hidden="true" ID="btn">
    <DirectEvents>
        <Click OnEvent="btn_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:Button>
<ext:Button runat="server" Hidden="true" ID="btnNew">
    <DirectEvents>
        <Click OnEvent="btnNew_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:Button>
<ext:Window ID="window" runat="server" Title="Event Details" Icon="Application" Height="550"
    Width="600" BodyPadding="5" Hidden="true" Modal="false">
    <Content>
        <table class="fieldTable">
            <tr>
                <td>
                    <ext:DisplayField ID="eventName" LabelWidth="100" LabelStyle="font-weight:bold;text-align:right"
                        runat="server" LabelAlign="Left" FieldLabel="Event" />
                </td>
                <td>
                    <ext:DisplayField ID="branch" LabelWidth="100" LabelStyle="font-weight:bold;text-align:right"
                        runat="server" LabelAlign="Left" FieldLabel="Branch" />
                </td>
            </tr>
            <tr>
                <td>
                    <ext:DisplayField ID="department" LabelWidth="100" LabelStyle="font-weight:bold;text-align:right"
                        runat="server" LabelAlign="Left" FieldLabel="Department" />
                </td>
                <td>
                    <ext:DisplayField ID="dispEffectiveDate" LabelWidth="100" LabelStyle="font-weight:bold;text-align:right"
                        runat="server" LabelAlign="Left" FieldLabel="Effective Nep Date" />
                </td>
            </tr>
            <tr>
                <td>
                    <ext:DisplayField ID="dispEffectiveDateEng" LabelWidth="100" LabelStyle="font-weight:bold;text-align:right"
                        runat="server" LabelAlign="Left" FieldLabel="Effective Date" />
                </td>
                <td>
                    <ext:DisplayField ID="dispLetterDateEng" LabelWidth="100" LabelStyle="font-weight:bold;text-align:right"
                        runat="server" LabelAlign="Left" FieldLabel="Letter Date" />
                </td>
            </tr>
            <tr>
                <td>
                    <ext:DisplayField ID="dispLevel" LabelWidth="100" LabelStyle="font-weight:bold;text-align:right"
                        runat="server" LabelAlign="Left" FieldLabel="Position" />
                </td>
                <td>
                    <ext:DisplayField ID="dispDesignation" LabelWidth="100" LabelStyle="font-weight:bold;text-align:right"
                        runat="server" LabelAlign="Left" FieldLabel="Designation" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <ext:DisplayField ID="dispNotes" LabelStyle="font-weight:bold" runat="server" LabelAlign="Top"
                        FieldLabel="Note" />
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <ext:GridPanel Title="Salary" Region="Center" ID="Grid" runat="server" Cls="itemgrid">
                        <Store>
                            <ext:Store ID="storeEmpList" runat="server" AutoLoad="true">
                                <Model>
                                    <ext:Model ID="Model4" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Text" Type="String" />
                                            <ext:ModelField Name="Value" Type="string" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:Column ID="Column7" Sortable="false" runat="server" Text="Income" Align="Left"
                                    DataIndex="Text" Width="180" StyleSpec="font-weight:bold">
                                </ext:Column>
                                <ext:Column ID="Column1" Sortable="false" runat="server" Text="Amount" Align="Right"
                                    DataIndex="Value" Width="150">
                                </ext:Column>
                            </Columns>
                        </ColumnModel>
                        <SelectionModel>
                            <ext:RowSelectionModel ID="CellSelectionModel1" runat="server" Mode="Single" />
                        </SelectionModel>
                        <View>
                        <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                    </ext:GridPanel>
                     <ext:LinkButton runat="server" Visible="false" StyleSpec="padding:0px;    margin-left: 304px;" ID="btnEditAmounts"
                            Cls="btnFlatLeftGap" Text="<i></i>Edit Amount">
                            <Listeners>
                                <Click Handler="#{windowAmounts}.show();">
                                </Click>
                            </Listeners>
                        </ext:LinkButton>
                </td>
            </tr>
            <tr>
                <td valign="bottom" colspan="2">
                    <div class="popupButtonDiv">
                        <ext:LinkButton runat="server" StyleSpec="padding:0px;margin-left:0px" ID="LinkButton2"
                            Cls="btnFlatLeftGap" Text="<i></i>Cancel">
                            <Listeners>
                                <Click Handler="#{window}.hide();">
                                </Click>
                            </Listeners>
                        </ext:LinkButton>
                    </div>
                </td>
            </tr>
        </table>
    </Content>
</ext:Window>

<ext:Window ID="windowAmounts" runat="server" Title="Amounts" Icon="Application" Height="250"
    Width="400" BodyPadding="5" Hidden="true" Modal="false">
    <Content>
        <table class="fieldTable">
            <tr>
                <td>
                    <ext:DisplayField ID="DisplayField1" LabelWidth="100" LabelStyle="font-weight:bold;text-align:right"
                        runat="server" LabelAlign="Left" FieldLabel="Basic Salary" />
                </td>
                <td>
                    <ext:TextField runat="server" ID="txtIncome1" />
                    <asp:CompareValidator Display="None" ID="RequiredFieldValidator22" runat="server"
                        ValidationGroup="Target" ControlToValidate="txtIncome1" ErrorMessage="Invalid basic amount."
                        Type="Currency" Operator="DataTypeCheck" />
                </td>
            </tr>
            <tr>
                <td>
                    <ext:DisplayField ID="DisplayField3" LabelWidth="100" LabelStyle="font-weight:bold;text-align:right"
                        runat="server" LabelAlign="Left" FieldLabel="Allowance" />
                </td>
                <td>
                    <ext:TextField runat="server" ID="txtIncome2" />
                     <asp:CompareValidator Display="None" ID="CompareValidator1" runat="server"
                        ValidationGroup="Target" ControlToValidate="txtIncome2" ErrorMessage="Invalid allowance amount."
                        Type="Currency" Operator="DataTypeCheck" />
                </td>
            </tr>
            <tr>
                <td>
                    <ext:DisplayField ID="DisplayField5" LabelWidth="100" LabelStyle="font-weight:bold;text-align:right"
                        runat="server" LabelAlign="Left" FieldLabel="Incentive Allowance" />
                </td>
                <td>
                    <ext:TextField runat="server" ID="txtIncome3" />
                      <asp:CompareValidator Display="None" ID="CompareValidator2" runat="server"
                        ValidationGroup="Target" ControlToValidate="txtIncome3" ErrorMessage="Invalid incentive allowance amount."
                        Type="Currency" Operator="DataTypeCheck" />
                </td>
            </tr>
             <tr>
                <td>
                    <ext:DisplayField ID="DisplayField2" LabelWidth="100" LabelStyle="font-weight:bold;text-align:right"
                        runat="server" LabelAlign="Left" FieldLabel="Stipend" />
                </td>
                <td>
                    <ext:TextField runat="server" ID="txtIncome4" />
                      <asp:CompareValidator Display="None" ID="CompareValidator3" runat="server"
                        ValidationGroup="Target" ControlToValidate="txtIncome4" ErrorMessage="Invalid stipend amount."
                        Type="Currency" Operator="DataTypeCheck" />
                </td>
            </tr>
            <tr>
                <td valign="bottom" colspan="2" style=''>
                    <div class="popupButtonDiv">
                        <ext:Button runat="server" ID="btnChangeHistoryAmount" Text="<i></i>Save" Width="100">
                            <Listeners>
                                <Click Handler="valGroup='Target';return CheckValidation();" />
                            </Listeners>
                            <DirectEvents>
                                <Click OnEvent="btnChangeHistoryAmount_Click">
                                    <Confirmation ConfirmRequest="true" Message="Confirm save the amounts?" />
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                    </div>
                </td>
            </tr>
        </table>
    </Content>
</ext:Window>