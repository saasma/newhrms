﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ViewDocumentCtrl.ascx.cs" Inherits="Web.NewHR.UserControls.ViewDocumentCtrl" %>

<ext:Hidden ID="hdnFileID" runat="server" />

<ext:LinkButton ID="btnDownload" runat="server" Hidden="true" AutoPostBack="true" OnClick="btnDownload_Click">
</ext:LinkButton>

<ext:Button ID="btnEmail" runat="server"  Hidden="true">
    <DirectEvents>
        <Click OnEvent="btnEmail_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:Button>

<ext:LinkButton ID="btnDeleteFile" runat="server" Hidden="true">
    <DirectEvents>
        <Click OnEvent="btnDeleteFile_Click">
            <ExtraParams>
                <ext:Parameter Name="itemList" Value="Ext.encode(#{gridFiles}.getRowsValues({selectedOnly : false}))"
                    Mode="Raw" />
            </ExtraParams>
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:LinkButton>

<div style="background-color:#f2f2f2;  height:40px; padding-left:20px; padding-top:10px; width:600px;">
    
    <table>
        <tr>
            <td style='width:520px'> <ext:DisplayField ID="dfTitle" Width="500" FieldStyle='color:#3C7DB5;font-size:20px;font-weight:bold;' runat="server" /></td>
            <td style='text-align:right'> <ext:HyperLink runat="server" ID="editLink" Text="Edit" Icon="ApplicationEdit"  Width="100" /></td>
        </tr>
    </table>
   
   
</div>
<br />
<table>
    <tr>
        <td class="td1Class">
            Name of the Party
        </td>
        <td class="td2Class" colspan="2">
            <span id="spanNameOfTheParty" runat="server" />
        </td>
    </tr>
    <tr>
        <td class="td1Class">
            Document ID
        </td>
        <td class="td2Class" colspan="2">
            <span id="spanID" runat="server" />
        </td>
    </tr>
    <tr>
        <td class="td1Class">
            Document Cateogry
        </td>
        <td class="td2Class" colspan="2">
            <span id="spanDocumentCategory" runat="server" />
        </td>
    </tr>
    <tr>
        <td class="td1Class">
            Document Type
        </td>
        <td class="td2Class" colspan="2">
            <span id="spanDocumentSubCategory" runat="server" />
        </td>
    </tr>
    <tr>
        <td class="td1Class">
            Document Date
        </td>
        <td class="td2Class">
            <span id="spanDocumentDate" runat="server" />
        </td>
        <td class="td2Class" style="font-style:italic;">
            <span id="spanDocumentDateDetails" runat="server" />
        </td>
    </tr>
    <tr>
        <td class="td1Class">
            Expiration Date
        </td>
        <td class="td2Class">
            <span id="spanExpirationDate" runat="server" />
        </td>
        <td class="td2Class"  style="font-style:italic;">
            <span id="spanExpirationDateDetails" runat="server" />
        </td>
    </tr>
    <tr>
        <td class="td1Class">
            Description
        </td>
        <td class="td2Class" colspan="2">
            <span id="spanDescription" runat="server" />
        </td>
    </tr>
    <tr style="display:none;">
        <td class="td1Class">
            Private
        </td>
        <td class="td2Class" colspan="2">
            <span id="spanPrivate" runat="server" />
        </td>
    </tr>
    <tr style="display:none;">
        <td class="td1Class">
            Assignees
        </td>
        <td class="td2Class" colspan="2">
            <span id="spanAssignees" runat="server" />
        </td>
    </tr>
    <tr>
        <td class="td1Class" style="padding-top:15px !important; vertical-align:top;">
            Attachments
        </td>
        <td class="td2Class" colspan="2" >
            <ext:GridPanel ID="gridFiles" runat="server" Width="320" MarginSpec="20 0 0 0"
                             MinHeight="120" Scroll="Vertical" Cls="itemgrid">
                            <Store>
                                <ext:Store ID="Store3" runat="server">
                                    <Model>
                                        <ext:Model ID="Model4" runat="server" IDProperty="ServerFileName">
                                            <Fields>
                                                <ext:ModelField Name="DocumentRef_ID" Type="String" />
                                                <ext:ModelField Name="FileID" Type="String" />
                                                <ext:ModelField Name="ServerFileName" Type="String" />
                                                <ext:ModelField Name="UserFileName" Type="String" />
                                                <ext:ModelField Name="FileLocation" Type="String" />
                                                <ext:ModelField Name="FileFormat" Type="String" />
                                                <ext:ModelField Name="Size" Type="String" />
                                                <ext:ModelField Name="FileType" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <ColumnModel>
                                <Columns>
                                    <ext:Column ID="Column6" Sortable="false" MenuDisabled="true" runat="server" Text="File Name"
                                        Align="Left" Width="200" DataIndex="UserFileName" />
                                    <ext:CommandColumn ID="CommandColumnDelete" runat="server" Width="40" Text="" Align="Center" >
                                        <Commands>
                                            <ext:CommandSeparator />
                                            <ext:GridCommand Cls="editGridButton" ToolTip-Text="Delete" Text="<i class='fa fa-trash-o'></i>"
                                                    CommandName="Delete" />
                                        </Commands>
                                        <Listeners>
                                            <Command Handler="CommandHandlerFile(command,record);" />
                                        </Listeners>
                                    </ext:CommandColumn>
                                    <ext:CommandColumn ID="CommandColumn3" runat="server" Width="40" Text="" Align="Center">
                                        <Commands>
                                            <ext:CommandSeparator />
                                            <ext:GridCommand Icon="PageWhitePut" CommandName="DownLoad">
                                                <ToolTip Text="DownLoad" />
                                            </ext:GridCommand>
                                        </Commands>
                                        <Listeners>
                                            <Command Handler="CommandHandlerFile(command,record);" />
                                        </Listeners>
                                    </ext:CommandColumn>
                                    <ext:CommandColumn ID="CommandColumnEmail" runat="server" Width="40" Text="" Align="Center">
                                        <Commands>
                                            <ext:CommandSeparator />
                                            <ext:GridCommand Icon="Email" CommandName="Email">
                                                <ToolTip Text="Email" />
                                            </ext:GridCommand>
                                        </Commands>
                                        <Listeners>
                                            <Command Handler="CommandHandlerFile(command,record);" />
                                        </Listeners>
                                    </ext:CommandColumn>
                                </Columns>
                            </ColumnModel>
                            <SelectionModel>
                                <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                            </SelectionModel>
                        </ext:GridPanel>
        </td>
    </tr>

     <tr runat="server" id="trAddInformation" visible="false">
        <td class="td1Class" style="padding-top:15px !important; vertical-align:top;">
            Additional Information
        </td>
        <td class="td2Class" colspan="2">
            <ext:GridPanel ID="gridAdditionalInformation" runat="server" Width="400" MarginSpec="20 0 0 0"
                    Scroll="None" Cls="itemgrid" HideHeaders="true" >
                <Store>
                    <ext:Store ID="Store5" runat="server">
                        <Model>
                            <ext:Model ID="Model7" runat="server" IDProperty="AdditionalInformationID">
                                <Fields>
                                    <ext:ModelField Name="AdditionalInformationID" Type="String" />
                                    <ext:ModelField Name="Name" Type="String" />
                                    <ext:ModelField Name="Value" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:Column ID="Column4" Sortable="false" MenuDisabled="true" runat="server" Text=""
                            Align="Left" Width="240" DataIndex="Name" />
                        <ext:Column ID="Column5" Sortable="false" MenuDisabled="true" runat="server" Text=""
                            Align="Right" Width="160" DataIndex="Value">
                        </ext:Column>                                    
                    </Columns>
                </ColumnModel>
                <SelectionModel>
                    <ext:RowSelectionModel ID="RowSelectionModel3" runat="server" Mode="Single" />
                </SelectionModel>
            </ext:GridPanel>
        </td>
    </tr>
</table>   

<br />
<h4>Document History</h4>
<ext:GridPanel ID="gridDocumentHistory" runat="server" Width="600" MarginSpec="0 0 0 0"
    Scroll="None" Cls="itemgrid" >
    <Store>
        <ext:Store ID="Store4" runat="server">
            <Model>
                <ext:Model ID="Model6" runat="server" IDProperty="HistoryID">
                    <Fields>
                        <ext:ModelField Name="Action" Type="String" />
                        <ext:ModelField Name="ActionDate" Type="Date" />
                        <ext:ModelField Name="UserName" Type="String" />
                        <ext:ModelField Name="Details" Type="String" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
    </Store>
    <ColumnModel>
        <Columns>
            <ext:Column ID="Column1" Sortable="false" MenuDisabled="true" runat="server" Text="Action"
                Align="Left" Width="100" DataIndex="Action" />
            <ext:DateColumn ID="DateColumn4" runat="server" Align="Left" Text="Date" Width="100"
                MenuDisabled="true" Sortable="false" Format="MM/dd/yyyy" DataIndex="ActionDate">
            </ext:DateColumn>
            <ext:Column ID="Column2" Sortable="false" MenuDisabled="true" runat="server" Text="User"
                Align="Left" Width="150" DataIndex="UserName" />
            <ext:Column ID="Column3" Sortable="false" MenuDisabled="true" runat="server" Text="Details"
                Align="Left" Width="250" DataIndex="Details" />
        </Columns>
    </ColumnModel>
    <SelectionModel>
        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
    </SelectionModel>
</ext:GridPanel>


 <ext:Window ID="windowSendEmail" runat="server" AutoRender="False" Collapsible="false"
        Height="600" Width="460" Modal="True" BodyPadding="10" Resizable="false" Title="Send Email"
        Hidden="True" ButtonAlign="Left">
        <Content>
            <ext:TextField ID="txtToEmail" StyleSpec="padding-top:10px" LabelWidth="70" LabelSeparator="" MarginSpec="10 0 0 0"
                runat="server" Width="410" FieldLabel="To" LabelAlign="Right" />
            <ext:TextField ID="txtSubject" runat="server" LabelWidth="70" LabelSeparator="" Width="410" MarginSpec="20 0 0 0"
                StyleSpec="padding-top:10px" FieldLabel="Subject" LabelAlign="Right" />
            <ext:TextField ID="txtCC" runat="server" LabelWidth="70" LabelSeparator="" Width="410" MarginSpec="20 0 0 0"
                StyleSpec="padding-top:10px" FieldLabel="CC" LabelAlign="Right" />
            <ext:TextArea ID="txtMessage" runat="server" LabelWidth="70" LabelSeparator="" Height="250" MarginSpec="20 0 0 0"
                Width="410" StyleSpec="padding-top:10px" FieldLabel="Message" LabelAlign="Right" />
            <ext:Checkbox ID="chkSendMeACopy" Width="410" BoxLabelAlign="After" runat="server" 
                Hidden="false"  BoxLabel="Send me ({0}) a copy" MarginSpec="20 0 0 83" />
            <ext:Checkbox ID="chkExcludeAttachment" Width="410" BoxLabelAlign="After" runat="server"
                 BoxLabel="Exclude Attachment" MarginSpec="20 0 0 83" Hidden="true" />
        </Content>
        <Buttons>
            <ext:Button ID="btnSendEmail" runat="server" Text="Send" Cls="bluesmall" MarginSpec="0 0 10 77">
                <Listeners>
                    <Click Handler="valGroup='sendinvoice';return CheckValidation();" />
                </Listeners>
                <DirectEvents>
                    <Click OnEvent="btnSendEmail_Click">
                        <EventMask ShowMask="true" />
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="Button1" runat="server" Text="Cancel" Cls="graysmall" MarginSpec="0 0 10 10">
                <Listeners>
                    <Click Handler="#{windowSendEmail}.hide();" />
                </Listeners>
            </ext:Button>
        </Buttons>
    </ext:Window>
 


<script type="text/javascript">

        var CommandHandlerFile = function(command, record){
            <%= hdnFileID.ClientID %>.setValue(record.data.FileID);
       
             if(command == "DownLoad")
            {
                <%= btnDownload.ClientID %>.fireEvent('click');
            }
            else if(command == "Email")
            {
                <%= btnEmail.ClientID %>.fireEvent('click');
            }
            else if(command == "Delete")
            {
                <%= btnDeleteFile.ClientID %>.fireEvent('click');
            }
        };

</script>

<style type="text/css">

    .td1Class
    {
        height:38px;
        background-color:#F2F2F2;
        padding-right:10px;
        padding-left:20px;
        text-align:right;
        vertical-align:bottom;
    }
    .td2Class
    {
        width:225px;
        color:#428BCA;
        border-bottom:2px solid #EEF6FB;
        padding-left:20px !important;
        vertical-align:bottom;
    }
</style>