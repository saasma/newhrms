﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;

using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;
using Utils.Helper;
using Utils.Security;
using Utils;
using Web.Helper;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

namespace Web.NewHR.UserControls
{
    public partial class PhotographCtl : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!X.IsAjaxRequest)
            {
                NewHRManager _NewHRManager = new NewHRManager();
                HHumanResource _HHumanResource = _NewHRManager.GetHumanResource(GetEmployeeID());
                if (_HHumanResource != null && !string.IsNullOrEmpty(_HHumanResource.UrlPhoto))
                {
                    string filename = "../Uploads/" + _HHumanResource.UrlPhoto;
                    ImgEmployee.ImageUrl = filename;

                    Image1.ImageUrl = filename;
                }

                if (_HHumanResource != null && !string.IsNullOrEmpty(_HHumanResource.UrlPhotoThumbnail))
                {
                    string filename = "../Uploads/" + _HHumanResource.UrlPhotoThumbnail;
                    ImgEmployeeThumbprint.ImageUrl = filename;
                }

                if (Session["Redirect"] != null && Session["Redirect"].ToString() == "1")
                {
                    Session["Redirect"] = "0";
                    X.Js.Call("myFunction");
                }
            }
        }

        bool IsAbsoluteUrl(string url)
        {
            Uri result;
            return Uri.TryCreate(url, UriKind.Absolute, out result);
        }

        protected void btnUpload_Click(object sender, DirectEventArgs e)
        {
            int employeeId = GetEmployeeID();
            if (fup.HasFile)
            {
                string ext = Path.GetExtension(fup.FileName);
                string guidFileName = Guid.NewGuid().ToString() + ext;
                string saveLocation = Path.Combine(Server.MapPath(Config.UploadLocation), guidFileName);
                string thumbnailLocation = Path.Combine(Server.MapPath(Config.UploadLocation), Guid.NewGuid().ToString() + ext);

                UploadType type = (UploadType)1;
                string code = "closeWindow(\"{0}\");";
                NewHRManager mgr = new NewHRManager();
                if (type != UploadType.Document)
                {
                    if (IsImage(ext) == false)
                    {
                        // JavascriptHelper.DisplayClientMsg("Invalid image type.", Page);
                        NewMessage.ShowWarningMessage("Invalid image type.");
                        return;
                    }
                    fup.PostedFile.SaveAs(saveLocation);
                    string filename = "";


                    try
                    {

                        WebHelper.CreateThumbnail(saveLocation, 200, 200, thumbnailLocation);
                        thumbnailLocation = Path.GetFileName(thumbnailLocation);
                        if (IsAbsoluteUrl(thumbnailLocation) || IsAbsoluteUrl(guidFileName))
                        {
                            NewMessage.ShowWarningMessage("Unable to upload. Try again...");
                            return;
                        }
                    }
                    catch { }

                    try
                    {


                        mgr.SaveOrUpdateImage(type, guidFileName, employeeId, thumbnailLocation);
                        filename = "../Uploads/" + guidFileName;
                        code = string.Format(code, filename);
                        // JavascriptHelper.DisplayClientMsg("Image added.", Page, code);

                        //NewMessage.ShowNormalMessage("Photo added.");

                        Session["Redirect"] = "1";

                        Response.Redirect("~/newhr/Identification.aspx?ID=" + GetEmployeeID().ToString());

                        //ImgEmployee.ImageUrl = filename;
                        //Image1.ImageUrl = filename;

                        //ImgEmployeeThumbprint.ImageUrl = "../Uploads/" + thumbnailLocation;



                    }
                    catch (Exception exp1)
                    {
                        JavascriptHelper.DisplayClientMsg(exp1.Message, Page);
                    }
                    //Page.ClientScript.RegisterClientScriptBlock(GetType(), "sdfdsf2333",
                    //   string.Format("opener.process('{0}');", filename), true);
                }
                else
                {
                    try
                    {
                        //

                        guidFileName = guidFileName.Replace(Path.GetExtension(guidFileName), ".ashx");
                        saveLocation = saveLocation.Replace(Path.GetExtension(saveLocation), ".ashx");



                        EncryptorDecryptor enc = new EncryptorDecryptor();
                        enc.Encrypt(fup.FileContent, saveLocation);

                        HDocument doc = new HDocument();
                        doc.EmployeeId = employeeId;
                        doc.Name = Path.GetFileName(fup.FileName);

                        //if (!string.IsNullOrEmpty(txtDesc.Text.Trim()))
                        //{
                        //    doc.Description = txtDesc.Text.Trim();
                        //}
                        //else
                        doc.Description = Path.GetFileNameWithoutExtension(fup.FileName);

                        doc.ContentType = fup.PostedFile.ContentType;
                        doc.Url = guidFileName;
                        doc.Size = GetSize(fup.PostedFile.ContentLength);

                        code = string.Format(code, mgr.GetHTMLDocuments(doc.EmployeeId.Value));
                        JavascriptHelper.DisplayClientMsg("Document added.", Page, code);
                    }
                    catch (Exception exp1)
                    {
                        JavascriptHelper.DisplayClientMsg(exp1.Message, Page);
                    }
                }
            }
        }

        protected int GetEmployeeID()
        {

            int EmpID = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                EmpID = int.Parse(Request.QueryString["ID"]);
            }
            return EmpID;
        }


        /// <summary>
        /// Checks if the file has the valid image extension or not
        /// </summary>
        /// <param name="pExtension">Extension type</param>
        /// <returns>Is valid image or not</returns>
        public static bool IsImage(string pExtension)
        {
            if ((pExtension.ToLower() == ".jpg" | pExtension.ToLower() == ".gif" | pExtension.ToLower() == ".bmp" | pExtension.ToLower() == ".jpeg" | pExtension.ToLower() == ".png"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// Helper method to convert Bytes in KiloBytes
        /// </summary>
        /// <param name="byteValue"></param>
        /// <returns></returns>
        public static int ConvertBytesIntoKB(int byteValue)
        {
            double mb = (byteValue / 1024);
            if (mb.ToString("#") == "")
                return 0;
            return Convert.ToInt32(mb.ToString("#"));
        }
        public static string ConvertKBIntoMB(object value)
        {
            long kb = Convert.ToInt64(value);
            double mb = (kb / 1024.0);
            return mb.ToString("#.#");
        }

        public string GetSize(int byteValue)
        {
            if (byteValue < 1024)
                return "1 kb";

            double kb = ConvertBytesIntoKB(byteValue);
            if (kb > 1024)
            {
                return ConvertKBIntoMB(kb) + " mb";
            }
            else
            {
                return kb + " kb";
            }
        }


        protected void btnCropImage_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(_xField.Value) || string.IsNullOrEmpty(_yField.Value) || string.IsNullOrEmpty(_widthField.Value) || string.IsNullOrEmpty(_heightField.Value))
                return;
            var x = int.Parse(_xField.Value);
            var y = int.Parse(_yField.Value);
            var width = int.Parse(_widthField.Value);
            var height = int.Parse(_heightField.Value);

            if (height == 0 || width == 0)
                return;

            string filename = "";
            string newFileName = "";

            NewHRManager _NewHRManager = new NewHRManager();

            string saveLocation = "";
            string guidFileName = "";
            string thumbnailLocation = "";

            HHumanResource _HHumanResource = _NewHRManager.GetHumanResource(GetEmployeeID());
            if (_HHumanResource != null && !string.IsNullOrEmpty(_HHumanResource.UrlPhoto))
            {
                filename = "../Uploads/" + _HHumanResource.UrlPhoto;


                string[] splFileName = _HHumanResource.UrlPhoto.Split('.');
                guidFileName = Guid.NewGuid().ToString() + "." + splFileName[1].ToString();

                saveLocation = Path.Combine(Server.MapPath(Config.UploadLocation), guidFileName);

                newFileName = "../Uploads/" + guidFileName;
                thumbnailLocation = Path.Combine(Server.MapPath(Config.UploadLocation), Guid.NewGuid().ToString() + "." + splFileName[1].ToString());
               
            }

            using (var photo =
                  System.Drawing.Image.FromFile(Server.MapPath(filename)))
            {
                using (var result =
                      new Bitmap(width, height, photo.PixelFormat))
                {
                    //result.SetResolution(
                    //        photo.HorizontalResolution,
                    //        photo.VerticalResolution);

                    result.SetResolution(200, 200);

                    using (Bitmap bmpNew = new Bitmap(result))
                    {
                        //result.Dispose();


                        using (var g = Graphics.FromImage(bmpNew))
                        {
                            g.InterpolationMode =
                                 InterpolationMode.HighQualityBicubic;
                            g.DrawImage(photo,
                                 new Rectangle(0, 0, width, height),
                                 new Rectangle(x, y, width, height),
                                 GraphicsUnit.Pixel);


                            photo.Dispose();
                            g.Dispose();

                            bmpNew.Height.Equals(height);
                            bmpNew.Width.Equals(width);

                            bmpNew.Save(Server.MapPath(newFileName));



                            NewHRManager mgr = new NewHRManager();
                            UploadType type = (UploadType)1;

                            try
                            {

                                WebHelper.CreateThumbnail(saveLocation, 200, 200, thumbnailLocation);
                                thumbnailLocation = Path.GetFileName(thumbnailLocation);
                            }
                            catch { }
                            if (IsAbsoluteUrl(thumbnailLocation) || IsAbsoluteUrl(guidFileName))
                            {
                                NewMessage.ShowWarningMessage("Unable to upload. Try again...");
                                return;
                            }
                            mgr.SaveOrUpdateImage(type, guidFileName, GetEmployeeID(), thumbnailLocation);

                            //ImgEmployee.ImageUrl = newFileName;

                            //ImgEmployeeThumbprint.ImageUrl = "../Uploads/" + thumbnailLocation;


                        }
                    }
                }
            }


            Response.Redirect("~/newhr/Identification.aspx?ID=" + GetEmployeeID().ToString());
        }

    }
}