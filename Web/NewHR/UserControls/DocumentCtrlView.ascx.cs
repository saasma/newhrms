﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using Utils.Helper;
using System.IO;
using Utils;
using Utils.Security;

namespace Web.NewHR.UserControls
{
    public partial class DocumentCtrlView : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        public void Initialise()
        {
            LoadLevels();
        }

        private int GetEmployeeId()
        {
            if (SessionManager.CurrentLoggedInEmployeeId == 0)
                return int.Parse(Request.QueryString["id"]);
            else
                return SessionManager.CurrentLoggedInEmployeeId;
        }

        private void LoadLevels()
        {
            List<HDocument> list = HRManager.GetDocuments(GetEmployeeId());

            GridLevels.Store[0].DataSource = list;
            GridLevels.Store[0].DataBind();

            if (list.Count <= 0)
                GridLevels.Hide();
        }

        protected void btnDownload_Click(object sender, DirectEventArgs e)
        {
            int documentId = int.Parse(hdnDocumentId.Text);
            HDocument obj = NewPayrollManager.GetDocumentById(documentId);

            string fileName = obj.Url.ToString();
            if (!string.IsNullOrEmpty(fileName))
                Response.Redirect("~/DocumentHandler.ashx?id=" + System.IO.Path.GetFileNameWithoutExtension(fileName).ToString());
            else
                NewMessage.ShowWarningMessage("This file does not exist.");

        }


    }
}