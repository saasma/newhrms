﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DocumentCtrlView.ascx.cs"
    Inherits="Web.NewHR.UserControls.DocumentCtrlView" %>
<script type="text/javascript">

     var DownloadHandler = function(command, record){
        <%= hdnDocumentId.ClientID %>.setValue(record.data.DocumentId);
            if(command == "DownLoad"){
                <%= btnDownload.ClientID %>.fireEvent('click');
               
            }
    };

</script>
<ext:Hidden runat="server" ID="hdnDocumentId" />
<ext:LinkButton ID="btnDownload" runat="server" Hidden="true">
    <DirectEvents>
        <Click OnEvent="btnDownload_Click">
        </Click>
    </DirectEvents>
</ext:LinkButton>
<ext:GridPanel ID="GridLevels" runat="server" Cls="itemgrid" Width="920">
    <Store>
        <ext:Store ID="Store3" runat="server">
            <Model>
                <ext:Model ID="Model4" runat="server" IDProperty="DocumentId">
                    <Fields>
                        <ext:ModelField Name="DocumentId" Type="String" />
                        <ext:ModelField Name="EmployeeId" Type="string" />
                        <ext:ModelField Name="Description" Type="string" />
                        <ext:ModelField Name="Name" Type="string" />
                        <ext:ModelField Name="Url" Type="string" />
                        <ext:ModelField Name="ContentType" Type="string" />
                        <ext:ModelField Name="Size" Type="string" />
                        <ext:ModelField Name="TypeName" Type="string" />
                        <ext:ModelField Name="Status" Type="Int" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
    </Store>
    <ColumnModel>
        <Columns>
            <ext:Column ID="Column8" Sortable="false" MenuDisabled="true" runat="server" Text="File Name"
                Wrap="true" Align="Left" DataIndex="Name" Width="250">
            </ext:Column>
            <ext:Column ID="Column5" Sortable="false" MenuDisabled="true" runat="server" Text="File Description"
                Wrap="true" Align="Left" DataIndex="Description" Width="250" />
            <ext:Column ID="Column2" Sortable="false" MenuDisabled="true" runat="server" Text="Document Type"
                Wrap="true" Align="Left" DataIndex="TypeName" Width="180">
            </ext:Column>
            <ext:Column ID="Column1" Sortable="false" MenuDisabled="true" runat="server" Text="Size"
                Wrap="true" Align="Center" DataIndex="Size" Width="80">
            </ext:Column>
            <ext:CommandColumn ID="CommandColumn1" runat="server" Text="Link">
                <Commands>
                    <ext:GridCommand Text="<i></i>Download" CommandName="DownLoad" />
                </Commands>
                <Listeners>
                    <Command Handler="DownloadHandler(command,record);" />
                </Listeners>
            </ext:CommandColumn>
            <ext:Column ID="Column3" Sortable="false" MenuDisabled="true" runat="server" Text=""
                Align="Center" Width="200">
            </ext:Column>
        </Columns>
    </ColumnModel>
    <SelectionModel>
        <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
    </SelectionModel>
</ext:GridPanel>
