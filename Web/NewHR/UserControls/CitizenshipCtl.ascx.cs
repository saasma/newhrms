﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;

using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;

namespace Web.NewHR.UserControls
{
    public partial class CitizenshipCtl : BaseUserControl
    {
        private bool _isDisplayMode = false;
        public bool IsDisplayMode
        {
            set
            {
                if (value == true)
                {
                    _isDisplayMode = value;
                    CommandColumn3.Visible = false;
                    CommandColumn2.Visible = false;
                    LinkButton1.Visible = false;
                    GridCitizenship.Width = 743;
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();

                if (SessionManager.CurrentLoggedInEmployeeId != 0)
                    hdnIsEmployee.Text = "1";
                else
                    hdnIsEmployee.Text = "0";
            }
        }
        protected void Initialise()
        {
            int EmployeeID = GetEmployeeID();

            cmbNationality.Store[0].DataSource = CommonManager.GetCitizenshipNationaliyList();
            cmbNationality.Store[0].DataBind();

            this.LoadCitizenshipGrid(EmployeeID);

        }
        protected void btnSave_Click(object sender, DirectEventArgs e)
        {

            this.SaveFormData();

        }
        private void SaveFormData()
        {

            Status myStatus = new Status();
            HCitizenship _HCitizenship = new HCitizenship();
            _HCitizenship.EmployeeId = this.GetEmployeeID();
            bool isSave = bool.Parse(this.hdnCitizenshipID.Text == "" ? "true" : "false");
            if (!isSave)
                _HCitizenship.CitizenshipId = int.Parse(this.hdnCitizenshipID.Text);

            if (!string.IsNullOrEmpty(cmbNationality.SelectedItem.Text))
            {
                _HCitizenship.Nationality = cmbNationality.SelectedItem.Text;
            }

            _HCitizenship.CitizenshipNo = txtCitizenshipNumber.Text.Trim();
            if (!string.IsNullOrEmpty(calIssueDate.Text.Trim()))
            {
                _HCitizenship.IssueDate = calIssueDate.Text.Trim();
                _HCitizenship.IssueDateEng = BLL.BaseBiz.GetEngDate(_HCitizenship.IssueDate, IsEnglish);
            }

            _HCitizenship.Place = txtPlace.Text.Trim();
            //file upload section
            string UserFileName = this.FileCitizenshipDocumentUpload.FileName;
            string ServerFileName = Guid.NewGuid().ToString() + Path.GetExtension(UserFileName);
            string relativePath = @"../uploads/" + ServerFileName;
            if (this.UploadFileCitizenship(relativePath))
            {
                double fileSize = FileCitizenshipDocumentUpload.PostedFile.ContentLength;

                _HCitizenship.FileFormat = Path.GetExtension(this.FileCitizenshipDocumentUpload.FileName).Replace(".", "").Trim();
                _HCitizenship.FileType = this.FileCitizenshipDocumentUpload.PostedFile.ContentType;
                _HCitizenship.FileLocation = @"../Uploads/";
                _HCitizenship.ServerFileName = ServerFileName;
                _HCitizenship.UserFileName = UserFileName;
                _HCitizenship.Size = this.ToSizeString(fileSize);

            }

            if (SessionManager.CurrentLoggedInEmployeeId == 0)
                _HCitizenship.Status = 1;
            else
                _HCitizenship.Status = 0;

            myStatus = NewHRManager.Instance.InsertUpdateCitizenship(_HCitizenship, isSave);

            if (myStatus.IsSuccess)
            {
                if (isSave)
                    NewMessage.ShowNormalMessage("Record Save successfully");
                else
                    NewMessage.ShowNormalMessage("Record Updated successfully");

                //  this.ClearFields();
                this.AECitizenshipWindow.Close();
                this.LoadCitizenshipGrid(this.GetEmployeeID());

            }
            else
                NewMessage.ShowWarningMessage(myStatus.ErrorMessage);
        }
        protected string ToSizeString(double bytes)
        {

            string[] SizeSuffixes = { "bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };
            int mag = (int)Math.Log(bytes, 1024);
            decimal adjustedSize = (decimal)bytes / (1 << (mag * 10));
            return string.Format("{0:n1} {1}", adjustedSize, SizeSuffixes[mag]);
        }
        protected void CitizenshipDownLoad(int ID)
        {

            //string contentType = "";
            HCitizenship doc = NewHRManager.GetCitizenshipDetailsById(ID);

            string path = Context.Server.MapPath(doc.FileLocation + doc.ServerFileName);
            string contentType = doc.FileType;
            string name = doc.UserFileName + "." + doc.FileFormat;

            byte[] bytes = File.ReadAllBytes(path);

            Context.Response.Clear();
            Context.Response.ClearHeaders();
            Context.Response.AppendHeader("Content-Disposition", "attachment; filename=\"" + name + "\"");
            Context.Response.ContentType = contentType;
            Context.Response.BinaryWrite(bytes);
            Context.Response.End();
            // Response.Redirect("~/DocumentHandler.ashx?ID=" + ID);
        }
        protected bool UploadFileCitizenship(string relativePath)
        {

            if (this.FileCitizenshipDocumentUpload.HasFile)
            {

                int fileSize = FileCitizenshipDocumentUpload.PostedFile.ContentLength;
                this.FileCitizenshipDocumentUpload.PostedFile.SaveAs(Server.MapPath(relativePath));
                return true;
            }
            else
                return false;

        }
        protected void LoadCitizenshipGrid(int EmployeeID)
        {
            List<HCitizenship> _HCitizenship = NewHRManager.GetCitizenshipByEmployeeID(EmployeeID);
            if (_HCitizenship != null)
            {
                this.StoreCitizenship.DataSource = _HCitizenship;
                this.StoreCitizenship.DataBind();
            }
            else
            {
                this.StoreCitizenship.DataSource = this.CitizenshipFillData;
                this.StoreCitizenship.DataBind();
            }
        }
        protected int GetEmployeeID()
        {
            //Guid? AssetID = null;
            int EmpID = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                EmpID = int.Parse(Request.QueryString["ID"]);
            }
            else
                EmpID = SessionManager.CurrentLoggedInEmployeeId;
            return EmpID;
        }
        protected void btnAddNewLine_Click(object sender, DirectEventArgs e)
        {
            this.ClearFields();
            this.AECitizenshipWindow.Show();

        }
        protected void lnkDeleteFile_Click(object sender, DirectEventArgs e)
        {
            int CitizenshipID = int.Parse(hdnCitizenshipID.Text);
            Status myStatus = new Status();

            HCitizenship _HCitizenship = NewHRManager.GetCitizenshipDetailsById(CitizenshipID);
            string path = Context.Server.MapPath(_HCitizenship.FileLocation + _HCitizenship.ServerFileName);
            myStatus = NewHRManager.Instance.DeleteCitizenshipFile(CitizenshipID);
            if (myStatus.IsSuccess)
            {
                if (File.Exists(path))
                {
                    File.Delete(path);
                    lnkDeleteFile.Hide();
                    lblUploadedFile.Hide();
                    NewMessage.ShowNormalMessage("file deleted successfully");
                    //refresh grid
                    this.LoadCitizenshipGrid(this.GetEmployeeID());

                }
            }



        }
        private object[] CitizenshipFillData
        {
            get
            {
                return new object[]
            {
                new object[] { "", "", "",""},
               
            };
            }
        }
        protected void GridCitizenship_Command(object sender, DirectEventArgs e)
        {
            string commandName = e.ExtraParams["command"];
            int CitizenshipID = int.Parse(e.ExtraParams["ID"]);
            HCitizenship _HCitizenship = NewHRManager.GetCitizenshipDetailsById(int.Parse(e.ExtraParams["ID"]));
            string Path = Server.MapPath(_HCitizenship.FileLocation) + _HCitizenship.ServerFileName;
            switch (commandName)
            {
                case "Delete":
                    this.DeleteCitizenship(CitizenshipID, Path);
                    break;
                case "DownLoad":
                    this.CitizenshipDownLoad(CitizenshipID);
                    break;
                case "Edit":
                    this.editCitizenship(CitizenshipID);
                    break;
            }

        }
        protected void DeleteCitizenship(int ID, string path)
        {

            bool result = NewHRManager.DeleteCitizenshipByID(ID);
            if (result)
            {
                if (File.Exists(path))
                    File.Delete(path);
                //refresh grid
                NewMessage.ShowNormalMessage("Record deleted successfully.");
                this.LoadCitizenshipGrid(this.GetEmployeeID());
            }

        }
        protected void ClearFields()
        {
            this.hdnCitizenshipID.Text = "";
            txtCitizenshipNumber.Text = "";
            txtPlace.Text = "";
            calIssueDate.Text = "";
            cmbNationality.Value = "";
            lblUploadedFile.Hide();
            lblUploadedFile.Text = "";
            lnkDeleteFile.Hide();
            FileCitizenshipDocumentUpload.Reset();
            btnSave.Text = Resources.Messages.Save;
        }
        public void editCitizenship(int CitizenshipID)
        {
            HCitizenship _HCitizenship = NewHRManager.GetCitizenshipDetailsById(CitizenshipID);
            this.hdnCitizenshipID.Text = CitizenshipID.ToString();

            //cmbNationality.Store[0].ClearFilter();
            if (_HCitizenship.Nationality != null)
                cmbNationality.SetValue(_HCitizenship.Nationality.ToString());
            else
                cmbNationality.ClearValue();
            
            //cmbNationality.Value = _HCitizenship.Nationality.ToString();

            txtCitizenshipNumber.Text =_HCitizenship.CitizenshipNo;
            calIssueDate.Text = _HCitizenship.IssueDate;
            txtPlace.Text = _HCitizenship.Place;
         
            if (!string.IsNullOrEmpty(_HCitizenship.UserFileName))
            {
                lblUploadedFile.Show();
                lblUploadedFile.Text = _HCitizenship.UserFileName;
                lnkDeleteFile.Show();
            }
            else
            {
                lblUploadedFile.Hide();
                lblUploadedFile.Text = "";
                lnkDeleteFile.Hide();
            }
            btnSave.Text = Resources.Messages.Update;
            this.AECitizenshipWindow.Show();
        }

    }
}