﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;
namespace Web.NewHR.UserControls
{
    public partial class SeminarCtrlView : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        protected void Initialise()
        {
            int EmployeeID = GetEmployeeID();
            this.LoadSeminarGrid(EmployeeID);
        }

        protected void LoadSeminarGrid(int EmployeeID)
        {
            List<HSeminar> _HSeminar = NewHRManager.GetSeminarByEmployeeID(EmployeeID);

            if (SessionManager.CurrentLoggedInEmployeeId == 0)
                _HSeminar = _HSeminar.Where(x => x.Status == (int)HRStatusEnum.Approved).ToList();

            if (Request.QueryString["ID"] != null)
            {
                if (int.Parse(Request.QueryString["ID"].ToString()) != SessionManager.CurrentLoggedInEmployeeId)
                    _HSeminar = _HSeminar.Where(x => x.Status == (int)HRStatusEnum.Approved).ToList();
            }

            this.StoreSeminar.DataSource = _HSeminar;
            this.StoreSeminar.DataBind();

            if (_HSeminar.Count <= 0)
                GridSeminar.Hide();
        }


        protected int GetEmployeeID()
        {
            int EmpID = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                EmpID = int.Parse(Request.QueryString["ID"]);
            }
            else
            {
                EmpID = SessionManager.CurrentLoggedInEmployeeId;
            }
            return EmpID;
        }

    }
}