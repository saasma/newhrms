﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StatusChangeHistoryCtrl.ascx.cs"
    Inherits="Web.NewHR.UserControls.StatusChangeHistoryCtrl" %>
<div class="widget-body">
    <table class="fieldTable firsttdskip">
        <tr>
            <td>
                <ext:GridPanel ID="gridChangeStatusHist" runat="server" Width="1000" Cls="itemgrid"
                    Scroll="None">
                    <Store>
                        <ext:Store ID="storeChangeStatusHist" runat="server">
                            <Model>
                                <ext:Model ID="Model1" runat="server" IDProperty="ECurrentStatusId">
                                    <Fields>
                                        <ext:ModelField Name="ECurrentStatusId" Type="String" />
                                        <ext:ModelField Name="StatusName" Type="string" />
                                        <ext:ModelField Name="FromDate" Type="string" />
                                        <ext:ModelField Name="ToDate" Type="string" />
                                        <ext:ModelField Name="Note" Type="string" />
                                        <ext:ModelField Name="LevelPosition" Type="string" />
                                        <ext:ModelField Name="TimeElapsed" Type="string" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                    </Store>
                    <ColumnModel>
                        <Columns>
                            <ext:Column ID="Column1" Width="140" runat="server" Text="Status" DataIndex="StatusName"
                                MenuDisabled="true" Sortable="false" />
                            <ext:Column ID="Column7" Width="140" runat="server" Text="Level/Position" DataIndex="LevelPosition"
                                MenuDisabled="true" Sortable="false" />
                            <ext:Column ID="Column2" runat="server" Width="120" Text="From Date" DataIndex="FromDate"
                                MenuDisabled="true" Sortable="false">
                            </ext:Column>
                            <ext:Column ID="Column3" runat="server" Width="120" Text="To Date" DataIndex="ToDate"
                                MenuDisabled="true" Sortable="false">
                            </ext:Column>
                            <ext:Column ID="Column5" runat="server" Width="140" Text="Duration" DataIndex="TimeElapsed"
                                MenuDisabled="true" Sortable="false">
                            </ext:Column>
                            <ext:Column ID="Column4" runat="server" Text="Note" Width="250" DataIndex="Note"
                                MenuDisabled="true" Sortable="false">
                            </ext:Column>
                            <ext:Column ID="Column6" runat="server" Text="" Width="230" MenuDisabled="true" Sortable="false">
                            </ext:Column>
                        </Columns>
                    </ColumnModel>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                    </SelectionModel>
                </ext:GridPanel>
            </td>
        </tr>
    </table>
</div>
