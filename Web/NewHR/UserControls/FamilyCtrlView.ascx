﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FamilyCtrlView.ascx.cs" Inherits="Web.NewHR.UserControls.FamilyCtrlView" %>

<ext:GridPanel StyleSpec="margin-top:15px;" ID="GridLevels" runat="server" Cls="itemgrid" Scroll="None" Width="920">
    <Store>
        <ext:Store ID="Store3" runat="server">
            <Model>
                <ext:Model ID="Model4" runat="server" IDProperty="FamilyId">
                    <Fields>
                        <ext:ModelField Name="FamilyId" Type="String" />
                        <ext:ModelField Name="Title" Type="String" />
                        <ext:ModelField Name="Name" Type="string" />
                        <ext:ModelField Name="Relation" Type="string" />
                        <ext:ModelField Name="DateOfBirth" Type="string" />
                        <ext:ModelField Name="HasDependent" Type="string" />
                        <ext:ModelField Name="Occupation" Type="string" />
                        <ext:ModelField Name="ContactNumber" Type="string" />
                        <ext:ModelField Name="Gender" Type="String" />
                        <ext:ModelField Name="Nationality" Type="String" />
                        <ext:ModelField Name="Remarks" Type="string" />
                        <ext:ModelField Name="SpecifiedDate" Type="string" />
                        <ext:ModelField Name="AgeOnSpecifiedSPDate" Type="string" />
                        <ext:ModelField Name="IsEditable" Type="Int" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
    </Store>
    <ColumnModel>
        <Columns>
            <ext:Column ID="Column8" Sortable="false" MenuDisabled="true" runat="server" Text="Relation" Wrap="true"
                Align="Left" Width="80" DataIndex="Relation" />
            <ext:Column ID="Column5" Sortable="false" MenuDisabled="true" runat="server" Text="Name" Wrap="true"
                Align="Left" Width="150" DataIndex="Name">
                <Renderer Fn="nameRender" />
            </ext:Column>
            <ext:Column ID="Column6" Sortable="false" MenuDisabled="true" runat="server" Text="DOB" Width="100" Wrap="true"
                Align="Center" DataIndex="DateOfBirth">
            </ext:Column>
            <ext:Column ID="Column1" Sortable="false" MenuDisabled="true" runat="server" Text="Dependent" Width="80" Wrap="true"
                Align="Center" DataIndex="HasDependent">
                <Renderer Fn="dependentRenderer" />
            </ext:Column>
            <ext:Column ID="Column2" Sortable="false" MenuDisabled="true" runat="server" Text="Occupation" Wrap="true"
                Align="Left" Width="100" DataIndex="Occupation">
            </ext:Column>
            <ext:Column ID="Column3" Sortable="false" MenuDisabled="true" runat="server" Text="Contact Number" Wrap="true"
                Align="Left" Width="120" DataIndex="ContactNumber">
            </ext:Column>
            <ext:Column ID="Column9" Sortable="false" MenuDisabled="true" runat="server" Text="Gender" Wrap="true" Align="Left" Width="80" DataIndex="Gender">
                <Renderer Fn="GenderRenderer" />
            </ext:Column>
            <ext:Column ID="Column10" Sortable="false" MenuDisabled="true" runat="server" Text="Nationality" Wrap="true" Align="Left" Weight="100" DataIndex="Nationality">
            </ext:Column>
            <ext:Column ID="Column7" Sortable="false" MenuDisabled="true" runat="server" Text="Remarks" Wrap="true"
                Align="Left" Width="200" DataIndex="Remarks">
            </ext:Column>
        </Columns>
    </ColumnModel>
    <SelectionModel>
        <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
    </SelectionModel>
</ext:GridPanel>

<script type="text/javascript">
    var dependentRenderer = function (value) {
        if (value == "true")
            return "Yes";
        return "No";
    }
    var nameRender = function (v1, v2, v3) {
        if (v3.data.Title != null && v3.data.Title!="")
            return v3.data.Title + ". " + v3.data.Name;
        else
            return v3.data.Name;
    }
    var GenderRenderer = function (v1, v2, v3) {
        if (v3.data.Gender != null) {
            if (v3.data.Gender == '1')
                return 'Male'
            else if (v3.data.Gender == '0')
                return 'Female'
            else
                return 'Other'
        }
    }


</script>
