﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;

namespace Web.NewHR.UserControls
{
    public partial class TrainingCtrlView : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack && !X.IsAjaxRequest)
                Initialise();
        }

        protected void Initialise()
        {
            int EmployeeID = GetEmployeeID();
            this.LoadTrainingGrid(EmployeeID);
        }

        protected int GetEmployeeID()
        {
            int EmpID = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                EmpID = int.Parse(Request.QueryString["ID"]);
            }
            else
            {
                EmpID = SessionManager.CurrentLoggedInEmployeeId;
            }
            return EmpID;
        }
        
        protected void LoadTrainingGrid(int EmployeeID)
        {
            List<HTraining> _HTraining = NewHRManager.GetTrainingByEmployeeID(EmployeeID)
                // descending sorting for NCC
                .OrderByDescending(x => x.TrainingFromEngDate).ToList();

            if (SessionManager.CurrentLoggedInEmployeeId == 0)
                _HTraining = _HTraining.Where(x => x.Status == (int)HRStatusEnum.Approved).ToList();

            if (Request.QueryString["ID"] != null)
            {
                if (int.Parse(Request.QueryString["ID"].ToString()) != SessionManager.CurrentLoggedInEmployeeId)
                    _HTraining = _HTraining.Where(x => x.Status == (int)HRStatusEnum.Approved).ToList();
            }

            this.StoreTraining.DataSource = _HTraining;
            this.StoreTraining.DataBind();

            if (_HTraining.Count <= 0)
                GridTraining.Hide();
        }


        public void btnDownloadTrFile_Click(object sender, EventArgs e)
        {
            int trainingId = int.Parse(hdnTrainingIdValue.Text);
            HTraining doc = NewHRManager.GetTrainingDetailsById(trainingId);

            string path = Server.MapPath(@"~/uploads/" + doc.FileLocation + doc.ServerFileName);
            string contentType = doc.FileType;
            string name = doc.UserFileName;

            byte[] bytes = File.ReadAllBytes(path);

            Context.Response.Clear();
            Context.Response.ClearHeaders();
            Context.Response.AppendHeader("Content-Disposition", "attachment; filename=\"" + name + "\"");
            Context.Response.ContentType = contentType;
            Context.Response.BinaryWrite(bytes);
            Context.Response.End();
        }

    }
}