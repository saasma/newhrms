﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;
using System.Text;
namespace Web.NewHR.UserControls
{
    public partial class DocumentSubCategoryCtrl : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        private void Initialise()
        {
            BindCategoryComboBox();
        }

        private void BindCategoryComboBox()
        {
            cmbCategory.Store[0].DataSource = DocumentManager.GetDocCategories();
            cmbCategory.Store[0].DataBind();
        }

        public void AddNewSubCategory(string IsSubCategoryPage, Guid? categoryID)
        {
            btnSaveSubCat.Text = "Save";
            txtName.Text = "";
            txtCode.Text = "";
            cmbCategory.ClearValue();

            BindCategoryComboBox();

            hdnIsSubCategoryPage.Text = IsSubCategoryPage;

            if (categoryID != null)
                cmbCategory.SetValue(categoryID.Value);

            hdnSubCategoryID.Text = "";
            WDocumentSubCategory.Center();
            WDocumentSubCategory.Show();
        }

        protected void btnSaveSubCat_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveUpdSubCat");
            if (Page.IsValid)
            {
                bool isSave = true;
                DocSubCategory obj = new DocSubCategory();

                obj.Name = txtName.Text.Trim();
                obj.Code = txtCode.Text.Trim();

                if (!string.IsNullOrEmpty(hdnSubCategoryID.Text))
                {
                    obj.SubCategoryID = new Guid(hdnSubCategoryID.Text);
                    isSave = false;
                }
                else
                    obj.SubCategoryID = Guid.NewGuid();

                obj.CategoryID = new Guid(cmbCategory.SelectedItem.Value);

                Status status = DocumentManager.SaveUpdateDocSubCategory(obj, isSave);
                if (status.IsSuccess)
                {
                    if (hdnIsSubCategoryPage.Text == "1")
                    {
                        if (!string.IsNullOrEmpty(hdnSubCategoryID.Text))
                            NewMessage.ShowNormalMessage("Record updated successfully.", "reloadSubCategoryGrid");
                        else
                            NewMessage.ShowNormalMessage("Record saved successfully.", "reloadSubCategoryGrid");
                    }
                    else
                    {
                        X.Js.Call("ReloadSubCategoryCombo", obj.SubCategoryID.ToString());
                    }


                    WDocumentSubCategory.Close();
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }

            }
        }

        public void EditSubCategory(Guid subCategoryID)
        {
            DocSubCategory objDocSubCategory = DocumentManager.GetDocSubCategoryById(subCategoryID);
            if (objDocSubCategory != null)
            {
                BindCategoryComboBox();

                hdnIsSubCategoryPage.Text = "1";
                hdnSubCategoryID.Text = subCategoryID.ToString();
                txtName.Text = objDocSubCategory.Name;
                txtCode.Text = objDocSubCategory.Code;
                cmbCategory.SetValue(objDocSubCategory.CategoryID.Value);
                btnSaveSubCat.Text = "Update";
                WDocumentSubCategory.Center();
                WDocumentSubCategory.Show();
            }
        }


    }
}