﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;

using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;

namespace Web.NewHR.UserControls
{




    public partial class EmpPayIncomeInfoCtl : BaseUserControl
    {

        public void LoadInfo()
        {
            int EmpID = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                EmpID = int.Parse(Request.QueryString["ID"]);
            }
            else
                EmpID = SessionManager.CurrentLoggedInEmployeeId;
            
            List<EmployeeIncomeListResult> _ListIncome = PayManager.GetEmployeeIncomeList(EmpID);
            storeIncomeList.DataSource = _ListIncome.Where(x=>x.Amount>0);
            storeIncomeList.DataBind();
        }

        
    }
}