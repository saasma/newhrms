﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UPassortCtrlPopup.ascx.cs" Inherits="Web.NewHR.UserControls.UPassortCtrlPopup" %>


<ext:Hidden runat="server" ID="hdnPassportID">
</ext:Hidden>


        <table class="fieldTable">
            <tr>
                <td>
                    <ext:TextField ID="txtPassportNumber" Width="180px" runat="server" FieldLabel="Passport Number"
                        LabelAlign="top" LabelSeparator="" />
                    <asp:RequiredFieldValidator Display="None" ID="valtxtPassportNumber" runat="server"
                        ValidationGroup="PassportSaveUpdate" ControlToValidate="txtPassportNumber" ErrorMessage="Please enter Passport Number." />
                </td>
            </tr>
            <tr>
                <td>
                    <pr:CalendarExtControl FieldLabel="Issue Date" Width="180px" ID="calIssueDate" runat="server"
                        LabelSeparator="" LabelAlign="Top" />
                    <asp:RequiredFieldValidator Display="None" ID="valcalIssueDate" runat="server" ValidationGroup="PassportSaveUpdate"
                        ControlToValidate="calIssueDate" ErrorMessage="Please enter passport Issue Date." />
                </td>
                <td style="padding-top: 5px">
                    <pr:CalendarExtControl FieldLabel="Valid Upto" Width="180px" ID="calValidUpto" runat="server"
                        LabelSeparator="" LabelAlign="Top" />
                    <asp:RequiredFieldValidator Display="None" ID="valcalValidUpto" runat="server" ValidationGroup="PassportSaveUpdate"
                        ControlToValidate="calValidUpto" ErrorMessage="Please check Valid Upto." />
                </td>
            </tr>
            <tr>
                <td>
                    <ext:FileUploadField ID="FilePassportDocumentUpload" runat="server" Width="200" Icon="Attach"
                        FieldLabel="&nbsp;" LabelAlign="Top" LabelSeparator="" />
                </td>
                <td valign="bottom">
                    <table>
                        <tr>
                            <td>
                                <ext:Label ID="lblUploadedFile" runat="server" Hidden="true">
                                </ext:Label>
                            </td>
                            <td>
                                <ext:LinkButton Icon="Delete" runat="server" ID="lnkDeleteFile" OnDirectClick="lnkDeleteFile_Click"
                                    Text="Delete file" Hidden="true">
                                    <DirectEvents>
                                        <Click OnEvent="lnkDeleteFile_Click">
                                         <Confirmation ConfirmRequest="true" Message="Do you want to delete the file?" />
                                            <EventMask ShowMask="true" />
                                        </Click>
                                    </DirectEvents>
                                </ext:LinkButton>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td valign="bottom" colspan="2">
                    <div class="popupButtonDiv">
                        <ext:Button runat="server" Cls="btn btn-primary" ID="btnSave"
                            Text="<i></i>Save">
                            <DirectEvents>
                                <Click OnEvent="btnSave_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                            <Listeners>
                                <Click Handler="valGroup = 'PassportSaveUpdate'; return CheckValidation();">
                                </Click>
                            </Listeners>
                        </ext:Button>
                        <div class="btnFlatOr">
                            or</div>
                        <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="LinkButton2" Cls="btnFlatLeftGap"
                            Text="<i></i>Cancel">
                            <Listeners>
                                <Click Handler="#{AEPassportWindow}.hide();">
                                </Click>
                            </Listeners>
                        </ext:LinkButton>
                    </div>
                </td>
            </tr>
        </table>
