﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DocumentListCtrl.ascx.cs" Inherits="Web.NewHR.UserControls.DocumentListCtrl" %>


<ext:Hidden ID="hdnPartyID" runat="server" />
<ext:Hidden ID="hdnFileID" runat="server" />
<ext:Hidden ID="hdnDocumentID" runat="server" />


<ext:Button runat="server" AutoPostBack="true" OnClick="btnDownload_Click"
    Hidden="true" ID="btnDownloadFile" Text="<i></i>Download">
</ext:Button>

<ext:Button ID="btnDelete" runat="server" Hidden="true">
    <DirectEvents>
        <Click OnEvent="btnDelete_Click">
            <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the document?" />
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:Button>

<table>
    
    <tr>
        <td valign="top">
            <table>
                <tr>
                    <td>
                        <ext:TextField ID="txtSearchParty" runat="server" LabelSeparator="" FieldLabel="" Text="" EmptyText="Search Party" Width="200" >
                            <DirectEvents>
                                <Change OnEvent="txtSearchParty_Change">
                                    <EventMask ShowMask="true" />
                                </Change>
                            </DirectEvents>
                        </ext:TextField>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:GridPanel ID="gridParty" runat="server" Width="200" Cls="itemgrid" MarginSpec="10 0 0 0" Header="false">
                            <Store>
                                <ext:Store ID="storeParty" runat="server">
                                    <Model>
                                        <ext:Model ID="Model4" runat="server" IDProperty="PartyID">
                                            <Fields>
                                                <ext:ModelField Name="PartyID" Type="String" />
                                                <ext:ModelField Name="Name" Type="string" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <ColumnModel>
                                <Columns>
                                    <ext:Column ID="Column5" Sortable="false" MenuDisabled="true" runat="server" Text="Party Name"
                                        Align="Left" Width="200" DataIndex="Name" />
                                </Columns>
                            </ColumnModel>
                            <Listeners>
                                <ItemClick Fn="selectParty" />
                            </Listeners>
                            <SelectionModel>
                                <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                            </SelectionModel>
                        </ext:GridPanel>
                    </td>
                </tr>
            </table>
             
        </td>

        <td  valign="top">
        
        

   

<div id="divList" runat="server" style="background-color:#ddebf7; height:80px; padding-top:10px; margin-left:20px; padding-right:10px;">
<table>
    <tr>    
        <td valign="bottom">
             <ext:Store runat="server" ID="storeDocument" AutoLoad="false">
                        <Proxy>
                            <ext:AjaxProxy Url="../../Handler/DocumentSearch.ashx">
                                <ActionMethods Read="GET" />
                                <Reader>
                                    <ext:JsonReader Root="plants" TotalProperty="total" />
                                </Reader>
                            </ext:AjaxProxy>
                        </Proxy>
                        <Model>
                            <ext:Model ID="Model3" IDProperty="Value" Name="ItemLineModel" runat="server">
                                <Fields>
                                    <ext:ModelField Name="Text" Type="String" />
                                    <ext:ModelField Name="Value" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                    <ext:ComboBox LabelSeparator="" ID="cmbDocument" runat="server" DisplayField="Text" MarginSpec="20 0 0 10"
                        FieldLabel="" LabelAlign="Top" ValueField="Value" EmptyText="Search Documents"
                        StoreID="storeDocument" TypeAhead="false" Width="200" HideBaseTrigger="true" MinChars="1" 
                        TriggerAction="All" ForceSelection="false">
                        <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                            <ItemTpl ID="ItemTpl1" runat="server">
                                <Html>
                                    <tpl>
                                            <div class="search-item">
                                                            <span>{Text}</span>  
                                             </div>
					                </tpl>
                                </Html>
                            </ItemTpl>
                        </ListConfig>
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                        </Triggers>
                        <Listeners>
                            <Select Handler="this.getTrigger(0).show();" />
                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                            <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                        </Listeners>
                    </ext:ComboBox>
        </td>
        <td>
            <ext:ComboBox Width="160" LabelSeparator="" LabelAlign="Top"  MarginSpec="0 0 0 10"
                QueryMode="Local" ID="cmbCategory" ForceSelection="true" DisplayField="Name" ValueField="CategoryID"
                runat="server" FieldLabel="Category">
                <Store>
                    <ext:Store ID="Store2" runat="server">
                        <Model>
                            <ext:Model ID="Model2" IDProperty="CategoryID" runat="server">
                                <Fields>
                                    <ext:ModelField Name="CategoryID" Type="String" />
                                    <ext:ModelField Name="Name" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <Triggers>
                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                </Triggers>
                <Listeners>
                    <Select Handler="this.getTrigger(0).show();" />
                    <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                    <TriggerClick Handler="if (index == 0) { 
                            this.clearValue(); 
                            this.getTrigger(0).hide();
                        }" />
                </Listeners>
            </ext:ComboBox>
        </td>
        <td>
            <ext:ComboBox Width="160" LabelSeparator="" LabelAlign="Top" MarginSpec="0 0 0 10"
                QueryMode="Local" ID="cmbSubCategory" ForceSelection="true" DisplayField="Name" ValueField="SubCategoryID"
                runat="server" FieldLabel="Sub-Category">
                <Store>
                    <ext:Store ID="Store1" runat="server">
                        <Model>
                            <ext:Model ID="Model1" IDProperty="SubCategoryID" runat="server">
                                <Fields>
                                    <ext:ModelField Name="SubCategoryID" Type="String" />
                                    <ext:ModelField Name="Name" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <Triggers>
                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                </Triggers>
                <Listeners>
                    <Select Handler="this.getTrigger(0).show();" />
                    <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                    <TriggerClick Handler="if (index == 0) { 
                            this.clearValue(); 
                            this.getTrigger(0).hide();
                        }" />
                </Listeners>
            </ext:ComboBox>
        </td>
        <td>
            <ext:ComboBox Width="110" LabelSeparator="" LabelAlign="Top" MarginSpec="0 0 0 10"
                QueryMode="Local" ID="cmbDateAdded" ForceSelection="true" DisplayField="Text" ValueField="Value"
                runat="server" FieldLabel="Date Added">
                <Items>
                    <ext:ListItem Text="Today" Value="1" />
                    <ext:ListItem Text="This Week" Value="2" />
                    <ext:ListItem Text="Last Week" Value="3" />
                    <ext:ListItem Text="This Month" Value="4" />
                    <ext:ListItem Text="Last Month" Value="5" />
                    <ext:ListItem Text="Custom Date" Value="6" />
                    </Items>
                <Triggers>
                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                </Triggers>
                <Listeners>
                    <Select Handler="this.getTrigger(0).show();" />
                    <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                    <TriggerClick Handler="if (index == 0) { 
                            this.clearValue(); 
                            this.getTrigger(0).hide();
                        }" />
                </Listeners>
                <DirectEvents>
                    <Select OnEvent="cmbDateAdded_Click" />
                    <TriggerClick OnEvent="cmbDateAdded_Click" />
                </DirectEvents>
            </ext:ComboBox>
        </td>
       
        <td>
            <ext:ComboBox Width="110" LabelSeparator="" LabelAlign="Top" MarginSpec="0 0 0 10"
                QueryMode="Local" ID="cmbExpiring" ForceSelection="true" DisplayField="Text" ValueField="Value"
                runat="server" FieldLabel="Expiring">
                <Items>
                    <ext:ListItem Text="Today" Value="1" />
                    <ext:ListItem Text="This Week" Value="2" />
                    <ext:ListItem Text="Last Week" Value="3" />
                    <ext:ListItem Text="This Month" Value="4" />
                    <ext:ListItem Text="Last Month" Value="5" />
                    <ext:ListItem Text="Custom Date" Value="6" />
                </Items>
                <Triggers>
                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                </Triggers>
                <Listeners>
                    <Select Handler="this.getTrigger(0).show();" />
                    <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                    <TriggerClick Handler="if (index == 0) { 
                            this.clearValue(); 
                            this.getTrigger(0).hide();
                        }" />
                </Listeners>
                <DirectEvents>
                    <Select OnEvent="cmbExpiring_Select" />
                    <TriggerClick OnEvent="cmbExpiring_Select" />
                </DirectEvents>
            </ext:ComboBox>
        </td>
        
        <td style="padding-top: 14px; ">
            <ext:Button ID="btnLoad" runat="server" Cls="btn btn-save" Text="Show" Width="100" MarginSpec="0 0 0 10">
                <Listeners>
                    <Click Fn="searchList">
                    </Click>
                </Listeners>
            </ext:Button>
        </td>
    </tr>
    <tr>
        <td></td>
         <td>
            <pr:CalendarExtControl ID="calDateAddedFrom" LabelSeparator="" runat="server" FieldLabel="Date Added From" MarginSpec="10 0 0 10" Hidden="true"
                Width="120" LabelAlign="Top">
            </pr:CalendarExtControl>
        </td>
        <td>
            <pr:CalendarExtControl ID="calDateAddedTo" LabelSeparator="" runat="server" FieldLabel="Date Added To" MarginSpec="10 0 0 10" Hidden="true"
                Width="120" LabelAlign="Top">
            </pr:CalendarExtControl>
        </td>
        <td>
            <pr:CalendarExtControl ID="calExpiringDateFrom" LabelSeparator="" runat="server" FieldLabel="Expiring Date From" MarginSpec="10 0 0 10" Hidden="true"
                Width="120" LabelAlign="Top">
            </pr:CalendarExtControl>
        </td>
        <td>
            <pr:CalendarExtControl ID="calExpiringDateTo" LabelSeparator="" runat="server" FieldLabel="Expiring Date To" MarginSpec="10 0 0 10" Hidden="true"
                Width="120" LabelAlign="Top">
            </pr:CalendarExtControl>
        </td>
    </tr>
</table>
</div>

<ext:GridPanel StyleSpec="margin-top:15px; margin-left:15px;" ID="gridDocumentList" runat="server" Cls="itemgrid" MinHeight="430"
            Scroll="Horizontal">
            <Store>
                <ext:Store ID="storeDocumentList" runat="server" AutoLoad="true" OnReadData="Store_ReadData"
                    RemotePaging="true" RemoteSort="true">
                    <Proxy>
                        <ext:PageProxy />
                    </Proxy>
                    <Model>
                        <ext:Model ID="Model6" runat="server" IDProperty="DocumentID">
                            <Fields>
                                <ext:ModelField Name="DocumentID" Type="String" />
                                <ext:ModelField Name="ID" Type="String" />
                                <ext:ModelField Name="ParyName" Type="String" />
                                <ext:ModelField Name="CategorySubCategory" Type="String" />
                                <ext:ModelField Name="DocumentDate" Type="String" />
                                <ext:ModelField Name="Title" Type="String" />
                                <ext:ModelField Name="Description" Type="String" />
                                <ext:ModelField Name="ExpiryDate" Type="String" />
                                <ext:ModelField Name="CreatedOn" Type="Date" />
                                <ext:ModelField Name="UserFileName" Type="String" />
                                <ext:ModelField Name="FileID" Type="String" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>
            <ColumnModel>
                <Columns>
                    <ext:TemplateColumn ID="TemplateColumn1" runat="server" Text="ID" Width="150" Locked="true"
                        MenuDisabled="false" Sortable="false" DataIndex="ID" TemplateString='<a href="../DocumentManagement/DocumentView.aspx?DocumentId={DocumentID}"> {ID}</a>' />
                      
                    <ext:TemplateColumn ID="TemplateColumn2" runat="server" Text="Document Name" Width="250"
                        MenuDisabled="false" Sortable="false" DataIndex="Title" TemplateString='<a href="../DocumentManagement/DocumentView.aspx?DocumentId={DocumentID}"> {Title}</a>' />
                      
                    <ext:Column ID="colParyName" Sortable="false" MenuDisabled="true" runat="server"
                        Text="Party" Width="150" Align="Left" DataIndex="ParyName">
                    </ext:Column>
                    <ext:Column ID="colCategorySubCategory" Sortable="false" MenuDisabled="true" runat="server"
                        Text="Category:Sub-Category" Width="250" Align="Left" DataIndex="CategorySubCategory">
                    </ext:Column>
                    <ext:Column ID="colDocumentDate" Sortable="false" MenuDisabled="true" runat="server"
                        Text="Document Date" Width="100" Align="Left" DataIndex="DocumentDate">
                    </ext:Column>
                    
                    <ext:Column ID="colDescription" Sortable="false" MenuDisabled="true" runat="server"
                        Text="Description" Width="250" Align="Left" DataIndex="Description">
                    </ext:Column>                    
                    <ext:Column ID="colExpiryDate" Sortable="false" MenuDisabled="true" runat="server"
                        Text="Expiry Date" Width="100" Align="Left" DataIndex="ExpiryDate">
                    </ext:Column>
                    <ext:DateColumn ID="DateColumn4" runat="server" Align="Left" Text="Date Added" Width="100"
                        MenuDisabled="true" Sortable="false" Format="yyyy/MM/dd" DataIndex="CreatedOn">
                    </ext:DateColumn>
                    <ext:Column ID="Column3" Sortable="false" MenuDisabled="true" runat="server"
                        Text="Attachment" Width="140" Align="Left" DataIndex="UserFileName" Hidden="true">
                    </ext:Column>
                     <ext:CommandColumn ID="CommandColumn3" runat="server" Width="40" Text="" Align="Center" MenuDisabled="true" Sortable="false" Hidden="true">
                        <Commands>
                            <ext:CommandSeparator />
                            <ext:GridCommand Icon="PageWhitePut" CommandName="DownLoad">
                                <ToolTip Text="DownLoad" />
                            </ext:GridCommand>
                        </Commands>
                        <PrepareToolbar Fn="prepareToolbar" />
                        <Listeners>
                            <Command Handler="CommandHandlerFile(command,record);" />
                        </Listeners>
                    </ext:CommandColumn>
                    <ext:CommandColumn ID="ComColView" runat="server" Width="60" Text="" Align="Center" MenuDisabled="true" Sortable="false">
                        <Commands>
                            <ext:CommandSeparator />
                            <ext:GridCommand Text="View" CommandName="View">
                                <ToolTip Text="View" />
                            </ext:GridCommand>
                        </Commands>
                        <Listeners>
                            <Command Handler="CommandHandler(command,record);" />
                        </Listeners>
                    </ext:CommandColumn>
                    <ext:CommandColumn ID="ComColEdit" runat="server" Width="40" Text="" Align="Center" MenuDisabled="true" Sortable="false">
                        <Commands>
                            <ext:CommandSeparator />
                            <ext:GridCommand Cls="editGridButton" ToolTip-Text="Edit" Text="<i class='fa fa-pencil'></i>"
                                CommandName="Edit">
                                <ToolTip Text="Edit" />    
                            </ext:GridCommand>
                        </Commands>
                        <Listeners>
                            <Command Handler="CommandHandler(command,record);" />
                        </Listeners>
                    </ext:CommandColumn>
                    <ext:CommandColumn ID="ComColDel" runat="server" Width="40" Text="" Align="Center" MenuDisabled="true" Sortable="false">
                        <Commands>
                            <ext:CommandSeparator />
                            <ext:GridCommand Cls="editGridButton" ToolTip-Text="Delete" Text="<i class='fa fa-trash-o'></i>"
                            CommandName="Delete">
                                <ToolTip Text="Delete" />
                            </ext:GridCommand>
                        </Commands>
                        <Listeners>
                            <Command Handler="CommandHandler(command,record);" />
                        </Listeners>
                    </ext:CommandColumn>                    
                </Columns>
            </ColumnModel>
            <View>
                <ext:GridView ID="GridView1" runat="server">
                    <GetRowClass Fn="getRowClass" />
                </ext:GridView>
            </View>
            <BottomBar>
                <ext:PagingToolbar Height="40" ID="PagingToolbar1" runat="server" StoreID="storeDocumentList"
                    DisplayInfo="true">
                    <Items>
                        <ext:Label ID="lblPageSize" runat="server" Text="Page size:" />
                        <ext:ToolbarSpacer ID="ToolbarSpacer_listItems" runat="server" Width="10" />
                        <ext:ComboBox ID="cmbPageSize" runat="server" Width="80" SelectOnFocus="true" Selectable="true"
                            ValueField="Value" DisplayField="Text" ForceSelection="true" AllowBlank="false">
                            <Listeners>
                                <Select Handler="searchList()" />
                            </Listeners>
                            <Items>
                                <ext:ListItem Value="20" Text="20" />
                                <ext:ListItem Value="50" Text="50" />
                                <ext:ListItem Value="100" Text="100" />
                            </Items>
                            <SelectedItems>
                                <ext:ListItem Index="0">
                                </ext:ListItem>
                            </SelectedItems>
                        </ext:ComboBox>
                        
                        <ext:Button ID="btnExport" runat="server" Text="To Excel" AutoPostBack="true" OnClick="btnExport_Click"  Icon="PageExcel">
                        </ext:Button>
                    </Items>
                </ext:PagingToolbar>
            </BottomBar>
        </ext:GridPanel>
</td>
    </tr>
</table>



<script type="text/javascript">

    var getRowClass = function (record) {

    };

    function searchList() {
        <%=gridDocumentList.ClientID %>.getStore().pageSize = <%=cmbPageSize.ClientID %>.getValue();
        <%=PagingToolbar1.ClientID %>.doRefresh();
    }

    function selectParty(dataview, record, item, index, e) 
    {
        <%= hdnPartyID.ClientID %>.setValue(record.data.PartyID);
        searchList();  
    }

    var CommandHandler = function(command, record){
        var documentID = record.data.DocumentID;
          
        if(command=="View")
        {
            window.location = 'DocumentView.aspx?DocumentId=' + documentID;
        }
        else if(command=="Edit")
        {
            window.location = 'Document.aspx?DocumentId=' + documentID;
        }
        else
        {
            <%= hdnDocumentID.ClientID %>.setValue(documentID);
            <%= btnDelete.ClientID %>.fireEvent('click');
        }

    };

     var CommandHandlerFile = function(command, record){
            <%= hdnFileID.ClientID %>.setValue(record.data.FileID);
       
         if(command == "DownLoad")
            {
                <%= btnDownloadFile.ClientID %>.fireEvent('click');
            }
        };

    var prepareToolbar = function (grid, toolbar, rowIndex, record) {

        if (record.data.FileID == '') {
            toolbar.hide();
        }
    };

    function SetMinHeight()
    {
        document.getElementById("<%= divList.ClientID %>").style.height = "80px";
    }

    function SetMaxHeight()
    {
        document.getElementById("<%= divList.ClientID %>").style.height = "140px";
    }


</script>