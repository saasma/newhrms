﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DefinedTypeImportCtl.ascx.cs"
    Inherits="Web.CP.Extension.UserControls.DefinedTypeImportCtl" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<script type="text/javascript">

    var btnLoad = null;
    var blockMultipleFormSubmit = true;
    var StoreIncomes= null;
    var cmbIncomes = null;
    var WindowAdd= null;
    Ext.onReady(
        function () {
            WindowAdd = <%=WindowAdd.ClientID %>;
            cmbIncomes = document.getElementById('<%=cmbIncomes.ClientID %>');    
            btnLoad = <%= btnLoad.ClientID %>;
        }

    );
    function refreshWindow() {
       // alert('h');
        btnLoad.fireEvent('click');
    }

    var forcePostBack = function () {
            window.location = 'DefinedSalaryImport.aspx';
        };

    function isPayrollSelected(btn) {
        if ($('#' + btn.id).prop('disabled'))
            return false;
       

        var ret = shiftPopup("ID=" + cmbIncomes.value);
        return false;
    }

    var columnRenderer = function (value, meta, record) {
        if (value == null)
            return "";

        if (value == "")
            return "";
        return value.toFixed(2);
    }

    var afterEdit = function (e) {
    };

    var amountRenderer = function (value, meta, record) {

        if (typeof (value) == 'undefined' || value == null)
            return "";

        var value = parseFloat(value);

        if (value < 0) {

            meta.attr = 'style="border: 1px solid red;"';
            meta.attr += ('title="Negative value."');
        }

        return value.toFixed(2);
    };

     function selectDeselectAll(chk) {
            $('input[type=checkbox]').each(
                    function (index) {
                        if ($(this).prop('disabled') == false && this.id.indexOf('chkVariableIncomes') > 0)
                            this.checked = chk.checked;
                    }
                );

                __doPostBack("","");
                }
</script>
<style type="text/css">
    .selectCell
    {
        border: 1px splid red !important;
    }
    label
    {
        padding-left: 5px;
        padding-right: 10px;
    }
</style>
<div class="contentArea">

    <div class="attribute" style="padding:10px;">
        <table>
            <tr>
                <td style='width: 800px'>
                    <%--<ext:Store ID="StoreIncomes" runat="server" AutoLoad="true">
                        <Model>
                            <ext:Model ID="Model1" runat="server" IDProperty="IncomeId">
                                <Fields>
                                    <ext:ModelField Name="Title" />
                                    <ext:ModelField Name="IncomeId" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                    <ext:ComboBox ID="cmbIncomes" DisplayField="Title" ValueField="IncomeId" StoreID="StoreIncomes"
                        Width="200px" ForceSelection="true" LabelAlign="Top" FieldLabel="Income" runat="server">
                        <DirectEvents>
                            <Select OnEvent="cmbIncomes_Select">
                                <EventMask ShowMask="true" />
                            </Select>
                        </DirectEvents>
                    </ext:ComboBox>--%>
                    <strong>Income</strong>
                    <asp:DropDownList ID="cmbIncomes" OnSelectedIndexChanged="cmbIncomes_Select" AutoPostBack="true"
                        DataValueField="IncomeId" DataTextField="Title" AppendDataBoundItems="true" runat="server"
                        Width="200px">
                        <asp:ListItem Text="" Value="" />
                    </asp:DropDownList>
                    <strong style="padding-left:15px">From</strong>
                    <asp:DropDownList ID="ddlFrom" OnSelectedIndexChanged="ddlFrom_Select" AutoPostBack="true"
                        DataValueField="DateID" DataTextField="Name" runat="server" Width="200px">
                        <asp:ListItem Text="" Value="" />
                    </asp:DropDownList>
                  
                    <asp:Label ID="lblFrom" runat="server"  style="padding-left:5px;padding-right:5px;"/>
                    <asp:Button ID="btnAdd" runat="server" CssClass="btn btn-success btn-sm btn-sect" Text="New Change" OnClientClick="WindowAdd.show();return false;" >
                    </asp:Button>
                </td>
                <td>
                </td>
                <td>
                    <ext:Button ID="btnLoad" Cls="btn btn-default btn-sm btn-sect" runat="server" Icon="ApplicationAdd" Width="100" Text="Load">
                        <DirectEvents>
                            <Click OnEvent="btnSave_DirectClick">
                                <EventMask ShowMask="true" />
                            </Click>
                        </DirectEvents>
                        <Listeners>
                            <Click Handler="">
                            </Click>
                        </Listeners>
                    </ext:Button>
                </td>
                <td style='padding-left: 10px'>
                    <asp:LinkButton ID="btnExport" runat="server" Text="Import from Excel" OnClientClick="return isPayrollSelected(this)"
                        CssClass=" excel marginRight tiptip" Style="float: left;" />
                </td>
            </tr>
        </table>
    </div>
    <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
    <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
    <div style="clear: both" runat="server" id="gridContainer">
    </div>
    <ext:GridPanel ID="gridProjects" ClicksToEdit="1" Border="true" StripeRows="true"
        Header="false" runat="server" Height="500" Width="1177px" Title="Title" Cls="gridtbl">
        <Store>
            <ext:Store runat="server" ID="storeProjects">
                <Reader>
                    <ext:ArrayReader>
                    </ext:ArrayReader>
                </Reader>
                <Model>
                    <ext:Model ID="ModelProject" runat="server">
                        <Fields>
                            <ext:ModelField Name="Name" Type="String" />
                        </Fields>
                    </ext:Model>
                </Model>
            </ext:Store>
        </Store>
        <Listeners>
        </Listeners>
        <SelectionModel>
            <ext:RowSelectionModel ID="RowSelectionModel11" runat="server">
            </ext:RowSelectionModel>
        </SelectionModel>
        <ColumnModel runat="server" ID="columnModelProjects">
            <Columns>
                <ext:Column runat="server" Sortable="false" MenuDisabled="true" Text="&nbsp;" ID="ColumnFirst"
                    Width="120" ColumnID="Name" DataIndex="Name">
                </ext:Column>
            </Columns>
        </ColumnModel>
    </ext:GridPanel>
    <ext:Window ID="WindowAdd" runat="server" Title="Structure Change - Name and Date" Icon="Application"
        Width="450" Height="180" BodyPadding="10" Hidden="true" Modal="true">
        <Items>
            <ext:Container ID="Container1" runat="server">
                <Content>
                    <table>
                        <tr>
                            <td>
                                <ext:TextField ID="txtName" FieldLabel="Name" runat="server" LabelAlign="Top" Width="180"
                                    LabelSeparator="">
                                </ext:TextField>
                                <asp:RequiredFieldValidator ErrorMessage="Please type a friendly name." Display="None" ID="RequiredFieldValidator10"
                                    ControlToValidate="txtName" ValidationGroup="InsertUpdate" runat="server">
                                </asp:RequiredFieldValidator>
                            </td>
                            <td style="padding-left: 10px">
                                <pr:CalendarExtControl Width="180px" FieldLabel="Effective From" ID="calEffectiveFrom"
                                    runat="server" LabelAlign="Top" LabelSeparator="">
                                </pr:CalendarExtControl>
                                <asp:RequiredFieldValidator ErrorMessage="Plese select an effective date." Display="None"
                                    ID="RequiredFieldValidator1" ControlToValidate="calEffectiveFrom" ValidationGroup="InsertUpdate"
                                    runat="server">
                                </asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:30px"></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <ext:Button  runat="server"  ID="btnChangeScale"  Cls="btn btn-primary"
                                   Text="<i></i>Save Scale" >
                                    <DirectEvents>
                                        <Click OnEvent="btnChangeScale_Click">
                                            <EventMask ShowMask="true" />
                                        </Click>
                                    </DirectEvents>
                                    <Listeners>
                                        <Click Handler="valGroup='InsertUpdate';return CheckValidation();" />
                                    </Listeners>
                                </ext:Button>
                            </td>
                        </tr>
                    </table>
                </Content>
            </ext:Container>
        </Items>
    </ext:Window>
</div>
