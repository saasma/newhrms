﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TrainingCtrlView.ascx.cs"
    Inherits="Web.NewHR.UserControls.TrainingCtrlView" %>

<script type="text/javascript">
    var prepareDownloadTraining = function (grid, toolbar, rowIndex, record) {
        var downloadBtn = toolbar.items.get(0);
        if (record.data.ServerFileName == null || record.data.ServerFileName == '') {
            downloadBtn.setVisible(false);
        }
    }
     
    var CommandHandlerTrF = function (command, record) {
        <%= hdnTrainingIdValue.ClientID %>.setValue(record.data.TrainingId);

        <%= btnDownloadTrFile.ClientID %>.fireEvent('click');
    };
   
</script>

<ext:Hidden ID="hdnTrainingIdValue" runat="server" />
<ext:Button runat="server" AutoPostBack="true" OnClick="btnDownloadTrFile_Click"
    Hidden="true" ID="btnDownloadTrFile" Text="<i></i>Download">
</ext:Button>

<ext:GridPanel ID="GridTraining" runat="server" Width="920" Cls="itemgrid" Scroll="None">
    <Store>
        <ext:Store ID="StoreTraining" runat="server">
            <Model>
                <ext:Model ID="Model1" runat="server" IDProperty="TrainingId">
                    <Fields>
                        <ext:ModelField Name="TrainingName" Type="string" />
                        <ext:ModelField Name="TrainingTypeName" Type="String" />
                        <ext:ModelField Name="InstitutionName" Type="string" />
                        <ext:ModelField Name="Country" Type="string" />
                        <ext:ModelField Name="Duration" Type="string" />
                        <ext:ModelField Name="DurationTypeName" Type="string" />
                        <ext:ModelField Name="TrainingFrom" Type="string" />
                        <ext:ModelField Name="TrainingTo" Type="string" />
                        <ext:ModelField Name="IsEditable" Type="Int" />
                        <ext:ModelField Name="ServerFileName" Type="String" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
    </Store>
    <ColumnModel>
        <Columns>
            <ext:Column ID="Column1" runat="server" Text="Training Name" DataIndex="TrainingName"
                Width="200" Wrap="true" Flex="1" />
            <ext:Column ID="Column2" runat="server" Text="Training Type" DataIndex="TrainingTypeName"
                Width="160" Wrap="true">
            </ext:Column>
            <ext:Column ID="Column3" runat="server" Text="Institute" DataIndex="InstitutionName"
                Width="160" Wrap="true">
            </ext:Column>
            <ext:Column ID="Column4" runat="server" Text="Duration" DataIndex="Duration" Width="120"
                Wrap="true">
                <Renderer Fn="renderDuration" />
            </ext:Column>
            <ext:Column ID="Column6" runat="server" Text="Start Date" DataIndex="TrainingFrom"
                Width="120">
            </ext:Column>
            <ext:Column ID="Column5" runat="server" Text="End Date" DataIndex="TrainingTo" Width="120">
            </ext:Column>
            <ext:CommandColumn ID="CommandColumn1" runat="server" Width="40">
                <Commands>
                    <ext:GridCommand Icon="PageWhitePut" CommandName="DownLoad">
                        <ToolTip Text="DownLoad" />
                    </ext:GridCommand>
                </Commands>
                <Listeners>
                    <Command Handler="CommandHandlerTrF(command,record);" />
                </Listeners>
                <PrepareToolbar Fn="prepareDownloadTraining" />
            </ext:CommandColumn>
        </Columns>
    </ColumnModel>
    <SelectionModel>
        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
    </SelectionModel>
</ext:GridPanel>
