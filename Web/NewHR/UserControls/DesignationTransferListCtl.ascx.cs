﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using Utils.Helper;

namespace Web.NewHR.UserControls
{
    public partial class DesignationTransferListCtl : BaseUserControl
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        public void Initialise()
        {
            LoadLevels();
        }

        private void LoadLevels()
        {
            int total = 0;

            int employeeId = UrlHelper.GetIdFromQueryString("ID");



            GridLevels.Store[0].DataSource = NewHRManager.GetDesignationTransfers(0, 999, "", ref total, employeeId,null,null);
            GridLevels.Store[0].DataBind();

            

        }

              
        

    }
}