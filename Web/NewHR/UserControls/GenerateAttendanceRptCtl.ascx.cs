﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using Utils.Helper;

namespace Web.NewHR.UserControls
{
    public partial class GenerateAttendanceRptCtl : BaseUserControl
    {

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack && !X.IsAjaxRequest)
            {
                DispDate();
            }
        }

        public void Clear()
        {
            txtFromDate.Clear();
            txtToDate.Clear();
            cmbSearch.Clear();
            chkUpdateOnlyEmployee.Clear();
            //string strlastDateCtl = dispLastDate.ClientID;
            //AttendanceEmployeeDate _AttendanceEmployeeDate = AttendanceManager.GetAttendanceGeneratedLastDate();
          //  string dateRange = String.Format("{0: MMMM d, yyyy}", _AttendanceEmployeeDate.Date.Value) + "-" + String.Format("{0:MMMM d, yyyy}", _AttendanceEmployeeDate.LastDate);
            //X.AddScript(strlastDateCtl + ".innerHTML ='" + dateRange + "'");
            //dispLastDate.InnerHtml = String.Format("{0: MMMM d, yyyy}", _AttendanceEmployeeDate.Date.Value) + " - " + String.Format("{0:MMMM d, yyyy}", _AttendanceEmployeeDate.LastDate);
        }

        protected void DispDate()
        {
            AttendanceEmployeeDate _AttendanceEmployeeDate = AttendanceManager.GetAttendanceGeneratedLastDate();
            if (_AttendanceEmployeeDate != null)
            dispLastDate.InnerHtml = String.Format("{0: MMMM d, yyyy}", _AttendanceEmployeeDate.Date.Value) + " - " + String.Format("{0:MMMM d, yyyy}", _AttendanceEmployeeDate.LastDate);

        }

        protected void btnGenerate_Click(object sender, EventArgs e)
        {
            int empId = -1;
            DateTime start, end;

            if (txtFromDate.SelectedDate == txtFromDate.EmptyDate)
            {
                NewMessage.ShowNormalMessage("From date is required.");
                return;
            }

            if (txtToDate.SelectedDate == txtToDate.EmptyDate)
            {
                NewMessage.ShowNormalMessage("To date is required.");
                return;
            }

            start = txtFromDate.SelectedDate.Date;
            end = txtToDate.SelectedDate.Date;


            if (start > end)
            {
                NewMessage.ShowWarningMessage("From date can not be greater than To date.");
                return;
            }


            if (cmbSearch.SelectedItem != null && cmbSearch.SelectedItem.Value != null)
            {
                empId = int.Parse(cmbSearch.SelectedItem.Value);
            }

            if (chkUpdateOnlyEmployee.Checked == false)
                empId = -1;

            AttendanceManager.GenerateAttendanceEmployeeTable(empId, start, end, false);

            AttendanceEmployeeDate _AttendanceEmployeeDate = AttendanceManager.GetAttendanceGeneratedLastDate();
            if (_AttendanceEmployeeDate != null)
            {
                string dateRange = String.Format("{0: MMMM d, yyyy}", _AttendanceEmployeeDate.Date.Value) + "-" + String.Format("{0:MMMM d, yyyy}", _AttendanceEmployeeDate.LastDate);
                string script = "RefreshProceedDate('" + dateRange + "');";

                NewMessage.ShowNormalMessage("Attendance report regenerated for the given parameters.", script + "refreshparentWindow();");
            }
            return;
        }

    }
}