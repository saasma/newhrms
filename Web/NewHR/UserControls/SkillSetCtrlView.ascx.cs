﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;

namespace Web.NewHR.UserControls
{
    public partial class SkillSetCtrlView : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        protected void Initialise()
        {
            int EmployeeID = GetEmployeeID();
            this.LoadSkillSetsGrid(EmployeeID);
        }

        protected void LoadSkillSetsGrid(int EmployeeID)
        {
            List<NewHREmployeeSkillSetBO> _EmployeeSkillSetBO = NewHRManager.GetEmployeesSkillSet(EmployeeID);

            this.StoreSkillSets.DataSource = _EmployeeSkillSetBO;
            this.StoreSkillSets.DataBind();

            if (_EmployeeSkillSetBO.Count <= 0)
                GridSkillSets.Hide();
        }


        protected int GetEmployeeID()
        {
            int EmpID = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                EmpID = int.Parse(Request.QueryString["ID"]);
            }
            else
            {
                EmpID = SessionManager.CurrentLoggedInEmployeeId;
            }
            return EmpID;
        }

    }
}