﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SeminarPopUpCtrl.ascx.cs" Inherits="Web.NewHR.UserControls.SeminarPopUpCtrl" %>
<ext:Hidden runat="server" ID="hdnSeminarID">
</ext:Hidden>


 <table class="fieldTable">
            <tr>
                <td>
                    <ext:TextField ID="txtOrganizer" Width="180" runat="server" FieldLabel="Organizer" LabelAlign="top"
                        LabelSeparator="" />
                    <asp:RequiredFieldValidator Display="None" ID="valtxtOrganizer" runat="server" ValidationGroup="SeminarSaveUpdate"
                        ControlToValidate="txtOrganizer" ErrorMessage="Organizer is required." />
                </td>
            </tr>
            <tr>
                <td>
                    <table class="fieldTable firsttdskip">
                        <tr>
                            <td>
                                <ext:ComboBox ID="cmbCountry"  Width="180" runat="server" ValueField="CountryName" DisplayField="CountryName"
                                    FieldLabel="Country" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                                    QueryMode="Local">
                                    <Store>
                                        <ext:Store ID="Store2" runat="server">
                                            <Model>
                                                <ext:Model ID="Model4" runat="server">
                                                    <Fields>
                                                        <ext:ModelField Name="CountryName" />
                                                        <ext:ModelField Name="CountryName" />
                                                    </Fields>
                                                </ext:Model>
                                            </Model>
                                        </ext:Store>
                                    </Store>
                                </ext:ComboBox>
                                <asp:RequiredFieldValidator Display="None" ID="valcmbCountry" runat="server" ValidationGroup="SeminarSaveUpdate"
                                    ControlToValidate="cmbCountry" ErrorMessage="Country is required." />
                            </td>
                            <td>
                                <ext:TextField ID="txtPlace"  Width="180" runat="server" FieldLabel="Place" LabelAlign="top" LabelSeparator="" />
                                <asp:RequiredFieldValidator Display="None" ID="valtxtPlace" runat="server" ValidationGroup="SeminarSaveUpdate"
                                    ControlToValidate="txtPlace" ErrorMessage="Place is required." />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <table class="fieldTable firsttdskip">
                        <tr>
                            <td>
                                <ext:TextField ID="txtDuration" runat="server" FieldLabel="Duration" LabelAlign="top"
                                    Width="85" LabelSeparator="" MaskRe="[0-9]|\.|%" />
                                <asp:RequiredFieldValidator Display="None" ID="valtxtDuration" runat="server" ValidationGroup="SeminarSaveUpdate"
                                    ControlToValidate="txtDuration" ErrorMessage="Duration  is required." />
                            </td>
                            <td style="padding-top: 5px">
                                <ext:ComboBox ID="cmbDurationType" runat="server" ValueField="Value" DisplayField="Text"
                                    Width="80" FieldLabel="&nbsp" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                                    QueryMode="Local">
                                    <Items>
                                        <ext:ListItem Text="Year" Value="1" />
                                        <ext:ListItem Text="Month" Value="2" />
                                        <ext:ListItem Text="Week" Value="3" />
                                        <ext:ListItem Text="Day" Value="4" />
                                    </Items>
                                    <%-- <Store>
                                            <ext:Store ID="Store1" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model2" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="Value" />
                                                            <ext:ModelField Name="Text" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>--%>
                                </ext:ComboBox>
                                <asp:RequiredFieldValidator Display="None" ID="valcmbDurationType" runat="server"
                                    ValidationGroup="SeminarSaveUpdate" ControlToValidate="cmbDurationType" ErrorMessage="Duration Type is required." />
                            </td>
                            <td>
                                <pr:CalendarExtControl FieldLabel="Start Date" Width="180px" ID="calStartDate" runat="server"
                                    LabelSeparator="" LabelAlign="Top" />
                                <asp:RequiredFieldValidator Display="None" ID="valcalStartDate" runat="server" ValidationGroup="SeminarSaveUpdate"
                                    ControlToValidate="calStartDate" ErrorMessage="Start Date is required." />
                            </td>
                            <td>
                                <pr:CalendarExtControl FieldLabel="End Date" Width="180px" ID="calEndDate" runat="server"
                                    LabelSeparator="" LabelAlign="Top" />
                                <asp:RequiredFieldValidator Display="None" ID="valcalEndDate" runat="server" ValidationGroup="SeminarSaveUpdate"
                                    ControlToValidate="calEndDate" ErrorMessage="End Date is required." />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <ext:TextArea ID="txtNote" runat="server" FieldLabel="Notes" LabelSeparator="" LabelAlign="Top"
                        Rows="4" Cols="55" />
                </td>
            </tr>
            <tr>
                <td valign="bottom" colspan="2">
                    <div class="popupButtonDiv">
                        <ext:Button runat="server" Cls="btn btn-primary" ID="btnSave"
                           Text="<i></i>Save">
                            <DirectEvents>
                                <Click OnEvent="btnSave_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                            <Listeners>
                                <Click Handler="valGroup = 'SeminarSaveUpdate'; if(CheckValidation()) return this.disable(); else return false;">
                                </Click>
                            </Listeners>
                        </ext:Button>
                        <div class="btnFlatOr">
                            or</div>
                        <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="LinkButton2" Cls="btnFlatLeftGap"
                            Text="<i></i>Cancel">
                            <Listeners>
                                <Click Handler="#{AESeminarWindow}.hide();">
                                </Click>
                            </Listeners>
                        </ext:LinkButton>
                    </div>
                </td>
            </tr>
        </table>