﻿<%@ Page Title="Other Details" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="OtherHRDetails.aspx.cs" Inherits="Web.NewHR.OtherHRDetails" %>

<%@ Register Src="~/newhr/UserControls/EmployeeWizard.ascx" TagName="EmployeeWizard"
    TagPrefix="uc2" %>
<%@ Register Src="~/NewHR/UserControls/PhotographCtl.ascx" TagName="PhotographCtl"
    TagPrefix="ucPhotographCtl" %>
<%@ Register Src="~/NewHR/UserControls/EmployeeDetailsCtl.ascx" TagName="EmployeeDetailsCtl"
    TagPrefix="uc1" %>
<%@ Register Src="~/NewHR/UserControls/CitizenshipCtl.ascx" TagName="CitizenshipCtl"
    TagPrefix="ucCitizenshipCtl" %>
<%@ Register Src="~/NewHR/UserControls/DrivingLiscenceCtl.ascx" TagName="DrivingLiscenceCtl"
    TagPrefix="ucDrivingLiscenceCtl" %>
<%@ Register Src="~/NewHR/UserControls/PassportCtl.ascx" TagName="PassportCtl" TagPrefix="ucPassportCtl" %>
<%@ Register Src="~/NewHR/UserControls/EmpSignatureCtrl.ascx" TagName="SignatureCtl"
    TagPrefix="ucSig" %>
<%@ Register Src="~/NewHR/UserControls/EmpDetailsLeftMenu.ascx" TagName="EmpDetailsWizard"
    TagPrefix="ucEW" %>
<%@ Register Src="~/NewHR/UserControls/NomineeCtrl.ascx" TagName="HrNominee" TagPrefix="HrN" %>
<%@ Register Src="~/NewHR/UserControls/NonMonetaryAwardCtrl.ascx" TagName="NMAward"
    TagPrefix="NMA" %>
<%@ Register Src="~/NewHR/UserControls/HobbyCtrl.ascx" TagName="EHobbyControl" TagPrefix="EHC" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .userDetailsClass
        {
            background-color: #D8E7F3;
            height: 50px;
            text-align: center;
            padding-top: 10px;
            margin-bottom: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body" style="margin-left: 145px;">
                <h4>
                    Other HR Details
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <%-- <uc2:EmployeeWizard Id="EmployeeWizard1" runat="server" />--%>
        <table>
            <tr>
                <td valign="top">
                    <div style="float: left; margin-right: 25px;">
                        <ucEW:EmpDetailsWizard Id="EmpDetailsWizard1" runat="server" />
                    </div>
                </td>
                <td valign="top" style="width: 100%;">
                    <div class="userDetailsClass">
                        <uc1:EmployeeDetailsCtl Id="EmployeeDetailsCtl1" runat="server" />
                    </div>
                    <div style="width: 845px; border: 1px solid #428BCA; margin-bottom: 15px;">
                        <div>
                            <!-- panel-btns -->
                            <h4 class="panel-title sectionHeading" style="margin-left: 0px;">
                                <i></i>Other HR Details
                            </h4>
                        </div>
                        <!-- panel-heading -->
                        <div class="panel-body" style="margin-top: 15px;">
                            <table class="fieldTable firsttdskip">
                                <tr>
                                    <td>
                                        <ext:ComboBox ID="cmbEmployee" ForceSelection="true" runat="server" Width="400" FieldLabel="Reports To"
                                            LabelAlign="Left" LabelWidth="140" QueryMode="Local" DisplayField="NameEIN" ValueField="EmployeeId">
                                            <Store>
                                                <ext:Store ID="storeEmployeeList" runat="server">
                                                    <Fields>
                                                        <ext:ModelField Name="EmployeeId" Type="String" />
                                                        <ext:ModelField Name="NameEIN" />
                                                    </Fields>
                                                </ext:Store>
                                            </Store>
                                            <Triggers>
                                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                            </Triggers>
                                            <Listeners>
                                                <Select Handler="this.getTrigger(0).show();" />
                                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                                <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                                            </Listeners>
                                        </ext:ComboBox>
                                        <%--<asp:RequiredFieldValidator ID="valReqd" ControlToValidate="cmbEmployee" Display="None"
                                ErrorMessage="Employee Manager is required" runat="server" ValidationGroup="savecode" />--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ext:TextField ID="txtProjectCategory" runat="server" StyleSpec="margin-top:10px"
                                            Width="400" FieldLabel="Project Category" LabelAlign="Left" LabelWidth="140">
                                        </ext:TextField>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ext:ComboBox ID="cmbCostCode"  StyleSpec="margin-top:10px" ForceSelection="true" runat="server" Width="400" FieldLabel="Cost Code"
                                            LabelAlign="Left" LabelWidth="140" QueryMode="Local" DisplayField="Name" ValueField="CostCodeId">
                                            <Store>
                                                <ext:Store ID="store1" runat="server">
                                                    <Fields>
                                                        <ext:ModelField Name="Name" Type="String" />
                                                        <ext:ModelField Name="CostCodeId"  Type="String" />
                                                    </Fields>
                                                </ext:Store>
                                            </Store>
                                        </ext:ComboBox>
                                        <asp:RequiredFieldValidator ID="valReqdCostCode" Visible="false" ControlToValidate="cmbCostCode"
                                            Display="None" ErrorMessage="Cost code is required." runat="server" ValidationGroup="savecode" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ext:ComboBox ID="cmbProgramme"  StyleSpec="margin-top:10px" ForceSelection="true" runat="server" Width="400" FieldLabel="Programme"
                                            LabelAlign="Left" LabelWidth="140" QueryMode="Local" DisplayField="Name" ValueField="ProgrammeId">
                                            <Store>
                                                <ext:Store ID="store2" runat="server">
                                                    <Fields>
                                                        <ext:ModelField Name="Name" Type="String" />
                                                        <ext:ModelField Name="ProgrammeId"  Type="String" />
                                                    </Fields>
                                                </ext:Store>
                                            </Store>
                                        </ext:ComboBox>
                                        <asp:RequiredFieldValidator ID="reqdProgram" Visible="false" ControlToValidate="cmbProgramme"
                                            Display="None" ErrorMessage="Program is required." runat="server" ValidationGroup="savecode" />
                                    </td>
                                </tr>
                                <tr>
                                <td>
                                  <ext:TextField ID="txtAccountCode" runat="server" StyleSpec="margin-top:10px"
                                            Width="400" FieldLabel="Account Code" LabelAlign="Left" LabelWidth="140">
                                        </ext:TextField>
                                </td>
                                </tr>
                                 <tr>
                                    <td>
                                        <ext:ComboBox Visible="false"  ID="cmbHPLVoucherGroup"  StyleSpec="margin-top:10px" ForceSelection="true" runat="server" Width="400" FieldLabel="Voucher Group"
                                            LabelAlign="Left" LabelWidth="140" QueryMode="Local" DisplayField="Name" ValueField="VoucherDepartmentID">
                                            <Store>
                                                <ext:Store ID="store3" runat="server">
                                                    <Fields>
                                                        <ext:ModelField Name="Name" Type="String" />
                                                        <ext:ModelField Name="VoucherDepartmentID"  Type="String" />
                                                    </Fields>
                                                </ext:Store>
                                            </Store>
                                            
                                        </ext:ComboBox>
                                        <asp:RequiredFieldValidator   ID="reqdVoucherGroup" Visible="false" ControlToValidate="cmbHPLVoucherGroup"
                                            Display="None" ErrorMessage="Voucher Group is required." runat="server" ValidationGroup="savecode" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-top: 20px;padding-bottom:20px;">
                                        <ext:Button runat="server" Text="Save Changes" ID="btnUpload" Cls="btn btn-primary btn-sect left">
                                            <DirectEvents>
                                                <Click OnEvent="btnUpload_Click">
                                                    <EventMask ShowMask="true" />
                                                </Click>
                                            </DirectEvents>
                                            <Listeners>
                                                <Click Handler="valGroup= 'savecode'; return CheckValidation();">
                                                </Click>
                                            </Listeners>
                                        </ext:Button>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <h4 class="heading nogap">
                        Nominees
                    </h4>
                    <table class="table table-bordered table-condensed table-striped table-primary">
                        <tr>
                            <td>
                                <HrN:HrNominee Id="HrNom" runat="server" />
                            </td>
                        </tr>
                    </table>
                    <h4 class="heading nogap">
                        Non Monetary Award
                    </h4>
                    <table class="table table-bordered table-condensed table-striped table-primary">
                        <tr>
                            <td>
                                <NMA:NMAward Id="NMAwardCtrl" runat="server" />
                            </td>
                        </tr>
                    </table>
                    <h4 class="heading nogap">
                        Hobby
                    </h4>
                    <table class="table table-bordered table-condensed table-striped table-primary">
                        <tr>
                            <td>
                                <EHC:EHobbyControl Id="EHobCtrl" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
