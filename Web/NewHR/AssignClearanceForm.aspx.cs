﻿using BLL;
using BLL.Manager;
using Ext.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Web.NewHR
{
    public partial class AssignClearanceForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest && !IsPostBack)
            {
                Initialize();
            }
        }

        protected void Initialize()
        {
            txtFormName.Text = "";
            chkLoanDetail.Value = false;
            chkAdvanceDetail.Value = false;
            chkRefundableDeposit.Value = false;
            chkAssetInCustody.Value = false;
            var userList = CommonManager.GetAllUsers();
            cmbType.Store[0].DataSource = userList;
            cmbType.Store[0].DataBind();
            
            //var clearanceDetailTypes = CommonManager.GetAllClearanceDetailTypes();
            //foreach (var detail in clearanceDetailTypes)
            //{
            //    X.GetCmp<FieldSet>("chkFormDetail").Add(new Ext.Net.Checkbox
            //    {
            //        BoxLabel = detail.ClearanceDetailType,
            //        ID = detail.ID.ToString(),
            //    });
            //}
        }



        protected void btnSave(object sender, DirectEventArgs e)
        {
            List<int> detailToShowInClearanceFormID = new List<int>();
            if (chkLoanDetail.Value.Equals(true))
            {
                detailToShowInClearanceFormID.Add((int)ClearanceDetailToShow.LoanDetail);
            }
            if (chkAdvanceDetail.Value.Equals(true))
            {
                detailToShowInClearanceFormID.Add((int)ClearanceDetailToShow.AdvanceDetail);
            }
            if (chkRefundableDeposit.Value.Equals(true))
            {
                detailToShowInClearanceFormID.Add((int)ClearanceDetailToShow.RefundableDeposit);
            }
            if (chkAssetInCustody.Value.Equals(true))
            {
                detailToShowInClearanceFormID.Add((int)ClearanceDetailToShow.AssetInCustody);
            }
            CommonManager.SaveClearanceFormDetail(txtFormName.Text, detailToShowInClearanceFormID, cmbType.Value.ToString());
            Initialize();
        }
    }
}