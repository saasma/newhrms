﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/HR.Master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="Web.NewHR.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript">
    window.location = 'DashboardHR.aspx';
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <ul class="breadcrumb">
        <li><a href="index.html?lang=en" class="glyphicons home"><i></i>Smashing</a></li>
        <li class="divider"></li>
        <li>Bookings</li>
    </ul>
    <div class="separator bottom">
    </div>
    <div class="heading-buttons" style="margin-top: -15px; margin-bottom: -3px;">
        <h3>
            Bookings</h3>
        <div class="buttons pull-right">
            <a href="" class="btn btn-primary btn-icon glyphicons circle_plus"><i></i>Add booking</a>
        </div>
    </div>
    <div class="separator bottom">
    </div>
    <div class="innerLR">
        main content
    </div>
    <br />
</asp:Content>
