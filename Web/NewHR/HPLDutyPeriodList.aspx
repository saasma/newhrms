﻿<%@ Page Title="Duty Period List" Language="C#" MasterPageFile="~/Master/NewDetails.Master" AutoEventWireup="true" CodeBehind="HPLDutyPeriodList.aspx.cs" Inherits="Web.NewHR.HPLDutyPeriodList" %>


<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">

  <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Duty Schedule Periods
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
       
        <table>
            <tr>
                <td>
                    <ext:Store ID="storeYear" runat="server">
                        <Model>
                            <ext:Model ID="modelYear" runat="server">
                                <Fields>
                                    <ext:ModelField Name="Text" Type="String" />
                                    <ext:ModelField Name="Value" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                    <ext:ComboBox ID="cmbYear" runat="server" FieldLabel="" Width="100" DisplayField="Text" EmptyText="Year" MarginSpec="13 0 0 0"
                        ValueField="Value" StoreID="storeYear" Editable="false"  QueryMode="Local">
                    </ext:ComboBox>
                     <asp:RequiredFieldValidator Display="None" ID="rfvYear" runat="server" ValidationGroup="SaveUpdate"
                            ControlToValidate="cmbYear" ErrorMessage="Please select year." />

                </td>
                <td style="padding-top: 14px;">
                    <ext:Button ID="btnGenerate" runat="server" Cls="btn btn-save" Text="Generate Periods" MarginSpec="0 0 0 20" >
                       <DirectEvents>
                            <Click OnEvent="btnGenerate_Click">
                                <EventMask ShowMask="true" />
                            </Click>
                       </DirectEvents>
                       <Listeners>
                            <Click Handler="valGroup = 'SaveUpdate'; if(CheckValidation()) return ''; else return false;">
                            </Click>
                        </Listeners>
                    </ext:Button>
                </td>
            </tr>
        </table>
        <br />
       
        <ext:GridPanel StyleSpec="margin-top:5px;" ID="gridSchedule" runat="server" Cls="itemgrid" Width="520">
            <Store>
                <ext:Store ID="storeSchedule" runat="server">
                    <Model>
                        <ext:Model ID="Model6" runat="server" IDProperty="PeriodId">
                            <Fields>
                                <ext:ModelField Name="PeriodId" Type="String" />
                                <ext:ModelField Name="Year" Type="String" />
                                <ext:ModelField Name="MonthName" Type="String" />
                                <ext:ModelField Name="PeriodName" Type="String" />
                                <ext:ModelField Name="WeekNo" Type="String" />
                                <ext:ModelField Name="StartDate" Type="Date" />
                                <ext:ModelField Name="EndDate" Type="Date" />
                                
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>
            <ColumnModel>
                <Columns>
                    <ext:Column ID="Column2" Sortable="false" MenuDisabled="true" runat="server" Text="Month" 
                        Width="100" Align="Left" DataIndex="MonthName">
                    </ext:Column>
                    <ext:Column ID="colEmployeeName" Sortable="false" MenuDisabled="true" runat="server" 
                        Text="Period" Width="100" Align="Left" DataIndex="PeriodName">
                    </ext:Column>
                    <ext:Column ID="Column3" Sortable="false" MenuDisabled="true" runat="server"
                        Text="Week No" Width="100" Align="Center" DataIndex="WeekNo">
                    </ext:Column>
                    <ext:DateColumn ID="colStartDate" runat="server"  Width="110" Align="Left" Text="Start Date"
                        MenuDisabled="true" Sortable="false" Format="yyyy-MM-dd" DataIndex="StartDate">
                    </ext:DateColumn>
                    <ext:DateColumn ID="DateColumn1" runat="server" Width="110" Align="Left" Text="End Date"
                        MenuDisabled="true" Sortable="false" Format="yyyy-MM-dd" DataIndex="EndDate">
                    </ext:DateColumn>

                </Columns>
            </ColumnModel>
            <SelectionModel>
                <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
            </SelectionModel>           
            <Plugins>
                <ext:CellEditing ID="CellEditing1" ClicksToEdit="1" runat="server">
                    <%--<Listeners>                              
                        <Edit Fn="afterEdit" />
                    </Listeners>--%>
                </ext:CellEditing>
            </Plugins>
           
        </ext:GridPanel>
        <br />
    </div>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
