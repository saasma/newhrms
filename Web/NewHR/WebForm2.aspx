﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm2.aspx.cs" Inherits="Web.NewHR.WebForm2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ Register Assembly="DayPilot" Namespace="DayPilot.Web.Ui" TagPrefix="DayPilot" %>

<script type="text/javascript">

    /* Fast filter helpers */

    function clear() {
        var filterBox = document.getElementById("TextBoxFilter");
        filterBox.value = '';
        filter();
    }
    function filter() {
        var filterBox = document.getElementById("TextBoxFilter");
        var filterText = filterBox.value;

        dps1.clientState = { "filter": filterText };
        dps1.commandCallBack("filter");
    }

    function key(e) {
        var keynum = (window.event) ? event.keyCode : e.keyCode;

        if (keynum === 13) {
            filter();
            return false;
        }

        return true;
    }

    /* Event editing helpers - modal dialog */
    function dialog() {
        var modal = new DayPilot.Modal();
        modal.top = 60;
        modal.width = 300;
        modal.opacity = 70;
        modal.border = "10px solid #d0d0d0";
        modal.closed = function () {
            if (this.result == "OK") {
                dps1.commandCallBack('refresh');
            }
            dps1.clearSelection();
        };

        modal.height = 250;
        modal.zIndex = 100;
        return modal;
    }

    function timeRangeSelected(start, end, resource) {
        var modal = dialog();
        modal.showUrl("New.aspx?start=" + start.toStringSortable() + "&end=" + end.toStringSortable() + "&r=" + resource + "&hash=<%= PageHash %>");
    }

    function eventClick(e) {
        var modal = dialog();
        modal.showUrl("Edit.aspx?id=" + e.value() + "&hash=<%= PageHash %>");
    }

</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <style type="text/css">
    
    </style>
    

<script type="text/javascript">

    /* Fast filter helpers */

    function clear() {
        var filterBox = document.getElementById("TextBoxFilter");
        filterBox.value = '';
        filter();
    }
    function filter() {
        var filterBox = document.getElementById("TextBoxFilter");
        var filterText = filterBox.value;

        dps1.clientState = { "filter": filterText };
        dps1.commandCallBack("filter");
    }

    function key(e) {
        var keynum = (window.event) ? event.keyCode : e.keyCode;

        if (keynum === 13) {
            filter();
            return false;
        }

        return true;
    }

    /* Event editing helpers - modal dialog */
    function dialog() {
        var modal = new DayPilot.Modal();
        modal.top = 60;
        modal.width = 300;
        modal.opacity = 70;
        modal.border = "10px solid #d0d0d0";
        modal.closed = function () {
            if (this.result == "OK") {
                dps1.commandCallBack('refresh');
            }
            dps1.clearSelection();
        };

        modal.height = 250;
        modal.zIndex = 100;
        return modal;
    }

    function timeRangeSelected(start, end, resource) {
        var modal = dialog();
        modal.showUrl("New.aspx?start=" + start.toStringSortable() + "&end=" + end.toStringSortable() + "&r=" + resource + "&hash=<%= PageHash %>");
    }

    function eventClick(e) {
        var modal = dialog();
        modal.showUrl("Edit.aspx?id=" + e.value() + "&hash=<%= PageHash %>");
    }

</script>
</head>
<body>
    <form id="form1" runat="server">
      <ext:ResourceManager ID="ResourceManager1" runat="server" DisableViewState="false"
        ShowWarningOnAjaxFailure="false" ScriptMode="Release" Namespace="CompanyX" />
        <ext:Button runat="server" Text="Click" Width="100" />
    <DayPilot:DayPilotScheduler 
        ID="DayPilotScheduler1" 
        runat="server" 
        DataStartField="start" 
        DataEndField="end" 
        DataTextField="name" 
        DataValueField="id" 
        DataResourceField="column" 
        DataTagFields="id, name"
         
         
        
         
        Width="100%" 
        RowHeaderWidth="120"
        CellDuration="1440" 
        CellGroupBy="Month" 
        CellWidth="60"
        TimeRangeSelectedHandling="JavaScript"
        TimeRangeSelectedJavaScript="timeRangeSelected(start, end, resource)"
        ClientObjectName="dps1" 
        EventMoveHandling="CallBack" 
        OnEventMove="DayPilotScheduler1_EventMove" 
        EventMoveJavaScript="dps1.eventMoveCallBack(e, newStart, newEnd, newResource);"
        EventResizeHandling="Notify"
        OnEventResize="DayPilotScheduler1_EventResize"
        OnTimeRangeSelected="DayPilotScheduler1_TimeRangeSelected" 
        EventClickHandling="JavaScript"
        EventClickJavaScript="eventClick(e);"
        EventEditHandling="CallBack" 
        OnEventEdit="DayPilotScheduler1_EventEdit" 
        OnBeforeEventRender="DayPilotScheduler1_BeforeEventRender"
        OnEventMenuClick="DayPilotScheduler1_EventMenuClick" 
        xContextMenuID="DayPilotMenu2" 
        ContextMenuResourceID="DayPilotMenuRes"
        BusinessBeginsHour="5" 
        BusinessEndsHour="24"
        AfterRenderJavaScript="afterRender(data, isCallBack);" 
        OnBeforeResHeaderRender="DayPilotScheduler1_BeforeResHeaderRender"
         
        EnableViewState="false" 
        ScrollLabelsVisible="false" 
        BubbleID="DayPilotBubble1" 
        ShowToolTip="false"
        HeightSpec="Max" 
        Height="500" 
        OnCommand="DayPilotScheduler1_Command" 
        OnEventClick="DayPilotScheduler1_EventClick"
        OnTimeRangeMenuClick="DayPilotScheduler1_TimeRangeMenuClick" 
        OnBeforeCellRender="DayPilotScheduler1_BeforeCellRender"
        TreeEnabled="true"
        TreeIndent="15"
        EventDoubleClickHandling="Edit"
        
        xResourceBubbleID="DayPilotBubble1"
        
         
        OnResourceHeaderMenuClick="DayPilotScheduler1_ResourceHeaderMenuClick"
        ResourceHeaderClickHandling="JavaScript" 
        OnResourceHeaderClick="DayPilotScheduler1_ResourceHeaderClick" 
        OnBeforeTimeHeaderRender="DayPilotScheduler1_BeforeTimeHeaderRender"
        ContextMenuSelectionID="DayPilotMenuSelection"
        
        SyncResourceTree="true" 
        DragOutAllowed="true"
        TimeRangeDoubleClickHandling="JavaScript"
        TimeRangeDoubleClickJavaScript="alert('double click');"
        >
        <Resources>
            <DayPilot:Resource Name="Room A" Value="A" Expanded="True">
                <Children>
                    <DayPilot:Resource Name="Room A.1" Value="A.1" Expanded="False" >
                        <Children>
                            <DayPilot:Resource Name="Room A.1.1" Value="A.1.1" Expanded="False" />
                            <DayPilot:Resource Name="Room A.1.2" Value="A.1.2" Expanded="False" />
                        </Children>
                    </DayPilot:Resource>
                    <DayPilot:Resource Name="Room A.2" Value="A.2" Expanded="False" />
                </Children>
            </DayPilot:Resource>
            <DayPilot:Resource Name="Room B" Value="B" Expanded="False" />
            <DayPilot:Resource Name="Room C" Value="C" ToolTip="Test" Expanded="False" />
            <DayPilot:Resource Name="Room D" Value="D" Expanded="False" />
            <DayPilot:Resource Name="Room E" Value="E" Expanded="False" />
            <DayPilot:Resource Name="Room F" Value="F" Expanded="False" />
            <DayPilot:Resource Name="Room G" Value="G" Expanded="False" />
            <DayPilot:Resource Name="Room H" Value="H" Expanded="False" />
            <DayPilot:Resource Name="Room I" Value="I" Expanded="False" />
            <DayPilot:Resource Name="Room J" Value="J" Expanded="False" />
            <DayPilot:Resource Name="Room K" Value="K" Expanded="False" />
            <DayPilot:Resource Name="Room L" Value="L" Expanded="False" />
            <DayPilot:Resource Name="Room M" Value="M" Expanded="False" />
            <DayPilot:Resource Name="Room N" Value="N" Expanded="False" />
            <DayPilot:Resource Name="Room O" Value="O" Expanded="False" />
            <DayPilot:Resource Name="Room P" Value="P" Expanded="False" />
            <DayPilot:Resource Name="Room Q" Value="Q" Expanded="False" />
            <DayPilot:Resource Name="Room R" Value="R" Expanded="False" />
            <DayPilot:Resource Name="Room S" Value="S" Expanded="False" />
            <DayPilot:Resource Name="Room T" Value="T" Expanded="False" />
            <DayPilot:Resource Name="Room U" Value="U" Expanded="False" />
            <DayPilot:Resource Name="Room V" Value="V" Expanded="False" />
            <DayPilot:Resource Name="Room W" Value="W" Expanded="False" />
            <DayPilot:Resource Name="Room X" Value="X" Expanded="False" />
            <DayPilot:Resource Name="Room Y" Value="Y" Expanded="False" />
            <DayPilot:Resource Name="Room Z" Value="Z" Expanded="False" />
       </Resources>
    </DayPilot:DayPilotScheduler>
    
    <DayPilot:DayPilotMenu ID="DayPilotMenu2" runat="server"  UseShadow="false">
        <DayPilot:MenuItem Text="Open" Action="JavaScript" JavaScript="alert('Opening event (id ' + e.value() + ')');">
        </DayPilot:MenuItem>
        <DayPilot:MenuItem Text="Send" Action="JavaScript" JavaScript="alert('Sending event (id ' + e.value() + ')');">
        </DayPilot:MenuItem>
        <DayPilot:MenuItem Text="Refresh" Action="JavaScript" JavaScript="dps1.commandCallBack('refresh');">
        </DayPilot:MenuItem>
        <DayPilot:MenuItem Text="-" Action="NavigateUrl"></DayPilot:MenuItem>
        <DayPilot:MenuItem Text="Delete (CallBack)" Action="Callback" Command="Delete"></DayPilot:MenuItem>
        <DayPilot:MenuItem Action="PostBack" Command="Delete" Text="Delete (PostBack)" />
        <DayPilot:MenuItem Action="JavaScript" JavaScript="alert('x:' + dps1.eventOffset.x + ' y:' + dps1.eventOffset.y + ' resource:' + e.row());" Text="Mouse offset (relative to event)" />
        <DayPilot:MenuItem Action="NavigateUrl" NavigateUrl="javascript:alert('Going somewhere else (id {0})');"
            Text="NavigateUrl test" />
    </DayPilot:DayPilotMenu>
    
    <DayPilot:DayPilotMenu ID="DayPilotMenuSpecial" runat="server" ClientObjectName="cmSpecial"  UseShadow="false"
        >
        <DayPilot:MenuItem Text="Open" Action="JavaScript" JavaScript="alert('Opening event (id ' + e.value() + ')');">
        </DayPilot:MenuItem>
        <DayPilot:MenuItem Text="Send" Action="JavaScript" JavaScript="alert('Sending event (id ' + e.value() + ')');">
        </DayPilot:MenuItem>
        <DayPilot:MenuItem Text="Delete (JavaScript using callback)" Action="JavaScript"
            Command='Delete' JavaScript="if (confirm('Do you really want to delete event ' + e.text() + ' ?')) dps1.eventMenuClickCallBack(e, command);">
        </DayPilot:MenuItem>
    </DayPilot:DayPilotMenu>
    
    <DayPilot:DayPilotMenu ID="DayPilotMenuSelection" runat="server"  >
        <DayPilot:MenuItem Action="JavaScript" JavaScript="dps1.timeRangeSelectedCallBack(e.start, e.end, e.resource);"
            Text="Create new event (JavaScript)" />
        <DayPilot:MenuItem Action="PostBack" Command="Insert" Text="Create new event (PostBack)" />
        <DayPilot:MenuItem Action="CallBack" Command="Insert" Text="Create new event (CallBack)" />
        <DayPilot:MenuItem Text="-" Action="JavaScript"></DayPilot:MenuItem>
        <DayPilot:MenuItem Action="JavaScript" JavaScript="alert('Start: ' + e.start + '\nEnd: ' + e.end + '\nResource id: ' + e.resource);"
            Text="Show selection details" />
        <DayPilot:MenuItem Action="JavaScript" JavaScript="dps1.clearSelection();" Text="Clean selection" />
    </DayPilot:DayPilotMenu>

    <DayPilot:DayPilotMenu ID="DayPilotMenuRes" runat="server" >
        <DayPilot:MenuItem Action="CallBack" Command="Insert" Text="Add child" />
        <DayPilot:MenuItem Text="-" Action="JavaScript"></DayPilot:MenuItem>
        <DayPilot:MenuItem Action="CallBack" Command="Delete" Text="Delete" />
        <DayPilot:MenuItem Action="CallBack" Command="DeleteChildren" Text="Delete children" />
        <DayPilot:MenuItem Text="-" Action="JavaScript"></DayPilot:MenuItem>
        <DayPilot:MenuItem Action="JavaScript" JavaScript="alert(e.name + '\n' + e.value);"
            Text="Show resource details" />
    </DayPilot:DayPilotMenu>

    
    <DayPilot:DayPilotBubble 
        ID="DayPilotBubble1" 
        runat="server" 
        OnRenderContent="DayPilotBubble1_RenderContent" 
        ClientObjectName="bubble" 
        OnRenderEventBubble="DayPilotBubble1_RenderEventBubble"
        Width="250"
        
        Corners="Rounded"
        Position="EventTop"
        
        
        >
    </DayPilot:DayPilotBubble>

    <DayPilot:DayPilotScheduler ID="DayPilotScheduler2" runat="server">
    </DayPilot:DayPilotScheduler>

    <br />
    <br />
    
    <script type="text/javascript">

        /* DayPilotScheduler.AfterRenderJavaScript handler */

        function afterRender(data, isCallBack) {
        }

</script>

    </form>
</body>
</html>
