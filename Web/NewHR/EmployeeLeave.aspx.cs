﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using Utils.Helper;
using Utils;
using Utils.Web;

namespace Web.NewHR
{
    public partial class EmployeeLeave : BasePage
    {
        PayManager mgr = new PayManager();

        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.HumanResource;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
            JavascriptHelper.AttachPopUpCode(Page, "popupLeave", "../cp/LeaveList.aspx", 480, 500);
            JavascriptHelper.AttachPopUpCode(Page, "popupUpdateLeave", "../cp/AELeave.aspx", 600, 720);
            JavascriptHelper.AttachPopUpCode(Page, "popupUpdateMannualLeave", "../cp/AELeaveMannualEntered.aspx", 800, 720);
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdfsf", "EmployeeId=" + GetEmployeeId() + ";", true);
       
        }

        public int GetEmployeeId()
        {
            return int.Parse(Request.QueryString["Id"]);
        }

        public void Initialise()
        {

            if(string.IsNullOrEmpty(Request.QueryString["Id"]))
            {
                Response.Write("Employee should be selected for leave assignment,please select employee first.");
                Response.End();
                return;
            }

            EEmployee emp = EmployeeManager.GetEmployeeById(GetEmployeeId());

            LeaveAttendanceManager mgr = new LeaveAttendanceManager();
            leaves.InnerHtml = mgr.GetHTMLEmployeeLeaveList(emp.EmployeeId);
            
        }

        

    }

}