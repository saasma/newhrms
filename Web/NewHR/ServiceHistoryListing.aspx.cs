﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using System.Xml;
using Utils.Calendar;

namespace Web.Appraisal
{
    public partial class ServiceHistoryListing : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.EmployeeAppraisal;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!X.IsAjaxRequest)
            {
                Initialise();
              
            }
        }



        public void btnExport_Click(object sender, EventArgs e)
        {
            DateTime? date = null;
            DateTime? todate = null;

            if (!string.IsNullOrEmpty(hiddenFrom.Text))
                date = GetEngDate(hiddenFrom.Text);

            if (!string.IsNullOrEmpty(hiddenTo.Text))
                todate = GetEngDate(hiddenTo.Text);



            int empId = -1;
            if (cmbSearch.SelectedItem != null && cmbSearch.SelectedItem.Value != null)
                empId = int.Parse(cmbSearch.SelectedItem.Value);

            int? eventId = null;

            if (cmbEventFilter.SelectedItem != null && cmbEventFilter.SelectedItem.Value != null)
                eventId = int.Parse(cmbEventFilter.SelectedItem.Value);

            List<GetServiceHistoryListResult> dataTerminations = EmployeeManager.GetServiceHistoryList
                (empId, date, todate, 0, 999999999,eventId);


            List<string> hiddenList = new List<string>();
            hiddenList.Add("SN");
            hiddenList.Add("TotalRows");
            hiddenList.Add("EventID");
            hiddenList.Add("SourceID");
            hiddenList.Add("Type");
            hiddenList.Add("LetterDateEng");
            hiddenList.Add("LetterDate");
            hiddenList.Add("LetterNo");
            hiddenList.Add("CreatedOn");
            hiddenList.Add("ServiceStatus");
            hiddenList.Add("Designation");
            hiddenList.Add("Note");

            Dictionary<string, string> renameList = new Dictionary<string, string>();
           renameList.Add("FromDate","Nepali Date");
            renameList.Add("FromDateEng","English Date");
            renameList.Add("Level", "Position");

            Bll.ExcelHelper.ExportToExcel("Serice History List", dataTerminations,
                hiddenList,
            new List<String>() { },
            renameList,
            new List<string>() {  }
            , new Dictionary<string, string>() { }
            , new List<string> {"EIN","EVENT","Name","FromDate","FromDateEng","Branch","Department","STATUS","Level" });


        }

        protected void Store_ReadDataDynamic(object sender, StoreReadDataEventArgs e)
        {


            //PayrollPeriod newPayrollPeriod = new PayrollPeriod();
            //CustomDate _customDate = new CustomDate();

            DateTime? date = null;
            DateTime? todate = null;

            if(!string.IsNullOrEmpty(calFromDate.Text))
                date = GetEngDate(calFromDate.Text);

            if(!string.IsNullOrEmpty(calToDate.Text))
                todate = GetEngDate(calToDate.Text);

            int? eventId = null;

            if (cmbEventFilter.SelectedItem != null && cmbEventFilter.SelectedItem.Value != null)
                eventId = int.Parse(cmbEventFilter.SelectedItem.Value);

            int empId = -1;
            if (cmbSearch.SelectedItem != null && cmbSearch.SelectedItem.Value != null)
                empId = int.Parse(cmbSearch.SelectedItem.Value);

            List<GetServiceHistoryListResult> dataTerminations = EmployeeManager.GetServiceHistoryList
                (empId, date, todate, e.Start, e.Limit, eventId);

            if (dataTerminations.Count > 0)
                e.Total = dataTerminations[0].TotalRows.Value;

            Store3.DataSource = dataTerminations;
            Store3.DataBind();

        }

        public void Initialise()
        {

            List<HR_Service_Event> eventList = CommonManager.GetServieEventTypes();
            if (eventList.Count > 0)
            {
                cmbEventFilter.Store[0].DataSource = eventList;
                cmbEventFilter.Store[0].DataBind();
            }

            CustomDate date = CustomDate.GetTodayDate(IsEnglish).GetFirstDateOfThisMonth();
            CustomDate date2 = CustomDate.GetTodayDate(IsEnglish).GetLastDateOfThisMonth();
            calFromDate.Text = date.ToString();
            calToDate.Text = date2.ToString();

          
        }
        
       


       



     

      



        


 

        




        
    }
}