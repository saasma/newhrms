﻿<%@ Page Title="Log Files" Language="C#" EnableEventValidation="false" ValidateRequest="false" MasterPageFile="~/Master/NewDetails.Master" AutoEventWireup="true" CodeBehind="LogList.aspx.cs" Inherits="Web.NewHR.LogList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

<style type="text/css">

     .BlueCls
     {
         color:Blue;
     }

</style>

<script type="text/javascript">

    function searchList() {
        <%=gridLog.ClientID %>.getStore().pageSize = <%=cmbPageSize.ClientID %>.getValue();
        <%=PagingToolbar1.ClientID %>.doRefresh();
    }

    var CommandHandler = function(value, command,record,p2){
        <%=txtLogDetail.ClientID %>.clear();
        <%= hdnFileName.ClientID %>.setValue(record.data.FileName);
        <%= btnView.ClientID %>.fireEvent('click');
        
    }

    function DownloadFn()
    {
        <%=btnDownloadHelper.ClientID %>.fireEvent('click');
    }

</script>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">

<ext:Hidden ID="hdnFileName" runat="server" />
<ext:Hidden ID="hdnFileNames" runat="server" />

<ext:Button ID="btnView" runat="server" Hidden="true">
    <DirectEvents>
        <Click OnEvent="btnView_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:Button>

 <div class="pageheader">
    <div class="media">
        <div class="media-body">
            <h4>
                Log Files
            </h4>
        </div>
    </div>
</div>

 <div class="contentpanel">

        <ext:Button ID="btnDownloadHelper" runat="server" Hidden="true" AutoPostBack="true" OnClick="btnDownloadHelper_Click" />
        <ext:Button ID="btnDownload" runat="server" Cls="btn btn-save" Text="Download">
            <DirectEvents>
                <Click OnEvent="btnDownload_Click">
                    <EventMask ShowMask="true" />
                    <ExtraParams>
                        <ext:Parameter Name="gridItems" Value="Ext.encode(#{gridLog}.getRowsValues({selectedOnly:false}))"
                            Mode="Raw" />
                    </ExtraParams>
                </Click>
            </DirectEvents>
        </ext:Button>

        <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridLog" runat="server" Cls="itemgrid"
            Scroll="None" Width="670">
            <Store>
                <ext:Store ID="storeLog" runat="server" AutoLoad="true" OnReadData="Store_ReadData"
                    RemotePaging="true" RemoteSort="false">
                    <Proxy>
                        <ext:PageProxy />
                    </Proxy>
                    <Model>
                        <ext:Model ID="Model6" runat="server" IDProperty="Id">
                            <Fields>
                                <ext:ModelField Name="CreatedDate" Type="Date" />
                                <ext:ModelField Name="CreatedDateString" Type="String" />
                                <ext:ModelField Name="FileName" Type="String" />
                                <ext:ModelField Name="IsChecked" Type="Boolean" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>
            <ColumnModel>
                <Columns>
                    <ext:Column ID="colDate" Sortable="false" MenuDisabled="true" runat="server" Text="Date"
                        Width="300" Align="Left" DataIndex="CreatedDateString">
                    </ext:Column>                    
                    <ext:Column ID="Column15" Sortable="false" MenuDisabled="true" runat="server" Text="File name"
                        Width="250" Align="Left" DataIndex="FileName">
                    </ext:Column>
                    <ext:ImageCommandColumn ID="ImageCommandColumn2" runat="server" Width="70" Text=""
                        MenuDisabled="true" Sortable="false" Align="Center">
                        <Commands>
                            <ext:ImageCommand CommandName="View" Text="View" Cls="BlueCls">
                                <ToolTip Text="View log details" />
                            </ext:ImageCommand>
                        </Commands>
                        <Listeners>
                            <Command Fn="CommandHandler">
                            </Command>
                        </Listeners>
                    </ext:ImageCommandColumn>
                    <ext:CheckColumn ID="colIsChecked" Editable="true" Sortable="false" MenuDisabled="true" runat="server" Width="50" Align="Center" DataIndex="IsChecked" />
                    

                </Columns>
            </ColumnModel>
            <BottomBar>
                <ext:PagingToolbar Height="40" ID="PagingToolbar1" runat="server" StoreID="storeLog"
                    DisplayInfo="true">
                    <Items>
                        <ext:Label ID="lblPageSize" runat="server" Text="Page size:" />
                        <ext:ToolbarSpacer ID="ToolbarSpacer_listItems" runat="server" Width="10" />
                        <ext:ComboBox ID="cmbPageSize" runat="server" Width="80" SelectOnFocus="true" Selectable="true"
                            ValueField="Value" DisplayField="Text" ForceSelection="true" AllowBlank="false">
                            <Listeners>
                                <Select Handler="searchList()" />
                            </Listeners>
                            <Items>
                                <ext:ListItem Value="25" Text="25" />
                                <ext:ListItem Value="50" Text="50" />
                                <ext:ListItem Value="100" Text="100" />
                            </Items>
                            <SelectedItems>
                                <ext:ListItem Index="0">
                                </ext:ListItem>
                            </SelectedItems>
                        </ext:ComboBox>
                    </Items>
                </ext:PagingToolbar>
            </BottomBar>
        </ext:GridPanel>

    <ext:Window ID="WLogDetail" runat="server" Title="Log Details" Icon="Application"
        Resizable="false" Width="900" Height="600" BodyPadding="5" Hidden="true" Modal="true">
        <Content>
            <table style="margin-left: 20px; margin-top: 20px;">
                <tr>
                    <td>
                        <ext:TextArea ID="txtLogDetail" runat="server" MarginSpec="0 20 0 0" LabelSeparator="" FieldLabel="" Text="" Width="850" Height="500" OverflowY="Scroll" >
                        </ext:TextArea>
                    </td>
                </tr>
            </table>
            
        </Content>
    </ext:Window>

 </div>




</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
