﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Serialization;
using BasicPrimitives.OrgDiagram;

namespace BasicPrimitivesDemo
{
    public partial class _Default : System.Web.UI.Page
    {
       

        protected void Page_Load(object sender, EventArgs e)
        {
            // only bind the data on the first page load...
            if (!Page.IsPostBack)
            {
                this.orgDiagram.Items.Clear();

                /* Root */
                Item MarkKornegay = new Item("Mark Kornegay", "1", "GM Enterprise Services Business Development &amp; Partner Group", "/images/photos/a.png");
                MarkKornegay.TemplateName = "UserTemplateContact";
                MarkKornegay.TitleColor = Color.Red;
                MarkKornegay.GroupTitle = "Group";
                MarkKornegay.GroupTitleColor = Color.Green;
                this.orgDiagram.Items.Add(MarkKornegay);

                /* Right Adviser & its team */
                Item BrianSelby = new Item("Brian Selby", "2", "VP, Global Accounts", "/images/photos/b.png");
                BrianSelby.TitleColor = Color.Orange;
                BrianSelby.ItemType = ItemType.Adviser;
                BrianSelby.GroupTitle = "Adviser";
                BrianSelby.ShowCheckBox = BasicPrimitives.OrgDiagram.Enabled.False;
                BrianSelby.ChildrenPlacementType = ChildrenPlacementType.Horizontal;
                MarkKornegay.Items.Add(BrianSelby);

                Item JoanneHarrell = new Item("Joanne Harrell", "3", "VP, Specialist Sales", "/images/photos/c.png");
                JoanneHarrell.ShowCheckBox = BasicPrimitives.OrgDiagram.Enabled.True;
                JoanneHarrell.ShowButtons = BasicPrimitives.OrgDiagram.Enabled.False;
                BrianSelby.Items.Add(JoanneHarrell);

                Item MarkHill = new Item("Mark Hill", "4", "GM, Worldwide Industry Strategy", "/images/photos/d.png");
                BrianSelby.Items.Add(MarkHill);

                /* Left Adviser */
                Item OrlandoAyala = new CustomItem("Orlando Ayala", "5", "VP &amp; Deputy General Counsel, Global Corporate Affairs", "/images/photos/z.png", "(647) 222-22-2", "1000 Younge St", "info@mail.com");
                OrlandoAyala.TemplateName = "UserTemplateContact";
                OrlandoAyala.Checked = true;
                OrlandoAyala.GroupTitle = "Adviser";
                OrlandoAyala.AdviserPlacementType = AdviserPlacementType.Left;
                OrlandoAyala.ShowCheckBox = BasicPrimitives.OrgDiagram.Enabled.False;
                OrlandoAyala.ItemType = ItemType.Adviser;
                MarkKornegay.Items.Add(OrlandoAyala);

                /* Assistant */
                Item LaneSorgen = new CustomItem("Lane Sorgen", "6", "GM, Enterprise, New York/New Jersey", "/images/photos/y.png", "(647) 111-11-11", "79 Wellington St W", "support@mail.com");
                LaneSorgen.TemplateName = "UserTemplateContact";
                LaneSorgen.GroupTitle = "Assistant";
                LaneSorgen.AdviserPlacementType = AdviserPlacementType.Left;
                LaneSorgen.ShowCheckBox = BasicPrimitives.OrgDiagram.Enabled.False;
                LaneSorgen.ItemType = ItemType.SubAssistant;
                MarkKornegay.Items.Add(LaneSorgen);

                /* Group of regular items without teams on the left side of invisible aggregator */
                for (int index = 0; index < 3; index++)
                {
                    Item IndexedItem = new Item("Left #" + index.ToString(), index.ToString(), "Description", "/images/photos/l.png");
                    MarkKornegay.Items.Add(IndexedItem);
                }

                /* Invisible Aggregator */
                Item Aggregator = new Item("Invisible Aggregator", "61");
                Aggregator.ItemType = ItemType.Regular;
                Aggregator.IsVisible = false;
                MarkKornegay.Items.Add(Aggregator);

                /* Group of regular items without teams on the right side of invisible aggregator */
                for (int index = 0; index < 3; index++)
                {
                    Item IndexedItem = new Item("Right #" + index.ToString(), index.ToString(), "Description", "/images/photos/r.png");
                    MarkKornegay.Items.Add(IndexedItem);
                }

                /* Robert Deshaies Team */
                Item RobertDeshaies = new Item("Robert Deshaies", "7", "SMS&amp;P, Pacific Northwest", "/images/photos/x.png");
                RobertDeshaies.TitleColor = Color.FromArgb(0x33, 0x33, 0x65);
                Aggregator.Items.Add(RobertDeshaies);
                for (int index = 0; index < 10; index++)
                {
                    Item IndexedItem = new Item("#" + index.ToString(), index.ToString(), "Description", "/images/photos/q.png");
                    RobertDeshaies.Items.Add(IndexedItem);
                }

                /* Mike Sidoroff Team */
                Item MikeSidoroff = new Item("Mike Sidoroff", "7", "SMS&amp;P, Northeast", "/images/photos/y.png");
                MikeSidoroff.TitleColor = Color.FromArgb(0x33, 0x33, 0x65);
                Aggregator.Items.Add(MikeSidoroff);
                for (int index = 0; index < 12; index++)
                {
                    Item IndexedItem = new Item("#" + index.ToString(), index.ToString(), "Description", "/images/photos/p.png");
                    MikeSidoroff.Items.Add(IndexedItem);
                }

                /* David Canon Team */
                Item DavidCanon = new Item("David Canon", "7", "SMS&amp;P, North", "/images/photos/y.png");
                DavidCanon.TitleColor = Color.FromArgb(0x33, 0x33, 0x65);
                Aggregator.Items.Add(DavidCanon);
                for (int index = 0; index < 8; index++)
                {
                    Item IndexedItem = new Item("#" + index.ToString(), index.ToString(), "Description", "/images/photos/t.png");
                    DavidCanon.Items.Add(IndexedItem);
                }

                MarkKornegay.Selected = true;
                JoanneHarrell.Checked = true;
                RobertDeshaies.Checked = true;
            }
        }

        protected void orgDiagram_SelectedItemChanged(object sender, EventArgs e)
        {
            Item selectedItem = this.orgDiagram.SelectedItem;
        }

        protected void orgDiagram_ItemCheckChanged(object sender, EventArgs e)
        {
            Items checkedItems = this.orgDiagram.CheckedItems;
        }

        protected void Button1_Click(object sender, EventArgs e)
        {

        }

        protected void orgDiagram_TemplateButtonClick(object sender, TemplateButtonClickEventArgs e)
        {
            String buttonName = e.ButtonName;
            Item clickedButtonItem = e.Item;
        }

    }
}