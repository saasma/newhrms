﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="BasicPrimitivesDemo._Default" %>

<%@ Register assembly="BPOrgDiagram" namespace="BasicPrimitives.OrgDiagram" tagprefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Basic Primitives orgDiagram Demo</title>
    <script type="text/javascript" src="Scripts/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="Scripts/jquery-ui-1.10.2.custom.min.js"></script>
    <script type="text/javascript" src="Scripts/json3.min.js"></script>
    <link rel="stylesheet" type="text/css" href="Scripts/ui-lightness/jquery-ui-1.10.2.custom.min.css" />
    <link href="Scripts/primitives.latest.css" rel="stylesheet" />
    <script type="text/javascript" src="Scripts/primitives.min.js?204"></script>
    <style type="text/css">
        #Form
        {
            width: 794px;
        }
        .bp-photo-frame img{width:50px;height:58px;}
    </style>
</head>
<body>
    <form id="Form" runat="server">
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
        <cc1:OrgDiagramServerControl ID="orgDiagram" runat="server" 
            BorderColor="#000066" BorderStyle="Solid" BorderWidth="1px" 
            Width="1024px" style="margin-right: 0px; top: 48px; left: 11px; height: 506px;" 
            OnItemCheckChanged="orgDiagram_ItemCheckChanged" 
            OnSelectedItemChanged="orgDiagram_SelectedItemChanged" 
            Height="600px" MaximumColumnsInMatrix="8" DotItemsInterval="10" DotLevelShift="10" ChildrenPlacementType="Horizontal" OnTemplateButtonClick="orgDiagram_TemplateButtonClick"
            >
            <Buttons>
                <cc1:Button Name="Home" />
                <cc1:Button Icon="Locked" Name="Locked" />
                <cc1:Button Icon="Print" Name="Print" />
            </Buttons>
        </cc1:OrgDiagramServerControl>
    <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Post Back" />
    </form>
    <script src="Scripts/UserTemplates.js?1029"></script>
</body>
</html>
