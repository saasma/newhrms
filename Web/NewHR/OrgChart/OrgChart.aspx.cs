﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using BLL.Manager;
using BLL;
using DAL;
using Utils;
using BLL.Base;
using System.IO;
using Utils.Helper;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Serialization;
using BasicPrimitives.OrgDiagram;
using DAL;
using BLL.Manager;
using Utils;
using System.IO;

namespace Web.NewHR.UserControls
{
    public partial class OrgChart : BasePage
    {
        public Item GetItem(EEmployee emp)
        {

            string photoUrl = "";
            if (emp.HHumanResources.Count > 0)
            {
                if (!string.IsNullOrEmpty(emp.HHumanResources[0].UrlPhotoThumbnail))
                {
                    photoUrl = this.ResolveUrl(Config.UploadLocation + "/" + emp.HHumanResources[0].UrlPhotoThumbnail);
                }
            }

            if (string.IsNullOrEmpty(photoUrl))
            {
                photoUrl = Page.ResolveUrl(Web.Helper.WebHelper.GetEmployeeNoImage(0, emp));
            }


            Item MarkKornegay = new Item(emp.Name, emp.EmployeeId.ToString()
                , emp.EDesignation.Name, photoUrl);
            MarkKornegay.TemplateName = "UserTemplateContact";
            MarkKornegay.TitleColor = Color.Red;
            MarkKornegay.GroupTitle = "Group";
            MarkKornegay.GroupTitleColor = Color.Green;

            return MarkKornegay;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            // only bind the data on the first page load...
            if (!Page.IsPostBack)
            {
                this.orgDiagram.Items.Clear();

                List<EEmployee> list = EmployeeManager.GetAllEmployees();

                Item root = GetItem(list[0]);
                this.orgDiagram.Items.Add(root);

                root.Items.Add(GetItem(list[1]));
                root.Items.Add(GetItem(list[2]));



                root.Items[0].Items.Add(GetItem(list[3]));
                root.Items[0].Items.Add(GetItem(list[4]));

                root.Items[1].Items.Add(GetItem(list[5]));
                root.Items[1].Items.Add(GetItem(list[6]));

            }
        }

        protected void orgDiagram_SelectedItemChanged(object sender, EventArgs e)
        {
            Item selectedItem = this.orgDiagram.SelectedItem;
        }

        protected void orgDiagram_ItemCheckChanged(object sender, EventArgs e)
        {
            Items checkedItems = this.orgDiagram.CheckedItems;
        }

        protected void Button1_Click(object sender, EventArgs e)
        {

        }

        protected void orgDiagram_TemplateButtonClick(object sender, TemplateButtonClickEventArgs e)
        {
            String buttonName = e.ButtonName;
            Item clickedButtonItem = e.Item;
        }
      

   

       
    }
}