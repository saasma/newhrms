﻿<%@ Page Title="Organization Chart" Language="C#" MasterPageFile="~/Master/HR.Master" AutoEventWireup="true"
    CodeBehind="OrgChart.aspx.cs" Inherits="Web.NewHR.UserControls.OrgChart" %>

<%@ Register Src="~/NewHR/UserControls/EmployeeDetailsCtl.ascx" TagName="EmployeeDetailsCtl"
    TagPrefix="uc1" %>
<%@ Register Src="~/NewHR/UserControls/EmploymentHistoryCtl.ascx" TagName="EmploymentHistoryCtl"
    TagPrefix="ucEmploymentHistoryCtl" %>

<%@ Register Assembly="BPOrgDiagram" Namespace="BasicPrimitives.OrgDiagram" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
    </style>
    <script type="text/javascript" src="Scripts/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="Scripts/jquery-ui-1.10.2.custom.min.js"></script>
    <script type="text/javascript" src="Scripts/json3.min.js"></script>
    <link rel="stylesheet" type="text/css" href="Scripts/ui-lightness/jquery-ui-1.10.2.custom.min.css" />
    <link href="Scripts/primitives.latest.css" rel="stylesheet" />
    <script type="text/javascript" src="Scripts/primitives.min.js?204"></script>
    <style type="text/css">
        #Form
        {
            width: 794px;
        }
        .bp-photo-frame img
        {
            width: 50px;
            height: 58px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <ext:Hidden runat="server" ID="hdnID" Text="">
    </ext:Hidden>
    <div class="innerLR">
        <div class="separator bottom">
        </div>
        <h3 class="heading">
            Organization Chart</h3>
       
        <cc1:OrgDiagramServerControl ID="orgDiagram" runat="server" BorderColor="#000066"
            BorderStyle="Solid" BorderWidth="1px" Width="1200px" Style="margin-right: 0px;
            top: 48px; left: 11px; height: 700px;" OnItemCheckChanged="orgDiagram_ItemCheckChanged"
            OnSelectedItemChanged="orgDiagram_SelectedItemChanged" Height="600px" MaximumColumnsInMatrix="8"
            DotItemsInterval="10" DotLevelShift="10" ChildrenPlacementType="Horizontal" OnTemplateButtonClick="orgDiagram_TemplateButtonClick">
            <Buttons>
                <cc1:Button Name="Home" />
                <cc1:Button Icon="Locked" Name="Locked" />
                <cc1:Button Icon="Print" Name="Print" />
            </Buttons>
        </cc1:OrgDiagramServerControl>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
<script src="Scripts/UserTemplates.js?1029"></script>
</asp:Content>

