﻿/**
 * Basic Primitives ASP.NET BPOrgDiagram
 *
 * (c) Basic Primitives Inc
 *
 *
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://jquery.org/license
 *
 */
using System;
using BasicPrimitives.OrgDiagram;


namespace BasicPrimitivesDemo
{
    [Serializable]
    public class CustomItem : Item
    {
        private string m_phone;
        private string m_address;
        private string m_email;

        #region Constructor

        public CustomItem()
            : base()
        {
        }

        public CustomItem(string title, string value, string description, string imageUrl, string phone, string address, string email)
            : base(title, value, description, imageUrl)
        {
            this.m_phone = phone;
            this.m_address = address;
            this.m_email = email;
        }

        public string Phone
        {
            get
            {
                return m_phone;
            }
            set
            {
                this.m_phone = value;
            }
        }

        public string Address
        {
            get
            {
                return m_address;
            }
            set 
            {
                this.m_address = value;
            }
        }

        public string Email
        {
            get
            {
                return m_email;
            }
            set
            {
                this.m_email = value;
            }
        }

        #endregion //COntructor
    }
}
