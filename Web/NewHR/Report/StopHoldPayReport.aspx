﻿<%@ Page Title="Stop/Hold Pay Report" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="StopHoldPayReport.aspx.cs" Inherits="Web.CP.StopHoldPayReport" %>

<%@ MasterType VirtualPath="~/Master/NewDetails.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

    function searchList() {
              <%=PagingToolbar1.ClientID %>.doRefresh();
        }

 
   
    function SelEmpSearch()
    {
        <%=hdnEmpSearch.ClientID %>.setValue(<%=cmbEmpSearch.ClientID %>.getValue());
    }

    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>
    <ext:Hidden ID="hdnEmployeeId" runat="server" />
    <ext:Hidden ID="hdnBranchDepartmentId" runat="server" />
    <ext:Hidden ID="hdnMsg" runat="server" />
    <ext:Hidden ID="hdnEmpSearch" runat="server" />
    <ext:Hidden ID="hdnSortBy" runat="server" />
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Stop/Hold Pay Report
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel" style="margin-top: 0px !important; padding-top: 0px !important;">
        <div class="innerLR">
            <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                <Proxy>
                    <ext:AjaxProxy Url="../../Handler/EmpSearchID.ashx">
                        <ActionMethods Read="GET" />
                        <Reader>
                            <ext:JsonReader Root="plants" TotalProperty="total" />
                        </Reader>
                    </ext:AjaxProxy>
                </Proxy>
                <Model>
                    <ext:Model ID="Model5" IDProperty="EmployeeId" Name="ItemLineModel" runat="server">
                        <Fields>
                            <ext:ModelField Name="Name" Type="String" />
                            <ext:ModelField Name="EmployeeId" Type="String" />
                        </Fields>
                    </ext:Model>
                </Model>
            </ext:Store>
            <div class="alert alert-info" style="margin-top: 10px; padding-top: 0px!important;">
                <ext:Container ID="listingContainer" runat="server">
                    <Content>
                        <table>
                            <tr>
                                <td>
                                    <ext:ComboBox Width="100" MatchFieldWidth="false" MarginSpec="0 5 0 5" LabelSeparator=""
                                        LabelAlign="Top" QueryMode="Local" ID="cmbType" runat="server" FieldLabel="Type">
                                        <Items>
                                            <ext:ListItem Index="0" Value="1" Text="All">
                                            </ext:ListItem>
                                            <ext:ListItem Value="2" Text="Stop Pay">
                                            </ext:ListItem>
                                            <ext:ListItem Value="3" Text="Hold Pay">
                                            </ext:ListItem>
<%--                                            <ext:ListItem Text="" Value="4" />--%>
                                        </Items>
                                        <SelectedItems>
                                            <ext:ListItem Index="0" />
                                        </SelectedItems>
                                    </ext:ComboBox>
                                </td>
                                <td style="padding-left: 10px;">
                                    <ext:DateField ID="txtFromDate" runat="server" FieldLabel="From" LabelSeparator=""
                                        LabelAlign="Top" Width="100">
                                        <Plugins>
                                            <ext:GenericPlugin ID="HighlightDates1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                                        </Plugins>
                                    </ext:DateField>
                                </td>
                                <td style="padding-left: 10px;">
                                    <ext:DateField ID="txtToDate" runat="server" FieldLabel="To" LabelSeparator="" LabelAlign="Top"
                                        Width="100">
                                        <Plugins>
                                            <ext:GenericPlugin ID="GenericPlugin1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                                        </Plugins>
                                    </ext:DateField>
                                </td>
                                <td style="padding-left: 10px; padding-right: 10px">
                                    <ext:Checkbox ID="chkExcludeRet" runat="server" Checked="true" BoxLabel="Exclude Retired" />
                                </td>
                                <td style="width: 310px;">
                                    <ext:ComboBox LabelSeparator="" ID="cmbEmpSearch" FieldLabel="Search Employee" EmptyText="Employee Name"
                                        LabelWidth="80" LabelAlign="Top" runat="server" DisplayField="Name" ValueField="EmployeeId"
                                        StoreID="storeSearch" TypeAhead="false" Width="250" PageSize="9999" HideBaseTrigger="true"
                                        MinChars="1" TriggerAction="All" ForceSelection="false">
                                        <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                                            <ItemTpl ID="ItemTpl2" runat="server">
                                                <Html>
                                                    <tpl>
                                                        <div class="search-item">
                                                                        <span>{Name}</span>  
                                                         </div>
					                            </tpl>
                                                </Html>
                                            </ItemTpl>
                                        </ListConfig>
                                        <Triggers>
                                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                        </Triggers>
                                        <Listeners>
                                            <Select Handler="this.getTrigger(0).show(); SelEmpSearch();" />
                                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                            <TriggerClick Handler="if (index == 0) { 
                                                       this.clearValue(); 
                                                       this.getTrigger(0).hide();
                                                   }" />
                                        </Listeners>
                                    </ext:ComboBox>
                                </td>
                                <td>
                                    <ext:Button ID="btnLoad" runat="server" Text="Load" Width="100" Cls="btn btn-default"
                                        MarginSpec="25 10 10 10">
                                        <Listeners>
                                            <Click Fn="searchList">
                                            </Click>
                                        </Listeners>
                                    </ext:Button>
                                </td>
                            </tr>
                        </table>
                    </Content>
                </ext:Container>
            </div>
            <ext:KeyMap ID="KeyMap1" runat="server" Target="={#{listingContainer}.getBody()}">
                <Binding>
                    <ext:KeyBinding Handler="#{btnLoad}.fireEvent('click');">
                        <Keys>
                            <ext:Key Code="ENTER" />
                        </Keys>
                    </ext:KeyBinding>
                </Binding>
            </ext:KeyMap>
            <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridBranchTransfer" runat="server"
                OnReadData="Store_ReadData" AutoScroll="true">
                <Store>
                    <ext:Store PageSize="50" ID="storeEmpList" runat="server" AutoLoad="true" OnReadData="Store_ReadData"
                        RemotePaging="true" RemoteSort="true">
                        <Proxy>
                            <ext:PageProxy />
                        </Proxy>
                        <Model>
                            <ext:Model ID="Model1" runat="server">
                                <Fields>
                                    <ext:ModelField Name="RowNumber" Type="String" />
                                    <ext:ModelField Name="EmployeeId" Type="String" />
                                    <ext:ModelField Name="AccountNo" Type="String" />
                                    <ext:ModelField Name="Name" Type="String" />
                                    <ext:ModelField Name="HoldStopType" Type="String" />
                                    <ext:ModelField Name="StartDate" Type="Date" />
                                    <ext:ModelField Name="EndDate" Type="Date" />
                                    <ext:ModelField Name="TotalDays" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Sorters>
                            <ext:DataSorter Property="EmployeeId" Direction="ASC" />
                        </Sorters>
                    </ext:Store>
                </Store>
                <ColumnModel ID="ColumnModel1" runat="server">
                    <Columns>
                        <ext:Column ID="colFromDate" runat="server" Text="SN" Locked="true" DataIndex="RowNumber"
                            MenuDisabled="false" Sortable="false" Align="Right" Width="50" />
                        <ext:Column ID="Column1" runat="server" Text="EIN" Locked="true" DataIndex="EmployeeId"
                            MenuDisabled="false" Sortable="true" Align="Right" Width="50" />
                        <ext:Column ID="Column2" runat="server" Text="Account No" Locked="true" DataIndex="AccountNo"
                            MenuDisabled="false" Sortable="false" Align="Left" Width="100" />
                        <ext:Column ID="Column3" runat="server" Text="Name" Locked="true" DataIndex="Name"
                            MenuDisabled="false" Sortable="true" Align="Left" Width="150" />
                        <ext:Column ID="Column9" runat="server" Text="Stop/Hold Type" Locked="true" DataIndex="HoldStopType"
                            MenuDisabled="false" Sortable="true" Align="Left" Width="150" />
                        <ext:DateColumn ID="colFromDateEng" runat="server" Text="Start Date" DataIndex="StartDate"
                            Format="dd-MMM-yyyy" MenuDisabled="false" Sortable="true" Align="Left" Width="100" />
                        <ext:DateColumn ID="DateColumn1" runat="server" Text="End Date" DataIndex="EndDate"
                            Format="dd-MMM-yyyy" MenuDisabled="false" Sortable="true" Align="Left" Width="100" />
                        <ext:Column ID="Column4" runat="server" Text="Total Days" DataIndex="TotalDays" MenuDisabled="false"
                            Sortable="false" Align="Left" Width="100" />
                    </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar1" runat="server" StoreID="storeEmpList" DisplayInfo="true">
                    </ext:PagingToolbar>
                </BottomBar>
            </ext:GridPanel>
        </div>
        <div class="alert alert-info" style="margin-top: 15px; height: 50px;">
            <div style="float: right;">
                <ext:Button runat="server" AutoPostBack="true" OnClick="btnExport_Click" ID="btnExport"
                    Text="<i></i>Export To Excel">
                </ext:Button>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
