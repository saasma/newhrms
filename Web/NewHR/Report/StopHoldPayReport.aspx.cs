﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using Utils.Helper;
using System.IO;
using Utils;
using Utils.Security;
using Utils.Calendar;

namespace Web.CP
{
    public partial class StopHoldPayReport : BasePage
    {
     
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest && !IsPostBack)
            {
               Initialize();

                              

            }
        }

        private void Initialize()
        {
            //PayrollPeriod period = CommonManager.GetLastPayrollPeriod();
            //cmbType.Items[3].Text = "Released in " + period.Name;

        }

        private void Clear()
        {
            
        }

       
        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            int totalRecords = 0;
            int type = 1;
            type = int.Parse(cmbType.SelectedItem.Value);


            DateTime? start = null, end = null;

            if (!string.IsNullOrEmpty(txtFromDate.RawText))
                start = txtFromDate.SelectedDate;

            if (!string.IsNullOrEmpty(txtToDate.RawText))
                start = txtToDate.SelectedDate;

            int employeeId = -1;
           
            int.TryParse(cmbEmpSearch.SelectedItem.Value, out employeeId);

            if (employeeId == 0)
                employeeId = -1;

            hdnSortBy.Text = (e.Sort[0].Property + " " + e.Sort[0].Direction).ToLower();

            List<Report_StopHoldPaymentResult> list = EmployeeManager.GetStopHoldList
                ( employeeId,type,start,end, e.Page-1, e.Limit, (e.Sort[0].Property + " " + e.Sort[0].Direction).ToLower(),chkExcludeRet.Checked,
                ref totalRecords);

            e.Total = totalRecords;

            if (list.Any())
                storeEmpList.DataSource = list;
            storeEmpList.DataBind();

        }

     
        public void btnExport_Click(object sender, EventArgs e)
        {
            int totalRecords = 0;
            int type = 1;
            type = int.Parse(cmbType.SelectedItem.Value);


            DateTime? start = null, end = null;

            if (!string.IsNullOrEmpty(txtFromDate.RawText))
                start = txtFromDate.SelectedDate;

            if (!string.IsNullOrEmpty(txtToDate.RawText))
                start = txtToDate.SelectedDate;

            int employeeId = -1;

            int.TryParse(cmbEmpSearch.SelectedItem.Value, out employeeId);

            if (employeeId == 0)
                employeeId = -1;


            List<Report_StopHoldPaymentResult> list = EmployeeManager.GetStopHoldList
                (employeeId, type, start, end, 0, 9999999, (hdnSortBy.Text).ToLower(), chkExcludeRet.Checked,
                ref totalRecords);
            
            List<string> hiddenList = new List<string>();
            hiddenList.Add("TotalRows");
            hiddenList.Add("StartDate");
            hiddenList.Add("EndDate");
           
            Dictionary<string, string> renameList = new Dictionary<string, string>();
         
            renameList.Add("RowNumber", "SN");
            renameList.Add("EmployeeId", "EIN");
            renameList.Add("HoldStopType", "Hold/Stop Type");
            renameList.Add("StartDateText", "StartDate");
            renameList.Add("EndDateText", "EndDate");

            Bll.ExcelHelper.ExportToExcel("Employee Stop/Hold Pay Report", list,
                hiddenList,
            new List<String>() { },
            renameList,
            new List<string>() { }, new List<string> { "Age", "RowNumber", "EmployeeId" }
            , new List<string>() { "StartDateText", "EndDateText" }
            , new Dictionary<string, string>() { { "Employee Stop/Hold Pay Report", "" } }
            , new List<string> { "RowNumber", "EmployeeId", "AccountNo", "Name", "HoldStopType", "StartDateText", "EndDateText", "TotalDays" });


        }


    }
}