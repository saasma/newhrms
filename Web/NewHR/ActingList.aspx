﻿<%@ Page Title="Acting List" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="ActingList.aspx.cs" Inherits="Web.NewHR.ActingList" %>

<%@ Register Src="~/newhr/UserControls/EmployeeWizard.ascx" TagName="EmployeeWizard"
    TagPrefix="uc2" %>
<%@ Register Src="~/NewHR/UserControls/ActingListCtl.ascx" TagName="ActingListCtl"
    TagPrefix="uc1" %>
<%@ Register Src="~/NewHR/UserControls/EmployeeDetailsCtl.ascx" TagName="EmployeeDetailsCtl"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Acting List
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel" style="padding-top: 0px;">
        <div class="innerLR">
            <uc1:ActingListCtl Id="EmployeeDetailsCtl2" runat="server" />
        </div>
    </div>
    <br />
</asp:Content>
