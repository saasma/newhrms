﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using Utils.Helper;
using System.IO;
using Utils;
using Utils.Security;
using Utils.Calendar;
using Web.Helper;

namespace Web.CP
{
    public partial class EmployeeCurrentPaySalary : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.HumanResource;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
               Initialize();

                              

            }

            JavascriptHelper.AttachPopUpCode(Page, "insertUpdate", "../cp/CurrentPayIncomeSelection.aspx", 670, 400);
        }

        private void Initialize()
        {
            List<Branch> branchList = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            cmbBranch.Store[0].DataSource = branchList;
            cmbBranch.Store[0].DataBind();

            cmbDepartment.Store[0].DataSource = DepartmentManager.GetAllDepartments().OrderBy(x => x.Name).ToList();
            cmbDepartment.Store[0].DataBind();

            cmbDesignation.Store[0].DataSource = new CommonManager().GetAllDesignations().OrderBy(x => x.LevelAndDesignation).ToList();
            cmbDesignation.Store[0].DataBind();

            this.storeLevel.DataSource = ActivityManager.GetLevels().OrderBy(x => x.Name);
            this.storeLevel.DataBind();


            int? incomeID1 = null;
            int? incomeID2 = null;
            int? incomeID3 = null;
            int? incomeID4 = null;
            int? incomeID5 = null;
            int? incomeID6 = null;
            int? incomeID7 = null;
            int? incomeID8 = null;
            int? incomeID9 = null;
            int? incomeID10 = null;
            EmployeeManager.SetIncomeListForEmployeeCurrentPayReport(ref incomeID1, ref incomeID2, ref incomeID3, ref incomeID4, ref incomeID5,
                ref incomeID6,ref incomeID7,ref incomeID8,ref incomeID9,ref incomeID10);
            PayManager mgr = new PayManager();

            PIncome income = null;
            if (incomeID1 != null)
            {                
                income = mgr.GetIncomeById(incomeID1.Value);
                income1.Text = income.Title;
            }

            if (incomeID2 != null)
            {
                income = mgr.GetIncomeById(incomeID2.Value);
                income2.Text = income.Title;
            }
            else
                income2.Hide();

            if (incomeID3 != null)
            {
                income = mgr.GetIncomeById(incomeID3.Value);
                income3.Text = income.Title;
            }
            else
                income3.Hide();

            if (incomeID4 != null)
            {
                income = mgr.GetIncomeById(incomeID4.Value);
                income4.Text = income.Title;
            }
            else
                income4.Hide();

            if (incomeID5 != null)
            {
                income = mgr.GetIncomeById(incomeID5.Value);
                income5.Text = income.Title;
            }
            else
                income5.Hide();

            if (incomeID6 != null)
            {
                income = mgr.GetIncomeById(incomeID6.Value);
                Income6.Text = income.Title;
            }
            else
                Income6.Hide();

            if (incomeID7 != null)
            {
                income = mgr.GetIncomeById(incomeID7.Value);
                Income7.Text = income.Title;
            }
            else
                Income7.Hide();

            if (incomeID8 != null)
            {
                income = mgr.GetIncomeById(incomeID8.Value);
                Income8.Text = income.Title;
            }
            else
                Income8.Hide();

            if (incomeID9 != null)
            {
                income = mgr.GetIncomeById(incomeID9.Value);
                Income9.Text = income.Title;
            }
            else
                Income9.Hide();

            if (incomeID10 != null)
            {
                income = mgr.GetIncomeById(incomeID10.Value);
                Income10.Text = income.Title;
            }
            else
                Income10.Hide();
        }

        private void Clear()
        {
            
        }

       
        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            int totalRecords = 0;
            int branchId = -1, depId = -1, levelId = -1, designationId = -1;

            if (cmbBranch.SelectedItem != null && cmbBranch.SelectedItem.Value != null)
                branchId = int.Parse(cmbBranch.SelectedItem.Value);
            if (cmbDepartment.SelectedItem != null && cmbDepartment.SelectedItem.Value != null)
                depId = int.Parse(cmbDepartment.SelectedItem.Value);
            if (cmbLevel.SelectedItem != null && cmbLevel.SelectedItem.Value != null)
                levelId = int.Parse(cmbLevel.SelectedItem.Value);
            if (cmbDesignation.SelectedItem != null && cmbDesignation.SelectedItem.Value != null)
                designationId = int.Parse(cmbDesignation.SelectedItem.Value);

            int employeeId = -1;
           
            int.TryParse(cmbEmpSearch.SelectedItem.Value, out employeeId);

            if (employeeId == 0)
                employeeId = -1;

            hdnSortBy.Text = (e.Sort[0].Property + " " + e.Sort[0].Direction).ToLower();

            List<Report_GetEmployeeCurrentSalaryPayResult> list = EmployeeManager.GetEmployeeCurrentPaySalary
                (branchId, depId, levelId, designationId, employeeId, e.Page-1, e.Limit, (e.Sort[0].Property + " " + e.Sort[0].Direction).ToLower(), ref totalRecords);

            e.Total = totalRecords;

            if (list.Any())
                storeEmpList.DataSource = list;
            storeEmpList.DataBind();

        }

     
        public void btnExport_Click(object sender, EventArgs e)
        {
            int totalRecords = 0;
            int branchId = -1, depId = -1, levelId = -1, designationId = -1;

            if (cmbBranch.SelectedItem != null && cmbBranch.SelectedItem.Value != null)
                branchId = int.Parse(cmbBranch.SelectedItem.Value);
            if (cmbDepartment.SelectedItem != null && cmbDepartment.SelectedItem.Value != null)
                depId = int.Parse(cmbDepartment.SelectedItem.Value);
            if (cmbLevel.SelectedItem != null && cmbLevel.SelectedItem.Value != null)
                levelId = int.Parse(cmbLevel.SelectedItem.Value);
            if (cmbDesignation.SelectedItem != null && cmbDesignation.SelectedItem.Value != null)
                designationId = int.Parse(cmbDesignation.SelectedItem.Value);

            int employeeId = -1;

            int.TryParse(cmbEmpSearch.SelectedItem.Value, out employeeId);

            if (employeeId == 0)
                employeeId = -1;


            List<Report_GetEmployeeCurrentSalaryPayResult> list = EmployeeManager.GetEmployeeCurrentPaySalary
                (branchId, depId, levelId, designationId, employeeId, 0, 9999999, (hdnSortBy.Text).ToLower(), ref totalRecords);

            foreach (Report_GetEmployeeCurrentSalaryPayResult item in list)
                item.JoinDateText = WebHelper.FormatDateForExcel(item.JoinDate);

            int? incomeID1 = null;
            int? incomeID2 = null;
            int? incomeID3 = null;
            int? incomeID4 = null;
            int? incomeID5 = null;
            int? incomeID6 = null;
            int? incomeID7 = null;
            int? incomeID8 = null;
            int? incomeID9 = null;
            int? incomeID10 = null;
            EmployeeManager.SetIncomeListForEmployeeCurrentPayReport(ref incomeID1, ref incomeID2, ref incomeID3, ref incomeID4, ref incomeID5
                , ref incomeID6, ref incomeID7, ref incomeID8, ref incomeID9, ref incomeID10);
            List<string> hiddenList = new List<string>();
            hiddenList.Add("TotalRows");
            hiddenList.Add("JoinDate");
            if (incomeID2 == null)
                hiddenList.Add("Income2");
            if (incomeID3 == null)
                hiddenList.Add("Income3");
            if (incomeID4 == null)
                hiddenList.Add("Income4");
            if (incomeID5 == null)
                hiddenList.Add("Income5");

            if (incomeID6 == null)
                hiddenList.Add("Income6");
            if (incomeID7 == null)
                hiddenList.Add("Income7");
            if (incomeID8 == null)
                hiddenList.Add("Income8");
            if (incomeID9 == null)
                hiddenList.Add("Income9");
            if (incomeID10 == null)
                hiddenList.Add("Income10");

            Dictionary<string, string> renameList = new Dictionary<string, string>();
         
            renameList.Add("RowNumber", "SN");
            renameList.Add("EmployeeId", "EIN");
            renameList.Add("IDCardNo", "I No");
            renameList.Add("JoinDateText", "Join Date");
            PayManager mgr = new PayManager();
            PIncome income = null;
            if (incomeID1 != null)
            {
                income = mgr.GetIncomeById(incomeID1.Value);
                renameList.Add("Income1", income.Title);
            
            }

            if (incomeID2 != null)
            {
                income = mgr.GetIncomeById(incomeID2.Value);
                renameList.Add("Income2", income.Title);
            }

            if (incomeID3 != null)
            {
                income = mgr.GetIncomeById(incomeID3.Value);
                renameList.Add("Income3", income.Title);
            }

            if (incomeID4 != null)
            {
                income = mgr.GetIncomeById(incomeID4.Value);
                renameList.Add("Income4", income.Title);
            }

            if (incomeID5 != null)
            {
                income = mgr.GetIncomeById(incomeID5.Value);
                renameList.Add("Income5", income.Title);
            }

            if (incomeID6 != null)
            {
                income = mgr.GetIncomeById(incomeID6.Value);
                renameList.Add("Income6", income.Title);
            }
            if (incomeID7 != null)
            {
                income = mgr.GetIncomeById(incomeID7.Value);
                renameList.Add("Income7", income.Title);
            }
            if (incomeID8 != null)
            {
                income = mgr.GetIncomeById(incomeID8.Value);
                renameList.Add("Income8", income.Title);
            }
            if (incomeID9 != null)
            {
                income = mgr.GetIncomeById(incomeID9.Value);
                renameList.Add("Income9", income.Title);
            }
            if (incomeID10 != null)
            {
                income = mgr.GetIncomeById(incomeID10.Value);
                renameList.Add("Income10", income.Title);
            }

            Bll.ExcelHelper.ExportToExcel("Employee Current Pay List", list,
                hiddenList,
            new List<String>() { },
            renameList,
            new List<string>() { "Total", "Income1", "Income2", "Income3", "Income4", "Income5", "Income6", "Income7", "Income8", "Income9", "Income10"}
            , new List<string> { }
            , new List<string> { "JoinDateText" }
            , new Dictionary<string, string>() { {"Employee Current Pay List",""} }
            , new List<string> { });


        }


    }
}