﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using Utils.Helper;
using System.IO;
using Utils;
using Utils.Security;
using Utils.Calendar;

namespace Web.CP
{
    public partial class EmployeeOTCalculation : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.HumanResource;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
               Initialize();

                              

            }
        }

        private void Initialize()
        {
            List<Branch> branchList = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            cmbBranch.Store[0].DataSource = branchList;
            cmbBranch.Store[0].DataBind();

            cmbDepartment.Store[0].DataSource = DepartmentManager.GetAllDepartments().OrderBy(x => x.Name).ToList();
            cmbDepartment.Store[0].DataBind();

            cmbDesignation.Store[0].DataSource = new CommonManager().GetAllDesignations().OrderBy(x => x.LevelAndDesignation).ToList();
            cmbDesignation.Store[0].DataBind();

            this.storeLevel.DataSource = ActivityManager.GetLevels().OrderBy(x => x.Name);
            this.storeLevel.DataBind();


            //int? incomeID1 = null;
            //int? incomeID2 = null;
            //int? incomeID3 = null;
            //int? incomeID4 = null;
            //int? incomeID5 = null;
            //EmployeeManager.SetIncomeListForEmployeeCurrentPayReport(ref incomeID1, ref incomeID2, ref incomeID3, ref incomeID4, ref incomeID5);
            //PayManager mgr = new PayManager();

            //PIncome income = null;
            //if (incomeID1 != null)
            //{                
            //    income = mgr.GetIncomeById(incomeID1.Value);
            //    income1.Text = income.Title;
            //}

            //if (incomeID2 != null)
            //{
            //    income = mgr.GetIncomeById(incomeID2.Value);
            //    if(income!=null)
            //    income2.Text = income.Title;
            //}
            //else
            //    income2.Hide();

            //if (incomeID3 != null)
            //{
            //    income = mgr.GetIncomeById(incomeID3.Value);
            //    if (income != null)
            //    income3.Text = income.Title;
            //}
            //else
            //    income3.Hide();

            //if (incomeID4 != null)
            //{
            //    income = mgr.GetIncomeById(incomeID4.Value);
            //    if (income != null)
            //    income4.Text = income.Title;
            //}
            //else
            //    income4.Hide();

            //if (incomeID5 != null)
            //{
            //    income = mgr.GetIncomeById(incomeID5.Value);
            //    if (income != null)
            //    income5.Text = income.Title;
            //}
            //else
            //    income5.Hide();
        }

        private void Clear()
        {
            
        }

       
        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            int totalRecords = 0;
            int branchId = -1, depId = -1, levelId = -1, designationId = -1;

            if (cmbBranch.SelectedItem != null && cmbBranch.SelectedItem.Value != null)
                branchId = int.Parse(cmbBranch.SelectedItem.Value);
            if (cmbDepartment.SelectedItem != null && cmbDepartment.SelectedItem.Value != null)
                depId = int.Parse(cmbDepartment.SelectedItem.Value);
            if (cmbLevel.SelectedItem != null && cmbLevel.SelectedItem.Value != null)
                levelId = int.Parse(cmbLevel.SelectedItem.Value);
            if (cmbDesignation.SelectedItem != null && cmbDesignation.SelectedItem.Value != null)
                designationId = int.Parse(cmbDesignation.SelectedItem.Value);

            DateTime? start = null, end =  null;

            if (!string.IsNullOrEmpty(calFromDate.Text))
                start = GetEngDate(calFromDate.Text);

            if (!string.IsNullOrEmpty(calToDate.Text))
                end = GetEngDate(calToDate.Text);

            //PayrollPeriodIdEnd = 15;
            //PayrollPeriodStartDate = DateTime.Parse("2015-04-14");
            //PayrolPeriodEndDate = DateTime.Parse("2015-05-14");


            int employeeId = -1;
           
            int.TryParse(cmbEmpSearch.SelectedItem.Value, out employeeId);

            if (employeeId == 0)
                employeeId = -1;

            hdnSortBy.Text = (e.Sort[0].Property + " " + e.Sort[0].Direction).ToLower();

            List<Report_GetEmployeeOTCalcResult> list = EmployeeManager.GetEmployeeOTCalculation
                (branchId, depId, levelId, designationId, employeeId, start,end, e.Page - 1, e.Limit, (e.Sort[0].Property + " " + e.Sort[0].Direction).ToLower(), ref totalRecords);

            e.Total = totalRecords;

            if (list.Any())
                storeEmpList.DataSource = list;
            storeEmpList.DataBind();

        }

     
        public void btnExport_Click(object sender, EventArgs e)
        {
            int totalRecords = 0;
            int branchId = -1, depId = -1, levelId = -1, designationId = -1;

            if (cmbBranch.SelectedItem != null && cmbBranch.SelectedItem.Value != null)
                branchId = int.Parse(cmbBranch.SelectedItem.Value);
            if (cmbDepartment.SelectedItem != null && cmbDepartment.SelectedItem.Value != null)
                depId = int.Parse(cmbDepartment.SelectedItem.Value);
            if (cmbLevel.SelectedItem != null && cmbLevel.SelectedItem.Value != null)
                levelId = int.Parse(cmbLevel.SelectedItem.Value);
            if (cmbDesignation.SelectedItem != null && cmbDesignation.SelectedItem.Value != null)
                designationId = int.Parse(cmbDesignation.SelectedItem.Value);

            int employeeId = -1;

            int.TryParse(cmbEmpSearch.SelectedItem.Value, out employeeId);

            if (employeeId == 0)
                employeeId = -1;



            DateTime? start = null, end = null;

            if (!string.IsNullOrEmpty(calFromDate.Text))
                start = GetEngDate(calFromDate.Text);

            if (!string.IsNullOrEmpty(calToDate.Text))
                end = GetEngDate(calToDate.Text);
   

            List<Report_GetEmployeeOTCalcResult> list = EmployeeManager.GetEmployeeOTCalculation
                (branchId, depId, levelId, designationId, employeeId,start,end, 0, 99999, "", ref totalRecords);


            List<string> hiddenlist = new List<string>();
            hiddenlist.Add("TotalRows");
            hiddenlist.Add("RowNumber");
            hiddenlist.Add("AccountNo");
            hiddenlist.Add("CalculationType");
            hiddenlist.Add("OvertimeTypeID");
            hiddenlist.Add("OTHours");
            hiddenlist.Add("PayrollPeriodId");

            Dictionary<string, string> renameList = new Dictionary<string, string>();
            renameList.Add("RowNumber", "SN");
            renameList.Add("EmployeeId", "EIN");
            renameList.Add("AccountNo", "Account No");
            renameList.Add("OTName", "OT Type");
            renameList.Add("EligibleAmount", "Eligible Amount");
            renameList.Add("OTAmount", "OT Amount");
            renameList.Add("OTHoursRounded", "OT Hours");


            Bll.ExcelHelper.ExportToExcel("Employee OT Calculation", list,
                hiddenlist,
            new List<String>() { },
            renameList,
            new List<string>() { "BasicSalary", "EligibleAmount", "OTAmount" }
            , new Dictionary<string, string>() { }
            , new List<string> { });


        }


    }
}