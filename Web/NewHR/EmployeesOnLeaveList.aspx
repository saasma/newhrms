﻿<%@ Page Title="Employees on leave" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="EmployeesOnLeaveList.aspx.cs" Inherits="Web.NewHR.EmployeesOnLeaveList" %>

<%@ Register Src="~/Employee/UserControls/TimeAttendCommentCtrl.ascx" TagName="TimeAttendCommentCtrl"
    TagPrefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
<script type="text/javascript">

    function halfDayRenderer(e1, e2, e3) {
        if (e2.record.data.IsHalfDayLeave)
            return 'Full Day';
        return 'Half Day';
    }

</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Employees on leave
                </h4>
            </div>
        </div>
    </div>
    <div style="padding-top: 20px; margin-left: 20px;">
        <table style='margin-bottom: 15px;'>
            <tr>
                <td>
                    <ext:DateField ID="DateField1" runat="server">
                    </ext:DateField>
                </td>
                <td style='padding-left: 20px;'>
                    <ext:Button runat="server" ID="btnLoad" Cls="btn btn-default" Text="<i></i>Load"
                        runat="server">
                        <DirectEvents>
                            <Click OnEvent="Load1_Click">
                                <EventMask ShowMask="true" />
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </td>
            </tr>
        </table>
        <ext:TabPanel ID="TabPanel1" runat="server" Height="400">
          <%--  <DirectEvents>
                <TabChange OnEvent="Tab_Change">
                </TabChange>
            </DirectEvents>--%>
            <Defaults>
                <ext:Parameter Name="bodyPadding" Value="10" Mode="Raw" />
                <ext:Parameter Name="autoScroll" Value="true" Mode="Raw" />
            </Defaults>
            <Items>
                <ext:Panel ID="Panel1" runat="server" Title="Today" AutoDataBind="true">
                    <Content>
                        <ext:GridPanel StyleSpec="margin-top:15px;" ID="GridOnLeaveToday" runat="server"
                            Cls="itemgrid">
                            <%-- <Plugins>
                                                                <ext:Badge ID="Badge1" runat="server" ScaleCustom="badge110" Cls="badge3" Text="3"
                                                                    TextStyle="color:yellow;" />
                                                            </Plugins>--%>
                            <Store>
                                <ext:Store ID="storeOnLeaveToday" runat="server">
                                    <Model>
                                        <ext:Model ID="Model2" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="Name" Type="String" />
                                                <ext:ModelField Name="Branch" Type="string" />
                                                <ext:ModelField Name="LeaveType" Type="string" />
                                                <ext:ModelField Name="IsHalfDayLeave" Type="Boolean" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <ColumnModel>
                                <Columns>
                                    <ext:Column ID="Column1" Sortable="false" MenuDisabled="true" runat="server" Text="Employee"
                                        Width="200" Align="Left" DataIndex="Name" />
                                    <ext:Column ID="Column2" Sortable="false" MenuDisabled="true" runat="server" Text="Branch"
                                        Width="200" Align="Left" DataIndex="Branch">
                                    </ext:Column>
                                    <ext:Column ID="Column3" Sortable="false" MenuDisabled="true" runat="server" Text="Leave Type"
                                        Width="175" Align="Left" DataIndex="LeaveType">
                                    </ext:Column>
                                    <ext:Column ID="Column15" Sortable="false" MenuDisabled="true" runat="server" Text=""
                                        Width="200" Align="Left" DataIndex="IsHalfDayLeave">
                                        <Renderer Fn="halfDayRenderer" />
                                        </ext:Column>
                                </Columns>
                            </ColumnModel>
                        </ext:GridPanel>
                    </Content>
                </ext:Panel>
                <ext:Panel ID="Panel2" runat="server" Title="This Week" AutoDataBind="true">
                    <Content>
                        <ext:GridPanel StyleSpec="margin-top:15px;" ID="GridOnLeaveThisWeek" runat="server"
                            Cls="itemgrid">
                            <Store>
                                <ext:Store ID="storeOnLeaveThisWeek" runat="server">
                                    <Model>
                                        <ext:Model ID="Model3" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="EmployeeName" Type="String" />
                                                <ext:ModelField Name="BranchName" Type="string" />
                                                <ext:ModelField Name="Title" Type="string" />
                                                <ext:ModelField Name="DateOnEng" Type="string" />
                                                <ext:ModelField Name="StatusCol" Type="string" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <ColumnModel>
                                <Columns>
                                    <ext:Column ID="Column4" Sortable="false" MenuDisabled="true" runat="server" Text="Employee"
                                        Width="200" Align="Left" DataIndex="EmployeeName" />
                                    <ext:Column ID="Column7" Sortable="false" MenuDisabled="true" runat="server" Text="Branch"
                                        Width="200" Align="Left" DataIndex="BranchName">
                                    </ext:Column>
                                    <ext:Column ID="Column8" Sortable="false" MenuDisabled="true" runat="server" Text="Leave Type"
                                        Width="150" Align="Left" DataIndex="Title">
                                    </ext:Column>
                                    <ext:DateColumn ID="DateColumn2" Sortable="false" MenuDisabled="true" runat="server"
                                        Text="Day" Format="l" Width="120" Align="Left" DataIndex="DateOnEng">
                                    </ext:DateColumn>
                                    <ext:Column ID="Column17" Sortable="false" MenuDisabled="true" runat="server" Text=""
                                        Width="120" Align="Left" DataIndex="StatusCol">
                                    </ext:Column>
                                    <ext:Column ID="Column16" runat="server" />
                                </Columns>
                            </ColumnModel>
                        </ext:GridPanel>
                    </Content>
                </ext:Panel>
                <ext:Panel ID="Panel3" runat="server" Title="Next Week" AutoDataBind="true">
                    <Content>
                        <ext:GridPanel StyleSpec="margin-top:15px;" ID="GridOnLeaveNextWeek" runat="server"
                            Cls="itemgrid">
                            <Store>
                                <ext:Store ID="storeOnLeaveNextWeek" runat="server">
                                    <Model>
                                        <ext:Model ID="Model5" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="EmployeeName" Type="String" />
                                                <ext:ModelField Name="BranchName" Type="string" />
                                                <ext:ModelField Name="Title" Type="string" />
                                                <ext:ModelField Name="DateOnEng" Type="string" />
                                                <ext:ModelField Name="StatusCol" Type="string" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <ColumnModel>
                                <Columns>
                                    <ext:Column ID="Column9" Sortable="false" MenuDisabled="true" runat="server" Text="Employee"
                                        Width="200" Align="Left" DataIndex="EmployeeName" />
                                    <ext:Column ID="Column10" Sortable="false" MenuDisabled="true" runat="server" Text="Branch"
                                        Width="200" Align="Left" DataIndex="BranchName">
                                    </ext:Column>
                                    <ext:Column ID="Column11" Sortable="false" MenuDisabled="true" runat="server" Text="Leave Type"
                                        Width="150" Align="Left" DataIndex="Title">
                                    </ext:Column>
                                    <ext:DateColumn ID="DateColumn1" Sortable="false" MenuDisabled="true" runat="server"
                                        Text="Day" Format="dd-MM-yyyy" Width="120" Align="Left" DataIndex="DateOnEng">
                                    </ext:DateColumn>
                                    <ext:Column ID="Column19" Sortable="false" MenuDisabled="true" runat="server" Text=""
                                        Width="120" Align="Left" DataIndex="StatusCol">
                                    </ext:Column>
                                    <ext:Column ID="Column18" runat="server" />
                                </Columns>
                            </ColumnModel>
                        </ext:GridPanel>
                    </Content>
                </ext:Panel>
                <ext:Panel ID="Panel4" runat="server" Title="This Month" AutoDataBind="true">
                    <Content>
                        <ext:GridPanel StyleSpec="margin-top:15px;" ID="GridOnLeaveThisMonth" runat="server"
                            Cls="itemgrid">
                            <Store>
                                <ext:Store ID="storeOnLeaveThisMonth" runat="server">
                                    <Model>
                                        <ext:Model ID="Model6" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="EmployeeName" Type="String" />
                                                <ext:ModelField Name="BranchName" Type="string" />
                                                <ext:ModelField Name="Title" Type="string" />
                                                <ext:ModelField Name="DateOnEng" Type="string" />
                                                <ext:ModelField Name="StatusCol" Type="string" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <ColumnModel>
                                <Columns>
                                    <ext:Column ID="Column12" Sortable="false" MenuDisabled="true" runat="server" Text="Employee"
                                        Width="200" Align="Left" DataIndex="EmployeeName" />
                                    <ext:Column ID="Column13" Sortable="false" MenuDisabled="true" runat="server" Text="Branch"
                                        Width="200" Align="Left" DataIndex="BranchName">
                                    </ext:Column>
                                    <ext:Column ID="Column14" Sortable="false" MenuDisabled="true" runat="server" Text="Leave Type"
                                        Width="150" Align="Left" DataIndex="Title">
                                    </ext:Column>
                                    <ext:DateColumn ID="Column20" Sortable="false" MenuDisabled="true" runat="server"
                                        Text="Day" Format="dd-MM-yyyy" Width="120" Align="Left" DataIndex="DateOnEng">
                                    </ext:DateColumn>
                                    <ext:Column ID="Column21" Sortable="false" MenuDisabled="true" runat="server" Text=""
                                        Width="120" Align="Left" DataIndex="StatusCol">
                                    </ext:Column>
                                    <ext:Column ID="Column22" runat="server" />
                                </Columns>
                            </ColumnModel>
                        </ext:GridPanel>
                    </Content>
                </ext:Panel>
            </Items>
        </ext:TabPanel>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
