﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Entity;
using BLL;
using System.IO;
using Utils;
using Utils.Helper;

namespace Web.NewHR
{
    public partial class EmployeeLeaveDetails : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack && !X.IsAjaxRequest)
                Initialise();

            JavascriptHelper.AttachPopUpCode(Page, "popupUpdateDeduction", "../cp/AEDeduction.aspx", 600, 600);
            JavascriptHelper.AttachPopUpCode(Page, "popupUpdateLeave", "../cp/AELeave.aspx", 600, 720);
        }

        private void Initialise()
        {
            storeLeaveType.DataSource = NewHRManager.GetLeaveType().OrderBy(x => x.Order).ToList();
            storeLeaveType.DataBind();

            storeEmployee.DataSource = EmployeeManager.GetAllEmployeesForEmployeeImportExport();
            storeEmployee.DataBind();

            cmbBranch.Store[0].DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            cmbBranch.Store[0].DataBind();

            cmbDepartment.Store[0].DataSource = DepartmentManager.GetAllDepartments();
            cmbDepartment.Store[0].DataBind();

            storeLevel.DataSource = ActivityManager.GetLevels();
            storeLevel.DataBind();

            cmbDesignation.Store[0].DataSource = new CommonManager().GetAllDesignations().OrderBy(x => x.Name).ToList();
            cmbDesignation.Store[0].DataBind();

            List<KeyValue> list = new JobStatus().GetMembers();
            list.RemoveAt(0);
            cmbJobStatus.Store[0].DataSource = list;
            cmbJobStatus.Store[0].DataBind();

            if (SessionManager.IsReadOnlyUser)
            {
                CommandColumnChange.Hide();
                btnApply.Hide();
                btnUnapply.Hide();
            }
        }

        

        protected void btnLoad_Click(object sender, DirectEventArgs e)
        {
            if (cmbLeaveType.SelectedItem == null || cmbLeaveType.SelectedItem.Value == null)
            {
                NewMessage.ShowWarningMessage("Please select deduction type.");
                cmbLeaveType.Focus();
                return;
            }

            int leaveTypeId = int.Parse(cmbLeaveType.SelectedItem.Value);
            string employeeIdList = "";

            foreach (Tag tag in tfEmployee.Tags)
            {
                employeeIdList += tag.Value + ",";
            }

            if (!string.IsNullOrEmpty(employeeIdList))
                employeeIdList = employeeIdList.TrimEnd(',');

            int branchId = -1, departmentId = -1, designationId = -1, levelId = -1, applySatus = 0;
            string statusList = "";

            if (cmbBranch.SelectedItem != null && cmbBranch.SelectedItem.Value != null)
                branchId = int.Parse(cmbBranch.SelectedItem.Value);

            if (cmbBranch.SelectedItem != null && cmbBranch.SelectedItem.Value != null)
                branchId = int.Parse(cmbBranch.SelectedItem.Value);

            if (cmbDepartment.SelectedItem != null && cmbDepartment.SelectedItem.Value != null)
                departmentId = int.Parse(cmbDepartment.SelectedItem.Value);

            if (cmbDesignation.SelectedItem != null && cmbDesignation.SelectedItem.Value != null)
                designationId = int.Parse(cmbDesignation.SelectedItem.Value);

            if (cmbLevel.SelectedItem != null && cmbLevel.SelectedItem.Value != null)
                levelId = int.Parse(cmbLevel.SelectedItem.Value);

            foreach (Ext.Net.ListItem item in cmbJobStatus.SelectedItems)
                statusList += item.Value.ToString() + ",";

            if (statusList != "")
                statusList = statusList.TrimEnd(',');

            if (cmbStatus.SelectedItem != null && cmbStatus.SelectedItem.Value != null)
                applySatus = int.Parse(cmbStatus.SelectedItem.Value);

            storeEmpLeave.DataSource = NewHRManager.GetGetEmployeeLeaveDetailsList(employeeIdList, leaveTypeId, branchId, departmentId, designationId, levelId,
                statusList, applySatus);
            storeEmpLeave.DataBind();           
                        
        }

        protected void btnApply_Click(object sender, DirectEventArgs e)
        {
            string gridItemsJson = e.ExtraParams["gridItems"];
            List<GetEmployeeLeaveDetailsListResult> list = JSON.Deserialize<List<GetEmployeeLeaveDetailsListResult>>(gridItemsJson);
            if (list.Count <= 0)
            {
                NewMessage.ShowWarningMessage("Please select employees to apply leave.");
                return;
            }

            
            int leavetypeId = list[0].LeaveTypeId;
            int count = 0;
            Status status = NewHRManager.AddLeaveToEmployees(list, leavetypeId, out count);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage(string.Format("Leave applied to {0} employees", count));
                btnLoad_Click(null, null);

                CheckboxSelectionModel1.SelectedRows.Clear();
                CheckboxSelectionModel1.UpdateSelection();
            }
            else
                NewMessage.ShowWarningMessage(status.ErrorMessage);

        }

        protected void btnUnapply_Click(object sender, DirectEventArgs e)
        {
            string gridItemsJson = e.ExtraParams["gridItems"];
            List<GetEmployeeLeaveDetailsListResult> list = JSON.Deserialize<List<GetEmployeeLeaveDetailsListResult>>(gridItemsJson);

            if (list.Count <= 0)
            {
                NewMessage.ShowWarningMessage("Please select employees to unapply deduction.");
                return;
            }

            int leaveTypeId = list[0].LeaveTypeId;

            Status status = NewHRManager.DeleteLeaveFromEmployees(list, leaveTypeId);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Leave unapplied to the employees successfully.");
                btnLoad_Click(null, null);

                CheckboxSelectionModel1.SelectedRows.Clear();
                CheckboxSelectionModel1.UpdateSelection();
            }
            else
                NewMessage.ShowWarningMessage(status.ErrorMessage);


        }


    }
}