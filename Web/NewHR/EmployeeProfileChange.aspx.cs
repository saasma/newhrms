﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;
using System.Text;

namespace Web.NewHR
{
    public partial class EmployeeProfileChange : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                BindGrid();

                Initialise();

                cmbType.SelectedItem.Text = "Name";
                GridPanel2.Hidden = true;
                GridPanel1.Hidden = false;

                btnSave.Hidden = false;
                btnSave2.Hidden = true;

                btnDelete1.Hidden = false;
                btnDelete2.Hidden = true;
                
            }
        }

        private void Initialise()
        {
            CommonManager comManager = new CommonManager();

            cmbCountryPerm.Store[0].DataSource = comManager.GetAllCountries();
            cmbCountryPerm.Store[0].DataBind();

            cmbCountryTemp.Store[0].DataSource = comManager.GetAllCountries();
            cmbCountryTemp.Store[0].DataBind();

            cmbZonePerm.Store[0].DataSource = comManager.GetAllZones();
            cmbZonePerm.Store[0].DataBind();

            cmbZoneTemp.Store[0].DataSource = comManager.GetAllZones();
            cmbZoneTemp.Store[0].DataBind();
        }
      
        private void BindGrid()
        {
            this.Store1.DataSource = NewHRManager.GetAllEmpProfileChangeList();
            this.Store1.DataBind();

            this.Store2.DataSource = NewHRManager.GetAllEmpProfileChangeList();
            this.Store2.DataBind();
        }

        protected void Button1_Click(object sender, DirectEventArgs e)
        {
            lblMsg.Html = "";
            string gridItemsJson = e.ExtraParams["gridItems"];
            List<SelectAll_EmployeeProfileChangeResult> list = JSON.Deserialize<List<SelectAll_EmployeeProfileChangeResult>>(gridItemsJson);
            List<SelectAll_EmployeeProfileChangeResult> newList = new List<SelectAll_EmployeeProfileChangeResult>();

            foreach (var item in list)
            {
                SelectAll_EmployeeProfileChangeResult obj = new SelectAll_EmployeeProfileChangeResult();
                obj.EmployeeId = item.EmployeeId;
                obj.Id = item.Id;
                obj.Tbl = item.Tbl;
                newList.Add(obj);
            }

            if (newList.Count == 0)
            {
                SetWarning(lblMsg, "Select employee profile change to approve.");
                return;
            }

            Status status = NewHRManager.ApproveEmployeeProfileChange(newList);
            if (status.IsSuccess)
            {
                CheckboxSelectionModel1.ClearSelection();
                CheckboxSelectionModel2.ClearSelection();

                BindGrid();
                SetMessage(lblMsg, "Employee profile change approved successfully.");
            }
            else
            {
                SetError(lblMsg, "Error while approving employee profile changes.");
            }
        }

        protected void btnView_Click(object sender, DirectEventArgs e)
        {
            int employeeId = int.Parse(hdnEmployeeId.Text);
            int tblId = int.Parse(hdnTableID.Text);
            string tableName = hdnTableName.Text;

            if (tableName == "HFamily")
            {
                WFamily.Show();               
                ucFamily.EditFamily(tblId);
                ucFamily.HideButtons();
            }
            else if (tableName == "HPreviousEmployment")
            {
                AEPreviousEmploymentWindow.Show();
                ucEmploymentCtrl.HideButtons();
                ucEmploymentCtrl.editPreviousEmployment(tblId);
            }
            else if (tableName == "HEducation")
            {
                HEducation obj = NewHRManager.GetEducationDetailsById(tblId);
                if (obj != null)
                {
                    if (obj.UserFileName != null)
                        AEEducationWindow.Height = 600;
                    else
                        AEEducationWindow.Height = 570;
                }
                AEEducationWindow.Show();
                ucEdu.HideButtons();
                ucEdu.editEducation(tblId);
            }
            else if (tableName == "HTraining")
            {
                HTraining obj = NewHRManager.GetTrainingDetailsById(tblId);
                if (obj != null)
                {
                    if (obj.UserFileName != null)
                        AETrainingWindow.Height = 640;
                    else
                        AETrainingWindow.Height = 580;
                }

                AETrainingWindow.Show();
                trainCtrl.HideButtons();
                trainCtrl.editTraining(tblId);
            }
            else if (tableName == "HSeminar")
            {
                AESeminarWindow.Show();
                seminarCtrl.HideButtons();
                seminarCtrl.editSeminar(tblId);
            }
            else if (tableName == "SkillSetEmployee")
            {
                AESkillSetsWindow.Show();
                skillSetCtrl.HideButtons();
                skillSetCtrl.SetEmployeeId = employeeId;
                skillSetCtrl.editSkillSets(tblId);
            }
            else if (tableName == "LanguageSetEmployee")
            {
                AELanguageSetsWindow.Show();
                languageSetCtrl.HideButtons();
                languageSetCtrl.SetEmployeeId = employeeId;
                languageSetCtrl.editLanguageSets(tblId);
                
            }
            else if (tableName == "HPublication")
            {
                AEPublicationWindow.Show();
                publicationCtrl.HideButtons();
                publicationCtrl.editPublication(tblId);
            }
            else if (tableName == "EAddress")
            {
                LoadEditContact(employeeId);
                LoadEditData(employeeId);
                AEEAddress.Show();
            }
            else if (tableName == "HCitizenship")
            {
                AECitizenshipWindow.Show();
                ucCtznCtrl.HideButtons();
                ucCtznCtrl.editCitizenship(tblId);
            }
            else if (tableName == "HDrivingLicence")
            {
                AEDrivingLiscenceWindow.Show();
                ucDrivLicense.HideButtons();
                ucDrivLicense.editDrivingLiscence(tblId);
            }
            else if (tableName == "HPassport")
            {
                AEPassportWindow.Show();
                ucPassport.HideButtons();
                ucPassport.editPassport(tblId);
            }
            else if (tableName == "HDocument")
            {
                AEDocument.Show();
                ucDocument.HideButtons();
                ucDocument.EditDoc(tblId);
            }
        }

        protected void btnComboChange_Click(object sender, DirectEventArgs e)
        {
            int value = int.Parse(cmbType.SelectedItem.Value.ToString());
            if (value == 1)
            {
                GridPanel1.Hidden = false;
                GridPanel2.Hidden = true;

                btnSave.Hidden = false;
                btnSave2.Hidden = true;

                btnDelete1.Hidden = false;
                btnDelete2.Hidden = true;
            }
            else
            {
                GridPanel1.Hidden = true;
                GridPanel2.Hidden = false;

                btnSave.Hidden = true;
                btnSave2.Hidden = false;

                btnDelete1.Hidden = true;
                btnDelete2.Hidden = false;
            }
            
        }

        protected void btnDownload_Click(object sender, DirectEventArgs e)
        {
            int ID = int.Parse(hdnTableID.Text);
            string tableName = hdnTableName.Text;

            if (tableName == "HEducation")
            {
                HEducation doc = NewHRManager.GetEducationDetailsById(ID);

                string path = Server.MapPath(@"~/uploads/" + doc.FileLocation + doc.ServerFileName);
                string contentType = doc.FileType;
                string name = doc.UserFileName + "." + doc.FileFormat;

                byte[] bytes = File.ReadAllBytes(path);

                Context.Response.Clear();
                Context.Response.ClearHeaders();
                Context.Response.AppendHeader("Content-Disposition", "attachment; filename=\"" + name + "\"");
                Context.Response.ContentType = contentType;
                Context.Response.BinaryWrite(bytes);
                Context.Response.End();
            }
            else if (tableName == "HTraining")
            {
                HTraining doc = NewHRManager.GetTrainingDetailsById(ID);

                string path = Server.MapPath(@"~/uploads/" + doc.FileLocation + doc.ServerFileName);
                string contentType = doc.FileType;
                string name = doc.UserFileName + "." + doc.FileFormat;

                byte[] bytes = File.ReadAllBytes(path);

                Context.Response.Clear();
                Context.Response.ClearHeaders();
                Context.Response.AppendHeader("Content-Disposition", "attachment; filename=\"" + name + "\"");
                Context.Response.ContentType = contentType;
                Context.Response.BinaryWrite(bytes);
                Context.Response.End();
            }
            else if (tableName == "HCitizenship")
            {
                HCitizenship doc = NewHRManager.GetCitizenshipDetailsById(ID);

                string path = Context.Server.MapPath(@"~/uploads/" + doc.FileLocation + doc.ServerFileName);
                string contentType = doc.FileType;
                string name = doc.UserFileName + "." + doc.FileFormat;

                byte[] bytes = File.ReadAllBytes(path);

                Context.Response.Clear();
                Context.Response.ClearHeaders();
                Context.Response.AppendHeader("Content-Disposition", "attachment; filename=\"" + name + "\"");
                Context.Response.ContentType = contentType;
                Context.Response.BinaryWrite(bytes);
                Context.Response.End();
            }
            else if (tableName == "HDrivingLicence")
            {
                HDrivingLicence doc = NewHRManager.GetDrivingLicenceDetailsById(ID);

                string path = Context.Server.MapPath(@"~/uploads/" + doc.FileLocation + doc.ServerFileName);
                string contentType = doc.FileType;
                string name = doc.UserFileName + "." + doc.FileFormat;

                byte[] bytes = File.ReadAllBytes(path);

                Context.Response.Clear();
                Context.Response.ClearHeaders();
                Context.Response.AppendHeader("Content-Disposition", "attachment; filename=\"" + name + "\"");
                Context.Response.ContentType = contentType;
                Context.Response.BinaryWrite(bytes);
                Context.Response.End();
            }
            else if (tableName == "HPassport")
            {
                HPassport doc = NewHRManager.GetPassportDetailsById(ID);

                string path = Context.Server.MapPath(@"~/uploads/" + doc.FileLocation + doc.ServerFileName);
                string contentType = doc.FileType;
                string name = doc.UserFileName + "." + doc.FileFormat;

                byte[] bytes = File.ReadAllBytes(path);

                Context.Response.Clear();
                Context.Response.ClearHeaders();
                Context.Response.AppendHeader("Content-Disposition", "attachment; filename=\"" + name + "\"");
                Context.Response.ContentType = contentType;
                Context.Response.BinaryWrite(bytes);
                Context.Response.End();
            }
            else if (tableName == "HDocument")
            {
                HDocument obj = NewPayrollManager.GetDocumentById(ID);

                string fileName = obj.Url.ToString();
                if (!string.IsNullOrEmpty(fileName))
                    Response.Redirect("~/DocumentHandler.ashx?id=" + System.IO.Path.GetFileNameWithoutExtension(fileName).ToString());
                else
                    NewMessage.ShowWarningMessage("This file does not exist.");
            }
        }

        private void LoadEditContact(int employeeId)
        {
            TempEAddress objTempEAddress = NewHRManager.GetTempEAddressByEmployeeId(employeeId);
            if (objTempEAddress != null)
            {
                EAddress objEAddress = NewHRManager.GetAddressByID(employeeId);
                if (objEAddress != null && objEAddress.CIEmail != null)
                    txtEmailOfficial.Text = objEAddress.CIEmail;  

                txtPhoneOfficial.Text = objTempEAddress.CIPhoneNo;

                if(!string.IsNullOrEmpty(objTempEAddress.Extension))
                    txtExtentionOfficial.Text = objTempEAddress.Extension;
                txtMobileOfficial.Text = objTempEAddress.CIMobileNo;

                txtEmailPersonal.Text = objTempEAddress.PersonalEmail;
                txtMobilePersonal.Text = objTempEAddress.PersonalMobile;
                txtPhonePersonal.Text = objTempEAddress.PersonalPhone;

                txtRelationEmergency.Text = objTempEAddress.EmergencyRelation;
                txtNameEmergency.Text = objTempEAddress.EmergencyName;
                txtPhoneEmergency.Text = objTempEAddress.EmergencyPhone;
                txtMobileEmergency.Text = objTempEAddress.EmergencyMobile;
            }
            
        }


        public void LoadEditData(int employeeId)
        {
            TempEAddress objTempEAddress = NewHRManager.GetTempEAddressByEmployeeId(employeeId);

            if (objTempEAddress != null)
            {
                if (objTempEAddress.PECountryId != null)
                {
                    cmbCountryPerm.SetValue(objTempEAddress.PECountryId.ToString());
                }
                if (objTempEAddress.PEZoneId != null)
                {
                    cmbZonePerm.SetValue(objTempEAddress.PEZoneId.ToString());

                    CommonManager comManager = new CommonManager();
                    cmbDistrictPerm.Clear();
                    cmbDistrictPerm.Store[0].DataSource = comManager.GetAllDistricts(objTempEAddress.PEZoneId.Value);
                    cmbDistrictPerm.Store[0].DataBind();
                }

                if (objTempEAddress.PEDistrictId != null)
                    cmbDistrictPerm.SetValue(objTempEAddress.PEDistrictId.ToString());

                if (objTempEAddress.PSCountryId != null)
                    cmbCountryTemp.SetValue(objTempEAddress.PSCountryId.ToString());
                if (objTempEAddress.PSZoneId != null)
                {
                    cmbZoneTemp.SetValue(objTempEAddress.PSZoneId.ToString());

                    CommonManager comManager = new CommonManager();
                    cmbDistrictTemp.Clear();
                    cmbDistrictTemp.Store[0].DataSource = comManager.GetAllDistricts(objTempEAddress.PSZoneId.Value);
                    cmbDistrictTemp.Store[0].DataBind();
                }
                if (objTempEAddress.PSDistrictId != null)
                    cmbDistrictTemp.SetValue(objTempEAddress.PSDistrictId.ToString());

                cmbVDCPerm.Text = objTempEAddress.PEVDCMuncipality;
                txtStreetColonyPerm.Text = objTempEAddress.PEStreet;
                txtWardNoPerm.Text = objTempEAddress.PEWardNo;
                txtHouseNoPerm.Text = objTempEAddress.PEHouseNo;
                txtStatePerm.Text = objTempEAddress.PEState;
                txtZipCodePerm.Text = objTempEAddress.PEZipCode;
                txtLocalityPerm.Text = objTempEAddress.PELocality;

                cmbVDCTemp.Text = objTempEAddress.PSVDCMuncipality;
                txtStreetColonyTemp.Text = objTempEAddress.PSStreet;
                txtWardNoTemp.Text = objTempEAddress.PSWardNo;
                txtHouseNoTemp.Text = objTempEAddress.PSHouseNo;
                txtStateTemp.Text = objTempEAddress.PSState;
                txtZipCodeTemp.Text = objTempEAddress.PSZipCode;
                txtLocalityTemp.Text = objTempEAddress.PSLocality;
                txtCitIssuseDist.Text = objTempEAddress.PSCitIssDis;
            }
            
        }

        protected void btnDelete_Click(object sender, DirectEventArgs e)
        {
            lblMsg.Html = "";
            string gridItemsJson = e.ExtraParams["gridItems"];
            List<SelectAll_EmployeeProfileChangeResult> list = JSON.Deserialize<List<SelectAll_EmployeeProfileChangeResult>>(gridItemsJson);
            List<SelectAll_EmployeeProfileChangeResult> newList = new List<SelectAll_EmployeeProfileChangeResult>();

            foreach (var item in list)
            {
                SelectAll_EmployeeProfileChangeResult obj = new SelectAll_EmployeeProfileChangeResult();
                obj.EmployeeId = item.EmployeeId;
                obj.Id = item.Id;
                obj.Tbl = item.Tbl;
                newList.Add(obj);
            }

            if (newList.Count == 0)
            {
                SetWarning(lblMsg, "Select employee profile change to delete.");
                return;
            }

            Status status = NewHRManager.DeleteEmployeeProfileChange(newList);
            if (status.IsSuccess)
            {
                CheckboxSelectionModel1.ClearSelection();
                CheckboxSelectionModel2.ClearSelection();

                BindGrid();
                SetMessage(lblMsg, "Employee profile change deleted successfully.");
            }
            else
            {
                SetError(lblMsg, "Error while deleting employee profile changes.");
            }
        }

    }
}