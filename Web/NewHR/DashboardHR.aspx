<%@ Page Title="HR Dashboard" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="DashboardHR.aspx.cs" Inherits="Web.CP.DashboardHR" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .dbTitle {
            display: inline-block;
            background-color: #E7EFFC;
            width: 97%;
            position: relative;
            border-bottom: thin solid #B2D0E8;
            line-height: 30px;
            vertical-align: middle;
            padding-left: 5px;
            padding-right: 6px;
            border-right: 1px solid #B2D0E8;
        }

            .dbTitle a {
                float: right;
                text-decoration: none; /* border:thin solid gray;*/
                cursor: pointer;
            }

            .dbTitle h2 {
                color: #0070C0;
                float: left;
                font-size: 12px;
                margin-top: 4px;
                margin-bottom: 4px;
            }

        fv .subTitle {
            color: #616161;
            font-weight: bold;
            font-size: 12px !important;
            background: #F9F9F9;
            line-height: 20px;
            width: 99%;
            margin-top: 2px;
            margin-bottom: 2px;
        }

        .dbPanel {
            display: none;
            border: thin solid #B2D0E8;
            margin: 10px 0px 15px 0px;
        }

        .dbPanelContent {
            padding: 3px;
            display: inline-block;
            width: 100%;
            overflow: hidden; /*width:100%;*/
        }

        .Column {
            width: 33%;
        }

        .contentWrapper > table {
            /* padding-left: 8px;
            padding-right: 8px;*/
            width: 100%;
        }

            .contentWrapper > table td {
                vertical-align: top; /* width:100%;*/ /* width:33%;*/
                padding-right: 5px;
            }

        .hideHeader .x-grid-header-ct {
            background-image: none !important;
            display: none !important;
            border: none !important;
        }

        .contentWrapper .x-grid-cell-last > .x-grid-cell-inner {
            text-align: right !important;
        }

        .roundCorner {
            border: thin solid #4cae4c;
            -moz-border-radius: 4px 4px 4px 4px;
            -webkit-border-radius: 4px 4px 4px 4px;
            padding: 0px;
            background: -moz-linear-gradient(center top, #999999 0%,#ffffff 0%);
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0, #999999),color-stop(0, #ffffff));
        }

        .spanCurrentMonth {
            position: relative !important;
            display: inline !important;
            background-color: transparent !important;
            border: transparent !important;
            left: 0px !important;
            padding-left: 0px !important;
        }

        /*options cases*/
        .option {
            padding-top: 10px;
        }

            .option a, .option span {
                text-align: center;
                text-decoration: none;
                font-weight: bold;
            }

            .option img, .option span {
                display: block;
                text-decoration: none;
                font-size: 14px;
                width: 150px;
            }

            .option img {
            }

        .reportBlock hr {
            margin: 7px 0 0 35px;
        }

        .reportArrow {
            margin: 3px 0 0 10px;
            width: 15px;
            height: 15px;
            float: left;
        }

        .reports {
            padding: 5px 0;
        }

        .x-grid3-cell-inner {
            padding-top: 0px;
            padding-bottom: 0px;
            padding-left: 0px;
            padding-right: 0px;
        }

        .x-grid-componentcolumn .x-grid-cell .x-grid-cell-inner {
            border: none;
        }
    </style>
    <style type="text/css">
        .hideLeftBlockCssInPage {
            margin: 0px !important;
            padding-left: 0px !important;
        }

        #menu {
            display: none;
        }
    </style>
    <script type="text/javascript">

        var myRenderer1 = function (value, metadata) {

            if (value != null && value != '') {
                if (value == 'On')
                    metadata.style = "background-color: #92d050; color:#006100;";
                else
                    metadata.style = "background-color: lightsalmon; color:#9c0006;";
            }

            return value;
        };


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>HR Dashboard
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel" style="padding-left: 10px">
        <div class="innerLR">
            <div>
                <div>
                    <div style="width: 100%; visibility: visible" class="contentWrapper" id="container">
                        <table>
                            <tr>
                                <td class="Column" style="width: 70%">
                                    <div class="col-md-4" style="display: none1;">
                                        <div class="panel panel-success-alt noborder">
                                            <div class="panel-heading noborder" style="border-radius: 0px;">
                                                <!-- panel-btns -->
                                                <div class="panel-icon">
                                                    <i class="fa fa-home"></i>
                                                </div>
                                                <div class="media-body">
                                                    <h5 class="md-title nomargin">BRANCHES</h5>
                                                    <h1 class="mt5">
                                                        <span id="spanBranches" runat="server" />
                                                    </h1>
                                                </div>
                                                <!-- media-body -->
                                                <hr>
                                                <div class="clearfix mt20">
                                                    <div class="pull-left">
                                                        <h5 class="md-title nomargin">DEPARTMENTS</h5>
                                                        <h4 class="nomargin">
                                                            <span id="spanDepartments" runat="server" />
                                                        </h4>
                                                    </div>
                                                    <div class="pull-right">
                                                        <h5 class="md-title nomargin">LOCATIONS</h5>
                                                        <h4 class="nomargin">
                                                            <span id="spanLocations" runat="server" />
                                                        </h4>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- panel-body -->
                                        </div>
                                        <!-- panel -->
                                    </div>
                                    <!-- col-md-4 -->
                                    <div class="col-md-4" style="display: none1;">
                                        <div class="panel panel-primary noborder">
                                            <div class="panel-heading noborder" style="border-radius: 0px;">
                                                <!-- panel-btns -->
                                                <div class="panel-icon">
                                                    <i class="fa fa-users"></i>
                                                </div>
                                                <div class="media-body">
                                                    <h5 class="md-title nomargin">EMPLOYEES</h5>
                                                    <h1 class="mt5">
                                                        <span id="spanTotalEmployees" runat="server" />
                                                    </h1>
                                                </div>
                                                <!-- media-body -->
                                                <hr>
                                                <div class="clearfix mt20">
                                                    <div class="pull-left">
                                                        <h5 class="md-title nomargin">MALE</h5>
                                                        <h4 class="nomargin">
                                                            <span id="spanMaleEmployees" runat="server" />
                                                        </h4>
                                                    </div>
                                                    <div class="pull-right">
                                                        <h5 class="md-title nomargin">FEMALE</h5>
                                                        <h4 class="nomargin">
                                                            <span id="spanFemaleEmployees" runat="server" />
                                                        </h4>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- panel-body -->
                                        </div>
                                        <!-- panel -->
                                    </div>
                                    <!-- col-md-4 -->
                                    <div class="col-md-4" style="display: none1;">
                                        <div class="panel panel-dark noborder">
                                            <div class="panel-heading noborder" style="border-radius: 0px;">
                                                <div class="panel-btns">
                                                    <%--<a href="#" class="panel-close tooltips" data-toggle="tooltip" data-placement="left"
                                                        title="Close Panel"><i class="fa fa-times"></i></a>--%>
                                                </div>
                                                <!-- panel-btns -->
                                                <div class="panel-icon">
                                                    <i class="fa fa-users"></i>
                                                </div>
                                                <div class="media-body">
                                                    <h5 class="md-title nomargin">EMPLOYEES</h5>
                                                    <h1 class="mt5">
                                                        <span id="spTotalEmployees" runat="server" />
                                                    </h1>
                                                </div>
                                                <!-- media-body -->
                                                <hr>
                                                <div class="clearfix mt20">
                                                    <div class="pull-left">
                                                        <h5 class="md-title nomargin">NEW JOIN</h5>
                                                        <h4 class="nomargin">
                                                            <span id="spanNewJoin" runat="server" />
                                                        </h4>
                                                    </div>
                                                    <div class="pull-right">
                                                        <h5 class="md-title nomargin">RETIRE</h5>
                                                        <h4 class="nomargin">
                                                            <span id="spanRetired" runat="server" />
                                                        </h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div style="clear: both; margin-bottom: 15px; padding-left: 10px; color: Gray; display: none">
                                        <table>
                                            <tr>
                                                <td>
                                                    <div style="width: 110px; height: 80px; background-color: #bfbfbf; text-align: center; padding-top: 20px;">
                                                        <div style="font-weight: bold; font-size: 18px;">
                                                            <span id="spanBr" runat="server"></span>
                                                            <br />
                                                        </div>
                                                        <div style="padding-top: 5px;">
                                                            BRANCHES
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div style="width: 110px; height: 80px; background-color: #d8d8d8; text-align: center; padding-top: 20px; margin-left: -5px;">
                                                        <div style="font-weight: bold; font-size: 18px;">
                                                            <span id="spanDept" runat="server"></span>
                                                            <br />
                                                        </div>
                                                        <div style="padding-top: 5px;">
                                                            DEPARTMENTS
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div style="width: 3px; height: 80px; background-color: #E4E7EA; text-align: center; padding-top: 20px; margin-left: 15px;">
                                                    </div>
                                                </td>

                                                <td>
                                                    <div style="width: 110px; height: 80px; background-color: #bcd6ee; text-align: center; padding-top: 20px; margin-left: 15px;">
                                                        <div style="font-weight: bold; font-size: 18px;">
                                                            <span id="spanEmp" runat="server"></span>
                                                            <br />
                                                        </div>
                                                        <div style="padding-top: 5px;">
                                                            EMPLOYEES
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div style="width: 110px; height: 80px; background-color: #deebf6; text-align: center; padding-top: 20px; margin-left: -5px;">
                                                        <div style="font-weight: bold; font-size: 18px;">
                                                            <span id="spanEMale" runat="server"></span>
                                                            <br />
                                                        </div>
                                                        <div style="padding-top: 5px;">
                                                            MALE
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div style="width: 110px; height: 80px; background-color: #ffe1ff; text-align: center; padding-top: 20px; margin-left: -5px;">
                                                        <div style="font-weight: bold; font-size: 18px;">
                                                            <span id="spanEFemale" runat="server"></span>
                                                            <br />
                                                        </div>
                                                        <div stlyle="padding-top:5px;">
                                                            FEMALE
                                                        </div>
                                                    </div>
                                                </td>

                                                <td>
                                                    <div style="width: 3px; height: 80px; background-color: #E4E7EA; text-align: center; padding-top: 20px; margin-left: 15px;">
                                                    </div>
                                                </td>
                                                <td>
                                                    <div style="width: 110px; height: 80px; background-color: #e2efd9; text-align: center; padding-top: 20px; margin-left: 15px;">
                                                        <div style="font-weight: bold; font-size: 18px;">
                                                            <span id="spanENewJoin" runat="server"></span>
                                                            <br />
                                                        </div>
                                                        <div style="padding-top: 5px;">
                                                            NEW JOIN
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div style="width: 110px; height: 80px; background-color: #fbe4d5; text-align: center; padding-top: 20px; margin-left: -5px;">
                                                        <div style="font-weight: bold; font-size: 18px;">
                                                            <span id="spanERetire" runat="server"></span>
                                                            <br />
                                                        </div>
                                                        <div style="padding-top: 5px;">
                                                            RETIRE
                                                        </div>
                                                    </div>
                                                </td>

                                            </tr>

                                        </table>


                                    </div>

                                    <div class="btn-list" style="padding-left: 10px">
                                        <a class="btn btn-default btn-metro" href="~/newhr/EmployeeList.aspx" runat="server"
                                            visible='<%# IsAccessible("newhr/EmployeeList.aspx") %>'><span><span class="glyphicon dashboard">
                                                <img src="../css/DashboardIcons/employee_list.png" alt="" /></span> Employees</span></a>
                                        <a id="linkServiceHistory" class="btn btn-default btn-metro" href="~/CP/servicehistorylist.aspx"
                                            runat="server" visible='<%# IsAccessible("CP/servicehistorylist.aspx") %>'><span><span
                                                class="glyphicon dashboard">
                                                <img src="../css/DashboardIcons/employee_list.png" alt="" /></span> Service History</span></a>
                                        <a id="A4" class="btn btn-default btn-metro" href="~/newhr/employeesearch.aspx" runat="server"
                                            visible='<%# IsAccessible("newhr/employeesearch.aspx") %>'><span><span class="glyphicon dashboard">
                                                <img src="../css/DashboardIcons/employee_list.png" alt="" /></span> Employee Advance
                                                Search</span></a> <a class="btn btn-default btn-metro" visible='<%# IsAccessible("cp/AAEmployeeCatalogue.aspx") %>'
                                                    id="A3" runat="server" href="~/cp/AAEmployeeCatalogue.aspx"><span class="glyphicon dashboard">
                                                        <img src="../css/DashboardIcons/employee_list.png" alt="" /></span>Employee
                                                    Catalogue</a> <a href="~/newhr/PersonalDetail.aspx" runat="server" visible='<%# IsAccessible("newhr/PersonalDetail.aspx") %>'>
                                                        <span class="btn btn-default btn-metro"><span class="glyphicon dashboard">
                                                            <img src="../css/DashboardIcons/add_employee.png" alt="" /></span> Add Employee</span></a>
                                        <a href="~/CP/BranchTransferList.aspx" runat="server" visible='<%# IsAccessible("CP/BranchTransferList.aspx") %>'>
                                            <span class="btn btn-default btn-metro"><span class="glyphicon dashboard">
                                                <img src="../css/DashboardIcons/briefcase.png" alt="" /></span>Branch Transfer</span></a>
                                        <a id="A1" href="~/CP/DepartmentTransferList.aspx" runat="server" visible='<%# IsAccessible("CP/DepartmentTransferList.aspx") %>'>
                                            <span class="btn btn-default btn-metro"><span class="glyphicon dashboard">
                                                <img src="../css/DashboardIcons/dept_transfer.png" alt="" /></span>Department Transfer</span></a>
                                        <a id="A6" href="~/newhr/DeputationList.aspx" runat="server" visible='<%# IsAccessible("newhr/DeputationList.aspx") %>'>
                                            <span class="btn btn-default btn-metro"><span class="glyphicon dashboard">
                                                <img src="../css/DashboardIcons/deputation.png" alt="" /></span>Deputation</span></a>
                                        <a id="A36" href="~/CP/DesignationTransferList.aspx" runat="server" visible='<%# IsAccessible("CP/DesignationTransferList.aspx") %>'>
                                            <span class="btn btn-default btn-metro"><span class="glyphicon dashboard">
                                                <img src="../css/DashboardIcons/designation.png" alt="" /></span>Designation Change</span></a>
                                        <a class="btn btn-default btn-metro" visible='<%# IsAccessible("CP/LocationTransferList.aspx") %>'
                                            id="A10" runat="server" href="~/CP/LocationTransferList.aspx"><span class="glyphicon dashboard">
                                                <img src="../css/DashboardIcons/designation.png" alt="" /></span> Location Transfer</a>
                                        <a class="btn btn-default btn-metro" runat="server" id="liNonLevelGrade" visible='<%# IsAccessible("CP/GradeTransferList.aspx") %>'
                                            href="~/CP/GradeTransferList.aspx"><span class="glyphicon dashboard">
                                                <img src="../css/DashboardIcons/designation.png" alt="" /></span>Grade Change</a>
                                        <a class="btn btn-default btn-metro" visible='<%# IsAccessible("newhr/PromotionListGeneral.aspx") %>'
                                            id="levelPromotionList" runat="server" href="~/newhr/PromotionListGeneral.aspx">
                                            <span class="glyphicon dashboard">
                                                <img src="../css/DashboardIcons/designation.png" alt="" /></span>Promotion /
                                            Grade Change</a> <a class="btn btn-default btn-metro" visible='<%# IsAccessible("newhr/SalaryChangeList.aspx") %>'
                                                id="levelSalaryChange" runat="server" href="~/newhr/SalaryChangeList.aspx"><span
                                                    class="glyphicon dashboard">
                                                    <img src="../css/DashboardIcons/designation.png" alt="" /></span>Salary Scale
                                                Grade Fitting </a><a class="btn btn-default btn-metro" visible='<%# IsAccessible("newhr/ActingList.aspx") %>'
                                                    id="acting" runat="server" href="~/newhr/ActingList.aspx"><span class="glyphicon dashboard">
                                                        <img src="../css/DashboardIcons/designation.png" alt="" /></span>Acting</a>
                                        <a class="btn btn-default btn-metro" visible='<%# IsAccessible("CP/EmployeeAwardList.aspx") %>'
                                            id="EmployeeAwardList" runat="server" href="~/CP/EmployeeAwardList.aspx"><span class="glyphicon dashboard">
                                                <img src="../css/DashboardIcons/designation.png" alt="" /></span>Employee Award
                                            List</a>
                                    </div>
                                    <div class="col-sm-12" style="display: none;">
                                        <div class="panel panel-primary">
                                            <div class="panel-heading">
                                                <div class="panel-btns" style="display: none;">
                                                    <a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel">
                                                        <i class="fa fa-minus"></i></a>
                                                </div>
                                                <h3 class="panel-title">Regular Tasks</h3>
                                            </div>
                                            <div class="panel-body">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <ul role="menu" class="dropdown-menu dropdown-demo-only icongap">
                                                                <li class="dashboardMenuHeader"><a>Employee</a></li>
                                                                <li class="divider fulldivider" role="presentation"></li>
                                                                <li><a visible='<%# IsAccessible("newhr/PersonalDetail.aspx") %>' id="A9" runat="server"
                                                                    href="~/newhr/PersonalDetail.aspx"><i class="fa fa-bars"></i>Add Employee</a></li>
                                                                <li><a visible='<%# IsAccessible("newhr/EmployeeListing.aspx") %>' id="A2" runat="server"
                                                                    href="~/newhr/EmployeeListing.aspx"><i class="fa fa-bars"></i>Employee List</a></li>
                                                            </ul>
                                                        </td>
                                                        <td>
                                                            <ul role="menu" class="dropdown-menu dropdown-demo-only icongap">
                                                                <li class="dashboardMenuHeader"><a>Transfer</a></li>
                                                                <li class="divider fulldivider" role="presentation"></li>
                                                                <li></li>
                                                                <li><a visible='<%# IsAccessible("CP/BranchTransferList.aspx") %>' id="A34" runat="server"
                                                                    href="~/CP/BranchTransferList.aspx"><i class="fa fa-bars"></i>Transfer</a></li>
                                                                <%--  <li><a visible='<%# IsAccessible("CP/DepartmentTransferList.aspx") %>' id="A40" runat="server"
                                                                    href="~/CP/DepartmentTransferList.aspx" ><i class="fa fa-bars"></i>
                                                                    Department Transfer</a></li>--%>
                                                                <li><a visible='<%# IsAccessible("newhr/DeputationList.aspx") %>' id="A5" runat="server"
                                                                    href="~/newhr/DeputationList.aspx"><i class="fa fa-bars"></i>Deputation</a></li>
                                                                <li runat="server" id="transferDesignation"><a visible='<%# IsAccessible("cp/DesignationTransferList.aspx") %>'
                                                                    id="A16" runat="server" href="~/cp/DesignationTransferList.aspx"><i class="fa fa-bars"></i>Designation Change</a></li>
                                                            </ul>
                                                        </td>
                                                        <td runat="server" id="otherLevelGradeAvilablCompany">
                                                            <ul role="menu" class="dropdown-menu dropdown-demo-only icongap">
                                                                <li class="dashboardMenuHeader"><a>Level/Grade</a></li>
                                                                <li class="divider fulldivider" role="presentation"></li>
                                                                <li><a visible='<%# IsAccessible("newhr/PromotionListGeneral.aspx") %>' id="A33"
                                                                    runat="server" href="~/newhr/PromotionListGeneral.aspx"><i class="fa fa-bars"></i>
                                                                    Promotion</a></li>
                                                                <li><a visible='<%# IsAccessible("newhr/SalaryChangeList.aspx") %>' id="A35" runat="server"
                                                                    href="~/newhr/SalaryChangeList.aspx"><i class="fa fa-bars"></i>Salary Grade Change
                                                                </a></li>
                                                                <li><a visible='<%# IsAccessible("newhr/ActingList.aspx") %>' id="A41" runat="server"
                                                                    href="~/newhr/ActingList.aspx"><i class="fa fa-bars"></i>Acting</a></li>
                                                            </ul>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <!-- panel-body -->
                                        </div>
                                        <!-- panel -->
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="panel panel-dark-head no-radius">
                                            <div class="panel-heading no-radius">
                                                <div class="panel-btns" style="display: none;">
                                                    <a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel">
                                                        <i class="fa fa-minus"></i></a>
                                                </div>
                                                <h3 class="panel-title">Common Tasks</h3>
                                            </div>
                                            <div class="panel-body panel-body-small">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <ul role="menu" class="dropdown-menu dropdown-demo-only icongap">
                                                                <li class="dashboardMenuHeader"><a>Message</a></li>
                                                                <li class="divider fulldivider" role="presentation"></li>
                                                                <li><a id="A37" runat="server" visible='<%# IsAccessible("CP/ComposeMessage.aspx") %>'
                                                                    href="~/CP/ComposeMessage.aspx" target="_self"><i class="fa fa-bars"></i>Send Message</a></li>
                                                                <%--<li><a id="A38" runat="server" visible='<%# IsAccessible("CP/Extension/MailMerge.aspx") %>'
                                                                href="~/CP/Extension/MailMerge.aspx" target="_self"><i class="fa fa-bars"></i>Mail
                                                                Merge</a></li>--%>
                                                                <li><a id="A7" runat="server" href="~/cp/NoticeList.aspx" visible='<%# IsAccessible("cp/NoticeList.aspx") %>'
                                                                    target="_self"><i class="fa fa-bars"></i>Notice List</a></li>
                                                            </ul>
                                                            <div style="clear: both">
                                                            </div>
                                                            <ul role="menu" class="dropdown-menu dropdown-demo-only icongap">
                                                                <li class="dashboardMenuHeader"><a>Employee Approval</a></li>
                                                                <li class="divider fulldivider" role="presentation"></li>
                                                                <li>
                                                                    <asp:HyperLink ID="HyperLink2" Visible='<%# IsAccessible("NewHR/EmployeeProfileChange.aspx") %>'
                                                                        runat="server" NavigateUrl="~/NewHR/EmployeeProfileChange.aspx">
                                                         <i class="fa fa-bars"></i> Profile Change
                                                                    </asp:HyperLink>
                                                                </li>
                                                                <li>
                                                                    <asp:HyperLink ID="HyperLink3" Visible='<%# IsAccessible("NewHR/EmpIdentificationApproval.aspx") %>'
                                                                        runat="server" NavigateUrl="~/NewHR/EmpIdentificationApproval.aspx">
                                                         <i class="fa fa-bars"></i> Photo Approval
                                                                    </asp:HyperLink>
                                                                </li>
                                                            </ul>
                                                            
                                                            <div style="clear: both">
                                                            </div>
                                                            <ul role="menu" class="dropdown-menu dropdown-demo-only icongap">
                                                                <li class="dashboardMenuHeader"><a>Seniority</a></li>
                                                                 <li class="divider fulldivider" role="presentation"></li>
                                                                 <li>
                                                                    <asp:HyperLink ID="HyperLink33" Visible='<%# IsAccessible("NewHR/EmployeeSeniority.aspx") %>'
                                                                        runat="server" NavigateUrl="~/NewHR/EmployeeSeniority.aspx">
                                                                     <i class="fa fa-bars"></i> Set Seniority
                                                                                </asp:HyperLink>
                                                                            </li>
                                                             </ul>

                                                            <div style="clear: both">
                                                            </div>
                                                            <ul role="menu" class="dropdown-menu dropdown-demo-only icongap" runat="server" id="ulDutySchedule" visible="false">
                                                                <li class="dashboardMenuHeader"><a>Duty Schedule</a></li>
                                                                <li class="divider fulldivider" role="presentation"></li>
                                                                <li>
                                                                    <asp:HyperLink ID="HyperLink26" Visible='<%# IsAccessible("NewHR/HPLDutyPeriodList.aspx") %>'
                                                                        runat="server" NavigateUrl="~/NewHR/HPLDutyPeriodList.aspx">
                                                         <i class="fa fa-bars"></i> Duty Schedule Periods
                                                                    </asp:HyperLink>
                                                                </li>
                                                                <li>
                                                                    <asp:HyperLink ID="HyperLink27" Visible='<%# IsAccessible("NewHR/EmpDutySchedule.aspx") %>'
                                                                        runat="server" NavigateUrl="~/NewHR/EmpDutySchedule.aspx">
                                                         <i class="fa fa-bars"></i> Employee Duty Schedule
                                                                    </asp:HyperLink>
                                                                </li>
                                                            </ul>
                                                        </td>
                                                        <td>
                                                            <ul role="menu" runat="server" id="ulTravelRequest" class="dropdown-menu dropdown-demo-only icongap">
                                                                <li class="dashboardMenuHeader"><a>Travel Request</a></li>
                                                                <li class="divider fulldivider" role="presentation"></li>
                                                                <li><a visible='<%# IsAccessible("travelrequest/TravelRequestListingAdminView.aspx") %>'
                                                                    id="A22" runat="server" href="~/travelrequest/TravelRequestListingAdminView.aspx">
                                                                    <i class="fa fa-bars"></i>Travel Requests</a></li>
                                                                <%-- <li><a visible='<%# IsAccessible("travelrequest/TravelRequestAdvance.aspx") %>' id="A21"
                                                                runat="server" href="~/TravelRequest/TravelRequestAdvance.aspx"><i class="fa fa-bars">
                                                                </i>Travel Advance</a></li>
                                                            <li><a visible='<%# IsAccessible("travelrequest/SettlementReportFinance.aspx") %>'
                                                                id="A23" runat="server" href="~/TravelRequest/SettlementReportFinance.aspx"><i class="fa fa-bars">
                                                                </i>Advance Settlement Report</a></li>--%>
                                                                <li><a visible='<%# IsAccessible("CP/TADAForward.aspx") %>' id="linkTADAList" runat="server"
                                                                    href="~/CP/TADAForward.aspx"><i class="fa fa-bars"></i>TADA Requests</a></li>
                                                                <li><a visible='<%# IsAccessible("CP/AllowanceLocation.aspx") %>' id="A45" runat="server"
                                                                    href="~/CP/AllowanceLocation.aspx"><i class="fa fa-bars"></i>Allowance Location</a></li>
                                                            </ul>
                                                            <div style="clear: both">
                                                            </div>
                                                            <ul role="menu" runat="server" id="ulDailyAcitivities" class="dropdown-menu dropdown-demo-only icongap">
                                                                <li class="dashboardMenuHeader"><a>Daily Activities</a></li>
                                                                <li class="divider fulldivider" role="presentation"></li>

                                                                <li><a id="A13" runat="server" href="~/NewHR/EmployeeActivityList.aspx" visible='<%# IsAccessible("NewHR/EmployeeActivityList.aspx") %>'
                                                                    target="_self"><i class="fa fa-bars"></i>Employee Activity</a></li>

                                                                <li><a style="display:none;" visible='<%# IsAccessible("NewHR/EmployeeActivityReport.aspx") %>' id="A48" runat="server" title="Employee Activity Report"
                                                                    href="~/NewHR/EmployeeActivityReport.aspx" target="_self"><i class="fa fa-bars"></i>Emp Activity Report
                                                                </a></li>
                                                                <li><a visible='<%# IsAccessible("NewHR/EmployeePlanList.aspx") %>' id="A49" runat="server" title="Employee Plan Report"
                                                                    href="~/NewHR/EmployeePlanList.aspx" target="_self"><i class="fa fa-bars"></i>Employee Plan
                                                                </a></li>
                                                                <li><a visible='<%# IsAccessible("NewHr/HREmpActivitySummary.aspx") %>' id="A15" runat="server" title="Employee Activity Summary Report"
                                                                    href="~/NewHr/HREmpActivitySummary.aspx" target="_self"><i class="fa fa-bars"></i>Activity Summary
                                                                </a></li>
                                                            </ul>
                                                        </td>
                                                        <td>
                                                            <ul role="menu" class="dropdown-menu dropdown-demo-only icongap">
                                                                <li class="dashboardMenuHeader"><a>Overtime and Allowance</a></li>
                                                                <li class="divider fulldivider" role="presentation"></li>
                                                                <li runat="server" id="linkOTUnitReport"><a runat="server" visible='<%# IsAccessible("newhr/OvertimeAllowanceUnit.aspx") %>'
                                                                    href="~/newhr/OvertimeAllowanceUnit.aspx"><i class="fa fa-bars"></i>Overtime/Allowance
                                                                Unit</a></li>
                                                                <li runat="server" id="liGenerateOvertime"><a visible='<%# IsAccessible("CP/overtimelist.aspx") %>'
                                                                    id="A42" runat="server" href="~/CP/overtimelist.aspx"><i class="fa fa-bars"></i>
                                                                    Generate Overtime </a></li>
                                                                <li runat="server" id="linkOTRequests"><a visible='<%# IsAccessible("CP/AAForwardOvertime.aspx") %>' id="A24" runat="server"
                                                                    href="~/CP/AAForwardOvertime.aspx"><i class="fa fa-bars"></i>Overtime Requests</a></li>
                                                                <li runat="server" id="linkOTCalculationReport"><a visible='<%# IsAccessible("NewHR/EmployeeOTCalculation.aspx") %>' id="A8"
                                                                    runat="server" href="~/NewHR/EmployeeOTCalculation.aspx"><i class="fa fa-bars"></i>
                                                                    Overtime Request Calculation</a></li>
                                                                <li runat="server" id="linkAllowanceRequests"><a visible='<%# IsAccessible("CP/AllowanceForward.aspx") %>' id="A32" runat="server"
                                                                    href="~/CP/AllowanceForward.aspx"><i class="fa fa-bars"></i>Allowance Requests</a></li>
                                                                <li runat="server" id="linkChildrenAllowance"><a visible='<%# IsAccessible("NewHR/ChildrenAllowances.aspx") %>' id="linkChildrenAllowance1" runat="server"
                                                                    href="~/NewHR/ChildrenAllowances.aspx"><i class="fa fa-bars"></i>Children Allowances</a></li>
                                                                <li runat="server" visible="false" id="leaveFareMenu"><a visible='<%# IsAccessible("CP/LeaveFareDetails.aspx") %>' id="A50" runat="server"
                                                                    href="~/CP/LeaveFareDetails.aspx"><i class="fa fa-bars"></i>Leave Fare Allowances</a></li>
                                                                <li runat="server" visible="false" id="menuHolidayPay"><a visible='<%# IsAccessible("CP/HolidayPayDetails.aspx") %>' id="A141" runat="server"
                                                                    href="~/CP/HolidayPayDetails.aspx"><i class="fa fa-bars"></i>Holiday Pay</a></li>
                                                            </ul>
                                                        </td>




                                                         <td>
                                                            <ul role="menu" class="dropdown-menu dropdown-demo-only icongap">
                                                                <li class="dashboardMenuHeader"><a>Resignation</a></li>
                                                                <li class="divider fulldivider" role="presentation"></li>
                                                                <li runat="server" id="li7"><a runat="server" visible='<%# IsAccessible("newhr/ResignationRequestList.aspx") %>'
                                                                    href="~/newhr/ResignationRequestList.aspx"><i class="fa fa-bars"></i>Resignation Requests
                                                                </a></li>                                                               
                                                            </ul>
                                                        </td>




                                                        <td>
                                                            <ul role="menu" class="dropdown-menu dropdown-demo-only icongap" runat="server" id="tdTraining">
                                                                <li class="dashboardMenuHeader"><a>Training</a></li>
                                                                <li class="divider fulldivider" role="presentation"></li>
                                                                <li runat="server" id="li2"><a id="A15222" runat="server" visible='<%# IsAccessible("newhr/TrainingMod/TrainingPlanning.aspx") %>'
                                                                    href="~/newhr/TrainingMod/TrainingPlanning.aspx"><i class="fa fa-bars"></i>Create Training</a></li>
                                                                <li runat="server" id="li3"><a visible='<%# IsAccessible("~/newhr/TrainingMod/TrainingAdministration.aspx") %>'
                                                                    id="A20" runat="server" href="~/newhr/TrainingMod/TrainingAdministration.aspx"><i class="fa fa-bars"></i>
                                                                    Training Requests </a></li>

                                                                <li runat="server" id="li6"><a visible='<%# IsAccessible("~/newhr/TrainingMod/Trainingfeedback.aspx") %>'
                                                                    id="A26" runat="server" href="~/newhr/TrainingMod/Trainingfeedback.aspx"><i class="fa fa-bars"></i>
                                                                    Training Feedback </a></li>

                                                                <li runat="server" id="li4"><a visible='<%# IsAccessible("~/newhr/TrainingMod/TrainingEmployee.aspx") %>'
                                                                    id="A21" runat="server" href="~/newhr/TrainingMod/TrainingEmployee.aspx"><i class="fa fa-bars"></i>
                                                                    Training Employee </a></li>

                                                                <li runat="server" id="li5"><a visible='<%# IsAccessible("~/newhr/TrainingMod/TrainingAttendance.aspx") %>'
                                                                    id="A23" runat="server" href="~/newhr/TrainingMod/TrainingEmpAttendance.aspx"><i class="fa fa-bars"></i>
                                                                    Training Attendance </a></li>

                                                            </ul>

                                                        </td>

                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="panel panel-dark-head no-radius">
                                            <div class="panel-heading no-radius">
                                                <div class="panel-btns" style="display: none;">
                                                    <a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel">
                                                        <i class="fa fa-minus"></i></a>
                                                </div>
                                                <h3 class="panel-title">Reports</h3>
                                            </div>
                                            <div class="panel-body panel-body-small">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <ul role="menu" class="dropdown-menu dropdown-demo-only custommenugap">
                                                                <li class="dashboardMenuHeader"><a>Employee Reports</a></li>
                                                                <li class="divider fulldivider" role="presentation"></li>
                                                                <li>
                                                                    <asp:HyperLink ID="HyperLink4" Visible='<%# IsAccessible("CP/Report/NewHrReport/HeadCount.aspx") %>'
                                                                        runat="server" NavigateUrl="~/CP/Report/NewHrReport/HeadCount.aspx">
                                                         <i class="fa fa-bars"></i> Head Count
                                                                    </asp:HyperLink>
                                                                </li>
                                                                <li>
                                                                    <asp:HyperLink ID="HyperLink11" Visible='<%# IsAccessible("CP/Report/NewHrReport/ReportGenderProfile.aspx") %>'
                                                                        runat="server" NavigateUrl="~/CP/Report/NewHrReport/ReportGenderProfile.aspx">
                                                         <i class="fa fa-bars"></i>Gender Profile
                                                                    </asp:HyperLink>
                                                                </li>
                                                                <li>
                                                                    <asp:HyperLink ID="HyperLink25" Visible='<%# IsAccessible("CP/Report/NewHrReport/reportGenderwisestatus.aspx") %>'
                                                                        runat="server" NavigateUrl="~/CP/Report/NewHrReport/reportGenderwisestatus.aspx">
                                                         <i class="fa fa-bars"></i>Genderwise Service Status
                                                                    </asp:HyperLink>
                                                                </li>
                                                                <li>
                                                                    <asp:HyperLink ID="HyperLink12" Visible='<%# IsAccessible("CP/Report/NewHrReport/ReportAgeProfile.aspx") %>'
                                                                        runat="server" NavigateUrl="~/CP/Report/NewHrReport/ReportAgeProfile.aspx">
                                                         <i class="fa fa-bars"></i>Age Profile
                                                                    </asp:HyperLink>
                                                                </li>
                                                                <li>
                                                                    <asp:HyperLink ID="HyperLink23" Visible='<%# IsAccessible("newhr/EmployeeByAge.aspx") %>'
                                                                        runat="server" NavigateUrl="~/newhr/EmployeeByAge.aspx">
                                                         <i class="fa fa-bars"></i>Agewise Employee
                                                                    </asp:HyperLink>
                                                                </li>
                                                                <li>
                                                                    <asp:HyperLink ID="HyperLink24" Visible='<%# IsAccessible("newhr/EmployeeReference.aspx") %>'
                                                                        runat="server" NavigateUrl="~/newhr/EmployeeReference.aspx">
                                                         <i class="fa fa-bars"></i>Employee Reference
                                                                    </asp:HyperLink>
                                                                </li>
                                                                <li>
                                                                    <asp:HyperLink ID="HyperLink13" Visible='<%# IsAccessible("CP/Report/NewHrReport/ReportPayBranchDepartmentWise.aspx") %>'
                                                                        runat="server" NavigateUrl="~/CP/Report/NewHrReport/ReportPayBranchDepartmentWise.aspx">
                                                         <i class="fa fa-bars"></i>Pay Branch/Department
                                                                    </asp:HyperLink>
                                                                </li>
                                                                <li>
                                                                    <asp:HyperLink ID="HyperLink14" Visible='<%# IsAccessible("CP/Report/NewHrReport/ReportBirthdays.aspx") %>'
                                                                        runat="server" NavigateUrl="~/CP/Report/NewHrReport/ReportBirthdays.aspx">
                                                         <i class="fa fa-bars"></i>Birthdays
                                                                    </asp:HyperLink>
                                                                </li>
                                                                <li>
                                                                    <asp:HyperLink ID="HyperLink15" Visible='<%# IsAccessible("CP/Report/NewHrReport/ReportAdditionsTerminations.aspx") %>'
                                                                        runat="server" NavigateUrl="~/CP/Report/NewHrReport/ReportAdditionsTerminations.aspx">
                                                         <i class="fa fa-bars"></i>Additions and Terminations
                                                                    </asp:HyperLink>
                                                                </li>
                                                                <li>
                                                                    <asp:HyperLink ID="HyperLink20" Visible='<%# IsAccessible("NewHR/EmployeeCountReport.aspx") %>'
                                                                        runat="server" NavigateUrl="~/NewHR/EmployeeCountReport.aspx">
                                                         <i class="fa fa-bars"></i>Employee Count
                                                                    </asp:HyperLink>
                                                                </li>
                                                                <li>
                                                                    <asp:HyperLink ID="HyperLink21" Visible='<%# IsAccessible("NewHR/EmployeeCurrentPaySalary.aspx") %>'
                                                                        runat="server" NavigateUrl="~/NewHR/EmployeeCurrentPaySalary.aspx">
                                                         <i class="fa fa-bars"></i>Employee Current Pay
                                                                    </asp:HyperLink>
                                                                </li>
                                                                <li>
                                                                    <asp:HyperLink ID="HyperLink22" Visible='<%# IsAccessible("CP/EmployeeLatestEvent.aspx") %>'
                                                                        runat="server" NavigateUrl="~/CP/EmployeeLatestEvent.aspx">
                                                         <i class="fa fa-bars"></i>Employee Latest Event
                                                                    </asp:HyperLink>
                                                                </li>
                                                            </ul>
                                                        </td>
                                                        <td>
                                                            <ul role="menu" class="dropdown-menu dropdown-demo-only custommenugap">
                                                                <li class="dashboardMenuHeader"><a>HR Reports</a></li>
                                                                <li class="divider fulldivider" role="presentation"></li>
                                                                <li>
                                                                    <asp:HyperLink ID="HyperLink1" Visible='<%# IsAccessible("CP/EmployeeMaster.aspx") %>'
                                                                        Text="" runat="server" NavigateUrl="~/CP/EmployeeMaster.aspx" pageid="48">
                                                         <i class="fa fa-bars"></i> Employee Master
                                                                    </asp:HyperLink>
                                                                </li>
                                                                <li>
                                                                    <asp:HyperLink Visible='<%# IsAccessible("CP/Report/HREmployeeList.aspx") %>' ID="HyperLink5"
                                                                        Text="" runat="server" NavigateUrl="~/CP/Report/HREmployeeList.aspx" pageid="48">
                                                         <i class="fa fa-bars"></i> Employee List
                                                                    </asp:HyperLink>
                                                                </li>
                                                                <li>
                                                                    <asp:HyperLink Visible='<%# IsAccessible("CP/Report/EmployeeListDetails.aspx") %>'
                                                                        ID="HyperLink6" Text="" runat="server" NavigateUrl="~/CP/Report/EmployeeListDetails.aspx"
                                                                        pageid="106">
                                                                 <i class="fa fa-bars"></i> Employee HR Details
                                                                    </asp:HyperLink>
                                                                </li>
                                                                <li>
                                                                    <asp:HyperLink Visible='<%# IsAccessible("CP/Report/HRStatusChange.aspx") %>' ID="HyperLink7"
                                                                        Text="" runat="server" NavigateUrl="~/CP/Report/HRStatusChange.aspx">
                                                  <i class="fa fa-bars"></i>    Status Change Report
                                                                    </asp:HyperLink>
                                                                </li>
                                                                <li>
                                                                    <asp:HyperLink ID="HyperLink8" Visible='<%# IsAccessible("CP/Report/HRDateOfJoining.aspx") %>'
                                                                        Text="" runat="server" NavigateUrl="~/CP/Report/HRDateOfJoining.aspx" pageid="47">
                                                                 <i class="fa fa-bars"></i> Date Of Joining
                                                                    </asp:HyperLink>
                                                                </li>
                                                                <li>
                                                                    <asp:HyperLink Visible='<%# IsAccessible("CP/Report/HREmployeeContact.aspx") %>'
                                                                        ID="HyperLink9" Text="" runat="server" NavigateUrl="~/CP/Report/HREmployeeContact.aspx"
                                                                        pageid="51">
                                                                     <i class="fa fa-bars"></i> Employee Contacts
                                                                    </asp:HyperLink>
                                                                </li>
                                                                <li>
                                                                    <asp:HyperLink ID="HyperLink10" Visible='<%# IsAccessible("CP/Report/SkillsetReport.aspx") %>'
                                                                        Text="Skill Set Report" runat="server" NavigateUrl="~/CP/Report/SkillsetReport.aspx"
                                                                        pageid="108">
                                                                 <i class="fa fa-bars"></i> Skill Set Report
                                                                    </asp:HyperLink>
                                                                </li>
                                                                <li>
                                                                    <asp:HyperLink ID="HyperLink16" Visible='<%# IsAccessible("CP/Report/NewHrReport/TrainingList.aspx") %>'
                                                                        Text="Training Report" runat="server" NavigateUrl="~/CP/Report/NewHrReport/TrainingList.aspx"
                                                                        pageid="109">
                                                                 <i class="fa fa-bars"></i> Training Report
                                                                    </asp:HyperLink>
                                                                </li>
                                                                <li>
                                                                    <asp:HyperLink ID="HyperLink19" Visible='<%# IsAccessible("NewHR/servicehistorylisting.aspx") %>'
                                                                        Text="Service History Report" runat="server" NavigateUrl="~/NewHR/servicehistorylisting.aspx"
                                                                        pageid="109">
                                                                 <i class="fa fa-bars"></i>Service History Report
                                                                    </asp:HyperLink>
                                                                </li>

                                                                <li>
                                                                    <asp:HyperLink ID="HyperLink34" Visible='<%# IsAccessible("NewHr/EmployeeAddressReport.aspx") %>'
                                                                        Text="Employee Address Report" runat="server" NavigateUrl="~/NewHR/EmployeeAddressReport.aspx"
                                                                        pageid="109">
                                                                 <i class="fa fa-bars"></i>Employee Address Report
                                                                    </asp:HyperLink>
                                                                </li>

                                                                <li>
                                                                    <asp:HyperLink ID="HyperLink31" Visible='<%# IsAccessible("NewHr/EmpInfoList.aspx") %>'
                                                                        Text="Employee Information Report" runat="server" NavigateUrl="~/NewHR/EmpInfoList.aspx"
                                                                        pageid="109">
                                                                 <i class="fa fa-bars"></i>Employee Information Report
                                                                    </asp:HyperLink>
                                                                </li>

                                                                <li>
                                                                    <asp:HyperLink ID="HyperLink29" Visible='<%# IsAccessible("NewHr/EmpEducationList.aspx") %>'
                                                                        Text="Employee Education Report" runat="server" NavigateUrl="~/NewHR/EmpEducationList.aspx"
                                                                        pageid="109">
                                                                 <i class="fa fa-bars"></i>Employee Education Report
                                                                    </asp:HyperLink>
                                                                </li>
                                                                <li>
                                                                    <asp:HyperLink ID="HyperLink30" Visible='<%# IsAccessible("NewHr/EmployeePreviousExperience.aspx") %>'
                                                                        Text="Employee Experience Report" runat="server" NavigateUrl="~/NewHR/EmployeePreviousExperience.aspx"
                                                                        pageid="109">
                                                                 <i class="fa fa-bars"></i>Employee Experience Report
                                                                    </asp:HyperLink>
                                                                </li>
                                                            </ul>
                                                        </td>
                                                        <td>
                                                            <ul role="menu" class="dropdown-menu dropdown-demo-only custommenugap">
                                                                <li class="dashboardMenuHeader"><a>Other Reports</a></li>
                                                                <li class="divider fulldivider" role="presentation"></li>
                                                                <li><a id="A18" visible='<%# IsAccessible("CP/ReportNew/RemoteAreaEmployeeList.aspx") %>'
                                                                    runat="server" href="~/CP/ReportNew/RemoteAreaEmployeeList.aspx" target="_self">
                                                                    <i class="fa fa-bars"></i>Remote Area Employees</a> </li>
                                                                <li runat="server" id="levelGradePromotionHistory"><a id="A19" runat="server" visible='<%# IsAccessible("newhr/PromotionListReport.aspx") %>'
                                                                    href="~/newhr/PromotionListReport.aspx" target="_self"><i class="fa fa-bars"></i>
                                                                    Promotion History</a> </li>
                                                                <li><a id="A12" runat="server" visible='<%# IsAccessible("CP/Report/HRNewJoinList.aspx") %>'
                                                                    href="~/CP/Report/HRNewJoinList.aspx" target="_self"><i class="fa fa-bars"></i>New
                                                                    Join list</a> </li>
                                                                <li><a id="A28" visible='<%# IsAccessible("CP/ServicePeriodList.aspx") %>' runat="server"
                                                                    href="~/CP/ServicePeriodList.aspx" target="_self"><i class="fa fa-bars"></i>Service
                                                                    Period</a> </li>
                                                                <li><a id="A39" visible='<%# IsAccessible("cp/extension/ServiceYearReport.aspx") %>'
                                                                    runat="server" href="~/cp/extension/ServiceYearReport.aspx" target="_self"><i class="fa fa-bars"></i>Check Service Period</a> </li>
                                                                <li><a id="A40" visible='<%# IsAccessible("CP/Report/ClericalNonClericalReport.aspx") %>'
                                                                    runat="server" href="~/CP/Report/ClericalNonClericalReport.aspx" target="_self">
                                                                    <i class="fa fa-bars"></i>Clerical and Non-Clerical</a> </li>
                                                            </ul>
                                                            <div style="clear: both">
                                                            </div>

                                                            <ul role="menu" class="dropdown-menu dropdown-demo-only custommenugap">
                                                                <li class="dashboardMenuHeader"><a>Emp Family Reports</a></li>
                                                                <li class="divider fulldivider" role="presentation"></li>
                                                                <li>
                                                                    <asp:HyperLink ID="HyperLink17" Visible='<%# IsAccessible("CP/Report/EmpFamilyMembersReport.aspx") %>'
                                                                        Text="Training Report" runat="server" NavigateUrl="~/CP/Report/EmpFamilyMembersReport.aspx"
                                                                        pageid="109">
                                                                 <i class="fa fa-bars"></i> Emp Family Members
                                                                    </asp:HyperLink>
                                                                </li>
                                                                <li>
                                                                    <asp:HyperLink ID="HyperLink18" Visible='<%# IsAccessible("CP/familylist.aspx") %>'
                                                                        Text="Training Report" runat="server" NavigateUrl="~/CP/familylist.aspx"
                                                                        pageid="109">
                                                                 <i class="fa fa-bars"></i> Family List
                                                                    </asp:HyperLink>
                                                                </li>
                                                                <li>
                                                                    <asp:HyperLink ID="HyperLink32" Visible='<%# IsAccessible("CP/familylistforinsurence.aspx") %>'
                                                                        Text=" Family List For Insurance" runat="server" NavigateUrl="~/CP/familylistforinsurence.aspx"
                                                                        pageid="109">
                                                                 <i class="fa fa-bars"></i> Family List For Insurance
                                                                    </asp:HyperLink>
                                                                </li>
                                                                <li>
                                                                    <asp:HyperLink ID="HyperLink28" Visible='<%# IsAccessible("NewHr/familylist.aspx") %>'
                                                                        Text="Family Report" runat="server" NavigateUrl="~/NewHr/EmpFamilyList.aspx"
                                                                        pageid="109">
                                                                 <i class="fa fa-bars"></i> Employee Family Report
                                                                    </asp:HyperLink>
                                                                </li>
                                                            </ul>
                                                              <div style="clear: both">
                                                            </div>
                                                            <ul role="menu" class="dropdown-menu dropdown-demo-only custommenugap">
                                                                <li class="dashboardMenuHeader"><a>Custom Reports</a></li>
                                                                <li class="divider fulldivider" role="presentation"></li>
                                                                <li><a id="A14" visible='<%# IsAccessible("cp/report/query/ReportViewer.aspx") %>'
                                                                    runat="server" href="~/cp/report/query/ReportViewer.aspx" target="_self">
                                                                    <i class="fa fa-bars"></i>Create Custom Report </a></li>

                                                            </ul>
                                                        </td>


                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- col-sm-6 -->
                                    <div class="widget" style="display: none">
                                        <div class="widget-head" style="display: none">
                                            <h4 class="heading glyphicons history">
                                                <i></i>Custom Forms</h4>
                                        </div>
                                        <div class="widget-body list" runat="server" id="otherRegularTasks" style="display: none">
                                            <div style="clear: both">
                                            </div>
                                            <table class="reportBlock">
                                                <tr>
                                                    <td>
                                                        <div id="Div3" class="reports" runat="server" visible='<%# IsAccessible("DynamicForms/DynamicRequestForm.aspx") %>'>
                                                            <img src="../images/reportBlockArrow.png" class="reportArrow" />
                                                            <a id="A30" runat="server" href="~/DynamicForms/DynamicRequestForm.aspx">Dynamic Form
                                                                Request</a>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div id="Div4" class="reportList" runat="server" visible='<%# IsAccessible("DynamicForms/DynamicFormList.aspx") %>'>
                                                            <div class="reports">
                                                                <img src="../images/reportBlockArrow.png" class="reportArrow" />
                                                                <a id="A31" runat="server" href="~/DynamicForms/DynamicFormList.aspx">Dynamic Form</a>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div id="Div5" class="reportList" runat="server" visible='<%# IsAccessible("DynamicForms/DynamicFormPublishList.aspx") %>'>
                                                            <div class="reports">
                                                                <img src="../images/reportBlockArrow.png" class="reportArrow" />
                                                                <a id="A27" runat="server" href="~/DynamicForms/DynamicFormPublishList.aspx">Dynamic
                                                                    Form Publish</a>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div id="Div6" class="reportList" runat="server" visible='<%# IsAccessible("DynamicForms/DynamicFormEmpListingAdminView.aspx") %>'>
                                                            <div class="reports">
                                                                <img src="../images/reportBlockArrow.png" class="reportArrow" />
                                                                <a id="A29" runat="server" href="~/DynamicForms/DynamicFormEmpListingAdminView.aspx">Dynamic Form Employee</a>
                                                            </div>
                                                        </div>
                                                        <%--<div class="reportList">
                                                        <div class="reports">
                                                            <img src="../images/reportBlockArrow.png" class="reportArrow" />
                                                            <a id="A28"  runat="server" href="~/DynamicForms/AdminDynamicFormEmployeeAdvance.aspx">
                                                                Dynamic Form Employee Advance</a>
                                                        </div>
                                                    </div>--%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td id="Td13" runat="server" visible='<%# IsAccessible("CP/AllowanceLocation.aspx") %>'>
                                                        <div class="reports">
                                                            <img src="../images/reportBlockArrow.png" class="reportArrow" />
                                                            <a id="A43" runat="server" href="~/CP/AllowanceLocation.aspx">Allowance Location</a>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="panel panel-info-alt no-radius">
                                            <div class="panel-heading no-radius">
                                                <div class="panel-btns" style="display: none;">
                                                    <a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel">
                                                        <i class="fa fa-minus"></i></a>
                                                </div>
                                                <h3 class="panel-title">Settings</h3>
                                            </div>
                                            <div class="panel-body panel-body-small">
                                                <ul role="menu" class="dropdown-menu dropdown-demo-only custommenulist">
                                                    <li><a id="A11" visible='<%# IsAccessible("newhr/ApprovalFlow.aspx") %>' runat="server"
                                                        href="~/newhr/ApprovalFlow.aspx"><i class="fa fa-bars"></i>Approval Flow</a>
                                                    </li>
                                                    <li runat="server" id="Li1"><a visible='<%# IsAccessible("CP/OvertimeType.aspx") %>'
                                                        id="A25" runat="server" href="~/CP/OvertimeType.aspx"><i class="fa fa-bars"></i>
                                                        Overtime Types</a></li>
                                                    <li><a id="A17" visible='<%# IsAccessible("CP/AllowanceType.aspx") %>' runat="server"
                                                        href="~/CP/AllowanceType.aspx"><i class="fa fa-bars"></i>Allowance Request Types</a>
                                                    </li>
                                                </ul>
                                                <div style="clear: both">
                                                </div>
                                                <ul role="menu" class="dropdown-menu dropdown-demo-only custommenulist">
                                                    <li><a id="anchorTravelAllowance" visible='<%# IsAccessible("CP/AllowanceSettings.aspx") %>' runat="server"
                                                        href="~/CP/AllowanceSettings.aspx"><i class="fa fa-bars"></i>Travel Allowances</a>
                                                    </li>
                                                    <li><a visible='<%# IsAccessible("newhr/ManageEmployeeManagers.aspx") %>' id="linkEmpSupervisor"
                                                        runat="server" href="~/CP/Extension/ManageEmployeeManagers.aspx"><i class="fa fa-bars"></i>Employee Manager/Supervisor</a> </li>
                                                    <li><a visible='<%# IsAccessible("CP/AllowanceLocation.aspx") %>' id="anchorTravelLocation" runat="server"
                                                        href="~/CP/AllowanceLocation.aspx"><i class="fa fa-bars"></i>Travel Allowance Location</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="Column" style="width: 30%" runat="server" id="tdHR">
                                    <div class="panel panel-info-alt">
                                        <div class="panel-heading">
                                            <!-- panel-btns -->
                                            <h3 class="panel-title">Current Period</h3>
                                        </div>
                                        <div class="panel-body panel-body-small">
                                            <span runat="server" id="currentPeriod"></span>
                                        </div>
                                    </div>
                                    <!-- panel -->
                                    <div class="panel panel-info-alt" runat="server" id="divOvertime" visible="false">
                                        <div class="panel-heading">
                                            <div class="panel-btns" style="display: none;">
                                                <a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel">
                                                    <i class="fa fa-minus"></i></a>
                                            </div>
                                            <h3 class="panel-title">Overtime</h3>
                                            <a style="display: block" href="../cp/AAForwardOvertime.aspx" class="details pull-right viewall">View All</a>
                                        </div>
                                        <div class="panel-body panel-body-small">
                                            <ul class="nav nav-pills nav-stacked dashboardNotificatioin">
                                                <li><a href='../cp/AAForwardOvertime.aspx?status=0'>Pending<span runat="server" id="spanOTPending"
                                                    class="badge badge-success pull-right">0</span> </a></li>
                                                <li><a href='../cp/AAForwardOvertime.aspx?status=1'>Recommended <span class="badge badge-warning pull-right"
                                                    runat="server" id="spanOTRecommended">0</span></a> </li>
                                                <li><a href='../cp/AAForwardOvertime.aspx?status=2'>Approved <span class="badge badge-danger pull-right"
                                                    runat="server" id="spanOTApproved">0</span></a> </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="panel panel-info-alt" runat="server" id="divEveningCounter" visible="false">
                                        <div class="panel-heading">
                                            <div class="panel-btns" style="display: none;">
                                                <a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel">
                                                    <i class="fa fa-minus"></i></a>
                                            </div>
                                            <h3 class="panel-title">Allowance</h3>
                                            <a style="display: block" href="../cp/AllowanceForward.aspx" class="details pull-right viewall">View All</a>
                                        </div>
                                        <div class="panel-body panel-body-small">
                                            <ul class="nav nav-pills nav-stacked dashboardNotificatioin">
                                                <li><a href='../cp/AllowanceForward.aspx?status=0'>Pending<span runat="server" id="spanEveningPending"
                                                    class="badge badge-success pull-right">0</span></a> </li>
                                                <li><a href='../cp/AllowanceForward.aspx?status=1'>Recommended <span class="badge badge-warning pull-right"
                                                    runat="server" id="spanEveningRecommended">0</span> </a></li>
                                                <li><a href='../cp/AllowanceForward.aspx?status=2'>Approved <span class="badge badge-danger pull-right"
                                                    runat="server" id="spanEveningApproved">0</span></a> </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="panel panel-info-alt" runat="server" id="divTravelRequests" visible="false">
                                        <div class="panel-heading">
                                            <div class="panel-btns" style="display: none;">
                                                <a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel">
                                                    <i class="fa fa-minus"></i></a>
                                            </div>
                                            <h3 class="panel-title">Travel Requests</h3>
                                        </div>
                                        <div class="panel-body panel-body-small">
                                            <ul class="nav nav-pills nav-stacked dashboardNotificatioin">
                                                <li><a href='../TravelRequest/TravelRequestAdvance.aspx?status=0'>Approved for Advance<span
                                                    runat="server" id="spanTRApproved" class="badge badge-success pull-right">0</span></a>
                                                </li>
                                                <li><a href='../TravelRequest/TravelRequestAdvance.aspx?status=2'>Employee Settled <span
                                                    class="badge badge-warning pull-right" runat="server" id="spanTREmpSettled">0</span>
                                                </a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="panel panel-info-alt" runat="server" id="divTADA" visible="false">
                                        <div class="panel-heading">
                                            <div class="panel-btns" style="display: none;">
                                                <a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel">
                                                    <i class="fa fa-minus"></i></a>
                                            </div>
                                            <h3 class="panel-title">TADA</h3>
                                            <a style="display: block" href="../cp/TADAForward.aspx" class="details pull-right viewall">View All</a>
                                        </div>
                                        <div class="panel-body panel-body-small">
                                            <ul class="nav nav-pills nav-stacked dashboardNotificatioin">
                                                <li><a href='../cp/TADAForward.aspx?status=0'>Pending <span runat="server" id="spanTADAPending"
                                                    class="badge badge-success pull-right">0</span> </a></li>
                                                <li><a href='../cp/TADAForward.aspx?status=1'>Recommended<span class="badge badge-warning pull-right"
                                                    runat="server" id="spanTADARecommended">0</span></a> </li>
                                                <li><a href='../cp/TADAForward.aspx?status=2'>Approved <span class="badge badge-danger pull-right"
                                                    runat="server" id="spanTADAApproved">0</span></a> </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="panel panel-info-alt">
                                        <div class="panel-heading">
                                            <div class="panel-btns" style="display: none;">
                                                <a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel">
                                                    <i class="fa fa-minus"></i></a>
                                            </div>
                                            <h3 class="panel-title">Contract/Status Expire</h3>
                                            <a style="display: block" href="EmployeeContractExpiring.aspx" class="details pull-right viewall">View All</a>
                                        </div>
                                        <div class="panel-body panel-body-small">
                                            <ul class="nav nav-pills nav-stacked dashboardNotificatioin">
                                                <li><a href='javascript:void(0)' style="color: Black"><span runat="server" id="contractCurrentMonth"
                                                    class="spanCurrentMonth">This Month</span><span runat="server" id="contractThisMonthCount"
                                                        class="badge badge-danger pull-right">0</span></a> </li>
                                                <asp:Repeater runat="server" ID="rptContractExpiringThisMonth">
                                                    <ItemTemplate>
                                                        <li><span style="width: 150px; padding-left: 10px;">
                                                            <asp:HyperLink CssClass="employeeClass" Width="150px" ID="Label2" runat="server"
                                                                NavigateUrl='<%# "~/newhr/PersonalDetail.aspx?Id=" +  Eval("EmployeeId") %>'
                                                                Text='<%# "&nbsp;" + Eval("Name") %>'></asp:HyperLink>
                                                            <%--(<%# Eval("StatusName")%>) --%>
                                                        </span><span style="text-align: right; width: 100px;">
                                                            <%# Eval("ToDateEng", "{0:dd MMM yyyy}").ToString().Replace("12:00AM", "")%>
                                                        </span><span style="text-align: right; width: 100px; color: Red;">
                                                            <%# Eval("DueDays")%>
                                                        </span></li>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                                <li><a href='javascript:void(0)' style="color: Black"><span runat="server" id="contractNextMonth"
                                                    class="spanCurrentMonth">Next Month</span><span runat="server" id="countContractExpireNextMonth"
                                                        class="badge badge-danger pull-right">0</span></a> </li>
                                                <asp:Repeater runat="server" ID="rptContractExpiringNextMonth">
                                                    <ItemTemplate>
                                                        <li><span style="width: 150px; padding-left: 20px;">
                                                            <asp:HyperLink CssClass="employeeClass" Width="150px" ID="Label2" runat="server"
                                                                NavigateUrl='<%# "~/newhr/PersonalDetail.aspx?Id=" +  Eval("EmployeeId") %>'
                                                                Text='<%# "&nbsp;" + Eval("Name") %>'></asp:HyperLink>
                                                        </span><span style="text-align: right; width: 100px;">
                                                            <%# Eval("ToDateEng", "{0:dd MMM yyyy}").ToString().Replace("12:00AM", "")%>
                                                        </span><span style="text-align: right; width: 100px; color: Red;">
                                                            <%# Eval("DueDays")%>
                                                        </span></li>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                                <li><a href='javascript:void(0)' style="color: Black"><span runat="server" id="contractNextNextMonth"
                                                    class="spanCurrentMonth"></span><span runat="server" id="countContractExpireNextNextMonth"
                                                        class="badge badge-danger pull-right">0</span></a> </li>
                                                <asp:Repeater runat="server" ID="rptContractExpiringNextNextMonth">
                                                    <ItemTemplate>
                                                        <li><span style="width: 150px; padding-left: 20px;">
                                                            <asp:HyperLink CssClass="employeeClass" Width="150px" ID="Label2" runat="server"
                                                                NavigateUrl='<%# "~/newhr/PersonalDetail.aspx?Id=" +  Eval("EmployeeId") %>'
                                                                Text='<%# "&nbsp;" + Eval("Name") %>'></asp:HyperLink>
                                                        </span><span style="text-align: right; width: 100px;">
                                                            <%# Eval("ToDateEng", "{0:dd MMM yyyy}").ToString().Replace("12:00AM", "")%>
                                                        </span><span style="text-align: right; width: 100px; color: Red;">
                                                            <%# Eval("DueDays")%>
                                                        </span></li>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="panel panel-info-alt" runat="server" id="divBirthday">
                                        <div class="panel-heading">
                                            <div class="panel-btns" style="display: none;">
                                                <a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel">
                                                    <i class="fa fa-minus"></i></a>
                                            </div>
                                            <h3 class="panel-title">Upcoming Birthdays</h3>
                                            <a style="display: block" href="../CP/Report/NewHrReport/ReportBirthdays.aspx" class="details pull-right viewall">View All</a>
                                        </div>
                                        <div class="panel-body panel-body-small">
                                            <ul class="nav nav-pills nav-stacked dashboardNotificatioin">
                                                <li><a href='javascript:void(0)' style="color: Black"><span runat="server" id="spnBrithDayThisMonthName"
                                                    class="spanCurrentMonth">This Month</span><span runat="server" id="spnBrithDayThisMonthCount"
                                                        class="badge badge-danger pull-right">0</span></a> </li>
                                                <asp:Repeater runat="server" ID="RptBirthDayThisMonth">
                                                    <ItemTemplate>
                                                        <li><span style="width: 150px; padding-left: 10px;">
                                                            <asp:HyperLink CssClass="employeeClass" Width="150px" ID="Label2" runat="server"
                                                                NavigateUrl='<%# "~/newhr/PersonalDetail.aspx?Id=" +  Eval("EmployeeId") %>'
                                                                Text='<%# "&nbsp;" + Eval("Name") %>'></asp:HyperLink>
                                                            <%--(<%# Eval("StatusName")%>) --%>
                                                        </span><span style="text-align: right; width: 100px;">
                                                            <%# Eval("Month")%>
                                                        </span></li>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                                <li><a href='javascript:void(0)' style="color: Black"><span runat="server" id="spnBrithDayNextMonthName"
                                                    class="spanCurrentMonth">Next Month</span><span runat="server" id="spnBrithDayNextMonthCount"
                                                        class="badge badge-danger pull-right">0</span></a> </li>
                                                <asp:Repeater runat="server" ID="RptBrithDayNextMonth">
                                                    <ItemTemplate>
                                                        <li><span style="width: 150px; padding-left: 20px;">
                                                            <asp:HyperLink CssClass="employeeClass" Width="150px" ID="Label2" runat="server"
                                                                NavigateUrl='<%# "~/newhr/PersonalDetail.aspx?Id=" +  Eval("EmployeeId") %>'
                                                                Text='<%# "&nbsp;" + Eval("Name") %>'></asp:HyperLink>
                                                        </span><span style="text-align: right; width: 100px;">
                                                            <%# Eval("Month")%>
                                                        </span></li>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                                <li><a href='javascript:void(0)' style="color: Black"><span runat="server" id="spnBrithDayNextNextMonthName"
                                                    class="spanCurrentMonth"></span><span runat="server" id="spnBrithDayNextNextMonthCount"
                                                        class="badge badge-danger pull-right">0</span></a> </li>
                                                <asp:Repeater runat="server" ID="RptBirthDayNextNetMonth">
                                                    <ItemTemplate>
                                                        <li><span style="width: 150px; padding-left: 20px;">
                                                            <asp:HyperLink CssClass="employeeClass" Width="150px" ID="Label2" runat="server"
                                                                NavigateUrl='<%# "~/newhr/PersonalDetail.aspx?Id=" +  Eval("EmployeeId") %>'
                                                                Text='<%# "&nbsp;" + Eval("Name") %>'></asp:HyperLink>
                                                        </span><span style="text-align: right; width: 100px;">

                                                            <%# Eval("Month")%>
                                                        </span></li>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="panel panel-info-alt" runat="server" id="divRetiring">
                                        <div class="panel-heading">
                                            <div class="panel-btns" style="display: none;">
                                                <a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel">
                                                    <i class="fa fa-minus"></i></a>
                                            </div>
                                            <h3 class="panel-title">Retiring Employee</h3>
                                            <a style="display: block" href="EmployeeRetiring.aspx" class="details pull-right viewall">View All</a>
                                        </div>
                                        <div class="panel-body panel-body-small">
                                            <ul class="nav nav-pills nav-stacked dashboardNotificatioin">
                                                <li><a href='javascript:void(0)' style="color: Black">This Month <span runat="server"
                                                    id="countRetiringThisMonth" class="badge badge-success pull-right">0</span> </a>
                                                </li>
                                                <li><a href='javascript:void(0)' style="color: Black">Next Month <span runat="server"
                                                    id="countRetiringNextMonth" class="badge badge-success pull-right">0</span> </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="panel panel-info-alt" runat="server" id="divNewJoin">
                                        <div class="panel-heading">
                                            <div class="panel-btns" style="display: none;">
                                                <a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel">
                                                    <i class="fa fa-minus"></i></a>
                                            </div>
                                            <h3 class="panel-title">New Joinees</h3>
                                            <a style="display: block" href="EmployeeNewJoinees.aspx" class="details pull-right viewall">View All</a>
                                        </div>
                                        <div class="panel-body panel-body-small">
                                            <ul class="nav nav-pills nav-stacked dashboardNotificatioin">
                                                <li><a href='javascript:void(0)' style="color: Black">This Month <span runat="server"
                                                    id="countNewJoineesThisMonth" class="badge badge-success pull-right">0</span> </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <%-- <div class="panel panel-info-alt" runat="server" id="divSixMonth">
                                        <div class="panel-heading">
                                            <div class="panel-btns" style="display: none;">
                                                <a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel">
                                                    <i class="fa fa-minus"></i></a>
                                            </div>
                                            <h3 class="panel-title">
                                                6 Month Completion</h3>
                                            <a style="display: block" href="EmployeePeriodSixMonthComplete.aspx" class="details pull-right viewall">
                                                View All</a>
                                        </div>
                                        <div class="panel-body panel-body-small">
                                            <ul class="nav nav-pills nav-stacked dashboardNotificatioin">
                                                <li><a href='javascript:void(0)' style="color: Black">This Month <span runat="server"
                                                    id="count6MonthThisMonth" class="badge badge-success pull-right">0</span> </a>
                                                </li>
                                                <li><a href='javascript:void(0)' style="color: Black">Next Month <span runat="server"
                                                    id="count6MonthNextMonth" class="badge badge-success pull-right">0</span> </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="panel panel-info-alt" runat="server" id="divYearMonth">
                                        <div class="panel-heading">
                                            <div class="panel-btns" style="display: none;">
                                                <a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel">
                                                    <i class="fa fa-minus"></i></a>
                                            </div>
                                            <h3 class="panel-title">
                                                1 Year Completion</h3>
                                            <a style="display: block" href="EmployeePeriodYearComplete.aspx" class="details pull-right viewall">
                                                View All</a>
                                        </div>
                                        <div class="panel-body panel-body-small">
                                            <ul class="nav nav-pills nav-stacked dashboardNotificatioin">
                                                <li><a href='javascript:void(0)' style="color: Black">This Month <span runat="server"
                                                    id="count1YearThisMonth" class="badge badge-success pull-right">0</span> </a>
                                                </li>
                                                <li><a href='javascript:void(0)' style="color: Black">Next Month <span runat="server"
                                                    id="count1YearNextMonth" class="badge badge-success pull-right">0</span> </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>--%>
                                    <div class="panel panel-info-alt">
                                        <div class="panel-heading">
                                            <div class="panel-btns" style="display: none;">
                                                <a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel">
                                                    <i class="fa fa-minus"></i></a>
                                            </div>
                                            <h3 class="panel-title">Profile and Photo Changes</h3>
                                            <%-- <a style="display: block" href="EmployeePeriodYearComplete.aspx" class="details pull-right viewall">
                                                view all</a>--%>
                                        </div>
                                        <div class="panel-body panel-body-small">
                                            <ul class="nav nav-pills nav-stacked dashboardNotificatioin">
                                                <li><a href='EmployeeProfileChange.aspx' style="color: Black">Employee Profile Changes
                                                    <span runat="server" id="spanProfileChanges" class="badge badge-success pull-right">0</span> </a></li>
                                                <li><a href='EmpIdentificationApproval.aspx' style="color: Black">Employee Photo Changes
                                                    <span runat="server" id="spanPhotoChanges" class="badge badge-success pull-right">0</span>
                                                </a></li>
                                            </ul>
                                        </div>
                                    </div>

                                    <div class="panel panel-info-alt" runat="server" id="divHPLSchedule" visible="false">
                                        <div class="panel-heading">
                                            <div class="panel-btns" style="display: none;">
                                                <a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel">
                                                    <i class="fa fa-minus"></i></a>
                                            </div>
                                            <h3 class="panel-title">Duty Schedule</h3>
                                            <a style="display: block" href="EmpDutySchedule.aspx" class="details pull-right viewall">View All</a>

                                        </div>
                                        <div class="panel-body panel-body-small">
                                            <ul class="nav nav-pills nav-stacked dashboardNotificatioin">
                                                <li>
                                                    <ext:GridPanel StyleSpec="margin-top:5px;" ID="gridSchedule" runat="server" Cls="itemgrid" Width="344">
                                                        <Store>
                                                            <ext:Store ID="storeSchedule" runat="server">
                                                                <Model>
                                                                    <ext:Model ID="Model6" runat="server" IDProperty="ScheduleId">
                                                                        <Fields>
                                                                            <ext:ModelField Name="ScheduleId" Type="String" />
                                                                            <ext:ModelField Name="EmployeeId" Type="String" />
                                                                            <ext:ModelField Name="Name" Type="String" />

                                                                        </Fields>
                                                                    </ext:Model>
                                                                </Model>
                                                            </ext:Store>
                                                        </Store>
                                                        <ColumnModel>
                                                            <Columns>
                                                                <ext:Column ID="Column2" Sortable="false" MenuDisabled="true" runat="server" Text="EIN"
                                                                    Width="40" Align="Center" DataIndex="EmployeeId">
                                                                </ext:Column>
                                                                <ext:Column ID="colEmployeeName" Sortable="false" MenuDisabled="true" runat="server"
                                                                    Text="Name" Width="139" Align="Left" DataIndex="Name">
                                                                </ext:Column>
                                                            </Columns>
                                                        </ColumnModel>
                                                        <SelectionModel>
                                                            <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                                                        </SelectionModel>
                                                        <Plugins>
                                                            <ext:CellEditing ID="CellEditing1" ClicksToEdit="1" runat="server">
                                                                <%--<Listeners>                              
                                                                    <Edit Fn="afterEdit" />
                                                                </Listeners>--%>
                                                            </ext:CellEditing>
                                                        </Plugins>

                                                    </ext:GridPanel>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>

                                </td>
                                <%-- <td class="Column">
                                <div class="dbPanel" runat="server" id="Div2">
                                    <div class="dbTitle">
                                        <h2>
                                            Employee Count</h2>
                                    </div>
                                    <div class="dbPanelContent">
                                        <ext:Panel ID="Panel1" runat="server" Title="Branch Count" Width="380" Height="370"
                                            Layout="FitLayout">
                                            <Items>
                                                <ext:Chart ID="chartBranchEmployeeCount" runat="server" Shadow="true" Animate="true">
                                                    <Store>
                                                        <ext:Store ID="chartBranchEmployeeCountStore" runat="server" AutoDataBind="true">
                                                            <Model>
                                                                <ext:Model ID="Model1" runat="server">
                                                                    <Fields>
                                                                        <ext:ModelField Name="Text" />
                                                                        <ext:ModelField Name="Count" />
                                                                    </Fields>
                                                                </ext:Model>
                                                            </Model>
                                                        </ext:Store>
                                                    </Store>
                                                    <Background>
                                                        <Gradient GradientID="backgroundGradient" Angle="45">
                                                            <Stops>
                                                                <ext:GradientStop Offset="0" Color="#ffffff" />
                                                                <ext:GradientStop Offset="100" Color="#eaf1f8" />
                                                            </Stops>
                                                        </Gradient>
                                                    </Background>
                                                    <Axes>
                                                        <ext:NumericAxis Grid="false" Fields="Count" Position="Bottom" Minimum="0">
                                                            <Label>
                                                                <Renderer Handler="return Ext.util.Format.number(value, '0,0');" />
                                                            </Label>
                                                        </ext:NumericAxis>
                                                        <ext:CategoryAxis Fields="Text" Position="Left" />
                                                    </Axes>
                                                    <Series>
                                                        <ext:BarSeries Axis="Bottom" Highlight="true" XField="Text" YField="Count">
                                                            <Tips TrackMouse="true" Width="140" Height="28">
                                                                <Renderer Handler="this.setTitle(storeItem.get('Text') + ': ' + storeItem.get('Count') + ' views');" />
                                                            </Tips>
                                                            <Label Display="InsideEnd" Field="Count" Orientation="Horizontal" Color="#333" TextAnchor="middle" />
                                                        </ext:BarSeries>
                                                    </Series>
                                                </ext:Chart>
                                            </Items>
                                        </ext:Panel>
                                    </div>
                                </div>
                            </td>--%>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <ext:Window ID="WPasswordNotification" runat="server" Title="Password Change Notification"
        Icon="None" Height="150" Width="300" BodyPadding="5" Hidden="true" Modal="true">
        <Content>
            <div style="background-color: White; height: 150px; width: 300px;">
                <br />
                <p style="margin-left: 20px; padding-bottom: 5px;">
                    <b>Your password has expired.</b>
                </p>
                <b><a style="margin-left: 20px; text-decoration: underline;" href="../CP/ChangePassword.aspx">Click here</a> to change your password.</b>
            </div>
        </Content>
    </ext:Window>
    <script type="text/javascript">

        function ShowMsg() {
            Ext.net.Notification.show({
                iconCls: 'icon-information',
                pinEvent: 'click',
                html: 'Your Password has been changed.',
                title: 'Title'
            });
        }

    </script>
</asp:Content>
