﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using BLL;
using DAL;
using Utils.Web;
using Utils.Helper;
using System.Text;
using Utils.Security;
using System.Data;
using System.IO;
using Utils;

namespace Web.CP
{



    public partial class Execute : BasePage
    {
        CompanyManager compMgr = new CompanyManager();

        private bool CheckPermission()
        {

            if (SessionManager.User.RoleId != (int)Role.Administrator)
                return false;

            //string p1 = pwd.Text.Trim();  //Request.QueryString["P1"];

            //if(!string.IsNullOrEmpty(p1) && p1 == SecurityHelper.ExecutePagePwd)
            //{
            //    return true;
            //}
            //else
            //{
            //    //if not accessible then redirect to Default page
            //    Response.Redirect("~/Default.aspx");
            //}
            return false;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            //CheckPermission();
            // CheckPermission();
            if (!IsPostBack)
            {

            }
        }

        public string GetKeyDate()
        {
            if (string.IsNullOrEmpty(pwd.Text.Trim()))
            {
                return "Key is required.";
            }

            try
            {
                EncryptorDecryptor enc = new EncryptorDecryptor();

                string encrypted = pwd.Text.Trim();


                string value = enc.Decrypt(encrypted);
                string[] strs = value.Split(new char[] { ':' });
                if (strs.Length < 3)
                    return "Invalid key";

                DateTime date;

                string dateValue = strs[2] + ":" + strs[3] + ":" + strs[4];

                if (DateTime.TryParse(dateValue, out date) == false)
                    return "Invalid key";

                if (date < DateTime.Now)
                {
                    return "Key already expired.";
                }


                return "";

            }
            catch
            {
                return "Invalid key";
            }
        }

        public void btn1_Click(object sender, EventArgs e)
        {
            string keyMsg = GetKeyDate();

            if (!string.IsNullOrEmpty(keyMsg))
            {
                lblStatus.Text = keyMsg;
                return;
            }

            //if (CheckPermission())
            {
                string logMessage = "";

                try
                {
                    DateTime now = DateTime.Now;
                    string msg = "";

                    Guid key = Guid.NewGuid();

                    logMessage =
                        string.Format("{0} by {1} {2}",
                        Environment.NewLine + Environment.NewLine + now.ToString("yyyy-mm-dd hh:mm:ss") + " || " + key,
                                    SessionManager.UserName,
                                    Environment.NewLine + txt1.Text.Trim());
                    Log.SaveAdditionalLog(logMessage, LogType.LogRun);

                    //string fileName =  string.Format("backup-a-{0}-{1}-{2}({3}.{4}-{5}).bak", now.Day,now.Month,now.Year,now.Hour,now.Minute,key );

                    //try
                    //{
                    //    CommonManager.TakeBacup(fileName);
                    //}
                    //catch
                    //{
                    //    //TODO: done for temporary purpose remember to set as msg = CommonManager.TakeBacup(fileName);, then only proceed forward
                    //}

                    if (msg == "")
                    {

                        DataSet data = CommonManager.ExecuteSql(txt1.Text.Trim());

                        if (data.Tables.Count > 0)
                        {
                            gvw.AllowPaging = false;
                            gvw.DataSource = data.Tables[0];
                            gvw.DataBind();

                            GridViewExportUtil.Export("data.xls", gvw);


                            //Response.ClearContent();
                            //Response.AddHeader("content-disposition", "attachment; filename=data.xls");
                            //Response.ContentType = "application/excel";
                            //System.IO.StringWriter sw = new System.IO.StringWriter();
                            //HtmlTextWriter htw = new HtmlTextWriter(sw);
                            //gvw.RenderControl(htw);
                            //Response.Write(sw.ToString());
                            //Response.End();
                        }
                        else
                            lblStatus.Text = "Success";
                    }
                    else
                    {
                        lblStatus.Text = "Could not take backup, so execution stopped, <br/>" + msg;
                    }
                }
                catch (Exception exp)
                {


                    lblStatus.Text = exp.ToString();
                }


                DateTime now1 = DateTime.Now;
                logMessage =
                       Environment.NewLine + "Respose : " + Environment.NewLine + lblStatus.Text;
                Log.SaveAdditionalLog(logMessage, LogType.LogRun);
            }
            //pwd.Text = "";
        }



    }

}

