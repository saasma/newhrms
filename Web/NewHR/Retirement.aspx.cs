﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using Utils.Helper;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Calendar;
using System.IO;
using Utils;
using System.Text;
using BLL.BO;
using Ext.Net;

namespace Web.NewHR
{
    public partial class Retirement : BasePage
    {
        BranchManager branchMgr = new BranchManager();
        EmployeeManager empMgr = new EmployeeManager();
        InsuranceManager insMgr = new InsuranceManager();
        CommonManager cmpMgr = new CommonManager();
        List<BranchDepartmentHistory> list = new List<BranchDepartmentHistory>();
        List<PIncome> variableIncomeList = new List<PIncome>();
        List<PIncome> variableDeductionList = new List<PIncome>();

        bool IsPastRetired = false;
        int RetiredPayrollPeriodId = 0;


        List<KeyValue> incomeDeductionForAdjustment = new List<KeyValue>();
       
        protected void Page_Load(object sender, EventArgs e)
        {
            PayrollPeriod payrollPeriod = CommonManager.GetLastPayrollPeriod();
            incomeDeductionForAdjustment = PayManager.GetEmployeesIncomeDeductionList(payrollPeriod.PayrollPeriodId);

            variableIncomeList = PayManager.GetVariableIncomeList(SessionManager.CurrentCompanyId, false);
            variableDeductionList = PayManager.GetVariableIncomeList(SessionManager.CurrentCompanyId, true);

            if (!IsPostBack && !X.IsAjaxRequest)
            {
                PayrollPeriod period = CommonManager.GetLastPayrollPeriod();
                if (period != null)
                {
                    header.InnerHtml += period.Name + " (" + period.StartDateEng.Value.ToString("dd MMM yyyy") + " - " +
                        period.EndDateEng.Value.ToString("dd MMM yyyy") + ")";
                }


                Initialise();
            }

            string eventTarget = Request.Form["__EVENTTARGET"];
         
            if (eventTarget != null && eventTarget.Equals("Reload"))
            {
                LoadEmployeeDetails(null, null);
            }
        }

        void LoadRetiringThisMonth()
        {

            PayrollPeriod period = CommonManager.GetLastPayrollPeriod();

            if (period == null)
                return;

            List<TextValue> list = DashboardManager.GetEmployeeRetiring(period.StartDateEng.Value, period.EndDateEng.Value);

            foreach (TextValue item in list)
            {
                if (item.ID.ToString() == hiddenEmpID.Value.Trim())
                {
                    item.Other = "class='selected'";
                }
            }

            rptTiisMonth.DataSource = list;
            rptTiisMonth.DataBind();
        }


        void Initialise()
        {

            List<HR_Service_Event> list = CommonManager.GetServieEventTypes();

            ddlRetirementType.DataSource = list;
            ddlRetirementType.DataBind();
            if (list.Count <= 0)
                reqdRetirementTypes.Enabled = false;


            LoadBranches();
            LoadRetiringThisMonth();

            LoadEmpFromQueryString();
        }

        public void LoadEmpFromQueryString()
        {
            if (Request.QueryString["ID"] != null)
            {
                int empid = 0;

                if (int.TryParse(Request.QueryString["ID"], out empid))
                {
                    bodyDetails.Visible = true;
                    txtEmpSearchText.Visible = false;
                    hiddenEmpID.Value = Request.QueryString["ID"];
                    
                    IsPastRetired = true;
                    RetiredPayrollPeriodId = int.Parse(Request.QueryString["pid"]);

                    LoadEmployeeDetails(null, null);

                    divSearch.Visible = false;
                    header.Visible = false;
                    btnReload.Visible = false;
                   
                    //PayrollPeriod period = CommonManager.GetPayrollPeriod(RetiredPayrollPeriodId);
                    //if (period != null)
                    //    LoadCalculation(period, empid);
                }
            }
        }

        void LoadBranches()
        {
           // ddlFilterByBranch.DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
           // ddlFilterByBranch.DataBind();

         

        
            //if( ddlFilterByBranch.Items.Count >0)
            //{
            //    ddlFilterByBranch.Items[1].Selected = true;
            //    LoadEmployeesByBranch();
            //}
            LoadEmployeesByBranch();
        }

        protected void LoadEmployees(object sender, EventArgs e)
        {
            LoadEmployeesByBranch();
        }

        void LoadEmployeesByBranch()
        {
            //if (ddlFilterByBranch.SelectedValue.Equals("-1") == false)
            //{
            //    ListItem first = ddlEmployeeList.Items[0];
            //    ddlEmployeeList.DataSource = empMgr.GetEmployeesByBranch(int.Parse(ddlFilterByBranch.SelectedValue), -1);
            //    ddlEmployeeList.DataBind();
            //    ddlEmployeeList.Items.Insert(0, first);
            //}
            body.Visible = false;
            bodyDetails.Visible = false;
        }

       


        protected void ddlFilterByValues_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadEmployees();
        }
        void LoadEmployees()
        {
            string type = string.Empty; int id = 0;

        }


        public void LoadEmployeeDetailsWithAtte(object sender, EventArgs e)
        {
            int employeeId = int.Parse(hiddenEmpID.Value);

            PayrollPeriod period = CommonManager.GetLastPayrollPeriod();
            EmployeeManager.ResaveAttendanceOfEmployee(employeeId, period);

            LoadEmployeeDetails(sender, e);
        }


        public void LoadEmployeeDetails(object sender, EventArgs e)
        {
            bodyDetails.Visible = false;

            LoadRetiringThisMonth();

            int employeeId = 0;


            if (int.TryParse(hiddenEmpID.Value, out employeeId) == false)
            {
                divWarningMsg.InnerHtml = "Please select an employee.";
                divWarningMsg.Hide = false;
                return;
            }
            
            employeeId = int.Parse(hiddenEmpID.Value);


            PayrollPeriod payrollPeriod = CommonManager.GetLastPayrollPeriod();

            bool isRetirementDateSet = false;

            btnSaveCalculation.OnClientClick = string.Format("openCalculationPopup({0});return false;", employeeId);
            EEmployee emp = EmployeeManager.GetEmployeeById(employeeId);

            txtEmpSearchText.Text = emp.Name;

            if (emp == null)
            {
                buttonsBar.Visible = false;
                divWarningMsg.InnerHtml = "Please select an employee.";
                divWarningMsg.Hide = false;
                return;
            }
            else
                buttonsBar.Visible = true;           

            body.Visible = true;

            EHumanResource hr = emp.EHumanResources[0];
            //rdbIsRetired.Checked = true;
            // If retirement date already set then load the calculation
            if (emp.IsResigned == true || emp.IsRetired == true)
            {
                txtRetirementNotes.Text = hr.RetirementNotes;

                if (hr.DateOfResignationEng != null)
                {
                    isRetirementDateSet = true;
                    //rdbIsResigned.Checked = true;
                    //rdbIsRetired.Checked = false;
                    calRetirementDate.Text = hr.DateOfResignation;
                }
                else
                {
                    isRetirementDateSet = true;
                    //rdbIsResigned.Checked = false;
                    //rdbIsRetired.Checked = true;
                    calRetirementDate.Text = hr.DateOfRetirement;
                    if (hr.RetirementType != null)
                        UIHelper.SetSelectedInDropDown(ddlRetirementType, hr.RetirementType.ToString());
                }

                // If page viewed from past retirement calculation
                if (IsPastRetired)
                {
                    payrollPeriod = CommonManager.GetPayrollPeriod(RetiredPayrollPeriodId);
                }
                // for past retired get retired payroll period
                else if (emp.IsRetired != null && emp.IsRetired.Value &&
                    hr.DateOfRetirementEng != null && hr.DateOfRetirementEng < payrollPeriod.StartDateEng)
                {
                    payrollPeriod = EmployeeManager.GetPastRetiredPayrollPeriod(emp.EmployeeId);
                }


                if (payrollPeriod != null)
                {
                    bodyDetails.Visible = true;
                    LoadLeaves(payrollPeriod, employeeId);
                    LoadCalculation(payrollPeriod, employeeId);
                }
            }
            else if(!string.IsNullOrEmpty(Request.QueryString["id"]))
            {
                // could be retire rehire case
                txtRetirementNotes.Text = hr.RetirementNotes;

                RetireReHireEmployee ret = BLL.BaseBiz.PayrollDataContext.RetireReHireEmployees
                    .Where(x => x.EmployeeId == employeeId).OrderByDescending(x => x.PayrollPeriodId).FirstOrDefault();

                if (ret == null)
                    return;

                txtRetirementNotes.Text = ret.Notes;
                isRetirementDateSet = true;
                calRetirementDate.Text = ret.RetirementDate;
                //if (ret.RetirementType != null)
                    UIHelper.SetSelectedInDropDown(ddlRetirementType, ret.RetirementType.ToString());
                payrollPeriod = CommonManager.GetPayrollPeriod(ret.PayrollPeriodId);


                divGratuity.Visible=true;
                lblGratuity.Text = string.Format("Service year : {0}, Amount : {1}",
                    ret.GratuityServiceYear, GetCurrency(ret.GratuityAmount));


                bodyDetails.Visible = true;
                LoadRetireRehireLeaves(employeeId);
                LoadRetireRehireCalculation(employeeId);

                btnDeleteRetirement.Visible = false;
                btnSetRetirement.Visible = false;
                btnSave.Visible = false;
                btnSaveCalculation.Visible = false;
                btnResetAllAdjustment.Visible = false;

                return;
            }
            else 
            {
                
                calRetirementDate.Text = "";
            }

            DisplayEmployeeHRDetails(emp);

            
            // If salary saved then only show amount details
            if (payrollPeriod != null &&
                CalculationManager.IsCalculationSavedForEmployee(payrollPeriod.PayrollPeriodId, employeeId))
            {
                divMsgCtl.InnerHtml = "Salary already saved for the employee, please delete the salary for any changes.";
                divMsgCtl.Hide = false;

                btnSetRetirement.Visible = false;
                btnSave.Visible = false;
                btnSaveCalculation.Visible = false;
                btnResetAllAdjustment.Visible = false;


                btnDeleteRetirement.Visible = false;
            }
            else
            {
                btnSetRetirement.Visible = true;
                btnSave.Visible = true;
                btnSaveCalculation.Visible = true;
                btnResetAllAdjustment.Visible = true;

                btnSetRetirement.Visible = false;
                btnDeleteRetirement.Visible = true;
                
            }


            if (isRetirementDateSet)
                calRetirementDate.Disable(true);
            else
            {
                calRetirementDate.Enable(true);
                btnSetRetirement.Visible = true;
                btnDeleteRetirement.Visible = false;
            }


            // check for already past retired emp
            if (emp.IsRetired != null && emp.IsRetired.Value &&
                hr.DateOfRetirementEng != null && 
                (payrollPeriod  == null || hr.DateOfRetirementEng < payrollPeriod.StartDateEng)
                )
            {
                divWarningMsg.InnerHtml = "Retired : " + hr.DateOfRetirement + ".";
                divWarningMsg.Hide = false;

                btnSetRetirement.Visible = false;
                btnSave.Visible = false;
                btnSaveCalculation.Visible = false;
                btnResetAllAdjustment.Visible = false;


                btnDeleteRetirement.Visible = false;
            }

        }

        private void DisplayEmployeeHRDetails(EEmployee emp)
        {
            // Photo
            bool hasPhoto = false;
            if (emp.HHumanResources.Count > 0)
            {
                if (!string.IsNullOrEmpty(emp.HHumanResources[0].UrlPhotoThumbnail))
                {
                    image.ImageUrl = Page.ResolveUrl(Config.UploadLocation + "/" + emp.HHumanResources[0].UrlPhotoThumbnail);
                    if (File.Exists(Server.MapPath(image.ImageUrl)))
                        hasPhoto = true;
                }
            }
            if (!hasPhoto)
            {
                image.ImageUrl = Page.ResolveUrl(Web.Helper.WebHelper.GetEmployeeNoImage(emp.EmployeeId));
            }

            lblName.Text = emp.EmployeeId + " - " +  emp.Title + " " + emp.Name;
            lblBranch.Text = emp.Branch.Name;
            lblDepartment.Text = emp.Department.Name;
            lblPosition.Text = emp.EDesignation.Name;

            int years, months, days, hours;
            int minutes, seconds, milliseconds;
            ServicePeroid firstStatus = EmployeeManager.GetServicePeroidDate(emp.EmployeeId);
            GetElapsedTime(firstStatus.FromDateEng, BLL.BaseBiz.GetCurrentDateAndTime(), out years, out months, out days, out hours, out minutes, out seconds, out milliseconds);

            lblSince.Text = "Since " +
                (IsEnglish ? firstStatus.FromDateEng.ToShortDateString() : firstStatus.FromDate + " (" + firstStatus.FromDateEng.ToShortDateString() + ")");
            lblTime.Text = string.Format("For {0} years {1} months {2} days",
                years, months, days);
        }

        // Return the number of years, months, days, hours, minutes, seconds,
        // and milliseconds you need to add to from_date to get to_date.
        private void GetElapsedTime(DateTime from_date, DateTime to_date,
            out int years, out int months, out int days, out int hours,
            out int minutes, out int seconds, out int milliseconds)
        {
            // If from_date > to_date, switch them around.
            if (from_date > to_date)
            {
                GetElapsedTime(to_date, from_date,
                    out years, out months, out days, out hours,
                    out minutes, out seconds, out milliseconds);
                years = -years;
                months = -months;
                days = -days;
                hours = -hours;
                minutes = -minutes;
                seconds = -seconds;
                milliseconds = -milliseconds;
            }
            else
            {
                // Handle the years.
                years = to_date.Year - from_date.Year;

                // See if we went too far.
                DateTime test_date = from_date.AddMonths(12 * years);
                if (test_date > to_date)
                {
                    years--;
                    test_date = from_date.AddMonths(12 * years);
                }

                // Add months until we go too far.
                months = 0;
                while (test_date <= to_date)
                {
                    months++;
                    test_date = from_date.AddMonths(12 * years + months);
                }
                months--;

                // Subtract to see how many more days,
                // hours, minutes, etc. we need.
                from_date = from_date.AddMonths(12 * years + months);
                TimeSpan remainder = to_date - from_date;
                days = remainder.Days;
                hours = remainder.Hours;
                minutes = remainder.Minutes;
                seconds = remainder.Seconds;
                milliseconds = remainder.Milliseconds;
            }
        }

        public int GetEmployeeId()
        {
            return int.Parse(hiddenEmpID.Value);
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            int employeeId = GetEmployeeId();
            PayrollPeriod payrollPeriod = CommonManager.GetLastPayrollPeriod();
            if (CalculationManager.IsCalculationSavedForEmployee(payrollPeriod.PayrollPeriodId, employeeId))
            {
                divWarningMsg.InnerHtml = "Salary already saved, can not change the adjustment, please delete the salary first.";
                divWarningMsg.Hide = false;
                return;
            }

            List<PIncome> companyIncomeList = PayManager.GetIncomeList();
            List<PDeduction> companyDeductionList = PayManager.GetDeductionListByCompany(SessionManager.CurrentCompanyId);

            if (Page.IsValid)
            {
               
                List<CalculationValue> incomeList = new List<CalculationValue>();
                List<CalculationValue> otherIncomeList = new List<CalculationValue>();

                List<CalculationValue> otherDeductionList = new List<CalculationValue>();

                StringBuilder str = new StringBuilder("<root>");

                foreach (GridViewRow row in gvw.Rows)
                {
                    int type = int.Parse(gvw.DataKeys[row.RowIndex][0].ToString());
                    int sourceId = int.Parse(gvw.DataKeys[row.RowIndex][1].ToString());
                    decimal existingAmount = decimal.Parse(gvw.DataKeys[row.RowIndex]["AmountWithOutAdjustment"].ToString());

                    TextBox txtAmount = row.FindControl("txtAmount") as TextBox;
                    if (txtAmount.Enabled == false)
                        continue;


                    decimal newAmount = 0;

                    if (decimal.TryParse(txtAmount.Text.Trim(), out newAmount) == false)
                    {
                        txtAmount.Focus();
                        divWarningMsg.InnerHtml = "Invalid amount, please check the amount.";
                        divWarningMsg.Hide = false;
                        return;
                    }


                    if (existingAmount != newAmount)
                    {
                        decimal adjustmentAmount = newAmount - existingAmount;

                        if (adjustmentAmount < -1 || adjustmentAmount > 1)
                        {

                            // If variable income then make direct change to Employee Income
                            if (type == 1)
                            {
                                PIncome income = companyIncomeList.FirstOrDefault(x => x.IncomeId == sourceId);
                                if (income != null && income.Calculation == IncomeCalculation.VARIABLE_AMOUNT)
                                {
                                    PayManager.UpdateEmployeeIncome(sourceId, employeeId, newAmount);
                                    continue;
                                }
                            }

                            str.Append(
                                string.Format("<row PayrollPeriodId=\"{0}\" EmployeeId=\"{1}\" IncomeDeductionId=\"{2}\" Type=\"{3}\"  ExistingAmount=\"{4}\" AdjustmentAmount=\"{5}\"   Note=\"{6}\" PaidAmount=\"{7}\" Exp=\"{8}\" /> ",
                                //overtimeId, payrollPeriodId, employeeId, overtimeHours, payPerHour, txtNote.Text.Trim()
                                    payrollPeriod.PayrollPeriodId, employeeId, sourceId, type, existingAmount, adjustmentAmount,
                                   "Retirement adjustment", 0, ""
                                ));
                        }
                    }
                }

                foreach (GridViewRow row in gvwDeductionList.Rows)
                {
                    int type = int.Parse(gvwDeductionList.DataKeys[row.RowIndex][0].ToString());
                    int sourceId = int.Parse(gvwDeductionList.DataKeys[row.RowIndex][1].ToString());
                    decimal existingAmount = decimal.Parse(gvwDeductionList.DataKeys[row.RowIndex]["AmountWithOutAdjustment"].ToString());

                    TextBox txtAmount = row.FindControl("txtAmount1") as TextBox;

                    if (sourceId + ":" + type == ((int)CalculationColumnType.DeductionCIT + ":" + (int)CalculationColumnType.DeductionCIT))
                    {
                        DropDownList ddlHasCIT = row.FindControl("ddlHasCIT") as DropDownList;
                        if (ddlHasCIT != null)
                        {
                            bool hasCIT = bool.Parse(ddlHasCIT.SelectedValue);
                            PayManager.ChangeNoCITFromRetirement(employeeId, !hasCIT, payrollPeriod);
                        }
                    }

                    if (txtAmount.Enabled == false)
                        continue;

                    decimal newAmount = 0;

                    if (decimal.TryParse(txtAmount.Text.Trim(), out newAmount) == false)
                    {
                        txtAmount.Focus();
                        divWarningMsg.InnerHtml = "Invalid amount, please check the amount.";
                        divWarningMsg.Hide = false;
                        return;
                    }


                    if (existingAmount != newAmount)
                    {
                        decimal adjustmentAmount = newAmount - existingAmount;

                        if (adjustmentAmount < -1 || adjustmentAmount > 1)
                        {

                            // If variable income then make direct change to Employee Income
                            if (type == 8)
                            {
                                PDeduction income = companyDeductionList.FirstOrDefault(x => x.DeductionId == sourceId);
                                if (income != null && income.Calculation == DeductionCalculation.VARIABLE)
                                {
                                    PayManager.UpdateEmployeeDeduction(sourceId, employeeId, newAmount);
                                    continue;
                                }
                            }

                            str.Append(
                                string.Format("<row PayrollPeriodId=\"{0}\" EmployeeId=\"{1}\" IncomeDeductionId=\"{2}\" Type=\"{3}\"  ExistingAmount=\"{4}\" AdjustmentAmount=\"{5}\"   Note=\"{6}\" PaidAmount=\"{7}\" Exp=\"{8}\" /> ",
                                //overtimeId, payrollPeriodId, employeeId, overtimeHours, payPerHour, txtNote.Text.Trim()
                                    payrollPeriod.PayrollPeriodId, employeeId, sourceId, type, existingAmount, adjustmentAmount,
                                   "Retirement adjustment", 0, ""
                                ));
                        }
                    }

                   

                }

                str.Append("</root>");

                // Case 3 : Other income adjustment
                foreach (GridViewRow row in gvwOtherIncomes.Rows)
                {
                    int type = int.Parse(gvwOtherIncomes.DataKeys[row.RowIndex][0].ToString());
                    int sourceId = int.Parse(gvwOtherIncomes.DataKeys[row.RowIndex][1].ToString());
                    decimal existingAmount = decimal.Parse(gvwOtherIncomes.DataKeys[row.RowIndex]["FullAmount"].ToString());

                    

                    TextBox txtAmount = row.FindControl("txtAmount") as TextBox;
                    if (txtAmount.Enabled == false)
                        continue;

                    TextBox txtTDSAdjustment = row.FindControl("txtTDSAdjustment") as TextBox;
                    if (txtAmount.Enabled == false)
                        continue;

                    decimal newAmount = 0, tdsAdjustmet = 0;

                    if (decimal.TryParse(txtAmount.Text.Trim(), out newAmount) == false)
                    {
                        txtAmount.Focus();
                        divWarningMsg.InnerHtml = "Invalid amount, please check the amount.";
                        divWarningMsg.Hide = false;
                        return;
                    }
                    if (decimal.TryParse(txtTDSAdjustment.Text.Trim(), out tdsAdjustmet) == false)
                    {
                        txtTDSAdjustment.Focus();
                        divWarningMsg.InnerHtml = "Invalid tds adjustment amount, please check the amount.";
                        divWarningMsg.Hide = false;
                        return;
                    }

                    //if (existingAmount != newAmount || tdsAdjustmet != 0)
                    {
                        decimal adjustmentAmount = newAmount - existingAmount;

                        //if (adjustmentAmount < -1 || adjustmentAmount > 1)
                        {

                            otherIncomeList.Add(
                                new CalculationValue
                                {
                                    Type = type,
                                    SourceId = sourceId,
                                    AmountWithOutAdjustment = existingAmount,
                                    Amount = newAmount,
                                    TDSAdjustment = tdsAdjustmet
                                }
                                );

                           
                        }
                    }
                }

                // Case 3 : Other Deduction adjustment
                foreach (GridViewRow row in gvwOtherDeductions.Rows)
                {
                    int settlementId = int.Parse(gvwOtherDeductions.DataKeys[row.RowIndex][0].ToString());

                    decimal existingAmount = decimal.Parse(gvwOtherDeductions.DataKeys[row.RowIndex]["AmountWithOutAdjustment"].ToString());


                    TextBox txtAmount = row.FindControl("txtAmountOtherDeduction") as TextBox;
                    if (txtAmount.Enabled == false)
                        continue;

                    CheckBox chkExclude = row.FindControl("chkExclude") as CheckBox;
                    if (chkExclude.Enabled == false)
                        continue;

                    decimal newAmount = 0;

                    if (decimal.TryParse(txtAmount.Text.Trim(), out newAmount) == false)
                    {
                        txtAmount.Focus();
                        divWarningMsg.InnerHtml = "Invalid amount, please check the amount.";
                        divWarningMsg.Hide = false;
                        return;
                    }


                    //if (existingAmount != newAmount)
                    {
                        decimal adjustmentAmount = newAmount - existingAmount;

                        //if (adjustmentAmount < -1 || adjustmentAmount > 1)
                        {

                            otherDeductionList.Add(
                                new CalculationValue
                                {
                                    SettlementId = settlementId,
                                    AmountWithOutAdjustment = existingAmount,
                                    Amount = newAmount,
                                    ExcludeFromRetirement = chkExclude.Checked
                                }
                                );


                        }
                    }
                }
                PayManager.SaveIncomeDeductionAdjustment(str.ToString(), "Retirement adjustment");
                PayManager.UpdateRetirementOtherIncomeAdjustment(txtRetirementNotes.Text.Trim(),otherIncomeList,otherDeductionList, payrollPeriod.PayrollPeriodId, employeeId);

                divMsgCtl.InnerHtml = "Saved";
                divMsgCtl.Hide = false;
                calRetirementDate.Disable(true);
                LoadCalculation(payrollPeriod, employeeId);
            }
        }

        IIndividualInsurance ProcessInsurance(IIndividualInsurance indiv)
        {

            return null;
        }


        protected void btnCancel_Click(object sender, EventArgs e)
        {
            //btnSave.Text = "Save";

            //int employeeId = int.Parse(ddlEmployeeList.SelectedValue);

            //if (employeeId != -1)
            //{

            //    gvw.SelectedIndex = -1;
            //    CommonManager.SaveFirstIBranchfNotExists(employeeId);
            //    BindHistory(employeeId);

            //    ClearFields();
            //}
        }

        void ClearFields()
        {
            //UIHelper.SetSelectedInDropDown(ddlTransferToBranch, -1);
            //UIHelper.SetSelectedInDropDown(ddlTransferToDepartment, -1);
            //txtNote.Text = "";
            //calFromDate.Text = "";  
            //calDepartureDate.Text = "";
            //calLetterDate.Text = "";
            //calFromDate.Enabled = true;
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (btnDeleteRetirement.Visible)
                calRetirementDate.Disable(true);
        }

        /// <summary>
        /// Button is hidden as for internal use only
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnDelete_Click(object sender, EventArgs e)
        {
        }

        public bool IsEditable(object typeE, object sourceIdE)
        {
            int type = int.Parse(typeE.ToString());
            int source = int.Parse(sourceIdE.ToString());
           

            //income deduction that comes in Income Adjustment are editable
            foreach (KeyValue item in incomeDeductionForAdjustment)
            {
                // disable adjsutment to income pf
                //if (source + ":" + type == ((int)CalculationColumnType.IncomePF + ":" + (int)CalculationColumnType.IncomePF))
                //    return false;

                if (item.Value == source + ":" + type)
                {
                    return true;
                }
            }

            if(type==1 && variableIncomeList.Any(x=>x.IncomeId==source))
                return true;

            if (type == 8 && variableDeductionList.Any(x => x.IncomeId == source))
                return true;


            return false;
        }

        protected void btnDeleteRetirement_Click(object sender, EventArgs e)
        {
            PayrollPeriod payrollPeriod = CommonManager.GetLastPayrollPeriod();
            int employeeId = GetEmployeeId();

            if (CalculationManager.IsCalculationSavedForEmployee(payrollPeriod.PayrollPeriodId, employeeId))
            {
                divWarningMsg.InnerHtml = "Salary already saved, can not reset the adjustment, please delete the salary first.";
                divWarningMsg.Hide = false;
                return;
            }

            PayManager.DeleteRetirement(payrollPeriod.PayrollPeriodId, employeeId);
            divMsgCtl.InnerHtml = "Retirement has been removed with all adjustment being deleted also.";
            divMsgCtl.Hide = false;

            LoadEmployeeDetails(null, null);
            LoadRetiringThisMonth();
        }

        protected void btnResetAllAdjustment_Click(object sender, EventArgs e)
        {
            PayrollPeriod payrollPeriod = CommonManager.GetLastPayrollPeriod();
            int employeeId = GetEmployeeId();

            if (CalculationManager.IsCalculationSavedForEmployee(payrollPeriod.PayrollPeriodId, employeeId))
            {
                divWarningMsg.InnerHtml = "Salary already saved, can not reset the adjustment, please delete the salary first.";
                divWarningMsg.Hide = false;
                return;
            }

            PayManager.DeleteAllEmployeeAdjustemntForPeriod(payrollPeriod.PayrollPeriodId, employeeId);
            divMsgCtl.InnerHtml = "All adjustment has been deleted.";
            divMsgCtl.Hide = false;

            LoadCalculation(payrollPeriod, employeeId);
            calRetirementDate.Disable(true);
        }

        protected void btnShowAdjustment_Click(object sender, DirectEventArgs e)
        {
            int leaveTypeId = int.Parse(hdnLeaveTypeId.Text);
            int employeeId = int.Parse(hiddenEmpID.Value);
            PayrollPeriod payrollPeriod = CommonManager.GetLastPayrollPeriod();

            DAL.SettlementLeaveBalanceAdjustment adjustment =
                BLL.BaseBiz.PayrollDataContext.SettlementLeaveBalanceAdjustments.FirstOrDefault(x =>
                    x.EmployeeId == employeeId && x.LeaveTypeId == leaveTypeId && x.PayrollPeriodId == payrollPeriod.PayrollPeriodId);

            DAL.SettlementLeaveBalance settlement = BLL.BaseBiz.PayrollDataContext.SettlementLeaveBalances.FirstOrDefault(x =>
                x.EmployeeId == employeeId && x.LeaveTypeId == leaveTypeId && x.PayrollPeriodId == payrollPeriod.PayrollPeriodId);

            double regular = 0, retiremnt = 0;
            regular = settlement == null || settlement.RegularBalance == null ? 0 : settlement.RegularBalance.Value;
            retiremnt = settlement == null || settlement.RetirementBalance == null ? 0 : settlement.RetirementBalance.Value;


            if (adjustment != null)
            {
                txtRegular.Number = (adjustment.RegularAdjustment == null ? 0 : adjustment.RegularAdjustment.Value) + regular;
                txtRetirement.Number = (adjustment.RetirementAdjustment == null ? 0 : adjustment.RetirementAdjustment.Value) + retiremnt;
                txtNotes.Text = adjustment.Notes;
            }
            else
            {
                txtRegular.Number = regular;
                txtRetirement.Number = retiremnt;
                txtNotes.Text = "";
            }

            windowAdjustment.Show();
        }


        protected void btnResetLeaveAdjustment_Click(object sender, EventArgs e)
        {
            int leaveTypeId = int.Parse(hdnLeaveTypeId.Text);
            int employeeId = int.Parse(hiddenEmpID.Value);
            PayrollPeriod payrollPeriod = CommonManager.GetLastPayrollPeriod();

            DAL.SettlementLeaveBalanceAdjustment adjustment =
                BLL.BaseBiz.PayrollDataContext.SettlementLeaveBalanceAdjustments.FirstOrDefault(x =>
                    x.EmployeeId == employeeId && x.LeaveTypeId == leaveTypeId && x.PayrollPeriodId == payrollPeriod.PayrollPeriodId);

            if (adjustment != null)
            {
                BLL.BaseBiz.PayrollDataContext.SettlementLeaveBalanceAdjustments.DeleteOnSubmit(adjustment);
                BLL.BaseBiz.PayrollDataContext.SubmitChanges();

               
            }

            LoadCalculation(payrollPeriod, employeeId);
            LoadLeaves(payrollPeriod, employeeId);
        }
        protected void btnSaveLeaveAdjustment_Click(object sender, EventArgs e)
        {
            int leaveTypeId = int.Parse(hdnLeaveTypeId.Text);
            int employeeId = int.Parse(hiddenEmpID.Value);
            PayrollPeriod payrollPeriod = CommonManager.GetLastPayrollPeriod();

            DAL.SettlementLeaveBalanceAdjustment adjustment =
                BLL.BaseBiz.PayrollDataContext.SettlementLeaveBalanceAdjustments.FirstOrDefault(x =>
                    x.EmployeeId == employeeId && x.LeaveTypeId == leaveTypeId && x.PayrollPeriodId == payrollPeriod.PayrollPeriodId);

            DAL.SettlementLeaveBalance settlement = BLL.BaseBiz.PayrollDataContext.SettlementLeaveBalances.FirstOrDefault(x =>
                x.EmployeeId == employeeId && x.LeaveTypeId == leaveTypeId && x.PayrollPeriodId == payrollPeriod.PayrollPeriodId);

            double regular = 0, retiremnt = 0;
            regular = settlement == null || settlement.RegularBalance == null ? 0 : settlement.RegularBalance.Value;
            retiremnt = settlement == null || settlement.RetirementBalance == null ? 0 : settlement.RetirementBalance.Value;


            if (adjustment != null)
            {
                adjustment.RegularAdjustment = txtRegular.Number - regular;
                adjustment.RetirementAdjustment = txtRetirement.Number - retiremnt;
                adjustment.Notes = txtNotes.Text.Trim();
            }
            else
            {
                adjustment = new SettlementLeaveBalanceAdjustment();

                adjustment.RegularAdjustment = txtRegular.Number - regular;
                adjustment.RetirementAdjustment = txtRetirement.Number - retiremnt;
                adjustment.Notes = txtNotes.Text.Trim();

                adjustment.EmployeeId = employeeId;
                adjustment.LeaveTypeId = leaveTypeId;
                adjustment.PayrollPeriodId = payrollPeriod.PayrollPeriodId;

                BLL.BaseBiz.PayrollDataContext.SettlementLeaveBalanceAdjustments.InsertOnSubmit(adjustment);

            }

            // double value like RetirementAdjustment could have high prevision so make upto 6 decimal digit only
            // to prevent high precision float to decimal
            if (adjustment.RegularAdjustment != null)
                adjustment.RegularAdjustment = double.Parse(adjustment.RegularAdjustment.Value.ToString("n6"));
            if (adjustment.RetirementAdjustment != null)
                adjustment.RetirementAdjustment = double.Parse(adjustment.RetirementAdjustment.Value.ToString("n6"));

            BLL.BaseBiz.PayrollDataContext.SubmitChanges();

            LoadCalculation(payrollPeriod, employeeId);
            LoadLeaves(payrollPeriod, employeeId);


        }

        protected void btnSetRetirement_Click(object sender, EventArgs e)
        {

            PayrollPeriod payrollPeriod = CommonManager.GetLastPayrollPeriod();
            int employeeId = GetEmployeeId();
            


            if (payrollPeriod == null || CalculationManager.IsAllEmployeeSavedFinally(payrollPeriod.PayrollPeriodId, false))
            {
                divWarningMsg.InnerHtml = "Valid period is required for retirement.";
                divWarningMsg.Hide = false;
                return;
            }

            if (CalculationManager.IsCalculationSavedForEmployee(payrollPeriod.PayrollPeriodId, employeeId))
            {
                divWarningMsg.InnerHtml = "Salary already saved, can not change the date, please delete the salary first.";
                divWarningMsg.Hide = false;
                return;
            }

            bool isRetirement,isResigned;

            //if (rdbIsRetired.Checked)
            //{
                isRetirement = true;
                isResigned = false;
            //}
            //else
            //{
            //    isRetirement = false;
            //    isResigned = true;
            //}
            string retirementdate = calRetirementDate.Text.Trim();
            DateTime retirementDateEng = GetEngDate(retirementdate);

            if (retirementDateEng < payrollPeriod.StartDateEng.Value || retirementDateEng > payrollPeriod.EndDateEng.Value)
            {
                divWarningMsg.InnerHtml = "Retirement date must lie between the current period " + payrollPeriod.StartDate + " and " +
                    payrollPeriod.EndDate + ".";
                divWarningMsg.Hide = false;
                return;
            }

            if (LeaveAttendanceManager.IsAttendanceSavedByFillingEachDayForEmployee(payrollPeriod.PayrollPeriodId, employeeId) == false)
            {
                divWarningMsg.InnerHtml = "Please save the attendance by filling each day before retiring the employee.";
                divWarningMsg.Hide = false;
                return;
            }

            bodyDetails.Visible = true;
            
            

            EmployeeManager.SetRetirement(employeeId, retirementdate, retirementDateEng, isRetirement, isResigned
                ,int.Parse(ddlRetirementType.SelectedValue));

            calRetirementDate.Disable(true);
            btnDeleteRetirement.Visible = true;
            btnSetRetirement.Visible = false;

            LoadLeaves(payrollPeriod, employeeId);
            LoadCalculation(payrollPeriod, employeeId);
            LoadRetiringThisMonth();
        }

        private void LoadLeaves(PayrollPeriod payrollPeriod, int employeeId)
        {
            List<Report_HR_GetRetirementLeaveDetailsResult> leaveList = LeaveAttendanceManager.GetRetirementLeaveDetails(payrollPeriod, employeeId);


            // old grid
            //List<LeaveAdjustment> leaveList = GetBalanceList(payrollPeriod, employeeId);

            gridEncashableLeaves.DataSource = leaveList;
            gridEncashableLeaves.DataBind();

            
        }

        private void LoadRetireRehireLeaves(int employeeId)
        {
            RetireReHireEmployee emp = BLL.BaseBiz.PayrollDataContext.RetireReHireEmployees
                .Where(x => x.EmployeeId == employeeId).OrderByDescending(x => x.PayrollPeriodId).FirstOrDefault();

            if (emp == null)
                return;

            PayrollPeriod payrollPeriod = CommonManager.GetPayrollPeriod(emp.PayrollPeriodId);

            List<Report_HR_GetRetirementLeaveDetailsResult> leaveList = LeaveAttendanceManager.GetRetireReHireLeaveDetails(payrollPeriod, employeeId);


            // old grid
            //List<LeaveAdjustment> leaveList = GetBalanceList(payrollPeriod, employeeId);

            gridEncashableLeaves.DataSource = leaveList;
            gridEncashableLeaves.DataBind();


        }
        private static List<LeaveAdjustment> GetBalanceList(PayrollPeriod payrollPeriod, int employeeId)
        {
            // 1. display Encashable leaves
            List<LeaveAdjustment> leaveList = BLL.BaseBiz.PayrollDataContext.LeaveAdjustments.Where(x => x.LLeaveType.UnusedLeave != "Lapse" && x.EmployeeId == employeeId && x.PayrollPeriodId == payrollPeriod.PayrollPeriodId
                ).ToList();
            PayManager payMgr = new PayManager();
            foreach (LeaveAdjustment item in leaveList)
            {
                SettlementLeaveBalance leaveBalance = BLL.BaseBiz.PayrollDataContext.SettlementLeaveBalances
                    .FirstOrDefault(x => x.EmployeeId == employeeId && x.PayrollPeriodId == payrollPeriod.PayrollPeriodId && x.LeaveTypeId == item.LeaveTypeId);

                if (item.Encased == null)
                    item.Encased = 0;
                if (item.RetirementEncash == null)
                    item.RetirementEncash = 0;


                LLeaveType leaveType = new LeaveAttendanceManager().GetLeaveType(item.LeaveTypeId);
                item.Name = leaveType.Title;

                foreach (LLeaveEncashmentIncome income in leaveType.LLeaveEncashmentIncomes)
                {
                    if (item.EncashmentIncomes == null)
                        item.EncashmentIncomes = "";

                    if (item.EncashmentIncomes == "")
                        item.EncashmentIncomes = income.IncomeId == 0 ? "PF" : payMgr.GetIncomeById(income.IncomeId).Title;
                    else
                        item.EncashmentIncomes += ", " + (income.IncomeId == 0 ? "PF" : payMgr.GetIncomeById(income.IncomeId).Title);
                }

                if (leaveType.UnusedLeave == UnusedLeave.ENCASE && leaveBalance != null)
                {
                    //if (SessionManager.CurrentCompany.LeaveEncashTypePaysOn != null &&
                    //    SessionManager.CurrentCompany.LeaveEncashTypePaysOn == 13)
                    // in retirement all remaining balance will come in 25% 
                    {
                        item.RetirementEncash = leaveBalance.RetirementBalance == null ? 0 : leaveBalance.RetirementBalance.Value;
                        item.Encased = leaveBalance.RegularBalance == null ? 0 : leaveBalance.RegularBalance.Value;
                    }
                }
                else if (leaveType.UnusedLeave == UnusedLeave.LAPSEENCASE && leaveBalance != null)
                {
                    if (item.Encased == null)
                        item.Encased = 0;
                    // Show Regular/Retirement balance
                    if (leaveBalance != null)
                    {

                        item.RetirementEncash = leaveBalance.RetirementBalance == null ? 0 : leaveBalance.RetirementBalance.Value;
                        item.Encased = leaveBalance.RegularBalance == null ? 0 : leaveBalance.RegularBalance.Value;
                    }
                    //else if (item.Encased.Value > leaveType.EncaseDays.Value)
                    //{
                    //    item.RetirementEncash = item.Encased.Value - leaveType.EncaseDays.Value;
                    //    item.Encased = leaveType.EncaseDays.Value;
                    //}
                }
            }
            return leaveList;
        }

        private void LoadCalculation(PayrollPeriod payrollPeriod, int employeeId)
        {
            List<CalculationValue> incomeList;
            List<CalculationValue> deductionList;
            List<CalculationValue> otherincomeList;
            List<CalculationValue> otherdeductionList;
           
            CalculationManager.GetRetirementCalculation(payrollPeriod, employeeId,IsPastRetired,
                out incomeList, out deductionList, out otherincomeList, out otherdeductionList);

            


            gvw.DataSource = incomeList;
            gvw.DataBind();

            gvwDeductionList.DataSource = deductionList;
            gvwDeductionList.DataBind();

            netSalary.InnerHtml = GetCurrency(incomeList.Sum(x => x.Amount) - deductionList.Sum(x => x.Amount));

           
            gvwOtherIncomes.DataSource = otherincomeList;
            gvwOtherIncomes.DataBind();

            gvwOtherDeductions.DataSource = otherdeductionList;
            gvwOtherDeductions.DataBind();

            // Income total
            decimal incomeTotal = incomeList.Sum(x => x.Amount) + otherincomeList.Sum(x => x.NetPayable);
            decimal deductionTotal = deductionList.Sum(x => x.Amount) + otherdeductionList.Where(x=>x.ExcludeFromRetirement==false)
                .Sum(x => x.Amount);

            spanInomeTotal.InnerHtml = GetCurrency(incomeTotal);
            spanDeductionTotal.InnerHtml = GetCurrency(deductionTotal);

            spanNetPay.InnerHtml = GetCurrency(incomeTotal - deductionTotal);
        }
        private void LoadRetireRehireCalculation(int employeeId)
        {
            RetireReHireEmployee emp = BLL.BaseBiz.PayrollDataContext.RetireReHireEmployees
                .Where(x => x.EmployeeId == employeeId).OrderByDescending(x => x.PayrollPeriodId).FirstOrDefault();

            if (emp == null)
                return;

            PayrollPeriod payrollPeriod = CommonManager.GetPayrollPeriod(emp.PayrollPeriodId);

            List<CalculationValue> incomeList;
            List<CalculationValue> deductionList;
            List<CalculationValue> otherincomeList;
            List<CalculationValue> otherdeductionList;

            CalculationManager.GetRetireRehireCalculation(payrollPeriod, employeeId,
                out incomeList, out deductionList, out otherincomeList, out otherdeductionList);




            gvw.DataSource = incomeList;
            gvw.DataBind();

            gvwDeductionList.DataSource = deductionList;
            gvwDeductionList.DataBind();

            netSalary.InnerHtml = GetCurrency(incomeList.Sum(x => x.Amount) - deductionList.Sum(x => x.Amount));


            gvwOtherIncomes.DataSource = otherincomeList;
            gvwOtherIncomes.DataBind();

            gvwOtherDeductions.DataSource = otherdeductionList;
            gvwOtherDeductions.DataBind();

            // Income total
            decimal incomeTotal = incomeList.Sum(x => x.Amount) + otherincomeList.Sum(x => x.NetPayable);
            decimal deductionTotal = deductionList.Sum(x => x.Amount) + otherdeductionList.Where(x => x.ExcludeFromRetirement == false)
                .Sum(x => x.Amount);

            spanInomeTotal.InnerHtml = GetCurrency(incomeTotal);
            spanDeductionTotal.InnerHtml = GetCurrency(deductionTotal);

            spanNetPay.InnerHtml = GetCurrency(incomeTotal - deductionTotal);
        }
       
        public List<CalculationValue> AddOtherIncome(List<CalculationValue> incomeList)
        {

            return incomeList;
        }
    }
}

