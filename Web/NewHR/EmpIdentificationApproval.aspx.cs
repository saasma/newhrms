﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;
using System.Text;
using Utils;
using System.Drawing;
using System.Drawing.Drawing2D;
using Web.Helper;

namespace Web.NewHR
{
    public partial class EmpIdentificationApproval : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                BindGrid();                
            }
        }

        private void BindGrid()
        {
            this.Store1.DataSource = NewHRManager.GetHHumanResourceListForPhotoApproval();
            this.Store1.DataBind();
        }

        protected void btnApprove_Click(object sender, DirectEventArgs e)
        {
            string gridItemsJson = e.ExtraParams["gridItems"];
            List<HHumanResource> list = JSON.Deserialize<List<HHumanResource>>(gridItemsJson);

            List<int> employeeIdList = new List<int>();
            foreach (var item in list)
            {
                employeeIdList.Add(item.EmployeeId.Value);
            }

            if (employeeIdList.Count == 0)
                return;

            Status status = NewHRManager.ApproveEmployeePhoto(employeeIdList);
            if (status.IsSuccess)
            {                
                CheckboxSelectionModel1.ClearSelection();
                BindGrid();
                SetMessage(lblMsg, "Employee photographs approved successfully.");
            }
            else
            {
                SetError(lblMsg, "Error while approving employee photographs.");
            }

        }

        protected void btnView_Click(object sender, DirectEventArgs e)
        {
            int employeeId = int.Parse(hdnEmployeeId.Text);
            NewHRManager _NewHRManager = new NewHRManager();
            HHumanResource _HHumanResource = _NewHRManager.GetHumanResource(employeeId);
            if (_HHumanResource != null && !string.IsNullOrEmpty(_HHumanResource.tempUrlPhoto))
            {
                string filename = "../Uploads/" + _HHumanResource.tempUrlPhoto;
                ImgEmployee.ImageUrl = filename;

                Image1.ImageUrl = filename;
            }
            //WPhotoView.Show();
        }

        protected void btnDelete_Click(object sender, DirectEventArgs e)
        {
            string gridItemsJson = e.ExtraParams["gridItems"];
            List<HHumanResource> list = JSON.Deserialize<List<HHumanResource>>(gridItemsJson);

            List<int> employeeIdList = new List<int>();
            foreach (var item in list)
            {
                employeeIdList.Add(item.EmployeeId.Value);
            }

            if (employeeIdList.Count == 0)
                return;

            Status status = NewHRManager.DeleteEmployeePhoto(employeeIdList);
            if (status.IsSuccess)
            {
                CheckboxSelectionModel1.ClearSelection();
                BindGrid();
                SetMessage(lblMsg, "Employee photographs deleted successfully.");
            }
            else
            {
                SetError(lblMsg, "Error while deleting employee photographs.");
            }
        }

        protected void btnCropImage_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(_xField.Value) || string.IsNullOrEmpty(_yField.Value) || string.IsNullOrEmpty(_widthField.Value) || string.IsNullOrEmpty(_heightField.Value))
                return;
            var x = int.Parse(_xField.Value);
            var y = int.Parse(_yField.Value);
            var width = int.Parse(_widthField.Value);
            var height = int.Parse(_heightField.Value);

            if (height == 0 || width == 0)
                return;

            string filename = "";
            string newFileName = "";

            NewHRManager _NewHRManager = new NewHRManager();

            string saveLocation = "";
            string guidFileName = "";
            string thumbnailLocation = "";

            int employeeId = int.Parse(hdnEmployeeId.Text);

            HHumanResource _HHumanResource = _NewHRManager.GetHumanResource(employeeId);
            if (_HHumanResource != null && !string.IsNullOrEmpty(_HHumanResource.tempUrlPhoto))
            {
                filename = "../Uploads/" + _HHumanResource.tempUrlPhoto;


                string[] splFileName = _HHumanResource.tempUrlPhoto.Split('.');
                guidFileName = Guid.NewGuid().ToString() + "." + splFileName[1].ToString();

                saveLocation = Path.Combine(Server.MapPath(Config.UploadLocation), guidFileName);

                newFileName = "../Uploads/" + guidFileName;
                thumbnailLocation = Path.Combine(Server.MapPath(Config.UploadLocation), Guid.NewGuid().ToString() + "." + splFileName[1].ToString());

            }

            using (var photo =
                  System.Drawing.Image.FromFile(Server.MapPath(filename)))
            {
                using (var result =
                      new Bitmap(width, height, photo.PixelFormat))
                {
                    //result.SetResolution(
                    //        photo.HorizontalResolution,
                    //        photo.VerticalResolution);

                    result.SetResolution(200, 200);

                    Bitmap bmpNew = new Bitmap(result);
                    result.Dispose();


                    using (var g = Graphics.FromImage(bmpNew))
                    {
                        g.InterpolationMode =
                             InterpolationMode.HighQualityBicubic;
                        g.DrawImage(photo,
                             new Rectangle(0, 0, width, height),
                             new Rectangle(x, y, width, height),
                             GraphicsUnit.Pixel);


                        photo.Dispose();
                        g.Dispose();

                        bmpNew.Height.Equals(height);
                        bmpNew.Width.Equals(width);

                        bmpNew.Save(Server.MapPath(newFileName));



                        NewHRManager mgr = new NewHRManager();
                        //UploadType type = (UploadType)1;

                        try
                        {

                            WebHelper.CreateThumbnail(saveLocation, 200, 200, thumbnailLocation);
                            thumbnailLocation = Path.GetFileName(thumbnailLocation);
                        }
                        catch { }

                        NewHRManager.SaveUpdateEmployeePhotograph(employeeId, guidFileName, thumbnailLocation);

                        X.Js.Call("AfterSaveCall");                     

                    }
                }
            }


        }

        protected void btnView1_Click(object sender, EventArgs e)
        {
            NewHRManager _NewHRManager = new NewHRManager();
            HHumanResource _HHumanResource = _NewHRManager.GetHumanResource(int.Parse(hdnEmployeeId.Text));
            if (_HHumanResource != null && !string.IsNullOrEmpty(_HHumanResource.tempUrlPhoto))
            {
                string filename = "../Uploads/" + _HHumanResource.tempUrlPhoto;
                Image1.ImageUrl = filename;
                X.Js.Call("myFunction");
            }
            
        }

        protected void btnSaveCall_Click(object sender, EventArgs e)
        {
            NewMessage.ShowNormalMessage("Employee photograph cropped successfully.");
            BindGrid();
        }
    }
}