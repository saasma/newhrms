﻿<%@ Page Title="Deputation list" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="DeputationList.aspx.cs" Inherits="Web.NewHR.DeputationList" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function medicalTaxCreditPopupCall(medicalTaxId) {
            var ret = medicalTaxCreditPopup("Id=" + medicalTaxId);
            if (ret == "Yes")
                __doPostBack('Reload', '');
        }

        function parentReloadCallbackFunction(childWindow, ret) {
            childWindow.close();
            if (ret == "Yes")
                __doPostBack('Reload', '');

        }

        function refreshWindow() {
            __doPostBack('Reload', '');
        }

        function popupUpdateLeaveCall(LeaveId) {
            var ret = popup('Id=' + LeaveId + '&type=Deputation');


            return false;
        }
    </script>
    <style type="text/css">
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
    </style>
    <link href="../css/core.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Deputation List
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <asp:LinkButton runat="server" Visible="false"></asp:LinkButton>
        <div class="attribute" style="padding:10px">
            <table>
                <tr>
                    <td>
                        <strong>Search </strong>&nbsp;
                        <asp:TextBox Width="160px" ID="txtEmpSearchText" runat="server" OnTextChanged="txtEmpSearchText_TextChanged"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Button ID="btnLoad" CssClass="btn btn-default btn-sm" Style="margin-left: 10px; width: 100px;" runat="server"
                            OnClick="btnLoad_Click" Text="Load" />
                        <cc1:TextBoxWatermarkExtender ID="TBWE2" runat="server" TargetControlID="txtEmpSearchText"
                            WatermarkText="Employee name" WatermarkCssClass="searchBoxText" />
                    </td>
                    <%-- <td>
                        <asp:LinkButton ID="btnExport" runat="server" Text="Import C/Y Expense" OnClientClick="medicalTaxImportPopup();return false;"
                            CssClass=" excel marginRight tiptip" Style="float: left;" />
                    </td>--%>
                </tr>
            </table>
        </div>

        <div style="text-align: left">
                <asp:Button ID="Button1" Width="110px" CausesValidation="false" CssClass="btn btn-primary btn-sm" runat="server"
                    Text="New Deputation" OnClientClick="window.location='DeputationDetails.aspx';return false;" />
            </div>

        <br />
        <div class="separator clear">
            <asp:GridView ID="gvwList" CssClass="tableLightColor" Width="100%" DataKeyNames="DeputationEmployeeId"
                PagerStyle-HorizontalAlign="Center" PagerStyle-CssClass="defaultPagingBar" AllowPaging="true"
                runat="server" AutoGenerateColumns="False" CellPadding="4" GridLines="None" PageSize="100">
                <Columns>
                    <asp:TemplateField HeaderText="Deputation" HeaderStyle-Width="80px" HeaderStyle-HorizontalAlign="Left"
                        ItemStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <%# Eval("DeputationText")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Employee" HeaderStyle-Width="120px" HeaderStyle-HorizontalAlign="Left"
                        ItemStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <%# Eval("EmployeeName")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="To Branch" HeaderStyle-Width="120px" HeaderStyle-HorizontalAlign="Left"
                        ItemStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <%# Eval("ToBranch")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="To Department" HeaderStyle-Width="120px" HeaderStyle-HorizontalAlign="Left"
                        ItemStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <%# Eval("ToDepartment")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Description" HeaderStyle-Width="120px" HeaderStyle-HorizontalAlign="Left"
                        ItemStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <%# Eval("Description")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Letter No" HeaderStyle-Width="80px" HeaderStyle-HorizontalAlign="Left"
                        ItemStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <%# Eval("LetterNumber")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="From Date" HeaderStyle-Width="80px" HeaderStyle-HorizontalAlign="Left"
                        ItemStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <%# Eval("FromDate")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="To Date" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left"
                        ItemStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <%# Eval("ToDate")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Commute Days" HeaderStyle-Width="80px" HeaderStyle-HorizontalAlign="Left"
                        ItemStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <%# Eval("CommuteDays")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Approved By" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left"
                        ItemStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <%# (Eval("ApprovedByText"))%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="" HeaderStyle-Width="90px" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <a href="javascript:void(0)" onclick='<%# "popupUpdateLeaveCall(" +  Eval("DeputationEmployeeId") + ");return false;" %>'
                                style="width: 80px; display: block;" id="ImageButton12" />Review Team</a>
                        </ItemTemplate>
                        <HeaderStyle Width="30px"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Edit" HeaderStyle-Width="100px" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:HyperLink ID="linkEdit" ImageUrl="~/images/edit.gif" NavigateUrl='<%# "~/newhr/deputationdetails.aspx?did=" + Eval("DeputationEmployeeId") %>'
                                runat="server"></asp:HyperLink>
                        </ItemTemplate>
                        <HeaderStyle Width="30px"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:TemplateField>
                    <asp:TemplateField></asp:TemplateField>
                </Columns>
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                <SelectedRowStyle CssClass="selected" />
                <PagerStyle HorizontalAlign="Center" CssClass="defaultPagingBar"></PagerStyle>
                <EmptyDataTemplate>
                    <b>No deputation history. </b>
                </EmptyDataTemplate>
            </asp:GridView>
            <uc1:PagingCtl ID="pagingCtl" OnNextRecord="btnNext_Click" OnPrevRecord="btnPrevious_Click"
                OnDropDownShowRecordsChanged="ddlRecords_SelectedIndexChanged" runat="server" />
        </div>
    </div>
</asp:Content>
