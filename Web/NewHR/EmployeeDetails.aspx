﻿<%@ Page Title="Employee Details" EnableViewState="false" EnableEventValidation="false" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="EmployeeDetails.aspx.cs" Inherits="Web.NewHR.EmployeeDetails" %>

<%@ Register Src="~/newhr/usercontrols/SalaryChangeListCtl.ascx" TagName="SalaryChangeCtl"
    TagPrefix="uc1" %>
<%@ Register Src="~/newhr/usercontrols/DesignationTransferListCtl.ascx" TagName="DesignationChangeCtl"
    TagPrefix="uc1" %>
<%@ Register Src="~/NewHR/UserControls/BranchTransferListCtl.ascx" TagName="BranchTrasferCtl"
    TagPrefix="uc1" %>
<%@ Register Src="~/NewHR/UserControls/ActingCtrlView.ascx" TagName="ActingCtl" TagPrefix="uc1" %>
<%@ Register Src="~/NewHR/UserControls/RewardListCtl.ascx" TagName="RewardCtl" TagPrefix="uc1" %>
<%@ Register Src="~/NewHR/UserControls/HealthCtrlView.ascx" TagName="HealthCtl" TagPrefix="ucHealthCtl" %>
<%@ Register Src="~/newhr/UserControls/EmployeeWizard.ascx" TagName="EmployeeWizard"
    TagPrefix="uc2" %>
<%@ Register Src="~/NewHR/UserControls/EducationCtrlView.ascx" TagName="UCEduCtrlTest"
    TagPrefix="uEduCtrlTest" %>
<%@ Register Src="~/NewHR/UserControls/FamilyCtrlView.ascx" TagName="UCFamilyCtrl" TagPrefix="uUFamilyCtrl" %>
<%@ Register Src="~/NewHR/UserControls/TrainingCtrlView.ascx" TagName="UCTrainingCtl"
    TagPrefix="uUTrainingCtrl" %>
<%@ Register Src="~/NewHR/UserControls/SeminarCtrlView.ascx" TagName="UUSeminarCtrl"
    TagPrefix="ucUSeminarCtrl" %>
<%@ Register Src="~/NewHR/UserControls/SkillSetCtrlView.ascx" TagName="UUSkillSetCtrl"
    TagPrefix="ucUSkillSetCtrl" %>
<%@ Register Src="~/NewHR/UserControls/LanguageCtrlView.ascx" TagName="UULanguageCtrl"
    TagPrefix="ucULanguageCtr" %>
<%@ Register Src="~/NewHR/UserControls/PublicationCtrlView.ascx" TagName="UUPublicationCtrl"
    TagPrefix="ucUPublicationCtrl" %>
<%@ Register Src="~/NewHR/UserControls/EmploymentCtrlView.ascx" TagName="UEmploymentCtrl"
    TagPrefix="ucEmploymentCtrl" %>
<%@ Register Src="~/NewHR/UserControls/StatusChangeHistoryCtrl.ascx" TagName="StatusChangeCtrl"
    TagPrefix="ucStatusChange" %>
<%@ Register Src="~/NewHR/UserControls/EmpDetailsLeftMenu.ascx" TagName="EmpDetailsWizard"
    TagPrefix="ucEW" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .items
        {
            padding-left: 20px;
        }
        .items span
        {
            
            display: block;
            font-family: 'Open Sans' ,sans-serif;
            padding-bottom: 2px;
        }
        .items img
        {
            float:left;
            width:18px;height:18px;
            
margin-right: 10px;
        }
        
        .fieldTable span
        {
            width: 400px;
        }
        
        /*ext grid to nomal grid*/


        .buttonBlock
        {
            display: none;
        }
        #content
        {
            background: white;
        }

        }
        .accordion-edit:hover, .accordion-edit
        {
            font-weight: normal;
        }
        a:hover
        {
            
            font-weight: normal;
        }
        .accordion {
margin-bottom: 5px;

}

    .x-tab-bar-default{background-color:Transparent!important;}

    .editImgPosition
    {
        float:right;
    }
   </style>
    <script type="text/javascript">

        function expandAllCollapseAllHandler(btn) {
            if (btn.getText() == "EXPAND") {
                btn.setText("COLLAPSE");

                var list = Ext.query(".accordion-toggle");
                for (i = 0; i < list.length; i++) {
                    var item = list[i];
                    item.className = "accordion-toggle collapsed";
                }


                list = Ext.query(".accordion-body");
                for (i = 0; i < list.length; i++) {
                    var item = list[i];

                    item.className = "accordion-body in collapse";
                    item.style.height = "";
                }
            }
            else {
                btn.setText("EXPAND");


                var list = Ext.query(".accordion-toggle");
                for (i = 0; i < list.length; i++) {
                    var item = list[i];
                    item.className = "accordion-toggle";
                }

                list = Ext.query(".accordion-body");
                for (i = 0; i < list.length; i++) {
                    var item = list[i];


                    item.className = "accordion-body collapse";
                    item.style.height = "";
                }
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <ext:Hidden ID="Hidden_CustomerID" runat="server">
    </ext:Hidden>
    <div class="contentpanel">
        <div class="innerLR">
            <%--<uc2:EmployeeWizard Id="EmployeeWizard1" runat="server" />--%>
            <div class="separator bottom">
            </div>
            <%-- <h4 class="heading">
            Employee Details
        </h4>--%>
            <table>
                <tr>
                    <td valign="top">
                        <%--<uc2:EmployeeWizard runat="server"  runat="server" id="EmployeeWizard1"/>--%>
                        <div style="float: left; margin-right: 10px;">
                            <ucEW:EmpDetailsWizard Id="EmployeeWizard1" runat="server" />
                        </div>
                    </td>
                    <td valign="top" style="padding-left: 10px;">
                        <ext:Label ID="lblMsg" runat="server" />
                        <div style="margin-bottom: 10px">
                            <table>
                                <tr>
                                    <td valign="top">
                                        <asp:Image ID="image" CssClass="left" runat="server" ImageUrl="~/images/sample.jpg"
                                            Width="150px" Height="150px" />
                                    </td>
                                    <td valign="top">
                                        <div class="left items">
                                            <h3 runat="server" class="heading" style="color: #214B71; margin: 0; margin-top: 2px;
                                                margin-bottom: 3px;" id="title">
                                            </h3>
                                            <span runat="server" style="font-size: 15px; font-weight: bold" id="positionDesignation">
                                            </span>
                                            <img runat="server" id="groupLevelImg" src="../Styles/images/emp_position.png" style="margin-top: 8px;" />
                                            <span runat="server" style="padding-top: 10px;" id="groupLevel"></span>
                                            <img src="../Styles/images/emp_br_dept.png" />
                                            <span runat="server" id="branch"></span>
                                            <img runat="server" id="imgMobile" src="../Styles/images/emp__phone.png" />
                                            <span runat="server" id="contactMobile"></span>
                                            <img src="../Styles/images/telephone.png" />
                                            <span runat="server" id="contactPhone"></span>
                                            <img src="../Styles/images/emp_email.png" />
                                            <span runat="server" id="email"></span>
                                            <img src="../Styles/images/emp_work.png" />
                                            <span runat="server" id="workingFor" style="width: 500px"></span><span runat="server"
                                                id="spanRetiredDate" style="width: 500px; color: Red"></span>
                                        </div>
                                    </td>
                                    <td style="width: 10px;">
                                    </td>
                                    <td valign="top">
                                        <ext:SplitButton ID="btnEditDetails" Cls="btn btn-primary" Width="130px" runat="server"
                                            Text="Edit Info">
                                            <DirectEvents>
                                                <Click OnEvent="btnEditEmployee_Click" />
                                            </DirectEvents>
                                            <Menu>
                                                <ext:Menu runat="server">
                                                    <Items>
                                                        <ext:HyperLink ID="HyperLink1" Target="_blank" NavigateUrl="~/newhr/PersonalDetail.aspx?ID="
                                                            runat="server" Text="Main Info" />
                                                        <ext:HyperLink Target="_blank" ID="HyperLink2" NavigateUrl="~/newhr/address.aspx?ID="
                                                            runat="server" Text="Address" />
                                                                 <ext:HyperLink Target="_blank" ID="HyperLink13" NavigateUrl="~/newhr/jd.aspx?ID="
                                                            runat="server" Text="JD" />
                                                        <ext:HyperLink Target="_blank" ID="HyperLink3" NavigateUrl="~/newhr/Health.aspx?ID="
                                                            runat="server" Text="Health" />
                                                        <ext:HyperLink Target="_blank" ID="HyperLink4" NavigateUrl="~/newhr/DocumentListing.aspx?ID="
                                                            runat="server" Text="Documents" />
                                                        <ext:HyperLink Target="_blank" ID="HyperLink5" NavigateUrl="~/newhr/Family.aspx?ID="
                                                            runat="server" Text="Family" />
                                                        <ext:HyperLink Target="_blank" ID="HyperLink6" NavigateUrl="~/newhr/Identification.aspx?ID="
                                                            runat="server" Text="Identification" />
                                                        <ext:HyperLink Target="_blank" ID="HyperLink7" NavigateUrl="~/newhr/PreviousEmployment.aspx?ID="
                                                            runat="server" Text="Experience" />
                                                        <ext:HyperLink Target="_blank" ID="HyperLink8" NavigateUrl="~/newhr/EducationTraining.aspx?ID="
                                                            runat="server" Text="Education" />
                                                        <ext:HyperLink Target="_blank" ID="HyperLink9" NavigateUrl="~/newhr/EmployeeLeave.aspx?ID="
                                                            runat="server" Text="Leaves" />
                                                        <ext:HyperLink Target="_blank" ID="HyperLink10" NavigateUrl="~/newhr/EmployeePayroll.aspx?ID="
                                                            runat="server" Text="Payroll" />
                                                        <ext:HyperLink Target="_blank" ID="linkRewardAddition" NavigateUrl="~/newhr/EmployeeGradeReward.aspx?ID="
                                                            runat="server" Text="Reward / Additional" />
                                                        <ext:HyperLink Target="_blank" ID="HyperLink11" NavigateUrl="~/newhr/OtherHRDetails.aspx?ID="
                                                            runat="server" Text="Other" />
                                                    </Items>
                                                </ext:Menu>
                                            </Menu>
                                        </ext:SplitButton>
                                        <br />
                                        <br />
                                        <ext:SplitButton ID="slitBtnQuickTasks" Cls="btn btn-primary" Width="130px" runat="server"
                                            Text="Quick Tasks">
                                            <%--  <DirectEvents>
                                                <Click OnEvent="btnEditEmployee_Click" />
                                            </DirectEvents>--%>
                                            <Menu>
                                                <ext:Menu>
                                                    <Items>
                                                        <ext:HyperLink ID="HyperLink12" Target="_blank" NavigateUrl="~/CP/Report/TaxDetails.aspx?ID="
                                                            runat="server" Text="Tax Calculation" />
                                                        <ext:HyperLink Target="_blank" ID="HyperLink14" NavigateUrl="~/CP/OptimumPFAndCIT.aspx?ID="
                                                            runat="server" Text="Optimum CIT" />
                                                        <ext:HyperLink Target="_blank" ID="HyperLink15" NavigateUrl="~/CP/InsuranceList.aspx?ID="
                                                            runat="server" Text="Life Insurance" />
                                                        <ext:HyperLink Target="_blank" ID="HyperLink16" NavigateUrl="~/CP/LoanAdjustment.aspx?ID="
                                                            runat="server" Text="Loan Adjustment" />
                                                        <ext:HyperLink Target="_blank" ID="HyperLink17" NavigateUrl="~/CP/AdvanceAdjustment.aspx?ID="
                                                            runat="server" Text="Advance Adjustment" />
                                                        <ext:HyperLink Target="_blank" ID="HyperLink18" NavigateUrl="~/CP/BranchTransferList.aspx?ID="
                                                            runat="server" Text="Branch Transfer" />
                                                    </Items>
                                                </ext:Menu>
                                            </Menu>
                                        </ext:SplitButton>
                                    </td>
                                </tr>
                            </table>
                            <div style="clear: both">
                            </div>
                        </div>

                        <ext:TabPanel ID="Panel2" runat="server" MinHeight="500" AutoHeight="true" StyleSpec="padding-top:10px"
                            AutoWidth="true" DefaultBorder="false">
                            <Items>
                                <ext:Panel Title="Personal Info" runat="server">
                                    <Content>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <!-- panel-btns -->
                                                <h4 class="panel-title">
                                                    <i></i><a class="accordion-toggle">Employee Details </a><a href="~/newhr/PersonalDetail.aspx?ID="
                                                        runat="server" id="editPersonalInfo" class="accordion-edit"></a>
                                                </h4>
                                            </div>
                                            <!-- panel-heading -->
                                            <div class="panel-body">
                                                <table>
                                                    <tr>
                                                        <td valign="top">
                                                            <table class="fieldTable firsttdskip">
                                                                <tr>
                                                                    <td style="width: 170px;">
                                                                        EIN
                                                                    </td>
                                                                    <td>
                                                                        <span runat="server" id="ein"></span>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        Date of Birth
                                                                    </td>
                                                                    <td>
                                                                        <span runat="server" id="dob"></span>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td runat="server" id="gender1">
                                                                        {0}
                                                                    </td>
                                                                    <td>
                                                                        <span runat="server" id="gender"></span>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        Marital Status
                                                                    </td>
                                                                    <td>
                                                                        <span runat="server" id="maritalStatus"></span>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        Father's Name
                                                                    </td>
                                                                    <td>
                                                                        <span runat="server" id="fatherName"></span>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        Hire Method
                                                                    </td>
                                                                    <td>
                                                                        <span runat="server" id="hireMethod"></span>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        Hire Date
                                                                    </td>
                                                                    <td>
                                                                        <span runat="server" id="hireDate"></span>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td style="width: 100px">
                                                        </td>
                                                        <td valign="top">
                                                            <table class="fieldTable firsttdskip">
                                                                <tr runat="server" id="trLevel">
                                                                    <td style="width: 170px;">
                                                                        Level
                                                                    </td>
                                                                    <td>
                                                                        <span runat="server" id="level"></span>
                                                                    </td>
                                                                </tr>
                                                                <tr runat="server" id="trGrade">
                                                                    <td style="width: 170px;">
                                                                        Grade
                                                                    </td>
                                                                    <td>
                                                                        <span runat="server" id="gradeDisplay"></span>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        Job Status
                                                                    </td>
                                                                    <td>
                                                                        <span runat="server" id="currentJobStatus"></span>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        Contract End Date
                                                                    </td>
                                                                    <td>
                                                                        <span runat="server" id="contractEndDate"></span>
                                                                    </td>
                                                                </tr>
                                                                <tr runat="server" id="rowFunctionalTitle">
                                                                    <td>
                                                                        Functional Title
                                                                    </td>
                                                                    <td>
                                                                        <span runat="server" id="spanFunctionalTitle"></span>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <!-- panel-btns -->
                                                <h4 class="panel-title">
                                                    <i></i><a class="accordion-toggle">Address </a><a href="~/newhr/Address.aspx?ID="
                                                        runat="server" id="editAddress" class="editImgPosition">
                                                        <img src="../Styles/images/side_edit.png" /></a></h4>
                                            </div>
                                            <!-- panel-heading -->
                                            <div class="panel-body">
                                                <table class="fieldTable firsttdskip">
                                                    <%--locality district country--%>
                                                    <tr>
                                                        <td style="width: 170px; font-weight: bold" valign="top">
                                                            Permanent Address
                                                        </td>
                                                        <td valign="top">
                                                            <span runat="server" id="addressPermanent"></span>
                                                        </td>
                                                        <td width="50">
                                                        </td>
                                                        <td valign="top" style="font-weight: bold">
                                                            Temporary Address
                                                        </td>
                                                        <td valign="top">
                                                            <span runat="server" id="addressTemporary"></span>
                                                        </td>
                                                    </tr>
                                                     <tr runat="server" id="rowCitizenshipIssueAddress" visible="false">
                                                        <td style="width: 170px; font-weight: bold" valign="top">
                                                            Citizenship Issue Address 
                                                        </td>
                                                        <td valign="top">
                                                            <span runat="server" id="spanCitizenshipIssueAddress"></span>
                                                        </td>
                                                       
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <!-- panel-btns -->
                                                <h4 class="panel-title">
                                                    <i></i><a class="accordion-toggle">Contact Information </a><a href="~/newhr/Address.aspx?ID="
                                                        runat="server" id="editContact" class="editImgPosition">
                                                        <img src="../Styles/images/side_edit.png" /></a></h4>
                                            </div>
                                            <!-- panel-heading -->
                                            <div class="panel-body">
                                                <table class="fieldTable firsttdskip">
                                                    <tr>
                                                        <td style="width: 170px;">
                                                            Extension No.
                                                        </td>
                                                        <td>
                                                            <span runat="server" id="extNo"></span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Personal No.
                                                        </td>
                                                        <td>
                                                            <span runat="server" id="noPersonalNo"></span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Mobile No.
                                                        </td>
                                                        <td>
                                                            <span runat="server" id="noPersonalMobile"></span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Emergency Contact Person .
                                                        </td>
                                                        <td>
                                                            <span runat="server" id="contactEmergency"></span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Emergency Contact No. .
                                                        </td>
                                                        <td>
                                                            <span runat="server" id="contactEmergencyNo"></span>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </Content>
                                </ext:Panel>
                                <ext:Panel ID="Panel1" Title="Service Details" runat="server">
                                    <Content>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <h4 class="panel-title">
                                                                <i></i>Service History</h4>
                                                        </td>
                                                        <td style="padding-left: 50px">
                                                            <ext:LinkButton ID="btnLoadHistory" runat="server" Text="Load History">
                                                                <DirectEvents>
                                                                    <Click OnEvent="btnLoadHistory_Click">
                                                                        <EventMask ShowMask="true" />
                                                                    </Click>
                                                                </DirectEvents>
                                                            </ext:LinkButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="panel-body">
                                                <ext:GridPanel StyleSpec="margin-top:15px;" Width="1200" ID="ServiceHistory" runat="server"
                                                    Cls="itemgrid">
                                                    <Store>
                                                        <ext:Store ID="Store3" runat="server" AutoLoad="false">
                                                            <Model>
                                                                <ext:Model ID="Model4" runat="server">
                                                                    <Fields>
                                                                        <ext:ModelField Name="EIN" Type="String" />
                                                                        <ext:ModelField Name="Name" Type="String" />
                                                                        <ext:ModelField Name="EVENT" Type="String" />
                                                                        <ext:ModelField Name="FromDate" Type="String" />
                                                                        <ext:ModelField Name="FromDateEng" Type="Date" />
                                                                        <ext:ModelField Name="Branch" Type="String" />
                                                                        <ext:ModelField Name="Department" Type="String" />
                                                                        <ext:ModelField Name="STATUS" Type="String" />
                                                                        <ext:ModelField Name="Level" Type="String" />
                                                                    </Fields>
                                                                </ext:Model>
                                                            </Model>
                                                        </ext:Store>
                                                    </Store>
                                                    <ColumnModel>
                                                        <Columns>
                                                            <ext:Column ID="Column7" Sortable="false" MenuDisabled="true" runat="server" Text="EIN"
                                                                Width="60" Align="Right" DataIndex="EIN" />
                                                            <ext:Column ID="Column8" Sortable="false" MenuDisabled="true" runat="server" Text="Employee Name"
                                                                Width="180" Align="Left" DataIndex="Name" />
                                                            <ext:Column ID="Column9" Sortable="false" MenuDisabled="true" runat="server" Text="Event"
                                                                Width="110" Align="Left" DataIndex="EVENT" />
                                                            <ext:Column ID="Column10" Sortable="false" MenuDisabled="true" runat="server" Text="Nepali Date"
                                                                Width="110" Align="Left" DataIndex="FromDate" />
                                                            <ext:DateColumn ID="colRolloutDate" runat="server" Align="Left" Text="English Date"
                                                                MenuDisabled="true" Sortable="false" Format="yyyy/MM/dd" DataIndex="FromDateEng"
                                                                Resizable="false" Draggable="false" Width="103">
                                                            </ext:DateColumn>
                                                            <ext:Column ID="Column11" Sortable="false" MenuDisabled="true" runat="server" Text="Branch"
                                                                Width="200" Align="Left" DataIndex="Branch" />
                                                            <ext:Column ID="Column12" Sortable="false" MenuDisabled="true" runat="server" Text="Department"
                                                                Width="200" Align="Left" DataIndex="Department" />
                                                            <ext:Column ID="Column13" Sortable="false" MenuDisabled="true" runat="server" Text="Status"
                                                                Width="100" Align="Left" DataIndex="STATUS">
                                                                <%-- <Renderer Handler='return value + "%"' />--%>
                                                            </ext:Column>
                                                            <ext:Column ID="Column14" Sortable="false" MenuDisabled="true" runat="server" Text="Position"
                                                                Width="200" Align="Left" DataIndex="Level">
                                                                <%--  <Renderer Handler='return value + "%"' />--%>
                                                            </ext:Column>
                                                        </Columns>
                                                    </ColumnModel>
                                                    <SelectionModel>
                                                        <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                                                    </SelectionModel>
                                                </ext:GridPanel>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <i></i>Status Change</h4>
                                            </div>
                                            <div class="panel-body">
                                                <ucStatusChange:StatusChangeCtrl Id="statusChgCtrl" runat="server" />
                                            </div>
                                        </div>
                                        <div id="divLevelGradeHistory" class="panel panel-default" runat="server">
                                            <div class="panel-heading">
                                                <!-- panel-btns -->
                                                <h4 class="panel-title">
                                                    <i></i>Level and Grade History</h4>
                                            </div>
                                            <!-- panel-heading -->
                                            <div class="panel-body">
                                                <ext:GridPanel ID="gridLevelGradeHistory" runat="server" Width="1100" Cls="itemgrid">
                                                    <Store>
                                                        <ext:Store ID="StorePreviousEmployment" runat="server">
                                                            <Model>
                                                                <ext:Model ID="Model1" runat="server">
                                                                    <Fields>
                                                                        <ext:ModelField Name="FromDate" Type="String" />
                                                                        <ext:ModelField Name="OldLevel" Type="string" />
                                                                        <ext:ModelField Name="PrevStepGrade" Type="string" />
                                                                        <ext:ModelField Name="NewLevel" Type="string" />
                                                                        <ext:ModelField Name="StepGrade" Type="string" />
                                                                        <ext:ModelField Name="ChangeType" Type="string" />
                                                                    </Fields>
                                                                </ext:Model>
                                                            </Model>
                                                        </ext:Store>
                                                    </Store>
                                                    <ColumnModel>
                                                        <Columns>
                                                            <ext:Column ID="Column1" runat="server" Text="From Date" DataIndex="FromDate" Width="100" />
                                                            <ext:Column ID="Column2" runat="server" Text="Old Level" DataIndex="OldLevel" Width="250">
                                                            </ext:Column>
                                                            <ext:Column ID="Column3" runat="server" Text="Old Grade" DataIndex="PrevStepGrade"
                                                                Width="80">
                                                            </ext:Column>
                                                            <ext:Column ID="Column4" runat="server" Text="New Level" DataIndex="NewLevel" Width="250">
                                                            </ext:Column>
                                                            <ext:Column ID="Column5" runat="server" Text="New Grade" DataIndex="StepGrade" Width="80">
                                                            </ext:Column>
                                                            <ext:Column ID="Column6" Width="200" runat="server" Text="Change Type" DataIndex="ChangeType">
                                                            </ext:Column>
                                                        </Columns>
                                                    </ColumnModel>
                                                    <View>
                                                        <ext:GridView EnableTextSelection="true" />
                                                    </View>
                                                    <SelectionModel>
                                                        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                                                    </SelectionModel>
                                                </ext:GridPanel>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <!-- panel-btns -->
                                                <h4 class="panel-title">
                                                    <i></i>Transfer</h4>
                                            </div>
                                            <!-- panel-heading -->
                                            <div class="panel-body">
                                                <uc1:BranchTrasferCtl IsDisplayMode="true" Id="ActingCtl1" runat="server" />
                                            </div>
                                        </div>
                                        <div class="panel panel-default" runat="server" id="designationChange">
                                            <div class="panel-heading">
                                                <!-- panel-btns -->
                                                <h4 class="panel-title">
                                                    <i></i>Designation Change</h4>
                                            </div>
                                            <!-- panel-heading -->
                                            <div class="panel-body">
                                                <uc1:DesignationChangeCtl Id="BranchTrasferCtl1" runat="server" />
                                            </div>
                                        </div>
                                        <div class="panel panel-default" runat="server" id="salaryChange">
                                            <div class="panel-heading">
                                                <!-- panel-btns -->
                                                <h4 class="panel-title">
                                                    <i></i>Salary Change</h4>
                                            </div>
                                            <!-- panel-heading -->
                                            <div class="panel-body">
                                                <uc1:SalaryChangeCtl Id="DesignationChangeCtl1" runat="server" />
                                            </div>
                                        </div>
                                        <div id="Div14" class="panel panel-default" runat="server">
                                            <div class="panel-heading">
                                                <!-- panel-btns -->
                                                <h4 class="panel-title">
                                                    <i></i>Acting
                                                </h4>
                                            </div>
                                            <!-- panel-heading -->
                                            <div class="panel-body">
                                                <uc1:ActingCtl Id="RewardCtl1" runat="server" />
                                            </div>
                                        </div>
                                    </Content>
                                </ext:Panel>
                                <ext:Panel ID="Panel3" Title="Education and Training" runat="server">
                                    <Content>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <!-- panel-btns -->
                                                <h4 class="panel-title">
                                                    <i></i><a class="accordion-toggle">Education </a><a href="~/newhr/EducationTraining.aspx?id="
                                                        runat="server" id="skill9" class="editImgPosition">
                                                        <img src="../Styles/images/side_edit.png" /></a></h4>
                                            </div>
                                            <!-- panel-heading -->
                                            <div class="panel-body">
                                                <uEduCtrlTest:UCEduCtrlTest ID="EducationCtl" runat="server" />
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <!-- panel-btns -->
                                                <h4 class="panel-title">
                                                    <i></i><a class="accordion-toggle">Training </a><a href="~/newhr/EducationTraining.aspx?id="
                                                        runat="server" id="skill10" class="editImgPosition">
                                                        <img src="../Styles/images/side_edit.png" /></a></h4>
                                            </div>
                                            <!-- panel-heading -->
                                            <div class="panel-body">
                                                <uUTrainingCtrl:UCTrainingCtl ID="TrainingCtl1" runat="server" />
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <!-- panel-btns -->
                                                <h4 class="panel-title">
                                                    <i></i><a class="accordion-toggle">Seminar </a><a href="~/newhr/EducationTraining.aspx?id="
                                                        runat="server" id="skill11" class="editImgPosition">
                                                        <img src="../Styles/images/side_edit.png" /></a></h4>
                                            </div>
                                            <!-- panel-heading -->
                                            <div class="panel-body">
                                                <ucUSeminarCtrl:UUSeminarCtrl ID="SeminarCtl1" runat="server" />
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <!-- panel-btns -->
                                                <h4 class="panel-title">
                                                    <i></i><a class="accordion-toggle">Previous Employment </a><a href="~/newhr/PreviousEmployment.aspx?id="
                                                        runat="server" id="prevEmployment" class="editImgPosition">
                                                        <img src="../Styles/images/side_edit.png" /></a></h4>
                                            </div>
                                            <!-- panel-heading -->
                                            <div class="panel-body">
                                                <ucEmploymentCtrl:UEmploymentCtrl ID="ucEmploymentHistoryCtl1"
                                                    runat="server" />
                                            </div>
                                        </div>
                                    </Content>
                                </ext:Panel>
                                <ext:Panel ID="Panel4" Title="Skills and Languages" runat="server">
                                    <Content>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <!-- panel-btns -->
                                                <h4 class="panel-title">
                                                    <i></i><a class="accordion-toggle">Skill Set </a><a href="~/newhr/EducationTraining.aspx?id="
                                                        runat="server" id="skill12" class="editImgPosition">
                                                        <img src="../Styles/images/side_edit.png" /></a></h4>
                                            </div>
                                            <!-- panel-heading -->
                                            <div class="panel-body">
                                                <ucUSkillSetCtrl:UUSkillSetCtrl ID="SkillSetsCtl1" runat="server" />
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <!-- panel-btns -->
                                                <h4 class="panel-title">
                                                    <i></i><a class="accordion-toggle">Languages </a><a href="~/newhr/EducationTraining.aspx?id="
                                                        runat="server" id="skill13" class="editImgPosition">
                                                        <img src="../Styles/images/side_edit.png" /></a></h4>
                                            </div>
                                            <!-- panel-heading -->
                                            <div class="panel-body">
                                                <ucULanguageCtr:UULanguageCtrl ID="LanguageSetsCtl1" runat="server" />
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <!-- panel-btns -->
                                                <h4 class="panel-title">
                                                    <i></i><a class="accordion-toggle">Publication </a><a href="~/newhr/EducationTraining.aspx?id="
                                                        runat="server" id="skill14" class="editImgPosition">
                                                        <img src="../Styles/images/side_edit.png" /></a></h4>
                                            </div>
                                            <!-- panel-heading -->
                                            <div class="panel-body">
                                                <ucUPublicationCtrl:UUPublicationCtrl ID="PublicationCtl1" runat="server" />
                                            </div>
                                        </div>
                                    </Content>
                                </ext:Panel>
                                <ext:Panel ID="Panel5" Title="Health and Family" runat="server">
                                    <Content>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <!-- panel-btns -->
                                                <h4 class="panel-title">
                                                    <i></i><a class="accordion-toggle">Health </a><a href="~/newhr/health.aspx?id=" runat="server"
                                                        id="health" class="editImgPosition">
                                                        <img src="../Styles/images/side_edit.png" /></a></h4>
                                            </div>
                                            <!-- panel-heading -->
                                            <div class="panel-body">
                                                <ucHealthCtl:HealthCtl Id="ucHealthCtl1" runat="server" />
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <!-- panel-btns -->
                                                <h4 class="panel-title">
                                                    <i></i><a class="accordion-toggle">Family </a><a href="~/newhr/family.aspx?id=" runat="server"
                                                        id="family" class="editImgPosition">
                                                        <img src="../Styles/images/side_edit.png" /></a></h4>
                                            </div>
                                            <!-- panel-heading -->
                                            <div class="panel-body">
                                                <uUFamilyCtrl:UCFamilyCtrl ID="FamilyCtl1" runat="server" />
                                            </div>
                                        </div>
                                    </Content>
                                </ext:Panel>
                            </Items>
                        </ext:TabPanel>
                        <%--RBB like reward--%>
                        <div id="divRBBReward" class="panel panel-default" runat="server">
                            <div class="panel-heading">
                                <!-- panel-btns -->
                                <h4 class="panel-title">
                                    <i></i><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2"
                                        href="#collapse15">Reward </a>
                                </h4>
                            </div>
                            <!-- panel-heading -->
                            <div class="panel-body">
                                <uc1:RewardCtl IsDisplayMode="true" Id="FamilyCtl12" runat="server" />
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
