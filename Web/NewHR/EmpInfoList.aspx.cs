﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using Utils.Helper;
using System.IO;
using Utils;
using Utils.Security;
using Utils.Calendar;
using Web.Helper;
using System.Collections;


namespace Web.NewHR
{
    public partial class EmpInfoList : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.HumanResource;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack && !X.IsAjaxRequest)
                Initialise();

            JavascriptHelper.AttachNonDialogPopUpCode(Page, "EEmployeeInfoImport", "../ExcelWindow/EEmployeeInfo.aspx", 450, 500);
        }

        private void Initialise()
        {
            storeBranch.DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            storeBranch.DataBind();
        }

        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            int total = 0, pageSize = 0, employeeId = -1, branchId = -1;
            string employeeName = cmbSearch.Text;

            if (cmbSearch.SelectedItem != null && cmbSearch.SelectedItem.Value != null)
            {
                int.TryParse(cmbSearch.SelectedItem.Value, out employeeId);

                if (employeeId != 0)
                    employeeName = "";
                else
                    employeeId = -1;
            }

            if (cmbBranch.SelectedItem != null && cmbBranch.SelectedItem.Value != null)
                branchId = int.Parse(cmbBranch.SelectedItem.Value);

            if (cmbPageSize.SelectedItem != null && cmbPageSize.SelectedItem.Value != null)
                pageSize = int.Parse(cmbPageSize.SelectedItem.Value);



            List<GetEmployeeInfoResult> list = NewHRManager.GetEmployeeInfoResult(e.Page - 1, e.Limit, employeeId, employeeName, branchId);

            ////-- start filtering -----------------------------------------------------------
            FilterHeaderConditions fhc = new FilterHeaderConditions(e.Parameters["filterheader"]);

            foreach (FilterHeaderCondition condition in fhc.Conditions)
            {
                string dataIndex = condition.DataIndex;
                FilterType type = condition.Type;
                string op = condition.Operator;
                object value = null;

                switch (condition.Type)
                {
                    case FilterType.Boolean:
                        value = condition.Value<bool>();
                        break;

                    case FilterType.Date:
                        switch (condition.Operator)
                        {
                            case "=":
                                value = condition.Value<DateTime>();
                                break;

                            case "compare":
                                value = FilterHeaderComparator<DateTime>.Parse(condition.JsonValue);
                                break;
                        }
                        break;
                    case FilterType.String:
                        value = condition.Value<string>();
                        break;
                    case FilterType.Numeric:
                        value = condition.Value<int>();
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                list.RemoveAll(item =>
                {
                    object oValue = item.GetType().GetProperty(dataIndex).GetValue(item, null);
                    string matchValue = null;
                    string itemValue = null;

                    if (type == FilterType.String)
                    {
                        matchValue = (string)value;

                        if (dataIndex == "EmployeeId")
                            itemValue = oValue.ToString();
                        else
                            itemValue = oValue as string;

                        matchValue = matchValue.ToLower();
                        itemValue = (itemValue == null ? "" : itemValue.ToLower());
                    }

                    switch (op)
                    {
                        case "=":
                            return oValue == null || !oValue.Equals(value);
                        case "compare":
                            return !((IEquatable<IComparable>)value).Equals((IComparable)oValue);
                        case "+":
                            return itemValue == null || !itemValue.Contains(matchValue); //return itemValue == null || !itemValue.StartsWith(matchValue);
                        case "-":
                            return itemValue == null || !itemValue.EndsWith(matchValue);
                        case "!":
                            return itemValue == null || itemValue.IndexOf(matchValue) >= 0;
                        case "*":
                            return itemValue == null || itemValue.IndexOf(matchValue) < 0;
                        default:
                            throw new Exception("Not supported operator");
                    }
                });
            }
            //-- end filtering ------------------------------------------------------------

            //-- start sorting ------------------------------------------------------------

            if (e.Sort.Length > 0)
            {
                list.Sort(delegate (GetEmployeeInfoResult obj1, GetEmployeeInfoResult obj2)
                {
                    object a;
                    object b;

                    int direction = e.Sort[0].Direction == Ext.Net.SortDirection.DESC ? -1 : 1;

                    a = obj1.GetType().GetProperty(e.Sort[0].Property).GetValue(obj1, null);
                    b = obj2.GetType().GetProperty(e.Sort[0].Property).GetValue(obj2, null);
                    return CaseInsensitiveComparer.Default.Compare(a, b) * direction;
                });
            }

            //Session["ExportEmpInfo"] = list;

            storeEmpInfo.DataSource = list;

            if (list.Count > 0)
                total = list[0].TotalRows.Value;

            e.Total = total;
        }

        protected Field OnCreateFilterableField(object sender, ColumnBase column, Field defaultField)
        {
            ((TextField)defaultField).Icon = Icon.Magnifier;

            return defaultField;
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            //if (Session["ExportEmpInfo"] == null)
            //{
            //    NewMessage.ShowWarningMessage("Please reload the data for export.");
            //    return;
            //}

            //List<GetEmployeeInfoResult> list = (List<GetEmployeeInfoResult>)Session["ExportEmpInfo"];

            //Session["ExportEmpInfo"] = null;

            int pageSize = 0, employeeId = -1, branchId = -1;
            string employeeName = cmbSearch.Text;

            if (cmbSearch.SelectedItem != null && cmbSearch.SelectedItem.Value != null)
            {
                int.TryParse(cmbSearch.SelectedItem.Value, out employeeId);

                if (employeeId != 0)
                    employeeName = "";
                else
                    employeeId = -1;
            }

            if (cmbBranch.SelectedItem != null && cmbBranch.SelectedItem.Value != null)
                branchId = int.Parse(cmbBranch.SelectedItem.Value);

            if (cmbPageSize.SelectedItem != null && cmbPageSize.SelectedItem.Value != null)
                pageSize = int.Parse(cmbPageSize.SelectedItem.Value);

            List<GetEmployeeInfoResult> list = NewHRManager.GetEmployeeInfoResult(0, 9999999, employeeId, employeeName, branchId);



            string template = ("~/App_Data/ExcelTemplate/EmployeeInfoImport.xlsx");

            List<CountryList> listCountries = new CommonManager().GetAllCountries().ToList();
            List<HBloodGroup> listBlood = CommonManager.GetAllBloodGroup().ToList();
            List<HReligion> listReligion = CommonManager.GetAllReligionList().ToList();
            List<DistrictList> listDistict = new CommonManager().GetAllDisticts().ToList();
            List<CitizenNationality> listNationality = new CommonManager().GetAllNationality().ToList();

            ExcelGenerator.ExportEmployeeInfo(template, list, listCountries, listBlood, listReligion, listDistict, listNationality);


            //List<string> hiddenColumnList = new List<string>();
            //hiddenColumnList = new List<string> { "TotalRows", "RowNumber", "IssueDateEng", "IssuingDateEng", "ValidUptoEng", "BranchId","MarriageAniversaryEng" };

            //Bll.ExcelHelper.ExportToExcel
            // ("Employee Information",
            //    list,
            //    hiddenColumnList,
            //    new List<String>() { },
            //    new Dictionary<string, string>()
            //    {
            //        { "EmployeeId","EIN"},
            //        { "IdCardNo","INO"},
            //        { "Name","Name"},
            //        { "BloodGroupName", "Blood Group" },
            //        { "Nationality", "Nationality"},
            //        { "CitizenshipNo", "Citizenship No."},
            //        { "IssueDate","Citizenship Issue Date"},
            //        { "Place","Citizenship Issue Place"},
            //        { "LiscenceTypeName","License Type"},
            //        { "DrivingLicenceNo", "License No." },
            //        { "DrivingLicenceIssueDate", "License Issue Date"},
            //        { "IssuingCountry", "License Issue Country"},
            //        { "PassportNo","Passport No."},
            //        { "IssuingDate","Passport Issue Date"},
            //        { "ValidUpto","Passport Valid Upto"},
            //        { "Birthmark", "Birthmark" },
            //        { "MarriageAniversary", "Marriage Anneversary"},
            //        { "ReligionName","Religion"},
            //        { "EthnicityName","Ethnicity"},
            //        { "MotherTongue", "MotherTongue"}

            //    },
            //   new List<string>() { }
            //   , new List<string> { }
            //   , new List<string> {  }
            //   , new Dictionary<string, string>() { }
            //   , new List<string> { });



        }

    }
}