﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Manager;
using BLL;
using Utils.Helper;
using System.Xml;
using DAL;
using Ext.Net;
using System.Xml.Linq;
using BLL.Base;

namespace Web.NewHR
{
    public partial class AllSettings : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.AdministratorTools;
            }
        }
        //to differtiate custom role user
        public bool isCustomRole = false;
        public string[] pagesPerimissible = null;
        private const int totlaFavItem = 12;
        private const int itemGroupType = 1;

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                ProcessAsPerRole();
                InsertFavMenuXmlContent();               
                SetFavButtonStatus();
                this.DataBind();

                trLeaveFareSettings.Visible = CommonManager.CompanySetting.WhichCompany == WhichCompany.Prabhu
                    || CommonManager.CompanySetting.WhichCompany == WhichCompany.Janata
                    || CommonManager.CompanySetting.WhichCompany == WhichCompany.Civil
                    || CommonManager.CompanySetting.WhichCompany == WhichCompany.MBL;

                if (CommonManager.CompanySetting.WhichCompany != WhichCompany.Mega)
                    mgAllowance.Visible = false;
                // As only one will be needed from choosen setting
                if (CommonManager.Setting.LeaveRequestSettingUsingPreDefinedList != null
                    && CommonManager.Setting.LeaveRequestSettingUsingPreDefinedList.Value)
                {
                    rowLeaveSettingByTeam.Visible = false;
                }
                else
                    rowLeaveSettingByDefinedType.Visible = false;

                //if (CommonManager.CompanySetting.WhichCompany != WhichCompany.NIBL)
                    AwardList.Visible = false;


                rowLeaveSpecificIndividualLeaveAuthority.Visible =
                    CommonManager.Setting.LeaveRequestSettngUsingOneToOne != null && CommonManager.Setting.LeaveRequestSettngUsingOneToOne.Value;

            }

            ////no compnay exists so don't display menus
            //if (SessionManager.CurrentCompanyId == 0)
            //{
            //}
            //else
            //{

               
            //    LoadMenus();
                
            
            
            //}

           
        

        }


        public static void InsertFavMenuXmlContent()
        {
          
        }


        protected void FavouriteItemBtn_Click(object sender, DirectEventArgs e)
        {
            Ext.Net.ImageButton btn = (Ext.Net.ImageButton)sender as Ext.Net.ImageButton;
            String itemId = btn.ID;

            if (btn.Pressed == false)
            {
                if (CommonManager.TotalFavouriteItem(itemGroupType) < totlaFavItem)
                {
                    CommonManager.UpdateFavMenuStatus(itemId, itemGroupType);
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(System.Web.UI.Page),"MyFavouriteItemStat", "MyFavouriteItemStat();", true);
                    return;
                }
            }
            else
            {
                CommonManager.UpdateFavMenuStatus(itemId, itemGroupType);
            }

            
            
            
            MyCache.ResetUserCache(SessionManager.UserName);
            MyCache.DeleteFromGlobalCache("Menu" + SessionManager.User.UserID.ToString());

        }


        protected void FavItemRestButton_Click(object sender, DirectEventArgs e)
        {
            CommonManager.ResetFavMenuItem(itemGroupType);
            MyCache.ResetUserCache(SessionManager.UserName);
            MyCache.DeleteFromGlobalCache("Menu" + SessionManager.User.UserID.ToString());  


        }
        void SetFavButtonStatus()
        {
            
        }

        public XmlDocument ProcessAsPerRole()
        {
            if (Page.User.IsInRole(Role.Administrator.ToString()))
            {
                return MyCache.GetMenuXml(SessionManager.User.UserID.ToString(), CommonManager.CompanySetting.HasLevelGradeSalary);
            }
            else if (Page.User.IsInRole(Role.Employee.ToString()))
            {
                return null;
            }
            else if (SessionManager.CurrentCompanyId == -1) //if db connection gives error then this will hapeen
            {
                return MyCache.GetMenuXml(SessionManager.User.UserID.ToString(), CommonManager.CompanySetting.HasLevelGradeSalary);
            }
            else
            {
                //if (Page.User.Identity.IsAuthenticated == false)
                //    return null;

                //isCustomRole = true;
                //XmlDocument doc = MyCache.GetMenuXml(SessionManager.User.UserID.ToString());
                //UUser user = UserManager.GetUserByUserName(Page.User.Identity.Name);
                //pagesPerimissible = UserManager.GetAccessiblePagesForRole(user.RoleId.Value);

                ////int currentPageId = UserManager.GetPageIdByPageUrl(currentPage);
                //////current page not accessible so do redirection
                ////if (!pagesPerimissible.Contains(currentPageId) && currentPage != "changepassword.aspx" && currentPage != "payrollmessage.aspx")
                ////{
                ////    if (Request.UrlReferrer != null)
                ////        Response.Redirect(Request.UrlReferrer.ToString());
                ////    else
                ////    {
                ////        if (Request.Url.AbsoluteUri.ToString().ToLower().Contains("/employee/"))
                ////            Response.Redirect("~/Employee/Default.aspx");
                ////        else
                ////            Response.Redirect("~/Default.aspx");
                ////    }
                ////}
                ////return doc;
                return null;
            }

        }
        private void LoadMenus()
        {

            XmlDocument doc = ProcessAsPerRole();


          
        }

        protected void Initialise()
        {
          
        }

        public new bool IsAccessible(string url)
        {
            return UserManager.IsPageAccessible(url);

        }
       

    }
}
