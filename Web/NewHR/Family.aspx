﻿<%@ Page Title="Family Setting" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="Family.aspx.cs" Inherits="Web.NewHR.Family" %>

<%@ Register Src="~/newhr/UserControls/EmployeeWizard.ascx" TagName="EmployeeWizard"
    TagPrefix="uc2" %>
<%@ Register Src="~/NewHR/UserControls/FamilyCtl.ascx" TagName="FamilyCtl" TagPrefix="uc1" %>
<%@ Register Src="~/NewHR/UserControls/EmployeeDetailsCtl.ascx" TagName="EmployeeDetailsCtl"
    TagPrefix="uc1" %>
<%@ Register Src="~/NewHR/UserControls/UFamilyCtrl.ascx" TagName="UFamilyCtrl" TagPrefix="ucF" %>
<%@ Register Src="~/NewHR/UserControls/EmpDetailsLeftMenu.ascx" TagName="EmpDetailsWizard"
    TagPrefix="ucEW" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

<style type="text/css">
    .userDetailsClass
        {
            background-color:#D8E7F3; 
            height:50px; text-align:center; 
            padding-top:10px; 
            margin-left:8px;
            margin-bottom:-10px;
        }
</style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <%--<ul class="breadcrumb">
        <li><a href="index.html?lang=en" class="glyphicons home"><i></i>Smashing</a></li>
        <li class="divider"></li>
        <li>Bookings</li>
    </ul>--%>

    <div class="pageheader">
        <div class="media">
            <div class="media-body" style="margin-left:140px;">
                <h4>
                    Family
                </h4>
            </div>
        </div>
    </div>


    <div class="contentpanel">
        <div class="separator bottom">
        </div>
        <div class="innerLR">
          
            <%--<uc2:EmployeeWizard Id="EmployeeWizard1" runat="server" />--%>
            <table>
                <tr>
                    <td valign="top">
                        <div style="float: left; margin-right: 10px;">
                            <ucEW:EmpDetailsWizard Id="EmployeeWizard1" runat="server" />
                        </div>
                    </td>
                    <td valign="top" style="width:100%;">
                         <div class="userDetailsClass">
                                <uc1:EmployeeDetailsCtl Id="EmployeeDetailsCtl1" runat="server" />
                              </div>

                        <div style="width: 795px">                      
                        
                            <div class="panel-body" style="width: 815px; margin-left:-10px;">
                                <%-- <uc1:FamilyCtl Id="EmployeeDetailsCtl2" runat="server" />--%>
                                <ucF:UFamilyCtrl Id="UFamilyCtrl" runat="server" />
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <br />
</asp:Content>
