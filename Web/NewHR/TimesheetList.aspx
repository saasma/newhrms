﻿<%@ Page Title="Time Sheet List" Language="C#" AutoEventWireup="true" CodeBehind="TimesheetList.aspx.cs"
    MasterPageFile="~/Master/NewDetails.Master" Inherits="Web.NewHR.TimesheetList" %>

<%@ Register Src="~/Employee/Timesheet/UserControl/TimesheetViewCtl.ascx" TagName="TimesheetCtl"
    TagPrefix="uc4" %>
<%@ Register Src="~/Controls/FooterCtl.ascx" TagName="FooterCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/HeaderCtl.ascx" TagName="HeaderCtl" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <ext:ResourcePlaceHolder ID="ResourcePlaceHolder1" runat="server" Mode="Script" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <script type="text/javascript">
  


    var CommandHandler = function (command, record) {
            <%= hdnTimeSheetId.ClientID %>.setValue(record.data.TimesheetId);
            if(command == 'Reject')
            { 
                 <%= hdnWeekNo.ClientID %>.setValue(record.data.WeekNo);
                 <%= btnReject.ClientID %>.fireEvent('click');
            }           
             if(command == 'View')
            { 
                <%= btnView.ClientID %>.fireEvent('click');
              //window.location = 'ViewTimesheet.aspx?id=' + record.data.TimesheetId; 
            } 

              if(command == 'Delete')
            { 
                <%= btnDeleteDraft.ClientID %>.fireEvent('click');
              //window.location = 'ViewTimesheet.aspx?id=' + record.data.TimesheetId; 
            } 
              if(command == 'Export')
            { 
                
                 <%= hdnEmployeeID.ClientID %>.setValue(record.data.EmployeeId);
                  
                 <%= hdnStartDate.ClientID %>.setValue(record.data.StartDate);

               __doPostBack('Reload', 'Reload');
                 

            } 
            
        };


        
         var fnPrepareToolbarShowHideCommand = function (grid, toolbar, rowIndex, record) {
         if(record.data.StatusText!="")
         {
         
            //-Notify 0
            //-Approve 1
            //Reset 2
            //Edit 3
            //Send Back 4
            //if Draft or Not Filled or Rejected - Show Notify,Edit   if Awaiting Approval then show Approve SendBack Reset Edit 
            // if Approved show Reset,Edit

             if(record.data.StatusText=="Draft" ||record.data.StatusText=="Not Filled" || record.data.StatusText=="Rejected" ){
             var btn = toolbar.items.get(0);
             btn.menu.items.get(0).show();

             btn.menu.items.get(3).show();

                }

             if(record.data.StatusText=="Awaiting Approval"){
             var btn = toolbar.items.get(0);
             btn.menu.items.get(1).show();

             
             btn.menu.items.get(2).show();

             
             btn.menu.items.get(3).show();

             
             btn.menu.items.get(4).show();
                }


                 if(record.data.StatusText=="Approved"  ){
             var btn = toolbar.items.get(0);
             btn.menu.items.get(2).show();

             btn = toolbar.items.get(3);
             btn.menu.items.get(3).show();

                }


                }
             };


                  function processBefore(btnId) {
     var startDate =  <%=txtStartDate.ClientID%>.getValue();
     
      <%=hdnStartDate.ClientID%>.setValue(startDate);
      
}




     
     

    function searchList() {
            <%=PagingToolbar1.ClientID %>.doRefresh();
        }
           

           var prepareCommand = function (grid, toolbar, rowIndex, record) {
            if(record.data.Status ==-1) {
                toolbar.hide();
            }

        };

    </script>
    <ext:Hidden ID="hdnTimeSheetId" runat="server" />
    <ext:Hidden ID="hdnEmployeeID" runat="server" />

    <ext:Hidden ID="hdnStartDate" runat="server" />
    <ext:Hidden ID="hdnWeekNo" runat="server" />
    <ext:LinkButton ID="btnReject" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnReject_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
     <ext:LinkButton ID="btnDeleteDraft" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnDeleteDraft_Click">
             <EventMask ShowMask="true" />
              <Confirmation Message="Are you sure, you want to delete?" ConfirmRequest="true">
                                                    </Confirmation>
               
            </Click>
        </DirectEvents>
    </ext:LinkButton>

    

    <%--  <ext:LinkButton ID="btnExport" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnExport_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    --%>
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4 id="header" runat="server">
                    Timesheet List</h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="alert alert-info">
            <table class="fieldTable">
                <tr>
                    <td style="width: 30px;">
                        Date
                    </td>
                    <td>
                        <ext:DateField Width="120px" ID="txtStartDate" runat="server" LabelSeparator="">
                        </ext:DateField>
                        <asp:RequiredFieldValidator Display="None" ID="val1" runat="server" ValidationGroup="LoadTimeSheet"
                            ControlToValidate="txtStartDate" ErrorMessage="Start Date is required." />
                    </td>
                    <td>
                        <ext:DateField Width="120px" ID="txtEndDate" runat="server" LabelSeparator="">
                        </ext:DateField>
                        <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator1" runat="server"
                            ValidationGroup="LoadTimeSheet" ControlToValidate="txtEndDate" ErrorMessage="End Date is required." />
                    </td>
                    <td style="width: 10px;">
                    </td>
                    <td>
                        <ext:ComboBox Width="180" ForceSelection="true" LabelWidth="45" ID="cmbStatus" runat="server"
                            FieldLabel="Status" LabelSeparator="">
                            <Items>
                                <ext:ListItem Text="All" Value="-2" />
                                <ext:ListItem Text="Draft" Value="0" />
                                <ext:ListItem Text="Awaiting Approval" Value="1" />
                                <ext:ListItem Text="Approved" Value="2" />
                                <ext:ListItem Text="Rejected" Value="10" />
                                <ext:ListItem Text="Not Filled" Value="-1" />
                            </Items>
                            <SelectedItems>
                                <ext:ListItem Index="0" />
                            </SelectedItems>
                        </ext:ComboBox>
                    </td>
                    <td style="width: 60px; padding-left: 10px">
                        Employee
                    </td>
                    <td style="width: 165px;">
                        <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                            <Proxy>
                                 <ext:AjaxProxy Url="../Handler/EmpSearchID.ashx">
                                    <ActionMethods Read="GET" />
                                    <ExtraParams>
                                        <ext:StoreParameter Name="RetiredAlso" Value="true" />
                                    </ExtraParams>
                                    <Reader>
                                        <ext:JsonReader Root="plants" TotalProperty="total" />
                                    </Reader>
                                </ext:AjaxProxy>
                            </Proxy>
                            <Model>
                                <ext:Model ID="Model2" IDProperty="Name" Name="ItemLineModel" runat="server">
                                    <Fields>
                                        <ext:ModelField Name="Name" Type="String" />
                                        <ext:ModelField Name="EmployeeId" Type="String" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                        <ext:ComboBox LabelSeparator="" ID="cmbSearch" LabelWidth="70" runat="server" DisplayField="Name"
                            ValueField="EmployeeId" StoreID="storeSearch" TypeAhead="false" Width="180" HideBaseTrigger="true"
                            MinChars="1" TriggerAction="All" ForceSelection="true">
                            <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                                <ItemTpl ID="ItemTpl1" runat="server">
                                    <Html>
                                        <tpl>
                                            <div class="search-item">
                                                            <span>{Name}</span>  
                                             </div>
					                </tpl>
                                    </Html>
                                </ItemTpl>
                            </ListConfig>
                            <Triggers>
                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                            </Triggers>
                            <Listeners>
                                <Select Handler="this.getTrigger(0).show();" />
                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                            </Listeners>
                        </ext:ComboBox>
                    </td>
                    <td style="width: 20px">
                    </td>
                    <td>
                        <ext:Button runat="server" Width="100" Height="30" ID="btnLoad" Cls="btn btn-default btn-sm btn-sect"
                            Text="Load" OnClientClick="searchList();">
                            <Listeners>
                                <Click Handler="valGroup = 'LoadTimeSheet'; return CheckValidation();">
                                </Click>
                            </Listeners>
                        </ext:Button>
                    </td>
                    <td>
                        <%--                    <ext:ComboBox Width="180" ForceSelection="true" LabelWidth="45" ID="cmbAction" runat="server" 
                            FieldLabel="Action" LabelSeparator="">
                            <Items>
                                <ext:ListItem Text="Export" Value="0" />
                                <ext:ListItem Text="Send Mail" Value="1" />
                                <ext:ListItem Text="Approve" Value="2" />
                            </Items>
                           <Listeners>
                    <Select Fn="processBeforeExport" />
                </Listeners>
                            <DirectEvents>
                                <Select OnEvent="cmbAction_Change">
                                </Select>
                                
                            </DirectEvents>
                        </ext:ComboBox>--%>
                        <ext:Button ID="Button1" Cls="btn btn-warning btn-sm btn-sect" runat="server" Text="Options"
                            Height="30">
                            <Menu>
                                <ext:Menu ID="Menu2" runat="server" ShowSeparator="false" Cls="dropdownbtn">
                                    <Items>
                                        <%-- <ext:MenuItem OnDirectClick="btnExcelPrint_Click" runat="server" Text="Excel" ID="btnExcelPrint">
                                            <Listeners>
                                                <Click Fn="processBefore" />
                                            </Listeners>
                                        </ext:MenuItem>--%>
                                        <ext:MenuItem runat="server" Text="Send Mail" ID="MenuItem1">
                                            <DirectEvents>
                                                <Click OnEvent="btnSendMail">
                                                    <EventMask ShowMask="true" />
                                                    <Confirmation Message="Are you sure, you want to Send Mail?" ConfirmRequest="true">
                                                    </Confirmation>
                                                </Click>
                                            </DirectEvents>
                                            <Listeners>
                                                <Click Fn="processBefore" />
                                            </Listeners>
                                        </ext:MenuItem>
                                         <ext:MenuItem runat="server" Text="Set as Draft" ID="btnSetAsDraft1">
                                            <DirectEvents>
                                                <Click OnEvent="btnSetAsDraft">
                                                    <EventMask ShowMask="true" />
                                                    <Confirmation Message="Are you sure, you want to set to Draft status?" ConfirmRequest="true">
                                                    </Confirmation>
                                                </Click>
                                            </DirectEvents>
                                            <Listeners>
                                                <Click Fn="processBefore" />
                                            </Listeners>
                                        </ext:MenuItem>

                                       <%--  <ext:MenuItem runat="server" Text="Approve" ID="btnSetApprove">
                                            <DirectEvents>
                                                <Click OnEvent="btnSetApprove_Click">
                                                    <EventMask ShowMask="true" />
                                                    <Confirmation Message="Are you sure, you want to Approve selected Time Sheet?" ConfirmRequest="true">
                                                    </Confirmation>
                                                </Click>
                                            </DirectEvents>
                                            <Listeners>
                                                <Click Fn="processBefore" />
                                            </Listeners>
                                        </ext:MenuItem>--%>
                                    </Items>
                                </ext:Menu>
                            </Menu>
                        </ext:Button>
                    </td>
                </tr>
            </table>
        </div>
        <table>
            <tr>
                <td>
                    <ext:GridPanel ID="gridTimeSheet" runat="server" Cls="itemgrid" Width="1200" Scroll="None">
                        <Store>
                            <ext:Store ID="Store3" runat="server" AutoLoad="true" PageSize="50">
                                <Proxy>
                                    <ext:AjaxProxy Json="true" Url="../Handler/TimeSheetList.ashx">
                                        <ActionMethods Read="GET" />
                                        <Reader>
                                            <ext:JsonReader Root="data" TotalProperty="total" />
                                        </Reader>
                                    </ext:AjaxProxy>
                                </Proxy>
                                <AutoLoadParams>
                                    <ext:Parameter Name="start" Value="0" Mode="Raw" />
                                    <ext:Parameter Name="IsEmpView" Value="-1" Mode="Raw" />
                                </AutoLoadParams>
                                <Parameters>
                                    <ext:StoreParameter Name="EmployeeId" Value="#{cmbSearch}.getValue()" Mode="Raw"
                                        ApplyMode="Always" />
                                    <ext:StoreParameter Name="StartDate" Value="#{txtStartDate}.getValue()" Mode="Raw"
                                        ApplyMode="Always" />
                                    <ext:StoreParameter Name="EndDate" Value="#{txtEndDate}.getValue()" Mode="Raw" ApplyMode="Always" />
                                    <ext:StoreParameter Name="status" Value="#{cmbStatus}.getValue()" Mode="Raw" />
                                </Parameters>
                                <Model>
                                    <ext:Model ID="Model1" runat="server" IDProperty="TimeSheetIDEmployeeID">
                                        <Fields>
                                            <ext:ModelField Name="TimesheetId" />
                                            <ext:ModelField Name="EmployeeId" />
                                            <ext:ModelField Name="IdCardNo" />
                                            <ext:ModelField Name="Name" />
                                            <ext:ModelField Name="WeekNo" />
                                            <ext:ModelField Name="StartDate" />
                                            <ext:ModelField Name="CreatedByName" />
                                            <ext:ModelField Name="ApprovedByName" />
                                            <ext:ModelField Name="ApprovedOn" Type="Date" />
                                            <ext:ModelField Name="TotalHours" />
                                            <ext:ModelField Name="StatusText" />
                                            <ext:ModelField Name="Status" Type="Int" />
                                            <ext:ModelField Name="Month" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel ID="ColumnModel2" runat="server">
                            <Columns>
                                <ext:Column ID="colEId" runat="server" Text="EIN" Width="50" DataIndex="EmployeeId"
                                    Align="Center" Sortable="false" MenuDisabled="true">
                                </ext:Column>
                                <ext:Column ID="colIDCardNo" runat="server" Text="I No" Width="60" DataIndex="IdCardNo"
                                    Sortable="false" Align="Center" MenuDisabled="true">
                                </ext:Column>
                                <ext:Column ID="Column1" runat="server" Text="Employee Name" Width="180" DataIndex="Name"
                                    Sortable="false" MenuDisabled="true">
                                </ext:Column>
                                <ext:Column ID="Column2" runat="server" Text="Month" Width="120" DataIndex="Month"
                                    Align="Left" Sortable="false" MenuDisabled="true">
                                </ext:Column>
                                <ext:Column ID="Column4" runat="server" Align="Center" Text="Hours" Width="60" DataIndex="TotalHours"
                                    Sortable="false" MenuDisabled="true">
                                </ext:Column>
                                <ext:Column ID="Column6" runat="server" Text="Created By" Width="180" DataIndex="CreatedByName"
                                    Sortable="false" MenuDisabled="true">
                                </ext:Column>
                                <ext:Column ID="Column5" runat="server" Text="Approved By" Width="150" DataIndex="ApprovedByName"
                                    Sortable="false" MenuDisabled="true">
                                </ext:Column>
                                <ext:DateColumn ID="colApprovedDate" runat="server" Text="Approved Date" Width="110"
                                    DataIndex="ApprovedOn" Sortable="false" MenuDisabled="true" Format="MM/dd/Y" />
                                <ext:Column ID="Column3" runat="server" Text="Status" Width="90" DataIndex="StatusText"
                                    Sortable="false" MenuDisabled="true">
                                </ext:Column>
                                <ext:CommandColumn ID="ContextCommand" runat="server" Width="40" OverOnly="false"
                                    Hidden="true">
                                    <PrepareToolbar Fn="fnPrepareToolbarShowHideCommand">
                                    </PrepareToolbar>
                                    <Commands>
                                        <ext:GridCommand Icon="ArrowDown">
                                            <Menu EnableScrolling="false">
                                                <Items>
                                                    <ext:MenuCommand Text="Notify" Icon="BulletBlue" CommandName="Notify" Hidden="true" />
                                                    <ext:MenuCommand Text="Approve" Icon="Delete" CommandName="Approve" Hidden="true" />
                                                    <ext:MenuCommand Text="Reset" Icon="ApplicationCascade" CommandName="Reset" Hidden="true" />
                                                    <ext:MenuCommand Text="Edit" Icon="Basket" CommandName="Edit" Hidden="true" />
                                                    <ext:MenuCommand Text="Send Back" Icon="Basket" CommandName="SendBack" Hidden="true" />
                                                </Items>
                                            </Menu>
                                            <ToolTip Text="Action" />
                                        </ext:GridCommand>
                                    </Commands>
                                    <Listeners>
                                        <Command Handler="CommandHandlerContextMenu(command,record);" />
                                    </Listeners>
                                </ext:CommandColumn>
                                <ext:CommandColumn ID="CommandColumn1" runat="server" Width="60" Sortable="false"
                                    MenuDisabled="true" Text="" Align="Center">
                                    <Commands>
                                        <ext:GridCommand Text="<i></i>View" CommandName="View" ToolTip-Text="View" />
                                    </Commands>
                                    <PrepareToolbar Fn="prepareCommand" />
                                    <Listeners>
                                        <Command Handler="CommandHandler(command,record);" />
                                    </Listeners>
                                </ext:CommandColumn>

                                           <ext:CommandColumn ID="CommandColumn3" runat="server" Width="60" Sortable="false"
                                    MenuDisabled="true" Text="" Align="Center">
                                    <Commands>
                                        <ext:GridCommand Text="<i></i>Delete" CommandName="Delete" ToolTip-Text="Delete" />
                                    </Commands>
                                    <PrepareToolbar Fn="prepareCommand" />
                                    <Listeners>
                                        <Command Handler="CommandHandler(command,record);" />
                                    </Listeners>
                                </ext:CommandColumn>

                                <ext:CommandColumn ID="CommandColumn2" runat="server" Width="60" Sortable="false"
                                    MenuDisabled="true" Text="" Align="Center">
                                    <Commands>
                                        <ext:GridCommand Text="<i></i>Export" CommandName="Export" ToolTip-Text="Export" />
                                    </Commands>
                                    <PrepareToolbar Fn="prepareCommand" />
                                    <Listeners>
                                        <Command Handler="CommandHandler(command,record);" />
                                    </Listeners>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <SelectionModel>
                            <ext:CheckboxSelectionModel ID="CheckboxSelectionModel1" runat="server" Mode="Multi" />
                        </SelectionModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar1" runat="server" PageSize="50" DisplayInfo="true"
                                DisplayMsg="Displaying Time Sheet {0} - {1} of {2}" EmptyMsg="No Records to display">
                            </ext:PagingToolbar>
                        </BottomBar>
                    </ext:GridPanel>
                </td>
            </tr>
        </table>
        <div class="buttonBlock" style="width: 68%;">
            <ext:Button ID="btnApprove" runat="server" Text="Approve" Width="100" Height="30"
                Hidden="true">
                <DirectEvents>
                    <Click OnEvent="btnApprove_Click">
                        <EventMask ShowMask="true" />
                        <Confirmation ConfirmRequest="true" Message="Are you sure, you want to approve the timesheets?" />
                        <ExtraParams>
                            <ext:Parameter Name="gridItems" Value="Ext.encode(#{gridTimeSheet}.getRowsValues({selectedOnly:true}))"
                                Mode="Raw" />
                        </ExtraParams>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <br />
        </div>
        <br />
    </div>
    <ext:LinkButton ID="btnView" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnView_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:Window ID="windowTimesheetDetails" Width="950" AutoScroll="true" Height="500"
        BodyPadding="5" runat="server" Hidden="true" Modal="true">
        <Content>
            <div style="margin-top: 20px">
            </div>
            <uc4:TimesheetCtl Id="TimesheetCtl1" runat="server" />
            <div style="margin-top: 10px; margin-left: 0px">
                <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="LinkButton1" Cls="btnFlatLeftGap"
                    Text="<i></i>Cancel">
                    <Listeners>
                        <Click Handler="#{windowTimesheetDetails}.hide();">
                        </Click>
                    </Listeners>
                </ext:LinkButton>
            </div>
        </Content>
    </ext:Window>
    <ext:Window ID="WTimeSheet" runat="server" Title="Time Sheet Details" Icon="Application"
        Width="550" Height="275" BodyPadding="5" Hidden="true" Modal="true">
        <Content>
            <table class="fieldTable">
                <tr>
                    <td>
                        <ext:DisplayField Width="300px" ID="txtEmployeeName" runat="server" FieldLabel="Employee Name"
                            ReadOnly="true" LabelAlign="Left" />
                    </td>
                    <td>
                        <ext:DisplayField Width="200px" ID="txtWeekNo" runat="server" FieldLabel="Week No"
                            ReadOnly="true" LabelAlign="Left" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:DisplayField Width="300px" LabelAlign="Left" runat="server" FieldLabel="Week Date"
                            ID="txtWeekDate" ReadOnly="true" />
                    </td>
                    <td>
                        <ext:DisplayField Width="200px" ID="txtTotalHours" runat="server" FieldLabel="Total Hours"
                            ReadOnly="true" LabelAlign="Left" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <ext:TextArea ID="txtNotes" runat="server" FieldLabel="Notes" LabelSeparator="" LabelAlign="Left"
                            Rows="4" Cols="55" />
                    </td>
                </tr>
                <tr>
                    <td valign="bottom" colspan="2">
                        <div class="popupButtonDiv">
                            <table>
                                <tr>
                                    <td>
                                        <ext:Button runat="server" ID="btnRejectSave" Height="30" Width="100" Text="Reject">
                                            <DirectEvents>
                                                <Click OnEvent="btnRejectSave_Click">
                                                    <EventMask ShowMask="true" />
                                                    <Confirmation Message="Are you sure, you want to reject the time sheet?" ConfirmRequest="true">
                                                    </Confirmation>
                                                </Click>
                                            </DirectEvents>
                                        </ext:Button>
                                    </td>
                                    <td style="padding-left: 10px; padding-right: 10px">
                                        <div class="btnFlatOr">
                                            or</div>
                                    </td>
                                    <td>
                                        <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="LinkButton2" Cls="btnFlatLeftGap"
                                            Text="<i></i>Cancel">
                                            <Listeners>
                                                <Click Handler="#{WTimeSheet}.hide();">
                                                </Click>
                                            </Listeners>
                                        </ext:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
        </Content>
    </ext:Window>
</asp:Content>
