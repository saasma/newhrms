﻿<%@ Page Title="Employee Count" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="EmployeeCountReport.aspx.cs" Inherits="Web.NewHR.EmployeeCountReport" %>

<%@ Register Src="~/newhr/UserControls/EmployeeContractTabStrip.ascx" TagName="EmployeeContractTabStripCtl"
    TagPrefix="ucEmployeeContractTabStrip" %>
<%@ Register Src="~/NewHR/UserControls/ActingListCtl.ascx" TagName="FamilyCtl" TagPrefix="uc1" %>
<%@ Register Src="~/NewHR/UserControls/EmployeeDetailsCtl.ascx" TagName="EmployeeDetailsCtl"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function focusEvent(e1, tab) {

            <%=hdnFilterValue.ClientID %>.setValue(tab.title.toString());
            <%=btnLoad.ClientID %>.fireEvent('click');

        };

        var cmbBranch = null;
        var cmbDepartment = null;
        var cmbLevel = null;
        var cmbDesignation = null;

     

        var rendername = function(e1,e2,e3)
        {
            var cmbBranch = <%= cmbBranch.ClientID %>;
              var    cmbDepartment = <%= cmbDepartment.ClientID %>;
              var     cmbLevel = <%= cmbLevel.ClientID %>;
               var     cmbDesignation = <%= cmbDesignation.ClientID %>;

            return "<a href='EmployeeCountReportDetails.aspx?type=" + <%=hdnFilterValue.ClientID %>.getValue()+ "&value=" + e1 
            + "&branchid=" + (cmbBranch.getValue() == null ? "-1" : cmbBranch.getValue())
            + "&depid=" + (cmbDepartment.getValue() == null ? "-1" : cmbDepartment.getValue())
            + "&levelid=" + (cmbLevel.getValue() == null ? "-1" : cmbLevel.getValue())
            + "&desigid=" + (cmbDesignation.getValue() == null ? "-1" : cmbDesignation.getValue())
            + "'>" + e1 + "</a>";
        }
    </script>
    <style type="text/css">
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <ext:Hidden ID="hdnFilterValue" runat="server" Text="Position">
    </ext:Hidden>
    <ext:Button ID="btnLoad" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnLoad_Change">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>
    <div class="separator bottom">
    </div>
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Employee Count
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel" style="padding-top: 10px">
        <div class="alert alert-info" style="margin-top: 10px; padding-top: 0px!important;">
            <ext:Container ID="listingContainer" runat="server">
                <Content>
                    <table>
                        <tr>
                            <td>
                                <ext:ComboBox Width="140" MatchFieldWidth="false" MarginSpec="0 5 0 5" LabelSeparator=""
                                    LabelAlign="Top" QueryMode="Local" ID="cmbBranch" DisplayField="Name" ValueField="BranchId"
                                    runat="server" FieldLabel="Branch">
                                    <Items>
                                        <ext:ListItem Index="0" Value="-1" Text="All">
                                        </ext:ListItem>
                                    </Items>
                                    <Store>
                                        <ext:Store ID="Store5" runat="server">
                                            <Model>
                                                <ext:Model ID="Model7" IDProperty="BranchId" runat="server">
                                                    <Fields>
                                                        <ext:ModelField Name="BranchId" />
                                                        <ext:ModelField Name="Name" />
                                                    </Fields>
                                                </ext:Model>
                                            </Model>
                                        </ext:Store>
                                    </Store>
                                </ext:ComboBox>
                            </td>
                            <td>
                                <ext:ComboBox FieldLabel="Department" MatchFieldWidth="false" ID="cmbDepartment"
                                    Width="140" MarginSpec="0 5 0 5" runat="server" ValueField="DepartmentId" DisplayField="Name"
                                    LabelAlign="Top" LabelSeparator="" QueryMode="Local">
                                    <Items>
                                        <ext:ListItem Index="0" Value="-1" Text="All">
                                        </ext:ListItem>
                                    </Items>
                                    <Store>
                                        <ext:Store ID="Store6" runat="server">
                                            <Model>
                                                <ext:Model ID="Model8" IDProperty="DepartmentId" runat="server">
                                                    <Fields>
                                                        <ext:ModelField Name="DepartmentId" />
                                                        <ext:ModelField Name="Name" />
                                                    </Fields>
                                                </ext:Model>
                                            </Model>
                                        </ext:Store>
                                    </Store>
                                </ext:ComboBox>
                            </td>
                            <td>
                                <ext:ComboBox ID="cmbLevel" MatchFieldWidth="false" runat="server" FieldLabel="Level/Position"
                                    Width="140" MarginSpec="0 5 0 5" DisplayField="Name" ValueField="LevelId" QueryMode="Local"
                                    LabelSeparator="" LabelAlign="Top">
                                    <Items>
                                        <ext:ListItem Index="0" Value="-1" Text="All">
                                        </ext:ListItem>
                                    </Items>
                                    <Store>
                                        <ext:Store ID="storeLevel" runat="server">
                                            <Model>
                                                <ext:Model ID="modelLevel" runat="server">
                                                    <Fields>
                                                        <ext:ModelField Name="LevelId" Type="Int" />
                                                        <ext:ModelField Name="Name" Type="String" />
                                                    </Fields>
                                                </ext:Model>
                                            </Model>
                                        </ext:Store>
                                    </Store>
                                </ext:ComboBox>
                            </td>
                            <td>
                                <ext:ComboBox MatchFieldWidth="false" FieldLabel="Designation" ID="cmbDesignation"
                                    Width="140" MarginSpec="0 5 0 5" runat="server" ValueField="DesignationId" DisplayField="LevelAndDesignation"
                                    LabelAlign="Top" LabelSeparator="" QueryMode="Local">
                                    <Items>
                                        <ext:ListItem Index="0" Value="-1" Text="All">
                                        </ext:ListItem>
                                    </Items>
                                    <Store>
                                        <ext:Store ID="Store7" runat="server">
                                            <Model>
                                                <ext:Model ID="Model9" IDProperty="DesignationId" runat="server">
                                                    <Fields>
                                                        <ext:ModelField Name="DesignationId" />
                                                        <ext:ModelField Name="LevelAndDesignation" />
                                                    </Fields>
                                                </ext:Model>
                                            </Model>
                                        </ext:Store>
                                    </Store>
                                </ext:ComboBox>
                            </td>
                            <td>
                                <ext:ComboBox MatchFieldWidth="false" FieldLabel="Type" ID="cmbRetType"
                                    Width="140" MarginSpec="0 5 0 5" runat="server" 
                                    LabelAlign="Top" LabelSeparator="" QueryMode="Local">
                                    <Items>
                                        <ext:ListItem Index="0" Value="-1" Text="All">
                                        </ext:ListItem>
                                         <ext:ListItem Index="0" Value="false" Text="Regular only">
                                        </ext:ListItem>
                                         <ext:ListItem Index="0" Value="true" Text="Retired Only">
                                        </ext:ListItem>
                                    </Items>
                                    <SelectedItems>
                                        <ext:ListItem Index="1" />
                                    </SelectedItems>
                                   
                                </ext:ComboBox>
                            </td>
                            <td>
                                <ext:Button ID="Button1" runat="server" Text="Load" Width="100" Cls="btn btn-default"
                                    MarginSpec="25 10 10 10">
                                    <DirectEvents>
                                        <Click OnEvent="btnLoad_Change">
                                            <EventMask ShowMask="true" />
                                        </Click>
                                    </DirectEvents>
                                </ext:Button>
                            </td>
                        </tr>
                    </table>
                </Content>
            </ext:Container>
        </div>
        <div class="widget">
            <div class="widget-body">
                <div>
                    <ext:TabPanel ID="TabPanel_Main" ActiveIndex="0" Border="false" Unstyled="true" runat="server"
                        Plain="true" OverflowY="Auto">
                        <Items>
                            <ext:Panel ID="Panel_ThisMonth" runat="server" AutoHeight="true" Title="Position"
                                Header="False" Border="false" OverflowY="Auto">
                                <Content>
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_NextMonth" AutoHeight="true" runat="server" Title="Designation"
                                Header="False" Border="false">
                                <Content>
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_LastMonth" AutoHeight="true" runat="server" Title="Branch" Header="False"
                                Border="false">
                                <Content>
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel1" AutoHeight="true" runat="server" Title="Department" Header="False"
                                Border="false">
                                <Content>
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel2" AutoHeight="true" runat="server" Title="Position-Branch" Header="False"
                                Border="false">
                                <Content>
                                </Content>
                            </ext:Panel>
                        </Items>
                        <Listeners>
                            <TabChange Fn="focusEvent">
                            </TabChange>
                        </Listeners>
                    </ext:TabPanel>
                </div>
                <div class="right">
                    <asp:LinkButton ID="btnExport" runat="server" Text="Export to Excel" OnClick="btnExport_Click"
                        CssClass=" excel marginRight" Style="float: left;" />
                </div>
                <div style="clear: both">
                </div>
                <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridEmployee" runat="server" Cls="itemgrid">
                    <Store>
                        <ext:Store ID="Store3" runat="server">
                            <Model>
                                <ext:Model ID="Model4" runat="server">
                                    <Fields>
                                        <ext:ModelField Name="SN" Type="String" />
                                        <ext:ModelField Name="Name" Type="string" />
                                        <ext:ModelField Name="Total" Type="string" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                    </Store>
                    <ColumnModel>
                        <Columns>
                            <ext:Column ID="Column1" Sortable="false" MenuDisabled="true" runat="server" Text="SN"
                                Align="Right" Width="50" DataIndex="SN" />
                            <ext:Column ID="Column9" Sortable="false" MenuDisabled="true" runat="server" Text="Name"
                                Align="Left" Width="300" DataIndex="Name">
                                <Renderer Fn="rendername" />
                            </ext:Column>
                            <ext:Column ID="Column8" Sortable="false" MenuDisabled="true" runat="server" Text="Count"
                                Align="Right" Width="100" DataIndex="Total" />
                        </Columns>
                    </ColumnModel>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel2111" runat="server" Mode="Single" />
                    </SelectionModel>
                </ext:GridPanel>
                <ext:GridPanel ID="gridBranchLevel" runat="server" Border="false" Collapsible="false"
                    StyleSpec="margin-top:15px;" >
                    <Store>
                        <ext:Store runat="server" ID="Store_ItemInSiteDetails">
                            <Reader>
                                <ext:ArrayReader>
                                </ext:ArrayReader>
                            </Reader>
                            <Model>
                                <ext:Model ID="Model_ItemInSite" runat="server">
                                    <Fields>
                                        <ext:ModelField Name="Name" Type="String" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                    </Store>
                    <ColumnModel runat="server" ID="ColumnModel_ItemInSite" Cls="gridheight">
                        <Columns>
                            <ext:Column ID="Column_ItemID" Locked="true" runat="server" Text="Level" Width="170"
                                Sortable="false" MenuDisabled="true" DataIndex="Name" />
                        </Columns>
                    </ColumnModel>
                </ext:GridPanel>
                <div style="clear: both">
                </div>
            </div>
        </div>
    </div>
    <br />
</asp:Content>
