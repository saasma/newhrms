﻿<%@ Page Title="Tax Certificate Incomes" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="YearlyIncomeSettings.aspx.cs" Inherits="Web.NewHR.UserControls.YearlyIncomeSettings" %>

<%@ Register Src="~/NewHR/UserControls/EmployeeDetailsCtl.ascx" TagName="EmployeeDetailsCtl"
    TagPrefix="uc1" %>
<%@ Register Src="~/NewHR/UserControls/EmploymentHistoryCtl.ascx" TagName="EmploymentHistoryCtl"
    TagPrefix="ucEmploymentHistoryCtl" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
    <style type="text/css">
        .readonlyClass
        {
            background-color: #F5F5F5 !important;
        }
    </style>
    <script type="text/javascript">
       
        function groupRenderer(value) {
            if (value == 0)
                return "";


            if (value == 1) return "Salary (तलव)";
            if (value == 2) return "Allowances ( भत्ता)";
            if (value == 3) return "Provident Fund (संचय कोष)";
            if (value == 4) return "Vehicle Facility (गाडी सुबिधा)";
            if (value == 5) return "Telephone Facility (टेलिफोन सुविधा)";
            if (value == 6) return "Housing Allowance (आवास सुविधा)";
            if (value == 7) return "Bonus (बोनस)";
            if (value == 8) return "Health Expenses (औषधि उपचार)";
            if (value == 9) return "Leave Encashment (बिदा वापतको भुक्तानी)";
            if (value == 10) return "Dashain Allowance (दशैं भत्ता)";
            if (value == 11) return "Life Insurance Income (जीवन बिमा)";
            if (value == 12) return "Reward (रिवार्ड)";
            if (value == 13) return "Concessessional Interest (ब्याज अन्तर)";
            if (value == 15) return "Other (अन्य)";


        }

        var afterEdit = function (editor, e) {
            e.record.commit();
        }


    </script>
    <ext:XScript ID="XScript1" runat="server">
        <script>
            var UpdateRow = function () {                
                var plugin = this.editingPlugin;
                VarEmployeeID=ctl00_ContentPlaceHolder_Main_Grid.getSelectionModel().getSelection()[0].data.TypeSourceId;
                if (this.getForm().isValid()) { // local validation                    
                   #{DirectMethods}.SaveRow(VarEmployeeID,plugin.context.record.phantom, this.getValues(false, false, false, true), {
                        success : function (result) {
                            if (!result.valid) {
                               
                                 Ext.Msg.show({
                                    title: 'Update failure!', //<- the dialog's title  
                                    msg: result.Message, //<- the message  
                                    buttons: Ext.Msg.OK, //<- YES and NO buttons  
                                    icon: Ext.Msg.WARNING, // <- error icon  
                                    minWidth: 300
                                });
                                return;
                            }

                            plugin.completeEdit();
                        }
                    });
                }
            };
        </script>
    </ext:XScript>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Tax Certificate Incomes
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <ext:Hidden runat="server" ID="hdnID">
        </ext:Hidden>
        <div class="innerLR">
            <div class="separator bottom">
            </div>
            <div style="width: 1030px">
                <!-- panel-heading -->
                <div class="panel-body1">
                    (Double click the row to edit the group)
                    <ext:GridPanel ID="Grid" runat="server" Cls="itemgrid" Width="800">
                        <Store>
                            <ext:Store ID="StoreYearlyIncome" runat="server">
                                <Model>
                                    <ext:Model ID="Model1" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="CalculationText" Type="string" />
                                            <ext:ModelField Name="HeaderName" Type="string" />
                                            <ext:ModelField Name="Group" Type="string" />
                                            <ext:ModelField Name="TypeSourceId" Type="String" />
                                            <ext:ModelField Name="Value" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn Text="SN" Width="50" runat="server" />
                                <ext:Column ID="Column2" runat="server" Text="Income" DataIndex="HeaderName" Width="250"
                                    Flex="1">
                                </ext:Column>
                                <ext:Column ID="Column1" runat="server" Text="Calculation" DataIndex="CalculationText"
                                    Width="180" />
                                <ext:Column ID="Column3" runat="server" Text="Group" DataIndex="Value" Width="250">
                                    <Renderer Fn="groupRenderer" />
                                    <Editor>
                                        <ext:ComboBox ID="ComboBox1" LabelSeparator="" ForceSelection="true" LabelAlign="Top"
                                            runat="server" FieldLabel="Group *" Width="300" DisplayField="Text" ValueField="Value"
                                            EmptyText="" QueryMode="Default">
                                            <Items>
                                                <ext:ListItem Text="Salary (तलव)" Value="1">
                                                </ext:ListItem>
                                                <ext:ListItem Text="Allowances (भत्ता)" Value="2">
                                                </ext:ListItem>
                                                <ext:ListItem Text="Provident Fund (संचय कोष)" Value="3">
                                                </ext:ListItem>
                                                <ext:ListItem Text="Vehicle Facility (गाडी सुबिधा)" Value="4">
                                                </ext:ListItem>
                                                <ext:ListItem Text="Telephone Facility (टेलिफोन सुविधा)" Value="5">
                                                </ext:ListItem>
                                                <ext:ListItem Text="Housing Allowance (आवास सुविधा)" Value="6">
                                                </ext:ListItem>
                                                <%--  <ext:ListItem Text="GarBhada" Value="6">
                                </ext:ListItem>--%>
                                                <ext:ListItem Text="Bonus (बोनस)" Value="7">
                                                </ext:ListItem>
                                                <ext:ListItem Text="Health Expenses (औषधि उपचार)" Value="8">
                                                </ext:ListItem>
                                                <ext:ListItem Text="Leave Encashment (बिदा वापतको भुक्तानी)" Value="9">
                                                </ext:ListItem>
                                                <ext:ListItem Text="Dashain Allowance (दशैं भत्ता)" Value="10">
                                                </ext:ListItem>
                                                <ext:ListItem Text="Life Insurance Income (जीवन बिमा)" Value="11">
                                                </ext:ListItem>
                                                <ext:ListItem Text="Reward (रिवार्ड)" Value="12">
                                                </ext:ListItem>
                                                <ext:ListItem Text="Concessessional Interest (ब्याज अन्तर)" Value="13">
                                                </ext:ListItem>
                                                <ext:ListItem Text="Other (अन्य)" Value="15">
                                                </ext:ListItem>
                                            </Items>
                                        </ext:ComboBox>
                                    </Editor>
                                </ext:Column>
                                <%--<ext:CommandColumn ID="CommandColumn1" runat="server" Width="45">
                                    <Commands>
                                        <ext:CommandSeparator />
                                        <ext:GridCommand Cls="editGridButton" ToolTip-Text="Edit" Text="<i class='fa fa-pencil'></i>"
                                            CommandName="Edit" />
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridCommand">
                                            <EventMask ShowMask="true">
                                            </EventMask>
                                            <ExtraParams>
                                                <ext:Parameter Name="ID" Value="record.data.TypeSourceId" Mode="Raw">
                                                </ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw">
                                                </ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>--%>
                            </Columns>
                        </ColumnModel>
                        <Plugins>
                            <ext:RowEditing ID="RowEditing1dd" runat="server" ClicksToMoveEditor="1" AutoCancel="false"
                                SaveHandler="UpdateRow">
                                <%--  <Listeners>
                            <BeforeEdit Fn="beforeEdit" />
                        </Listeners>--%>
                                <Listeners>
                                    <Edit Fn="afterEdit" />
                                </Listeners>
                            </ext:RowEditing>
                        </Plugins>
                        <SelectionModel>
                            <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                        </SelectionModel>
                    </ext:GridPanel>
                    <%--  <div class="buttonBlock">
                        <ext:Button runat="server" ID="LinkButton1" Cls="btn btn-primary btn-sect" Text="<i></i>Add Income Type">
                            <DirectEvents>
                                <Click OnEvent="btnAddNewLine_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                    </div>--%>
                </div>
            </div>
        </div>
    </div>
    <ext:Window ID="AEWindow" runat="server" Title="Change Income Group" Icon="Application"
        Height="300" Width="380" BodyPadding="5" Hidden="true" Modal="true">
        <Content>
            <table class="fieldTable">
                <tr>
                    <td>
                        <ext:Store ID="storeIncomes" runat="server">
                            <Model>
                                <ext:Model ID="model2" runat="server">
                                    <Fields>
                                        <ext:ModelField Name="TypeSourceId" Type="String" />
                                        <ext:ModelField Name="HeaderName" Type="String" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                        <ext:ComboBox ID="cmbIncomes" ReadOnly="true" SelectionMode="All" LabelSeparator=""
                            ForceSelection="true" LabelAlign="Top" runat="server" FieldLabel="Income *" Width="300"
                            DisplayField="HeaderName" ValueField="TypeSourceId" StoreID="storeIncomes" EmptyText=""
                            QueryMode="Local">
                        </ext:ComboBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:Store ID="storeTypes" runat="server">
                            <Model>
                                <ext:Model ID="modelLevel" runat="server">
                                    <Fields>
                                        <ext:ModelField Name="TypeID" Type="String" />
                                        <ext:ModelField Name="Name" Type="String" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                        <ext:ComboBox ID="cmbType" LabelSeparator="" ForceSelection="true" LabelAlign="Top"
                            runat="server" FieldLabel="Group *" Width="300" DisplayField="Text" ValueField="Value"
                            EmptyText="" QueryMode="Default">
                            <Items>
                                <ext:ListItem Text="Salary (तलव)" Value="1">
                                </ext:ListItem>
                                <ext:ListItem Text="Allowances (भत्ता)" Value="2">
                                </ext:ListItem>
                                <ext:ListItem Text="Provident Fund (संचय कोष)" Value="3">
                                </ext:ListItem>
                                <ext:ListItem Text="Vehicle Facility (गाडी सुबिधा)" Value="4">
                                </ext:ListItem>
                                <ext:ListItem Text="Telephone Facility (टेलिफोन सुविधा)" Value="5">
                                </ext:ListItem>
                                <ext:ListItem Text="Housing Allowance (आवास सुविधा)" Value="6">
                                </ext:ListItem>
                                <%--  <ext:ListItem Text="GarBhada" Value="6">
                                </ext:ListItem>--%>
                                <ext:ListItem Text="Bonus (बोनस)" Value="7">
                                </ext:ListItem>
                                <ext:ListItem Text="Health Expenses (औषधि उपचार)" Value="8">
                                </ext:ListItem>
                                <ext:ListItem Text="Leave Encashment (बिदा वापतको भुक्तानी)" Value="9">
                                </ext:ListItem>
                                <ext:ListItem Text="Dashain Allowance (दशैं भत्ता)" Value="10">
                                </ext:ListItem>
                                <ext:ListItem Text="Life Insurance Income (जीवन बिमा)" Value="11">
                                </ext:ListItem>
                                <ext:ListItem Text="Reward (रिवार्ड)" Value="12">
                                </ext:ListItem>
                                <ext:ListItem Text="Concessessional Interest (ब्याज अन्तर)" Value="13">
                                </ext:ListItem>
                                <ext:ListItem Text="Other (अन्य)" Value="15">
                                </ext:ListItem>
                            </Items>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator5" runat="server"
                            ValidationGroup="SaveUpdate" ControlToValidate="cmbType" ErrorMessage="Type is required." />
                    </td>
                </tr>
                <tr>
                    <td valign="bottom" colspan="2">
                        <div class="popupButtonDiv" style="padding-top: 15px">
                            <ext:Button runat="server" ID="btnSave" Cls="btn btn-primary btn-sect btn-sm" Text="<i></i>Save">
                                <DirectEvents>
                                    <Click OnEvent="btnSave_Click">
                                        <EventMask ShowMask="true" />
                                    </Click>
                                </DirectEvents>
                                <Listeners>
                                    <Click Handler="valGroup = 'SaveUpdate'; return CheckValidation();">
                                    </Click>
                                </Listeners>
                            </ext:Button>
                            <div class="btnFlatOr">
                                or</div>
                            <ext:LinkButton runat="server" Cls="btnFlatLeftGap" ID="LinkButton2" Text="<i></i>Cancel">
                                <Listeners>
                                    <Click Handler="#{AEWindow}.hide();">
                                    </Click>
                                </Listeners>
                            </ext:LinkButton>
                        </div>
                    </td>
                </tr>
            </table>
        </Content>
    </ext:Window>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
