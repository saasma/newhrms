﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using Utils.Calendar;
using Utils.Helper;
using Bll;

namespace Web.NewHR
{
    public partial class PromotionListReport : BasePage
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMsg.Text = "";
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }

            JavascriptHelper.AttachNonDialogPopUpCode(Page, "taxImportPopup", "../ExcelWindow/PromotionHistoryExcel.aspx", 450, 500);
        }

        public void Initialise()
        {
            LoadData();
        }

        public void btnLoad_Click(object sender, DirectEventArgs e)
        {
            DateTime? fromDate = null;

            if (!string.IsNullOrEmpty(calFromDate.Text.Trim()))
                fromDate = GetEngDate(calFromDate.Text.Trim());

            DateTime? toDate = null;

            if (!string.IsNullOrEmpty(calToDate.Text.Trim()))
                toDate = GetEngDate(calToDate.Text.Trim());

            int? empId = null;

            if (cmbSearch.SelectedItem != null && cmbSearch.SelectedItem.Value != null)
                empId = int.Parse(cmbSearch.SelectedItem.Value);

            PagingToolbar1.DoRefresh();

            //gridLevelGradeHistory.Store[0].DataSource = NewHRManager.GetLevelGradeReport(fromDate, toDate, empId);
            //gridLevelGradeHistory.Store[0].DataBind();
        }

        protected void btnEdit_Click(object sender, DirectEventArgs e)
        {
            int RowID = int.Parse(hiddenValueRow.Text.Trim());
            LevelGradeChangeDetail pro =
                BLL.BaseBiz.PayrollDataContext.LevelGradeChangeDetails.FirstOrDefault(x => x.DetailId == RowID);

            if(pro.LevelId != null)
            {
                NewMessage.ShowWarningMessage("Only imported promotion history can be edited.");
                return;
            }

            window.Center();
            window.Show();

            lblEmpName.Text = EmployeeManager.GetEmployeeName(pro.EmployeeId.Value);
            calDate.Text = pro.FromDate;
            txtFromLevel.Text = pro.PrevLevelName;
            txtNewLevel.Text = pro.LevelName;


        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            int RowID = int.Parse(hiddenValueRow.Text.Trim());
            Page.Validate("SaveBrTran");
            if (Page.IsValid)
            {
                LevelGradeChangeDetail obj = new LevelGradeChangeDetail();
                obj.DetailId = RowID;

                obj.FromDate = calDate.Text;
                obj.FromDateEng = GetEngDate(obj.FromDate);
                obj.PrevLevelName = txtFromLevel.Text.Trim();
                obj.LevelName = txtNewLevel.Text.Trim();

                    CommonManager.UpdatePromotionHistory(obj);

                    NewMessage.ShowNormalMessage("Promotion history updated.");

                    window.Close();
                    PagingToolbar1.DoRefresh();
            }
        }

        protected void btnDeleteLevel_Click(object sender, DirectEventArgs e)
        {
            int RowID = int.Parse(hiddenValueRow.Text.Trim());
            Status status = NewHRManager.DeletePromotionID(RowID);
            if (status.IsSuccess)
            {
                PagingToolbar1.DoRefresh();
                NewMessage.ShowNormalMessage("Promotion History deleted.");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        protected void LoadData()
        {

            PagingToolbar1.DoRefresh();
            //gridLevelGradeHistory.Store[0].DataSource = NewHRManager.GetLevelGradeReport
            //    (null, null, null);
            //gridLevelGradeHistory.Store[0].DataBind();

            StoreNames.DataSource = SalaryManager.GetPromotionNames();
            StoreNames.DataBind();
        }


        public void btnExport_Click(object sender, EventArgs e)
        {

            //List<Report_GetRemoteAreaEmployeeListResult> _ListAppraisalForm = ReportManager.GetRemoteAreaList();

            DateTime? fromDate = null;
            bool? type = null;

            if (!string.IsNullOrEmpty(calFromDate.Text.Trim()))
                fromDate = GetEngDate(calFromDate.Text.Trim());

            DateTime? toDate = null;

            if (!string.IsNullOrEmpty(calToDate.Text.Trim()))
                toDate = GetEngDate(calToDate.Text.Trim());

            int? empId = null;

            if (cmbSearch.SelectedItem != null && cmbSearch.SelectedItem.Value != null)
                empId = int.Parse(cmbSearch.SelectedItem.Value);

            int? levelGradeId = null;

            if (cmbName.SelectedItem != null && cmbName.SelectedItem.Value != null)
                levelGradeId = int.Parse(cmbName.SelectedItem.Value);

            if (cmbType.SelectedItem != null && cmbType.SelectedItem.Value != null && cmbType.SelectedItem.Value!="All")
                type = bool.Parse(cmbType.SelectedItem.Value);

            List<GetLevelGradeChangeDetailsSPResult> _ListAppraisalForm = NewHRManager.GetLevelGradeReport(0, 99999, fromDate, toDate, empId,
                type, levelGradeId);


            Dictionary<string, string> title = new Dictionary<string, string>();
            title["Promotion History"] = "";



            ExcelHelper.ExportToExcel("Promotion History",
                           _ListAppraisalForm, new List<string> {"TotalRows","RowNumber","ServiceStatus","DetailId","LevelGradeChangeId","BranchId","PrevDesignationId","IsBackdatedPromotion","PrevLevelId","LevelId","DesignationId","Type","Status","StatusChangeOn","StatusChangeBy","SequenceNo","ChangeType","StatusText","IsUsedInSalary" }, 
                           new List<string> { }, new Dictionary<string, string> { { "EmployeeId", "EIN" }, { "PrevStepGrade", "Old Grade"}, { "StepGrade", "New Grade" }, { "IdCardNo", "INo"}, { "OldDesignation", "Old Designation" }, { "NewDesignation", "New Designation" } }
                           , new List<string> { }, title,
                           new List<string> { "EmployeeId", "IdCardNo", "Name", "FromDate", "STATUS", "FromDateEng", "OldLevel", "PrevStepGrade", "NewLevel", "StepGrade","OldDesignation","NewDesignation", "Arrear" } // Order
                           );


        }

       
    }
}