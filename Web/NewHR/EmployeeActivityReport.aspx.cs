﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Entity;
using BLL;
using System.IO;
using Utils;

namespace Web.NewHR
{
    public partial class EmployeeActivityReport : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.HumanResource;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                BindCombos();
                
                if (CommonManager.CompanySetting.WhichCompany == WhichCompany.Sana)
                {
                    lblActivityClients.Hide();
                    tdClients.InnerText = "";
                    ColClients.Hide();
                    gridActivity.Width = 960;
                    TabPanel1.Width = 960;
                }
            }
        }

        private void BindCombos()
        {
            storeClient.DataSource = ListManager.GetAllClients();
            storeClient.DataBind();

            storeType.DataSource = ListManager.GetActivityTypes();
            storeType.DataBind();
        }

        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            int totalRecords = 0;
            int clientId = -1;
            int activityTypeId = -1;
            DateTime? startDate = null, endDate = null;

            if (cmbClientFilter.SelectedItem != null && cmbClientFilter.SelectedItem.Value != null)
                clientId = int.Parse(cmbClientFilter.SelectedItem.Value);

            if (cmbTypeFilter.SelectedItem != null && cmbTypeFilter.SelectedItem.Value != null)
                activityTypeId = int.Parse(cmbTypeFilter.SelectedItem.Value);

            int type = 0, employeeId = -1;
            bool all = false;

            type = TabPanel1.ActiveTabIndex + 1;
           
            if (TabPanel1.ActiveTabIndex == 5)
            {
                type = -1;
                all = true;

                if (txtStartDate.SelectedDate != new DateTime())
                {
                    startDate = txtStartDate.SelectedDate;

                    if (txtEndDate.SelectedDate == new DateTime())
                    {
                        NewMessage.ShowWarningMessage("To Date is required for date filter.");
                        txtEndDate.Focus();
                        return;
                    }
                }

                if (txtEndDate.SelectedDate != new DateTime())
                {
                    endDate = txtEndDate.SelectedDate;

                    if (txtStartDate.SelectedDate == new DateTime())
                    {
                        NewMessage.ShowWarningMessage("From Date is required for date filter.");
                        txtStartDate.Focus();
                        return;
                    }
                }
            }

            if (cmbSearch.SelectedItem != null && cmbSearch.SelectedItem.Value != null)
                employeeId = int.Parse(cmbSearch.SelectedItem.Value);

            int pageSize = int.Parse(cmbPageSize.SelectedItem.Value);
            List<GetActivityListForAdminResult> list = NewHRManager.GetActivityForAdmin(e.Start / pageSize, pageSize, employeeId, clientId, activityTypeId, type, startDate, endDate, all, (int)AtivityStatusEnum.Approved);
            
            if (list.Count > 0)
                totalRecords = list[0].TotalRows.Value;

            e.Total = totalRecords;

            storeActivity.DataSource = list;
            storeActivity.DataBind();

        }

        private void Clear()
        {
            lblActivityDate.Text = "";
        }

        protected void btnView_Click(object sender, DirectEventArgs e)
        {
            int detailId = int.Parse(hdnActivityId.Text);
            NewActivityDetail obj = NewHRManager.GetNewActivityDetailById(detailId);

            if (obj != null)
            {
                lblActivityDate.Text = string.Format("{0}, {1}", obj.DateEng.Value.DayOfWeek, obj.DateEng.Value.ToString("MMMMM dd"));

                if (obj.CreatedOn != null)
                    lblActivityDateDetls.Html = "<span style='color:#5E61FF;'>" + "Submitted &nbsp;&nbsp; " + "</span>" + obj.CreatedOn.Value.ToString("MMMMM dd, yyyy hh:mm:ss tt");

                int employeeId = obj.EmployeeId.Value;
                EEmployee emp = EmployeeManager.GetEmployeeById(employeeId);
                bool hasPhoto = false;
                if (emp.HHumanResources.Count > 0)
                {
                    if (!string.IsNullOrEmpty(emp.HHumanResources[0].UrlPhotoThumbnail))
                    {
                        img.ImageUrl = Page.ResolveUrl(Config.UploadLocation + "/" + emp.HHumanResources[0].UrlPhotoThumbnail);
                        if (File.Exists(Server.MapPath(img.ImageUrl)))
                            hasPhoto = true;
                    }
                }
                if (!hasPhoto)
                {
                    img.ImageUrl = Page.ResolveUrl(Web.Helper.WebHelper.GetEmployeeNoImage(employeeId));
                }

                lblEmpName.Text = emp.Name;
                lblDesignation.Text = emp.EDesignation.Name;

                DateTime date = obj.DateEng.Value;
                //int i = 0;
                //foreach (var item in obj.NewActivityDetails)
                //{
                //    i++;
                //    item.SN = i;
                //    item.StartTimeDate = new DateTime(date.Year, date.Month, date.Day, item.StartTime.Value.Hours, item.StartTime.Value.Minutes, 0);

                //    if (item.EndTime != null)
                //    {
                //        TimeSpan tsDiff = item.EndTime.Value - item.StartTime.Value;
                //        item.DurationTime = tsDiff.Hours + ":" + tsDiff.Minutes.ToString().PadLeft(2, '0');
                //    }
                //}

                //gridActivityAdd.Store[0].DataSource = obj.NewActivityDetails;
                //gridActivityAdd.Store[0].DataBind();

                lblActivityType.Text = hdnActivityType.Text;

                lblActivityClients.Text = hdnClients.Text;
                lblActivityDuration.Text = hdnActivityDuration.Text;
                txtActivityDetails.Text = obj.Description;

                portalPlannedActivities.Hide();
                EmployeePlan dbEmpPlan = NewHRManager.GetEmployeePlanByDate(obj.DateEng.Value, obj.EmployeeId.Value);
                if (dbEmpPlan != null)
                {
                    if (dbEmpPlan != null)
                    {
                        gridPlan.Store[0].DataSource = dbEmpPlan.EmployeePlanDetails;
                        gridPlan.Store[0].DataBind();

                        Portlet1.Collapse();
                        portalPlannedActivities.Show();
                    }
                }

                List<ActivityComment> listActivityComments = BLL.BaseBiz.PayrollDataContext.ActivityComments.Where(x => x.ActivityId == obj.ActivityId).ToList();
                foreach (var item in listActivityComments)
                {
                    item.Name = EmployeeManager.GetEmployeeById(UserManager.GetUserByUserID(item.CreatedBy.Value).UserMappedEmployeeId.Value).Name;
                }

                gridComments.Store[0].DataSource = listActivityComments;
                gridComments.Store[0].DataBind();

                if (listActivityComments.Count > 0)
                    gridComments.Show();
                else
                    gridComments.Hide();

                ActivityEmpComment objActivityEmpComment = NewHRManager.GetActivityEmpComment(obj.ActivityId, obj.EmployeeId.Value);
                if (objActivityEmpComment != null)
                {
                    dfEmpComment.Text = objActivityEmpComment.Comment;
                    dfEmpComment.Show();
                }
                else
                    dfEmpComment.Hide();

                BindNoOfActivityButtonList(obj.EmployeeId.Value, obj.DateEng.Value);

                WActivity.Center();
                WActivity.Show();
            }
        }

        [DirectMethod]
        public static string GetGrid(Dictionary<string, string> parameters)
        {
            //int activityId = int.Parse(parameters["id"]);

            //NewActivity obj = NewHRManager.GetNewActivityById(activityId);
            //if (obj != null)
            //{
            //    List<object> data = new List<object>();

            //    int i = 0;
            //    DateTime date = obj.DateEng.Value;

            //    foreach (var item in obj.NewActivityDetails)
            //    {
            //        i++;
            //        item.SN = i;
            //        item.StartTimeDate = new DateTime(date.Year, date.Month, date.Day, item.StartTime.Value.Hours, item.StartTime.Value.Minutes, 0);

            //        if (item.EndTime != null)
            //        {
            //            TimeSpan tsDiff = item.EndTime.Value - item.StartTime.Value;
            //            item.DurationTime = tsDiff.Hours + ":" + tsDiff.Minutes.ToString().PadLeft(2, '0');
            //        }

            //        item.ClientName = ListManager.GetClientById(item.ClientSoftwareId.Value).ClientName;
            //        item.ActivityName = ListManager.GetActivityTypeById(item.ActivityType.Value).Name;

            //        data.Add(new
            //        {
            //            SN = i,
            //            Client = item.ClientName,
            //            ActType = item.ActivityName,
            //            StartTimeDate = item.StartTimeDate.ToString("HH:mm tt"),
            //            DurationTime = item.DurationTime,
            //            Representative = item.Representative,
            //            Description = item.Description
            //        });
            //    }

            //    int count = obj.NewActivityDetails.Count();
            //    int height = 42 + 32 * count;

            //    GridPanel grid = new GridPanel
            //    {
            //        Height = height,
            //        EnableColumnHide = false,
            //        Store = 
            //    { 
            //        new Store 
            //        { 
            //            Model = {
            //                new Model {
            //                    IDProperty = "SN",
            //                    Fields = 
            //                    {
            //                        new ModelField("SN"),
            //                        new ModelField("Client"),
            //                        new ModelField("ActType"),
            //                        new ModelField("StartTimeDate"),
            //                        new ModelField("DurationTime"),
            //                        new ModelField("Representative"),
            //                        new ModelField("Description")
            //                    }
            //                }
            //            },
            //            DataSource = data
            //        }
            //    },
            //        ColumnModel =
            //        {
            //            Columns = 
            //        { 
            //            new Column { Text = "SN", DataIndex = "SN", Width=10, Sortable = false, MenuDisabled = true, Hidden=true },
            //            new Column { Text = "Client", DataIndex = "Client", Width=150, Sortable = false, MenuDisabled = true },
            //            new Column { Text = "Type", DataIndex = "ActType", Width=150, Sortable = false, MenuDisabled = true },
            //            new Column { Text = "Start Time", DataIndex = "StartTimeDate", Width=90, Sortable = false, MenuDisabled = true, Align = Alignment.Center },
            //            new Column { Text = "Duration(h:m)", DataIndex = "DurationTime", Width=90, Sortable = false, MenuDisabled = true, Align = Alignment.Center },
            //            new Column { Text = "Person", DataIndex = "Representative", Width=180, Sortable = false, MenuDisabled = true },
            //            new Column { Text = "Description", DataIndex = "Description", Width=430, Sortable = false, MenuDisabled = true, Wrap = true  }
            //        }
            //        }
            //    };

            //    return ComponentLoader.ToConfig(grid);
            //}
            //else
            //    return "";

            return "";
        }

        private void BindNoOfActivityButtonList(int employeeId, DateTime activityDate)
        {
            List<GetActivityListForAdminResult> list = NewHRManager.GetActivityForAdmin(0, 1000, employeeId, -1, -1, -1, activityDate.Date, activityDate.Date, true, -1);

            int i = 1;
            if (list.Count > 1)
            {
                foreach (var item in list)
                {
                    item.SN = i;
                    i++;
                }

                storeActivityDV.DataSource = list;
                storeActivityDV.DataBind();
                dvActivity.Show();
            }
            else
                dvActivity.Hide();
        }


    }
}