﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using Utils.Calendar;

namespace Web.NewHR
{
    public partial class SalaryChangeList : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.HumanResource;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMsg.Text = "";
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        //[DirectMethod]
        //public string GetNewGrade(int levelId, int gradeStep,int newLevelId)
        //{
        //    return NewPayrollManager.GetNewGrade(levelId, gradeStep, newLevelId, GetCurrentDateForSalary(), LevelGradeChangeType.Promotion).ToString();            
        //}


        public void btnGenerate_Click(object sender, DirectEventArgs e)
        {
            //PayrollPeriod period = CommonManager.GetLastFinalSalaryPayrollPeriod();
            //period
            string fromDate = calFromDate.Text.Trim();
            if (string.IsNullOrEmpty(fromDate))
            {
                NewMessage.ShowWarningMessage("From date is required.");
                return;
            }

            DateTime fromDateEng = GetEngDate(fromDate);

            if (SalaryManager.IsValidSalaryChangeGradeFittingForDate(fromDateEng)==false)
            {
                SetWarning(lblMsg, "Salary change grade fitting can only be done if salary change has been saved for this date.");
                return;
            }

            List<GetEmployeeListForSalaryChangeGradeFittingResult> list
                = SalaryManager.GetEmployeeListForSalaryChangeGradeFitting(fromDateEng);
            //if (list.Count>0)
            {
                //txtName.Text = promotion.Name;
                //calFromDate.Text = promotion.FromDate;

                //List<PromotionBO> list = new List<PromotionBO>();
                //List<LevelGradeChangeDetail> details = promotion.LevelGradeChangeDetails.OrderBy(x => x.SequenceNo).ToList();
                //EmployeeManager empMgr = new EmployeeManager();
                //foreach (LevelGradeChangeDetail item in details)
                //{
                //    PromotionBO emp = new PromotionBO();
                //    emp.EmployeeId = item.EmployeeId.Value;
                //    emp.EmployeeName = empMgr.GetById(item.EmployeeId.Value).Name;
                //    emp.PrevLevelId = item.PrevLevelId.ToString();
                //    emp.PrevDesignationId = item.PrevDesignationId.ToString();
                //    emp.PrevGrade = item.PrevStepGrade.ToString();
                //    emp.FromDate = item.FromDate;

                //    emp.DesignationId = item.DesignationId.ToString();
                //    emp.LevelId = item.LevelId.ToString();
                //    emp.StepGrade = item.StepGrade.ToString();

                //    list.Add(emp);
                //}

                gridGradeFitting.Store[0].DataSource = list;
                gridGradeFitting.Store[0].DataBind();
            }
        }

        public void Initialise()
        {
            StoreLevel.DataSource = NewPayrollManager.GetAllParentLevels();
            StoreLevel.DataBind();

            StorePostDesignation.DataSource = NewPayrollManager.GetAllPositionOrDesignationsList();
            StorePostDesignation.DataBind();

            LevelGradeChange gradeFitting = SalaryManager.GetUnapprovedSalaryChange();
            if (gradeFitting != null)
            {
                txtName.Text = gradeFitting.Name;
                calFromDate.Text = gradeFitting.FromDate;

                List<PromotionBO> list = new List<PromotionBO>();
                List<LevelGradeChangeDetail> details = gradeFitting.LevelGradeChangeDetails.OrderBy(x => x.SequenceNo).ToList();
                EmployeeManager empMgr = new EmployeeManager();
                foreach (LevelGradeChangeDetail item in details)
                {
                    PromotionBO emp = new PromotionBO();
                    emp.EmployeeId = item.EmployeeId.Value;
                    emp.EmployeeName = empMgr.GetById(item.EmployeeId.Value).Name;
                    emp.PrevLevelId = item.PrevLevelId.ToString();
                    emp.PrevDesignationId = item.PrevDesignationId.ToString();
                    emp.PrevGrade = item.PrevStepGrade.ToString();
                    

                   
                    emp.LevelId = item.LevelId.ToString();
                    emp.StepGrade = item.StepGrade.ToString();

                    list.Add(emp);
                }

                gridGradeFitting.Store[0].DataSource = list;
                gridGradeFitting.Store[0].DataBind();
            }
        }

        public void lnkBtnApprove_Click(object sender, DirectEventArgs e)
        {
            string gridJSON = e.ExtraParams["List"];
            List<PromotionBO> items = JSON.Deserialize<List<PromotionBO>>(gridJSON);
            LevelGradeChange change = new LevelGradeChange();
            change.Name = txtName.Text.Trim();
            change.FromDate = calFromDate.Text.Trim();
            if (!string.IsNullOrEmpty(calFromDate.Text.Trim()))
                change.FromDateEng = GetEngDate(change.FromDate);
            change.Type = (int)LevelGradeChangeType.SalaryStructureChange;

            LevelGradeChangeStatusType status = LevelGradeChangeStatusType.Approved;
            //if (sender == lnkBtnReject)
            //    status = LevelGradeChangeStatusType.Reject;

            foreach (PromotionBO item in items)
            {
                
                if (string.IsNullOrEmpty(item.StepGrade))
                {
                    SetWarning(lblMsg, string.Format("New Grade is required for the employee {0}.", item.EmployeeName));
                    return;
                }
               

                LevelGradeChangeDetail emp = new LevelGradeChangeDetail();
                emp.EmployeeId = item.EmployeeId;
                emp.PrevDesignationId = int.Parse(item.PrevDesignationId);
                emp.FromDate = change.FromDate;
                emp.FromDateEng = change.FromDateEng;
                emp.PrevLevelId = int.Parse(item.PrevLevelId);
                emp.PrevDesignationId = int.Parse(item.PrevDesignationId);
                emp.PrevStepGrade = double.Parse(item.PrevGrade);

                emp.LevelId = emp.PrevLevelId;

                bool allowDecimalInGrade = NewPayrollManager.AllowDecimalInGrade();

                if (!allowDecimalInGrade && item.StepGrade.Contains("."))
                {
                    SetWarning(lblMsg, string.Format("New Grade {1} not valid for the employee {0}.", item.EmployeeName, emp.StepGrade));
                    return;
                }

                if (allowDecimalInGrade && item.StepGrade.Contains("."))
                {
                    string[] splitString = item.StepGrade.Split('.');

                    int numberValue = int.Parse(splitString[0]);
                    int decimalValue = int.Parse(splitString[1]);

                    if (numberValue == 0)
                    {
                        SetWarning(lblMsg, string.Format("New Grade {1} not valid for the employee {0}.", item.EmployeeName, emp.StepGrade));
                        return;
                    }

                    if (decimalValue != 0 && decimalValue != 5)
                    {
                        SetWarning(lblMsg, string.Format("New Grade {1} not valid for the employee {0}.", item.EmployeeName, emp.StepGrade));
                        return;
                    }
                }

                emp.StepGrade = double.Parse(item.StepGrade);

               
                BLevelRate rate = NewPayrollManager.GetLevelRate(emp.LevelId.Value,change.FromDateEng.Value);


                if (allowDecimalInGrade)
                {
                    double maxStepGrade = double.Parse(rate.NoOfStepsGrade == null ? "0" : rate.NoOfStepsGrade.Value.ToString()) + 0.5;

                    if (emp.StepGrade > maxStepGrade)
                    {
                        SetWarning(lblMsg, string.Format("New Grade {1} not valid for the employee {0}, max grade value is {2}.", item.EmployeeName, emp.StepGrade, maxStepGrade));
                        return;
                    }
                }
                else
                {
                    if (emp.StepGrade > rate.NoOfStepsGrade)
                    {
                        SetWarning(lblMsg, string.Format("New Grade {1} not valid for the employee {0}, max grade value is {2}.", item.EmployeeName, emp.StepGrade, rate.NoOfStepsGrade));
                        return;
                    }
                }

                emp.Type = (int)LevelGradeChangeType.SalaryStructureChange;
                emp.Status = (int)status;


                change.LevelGradeChangeDetails.Add(emp);
            }


            Status respStatus = SalaryManager.ApprovePromotionSalaryChangeStatus(change, LevelGradeChangeStatusType.Approved);

            if (respStatus.IsSuccess)
            {
                if (status==LevelGradeChangeStatusType.Approved)
                    SetMessage(lblMsg, string.Format("Approved for {0} employees.", respStatus.Count));
                else
                {
                    SetMessage(lblMsg, string.Format("Rejected for {0} employees.", respStatus.Count));
                }
            }
            else
            {
                SetWarning(lblMsg, respStatus.ErrorMessage);
            }
        }
        public void btnSaveUpdate_Save(object sender, DirectEventArgs e)
        {
            string gridJSON = e.ExtraParams["List"];
            List<PromotionBO> items = JSON.Deserialize<List<PromotionBO>>(gridJSON);

            LevelGradeChange change = new LevelGradeChange();
            change.Name = txtName.Text.Trim();
            change.FromDate = calFromDate.Text.Trim();
            if (!string.IsNullOrEmpty(calFromDate.Text.Trim()))
                change.FromDateEng = GetEngDate(change.FromDate);
            change.Type = (int)LevelGradeChangeType.SalaryStructureChange;

            int seq = 0;
            foreach (PromotionBO item in items)
            {              
               
                if (string.IsNullOrEmpty(item.StepGrade))
                {
                    SetWarning(lblMsg, string.Format("New Grade is required for the employee {0}.", item.EmployeeName));
                    return;
                }              

                LevelGradeChangeDetail emp = new LevelGradeChangeDetail();
                emp.EmployeeId = item.EmployeeId;
                emp.PrevDesignationId = int.Parse(item.PrevDesignationId);
                emp.FromDate = change.FromDate;
                emp.FromDateEng = change.FromDateEng;
                emp.PrevLevelId = int.Parse(item.PrevLevelId);
                emp.PrevStepGrade = double.Parse(item.PrevGrade);


                emp.LevelId = emp.PrevLevelId;

                bool allowDecimalInGrade = NewPayrollManager.AllowDecimalInGrade();

                if(!allowDecimalInGrade && item.StepGrade.Contains("."))
                {
                    SetWarning(lblMsg, string.Format("New Grade {1} not valid for the employee {0}.", item.EmployeeName, emp.StepGrade));
                    return;
                }

                if (allowDecimalInGrade && item.StepGrade.Contains("."))
                {
                    string[] splitString = item.StepGrade.Split('.');

                    int numberValue = int.Parse(splitString[0]);
                    int decimalValue = int.Parse(splitString[1]);

                    if (numberValue == 0)
                    {
                        SetWarning(lblMsg, string.Format("New Grade {1} not valid for the employee {0}.", item.EmployeeName, emp.StepGrade));
                        return;
                    }

                    if (decimalValue != 0 && decimalValue != 5)
                    {
                        SetWarning(lblMsg, string.Format("New Grade {1} not valid for the employee {0}.", item.EmployeeName, emp.StepGrade));
                        return;
                    }
                }

                emp.StepGrade = double.Parse(item.StepGrade);


                BLevelRate rate = NewPayrollManager.GetLevelRate(emp.LevelId.Value,change.FromDateEng.Value);

                if (allowDecimalInGrade)
                {
                    double maxStepGrade = double.Parse(rate.NoOfStepsGrade == null ? "0" : rate.NoOfStepsGrade.Value.ToString()) + 0.5;

                    if (emp.StepGrade > maxStepGrade)
                    {
                        SetWarning(lblMsg, string.Format("New Grade {1} not valid for the employee {0}, max grade value is {2}.", item.EmployeeName, emp.StepGrade, maxStepGrade));
                        return;
                    }
                }
                else
                {
                    if (emp.StepGrade > rate.NoOfStepsGrade)
                    {
                        SetWarning(lblMsg, string.Format("New Grade {1} not valid for the employee {0}, max grade value is {2}.", item.EmployeeName, emp.StepGrade, rate.NoOfStepsGrade));
                        return;
                    }
                }

                emp.Type = (int)LevelGradeChangeType.SalaryStructureChange;
                emp.Status = (int)LevelGradeChangeStatusType.Saved;

                emp.SequenceNo = (seq++);

                change.LevelGradeChangeDetails.Add(emp);
            }

            if (change.LevelGradeChangeDetails.Count <= 0)
            {
                SetWarning(lblMsg, string.Format("Employee(s) must be selected for salary chnage Grade Fitting."));
                return;
            }

            bool isInsert = true;
            Status status = SalaryManager.InsertUpdateGradeFitting(change, ref isInsert);

            if (status.IsSuccess)
            {
                if (isInsert)
                    SetMessage(lblMsg, string.Format("Grade fitting saved for {0} employees.", status.Count));
                else
                {
                    SetMessage(lblMsg, string.Format("Grade fitting changed for {0} employees.", status.Count));
                }
            }
            else
            {
                SetWarning(lblMsg, status.ErrorMessage);
            }
        }


     
    }
}